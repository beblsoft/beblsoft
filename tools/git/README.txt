OVERVIEW
===============================================================================
Git Technology Documentation
- Description                       : Git is a version control system (VCS) that records changes to a file or
                                      set of files over time so that you can recall specific versions
                                      later. Git is a distributed VCS.
                                    : Git is fundamentally a content addressable filesystem with a
                                      VCS interface on top of it.
- Installation ----------------------
  * Fedora                          : sudo yum install git
  * Ubuntu                          : sudo apt-get install git
  * Useful scripts                  : See $TOOLS/bash/git.sh
- Books -----------------------------
  * Pro Git, Scott Chacon           : Basis for much of this documentation
- Relevant URLs ---------------------
  * Home                            : https://git-scm.com/
  * Documentation                   : https://git-scm.com/doc



THREE FILE STATES
===============================================================================
- 3 parts of a git project          : -------------            -------------             -------------
                                      | Working   |  add--->   |Staging    |  commit-->  |Repository |
                                      | Directory |            |Area       |             | .git      |
                                      |           |  <-reset   |           |             | directory |
                                      -------------            -------------             -------------
- Basic Workflow                    1. You modify files in your working directory.
                                    2. You stage the files, adding snapshots of them to your
                                       staging area.
                                    3. You do a commit, which takes the files as they are in the
                                       staging area and stores that snapshot permanently to your
                                       Git directory.



CONFIGURATION
===============================================================================
- Configuration help                : man git-config
- Configuration Files
  1. Repo config                    : .git/config
  2. User config                    : ~/.gitconfig or ~/.config/git/config
                                      git config --global
  3. System config                  : /etc/gitconfig
                                      git config --system
- Set username                      : git config --global user.name "Jamester Bensson"
- Set email                         : git config --global user.email jbensson@beblsoft.com"
- Set editor                        : git config --global core.editor emacs
- List configuration                : git config --list
- Help                              : git help <verb>, man git-<verb>
- Create an alias                   : git config --global alias.<alias-name> '<command-name>'
                                      git config --global alias.unstage 'reset HEAD '
                                      git unstage <filename>
- Create an external command alias  : git config --global alias.<alias-name> '!<command-name>'
                                      git config --global alias.visual '!gitk'
                                      git visual
- Set a template commit message     : git config --global commit.template <template-file>
- Set global git ignore file        : git config --global core.excludes <file path>
- Turn on git auto correct          : git config --global help.autocorrect 1
- Enable git command colors         : git config --global color.ui <true|false>
- Set the merge tool                : git config --global merge.tool <tool-name>
- Set CR/LF conversions             : git config --global core.autocrlf <true|false|input>
- Enable whitespace settings        : git config --global core.whitespace <whitespace-issues>
                                      whitespace-issues (default): blank-at-eol, blank-at-eof, space-before-tab
                                      whitespace-issues (non-default): indent-with-non-tab (NO), cr-at-eol, tab-in-indent
- Force git to receive file
  system objects                    : git config --system receive.fsckObjects
- Deny nonfastforward merges
  (force rebase)                    : git config --system receive.denyNonFastForwards <true|false>
- Deny branch deletes               : git config --system receive.denyDeletes <true|false>
- Track common resolutions          : git config --global rerere.enable true
- Git attribute                     : .gitattributes is a git attribute file specific to a directory
                                      in a git repository.
                                    : Can be used to specify diffs on non-text files (see book p. 289-291)
                                    : Good for filtering content before check-in and after check-out
                                    : A smudge filter runs on a file when it is checked-out
                                      (see book p. 292-293)
                                    : A clean filter runs on the file when it is staged
- Set a smudge or clean filter      : git config --global filter.<filter-name>.<clean|smudge> <program>
- Git Hooks                         : .git/hook/<hook-name> are the programs that run git hooks
                                    : Allow you to do the following: check commit messages, notify
                                      developers via email, validate set of update,
                                      ensure correct rebasing, start continuous integration,
                                      udpate bug system, enforce ACL
                                    : Client-side hooks (See book, p. 295): pre-commit, prepare-commit-msg,
                                      commit-msg, post-commit, applypatch-msg, pre-applypatch, post-applypatch,
                                      pre-rebase, post-rewrite, post-checkout, post-merge, pre-push, pre-auto-gc
                                    : Server-side hooks (See book, p. 298): pre-receive, update, post-receive



SETUP REPOSITORY
===============================================================================
 - Turn directory into git project  : git init
 - Clone an existing repository     : git clone <repo>
                                      git clone https://github.com/libgit2/libgit2



COMMITING
===============================================================================
- Commit message rules              : third person
                                    : present tense
                                    : single concise line (<=50 chars) followed by blank line, followed by an optional
                                      more detailed explanation
- Commit change(s) from staging to
  repository                        : git commit -m "comment/message"
                                      -a: add all files changed in working directory before committing
                                      -v: shows a reminder of what has been committed
- Amending a previous commit        : git commit --amend
- Rebase a single commit onto
  current branch                    : git cherry-pick <commit SHA>



MERGING
===============================================================================
- Merge a branch into the current
  branch                            : git merge <branch>
- Backout the merge                 : git merge --abort
- Show the different files involved
  in the merge                      : git show :<stage>:<file>
                                      stage 1 - common ancester
                                      stage 2 - current branch version
                                      stage 3 - version being merged in
- Merge all delineation             : git merge-file <our file> <common file> <their file>
- Compare the working directory with
  the last commit in current branch : git diff --ours
- Compare the working directory with
  the last commit in their branch   : git diff --theirs
- Compare the working directory with
  the common ancestor               : git diff --base
- Add base conflict resolution
  markers to the file               : git checkout --conflict=diff3 <filename>
- Show the commits in HEAD vs. the
  commits in MERGE_HEAD (aka theirs): git log --oneline --left-right HEAD...MERGE_HEAD
- Revert a merge after it has been
  commited                          : git revert



STASHING
===============================================================================
- Description                       : Stashing allows you to save the dirty state of a working
                                      directory in a stashed stack.
                                    : You can subsequently switch branches and then come back
                                      later to your saved stash.
- Save working directory in a stash : git stash
                                      git stash save
- Only stash working directory, NOT
  the staging area                  : git stash save --keep-index
- Include untracked files in the
  stash                             : git stash save --include-untracked
- Interactively create a stash      : git stash --patch
- Get stash list                    : git stash list
- Apply the most recent stash       : git stash apply
- Apply the nth stash in the stash
  stack                             : git stash apply stash@{n}
- Apply stash changes to the staging
  area                              : git stash apply --index
- Unapply a stash                   : git stash show -p stash@{n} | git apply -R
- Drop the nth stash in the stash
  stack                             : git stash drop stash@{n}
- Apply a stash and drop it
  simultaneously                    : git stash pop
- Create a branch from a stash      : git stash branch <branch-name>
- Save all working directory changes
  in a stash                        : git stash --all



BRANCHING
===============================================================================
- Description                       : A branch is a pointer to a git commit
                                    : HEAD always points to the current branch
                                    : master is the default branch
                                    : When you create a new branch, it creates the branch off
                                      the current branch, NOT the master.
                                    : Switching branches restores working directory to new branch.
                                    : Switching branches will remove only COMMITTED files *
- Example Picture                   :
                                                                             issue1, HEAD
                                                                                |
                                                                                v
                                                                       ----   ----
                                                                       |c5|<--|c6|
                                                                       ----   ----
                                                                      /
                                      ----   ----   ----   ----   ----   ----
                                      |c0|<--|c1|<--|c2|<--|c3|<--|c4|<--|c7|
                                      ----   ----   ----   ----   ----   ----
                                                                          ^
                                                                          |
                                                                        master
- List all git project branches			: git branch
- Create new branches 					    : git branch <branch-name> <commit-sha>
- Switch from one branch to another : git checkout <branch-name>
- Merge one branch into current
  branch                            : git merge <branch-name>
- Delete a branch                   : git branch -d <branch-name>
- Create and checkout branch        : git checkout -b <branch-name>
- Show last commit on each branch   : git branch -v
- Show branches that have been
  merged into current branch        : git branch --merged
- Show branches that have NOT been
  merged into current branch        : git branch --no-merged
- Run a command on each commit
  in the branch                     : git filter-branch --tree-filter <command> <branch>
- Run a bash script on each commit
  in a branch                       : git filter-branch --commit-filter '<script>' <branch>
- Push local branch to remote       : git push <remote-name> <branch-name>
                                      git push origin bug1234
- Delete remote branch              : git push <remote-name> --delete <branch-name>



RESET
===============================================================================
                                             Index
                                      HEAD | Staging | Working Dir | Working Dir Safe
                                    -------|---------|-------------|-----------------
- git reset --soft [commit]         :  REF |   No    |      No     |     Yes
- git reset [--mixed] [commit]      :  REF |   Yes   |      No     |     Yes
- git reset --hard [commit]         :  REF |   Yes   |      Yes    |     No
- git checkout [commit]             :  HEAD|   Yes   |      Yes    |     Yes
- git reset [commit] [file]         :  NO  |   Yes   |      No     |     Yes
- git checkout [commit] [file]      :  NO  |   Yes   |      Yes    |     No

- Example Picture ------------------: Tracking 2 files foo,bar
                                                        Branch0                       Branch1, HEAD
                                                         |                                   |
                                                         v                                   v
                                                        ----             ----               ----
                                                        |c0|<------------|c1|<--------------|c2|
                                                        ----             ----               ----
                                           Last Commit: fooV0, barV0     fooV1, barV1       fooV2, barV2
                                       Index (Staging): ----             ----               fooI2, barI2
                                           Working Dir: ----             ----               fooW2, barW2

                            Branch0|Branch1|HEAD|Last Commit |Index (Staging)|Working Dir |Motivation
- git reset --soft c0       c0     |c0     |c0  |fooV0, barV0|fooI2, barI2   |fooW2, barW2|Setup to squash commit c1
- git reset c0              c0     |c0     |c0  |fooV0, barV0|fooV0, barV0   |fooW2, barW2|Setup to squash commit c1, resetting staging
- git reset --hard c0       c0     |c0     |c0  |fooV0, barV0|fooV0, barV0   |fooV0, barV0|Want to erase changes and go back to commit c0
- git checkout Branch0      c0     |c2     |c0  |fooV0, barV0|fooV0, barV0   |fooV0, barV0|Go work on Branch0, keeping Branch1 alive
- git reset c0 foo          c0     |c2     |c2  |fooV2, barV2|fooV0, barV2   |fooW2, barW2|Reset foo in staging
- git checkout c0 foo       c0     |c2     |c2  |fooV2, barV2|fooV0, barV2   |fooV0, barW2|Reset foo in staging and working dir



REMOTES
===============================================================================
- Description:                      : origin in the default remote name
                                    : upstream refers to the remote branch tracked by local (downstream) branch;
                                      upstream branches can be referenced by @{upstream} or @{u}
                                    : fetching retrieves remote changes; merging combines local a fetched changes;
                                      pulling does a combined fetch and merge
                                    : book suggests namespacing remote branches with developer initials, origin/db/bug26
- Create a local copy of a remote   : git clone <remote-path> <clone-name>
- List a Git project's remotes 			: git remote -v
- Add a remotes                     : git remote add <remote-name> <remote-path>
- Remove a remote                   : git remote rm <remote-name>
- Rename a remote                   : git remote rename <old-name> <new-name>
- Inspect a remote                  : git remote show <remote-name>
- Fetch work from the remote to
  local copy. (origin assumed)      : git fetch [<remote-name>]
- Merge remote-name/branch-name
  into local-branch                 : git merge <remote-name>/<branch-name>
                                      git merge origin/master
- Push local branch to remote       : git push <remote-name> <branch-name>
                                      git push origin bug1234
- Push local branch to different
  remote brch                       : git push -u <remote-name> <local-branch>:<remote-branch>
                                      git push -u origin featureB:featureBee
- Issue pull request to have remote
  feature branch merged into remote
  development branch (our code
  review process)                   : git request-pull <remote>/<development-branch> <remote>/<feature-branch>
- Delete a remote branch            : git push <remote-name> --delete <branch-name>
                                      git push origin --delete bug1234
- Create a local branch to track
  remote branch                     : git checkout -b <local-branch> <remote-name>/<remote-branch>
                                      git checkout -b localDev origin/Development
- Change the upstream branch that
  local branch is tracking
  (may not use, danger!)            : git branch -u <remote-name>/<remote-branch>
                                      git branch -u origin/Production
- To sync up with remote branches   : git fetch --all; git branch --all git st-vv
- Delete stale remote references    : git remote prune origin



GITIGNORE
===============================================================================
- Description                       : .gitignore is a file that specifies file patterns
                                      that Git should ignore.
- Rules                             : blank lines or lines starting with # are ignored.
                                    : standard glob patterns work.
                                    : you can end patterns with a forward slash to specify a directory.
                                    : you can negate a pattern by starting it with an !.
- Example                           : See ./sample.gitignore for example
- Find what .gitignore is ignoring
  file                              : git check-ignore -v foo.txt



TAGGING
===============================================================================
- Show all tags                     : git tag
                                    : git show <tag-name>
- Search for tag matching regular
  express.                          : git tag -l '<regex>'
- Create an annotated tag (contains
  name, email, date, checksum,
  GPG signature)                    : git tag -a <tag-name> -m '<tag-message>'
- Create a lightweight tag (ptr to
  commit)                           : git tag <tag-name>
- Tagging a commit made in the past : git tag -a <tag-name> <commit-SHA>
- Sharing tags                      : git push <remote-name> --tags
- Rename tag                        : git tag <new-name> <old-name>
- Delete tag [local]                : git tag -d <tag-name>
- Delete tag [remote]               : git push <remote-name> :refs/tags/<tag-name>



REBASING
===============================================================================
- Description                       : Two ways to integrate code from one branch to another: merging and rebasing.
                                    : Merging performs a three-way merge between the two latest snapshots (c3 and c6)
                                      and the most recent common ancestor of the two (c1).
                                    : Rebasing takes the patch of one branch and applies it on to another, i.e.,
                                      applying c4', c5', and c6' onto c3.  This cleans up the log history making
                                      all work appears as if it happened in a series.
                                    : In general the way to get the best of both worlds is to rebase local changes
                                      you've made but haven't shared yet BEFORE you push push them in order to clean up
                                      your story, but NEVER rebase anything you've pushed somewhere.
                                    : When rebasing changes from remote to local, changes made remotely since original
                                      snapshot are integrated into the local stream AHEAD of local changes.
                                    : See book pg. 71 for indepth example.
                                    : See book pg. 73 for resolving rebase conflicts.
                                    : Clean it up before you push it up! Rebase branches before pushing.
- Rebase the source-branch onto
  another dest-branch               : git rebase <dest-branch>
                                      git rebase master
- Fast forward merge the dest-branch: git checkout master
                                      git merge <source-branch>
- Rebase source-branch onto dest
  branch; this checks out src-branch
  and replays its changes onto
  dest-branch                       : git rebase <dest-branch> <source-branch>
- Pull the remote repository and
  rebase changes on top of it       : git pull --rebase
- Set rebasing as the default for
  pull                              : git config --global pull.rebase true
- Interactive Rebasing (commands:
  squash, split, edit, or reorder
  commits)                          : git rebase -i
- Regular Merge Picture -------------------------------------------------------

                     Branch: Development
                           |
                           v
    ----   ----   ----   ----       ----
    |c0|<--|c1|<--|c2|<--|c3| <-----|c7|
    ----   ----   ----   ----       ----
               \                   /
                ----   ----   ----
                |c4|<--|c5|<--|c6|
                ----   ----   ----
                               ^
                               |
                         Branch: Bug1234

- Rebasing Picture ------------------------------------------------------------

                    Branch: Development
                          |
                          v
    ----   ----   ----   ----   -----   -----   ----
    |c0|<--|c1|<--|c2|<--|c3|<--|c4'|<--|c5'|<--|c7|
    ----   ----   ----   ----   -----   -----   ----
                                                  ^
                                                  |
                                            Branch: Bug1234

- Fast-forward Merge Picture --------------------------------------------------

                                            Branch: Development
                                                 |
                                                 v
    ----   ----   ----   ----   -----   -----   ----
    |c0|<--|c1|<--|c2|<--|c3|<--|c4'|<--|c5'|<--|c7|
    ----   ----   ----   ----   -----   -----   ----
                                                  ^
                                                  |
                                            Branch: Bug1234



DEBUGGING
===============================================================================
- LOG ------------------------------: Shows history of commits
  * View the committed history      : git log
  * Show differences in each commit : git log -p
  * Show last n commits             : git log -<n>
  * Show summary for each commit    : git log --stat
  * Pretty logging                  : git log --pretty=<option>
                                      options=[oneline, short, full, fuller, format]
                                      git log --pretty=format: "%h - %an, %ar : %s"
  * Show commits since/after a date : git log --since=<time>
                                      git log --since=2.weeks
                                              --after=<time>
  * Show commits until/before       : git log --until=<time>
                                              --before=<time>
  * Show commits authored by X      : git log --author <X>
  * Show commits committed by X     : git log --committer <X>
  * Show commits with specific
    commit string                   : git log --grep <string>

  * Show ASCII Art Graph of commits : git log --graph
  * Search for a string in commit   : git log --S<function name>
  * Show differences between branch
    1 and 2                         : git log <branch-1>..<branch-2>
  * Show commits in branch 1 that
    are not in branch 2             : git log <branch-1> --not <branch-2>
  * Show a summary of changes within
    range                           : git shortlog <branch> --not <release-tag>
  * Picture                         :

                                                                         branch1
                                                                           |
                                                                           v
                                       ----   ----   ----   ----   ----   ----
                                       |c0|<--|c1|<--|c2|<--|c3|<--|c4|<--|c7|
                                       ----   ----   ----   ----   ----   ----
                                                       ^
                                                       |     ----   ----   -----
                                                       |---  |c8|<--|c9|<--|c10|
                                                             ----   ----   -----
                                                                            ^
                                                                            |
                                                                          branch0
  * Show all commits that are in
    branch 0 and not in branch 1
    (c8, c9, c10)                   : git log branch1..branch0
                                    : git log branch0 --not branch1
  * Show all commits that are in
    either branch 1 or branch 0 but
    not in both (all but c0 and c1) : git log --left-right branch0...branch1
  * Show commits that contain a
    specific string                 : git log -S<string>
                                      git log -G<regex>
  * Show commits that modified a
    function and file               : git log -L :<function>:<file>
- SHOW -----------------------------: Show/displays details about a specific commit
                                    : Commits can be specified in various formats:
                                      - HEAD is the head commit
                                      - HEAD@{n} is the head commit n steps previous
                                      - HEAD^n is the nth parent of HEAD, think mom, dad
                                      - HEAD~n is the nth parent of HEAD, think parent, grandparent, great grandparent
  * Show a specific commit          : git show <SHA>
                                      git show <tag>
  * Show files affected by a commit : git show --name-only <SHA>
  * Show history of HEAD pointer    : git reflog
- DIFF -----------------------------: See file differences
  * See differences between stagging
    area and last commit            : git diff --staged
  * See differences between working
    directory and stagging area     : git diff [<filename>]
  * See differences between index
    and last commit (HEAD)          : git diff --cached
  * Identify whitespace errors      : git diff --check
  * Show differences between branch
    1 and the common ancester of
    branch 1 and branch 2           : git merge-base <branch-1> <branch-2>
                                      git diff <branch-2>...<branch1>
- BLAME -----------------------------
  * Show last commit on each line
    give a range of lines           : git blame <file> -L  <start>,<stop>
- BISECT ----------------------------
  * Mark last known good commit
    and first known bad commit      : git bisect start <bad-tag> <good-tag>
  * Run binary (0 good, 1 bad) test
    script to determine if each
    commit is good                  : git bisect run <script>



MISCELLANEOUS
===============================================================================
- PATCHING --------------------------
  * Create a patch file from where-
    branch to current branch        : git format-patch -M <where-branch>
  * Send an email with patch files  : git send-email *.patch
  * Apply a patch transactionally   : git apply <patch-name>
  * Check if patch will cleanly
    apply without applying it       : git apply --check <patch-name>
  * Apply with 'am' to manual go
    through merge conflicts         : git am <patch-name>
  * Resolve merge conflicts         : git am --resolve

- RELEASE ---------------------------
  * Generate a build number         : git describe <branch>
                                      Generates: <last-tag>-<#commits-ahead>-<commit SHA>
                                      v1.6.2-rc1-20-g8c5b85c
  * Create an archive               : git archive <branch>
  * Tag a branch for release        : git tag -a <tag-name> <object SHA>
  * Have git ignore files in an
    archive                         : echo '<directory> export-ignore' >> .gitattributes
  * Have git update file on archive
    creation                        : echo '<file-name> export-subst' >> .gitattributes (See book p. 294)

- BUNDLES ---------------------------
  * Description                     : Provides a mechanism to containerize and share commits without a network
  * Create a bundle                 : git bundle create <bundle-file>
  * Clone an existing bundle        : git clone <bundle-file>
  * Verify that the bundle can be
    applied on the current repo     : git bundle verify <bundle-file>
  * Fetch the bundle changes into
    the current repository          : git fetch <bundle-file>

- RERERE ---------------------------: Reuse recorded resolution
  * Show files modified by merge    : git rerere status
  * Show changes made by rerere     : git rerere diff
  * Enable rerere                   : git config --global rerere.enable true

- SIGNING ---------------------------
  * TList gpg keys                  : gpg --list-keys
  * Export a key                    : gpg -a --export <key SHA>
  * Set personal signing key        : git config --global user.signingkey <gpg key ID>

- OBJECTS ---------------------------
  * Import an object into git       : git hash-object
  * Convert a tag to a SHA          : git rev-parse <tag>

- CLEAN -----------------------------
  * Clean out all the changes in
    working directory               : git clean
                                      -i: interactive
                                      -d: remove directories
                                      -f: force
                                      -n: dry run
                                      -x: remove files in the gitignore

  * Clean Beblsoft dry run          : cd $BEBLSOFT; git clean -ndfx *

- SEARCH ----------------------------
  * Search for text in files tracked
    by git                          : git grep [options] <regex>
                                      --count: show number of matches
                                      -n: shows line #
                                      -p: shows containing function
- REV-PARSE -------------------------
  * Show git directory              : git rev-parse --git-dir   # /home/jbensson/git/beblsoft/.git
  * Show HEAD sha                   : git rev-parse HEAD        # 1a2bd7f06f148b1c766a28a4124cb21996fecf86



INTERNALS
===============================================================================
- .git directory contents           : HEAD        - current checked-out branch
                                    : config      - project-specific config
                                    : description - git web description
                                    : hooks/      - client/server hooks
                                    : info/       - global exclude info
                                    : objects/    - all git db content
                                    : refs/       - pointer to commit objects
                                    : git is a key/value store; each object is given a hash; objects
                                      are stored in .git/objects/<SHA2>/<SHA38>
- Hash an object                    : git hash -object
- Cat an object file                : git cat-file
- Add to staging                    : git update-index (git add)
- Write a tree object from an index : git write-tree
- Create a commit object to point at
  a tree object                     : git commit-tree (git commit)
- Tree                              : git is composed of tree objects pointing to tree objects
                                      pointing to blob objects, e.g.,
                                      ----   ----   ----
                                      |c0|<--|c1|<--|c2|
                                      ----   ----   ----
                                                      |
                                                   --------
                                                   | tree |
                                                   --------
                                                  /    |     \
                                                 /     |      \  ...
                                            ------  ------  --------
                                            |blob|  |blob|  | tree |
                                            ------  ------  --------
                                                             /    |
                                                            /     |
                                                           /      |
                                                        ------  ------
                                                        |blob|  | blob|
                                                        ------  -------
- git references                    : stored in .git/refs
                                      heads/   - branch pointers to commit objects (mater, test, branch1)
                                      tags/    - tag pointers to any git object
                                      remotes/ - stores all remote references
- git object storage                : git initially stores objects in a loose object format
                                    :  any change in a single file results in another file being stored
                                    :  git gc efficiently stores like files in pack files
                                    :  pack files are located in .git/objects/info/packs
- git gc command does 3 things      1 converts loose objects into pack files
                                    2 combines pack files into larger pack files
                                    3 removes objects that are not reachable and months old
- git transfer protocols            : 2 exist: smart and dumb
  * smart upload                    : spawns 2 processes: client git send-pack,
                                      and server git receive-pack.
                                      These 2 processes communicate together to transfer the minimal
                                      amount of data needed.
  * smart download                  : spawns 2 processes: client git fetch-pack, and server git upload-pack.
                                      Again, these 2 processes minimize data transfer




