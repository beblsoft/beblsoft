OVERVIEW
===============================================================================
- GitLab Runner Technology Documentation
- Description                       : GitLab runner is the open source project that is used to run your
                                      jobs and send the results back to gitlab
                                    : It is used in conjunction with GitLab CI, the open source continuous
                                      integration service included with GitLab that coordinates the jobs
                                    : Written in Go and can be run as a single binary, no language specific
                                      requirements are needed
                                    : Designed to run on the GNU/Linux, maxOS, and Windows operating systems
                                      If Docker used, requires Docker > v1.5.0

- Features --------------------------
  * Allows to run                   : Multiple jobs concurrently, multiple tokens with multiple server,
                                      limit number of concurrent jobs per-token
  * Jobs can be                     : Local, docker container, docker container over ssh, docker container with
                                      autoscaling on different cloud platforms
  * Shell support                   : Supports Bash, Windows Batch, and Windows PowerShell
  * OS support                      : Works on GNU/Linux, macOS, and Windows
  * Config reload                   : Reload config without restart
  * Prometheus metrics              : Embedded HTTP server
- Install ---------------------------
  * Install with gitLab.sh          : gitLabRunner_install
  * Remove with gitLab.sh           : gitLabRunner_uninstall
  * Debug installation              : cat /var/log/syslog
- Relevant URLS ---------------------
  * Documentation                   : https://docs.gitlab.com/runner/
  * Install Docs                    : https://docs.gitlab.com/runner/install/linux-repository.html
  * TOML Spec                       : https://github.com/toml-lang/toml
  * Autoscaling on AWS              : https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/index.html
  * Speed Up                        : https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9
  * Docker Configurations           : https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/docker/using_docker_build.md


EXECUTORS
===============================================================================
- Description                       : GitLab Runner implements a number of executors that can be used to run your
                                      builds in different scenarios
- Selecting an executor ------------: Shell   VirtualBox  Parallels  Docker Kubernetes
  * Clean env for each build        : N       Y           Y          Y       Y
  * Migrate runner machine          : N       partial     partial    Y       Y
  * Easily concurrent               : N       Y           Y          Y       Y
  * Debugging build problems        : easy    hard        hard       medium  medium
  * Secure Variables                : Y       Y           Y          Y       Y
  * GitLab Runnner Exec             : Y       N           N          Y       Y
  * gitlab-ci.yml: image            : N       N           N          Y       Y
  * gitlab-ci.yml: services         : N       N           N          Y       Y
  * gitlab-ci.yml: cache            : Y       Y           Y          Y       Y
  * gitlab-ci.yml: artifacts        : Y       Y           Y          Y       Y
  * Absolute paths: caching,
    artifacts                       : N       N           N          N       Y
  * Passing artifacts between stages: Y       Y           Y          Y       Y
  * Use GitLab Container Registry
    private images                  : N/A     N/A         N/A        Y       Y
  * Interactive Web terminal        : Bash    N           N          Y       Y
- Executors -------------------------
  * Shell Executor                  : Shell is the simple executor to configure. All required dependencies for your
                                      builds need to be installed manually on the same machine that the runner is
                                      installed on
  * Virtual Machine Executor
    (VirtualBox/Parallels)          : Use already created virtual machine, which is cloned and used to run your build
                                      Useful to run builds on different operating systems (Windows, Linux, OSX, or FreeBSD)
  * Docker Executor                 : Use Docker for build environment. Provides clean build environment with
                                      easy dependency management (all dependencies are put in Docker image)
  * Docker Machine                  : Special version of the Docker executor with support for auto-scaling
                                      It works like the normal Docker executor but with build hosts created on
                                      demand by Docker Machine
  * Kubernetes Executor             : Use an existing Kubernetes cluster for your builds.
                                      Executor calls the Kubernetes cluster API and create a new Pod for each
                                      GitLab CI job



DOCKER EXECUTOR
===============================================================================
- Description                       : GitLab Runner can use Docker to run builds on user provided images
                                    : The Docker executor when used with GitLab CI, connects to Docker Engine
                                      and runs each build in a separate and isolated container using the predefined
                                      image that is set up in .gitlab-ci.yml and in accordance with config.toml
- Workflow                          : Docker executor divides the build into multiple steps
  1. Prepare                        : Create and start the services
  2. Pre-build                      : Pre-build: Clone, restore cache and download artifacts from previous stages
                                      This is run on a special docker image
  3. Build                          : User build. This is run on the user-provided docker image
  4. Post-build                     : Create cache, upload artifacts to GitLab. Run on a special Docker image
- Image Keyword                     : Name of the Docker image that is present in the local Docker Engine
- Services Keyword                  : Defines just another Docker image that is run during build and is linked
                                      to the Docker image that the image keyword defines
- Builds and cache storage ----------
  Builds stored in                  : /builds/<namespace>/<project-name> (inside container)
                                      Modified in builds_dir [config.toml]
  Caches in                         : /cache                             (inside container)
                                      Modified in cached_dir [config.toml]
- Persistent storage ---------------: Docker executor can provide a persistent storage when running the containers
                                      All directories under volumes = will be persistent between builds
  Dynamic Storage                   : volumes = <path>
                                      Is persistent between subseuqnet runs of the same concurrent job for project
                                      Data attached to cache container:
                                      runner-<short-token>-project-<id>-concurrent-<job-id>-cache-<unique-id>
  Host bound storage                : volumes = <host-path>:<path>[:<mode>]
                                      The <path> is bind to <host-path> on the host system
                                      Mode can specify read-only or read-write(default)
- Privileged Mode ------------------: The Docker executor supports a number of options that allows to fine tune the
                                      build container. One is privileged mode
  Docker-in-docker with privileged  : config.toml
                                      [[runners]]
                                        executor = "docker"
                                        [runners.docker]
                                          privileged = true
                                    : .gitlab-ci.yml
                                      image: docker:git
                                        services:
                                        - docker:dind

                                        build:
                                          script:
                                          - docker build -t my-image .
                                          - docker push my-image
- Pull Policy ----------------------: pull_policy parameter defines how the runner will work when pulling Docker images
  never                             : disables images pulling completely. If image not found locally, runner fails
  if-not-present                    : Runner first checks to see if image is present locally
  always                            : Ensures that image is always pulled (default)



CONFIG.TOML
===============================================================================
- File -----------------------------: GitLab Runner configuration uses TOML format
  * root                            : /etc/gitlab-runner/config.toml
  * non-root                        : ~/.gitlab-runner/config.toml
  * non-*nix systems                : ./config.toml
- Global Section -------------------:
  * concurrent                      : limits how many jobs globally can be run concurrently.
                                      The most upper limit of jobs using all defined runners. 0 does not mean unlimited
  * log_level                       : Log level (options: debug, info, warn, error, fatal, panic).
                                      Note that this setting has lower priority than level set by command
                                      line argument --debug, -l or --log-level
  * log_format                      : Log format (options: runner, text, json). Note that this setting has lower
                                      priority than format set by command line argument --log-format
  * check_interval                  : defines the interval length, in seconds, between new jobs check.
                                      The default value is 3; if set to 0 or lower, the default value will be used.
  * sentry_dsn                      : enable tracking of all system level errors to sentry
  * listen_address                  : address (<host>:<port>) on which the Prometheus metrics HTTP server
                                      should be listening
  * Example                         : concurrent = 4
                                      log_level = "warning"
- [session_server] section ---------: System runner level configuration, should be specified at the root level
  * listen_address                  : An internal URL to be used for the session server.
  * advertise_address               : The URL that the Runner will expose to GitLab to be used to
                                      access the session server. Fallbacks to listen_address if not defined.
  * session_timeout                 : How long in seconds the session can stay active after the
                                      job completes (which will block the job from finishing),
                                      defaults to 1800 (30 minutes).
  * Example                         : [session_server]
                                        listen_address = "0.0.0.0:8093" #  listen on all available interfaces on port 8093
                                        advertise_address = "runner-host-name.tld:8093"
                                        session_timeout = 1800
- [[runners]] section --------------: Defines one runner entry
  * name                            : The Runner’s description, just informatory
  * url                             : GitLab URL
  * token                           : The Runner’s special token (not to be confused with the
                                      registration token)
  * tls-ca-file                     : File containing the certificates to verify the peer when using HTTPS
  * tls-cert-file                   : File containing the certificate to authenticate with the peer
                                      when using HTTPS
  * tls-key-file                    : File containing the private key to authenticate with the peer
                                      when using HTTPS
  * limit                           : Limit how many jobs can be handled concurrently by this token.
                                      0 (default) simply means don’t limit
  * executor                        : Select how a project should be built, see next section
  * shell                           : The name of shell to generate the script (default value is platform
                                      dependent)
                                      bash       - default for all Unix systems
                                      sh         - fallback for bash on Unix systems
                                      cmd        - Windows Batch script, default for windows
                                      powershell -
  * builds_dir                      : Directory where builds will be stored in context of selected executor
                                      (Locally, Docker, SSH)
  * cache_dir                       : Directory where build caches will be stored in context of selected
                                      executor (locally, Docker, SSH). If the docker executor is used, this
                                      directory needs to be included in its volumes parameter.
  * environment                     : Append or overwrite environment variables
  * request_concurrency             : Limit number of concurrent requests for new jobs from GitLab (default 1)
  * output_limit                    : Set maximum build log size in kilobytes, by default set to 4096 (4MB)
  * pre_clone_script                : Commands to be executed on the Runner before cloning the Git
                                      repository. this can be used to adjust the Git client configuration
                                      first, for example. To insert multiple commands, use a (triple-quoted)
                                      multi-line string or “\n” character.
  * pre_build_script                : Commands to be executed on the Runner after cloning the Git repository,
                                      but before executing the build. To insert multiple commands, use a
                                      (triple-quoted) multi-line string or “\n” character.
  * post_build_script               : Commands to be executed on the Runner just after executing the build,
                                      but before executing after_script. To insert multiple commands, use a
                                      (triple-quoted) multi-line string or “\n” character.
  * clone_url                       : Overwrite the URL for the GitLab instance. Used if the Runner can’t connect
                                      to GitLab on the URL GitLab exposes itself.
  * Example                         : [[runners]]
                                        name = "ruby-2.1-docker"
                                        url = "https://CI/"
                                        token = "TOKEN"
                                        limit = 0
                                        executor = "docker"
                                        builds_dir = ""
                                        shell = ""
                                        environment = ["ENV=value", "LC_ALL=en_US.UTF-8"]
                                        clone_url = "http://gitlab.example.local"
- [runners.docker] section ---------: Defines Docker container parameters
  * host                            : Specify custom Docker endpoint, by default DOCKER_HOST environment
                                      is used or unix:///var/run/docker.sock
  * hostname                        : Specify custom hostname for Docker container
  * runtime                         : Specify a runtime for Docker container
  * tls_cert_path                   : When set it will use ca.pem, cert.pem and key.pem from
                                      that folder to make secure TLS connection to Docker (useful in boot2docker)
  * image                           : Use this image to run builds
  * memory                          : String value containing the memory limit
  * memory_swap                     : String value containing the total memory limit
  * memory_reservation              : String value containing the memory soft limit
  * oom_kill_disable                : Do not kill processes in a container if an out-of-memory
                                      (OOM) error occurs
  * cpuset_cpus                     : String value containing the cgroups CpusetCpus to use
  * cpus                            : Number of CPUs (available in docker 1.13 or later)
  * dns                             : A list of DNS servers for the container to use
  * dns_search                      : A list of DNS search domains
  * privileged                      : Make container run in Privileged mode (insecure)
  * disable_entrypoint_overwrite    : Disable the image entrypoint overwriting
  * userns_mode                     : Sets the usernamespace mode for the container when
                                      usernamespace remapping option is enabled.
                                      (available in docker 1.10 or later)
  * cap_add                         : Add additional Linux capabilities to the container
  * cap_drop                        : Drop additional Linux capabilities from the container
  * security_opt                    : Set security options (–security-opt in docker run),
                                      takes a list of ‘:’ separated key/values
  * devices                         : Share additional host devices with the container
  * cache_dir                       : Specify where Docker caches should be stored
                                      (this can be absolute or relative to current working directory).
  * disable_cache                   : The Docker executor has 2 levels of caching: a global one (like any other executor)
                                      and a local cache based on Docker volumes. This configuration flag acts only on the
                                      local one which disables the use of automatically created (not mapped to a
                                      host directory) cache volumes. In other words, it only prevents creating a
                                      container that holds temporary files of builds, it does not disable the cache
                                      if the Runner is configured in distributed cache mode.
  * network_mode                    : Add container to a custom network
  * wait_for_services_timeout       : Specify how long to wait for docker services, set to 0
                                      to disable, default: 30
  * volumes                         : Specify additional volumes that should be mounted
                                      (same syntax as Docker’s -v flag)
  * extra_hosts                     : Specify hosts that should be defined in container environment
  * shm_size                        : Specify shared memory size for images (in bytes)
  * volumes_from                    : Specify a list of volumes to inherit from another container
                                      in the form \<container name\>[:\<ro&#124;rw\>]
  * volume_driver                   : Specify the volume driver to use for the container
  * links                           : Specify containers which should be linked with building container
  * services                        : Specify additional services that should be run with build.
                                      Please visit Docker Registry for list of available applications.
                                      Each service will be run in separate container and linked to the build.
  * allowed_images                  : Specify wildcard list of images that can be specified in .gitlab-ci.yml.
                                      If not present all images are allowed (equivalent to ["*/*:*"])
  * allowed_services                : Specify wildcard list of services that can be specified in .gitlab-ci.yml.
                                      If not present all images are allowed (equivalent to ["*/*:*"])
  * pull_policy                     : Specify the image pull policy: never, if-not-present or always (default);
  * sysctls                         : specify the sysctl options
  * helper_image                    : Override the default helper image used to clone repos and
                                      upload artifacts. Read the helper image section for more details
- [runners.machine] section --------: Defines Docker Machine based autoscaling feature
  * IdleCount                       : Number of machines, that need to be created and waiting in Idle state.
  * IdleTime                        : Time (in seconds) for machine to be in Idle state before it is removed.
  * OffPeakPeriods                  : Time periods when the scheduler is in the OffPeak mode. An array of
                                      cron-style patterns (described below).
  * OffPeakTimezone                 : Time zone for the times given in OffPeakPeriods. A timezone string like
                                      Europe/Berlin (defaults to the locale system setting of the host
                                      if omitted or empty).
  * OffPeakIdleCount                : Like IdleCount, but for Off Peak time periods.
  * OffPeakIdleTime                 : Like IdleTime, but for Off Peak time mperiods.
  * MaxBuilds                       : Builds count after which machine will be removed.
  * MachineName                     : Name of the machine. It must contain %s, which will be replaced
                                      with a unique machine identifier.
  * MachineDriver                   : Docker Machine driver to use. More details can be found in the
                                      Docker Machine configuration section.
  * MachineOptions                  : Docker Machine options. More details can be found in the
                                      Docker Machine configuration section.
- [runners.cache.s3] section -------: Allows to configure S3 storage for cache
  * ServerAddress                   : A host:port to the used S3-compatible server.
  * AccessKey                       : The access key specified for your S3 instance.
  * SecretKey                       : The secret key specified for your S3 instance.
  * BucketName                      : Name of the storage bucket where cache will be stored.
  * BucketLocation                  : Name of S3 region.
  * Insecure                        : Set to true if the S3 service is available by HTTP. Set to false by default.



AUTOSCALE WITH DOCKER MACHINE
===============================================================================
- Description                       : Autoscale provides the ability to utilize resources in a more
                                      elastic and dynamic way
                                    : Thanks to Runners being able to autoscale, infrastructure contains
                                      only as many build instances as necessary at anytime
                                    : If runner is configured to only use autoscale, system on
                                      which the runner is installed acts as a bastion for all the machines
                                      it creates
- Machines on Demand                : Jobs are executed on machines created on demand
                                      Those machines, after the job is finished, can wait to run the next
                                      jobs or can be removed after the configured IdleTime
                                    : Note: Multiple concurrent requests to docker-machine create that are done
                                      at first usage are not good
- Supported cloud providers         : Autoscale mechanism is based on Docker Machine
                                    : Supported providers include: AWS, Azure, Digital Ocean,
                                      Google Compute Engine, ...
- Global options --------------------
  * concurrent                      : Limits how many jobs globally can be run concurrently
                                      This is the most upper limit of number of jobs using all
                                      defined runners, local and autoscale
- [[runnners]] options --------------
  * executor                        : executor must be set to docker+machine
  * limit                           : Limits how many jobs can be handled concurrently by this specific
                                      token. 0 simply means don't limit. For autoscale it's the
                                      upper limit of machines created by this provider
- Autoscaling algorithm and params--: Autoscaling based on 3 parameters: IdleCount, IdleTime, and limit
  * Idle Machines                   : Machine that does not run a job is in idle state
                                      GitLab monitors all machines and ensures that there is always
                                      IdleCount of machines in idle state
                                    : At the same time, Runner is checking the duration of the idle
                                      state of each machine. If the time exceeds the IdleTime value,
                                      the machine is automatically removed
  * Example                         : [[runners]]
                                        limit = 10
                                        (...)
                                        executor = "docker+machine"
                                        [runners.machine]
                                          IdleCount = 2
                                          IdleTime = 1800
                                          (...)
                                    : Beginning: 2 machines are started (IdleCount)
                                    : 5 jobs are queued in GitLab CI. First 2 jobs are sent to idle
                                      machines. Now Idle is less than 2, so runner starts 2 new machines,
                                      and gives out jobs, ...
                                    : Scaling down: after the job is finished, the machine is set to idle
                                      state and is waiting for the next jobs to be executed
                                      After IdleTime (30min = 1800sec) passes, idle machines are removed
  * concurrent, limit, IdleCount    : These three variables determine the upper limit of running machines
    20          40     10           : Max machines = 30 (20 concurrent + 10 idle)
    20          25     10           : Max machines = 25 (20 concurrent + 5 idle)
- Off Peak time mode config --------: Autoscale can be configured with the support for Off Peak time periods
                                    : Most commercial companies are working M-F 10am-6pm
                                      Rest of time, no one is working
                                    : Don't want to pay for Idle machines during off peak times
                                    : When the Off peak time mode is enabled machines scheduler use
                                      OffPeakIdleCount instead of IdleCount
                                      and OffPeakIdleTime instead of IdleTime
  * Example                         : [runners.machine]
                                        IdleCount = 1
                                        IdleTime = 1800
                                        OffPeakPeriods = [
                                          "* * 0-9,18-23 * * mon-fri *",
                                          "* * * * * sat,sun *"
                                        ]
                                        OffPeakIdleCount = 0
                                        OffPeakIdleTime = 1200
- Distributed runners cache --------: To speed up jobs, GitLab runner provides a cache mechanism
                                      where selected directories and/or files are saved and shared
                                      between subsequent jobs
                                    : This is fine when jobs are run on the same host, but when runners
                                      are autoscaled, most jobs will be running on a new host
                                    : To overcome the issue of different hosts, distributed runners
                                      cache introduced
  * Example                         : [[runners]]
                                        limit = 10
                                        executor = "docker+machine"
                                        [runners.cache]
                                          Type = "s3"
                                          Path = "path/to/prefix"
                                          Shared = false
                                          [runners.cache.s3]
                                            ServerAddress = "s3.example.com"
                                            AccessKey = "access-key"
                                            SecretKey = "secret-key"
                                            BucketName = "runner"
                                            Insecure = false
                                    : S3 Cache URL
                                      http(s)://<ServerAddress>/<BucketName>/<Path>/runner/<runner-id>/project/<id>/<cache-key>
- Container registry mirroring -----: To speed jobs executed inside of Docker containers, you can
                                      use the Docker registry mirroring service
                                      Provides a proxy between oOcker machines and all used registries.
                                      Images download once by the registry mirror
                                    : Registry mirror IP address must be available to all machines
  * Example                         : [[runners]]
                                        limit = 10
                                        executor = "docker+machine"
                                        [runners.machine]
                                          (...)
                                          MachineOptions = [
                                            (...)
                                            "engine-registry-mirror=http://10.11.12.13:12345"
                                          ]



COMMAND REFERENCE
===============================================================================
- Generic ---------------------------
  * help                            : gitlab-runner --help
  * command help                    : gitlab-runner <command> --help
  * debug mode                      : gitlab-runner --debug <command>
  * Run as root                     : sudo gitlab-runner run

- Variables, Arguments --------------
  * Environment Variables           : Most commands support environment variables as a method to pass the
                                      configuration to the command
                                      Ex. CONFIG_FILE, REGISTER_NON_INTERACTIVE
  * Boolean Parameters              : Boolean parameters must be passed in the command line with --key={true|false}
- Configuration file ----------------
  * Root user on *nix systems       : /etc/gitlab-runner/config.toml
  * Non-root user                   : ~/.gitlab-runner/config.toml
  * Other systems                   : ./config.toml
  * Specify alternate config file   : gitlab-runner --config=/foo/bar/config.toml
                                      Environment variable CONFIG_FILE

- Signals --------------------------: Possible to use system signals to interact with Runner
  * register,            SIGINT     : Cancel runner registration and delete if it was already registered
  * run exec run-single, SIGINT     : Abort all running builds and exit as soon as possible (Forceful shutdown)
  * run exec run-single, SIGQUIT    : Stop accepting new builds. Exit as soon as active builds finish. (Graceful Shutdown)
  * run,                 SIGHUB     : Force to reload configuration file
  * Reload runner's config file     : sudo killall -SIGHUP gitlab-runner

- Registration ----------------------
  * register                        : register a new runner
                                      Registered runner is added to the configuration file. It doesn't remove previous.
    Interactive                     : Asked multiple questions during registration. Can prefil questions on command line
    Answering questions             : sudo gitlab-runner register
                                      Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
                                      https://gitlab.com
                                      Please enter the gitlab-ci token for this runner
                                      xxx
                                      Please enter the gitlab-ci description for this runner
                                      [hostame] my-runner
                                      Please enter the gitlab-ci tags for this runner (comma separated):
                                      my-tag,another-tag
                                      Please enter the executor: ssh, docker+machine, docker-ssh+machine,
                                      kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
                                      docker
                                      Please enter the Docker image (eg. ruby:2.1):
                                      alpine:latest
    CLI env vars                    : export CI_SERVER_URL=http://gitlab.example.com
                                      export RUNNER_NAME=my-runner
                                      export REGISTRATION_TOKEN=my-registration-token
                                      export REGISTER_NON_INTERACTIVE=true
                                      sudo gitlab-runner register
    Non-interactive                 : Skip questions and provide all information on command line
                                    : sudo gitlab-runner register --non-interactive <other-arguments>
  * unregister                      : unregister specific runner
    Single runner                   : sudo gitlab-runner unregister --url http://gitlab.example.com/ --token t0k3n
    All runners                     : sudo gitlab-runner unregister --all-runners

- Information -----------------------
  * list                            : Lists all runners in saved configuration file
                                      sudo gitlab-runner list
  * verify                          : verify all registered runners
                                      Command checks if the registered runners can connect to GitLab, but it doesn't
                                      verify if the runners are being used by the GitLab runner service
                                      sudo gitlab-runner verify

- Runner Service -------------------: Following commands allow you to manage the runner as a system or user service
  * install                         : Installs GitLab Runner as a service.
  * uninstall                       : Stops and uninstalls the GitLab runner from being run as a service
  * start                           : start service
  * stop                            : stop service
  * restart                         : restart service
  * status                          : get status of a service
                                      sudo gitlab-runner status

- Run ------------------------------:
  * run                             : Main command that is executed when GitLab Runner is started as a service
                                      It reads all defined Runners from config.toml and tries to run all of them
  * run-single                      : This is a supplementary command that can be used to run only a single
                                      build from a single GitLab instance
                                      sudo gitlab-runner run-single -u http://gitlab.example.com -t my-runner-token --executor docker --docker-image ruby:2.1
  * exec                            : Command allows you to run builds locally, trying to replicate the CI environment
                                      as much as possible
                                      Doesn't need to connect to GitLab, instead it reads local .gitlab-ci.yml and
                                      creates a new build environment in which all the build steps are executed
                                      Useful for debugging broken builds
                                      Specify executor and the job name
                                      Will clone the current state of the local git repository
                                    : sudo gitlab-runner exec shell tests

- Internal -------------------------: Internal commands used during builds
  * artifacts-downloader            : Download the artifacts archive from GitLab.
  * artifacts-uploader              : Upload the artifacts archive to GitLab.
  * cache-archiver                  : Create a cache archive, store it locally or upload it to an external server.
  * cache-extractor                 : Restore the cache archive from a locally or externally stored file.

