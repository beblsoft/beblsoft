OVERVIEW
===============================================================================
- GitLab Technology Documentation
- Description                       : GitLab is a Git-based repository manager and a powerful complete
                                      application for software development
                                    :
- Relevant URLS ---------------------
  * Home                            : https://gitlab.com/
  * Dashboard                       : https://gitlab.com/dashboard/projects
- Documentation URLS ----------------
  * Documentation                   : https://docs.gitlab.com/ee/
  * Overview                        : https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/
  * Features                        : https://about.gitlab.com/features/
  * CI/CD Overview                  : https://docs.gitlab.com/ee/ci/README.html
  * Runners                         : https://docs.gitlab.com/ee/ci/runners/README.html
  * GitLab Flow                     : https://docs.gitlab.com/ee/workflow/gitlab_flow.html
- Future Topics ---------------------
  * Cache Dependencies in CI/CD     : https://docs.gitlab.com/ee/ci/caching/index.html
  * Job Artifacts                   : https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html
  * Kubernetes Cluster Integration  : https://docs.gitlab.com/ee/user/project/clusters/index.html#installing-applications
  * Container Registry              : https://docs.gitlab.com/ee/user/project/container_registry.html
  * Prometheus                      : https://docs.gitlab.com/ee/administration/monitoring/prometheus/index.html
  * Code Quality Reports            : https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
  * Security Testing                : https://docs.gitlab.com/ee/user/project/merge_requests/dast.html
  * License Management              : https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html
  * Service Desk                    : https://docs.gitlab.com/ee/user/project/service_desk.html
  * JUnit Test Reports              : https://docs.gitlab.com/ee/ci/junit_test_reports.html
  * Auto DevOps                     : https://docs.gitlab.com/ee/topics/autodevops/index.html
  * GitLab API                      : https://docs.gitlab.com/ee/api/README.html
  * Quick Actions                   : https://docs.gitlab.com/ee/user/project/quick_actions.html


SETUP
===============================================================================
Setting up new computer with source:

1. Login to gitlab at https://gitlab.com/users/sign_in with Google account.
2. Go to Settings/SSH Keys (on left navigation panel).
3. Issue ‘cat ~/.ssh/id_rsa.pub’ within terminal window on computer that you want to register.
4. Cut and paste the results into the key text window on gitlab.
5. Press add key button!
6. Select Projects/beblsoft project.
7. Push blue button, Clone and copy “Clone with SSH” string.
8. From terminal window in $HOME/git directory, issue command, git clone ‘paste buffer’



GITLAB FEATURES: TRACKING STAGES OF THE DEVOPS LIFECYCLE
===============================================================================
- Manage ---------------------------: Statistics and analytics features
  * Granular user roles and perms   : Manage access and permissions with five different user rolse
  * Protected tags                  : Granular permissions for tags you want to protect
  * Multiple integrations           : GitLab can integrate with Authentication and Authorization mechanisms
                                      from multiple third-party services
- Plan -----------------------------: Project planning and management features
  * Issues                          : Quickly set the status, assignee or milestone for multiple issues
                                      at the same time or easily filter them on any properties
  * Description Templates           : Templates for common files
  * Task Lists                      : Task lists in issues, merge requests, and epics to manage tasks
  * GitLab Flavored Markdown
  * Threaded Discussions
  * Filterable System Activity      : View system activity to see a history of changes to issues
  * Project Issue Board             : Each list of an issue board is based on a label that exists in issue tracker
  * Time Tracking                   : Add estimats and record time spent on issues and merge requests
- Create ---------------------------: Source code and data creation management features
  * Commit graph and reporting tools: Commit graphs
  * Search files                    : Search file in repository
  * Fast-forward merge,rebase option: When fast forward merge is not possible, user can rebase
  * Squash and merge                : Combine commits into one so that main branch is simpler to follow
  * Merge requests                  : Create merge requests and @mention team members to review and merge changes
  * Protected branches              : Granular permissions for branches you want to protect
  * Cherry-picking changes          : Cherry-pick any commit in the UI
  * User status message             : Define and show personal status message
  * Import projects                 : Import projects for multiple sources
  * Export projects                 : Export project to other systems
  * Push rules                      : Reject new code and commits that don't comply with policy
  * Snippets                        : Store and share code snippets to engage in a conversation about a piece of code
  * GPG signed commits              : Sign commits and prove that a commit was performed by a certain user
  * Code Owners                     : Assign code owners to files to indicate the team members responsible for code
  * Assignee                        : Assign a user to an issue or a merge request
  * Merge approvals                 : Verify a certain number of people have signed off on approval
  * Web IDE Contributions           : Contribute to projects faster by using the WebIDE to avoid context switching
- Verify ---------------------------: Testing, code quality, and continuous integration features
  * Built-in CI/CD                  : Built-in continuous integration/continuous delivery
  * CI/CD Horizontal autoscaling    : Automatically spin up more runners
  * Visualization of artifacts      : Access test reports, code quality, and coverage information directly from browser
  * Scheduled pipeline triggering   : Have pipelines run on a cron-like schedule
  * Code quality                    : Code quality reports, available in merge request
  * Protected variables             : Mark variable as protected to make it available only to jobs running on protected branches
  * Group-level variables           : Define variables at the group level, use in any project of the group
  * Protected runners               : Protect sensitive information
  * Automatic retry for failed jobs : Retry a specific number of times before marking as failed
  * Include external files in
    pipeline definition             : Include external files in pipeline definition file, to use other templates for common jobs
  * Run jobs only when there are
    changes to a file or path       : Configure jobs to run only when there are changes to a specific file or path
  * Explicit support for monorepos  : Execute jobs only/except when there are changes for a given path or file
- Package --------------------------: Registries
  * Built-in container registry     : Secure and private registry for docker images
  * NPM binary repository           : Binary repository
- Release --------------------------: Application release and delivery features
  * Built in CI/CD                  : Use to build, test, and deploy website or webapp
  * Preview changes with review apps: Create new environment for each branch
  * Interactive web terminals       : Connect to running or completed containers
  * Protected environments          : Specify which person, group, or account is allowed to deploy to a given environment
  * Free, shared or personal runners: Use 2000 free minutes per month of gitlab runners, or set up your
                                      own runner for faster build processing, unlimited build minutes, or special requirements
  * Protected variables             : Make variable only avaiable on protected branches
  * Environment-specific variables  : Limit the environment scope of a variable
  * Deploy boards                   : Consolidated view of current health status of each CI/CD environment
- Monitor --------------------------: Application monitoring and metrics features
  * Application performance monitor : Collect and display performance metrics, leveraging Prometheus
  * Alerts                          : Create service level indicator alerts
  * Error Tracking                  : Error tracking with sentry
- Secure ---------------------------: Security capability features
  * Security dashboards             : Report the security status of the the default branch of each project
  * License management              : Check that licenses of dependencies are compatible with your application



GITLAB FLOW: DEFINED SET OF BEST PRACTICES
===============================================================================
- Description                       : Git allows a wide variety of branching strategies and workflows
                                      GitLab flow offers a simple, transparent, and effective way to work with Git
- Git steps to share code           : Working Copy
                                        v           - git add
                                      Index
                                        v           - git commit
                                      Local Repo
                                        v           - git push
                                      Remote Repo
- Different flows
  * Git Flow                        : Multiple branches: master, develop, release, feature, hotfixes
                                      Lots of added complexity, ending in merge hell, don't use
  * GitHub Flow                     : One master branch, always kept deployable

                                                      FeatureX
                                                            \
                                        master   CX -> CY -> CZ -> ...
                                                      /
                                                   Nav

                                      Does not address deployments, environments, releases, and integration issues
  * GitLab Flow                     : Release branches to release software to the outside world
                                      Each branch versioned with semantic versioning
                                      Merge fixes to master first, then cherry pick to release branches

                                        2-3 Release          CN-> -> CO'
                                                             /      /
                                        master   CL -> CM -> CN -> CO -> CP -> ...
                                                        \
                                        2-2 Release     CM

- Merge Requests                    : Synonymous with pull request.
                                      After a few hours of work, share intermediate result with the rest of the
                                      team. Create a merge request without assigning it to anyone.
                                      Mention people in the description or a comment @darryl
                                      If review reveals shortcomings anyone can push a fix (usually the creator does)
                                      In GitLab, it is common to protet the long-lived branches (ex. master)
                                      A maintainer can merge result
                                      After branch is merged it should be removed
- Issue Tracking                    : Any significant change to the code should start with an issue that
                                      describes the goal. Open branch to do the work
                                      Create merge request into master when the branch is ready
                                      Link issues by mentioning them in commit messages or description of merge request
- Squashing Commits with Rebase     : With git you can squash multiple commits into one to reorder them
                                      Should never rebase commits that have already been pushed to remote server
                                      Never rebase commits authored by other people
- Reducing merge commits in feature
  branches                          : Having lots of merge commits can make your repository history messy
                                      Try to avoid merge commits in feature branches
                                      Merge to master frequently, using feature toggles to enable and disable features
- Commit often and push frequently  : Every time you have a working set of tests and code, you should make a commit
                                      Push feature branch frequently, even when its is not yet ready for review
                                      By sharing work before its complete, prevent duplication in team
- Good commit message               : Bad  : git commit -m "add user.rb"
                                      Good : git commit -m "create user model to store user session information"
                                      Should reflect intention, not contents of commit
- Testing before merging            : Each merge request must be tested before merged
- Working with feature branches     : When creating a feature branch, always branch from an up-to-date master
                                      Merge only when needed prevents creating merge commits in feature branch
                                      that later end up littering the master history



ISSUES
===============================================================================
- Description                       : GitLab Issue Tracker is an advanced and complete tool for tracking
                                      the evolution of a new idea or the process of solving a problem
                                      Allows team and collaborators to share and discuss proposals before
                                      implementing them
- Use Cases                         : Discussing the implementation of a new idea
                                      Submitting feature proposals
                                      Asking questions
                                      Reporting bugs and malfunction
                                      Obtaining support
                                      Elaborating new code implementations
- Keep Private things private       : Confidential issues, discuss private maters among project team members
                                      while project is public, open to collaboration
- Streamline collaboration          : All shared responsibilities to be clearly displayed
- Templates                         : Create issue templates to make collaboration consistent and containing
                                      all needed information
- Tracker                           : Collection of opened and closed issues created in a project
                                      Project Homepage > Issues
- Export to CSV                     : Issues can be exported as CSV from GitLab



GROUPS, SUB GROUPS
===============================================================================
- Groups ----------------------------
  * Descrip                         : With GitLab groups you can assemble related projects together and
                                      grant members access to several projects at once
                                      Groups can also be nested in subgroups
  * Use cases                       : Organize related projects under the same namespace
                                      Include members of team, reference members with @mention
  * Namespaces                      : A namespace is a unique name to be used as a username,
                                      group name, or subgroup name
                                      http://gitlab.example.com/username
                                      http://gitlab.example.com/groupname
                                      http://gitlab.example.com/groupname/subgroup_name
  * Member Lock                     : Lock membership in project to the level of members in group
- Sub Groups ------------------------
  * Description                     : With subgroups, aka nested groups or hierarchical groups, can have up
                                      to 20 levels of nested groups
  * Use cases                       : Separate internal / external organizations. Each group has visibility level
                                      Organize large projects
                                      Make it easier to manage people and control visibility
  * Membership                      : When you add a member to a subgroup, they inherit the membership permission
                                      level from the parent group



PUSH RULES
===============================================================================
- Description                       : Gain additional control over pushes to repository
                                      Push rules are pre-receive Git hooks that are easy to enable
                                      Defined globally in project
- Commit Messages with specific
  reference                         : Can force commits to reference a particular regex
                                      Useful to have commits reference issues
- Restrict branch names             : Only allow specific branch names
- Enabling                          : Project Settings > Repository > Push Rules
- Available Rules                   : Removal of tags
                                      Check auther is GitLab user
                                      Check whether commiter is the current authenticated user
                                      Check wheter commit is signed through GPG
                                      Prevent commiting secrets to Git
                                      Restrict commit message
                                      Restrict commit message, negative match
                                      Restrict by branch name
                                      Retrict by commit author's email
                                      Prohibited file names
                                      Maximum file size



MERGE REQUESTS
===============================================================================
- Description                       : Merge requests allow you to exchange changes you made to source
                                      code and collaborate with other people on the same project
                                      A merge request (MR) is the basis of code collaboration. It is a request
                                      to merge one branch into another
- Merge Requests Enable             : Compare changes between two branches
                                      Review and discussed proposed modifications inline
                                      Live and preview changes with review apps
                                      Build, test, and deploy code in per-branch basis
                                      Prevent from being merged with Work In Progress (WIP) MRs
                                      View deployment process through pipeline graphs
                                      Automatically close the issue(s) that originated the MR
                                      Resolve merge conflicts from the UI
                                      Enable fast-forward merge requests
                                      Squash and merge for a cleaner commit history
- Project merge requests            : Project > Merge Requests
- Group merge requests              : Group > Merge Requests
- Deleting the source branch        : When creating a merge request, select the "Delete the source branch when
                                      merge request accepted" option
- Cherry-pick changes               : Cherry-pick any commit in the UI by simply clicking the Cherry-pick button
- Merge when pipeline succeeds      : Can set merge request to automatically merge when the pipeline succeeds
- Suggest changes                   : As a reviewer, you can add suggestions to change the content in merge
                                      request discussions, and users with appropriate permission can easily
                                      apply them to the codebase directly from the UI
- Create new merge request by email : Create a new merge request by sending an email to a user-specific
                                      email address
- Revert changes                    : GitLab can rever any commit in UI
- Merge request versions            : Every time you push a branch that is tied to a merge request, a new
                                      version of merge request diff is created.
                                      When visiting a merge request with more than one push, you can select and
                                      compare the versions of those merge request diffs
- Work in progress                  : To prevent merge requests from accidentally being accepted before they're
                                      ready GitLab blocks the Accept button for merge requests that have been
                                      marged as Work In Progress
- Approvals                         : Can define manadatory approvers
- Static Analyzers                  : CodeClimate to ensure code quality
                                      Sitespeed.io to verify performance
                                      License Management check
                                      Security checking
- Live preview                      : Configure review apps to preform a live preview of merge request
- Ignore whitespace changes         : Can hide whitespace changes when reviewing
- Pipelines for MRs                 : When a developer updates (or creates) a merge request, a pipeline
                                      should quickly report back its result to the developer
                                      Can customize a specific pipeline structure for merge requests
                                      to speed up the cycle, running only important jobs
- Checkout locally                  : Can configure git locally to pull merge request refs down
                                      This allows merge requests to be inspected locally
- Settings                          : Project > Settings > General > Merge Request
                                      Merge Method: merge commit, merge commit with semi-linear, fast-forward merge
                                      Only allow if pipeline succeeds
                                      Only allow if discussions are resolved



CI/CD OVERVIEW
===============================================================================
- Continuous Integration            : Practice of integrating code into a shared repository and building/testing
                                      each change automatically, as early as possible, usually several times a day
- Continuous Delivery               : Adds that software can be released to production at any time, often by
                                      automatically pushing changes to a staging system
- Continuous Deployment             : Pushes changes to production automatically
- Process                           : Commit
                                      Build
                                      Unit Test
                                      Integration Test
                                      Review
                                      Stage
                                      Production
- GitLab Advantages -----------------
  * Integrated                      : Part of GitLab
  * Easy to Learn
  * Seamless                        : Part of a single GitLab application
  * Scalable                        : Tests run distributed on separate machines
  * Faster results                  : Each build split into multiple jobs
  * Optimized for delivery          : Multiple stages, deploy gates, environments, and variables
  * Open Source                     : Community edition and enterprise edition
- Features --------------------------
  * Multi-Platform                  : Unix, Windows, macOS, any platform with Go
  * Multi-Language                  : Any language supported
  * Stable                          : Builds run on different machine than GitLab
  * Parallel builds                 : Split build over multiple machines
  * Realtime logging                : Link in merge request goes to realtime log
  * Flexible pipelines              : Multiple and parallel jobs per stage
  * Versioned pipelines             : .gitlab-ci.yml contains tests and overall process stems
  * Autoscallinging                 : Automatically scale VM's up and down to minimize cost
  * Build artifacts                 : Upload binaries after build
  * Test locally                    : Reproduce tests locally
  * Docker support                  : Run in docker containers, use services
  * Container registry              : Built-in container registry
  * Protected variables             : Secure secrets during deployments
  * Environments                    : Define multiple environments
- Architecture ----------------------
  * GitLab CI/CD                    : Part of gitlab, web application
  * GitLab Runner                   : Application that processes builds
  * Picture                                    GitLab Server
                                                    |
                                          ------------------------
                                          |                      |
                                      Runner Server             Your Laptop
                                      0..n Runner Processes     0..n Runner process




PIPELINE CONFIGURATION WITH .GITLAB-CI.YML
===============================================================================
- .gitlab-ci.yml                    : File used by GitLab Runner to manage project's pipelines
                                      Placed in root of repository and contains definitions of how project should be built
- Jobs                              : YAML file defines a set of jobs with constraints stating when they should run
                                    : Unlimited number of jobs can be specified
                                    : Always Contain at least the script clause
                                    : Ex.
                                      job1:
                                        script: echo "Hello World!"
                                    : Picked up by runners and executed within the environment of the runner
                                    : Each job is run independently
                                    : Reserved Names: image, services, stages, types, before_script, after_script
                                      variables, cached

- Global parameters -----------------
  * stages                          : Used to define stages that can be used by jobs
  * cache                           : Define list of files that should be cached between subsequent runs
  * pages                           : Special job that is used to upload static content to GitLab that can be used
                                      to serve static website
  * variables                       : Define variables that are passed in the job environment

- Jobs parameters -------------------
  * script                          : Shell script to be executed inside runner
  * extends                         : Defines entry name that a job that uses extends can inherit from
  * include                         : Allow the inclusion of external YAML files
  * image                           : Use docker image
  * services                        : Use docker services
  * stage                           : Defines a job stage (Default:test)
  * variables                       : Define job variables on a job level
  * only                            : Define a list of git refs for which job is created
  * except                          : Define a list of git refs for which job is not created
  * tags                            : Defines a list of tags which are used to select Runner
  * allow_failure                   : Allow job to fail. Failed job doesn't contribute to commit status
  * when                            : Define when to run job.
  * dependencies                    : Define other jobs that a job depends on so that artifacts can be passed
  * artifacts                       : Define list of job artifacts
  * cache                           : Define list of files that should be cached between subsequent runs
  * before_script                   : Override a set of commands that are executed before job
  * after_script                    : Override a set of commands that are executed after job
  * environment                     : Define name of environment to which deployment is done by job
  * coverage                        : Define code coverage settings for a given job
  * retry                           : Define when and how many times a job can be auto-retried
  * parallel                        : Defines how many instances of a job should be run in parallel
  * trigger                         : Defines a downstream pipeline trigger
  * variables                       : Define variables that are passed in the job environment



PIPELINE SCHEDULES
===============================================================================
- Description                       : Pipeline schedules can be used to run a pipeline at specific intervals,
                                      for example every month on the 22nd for a certain branch
- Setup                             : Project CI/CD > Schedules > New Schedule
                                      Enter Cron Syntax > Save Pipeline Schedule
- Run Manually                      : Can run a scheduled pipeline manually by hitting the play button
- Pass Variables                    : Pass arbitrary variables with schedule



ENVIRONMENTS
===============================================================================
- Description                       : During the development of software, there can be many stages
                                      until its ready for public consumption
                                    : First test code and deploy it to testing or staging enviroment
                                      before it is released to the public
- Environments                      : Like tags for CI jobs, describing where code gets deployed
                                      Ex. test, nightly, stage, production
                                      Can also be scoped
                                      Ex scoped. smeckn/production, smeckn/staging
- Deployments                       : Created when jobs deploy code to environments, every environment
                                      can have one or more deployments
                                    : Each time a job that has an environment specified and succeeds,
                                      a deployment is recorded
- Environment status                : Project > Operations > Environments
                                      Shows deployed status of environment
- Rolling Back Changes              : Project > Operations > Environments
                                      Can rollback changes to a previous commit
- Dynamic environments              : It is possible to create environments on the fly by declaring names
                                      dynamically in .gitlab-ci.yml
                                      Useful for review apps
- Stopping an environment           : By stopping an environment, you are effectively terminating its
                                      recording of the deployments that happen in it
- Grouping similar environments     : Environments that are named like "type/foo" are presented
                                      under a group named type
- Monitoring Environments           : If you have enabled Prometheus for monitoring system and response metrics
                                      you can monitor the performance behavior of your app running in each environment
- Scoping environments with specs   : Can create variables to only be injected into some environments
                                      Ex.
                                      VarName    : AWS_SECRET_KEY
                                      VarValue   : "0q2398uasdkfljhasdf..."
                                      EnvWildCard: smeckn/*
                                      The most specific spec takes precedence over the other wildcard matching



CI/CD VARIABLES
===============================================================================
- Runner Execution                  : When receiving a job from GitLab CI, the Runner preparse the build
                                      environment by setting predefined environment variables and other variables
- Variable Priority                 1. Trigger variables or scheduled pipeline variables.
                                    2. Project-level variables or protected variables.
                                    3. Group-level variables or protected variables.
                                    4. YAML-defined job-level variables.
                                    5. YAML-defined global variables.
                                    6. Deployment variables.
                                    7. Predefined environment variables.
- .gitlab-ci.yml defined variables  : Can set variables to used in build environment
- Unmasked variables                : Be aware that variables are not masked, and their values can be shown
                                      in job logs if explicitly asked to do so
                                      If project is public or internal, set pipelines private
- User, Group level variables       : Define per-project or per-group variables that are set in the pipeline env
                                      Variables are store out of the repository and securely passed to GitLab Runner
                                      Its recommended method to use for storing passwords, SSH keys, and credentials
- Project level variables           : Project Settings > CI/CD > Environment Variables
- Protected variables               : Variables could be protected. Whenever a variable is protected, it would only
                                      be securely passed to pipelines running on the protected branches or
                                      protected tags. Other pipelines would not get any protected variables
- Limiting environment scopes       : Limit the environment scope of a variable by defining which environments
                                      it is available for. Wildcards can be used
- Debug Tracing                     : By default, GitLab runner hides most of the details of what is doing
                                      when processing a job. This behavior keeps job traces, short and prevents
                                      secrets from being leaked into trace
                                    : To enable set CI_DEBUG_TRACE variable to true
                                    : Note: security implications
- CI variables in job scripts       : All variables are set as environment variables in the build environment,
                                      and they are accessible with normal methods that are used to access such
                                      variables
                                      bash/sh       : $variable
                                      windows batch : %variable%
                                      PowerShell    : $env:variable
- Predefined Environment Variables  : Automatically set up by runner
  * ARTIFACT_DOWNLOAD_ATTEMPTS      : Number of attempts to download artifacts running a job
  * CHAT_INPUT                      : Additional arguments passed in the ChatOps command
  * CHAT_CHANNEL                    : Source chat channel which triggered the ChatOps command
  * CI                              : Mark that job is executed in CI environment
  * CI_COMMIT_BEFORE_SHA            : The previous latest commit present on a branch before a push request.
  * CI_COMMIT_DESCRIPTION           : The description of the commit: the message without first line, if the title is shorter than 100 characters; full message in other case.
  * CI_COMMIT_MESSAGE               : The full commit message.
  * CI_COMMIT_REF_NAME              : The branch or tag name for which project is built
  * CI_COMMIT_REF_SLUG              : $CI_COMMIT_REF_NAME lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -. Use in URLs, host names and domain names.
  * CI_COMMIT_SHA                   : The commit revision for which project is built
  * CI_COMMIT_SHORT_SHA             : The first eight characters of CI_COMMIT_SHA
  * CI_COMMIT_TAG                   : The commit tag name. Present only when building tags.
  * CI_COMMIT_TITLE                 : The title of the commit - the full first line of the message
  * CI_CONFIG_PATH                  : The path to CI config file. Defaults to .gitlab-ci.yml
  * CI_DEBUG_TRACE                  : Whether debug tracing is enabled
  * CI_DEPLOY_PASSWORD              : Authentication password of the GitLab Deploy Token, only present if the Project has one related.
  * CI_DEPLOY_USER                  : Authentication username of the GitLab Deploy Token, only present if the Project has one related.
  * CI_DISPOSABLE_ENVIRONMENT       : Marks that the job is executed in a disposable environment (something that is created only for this job and disposed of/destroyed after the execution - all executors except shell and ssh). If the environment is disposable, it is set to true, otherwise it is not defined at all.
  * CI_ENVIRONMENT_NAME             : The name of the environment for this job. Only present if environment:name is set.
  * CI_ENVIRONMENT_SLUG             : A simplified version of the environment name, suitable for inclusion in DNS, URLs, Kubernetes labels, etc. Only present if environment:name is set.
  * CI_ENVIRONMENT_URL              : The URL of the environment for this job. Only present if environment:url is set.
  * CI_JOB_ID                       : The unique id of the current job that GitLab CI uses internally
  * CI_JOB_MANUAL                   : The flag to indicate that job was manually started
  * CI_JOB_NAME                     : The name of the job as defined in .gitlab-ci.yml
  * CI_JOB_STAGE                    : The name of the stage as defined in .gitlab-ci.yml
  * CI_JOB_TOKEN                    : Token used for authenticating with GitLab Container Registry, downloading dependent repositories, authenticate with multi-project pipelines when triggers are involved, and for downloading job artifacts
  * CI_JOB_URL                      : Job details URL
  * CI_MERGE_REQUEST_ID             : The ID of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_IID            : The IID of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_PROJECT_ID     : The ID of the project of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_PROJECT_PATH   : The path of the project of the merge request if it’s pipelines for merge requests (e.g. namespace/awesome-project)
  * CI_MERGE_REQUEST_PROJECT_URL    : The URL of the project of the merge request if it’s pipelines for merge requests (e.g. http://192.168.10.15:3000/namespace/awesome-project)
  * CI_MERGE_REQUEST_REF_PATH       : The ref path of the merge request if it’s pipelines for merge requests. (e.g. refs/merge-requests/1/head)
  * CI_MERGE_REQUEST_SOURCE_BRANCH
    _NAME                           : The source branch name of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_SOURCE_PROJECT
    _ID                             : The ID of the source project of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_SOURCE_PROJECT
    _PATH                           : The path of the source project of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_SOURCE_PROJECT
    _URL                            : The URL of the source project of the merge request if it’s pipelines for merge requests
  * CI_MERGE_REQUEST_TARGET_BRANCH
    _NAME                           : The target branch name of the merge request if it’s pipelines for merge requests
  * CI_NODE_INDEX                   : Index of the job in the job set. If the job is not parallelized, this variable is not set.
  * CI_NODE_TOTAL                   : Total number of instances of this job running in parallel. If the job is not parallelized, this variable is set to 1.
  * CI_API_V4_URL                   : The GitLab API v4 root URL
  * CI_PAGES_DOMAIN                 : The configured domain that hosts GitLab Pages.
  * CI_PAGES_URL                    : URL to GitLab Pages-built pages. Always belongs to a subdomain of CI_PAGES_DOMAIN.
  * CI_PIPELINE_ID                  : The unique id of the current pipeline that GitLab CI uses internally
  * CI_PIPELINE_IID                 : The unique id of the current pipeline scoped to project
  * CI_PIPELINE_SOURCE              : Indicates how the pipeline was triggered. Possible options are: push, web, trigger, schedule, api, and pipeline. For pipelines created before GitLab 9.5, this will show as unknown
  * CI_PIPELINE_TRIGGERED           : The flag to indicate that job was triggered
  * CI_PIPELINE_URL                 : Pipeline details URL
  * CI_PROJECT_DIR                  : The full path where the repository is cloned and where the job is run. If the GitLab Runner builds_dir parameter is set, this variable is set relative to the value of builds_dir. For more information, see Advanced configuration for GitLab Runner.
  * CI_PROJECT_ID                   : The unique id of the current project that GitLab CI uses internally
  * CI_PROJECT_NAME                 : The project name that is currently being built (actually it is project folder name)
  * CI_PROJECT_NAMESPACE            : The project namespace (username or groupname) that is currently being built
  * CI_PROJECT_PATH                 : The namespace with project name
  * CI_PROJECT_PATH_SLUG            : $CI_PROJECT_PATH lowercased and with everything except 0-9 and a-z replaced with -. Use in URLs and domain names.
  * CI_PROJECT_URL                  : The HTTP(S) address to access project
  * CI_PROJECT_VISIBILITY           : The project visibility (internal, private, public)
  * CI_REGISTRY                     : If the Container Registry is enabled it returns the address of GitLab’s Container Registry
  * CI_REGISTRY_IMAGE               : If the Container Registry is enabled for the project it returns the address of the registry tied to the specific project
  * CI_REGISTRY_PASSWORD            : The password to use to push containers to the GitLab Container Registry
  * CI_REGISTRY_USER                : The username to use to push containers to the GitLab Container Registry
  * CI_REPOSITORY_URL               : The URL to clone the Git repository
  * CI_RUNNER_DESCRIPTION           : The description of the runner as saved in GitLab
  * CI_RUNNER_EXECUTABLE_ARCH       : The OS/architecture of the GitLab Runner executable (note that this is not necessarily the same as the environment of the executor)
  * CI_RUNNER_ID                    : The unique id of runner being used
  * CI_RUNNER_REVISION              : GitLab Runner revision that is executing the current job
  * CI_RUNNER_TAGS                  : The defined runner tags
  * CI_RUNNER_VERSION               : GitLab Runner version that is executing the current job
  * CI_SERVER                       : Mark that job is executed in CI environment
  * CI_SERVER_NAME                  : The name of CI server that is used to coordinate jobs
  * CI_SERVER_REVISION              : GitLab revision that is used to schedule jobs
  * CI_SERVER_VERSION               : GitLab version that is used to schedule jobs
  * CI_SERVER_VERSION_MAJOR         : GitLab version major component
  * CI_SERVER_VERSION_MINOR         : GitLab version minor component
  * CI_SERVER_VERSION_PATCH         : GitLab version patch component
  * CI_SHARED_ENVIRONMENT           : Marks that the job is executed in a shared environment (something that is persisted across CI invocations like shell or ssh executor). If the environment is shared, it is set to true, otherwise it is not defined at all.
  * GET_SOURCES_ATTEMPTS            : Number of attempts to fetch sources running a job
  * GITLAB_CI                       : Mark that job is executed in GitLab CI environment
  * GITLAB_USER_EMAIL               : The email of the user who started the job
  * GITLAB_USER_ID                  : The id of the user who started the job
  * GITLAB_USER_LOGIN               : The login username of the user who started the job
  * GITLAB_USER_NAME                : The real name of the user who started the job
  * GITLAB_FEATURES                 : The comma separated list of licensed features available for your instance and plan
  * RESTORE_CACHE_ATTEMPTS          : Number of attempts to restore the cache running a job



GITLAB RUNNERS
===============================================================================
- Description                       : In GitLab CI, Runners run the code defined in .gitlab-ci.yml
                                      They are isolated (virtual) machines that pick up jobs through
                                      the coordinator API of GitLab CI
                                    : Ideally, the GitLab runner should not be installed as the same
                                      machine as GitLab
- Types
  * Shared                          : A runner that serves all projects
                                      Useful for jobs that have similar requirements, between multiple projects
                                      Process jobs using a fair usage queue which prevents project starvation
  * Specific                        : Useful for jobs that have special requirements or for projects with a
                                      specific demand. Tags might also be useful in this case
                                      Specific runners process jobs using FIFO queue
  * Group                           : Useful for multiple projects under one group and all projects need
                                      to access same runners. Group runners use a FIFO queue.
- Registering Runners
  * Shared                          : Must be GitLab admin
                                      Project Settings > CICD
  * Specific                        : Create runner with a project registration token
                                      Project Settings > CI/CD > Runners, obtain token
                                      Register runner
- Protected Runners                 : Protect runners from revealing sensitive information
                                      Whenever a runner is protected, it only picks jobs created on
                                      protected branches or protected tags and ignores other jobs
- Cache                             : GitLab runners use cache to speed up the execution of jobs by
                                      reusing existing data, this can sometimes lead to inconsistent behavior
                                      To clear cache
                                      Project CI/CD > PipeLines > Clear Runner Caches
- Maximum job timeout               : For each Runner a maximum job timeout can be specified
                                      Runner takes smaller timeout of project and runner configuration
- Resetting project registration tok: If you think that registration token for a project was revealed, you should
                                      reset them
                                    : Such token can be used to register another runner to the project, it
                                      may be next used to obtain the values of secret variables or clone
                                      the projet code
                                    : To do so
                                      Project Settings > CI/CD > Expand Runners > Reset Runners Registration Token



CONTAINER REGISTRY
===============================================================================
- Description                       : GitLab Container Registry is a secure and private registry for
                                      Docker images
                                    : GitLab Container registry is completely integrated with GitLab
                                    : Max size (including all source in repository) 10GB
- Tight integration                 : Fully integrated with GitLab making it easy for developers to
                                      code, test, and deploy Docker container images using GitLab CI
                                      and docker tooling
                                      - User auth from GitLab
                                      - Projects have a container registry tab
                                      - Every project can have an image repo that can be turned off per-project
                                      - Developers can upload and download images from GitLab CI
- Simplify workflow                 : Easily build Docker images with GitLab CI and store in registry
                                    : Create images per branches, tags, or other ways
                                    : Use own build images, stored in registry to test applications
                                    : Let team contribute to images
                                    : Have full Continous Deployment and Delivery workflow by pointing
                                      Container as a Service to use images directly from GitLab Container Registry
                                      Ex. Docker Cloud, Docker Swarm, Kubernetes
- Login to GitLab registry          : docker login registry.gitlab.com
- Build image                       : docker build -t registry.gitlab.com/group/project .
- Push image                        : docker push registry.example.com/group/project
- Use with GitLab CI                : .gitlab-ci.yml
                                      build_image:
                                        image: docker:git
                                        services:
                                        - docker:dind
                                        script:
                                          - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.example.com
                                          - docker build -t registry.example.com/my-group/my-project .
                                          - docker run registry.example.com/my-group/my-project /script/to/run/tests
                                          - docker push registry.example.com/my-group/my-project:latest
                                        only:
                                          - master



ERROR TRACKING
===============================================================================
- Description                       : Error tracking allows developers to easily discover and view
                                      the errors that their application may be generating. By surfacing
                                      error information where the code is being developed,
                                      efficiency and awareness can be increased
- Sentry                            : Sentry is an open source error tracking system
                                      GitLab allows administrators to connect Sentry to GitLab,
                                      to allow users to view a list of Sentry errors in GitLab
                                      itself
- Enabling Sentry                   : - Need Maintainer permissions
                                      - Sign up to Sentry.io or host own Sentry instance. Need the following scopes:
                                        event:read, project:read
                                      - In Gitlab, project's Settings > Operations
                                        Check Active
                                        Sentry API URL: https://sentry.io/
                                        Auth Token: asdflkjasdfaldksfj (from Sentry)
                                        Click Connect
                                        Select Appropriate Sentry Project
                                        Click Save Chagnes
                                      - In Giltlab, project Opaerations > Error Tracking
                                        Will now show a list of Sentry Errors

