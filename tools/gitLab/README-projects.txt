OVERVIEW
===============================================================================
GitLab Projects Documentation



PROJECT: YAML
===============================================================================
- Description                       : Example .gitlab-ci.yml files
- To Use                            : Files must be renamed to .gitlab-ci.yml
                                      and placed at the root directory of a gitlab repository
- Directory                         : ./yml
- Files
  * helloWorld.yml                  : Hello World yaml file
  * allowFailure.yml                : Allow a job to fail without impacting CI suite
  * anchorServices.yml              : Demonstrate service anchors
  * anchor.yml                      : Demonstrate anchors, allowing jobs to extend others
  * artifacts.yml                   : Allow for files to be passed beteen jobs
  * beforeAfterScript.yml           : Demonstrate before_script and after_script usage
  * cache.yml                       : Cache files in between jobs
  * coverage.yml                    : Retrieve code coverage reports from job output
  * dependencies.yml                : Demonstrate jobs depending on artifacts from previous jobs
  * environment.yml                 : Execute jobs in specific environments
  * environmentMulti.yml            : Deploy to multiple environments [review, staging, production]
  * extends.yml                     : Job subclassing
  * gitCheckout.yml                 : Different ways to checkout git repo
  * gitStrategy.yml                 : Different ways to clone/fetch git repo
  * gitSubmoduleStrategy.yml        : Different ways to load git repo submodules
  * helloWorld.yml                  : Hello World File
  * hiddenJob.yml                   : Create a hidden job that can be extended, but not executed
  * include.yml                     : Include other yaml files in parent yaml file
  * onlyExcept.yml                  : Only run jobs when certain condition applies. Don't run job if condition applies
  * pages.yml                       : Deploy to gitlab pages
  * parallel.yml                    : Run jobs in parallel
  * retry.yml                       : Retry job if conditions are met
  * stages.yml                      : Define multiple stages
  * tags.yml                        : Register runners with tags, then have jobs only run on tagged runners
  * trigger.yml                     : Trigger downstream pipelines
  * variables.yml                   : Define environment variables for jobs
  * when.yml                        : Define certain contitions when a job will run



PROJECT: TOML
===============================================================================
- Description                       : Example gitlab-runner config.toml files
- To Use                            : Files must be gitlab-runner with --config option
- Files
  * shell.toml                      : Shell executor toml file
  * docker.toml                     : Docker executor toml file
  * dockerMachineDigitalOcean.toml  : Docker executor using Digital Ocean docker machine
  * dockerMachineAWS.toml           : See beblsoft/projects/cicd/runner/config.toml
                                      Docker executor using AWS docker machine
