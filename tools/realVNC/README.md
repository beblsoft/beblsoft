# RealVNC Documentation

RealVNC is a Secure, Reliable screen sharing program.
It's products and services connect people and their devices wherever they are, for control, support,
administration, monitoring, training, collaboration and more.

Products:

- VNC Connect: Simple, secure, ready-to-use remote access and support software or all computers and
  mobile devices

- VNC Developer: Solutions and toolkits for integrating secure, real-time remote access into devices,
  products, and services

Relevant URLS:
[Home](https://www.realvnc.com/en/),
[Commands](https://www.realvnc.com/en/connect/docs/unix-start-stop.html)

## VNC Connect

VNC connect is a screen sharing software that lets you connect to a remote computer anywhere in the
world, watch its screen in real-time, and take control as though sitting in front of it

Features:

- Simple: Easy to deploy, responsive and intuitive, no training required, just features you need
- Flexible: Attended and unattended access, direct and cloud connectivity, pre-install or connect
  on demand, file transfer, printing and chat
- Secure: 256-bit AES session encryption, multifactor auth, granular access control, rich session permissions
- Cost effective: Cover every use case or just the ones you need, connect entire ecosystem

Architecture
```text
  -----------------------------------
  | Client (Computer doing viewing) |
  |                                 |
  | VNCViewer                       |
  -----------------------------------
       |
       |
  -----------------------------------
  | VNCServer                       |
  |                                 |
  | Server (Computer being viewed)  |
  -----------------------------------
```

## VNC Connect Setup

Create an account [here](https://manage.realvnc.com/en/?_ga=2.124299906.968672693.1555376972-1500948028.1555376972)

### Server Setup

This should only be done if the machine needs to be viewed remotely

```bash
# Download server (realVNC.sh)
realVNCServer_install

# Start server (realVNC.sh)
realVNCServer_start

# Show server status (realVNC.sh)
realVNCServer_status

# Open VNC Server
# Linux Mint Menu > "VNC Server"
```

### Client Setup

This should only be done on machines that need to view other machines

```bash
# Download client (realVNC.sh)
realVNCViewer_install

# Start client (realVNC.sh)
realVNCViewer_open
```
