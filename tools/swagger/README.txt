OVERVIEW
===============================================================================
- Swagger is the world's largest framework of API developer tools for the
  OpenAPI Specification (OAS), enabling development across the entire API
  lifecycle, from design and documentation, to test and deployment
- Home
  https://swagger.io/


OPENAPI SPECIFICATION
===============================================================================
- The OpenAPI specification (formerly known as the Swagger Specification) is a
  powerful definition format to describe RESTful APIs. The specification creates
  a RESTful interface for easily developing and consuming an API by effectively
  mapping all the resources and operations associated with it.
  It’s easy-to-learn, language agnostic, and both human and machine readable.
- Version 3 Spec
  https://swagger.io/specification/
- Version 2 Spec
  https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md


FLASK RESTPLUS
===============================================================================
- Description
  Extends flask and adds support for quickly building REST APIs.
  Provides coherent collection of decorators and tools to describe you API
  and expose its documentation properly using swagger
- Home
  http://flask-restplus.readthedocs.io/en/stable/
- Github
  https://github.com/noirbizarre/flask-restplus
- Examples
  http://michal.karzynski.pl/blog/2016/06/19/building-beautiful-restful-apis-using-flask-swagger-ui-flask-restplus/
  https://github.com/frol/flask-restplus-server-example


CODE GENERATION
===============================================================================
- Given a OpenAPI document, client API's may be generated in various
  languages
- Github
  https://github.com/swagger-api/swagger-codegen
- Vue
  https://www.npmjs.com/package/swagger-vue