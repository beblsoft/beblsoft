OVERVIEW
===============================================================================
Thunderbird documentation

SHORTCUTS
===============================================================================
Quick Folder Move
 - Open context menu to move message to folder : Shift + M
 - Oven context menu to navigate to folder     : Shift + G
Main Shortcuts
 - New Message                                 : Ctrl + N
 - Open Message in new window                  : Ctrl + O
 - Edit as new                                 : Ctrl + E
 - Expand all threads                          : *
 - Collapse all threads                        : \
 - Forward message                             : Ctrl + L
 - Get new messages for current account        : Ctrl + T, F9
 - Get new messages for all accounts           : Ctrl + Shift + T
 - Toggle message pane                         : F8
 - Go to next message                          : F
 - Go to next unread message                   : N
 - Go to next viewd message                    : ]
 - Go to next unread thread                    : T
 - Go to previous message                      : B
 - Go to previous unread message               : P
 - Go to previously viewed message             : [
 - Move to next mail pane                      : F6, Tab
 - Remove tags from message                    : 0
 - Add message tag                             : 1 to 9
 - Mark message as read, unread                : M
 - Mark thread read                            : R
 - Mark all read                               : Shift + C
 - Message source                              : Ctrl + U
 - Reply to message (sender only)              : Ctrl + R
 - Reply to message (all recipients)           : Ctrl + Shift + R
 - Search messages                             : Ctrl + F
 - Send message now                            : Ctrl + Enter
 - Send message later                          : Ctrl + Shift + Enter
 - Save message as file                        : Ctrl + S
Deleting Large messages
 - Edit > Find > Search Messages
 - In Search Messages Box
   - Seach all foldeers, subfolders
   - Size (KB) is greater than 10000 KB


