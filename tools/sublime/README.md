# Sublime Technology Documentation

## Installation

- Install and configure from `$TOOLS/bash/sublime.sh`
   ```bash
   $ sb;
   $ sublime3_install;
   $ sublime3_config
   ```
- Install package control: `Ctrl + Shift + p` `"Install Package Control"`
- Install license via Lastpass
- Install Material Theme through Package Installer: `Alt + Shift + P, Install`

## Configuration

- Configuration:
    * Global Settings: `Preferences > Settings`
    * Global Key Bindings: `Preferences > Key Bindings`
    * Package Settings: `Preferences > Package Settings > <Package Name>`
- Configuration paths:
    * Local user settings: `~/.config/sublime-text-3`, `~/.config/sublime-text-3/Packages/User`
    * Language specific configuration: `~/.config/sublime-text-3/Packages/User/<language>.sublime-settings`
    * Git backed sublime settings: `$TOOLS/sublime/User`
    * Session Tracking: `~/.config/sublime-text-3/Local/Session.sublime_session`

## Relevant Links

- [Home](https://www.sublimetext.com/)
- [Download](https://www.sublimetext.com/3)
- [List of all commands](http://docs.sublimetext.info/en/latest/reference/commands.html)
- [Sublime Themes](https://www.longren.io/gigantic-list-of-sublime-text-themes/)
- [Package Control](https://packagecontrol.io/)
- [Wikipedia](https://en.wikipedia.org/wiki/Sublime_Text)

## Shortcuts

### General

| Command                | Shortcut                 |
|:-----------------------|:-------------------------|
| Command prompt/pallete | `Ctrl + Shift + P`       |
| Toggle side bar        | `Ctrl + KB`              |
| Open console           | ```Ctrl + ` ```          |
| Toggle Menu            | `Ctrl + Shift + p, vmen` |

### Windows

| Command                      | Shortcut                      |
|:-----------------------------|:------------------------------|
| Open New Window              | `Ctrl + Shift + N`            |
| Close Window                 | `Ctrl + Shift + W`            |
| Open New Window to Same File | `File > New Window into file` |
| Enter distraction free mode  | `Shift + F11`                 |

### Split Window

| Command                     | Shortcut                   |
|:----------------------------|:---------------------------|
| Single Column               | `Alt + Shift + 1`          |
| Two Columns                 | `Alt + Shift + 2`          |
| Three Columns               | `Alt + Shift + 3`          |
| Four Columns                | `Alt + Shift + 4`          |
| Two Rows                    | `Alt + Shift + 8`          |
| Grid                        | `Alt + Shift + 5`          |
| Jump to pane                | `Ctrl + [1,2,3,4]`         |
| Move file to specified pane | `Ctrl + Shift + [1,2,3,4]` |

### Tabs

| Command                 | Shortcut           |
|:------------------------|:-------------------|
| Open last closed tab    | `Ctrl + Shift + t` |
| Cycle up through tabs   | `Ctrl + PageUp`    |
| Cycle down through tabs | `Ctrl + PageDown`  |
| Cycle through last tabs | `Ctrl + Tab`       |
| Close tab               | `Ctrl + W`         |

### Projects/Workspaces

| Command              | Shortcut         |
|:---------------------|:-----------------|
| Quick Switch Project | `Ctrl + Alt + P` |

### Files

| Command   | Shortcut           |
|:----------|:-------------------|
| New File  | `Ctrl + N`         |
| Open File | `Ctrl + O`         |
| Save      | `Ctrl + S`         |
| Save As   | `Shift + Ctrl + S` |

### Selection

| Command                                 | Shortcut             |
|:----------------------------------------|:---------------------|
| Select line                             | `Ctrl + L`           |
| Select word                             | `Ctrl + D`           |
| Select all contents of current brackets | `Ctrl + Shift + M`   |
| Split into lines                        | `Ctrl + Shift + L`   |
| Add previous line                       | `Alt + Shift + Up`   |
| Add next line                           | `Alt + Shift + Down` |
| Single Selection                        | `Escape`             |

### Editing

| Command                                | Shortcut               |
|:---------------------------------------|:-----------------------|
| Copy                                   | `Ctrl + C`             |
| Paste                                  | `Ctrl + V`             |
| Paste and indent correctly             | `Ctrl + Shift + V`     |
| Cut line                               | `Ctrl + X`             |
| Redo                                   | `Ctrl + Y`             |
| Undo                                   | `Ctrl + Z`             |
| Insert line after                      | `Ctrl + Enter`         |
| Insert line before                     | `Ctrl + Shift + Enter` |
| Move line up                           | `Ctrl + Shift + Up`    |
| Move line down                         | `Ctrl + Shift + Down`  |
| Delete word forward                    | `Ctrl + Delete`        |
| Delete word backword                   | `Ctrl + Backspace`     |
| Delete from cursor to end of line      | `Ctrl + KK`            |
| Delete from cursor to start of line    | `Ctrl + K + Backspace` |
| Delete line                            | `Ctrl + Shift + K`     |
| Intent line                            | `Ctrl + ]`             |
| Unindent line                          | `Ctrl + [`             |
| Duplicate line                         | `Ctrl + Shift + D`     |
| Join line below to end of current line | `Ctrl + J`             |
| Toggle line comment                    | `Ctrl + /`             |
| Block comment selection                | `Ctrl + Shift + /`     |
| Soft undo, undoes movements            | `Ctrl + U`             |
| Upper case                             | `Ctrl + KU`            |
| Lower case                             | `Ctrl + KL`            |
| Wrap paragraph at ruler                | `Ctrl + Q`             |
| Sort lines                             | `F9`                   |
| Soft lines (case sensitive)            | `Ctrl + F9`            |
| Fold                                   | `Ctrl + Shift + [`     |
| Unfold                                 | `Ctrl + Shift + ]`     |
| Format Paragraph                       | `Alt + Q`              |

### Find/Replace

| Command       | Shortcut           |
|:--------------|:-------------------|
| Find          | `Ctrl + F`         |
| Replace       | `Ctrl + H`         |
| Find in files | `Ctrl + Shift + F` |

### Navigation

| Command                                  | Shortcut                   |
|:-----------------------------------------|:---------------------------|
| Open file                                | `Ctrl + P`                 |
| Goto symbol (Functions and classes)      | `Ctrl + R`                 |
| Goto line                                | `Ctrl + G + <line number>` |
| Scroll down                              | `Ctrl + Down`              |
| Scroll up                                | `Ctrl + Up`                |
| Beginning of line                        | `Home`                     |
| End of line                              | `End`                      |
| Beginning of file                        | `Ctrl + Home`              |
| End of file                              | `Ctrl + End`               |
| Jump to closing bracket for current code | `Ctrl + M`                 |
| Jump to definition                       | `F12`                      |
| Jump forward                             | `Alt + Shift + -`          |
| Jump backward                            | `Alt + -`                  |

### Bookmarks

| Command           | Shortcut     |
|:------------------|:-------------|
| Toggle Bookmark   | `Ctrl + F2`  |
| Next Bookmark     | `F2`         |
| Previous Bookmark | `Shift + F2` |

## Packages

### Package Control

- URL: https://packagecontrol.io/docs/usage
- Activate: `Ctrl + Shift + P`

### HTMLPrettify

- Install node.js: `sudo apt-get install -y nodejs`
- Setup `node_path`:
    * Preferences > Package Settings > HTML/CSS/JS Prettify > Plugin Options - User
    *
    ```json
      {
        "node_path":
        {
            "windows": "C:/Program Files/nodejs/node.exe",
            "linux": "/usr/bin/nodejs",
            "osx": "/usr/local/bin/node"
        },
      }
    ```
- Reformatting HTML: `Ctrl + Shift + H`
- Adding extensions
    * Using package control, install MJML
    * Using `Preferences->Package Settings->HTML/CS/JS Prettify->Plugin Options - User`
    * Add to HTMLPrettify.sublime-settings under HTML allowed_file_extensions,
      e.g. `"allowed_file_extensions": ["htm", "html", "xhtml", "shtml", "xml", "svg", "vue", "mjml"]`

### Emmet

- URL: http://docs.emmet.io/
- Nesting Operators: `(Child >)(Sibling +)(Climb-up ^)(Multiply *)(Group())`
- Attribute Operators: `(Class .)(ID #)(Custom [])`
- Item Numbering: `(Multiplication $*#)(Zero Pad with more $'s)(Change base direction @)`
- Add text: `{}`
- Examples
    * Child: `div>ul>li`
    * Sibling: `div+p+bq`
    * Climb-up: `div+div>p>span+em^bq`
    * Multiplication: `ul>li*5`
    * Grouping: `div>(header>ul>li*2>a)+footer>p`
    * ID and Class: `div#header+div.page+div#footer.class1.class2.class3`
    * Custom attributes: `td[title="Hello world!" colspan=3]`
    * Item Numbering: `ul>li.item$$$*5`
    * Changing base, direction: `ul>li.item$@-3*5`
    * Text: `a{Click me}`

### Line Jumper

- URL: https://packagecontrol.io/packages/LineJumper
- Jump up 10 lines: `Alt + Up`
- Jump down 10 lines: `Alt + Down`

### Python PEP8 Autoformat

- URL: https://packagecontrol.io/packages/Python%20PEP8%20Autoformat
- Format whole document: `Ctrl + Shift + R`
- Set indent to 4: `User Settings > "indent-size": 4`

### TypeScript

- URL: https://packagecontrol.io/packages/TypeScript

### Sublime SFTP

- URL: https://wbond.net/sublime_packages/sftp
- Connect to sftp:
    * Right click on folder
    * SFTP/FTP > Map to Remote
    * Fill out sftp-config.json file
- sftp-config.json options: https://wbond.net/sublime_packages/sftp/settings
    * save_before_upload : Save file before upload
    * upload_on_save : Auto upload when saved
    * sync_down_on_open : If remote is newer, user is prompted
    * confirm_downloads : If yes/no confirmation should be presented to userwhen downloading
    * confirm_overwrite_newer: User should be presented with yes/no confirmation
      when modification time that is older than the server file

### ProjectManager

- Allows all projects to be managed from one location
- Github page: https://github.com/randy3k/ProjectManager
- Import current project: `Project > Project Manager > import *.sublime-project File`
- Switch Projects: `Ctrl + Alt + P`

### Pylinter

- URL: https://github.com/biermeester/Pylinter
- Must first install PyLint: `sudo pip3 install pylint`
- Point pylinter at pylintrc file Project: `Preferences > Package Settings > Pylinter Settings User > Set pylint_rc variable`
- Run: `Ctrl + Alt + z`
- Add pylint ignore comment/statement: `Ctrl + Alt + i`
- Toggle Marking: `Ctrl + Alt + x`
- Quick List of Pylint Errors: `Ctrl + Alt + c`
- Pylinter.sublime-settings: File is purposefully not in version control due to user-specific paths
  Use. `~/$TOOLS/sublime/pylint/sublime_sample_pylinter_rc` as a template

### TrailingSpaces

- Highlight trailing whitespaces
- URL: https://github.com/SublimeText/TrailingSpaces
- Add commands to `Preference > KeyBindings`
    * `{ "keys": ["ctrl+shift+t"], "command": "delete_trailing_spaces" },`
    * `{ "keys": ["ctrl+shift+d"], "command": "toggle_trailing_spaces" }`

### PrettyJSON

- Prettify JSON
- URL: https://blog.adriaan.io/sublime-pretty-json.html
- Prettify JSON: `Ctrl + Alt + J`

### VueSyntaxHighlight

- Prettify vue
- URL: https://github.com/vuejs/vue-syntax-highlight

### A File Icon

- Pretty File Icons in Sidebar

### CSCOPE

- URL: https://github.com/ameyp/CscopeSublime
- Show Cscope options: `Ctrl + \`
- Look up symbol under cursor: `Ctrl + L, Ctrl + S`
- Look up definition under cursor: `Ctrl + L, Ctrl + D`
- Look up functions called by the function under the cursor: `Ctrl + L, Ctrl + E`
- Look up functions calling the function under the cursor: `Ctrl + L, Ctrl + R`
- Jump back: `Ctrl + Shift + [`
- Jump forward: `Ctrl + Shift + ]`
- Setup
  ```json
  {
     "executable": "/usr/local/bin/cscope",
     "database_location": "/ade/jbensson_ossmainlinux0/oss/utl/cscope.out",
     "CscopeSublime_display_outline": true
  }
  ```

### Sublime Text Go To File

- URL: https://github.com/gs/sublime-text-go-to-file
- Install:
  ```bash
  $ cd ~/.config/sublime-text-3/Packages;
  $ git clone https://github.com/stvkoch/sublime-text-go-to-file.git GoToFile
  ```
- Add to key map file
  ```json
  {
      { "keys": ["alt+d"], "command": "go_to_file" }
      { "keys": ["alt+i"], "command": "file_info" }
      { "keys": ["alt+k"], "command": "file_info_short" }
  }
  ```

### ESLint

- Description: Allows ESlinting inside sublime
- URL: https://packagecontrol.io/packages/ESLint
- Run eslinter: `Ctrl + Alt + e`
- linter.js fix: `./jbenssonUser/linter.js`

### DocBlockr

- Description: Create jsdoc brackets easily in sublime
- Github: https://github.com/spadgos/sublime-jsdocs
- Create ** comment: `"/** " + Enter`
- Create * comment: `"/* " + Enter`
- Create ** inline comment: `"/** " + Shift + Enter`

### Dockerfile Syntax Highlighting

- Syntax Highlighting for Dockerfiles
- URL: https://packagecontrol.io/packages/Dockerfile%20Syntax%20Highlighting

### TOML

- Syntax highlighting for `.toml/.tml` format
- URL: https://packagecontrol.io/packages/TOML

### Markdown Editing

- Markdown editing plugin from Sublime Text
- Flavors supported: Standard Markdown, GitHub Flavored, MultiMarkdown
- Package Control: https://packagecontrol.io/packages/MarkdownEditing
- Github: https://github.com/SublimeText-Markdown/MarkdownEditing
- Documentation: `Preferences > Package Settings > Markdown Editing > README`
- Key Bindings
    * Navigate All Headers: `Ctrl + R`
    * Insert Footnote: `Alt + Shift + 6`
    * Bold: `Alt + B`
    * Italic: `Alt + I`
    * Add number of headings: `Ctrl 1...6`
    * Fold/unfold current section: `Shift + Tab`
    * Show folding options: `Ctrl + Shift + Tab`
    * Go to prev/next heading of same or higher: `Ctrl + Alt + Shift + PageUp` `Ctrl + Alt + Shift + PageDown`
    * Go to prev/next heading: `Ctrl + Shift + PageUp` `Ctrl + Shift + PageDown`
- Lint Setup
    * Note: Markdown.sublime-settings should point sublime at the host's mdl executable and
      Beblsoft's mdlrc file that is used during CI
    * Install markdownlint: `$ markdownlint_install`
    * Add to key bindings:
      ```json
      {
          "keys": ["ctrl+shift+l"], "command": "markdown_lint_mdl"
      }
      ```
- Configure settings: `Preferences > Package Settings > Markdown Editing > Markdown GFM Settings`
  ```json
  {
      "color_scheme": "Packages/Theme - Flatland/Flatland Dark.tmTheme",
      "extensions": [ "mdown", "md" ],
      "highlight_line": true,
      "line_numbers": true,
      "line_padding_bottom": 0,
      "line_padding_top": 0,
      "detect_indentation": false,
      "mde.list_indent_bullets": ["-", "*", "+"],
      "rulers": [100],
      "wrap_width": 500,
      "mde.lint":
      {
          "mdl":
          {
              "additional_arguments": [
                  "--config",
                  "~/git/beblsoft/tools/markdownlint/mdlrc.rb"
              ],
              "executable": "/usr/local/bin/mdl"
          },
          "disable": [
              "md013"
          ],
          "md003": "any",
          "md004": "cyclic",
          "md007": 0,
          "md013": 0,
          "md026": ".,;:!",
          "md029": "any",
          "md030":
          {
              "ol_multi": 1,
              "ol_single": 1,
              "ul_multi": 1,
              "ul_single": 1
          },
      },
  }
  ```

### Markdown Preview

- Features
    * Preview and build markdown files directly in web browser
    * Use builtin Python Markdown parser (offline) or use
    * GitHub Markdown API or GitLab Markdown API (online)
- URLs: [Package Control](https://packagecontrol.io/packages/MarkdownPreview),
  [Documentation](https://facelessuser.github.io/MarkdownPreview/)
- Live Reload
    * Note: Use [Chrome Markdown Viewer Plugin](https://chrome.google.com/webstore/category/extensions) instead
    * Install LiveReload Package from PackageControl
    * Restart Sublime
    * Configure Live Reload: `Ctrl + Shift + P`, `LiveReload: Enable/disbale plug-ins`,
      `Simple Reload with delay(400ms)`
- Add to key bindings
  ```json
  {
    "keys": ["alt+m"], "command": "markdown_preview", "args": {"target": "browser", "parser":"markdown"}
  }
  ```
- Configure settings: `Preferences > Package Settings > Markdown Preview > Settings`
  ```json
  {
      "parser": "gitlab",
      "enable_autoreload": true
  }
  ```

### Markdown Table Formatter

- Markdown plugin that offers table formatting
- URL: https://packagecontrol.io/packages/Markdown%20Table%20Formatter
- Format selection: Highlight Text, `Ctrl + Alt + Shift + T`
- Format entire Document: `Ctrl + Alt + Shift + T`
- Configure settings: `Preferences > Package Settings > Markdown Table Formatter > Settings`
  ```json
  {
      "autoformat_on_save": true
  }
  ```
- Formatting notes
    * Can't have trailing spaces after table

### View in Browser

- Plugin that will open whatever is in current view/tab in a browser
- URL: https://packagecontrol.io/packages/View%20In%20Browser
- Default key binding (defaults firefox)   : `{ "keys": [ "ctrl+alt+v" ], "command": "view_in_browser" }`
- Update default browser to Chrome: `Preferences > Package Settings > View in Browser > User Settings`
  ```json
  {
     "browser": "chrome"
  }
  ```

### Sass

Sass package provides Sass and SCSS syntax for Sublime text.

Relevant URLs: [Package Control](https://packagecontrol.io/packages/Sass) package provies

### mjml-syntax

Sublime package for the MJML syntax to provide syntax highlight and snippets for your MJML files.

Relevant URLs: [GitHub](https://github.com/mjmlio/mjml-syntax)

### EML (E-Mail)

Syntax highlighting for E-Mail in Sublime.

Relevant URLs: [Package Control](https://packagecontrol.io/packages/EML%20(E-Mail))

### Stylus

Stylus package for Sublime includes build system and syntax highlighting for stylus CSS preprocessor.

Relevant URLs:
[Github](https://github.com/billymoon/Stylus)

### LaTeXTools

A LaTeX Plugin for Sublime.

Relevant URLs:
[Github](https://github.com/SublimeText/LaTeXTools),
[Docs](https://latextools.readthedocs.io/en/latest/),
[Key Bindings](https://github.com/SublimeText/LaTeXTools/blob/master/Default%20(Linux).sublime-keymap)

Installation:
- Install package from package control
- Install Latex Binaries: `sb; latex_install`
- Check that all necessary binaries are installed: `Ctrl + Shift + P: LaTeXTools: Check System`

Usage:
- Open `*.tex` file
- To Build: `Ctrl + b`
- To View PDF: `Ctrl + Shift + P: LaTeXTools:View PDF`

## Issues

- Console
    * Open sublime console: ```Ctrl + ` ```
    * Show commands:`sublime.log_commands(True)`
    * Don't show commands: `sublime.log_commands(False)`
- Tabs to Space conversion
    * See bottom right hand corner. `Spaces: #` Gives different options to
  convert tabs to spaces, indentation to tabs.
- Plugins
    * Find plugin commands available(1): `Ctrl + Shift + P`,  `Type <Package Name>`
    * Find plugin commands available(2): Search for the plugin source file `Default.sublime-commands`
- Slow Sublime Performance
    * Decrease number of files in projects: I.e. remove and reinstall git repo. Or `git clean`
    * Indexing Issues
    * Enable index logging: sublime.log_indexing(True)
    * Don't index certain files: `Preferences.sublime-settings`
      ```json
      {
          "index_exclude_patterns": [
            "*.log",
            "*/.git/*",
            "*/venv/*",
            "*/__pycache__/*",
            "*/node_modules/*",
            "*/dist/*"
          ],
          "folder_exclude_patterns": [
            "venv",
            "node_modules"
          ],
      }
      ```

## Plugin Creation

- Sublime can be extended via its plugin API.
- API Reference: https://www.sublimetext.com/docs/3/api_reference.html
- For examples:
  ```bash
  $ cd ~/.config/sublime-text-3/Packages
  $ grep -r "import sublime" *
  ```

