# License Finder Documentation

LicenseFinder works with your package managers to:

- find dependencies
- detect the licenses of the packages in them
- compare those licenses against a user-defined whitelist
- give you an actionable exception report

Relevant URLs:
[GitHub](https://github.com/pivotal/LicenseFinder)

## Project Types

### Supported

| Project Type                      | Package Manager | Tested on Version |
|:----------------------------------|:----------------|:------------------|
| Ruby Gems                         | bundler         | 1.16.6            |
| Python Eggs                       | pip             | 19.0.2            |
| Node.js                           | npm             | 6.4.1             |
| Bower                             | bower           | 1.8.4             |
| Nuget (without license discovery) | nuget           | 4.7.1.5393        |
| Godep                             | Godep           | 80                |
| Go workspace (via a .envrc file)  | Go              | lang 1.11.5       |
| Go modules                        | Go lang         | 1.11.5            |
| Java                              | maven           | 3.6.0             |
| Java                              | gradle          | 4.10.3            |

### Experimental

- Erlang (via rebar)
- Objective-C, Swift (via Carthage or CocoaPods)
- Objective-C (+ CocoaPods 0.39 and below. See CocoaPods Specs Repo Sharding)
- Elixir (via mix)
- Golang (via gvt, glide,dep, trash and govendor)
- JavaScript (via yarn)
- C++/C (via conan)
- Scala (via sbt)
- Rust (via cargo)
- Go Modules (via go mod)
- PHP (via composer)

## Installation

License Finder requires Ruby 2.3.3 or greater to run.

```bash
# Install with gem
gem install license_finder
```

## Usage

Make sure all your dependencies are installed with your package manager's install command.
For example:
```bash
bundle install
npm install
```

The first time you run `license_finder` it will list all your project's packages.
```bash
license_finder
```

The output will report that non of your packages have been approved. Over time you will tell
`license_finder` which packages are approved, so when you run the command in the future, it will
report current action items: i.e. packages that are new or have never been approved.

### Help

```bash
$ license_finder -h
Commands:
  license_finder action_items                            # List unapproved dependencies (the default action
                                                         # for `license_finder`)
  license_finder approvals [add|remove]                  # Manually approve dependencies, even if their
                                                         # licenses are not whitelisted - see `license_finder approvals help` for...
  license_finder blacklist [list|add|remove]             # Forbid approval of any dependency whose licenses
                                                         # are all blacklisted - see `license_finder blacklist help` for more...
  license_finder dependencies [add|remove|list]          # Add or remove dependencies that your package
                                                         # managers are not aware of - see `license_finder dependencies help` for...
  license_finder diff OLDFILE NEWFILE                    # Command to view the differences between two
                                                         # generated reports (csv).
  license_finder help [COMMAND]                          # Describe available commands or one specific command
  license_finder ignored_dependencies [list|add|remove]  # Exclude individual dependencies from action items
                                                         # and reports - see `license_finder ignored_dependencies help` for ...
  license_finder ignored_groups [list|add|remove]        # Exclude test and development dependencies from action
                                                         # items and reports - see `license_finder ignored_groups help` ...
  license_finder licenses [add|remove]                   # Set a dependency's licenses, if the licenses found
                                                         # by license_finder are missing or wrong - see `license_finder lic...
  license_finder project_name [show|add|remove]          # Set the project name, for display in reports - see
                                                         # `license_finder project_name help` for more information
  license_finder project_roots                           # List project directories to be scanned
  license_finder report                                  # Print a report of the project's dependencies to stdout
  license_finder version                                 # Print the version of LicenseFinder
  license_finder whitelist [list|add|remove]             # Automatically approve any dependency that has a whitelisted license - see `license_finder whitelist help` for more ...

Options:
  [--project-path=PROJECT_PATH]                    # Path to the project. Defaults to current working directory.
  [--decisions-file=DECISIONS_FILE]                # Where decisions are saved.
                                                   # Defaults to doc/dependency_decisions.yml.
  [--log-directory=LOG_DIRECTORY]                  # Where logs are saved.
                                                   # Defaults to ./lf_logs/$PROJECT/prepare_$PACKAGE_MANAGER.log
  [--go-full-version=GO_FULL_VERSION]              # Whether dependency version should include full version.
                                                   # Only meaningful if used with a Go project. Defaults to false.
  [--gradle-include-groups=GRADLE_INCLUDE_GROUPS]  # Whether dependency name should include group id.
                                                   # Only meaningful if used with a Java/gradle project. Defaults to false.
  [--gradle-command=GRADLE_COMMAND]                # Command to use when fetching gradle packages.
                                                   # Only meaningful if used with a Java/gradle project.
                                                   # Defaults to 'gradlew' / 'gradlew.bat' if the wrapper is present, otherwise to 'gradle'.
  [--maven-include-groups=MAVEN_INCLUDE_GROUPS]    # Whether dependency name should include group id.
                                                   # Only meaningful if used with a Java/maven project. Defaults to false.
  [--maven-options=MAVEN_OPTIONS]                  # Maven options to append to command.
                                                   # Defaults to empty.
  [--pip-requirements-path=PIP_REQUIREMENTS_PATH]  # Path to python requirements file.
                                                   # Defaults to requirements.txt.
  [--rebar-command=REBAR_COMMAND]                  # Command to use when fetching rebar packages.
                                                   # Only meaningful if used with a Erlang/rebar project. Defaults to 'rebar'.
  [--rebar-deps-dir=REBAR_DEPS_DIR]                # Path to rebar dependencies directory.
                                                   # Only meaningful if used with a Erlang/rebar project. Defaults to 'deps'.
  [--mix-command=MIX_COMMAND]                      # Command to use when fetching packages through Mix.
                                                   # Only meaningful if used with a Mix project (i.e., Elixir or Erlang). Defaults to 'mix'.
  [--mix-deps-dir=MIX_DEPS_DIR]                    # Path to Mix dependencies directory.
                                                   # Only meaningful if used with a Mix project (i.e., Elixir or Erlang). Defaults to 'deps'.
  [--sbt-include-groups=SBT_INCLUDE_GROUPS]        # Whether dependency name should include group id.
                                                   # Only meaningful if used with a Scala/sbt project. Defaults to false.

```

### Activation

`license_finder` will find and include packages for all supported languages, as long as that language
has a package definition in the project directory:

- `Gemfile` (for `bundler`)
- `requirements.txt` (for `pip`)
- `package.json` (for `npm`)
- `pom.xml` (for `maven`)
- `build.gradle` (for `gradle`)
- `settings.gradle` that specifies r`ootProject.buildFileName` (for `gradle`)
- `bower.json` (for `bower`)
- `Podfile` (for `pod`)
- `Cartfile` (for `carthage`)
- `rebar.config` (for `rebar`)
- `mix.exs` (for `mix`)
- `packages/` directory (for `nuget`)
- `*.csproj` (for `dotnet`)
- `vendor/manifest` or `*/vendor/manifest` file (for `gvt`)
- `glide.lock` file (for `glide`)
- `vendor/vendor.json` file (for `govendor`)
- `Gopkg.lock` file (for `dep`)
- `go.sum` file (for `go mod`)
- `vendor.conf` file (for `trash`)
- `yarn.lock` file (for `yarn`)
- `conanfile.txt` file (for `conan`)
- `build.sbt` file (for `sbt`)
- `Cargo.lock` file (for `cargo`)
- `composer.lock` file (for `composer`)

### Continuous Integration

`license_finder` will return a non-zero exit status if there are unapproved dependencies. This can
be useful for inclusion in a CI environment to alert you if someone adds an unapproved dependency
to the project.

## Approving Dependencies

### Specific Packages

`license_finder` will inform you whenever you have an unapproved dependency. If your business decides
that this is an acceptable risk, the easiest way to approve the dependency is by running
`license_finder approval add`.

For example:
```bash
license_finder approval add awesome_gpl_gem
```

To record who approved the dependency and why:
```bash
license_finder approval add awesome_gpl_gem --who CTO --why "Go ahead"
```

### Whitelisting

Approving packages one-by-one can be tedious. Usually your business has blanket policies about which
packages are approved. To tell `license_finder` that any package with the MIT license should be
approved, run:

```bash
license_finder whitelist add MIT
```

## Outputs and Artifacts

### Decisions File

Any decisions you make about approvals will be recorded in a YAML file named `doc/dependency_decisions.yml`.

This file must be committed to version control. Rarely, you will have to manually resulve conflicts init.
In this situation, keep in mid that each decision has an associated timestamp, and the decisions are
processed top-to-bottom, with later decisions overwriting or appending to earlier decisions.

### Output Format

By default `license_finder` outputs something like:
```bash
license_finder

Dependencies that need approval:

highline, 1.6.14, ruby
json, 1.7.5, ruby
mime-types, 1.19, ruby
rails, 3.2.8, unknown
rdoc, 3.12, unknown
rubyzip, 0.9.9, ruby
xml-simple, 1.1.1, unknown
```

To change the output format use the `--format` option. For example:
```bash
license_finder action_items --quiet --prepare-no-fail --format=markdown > /tmp/out.md
license_finder action_items --quiet --prepare-no-fail --format=html > /tmp/out.html
```

## Manual intervention

### Setting Licenses

When `license_finder` reports that a dependency's license is 'unknown', you should manually research
what the actual license is. When you have established the real license, you can record it with:

```bash
license_finder dependencies add my_unknown_dependency MIT --homepage="www.unknown-code.org"
```

This command would assign the MIT license to the dependency `my_unknown_dependency`. It will also
set its homepage to `www.unknown-code.org`.

### Adding Hidded Dependencies

`license_finder` can track dependencies that your package manager doesn't know about (JS libraries that
don't appear in your Gemfile/requirements.txt/package.json, etc.) as follows:

```bash
license_finder dependencies add my_js_dep MIT 0.1.2
```

These dependencies can later be removed as follows:

```bash
license_finder dependencies remove my_js_dep
```

### Excluding Dependencies

Sometimes a project will have development or test which you don't want to track. You can exclude these
dependencies by running:

```bash
license_finder ignored_groups
```

On rare occasions a package manager will report an individual dependency that you want to exclude
from all reports, even though it is approved. You can exclude an individual dependency by running:

```bash
license_finder ignored_dependencies
```

### Blacklisting Licenses

Some projects will have a list of licenses that cannot be used. You can add these licenses to the
blacklist using:

```bash
license_finder blacklist add
```

## Configuration

By default, `license_finder` expects the decisions file to be stored at `doc/dependency_decisions.yml`.
However, all commands can be passed `--decisions_file` to override this location.

### Saving Configuration

It may be difficult to remember to pass command line options to every command. In some of these cases
you can store default values in YAML formatted config file. `license_finder` looks for this file
in `config/license_finder.yml`.

For example:
```yaml
decisions_file: './some_path/decisions.yml'
gradle_command: './gradlew'
rebar_command: './rebarw'
rebar_deps_dir: './rebar_deps'
mix_command: './mixw'
mix_deps_dir: './mix_deps'
```

### Python3 Support

LicenseFinder supports Python3 is by fixing this [Issue](https://github.com/pivotal/LicenseFinder/issues/140).

Simply enter python3 virtualenv and then run `license_finder`.

## Command Reference

### `action-items`

List unapproved dependencies. `license_finder` with no command is aliased to `license_finder action_items`.

Example:

```bash
license_finder action_items --debug --prepare-no-fail --project-path=${SMECKN}/server
```

Help:

```bash
$ license_finder help action_items
Usage:
  license_finder action_items

Options:
  -d, [--debug], [--no-debug]                     # Emit detailed info about what LicenseFinder is doing
  -p, [--prepare], [--no-prepare]                 # Prepares the project first for license_finder
      [--prepare-no-fail], [--no-prepare-no-fail] # Prepares the project first for license_finder but carries on
                                                  # despite any potential failures
  -r, [--recursive], [--no-recursive]             # Recursively runs License Finder on all sub-projects
  -a, [--aggregate-paths=one two three]           # Generate a single report for multiple projects.
                                                  # Ex: --aggregate_paths='path/to/project1' 'path/to/project2'
  -q, [--quiet], [--no-quiet]                     # Silences progress report
      [--columns=one two three]                   # For text or CSV reports, which columns to print.
                                                  # Pick from: ["name", "version", "authors", "licenses",
                                                  # "license_links", "approved", "summary", "description",
                                                  # "homepage", "install_path", "package_manager", "groups", "texts", "notice"]
      [--format=FORMAT]                           # Emit detailed info about what LicenseFinder is doing
                                                  # Default: text
                                                  # Possible values: text, html, markdown, csv, xml, json
```

### `approvals add`

Approve one or more dependencies by name

Example:
```bash
license_finder approvals add my_random_dependency --version=2.0 --who=jbensson --why="Feels Right"
```

### `approvals remove`

Unapprove a dependency

Example:
```bash
license_finder approvals remove my_random_dependency
```

### `blacklist add`

Add one or more licenses to the blacklist

Example:
```bash
license_finder blacklist add GPL
```

### `blacklist remove`

Remove one or more licenses from the blacklist

Example:
```bash
license_finder blacklist remove GPL
```

### `blacklist list`

List all blacklisted licences

Example:
```bash
license_finder blacklist list
```

### `dependencies add`

Add a dependency that is not managed by a package manager, optionally approve

Example:
```bash
license_finder dependencies add foo_bar_package MIT --homepage=foo_bar.com --approve
```

### `dependencies remove`

Remove a dependency that is not managed by a package manager

Example:
```bash
license_finder dependencies remove foo_bar_package MIT
```

### `dependencies list`

List manually added dependencies

Example:
```bash
license_finder dependencies list
```

### `help`

Describe available commands or one specific command

Example:
```bash
license_finder help [COMMAND]
```

### `ignored_dependencies add`

Add a dependency to be ignored

Example:
```bash
license_finder ignored_dependencies add foo_bar_package
```

### `ignored_dependencies remove`

Remove a dependency from the ignored dependencies

Example:
```bash
license_finder ignored_dependencies remove foo_bar_package
```

### `ignored_dependencies list`

List all the ignored dependencies

Example:
```bash
license_finder ignored_dependencies list
```

### `ignored_groups add`

Add a group to be ignored

Example:
```bash
license_finder ignored_groups add devDependencies
```

### `ignored_groups remove`

Remove a group from the ignored groups

Example:
```bash
licnese_finder ignored_groups remove devDependencies
```

### `ignored_groups list`

List all the ignored groups

Example:
```bash
licnese_finder ignored_groups list
```

### `licenses add`

Set a dependency's licenses, overwriting any license_finder has found

Example:
```bash
license_finder licenses add foo_bar_package MIT
```

### `licenses remove`

Remove a manually set license

Example:
```bash
license_finder licenses remove foo_bar_package MIT
```

### `project_name add`

Set the project name

Example:
```bash
license_finder project_name add Smeckn
```

### `project_name remove`

Remove the project's name

Example:
```bash
license_finder project_name remove
```

### `project_name show`

Show the project's name

Example:
```bash
license_finder project_name show
```

### `report`

Print a report of the project's dependencies to stdou

Example:
```bash
license_finder report --format=html --save=/tmp/out.html
```

### `version`

Print the version of license_finder

Example:
```bash
license_finder version
```

### `whitelist add`

Add one or more licenses to the whitelist.

Example:

```bash
license_finder whitelist add --who=jbensson --why="Acceptable License" MIT
```

### `whitelist remove`

Remove one or more licenses from the whitelist

Example:

```bash
license_finder whitelist remove MIT
```

### `whitelist list`

List all whitelisted licenses

Example:

```bash
license_finder whitelist list
```




