#!/usr/bin/ruby
#
# NAME:
#  mdlrc
#
# DESCRIPTION:
#  Markdownlint Configuration File
#
# REFERENCE:
#  $ /usr/local/bin/mdl --help
#  Usage: mdl [options] [FILE.md|DIR ...]
#      -c, --config FILE                The configuration file to use
#      -g, --git-recurse                Only process files known to git when given a directory
#      -i, --[no-]ignore-front-matter   Ignore YAML front matter
#      -j, --json                       JSON output
#      -l, --list-rules                 Don't process any files, just list enabled rules
#      -r, --rules RULE1,RULE2          Only process these rules
#      -u, --rulesets RULESET1,RULESET2 Specify additional ruleset files to load
#      -a, --[no-]show-aliases          Show rule alias instead of rule ID when viewing rules
#      -w, --[no-]warnings              Show kramdown warnings
#      -d, --skip-default-ruleset       Don't load the default markdownlint ruleset
#      -s, --style STYLE                Load the given style
#      -t, --tags TAG1,TAG2             Only process rules with these tags
#      -v, --[no-]verbose               Increase verbosity
#      -h, --help                       Show this message
#      -V, --version                    Show version

# verbose true

# Rules to ignore
# MD004 - Unordered list style
# MD007 - Unordered list indentation
# MD013 - Line Length
# MD014 - Dollar signs used before commands without showing output
# MD024 Multiple headers with the same content
# MD026 Trailing punctuation in header
# MD029 Ordered list item prefix
# MD030 Spaces after list markers
# MD031 Fenced code blocks should be surrounded by blank lines
# MD032 Lists should be surrounded by blank lines
# MD033 Inline HTML
# MD034 - Bare URL used
# MD046 Code block style

rules "~MD004", "~MD007", "~MD013", "~MD014", "~MD024", "~MD026", "~MD029", "~MD030", "~MD031", "~MD032", "~MD033", "~MD034", "~MD046"
