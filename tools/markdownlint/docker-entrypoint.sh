#!/bin/bash
#
# File:
#  docker-entrypoint.sh
#
# Description
#  Markdownlint Docker Entrypoint


###############################################################################
#                              IMPORTS                                        #
###############################################################################
. ${TOOLS}/bash/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function markdownlint() {
  local mdlrcFile="${TOOLS}/markdownlint/mdlrc.rb"
  local files=($(find ${BEBLSOFT} -name "*.md"                                             | \
                                  grep -v venv                                             | \
                                  grep -v node_modules                                     | \
                                  grep -v "playground/angular/angular2typescriptbook"      | \
                                  grep -v "playground/angular/mojourneyClient/src/assets"  | \
                                  grep -v "playground/vue/apps/copilot"                    | \
                                  grep -v "playground/bootstrap/BootstrapMade/NiceAdminMJ" | \
                                  grep -v "playground/vue/courses/Vue.jsCookbookSource"    | \
                                  grep -v "playground/docker/ecs-php-simple"))

  pushd ${BEBLSOFT} > /dev/null
  mdl --config ${mdlrcFile} ${files[*]}
  # mdl --config ${mdlrcFile} --git-recurse .
  popd > /dev/null
}


###############################################################################
#                              MAIN                                           #
###############################################################################
set -e # Abort on non-zero returns
markdownlint
exit $?