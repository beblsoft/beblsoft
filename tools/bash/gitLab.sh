#!/bin/bash
#
# File:
#  gitLab.sh
#
# Description
#  GitLab Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              RUNNER                                         #
###############################################################################
# --------------------- INSTALL ------------------------- #
function gitLabRunner_install()
{
  curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
  sudo apt-get install gitlab-runner
}

function gitLabRunner_uninstall()
{
  sudo apt-get remove -y gitlab-runner
}


# --------------------- UPDATE -------------------------- #
function gitLabRunner_update()
{
  sudo apt-get update
  sudo apt-get install gitlab-runner
}

function gitLabRunner_reloadConfig()
{
  sudo killall -SIGHUP gitlab-runner
}



# --------------------- REGISTER -------------------------- #
function gitLabRunner_dockerRegister()
{
  [ -z $1 ] && echo "gitLabRunner_dockerRegister <regToken>" && return


  local REG_TOKEN=$1
  sudo gitlab-runner register           \
      --non-interactive                 \
      --request-concurrency 4           \
      --url "https://gitlab.com/"       \
      --registration-token ${REG_TOKEN} \
      --executor "docker"               \
      --docker-image "docker:stable"    \
      --docker-privileged "true"        \
      --description "jb-home-desktop"   \
      --tag-list "docker"               \
      --run-untagged                    \
      --locked="true"
}
