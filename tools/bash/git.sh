#!/bin/bash
#
# File:
#  git.sh
#
# Description
#  Git Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function git_install()
{
  sudo apt install -y git
}

function git_uninstall()
{
  sudo apt remove -y git
}

# ---------------------------- CONFIG --------------------------------------- #
function git_config()
{
  #James User data
  # git config --global user.name "James Bensson"
  # git config --global user.email "benssonjames@gmail.com"
  # git config --global core.editor emacs

  #Darryl User data
  # git config --global user.name "Darryl Black"
  # git config --global user.email "darrylpblack@gmail.com"
  # git config --global core.editor gedit

  #Always rebase
  git config --global pull.rebase true

  #Aliases
  git config --global alias.unstage 'reset HEAD '
  git config --global alias.logg "log --color --graph --pretty=format:'%Cred%H%Creset -%C(yellow)%d%Creset %Cgreen(%ad)%Creset %C(bold blue)<%an>%Creset \"%s\"' --abbrev-commit --date=local"

  #Push
  git config --global push.default matching  #Push to remote branch matching local branch name
}


# ---------------------------- CLEAN ---------------------------------------- #
function git_clean()
{
  # Clean Out all non-tracked Beblsoft files, save for some important ones
  pushd ${BEBLSOFT} > /dev/null;
  sudo chown -R $(whoami):$(whoami) *
  git clean --force -x -d --exclude="tools/sublime/*"
  popd > /dev/null;
}
