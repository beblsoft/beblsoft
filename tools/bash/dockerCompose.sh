#!/bin/bash
#
# File:
#  dockerCompose.sh
#
# Description
#  Docker compose functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                               DOCKER COMPOSE                                #
###############################################################################
# ------------------------------ INSTALL ------------------------------------ #
function dockerCompose_install()
{
  sudo pip3 install docker-compose
  # sudo apt-get install -y docker-compose # Installs version 1.8
}

function dockerCompose_uninstall()
{
  sudo pip3 uninstall -y docker-compose
  # sudo apt-get remove -y docker-compose
}

