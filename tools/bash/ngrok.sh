#!/bin/bash
#
# File:
#  ngrok.sh
#
# Description
#  Ngrok Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function ngrok_install()
{
	# Reference: https://ngrok.com/download
	pushd ~/Downloads > /dev/null;
	wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
	unzip ngrok-stable-linux-amd64.zip
	sudo cp ngrok /sbin
	popd  > /dev/null;
	# Set ngrok authentication token
	# ./ngrok authtoken <Token>
}

function ngrok_uninstall()
{
	sudo rm /sbin/ngrok
}

