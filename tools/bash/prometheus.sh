#!/bin/bash
#
# File:
#  prometheus.sh
#
# Description
#  Prometheus functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                               PROMETHEUS                                    #
###############################################################################
# ------------------------------ INSTALL ------------------------------------ #
function prometheus_install()
{
  sudo apt-get install -y prometheus
  # Starts the daemon
}

function prometheus_uninstall()
{
  sudo apt-get remove -y prometheus
}


# ------------------------------ RUNNING ------------------------------------ #
function prometheus_start()
{
  sudo service prometheus start
}

function prometheus_stop()
{
  sudo service prometheus stop
}

function prometheus_status()
{
  sudo service prometheus status
}