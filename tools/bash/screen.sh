#!/bin/bash
#
# NAME
# ----------------------------
#  screen.sh
#
# FILES
# ----------------------------
#  Beblsoft Screenrc                   : $TOOLS/screen/screenrc
#  User Screenrc                       : ~/.screenrc
#  Run directory                       : /var/run/screen/S-jbensson/
#
# DESCRIPTION
# ----------------------------
# Screen is a full-screen window manager. Allows the following:
#  - Multiple shells from a single SSH session
#  - Keep shell active through network disruptions
#  - Disconnect and re-connect to a shell session from multiple locations
#  - Run long running processes without maintaining an active shell session
#
# GETTING IN
# ----------------------------
# Start a new session with name        : screen -S <name>
# List running sessions                : screen -list
# Attach to a running session name     : screen -r <name>
# Attach to session, kick out others   : screen -dRR <name>
# Detach a running session             : screen -d <name>
# Kill a detached session              : screen -X -S <name> quit
#
# GETTING OUT
# ----------------------------
# Detach                               : C-a d
# Exit Screen                          : C-a \
# Kill a screen session                : screen -X -S <session name> quit
# Clear dead sessions                  : screen -wipe
#
# HELP
# ----------------------------
# See help                             : C-a ?
#
# WINDOW MANAGEMENT
# ----------------------------
# Create new window                    : C-a c
# Change to last active window         : C-a a
# Change to window by number           : C-a <number,0-9>
# Change to window by number or name   : C-a '<number or title>
# Change to next window in list        : C-a n -or- C-a <space>
# Change to previous window in list    : C-a p -or- C-a <backspace>
# See window list                      : C-a "
# Kill current window                  : C-a k
# Kill all windows                     : C-a \
# Rename current window                : C-a A
# Rename current window                : C-a :title <new title>
# Renumber window                      : C-a :number <num>
#
# SPLIT SCREEN
# ----------------------------
# Split display horizontally           : C-a S
# Split display veritcally             : C-a | -or- C-a V
# Unsplit screen                       : C-a :remove
# Jump to next display region          : C-a tab
# Remove current region                : C-a X
# Remove all regious but the current   : C-a Q
#
# MISC
# ----------------------------
# Change session name                  : C-a :sessionname <name>
# Change screen number                 : C-a :number <number>
# Redraw window                        : C-a C-l
# Enter copy mode                      : C-a [
# Copy mode scroll up                  : C-u
# Copy mode scroll down                : C-d
# Paste                                : C-a ]
# Monitor window for activity          : C-a M
# Monitor window for silence           : C-a _
# Lock Display                         : C-a x
# Enter screen command                 : C-a :
# Enable logging in the screen session : C-a H
# Get out of the screen session        : Exit
#
# WORKAROUNDS
# ----------------------------
# EMACS not recognizing Ctrl-s         : C-a :flow off                <---Didn't work
#                                      : stty -ixon; stty stop undef  <----Did work
#


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function screen_install() {
	sudo apt-get install -y screen
}

function screen_uninstall() {
	sudo apt-get remove -y screen
}

function screen_init() {
	# Link user screenrc to Beblsoft screenrc
	ln -sf $TOOLS/screen/screenrc ~/.screenrc
}

