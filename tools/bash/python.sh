#!/bin/bash
#
# File:
#  python.sh
#
# Description
#  Python functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                             NOTES                                           #
###############################################################################
# To install package for a particular version of python
#   - python3.5 -m pip install six
#   - python3.6 -m pip install six


###############################################################################
#                             PYTHON PATH                                     #
###############################################################################
export PYTHONPATH=${PYTHONPATH}:"${BEBLSOFT}":"${PROJECTS}"


###############################################################################
#                                PYTHON3                                      #
###############################################################################
function python3_linuxMint_install()
{
  local PYTHON_VERS=${1:-"3.6"} # Arg #1, Default=3.6

  sudo add-apt-repository -y ppa:jonathonf/python-${PYTHON_VERS}
  sudo apt-get update -y
  sudo apt-get install -y python${PYTHON_VERS}
  sudo apt-get install -y python${PYTHON_VERS}-dev
  sudo apt-get install -y python${PYTHON_VERS}-venv
  sudo apt-get install -y libffi-dev libssl-dev libxml2-dev libxslt1-dev libjpeg8-dev zlib1g-dev
  sudo apt-get install -y virtualenv
  sudo apt-get install -y python3-pip
  sudo pip3 install --upgrade pip
  sudo apt-get autoremove -y
  sudo pip3 install setuptools
  sudo pip3 install pylint

  #Python Versions
  python3 --version
  $(head -1 `which pip3` | tail -c +3) --version
}
function python3_ubuntu_install() { python3_linuxMint_install; }

function python3_redHat_install()
{
  local PYTHON_VERS=${1:-"3.6.3"}
  local INSTALL_DIR="/opt"
  local TMP_DIR="/tmp"

  pushd ${TMP_DIR}; sudo chmod -R 777 ${TMP_DIR}
  wget --no-check-certificate https://www.python.org/ftp/python/${PYTHON_VERS}/Python-${PYTHON_VERS}.tgz
  tar -xzf Python-${PYTHON_VERS}.tgz;
  cd Python-${PYTHON_VERS};
  ./configure --with-zlib --prefix=${INSTALL_DIR}; make; chmod -R 777 ${TMP_DIR};
  sudo make install #Optional 'make test' after make
  cd ${TMP_DIR}; rm -rf Python*
  python3 --version
  popd;
}

function python3_setupVenv()
{
  local PYTHON_VERS=${1:-"3.6"}

  if [ -d "./venv" ]; then
    sudo rm -rf ./venv
  fi
  virtualenv -p /usr/bin/python${PYTHON_VERS} venv
  source venv/bin/activate
  pip3 install -r requirements.txt
  deactivate
}
