#!/bin/bash
#
# File:
#  lib.sh
#
# Description
#  Shell script library


###############################################################################
#                                  PATHS                                      #
###############################################################################
export BEBLSOFT="${HOME}/git/beblsoft"
export PLAY="${BEBLSOFT}/playground"
export TOOLS="${BEBLSOFT}/tools"
export BASE="${BEBLSOFT}/base"
export TEMPLATES="${BEBLSOFT}/templates"
export RESEARCH="${BEBLSOFT}/research"
export PROJECTS="${BEBLSOFT}/projects"
export MOJOURNEY="${PROJECTS}/mojourney"
export QUOTE="${PROJECTS}/quote"
export SMECKN="${PROJECTS}/smeckn"
export ZUMPER="${PROJECTS}/zumper"
export CICD="${PROJECTS}/cicd"
export BASH_PROFILE="${TOOLS}/bash/bash_profile"
export EMACS_INIT="${TOOLS}/emacs/jbensson.el"
export CRON_JOB="${PLAY}/bash/cron/cron.jbensson"
export SCREEN_INIT="${TOOLS}/screen/screenrc"
