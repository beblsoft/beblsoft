#!/bin/bash
#
# File:
#  linuxMint.sh
#
# Description
#  Linux Mint functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENATION                                   #
###############################################################################
# OVERVIEW
# --------------------------
# - modern, elegant and comfortable operating system
# - powerful and easy to use.
# - one of the most popular desktop Linux distributions and used by
#   millions of people.
# - Works out of the box, with full multimedia support and is extremely easy to use.
# - Free of cost and open source.
# - Community-driven. Users are encouraged to send feedback to the project
# - Based on Debian and Ubuntu, it provides about 30,000 packages and one of
#   the best software managers.
# - It's safe and reliable. Thanks to a conservative approach to software
#   updates, a unique Update Manager and the robustness of its Linux
#   architecture, Linux Mint requires very little maintenance
#   (no regressions, no antivirus, no anti-spyware...etc).
#
# KEY FILES
# --------------------------
# - Upstream release             : /etc/upstream-release/lsb-release
#



###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
# -------------- RELEASE METADATA ----------------------- #
function lm_getRelease()
{
	echo $(lsb_release -r                         | \
	       grep Release                           | \
	       awk '{split($0, a, ":"); print a[2]}')
}

function lm_getCodename()
{
	echo $(lsb_release -c                         | \
	       grep Codename                          | \
	       awk '{split($0, a, ":"); print a[2]}')
}

# -------------- UBUNTU UPSTREAM ------------------------ #
function lm_getUbuntuRelease()
{
	# Ex. "16.04"
	echo $(cat /etc/upstream-release/lsb-release   | \
	       grep DISTRIB_RELEASE                    | \
	       awk '{split($0, a, "="); print a[2]}')
}

function lm_getUbuntuCodename()
{
	# Ex. "xenial"
	echo $(cat /etc/upstream-release/lsb-release   | \
	       grep DISTRIB_CODENAME                   | \
	       awk '{split($0, a, "="); print a[2]}')
}