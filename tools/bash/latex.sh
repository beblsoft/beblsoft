#!/bin/bash
#
# File:
#  latex.sh
#
# Description
#  Latex Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function latex_install()
{
  sudo apt-get update
  sudo apt-get install -y texlive-full
  sudo apt-get install -y latexmk
  sudo apt-get install -y texmaker
  sudo apt-get install -y evince # PDF Viewer
}

function latex_uninstall()
{
  sudo apt-get remove -y evince # PDF Viewer
  sudo apt-get remove -y latexmk
  sudo apt-get remove -y texmaker
  sudo apt-get remove -y texlive-full
}
