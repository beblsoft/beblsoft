#!/bin/bash
#
# File:
#  ssh.sh
#
# Description
#  Secure Shell (SSH) related scripts and documentation


###############################################################################
#                               ALIASES                                       #
###############################################################################
# See below for passwordless ssh instructions
alias o0y=" ssh nshgb2413 -Y"
alias o0="  ssh nshgb2413"
alias o1="  ssh nshgb2401"
alias o2="  ssh nshgb2402"
alias o3="  ssh nshgb2403"
alias o4="  ssh nshgb2404"
alias o5="  ssh nsh00aai"
alias o6="  ssh nsh00aaj"
alias o7="  ssh slc07qsw"
alias o8="  ssh slc05dvq"
alias o9="  ssh josmith-vm4"
alias o10=" ssh 10.149.239.126"
alias o11=" ssh nshgc11" #User/Password:grid/Baldeagle
alias o12=" ssh nshgc12" #User/Password:grid/Baldeagle
alias o13=" ssh nshgb2711"
alias o14=" ssh nshgb2712"
alias o15=" ssh nshga2310"
alias o16="  "
alias o17=" ssh denab887"
alias o18=" ssh denab888"
alias pi_ssh="ssh pi@192.168.1.10"


###############################################################################
#                               FUNCTIONS                                     #
###############################################################################
function seed()
{
  # "seed" a remote host using your RSA public key
  ssh "${1}" "umask 077; test -d ~/.ssh || mkdir ~/.ssh ;    \
  echo '$(< ~/.ssh/id_rsa.pub)' >> ~/.ssh/authorized_keys" ; \
}

function sshPass()
{
  #Allow current node to ssh to argument node without password.
  #Does the following:
  # - Correctly permissions ssh files
  # - If current node doesn't already have public key, generate one.
  #   In this step, when prompted, just hit ENTER.
  # - Puts current node public key inside argument node authorized keys file
  # - Adds new crediatials to local ssh client
  #Note:
  # - Remote node must have correct directory permissions:
  #    chmod go-w ~
  #    chmod 700 ~/.ssh
  #    chmod 600 ~/.ssh/*
  # - Reboot may be required for SSH daemons to pick up on changes
  #
  [ -z ${1} ] && echo "sshPass <node>" && return
  node=${1}
  sshDir="${HOME}/.ssh"
  sshPubFile="${sshDir}/id_rsa.pub"
  sshAuthKeysFile="${sshDir}/authorized_keys"

  [ ! -e ${sshDir} ] && mkdir ${sshDir}
  chmod go-w $HOME
  chmod go-rw ${sshDir}

  if [[ ! -e  ${sshPubFile} ]];
  then
      ssh-keygen -t rsa
      #Useful for shared home, all nodes can ssh to each other
      cat ${sshPubFile} >> ${sshAuthKeysFile}
  fi

  ssh ${USER}@${node} "cat >> ${sshAuthKeysFile}" < ${sshPubFile}
}

