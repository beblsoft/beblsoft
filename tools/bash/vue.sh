#!/bin/bash
#
# File:
#  vue.sh
#
# Description
#  Vue Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function vue_install() {
	sudo npm uninstall -g vue-cli  # Uninstall v.1, v.2
	sudo npm install -g @vue/cli   # Install v.3
	sudo npm install -g @vue/cli-service-global
}

function vue_uninstall() {
	sudo npm uninstall -g vue-cli
	sudo npm uninstall -g @vue/cli
	sudo npm uninstall -g @vue/cli-service-global
}