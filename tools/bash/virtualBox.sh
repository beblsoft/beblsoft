#!/bin/bash
#
# File:
#  virtualBox.sh
#
# Description
#  Virtual Box Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function virtualBox_installLinuxMint18()
{
	# Installation:
	# https://www.virtualbox.org/wiki/Linux_Downloads
	# https://www.itzgeek.com/how-tos/linux/linux-mint-how-tos/install-virtualbox-4-3-on-linux-mint-17.html

	# Enable BIOS for virtualization:
	# https://ourcodeworld.com/articles/read/708/how-to-solve-virtualbox-exception-vt-x-is-disabled-in-the-bios-for-both-all-cpu-modes-verr-vmx-msr-all-vmx-disabled
	wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
	echo "deb http://download.virtualbox.org/virtualbox/debian xenial contrib"  | sudo tee /etc/apt/sources.list.d/virtualbox.list
  sudo apt-get update -y
  sudo apt-get upgrade -y
  sudo apt-get install -y virtualbox-6.0
  sudo apt-get autoremove -y
}

function virtualBox_uninstall()
{
  sudo apt-get remove -y virtualbox-6.0
  sudo apt-get remove -y virtualbox
  sudo apt-get remove -y virtualbox-dkms
  sudo apt-get purge -y virtualbox
}

