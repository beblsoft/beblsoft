#!/bin/bash
#
# File:
#  sqlite.sh
#
# Description
#  SQLite functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# OVERVIEW
# --------------------------
# SQLite is a relational database management system contained in a C programming
# library. In contrast to many other database management systems, SQLite is not
# a client–server database engine. Rather, it is embedded into the end program.
#


###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
# ----------------- INSTALL ----------------------------- #
function sqlite_install()
{
	sudo apt-get install -y sqlite
}

function sqlite_uninstall()
{
	sudo apt-get remove -y sqlite
}
