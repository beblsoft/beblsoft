#!/bin/bash
#
# File:
#  mysql.sh
#
# Description
#  MySQL functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# OVERVIEW
# --------------------------
# MySQL is an open-source relational database management system (RDBMS).
#
# BASIC USAGE
# --------------------------
# - Help                             : mysql --help
# - Connect as client                : mysql -h localhost -u root -p
# - Run script, pipe to output       : mysql < inputfile > outputfile
#
# DUMP DATABASE
# --------------------------
# - Dump db contents to file         > msqldump body > dump.sql
# - Send db to another host          > mysql -h other-host.example.com cookbook < dump.sql


###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
# ----------------- CLIENT INSTALL ---------------------- #
function mysqlClient_install()
{
  sudo apt-get update -y
  sudo apt-get install -y mysql-common
  sudo apt-get install -y mysql-client
  sudo apt-get install -y libmysqlclient-dev
}

function mysqlClient_uninstall()
{
  sudo apt-get remove -y mysql-client
  sudo apt-get remove -y mysql-common
}

# ----------------- SERVER INSTALL ---------------------- #
function mysqlServer_install()
{
  sudo apt-get update -y
  sudo apt-get install -y mysql-server
  sudo apt-get install -y mysql-common
}

function mysqlServer_uninstall()
{
  sudo apt-get remove -y mysql-server
}

function mysqlServer_status()
{
	sudo service mysql status
}

# ----------------- USER CONTROL ------------------------ #
# - Show user table                  > SHOW CREATE TABLE mysql.user;
# - Examine users                    > SELECT USER,HOST,PASSWORD from mysql.user;
# - Create user                      > CREATE USER 'jbensson'@'localhost' IDENTIFIED BY 'password';
# - Change way passwords are hashed  > SET old_passwords = 1;
# - Global Permissions               > GRANT ALL    ON *.*        TO 'jbensson'@'localhost';
# - Database Permissions             > GRANT SELECT ON mysql.*    TO 'jbensson'@'localhost';
# - Table Permissions                > GRANT SELECT ON mysql.user TO 'jbensson'@'localhost';
# - Display permissions              > SHOW GRANTS FOR 'jbensson'@'localhost';
# - Change password                  > SET PASSWORD FOR 'jbensson'@'localhost' = PASSWORD('testpass');
# - Remove User                      > DROP USER 'jbensson'@'localhost';
# - Rename user                      > RENAME USER 'jbensson'@'localhost' TO 'newuser'@'localhost'
function mysql_createUser()
{
	[ -z ${1} ] && echo "Specify User" && return
	local user=${1}

	# mysql -e "SET old_passwords = 1;"
	sudo mysql -e "CREATE USER '${user}'@'localhost';"
	sudo mysql -e "GRANT ALL    ON *.*        TO '${user}'@'localhost';"
	sudo mysql -e "GRANT SELECT ON mysql.*    TO '${user}'@'localhost';"
	sudo mysql -e "GRANT SELECT ON mysql.user TO '${user}'@'localhost';"
}

function mysql_dropUser()
{
	[ -z ${1} ] && echo "Specify User" && return
	local user=${1}

	sudo mysql -e "DROP USER '${user}'@'localhost';"
}

function mysql_showUsers()
{
	mysql -e "SELECT USER,HOST from mysql.user;"
}

# ----------------- DATABASE CONTROL -------------------- #
# - Show all databases               > SHOW DATABASES;
# - Create database                  > CREATE DATABASE body;
# - Set database as default          > USE body;
# - Create table with specified rows > CREATE TABLE limbs (thing VARCHAR(20), legs INT, arms INT);
# - Insert row into database         > INSERT INTO limbs (things,legs,arms) VALUES ('human',2,2);
# - Show table                       > SELECT * FROM limbs
# - Show columns                     > SHOW COLUMNS FROM limbs;
# - Vertical Columns                 > SHOW FULL COLUMNS FROM limbs LIKE 'thing'\G
# - Drop table                       > DROP TABLE IF EXISTS limbs;
# - Drop database                    > DROP DATABASE IF EXISTS cookbook;
function mysql_createDatabase()
{
	[ -z ${1} ] && echo "Specify DB" && return
	local db=${1}

	mysql -e "CREATE DATABASE ${db};"
}

function mysql_dropDatabase()
{
	[ -z ${1} ] && echo "Specify DB" && return
	local db=${1}

	mysql -e "DROP DATABASE IF EXISTS ${db};"
}

function mysql_showDatabases()
{
	mysql -e "SHOW DATABASES;"
}

function mysql_renameDatabase()
{
	[ -z ${2} ] && echo "mysql_renameDatabase <oldName> <newName>" && return
	local oldName=${1}
	local newName=${2}

	mysql_createDatabase $newName
	mysql $oldName -sNe "SHOW TABLES" | while read table; \
	  do mysql -sNe "RENAME table ${oldName}.${table} to ${newName}.${table};"; done
	mysql_dropDatabase $oldName
}

# ----------------- DUMP DATABASE ----------------------- #
function mysql_dumpDatabase()
{
	[ -z ${1} ] && echo "Specify database" && return

	local db=${1}
	local file="/tmp/dump-${db}.sql"

	mysqldump --databases ${db} > ${file}
	echo "Dumped to ${file}"
}

function mysql_dumpDatabaseTStamp()
{
	[ -z ${1} ] && echo "Specify database" && return

	local db=${1}
	local tStamp=$(date "+%Y%m%d:%H%M%S")
	local file="/tmp/dump-${db}-${tStamp}.sql"

	mysqldump --databases ${db} > ${file}
	echo "Dumped to ${file}"
}

function mysql_importDatabase()
{
	[ -z ${1} ] && echo "Specify file" && return

	local file=${1}

	mysql < $file
}

# ----------------- CONNECTIONS ------------------------- #
function mysql_showMaxConnections()
{
	mysql -e "SHOW VARIABLES LIKE 'max_connections';"
}

function mysql_setMaxConnections()
{
	mysql -e "SET GLOBAL max_connections = 1024;"
}

# ----------------- VERSION ----------------------------- #
function mysql_getVersion()
{
	mysql -e 'SHOW VARIABLES LIKE "%version%"';
}

# ----------------- AWS DATABASE ------------------------ #
function mysql_awsConnect()
{
	mysql -u jbensson -h quotetestdbinstance.cwmxk4jdpud3.us-east-1.rds.amazonaws.com -p
}
