#!/bin/bash
#
# File:
#  markdownlint.sh
#
# Description
#  Markdownlint functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function markdownlint_install() {
  sudo gem install mdl;
}


function markdownlint_uninstalll() {
  sudo gem uninstall mdl --all --executables;
}


function markdownlint_checker() {
  pushd ${TOOLS}/markdownlint > /dev/null;
  docker-compose run markdownlint;
  popd > /dev/null;
}