#!/bin/bash
#
# File:
#  proxyt.sh
#
# Description
#  ProxyT functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENATION                                   #
###############################################################################
# EXECUTION
# --------------------------
# - Start Oracle CISCO
# - proxyt_vanilla
#
#
# SOURCES
# --------------------------
# Documentation Site          : http://wiki.us.oracle.com/calpg/TransparentProxy
# Source Downloads            : http://kernel.us.oracle.com/~jhaxby/proxyt
# Git Repo                    : git://git.uk.oracle.com/util/proxyt
#                               origin  git://git.uk.oracle.com/util/proxyt (fetch)
#                               origin  git://git.uk.oracle.com/util/proxyt (push)
# jbensson repo               : git@bitbucket.org:jbensson/proxyt.git
#
#
# BASIC
# --------------------------
# Process: proxyt             : Spawned at init
# Process: proxytctl          : Pipes in commands to proxyt over /run/proxyt.sock
# Log file                    : /var/log/syslog
# SystemD proxyt startup file : /lib/systemd/system/proxyt.service
#                               proxyt -D = debug mode
#
#
# SOLVED ISSUES
# --------------------------
# If proxyT isn't working
# uninstall docker            : docker_uninstall
# Proxyt wasn't seeing any
# traffic                     : Remove network settings > network proxy >
#                               automatic > http://wpad.oraclecorp.com/wpad.dat
#                               Reinstall proxyt from source
#
# RELATED COMMANDS
# --------------------------
# List IP tables
# (Should show proxyt chain)  : sudo iptables --list
# Save IP Tables              : sudo iptables --save



###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
alias proxyt_start="     sudo service proxyt start    "
alias proxyt_stop="      sudo service proxyt stop     "
alias proxyt_restart="   sudo service proxyt restart  "
alias proxyt_tailsys="   tail -f /var/log/syslog      "
alias proxyt_tailt="     tail /tmp/proxyt.log         "

function proxyt_build()
{
  pushd ~/git/proxyt; make clean; make all; popd
}

function proxyt_install()
{
  pushd ~/git/proxyt; sudo su -c "make install"; popd
}

function proxyt_uninstall()
{
  sudo rm -rf $(sudo find /usr -name "*proxyt*")
  sudo rm -rf $(sudo find /run -name "proxyt.sock")
  sudo rm -rf $(sudo find /sys -name "*proxyt*")
  sudo rm -rf $(sudo find /lib -name "*proxyt*")
}

function proxyt_status()
{
  sudo proxytctl status
  sudo service proxyt status
}

function proxyt_vanilla()
{
  proxyt_stop
  proxyt_uninstall
  proxyt_build
  proxyt_install
  proxyt_start
}

function proxyt_install_dist()
{
  local vers="proxyt-3.2.1"
  pushd /tmp
  wget http://kernel.us.oracle.com/~jhaxby/proxyt/${vers}.tar.gz
  tar xf ${vers}.tar.gz
  pushd ${vers}
  make
  sudo su -c "make install"
  popd;
  rm -rf ${vers};rm -rf ${vers}.tar.gz
  popd;
}