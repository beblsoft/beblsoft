#!/bin/bash
#
# File:
#  kazam.sh
#
# Description
#  Kazam Scripts and Documentation
#
# Recording:
#  - Open Kazam: Click Menu, Type "kazam", Click Kazam Icon
#  - Select screencast/screenshot settings
#  - Click Capture to start
#  - Record Video
#  - To stop, right click Kazam video recording bar icon on menu mar, click finish recording


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function kazam_install()
{
	sudo apt-get install -y kazam
}

function kazam_uninstall()
{
  sudo apt-get remove -y kazam
}

