#!/bin/bash
#
# File:
#  pylint.sh
#
# Description
#  Pylint functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh



###############################################################################
#                              PYLINT                                         #
###############################################################################
function pylint_checker()
{
  [ -z $1 ] && echo "pylint_checker <command> ..." && return 1;
  local TARGET=$1

  pushd ${TOOLS}/pylint > /dev/null;
  case $TARGET in
    base)       docker-compose run pylint pylint_base;;
    cicd)       docker-compose run pylint pylint_cicd;;
    mojourney)  docker-compose run pylint pylint_mojourney;;
    quote)      docker-compose run pylint pylint_quote;;
    smeckn)     docker-compose run pylint pylint_smeckn;;
    all)        pylint_checker base
                pylint_checker mojourney
                pylint_checker quote
                pylint_checker smeckn;;
  esac
  popd > /dev/null;
}

