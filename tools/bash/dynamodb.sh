#!/bin/bash
#
# File:
#  dynamodb.sh
#
# Description
#  DynamoDB functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                                DYNAMO DB                                    #
###############################################################################
DYNAMODB_INSTALL_DIR="~/dynamodb"


function dynamodb_install()
{
  # Instructions: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html
  eval mkdir $DYNAMODB_INSTALL_DIR
  eval pushd $DYNAMODB_INSTALL_DIR
  wget --continue https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz
  extract dynamodb_local_latest.tar.gz
  popd
}

function dynamodb_uninstall()
{
  eval rm -rf $DYNAMODB_INSTALL_DIR
}


function dynamodb_run()
{
  eval pushd $DYNAMODB_INSTALL_DIR
  # DynamoDB options
  # -cors                    : Enables CORS support. Default is "*"
  # -dbPath                  : Path where DynamoDB writes DB files
  # -delayTransientStatuses  : Introduce delays that web app has
  # -help                    : Usage
  # -inMemory                : Run in memory, no data is persisted
  # -optimizeDbBeforeStartup
  # -port <value>            : Port number dynamodb uses
  # -sharedDb                : Use single database file instead of separate
  java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
  popd
}
