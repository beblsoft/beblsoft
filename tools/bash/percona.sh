#!/bin/bash
#
# File:
#  percona.sh
#
# Description
#  Percona functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh
. ${SCRIPT_DIR}/linuxMint.sh


###############################################################################
#                              DOCUMENATION                                   #
###############################################################################
# OVERVIEW
# --------------------------
# Percona XtraBackup
#  - Free, open source, complete online backup solution for Percona Server MySQL,
#    MySQL, and MariaDB.
#  - Performs online non-blocking, tightly compressed, highly secure backups on
#    transactional systems so that applications remain fully available
#    during planned maintenance windows.
#
# LINKS
# --------------------------
# - Ubuntu Install    : https://www.percona.com/doc/percona-server/LATEST/installation/apt_repo.html
# - Creating a backup : https://www.percona.com/doc/percona-xtrabackup/LATEST/innobackupex/creating_a_backup_ibk.html#
# - Partial backup    : https://www.percona.com/doc/percona-xtrabackup/LATEST/innobackupex/partial_backups_innobackupex.html


###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
# ----------------- INSTALL ----------------------------- #
function percona_install()
{
	local ucn=$(lm_getUbuntuCodename)

	pushd /tmp;
	wget https://repo.percona.com/apt/percona-release_0.1-4.${ucn}_all.deb
	sudo dpkg -i percona-release_0.1-4.${ucn}_all.deb
	sudo apt-get update
	sudo apt-get install -y percona-xtrabackup-24
	popd;
}

function percona_uninstall()
{
	sudo apt-get remove -y percona-xtrabackup-24
}

# ----------------- BACKUP ------------------------------ #
function percona_backup()
{
	local user="jbensson"
	local backupDir="/tmp/backup"

	sudo innobackupex --user=${user} ${backupDir}
	sudo chmod -R 777 ${backupDir}
}
