#!/bin/bash
#
# File:
#  ruby.sh
#
# Description
#  Ruby scripts


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


# Only works after ruby_install
# - https://rvm.io/integration/ubuntu-on-windows
[[ -f /usr/share/rvm/scripts/rvm ]] && source /usr/share/rvm/scripts/rvm


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function ruby_install() {
  # Install RVM
  #  - http://rvm.io/rvm/install
  #  - https://github.com/rvm/ubuntu_rvm
	sudo gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  sudo apt-get -y install software-properties-common
  sudo apt-add-repository -y ppa:rael-gc/rvm
  sudo apt-get update -y
  sudo apt autoremove -y
  sudo apt-get install -y rvm

  # Install Ruby
  rvmsudo rvm install ruby
  rvmsudo rvm install 2.6.3
  rvm use 2.6.3
}

function ruby_uninstall() {
	sudo apt-get uninstall -y rvm
}


###############################################################################
#                              RVM                                            #
###############################################################################
function rvm_info() {
  rvm info
}

function rvm_version() {
  rvm version
}

function rvm_list_installed() {
  rvm list
}

function rvm_list_known() {
  rvm list known
}

function rvm_setRubyVersion {
  local RUBY_VERS=${1:-"2.6.3"} # Arg #1, Default=2.6.3

  rvm use ${RUBY_VERS}
}