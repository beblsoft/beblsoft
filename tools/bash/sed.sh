#!/bin/bash
#
# File:
#  sed.sh
#
# Description
#  Useful sed expressions


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh



###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function sed_codeReplacer()
{
  # find . -type f -name "*.py" -exec sed -i 's/base\.pyutl\.misc\.bRun/base.pyutl.core.run.bRun/g' {} +
  # find . -type f -name "*.py" -exec sed -i 's/base\.pyutl\.misc\.bSingleton/base.pyutl.core.run.bSingleton/g' {} +

  # find . -type f -name "*.py" -exec sed -i 's/#-----------------/# ----------------/g' {} +
  # find . -type f -name "*.py" -exec sed -i 's/smeckn\.server\.stripe/smeckn.server.payments/g' {} +
  # find . -type f -name "*.py" -exec sed -i 's/mgr\.stripe/mgr.payments/g' {} +
  # find . -type f -name "*.py" -exec sed -i 's/smeckn\.server\.sJWT/smeckn.server.webToken/g' {} +

  # pushd $BEBLSOFT
  # find . -type f -name "*.py" -exec sed -i "s/CloudWatch\.bLogGroup/CloudWatchLogs.bLogGroup/g" {} +
  # popd

  pushd $SMECKN/server
  find . -type f -name "*.py" -exec sed -i "s/@requestStart(/@requestWrapper(/g" {} +
  find . -type f -name "*.py" -exec sed -i "s/wAPIV1\.common\.requestStart import requestStart/wAPIV1.common.requestWrapper import requestWrapper/g" {} +
  popd
}
