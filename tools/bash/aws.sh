#!/bin/bash
#
# File:
#  aws.sh
#
# Description
#  AWS functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# - Notes
#   * AWS CLI                              : Allows programmatic access to AWS resources
# - Key Files
#   * Configuration file                   : ~/.aws/config
#   * Credential file                      : ~/.aws/credentials


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function aws_install()
{
	sudo apt-get install -y awscli
}

function aws_uninstall()
{
	sudo apt-get remove -y awscli
}

function aws_configure()
{
	# Setup CLI with credentials
	# Create AWS Access Key : mgmtconsole > IAM > users > jbensson > Security Credentials > Create Access Key
	echo "------ README ----------------"
	echo "To configure aws for Beblsoft use..."
	echo "Have Beblsoft admin create you an account"
	echo "Create access keys:"
  echo " - Visit AWS Management Console: https://aws.amazon.com/console/"
  echo " - Sign In"
  echo " - Services > IAM > users"
  echo " - Select User I.e. dblack"
  echo " - Security Credentials"
  echo " - Create Access Key  "
	echo "Default Region Name: us-east-1"
	echo "Default Output Format: json"
	echo "------------------------------"
	aws configure
  # AWS Access Key ID [None]: <Access Key>
  # AWS Secret Access Key [None]: <Secret Key>
  # Default region name [None]: us-east-1
  # Default output format [None]: json
}

function aws_exportCredentials()
{
	AWS_CRED_FILE="$HOME/.aws/credentials"

	if [ ! -f $AWS_CRED_FILE ]; then
		echo "AWS Credential file doesn't exist"
	else
    export AWS_ACCESS_KEY_ID=$(cat ${AWS_CRED_FILE} | grep aws_access_key_id | awk '{ print $3 }')
    export AWS_SECRET_ACCESS_KEY=$(cat ${AWS_CRED_FILE} | grep aws_secret_access_key | awk '{ print $3 }')
	fi
}