#!/bin/bash
#
# File:
#  nodejs.sh
#
# Description
#  NodeJS, NPM functions and Documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                               NODEJS                                        #
###############################################################################
# OVERVIEW
# --------------------------
# - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.
# - Uses an event-driven, non-blocking I/O model that makes it lightweight and
#   efficient.
# - Package ecosystem is npm, the largest ecosystem of open source libraries
#   in the world.
function nodejs_install() {
  sudo apt-get remove -y nodejs
  sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt-get install -y nodejs
}

function nodejs_upgrade() {
	sudo npm cache clean -f
	sudo npm install -g n
	sudo n stable
}

function nodejs_uninstall() {
	sudo apt-get remove -y nodejs
}

function nodejs_version() {
	nodejs -v
}


###############################################################################
#                                NPM                                          #
###############################################################################
function npm_install() {
	sudo npm install -g npm@latest
}

function npm_version() {
	npm -v
}

function npm_setup() {
  npm config set init-author-name "James Bensson"
  npm config set init-author-email "bensson.james@gmail.com"
  npm config set init-license "Beblsoft LLC"
}

function npm_link() {
	pushd ~;

	# NPM Link Demo
	cd $PLAY/npm/npm-link-demo;        sudo npm link              #           global -> local
	cd $PLAY/npm/npm-link-demo-client; npm link npm-link-demo     # client -> global -> local

	popd
}