#!/bin/bash
#
# File:
#  yaml.sh
#
# Description
#  Yaml Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function yaml_install()
{
  sudo apt-get install -y libyaml-dev
}

function yaml_uninstall()
{
  sudo apt-get remove -y libyaml-dev
}

