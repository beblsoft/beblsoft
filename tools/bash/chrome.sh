#!/bin/bash
#
# File:
#  chrome.sh
#
# Description
#  Google Chrome functions and documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function chrome_install()
{
	pushd ~/Downloads > /dev/null
  sudo apt-get update && sudo apt-get install -f # Dependencies
	sudo apt-get install libxss1 libappindicator1 libindicator7
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo apt-get install -y ./google-chrome*.deb
  popd > /dev/null
}

function chrome_uninstall()
{
	sudo apt-get remove -y google-chrome-stable
}
