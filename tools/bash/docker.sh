#!/bin/bash
#
# File:
#  docker.sh
#
# Description
#  Docker functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              TROUBLESHOOTING                                #
###############################################################################
# Common things to try:
#  - export DOCKER_HOST=unix:///var/run/docker.sock
#  - Images cannot have uppercase


###############################################################################
#                              INSTALL                                        #
###############################################################################
function docker_install() {
  # Linux Mint
  UBUNTU_CODENAME=$(. /etc/os-release; echo "$UBUNTU_CODENAME")
  sudo apt-get update
  sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${UBUNTU_CODENAME} stable"
  sudo apt-get update
  sudo apt-get install -y docker-ce docker-ce-cli containerd.io
  sudo groupadd docker
  sudo usermod -aG docker $USER # Allow use without sudo (relogin needed)
}

function docker_installOverlay2() {
  # sudo docker save                             # Save any existing images
  # sudo service docker stop                     # Stop docker
  # sudo mv /var/lib/docker /var/lib/docker.bk   # Backup if need be
  # sudo emacs -nw /etc/docker/daemon.json       # (add if it doesn't exist)
  #  {
  #    "storage-driver": "overlay2"
  #  }
  # sudo service docker start                    # Start docker
  sudo docker info                               # Check storage driver
}

function docker_uninstall() {
  sudo apt-get purge -y docker-ce docker-ce-cli
  sudo rm -rf /var/lib/docker  # Remove all images, containers, volumes, or customized configuration
}


###############################################################################
#                              REGISTRY                                       #
###############################################################################
function docker_login() {
  docker login
}

function docker_loginGitLab() {
  docker login registry.gitlab.com
}

function docker_logout() {
  docker logout
}


###############################################################################
#                              DAEMON                                         #
###############################################################################
function docker_status() {
  sudo service docker status
}

function docker_start() {
  sudo service docker start
}

function docker_stop() {
  sudo service docker stop
}


###############################################################################
#                              IMAGES                                         #
###############################################################################
function docker_build() {
  TARGET=$1
  case $TARGET in
    smeckn)              pushd ${SMECKN} > /dev/null;
                         time docker-compose build base;
                         time docker-compose build --parallel server webAPI webClient aws licenseFinder email;
                         popd > /dev/null;;
    smecknBase)          pushd ${SMECKN} > /dev/null;
                         time docker-compose build base;
                         popd > /dev/null;;
    smecknServer)        pushd ${SMECKN} > /dev/null;
                         time docker-compose build server;
                         popd > /dev/null;;
    smecknWebAPI)        pushd ${SMECKN} > /dev/null;
                         time docker-compose build webAPI;
                         popd > /dev/null;;
    smecknWebClient)     pushd ${SMECKN} > /dev/null;
                         time docker-compose build webClient;
                         popd > /dev/null;;
    smecknAWS)           pushd ${SMECKN} > /dev/null;
                         time docker-compose build aws;
                         popd > /dev/null;;
    smecknLicenseFinder) pushd ${SMECKN} > /dev/null;
                         time docker-compose build licenseFinder;
                         popd > /dev/null;;
    smecknEmail)         pushd ${SMECKN} > /dev/null;
                         time docker-compose build email;
                         popd > /dev/null;;
    pylint)              pushd ${TOOLS}/pylint > /dev/null;
                         time docker-compose build;
                         popd > /dev/null;;
    markdownlint)        pushd ${TOOLS}/markdownlint > /dev/null;
                         time docker-compose build;
                         popd > /dev/null;;
    ubuntuDIND)          pushd ${TOOLS}/ubuntuDIND > /dev/null;
                         time docker build -t registry.gitlab.com/beblsoft/beblsoft/tools/ubuntudind:latest .
                         popd > /dev/null;;
    *)                   echo "docker_build <command> ..." && return 1;;
  esac
}

function docker_push() {
  TARGET=$1
  case $TARGET in
    smeckn)         pushd ${SMECKN} > /dev/null;
                    time docker-compose push;
                    popd > /dev/null;;
    pylint)         pushd ${TOOLS}/pylint > /dev/null;
                    time docker-compose push;
                    popd > /dev/null;;
    markdownlint)   pushd ${TOOLS}/markdownlint > /dev/null;
                    time docker-compose push;
                    popd > /dev/null;;
    ubuntuDIND)     time docker push registry.gitlab.com/beblsoft/beblsoft/tools/ubuntudind:latest;;
    *)              echo "docker_push <command> ..." && return 1;;
  esac
}

function docker_pull() {
  TARGET=$1
  case $TARGET in
    smeckn)         pushd ${SMECKN} > /dev/null;
                    time docker-compose pull;
                    popd > /dev/null;;
    pylint)         pushd ${TOOLS}/pylint > /dev/null;
                    time docker-compose pull;
                    popd > /dev/null;;
    markdownlint)   pushd ${TOOLS}/markdownlint > /dev/null;
                    time docker-compose pull;
                    popd > /dev/null;;
    ubuntuDIND)     time docker pull registry.gitlab.com/beblsoft/beblsoft/tools/ubuntudind:latest;;
    *)              echo "docker_pull <command> ..." && return 1;;
  esac
}


###############################################################################
#                              CONTAINERS                                     #
###############################################################################
function docker_dind() {
  docker run -it                                             \
    -e AWS_ACCESS_KEY_ID                                     \
    -e AWS_SECRET_ACCESS_KEY                                 \
    -e "BEBLSOFT=/root/git/beblsoft"                         \
    -e "TOOLS=/root/git/beblsoft/tools"                      \
    -e "PROJECTS=/root/git/beblsoft/projects"                \
    -e "SMECKN=/root/git/beblsoft/projects/smeckn"           \
    -w "/root/git/beblsoft/projects/smeckn"                  \
    -v ~/git/beblsoft:/root/git/beblsoft                     \
    -v /tmp:/tmp                                             \
    --privileged                                             \
    registry.gitlab.com/beblsoft/beblsoft/tools/ubuntudind
    # --network="host"                                         \
    # docker inspect $(docker ps -aq)
    # -v /var/run/docker.sock:/var/run/docker.sock             \

  # In container
  # service docker start
  # docker login registry.gitlab.com
}

function docker_killContainers() {
  local containers=$(docker ps | grep -v CONTAINER | awk '{ print $1}')
  docker kill ${containers}
}

function docker_rmContainers() {
  local containers=$(docker ps -a | grep -v CONTAINER | awk '{ print $1}')
  docker rm ${containers}
}