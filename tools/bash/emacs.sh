#!/bin/bash
#
# File:
#  emacs.sh
#
# Description
#  Emacs specific bash functionality


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                               MISC                                          #
###############################################################################
export EDITOR=emacs
alias e="emacs -nw"


###############################################################################
#                             EMACS                                           #
###############################################################################
# --------------------------- INSTALL --------------------------------------- #
function emacs_install() {
  sudo apt-get install -y emacs
}

function emacs_uninstall() {
  sudo apt-get install -y emacs
}

function emacs_init() {
  touch ~/.emacs;
  echo "(load-file \"$HOME/git/beblsoft/tools/emacs/jbensson.el\")" > ~/.emacs;
}

# --------------------------- RUNNING --------------------------------------- #
#emacs file loading
function el() {
    (( "$#" == "0" )) && el -h && return;
    local FILES=()
    local SUDO=0
    # getopts notes:
    #   1. OPTIND made local so getopts works inside function
    #   2. optstring: arguments followed by ':' have argument
    local OPTIND
    while getopts ":ias" opt; do
        case "$opt" in
            i)    FILES+=("${EMACS_INIT}")
                  FILES+=("${SCREEN_INIT}")
                  FILES+=("${CRON_JOB}")
                  FILES+=("${BASH_PROFILE}");;
            a)    el -i; return;;
            h|\?) printf "emacs load usage:           \n"
                  printf "el <option(s)>              \n"
                  printf "   -i : init files          \n"
                  printf "   -s : Oracle status files \n"
                  printf "   -a : all files           \n"
                  return;;
        esac
    done
    emacs -nw ${FILES[*]}
}
