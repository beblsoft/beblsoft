#!/bin/bash
#
# File:
#  display.sh
#
# Description
#  Display and screen related scripts and documentation.



###############################################################################
#                                  XRANDR                                     #
###############################################################################
#Installation
# sudo apt-get install xrandr
#
#Basic Usage
# xrandr --verbose       # show monitors detected
# xrandr --auto          # create mirrors across all monitors
#
#My Personal Setup
#From Left to Right:
#Laptop Screen (eDP1) -- Large Dell (DP2) Screen -- Small Dell (VGA1) Screen
function screen_init()
{
  xrandr --output DP2  --mode 1920x1080 --right-of eDP1 --primary
  xrandr --output VGA1 --mode 1280x1024 --right-of DP2
}


###############################################################################
#                                SCREEN SHOT                                  #
###############################################################################
alias sShot=" gnome-screenshot --file=${HOME}/Downloads/sh$(date +'%y%m%d%H%M%S').png" #screen shot
#To view picture: install imagemagick, dispaly <path-to-pic>


