#!/bin/bash
#
# File:
#  sublime.sh
#
# Description
#  Sublime Scripts


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh



###############################################################################
#                              EXPORTS                                        #
###############################################################################
export SUBLIME3_BIN="/opt/sublime_text/sublime_text"


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function sublime3_install()
{
  # Download from: https://www.sublimetext.com/3
  local compressedFile="sublime_text_3_build_3207_x64.tar.bz2"
  local url="https://download.sublimetext.com/${compressedFile}"

  pushd ~/Downloads > /dev/null;
  wget ${url};
  tar -vxjf ${compressedFile}; # Options: Verbose, eXtract, pass through bzip2 (J), specify File
  sudo mv sublime_text_3 /opt/sublime_text
  sudo ln -sf /opt/sublime_text/sublime_text /usr/bin/sublime
  sudo cp /opt/sublime_text/sublime_text.desktop /usr/share/applications/sublime.desktop
  sudo rm -rf ${compressedFile}
  popd > /dev/null;
}

sublime3_merge_install()
{
  pushd ~/Downloads > /dev/null;
  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
  sudo apt-get install -y apt-transport-https
  echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
  sudo apt-get update -y
  sudo apt-get install -y sublime-merge
  popd > /dev/null;
}

function sublime3_uninstall()
{
  sudo rm -rf ~/.config/sublime-text-3
  sudo rm -rf /opt/sublime_text
  sudo rm -rf /usr/bin/sublime
  sudo rm -rf /usr/share/applications/sublime_text.desktop
}

# ---------------------------- CONFIG --------------------------------------- #
function sublime3_config()
{
  # Create or use existing version controlled sublime user settings directory
  # Ex. ~/git/beblsoft/tools/sublime/jbenssonUser
  local user=$(whoami)
  local userDir="${TOOLS}/sublime/${user}User"
  if [ ! -d "$userDir" ]; then mkdir -p ${userDir}; fi

  pushd ~/.config/sublime-text-3/Packages > /dev/null;
  mv User UserOld
  ln -s ${userDir} User
  popd > /dev/null;
}

function sublime3_rmConfig()
{
  rm -rf ~/.config/sublime-text-3
}

function sublime3_rmSessions()
{
  rm ~/.config/sublime-text-3/Local/*.sublime_session
}

function sublime3_configWorkspaces()
{
  echo 'Add following to ~/.config/sublime-text-3/Local/Session.sublime_session:'
  echo ''
  echo '"workspaces":'
  echo '{'
  echo '  "recent_workspaces":'
  echo '  ['
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/beblsoft.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/beblsoft2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/playground.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/playground2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/beblsoft.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/base.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/base2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/tools.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/tmp.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/tmp2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/aws.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/aws2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smeckn2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknAPI2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknAPI.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknAPITest.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknCLI.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknConfig.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBAccount2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBAccount.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBGlobal2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBGlobal.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBManager.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBM.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBPopulater.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDBShard.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknDB.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknManager.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknScripts.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerApp.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerCLI.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerConfig.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerJWT.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerLog2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerLog.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerManager.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServer.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknServerTest.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smeckn.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknWebAPI2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknWebAPI.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknWebClient2.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/smecknWebClient3.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/stripe.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/sqlalchemy.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/quote.sublime-workspace",'
  echo '    "/home/jbensson/git/beblsoft/sublime/jbenssonProjects/error.sublime-workspace",'
  echo '  ]'
  echo '}'
}

# ---------------------------- START ---------------------------------------- #
function sublime3_start ()
{
  ${SUBLIME3_BIN} -n -p ~/sublime/beblsoft.sublime-project
  ${SUBLIME3_BIN} -a -p ~/sublime/playground.sublime-project
}

