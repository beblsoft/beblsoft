#!/bin/bash
#
# File:
#  raspberryPi.sh
#
# Description
#  Raspberry Pi functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# Login:
#   Username: pi
#   Password: raspberry
#
# MicroSD
#  MicroSD card in Canakit appears as 1GB, see http://goo.gl/C9MH94 for
#  instructions on how to partition to full 8GB
#

###############################################################################
#                              WIFI                                           #
###############################################################################
function rp_setupWIFI() {
	#Scan for all wireless networks
	#ESSID:"NETGEAR"
	#IE:WPA Version 1 Group Cipher:TKIP Authentication Suites (1):PSF
	sudo iwlist wlan0 scan

	#Open wpa-supplicant in nano
	sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
	#Add line at bottom:
	#  network={
	#    ssid="NETGEAR"
	#    psk="25pooch1"
	#  }
}

function rp_restartWIFI() {
	sudo ifdown wlan0
	sudo ifup wlan0
}

function rp_verifyWIFI() {
	#Verify Connection, check for inet addr
	ifconfig wlan0
}


###############################################################################
#                              SSH                                            #
###############################################################################
function rp_startSSH() {
	sudo service ssh start
}

function rp_configSSH() {
	#Ensure ssh always starts on boot
	sudo insserv ssh

	#Find IP address
	hostname -I

	#From computer
	ssh pi@192.168.1.6
}
