#!/bin/bash
#
# File:
#  realvnc.sh
#
# Description
#  RealVNC functions and documenation


###############################################################################
#                         IMPORTS                                             #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                         VIEWER FUNCTIONS                                    #
###############################################################################
function realVNCViewer_install()
{
	# Reference: https://www.realvnc.com/en/connect/download/viewer/
	pushd /tmp > /dev/null
	wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.19.325-Linux-x64-ANY.tar.gz
	tar -xvzf VNC-Viewer-6.19.325-Linux-x64-ANY.tar.gz # Unzip
	cd VNC-Viewer-6.19.325-Linux-x64
	sudo ./vncinstall
	popd
}

function realVNCViewer_open()
{
	vncviewer
}


###############################################################################
#                         SERVER FUNCTIONS                                    #
###############################################################################
function realVNCServer_install()
{
	# Reference: https://www.realvnc.com/en/connect/download/vnc/
	pushd /tmp > /dev/null
	wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.4.1-Linux-x64-ANY.tar.gz
	tar -xvzf VNC-Server-6.4.1-Linux-x64-ANY.tar.gz # Unzip
	cd VNC-Server-6.4.1-Linux-x64
	sudo ./vncinstall
	popd
	realVNCServer_start
}

function realVNCServer_start()
{
  sudo systemctl start vncserver-x11-serviced.service
}

function realVNCServer_stop()
{
  sudo systemctl stop vncserver-x11-serviced.service
}

function realVNCServer_status()
{
  sudo systemctl status vncserver-x11-serviced.service
}


###############################################################################
#                         VIEWER AND SERVER FUNCTIONS                         #
###############################################################################
function realVNC_uninstall()
{
	# See: https://www.realvnc.com/en/connect/docs/uninstall.html#uninstall
  sudo rm -rf /etc/vnc/
  sudo rm -rf /root/.vnc/
  sudo rm -rf ~/.vnc/	                            # For each user account running VNC Connect
  sudo rm -rf /etc/pam.d/vncserver*
  sudo rm -rf /etc/init.d/vncserver*            	# Linux distributions using initd
  sudo rm -rf /etc/rc*.d/*vncserver*            	# Linux distributions using initd
  sudo rm -rf /usr/lib/systemd/system/vncserver*	# Linux distributions using systemd
  sudo rm -rf /var/log/vncserver*
  sudo rm -rf /tmp/.vnc*
}