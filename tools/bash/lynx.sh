#!/bin/bash
#
# File:
#  lynx.sh
#
# Description
#  Lynx functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              INFORMATION                                    #
###############################################################################
# Configuration File: /etc/lynx-cur/lynx.cfg


###############################################################################
#                              INSTALL                                        #
###############################################################################
function lynx_install() {
  sudo apt-get install -y lynx
}

function lynx_uninstall() {
  sudo apt-get remove -y lynx
}


###############################################################################
#                              MISCELLANEOUS                                  #
###############################################################################
function lynx_getWeather() {
  URL="https://weather.yahoo.com/united-states/new-hampshire/nashua-2457142/"
  LYNX=`which lynx`
  TMPFILE=`mktemp tempXXXXX`
  $LYNX -dump $URL > $TMPFILE
  cat $TMPFILE
  rm -f $TMPFILE
}
