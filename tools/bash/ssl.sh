#!/bin/bash
#
# File:
#  ssl.sh
#
# Description
#  SSL functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                            HTTPS CERTS                                      #
###############################################################################
# Terminology
# ------------------
# Certificate Signing Request(.csr) : A CSR or Certificate Signing request is a block of encoded text
#                                     that is given to a Certificate Authority when applying for an SSL
#                                     Certificate. It is usually generated on the server where the
#                                     certificate will be installed and contains information that will
#                                     be included in the certificate such as the organization name,
#                                     common name (domain name), locality, and country. It also contains
#                                     the public key that will be included in the certificate.
#                                     A private key is usually created at the same time that you create
#                                     the CSR, making a key pair. A CSR is generally encoded using ASN.1
#                                     according to the PKCS 10 specification.
# Privacy Enhanced Email(.pem)      : PEM certificates are frequently
#                                     used for web servers as they can easily be translated into
#                                     readable data using a simple text editor.  Generally when a PEM
#                                     encoded file is opened in a text editor, it contains very
#                                     distinct headers and footers. Ex.
#                                     -----BEGIN ENCRYPTED PRIVATE KEY-----
#                                     MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDg
#                                     MBQGCCqGSIb3DQMHBAgD1kGN4ZslJgSCBMi1xk9jhlPxPc
#                                     9g73NQbtqZwI+9X5OhpSg/2ALxlCCjbqvzgSu8gfFZ4yo+
#                                     A .... MANY LINES LIKE THAT ....
#                                     X0R+meOaudPTBxoSgCCM51poFgaqt4l6VlTN4FRpj+c/Wc
#                                     blK948UAda/bWVmZjXfY4Tztah0CuqlAldOQBzu8TwE7WD
#                                     H0ga/iLNvWYexG7FHLRiq5hTj0g9mUPEbeTXuPtOkTEb/0
#                                     GEs=
#                                     -----END ENCRYPTED PRIVATE KEY-----
#                                     -----BEGIN CERTIFICATE-----
#                                     MIIDXTCCAkWgAwIBAgIJAJC1HiIAZAiIMA0GCSqGSIb3Df
#                                     BAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVx
#                                     aWRnaXRzIFB0eSBMdGQwHhcNMTExMjMxMDg1OTQ0WhcNMT
#                                     A .... MANY LINES LIKE THAT ....
#                                     JjyzfN746vaInA1KxYEeI1Rx5KXY8zIdj6a7hhphpj2E04
#                                     C3Fayua4DRHyZOLmlvQ6tIChY0ClXXuefbmVSDeUHwc8Yu
#                                     B7xxt8BVc69rLeHV15A0qyx77CLSj3tCx2IUXVqRs5mlSb
#                                     vA==
#                                     -----END CERTIFICATE-----
# Key (.key)                        : PEM formatted file containing just the private
#                                     Key of a specific certificate
# Public-Key Cryptography Standards
# (.pkcs12 .pfx .p12)               : Passworded container format that contains both
#                                     public and private certificate pairs. openssl can
#                                     turn this into a .pem file with both public and private
#                                     keys.
#                                     openssl pkcs12 -in file-to-convert.p12 -out converted-file.pem -nodes
# Certificate Revocation List (.crl): a list of digital certificates that have been revoked by the
#                                     issuing certificate authority (CA) before their scheduled
#                                     expiration date and should no longer be trusted
# Certificate (.crt)                : Extension used for certificates. Encoded in DER or PEM

function ssl_generateSelfSignedCert()
{
	[ -z ${1} ] && echo "Specify FileBase" && return
	local fileBase=${1}

	#fileBase.key = private key
	#fileBase.csr = certificate signing request
	#fileBase.pem = self-signed certificate
  openssl genrsa -out ${fileBase}.key 2048
  openssl req -new -key ${fileBase}.key -out ${fileBase}.csr
  openssl x509 -req -in ${fileBase}.csr -signkey ${fileBase}.key -out ${fileBase}.pem
}
