#!/bin/bash
#
# File:
#  terminals.sh
#
# Description
#  Terminal related scripts and documation


###############################################################################
#                            TERMINALS                                        #
###############################################################################
alias mouse_getlocation="xdotool getmouselocation"
# gnome-terminal
#  General options
#   --help                 :Help
#   --window               :Open a new window containing a tab with the default profile
#   --tab                  :Open a new tab in the last-opend window with the default profile
#  Window options
#   --show-menubar
#   --hide-menubar
#   --maximize
#   --full-screen
#   --geometry=GEOMETRY    :Set the window size; ex: 80x24 (COLSxROWS+X+Y)
#   --role=ROLE            :Set the window role
#   --active               :Set the last specified tab as the active one in its window
#   --zoom=ZOOM            :Set the terminal's zoom factor (1.0 = normal size)
#  Execution options
#   -e, --command          :Execute the argument inside terminal
#   --profile=PROFILE      :Use given profile
#   -t,--title=TITLE       :Set terminal title
#   --working-directory=WD :Set working directory
#
alias gt_simple="    gt_create 1left   2 'echo \"Hello World!\"';"
alias gt_oracleTest="gt_create 2left   3 'ssh nshgb2401        '; \
                     gt_create 2middle 3 'ssh nshgb2402        '; \
                     gt_create 2right  3 'ssh nsh00aai         '; "

function gt_create()
{
  if [[ -z  $1 ]] ; then
    printf "gt_create <location> [tabs] [cmd]\n"
    printf "Create terminals and have them execute commands"
    return
  fi

  local LOCATION=$1
  local GEOMETRY=""
  local ZOOM=""
  local TABS=1
  local TABSTR=""
  local CMDSTR=""
  [ $# -ge 2 ] && TABS=$2
  [ $# -ge 3 ] && CMDSTR=$3

  case $LOCATION in
    1left)   GEOMETRY="85x73+0+0"      ZOOM=0.9;;
    1middle) GEOMETRY="85x73+530+0"    ZOOM=0.9;;
    1right)  GEOMETRY="85x73+1070+0"   ZOOM=0.9;;
    2left)   GEOMETRY="101x85+1600+0"  ZOOM=0.9;;
    2middle) GEOMETRY="103x85+2230+0"  ZOOM=0.9;;
    2right)  GEOMETRY="103x85+2880+0"  ZOOM=0.9;;
    3left)   GEOMETRY="103x82+3525+0"  ZOOM=0.9;;
    3right)  GEOMETRY="103x85+4171+0"  ZOOM=0.9;;
    \?)      gt_create; return;
  esac

  for i in $(seq 0 $(echo "scale=0;$TABS-1" | bc)); do
    TABSTR="$TABSTR --zoom=$ZOOM --tab --command bash "
  done

  #Execute command after bash loads
  export BASH_POST_RC=$CMDSTR;
  gnome-terminal --geometry=$GEOMETRY $TABSTR
  unset BASH_POST_RC;
  return 0;
}
