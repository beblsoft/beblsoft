#!/bin/bash
#
# File:
#  lastpass.sh
#
# Description
#  Lastpass related scripts and documentation.

###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# lpass OVERVIEW
# --------------------------
# - Command line interface to LastPass.com
# - Designed to run on GNU/Linux, Cygwin and Mac OS X
#
# LINKS
# --------------------------
# - Github page : https://github.com/lastpass/lastpass-cli
#

###############################################################################
#                          FUNCTIONS, ALIAS                                   #
###############################################################################
# ----------------- INSTALL ----------------------------- #
function lastpass_install()
{
	pushd /tmp;

	# lpass binary
    git clone https://github.com/lastpass/lastpass-cli.git
    cd lastpass-cli
	sudo apt-get install -y openssl libcurl4-openssl-dev libxml2 libssl-dev libxml2-dev pinentry-curses xclip cmake build-essential
    cmake . && make
    sudo make install
    sudo ln -s /usr/local/bin/lpass /usr/bin/lpass

    # Documentation
    sudo apt-get install -y asciidoc xsltproc
    sudo make install-doc #Now: man lpass works
	popd;
}