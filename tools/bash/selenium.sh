#!/bin/bash
#
# File:
#  selenium.sh
#
# Description
#  Selenium functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              DOCUMENTATION                                  #
###############################################################################
# Overview
# - Selenium automates browsers. Primarily, it is for automating web applications
#   for testing purposes, but is certainly not limited to just that. Boring web-based
#   administration tasks can (and should!) be automated as well.
# - Selenium has the support of some of the largest browser vendors who have taken
#   (or are taking) steps to make Selenium a native part of their browser. It is
#   also the core technology in countless other browser automation tools, APIs
#   and frameworks.
#
# Basic Python Usage
#   #!/usr/bin/env python3
#   from selenium import webdriver
#   browser = webdriver.Firefox()
#   browser.get('http://www.ubuntu.com/')


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function geckoDriver_install() {
	# Needed by selenium
	local version="v0.21.0"
	local geckoTar="geckodriver-${version}-linux64.tar.gz"
	local geckoHttp=" https://github.com/mozilla/geckodriver/releases/download/${version}/${geckoTar}"
	local tmpDir="/tmp"

	pushd ${tmpDir}
	wget ${geckoHttp}
	tar -xzf ${geckoTar}
	sudo mv geckodriver /usr/bin
	rm ${geckoTar}
	popd
}

function geckoDriver_uninstall() {
	sudo rm /usr/bin/geckodriver
}


function selenium_install() {
	# Requires geckodriver installed

	# Uses python virtual environment
	# - virtualenv -p /usr/bin/python3.6 venv
	# - source venv/bin/activate

	pip3 install selenium

	# Add to requirements.txt
	# - pip3 freeze > requirements.txt
	# - deactivate
}

function selenium_uninstall() {
	pip3 uninstall selenium
}
