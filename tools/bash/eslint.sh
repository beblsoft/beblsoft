#!/bin/bash
#
# File:
#  eslint.sh
#
# Description
#  ESLint scripts


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                          INSTALL, UNINSTALL                                 #
###############################################################################
function eslint_install() {
  sudo npm install -g                  \
     eslint@^5.6.0                     \
     babel-eslint@^9.0.0               \
     eslint-config-standard@^12.0.0    \
     eslint-friendly-formatter@^4.0.1  \
     eslint-loader@^2.1.1              \
     eslint-plugin-html@^4.0.5         \
     eslint-plugin-promise@^4.0.0      \
     eslint-plugin-standard@^4.0.0     \
     eslint-plugin-mocha@^5.2.0        \
     eslint-plugin-vue@^4.7.1          \
     eslint-plugin-cypress@^2.0.1
}

function eslint_uninstall() {
  sudo npm uninstall -g                \
     eslint                            \
     babel-eslint                      \
     eslint-config-standard            \
     eslint-friendly-formatter         \
     eslint-loader                     \
     eslint-plugin-html                \
     eslint-plugin-promise             \
     eslint-plugin-standard            \
     eslint-plugin-mocha               \
     eslint-plugin-vue                 \
     eslint-plugin-cypress
}


###############################################################################
#                              CHECKER                                        #
###############################################################################
function eslint_checker()
{
  local TARGET=$1
  local eslintrc="${TOOLS}/eslint/eslintrc.js"
  local files=()

  case "$TARGET" in
    playNode)         echo "Playground Node...";
                      files=($(find ${PLAY}/node -name "*.js" | grep -v node_modules))
                      eslint --config=${eslintrc} ${files[*]};;

    playMocha)        echo "Playground Mocha...";
                      files=($(find ${PLAY}/mocha -name "*.js" | grep -v node_modules))
                      eslint --config=${eslintrc} ${files[*]};;

    smeckn)           echo "Smeckn Web API...";
                      files=($(find ${SMECKN}/webAPI/src -name "*.js"))
                      eslint --config=${eslintrc} ${files[*]};

                      echo "Smeckn Web Client...";
                      files=($(find ${SMECKN}/webClient/node -name "*.js" ))
                      files+=($(find ${SMECKN}/webClient/src -name "*.js" ))
                      files+=($(find ${SMECKN}/webClient/src -name "*.vue" ))
                      eslint --config=${eslintrc} ${files[*]};;

    all)              eslint_checker playNode
                      eslint_checker playMocha
                      eslint_checker smeckn;;

    help|*)           printf "Usage: eslint_checker <target> \n"
                      return;;
  esac
}


