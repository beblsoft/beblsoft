#!/bin/bash
#
# File:
#  vagrant.sh
#
# Description
#  Vagrant Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
# ---------------------------- INSTALL -------------------------------------- #
function vagrant_install()
{
	# Vagrant itself
	pushd ~/Downloads > /dev/null;
	wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.deb
	sudo dpkg -i vagrant_2.2.5_x86_64.deb
	sudo apt-get install -y -f
	popd  > /dev/null;

	# Vagrant Plugins
	vagrant plugin install vagrant-share
	vagrant plugin install vagrant-aws
}

function vagrant_uninstall()
{
  sudo apt-get remove -y vagrant
}

