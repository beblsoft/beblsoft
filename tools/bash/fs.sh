#!/bin/bash
#
# File:
#  fs.sh
#
# Description
#  Filesystem related scripts and documentation.


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
#Create executable file
function touchx ()
{
  [ $# -ne 1 ] &&  echo "Usage: touchx <filename>." && return;
  name=$1;
  touch $name;
  chmod 744 $name;
}

#Move to higher directory
function up()
{
  dir=""
  if [ -z "$1" ]; then
    dir=..
  elif [[ $1 =~ ^[0-9]+$ ]]; then
    x=0
    while [ $x -lt ${1:-1} ]; do
      dir=${dir}../
      x=$(($x+1))
    done
  else
    dir=${PWD%/$1/*}/$1
  fi
  cd "$dir";
}

# Creates a self extracting tar ball.  The tar file consists
# of a directory and all of its files.  The one argument is
# the name of the directory.
function grabit()
{
  dir=$1 # input directory or file
  file=/tmp/extract.`echo $dir | sed 's/://g'` # output file
  [ ! $dir ] && printf "${r}Error: must specify a directory$w\n" && return 1;
  [ ! -e $dir ] && printf "${r}Error: $dir doesn't exist$w\n" && return 1;

  tar hpczvf $file.tar.gz $dir
  status=$?
  [ $status -ne 0 ] && printf "${r}Error: tar error status=$status$w\n" && return 1;
  cat << EOF > $file
#!/bin/bash
dir=`echo "\$0" | sed s.extract..`
[ -e $dir ] && printf "Error: $dir already exists\n" && exit 1;
sed -e '1,/^exit$/d' "\$0" | tar -zxvf -
exit
EOF
  cat $file.tar.gz >> $file
  rm -f $file.tar.gz
  chmod 777 $file
  printf "\nRun $y$file$n to extract the directory\n\n"
}

#Uncompress file
function extract ()
{
  [ ! $1 ] && { printf "Error: must specify a filename to extract\n"; return; }
  case $1 in
    *.tar.bz2)   tar xjf $1    ;;
    *.tar.gz)    tar xzf $1    ;;
    *.bz2)       bunzip2 $1    ;;
    *.rar)       rar x   $1    ;;
    *.gz)        gunzip  $1    ;;
    *.tar)       tar xf  $1    ;;
    *.tbz2)      tar xjf $1    ;;
    *.tgz)       tar xzf $1    ;;
    *.zip)       unzip   $1    ;;
    *.Z)         uncompress $1 ;;
    *.7z)        7z x    $1    ;;
    *)           echo "'$1' cannot be extracted via extract()" ;;
  esac
}
