#!/bin/bash
#
# File:
#  redis.sh
#
# Description
#  Redis functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                          INSTALL, UNINSTALL                                 #
###############################################################################
function redis_install()
{
	sudo apt-get update -y
  sudo apt-get install -y redis-server
}

function mysql_uninstall()
{
  sudo apt-get uninstall -y redis-server
}
