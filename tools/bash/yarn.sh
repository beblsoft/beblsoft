#!/bin/bash
#
# File:
#  yarn.sh
#
# Description
#  Yarn Scripts and Documentation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              FUNCTIONS                                      #
###############################################################################
function yarn_install()
{
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  sudo apt-get update -y && sudo apt-get install -y yarn
}

function yarn_uninstall()
{
  sudo apt-get remove -y yarn
}

