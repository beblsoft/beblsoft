#!/bin/bash
#
# File:
#  sentry.sh
#
# Description
#  Sentry functions and documenation


###############################################################################
#                              IMPORTS                                        #
###############################################################################
SCRIPT_DIR=$(dirname $BASH_SOURCE[0])
. ${SCRIPT_DIR}/lib.sh


###############################################################################
#                              INSTALL                                        #
###############################################################################
function sentry_install() {
  sudo curl -sL https://sentry.io/get-cli/ | bash
}

function sentry_update() {
  sudo sentry-cli update
}

function sentry_uninstall() {
  sudo sentry-cli uninstall
}

function sentry_info() {
  sudo sentry-cli info
}


###############################################################################
#                              HELLO WORLD                                    #
###############################################################################
function sentry_sendHelloWorldEvent() {
  export SENTRY_DSN="https://3cbcc98690934b1681e721dea4caf2cc:97da6aef85ea4accb1092ef172c134f9@sentry.io/1426526"
  sentry-cli send-event -m "Hello from Sentry"
}

function sentry_createHelloWorldRelease() {
  pushd $SMECKN;
  local VERSION=`sentry-cli releases propose-version`
  sentry-cli releases --org beblsoft new --finalize --project helloworld "$VERSION"
  sentry-cli releases --org beblsoft set-commits --auto "$VERSION"
  popd;
}