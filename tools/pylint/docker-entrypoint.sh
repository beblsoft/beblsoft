#!/bin/bash
#
# File:
#  docker-entrypoint.sh
#
# Description
#  Pylint Docker Entrypoint


###############################################################################
#                              IMPORTS                                        #
###############################################################################
. ${TOOLS}/bash/lib.sh


###############################################################################
#                              LINT                                           #
###############################################################################
PYLINT_RCFILE="${TOOLS}/pylint/pylintrc"

function pylint_base() {
  # Prune single directory
  #  find . -path ./misc -prune -o -name '*.txt' -print
  # Prune multiple directories
  #  find . -type d \( -path dir1 -o -path dir2 -o -path dir3 \) -prune -o -print

  echo "Base...";
  files=($(find ${BASE} -name "*.py" | grep -v venv))
  pylint --rcfile=$PYLINT_RCFILE ${files[*]};
}

function pylint_cicd() {
  echo "CICD...";
  files=($(find ${CICD} -name "*.py" | grep -v venv ))
  pylint --rcfile=$PYLINT_RCFILE ${files[*]};
}

function pylint_mojourney() {
  echo "Mojourney...";
  files=($(find ${MOJOURNEY} -name "*.py" | grep -v venv))
  pylint --rcfile=$PYLINT_RCFILE ${files[*]};
}

function pylint_quote() {
  echo "Quote...";
  files=($(find ${QUOTE} -name "*.py" | grep -v venv))
  pylint --rcfile=$PYLINT_RCFILE ${files[*]};
}

function pylint_smeckn() {
  echo "Smeckn...";
  files=($(find ${SMECKN} -name "*.py" | grep -v venv | grep -v mysqlclient | grep -v versions ))
  pylint --rcfile=$PYLINT_RCFILE ${files[*]};
}


###############################################################################
#                              MAIN                                           #
###############################################################################
[ -z $1 ] && echo "docker-entrypoint <command> ..." && exit 1;
TARGET=$1
shift # Removes $1

case $TARGET in
  pylint_base)       pylint_base $@;;
  pylint_cicd)       pylint_cicd $@;;
  pylint_mojourney)  pylint_mojourney $@;;
  pylint_quote)      pylint_quote $@;;
  pylint_smeckn)     pylint_smeckn $@;;
  *)                 "Invalid target=$TARGET"; exit 1;;
esac
exit $?