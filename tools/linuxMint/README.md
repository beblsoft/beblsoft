# Linux Mint Documentation

## Installation via USB

Steps:

1. Download Mint [here](https://www.linuxmint.com/download.php)

2. Start mint program "USB Image Writer"

   select downloaded image

   select USB stick

3. Reboot: Reboot machine with USB stick inserted

   Hit F10

   Follow instructions to install Linux Mint

## Configuration

### Passwordless sudo

```bash
sudo chmod 600 /etc/sudoers

# Edit /etc/sudoers
# At end of file, after "#includedir /etc/sudoers.d" Add:
jbensson ALL=(ALL) NOPASSWD: ALL

# Note: Don't make /etc/sudoers world writable.
# This disables the sudo command. To fix run: pkexec chmod 0600 /etc/sudoers
```

## Essential Development Tools

### Core Libraries

```bash
sudo apt-get update && sudo apt-get install -f
sudo apt-get install build-essential
sudo apt-get install linux-common-tools
```

### Google Chrome Browser

- Install: Open firefox and search for Chrome

- Accept self-signed certificates:

  Go to the following URL: `chrome://flags/#allow-insecure-localhost`

  Allow invalid certificates for resources loaded from localhost. Click Enable.

- Allow experimental JavaScript:

  Go to the following URL: `chrome://flags/#enable-javascript-harmony`

  Experimental JavaScript. Click Enable.

- [View your chrome version](https://www.whatismybrowser.com/)

- Relevant Chrome Extensions:
    * `Markdown Viewer`: Allows you to view local markdown files
    * `Screen Recorder`: Creates video screen casts

### Git

- Install : `sudo apt-get install git`
- Configure
  ```bash
  git config --global user.name "Darryl Black"
  git config --global user.email "darrylpblack@gmail.com"
  git config --global core.editor gedit
  git_config  # Execute after bash environment is setup (see below)
  ```

### GitLab

- Key setup
  ```bash
  ssh-keygen; # take defaults;
  cat ~/.ssh/id_rsa.pub;
  copy to provider
  ```

- Setup git directory and start cloning repos
  ```bash
  mkdir ~/git;
  cd ~/git
  git clone git@gitlab.com:beblsoft/beblsoft.git
  ```

### Bash Profile

The Beblsoft bash_profile (`~/git/beblsoft/tools/bash/bash_profile`) configures the terminal environment
for running various Beblsoft command. It sources several third party scripts located in
`~/git/beblsoft/tools/bash/`

```bash
# Setup single terminal
source ~/git/beblsoft/tools/bash/bash_profile

# Run script that will update update .bash_profile to always source the Beblsoft bash profile
# when a new terminal is created
init_bash

# After ever making a change to the bash_profile the "sb" command will source the new changes
# "sb" stands for "Source Bash"
sb
```

### Sublime Text 3

```bash
# Install sublime (see sublime.sh)
sublime3_install;

# Configure Sublime
sublime3_config;

# Open Sublime
# Install Package Control
#   Ctrl + Shift + P
#   "Install Package Control" + Enter
# Exit Sublime and Reenter: On reenter, packages will be installed

# Register Sublime
```

See `${TOOLS}/sublime/README.txt` and `${TOOLS}/bash/sublime.sh` for more information.

### nodejs

```bash
# Install nodejs
nodejs_install
```

See `${TOOLS}/bash/nodejs.sh` for more information.

### npm

```bash
# Install
npm_install;
```

See `${TOOLS}/bash/nodejs.sh` for more information.

### Python

```bash
# Install
python3_linuxMint_install
```

See `${TOOLS}/bash/python.sh` for more information.

### mysql

```bash
# Install
mysqlServer_install
mysqlClient_install
mysql_createUser $(whoami)
```

### RealVNC

```bash
# Install Server
realVNCServer_install

# Install Viewer
realVNCViewer_install
```

See `${TOOLS}/realVNC/README.txt` and `${TOOLS}/bash/realVNC.sh` for more information.

### Docker

```bash
# Install
docker_install;
# Restart Computer for Docker Group to take effect
```

See `${TOOLS}/bash/docker.sh` for more information

### Docker Compose

```bash
# Install
dockerCompose_install;
```

See `${TOOLS}/bash/dockerCompose.sh` for more information.

### Ruby

```bash
# Install
ruby_install
```

See `${TOOLS}/bash/ruby.sh` for more information.

### Markdown Lint

```bash
# Install
markdownlint_install;
```

See `${TOOLS}/bash/markdownlint.sh` for more information.

## Handy Development Tools

### Screen

```bash
# Install
screen_install

# Initialize
screen_init
```

See `${TOOLS}/bash/screen.sh` for more information.

### Emacs

```bash
# Install
emacs_install

# Initialize ~/.emacs
# Has .emacs source $HOME/git/beblsoft/tools/emacs/jbensson.el on load
emacs_init
```

### htop

```bash
# Install
sudo apt-get install -y htop
```

### tree

```bash
# Install
sudo apt-get install -y tree
```

### kazam

```bash
# Install
kazam_install
```

See `${TOOLS}/bash/kazam.sh` for more infomation.

### Insync

Download via at their [website](https://www.insynchq.com/).

See `${TOOLS}/bash/emacs.sh` for more infomation.

### SSH Server

```bash
# Install
sudo apt-get install openssh-server
```

### NFS

```bash
# Install
sudo apt-get install nfs-common
```

### jq: json processor

```bash
# Install
sudo apt-get install jq
```

### Visual Studio Code

```bash
# Install
# extract VSCode-linux-x64-stable.zip; mv VSCode-linux-x64 ~
ln -s ~/VSCode-linux-x64/code /usr/bin/local/code
```

### Remmina

```bash
# Install
sudo apt-get install remmina remmina-common remmina-plugin-rdp  remmina-plugin-vnc remmina-plugin-gnome \
     remmina-plugin-nx remmina-plugin-telepathy remmina-plugin-xdmcp
# reboot machine
```

### Zoom

Install via [website](https://zoom.us/)

### geckodriver, selenium

```bash
# Install
geckoDriver_install
```

See `${TOOLS}/bash/selenium.sh` for more information.

## Peripheral Support

### RTL8812AU Realtek USB Wireless Adapter

Features:
- USB 3.0 Network Adapter
- 1200 Mbps 5.8 GHz 2.4GHz
- Dual Band AC1200 WiFi Dongle
- 802.11 a b g n ac

Installation instructions from [github](https://github.com/gordboy/rtl8812au):

```bash
git clone https://github.com/gordboy/rtl8812au.git
cd rtl8812au
make
sudo make install
sudo cp -r . /usr/src/rtl8812au-5.2.20

# Clean out any dkms rtl8812 modules prior to intalling this one
sudo dkms add -m rtl8812au -v 5.2.20
sudo dkms build -m rtl8812au -v 5.2.20
sudo dkms install -m rtl8812au -v 5.2.20

# To remove
# sudo dkms remove -m rtl8812au -v 5.2.20 --all
```
### WebCam

Model: Papalook Web Camera HD 720. Model PA150

```bash
# Install Cheese program
sudo apt-get install cheese
```

### Pulse Audio Volume Control (pavucontrol)

```bash
# Install
sudo apt-get install pavucontrol

# Configure
# Open pavucontrol
# Input Devices
# Click Checkbox (set as fallback) for desired microphone
# Raise mic DB level to 100%
# If any issue, unplug camera and replug
```

### Graphics Card: NVIDIA GEFORCE GTX 1060

```bash
# Install
# Without the graphics card loaded do the following
sudo apt-get purge nvidia
sudo add-apt-repository ppa:graphics-drivers
sudo apt-get update
sudo apt-get install nvidia-367
# Turn off computer, install graphics card, turn on computer
```

For more information see [website](https://forums.linuxmint.com/viewtopic.php?t=226145)

### Blueman, Bose Headphones

```bash
# Install
sudo apt-get install blueman
# Edit /etc/bluetooth/audio.conf
# [General]
# Disable=Socket
# Disable=Headset
# Enable=Media,Source,Sink,Gateway
# AutoConnect=true
# load-module module-switch-on-connect

# Edit /etc/bluetooth/audio.conf
# ControllerMode = bredr
# AutoEnable=true
# Reboot machine

# Open BlueTooth Manager, pair with Bose Quiet Comfort 35
# Sound Settings > Output Profile > High Fidelity Playback A2DP Sink
```
See [website](https://erikdubois.be/installing-bose-quietcomfort-35-linux-mint-18/) for more information.

### Asunder (CD ripper)

```bash
# Install
sudo apt-get install asunder
```

## Keyboard Shortcuts

### General

- Open Applications Menu            : `Win`
- Show desktop                      : `Win + D`
- Show file manager                 : `Win + E`
- Open the run dialog               : `Alt + F2`
- Terminal                          : `Ctrl + Alt + T`
- Undo                              : `Ctrl + Z`
- Redo                              : `Ctrl + Y`

### Mange Windows and Workspaces

- Switch through open windows       : `Alt + Tab`
- Move to left/right workspace      : `Ctrl + Alt + Arrow Left/Arrow Right`
- Move curr window to left/right ws : `Ctrl + Alt + Shift  + Arrow Left/Arrow Right`
- Move current wind to diff monitor : `Win + Shift + Arrow Keys`
- Toggle expo view                  : `Ctrl + Alt + Arrow Up`
- Toggle scale view                 : `Ctrl + Alt + Arrow Down`

### Current Application Window

- Close Window                      : `Alt + F4`
- Unmazimize Window                 : `Alt + F5`
- Resize Window                     : `Alt + F7`
- Toggle Maximize/Unmaximize        : `Alt + F10`
- Snap Window to Sides, Corners     : `Win + Arrow Keys`
- Open window menu                  : `Alt + Space`

### Session

- Lock Screen                       : `Ctrl + Alt + L`
- Log out                           : `Ctrl + Alt + Delete`
- Open Shutdown Menu                : `Ctrl + Alt + End`

### Screenshots and Screen Recording

- Print Screen                      : `Print Screen`
- Copy a screen to clipboard        : `Ctrl + Print Screen`
- Toggle desktop recording          : `Ctrl + Alt + Shift + R`

See [website](https://www.shortcutworld.com/en/linux/Linux-Mint-Cinnamon-Edition_17.2.html) for more information.
