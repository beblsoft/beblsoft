# Cron Technology Documentation

cron is a time-based job scheduler software utility in Unix-like computer operating systems.
People who setup and maintain software environments use cron to schedule jobs to run periodically
at fixed times, dates, or intervals.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Cron),
[Translator](https://crontab.guru/),
[Crontab Reference](https://www.adminschoice.com/crontab-quick-reference)

## crontab file

Cron is driven by a __crontab__ (cron table) file, a configuration file that specifies shell commands
to run periodically on a given schedule. crontab files are stored where the lists of jobs
and other instructions to the cron daemon are kept. Users can have their own individual crontab
files and often there is a system-wide crontab file.

Each line of the crontab file represents a job with the following syntax
```text
┌───────────── minute (0 - 59)
│ ┌───────────── hour (0 - 23)
│ │ ┌───────────── day of the month (1 - 31)
│ │ │ ┌───────────── month (1 - 12)
│ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;
│ │ │ │ │                                   7 is also Sunday on some systems)
│ │ │ │ │
│ │ │ │ │
* * * * * command to execute
```

For example, the following clears the Apache error log at one minute past midnight (00:01) every day:
```bash
1 0 * * * printf "" > /var/log/apache/error_log
```

## Usage

Basic usage is as follows:

```bash
# Install crontab file
crontab -u jbensson -i $BEBLSOFT/tools/cron/cron.jbensson

# list the current crontab file
crontab -l

# edit the current crontab file
crontab -e
```