#!/bin/bash
#
# Script to backup directories
#

srcDir="nshgb2413:~jbensson/oracle"
destDir="$HOME/backup/oracle"

usage()
{
 cat << EOF 
usage: $0 options
 OPTIONS:
  -h      Show this message
  -d      daily backup
  -w      weekly backup
  -m      monthly backup
  -y      yearly backup
EOF
}

# ------------------------ OPTION PARSING ------------------------------------ #
if [ $# -ne 1 ] 
then
    usage
    exit
fi

while getopts “dwmy” OPTION
do
    case $OPTION in
        d)
            destDir=$destDir"DAY"
            ;;
        w)
            destDir=$destDir"WEEK"
            ;;
        m)
            destDir=$destDir"MONTH"
            ;;
        y)
            destDir=$destDir"YEAR"
            ;;
        h)
            usage
            exit
            ;;
        ?)
            usage
            exit
            ;;
     esac
done

# ----------------------------- BACKUP ------------------------------------------ #
rsync -rbtqL --delete-excluded $srcDir/* $destDir 




