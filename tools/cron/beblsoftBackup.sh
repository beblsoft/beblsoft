#!/bin/bash
#
# Name:
#  beblsoftBackup.sh
#
# Description:
#  Script backs up relative beblsoft directories to hard drive.
#


# ----------------------------- SRC DIRS ------------------------------------ #
srcDirs=()
srcDirs+=("/home/jbensson/git")
srcDirs+=("/home/jbensson/benssonjames@gmail.com/Work/beblsoft")

# ----------------------------- DEST DIRS ----------------------------------- #
seagateDir="/home/jbensson/seagate"
dateStr=$(date +'%m-%d-%y')
destDirs=()
destDirs+=("${seagateDir}/${dateStr}/git" "${seagateDir}/${dateStr}/beblsoft")


# ----------------------------- BACKUP -------------------------------------- #
for (( c=0; c<${#srcDirs[@]}; c++ ))
do
  srcDir=${srcDirs[${c}]}
  destDir=${destDirs[${c}]}
  echo "Iteration $c: Copying source ${srcDir} to destination ${destDir}"

  cd ${seagateDir}
  mkdir -p ${destDir}; cd ${destDir}
  cp -r ${srcDir}/* .
done





