;; .emacs
;;
;; ----------------------- Helpful Key Bindings ----------------------------- ;;
;;List All Bindings  : C-h b
;;Search Bindings    : C-u M-x apropos RET <binding> RET
;;Descibe Binding    : C-h k <binding>
;;Describe Hooks     : M-x apropos RET hook RET
;;Search for command : C-h a PATTERN RET
;;Function help      : C-h f FUNC RET
;;Kill ring save     : M-w
;;Emacs Manual       : C-h i
;;Column witdth to 80: M-1 M-q
;;Tab # of characters: C-u <number> C-x <TAB>
;;Reload .emacs,.el  : M-x load-file <path to .emacs>
;;
;; ----------------------- EDebug ------------------------------------------- ;;
;; edebug-all-defs     - instrument all defs
;; edebug-all-defuns   - instrument all defuns
;; eval-current-buffer
;; eval-region
;; eval-buffer
;;
;; Commands
;; --------
;; ?   = show all commands
;; S   = Stop: don’t execute any more of the program, but wait for more Edebug commands (edebug-stop).
;; SPC = Step: stop at the next stop point encountered (edebug-step-mode).
;; n   = Next: stop at the next stop point encountered after an expression (edebug-next-mode).
;; t   = Trace: pause (normally one second) at each Edebug stop point (edebug-trace-mode).
;; T   = Rapid trace: update the display at each stop point, but don’t actually pause (edebug-Trace-fast-mode).
;; g   = Go: run until the next breakpoint (edebug-go-mode). See Breakpoints.
;; c   = Continue: pause one second at each breakpoint, and then continue (edebug-continue-mode).
;; C   = Rapid continue: move point to each breakpoint, but don’t pause (edebug-Continue-fast-mode).
;; G   = Go non-stop: ignore breakpoints (edebug-Go-nonstop-mode).


;; --------------------------- EMACS FUNCTIONS! ----------------------------- ;;
(defun beginning-of-defun-forward (&optional n)
  "Move forward to the beginning of a defun."
  (interactive "P")
  (beginning-of-defun (- (prefix-numeric-value n))))

(defun end-of-defun-backward (&optional n)
  "Move backward to the end of a defun."
  (interactive "P")
  (end-of-defun (- (prefix-numeric-value n))))

(defun other-window-backward (&optional n)
  "Select Nth previous window."
  (interactive "P")
  (other-window (- (prefix-numeric-value n))))

(defun scroll-down-in-place (n)
  (interactive "p")
  (previous-line n)
  (unless (eq (window-start) (point-min))
    (scroll-down n)))

(defun scroll-up-in-place (n)
  (interactive "p")
  (next-line n)
  (unless (eq (window-end) (point-max))
    (scroll-up n)))


;; --------------------------- MISC FILES TO LOAD --------------------------- ;;
(setq home-path (getenv "HOME"))
(setq load-path (cons (format "%s/%s" home-path "git/beblsoft/tools/emacs") load-path))
(load (format "%s/%s" home-path "git/beblsoft/tools/emacs/wide_column/wide-column.el"))
(load (format "%s/%s" home-path "git/beblsoft/tools/emacs/cycle-buffer.el"))

(load (format "%s/%s" home-path "git/beblsoft/tools/emacs/hideshow.el"))
(add-hook 'c-mode-hook '(lambda () (hs-minor-mode 1)))
(add-hook 'c++-mode-hook        '(lambda () (hs-minor-mode 1)))
(add-hook 'perl-mode-hook       '(lambda () (hs-minor-mode 1)))
(add-hook 'emacs-lisp-mode-hook '(lambda () (hs-minor-mode 1)))
(add-hook 'java-mode-hook       '(lambda () (hs-minor-mode 1)))


;; --------------------------- TEMPORARY FILES ------------------------------ ;;
(setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))


;; --------------------------- MISCELLANEOUS -------------------------------- ;;
;; enable visual feedback on selections
(setq transient-mark-mode t)

;; parens/brace matching
(setq blink-matching-paren-distance 150000)

;; Setup coloring package
(setq load-path (cons "/home/jbensson/git/beblsoft/tools/emacs/color-theme-6.6.0" load-path))
(require 'color-theme)
(color-theme-initialize)
(color-theme-jbensson)

(require 'whitespace)    ;; https://www.emacswiki.org/emacs/WhiteSpace

;; If possible set up a custom color scheme, otherwise turn colors off
(autoload 'custom-set-faces "font-lock" "Set the color scheme" t)
(autoload 'font-lock-fontify-buffer "font-lock" "Fontify Buffer" t)
(condition-case err
    (progn (apply 'custom-set-faces lconfig-font-lock-faces)
           (add-hook 'c-mode-common-hook 'font-lock-fontify-buffer)
           (add-hook 'emacs-lisp-mode-hook 'font-lock-fontify-buffer)
           )
  (error (progn
           (message "Could not customize colors, disabling colored fonts.")
           (setq-default font-lock-auto-fontify t))))

;; To Check the Font C-u C-x =
;; (set-face-attribute 'default nil :font "Courier-12")
(set-face-bold-p 'bold nil)
(setq-default c-basic-offset 2)

;; Highlight past 79 columns
(setq-default c-max-one-liner-length 80)
(setq-default fill-column 80)
(setq column-number-mode t)
(setq line-number-mode t)
(setq longlines-mode t)

;; Email
(setq user-mail-address "bensson.james@gmail.com")

;; Stop Emacs tutorial from showing on startup
(setq inhibit-startup-echo-area-message t)
(setq inhibit-startup-message t)

;;Display current function
(which-function-mode 1)

;; --------------------------- KEY DEFINES ---------------------------------- ;;
;;Moving text
(global-set-key [delete] 'delete-char)
(global-set-key [C-delete] 'kill-word)
;; M-x delete-rectangle: deletes a rectangle
(global-set-key "\C-k" 'kill-line)
(global-set-key "\C-j" 'advertised-undo)
(global-set-key "\M-b" 'shell-command)
(global-set-key "\M-s" 'query-replace-regexp)
(global-set-key "\C-f" 'indent-code-rigidly)
(global-set-key "\C-xu" 'uncomment-region)
(global-set-key "\C-xc" 'comment-region)
(global-set-key "\C-xr" 'revert-buffer)
;;(global-unset-key "\C-o")
;;(global-set-key (kbd "RET") 'open-next-line)

;;Cursor/Buffer Movement Key Defines
(global-set-key "\C-q" 'goto-line)
(global-set-key "\C-r" 'backward-word)
(global-set-key "\C-t" 'forward-word)
(global-set-key "\C-f" 'beginning-of-line)
(global-set-key "\C-g" 'end-of-line)
(global-set-key "\C-v" 'backward-paragraph)
(global-set-key "\C-b" 'forward-paragraph)
(global-set-key "\C-\M-n" 'beginning-of-defun-forward)
(global-set-key [home] 'beginning-of-buffer)
(global-set-key [end] 'end-of-buffer)
(global-set-key [next] 'scroll-up)
(global-set-key [prior] 'scroll-down)
(global-set-key "\M-n" 'scroll-up-in-place)
(global-set-key "\M-p" 'scroll-down-in-place)

;;Buffer Movement
(global-set-key "\C-xz" 'other-window)
(global-set-key "\C-xa" 'other-window-backward)
(autoload 'cycle-buffer "cycle-buffer" "Cycle forward." t)
(autoload 'cycle-buffer-backward "cycle-buffer" "Cycle backward." t)
(autoload 'cycle-buffer-permissive "cycle-buffer" "Cycle forward allowing *buffers*." t)
(autoload 'cycle-buffer-backward-permissive "cycle-buffer" "Cycle backward allowing *buffers*." t)
(autoload 'cycle-buffer-toggle-interesting "cycle-buffer" "Toggle if this buffer will be considered." t)
(global-set-key [(f9)]        'cycle-buffer-backward)
(global-set-key [(f10)]       'cycle-buffer)
(global-set-key [(shift f9)]  'cycle-buffer-backward-permissive)
(global-set-key [(shift f10)] 'cycle-buffer-permissive)
