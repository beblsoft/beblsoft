# Emacs Technology Documentation

Emacs or EMACS is a family of text editors that are characterized by their extensibility.
Development of the first Emacs began in the mid-1970s. The most widely used variant, GNU Emacs,
is still actively developed as of 2019.

Features:
- Content-aware editing modes, including syntax coloring, for many file types
- Complete built-in documentation
- Full unicode support
- Highly customizable, using Emacs Lisp code
- Packaging system for downloading and installing extensions

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Emacs),
[Home](https://www.gnu.org/software/emacs/)

## Configuration

To confgure emacs see `tools/bash/emacs.sh:emacs_init`