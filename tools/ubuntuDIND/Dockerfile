#
# NAME:
#  Dockerfile
#
# DESCRIPTION:
#  Ubuntu Docker-in-Docker
#

FROM ubuntu:bionic


# Install Docker inside Docker
RUN apt-get update -y                                                                       && \
    apt-get install -y                                                                         \
      apt-transport-https                                                                      \
      ca-certificates                                                                          \
      curl                                                                                     \
      sudo                                                                                     \
      gnupg-agent                                                                              \
      software-properties-common                                                            && \
   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -                  && \
   add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
   apt-get update -y                                                                        && \
   apt-get install -y docker-ce docker-ce-cli containerd.io                                 && \
   apt-get autoremove -y

# /var/lib/docker cannot be on AUFS, so it is a volume
# Docker creates this volume internally when container is run
# See also:
#  - DIND on dockerhub : https://github.com/docker-library/docker/blob/65fab2cd767c10f22ee66afa919eda80dbdc8872/18.09/dind/Dockerfile
#  - Discussion        : https://github.com/jpetazzo/dind
#
VOLUME /var/lib/docker

# Docker Machine
RUN apt-get update -y               && \
    apt-get install -y python3-pip  && \
    pip3 install docker-compose     && \
    apt-get autoremove -y

# Misc
RUN apt-get install -y nano