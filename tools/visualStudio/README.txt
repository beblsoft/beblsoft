###############################################################################
#                             VISUAL STUDIO CODE                              #
###############################################################################
# Download from Visual Studio Website:
#  - https://code.visualstudio.com/Download
# Unpack; Move to /opt; Link Into User executables
#  - cd Downloads; extract VSCode-linux-x64-stable.zip
#  - rrun mv ~/Downloads/VSCode-linux-x64 /opt
# Download Visual Studio Icon; Move to correct directory
#  - rrun cp ~/Downloads/vso.png /usr/share/icons
# Make Desktop file for displaying program:
#  - rrun touch /usr/share/applications/visualstudiocode.desktop
#  - Add this to new file:
#    ---
#    [Desktop Entry]
#    Name=Visual Studio Code
#    Comment=Multi-platform code editor for Linux
#    Exec=/opt/VSCode-linux-x64/code
#    Icon=/usr/share/icons/vso.png
#    Terminal=false
#    Type=Application
#    StartupNotify=true
#    Categories=TextEditor;Development;Utility;
#    MimeType=text/plain;
#  - If the paths are correct, the menu will automatically see the new visual
#    studio code entry
