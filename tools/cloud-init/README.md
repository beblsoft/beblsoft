# cloud-init Documentation

cloud-init is the defacto multi-distribution package that handles early initialization of a cloud
instance.

Relevant URLs:
[Launchpad](https://launchpad.net/cloud-init),
[Documentation](https://cloudinit.readthedocs.io/en/19.2/index.html)

## CLI Interface

The command line documentation is accessible on any cloud-init installed system:

```bash
% cloud-init --help
usage: cloud-init [-h] [--version] [--file FILES]
                  [--debug] [--force]
                  {init,modules,single,query,dhclient-hook,features,analyze,devel,collect-logs,clean,status}
                                                       ...

optional arguments:
  -h, --help            show this help message and exit
  --version, -v         show program's version number and exit
  --file FILES, -f FILES
                        additional yaml configuration files to use
  --debug, -d           show additional pre-action logging (default: False)
  --force               force running even if no datasource is found (use at
                        your own risk)

Subcommands:
  {init,modules,single,query,dhclient-hook,features,analyze,devel,collect-logs,clean,status}
    init                initializes cloud-init and performs initial modules
    modules             activates modules using a given configuration key
    single              run a single module
    query               Query instance metadata from the command line
    dhclient-hook       run the dhclient hookto record network info
    features            list defined features
    analyze             Devel tool: Analyze cloud-init logs and data
    devel               Run development tools
    collect-logs        Collect and tar all cloud-init debug info
    clean               Remove logs and artifacts so cloud-init can re-run
    status              Report cloud-init status or wait on completion
```

### `cloud-init features`

Print out each feature supported. If cloud-init does not have a features subcommand, it also does
not support any features described.

```bash
% cloud-init features
NETWORK_CONFIG_V1
NETWORK_CONFIG_V2
```

### `cloud-init status`

Report whether cloud-init is running, done, disabled, or errored. Exits non-zero if an error is detected
in cloud-init.

```bash
# Cloud-init short
% cloud-init status
status: running

# Long
% cloud-init status --long
status: running
time: Fri, 26 Jan 2018 21:39:43 +0000
detail:
Running in stage: init-local
```

### `cloud-init collect-logs`

Collect and tar cloud-init generated logs, data files, and system information for triage.

Logs collected are:

- `/var/log/cloud-init*log`
- `/run/cloud-init`
- `cloud-init package version`
- dmesg output
- journalctl output
- `/var/lib/cloud/instance/user-data.txt`

### `cloud-init query`

Query standardized cloud instance metadata crawled by cloud-init and stored in
`/run/cloud-init/instance-data.json`

Options:
- `–all`: Dump all available instance data as json which can be queried.
- `–instance-data`: Optional path to a different instance-data.json file to source for queries.
- `–list-keys`: List available query keys from cached instance data.

  ```bash
  # List all top-level query keys available (includes standardized aliases)
  % cloud-init query --list-keys
  availability_zone
  base64_encoded_keys
  cloud_name
  ds
  instance_id
  local_hostname
  region
  v1
  ```

- `<varname>`: A dot-delimited variable path into the instance-data.json

  ```bash
  # Query cloud-init standardized metadata on any cloud
  % cloud-init query v1.cloud_name
  aws  # or openstack, azure, gce etc.

  # Any standardized instance-data under a <v#> key is aliased as a top-level
  # key for convenience.
  % cloud-init query cloud_name
  aws  # or openstack, azure, gce etc.

  # Query datasource-specific metadata on EC2
  % cloud-init query ds.meta_data.public_ipv4
  ```

- `-format`: A string that will use jinja-template syntax to render a string

  ```bash
  # Generate a custom hostname fqdn based on instance-id, cloud and region
  % cloud-init query --format 'custom-{{instance_id}}.{{region}}.{{v1.cloud_name}}.com'
  custom-i-0e91f69987f37ec74.us-east-2.aws.com
  ```

### `cloud-init analyze`

Get detauled reports of where cloud-init spends most of its time.

Subcommands:
- __blame__: Report ordered by most costly operations
- __dump__: Machine-readable JSON dump of all cloud-init tracked events.
- __show__: show time-ordered report of the cost of operations during each boot stage.
- __boot__: show timestamps from kernel initialization, kernel finish initialization, and cloud-init start.

### `cloud-init clean`

Remove cloud-init artifacts from `/var/lib/cloud` and optionally reboot the machine so cloud-init re-runs
all stages as it did on first boot.

Options:
- `–logs`: Optionally remove `/var/log/cloud-init*log` files.
- `–reboot`: Reboot the system after removing artifacts.

### `cloud-init init`

Generally run by OS init systems to execute cloud-init's stages init and init-local.

Can be run on the commandline, but is generally gated to run only once due to semaphores in
`/var/lib/cloud/instance/sem/` and `/var/lib/cloud/sem`

Options:
- `–local`: Run init-local stage instead of init.

### `cloud-init modules`

Generally run by OS init systmes to execute _modules:config_ and _modules:final_ boot stages.
This executes cloud config Modules configured to run in the init, config, and final stages.

The modules are declared to run in various boot stages in the file `/etc/cloud/cloud.cfg` under
key `cloud_init_modules`, `cloud_config_modules`, and `cloud_final_modules`.

Options:
- `–mode (init|config|final)`: Run modules:init, modules:config or modules:final cloud-init stages.

### `cloud-init single`

Attempt to run a single named cloud config module.

Options:
- `–name`: The cloud-config module name to run
- `–frequency`: Optionally override the declared module frequency with one of (always|once-per-instance|once)

```bash
% cloud-init single --name set_hostname --frequency always
```

## Availability

It is currently installed in the Ubuntu Cloud Images and also in the official Ubuntu images available
on EC2, Azure, GCE and many other clouds.

Versions for other systems can be (or have been) created for the following distributions:
- Ubuntu
- Fedora
- Debian
- RHEL
- CentOS

## User-Data Formats

User data that will be acted upon by cloud-init must be in one of the following types

### Gzip Compressed Content

Content found to be gzip compressed will be uncompressed. The uncompressed data will then be used as
if it were not compressed. This is typically useful because user-data is limited to ~16384 bytes.

### Mime Mulit Part Archive

This list of rules is applie to each part of this mulit-part file. Using a mimi-multipart file, the
user can specify more than one type of data.

Supported content-types:

- text/x-include-once-url
- text/x-include-url
- text/cloud-config-archive
- text/upstart-job
- text/cloud-config
- text/part-handler
- text/x-shellscript
- text/cloud-boothook

See [example here](https://github.com/ukayani/cloud-init-example/blob/master/README.md).

### User-Data Script

Typically used by those who just want to execute a shell script.

Begins with `#!` or `Content-Type: text/x-shellscript` when using a MIME archive.

### Include File

This content is a `include` file

The finle contains a list of urls, one per line. Each of the URLs will be read, and their content
will be passed through this same set of rules. I.E., the content read from the URL can be gzipped,
mimi-multi-part, or plain text.

Begins with `#include` or `Content-Type: text/x-include-url`

### Cloud Config Data

Cloud-config is the simplest way to accomplish some things via user-data. Using cloud-config syntax,
the user can specify certain things in a human friendly format.

These things include:
- apt upgrade should be run on first boot
- a different apt mirror should be used
- additional apt sources should be added
- certain ssh keys should be imported
- and many more…

Begins with `#cloud-config` or `Content-Type: text/cloud-config`

### Upstart Job

Content is placed into a file in `/etc/init` and will be consumed by upstart as any other upstart
job.

Begins with : `#upstart-job` or `Content-Type: text/upstart-job`

### Cloud Boothookd

This content is `boothook` data. It is stored in a file under `/var/lib/cloud` and then executed
immediately. This is the earliest `hook` available. Note, that there is no mechanism provided for
running only once. The boothook must take care of itself.

Begins with `#cloud-boothook` or `Content-Type: text/cloud-boothook`

### Part Handler

Part handler contains custom code for either supporting new mime-types in multi-part user data,
or overriding the existing handlers for supported mime-types.

Begins with: `#part-handler` or `Content-Type: text/part-handler` when using a MIME archive.

For example:

```python
#part-handler
# vi: syntax=python ts=4

def list_types():
    # return a list of mime-types that are handled by this module
    return(["text/plain", "text/go-cubs-go"])

def handle_part(data,ctype,filename,payload):
    # data: the cloudinit object
    # ctype: '__begin__', '__end__', or the specific mime-type of the part
    # filename: the filename for the part, or dynamically generated part if
    #           no filename is given attribute is present
    # payload: the content of the part (empty for begin or end)
    if ctype == "__begin__":
       print "my handler is beginning"
       return
    if ctype == "__end__":
       print "my handler is ending"
       return

    print "==== received ctype=%s filename=%s ====" % (ctype,filename)
    print payload
    print "==== end ctype=%s filename=%s" % (ctype, filename)
```

## Instance Metadata

Instance data is the collection of all configuration data the cloud-init processes to configure the
instance. This comes in the following sources:
- cloud-provided metadata services (aka metadata)
- custom config-drive attached to the instance
- cloud-config seed files in the booted cloud image or distribution
- vendordata provided from files or cloud metadata services
- userdata provided at instance creation

Cloud-init produces a simple json object in `/run/cloud-init/instance-data.json` which represents
standardized and version representation of the metadata it consumes during initial boot.

Cloud-init stores any instance data processed in the following files:
- `/run/cloud-init/instance-data.json`: world-readable json containing standardized keys, sensitive keys redacted
- `/run/cloud-init/instance-data-sensitive.json`: root-readable unredacted json blob
- `/var/lib/cloud/instance/user-data.txt`: root-readable sensitive raw userdata
- `/var/lib/cloud/instance/vendor-data.txt`: root-readable sensitive raw vendordata

### Format of instance-data.json

The `instance-data.json` and `instance-data-sensitive.json` files are well-formed JSON and record
the set of keys and values for any metadata processed by cloud-init. Cloud-init standardizes the format
for this content so that it can be generalized across different cloud platforms.

Basic top-level keys:
- __base64_encoded_keys__: A list of forward-slash delimited key paths into the instance-data.json
  object whose value is base64encoded for json compatibility. Values at these paths should be decoded
  to get the original value.

- __sensitive_keys__: A list of forward-slash delimited key paths into the instance-data.json object
  whose value is considered by the datasource as ‘security sensitive’. Only the keys listed here
  will be redacted from instance-data.json for non-root users.

- __ds__: Datasource-specific metadata crawled for the specific cloud platform. It should closely
  represent the structure of the cloud metadata crawled. The structure of content and details
  provided are entirely cloud-dependent. Mileage will vary depending on what the cloud exposes.
  The content exposed under the ‘ds’ key is currently experimental and expected to change slightly in
  the upcoming cloud-init release.

- __v1__: Standardized cloud-init metadata keys, these keys are guaranteed to exist on all cloud
  platforms. They will also retain their current behavior and format and will be carried forward
  even if cloud-init introduces a new version of standardized keys with v2.

Below is an example of `/run/cloud-init/instance_data.json` on an EC2 instance:
```json
{
 "base64_encoded_keys": [],
 "ds": {
  "_doc": "EXPERIMENTAL: The structure and format of content scoped under the 'ds' key may change in subsequent releases of cloud-init.",
  "_metadata_api_version": "2016-09-02",
  "dynamic": {
   "instance-identity": {
    "document": {
     "accountId": "437526006925",
     "architecture": "x86_64",
     "availabilityZone": "us-east-2b",
     "billingProducts": null,
     "devpayProductCodes": null,
     "imageId": "ami-079638aae7046bdd2",
     "instanceId": "i-075f088c72ad3271c",
     "instanceType": "t2.micro",
     "kernelId": null,
     "marketplaceProductCodes": null,
     "pendingTime": "2018-10-05T20:10:43Z",
     "privateIp": "10.41.41.95",
     "ramdiskId": null,
     "region": "us-east-2",
     "version": "2017-09-30"
    },
    "pkcs7": [
     "MIAGCSqGSIb3DQEHAqCAMIACAQExCzAJBgUrDgMCGgUAMIAGCSqGSIb3DQEHAaCAJIAEggHbewog",
     "ICJkZXZwYXlQcm9kdWN0Q29kZXMiIDogbnVsbCwKICAibWFya2V0cGxhY2VQcm9kdWN0Q29kZXMi",
     "IDogbnVsbCwKICAicHJpdmF0ZUlwIiA6ICIxMC40MS40MS45NSIsCiAgInZlcnNpb24iIDogIjIw",
     "MTctMDktMzAiLAogICJpbnN0YW5jZUlkIiA6ICJpLTA3NWYwODhjNzJhZDMyNzFjIiwKICAiYmls",
     "bGluZ1Byb2R1Y3RzIiA6IG51bGwsCiAgImluc3RhbmNlVHlwZSIgOiAidDIubWljcm8iLAogICJh",
     "Y2NvdW50SWQiIDogIjQzNzUyNjAwNjkyNSIsCiAgImF2YWlsYWJpbGl0eVpvbmUiIDogInVzLWVh",
     "c3QtMmIiLAogICJrZXJuZWxJZCIgOiBudWxsLAogICJyYW1kaXNrSWQiIDogbnVsbCwKICAiYXJj",
     "aGl0ZWN0dXJlIiA6ICJ4ODZfNjQiLAogICJpbWFnZUlkIiA6ICJhbWktMDc5NjM4YWFlNzA0NmJk",
     "ZDIiLAogICJwZW5kaW5nVGltZSIgOiAiMjAxOC0xMC0wNVQyMDoxMDo0M1oiLAogICJyZWdpb24i",
     "IDogInVzLWVhc3QtMiIKfQAAAAAAADGCARcwggETAgEBMGkwXDELMAkGA1UEBhMCVVMxGTAXBgNV",
     "BAgTEFdhc2hpbmd0b24gU3RhdGUxEDAOBgNVBAcTB1NlYXR0bGUxIDAeBgNVBAoTF0FtYXpvbiBX",
     "ZWIgU2VydmljZXMgTExDAgkAlrpI2eVeGmcwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkq",
     "hkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE4MTAwNTIwMTA0OFowIwYJKoZIhvcNAQkEMRYEFK0k",
     "Tz6n1A8/zU1AzFj0riNQORw2MAkGByqGSM44BAMELjAsAhRNrr174y98grPBVXUforN/6wZp8AIU",
     "JLZBkrB2GJA8A4WJ1okq++jSrBIAAAAAAAA="
    ],
    "rsa2048": [
     "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwGggCSABIIB",
     "23sKICAiZGV2cGF5UHJvZHVjdENvZGVzIiA6IG51bGwsCiAgIm1hcmtldHBsYWNlUHJvZHVjdENv",
     "ZGVzIiA6IG51bGwsCiAgInByaXZhdGVJcCIgOiAiMTAuNDEuNDEuOTUiLAogICJ2ZXJzaW9uIiA6",
     "ICIyMDE3LTA5LTMwIiwKICAiaW5zdGFuY2VJZCIgOiAiaS0wNzVmMDg4YzcyYWQzMjcxYyIsCiAg",
     "ImJpbGxpbmdQcm9kdWN0cyIgOiBudWxsLAogICJpbnN0YW5jZVR5cGUiIDogInQyLm1pY3JvIiwK",
     "ICAiYWNjb3VudElkIiA6ICI0Mzc1MjYwMDY5MjUiLAogICJhdmFpbGFiaWxpdHlab25lIiA6ICJ1",
     "cy1lYXN0LTJiIiwKICAia2VybmVsSWQiIDogbnVsbCwKICAicmFtZGlza0lkIiA6IG51bGwsCiAg",
     "ImFyY2hpdGVjdHVyZSIgOiAieDg2XzY0IiwKICAiaW1hZ2VJZCIgOiAiYW1pLTA3OTYzOGFhZTcw",
     "NDZiZGQyIiwKICAicGVuZGluZ1RpbWUiIDogIjIwMTgtMTAtMDVUMjA6MTA6NDNaIiwKICAicmVn",
     "aW9uIiA6ICJ1cy1lYXN0LTIiCn0AAAAAAAAxggH/MIIB+wIBATBpMFwxCzAJBgNVBAYTAlVTMRkw",
     "FwYDVQQIExBXYXNoaW5ndG9uIFN0YXRlMRAwDgYDVQQHEwdTZWF0dGxlMSAwHgYDVQQKExdBbWF6",
     "b24gV2ViIFNlcnZpY2VzIExMQwIJAM07oeX4xevdMA0GCWCGSAFlAwQCAQUAoGkwGAYJKoZIhvcN",
     "AQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTgxMDA1MjAxMDQ4WjAvBgkqhkiG9w0B",
     "CQQxIgQgkYz0pZk3zJKBi4KP4egeOKJl/UYwu5UdE7id74pmPwMwDQYJKoZIhvcNAQEBBQAEggEA",
     "dC3uIGGNul1OC1mJKSH3XoBWsYH20J/xhIdftYBoXHGf2BSFsrs9ZscXd2rKAKea4pSPOZEYMXgz",
     "lPuT7W0WU89N3ZKviy/ReMSRjmI/jJmsY1lea6mlgcsJXreBXFMYucZvyeWGHdnCjamoKWXkmZlM",
     "mSB1gshWy8Y7DzoKviYPQZi5aI54XK2Upt4kGme1tH1NI2Cq+hM4K+adxTbNhS3uzvWaWzMklUuU",
     "QHX2GMmjAVRVc8vnA8IAsBCJJp+gFgYzi09IK+cwNgCFFPADoG6jbMHHf4sLB3MUGpiA+G9JlCnM",
     "fmkjI2pNRB8spc0k4UG4egqLrqCz67WuK38tjwAAAAAAAA=="
    ],
    "signature": [
     "Tsw6h+V3WnxrNVSXBYIOs1V4j95YR1mLPPH45XnhX0/Ei3waJqf7/7EEKGYP1Cr4PTYEULtZ7Mvf",
     "+xJpM50Ivs2bdF7o0c4vnplRWe3f06NI9pv50dr110j/wNzP4MZ1pLhJCqubQOaaBTF3LFutgRrt",
     "r4B0mN3p7EcqD8G+ll0="
    ]
   }
  },
  "meta-data": {
   "ami-id": "ami-079638aae7046bdd2",
   "ami-launch-index": "0",
   "ami-manifest-path": "(unknown)",
   "block-device-mapping": {
    "ami": "/dev/sda1",
    "ephemeral0": "sdb",
    "ephemeral1": "sdc",
    "root": "/dev/sda1"
   },
   "hostname": "ip-10-41-41-95.us-east-2.compute.internal",
   "instance-action": "none",
   "instance-id": "i-075f088c72ad3271c",
   "instance-type": "t2.micro",
   "local-hostname": "ip-10-41-41-95.us-east-2.compute.internal",
   "local-ipv4": "10.41.41.95",
   "mac": "06:74:8f:39:cd:a6",
   "metrics": {
    "vhostmd": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
   },
   "network": {
    "interfaces": {
     "macs": {
      "06:74:8f:39:cd:a6": {
       "device-number": "0",
       "interface-id": "eni-052058bbd7831eaae",
       "ipv4-associations": {
        "18.218.221.122": "10.41.41.95"
       },
       "local-hostname": "ip-10-41-41-95.us-east-2.compute.internal",
       "local-ipv4s": "10.41.41.95",
       "mac": "06:74:8f:39:cd:a6",
       "owner-id": "437526006925",
       "public-hostname": "ec2-18-218-221-122.us-east-2.compute.amazonaws.com",
       "public-ipv4s": "18.218.221.122",
       "security-group-ids": "sg-828247e9",
       "security-groups": "Cloud-init integration test secgroup",
       "subnet-id": "subnet-282f3053",
       "subnet-ipv4-cidr-block": "10.41.41.0/24",
       "subnet-ipv6-cidr-blocks": "2600:1f16:b80:ad00::/64",
       "vpc-id": "vpc-252ef24d",
       "vpc-ipv4-cidr-block": "10.41.0.0/16",
       "vpc-ipv4-cidr-blocks": "10.41.0.0/16",
       "vpc-ipv6-cidr-blocks": "2600:1f16:b80:ad00::/56"
      }
     }
    }
   },
   "placement": {
    "availability-zone": "us-east-2b"
   },
   "profile": "default-hvm",
   "public-hostname": "ec2-18-218-221-122.us-east-2.compute.amazonaws.com",
   "public-ipv4": "18.218.221.122",
   "public-keys": {
    "cloud-init-integration": [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSL7uWGj8cgWyIOaspgKdVy0cKJ+UTjfv7jBOjG2H/GN8bJVXy72XAvnhM0dUM+CCs8FOf0YlPX+Frvz2hKInrmRhZVwRSL129PasD12MlI3l44u6IwS1o/W86Q+tkQYEljtqDOo0a+cOsaZkvUNzUyEXUwz/lmYa6G4hMKZH4NBj7nbAAF96wsMCoyNwbWryBnDYUr6wMbjRR1J9Pw7Xh7WRC73wy4Va2YuOgbD3V/5ZrFPLbWZW/7TFXVrql04QVbyei4aiFR5n//GvoqwQDNe58LmbzX/xvxyKJYdny2zXmdAhMxbrpFQsfpkJ9E/H5w0yOdSvnWbUoG5xNGoOB cloud-init-integration"
    ]
   },
   "reservation-id": "r-0594a20e31f6cfe46",
   "security-groups": "Cloud-init integration test secgroup",
   "services": {
    "domain": "amazonaws.com",
    "partition": "aws"
   }
  }
 },
 "sensitive_keys": [],
 "v1": {
  "_beta_keys": [
   "subplatform"
  ],
  "availability-zone": "us-east-2b",
  "availability_zone": "us-east-2b",
  "cloud_name": "aws",
  "instance_id": "i-075f088c72ad3271c",
  "local_hostname": "ip-10-41-41-95",
  "platform": "ec2",
  "public_ssh_keys": [
   "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSL7uWGj8cgWyIOaspgKdVy0cKJ+UTjfv7jBOjG2H/GN8bJVXy72XAvnhM0dUM+CCs8FOf0YlPX+Frvz2hKInrmRhZVwRSL129PasD12MlI3l44u6IwS1o/W86Q+tkQYEljtqDOo0a+cOsaZkvUNzUyEXUwz/lmYa6G4hMKZH4NBj7nbAAF96wsMCoyNwbWryBnDYUr6wMbjRR1J9Pw7Xh7WRC73wy4Va2YuOgbD3V/5ZrFPLbWZW/7TFXVrql04QVbyei4aiFR5n//GvoqwQDNe58LmbzX/xvxyKJYdny2zXmdAhMxbrpFQsfpkJ9E/H5w0yOdSvnWbUoG5xNGoOB cloud-init-integration"
  ],
  "region": "us-east-2",
  "subplatform": "metadata (http://169.254.169.254)"
 }
}
```

### Usage

As of cloud-init v 18.4, any variables present in `/run/cloud-init/instance-data.json` can be used in:
- User-data scripts
- Cloud config data
- Command line interface via cloud-init query or cloud-init devel render

For example:
```bash
## template: jinja
#!/bin/bash
{% if v1.region == 'us-east-2' -%}
echo 'Installing custom proxies for {{ v1.region }}
sudo apt-get install my-xtra-fast-stack
{%- endif %}
```

## Directory Layout

Cloud-init's directory structure is somewhat different from a regular application:

```text
/var/lib/cloud/                 - Main directory containing cloud-init specific subdirectories
    - data/                     - Contains information related to intsance ids, datasources, and hostnames
       - instance-id
       - previous-instance-id
       - datasource
       - previous-datasource
       - previous-hostname
    - handlers/                 - Custom part-handlers code is written out here
    - instance                  - Sympink to the current active instance in the `instances/` directory
    - instances/                - All instances that were created using this image end up with instance
                                  identifier subdirectories
        i-00000XYZ/
          - boot-finished
          - cloud-config.txt
          - datasource
          - handlers/
          - obj.pkl
          - scripts/            - Instance scripts parsed from user-data.txt
            - part-002          - Script parsed from user-data.txt
          - sem/
          - user-data.txt
          - user-data.txt.i
    - scripts/                  - Scripts that are downloaded/created by the corresponding part-handler
                                  end up here
       - per-boot/
       - per-instance/
       - per-once/
    - seed/                     - TBD
    - sem/                      - Cloud-init module semaphore, ensuring modules run _per-once_, _per-instance_,
                                  or _per-always_

/var/log/                       - Log directory
    - cloud-init-output.log
    - cloud-init.log
```

## Boot Stages

In order to be able to provide the functionality that it does, cloud-init must be integrated into
the boot in a fairly controlled way.

There are 5 stages:
1. Generator
2. Local
3. Network
4. Config
5. Final

### Generator

When booting under systemd, a generator will run that determines if cloud-init.target should be
included in the boot goals. By default this generator will enable cloud-init. It will not enable
cloud-init if either:

- A file exists: `/etc/cloud/cloud-init.disabled`
- The kernel command line as found in `/proc/cmdline` contains `cloud-init=disabled`.
  When running in a container, the kernel command line is not honored, but cloud-init will read an
  environment variable named `KERNEL_CMDLINE` in its place.

This mechanism for disabling at runtime currently only exists in systemd.

### Local

- __systemd service__: cloud-init-local.service
- __runs__: As soon as possible with / mounted read-write.
- __blocks__: as much of boot as possible, must block network bringup.
- __modules__: none

The purpose of the local stage is to locate "local" data sources and apply network configuration to
the system.

### Network

- __systemd service__: cloud-init.service
- __runs__: After local stage and configured networking is up.
- __blocks__: As much of remaining boot as possible.
- __modules__: cloud_init_modules in `/etc/cloud/cloud.cfg`

This stage requires all configured networking to be online, as it will fully process any user-data
that is found. Here, processing means:
- retrive any `#include` or `#include-once` (recursively) including http
- uncompress any compressed content
- run any part-handler found.

### Config

- __systemd service__: cloud-config.service
- __runs__: After network stage.
- __blocks__: None.
- __modules__: cloud_config_modules in `/etc/cloud/cloud.cfg`

This stages runs config modules only. Modules that do not really have an effect on other stages of
boot are run here.

### Final

- __systemd service__: cloud-final.service
- __runs__: As final part of boot (traditional “rc.local”)
- __blocks__: None.
- __modules__: cloud_final_modules in `/etc/cloud/cloud.cfg`

This stage runs as late in boot as possible. Any scripts that a user is accustomed to running after
logging into a system should run correctly here. Things that run here include
- package installations
- configuration management plugins (puppet, chef, salt-minion)
- user-scripts (including runcmd).

For scripts external to cloud-init looking to wait until cloud-init finished, the `cloud-init status`
subcommand can help block external scripts until cloud-init is done without having to write your own
subcommand.

## Datasources

Datasources are sources of configuration data for cloud-init that typically come from the user (aka
userdata) or come from the stack that created the configuration drive (aka metadata). Typical
userdata would include files, yaml, and shell scripts while typical metadata would include server
name, instance id, and cloud specific details. Since there are multiple ways to provide this data (each
cloud solution seems to prefer its own way) internally a datasource abstract class was created to allow
for a single way to access the different cloud system methods to provide this data through the typical
usage of subclasses.

Any metadata processed by cloud-init's datasources is persisted as `/run/cloud-init/instance-data.json`.

## Modules

### Runcmd

Run aribtrary commands at a rc.local like level with output to the console. Each item can be either
a list or a string.

Ex:
```yaml
runcmd:
    - [ ls, -l, / ]
    - [ sh, -xc, "echo $(date) ': hello world!'" ]
    - [ sh, -c, echo "=========hello world'=========" ]
    - ls -l /root
    - [ wget, "http://example.org", -O, /tmp/index.html ]
```

### Scripts Per Boot

Run per boot scripts. Any scripts in the `scripts/per-boot` directory on the datasource will be run
every time the system boots. Scripts will be run in alphabetical order. This module does not accept
any config keys.

### Scripts Per Instance

Any scripts in the `scripts/per-instance` directory on the datasource will be run when a new instance
is first booted. Scripts will be run in alphabetical order. This module does not accept any config keys.

### Scripts Per Once

Any scripts in the `scripts/per-once` directory on the datasource will be run only once.
Scripts will be run in alphabetical order. This module does not accept any config keys.

### Scripts User

This module runs all user scripts. User scripts are not specified in the scripts directory in the
datasource, but rather are present in the scripts dir in the instance configuration. Any cloud-config
parts with a `#!` will be treated as a script and run. Scripts specified as cloud-config parts will
be run in the order they are specified in the configuration. This module does not accept any config keys.

### Scripts Vendor

Any scripts in the `scripts/vendor` directory in the datasource will be run when a new instance is
first booted. Scripts will be run in alphabetical order. Vendor scripts can be run with an optional
prefix specified in the prefix entry under the vendor_data config key.
