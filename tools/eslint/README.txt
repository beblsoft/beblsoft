OVERVIEW
===============================================================================
ESLint Technology Documentation
- Description
  * ESLint a pluggable linting utility for JavaScript and JSX
  * Code linting is a type of static analysis that is frequently used to find
    problematic patterns or code that doesn’t adhere to certain style guidelines.
    There are code linters for most programming languages, and compilers sometimes
    incorporate linting into the compilation process.
  * ESLint was created to allow developers to create their own linting rules.
    ESLint is designed to have all rules completely pluggable.
  * Written using Node.js, installed via NPM
- Relevant URLs
  * Home                            : https://eslint.org/
  * Wikipedia                       : https://en.wikipedia.org/wiki/JSLint
  * Rules                           : https://eslint.org/docs/rules/


INSTALLATION, BASIC USAGE
===============================================================================
- Local
  * install                         : npm install eslint --save-dev
  * Setup configuration file        : ./node_modules/.bin/eslint --init
  * Run on file                     : ./node_modules/.bin/eslint yourfile.js
- Global
  * install                         : npm install -g eslint
  * Setup configuration file        : eslint --init
  * Run on file                     : eslint yourfile.js


CONFIGURATION
===============================================================================
- Configuration File                : eslint --init craetes .eslintrc in directory
- Example .eslintrc                 : Each rule has different levels for fined grain control
                                      {
                                      	"rules": {
                                      	  "semi": ["error", "always"],
                                      	  "quotes": ["error", "double"]
                                      	}
                                      }


IGNORING RULES
===============================================================================
- On a specific line                : /* eslint-disable-line */

