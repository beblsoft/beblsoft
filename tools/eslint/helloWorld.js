/*
  NAME:
    helloWorld.js
*/

/* ------------------------ IMPORTS ---------------------------------------- */
let http = require('http');


/* ------------------------ MAIN ------------------------------------------- */
http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World!');
}).listen(8080);
