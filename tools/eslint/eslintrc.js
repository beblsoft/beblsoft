/**
 * @file ESLint Configuration File
 */


/* ------------------------ GLOBALS ---------------------------------------- */
let OFF = 0;
let WARN = 1;
let ERROR = 2;


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = exports = {
  env: {
    'es6': true,
    'commonjs': true,
    'mocha': true,
    'node': true,
    'cypress/globals': true
  },

  plugins: [
    'mocha',
    'cypress'
  ],

  extends: [
    'eslint:recommended',
    'plugin:vue/essential'
  ],

  parserOptions: {
    ecmaVersion: 2017,
    parser: 'babel-eslint',
    sourceType: 'module'
  },

  globals: {
    env: true,
    process: true,
    require: true,
    module: true,
    exports: true,
    __dirname: true,
    tg: true
  },

  rules: {
    // Possible Errors
    'no-extra-parens': OFF,
    'no-unexpected-multiline': ERROR,
    'valid-jsdoc': [OFF, {
      requireReturn: false,
      requireReturnDescription: false,
      requireParamDescription: false,
      prefer: { returns: 'return' }
    }],

    // Best Practices
    'accessor-pairs': [ERROR, {
      getWithoutSet: false,
      setWithoutGet: true
    }],
    'block-scoped-var': WARN,
    'consistent-return': ERROR,
    'curly': ERROR,
    'default-case': WARN,
    'dot-location': [WARN, 'property'],
    'dot-notation': WARN,
    'eqeqeq': [ERROR, 'smart'],
    'guard-for-in': WARN,
    'no-alert': ERROR,
    'no-caller': ERROR,
    'no-case-declarations': WARN,
    'no-div-regex': WARN,
    'no-else-return': WARN,
    'no-empty-pattern': WARN,
    'no-eq-null': WARN,
    'no-eval': ERROR,
    'no-extend-native': ERROR,
    'no-extra-bind': WARN,
    'no-floating-decimal': WARN,
    'no-implicit-coercion': [WARN, {
      boolean: true,
      number: true,
      string: true
    }],
    'no-implied-eval': ERROR,
    'no-invalid-this': OFF,
    'no-iterator': ERROR,
    'no-labels': WARN,
    'no-lone-blocks': WARN,
    'no-loop-func': ERROR,
    'no-magic-numbers': OFF,
    'no-multi-spaces': ERROR,
    'no-multi-str': WARN,
    'no-native-reassign': ERROR,
    'no-new-func': ERROR,
    'no-new-wrappers': ERROR,
    'no-new': ERROR,
    'no-octal-escape': ERROR,
    'no-param-reassign': ERROR,
    'no-process-env': OFF,
    'no-proto': ERROR,
    'no-redeclare': ERROR,
    'no-return-assign': ERROR,
    'no-script-url': ERROR,
    'no-self-compare': ERROR,
    'no-throw-literal': ERROR,
    'no-unused-expressions': ERROR,
    'no-useless-call': ERROR,
    'no-useless-concat': ERROR,
    'no-void': WARN,
    'no-warning-comments': [WARN, {
      terms: ['TODO', 'FIXME', 'WARN'],
      location: 'start'
    }],
    'no-with': WARN,
    'radix': WARN,
    'vars-on-top': ERROR,
    'wrap-iife': [ERROR, 'outside'],
    'yoda': OFF,
    'strict': [ERROR, 'never'],

    // Variables
    'init-declarations': [ERROR, 'always'],
    'no-catch-shadow': WARN,
    'no-delete-var': ERROR,
    'no-label-var': ERROR,
    'no-shadow-restricted-names': ERROR,
    'no-shadow': WARN,
    'no-undef-init': OFF,
    'no-undef': ERROR,
    'no-undefined': OFF,
    'no-unused-vars': [WARN, {
      varsIgnorePattern: '_|should|expect|njstrace',
      args: 'none'
    }], // Chai
    'no-use-before-define': [ERROR, { functions: true, classes: false }],

    // Node.js and CommonJS
    'callback-return': [WARN, ['callback', 'next', 'cb']],
    'global-require': ERROR,
    'handle-callback-err': OFF,
    'no-mixed-requires': WARN,
    'no-new-require': ERROR,
    'no-path-concat': ERROR,
    'no-process-exit': ERROR,
    'no-restricted-modules': OFF,
    'no-sync': OFF,
    'no-console': OFF,

    // ECMAScript 6 support
    'arrow-body-style': OFF,
    'arrow-parens': [ERROR, 'always'],
    'arrow-spacing': [ERROR, { before: true, after: true }],
    'constructor-super': ERROR,
    'generator-star-spacing': [ERROR, 'after'],
    'no-confusing-arrow': ERROR,
    'no-class-assign': ERROR,
    'no-const-assign': ERROR,
    'no-dupe-class-members': ERROR,
    'no-this-before-super': ERROR,
    'no-var': WARN,
    'object-shorthand': OFF,
    'prefer-arrow-callback': OFF,
    'prefer-spread': WARN,
    'prefer-template': WARN,
    'require-yield': ERROR,

    // Stylistic - everything here is a warning because of style.
    'array-bracket-spacing': OFF,
    'block-spacing': [WARN, 'always'],
    'brace-style': [WARN, '1tbs', { allowSingleLine: true }],
    'camelcase': WARN,
    'comma-spacing': [WARN, { before: false, after: true }],
    'comma-style': [WARN, 'last'],
    'computed-property-spacing': [WARN, 'never'],
    'consistent-this': [WARN, 'self'],
    'eol-last': WARN,
    'func-names': OFF,
    'func-style': OFF,
    'id-length': [WARN, { min: 1, max: 50 }],
    'indent': [OFF, 2],
    'jsx-quotes': [WARN, 'prefer-double'],
    'linebreak-style': [WARN, 'unix'],
    'lines-around-comment': [WARN, { allowClassStart: true, allowBlockStart: true }],
    'max-depth': [WARN, 8],
    'max-len': [WARN, 1028],
    'max-nested-callbacks': [WARN, 8],
    'max-params': [WARN, 8],
    'new-cap': WARN,
    'new-parens': WARN,
    'no-array-constructor': WARN,
    'no-bitwise': OFF,
    'no-continue': OFF,
    'no-inline-comments': OFF,
    'no-lonely-if': WARN,
    'no-mixed-spaces-and-tabs': WARN,
    'no-multiple-empty-lines': WARN,
    'no-negated-condition': OFF,
    'no-nested-ternary': WARN,
    'no-new-object': WARN,
    'no-plusplus': OFF,
    'no-spaced-func': WARN,
    'no-ternary': OFF,
    'no-trailing-spaces': WARN,
    'no-underscore-dangle': OFF,
    'no-unneeded-ternary': WARN,
    'object-curly-spacing': [WARN, 'always'],
    'one-var': OFF,
    'operator-assignment': [WARN, 'always'],
    'operator-linebreak': [WARN, 'after'],
    'padded-blocks': OFF,
    'quote-props': [WARN, 'consistent-as-needed'],
    'quotes': [WARN, 'single'],
    'require-jsdoc': [WARN, {
      require: {
        FunctionDeclaration: true,
        MethodDefinition: true,
        ClassDeclaration: false
      }
    }],
    'semi-spacing': [WARN, { before: false, after: true }],
    'semi': [ERROR, 'always'],
    'sort-vars': OFF,
    'space-before-blocks': [WARN, 'always'],
    'space-before-function-paren': [OFF, {
      anonymous: 'always',
      named: 'never',
      asyncArrow: 'always'
    }],
    'space-in-parens': [WARN, 'never'],
    'space-infix-ops': [WARN, { int32Hint: true }],
    'keyword-spacing': WARN,
    'space-unary-ops': ERROR,
    'spaced-comment': [WARN, 'always'],
    'wrap-regex': WARN,

    // mocha
    'mocha/no-exclusive-tests': WARN,
  }
};
