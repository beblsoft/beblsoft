OVERVIEW
===============================================================================
NoMachine Documentation - A solution for accessing another computer (on same LAN)


NoMachine Download
===============================================================================
- Download NoMachine here           : https://www.nomachine.com/
- Select                            : NoMachine for Linux DEB i386
- Install on mint                   : Click on image
- See NoMachine red icon    	    : Open Linux Mint Menu and add to panel


USE
===============================================================================
- Discover Local Machines           : install nomachine on any machine that you want
                                      to discover on your LAN, log in with username/password
- Turn off adjusting screen res.    : To prevent screen distortion.


HANDY TOOLS
===============================================================================
- What is my IP                     : https://www.whatismyip.com, ifconfig
- Can I reach system x?             : ping x's IP address


