#!/usr/bin/python
"""
NAME
 python.py

DESCRIPTION
 Python Template Sample
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FUNCTIONS ----------------------------------------- #
def SampleFunc():
    """
    SampleFunc documentation
    """
    pass


# ----------------------- CLASSES ------------------------------------------- #
class SampleClass:
    """
    SampleClass documentation.
    """

    def __init__(self):
        """
        Sample initialization routine.
        """
        pass

    def SampleClassFunc(self):
        """
        SampleFunc documentation
        """
        pass
