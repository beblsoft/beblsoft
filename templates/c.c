/*
  FILE
    c.c

  DESCRIPTION:
    C Template Program
*/

/* --------------------- INCLUDES ------------------------------------------ */
#include <stdio.h>


/* --------------------- MAIN ---------------------------------------------- */
/*
  NAME
   main

  DESCRIPTION
    Main program

  ARGS
    N/A

  RETURNS
    0
 */
int main()
{
	printf("Hello Template!\n");
	return 0;
}