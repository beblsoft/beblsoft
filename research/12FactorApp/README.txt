OVERVIEW
===============================================================================
- The Twelve-Factor App Methodology Documentation
- Software Features ----------------: Use declarative formats for setup automation, minimize time
                                      and cost for new developers to join
                                    : Clean contract to underlying operating system, maximizing
                                      protability between execution environments
                                    : Suitable for deployment on cloud platforms
                                    : Minimize divergence between development and production, leveraging
                                      continuous deployment
                                    : Scale up without significant changes to tooling, architecture, or
                                      development practices
- 12 Factors ------------------------
  1.  Codebase                      : One codebase tracked in revision control, many deploys
  2.  Dependencies                  : Explicitly declare and isolate dependencies
  3.  Config                        : Store config in the environment
  4.  Backing Services              : Treat backing services as attached resources
  5.  Build, release, run           : Strictly separate build and run stages
  6.  Processes                     : Execute the app as one or more stateless processes
  7.  Port binding                  : Export services via port binding
  8.  Concurrency                   : Scale out via the process model
  9.  Disposability                 : Maximize robustness with fast startup and graceful shutdown
  10. Dev/prod parity               : Keep development, staging, and production as similar as possible
  11. Logs                          : Treat logs as event streams
  12. Admin Processes               : Run admin/management tasks as one-off processes
- URL ------------------------------: https://12factor.net/



1. CODEBASE
===============================================================================
- Do                                : Use one codebase tracked in revision control, many deploys
- Version Control systems           : Git, Mercurial, Subversion
- Code Repository                   : Copy of the tracking database
- Deploy                            : Running instance of the app. Ex. dev, staging, production



2. DEPENDENCIES
===============================================================================
- Do                                : Explicitly declare and isolate dependencies
                                    : Never rely on implicit existence of system-wide packages
                                    : Do not rely on implicit existence of system tools, Ex. ImageMagick or curl
- Dependency Declaration            : Declares all dependencies
                                      Ex. bundler for Ruby, pip for python
- Dependency Isolation              : Ensure that no implicit dependencies leak in from the surrounding system
                                      Ex. virtualenv for python
- Benefit                           : Simplifies setup for new developers



3. CONFIG
===============================================================================
- Do                                : Store config in environment variables.
                                      Env vars are easy to change between deploys without changing any code and are
                                      language and OS agnostic
                                    : Separate config from code. Be able to make code open source at any moment
                                      without compromising credentials
                                    : Store con
- App Config includes               : Resource handles to DB, memcached, or backing services
                                      Credentials to external services Amazon S3 or Twitter
                                      Per-deploy values such as hostname for the deploy



4. BACKING SERVICES
===============================================================================
- Do                                : Treat backing services as attached resources
                                    : Make no distinction between local and third party services.
                                      Both are attached resources, accessed via a URL or other locator/credentials
                                      stored in the config
                                      Deploy (Dev, Prod) should swap out config locators without any code change
                                    : Resources should be attached and detached from deploys at will
- Backing Service                   : Any service the app consumes over the network as part of its normal operation
- Picture                           : Production Deploy
                                       |
                                       |
                                       |-- mysql://auth@host/db -------------- MySQL
                                       |
                                       |-- smtp://auth@host/ ----------------- Outbound email service
                                       |
                                       |-- https://auth@s3.amazonaws.com/ ---- Amazon S3
                                       |
                                       |-- http://auth@api.twittter.com/ ----- Twitter



5. BUILD, RELEASE, RUN
===============================================================================
- Do                                : Strictly separate build and run stages
                                    : Allow for quick rollback between releases

- Codebase transformed into deploy
  1 Build                           : Transform code repo into executable bundle known as a build
                                      Compile all assets
  2 Release                         : Take the build produced and combine with deploy's config
                                      Release is now ready for immediate execution in the execution context
                                      Releases should have a unique ID. Ex. timestamp or incrementing number (v1020)
  3 Run                             : Run the app in the execution environment by launching some set of the app's
                                      processes against a selected release
- Picture                           : Code
                                       |
                                       v
                                      Build    Config
                                        \       /
                                         -------
                                            |
                                            v
                                         Release



6. PROCESSES
===============================================================================
- Do                                : Execute app as one or more stateless processes
                                    : Design processes to be statelesss and share nothing. Any persisted data
                                      must be stored in a stateful backing service such as a database
                                    : Processes must be able to be wiped out and restarted quickly due
                                      to deploys or config changes
- Simplest case                     : Code is a stand-alone script, running on developer's local laptop
                                      with an installed language runtime
- Complex case                      : Production deploy of sophisticated app using multiple process types,
                                      instantiated into zero or more running processes



7. PORT BINDING
===============================================================================
- Do                                : Export services via port binding
                                    : App is completely self-contained and does not rely on runtime injection
                                      of webserver into the execution environment
- Benefits                          : Allows one app to become the backing service for another app
                                      by providing the URL to the backing app as a resource handle to the consuming app



8. CONCURRENCY
===============================================================================
- Do                                : Scale out via the process model
                                    : Each type of work is mapped to a process type
                                    : When processes share-nothing they can be horizontally scalled
- Picture                           :
                                        Scale |
                                              |          worker.5
                                              |
                                              |          worker.4
                                              |
                                              |  web.3   worker.3
                                              |
                                              |  web.2   worker.2  clock.2
                                              |
                                              |  web.1   worker.1  clock.1
                                              |
                                              -------------------------------
                                                                     Diversity



9. Disposability
===============================================================================
- Do                                : Maximize robustness with fast startup and graceful shutdown
                                    : Processes are disposable, meaning they can be started or stopped at a
                                      moment's notice. This allows for fast elastic scaling and rapid deployment.
                                    : Minimize process startup time
                                    : Shutdown gracefully when SIGTERM received
                                    : Robust against sudden death



10. DEV/PROD PARITY
===============================================================================
- Do                                : Keep development, staging, and production as similar as possible
                                    : Practice continuous deployment
                                    : Resist urg to use different backing services between development and production
- Minimize gaps
  * Time gap                        : Developer code released to production in hours or minutes
  * Personnel gap                   : Developers can deploy code
  * Tools gap                       : Development and production environments are as similar as possible



11. LOGS
===============================================================================
- Do                                : Treat logs as event streams
                                    : App isn't concerned with routing or storage of output stream
                                      Each running process should write event stream, unbuffered to STDOUT
- Logs                              : Stream of aggregated, time-ordered events collected from the output
                                      streams of all running processes and backing services
- Aggregation                       : App event streams can be aggregated for indexing and analysis



12. ADMIN PROCESSES
===============================================================================
- Do                                : Run admin/management processes as one-off processes
                                    : Run in identical environment as the regular long-running processes
- Typical management commands
  * Database migration              : Alter database schema
  * Console                         : Inspect live database models
  * One time scripts                : Run one-time scripts committed to app's repo


