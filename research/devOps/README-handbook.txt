OVERVIEW
===============================================================================
- This document overviews DevOps as described in
  The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations
  By Gene Kim, Jez Humble, Patrick Debois, and John Willis



DEVOPS MYTHS
===============================================================================
1 DevOps is Only for Startups       : Also used by Google, Amazon, Etsy, Netflix, ...
2 DevOps Replaces Agile             : Two are compatible
3 DevOps is incompatible with ITIL
  (Info Tech Infrastructure Lib)    : False, ITIL with highly automated processes
4 DevOps is incompatible with info
  security and compliance           : Integrate controls into each stage of process, not the end
5 DevOps Means Eliminating IT OPs   : IT just becomes more like development, engaged in product
                                      development, where the product is the platform that
                                      developers use to safely, quickly, and securely test, deploy,
 		                              and run their IT services in production
6 DevOps is Just "Infrastructure
  as code" or automation            : Yes, but more too. Culture of shared goals throughout IT value stream
7 DevOps is only for open source
  software                          : Success achieved with Microsoft.NET, COBOL, ...



OPS -> DEVOPS
===============================================================================
- Working together                  : Development, QA, IT Operations, and Info security (Infosec)
                                      all work together
- Goal                              : Fast flow of planned work into production while achieving world-class
                                      stability, reliability, availability, and security
- Value of software                 : "Every induestry and company that is not brining software to the core
                                       or their business will be disrupted"- Jeffrey Immelt, CEO of GE
- Technical Debt                    : Decisions that lead to problems that get increasingly more difficult
                                      to fix over time, continually reducing available options in future
- Ideal                             : Small teams of developers independently implement their features,
                                      validate their correctness in production-like environments, and have their
                                      code deployed to production quickly, safely, and securely
- Fast feedback loops               : Enables quick corrections throughout entire process
- Ownership                         : Everyone fully owns the quality of their work. Everyone builds automated
                                      testing into their daily work and uses peer review to gain confidence
                                      that problems are addressed long before they can impact customer
- DevOps companies relative to peers
  outperforming in areas            : Throughput metrics
                                      Code and change deployments (30x more frequent)
                                      Code and change deployment lead time (200x faster)
                                      Reliability metrics
                                      Production deployments (60x higher succes rate)
                                      Mean time to restore service (168x faster)
                                      Organization performance metrics
                                      Productivity, market share, and profitability goals
                                      Market capitalization grown (50% higher over three years)
- Developer productivity            : When number of developers increases, individual developer productivity
                                      can sharply decrease due to communication, integration, and testing overhead
                                      DevOps enables even big companies to allow small teams of developers to
                                      be incredibly efficient



THE THREE WAYS
===============================================================================
- Three principles
  1 Priciples of Flow               : Accelerate delivery of work from
                                      Development->Operations->Customers
  2 Principles of Feedback          : Create ever safer systems of work
  3 Principles of Continual
    Learning and Experimentation    : Foster high-trust culter and a scientific approach
                                      to organizational improvement risk-taking as part of daily work
- Lean Movement                     : Toyota Production System in 1980s
                                      Manufacturing lead time required to convert raw materials into finished goods
                                      was best way to predictor of quality, customer satisfaction, and employee
                                      happienss, and that one of the best predictors of short lead times was small
                                      batch sizes of work
                                      Lean principles focus on how to create value for the customer through systems
                                      creating constancy of purpose, embracing scientific thinking, creating flow,
                                      assuring quality at the source, leading with humility, and respecting
                                      every individual
- Agile Manifesto                   : 2001 by seventeen of the leading thinkers in software development
                                      Deliver working software frequently from a couple of weeks to a couple
                                      of months
                                      Emphasized small batch sizes, incremental releases instead of waterfall
- Continuous Delivery Movement      : Defined role of deployment pipeline to ensue that code and
                                      infrastructure are always in a deployable state
- Toyota Kata                       : Creating structure for the daily, habitual practice of improvement
                                      work, because daily practice is what improves outcomes
- Technology Value Stream           : Process required to convert a busines hypothesis into a technology
                                      enabled service that delivers value to the customer

                                      Ticket                Work               Work
                                      Created               Started            Completed
                                       |----------------------|-------------------|
                                       |<---------- Lead Time ------------------->|
                                                              |<- Process Time -->|
                                      Goal to minimize lead time, process time



THE PRINCIPLES OF FLOW
===============================================================================
- Make work visiple                 : Sprint planning boards, kanban boards
                                      Prevents duplication
                                      Allows for prioritization
                                      Work only doen when application running in production
- Limit work in progress (WIP)      : Very high cost of context switching
                                      Enforce WIP limits
                                      Stop starting, start finishing
- Reduce batch sizes                : The larger the change going into production, the more difficult
                                      the production errors are to diagnose and fix, and the longer
                                      they take to remediate
                                      Small batch sizes allow for quicker work and faster feedback
- Reduce number of handoffs         : Each handoff incurs an additional delay
                                      Even under the best circumstances, some knowledge is inevitably lost
                                      with each handoff
- Continually identify and elevate
  our constraints                   : Identify constraints to improve capacity
                                      Automate environment creation
                                      Automate code deployment
                                      Automate test setup and run in parallel
                                      Loosely couple architecture
- Eliminate hardships and waste
  in value stream                   : Lean = eliminate waste
  Type of waste
  * Partially done work             : Work in value stream that has not been completed.
                                      Eventually becomes obsolete
  * Extra processes                 : Any extra work (e.g. approval processes) that doesn't add value to
                                      the customer
  * Extra features                  : Gold planting that doesn't add value
  * Task switching                  : Time wasted in context switching
  * Waiting                         : Delays prevent customer from getting value
  * Motion                          : Moving product from one center to another
  * Defects                         : Incorrect, missing, unclear information, materials, or products
  * Nonstandard or manual work      : Rely on automation as much as possible
  * Heroics                         : No waking up at 2 AM to fix problems



THE PRINCIPLES OF FEEDBACK
===============================================================================
- Working safely within complex
  systems                           : Doning the same thing twice -> same result
                                      Perform work without fear, confident that any errors will be
                                      detected quickly, long before they cause catastrophic outcomes
                                      Problems are swarmed and solved, generating new knowledge
- See problems as they occur        : Constantly test design and operating assumptions
                                      Auto build, test
                                      Production telemetry to detect issues before customer
- Swarm and solve problems to build
  new knowledge                     : Toyota Andon cord. Whole assembly line stops
                                      and figure out what went wrong
                                      Swarming enables learning
- Keep pushing quality closer to
  the source                        : The effectiveness of approval processses decreases as we push decision
                                      making further away from where the work is performed
                                      Everyone in value stream to find and fix problems in area of control
                                      as part of daily work
                                      Peer review as propose changes to gain assurance that changes will operate
                                      as designed
- Enable optimizing for downstream
  work centers                      : Most importand customer is our next step downstream



THE PRINCIPLES OF CONTINUAL LEARNING AND EXPERIMENTATION
===============================================================================
- Enabling organization learning
  and a safety culture              : Actively seeking and sharing information to better enable
                                      the organization to achieve its mission
                                      When accidents and failures occur, don't blame, look to see
                                      how mistake can be prevented in the future
- Insitutionalize improvement of
  daily work                        : Be willing to constantly improve internal processes
- Transform local discoveries
  into global improvements          : Document why failures occured to let everyone in organization review
- Inject resilience patterns into
  daily work                        : Constantly expirimenting to improve processes
- Leaders reinforce a learning
  culture                           : Leaders embrace learning



WHERE TO START
===============================================================================
- Specific, quick                   : Focus on specific area of business to experiment and learn.
                                      Goal to demonstrate early wins to give everyone confidence that
                                      these improvements could be replicated to other areas of org
- Greenfield vs. Brownfield         : Greenfield - develop on undeveloped land
                                      Brownfield - Build on land that was previously used
                                                   Often come with significant amount of technical debt
- Sympathetic and innovative groups : Spend time working with groups less averse to change
                                      Show successes to entire organization
- Expanding accross organization    : 1 Find Innovators and Early Adopters
                                      2 Build critical mass and silent majority
                                      3 Identify the holdouts



CONWAY'S LAW
===============================================================================
- Conway's Law                      : Organizations which design systems are constrained to produce designs
                                      which are copies of the comminication structures of these organizations
                                      The larger the organization, the more pronounced
- Team organization                 : Team organization has a profound imact on software produced,
                                      as well as our resulting architectural and production outcomes
- Organizational Archetypes
  * Functional-Orientation          : Optimize for expertise, division of labor, or reducing cost
                                      Tend to result in hierarchical organizations
  * Market-Orientation              : Optimize for responding quickly to customer needs
                                      Tend to result in flatter organizations
  * Matrix-Orientation              : Combine functional and market
- Devops                            : Enable market-orientation, optimizing for speed. Small teams
                                      working safely and independently delivering value to the customer
- Testing, Ops, Security everyone's
  job
- Enable every team member to be a
  generalist                        : When departments specialize, it causes siloization
                                      Full Stack Engineer (Application, Databases, Operating Systems, Network, Cloud)
                                      Foster and develop a growth mindset
- Loosely-coupled architectures     : Service-Oriented Architectures (SOAs)
                                      Services are independently testable and deployable
                                      Decoupled from other services and from shared databases
                                      Developers should be able to understand and update the code
                                      of a service without knowing anything about the internals of its
                                      peer services
- Keep team sizes small             : Ensures clear, shared understanding of the system
                                      Limits growth rate of the product or service being worked on
                                      Decentralizes poer and enables autonomy
                                      Allows employees to gain leadership experience



TECHNICAL PRACTICES OF FLOW
===============================================================================
- Continuous delivery practices     : Automated deployment pipeline
                                      Tests validating constantly in deployable state
                                      Developers integrate code daily into trunk
                                      Architecting environments and code to enable low-risk releases
- Deployment pipeline ---------------
  * Production-like environments    : At every stage in value stream
                                      Recreate from version control
  * Enable on demand creation of
    production-like environments    : Devs run production like environments on own workstations
                                      Common build mechanism to build environments
                                      Technologies: EC2, Pupet, Checf, Ansible, Salt, Vagrant, Docker
                                      Allows developers to fix bugs early in project
  * Single repository source of
    truth                           : Version control becomes single repository source of truth
                                      Contents: Appplication code, deps, environment creation tools
                                                automated tests, packing deployment tools, database
                                                migration, project artifacts, cloud files
                                      See all changes, easily roll back to safe known state
  * Make infrastructure easier to
    rebuild that to repair          : Rebuild over repair
                                      Enables horizontal scaling
                                      Disallow manual changes to infrastructure
                                      Developers stay running on most current environment
  * Development done == running
    in production                   : Only accept development work when it can be successfully built,
                                      deployed, and confirmed that it runs as expected in
                                      production-like environment
- Fast, Reliable Automated Tests ----
  * Continuously, build, test, and
    integrate code and envs         : Build deployment pipeline, which performs integration of code and
                                      environments and triggers a series of tests on each commit
                                      Goal is to give everyone in the value stream, especially developers,
                                      the fastest feedback possible
  * Technologies                    : Jenkins, ThoughWorks Go, Concourse, Bamboo, Microsoft Team Foundation
                                      Server, TeamCity, Gitlab CI, Travis CI, Snap
  * Continuous Integration          : Comprehensive and reliable set of automated tests
                                      Culture that stops the entire production line of failure
                                      Developers working in small batch sizes on trunk
  * Build a fast and reliable
    automated validation test suite : Slow and periodic feedback kills, especially for large development teams
                                      Unit Tests       : Single method, class or function in isolation
                                      Acceptance Tests : High level of functionality
                                      Integration tests: Interacts correctly with production environments
                                      Measure and make visible test coverage
                                      Try to keep under 10 minutes. Run in parallel
                                      Find bugs as quickly in test process
                                      Errors should be found in fastest category.
                                      Whenever error found, create unit test to reproduce and verify fix
                                      Test-Driven development (TDD) write tests before code
                                      Kill false positives
                                      Start with small number of tests, grow over time
                                      Integrate performance tests early and often
                                      Verify environment correctness
  * Ideal pyramid                   : Want most tests to be automated unit tests
                                                 -----------------
                                                /                 \
                                               / Manual-Based      \
                                              /  Automated GUI      \
                                             /   Automated API       \
                                            /    Automated Integration\
                                           /     Automated Component   \
                                          /      Automated Unit         \
                                          --------------------------------
  * Pull andon cord with pipeline
    breaks                          : No false positive tests
                                      Critical test problems are dealt with when they are hit
- Continous Integration -------------
  * Small batch development         : Long-lived branch merges cause significant chaos when introduced
                                      Optimize for team over developer
                                      No branches, everyone works in common area
                                      Commits are simple, but each commit can break the entire project
                                      and bring progress to a screeching halt
  * Leverage Trunk Based Development: Integrating branches becomes exponentially harder as we increase
                                      the number of branches
                                      Everyone uses same branch that is tested and verified on each commit
                                      Daily checkins
                                      At the end of each development interval, have integrated, tested, working,
                                      and potentailly shippable code, demonstrated in production-like environment
                                      created from trunk using a one-click process
- Automate, Enable Low Risk Releases-
  * Automate deployment process     : Deploy the same way to every environment
                                      Smoke test deployments
                                      Ensure consistent environments are maintained
                                      Build, Test, Deploy
  * Decouple deployments from
    releases                        : Deployment = installation of a specified version of software to a given env
                                      Release    = making a feature (or set of features) available to all
                                                   customers or a segment of customers
                                                   Shouldn't require code change
                                      Make changes to release with environment variables
  * Blue-Green Deployment pattern   : Two production environments: blue and green
                                      At any given time only one is serving customer traffic
                                      When everything is working as designed move to blue
                                      If something bad happens, roll back to green
  * Canary release pattern          : Release to a subset of all production infrastructure.
                                      Gradually increase percentage as feature proves to work correctly.
                                      Coal miners brought caged canaries into mines to provide
                                      early detection of toxic carbon monoxide levels
                                      Canaries would die before human, alerting evacuation
  * Feature toggles                 : Selectively enable and disable features without requiring
                                      a production code deployment
                                      Roll back easily
                                      Gracefully degrade performance (disable CPU-intensive tasks)
                                      Increase resilience through service-oriented architecture
  * Dark launches                   : Deploy all functionality into production and then perform
                                      testing of that functionality while it is invisible to users
- Architect Low Risk Releases ------: Evolutionary architecture. Keep migrating architecture we have
                                      to architecture we need
  * strangler application           : Instead of ripping out and replacing, put existing functionality
                                      behind an api and avoid making changes to it
                                      All new functionality is then implemented in the new services that use
                                      the new desired architecture
                                      Overly tight architectures cause fear, make work unsafe
  * Architecture for productivity,
    testability, and safety         : Service-oriented architecture
                                      Allows small teams to work on smaller and simpler units of development
                                      that can be deployed independently
                                      Monoliths eventually fall apart
                                      No one perfect architecture for all products and all scales



TECHNICAL PRACTICES OF FEEDBACK
===============================================================================
- Create Telemetry -----------------: In operations things go wrong. Small changes may result
                                      in unexpected failures, including outages and global failures
                                      Best organizations were much better at diagnosing and fixing service
                                      incidents
  * Technologies                    : Ganglia, Graphite, HP openView, IBM Tivoli, StatsD, Prometheus
                                      Grafana
  * Centralized telemetry infra     : Data collection at business logic, application, and environments, layer
                                      Events router responsible for storing events and metrics
                                      Picture
                                      ---------------------------------------------------------------------
                                      | Business Logic---->-|Events  |                                    |
                                      | Application------->-|Logs    |---> Event Router --> Destinations  |
                                      | Operating System-->-|Metrics |                        Store       |
                                      |                        ^                              Graph       |
                                      |                        |                              Alert       |
                                      | Monitoring Framework----                                          |
                                      ---------------------------------------------------------------------

  * Application logging telemetry   : DEBUG - Anything happening in the program
                                      INFO  - User-driven or system specific
                                      WARN  - Conditions could potentially become an error (DB call taking long)
                                      ERROR - Error conditions (API call failures)
                                      FATAL - Must terminate (Network daemon didn't bind to socket)
  * Events                          : Authentication/authorizations (including logoff)
                                      System and data access
                                      System and application changes
                                      Data changes: adding, deleting, editing
                                      Invalid Input (possibly malicious injection, threats)
                                      Resources (RAM, CPU, bandwidth)
                                      Health and availability
                                      Startups and shutdowns
                                      Faults and errors
                                      Circuit breaker trips
                                      Delays
                                      Backup success/failure
  * Telemetry for problem solving   : Use scientific process to solve problems
  * Information radiators           : Generic term for any of a number of handwritten, drawn, printed, or
                                      electronic displays which team places in a highly visible location,
                                      so all team members as well as passers-by can see the latest
                                      information at a glance:
                                      automated tests, velocity, incident reports, continuous integration
                                      status,...
                                      Service status page
  * Application and business metrics: Use for refining the customer acquisition funnel
                                      Product link clicks, shopping cart clicks, completed orders
  * Infrastructure metrics          : Telemetry on all environments (dev, stage, production)
                                      Find exactly when something goes wrong
                                      Overlay metrics, ex. show when production deployments happenend in relation
                                      to outages
- Telemetry to anticipate, achieve --
  * Outlier detection               : Use means and standard deviation to detect outliers
  * Alert                           : Alert on undesired telemetry
  * Anticipate                      : Anticipate load based on past usage
- Deployment feedback ---------------
  * Safe deployments                : Catch errors in deployment pipeline before they reach production
                                      fix forward: make changes to fix code
                                      roll back  : roll back to previous working state
  * Devs share feedback             : Give devs same pager as ops so they feel the same pain
                                      Feature is not done until it is running in production
- Hypothesis Driven Development ----:"The most inefficient way to test a business model or product idea is to
                                      build the complete product to see whether the predicted demand
                                      actually exists" - Jez Humble
                                      Build the cheapest and fastest experiments possible to validate user
                                      research
                                      Faster one can experiment, iterate, and integrate feedback into product,
                                      the faster one can learn and out-experiment competition
  * A/B Testing                     : Pioneered by direct response marketing
                                      Ex. Test two advertising campaigns to see which one generates most responses
                                      A/B tests are also known as controlled online experiments
                                      Enable and disable features for different customers
- Create review processes ----------: Reviewing code increases the quality of work
  * Danger of overly controlling    : People closest to the problem know the most
                                      Creating approval steps from people located further and further away
                                      from the work may actually kill the likelihood of success
                                      Peer Review >> Change Approval
  * Require code review             : Require engineers to get peer review of changes
                                      Critical for engineers to work in small incremental steps rather
                                      than long-lived feature branches
                                      Need a culture that values reviewing as part of daily work
  * Code review forms               : Pair programming
                                      Over the shoulder
                                      Email pass-around
                                      Tools-assisted code review
  * Cut bureaucratic processes      : Relentlessly reduce the effort required for engineers to perform work
                                      and deliver it to the customer



TECHNICAL PRACTICES OF CONTINUAL LEARNING AND EXPERIMENTATION
===============================================================================
- Enable daily learning ------------: Better at self-diagnostics, self-improvement
                                      Skilled at detecting problems, solving them, and multiplying the effects
                                      Deliberately create failures to accelerate learning
  * Blameless post-mortem           : Meeting after accident
                                      Construct timeline of events from multiple perspectives
                                      Encourage those who made failures to explain mistacts
                                      Propose countermeasures
                                    : Stakeholders present
                                      Poeple involved in creation of problem
                                      People who identified problem
                                      Poeple who responded
                                      People who diagnosed
                                      People who were affected
                                      Anyone else interested
                                    : Culture of information without fear of punishment
  * Publish post-mortems widely     : Centralized location where entire organization can learn from
                                      the incident
  * Amplify weak failure signals
  * Encourage calculated risks      : Everyone should feel comforable with and responsible for surfacing
                                      failures
                                      Enable risk taking
  * Inject production failures      : Netflix's chaos Monkey took down production servers
                                      Allowed them to prematurely learn about the system and gracefully fail
                                      "A service is not really tested until it is broken in production"
- Local discovery -> Globaly Improve: Share local improvements globally
  * Chat rooms close to work        : Put automation tools in the middle of the conversation
                                      Culture of transparency and trus
                                      Chat rooms are the water coolers for remote developers
  * Codify processes                : Encode manual processes into automated processes
  * Single shared source repo       : Firm-wide, shared source code repository is one of the most powerful
                                      mechanisms used to integrate local discoveries across the entire org
                                      Don't reinvent the wheel!
                                    : What to store:
                                      Configuration standards
                                      Deployment tools
                                      Testing standards, tools
                                      Deployment pipeline tools
                                      Monitoring and analysis tools
                                      Tutorials and standards
                                    : Write one tool and have it be available to all projects
  * Document through test           : Test suites are living up-to-date spec of system
                                      Allow only one version in production
  * Consolodate technologies        : Remove technologies that:
                                      - impede or slow down the flow of work
                                      - disproportionately create high levels of unplanned work
                                      - disproportionately create large numbers of support requests
                                      - are inconsistent with architectural outcomes
                                    : Three languages: compiled, scripting, UI
- Reserver time for learning --------
  * Pay down technical debt         : Schedule improvement blitzes (work off technical debt)
                                      Solve daily workarounds
  * Continual learning              : Enable everyone to teach and learn
                                      Lots to learn: development techniques, rituals, and skills,
                                      version control, auto test, deploy pipes, config management
                                    : Attend conferences, listen and give talks
  * Consulting coaches              : Internal/external coaches to learn new practices



TECHNICAL PRACTICES OF INTEGRATING INFOMATION SECURITY, CHANGE MANAGEMENT, AND COMPLIANCE
===============================================================================
- Information security for everyone-
  * Security issues                : Track with same issue tracker used by devs
  * Security controls in repo      : Code libraries and recommended configurations
                                     Secret management
                                     OS packages and builds
  * Application security           : Static analysis: Brakeman, Code Climate
                                     Dynameic analysis: Nmap, Metasploit, Gauntlt, Gherkin
                                     Dependency scanning: Gemnasium, OWASP
                                     License checking: license_finder
                                     Code signing: keybase.io, gpg
  * Selecting third party software : Choose trusted third parties
  * Telemetry monitoring           : OS changes
                                     Security group changes
                                     Changes to configurations
                                     Cloud infrastructure changes
                                     XSS attempts
                                     SQL injection attempts
                                     Web server errors
- Protect deployment pipeline ------
  * Methods                        : Hardening continuous integration servers
                                     Review all changes put in version control
                                     Detect suspicious API calls from tests.. quarantine and go to code review
                                     Ensure CI process runs in isolated container or VM
                                     Version control credentials by CI are read only
  * No separation of duty          : Avoid using separation of duty as a control
                                     Choose pair programming and code review











