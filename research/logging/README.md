# Logging Documentation

Programmatic logging is used to record events and errors in the course of execution that would be
of interest to an application developer.

Relevant URLs:
[Quora](https://www.quora.com/What-is-Python-Logging-And-Why-Is-It-Useful),
[10 Commandments](http://www.masterzen.fr/2013/01/13/the-10-commandments-of-logging/),
[Loggly Best Practices](https://www.loggly.com/blog/30-best-practices-logging-scale/),
[Loom Best Practices](https://www.loomsystems.com/blog/single-post/2017/01/26/9-logging-best-practices-based-on-hands-on-experience)

## The 10 Commandments of Logging

Knowing how and what to log is one of the hardest task a software engineer will have to solve.
This is because its very hard to know what information you'll need during troubleshooting.

### 1. Don't write the log by yourself

Don't implement the log library or handle log rotation by yourself. Use a standard library or system
API call. This allows your system application to play nicely with the other system components.

### 2. Log at the proper level

- __TRACE level__: used during development to track bugs, but never committed to version control

- __DEBUG level__: log at this level about anything that happens in the program. This is mostly used
  during debugging. Should not be turned on in production.

- __INFO level__: log at this level all actions that are user-driven, or system specific. I.e. regularly
  scheduled operations

- __NOTICE level__: log all notable events that are not considered an error. Production runs here

- __WARN level__: log at this level all events that could potentially become an error. For instance
  if one database call took more than a predefined time, or if a in memory cache is near capacity

- __ERROR level__: log every error condition at this level. That can be API calls that return errors
  or internal error conditions

- __FATAL level__: Use this very scarcely, this shouldn't happen a lot in a real program. Usually logging
  at this level signifies the end of the program. For instance, if a network daemon can't bind to a network
  socket, log at this level and exit

### 3. Honor the log category

Leverage hierarchical logging libraries to log only a specific subset at a higher level.
For example, in Java, turn on debug logging for `com.daysofwonder.ranking.ELORankingComputation`.

### 4. Write Meaningful Logs

There's nothing worse than cryptic log entries assuming you have a deep understanding of the
program internals.

Always anticipate that there are emergency situations where the only thing you have is the log file,
from which you have to understand what happened.

When a developer writes a log message, it is in the context of the code in which the log directive
is to be inserted. In those conditions we tend to write messages that infer the current context.
Unfortunately, when reading the log itself this context is absent, and those messages might not be
understandable.

Do not log messages that depend on previous messages. The previous messages might not appear if they
are logged in a different category or level.

### 5. Write logs in English

- English means messages will be logged with ASCII characters.
- If your program is to be used by the most and you don't have resources for full localization, then
  English is probably the best alternative
- Use error codes to internationalize messages. If you were to design such scheme, you can adopt this
  scheme: APP-S-CODE or APP-S-SUB-CODE, with respectively:
    * APP: you application name on 3 letters
    * S: severity on 1 letter (ie D: debug, I: info...)
    * SUB: the sub part of the application this code pertains to
    * CODE: a numeric code specific to the error in questino

### 6. Log with context

Messages are much more valuable with context. For example

```
Transaction 2346432 failed: cc number checksum incorrect
```

### 7. Log in machine parseable format

Log entries are really good for human, but very poor for machines.

Put data context in a machine parsable format in your log entry.

```
2013-01-12 17:49:37,656 [T1] INFO  c.d.g.UserRequest  User plays {'user':1334563, 'card':'4 of spade', 'game':23425656}
```

### 8. Don't log too much or too little

There is a right balance for the amount of log.

Too much log and it is hard to get value from it. Too little log and your risk not being able to troubleshoot
problems.

One way to overcome this issue is to log as much during development as possible. Then when the application
enters production, perform an analysis of the produced logs and reduce or increase the logging statement
accordingly to the problems found.

Refactor logging statements as much as you refactor the code.

### 9. Think to the reader

Why add logging to an application?

Who will be reading it?
- An end-user trying to troubleshoot herself
- A system-administrator or operation engineer trouble shootin a production issue
- A developer either for debugging during development or solving a production issue

### 10. Don't only use logs for troubleshooting

Can also use logs for:

- Auditing: this is sometimes a business requirement. The idea is to capture significant events
  that matter to the management or legal people. For example, who signed-in, who edited what, etc..

- Profiling: Logs are a good tool for profiling sections of a program, for instance, start and ends
  of operations.

- Statistics: If you log each time a certain event occurs you can then compute interesting statistics
  about the running program (or the user behaviors). It's also possible to hook this to an alert
  system that can detect too many errors in a row.




