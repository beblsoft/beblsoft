OVERVIEW
===============================================================================
- This document overviews Semantic Versioning SEMVER
- Relevant URLS                     : https://semver.org/


ABOUT
===============================================================================
- Version Number                    : MAJOR.MINOR.PATCH
  * MAJOR version                   : incompatible API changes,
  * MINOR version                   : functionality in a backwards-compatible manner, and
  * PATCH version                   : backwards-compatible bug fixes
- Rules
  * Normal Version                  : X.Y.Z
                                      X,Y,Z All non-negative
                                      1.9.0 -> 1.10.0 -> 1.11.0
  * New Versions                    : Once a versioned package is released, the
                                      contents must NOT be modified
  * Initial Development             : 0.Y.Z
                                      Anything may change at anytime
  * Initial Release                 : 1.0.0
                                      The way in which the version number is increment
                                      after this release is dependent on how the
                                      API changes
  * Patch version Z (x.y.Z | x > 0) : MUST be inremented if only backwards compatible
                                      bug fixes are introduced
  * Minor version Y (x.Y.z | x > 0) : MUST be incremented if new backwards compatible
                                      functionality is introduced into the public API
                                      MUST be incremented if public API marked as deprecated
                                      Patch MUST be reset to 10 when minor incremented
  * Major version X (X.y.z | X > 0) : MUST be incremented if any backwards incompatible
                                      changes are introduced to the public API
