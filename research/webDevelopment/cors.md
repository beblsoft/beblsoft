# Cross-Origin Resource Sharing (CORS) Documentation

Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser
to let a web application running at one origin (domain) have permission to access selected resources
from a server at a different origin. A web application executes a __cross-origin__ HTTP request when it
requests a resource that has a different origin (domain, protocol, or port) than its own origin.

An example of a cross-origin request: The frontend JavaScript for a web application served from
`http://domain-a.com` uses `XMLHttpRequest` to make a request for `http://api.domain-b.com/data.json`.

For security reasons, browsers restrict cross-origin HTTP requests initiated from within scripts.
For example, `XMLHttpRequest` and the Fetch API follow the same-origin policy. This means that a
web application using those APIs can only request HTTP resources from the same origin the application
was loaded from , unless the response from the other origin includes the right CORS headers.

![Cors Image](https://mdn.mozillademos.org/files/14295/CORS_principle.png)

The CORS mechanism supports securt cross-origin requests and data transfers between browsers and web
servers. Modern browsers use CORS in an API container such as `XMLHttpRequest` or Fetch help mitigate
the risks of cross-origin HTTP requests.

Relevant URLs:
[Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

## What requests use CORS?

The cross-origin sharing standard is used to enable cross-site HTTP requests for:

- Invocations of the XMLHttpRequest or Fetch APIs in a cross-site manner, as discussed above.
- Web Fonts (for cross-domain font usage in @font-face within CSS), so that servers can deploy
  TrueType fonts that can only be cross-site loaded and used by web sites that are permitted to do so.
- WebGL textures.
- Images/video frames drawn to a canvas using drawImage().

## Functional Overview

The Cross-Origin Resource Sharing standard works by adding new HTTP headers that allow servers to
describe the set of origins that are permitted to read that information using a web browser.
Additionally, for HTTP request methods that can cause side-effects on server's data (in particular,
for HTTP methods other than `GET`, or for `POST` usage with certain MIME types), the specification mandates
that browsers "preflight" the request, soliciting supported methods from the server with an HTTP
`OPTIONS` request method, and then, upon "approval" from the server, sending the actual request with
the actual HTTP request method. Servers can also notify clients whether "credentials" (including
Cookies and HTTP Authentication data) should be sent with requests.

CORS failures result in errors, but for security reasons, specifics about what went wrong are not
available to JavaScript code. All the code knows is that an error occurred. The only way to determine
what specifically went wrong is to look at the browser's console for details.

## Example Access Control Scenarios

### Simple Requests

Some requests don't trigger CORS preflight. Those are called "simple requests" in this article. A
request that doesn't trigger CORS preflight - a so-called "simple request" - is one that meets all the
following conditions:

Allowed methods are:

- GET
- HEAD
- POST

Apart from the headers set automatically by the user agent, only headers allowed are:

- Accept
- Accept-Language
- Content-Language
- Content-Type (but note the additional requirements below)
- DPR
- Downlink
- Save-Dat
a- Viewport-Width
- Width

Only allowed values for `Content-Type` header are:

- application/x-www-form-urlencoded
- multipart/form-data
- text/plain

No event listeners are registered on XMLHttpRequestUpload

No ReadableStream object is used in the request.

Take the following code on domain `http://foo.example`:

```js
const invocation = new XMLHttpRequest();
const url = 'http://bar.other/resources/public-data/';

function callOtherDomain() {
  if (invocation) {
    invocation.open('GET', url, true);
    invocation.onreadystatechange = handler;
    invocation.send();
  }
}
```

This will lead to a simple exchange between the client and the server, using CORS headers to handle
the privileges.

![Simple Request Image](https://mdn.mozillademos.org/files/14293/simple_req.png)

### Preflighted Requests

Unlike "simple requests" (discussed above), "preflighted" requests first send an HTTP request by the
`OPTIONS` method to the resource on the other domain, in order to determine whether the actual request
is safe to send. Cross-site request are preflighted like this since they may have implications to user
data.

In particular, a request is preflighted if __any of the following conditions__ is true:

If the request uses any of the following methods:

- `PUT`
- `DELETE`
- `CONNECT`
- `OPTIONS`
- `TRACE`
- `PATCH`

Or if, apart from the headers set automatically by the user agent (for example, Connection, User-Agent, or any of the OTHER header with a name defined in the Fetch spec as a “forbidden header name”), the request includes any headers other than those which the Fetch spec defines as being a “CORS-safelisted request-header”, which are the following:

- `Accept`
- `Accept-Language`
- `Content-Language`
- `Content-Type` (but note the additional requirements below)
- `DPR`
- `Downlink`
- `Save-Data`
- `Viewport-Width`
- `Width`

Or if the Content-Type header has a value OTHER THAN the following:

- `application/x-www-form-urlencoded`
- `multipart/form-data`
- `text/plain`

Or if one or more event listeners are registered on an XMLHttpRequestUpload object used in the request.

Or if a ReadableStream object is used in the request.

The following is an example of a request that will be preflighted:
```js
const invocation = new XMLHttpRequest();
const url = 'http://bar.other/resources/post-here/';
const body = '<?xml version="1.0"?><person><name>Arun</name></person>';

function callOtherDomain() {
  if (invocation) {
    invocation.open('POST', url, true);
    invocation.setRequestHeader('X-PINGOTHER', 'pingpong');
    invocation.setRequestHeader('Content-Type', 'application/xml');
    invocation.onreadystatechange = handler;
    invocation.send(body);
  }
}
```

This will lead to preflight requests and the following exchange:

![Preflight Request Image](https://mdn.mozillademos.org/files/16753/preflight_correct.png)

The first request is an `OPTIONS` request to ensure that the server allows the desired method.

### Preflighted Requests and Redirects

Not all browsers currently support redirects after a preflighted request.

### Requests with credentials

CORS allows "credentialed" requests that are aware of HTTP cookies and HTTP Authentication information.
By default, in cross-site XMLHttpRequest or Fetch invocations, browsers will __not__ send credentials.
A specific flag has to be set on the XMLHttpRequest object or the Request constructor when it is
invoked.

In Javascript this might look like:
```js
const invocation = new XMLHttpRequest();
const url = 'http://bar.other/resources/credentialed-content/';

function callOtherDomain() {
  if (invocation) {
    invocation.open('GET', url, true);
    invocation.withCredentials = true;
    invocation.onreadystatechange = handler;
    invocation.send();
  }
}
```

By default, the invocation is made without Cookies. Since this is a simple `GET` request, it is not
preflighted, but the browser will __reject__ any response that does not have the
`Access-Control-Allow-Credentials: true` header, and __not__ make the response available to the invoking
web content.

Here is the exchange between client and server:

![Credentialed Request Image](https://mdn.mozillademos.org/files/14291/cred-req.png)

#### Credentialed Requests and Wildcards

When responding to a credentialed request, the server __must__ specify an origin in the value of
'Access-Control-Allow-Origin' header, instead of specifying the "*" wildcard.

#### Third-party Cookies

Note that cookies set in CORS responses are subject to normal third-party cookie policies. In the
example above, the plage is loaded from `foo.example`, but the cookie is sent by `bar.other`, and
would thus not be saved if the user whas configured their browser to reject all third-party cookies.

## HTTP Response Headers

Section lists HTTP response headers that servers send back for access control requests as defined by
the Cross-Origin Resource Sharing Specification.

### Access-Control-Allow-Origin

Specify single origin or all origins that can access endpoint.

```text
Access-Control-Allow-Origin: <origin> | *
```

### Access-Control-Expose-Headers

Lets server whitelist headers that browsers are allowed to access.

```text
Access-Control-Expose-Headers: <field-name>[, <field-name>]*
```

### Access-Control-Allow-Credentials

Indicates whether or not the response to the request can be exposed when the `credentials` flag is
true. When used as part of a response to a preflight request, this indicates whether or not the actual
request can be made using credentials.

```text
Access-Control-Allow-Credentials: true
```

### Access-Control-Allow-Methods

Specifies the method or methods allowed when accessing the resource. This is used in response to a
preflight request.

```text
Access-Control-Allow-Methods: <method>[, <method>]*
```

### Access-Control-Allow-Headers

Used in response to a preflight request to indicate which HTTP headers can be used when making the
actual request.

```text
Access-Control-Allow-Headers: <field-name>[, <field-name>]*
```

## HTTP Request Headers

This section lists headers that clients may use when issuing HTTP requests in order to make use of the
cross-origin sharing feature. Note that these headers are set for you when making invocations to servers.
Developers using cross-site `CMLHttpRequest` capability do not have to set any cross-origin sharing request
headers programmatically.

### Origin

Indicates the origin of the cross-site access request or preflight request.

```text
Origin: <origin>
```

### Access-Control-Request-Method

Used when issuing a preflight request to let the server know what HTTP method will be used when the
actual request is made.

```text
Access-Control-Request-Method: <method>
```

### Access-Control-Request-Headers

Used when issuing a preflight request to let the server know what HTTP headers will be used when the
actual request is made.

```text
Access-Control-Request-Headers: <field-name>[, <field-name>]*
```


