# Website Cookies Documentation

An HTTP cookie (web cookie, browser cookie) is a small piece of data that a server sends to the user's
web browser. The browser may store it and send it back with the next request to the same server. Typically,
it's used to tell if two requests came from the same browser - keeping a user logged-in, for example.
It remebers stateful information for the stateless HTTP protocol.

Cookies are used for three main purposes:
1. Session management: Logins, shopping carts, game scores, or anything else the server should remember
2. Personalization: User preferences, themes, and other settings
3. Tracking: recording and analyzing user behavior

Cookies were once used for general client-side storage. While this was legitimate when they were the only
way to store data on the client, it is recommended nowadays to prefer modern storage APIs. Cookies are
sent with every request, so they can worsen performance (especially for mobile data connections). Modern
APIs for client storage are the Web storage API (localStorage and sessionStorage) and IndexedDB.

Relevant URLs:
[Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies),
[Who is hosting this blog](https://www.whoishostingthis.com/resources/cookies-guide/)

## Creating Cookies

When receiving an HTTP request, a server can send a `Set-Cookie` header with the response. The cookie
is usually stored by the browser, and then the cookie is sent with requests made to the same server
inside a `Cookie` HTTP header. An expiration date or duration can be specified, after which the cookie
is no longer sent. Additionally, restrictions to a specific domain and path can be set, limiting where
the cookie is sent.

```text
Set-Cookie: <cookie-name>=<cookie-value>
```

This header from the server tells the client to store a cookie.

For example:
```text
HTTP/2.0 200 OK
Content-type: text/html
Set-Cookie: yummy_cookie=choco
Set-Cookie: tasty_cookie=strawberry

[page content]
```

Now, with every new request to the server, the browser will send back all previously stored cookies
to the server using the `Cookie` header.

```text
GET /sample_page.html HTTP/2.0
Host: www.example.org
Cookie: yummy_cookie=choco; tasty_cookie=strawberry
```

### Session Cookies

The cookie created above is a _session cookie_: it is deleted when the client shuts down, because
it didn't specify an `Expires` or `Max-Age` directive. However, web browsers may use __session restoring__,
which makes most session cookies permanent, as if the browser was never closed.

### Permanent Cookies

Instead of expiring when the client closes, _permanent cookies_ expire at a specific date (`Expires`)
or after a specific length of time (`Max-Age`). When an expiry date is set, the time and date set is
relative to the client the cookie is being set on, not the server.

```text
Set-Cookie: id=a3fWa; Expires=Wed, 21 Oct 2015 07:28:00 GMT;
```

### Secure and HttpOnly

A _secure cookie_ is only sent to the server with an encrypted request over the HTTPS protocol. Even
with `Secure`, sensitive information should _never_ be stored in cookies, as they are inherently
insecure and this flag can't offer real protection.

To help mitigate cross-site scripting (XSS) attacks, `HttpOnly` cookies are inaccessible to JavaScript's
`Document.cookie` API; they are only sent to the server. For example, cookies that persist server-side
sessions don't need to be available to JavaScript, and `HttpOnly` flag should be set.

```text
Set-Cookie: id=a3fWa; Expires=Wed, 21 Oct 2015 07:28:00 GMT; Secure; HttpOnly
```

### Cookie Scope

The `Domain` and `Path` directives define the _scope_ of the cookie: what URLs the cookies should be
sent to.

`Domain` specifies allowed hosts to receive the cookie. If unspecified, it defaults to the host of the
current document location, __excluding subdomains__. If `Domain` is specified, then subdomains are
always included.

For example, if `Domain=mozilla.org` is set, then cookies are included on subdomains like
`developer.mozilla.org`.

`Path` indicates a URL path that must exist in the requested URL in order to send the `Cookie` header.

For example. if `Path=/docs` is set, these paths will match:

- `/docs`
- `/docs/Web`
- `/docs/Web/HTTP`

### SameSite Cookies

`SameSite` cookies let servers require that a cookie shouldn't be sent with cross-site (where Site
is defined by the registrable domain) requests, which provides some protection against cross-site
request forgery attacks (CSRF).

`SameSite` cookies are relatively new and supported by all major browsers

```text
Set-Cookie: key=value; SameSite=Strict
```

The same-site attribute can have two values (case-insensitive):
- `Strict`: If a same site cookie has this attribute, the browser will only send cookies if the request
  originated from the website that set the cookie. If the request originated from a different URL
  than the URL of the current location, none of the cookies tagged with the `Strict` attribute will
  be included

- `Lax`: If the attribute is set to Lax, same-site cookies are withheld on cross-site subrequests,
  such as calls to load images or frames, but will be sent when the user navigatges to the URL from
  an external site, for example, by following a link.

### Cookie prefixes

- `__Host-`: If a cookie name has this prefix, it will only be accepted in a `Set-Cookie` directive if
  it is marked Secure, does not include a Domain attribute and was sent from a secure origin. In this
  way, these cookies can be seen as "domain-locked".

- `__Secure-`: If a cookie name has this prefix, it will only be accepted in a `Set-Cookie` directive
  if it is marked `Secure` and was sent from a secure origin. This is weaker than the `__Host-` prefix.

### Javascript access using `Document.cookie`

New cookies can also be created with JavaScript using the `Document.cookie` property, and if the
`HttpOnly` flag is not set, existing cookies can be accessed from JavaScript as well.

```js
document.cookie = "yummy_cookie=choco";
document.cookie = "tasty_cookie=strawberry";
console.log(document.cookie);
// logs "yummy_cookie=choco; tasty_cookie=strawberry"
```

Cookies created via JavaScript cannot include the `HttpOnly` flag.

## Security

Information should be stored in cookies with the understanding that all cookie values will be visible
to and can be changed by the end-user. Depending on the application, it may be desireable to use an
opaque identifier which is looked-up server-side or investigate alternative authentication/confidentiality
mechanisms such as JSON Web Tokens.

### Cross-site scripting (XSS)

A users visits a malicious website that recieves a cookie that contains a script payload targeting
a different website. The malicious cookie is discuised to look like it originated from the targeted
website. When the user visits the targeted site, the malicious cookie, including the script payload,
is sent to the server hosting the targeted site.

### Session Fixation

A user receives a malicious cookie that contains the cookie issuer's session ID. When the user attempts
to log into a targeted domain, the issuer's session ID is logged in instead of the user's session ID.

### Cross site request forgery attack (XSRF)

A user visits a legitimate site and receives a legitimate cookie. The user then visits a malicious
site that instructs the user's browser to perform some action targeting the legitimate site. The
legitimate site receives the request along with the the legitimate cookie and performs the action since
it appears to be initiated by a legitimate user.

### Cookie tossing attack

A user visits a malicious site that provides a cookie designed to look like it originated from the
subdomain of a targeted site, such as _http://subdomain.example.com_. When the user visits the targeted
site, _http://subdomain.example.com_, the subdomain cookie is sent along with any legitimate cookies.
If the subdomain cookie is interpreted first, the data in that cookie will overrule the data contained
in any subsequent legitimate cookies.

### Protecting against cookie fraud

Keep your browser up to date.

Avoid questionable sites.

## Tracking and Privacy

### Third-party Cookies

Cookies have a domain associated to them. If this domain is the same as the domain of the page you
are on, the cookies is said to be a _first-party cookie_. If the domain is different, it is said to be
a _third-party cookie_. While first-party cookies are sent only to the server setting them, a web
page may contain images or other components stored on servers in other domains (like ad banners).
Cookies that are sent through these third-party components are called third-party cookies and are mainly
used for advertisiing and tracking across the web.

### Do-Not-Track

There are no legal or technological requirements for its use, but the `DNT` header can be used to signal
that a web application should disable either its tracking or cross-site user tracking of an individual user.
