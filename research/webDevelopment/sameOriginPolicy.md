# Same Origin Policy Documentation

The same-origin policy is a critical security mechanism that restricts how a document or script loaded
from one origin can interact with a resource from another origin. It helps isolate potentially
malicious documents, reducing possible attack vectors.

Relevent URLs:
[Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy)

## Origin Definition

Two URLs have the _same origin_ if the protocol, port (if specified), and host are the same for both.
You may see this referenced as the "scheme/host/port tuple", or just "tuple".

The following table gives examples of origin comparisons with the URL
`http://store.company.com/dir/page.html`:

| URL                                               | Outcome     | Reason                                         |
|:--------------------------------------------------|:------------|:-----------------------------------------------|
| `http://store.company.com/dir2/other.html`        | Same origin | Only the path differs                          |
| `http://store.company.com/dir/inner/another.html` | Same origin | Only the path differs                          |
| `https://store.company.com/page.html`             | Failure     | Different protocol                             |
| `http://store.company.com:81/dir/page.html`       | Failure     | Different port (http:// is port 80 by default) |
| `http://news.company.com/dir/page.html`           | Failure     | Different host                                 |

### Inherited Origins

Scripts executed from pages with an `about:blank` or `javascript:` URL inherit the origin of the
document containing that URL, since these types of URLs do not contain information about an origin
server.

For example, `auto:blank` is often used as a URL of new, empty popup windows into which the parent
script writes content.

### Exceptions in Internet Explorer

Internet explorer has two major exceptions to the same-origin policy:

- Trust Zones: If both domains are in the _highly trusted zone_ (e.g. corporate intranet domains),
  then the same-origin limitations are not applied

- Port: IE doesn't include port into same-origin checks. Therefore, `https://company.com:81/index.html`
  and `https://company.com/index.html` are considered the same origin and no restrictions are applied

These exceptions are nonstandard and unsupported in any other browser.

## Changing Origin

A page may change its own origin, with some limitations. A script can set the value of `document.domain`
to its current domain or a superdomain of its current domain. If set to a superdomain of the current
domain, the shorter superdomain is used for same-origin checks.

For example, assue a script from the document at `http://store.company.com/dir/other.html` executes
the following:

```js
document.domain = "company.com";
```

Afterward, the page can pass the same-origin check with `http://company.com/dir/page.html`.
However `company.com` could __NOT__ set `document.comain = "othercompany.com";`, since that is not a
superdomain of `company.com`.

The port number is checked separately by the browser. Any call to `document.domain` causes the port
number to be overwritten to `null`.

## Cross-origin network access

The same-origin policy controls interactions between two different origins, such as when you use
`XMLHttpRequest` or an `<img>` element. These interactions are typically placed into three categories:

- Cross-origin _writes_ are typically allowed. Examples are links, redirects, and form submissions.
  Some HTTP requests require preflight.

- Cross-origin _embedding_ is typically allowed

- Cross-origin _reads_ are typically disallowed, but read access is often leaked by embedding. For
  example, you can read the dimensions of an embedded image, the actions of an embedded script,
  or the availability of an embedded resource

Here are some examples of resources which may be embedded cross-origin:

- JavaScript with `<script src="…"></script>`. Error details for syntax errors are only available for
  same-origin scripts.
- CSS applied with `<link rel="stylesheet" href="…">`. Due to the relaxed syntax rules of CSS,
  cross-origin CSS requires a correct Content-Type header. Restrictions vary by browser: IE,
  Firefox, Chrome, Safari (scroll down to CVE-2010-0051) and Opera.
- Images displayed by `<img>`.
- Media played by `<video>` and `<audio>`.
- Plugins embedded with `<object>`, `<embed>`, and `<applet>`.
- Fonts applied with @font-face. Some browsers allow cross-origin fonts, others require same-origin.
- Anything embedded by `<frame>` and `<iframe>`. Sites can use the X-Frame-Options header to
  prevent cross-origin framing.

### How to allow cross-origin access

Use CORS to allow cross-origin access. CORS is part of HTTP that lets servers specify what hosts are
permitted to load content from that server.

### Block Cross-Origin Access

- To prevent cross-origin writes, check an unguessable token in the request — known as a
  `Cross-Site Request Forgery (CSRF)` token. You must prevent cross-origin reads of pages that require
  this token.

- To prevent cross-origin reads of a resource, ensure that it is not embeddable. It is often necessary
  to prevent embedding because embedding a resource always leaks some information about it.

- To prevent cross-origin embeds, ensure that your resource cannot be interpreted as one of the
  embeddable formats listed above. Browsers may not respect the Content-Type header. For example,
  if you point a `<script>` tag at an HTML document, the browser will try to parse the HTML as
  JavaScript. When your resource is not an entry point to your site, you can also use a CSRF token
  to prevent embedding.

## Cross-origin script API access

Javascript APIs like `iframe.contentWindow`, `window.parent`, `window.open`, and `window.opener`
allow documents to directly reference each other. When two documents do not have the same origin,
these references provide very limited access to `Window` and `Location` objects.

### Window

Methods:

- `window.blur`
- `window.close`
- `window.focus`
- `window.postMessage`

Attributes:

- `window.closed`:Read only.
- `window.frames`:Read only.
- `window.length`:Read only.
- `window.location`:Read/Write.
- `window.opener`:Read only.
- `window.parent`:Read only.
- `window.self`:Read only.
- `window.top`:Read only.
- `window.window`:Read only.

### Location

Methods:

- `location.replace`

Attributes:

- `URLUtils.href`: Write-only.

## Cross-origin data storage access

Access to data stored in the browser such as `localStorage` and `IndexedDB` are separated by origin.
Each origin gets its own separate storage, and JavaScript in one origin cannot read from or write to
the storage belonging to another origin.

Cookies use a separate definition of origins. A page can set a cookie for its own domain or any parent
domain, as long as the parent domain is not a public suffix. Firefox and Chrome use the
`Public Suffix List` to determine if a domain is a public suffix. The browser will make a cookie
available to the given domain including any sub-domains, no matter which protocol (HTTP/HTTPS) or
port is used. WHen you set a cookie, you can limit its using Domain, Path, Secure, and HttpOnly
flags. When you read a cookie, you cannot see from where it was set. Even if you use only secure
https connections, any cookie you see may have been set using an insecure connection.


