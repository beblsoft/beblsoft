# Website Legal Documentation

Websites should contain various documentation to protect themselves from legal action.

Relevant URLs:

- [Website Policy Types](https://www.websitepolicies.com/create)
- [Generator Review](https://thelegality.com/reviews/best-privacy-policy-generators/)
- [Analyze Generation Review](https://www.analyzo.com)
- [Legal Generator Review](https://thelegality.com/reviews/best-privacy-policy-generators/)

Relevant Generators:

- **[TermsFeed](https://www.termsfeed.com/privacy-policy-generator/?sscid=61k3_5nseq) - Yes, expensive
  (~$400+.) but comprehensive/consistent privacy, cookie, terms, refund and disclaimers.**
- [Iubenda](https://www.iubenda.com/en/) - No, because subscription and only privacy and cookie.
- [WebsitePolicies](https://www.websitepolicies.com/) - No, because not applicable to Facebook Apps
- [PrivacyPolicies](https://www.privacypolicies.com/) - Must go with International Privacy Policy Level.
  No, because not for SasS Apps
- [FreePrivacyPolicy](https://www.freeprivacypolicy.com/)  - No, because not applicable to Facebook Apps

## Document Types

### Privacy Policy (Facebook App as we use their APIs)

A privacy policy is a document that discloses some or all of the ways your website or
product gathers, uses, discloses, and manages a customer or client's data. It fulfills a
legal requirement to protect a customer or client's privacy.

### Cookie Policy

A cookie policy outlines the types of cookies and other tracking technologies used on your
website, what they do, and how they are used. You are required by the EU law to have a
cookie policy and notify visitrs that your site uses them.

### Terms and Conditions

Terms and conditions (also known as terms of use and terms of service) are rules which one
must agree to abide by in order to use your website or product. Terms and conditions can
also be a disclaimer and is legally binding for both parties.

### Refund Policy

A good refund policy can help protect your company and win your customer's trust. Refund
policy outlines the requirements and steps your customers need to take in order to return
or exchange purchased product and receive a refund.

### Disclaimers

A disclaimer is often described as a common method used by website owners in order to limit
their liability on their website. The main purpose of a disclaimer is to make sure the
information on your website will not be improperly relied upon.


