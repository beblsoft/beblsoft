# GDPR Documentation

The General Data Protection Regulation (GDPR) is a regulation in EU law on data protection and privacy
for all individual citizens of the European Union (EU) and the European Economic Area (EEA). It also
addresses the export of personal data outside the EU and EEA areas. The GDPR aims primarily to give
control to individuals over their personal data and to simplify the regulatory environment for
international business by unifying the regulation within the EU.

Put simply, GDPR is designed to:
- Harmonize data privacy laws across Europe
- Protect and empower all EU citizens data privacy
- Reshape the way organizations across the region approach data privacy

Relevant URLs:
[Official Site](https://ec.europa.eu/commission/priorities/justice-and-fundamental-rights/data-protection/2018-reform-eu-data-protection-rules_en),
[eugdpr.org](https://eugdpr.org/),
[Wikipedia](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)

## Terms

### Data Controller

A data controller is a person, company, or other body that determines the purpose and means of personal
data processing

### Data Processor

The data processor processes personal data only on behalf of the controller. The data processor is usually
a third party external to the company

## Rules

### Consent

The conditions for consent have been strengthened, and companies are no longer able to use long illegible
terms and conditions full of legalese. The request for consent must be given in an intelligible and
easily accessible form, with the purpose for data processing attached to that consent. Consent must be
clear and distinguishable from other matters and provided in an intelligible and easily accessible form,
using clear and plain language. It must be as easy to withdraw consent as it is to give it.

### Breach Notifications

Under the GDPR, breach notifications are now mandatory if all memeber states where a data breach is
likely to "result in a risk for the rights and freedoms of individuals". This must be done within
__72 hours__ of first having become aware of the breach. Data processors are also required to notify
their customers, the controllers, "without undo delay" after first becoming aware of a data breach.

### Right to Access

Part of the expanded rights of data subjects outline by the GDPR is the right for data subjects to obtain
confirmation from the data controller as to whether or not personal data concerning them is being processed,
where and for what purpose. Further, the controller shall provide a copy of the personal data, free of
charge, in an electronic format. This change is a dramatic shift to data transparency and empowerment
of data subjects.

### Right to be Forgotten

Also know as Data Erasure, the right to be forgotten entitles the data subject to have the data
controllers erase his/her personal data, xease further dissemination of the data, and potentially
have third parties halt processing of the data.

### Data Portability

GDPR introduces data portability - the right for a data subject to receive the personal data concerning
them - which they have previously provided in a 'commonly use and machine readable format' and have the
right to transmit that data to another controller.

### Privacy By Design

Privacy be design calls for the inclusing of data protection from the onset of the designing of systems,
rather than an addition. More specifically, "the controller shall... implement appropriate technical
and organizational measures... in an effective way.. in order to meet the requirements of this regulation
and protect the rights of data subjects." Article 23 calls for controllers to hold and process only
the data absolutely necessary for the completion of its duties (data minimization), as well as limiting
the access to personal data to those needing to act out the processing.

### Data Protection Officers

Under GDPR it is not necessary to submit notifications / registrations to each local DPA of data processing
activities, nor is it a requirement to notify / obtain approval for transfers based on the Model Contract
Clauses (MCCs). Instead, there are internal record keeping requirements, as further explained below, and DPO
appointment is mandatory only for those controllers and processors whose core activities consit
of processing operations which require regular and systematic monitoring of data subjects on a large scale
or of special categories of data or data relating to criminal convictions and offences. Importantly, the
Data Protection OFficer:
- Must be appointed on the basis of professional qualities and, in particular, expert knowledge on
  data protection law and practices
- May be a staff member or an external service provider
- Contact details must be provided to the relevant DPA
- Must be provided with appropriate resources to carry out their tasks and maintain their expert knowledge
- Must report directly to the highest levels of management
- Must not carry out any other tasks that could result in conflict of interest

### Increases Territorial Scope

GDPR applies to all companies processing the personal data of data subjects residing in the Union,
regardless of the company's location. Non-EU businesses processing the data of EU citizens also have
to appoint a representative in the EU.

### Penalties

Organizations in breach of GDPR can be fined up to 4% of annual global turnover or 20 Million euro
(whichever is greater). This is the maximum find that can be imposed for the most serios infringements
e.g. not having sufficient customer consent to process data or violating the core of Privacy by Design
concepts.

There is a tiered approach to fines e.g. a company can be fined 2% for not having their records in order
(article 28), not notifying the supervising authority and data subject about a breach or not conducting
impact assessment. It is important to not that these rules apply to both controllers and processors - meaning
'clouds' are not exempt from GDPR enforcement.
