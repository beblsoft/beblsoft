# Open Source Licenses Docuemntation

An open-source license is a type of license for computer software and other products that allows the
source code, blueprint or design to be used, modified and/or shared under defined terms and conditions.
This allows end users and commercial companies to review and modify the source code, blueprint or
design for their own customization, curiosity or troubleshooting needs.

Open-source licensed software is mostly available free of charge, though this does not necessarily
have to be the case. Licenses which only permit non-commercial redistribution or modification of the
source code for personal use only are generally not considered as open-source licenses. However,
open-source licenses may have some restrictions, particularly regarding the expression of respect to
the origin of software, such as a requirement to preserve the name of the authors and a copyright
statement within the code, or a requirement to redistribute the licensed software only under the
same license (as in a copyleft license). One popular set of open-source software licenses are those
approved by the Open Source Initiative (OSI) based on their Open Source Definition (OSD).

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Open-source_license),
[Heather Meeker Overview](https://opensource.com/article/17/9/open-sourc-licensing),
[Open Source Initiative Licenses](https://opensource.org/licenses)

## License Types

Open source licenses generally fall into one or two categories: _permissive_ and _copyleft_

### Permissive

A permissive license is simple and is the most basic type of open soure license: It allows you to
do whatever you want with the software as long as you abide by the notice requirements. Permissive
licenses provide the software as-is, with no warranties. They are summarized as follows:

- Do whatever you want with the code
- Use at your own risk
- Acknowledge the author/contributor

Some licenses include:

- __BSD (Berkely Software Distribution)__
- __MIT__
- __Apache 2__

### Copyleft

Copyleft licenses add requirements to the permissive license. In addition to the requirements
listed about, copyleft licenses also require that:

- If you distribute binaries, you must make the source code for those binaries available
- The source code must be available under the same copyleft terms under which you got the code
- You cannot place additional restrictions on the licensee's exercise of the license

Some licenses include:

- __Affero GPL (AGPL)__: requires server be GPL if client contains GPL code
- __GPL__: requires that any program that contains GPL code must contain only GPL code
- __Lesser GPL (LGPL)__:allows dynamic linking to other proprietary code without subjecting that linked
  code to the same GPL requirements
- __Mozilla Public License (MPL)__
- __Eclipse Public License (EPL)__
- __Common Development and Distribution License (CDDL)__
