OVERVIEW
===============================================================================
- Trunk Based Development Documentation
- Relevant URL                      : https://trunkbaseddevelopment.com/
- Description                       : A source-control branching model, where developers collaborate on code
                                      in a single branch called "trunk" (master in Git).
                                    : They therefore avoid merge hell, do not break the build, and live happily ever after
                                    : Key enabler of continuous integration and continuous delivery. Codebase is always
                                      releasable on demand
- Picture/Flow                      : Whole dev team should develop in a single branch, trunk
                                    : Devs don't commit to release branches
                                    : Code review and automated CI-style builds are done before integrating/merging
                                      into trunk

                                               Release 12.1              Release 12.2
                                          ------------------         ------------------
                                          |                          |
                                          |                          |
                                          |                          |
                                      --------------------------------------------------------\
                                       Trunk (or master)                                       \
                                                                                               /
                                      --------------------------------------------------------/
                                       |  /   |     /                         |  /   |     /
                                       | /    | ----       Short-Lived        | /    | ----
                                       |/     |/           Feature Branches   |/     |/

- Claims                            : You should do Trunk-Based Development instead of GitFlow and other branching models
                                    : Use direct trunk commit/push or pull request workflow
- History                           : Trunk is referent to the concept of a growing tree, where the fattest and
                                      longest span is the trunk, not the branches that radiate from it
                                    : Google and Facebook practice it at scale



DEVOPS CONTEXT
===============================================================================
- Devops is composed of             : Lean Experiments
                                      Continouous Deployment
                                      Continuous Delivery
                                      Continuous Integration
                                      Trunk Based Developmebt
                                      Development Infrastructure
- Trunk Prerequisites
  * Development Infrastructure      : Infrastructure as code

- Trunk Facilitates
  * Continuous Integration          : Integrating to a shared code line frequently and testing that
                                      Have machines issue early warnings on breakages and incompatibilities
  * Continuous Delivery             : Expands CI to automatically re-deploy a proven build to a QU or UAT environment
  * Lean Experiments                : Design scientific experiments to meet business objectives




FIVE-MINUTE OVERVIEW
===============================================================================
- The Problem: Distance
  * Description                     : Distance refers to the integration of code from multiple
                                      components/modules/sub-teams. For a binary that could be deployed or shipped
  * Distance Can                    : Break something unexpected once merged
                                      Be difficult to merge in
                                      Not show that work was duplicated until it is merged
                                      Not show problems of incompatibility/undesirability that does not break the build
  * Goal                            : Minimize distance
- Deciding factors
  * Releasability of work in prog   : Trunk-Based development will always be release ready
                                      Never break the build, and always be release ready
                                      Release from trunk, or create branch for actual release
  * Check out/ cloning              : Checkout from trunk frequently, knowing that build always passes
  * Committing                      : Commits are typically small, are only allowed if they don't break the build
  * Code Review                     : Have team review changes before they go into trunk
  * Safety net                      : CI daemons are set up to watch and test the trunk. If the trunk is broken
                                      the team is alerted


TOPICS
===============================================================================
- Feature Flags ---------------------
  Description                       : Time-honored way to control capabilities of an application or service
  Granularity                       : Example: could toggle UI component
  Continuous Integration pipelines  : Test all meaningful permutations in parallel
  Runtime switchable                : Switch at runtime
  A/B testing and betas             : Turn on flags for a particular segment of users to test it in isolation
  Technical Debt                    : Remove old flags that are no longer toggled

- Branch By Abstraction -------------
  Description                       : Technique to effect a "longer to complete" change in the trunk
  Rules                             1 Lot of developers already depend on code that is subject to "longer to complete"
                                      change, and we don't want them to be slowed down
                                    2 No commit pushed to shared repo should jeopardize ability to go live
  Ideal steps                       1 Introduce the abstraction around the code that is to be replaced,
                                      commit that for all to see. No commit is allowed to break the build
                                    2 Write second implementation of abstraction, maybe in turned-off state
                                    3 Flip software software from off to on
                                    4 Remove the to-be replaced implementation
                                    5 Remove the abstraction
  Benefits
  * Cheaply pause/resume migrations : Can always pause and restart work later
  * Cancellation is cheap           : Not expensive to delete branch by abstraction



RELEASING
===============================================================================
- Option: Branch for release --------
  Picture/Flow                      : All devs commit to trunk
                                    : Each release a new branch is spawned from trunk
                                    : Release managers can cherry-pick merges from trunk and put them into release

                                               Release 12.1
                                          ------------------
                                          |       ^
                                          |      /
                                          |    Cherry-Pick
                                          |    /
                                      --------------------------------------------------------\
                                       Trunk (or master)                                       \
                                                                                               /
                                      --------------------------------------------------------/
  Fix production bugs on trunk      : Reproduce the bug on trunk, fix it there with a test,
                                      watch test to be verified on CI server, then cherry-pick to release branch
- Release branch deletion           : Delete release branches after they are no longer used
  Option: Release from trunk --------
  Picture/Flow                      : Teams with a very high release cadence do not need (and cannot use) release
                                      branches at all, they have to release from trunk
                                    : Choose to roll forward and fix the bug on the trunk as if it were a feature

                                        8sdsadk123            ddddk123
                                          ^                      ^
                                      --------------------------------------------------------\
                                       Trunk (or master)                                       \
                                                                                               /
                                      --------------------------------------------------------/



CONTINUOUS...
===============================================================================
- Continuous Integration -----------
  As defined                       : A daemon process that is watching the source-control repository for changes
                                     and verifying that they are correct. Regardless of branching model
  CI Dauemons Perform Verifications
  * Humans and daemons do the same : Humans use the same script to test as CI daemon
  * Radiators                      : Colocated radiator to show build status
                                     Red/Green lights for status/fail
                                     #4321 Compile > Unit > Integration > Functional
  * Quick build news               : Key off notification quickly
  * Pipelines
  Advanced Topics
  * CI per commit or batching      : Best to have CI daemon run on every commit separatebly
  * Master/Slave CI infrastructure : Run slaves in parallel
  * Tests never green incorrectly
  * Steps to commit                1 Coding
                                   2 Propose push to trunk
                                   3 CI daemon verification
                                   4 Code review
                                   5 Push to trunk
                                   6 Available on trunk

- Continous Code Review ------------
  Description                      : Team commits to processing proposed commits (to trunk) from teammates trunk speedily
                                   : Important that code reviews are not allowed to back up
  Pull Requests (PRs)              : A PR is one or more commits towards a goal described in an accompying piece of text
                                     Signifies end or pause in work
  Common Code Owners               : Commits being reviewed are never rejected for ownership sake

- Continous Delivery ---------------
  Description                      : Practice of expanding your continuous integration(CI) usage to automatically
                                     re-deploy a proven build to QA or UAT environment
                                     # 4321 Compile > Unit > Integration > Functional > Deploy
- Continuous Deployment ------------
  Description                      : Extension of Continuous Delivery but deployment is to production



OBSERVED HABITS
===============================================================================
- No code freeze                   : Developers living in trunk-based development do not experience variance in their
                                     days or weeks on the trunk. In particular there is no "we're freezing the code"
                                   : Generally speacing, the trunk is a place to firehose commits into
- Quick Reviews                    : Quick peer-reviews or pair programming
- Chasing Head                     : Update/pull/sync from shared trunk often (multiple times a day)
- Running the build locally        : Run build before pushing
- Facilitating commits             : Big changes (ex. directory renames) are done in small transactions
- Powering through broken builds   : Automated rollback of broken commit
- Shared nothing                   : Developers on their workstations rely on a microcosm setup for the application
                                     or service they are developing
                                   : Bring up application and play with it
                                   : Run all unit, integration, and functional tests locally
- Common code ownership            : Everyone owns the code
- Always release ready             : Go live on short notice
- Thin vertical slices             : Short stories or tasks that transcend all layers of the stack



MONOREPOS
===============================================================================
- Description                      : A monorepo is a specific trunk-based development implementation where the
                                     organization in question puts its source for all
                                     applications/services/libraries/frameworks into one trunk
                                   : Used by Google
- Goals                            : Acquire as many third-part and in-house dependencies as possible for a build from
                                     the same source-control repository/branch
                                   : Keep all teams in agreement on the versions of third-party dependencies
                                   : Allow changes to multiple modules via atomic commits
                                   : Allow the extraction of new common dependencies
                                   : Force all developers to focus on the HEAD revisisions
                                   : Solve root cause of bug in one location
- Risk of chaotic directory layout : Directory structure must be enforced globally
- Lock step deployments            : Monorepos say "what" will be released, not "when"
- If no monorepo                   : Repository separation should be no more fine grained than things that
                                     have separate deployment cadence

