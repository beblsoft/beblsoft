#!/usr/bin/env python3
"""
 NAME:
  quotes.py

 DESCRIPTION
  Quote Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
from datetime import datetime, timedelta
import copy

# ----------------------------- GLOBALS ------------------------------------- #
quoteList = []

# ----------------------------- QUOTE CLASS --------------------------------- #


class Quote():
    """
    Quote Class
    """

    def __init__(self, text, author, pictureURL, tag):
        """
        Initialize quote object
        Args
          text:
            quote text
            ex. "Hello World"
          author:
            quote author
            ex. "Abraham Lincoln"
          pictureURL:
            Picture to display with quote
            ex. "https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg"
          tag:
            Theme or focus of quote
            ex. 'help'
        """
        self.text        = text
        self.author      = author
        self.displayDate = None
        self.pictureURL  = pictureURL
        self.tag         = tag
        quoteList.insert(0, self)

    @staticmethod
    def getQuoteList(startDate, endDate):
        """
        Return Quote List, sorted by reverse date [later date, ..., earlier date]

        Args
          startDate:
            Datetime of first quote
          endDate:
            Datetime of last quote
        """
        firstDate    = datetime(2017, 11, 1)
        startIdx     = (startDate - firstDate).days
        endIdx       = (endDate - firstDate).days

        # Iterate over quotes in quoteList containing a new quoteList which
        # has dates assigned to the quotes
        quoteListLen = len(quoteList)
        newQuoteList = []
        idx          = 0
        while startIdx + idx <= endIdx:
            print(startIdx)
            print(idx)
            quote                = quoteList[(startIdx + idx) % quoteListLen]
            newQuote             = copy.deepcopy(quote)
            newQuote.displayDate = firstDate + timedelta(days=(startIdx + idx))
            newQuoteList.append(newQuote)
            idx                 += 1

        newQuoteList = sorted(newQuoteList, key=lambda quote: quote.displayDate,
                              reverse=True)
        return newQuoteList


# ----------------------------- QUOTE DATA ---------------------------------- #
Quote(text="Live with passion!",
      author="Tony Robbins",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="It's not what you look at that matters, it's what you see.",
      author="Henry David Thoreau",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Be yourself; everyone else is already taken.",
      author="Oscor Wilde",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Why fit in when you were born to stand out.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="There is no such thing as failure. There are only results.",
      author="Tony Robbins",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="If you do what you've always done, you'll get what you've always gotten.",
      author="Tony Robbins",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy.",
      author="Norman Vincent Peale",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Quality means doing it right when no one is looking.",
      author="Henry Ford",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Don't find fault, find a remedy.",
      author="Henry Ford",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="You know you're in love when you can't fall asleep because reality is finally better than your dreams.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="Imagination is the true magic carpet.",
      author="Norman Vincent Peale",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Christmas waves a magic wand over this world, and behold, everything is softer and more beautiful.",
      author="Norman Vincent Peale",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="You only live once, but if you do it right, once is enough.",
      author="Mae West",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Quality means doing it right when no one is looking.",
      author="Henry Ford",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Setting goals is the first step in turing the invisible into the visible.",
      author="Tony Robbins",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Failure is simply the opportunity to begin again, this time more intelligently",
      author="Henry Ford",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="If you want to know what a man's like, take a good look at how he treats his inferiors, not his equals.",
      author="J.K. Rowling",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="The secret of getting ahead is getting started.",
      author="Mark Twain",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="The best way to cheer yourself up is to try to cheer somebody else up.",
      author="Mark Twain",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="Humor is mankind's greatest blessing.",
      author="Mark Twain",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Wrinkles should merely indicate where smiles have been.",
      author="Mark Twain",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="I can live for two months on a good compliment.",
      author="Mark Twain",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Attitude is a little thing that makes a big difference.",
      author="Winston Churchhill",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="We make a living by what we get, but we make a life by what we give.",
      author="Winston Churchhill",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Never, never, never give up.",
      author="Winston Churchill",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="The price of greatness is responsibilty",
      author="Winston Churchhill",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="History will be kind to me for I indend to write it.",
      author="Winston Churchhill",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="How wonderful it is that nobody need wait a single moment before starting to improve the world.",
      author="Anne Frank",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="Whoever is happy will make others happy too.",
      author="Anne Frank",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Alone we can do so little; together we can do so much.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="Do not go where the path may lead, go instead where there is no path and leave a trail.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="There are no secrets to success. It is the result of preparation, hard work, and learning from failure.",
      author="Colin Powell",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
Quote(text="Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="We all need people who will give us feedback. That's how we improve.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="Peace begins with a smile.",
      author="Mother Teresa",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Love all, trust a few, do wrong to none.",
      author="William Shakespeare",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Your time is limited, so don't waste it living someone else's life. ... most important, have the courage to follow your heart and intuition.",
      author="Steve Jobs",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="If you judge people, you have no time to love them.",
      author="Mother Teresa",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="Life isn't about finding yourself. Life is about creating yourself.",
      author="George Bernard Shaw",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Life is like riding a bicycle. To keep your balance, you must keep moving.",
      author="Albert Einstein",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="Anything's possible if you've got enough nerve.",
      author="J.K. Rowling",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="I just love working hard. I love being part of a team; I love working toward a common goal.",
      author="Tom Brady",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="You can design and create, and build the most wonderful place in the world. But it takes people to make the dream a reality.",
      author="Walt Disney",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="There is nothing so rewarding as to make people realize that they are worthwhile in this world.",
      author="Bob Anderson",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Don't cry because it's over, smile because it happened.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="The most important thing in the world is family and love.",
      author="John Wooden",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="My best friend is the one who brings out the best in me.",
      author="Henry Ford",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
Quote(text="Do not go where the path may lead, go instead where there is no path and leave a trail.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="There is only one happiness in this life, to love and be loved",
      author="George Sand",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="The only thing worse than being blind is having sight but no vision.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="I am a slow walker, but I never walk back.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="An early-morning walk is a blessing for the whole day.",
      author="Henry David Thoreau",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Things do not happen.  Things are made to happen.",
      author="John F. Kennedy",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Don't worry when you are not recognized, but strive to be worthy of recognition.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Live the life you've dreamed.",
      author="Henry David Thoreau",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="Those who dare to fail miserably can achieve greatly.",
      author="John F. Kennedy",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="Success usually comes to those who are too busy to be looking for it.",
      author="Henry David Thoreau",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="Whatever you are, be a good one.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="Adopt the pace of nature: her secret is patience.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Alone we can do so little; together we can do so much.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="Do not go where the path may lead, go instead where there is no path and leave a trail.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="No one can make you feel inferior without your consent.",
      author="Eleanor Roosevelt",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
Quote(text="Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="We all need people who will give us feedback. That's how we improve.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="Peace begins with a smile.",
      author="Mother Teresa",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="If you judge people, you have no time to love them.",
      author="Mother Teresa",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="You have brains in your head. You have feet in your shoes. You can steer yourself any direction you choose. You're on your own. And you know what you know. And YOU are the one who'll decide where to go...",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="For every minute you are angry you lose sixty seconds of happiness.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="All you need is love. But a little chocolate now and then doesn't hurt.",
      author="Charles M. Schultz",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Not all of us can do great things. But we can do small things with great love.",
      author="Mother Teresa",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="I like nonsense, it wakes up the brain cells. Fantasy is a necessary ingredient in living.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="Love all, trust a few, do wrong to none.",
      author="William Shakespeare",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="You can design and create, and build the most wonderful place in the world. But it takes people to make the dream a reality.",
      author="Walt Disney",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="There is nothing so rewarding as to make people realize that they are worthwhile in this world.",
      author="Bob Anderson",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Don't cry because it's over, smile because it happened.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="The most important thing in the world is family and love.",
      author="John Wooden",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="My best friend is the one who brings out the best in me.",
      author="Henry Ford",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
Quote(text="There is only one happiness in this life, to love and be loved",
      author="George Sand",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="The only thing worse than being blind is having sight but no vision.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="I am a slow walker, but I never walk back.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="An early-morning walk is a blessing for the whole day.",
      author="Henry David Thoreau",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Things do not happen.  Things are made to happen.",
      author="John F. Kennedy",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Don't worry when you are not recognized, but strive to be worthy of recognition.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Live the life you've dreamed.",
      author="Henry David Thoreau",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="Those who dare to fail miserably can achieve greatly.",
      author="John F. Kennedy",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="Success usually comes to those who are too busy to be looking for it.",
      author="Henry David Thoreau",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="Whatever you are, be a good one.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="Adopt the pace of nature: her secret is patience.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Alone we can do so little; together we can do so much.",
      author="Helen Keller",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="Do not go where the path may lead, go instead where there is no path and leave a trail.",
      author="Ralph Waldo Emerson",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="If you tell the truth, you don't have to remember anything.",
      author="Mark Twain",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
Quote(text="Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg",
      tag="help")
Quote(text="We all need people who will give us feedback. That's how we improve.",
      author="Bill Gates",
      pictureURL="https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg",
      tag="help")
Quote(text="Peace begins with a smile.",
      author="Mother Teresa",
      pictureURL="https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg",
      tag="help")
Quote(text="Love all, trust a few, do wrong to none.",
      author="William Shakespeare",
      pictureURL="https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg",
      tag="help")
Quote(text="Your time is limited, so don't waste it living someone else's life. ... most important, have the courage to follow your heart and intuition.",
      author="Steve Jobs",
      pictureURL="https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg",
      tag="help")
Quote(text="Technology is nothing. What's important is that you have a faith in people, that they're basically good and smart, and if you give them tools, they'll do wonderful things with them",
      author="Steve Jobs",
      pictureURL="https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg",
      tag="help")
Quote(text="Logic will get you from A to B. Imagination will take you everywhere.",
      author="Albert Einstein",
      pictureURL="https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg",
      tag="help")
Quote(text="Folks are usually about as happy as they make their minds up to be.",
      author="Abraham Lincoln",
      pictureURL="https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg",
      tag="help")
Quote(text="Anything's possible if you've got enough nerve.",
      author="J.K. Rowling",
      pictureURL="https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg",
      tag="help")
Quote(text="I just love working hard. I love being part of a team; I love working toward a common goal.",
      author="Tom Brady",
      pictureURL="https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg",
      tag="help")
Quote(text="“All you need is love. But a little chocolate now and then doesn't hurt.",
      author="Charles M. Schulz",
      pictureURL="https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg",
      tag="help")
Quote(text="There is nothing so rewarding as to make people realize that they are worthwhile in this world.",
      author="Bob Anderson",
      pictureURL="https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg",
      tag="help")
Quote(text="Don't cry because it's over, smile because it happened.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg",
      tag="help")
Quote(text="Sometimes the questions are complicated and the answers are simple.",
      author="Dr. Seuss",
      pictureURL="https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg",
      tag="help")
Quote(text="My best friend is the one who brings out the best in me.",
      author="Henry Ford",
      pictureURL="https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg",
      tag="help")
