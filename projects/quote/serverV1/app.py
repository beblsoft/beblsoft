#!/usr/bin/env python3
"""
 NAME:
  app.py

 DESCRIPTION
  Flask Application
"""


# ----------------------------- IMPORTS ------------------------------------- #
import json
import logging
import pprint
from datetime import date, datetime
from flask import Flask, Response, request
from flask_cors import CORS
from quote.serverV1.quotes import Quote


# ----------------------------- GLOBALS ------------------------------------- #
quoteMap  = {}
app       = Flask(__name__)
CORS(app)
app.debug = True
logger    = logging.getLogger(__name__)


# ------------------------ CUSTOM JSON ENCODER ------------------------------ #
class CustomJSONEncoder(json.JSONEncoder):
    """
    Encode objects into JSON
    """

    def default(self, o):  # pylint: disable=E0202
        """
        Encode object
        """
        rval = None
        if isinstance(o, Quote):
            rval = o.__dict__
        elif isinstance(o, (datetime, date)):
            rval = o.strftime("%d-%b-%Y")
        else:
            rval = json.JSONEncoder.default(self, o) #pylint: disable=E1111
        return rval


# ------------------------ JSON RESPONSE ------------------------------------ #
class JSONResponse(Response):
    """
    JSON Response Class
    """
    MAGIC = 7292018

    def __init__(self, data=None, **kwargs):
        """
        Initialize respone object
        Args
          status: HTTP status code
          code: Beblsoft error code
          data: String of data
        """
        super().__init__(self, **kwargs)
        self.mimetype = "application/json"
        if not data:
            data      = {}
        dataDict      = dict(magic=JSONResponse.MAGIC, data=data)
        self.data     = json.dumps(dataDict, cls=CustomJSONEncoder)


# ----------------------------- ROUTES -------------------------------------- #
@app.route("/hw", methods=["GET"])
def hello_world():  # pylint: disable=C0111
    return JSONResponse(data={"Hello": "World!"})


@app.route("/quote", methods=["POST"])
def quote():
    """
    Return quotes
    inputJSON:
      startDate
        ex. "01-01-2018"
      endDate
        ex. "02-01-2015"
    outputJSON:
      array of quote objects
    """
    logger.warning("request={}".format(pprint.pformat(request.__dict__)))
    inData    = request.get_json()
    logger.warning("inData={}".format(pprint.pformat(inData)))
    startDate = datetime.strptime(inData.get("startDate", None), "%d-%b-%Y")
    endDate   = datetime.strptime(inData.get("endDate", None), "%d-%b-%Y")
    quoteList = Quote.getQuoteList(startDate=startDate, endDate=endDate)
    return JSONResponse(data=quoteList)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    app.run()
