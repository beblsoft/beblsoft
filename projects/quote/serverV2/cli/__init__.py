#!/usr/bin/env python3
"""
 NAME:
  __init__.py

DESCRIPTION
 Quote Server CLI Module
"""


# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import sys
import traceback
import logging
import click
from base.bebl.python.log.bLog import BeblsoftLog
from quote.serverV2.config.localDevConfig import LocalDevConfig
from quote.serverV2.config.localScrapyConfig import LocalScrapyConfig
from quote.serverV2.config.awsConfig import AWSConfig
from quote.serverV2.app import Application
from quote.serverV2.db import Database
from .app import app
from .db import db
from .shell import shell
from .test import test


# ----------------------- GLOBALS ------------------------------------------- #
class GlobalContext():
    """Global Context object for CLI"""

    def __init__(self):
        self.cfgMap    = {
            'localDev': LocalDevConfig,
            'localScrapy': LocalScrapyConfig,
            'aws': AWSConfig,
        }
        self.cfg              = None
        self.db               = None
        self.app              = None
        self.bLog             = None
        self.verboseFilterMap = {
            "0": logging.WARNING,
            "1": logging.DEBUG,
            "2": logging.NOTSET
        }
        self.startTime        = None
        self.endTime          = None

logger = logging.getLogger(__file__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("-c", "--config", default="localDev",
              type=click.Choice(gc.cfgMap.keys()),
              help="Configuration Type. Default=localDev")
@click.option("-v", "--verbose", default="1",
              type=click.Choice(gc.verboseFilterMap.keys()),
              help="Console verbosity level. Default=1")
@click.option("--logfile", default="/tmp/quoteServerCLI.log", type=click.Path(),
              help="Specify log file")
@click.pass_context
def cli(ctx, config, logfile, verbose):
    """
    Quote Server Command Line Interface
    """
    gc.cfg  = gc.cfgMap[config]()
    gc.db   = Database(cfg=gc.cfg)
    gc.app  = Application(cfg=gc.cfg)
    gc.bLog = BeblsoftLog(logFile=logfile, cFilter=gc.verboseFilterMap[verbose])
    gc.bLog.logHeader()
    ctx.obj["gc"] = gc

cli.add_command(app)
cli.add_command(db)
cli.add_command(shell)
cli.add_command(test)


# ----------------------- MAIN ---------------------------------------------- #
def main():
    """
    Execute CLI
    """
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
