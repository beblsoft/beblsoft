#!/usr/bin/env python3
"""
NAME:
 shell.py

DESCRIPTION
 shell Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import code
import click


# ----------------------- SHELL --------------------------------------------- #
@click.command()
@click.pass_context
def shell(ctx):
    """
    Create interactive python shell
    """
    gc       = ctx.obj["gc"]
    shellCtx = {"gc": gc}
    code.interact(banner="", local=shellCtx)
