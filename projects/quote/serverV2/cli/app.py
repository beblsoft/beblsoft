#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 app Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- APPLICATION --------------------------------------- #
@click.group()
def app():
    """
    Flask application Control
    """
    pass


@app.command()
@click.option("--host", default="localhost", help="Host Name")
@click.option("-p", "--port", default=5000, help="Port")
@click.option("--debug", is_flag=True, default=True, help="Enable the Werkzeug Debugger")
@click.option("--threaded", is_flag=True, default=False, help="Multiple threads")
@click.option("--processes", default=1, help="Number of processes")
@click.option("--passthrough_errors", default=False, help="Set this to True to disable the error catching")
@click.option("--noreload", is_flag=True, default=False, help="Reload server on module change?")
@click.pass_context
def run(ctx, host, port, threaded, processes, debug, passthrough_errors, noreload):
    """
    Run the flask application
    """
    gc = ctx.obj["gc"]
    # **options are forwarded to the following function:
    # werkzeug.serving.run_simple(hostname, port, application, use_reloader=False,
    #                             use_debugger=False, use_evalex=True, extra_files=None,
    #                             reloader_interval=1, reloader_type='auto', threaded=False,
    #                             processes=1, request_handler=None, static_files=None,
    #                             passthrough_errors=False, ssl_context=None)
    gc.app.fApp.run(host=host, port=port, debug=debug, processes=processes,
                    threaded=threaded, passthrough_errors=passthrough_errors,
                    use_reloader=(not noreload))


@app.command()
@click.pass_context
def show(ctx):
    """
    Print flask application
    """
    gc = ctx.obj["gc"]
    gc.app.show()


@app.command()
@click.pass_context
def create_specs(ctx):
    """
    Create swagger specs
    """
    gc = ctx.obj["gc"]
    gc.app.createSpecs()
