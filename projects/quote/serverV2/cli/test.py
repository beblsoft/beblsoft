#!/usr/bin/env python3
"""
NAME:
 test.py

DESCRIPTION
 test Command
"""

# ------------------------ IMPORTS ------------------------------------------ #
import unittest
import click


# ------------------------ TEST --------------------------------------------- #
@click.command()
def test():
    """
    Discover and run application unit tests
    """
    # Load specific tests
    # tests = unittest.TestLoader().loadTestsFromNames(
    #     ["tests.routes.v_1_0.test_test"])

    # Load all tests
    tests = unittest.TestLoader().discover(start_dir='app', pattern="*_test.py")

    unittest.TextTestRunner(verbosity=2).run(tests)
