#!/usr/bin/env python3
"""
NAME:
 db.py

DESCRIPTION
 db Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import click


# ----------------------- DATABASE ------------------------------------------ #
@click.group()
def db():
    """Database Control"""
    pass


@db.command()
@click.pass_context
def create(ctx):
    """
    Create Database Tables
    """
    gc = ctx.obj["gc"]
    gc.db.create()


@db.command()
@click.pass_context
def destroy(ctx):
    """
    Destroy Database Tables
    """
    gc = ctx.obj["gc"]
    gc.db.destroy()


@db.command()
@click.option("--nauthors", default=0, help="Number of authors to add to database")
@click.option("--nquotes", default=0, help="Number of quotes to add to database")
@click.option("--sdate", default="01/01/2018", help="Start date. Default=01/01/2018")
@click.option("--edate", default="12/31/2067", help="End date. Default=12/31/2067")
@click.pass_context
def populate(ctx, nauthors, nquotes, sdate, edate):
    """
    Populate Database
    """
    gc = ctx.obj["gc"]
    startDate = datetime.strptime(sdate, "%m/%d/%Y")
    endDate   = datetime.strptime(edate, "%m/%d/%Y")
    gc.db.populate(nAuthors=nauthors, nQuotes=nquotes, startDate=startDate,
                   endDate=endDate)

@db.command()
@click.option("--authors", is_flag=True, default=False, help="Display all authors")
@click.option("--quotes", is_flag=True, default=False, help="Display all quotes")
@click.option("--displaydates", is_flag=True, default=False, help="Display all displayDates")
@click.pass_context
def show(ctx, authors, quotes, displaydates):  # pylint: disable=E0102
    """
    Print Database
    """
    gc = ctx.obj["gc"]
    gc.db.show(authors=authors, quotes=quotes, displayDate=displaydates)


@db.command()
@click.pass_context
def stats(ctx):
    """
    Print database stats
    """
    gc = ctx.obj["gc"]
    gc.db.stats()
