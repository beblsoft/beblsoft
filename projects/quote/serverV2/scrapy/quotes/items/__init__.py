#!/usr/bin/env python
"""
NAME:
 __init__.py

DESCRIPTION
 Items for quotes project Spider
"""

# ------------------------ IMPORTS ------------------------------------------ #
from scrapy import Field, Item
from scrapy.utils.python import flatten
from scrapy.loader import ItemLoader


# ------------------------ XPATH CSS ITEM LOADER CLASS ---------------------- #
class XCItemLoader(ItemLoader):
    """
    Extend ItemLoader to load both xpath and css expressions

    Reference:
      https://stackoverflow.com/questions/31752168/combine-xpath-and-css-selectors-in-item-loader
    """

    def add_xpath_and_css(self, field_name, xpaths, csss, *processors, **kw):
        """
        Filter on xpath and then css
        """
        xpathRets = flatten([self.selector.xpath(xpath) for xpath in xpaths])
        values    = flatten([xpathRet.css(css).extract()
                             for xpathRet in xpathRets for css in csss])
        self.add_value(field_name, values, *processors, **kw)

    def add_css_and_xpath(self, field_name, csss, xpaths, *processors, **kw):
        """
        Filter on css first then xpath
        """
        cssRets = flatten([self.selector.css(css) for css in csss])
        values  = flatten([cssRet.xpath(xpath).extract()
                           for cssRet in cssRets for xpath in xpaths])
        self.add_value(field_name, values, *processors, **kw)
