#!/usr/bin/env python
"""
NAME:
 author.py

DESCRIPTION
 Author Item
"""

# ------------------------ IMPORTS ------------------------------------------ #
from scrapy import Field, Item
from scrapy.loader.processors import TakeFirst, MapCompose
from .processors import filterNonUnicode, strToSingleSpace



# ------------------------ AUTHOR ITEM -------------------------------------- #
class AuthorItem(Item):
    """
    Encapsulate Author Data
    Note: Field names must exactly match Author database fields
    """
    name            = Field(input_processor  = MapCompose(filterNonUnicode, strToSingleSpace),
                            output_processor = TakeFirst())

    # Good Reads
    grURL           = Field(output_processor = TakeFirst())
    grScrapeDate    = Field(output_processor = TakeFirst())

    # BrainyQuote
    bqURL           = Field(output_processor = TakeFirst())
    bqScrapeDate    = Field(output_processor = TakeFirst())

    def __str__(self):
        return u"{}[name={}]".format(
            self.__class__.__name__, self.get("name", None))
