#!/usr/bin/env python
"""
NAME:
 quote.py

DESCRIPTION
 Quote Item
"""

# ------------------------ IMPORTS ------------------------------------------ #
from scrapy import Field, Item
from scrapy.loader.processors import TakeFirst, Join, MapCompose
from .processors import strToInt, filterNonUnicode, strToSingleSpace


# ------------------------ QUOTE ITEM --------------------------------------- #
class QuoteItem(Item):
    """
    Encapsulate Quote Data
    Note: Field names must exactly match Quote database fields
    """
    text            = Field(input_processor  = MapCompose(filterNonUnicode, strToSingleSpace),
                            output_processor = TakeFirst())
    authorName      = Field(input_processor  = MapCompose(filterNonUnicode, strToSingleSpace),
                            output_processor = TakeFirst())
    work            = Field(input_processor  = MapCompose(filterNonUnicode, strToSingleSpace),
                            output_processor = TakeFirst())

    # Good Reads
    grURL           = Field(output_processor = TakeFirst())
    grScrapeDate    = Field(output_processor = TakeFirst())
    grLikes         = Field(input_processor  = MapCompose(strToInt),
                            output_processor = TakeFirst())
    grTags          = Field(output_processor = Join())

    # Brainy Quotes
    bqURL           = Field(output_processor = TakeFirst())
    bqScrapeDate    = Field(output_processor = TakeFirst())
    bqTags          = Field(output_processor = TakeFirst())

    def __str__(self):
        return u"{}[text={} author={}]".format(
            self.__class__.__name__, self.get("text", None),
            self.get("authorName", None))
