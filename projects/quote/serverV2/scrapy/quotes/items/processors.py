#!/usr/bin/env python
"""
NAME:
 processors.py

DESCRIPTION
 Item Processors
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.error.bError import BeblsoftError


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ INPUT PROCESSORS --------------------------------- #
def strToInt(value):
    """
    Convert Unicode to Integer
    """
    if value.isdigit():
        return int(value)
    raise BeblsoftError(msg="Invalid integer string {0}".format(value))


def strToSingleSpace(value):
    """
    Convert multi-spaced string to single-spaced string
    """
    if value is not None:
        return u" ".join(value.split())
    return u""


def filterNonUnicode(value):
    """
    Filter out values that aren't UTF-8
    """
    try:
        value = u"{}".format(value)
    except UnicodeError as e:
        logger.exception(e)
        logger.warning(u"{} not vaild unicode".format(value))
    else:
        return value
