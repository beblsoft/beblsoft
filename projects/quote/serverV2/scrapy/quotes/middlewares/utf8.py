#!/usr/bin/env python
"""
NAME:
 utf8.py

DESCRIPTION
 utf8 Downloader Middleware
"""


# ------------------------ UTF8 RESPONSE MIDDLEWARE ------------------------- #
class UTF8ResponseMiddleware(object): #pylint: disable=R0205
    """
    Downloader to force UTF-8 encoding on all responses
    """
    encoding = "utf-8"

    def process_response(self, request, response, spider):  # pylint: disable=W0613
        """
        Encode response body in new encoding
        """
        newBody = response.text.encode(self.encoding)
        return response.replace(body=newBody, encoding=self.encoding)
