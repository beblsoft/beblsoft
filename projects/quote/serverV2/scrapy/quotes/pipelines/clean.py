#!/usr/bin/env python
"""
NAME:
 clean.py

DESCRIPTION
 Cleaning Pipeline
"""

# ------------------------ IMPORTS ------------------------------------------ #
from scrapy.exceptions import DropItem
from ..items.quote import QuoteItem
from ..items.author import AuthorItem


# ------------------------ CLEAN PIPELINE ----------------------------------- #
class CleanPipeline(object): #pylint: disable=R0205
    """
    Pipeline to sanitize quote and author data
    """

    def __init__(self):
        """
        Initialize object
        """
        pass

    def process_item(self, item, spider):  # pylint: disable=W0613
        """
        Called for every item pipeline component
        """
        # Functions will raise DropItem exception if item should
        # not be returned
        if isinstance(item, QuoteItem):
            self.processQuote(item)
        elif isinstance(item, AuthorItem):
            self.processAuthor(item)
        return item

    def processQuote(self, item): #pylint: disable=R0201
        """
        Process quote item
        """
        requiredFields = ["text", "authorName"]
        CleanPipeline.verifyFields(item, requiredFields)

    def processAuthor(self, item): #pylint: disable=R0201
        """
        Process author item
        """
        requiredFields = ["name"]
        CleanPipeline.verifyFields(item, requiredFields)

    @staticmethod
    def verifyFields(item, requiredFields):
        """
        Verify that all requiredFields are present in item
        Args
          item           - item to check
          requiredFields - list of required fields
        Raises
          DropItem exection if field not present
        """
        for field in requiredFields:
            if field not in item:
                raise DropItem("{} doesn't contain field {}".format(item, field))
