#!/usr/bin/env python
"""
NAME:
 db.py

DESCRIPTION
 Database Pipeline
"""

# ------------------------ IMPORTS ------------------------------------------ #
import copy
import logging
from scrapy.exceptions import DropItem
from sqlalchemy.exc import SQLAlchemyError
from quote.serverV2.db import Database
from quote.serverV2.db.models.author import Author
from quote.serverV2.db.models.quote import Quote
from ..items.quote import QuoteItem
from ..items.author import AuthorItem


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ DB PIPELINE -------------------------------------- #
class DBPipeline(object): #pylint: disable=R0205
    """
    Pipeline to write quotes to database
    """

    def __init__(self, cfg, nCommitRetries=5):
        """
        Initialize object
        Args:
          cfg: quote configuration object
        """
        self.cfg            = cfg
        self.db             = Database(cfg=cfg)
        self.nCommitRetries = nCommitRetries

    def __str__(self):
        return self.__class__.__name__

    @classmethod
    def from_crawler(cls, crawler):
        """
        Instantiate pipeline from crawler
        """
        return cls(cfg=crawler.settings.get("QUOTE_CFG"))

    def process_item(self, item, spider):  # pylint: disable=W0613
        """
        Called for every item pipeline component
        Process author and quote items
        """
        nRetries  = 0
        committed = False
        session   = self.db.session()

        while not committed:
            try:
                if isinstance(item, QuoteItem):
                    self.processQuoteItem(item)
                elif isinstance(item, AuthorItem):
                    self.processAuthorItem(item)
                session.commit()
            except SQLAlchemyError as e:
                session.rollback()
                if nRetries >= self.nCommitRetries:
                    logger.exception(e)
                    raise DropItem(u"{} DB commit failed. tries={}".format(
                        item, nRetries))
                nRetries += 1
            else:
                committed = True
            finally:
                session.close()
        return item

    def processQuoteItem(self, item):
        """
        Process quote item
        Args
          session - db session
          item    - quote item to process
        Note
          Create deep copy of item in case commit fails and needs to be retried
        """
        item   = copy.deepcopy(item)
        author = self.db.getOrCreate(Author, name=item.pop("authorName"))
        quote  = self.db.getOrCreate(Quote, text=item.pop("text"))
        self.db.updateInstance(quote, author=author, **item)

    def processAuthorItem(self, item):
        """
        Process author item
        Args
          session - db session
          item    - quote item to process
        Note
          Create deep copy of item in case commit fails and needs to be retried
        """
        item   = copy.deepcopy(item)
        author = self.db.getOrCreate(Author, name=item.pop("name"))
        self.db.updateInstance(author, **item)
