#!/usr/bin/env python
"""
NAME:
 brainyquote_spider.py

DESCRIPTION
 Brainyquote Spider

PARSING ALGORITHM
  Home Page
  Author List Page (i.e. 'A' authors)
  Author Quote Page
"""

# ------------------------ IMPORTS ------------------------------------------ #
from datetime import datetime
from scrapy import Spider, Request
from quotes.items import XCItemLoader
from quotes.items.quote import QuoteItem
from quotes.items.author import AuthorItem


# ------------------------ BRAINY QUOTE SPIDER CLASS ------------------------ #
class BrainyQuoteSpider(Spider):
    """
    BrainyQuote Spider
    """
    name            = 'brainyquote'
    allowed_domains = ['brainyquote.com']
    start_urls      = ["https://www.brainyquote.com"]

    # ------------------------ HOME PAGE -------------------------- #
    def parse(self, response):
        """
        Parse home page

        Test:
          scrapy parse --spider=brainyquote --callback=parse \
            -d 2 "https://www.brainyquote.com/"

        URLs out:
          https://www.brainyquote.com/authors/a2
        """
        authorRelLinks = response.css('.bq-tn-letters').xpath('.//@href').extract()
        for authorRelLink in authorRelLinks:
            yield Request(response.urljoin(authorRelLink),
                          callback=self.parseAuthorListPage)

    # ------------------------ TEST HOME PAGE --------------------- #
    def test_parse(self, response):
        """
        @url https://www.brainyquote.com
        @returns items 0 0
        @returns requests 26 26
        """
        return self.parse(response)

    # ------------------------ AUTHOR LIST PAGE ------------------- #
    def parseAuthorListPage(self, response):  # pylint: disable=R0201
        """
        Parse list of authors

        Test:
          scrapy parse --spider=brainyquote --callback=parseAuthorListPage \
            -d 2 "https://www.brainyquote.com/authors/a11"

        URLs out:
          https://www.brainyquote.com/authors/a3
          https://www.brainyquote.com/authors/albert_einstein?SPvm=1&vm=l
        """
        # Parse next author list page
        nextAuthorListLink = self.getNextAuthorListPage(response)
        self.logger.info("nextAuthorListLink {}".format(nextAuthorListLink))
        if nextAuthorListLink:
            yield Request(nextAuthorListLink, callback=self.parseAuthorListPage)

        # Parse links to author pages
        authorLinks = response.css('.bq_s .table-bordered').xpath(
            './/a[starts-with(@href, "/authors")]/@href').extract()

        for authorLink in authorLinks:
            yield Request(
                response.urljoin("{}{}".format(authorLink, "?SPvm=1&vm=l")),
                callback=self.parseAuthorPage)

    def getNextAuthorListPage(self, response):  # pylint: disable=R0201
        """
        Return next author list page

        Examples:
          Response for https://www.brainyquote.com/authors/a2
          Returns https://www.brainyquote.com/authors/a3

        Returns:
          Next author list URL
          None if there is no next author list page
        """
        nextAbsLink = None
        nextLink    = response.css('.bq_s').xpath(
            './/a[contains(text(), "Next")]/@href').extract_first()

        if nextLink:
            nextAbsLink = response.urljoin(nextLink)
        return nextAbsLink

    # ------------------------ TEST AUTHOR LIST PAGE -------------- #
    def test_parseAuthorListPage(self, response):
        """
        @url https://www.brainyquote.com/authors/a
        @returns requests 201 201
        """
        return self.parseAuthorListPage(response)

    def test_parseAuthorListPageEnd(self, response):
        """
        @url https://www.brainyquote.com/authors/a11
        @returns requests 153 201
        """
        return self.parseAuthorListPage(response)

    # ------------------------ AUTHOR PAGE ------------------------ #
    def parseAuthorPage(self, response, scrapeQuotes=True, scrapeAuthor=True):  # pylint: disable=R0201
        """
        Parse author page

        TEST:
          scrapy parse --spider=brainyquote --callback=parseAuthorPage \
            "https://www.brainyquote.com/authors/albert_einstein?SPvm=1&vm=l"

        Items Out:
          AuthorItem
          QuoteItem
        """
        authorName = response.xpath(
            '//title/text()').re(r"(.*) Quotes - BrainyQuote")

        if scrapeQuotes:
            quoteSels = response.css('#quotesList')
            quoteSels = quoteSels.xpath('.//div[starts-with(@id, "qpos_")]')
            for quoteSel in quoteSels:
                ql = XCItemLoader(item=QuoteItem(), selector=quoteSel)
                ql.add_css_and_xpath('text', csss=['.b-qt'],
                                     xpaths=['.//text()'])
                ql.add_value('authorName', authorName)
                ql.add_xpath('bqTags', './/a[starts-with(@href, "/topics")]/text()')
                url = quoteSel.css('.b-qt').xpath('.//@href').extract_first()
                ql.add_value('bqURL', response.urljoin(url))
                ql.add_value('bqScrapeDate', datetime.now())
                yield ql.load_item()

        if scrapeAuthor:
            al = XCItemLoader(item=AuthorItem(), selector=response.css('.bq_s'))
            al.add_value('name', authorName)
            al.add_value('bqURL', response.url)
            al.add_value('bqScrapeDate', datetime.now())
            yield al.load_item()

    # ------------------------ TEST AUTHOR PAGE QUOTES ------------ #
    def test_parseAuthorPageForQuotesBalasubramaniam(self, response):
        """
        @url https://www.brainyquote.com/authors/a_balasubramaniam?vm=l
        @returns items 5 5
        @scrapes text authorName bqURL bqScrapeDate bqTags
        """
        return self.parseAuthorPage(response, scrapeQuotes=True,
                                    scrapeAuthor=False)

    def test_parseAuthorPageForQuotesEinstein(self, response):
        """
        @url https://www.brainyquote.com/authors/albert_einstein?SPvm=1&vm=l
        @returns items 26 26
        @scrapes text authorName bqURL bqScrapeDate bqTags
        """
        return self.parseAuthorPage(response, scrapeQuotes=True,
                                    scrapeAuthor=False)

    # ------------------------ TEST AUTHOR PAGE AUTHORS ----------- #
    def test_parseAuthorPageForAuthorAristotle(self, response):
        """
        @url https://www.brainyquote.com/authors/aristotle?vm=l
        @returns items 1 1
        @scrapes name bqURL bqScrapeDate
        """
        return self.parseAuthorPage(response, scrapeQuotes=False,
                                    scrapeAuthor=True)

    def test_parseAuthorPageForAuthorAppleJr(self, response):
        """
        @url https://www.brainyquote.com/authors/r_w_apple_jr?vm=l
        @returns items 1 1
        @scrapes name bqURL bqScrapeDate
        """
        return self.parseAuthorPage(response, scrapeQuotes=False,
                                    scrapeAuthor=True)
