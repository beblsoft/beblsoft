#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME:
 goodreads_spider.py

DESCRIPTION
 Goodreads Spider

PARSING ALGORITHM
   HOME PAGE
     |
     v
   TAG QUOTE PAGES -------
     |                   |
     v                   v
   QUOTES            AUTHOR PAGE
                         |
                         v
                     AUTHOR QUOTE PAGES
                         |
                         v
                     QUOTES Parse quotes, authors
"""

# ------------------------ IMPORTS ------------------------------------------ #
from datetime import datetime
from scrapy import Spider, Request
from quotes.items import XCItemLoader
from quotes.items.quote import QuoteItem
from quotes.items.author import AuthorItem


# ------------------------ GOOD READS SPIDER CLASS -------------------------- #
class GoodReadsSpider(Spider):
    """
    GoodReads Spider
    """
    name            = 'goodreads'
    allowed_domains = ['goodreads.com']
    start_urls      = ["https://www.goodreads.com/quotes"]

    def __init__(self, *args, **kwargs):  # pylint: disable=W0235
        """
        Initialize object
        """
        super(GoodReadsSpider, self).__init__(*args, **kwargs)

    # ------------------------ HOME PAGE -------------------------- #
    def parse(self, response):
        """
        Parse home page for tags

        Test:
          scrapy parse --spider=goodreads --callback=parse -d 2 https://www.goodreads.com/quotes

        URLs out:
          https://www.goodreads.com/quotes/tag/love

        """
        tagList = response.css('.listTagsTwoColumn').xpath('.//a')
        for tag in tagList:
            tagURL = tag.xpath('.//@href').extract_first()
            yield Request(response.urljoin(tagURL), callback=self.parseQuotePage)

    # ------------------------ TEST HOME PAGE --------------------- #
    def test_parse(self, response):
        """
        @url https://www.goodreads.com/quotes
        @returns items 0 0
        @returns requests 30 30
        """
        return self.parse(response)

    # ------------------------ PARSE AUTHOR PAGE ------------------ #
    def parseAuthorPage(self, response):
        """
        Parse author page for quote page links

        Test:
          scrapy parse --spider=goodreads --callback=parseAuthorPage -d 2 https://www.goodreads.com/author/show/1673.Thomas_Jefferson
        """
        authorName = response.css("h1.authorName").xpath(".//text()").extract()
        authorName = " ".join(authorName)
        authorName = authorName.strip()

        xpathStr = "//a[text()='Quotes by {}']/@href".format(authorName)
        quoteURL = response.xpath(xpathStr).extract_first()
        yield Request(response.urljoin(quoteURL), callback=self.parseQuotePage)

    # ------------------------ PARSE AUTHOR PAGE ------------------ #
    def test_parseThomasJeffersonPage(self, response):
        """
        @url https://www.goodreads.com/author/show/1673.Thomas_Jefferson
        @returns requests 1 1
        """
        return self.parseAuthorPage(response)

    # ------------------------ QUOTE PAGE ------------------------- #
    def parseQuotePage(self, response, scrapeNextPage=True,
                       scrapeQuotes=True, scrapeAuthors=True,
                       scrapeAuthorQuotes=True):  # pylint: disable=R0201
        """
        Parse quotes from tag page

        Test:
          scrapy parse -v --spider=goodreads --callback=parseQuotePage -d 2 https://www.goodreads.com/quotes/tag/love > /tmp/out
        """
        # Do the following
        #  - Iterate through all quotes on page. For each quote:
        #    - Scrape author and quote items
        #    - Scrape link to author page
        #  - Scrape next quote page link
        quoteSels = response.css('.quote')
        for quoteSel in quoteSels:
            authorName = quoteSel.xpath(
                './/a[starts-with(@href, "/author")]/text()').extract()
            authorName = " ".join(authorName).lstrip().rstrip()
            authorURL  = quoteSel.xpath(
                './/a[starts-with(@href, "/author")]/@href').extract_first()
            authorURL  = response.urljoin(authorURL)

            if scrapeAuthorQuotes:
                yield Request(authorURL, callback=self.parseAuthorPage)

            if scrapeQuotes:
                ql = XCItemLoader(item=QuoteItem(), selector=quoteSel)
                text = quoteSel.css(".quoteText").xpath(".//text()").extract()
                text = " ".join(text)
                text = text.split(u"―")[0]
                text = text.strip()
                text = text.replace(u"“", "")
                text = text.replace(u"”", "")
                ql.add_value("text", text)
                ql.add_value("authorName", authorName)
                work = quoteSel.xpath(
                    './/a[starts-with(@href, "/work")]/text()').extract_first()
                ql.add_value("work", work)
                likes = quoteSel.css('.quoteFooter').xpath(
                    './/a[contains(text(), "likes")]/text()').re(r"(\d*) likes")
                ql.add_value("grLikes", likes)
                ql.add_css_and_xpath("grTags", csss=['.quoteFooter'],
                                     xpaths=['.//div[contains(text(), "tags:")]/a/text()'])
                url = response.urljoin(quoteSel.css('.quoteFooter').xpath(
                    './/a[@title="View this quote"]/@href').extract_first())
                ql.add_value("grURL", url)
                ql.add_value("grScrapeDate", datetime.now())
                yield ql.load_item()

            if scrapeAuthors:
                al = XCItemLoader(item=AuthorItem(), selector=quoteSel)
                al.add_value("name", authorName)
                al.add_value("grURL", authorURL)
                al.add_value("grScrapeDate", datetime.now())
                yield al.load_item()

        if scrapeNextPage:
            nextTagURL = self.getNextQuotePage(response)
            if nextTagURL:
                yield Request(nextTagURL, self.parseQuotePage)

    def getNextQuotePage(self, response):  # pylint: disable=R0201
        """
        Given a tag page response, return the next tag page URL

        Returns
          Next tag page URL: https://www.goodreads.com/quotes/tag/life?page=2
          None, if next page doesn't exist
        """
        nextURL = None
        nextRelURL = response.css('.next_page').xpath('.//@href').extract_first()
        if nextRelURL:
            nextURL = response.urljoin(nextRelURL)
        return nextURL

    # ------------------------ TEST QUOTE PAGE NEXT --------------- #
    def test_parseLovePage1Next(self, response):
        """
        @url https://www.goodreads.com/quotes/tag/love
        @returns requests 1 1
        """
        return self.parseQuotePage(response, scrapeNextPage=True,
                                   scrapeQuotes=False, scrapeAuthors=False,
                                   scrapeAuthorQuotes=False)

    # ------------------------ TEST QUOTE PAGE LAST --------------- #
    def test_parseLovePage1Last(self, response):
        """
        @url https://www.goodreads.com/quotes/tag/love?page=100
        @returns requests 0 0
        """
        return self.parseQuotePage(response, scrapeNextPage=True,
                                   scrapeQuotes=False, scrapeAuthors=False,
                                   scrapeAuthorQuotes=False)

    # ------------------------ TEST QUOTE PAGE AUTHORS ------------ #
    def test_parseLovePage2Authors(self, response):
        """
        @url https://www.goodreads.com/quotes/tag/love?page=2
        @returns items 30 30
        @scrapes name grURL grScrapeDate
        """
        return self.parseQuotePage(response, scrapeNextPage=False,
                                   scrapeQuotes=False, scrapeAuthors=True,
                                   scrapeAuthorQuotes=False)

    # ------------------------ TEST QUOTE PAGE QUOTES ------------- #
    def test_parseLovePage3Quotes(self, response):
        """
        @url https://www.goodreads.com/quotes/tag/love?page=3
        @returns items 30 30
        @scrapes text authorName grURL grScrapeDate grLikes grTags
        """
        return self.parseQuotePage(response, scrapeNextPage=False,
                                   scrapeQuotes=True, scrapeAuthors=False,
                                   scrapeAuthorQuotes=False)

    # ------------------------ TEST QUOTE PAGE AUTHOR PAGES ------- #
    def test_parseLovePage1AuthorLinks(self, response):
        """
        @url https://www.goodreads.com/quotes/tag/love
        @returns requests 30 30
        """
        return self.parseQuotePage(response, scrapeNextPage=False,
                                   scrapeQuotes=False, scrapeAuthors=False,
                                   scrapeAuthorQuotes=True)
