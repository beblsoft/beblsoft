#!/usr/bin/env python
"""
NAME:
 settings.py

DESCRIPTION
 Settings for quote scraping
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from quote.serverV2.config.localScrapyConfig import LocalScrapyConfig


# ------------------------ SETTINGS ----------------------------------------- #
# ---- QUOTE CONFIGURATION ------------
QUOTE_CFG              = LocalScrapyConfig()

# ---- GENERAL ------------------------
BOT_NAME               = 'quotes'
SPIDER_MODULES         = ['quotes.spiders']
NEWSPIDER_MODULE       = 'quotes.spiders'
ROBOTSTXT_OBEY         = True

# ---- LOGGING ------------------------
LOG_ENABLED            = True
LOG_LEVEL              = logging.INFO
LOG_FILE               = "/tmp/scrapy.log"
LOG_FORMAT             = "%(asctime)s:%(levelname)8s:%(threadName)16s: %(message)s"
LOG_DATEFORMAT         = "%a, %d %b %Y %H:%M:%S"

# ---- DB -----------------------------
DB_RETRY_COUNT         = 5

# ---- CLOSE SPIDER -------------------
CLOSESPIDER_ERRORCOUNT = 0
CLOSESPIDER_ITEMCOUNT  = 0

# ---- PIPELINES ----------------------
# Run from lowest to highest order
ITEM_PIPELINES         = {
    'quotes.pipelines.clean.CleanPipeline': 0,
    'quotes.pipelines.db.DBPipeline': 1,
}

# ---- MIDDLEWARES --------------------
DOWNLOADER_MIDDLEWARES = {
	# 'quotes.middlewares.utf8.UTF8ResponseMiddleware': 0
}
