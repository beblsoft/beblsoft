#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 Quote Server Command Line Interface
"""

# ------------------------ IMPORTS ------------------------------------------ #
from quote.serverV2.cli import main #pylint: disable=W0406


# ------------------------ MAIN --------------------------------------------- #
main()
