#!/usr/bin/env python3
"""
 NAME:
  localScrapyConfig.py

 DESCRIPTION
   Local Scrapy Configuration Functionality
"""


# --------------------------- IMPORTS --------------------------------------- #
from . import Config


# --------------------------- LOCAL SCRAPY CONFIGURATION CLASS -------------- #
class LocalScrapyConfig(Config):
    """
    Local Scrapy Configuration class
    """
    TESTING               = False
    PRODUCTION            = True

    # Database
    DB_NAME               = "quote20180326_055500"

    # Flask Application
    APP_DEBUG             = False
