#!/usr/bin/env python3
"""
 NAME:
  localDevConfig.py

 DESCRIPTION
   Local Development Configuration Functionality
"""


# --------------------------- IMPORTS --------------------------------------- #
from . import Config


# --------------------------- LOCAL DEV CONFIGURATION CLASS ----------------- #
class LocalDevConfig(Config):
    """
    Local Development Configuration class
    """
    TESTING               = True

    # Database
    DB_ECHO               = False
    DB_NAME               = "quoteForgery"

    # Flask Application
    APP_DEBUG             = True
