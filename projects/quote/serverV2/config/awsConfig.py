#!/usr/bin/env python3
"""
 NAME:
  awsConfig.py

 DESCRIPTION
   AWS Configuration Functionality
"""


# --------------------------- IMPORTS --------------------------------------- #
import os
from . import Config


# --------------------------- AWS CONFIGURATION CLASS ----------------------- #
class AWSConfig(Config):
    """
    AWS Configuration class
    """
    PRODUCTION            = True

    # Database
    DB_USER               = os.environ.get("DB_USER", None)
    DB_PASSWORD           = os.environ.get("DB_PASSWORD", None)
    DB_HOST               = os.environ.get("DB_HOST", None)
    DB_NAME               = os.environ.get("DB_NAME", None)
