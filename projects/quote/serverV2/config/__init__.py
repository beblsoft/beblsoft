#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
   Configuration Functionality
"""

# --------------------------- IMPORTS --------------------------------------- #
import getpass


# --------------------------- CONFIGURATION CLASS --------------------------- #
class Config():
    """
    Configuration class
    """
    TESTING               = False
    PRODUCTION            = False

    # Database
    DB_USER               = getpass.getuser()
    DB_PASSWORD           = ""
    DB_HOST               = "localhost"
    DB_NAME               = ""
    DB_ECHO               = False
    DB_CHARSET            = "utf8"

    # Flask Application
    APP_DEBUG             = False
    APP_RESTPLUS_VALIDATE = True

    @property
    def DB_ENGINE(self):
        """
        Return database engine
        """
        return "mysql://{}:{}@{}/{}?charset={}".format(
            self.DB_USER, self.DB_PASSWORD, self.DB_HOST, self.DB_NAME,
            self.DB_CHARSET)
