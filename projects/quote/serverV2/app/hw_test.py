#!/usr/bin/env python3
"""
 NAME:
  hw_test.py

 DESCRIPTION
  Test Hello World Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import unittest
import logging


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class HelloWorldCase(unittest.TestCase):
    """
    Test Hello World
    """

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_helloWorld(self):
        """
        Test hello world
        """
        pass
