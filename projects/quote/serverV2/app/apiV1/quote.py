#!/usr/bin/env python3
"""
 NAME:
  quote.py

 DESCRIPTION
  Quote Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Namespace, Resource, fields  # pylint: disable=E0401
from flask_restplus import reqparse, abort  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from quote.serverV2.db.models.quote import Quote


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
ns     = Namespace("quote", description="Quote Operations")


# ------------------------ QUOTE MODEL -------------------------------------- #
quoteModel = ns.model("Quote", {
    "text": fields.String(
        required    = True,
        description = "Quote Text",
        example     = ("The best and most beautiful things in the world cannot be seen"
                       " or even touched - they must be felt with the heart.")),
    "author": fields.String(
        attribute   = "author.name",
        required    = True,
        description = "Quote Author",
        example     = "Helen Keller")
})


# ------------------------ QUOTE GET PARSER --------------------------------- #
qGetParser = reqparse.RequestParser()
qGetParser.add_argument(
    name     = "type",
    type     = str,
    help     = ("Specify query type. "
                "If byText is specified, text argument must be "
                "supplied. "
                "If byAuthor is specifed, author argument must "
                "be supplied. "),
    location = "args",
    required = True,
    choices  = ["byText", "byAuthor"],
    default  = "byText")
qGetParser.add_argument(
    name     = "text",
    type     = str,
    help     = "Specify text query. Example: 'love'",
    location = "args",
    required = False)
qGetParser.add_argument(
    name     = "author",
    type     = str,
    help     = "Specify quote author. Example: 'Helen Keller'",
    location = "args",
    required = False)


# ------------------------ QUOTE RESOURCE ----------------------------------- #
@ns.route("/")
class QuoteResource(Resource):
    """
    Quote Endpoints
    """
    @ns.marshal_list_with(quoteModel)
    @ns.expect(qGetParser)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get quotes
        """
        db     = current_app.db
        args   = qGetParser.parse_args()
        quotes = []

        if args["type"] == "byText":
            text = args["text"]
            if not text:
                abort(400, "Specify text argument")
            quotes = Quote.getByText(db, text)
        elif args["type"] == "byAuthor":
            author = args["author"]
            if not author:
                abort(400, "Specify author argument")
            quotes = Quote.getByAuthor(db, author)
        return quotes
