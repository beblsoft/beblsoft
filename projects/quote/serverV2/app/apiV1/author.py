#!/usr/bin/env python3
"""
 NAME:
  author.py

 DESCRIPTION
  Author Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Namespace, Resource, fields  # pylint: disable=E0401
from flask_restplus import abort, reqparse  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from quote.serverV2.db.models.author import Author


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
ns     = Namespace('author', description="Author Operations")


# ------------------------ AUTHOR MODEL ------------------------------------- #
authorModel = ns.model("Author", {
    'name': fields.String(
        required    = True,
        description = "Author Name",
        example     = "Helen Keller")
})


# ------------------------ AUTHOR GET PARSER -------------------------------- #
aGetParser = reqparse.RequestParser()
aGetParser.add_argument(
    name     = "type",
    type     = str,
    help     = ("Specify query type. "
                "If byName is specified, name argument must be "
                "supplied. "),
    location = "args",
    required = True,
    choices  = ["byName"],
    default  = "byName")
aGetParser.add_argument(
    name     = "name",
    type     = str,
    help     = "Specify name query. Example: 'hel'",
    location = "args",
    required = False)


# ------------------------ AUTHOR RESOURCE ---------------------------------- #
@ns.route('/')
class AuthorResource(Resource):
    """
    Author Endpoints
    """

    @ns.marshal_list_with(authorModel)
    @ns.expect(aGetParser)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get authors
        """
        db      = current_app.db
        args    = aGetParser.parse_args()
        authors = []

        if args["type"] == "byName":
            name = args["name"]
            if not name:
                abort(400, "Specify string argument")
            authors = Author.getByName(db, name)
        return authors
