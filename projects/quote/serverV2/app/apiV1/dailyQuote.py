#!/usr/bin/env python3
"""
 NAME:
  dailyQuote.py

 DESCRIPTION
  Daily Quote Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
from flask_restplus import Namespace, Resource, fields  # pylint: disable=E0401
from flask_restplus import reqparse, abort  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from quote.serverV2.db.models.quoteDisplayDate import QuoteDisplayDate
from .quote import ns as quoteNS


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
ns     = Namespace("dailyQuote", description="Daily Quote Operations")


# ------------------------ DAILY QUOTE MODEL -------------------------------- #
dailyQuoteModel = ns.model("DailyQuote", {
    "date": fields.Date(
        required    = True,
        description = "Quote Display Date"),
    "quote": fields.Nested(
        quoteNS.models["Quote"],
        description = "Quote Data")
})


# ------------------------ DAILY QUOTE GET PARSER --------------------------- #
dqGetParser = reqparse.RequestParser()
dqGetParser.add_argument(
    name     = "startDate",
    type     = str,
    help     = "Specify quote startDate in ISO 8601. Example: '2018-02-04'",
    location = "args",
    required = True,
    default  = None)
dqGetParser.add_argument(
    name     = "endDate",
    type     = str,
    help     = "Specify quote endDate in ISO 8601. Example: '2018-02-10'",
    location = "args",
    required = True,
    default  = None)


# ------------------------ DAILY QUOTE RESOURCE ----------------------------- #
@ns.route("/")
class DailyQuoteResource(Resource):
    """
    Daily Quote Endpoints
    """
    @ns.marshal_list_with(dailyQuoteModel)
    @ns.expect(dqGetParser)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get daily quotes
        """
        db       = current_app.db
        args     = dqGetParser.parse_args()
        quoteDDs = []

        try:
            dateFormat = "%Y-%m-%d"
            startDate  = datetime.strptime(args["startDate"], dateFormat)
            endDate    = datetime.strptime(args["endDate"], dateFormat)
            quoteDDs   = QuoteDisplayDate.getByDate(
                db, startDate=startDate, endDate=endDate)
        except Exception as _:  # pylint: disable=W0703
            abort(400, "Invalid date format")
        return quoteDDs
