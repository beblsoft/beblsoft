{
    "swagger": "2.0",
    "basePath": "/api/v1",
    "paths": {
        "/author/": {
            "get": {
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/Author"
                            }
                        }
                    }
                },
                "summary": "Get authors",
                "operationId": "get_author_resource",
                "parameters": [
                    {
                        "name": "type",
                        "in": "query",
                        "type": "string",
                        "required": true,
                        "description": "Specify query type. If byName is specified, name argument must be supplied. ",
                        "default": "byName",
                        "enum": [
                            "byName"
                        ],
                        "collectionFormat": "multi"
                    },
                    {
                        "name": "name",
                        "in": "query",
                        "type": "string",
                        "description": "Specify name query. Example: 'hel'"
                    },
                    {
                        "name": "X-Fields",
                        "in": "header",
                        "type": "string",
                        "format": "mask",
                        "description": "An optional fields mask"
                    }
                ],
                "tags": [
                    "author"
                ]
            }
        },
        "/dailyQuote/": {
            "get": {
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/DailyQuote"
                            }
                        }
                    }
                },
                "summary": "Get daily quotes",
                "operationId": "get_daily_quote_resource",
                "parameters": [
                    {
                        "name": "startDate",
                        "in": "query",
                        "type": "string",
                        "required": true,
                        "description": "Specify quote startDate in ISO 8601. Example: '2018-02-04'"
                    },
                    {
                        "name": "endDate",
                        "in": "query",
                        "type": "string",
                        "required": true,
                        "description": "Specify quote endDate in ISO 8601. Example: '2018-02-10'"
                    },
                    {
                        "name": "X-Fields",
                        "in": "header",
                        "type": "string",
                        "format": "mask",
                        "description": "An optional fields mask"
                    }
                ],
                "tags": [
                    "dailyQuote"
                ]
            }
        },
        "/quote/": {
            "get": {
                "responses": {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/Quote"
                            }
                        }
                    }
                },
                "summary": "Get quotes",
                "operationId": "get_quote_resource",
                "parameters": [
                    {
                        "name": "type",
                        "in": "query",
                        "type": "string",
                        "required": true,
                        "description": "Specify query type. If byText is specified, text argument must be supplied. If byAuthor is specifed, author argument must be supplied. ",
                        "default": "byText",
                        "enum": [
                            "byText",
                            "byAuthor"
                        ],
                        "collectionFormat": "multi"
                    },
                    {
                        "name": "text",
                        "in": "query",
                        "type": "string",
                        "description": "Specify text query. Example: 'love'"
                    },
                    {
                        "name": "author",
                        "in": "query",
                        "type": "string",
                        "description": "Specify quote author. Example: 'Helen Keller'"
                    },
                    {
                        "name": "X-Fields",
                        "in": "header",
                        "type": "string",
                        "format": "mask",
                        "description": "An optional fields mask"
                    }
                ],
                "tags": [
                    "quote"
                ]
            }
        }
    },
    "info": {
        "title": "QuotableJoy API",
        "version": "1.0",
        "description": "API to show authors and quotes"
    },
    "produces": [
        "application/json"
    ],
    "consumes": [
        "application/json"
    ],
    "tags": [
        {
            "name": "default",
            "description": "Default namespace"
        },
        {
            "name": "quote",
            "description": "Quote Operations"
        },
        {
            "name": "dailyQuote",
            "description": "Daily Quote Operations"
        },
        {
            "name": "author",
            "description": "Author Operations"
        }
    ],
    "definitions": {
        "Quote": {
            "required": [
                "author",
                "text"
            ],
            "properties": {
                "text": {
                    "type": "string",
                    "description": "Quote Text",
                    "example": "The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart."
                },
                "author": {
                    "type": "string",
                    "description": "Quote Author",
                    "example": "Helen Keller"
                }
            },
            "type": "object"
        },
        "DailyQuote": {
            "required": [
                "date"
            ],
            "properties": {
                "date": {
                    "type": "string",
                    "format": "date",
                    "description": "Quote Display Date"
                },
                "quote": {
                    "description": "Quote Data",
                    "$ref": "#/definitions/Quote"
                }
            },
            "type": "object"
        },
        "Author": {
            "required": [
                "name"
            ],
            "properties": {
                "name": {
                    "type": "string",
                    "description": "Author Name",
                    "example": "Helen Keller"
                }
            },
            "type": "object"
        }
    },
    "responses": {
        "ParseError": {
            "description": "When a mask can't be parsed"
        },
        "MaskError": {
            "description": "When any error occurs on mask"
        }
    }
}