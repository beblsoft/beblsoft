#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  API V1 Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import os
import pprint
import logging
import json
from flask import Blueprint, jsonify, request
from flask_restplus import Api, apidoc  # pylint: disable=E0401
from .quote import ns as quoteNS
from .dailyQuote import ns as dailyQuoteNS
from .author import ns as authorNS


# -------------------------- GLOBALS ---------------------------------------- #
logger  = logging.getLogger(__name__)
bp      = Blueprint("apiV1", __name__)
api     = Api(bp,
              title       = "QuotableJoy API",
              version     = "1.0",
              description = "API to show authors and quotes")

# Add all namespaces here
api.add_namespace(quoteNS)
api.add_namespace(dailyQuoteNS)
api.add_namespace(authorNS)


# -------------------------- API SPEC ROUTE --------------------------------- #
@bp.route("/spec")
def spec():
    """
    Return swagger JSON spec
    """
    print(pprint.pformat(request.__dict__))
    return jsonify(api.__schema__)


# -------------------------- CREATE SWAGGER SPEC ---------------------------- #
def createSpec(fApp, dirName=os.path.dirname(os.path.realpath(__file__)),
               fileName="swagger.json", indent=4):
    """
    Create Swagger Spec
    Args
      fApp: Flask application
      dirName: Directory where to create the swagger spec
      fileName: Swagger Spec file name
    """
    pathName = "{}/{}".format(dirName, fileName)
    with open(pathName, "w") as f:
        with fApp.test_request_context("/"):
            f.write(json.dumps(api.__schema__, indent=indent))
