#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Application Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
import pprint
from flask import Flask, current_app
from flask_cors import CORS
from quote.serverV2.db import Database
from . import apiV1


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- APPLICATION FACTORY ---------------------------- #
class Application():
    """
    Application class
    """

    def __init__(self, cfg):
        """
        Initialize object
        Args
          cfg:
            Configuration object
        """
        self.cfg  = cfg
        self.fApp = None
        self.init_app()
        self.init_db()
        self.init_apis()

    def init_app(self):
        """
        Init flask application
        """
        self.fApp       = Flask(__name__)
        self.fApp.debug = self.cfg.APP_DEBUG
        CORS(self.fApp)
        # self.fApp.config['SERVER_NAME'] = ""
        self.fApp.config['RESTPLUS_VALIDATE'] = self.cfg.APP_RESTPLUS_VALIDATE

    def init_db(self):
        """
        Init Database
        """
        db           = Database(cfg = self.cfg)
        self.fApp.db = db

        @self.fApp.teardown_appcontext
        def shutdownDBSession(exception=None):  # pylint: disable=W0612,W0613
            """
            Shutdown Database session
            """
            db.session.remove()

    def init_apis(self):
        """
        Init APIs
        """
        self.fApp.register_blueprint(apiV1.bp, url_prefix="/api/v1")

    def show(self):
        """
        Print flask application state
        """
        logger.info(pprint.pformat(self.fApp.__dict__))

    def createSpecs(self): #pylint: disable=R0201
        """
        Generate swagger specs
        """
        apiV1.createSpec(fApp=self.fApp)
