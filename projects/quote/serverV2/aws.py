#!/usr/bin/env python3
"""
 NAME:
  aws.py

DESCRIPTION
 AWS Quote Server Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from quote.serverV2.config.awsConfig import AWSConfig
from quote.serverV2.app import Application


# ----------------------- GLOBALS ------------------------------------------- #
cfg    = AWSConfig()
app    = Application(cfg=cfg)
logger = logging.getLogger(__name__)
fApp   = app.fApp
