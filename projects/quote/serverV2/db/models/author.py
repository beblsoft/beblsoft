#!/usr/bin/env python3
"""
 NAME
  author.py

 DESCRIPTION
  Author Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from sqlalchemy import Column, String, Integer, DateTime
from sqlalchemy.orm import relationship
import forgery_py
from . import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- AUTHOR CLASS -------------------------------------- #
class Author(base):
    """
    Author Table
    Relationships
      Quote-Author : Many-to-One
    """
    __tablename__ = "author"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(256), unique=True)
    creationDate  = Column(DateTime, default=datetime.utcnow())
    quotes        = relationship("Quote", back_populates="author")

    # Good Reads
    grURL         = Column(String(512))
    grScrapeDate  = Column(DateTime)

    # BrainyQuotes
    bqURL         = Column(String(512))
    bqScrapeDate  = Column(DateTime)

    def __repr__(self):
        return "Author[id={}, name={}]".format(self.id, self.name)

    # --------------------- POPULATION -------------------------------------- #
    @staticmethod
    def populateFake(db, nAuthors=10):
        """
        Add fake authors to the database
        Args
          nAuthors: number of fake authors to add to the database
        """
        for _ in range(nAuthors):
            name      = forgery_py.name.full_name()
            shortName = name.replace(" ", "")
            author = Author(
                name         = name,
                grURL        = "www.goodreads.com/quotes/authors/show/{}".format(
                    shortName),
                grScrapeDate = datetime.now(),
                bqURL        = "https://www.brainyquote.com/authors/{}?vm=l".format(
                    shortName),
                bqScrapeDate = datetime.now())
            db.session.add(author)
            db.session.commit()


    # --------------------- RETRIEVAL --------------------------------------- #
    @staticmethod
    def getAll(db):
        """
        Return list of all authors
        """
        return db.session.query(Author).all()

    @staticmethod
    def getByName(db, name, limit=40):
        """
        Return list of authors by string
        Args
          name: Author Name. Ex "He" or "Helen"
        Returns
          List of matching authors
        """
        queryStr = "%{}%".format(name)
        return db.session.query(Author).filter(
            Author.name.ilike(queryStr)).limit(limit).all()
