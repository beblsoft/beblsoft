#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Models Functionality
"""


# ----------------------------- IMPORTS 1 ----------------------------------- #
from sqlalchemy.ext.declarative import declarative_base


# ----------------------------- GLOBALS ------------------------------------- #
base = declarative_base()


# ----------------------------- IMPORTS 2 ----------------------------------- #
from . import author #pylint: disable=C0413
from . import quote #pylint: disable=C0413
from . import quoteDisplayDate  #pylint: disable=C0413
