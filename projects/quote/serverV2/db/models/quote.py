#!/usr/bin/env python3
"""
 NAME
  quote.py

 DESCRIPTION
  Quote Model
"""

# ----------------------------- IMPORTS ------------------------------------- #
import random
import logging
from datetime import datetime
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, Text, desc
from sqlalchemy.orm import relationship
import forgery_py
from . import base


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- QUOTE CLASS --------------------------------- #
class Quote(base):
    """
    Quote Table
    Relationships
      Quote-Author           : Many-To-One
      Quote-QuoteDisplayDate : One-To-Many

    text Column note:
      Since text is a unique column, it needs a size parameter.
      Therefore, we force it to be a string.
    """
    __tablename__ = "quote"
    id            = Column(Integer, primary_key=True)
    text          = Column(String(length=3072), unique=True)
    authorId      = Column(Integer, ForeignKey("author.id"))
    author        = relationship("Author", back_populates="quotes")
    work          = Column(Text)
    creationDate  = Column(DateTime, default=datetime.utcnow())
    displayDates  = relationship("QuoteDisplayDate", back_populates="quote")

    # Good Reads
    grURL         = Column(String(1024))
    grScrapeDate  = Column(DateTime)
    grLikes       = Column(Integer)
    grTags        = Column(Text)

    # BrainyQuotes
    bqURL         = Column(String(1024))
    bqScrapeDate  = Column(DateTime)
    bqTags        = Column(Text)

    def __repr__(self):
        return "Quote[id={}, text='{}', author={} grURL={} grLikes={} grTags='{}']".format(
            self.id, self.text, self.author.name, self.grURL, self.grLikes, self.grTags)

    # --------------------- POPULATION -------------------------------------- #
    @staticmethod
    def populateFake(db, nQuotes=100):
        """
        Add fake quotes to the database
        Args
          nQuotes: number of fake quotes to add to the database
        """
        from .author import Author

        authors = db.session.query(Author).all()
        for _ in range(nQuotes):
            text      = forgery_py.lorem_ipsum.sentences(quantity=5, as_list=False)
            shortText = text.replace(" ", "")[:50]
            quote = Quote(
                text         = text,
                author       = random.choice(authors),
                work         = forgery_py.lorem_ipsum.title(),
                grURL        = "https://www.goodreads.com/quote/{}".format(
                    shortText),
                grScrapeDate = datetime.now(),
                grLikes      = random.randint(0, 100000),
                grTags       = forgery_py.lorem_ipsum.words().replace(" ", ","))
            db.session.add(quote)
            db.session.commit()

    # --------------------- RETRIEVAL --------------------------------------- #
    @staticmethod
    def getAll(db):
        """
        Return list of all quotes
        """
        return db.session.query(Quote).all()

    @staticmethod
    def getByText(db, text, limit=40):
        """
        Return list of quotes by string
        Args
          text: Quote string. Ex "Lov" or "I dreamed a dream"
          limit: Max number of quotes to return
        """
        queryStr = "%{}%".format(text)
        return db.session.query(Quote).filter(
            Quote.text.ilike(queryStr)).order_by(
            desc(Quote.grLikes)).limit(limit).all()

    @staticmethod
    def getByAuthor(db, author, limit=40):
        """
        Return list of quotes by author
        Args
          author: Author string. Ex "He" or "Helen Keller"
          limit: Max number of quotes to return
        Returns
          List of quotes matching author string
        """
        from .author import Author

        queryStr = "%{}%".format(author)
        return db.session.query(Quote).join(Author).filter(
            Author.name.ilike(queryStr)).order_by(
            desc(Quote.grLikes)).limit(limit).all()
