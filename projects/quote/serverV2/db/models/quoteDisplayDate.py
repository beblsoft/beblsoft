#!/usr/bin/env python3
"""
 NAME
  quoteDisplayDate.py

 DESCRIPTION
  Quote Display Date Model
"""


# ----------------------------- IMPORTS ------------------------------------- #
import pprint  # pylint: disable=W0611
import logging
import random
from datetime import timedelta
from sqlalchemy import Column, Integer, DateTime, ForeignKey, desc
from sqlalchemy import or_
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import func
from . import base


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- QUOTE DISPLAY DATE CLASS -------------------- #
class QuoteDisplayDate(base):
    """
    Quote Display Date Table
    Relationships
      Quote-QuoteDisplayDate : One-To-Many
    """
    __tablename__ = "quote_display_date"
    id            = Column(Integer, primary_key=True)
    date          = Column(DateTime, unique=True)
    quoteId       = Column(Integer, ForeignKey("quote.id"))
    quote         = relationship("Quote", back_populates="displayDates")

    def __repr__(self):
        return "QuoteDisplayDate[id={} date={} quote={}']".format(
            self.id, self.date, self.quote)

    # --------------------- POPULATION -------------------------------------- #
    @staticmethod
    def populate(db, startDate, endDate, maxChars=200, minLikes=5000):
        """
        Populate quote display date table
        Args
          startDate:
            datetime object at which to start adding quotes
          endDate:
            datetime object at which to stop adding quotes
          minLikes:
            minimum number of good reads likes
          maxChars:
            maximum number of characters to allow
        """
        from .quote import Quote

        days      = (endDate - startDate).days
        quotes    = db.session.query(Quote).filter(
            Quote.grLikes > minLikes).filter(
            func.length(Quote.text) <= maxChars).filter(or_(
                Quote.grTags.ilike("%{}%".format("love")),
                Quote.grTags.ilike("%{}%".format("hope")),
                Quote.grTags.ilike("%{}%".format("inspirational")),
                Quote.grTags.ilike("%{}%".format("joy")),
                Quote.grTags.ilike("%{}%".format("optimism")),
                Quote.grTags.ilike("%{}%".format("wisdom")),
                Quote.grTags.ilike("%{}%".format("happiness")),
                Quote.grTags.ilike("%{}%".format("wish")),
                Quote.grTags.ilike("%{}%".format("friendship")),
                Quote.grTags.ilike("%{}%".format("life-lessons")))).order_by(
            desc(Quote.grLikes)).all()
        if not quotes:
            quotes = db.session.query(Quote).all() #ForgeryPy Case

        print("lenquotes={}".format(len(quotes)))
        db.session.query(QuoteDisplayDate).delete()
        qddList = []
        for day in range(days):
            date = startDate + timedelta(days=day)
            qdd  = QuoteDisplayDate(
                date  = date,
                quote = random.choice(quotes))
            qddList.append(qdd)
        db.session.add_all(qddList)
        db.session.commit()

    # --------------------- RETRIEVAL --------------------------------------- #
    @staticmethod
    def getByDate(db, startDate, endDate):
        """
        Return list of quotes by date
        Args
          startDate: Start datetime object
          endDate: End datetime object
        """
        from .quote import Quote

        return db.session.query(QuoteDisplayDate).join(Quote).filter(
            QuoteDisplayDate.date >= startDate,
            QuoteDisplayDate.date <= endDate).order_by(
            QuoteDisplayDate.date).all()
