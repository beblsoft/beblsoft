#!/usr/bin/env python3
"""
 NAME
  database.py

 DESCRIPTION
  Database Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import pprint
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.sql.expression import func
from base.bebl.python.log.bLogFunc import logFunc
from . import models
from .models.author import Author
from .models.quote import Quote
from .models.quoteDisplayDate import QuoteDisplayDate


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- DATABASE CLASS ------------------------------ #
class Database():
    """
    Database class
    Encapsulate all database access details
    """

    def __init__(self, cfg):
        """
        Initialize object

        Args
          cfg:
            Configuration object
        """
        self.engine       = create_engine(cfg.DB_ENGINE, echo=cfg.DB_ECHO)
        sessionMaker      = sessionmaker(autocommit=False, autoflush=False,
                                         bind=self.engine)
        self.session      = scoped_session(sessionMaker)
        models.base.query = self.session.query_property()


    # --------------------- CREATE/READ/UPDATE/DELETE ----------------------- #
    @logFunc()
    def create(self):
        """
        Create all database tables
        """
        models.base.metadata.create_all(self.engine)

    @logFunc()
    def destroy(self):
        """
        Destroy all database tables
        """
        models.base.metadata.drop_all(self.engine)

    @logFunc()
    def populate(self, nAuthors=0, nQuotes=0, startDate=None,
                 endDate=None):
        """
        Populate database
        Args
          nAuthors:
            Number of fake authors to add
            Ex. 10
          nQuotes:
            Number of fake quotes to add
            Ex. 100
          startDate:
            Date to start daily quote
          endDate:
            Date to end daily quote
            Ex. datetime(2017, 1, 1)
        """
        if nAuthors > 0:
            Author.populateFake(db=self, nAuthors=nAuthors)
        if nQuotes > 0:
            Quote.populateFake(db=self, nQuotes=nQuotes)
        if startDate and endDate:
            QuoteDisplayDate.populate(db=self, startDate=startDate, endDate=endDate)

    @logFunc()
    def show(self, authors=True, quotes=True, displayDate=True):
        """
        Show database contents
        Args
          authors: If True, print authors
          quotes: If True, print quotes
          displayDate: If True, print quote display dates
        """
        if authors:
            authors = self.session.query(Author).all()
            logger.info(pprint.pformat(authors))

        if quotes:
            quotes = self.session.query(Quote).all()
            logger.info(pprint.pformat(quotes))

        if displayDate:
            dds = self.session.query(QuoteDisplayDate).all()
            for dd in dds:
                logger.info("Day={} Quote={}".format(dd.date, dd.quote.text))

    @logFunc()
    def stats(self):
        """
        Dump relevant database stats
        """
        nQuotes      = self.session.query(Quote).count()
        nAuthors     = self.session.query(Author).count()
        nDailyQuotes = self.session.query(QuoteDisplayDate).count()

        logger.info("nQuotes: {}".format(nQuotes))
        logger.info("nAuthors: {}".format(nAuthors))
        logger.info("nDailyQuotes: {}".format(nDailyQuotes))

    # --------------------- MODEL/INSTANCE FUNTIONS -------------------------- #
    @logFunc(logArgs=False, logRval=False)
    def getOrCreate(self, model, **kwargs):
        """
        Get or create a particular model instance
        Args
          model:
            Database model.
            Ex. Author
          kwargs:
            Dictionary of keys to search query model

        URL:
          https://stackoverflow.com/questions/2546207/does-sqlalchemy-have-an-equivalent-of-djangos-get-or-create
        """
        instance = self.session.query(model).filter_by(**kwargs).one_or_none()
        if not instance:
            params   = dict((k, v)
                            for k, v in kwargs.iteritems())  # pylint: disable=E1101
            instance = model(**params)
            self.session.add(instance)
        return instance

    @logFunc(logArgs=False, logRval=False)
    def updateInstance(self, instance, setNoneValues=False, **kwargs):  # pylint: disable=R0201
        """
        Update database model instance fields
        Args
          instance:
            Database model instance
          setNoneValues:
            If True, update instance with None values in kwargs
          kwargs:
            Dictionary of attributes to update instance
        """
        for k, v in kwargs.iteritems():  # pylint: disable=E1101
            if not setNoneValues and v is None:
                pass
            else:
                setattr(instance, k, v)
