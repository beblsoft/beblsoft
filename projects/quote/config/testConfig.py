#!/usr/bin/env python3
"""
NAME:
 testConfig.py

DESCRIPTION
 Test Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from . import Config


# ------------------------ TEST CONFIG OBJECT ------------------------------- #
class TestConfig(Config):
    """
    Test Configuration Object
    """

    def __init__(self, **kwargs):
        super().__init__(name="Test", **kwargs)

    @property
    def dbName(self):
        return "quote20180326_055500"

    @property
    def apiDomain(self):
        return "{}api.{}".format(self.lName, self.domainName)

    @property
    def clientDomains(self):
        return ["{}.{}".format(self.lName, self.bHostedZone.domainName),
                self.vueBucketName]

    @property
    def vueBucketName(self):
        return "{}www.{}".format(self.lName, self.bHostedZone.domainName)
