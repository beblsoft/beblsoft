#!/usr/bin/env python3
"""
NAME:
 productionConfig.py

DESCRIPTION
 Production Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from . import Config


# ------------------------ PRODUCTION CONFIG OBJECT ------------------------- #
class ProductionConfig(Config):
    """
    Production Configuration Object
    """

    def __init__(self, **kwargs):
        super().__init__(name="Prod", **kwargs)

    @property
    def dbName(self):
        return "quote20180326_055500"

    @property
    def apiDomain(self):
        return "api.{}".format(self.domainName)

    @property
    def clientDomains(self):
        return ["{}".format(self.bHostedZone.domainName),
                self.vueBucketName]

    @property
    def vueBucketName(self):
        return "www.{}".format(self.bHostedZone.domainName)
