#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import os
import abc
from pathlib import Path
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.S3.bBucket import BeblsoftBucket
from base.aws.python.EC2.bVPC import BeblsoftVPC
from base.aws.python.EC2.bSubnet import BeblsoftSubnet
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bInternetGateway import BeblsoftInternetGateway
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.Lambda.bPackage import BeblsoftLambdaPackage
from base.aws.python.Flask.bDeployment import BeblsoftFlaskDeployment
from base.aws.python.Vue.bDeployment import BeblsoftVueDeployment
from base.aws.python.Route53.bHostedZone import BeblsoftHostedZone
from base.aws.python.RDS.bSubnetGroup import BeblsoftDatabaseSubnetGroup
from base.aws.python.RDS.bDatabaseInstance import BeblsoftDatabaseInstance
from base.aws.python.ACM.bCertificate import BeblsoftACMCertificate
from . import secrets #pylint: disable=W0406


# ------------------------ CONFIG OBJECT ------------------------------------ #
class Config(abc.ABC):
    """
    Abstract Base Class Configuration Object
    """

    def __init__(self, name):
        """
        Initialize Object
        Args
          name:
            Configuration name
            Ex. "test" or "prod"
        """
        # ------------- PATHS, NAMES ---------------------------------------- #
        self.name                 = name
        self.lName                = name.lower()
        self.uName                = name.upper()
        self.beblsoftDir          = Path(__file__).parents[3]
        self.baseDir              = "{}/base".format(self.beblsoftDir)
        self.projectsDir          = Path(__file__).parents[2]
        self.quoteDir             = Path(__file__).parents[1]
        self.serverV2Dir          = "{}/serverV2".format(self.quoteDir)
        self.serverV2RelDir       = self.serverV2Dir.split("beblsoft/projects/")[1]  # quote/serverV2
        self.clientV2Dir          = "{}/clientV2".format(self.quoteDir)
        self.configDir            = Path(__file__).parents[0]
        self.templateDir          = "{}/templates".format(self.configDir)
        self.mySQLLib             = "/usr/lib/x86_64-linux-gnu/libmysqlclient.so.20"
        self.domainName           = "quotablejoy.com"
        self.privDataBucketName   = "quote-private-data"

        # ------------- BASE OBJECTS ---------------------------------------- #
        self.bRegion              = BeblsoftRegion(name="us-east-1")
        self.bPrivDataBucket      = BeblsoftBucket(
            name                          = self.privDataBucketName)
        self.bCertificate         = BeblsoftACMCertificate(
            name                          = "QuoteCert",
            domainName                    = "*.{}".format(self.domainName),
            additionalDomains             = [self.domainName])
        self.bHostedZone          = BeblsoftHostedZone(
            domainName                    = self.domainName)

        # ------------- NETWORK --------------------------------------------- #
        self.bVPC                 = BeblsoftVPC(
            name                          = "Quote{}VPC".format(self.name),
            cidrBlock                     = "10.0.0.0/16",
            enableDnsHostnames            = True,
            enableDnsSupport              = True)
        self.bInternetGateway     = BeblsoftInternetGateway(
            name                          = "Quote{}InternetGateway".format(self.name),
            bVPC                          = self.bVPC)
        self.bSubnet1             = BeblsoftSubnet(
            name                          = "Quote{}Subnet1".format(self.name),
            cidrBlock                     = "10.0.0.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "{}a".format(self.bRegion.name))
        self.bSubnet2             = BeblsoftSubnet(
            name                          = "Quote{}Subnet2".format(self.name),
            cidrBlock                     = "10.0.1.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "{}b".format(self.bRegion.name))
        self.bSubnet3             = BeblsoftSubnet(
            name                          = "Quote{}Subnet3".format(self.name),
            cidrBlock                     = "10.0.2.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "{}c".format(self.bRegion.name))
        self.bSubnet4             = BeblsoftSubnet(
            name                          = "Quote{}Subnet4".format(self.name),
            cidrBlock                     = "10.0.3.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "{}d".format(self.bRegion.name))
        self.bSecurityGroup       = BeblsoftSecurityGroup(
            name                          = "Quote{}SecurityGroup".format(self.name),
            bVPC                          = self.bVPC,
            description                   = "Quote {} Security Group".format(self.name))

        # ------------- DATABASE -------------------------------------------- #
        self.dbUser               = "jbensson"
        self.dbPassword           = getattr(secrets, "AWSQuote{}DB".format(self.name), None)
        self.bDBSecurityGroup     = BeblsoftSecurityGroup(
            name                          = "Quote{}DBSecurityGroup".format(self.name),
            bVPC                          = self.bVPC,
            description                   = "Quote {} DB Security Group".format(self.name))
        self.bDBSubnetGroup       = BeblsoftDatabaseSubnetGroup(
            name                          = "Quote{}DBSubnetGroup".format(self.name),
            description                   = "Quote DB Subnet group",
            bSubnets                      = [self.bSubnet3, self.bSubnet4])
        self.bDBInstance          = BeblsoftDatabaseInstance(
            name                          = "Quote{}".format(self.name),
            instanceId                    = "Quote{}DBInstance".format(self.name),
            instanceClass                 = "db.t2.micro",
            engine                        = "mysql",
            engineVersion                 = "5.7.19",
            port                          = 3306,  # MySQL default
            masterUsername                = self.dbUser,
            masterUserPassword            = self.dbPassword,
            bSecurityGroup                = self.bDBSecurityGroup,
            bDatabaseSubnetGroup          = self.bDBSubnetGroup,
            allocatedStorageGB            = 5,  # GB,
            storageType                   = "standard",
            backupRetentionPeriod         = 0,  # Disable automated backups
            multiAZ                       = False,
            publiclyAccessible            = True,
            skipFinalSnapshot             = True)

        # ------------- FLASK DEPLOYMENT ------------------------------------ #
        self.bLFIAMRole           = BeblsoftIAMRole(
            name                          = "Quote{}FlaskTP".format(self.name),
            trustPolicyPath               = "{}/flaskIAMRoleTrustPolicy.json".format(self.templateDir))
        self.bLFIAMRolePolicy     = BeblsoftIAMRolePolicy(
            name                          = "Quote{}FlaskRP".format(self.name),
            bIAMRole                      = self.bLFIAMRole,
            permissionPolicyPath          = "{}/flaskIAMRolePermissionPolicy.json".format(self.templateDir))
        self.bLambdaPackage       = BeblsoftLambdaPackage(
            bucketName                    = self.privDataBucketName,
            bucketKey                     = "Quote{}Server.zip".format(self.name),
            fromToPaths                   = [{"fromPath": "{}/*".format(self.serverV2Dir),
                                              "toPath": self.serverV2RelDir},
                                             {"fromPath": "{}/venv/lib/python3.6/site-packages/*".format(self.serverV2Dir),
                                              "toPath": "."},
                                             {"fromPath": self.mySQLLib,
                                              "toPath": "."},
                                             {"fromPath": "{}/*".format(self.baseDir),
                                              "toPath": "base"}],
            buildDir                      = "/tmp/Quote{}FlaskBuild".format(self.name),
            bRegion                       = self.bRegion)
        self.bFlaskDeployment     = BeblsoftFlaskDeployment(
            bHostedZone                   = self.bHostedZone,
            domainName                    = self.apiDomain,
            bCertificate                  = self.bCertificate,
            name                          = "Quote{}FlaskDeployment".format(self.name),
            appModObjPath                 = "quote.serverV2.aws:fApp",
            bLambdaPackage                = self.bLambdaPackage,
            bIAMRole                      = self.bLFIAMRole,
            bSubnets                      = [self.bSubnet1,
                                             self.bSubnet2],
            bSecurityGroups               = [self.bSecurityGroup],
            envDictFunc                   = lambda: {
                                            "DB_USER": self.dbUser,
                                            "DB_PASSWORD" : self.dbPassword,
                                            "DB_HOST" : self.bDBInstance.domainName,
                                            "DB_NAME" : self.dbName,
                                            },
            description                   = "Quote {} Lambda Flask".format(self.name),
            logLevel                      = logging.NOTSET,
            timeoutS                      = 20)

        # ------------- VUE DEPLOYMENT -------------------------------------- #
        self.bVueDeployment       = BeblsoftVueDeployment(
            name                          = "Quote{}VueDeployment".format(name),
            codeDir                       = self.clientV2Dir,
            bucketName                    = self.vueBucketName,
            bHostedZone                   = self.bHostedZone,
            domainList                    = self.clientDomains,
            bCertificate                  = self.bCertificate,
            autoGenDictFunc               = lambda: {
                                             "APIDomain" :
                                             "https://{}/api/v1".format(self.apiDomain)
                                             })


    # ------------- ABSTRACT PROPERTIES ------------------------------------- #
    @property
    @abc.abstractmethod
    def dbName(self):
        """
        Name of database within instance
        """
        pass

    @property
    @abc.abstractmethod
    def apiDomain(self):
        """
        Return flask api domain
        """
        pass

    @property
    @abc.abstractmethod
    def clientDomains(self):
        """
        Return list of client domain names
        """
        pass

    @property
    @abc.abstractmethod
    def vueBucketName(self):
        """
        Return Vue Bucket Name
        """
        pass
