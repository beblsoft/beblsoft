#!/usr/bin/env python3
"""
NAME:
 secrentGen.py

DESCRIPTION
 secrets.py generator
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import sys
import traceback
from datetime import datetime
import click
from base.lastpass.python.bLastpass import BeblsoftLastpass


# ------------------------ CLI ---------------------------------------------- #
@click.command(context_settings=dict(help_option_names=["-h", "--help"]),
               options_metavar="[options]")
@click.option('--account', default="beblsoftmanager@gmail.com",
              help="Lastpass account")
@click.option('--password', prompt=True, hide_input=True,
              confirmation_prompt=False)
def cli(account, password):
    """
    Create secrets file using lastpass
    """
    accounts    = ["AWSQuoteTestDB",
                   "AWSQuoteProdDB"]
    curDir      = os.path.dirname(os.path.realpath(__file__))
    secretsFile = "{}/secrets.py".format(curDir)
    bLastpass   = BeblsoftLastpass(
        masterAccount   = account,
        masterPassword  = password,
        accounts        = accounts,
        login           = True)
    with open(secretsFile, "w") as f:
        f.write("# pylint: skip-file\n")
        for a in accounts:
            p = bLastpass.getPassword(a)
            f.write("{}='{}'\n".format(a, p))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    try:
        startTime = datetime.now()
        cli()  # pylint: disable=E1120
    except Exception as e:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
