import axios from 'axios'
import moment from 'moment'
import ResponseObject from './ResponseObject'
import AutoGen from '../../autogen'

export default class QuoteInterface {
  current = 0;
  numQuotes = 0;
  /* quotes = [
     { 'displayDate': '15-Jan-2018', 'author': 'George Sand', 'text': 'There is only one happiness in this life, to love and be loved.', 'pictureURL': 'https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg' },
     { 'displayDate': '14-Jan-2018', 'author': 'Helen Keller', 'text': 'The only thing worse than being blind is having sight but no vision.', 'pictureURL': 'https://images.pexels.com/photos/40976/beach-beautiful-blue-coast-40976.jpeg' },
     { 'displayDate': '13-Jan-2018', 'author': 'Abraham Lincoln', 'text': 'I am a slow walker, but I never walk back.', 'pictureURL': 'https://images.pexels.com/photos/533858/pexels-photo-533858.jpeg' },
     { 'displayDate': '12-Jan-2018', 'author': 'Henry David Thoreau', 'text': 'An early-morning walk is a blessing for the whole day.', 'pictureURL': 'https://static.pexels.com/photos/46253/mt-fuji-sea-of-clouds-sunrise-46253.jpeg' },
     { 'displayDate': '11-Jan-2018', 'author': 'John F. Kennedy', 'text': 'Things do not happen.  Things are made to happen.', 'pictureURL': 'https://images.pexels.com/photos/635279/pexels-photo-635279.jpeg?h=350&auto=compress&cs=tinysrgb' },
     { 'displayDate': '10-Jan-2018', 'author': 'Abraham Lincoln', 'text': 'Don\'t worry when you are not recognized, but strive to be worthy of recognition.', 'pictureURL': 'https://images.pexels.com/photos/339614/pexels-photo-339614.jpeg' },
     { 'displayDate': '09-Jan-2018', 'author': 'Helen Keller', 'text': 'Optimism is the faith that leads to achievement. Nothing can be done without hope and confidence.', 'pictureURL': 'https://images.pexels.com/photos/635874/pexels-photo-635874.jpeg' },
     { 'displayDate': '08-Jan-2018', 'author': 'Henry David Thoreau', 'text': 'Live the life you\'ve dreamed.', 'pictureURL': 'https://images.pexels.com/photos/130184/pexels-photo-130184.jpeg' },
     { 'displayDate': '07-Jan-2018', 'author': 'John F. Kennedy', 'text': 'Those who dare to fail miserable can achieve greatly.', 'pictureURL': 'https://images.pexels.com/photos/619950/pexels-photo-619950.jpeg' },
     { 'displayDate': '06-Jan-2018', 'author': 'Henry David Thoreau', 'text': 'Success usually comes to those who are too busy to be looking for it.', 'pictureURL': 'https://images.pexels.com/photos/590796/pexels-photo-590796.jpeg' },
     { 'displayDate': '05-Jan-2018', 'author': 'Abraham Lincoln', 'text': 'Whatever you are, be a good one.', 'pictureURL': 'https://images.pexels.com/photos/633719/pexels-photo-633719.jpeg' },
     { 'displayDate': '04-Jan-2018', 'author': 'Ralph Waldo Emerson', 'text': 'Adopt the pace of nature: her secret is patience.', 'pictureURL': 'https://images.pexels.com/photos/612999/pexels-photo-612999.jpeg' },
     { 'displayDate': '03-Jan-2018', 'author': 'Helen Keller', 'text': 'Alone we can do so little; together we can do so much.', 'pictureURL': 'https://images.pexels.com/photos/655676/pexels-photo-655676.jpeg' },
     { 'displayDate': '02-Jan-2018', 'author': 'Ralph Waldo Emerson', 'text': 'Do not go where the path may lead, go instead where there is no path and leave a trail.', 'pictureURL': 'https://images.pexels.com/photos/414160/pexels-photo-414160.jpeg' },
     { 'displayDate': '01-Jan-2018', 'author': 'Colin Powell', 'text': 'There are no secrets to success. It is the result of preparation, hard work, and learning from failure.', 'pictureURL': 'https://static.pexels.com/photos/629168/pexels-photo-629168.jpeg' }
   ]; */
  quotes = null;

  postRequest(url) {
    console.log('QuoteInterface: call to postRequest')
    return axios.get(url)
  }

  alreadyHaveQuotes() {
    if (this.numQuotes > 0) {
      return true
    } else return false
  }

  getQuotesFromServer() {
    var url = AutoGen.QuoteServerURL
    var todaysDate = moment().format('DD-MMM-YYYY')
    var todaysDateMinus30 = moment().subtract(30, 'days').format('DD-MMM-YYYY')

    let json = { 'startDate': todaysDateMinus30, 'endDate': todaysDate }
    console.log('QuoteInterface: call to Server: ' + url + ' ' + JSON.stringify(json))
    var responseObj = new ResponseObject()
    var thisPointer = this // must make copy of this pointer locally prior to Promise
    var promise = new Promise(function(resolve, reject) {
      axios.post(url, json)
        .then((response) => {
          //console.log('Data Returned: ' + JSON.stringify(response.data.data, null, 2))
          thisPointer.quotes = response.data.data
          thisPointer.numQuotes = thisPointer.quotes.length
          console.log(thisPointer.numQuotes + ' quotes returned.')
          responseObj.payload = response
          resolve(responseObj)
        })
        .catch((error) => {
          console.log(error)
          responseObj.status = ResponseObject.BS_FAILURE
          responseObj.error = 'bad error'
          reject(responseObj)
        })
    })
    return promise
  }

  getCurrentQuote() {
    this.current = 0
    return this.quotes[this.current]
  }
  getNextQuote() {
    if (this.current > 0) this.current--
      else return null

    return this.quotes[this.current]
  }
  isNext() {
    if (this.current === 0) return false
    else return true
  }
  getPreviousQuote() {
    if (this.current < this.numQuotes) this.current++
      else return null

    return this.quotes[this.current]
  }
  isPrevious() {
    if (this.current === (this.numQuotes - 1)) return false
    else return true
  }
  getQuotes() {
    return this.quotes
  }
}
