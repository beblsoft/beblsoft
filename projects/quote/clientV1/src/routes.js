import Home from './components/Home'
import Component404 from './components/Component404'
import About from './components/About'
import PrivacyPolicy from './components/PrivacyPolicy'
import ContactUs from './components/ContactUs'
import Quotes from './components/Quotes'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '*',
    name: 'Component404',
    component: Component404
  }, {
    path: '/About',
    name: 'About',
    component: About
  }, {
    path: '/PrivacyPolicy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  }, {
    path: '/ContactUs',
    name: 'ContactUs',
    component: ContactUs
  },
  {
    path: '/Quotes',
    name: 'Quotes',
    component: Quotes
  }
]

export default routes
