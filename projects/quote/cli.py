#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 Quote Command Line Interface
"""

# ------------------------ IMPORTS ------------------------------------------ #
from quote.cli import main #pylint: disable=W0406


# ------------------------ MAIN --------------------------------------------- #
main()
