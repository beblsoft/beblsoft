#!/usr/bin/env python3
"""
NAME:
 delete.py

DESCRIPTION
 Delete Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- DELETE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Delete all objects")
@click.option('--base', is_flag=True, default=False,
              help="Delete base objects")
@click.option('--network', is_flag=True, default=False,
              help="Delete network objects")
@click.option('--database', is_flag=True, default=False,
              help="Delete database objects")
@click.option('--flask', is_flag=True, default=False,
              help="Delete flask objects")
@click.option('--vue', is_flag=True, default=False,
              help="Delete vue objects")
@click.pass_context
def delete(ctx, allobjs, base, network, database, flask, vue):
    """
    Delete Infrastucture
    """
    cfg = ctx.obj["cfg"]
    if vue or allobjs:
        cfg.bVueDeployment.delete()
    if flask or allobjs:
        cfg.bFlaskDeployment.delete(lam=True,se=True, api=True, resources=True,
                                    deployment=True, stage=True, edge=True,
                                    bp=True, alias=True)
        cfg.bLFIAMRolePolicy.delete()
        cfg.bLFIAMRole.delete()
    if database or allobjs:
        cfg.bDBInstance.delete()
        cfg.bDBSubnetGroup.delete()
        cfg.bDBSecurityGroup.delete()
    if network or allobjs:
        cfg.bSecurityGroup.delete()
        cfg.bSubnet4.delete()
        cfg.bSubnet3.delete()
        cfg.bSubnet2.delete()
        cfg.bSubnet1.delete()
        cfg.bInternetGateway.delete()
        cfg.bVPC.delete()
    if base or allobjs:
        cfg.bPrivDataBucket.delete()
