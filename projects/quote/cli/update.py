#!/usr/bin/env python3
"""
NAME:
 update.py

DESCRIPTION
 Update Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ------------------------ UPDATE ------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Update all objects")
@click.option('--database', is_flag=True, default=False,
              help="Update database objects")
@click.option('--flask', is_flag=True, default=False,
              help="Update flask objects")
@click.option('--vue', is_flag=True, default=False,
              help="Update vue objects")
@click.pass_context
def update(ctx, allobjs, database, flask, vue):
    """
    Update Infrastructure
    """
    cfg = ctx.obj["cfg"]
    if database or allobjs:
        cfg.bDBDump.load()
    if flask or allobjs:
        cfg.bFlaskDeployment.update(allobjs=True)
    if vue or allobjs:
        cfg.bVueDeployment.update()
