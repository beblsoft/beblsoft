#!/usr/bin/env python3
"""
NAME:
 invoke.py

DESCRIPTION
 Invoke Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import json
import click


# ----------------------- INVOKE -------------------------------------------- #
@click.command()
@click.option('--flask', is_flag=True, default=False,
              help="Invoke flask lambda")
@click.option('--flaskapi', is_flag=True, default=False,
              help="Invoke flask api")
@click.pass_context
def invoke(ctx, flask, flaskapi):
    """
    Invoke Infrastructure
    """
    cfg = ctx.obj["cfg"]

    if flask:
        cfg.bFlaskDeployment.invokeLambda(payloadJSON={"HWL": "FUNCTION"})
    if flaskapi:
        cfg.bFlaskDeployment.invokeAPI(httpMethod="POST", pathWithQueryString="/quote",
                                       body=json.dumps({"startDate": "01-Dec-2017",
                                                        "endDate": "30-Jan-2018"}),
                                       headers={"james": "bensson"},
                                       stageVariables=None)
