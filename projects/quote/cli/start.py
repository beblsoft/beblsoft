#!/usr/bin/env python3
"""
NAME:
 start.py

DESCRIPTION
 Start Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- START --------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Start all objects")
@click.option('--database', is_flag=True, default=False,
              help="Start database objects")
@click.pass_context
def start(ctx, allobjs, database):
    """
    Start Infrastructure
    """
    cfg = ctx.obj["cfg"]
    if database or allobjs:
        cfg.bDBInstance.start()
