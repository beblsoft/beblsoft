#!/usr/bin/env python3
"""
NAME:
 stop.py

DESCRIPTION
 Stop Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- STOP ---------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Stop all objects")
@click.option('--database', is_flag=True, default=False,
              help="Stop database objects")
@click.pass_context
def stop(ctx, allobjs, database):
    """
    Stop Infrastructure
    """
    cfg = ctx.obj["cfg"]
    if database or allobjs:
        cfg.bDBInstance.stop()
