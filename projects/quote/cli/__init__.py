#!/usr/bin/python3
"""
NAME:
 __init__.py

DESCRIPTION
 Quote CLI Module
"""

# ------------------------ IMPORTS ------------------------------------------ #
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from quote.config.productionConfig import ProductionConfig
from quote.config.testConfig import TestConfig
from .create import create
from .delete import delete
from .describe import describe
from .invoke import invoke
from .start import start
from .stop import stop
from .update import update
from .tail import tail


# ----------------------- GLOBALS ------------------------------------------- #
logger        = logging.getLogger(__file__)
cfgDict       = {
    "prod": ProductionConfig,
    "test": TestConfig
}
bLog          = None
cLogFilterMap = {
    "0": logging.CRITICAL,
    "1": logging.WARNING,
    "2": logging.INFO,
    "3": logging.DEBUG
}


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--config", "-c", type=click.Choice(cfgDict.keys()), default="test",
              help="Specify configuration. Default=test")
@click.option("--logfile", default="/tmp/quoteCLI.log", type=click.Path(writable=True),
              help="Specify log file. Default=/tmp/quoteCLI.log")
@click.option("-v", "--verbose", default="2", type=click.Choice(cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
@click.pass_context
def cli(ctx, config, logfile, verbose):
    """
    Quote Command Line Interface
    """
    global bLog

    # Log Setup
    bLog = BeblsoftLog(logFile=logfile, cFilter=cLogFilterMap[verbose])
    bLog.logHeader()

    # Config Setup
    cfg = cfgDict[config]()
    ctx.obj["cfg"] = cfg

cli.add_command(create)
cli.add_command(delete)
cli.add_command(describe)
cli.add_command(invoke)
cli.add_command(start)
cli.add_command(stop)
cli.add_command(update)
cli.add_command(tail)


# ------------------------ MAIN --------------------------------------------- #
def main():
    """
    Execute CLI
    """
    try:
        startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if bLog:
            endTime = datetime.now()
            bLog.logFooter(startTime, endTime)
