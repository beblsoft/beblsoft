#!/usr/bin/env python3
"""
NAME:
 describe.py

DESCRIPTION
 Describe Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click
from base.bebl.python.color.bColor import BeblsoftColor


# ----------------------- IMPORTS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- DESCRIBE DATA ------------------------------------- #
class DescribeData():
    """
    Class to organize describe data
    """

    def __init__(self, obj, objStr):
        """
        Initialize OBject
        Args
          obj:
            Beblsoft Object
            Note: must have an "exists" method
          objStr:
            String describing object state
        """
        self.obj    = obj
        self.objStr = objStr

    @property
    def existStr(self):
        """
        Return exists str
        """
        existStr = ""
        if self.obj.exists:
            existStr = BeblsoftColor.str2Color("ONLINE", BeblsoftColor.GREEN)
        else:
            existStr = BeblsoftColor.str2Color("OFFLINE", BeblsoftColor.RED)
        return existStr

    @staticmethod
    def logList(titleStr, dataList, titleLength=100, titleStartDashes=30):
        """
        Log Describe Data
        Args
          title:
            String title
          dataList:
            List of DescribeData objects
        """
        logger.info("\n{} {} {}".format(
            "-" * titleStartDashes, titleStr,
            "-" * (titleLength - titleStartDashes - len(titleStr))))
        for d in dataList:
            logger.info("{} {} {}".format(
                str(d.obj).ljust(80), d.existStr.ljust(20), d.objStr.ljust(40)))


# ----------------------- DESCRIBE ------------------------------------------ #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Describe all objects")
@click.option('--base', is_flag=True, default=False,
              help="Describe base objects")
@click.option('--network', is_flag=True, default=False,
              help="Describe network objects")
@click.option('--database', is_flag=True, default=False,
              help="Describe database objects")
@click.option('--flask', is_flag=True, default=False,
              help="Describe flask objects")
@click.option('--vue', is_flag=True, default=False,
              help="Describe vue objects")
@click.pass_context
def describe(ctx, allobjs, base, network, database, flask, vue):
    """
    Describe Infrastructure
    """
    cfg = ctx.obj["cfg"]
    if base or allobjs:
        data = [
            DescribeData(cfg.bPrivDataBucket, ""),
            DescribeData(cfg.bCertificate, "Arn={}".format(cfg.bCertificate.arn)),
            DescribeData(cfg.bHostedZone, "ID={}".format(cfg.bHostedZone.id))
        ]
        DescribeData.logList("BASE", data)
    if network or allobjs:
        data = [
            DescribeData(cfg.bVPC,
                         "ID={}".format(cfg.bVPC.id)),
            DescribeData(cfg.bInternetGateway,
                         "ID={}".format(cfg.bInternetGateway.id)),
            DescribeData(cfg.bSubnet1,
                         "ID={}".format(cfg.bSubnet1.id)),
            DescribeData(cfg.bSubnet2,
                         "ID={}".format(cfg.bSubnet2.id)),
            DescribeData(cfg.bSubnet3,
                         "ID={}".format(cfg.bSubnet3.id)),
            DescribeData(cfg.bSubnet4,
                         "ID={}".format(cfg.bSubnet4.id)),
            DescribeData(cfg.bSecurityGroup,
                         "ID={}".format(cfg.bSecurityGroup.id))]
        DescribeData.logList("NETWORK", data)
    if database or allobjs:
        data = [
            DescribeData(cfg.bDBSecurityGroup, "arn={}".format(
                cfg.bDBSubnetGroup.arn)),
            DescribeData(cfg.bDBSubnetGroup, "ID={}".format(
                cfg.bDBSecurityGroup.id)),
            DescribeData(cfg.bDBInstance, "domainName={} arn={}".format(
                cfg.bDBInstance.domainName, cfg.bDBInstance.arn))
        ]
        DescribeData.logList("DATABASE", data)

    if flask or allobjs:
        bLFIAMRole      = cfg.bLFIAMRole
        bLambdaFunction = cfg.bFlaskDeployment.bLambdaFunction
        bAGDeployment   = cfg.bFlaskDeployment.bAGDeployment
        bAGStage        = cfg.bFlaskDeployment.bAGStage
        bAGEdgeDomain   = cfg.bFlaskDeployment.bAGEdgeDomain
        bAliasRecord    = cfg.bFlaskDeployment.bAliasRecord
        data = [
            DescribeData(bLFIAMRole,
                         "ID={}".format(bLFIAMRole.id)),
            DescribeData(bLambdaFunction,
                         "Arn={}".format(bLambdaFunction.arn)),
            DescribeData(bAGDeployment,
                         "Id={}".format(bAGDeployment.id)),
            DescribeData(bAGStage,
                         "DomainName={}".format(bAGStage.url)),
            DescribeData(bAGEdgeDomain,
                         "cfDomain={}".format(bAGEdgeDomain.cfDomain)),
            DescribeData(bAliasRecord,
                         "DomainName={}".format(bAliasRecord.domainName))]
        DescribeData.logList("FLASK", data)
    if vue or allobjs:
        bBucketWebsite  = cfg.bVueDeployment.bBucketWebsite
        bCFDistribution = cfg.bVueDeployment.bCFDistribution
        data = [
            DescribeData(bBucketWebsite,
                         "DomainName={}".format(bBucketWebsite.domainName)),
            DescribeData(bCFDistribution,
                         "DomainName={}".format(bCFDistribution.domainName))]
        data += [
            DescribeData(bRRS, "DomainName={}".format(bRRS.domainName))
            for bRRS in cfg.bVueDeployment.bResourceRecordSetList]
        DescribeData.logList("VUE", data)
