#!/usr/bin/env python3
"""
NAME:
 tail.py

DESCRIPTION
 Tail Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- TAIL ---------------------------------------------- #
@click.command()
@click.option('--flask', is_flag=True, default=False,
              help="Tail flask lambda")
@click.pass_context
def tail(ctx, flask):
    """
    Tail Infrastructure Logs
    """
    cfg = ctx.obj["cfg"]
    if flask:
        cfg.bFlaskDeployment.tailLambda()
