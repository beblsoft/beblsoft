#!/usr/bin/env python3
"""
NAME:
 create.py

DESCRIPTION
 Create Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- CREATE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Create all objects")
@click.option('--base', is_flag=True, default=False,
              help="Create base objects")
@click.option('--network', is_flag=True, default=False,
              help="Create network objects")
@click.option('--database', is_flag=True, default=False,
              help="Create database objects")
@click.option('--flask', is_flag=True, default=False,
              help="Create flask objects")
@click.option('--vue', is_flag=True, default=False,
              help = "Create vue objects")
@click.pass_context
def create(ctx, allobjs, base, network, database, flask, vue):
    """
    Create Infrastucture
    """
    cfg = ctx.obj["cfg"]
    if base or allobjs:
        cfg.bPrivDataBucket.create()
    if network or allobjs:
        cfg.bVPC.create()
        cfg.bInternetGateway.create()
        cfg.bSubnet1.create()
        cfg.bSubnet2.create()
        cfg.bSubnet3.create()
        cfg.bSubnet4.create()
        cfg.bSecurityGroup.create()
    if database or allobjs:
        cfg.bDBSecurityGroup.create()
        cfg.bDBSubnetGroup.create()
        cfg.bDBInstance.create()
        cfg.bDBDump.load()
    if flask or allobjs:
        cfg.bLFIAMRole.create()
        cfg.bLFIAMRolePolicy.create()
        cfg.bFlaskDeployment.create(allobjs=True)
    if vue or allobjs:
        cfg.bVueDeployment.create()
