OVERVIEW
===============================================================================
Beblsoft's Quote Application Documentation. www.quotablejoy.com


CLI
===============================================================================
- Install python3.6 for linux mint      : See python.sh: python3_lm_install
- Go to correct directory               : cd ${QUOTE}
- Setup virtual environment             : virtualenv -p /usr/bin/python3.6 venv
                                          source venv/bin/activate
                                          pip3 install -r requirements.txt
- Install secrets file to suck
  down relevant passwords               : ./config/secretGen.py
- Print CLI usage                       : ./cli.py -h
- Describe test infrastructure          : ./cli.py --config=test describe --allobjs
- Describe production infrastructure    : ./cli.py --config=prod describe --allobjs
- Create all test infrastructure        : ./cli.py create --allobjs
- Update test vue code                  : ./cli.py update --vue
- Update test flask code                : ./cli.py update --flask
- Delete all test infrastructure        : ./cli.py delete --allobjs


SERVERV2 CLI
===============================================================================
- Install python3.6 for linux mint      : See python.sh: python3_lm_install
- Go to correct directory               : cd ${QUOTE}/serverV2
- Setup virtual environment             : virtualenv -p /usr/bin/python3.6 venv
                                          source venv/bin/activate
                                          pip3 install -r requirements.txt
- Print CLI usage                       : ./cli.py -h
- Run local server                      : ./cli.py app run
- Create database                       : ./cli.py --cfg=localScrapy db create


SERVERV2 SCRAPING
===============================================================================
- Go to correct directory               : cd ${QUOTE}/serverV2
- Create a new scraping database        : mysql> create database <name>;
- Update scrapy config with new db      : Edit config/localScrapyConfig.py
- Create database tables                : ./cli.py --config=localScrapy db create
- Scrape goodreads                      : scrapy crawl goodreads
                                          Last run: 6 hours, 1.2 million quotes, 14 thousand authors
- Add quote display date                : ./cli.py --config=localScrapy db populate --sdate=01/01/2018 --edate=12/31/2067
