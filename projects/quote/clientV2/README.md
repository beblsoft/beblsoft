# Client V2 Documentation

## Environment differences from base application

## Add lodash for debouncing

`npm install lodash --save`

## Add good table

`npm install --save vue-good-table`

## Add state with a vue-stash

`npm install --save vue-stash`

## Add time management

`npm install moment --save`

## Favicon generator

http://antifavicon.com/

## Generate block of color, using imageMagic

convert -size 728*90 xc:#ffedcd x.png

## Colors used for overlay:

- 5f9ea0 - cadetblue
- ffebcd - offwhite
- 008080 - teal
- 8fbc8f - darseagreen
- d3d3d3 - lightgreen
- ff85ec - seashell
- cd5c5c - indianred

## See top level README.txt

1. Go to correct directory                : `cd $HOME/git/beblsoft/projects/quote`
2. Setup virtual environment
    * `virtualenv -p /usr/bin/python3.6 venv`
    * `source venv/bin/activate`
    * `pip3 install -r requirements.txt`
3. Install secrets file to suck
   down relevant passwords, Mojourney123  : `./config/secretGen.py`
4. Upload to test, ensure works:          : `./cli.py update vue`
5. Upload to production:                  : `./cli.py --config prod update --vue`

## Initial deployment on AWS

1. Log in to aws at:  https://console.aws.amazon.com/console/home?region=us-east-1
2. Go to the S3 service
3. Build distribution with npm run build command
4. Make quotalejoyclient bucket with public access, static website hosting enabled
5. Drag up the contents of dist to the bucket:  static directory and compiled index.html
6. Go to browser and bring up application: http://quotablejoyclient.s3-website-us-east-1.amazonaws.com/
7. Go to CloudFront, distribution instance and create Invalidation (tab) to force reload of site. Enter
   object paths= '/' to invalidate all objects

Reference article:  https://medium.com/@connorleech/host-a-vue-js-website-on-amazon-s3-for-the-best-hosting-solution-ever-%EF%B8%8F-eee2a28b2506

## quotablejoy client (a Vue app) documentation

- Below is a file tree with text indicating what the files are used for.

```bash
├── .babelrc - bable setup
├── build - standard build files produced by vue cli, note changes in webpack.dev.conf.js to force auto reload
│   ├── build.js
│   ├── check-versions.js
│   ├── dev-client.js
│   ├── dev-server.js
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   ├── webpack.prod.conf.js
│   └── webpack.test.conf.js
├── config - standard configuration files produced by vue cli
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── test.env.js
├── .editorconfig - editor customization
├── .eslintignore - files to not do ES lint on
├── .eslintrc.js - where custom ES lint configuration is
├── .gitignore - git overrides
├── index.html - start of program, pull in dependencies like bootstrap
├── package.json - contains all project dependencies, scripts, metadata about application
├── package-lock.json - automatically generated exact tree of dependencies generated.
├── .postcssrc.js - a file defining module.exports that is ignored, used to add JS logic to generate your config.
├── README.md - this file, documentation for application
├── src
│   ├── App.vue - basic app layout, icon and navigation to route to, loads Home page
│   ├── NavBar.vue - component for Navigation (top searchbar and History/Home routes)
│   ├── components
│   │   ├──About.vue - page that provides description of site
│   │   ├──Component404.vue - page that indicates a bad route was typed in, e.g. http://www.quotablejoy.com/badRoute
│   │   ├──ContactUs.vue- page that provides our email
│   │   ├──Home.vue - main view with quote of the day, picture, navigation to prev/next quotes
│   │   ├──PrivacyPolicy.vue - page that boilerplates our privacy policy
│   │   ├──Quotes.vue - page that shows table of last 30-days of quotes
│   │   ├──Search.vue - page that provides search functionality
│   ├── main.js - instantiates Vue from index, which loads App Component and instantiates the routes, sets up persistent stash
│   ├── routes.js - the routes for the application
│   ├── store.js - the persistence state of the application
│   └── services
│       ├── ServerInterface.js - actual AJAX goes out here, base service
├── static
│   ├── css
│   │   └── bootstrap-3.3.7.min.css - css for bootstrap
│   │   └── font-awesome-4.7.0.min.css - css for font awesome
│   │   └── quotablejoy.css - css for quotablejoy
│   ├── fonts
│   │   └── glyphicons*.* (5 files) - provides bootstrap support for fancy icons
│   │   └── f[F]onta[A]wesome*.* (6 files) - provides fontawesome support for fancy icons, e.g. magnifying glass
│   ├── icons
│   │   └── faviconAlternative - alternative icon for quotablejoy
│   │   └── favicon - main icon for quotablejoy, used in tab when invoked (see index.html)
│   ├── js
│   │   └── bootstrap.min.js* - java script for bootstrap, needed for tables and navigation bar
│   │   └── jQuery-2-2.0.min - java script for bootstrap, needed for tables and navigation bar
└── test - vue auto generated test files
    ├── e2e
    │   ├── custom-assertions
    │   │   └── elementCount.js
    │   ├── nightwatch.conf.js
    │   ├── runner.js
    │   └── specs
    │       └── test.js
    └── unit
        ├── index.js
        ├── karma.conf.js
        └── specs
            └── Hello.spec.js
```

## Basic Commandline for Sample App

```bash
    # install dependencies
    npm install

    # serve with hot reload at localhost:8080
    npm run dev

    # build for production with minification
    npm run build

    # build for production and view the bundle analyzer report
    npm run build --report

    # run unit tests
    npm run unit

    # run e2e tests
    npm run e2e

    # run all tests
    npm test
```

## For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
