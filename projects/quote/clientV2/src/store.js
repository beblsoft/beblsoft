/* store.js - defines centralized storage for application, reducing calls for the same data from the server */
import QuoteInterface from './services/QuoteInterface'

var theQuotes = new QuoteInterface()

export default {
  quotes: theQuotes
}
