/* main.js - main JavaScript file for application.  Defines all Vue Components. */
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routes from './routes'
import VueStash from 'vue-stash'
import store from './store'
import VueGoodTable from 'vue-good-table'
import NavBar from './NavBar'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VueStash)
Vue.use(VueGoodTable)

// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  scrollBehavior: function(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

Vue.component('navbar', NavBar)
/* temporary user always logged in, set in some global place */
/*var userNotLoggedIn = false*/
/*router.beforeEach((to, from, next) => {*/
/*  if (to.meta.auth && userNotLoggedIn) {*/
/*    next('/not-logged-in/login')*/
/*  } else {*/
/*    next()*/
/*  }*/
/*})*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  data: { store },
  template: '<App/>',
  components: {
    App,
    NavBar
  }
})

/* declare bus to send search data between app component and search component. */
export default window.bus = new Vue()
