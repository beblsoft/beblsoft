/* QuoteInterface.js - QuoteInterface Component provides access to all server data */
import moment from 'moment'
import autoGen from '../../autogen'
import * as apiV1 from './apiV1/api.js'

apiV1.setDomain(autoGen.APIDomain)
console.log(apiV1.getDomain())

export default class QuoteInterface {
  current = 0;
  numDailyQuotes = 0;
  dailyQuotes = [];
  searchAuthorsArray = [];
  searchQuotesArray = [];

  alreadyHaveQuotes() {
    if (this.numDailyQuotes > 0) {
      return true
    } else return false
  }

  getQuotesFromServer() {
    var todaysDate = moment().format('YYYY-MM-DD')
    var todaysDateMinus20 = moment().subtract(20, 'days').format('YYYY-MM-DD')
    let parameters = { 'startDate': todaysDateMinus20, 'endDate': todaysDate }
    var thisPointer = this // must make copy of this pointer locally prior to Promise
    var promise = apiV1.get_daily_quote_resource(parameters)
      .then((resp) => {
        thisPointer.dailyQuotes = resp.data
        thisPointer.dailyQuotes.reverse()
        thisPointer.numDailyQuotes = thisPointer.dailyQuotes.length
        console.log(thisPointer.numDailyQuotes + ' daily quotes returned.')
      })
      .catch((err) => {
        console.log(err)
      })
    return promise
  }

  getCurrentQuote() {
    this.current = 0
    return this.dailyQuotes[this.current]
  }
  getNextQuote() {
    if (this.current > 0) this.current--
      else return null

    return this.dailyQuotes[this.current]
  }
  isNext() {
    if (this.current === 0) return false
    else return true
  }
  getPreviousQuote() {
    if (this.current < this.numDailyQuotes) this.current++
      else return null

    return this.dailyQuotes[this.current]
  }
  isPrevious() {
    if (this.current === (this.numDailyQuotes - 1)) return false
    else return true
  }
  getQuotes() {
    /* console.log(JSON.stringify(this.dailyQuotes)) */
    return this.dailyQuotes
  }
  /* start of new functionality for search */
  searchAuthors(theName) {
    let parameters = {
      type: 'byName',
      name: theName
    }
    var thisPointer = this // must make copy of this pointer locally prior to Promise
    var promise = apiV1.get_author_resource(parameters)
      .then((resp) => {
        thisPointer.searchAuthorsArray = resp.data
      })
      .catch((err) => {
        console.log(err)
      })
    return promise
  }
  getAuthorsSearch() {
    return this.searchAuthorsArray
  }

  searchQuotes(theText) {
    let parameters = {
      type: 'byText',
      text: theText
    }
    var thisPointer = this // must make copy of this pointer locally prior to Promise
    var promise = apiV1.get_quote_resource(parameters)
      .then((resp) => {
        thisPointer.searchQuotesArray = resp.data
      })
      .catch((err) => {
        console.log(err)
      })
    return promise
  }

  searchQuotesByAuthor(theAuthor) {
    let parameters = {
      type: 'byAuthor',
      author: theAuthor
    }
    var thisPointer = this // must make copy of this pointer locally prior to Promise
    var promise = apiV1.get_quote_resource(parameters)
      .then((resp) => {
        thisPointer.searchQuotesArray = resp.data
      })
      .catch((err) => {
        console.log(err)
      })
    return promise
  }

  getQuotesSearch() {
    return this.searchQuotesArray
  }

}
