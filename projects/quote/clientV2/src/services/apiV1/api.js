/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, {
      params: form
    }, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    API to show authors and quotes
 ==========================================================*/
/**
* Get authors
* request: get_author_resource
* url: get_author_resourceURL
* method: get_author_resource_TYPE
* raw_url: get_author_resource_RAW_URL
    
     * @param name - Specify name query. Example: 'hel'
     * @param xFields - An optional fields mask
*/
export const get_author_resource = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/author/'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['type'] !== undefined) {
    queryParameters['type'] = parameters['type']
  }
  if (parameters['type'] === undefined) {
    return Promise.reject(new Error('Missing required  parameter: type'))
  }
  if (parameters['name'] !== undefined) {
    queryParameters['name'] = parameters['name']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const get_author_resource_RAW_URL = function() {
  return '/author/'
}
export const get_author_resource_TYPE = function() {
  return 'get'
}
export const get_author_resourceURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/author/'
  if (parameters['type'] !== undefined) {
    queryParameters['type'] = parameters['type']
  }
  if (parameters['name'] !== undefined) {
    queryParameters['name'] = parameters['name']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * Get daily quotes
 * request: get_daily_quote_resource
 * url: get_daily_quote_resourceURL
 * method: get_daily_quote_resource_TYPE
 * raw_url: get_daily_quote_resource_RAW_URL
 * @param startDate - Specify quote startDate in ISO 8601. Example: '2018-02-04'
 * @param endDate - Specify quote endDate in ISO 8601. Example: '2018-02-10'
 * @param xFields - An optional fields mask
 */
export const get_daily_quote_resource = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  console.log("Config="$config)
  const config = parameters.$config
  let path = '/dailyQuote/'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['startDate'] !== undefined) {
    queryParameters['startDate'] = parameters['startDate']
  }
  if (parameters['startDate'] === undefined) {
    return Promise.reject(new Error('Missing required  parameter: startDate'))
  }
  if (parameters['endDate'] !== undefined) {
    queryParameters['endDate'] = parameters['endDate']
  }
  if (parameters['endDate'] === undefined) {
    return Promise.reject(new Error('Missing required  parameter: endDate'))
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const get_daily_quote_resource_RAW_URL = function() {
  return '/dailyQuote/'
}
export const get_daily_quote_resource_TYPE = function() {
  return 'get'
}
export const get_daily_quote_resourceURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/dailyQuote/'
  if (parameters['startDate'] !== undefined) {
    queryParameters['startDate'] = parameters['startDate']
  }
  if (parameters['endDate'] !== undefined) {
    queryParameters['endDate'] = parameters['endDate']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * Get quotes
 * request: get_quote_resource
 * url: get_quote_resourceURL
 * method: get_quote_resource_TYPE
 * raw_url: get_quote_resource_RAW_URL
 * @param type - Specify query type. If byText is specified, text argument must be supplied. If byAuthor is specifed, author argument must be supplied. 
 * @param text - Specify text query. Example: 'love'
 * @param author - Specify quote author. Example: 'Helen Keller'
 * @param xFields - An optional fields mask
 */
export const get_quote_resource = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/quote/'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['type'] !== undefined) {
    queryParameters['type'] = parameters['type']
  }
  if (parameters['type'] === undefined) {
    return Promise.reject(new Error('Missing required  parameter: type'))
  }
  if (parameters['text'] !== undefined) {
    queryParameters['text'] = parameters['text']
  }
  if (parameters['author'] !== undefined) {
    queryParameters['author'] = parameters['author']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const get_quote_resource_RAW_URL = function() {
  return '/quote/'
}
export const get_quote_resource_TYPE = function() {
  return 'get'
}
export const get_quote_resourceURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/quote/'
  if (parameters['type'] !== undefined) {
    queryParameters['type'] = parameters['type']
  }
  if (parameters['text'] !== undefined) {
    queryParameters['text'] = parameters['text']
  }
  if (parameters['author'] !== undefined) {
    queryParameters['author'] = parameters['author']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}