/*
 NAME
   generator.js

 DESCRIPTION
   Generate a Vue API given a swagger spec

 USAGE
   node ./generator.js
*/

/* ------------------------------ IMPORTS ----------------------------------- */
const path        = require('path')
const swaggerGen  = require('swagger-vue')
const fs          = require('fs')


/* ------------------------------ GLOBALS ----------------------------------- */
const apiPath     = path.resolve(__dirname, 'api.js')
const jsonData    = require('../../../../serverV2/app/apiV1/swagger.json')
let   opt         = {
  swagger: jsonData,
  moduleName: 'api',
  className: 'api'
}

/* ------------------------------ MAIN -------------------------------------- */
const codeResult = swaggerGen(opt)
fs.writeFile(apiPath, codeResult)