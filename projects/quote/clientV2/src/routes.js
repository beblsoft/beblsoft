/* route.js - defines all routes for application*/
import Home from './components/Home'
import Component404 from './components/Component404'
import About from './components/About'
import PrivacyPolicy from './components/PrivacyPolicy'
import History from './components/History'
import Search from './components/Search'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '*',
    name: 'Component404',
    component: Component404
  }, {
    path: '/About',
    name: 'About',
    component: About
  }, {
    path: '/PrivacyPolicy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  }, {
    path: '/History',
    name: 'History',
    component: History
  }, {
    path: '/Search',
    name: 'Search',
    component: Search
  }
]

export default routes
