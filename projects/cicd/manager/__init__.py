#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 Manager Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import os
from cicd.manager.network import NetworkManager
from cicd.manager.bucket import BucketManager
from cicd.manager.bastion import BastionManager
from cicd.manager.lambdaSwitch import LambdaSwitchManager


# ------------------------ CICD MANAGER OBJECT ------------------------------ #
class CICDManager():
    """
    CICD Manager Class
    """

    def __init__(self, cfg):  # pylint: disable=R0915
        """
        Initialize Object
        """
        self.cfg      = cfg
        self.network  = NetworkManager(mgr=self)
        self.bucket   = BucketManager(mgr=self)
        self.bastion  = BastionManager(mgr=self)
        self.ls       = LambdaSwitchManager(mgr=self)
