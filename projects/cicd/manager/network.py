#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 network.py

DESCRIPTION
 Network Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bVPC import BeblsoftVPC
from base.aws.python.EC2.bSubnet import BeblsoftSubnet
from base.aws.python.EC2.bInternetGateway import BeblsoftInternetGateway
from base.aws.python.EC2.RouteTable.bRouteTableV2 import BeblsoftRouteTableV2
from base.aws.python.EC2.bNATGateway import BeblsoftNATGateway
from base.aws.python.EC2.Route.bNATGateway import BeblsoftNATGatewayRoute
from base.aws.python.EC2.Route.bInternetGateway import BeblsoftInternetGatewayRoute
from base.aws.python.EC2.Address.bAddress import BeblsoftAddress
from cicd.manager.common import CommonManager
from cicd.commands.describe import DescribeData


# ------------------------ NETWORK MANAGER OBJECT --------------------------- #
class NetworkManager(CommonManager):
    """
    Network Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        ------------------------------------------------------------------
        | VPC                                                            |
        | ----------------------------------------------                 |
        | | Public Subnets                             |           ------------
        | | RouteTable                                 |           | Internet |
        | |   Route: 10.0.0.0/0  -> Internet Gateway ---<---->---- | Gateway  |
        | |                          ----------------  |           ------------
        | |                          | Elastic IP   |  |                 |
        | |                          | NAT Gateway  |  |                 |
        | |                          ----------------  |                 |
        | ----------------------------------------|-----                 |
        |                                         |                      |
        | ----------------------------------------|-----                 |
        | |Private NAT Subnets                    |    |                 |
        | | RouteTable                            |    |                 |
        | |   Route: 10.0.0.0/0  -> NAT Gateway----    |                 |
        | ----------------------------------------------                 |
        |                                                                |
        | ----------------------------------------------                 |
        | | Private Subnets                            |                 |
        | ----------------------------------------------                 |
        ------------------------------------------------------------------
        """
        super().__init__(**kwargs)

        # Base Network ----------------
        self.bVPC                 = BeblsoftVPC(
            name                          = self.cfg.network.vpcName,
            cidrBlock                     = "10.0.0.0/16",
            enableDnsHostnames            = True,
            enableDnsSupport              = True)
        self.bInternetGateway     = BeblsoftInternetGateway(
            name                          = "BeblsoftCICDInternetGateway",
            bVPC                          = self.bVPC)

        # Public Subnets --------------
        self.nPublicSubnets       = 1
        self.maxPublicSubnets     = 40
        self.publicCIDRStart      = 0
        self.publicCIDREnd        = self.publicCIDRStart + self.maxPublicSubnets - 1
        self.bPublicSubList       = []
        self.bPublicRTList        = []
        self.bPublicIGRouteList   = []
        for idx in range(0, self.nPublicSubnets):
            self.bPublicSubList.append(
                BeblsoftSubnet(
                    name                  = "BeblsoftCICDPublicSubnet{}".format(idx),
                    cidrBlock             = "10.0.{}.0/24".format(self.publicCIDRStart + idx),
                    bVPC                  = self.bVPC,
                    availabiltyZone       = self.cfg.aws.AZList[idx % len(self.cfg.aws.AZList)]))
            self.bPublicRTList.append(
                BeblsoftRouteTableV2(
                    name                  = "BeblsoftCICDPublicRT{}".format(idx),
                    bSubnet               = self.bPublicSubList[idx]))
            self.bPublicIGRouteList.append(
                BeblsoftInternetGatewayRoute(
                    bRouteTableV2         = self.bPublicRTList[idx],
                    destCIDR              = "0.0.0.0/0",
                    bInternetGateway      = self.bInternetGateway))

        # NAT Gateways ----------------
        # Note: not every public subnet has a NAT Gateway
        self.nNATGateways         = 1
        self.bNATAddressList      = []
        self.bNATGatewayList      = []
        self.bPrivateNATRouteList = []
        for idx in range(0, self.nNATGateways):
            self.bNATAddressList.append(
                BeblsoftAddress(
                    name                 = "BeblsoftCICDNATAddress{}".format(idx),
                    domain               = "vpc"))
            self.bNATGatewayList.append(
                BeblsoftNATGateway(
                    name                 = "BeblsoftCICDNATGateway{}".format(idx),
                    bSubnet              = self.bPublicSubList[idx % len(self.bPublicSubList)],
                    bAddress             = self.bNATAddressList[idx]))

        # Private-NAT Subnets ---------
        self.nPrivateNATSubnets   = 3
        self.maxPrivateNATSubnets = 40
        self.privateNATCIDRStart  = self.publicCIDREnd + 1
        self.privateNATCIDREnd    = self.privateNATCIDRStart + self.maxPrivateNATSubnets - 1
        self.bPrivateNATSubList   = []
        self.bPrivateNATRTList    = []
        for idx in range(0, self.nPrivateNATSubnets):
            self.bPrivateNATSubList.append(
                BeblsoftSubnet(
                    name                  = "SBeblsoftCICDPrivateNATSubnet{}".format(idx),
                    cidrBlock             = "10.0.{}.0/24".format(self.privateNATCIDRStart + idx),
                    bVPC                  = self.bVPC,
                    availabiltyZone       = self.cfg.aws.AZList[idx % len(self.cfg.aws.AZList)]))
            self.bPrivateNATRTList.append(
                BeblsoftRouteTableV2(
                    name                  = "BeblsoftCICDPrivateNATRT{}".format(idx),
                    bSubnet               = self.bPrivateNATSubList[idx]))
            self.bPrivateNATRouteList.append(
                BeblsoftNATGatewayRoute(
                    bRouteTableV2        = self.bPrivateNATRTList[idx],
                    destCIDR             = "0.0.0.0/0",
                    bNATGateway          = self.bNATGatewayList[idx % len(self.bNATGatewayList)]))

        # Private Subnets -------------
        self.nPrivateSubnets      = 0
        self.maxPrivateSubnets    = 40
        self.privateCIDRStart     = self.privateNATCIDREnd + 1
        self.privateCIDREnd       = self.privateCIDRStart + self.maxPrivateSubnets - 1
        self.bPrivateSubList      = []
        self.bPrivateRTList       = []
        for idx in range(0, self.nPrivateSubnets):
            self.bPrivateSubList.append(
                BeblsoftSubnet(
                    name                  = "BeblsoftCICDPrivateSubnet{}".format(idx),
                    cidrBlock             = "10.0.{}.0/24".format(self.privateCIDRStart + idx),
                    bVPC                  = self.bVPC,
                    availabiltyZone       = self.cfg.aws.AZList[idx % len(self.cfg.aws.AZList)]))
            self.bPrivateRTList.append(
                BeblsoftRouteTableV2(
                    name                  = "BeblsoftCICDPrivateRT{}".format(idx),
                    bSubnet               = self.bPrivateSubList[idx]))

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self, base=False, pub=False, prinat=False, pri=False, nat=False): #pylint: disable=R0912
        """
        Create objects
        """
        if base:
            self.bVPC.create()
            self.bInternetGateway.create()
        if pub:
            for bSub in self.bPublicSubList:
                bSub.create()
            for bRT in self.bPublicRTList:
                bRT.create()
            for bR in self.bPublicIGRouteList:
                bR.create()
        if prinat:
            for bSub in self.bPrivateNATSubList:
                bSub.create()
            for bRT in self.bPrivateNATRTList:
                bRT.create()
        if pri:
            for bSub in self.bPrivateSubList:
                bSub.create()
            for bRT in self.bPrivateRTList:
                bRT.create()
        if nat:
            for bA in self.bNATAddressList:
                bA.create()
            for bNG in self.bNATGatewayList:
                bNG.create()
            for bR in self.bPrivateNATRouteList:
                bR.create()

    @logFunc()
    def delete(self, base=False, pub=False, prinat=False, pri=False, nat=False): #pylint: disable=R0912
        """
        Delete objects
        """
        if nat:
            for bR in self.bPrivateNATRouteList:
                bR.delete()
            for bNG in self.bNATGatewayList:
                bNG.delete()
            for bA in self.bNATAddressList:
                bA.delete()
        if pri:
            for bRT in self.bPrivateRTList:
                bRT.delete()
            for bSub in self.bPrivateSubList:
                bSub.delete()
        if prinat:
            for bRT in self.bPrivateNATRTList:
                bRT.delete()
            for bSub in self.bPrivateNATSubList:
                bSub.delete()
        if pub:
            for bR in self.bPublicIGRouteList:
                bR.delete()
            for bRT in self.bPublicRTList:
                bRT.delete()
            for bSub in self.bPublicSubList:
                bSub.delete()
        if base:
            self.bInternetGateway.delete()
            self.bVPC.delete()

    @logFunc()
    def describe(self, base=False, pub=False, prinat=False, pri=False, nat=False): #pylint: disable=R0912
        """
        Describe objects
        """
        if base:
            data = [
                DescribeData(self.bVPC, "ID={}".format(self.bVPC.id)),
                DescribeData(self.bInternetGateway, "ID={}".format(self.bInternetGateway.id)),
            ]
            DescribeData.logList("NETWORK: BASE", data)
        if pub:
            data = []
            for bSub in self.bPublicSubList:
                data.append(DescribeData(bSub, "ID={}".format(bSub.id)))
            for bRT in self.bPublicRTList:
                data.append(DescribeData(bRT, "ID={}".format(bRT.id)))
            for bR in self.bPublicIGRouteList:
                data.append(DescribeData(bR, "State={}".format(bR.state)))
            DescribeData.logList("NETWORK: PUBLIC", data)

        if prinat:
            data = []
            for bSub in self.bPrivateNATSubList:
                data.append(DescribeData(bSub, "ID={}".format(bSub.id)))
            for bRT in self.bPrivateNATRTList:
                data.append(DescribeData(bRT, "ID={}".format(bRT.id)))
            for bR in self.bPrivateNATRouteList:
                data.append(DescribeData(bR, "State={}".format(bR.state)))
            DescribeData.logList("NETWORK: PRIVATE NAT", data)

        if pri:
            data = []
            for bSub in self.bPrivateSubList:
                data.append(DescribeData(bSub, "ID={}".format(bSub.id)))
            for bRT in self.bPrivateRTList:
                data.append(DescribeData(bRT, "ID={}".format(bRT.id)))
            DescribeData.logList("NETWORK: PRIVATE", data)

        if nat:
            data = []
            for bA in self.bNATAddressList:
                data.append(DescribeData(bA, "AllocationID={}".format(bA.allocationID)))
            for bNG in self.bNATGatewayList:
                data.append(DescribeData(bNG, "ID={} State={}".format(bNG.id, bNG.state)))
            DescribeData.logList("NETWORK: NAT", data)
