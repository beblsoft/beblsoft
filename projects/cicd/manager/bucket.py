#!/usr/bin/env python3
"""
NAME:
 bucket.py

DESCRIPTION
 Bucket Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.S3.bBucket import BeblsoftBucket
from cicd.manager.common import CommonManager
from cicd.commands.describe import DescribeData


# ------------------------ BUCKET MANAGER OBJECT ---------------------------- #
class BucketManager(CommonManager):
    """
    Bucket Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bBucket = BeblsoftBucket(
            name         = self.cfg.bucket.name,
            acl          = "private",
            bRegion      = self.cfg.aws.bRegion)

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bBucket.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        self.bBucket.delete()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bBucket, ""),
        ]
        DescribeData.logList("BUCKET", data)
