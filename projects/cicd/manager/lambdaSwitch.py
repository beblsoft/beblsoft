#!/usr/bin/env python3
"""
NAME:
 lambdaSwitch.py

DESCRIPTION
 Lambda Switch Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.Lambda.bPackage import BeblsoftLambdaPackage
from base.aws.python.LambdaSwitch.bDeployment import BeblsoftLSDeployment
from base.aws.python.LambdaSwitch.cases.ScheduledEvent.bDeployment import BeblsoftLSScheduledEventDeployment
from cicd.manager.common import CommonManager
from cicd.commands.describe import DescribeData


# ------------------------ LAMBDA SWITCH MANAGER OBJECT --------------------- #
class LambdaSwitchManager(CommonManager):
    """
    Lambda Switch Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bLFIAMRole           = BeblsoftIAMRole(
            name                          = "BeblsoftCICDLSRole",
            trustPolicyPath               = "{}/LSIAMRoleTrustPolicy.json".format(self.cfg.dirs.lambdaSwitch))
        self.bLFIAMRolePolicy     = BeblsoftIAMRolePolicy(
            name                          = "BeblsoftCICDLSRP",
            bIAMRole                      = self.bLFIAMRole,
            permissionPolicyPath          = "{}/LSIAMRolePermissionPolicy.json".format(self.cfg.dirs.lambdaSwitch))
        self.bLSSecurityGroup     = BeblsoftSecurityGroup(
            name                          = "BeblsoftCICDLSSecurityGroup",
            bVPC                          = self.mgr.network.bVPC)
        self.bLambdaPackage       = BeblsoftLambdaPackage(
            bucketName                    = self.cfg.bucket.name,
            bucketKey                     = "BeblsoftCICDLS.zip",
            fromToPaths                   = [{"fromPath": "{}/*".format(self.cfg.dirs.lambdaSwitch),
                                              "toPath": "cicd/lambdaSwitch",
                                              "excludeStrList": ["venv", "__pycache__"]},
                                             {"fromPath": "{}/venv/lib/python3.6/site-packages/*".format(self.cfg.dirs.lambdaSwitch),
                                              "toPath": "."},
                                             {"fromPath": "{}/bebl/python/*".format(self.cfg.dirs.base),
                                              "toPath": "base/bebl/python"},
                                             {"fromPath": "{}/aws/python/*".format(self.cfg.dirs.base),
                                              "toPath": "base/aws/python"},
                                             ],
            buildDir                      = "/tmp/BeblsoftCICDLSBuild",
            bRegion                       = self.cfg.aws.bRegion)
        self.bLSDeployment        = BeblsoftLSDeployment(
            name                          = "BeblsoftCICDLSDeployment",
            bLambdaPackage                = self.bLambdaPackage,
            bIAMRole                      = self.bLFIAMRole,
            logLevel                      = logging.NOTSET,
            bSubnets                      = self.mgr.network.bPrivateNATSubList,
            bSecurityGroups               = [self.bLSSecurityGroup],
            description                   = "Beblsoft CICD Lambda Switch",
            timeoutS                      = 30,  # 30 seconds
            memorySizeMB                  = 1028,
            logRetentionInDays            = 30)
        self.bLSSEDeployment      = BeblsoftLSScheduledEventDeployment(
            bLSDeployment                 = self.bLSDeployment,
            index                         = 0,
            funcStr                       = "cicd.lambdaSwitch.cullInstances:handleCullEvent",
            expression                    = "rate(3 minutes)",
            kwargsFunc                    = lambda: {
                                            "bRegionName": self.cfg.aws.bRegion.name,
                                            "vpcID": self.mgr.network.bVPC.id,
                                            "bastionName": self.mgr.bastion.instanceName })

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self, ls, lsse):
        """
        Create objects
        """
        if ls:
            self.bLFIAMRole.create()
            self.bLFIAMRolePolicy.create()
            self.bLSSecurityGroup.create()
            self.bLSDeployment.create()
        if lsse:
            self.bLSSEDeployment.create()

    @logFunc()
    def delete(self, ls, lsse):
        """
        Delete objects
        """
        if lsse:
            self.bLSSEDeployment.delete()
        if ls:
            self.bLSDeployment.delete()
            self.bLSSecurityGroup.delete()
            self.bLFIAMRolePolicy.delete()
            self.bLFIAMRole.delete()

    @logFunc()
    def update(self, ls, lsse):
        """
        Update objects
        """
        if ls:
            self.bLSDeployment.update()
        if lsse:
            self.bLSSEDeployment.update()

    @logFunc()
    def describe(self, ls, lsse):
        """
        Describe objects
        """
        if ls:
            bLFIAMRole      = self.bLFIAMRole
            bLambdaFunction = self.bLSDeployment.bLambdaFunction
            data = [
                DescribeData(bLFIAMRole, "ID={}".format(bLFIAMRole.id)),
                DescribeData(self.bLSSecurityGroup,
                             "ID={}".format(self.bLSSecurityGroup.id)),
                DescribeData(bLambdaFunction, "Arn={}".format(bLambdaFunction.arn)),
            ]
            DescribeData.logList("LAMBDA SWITCH", data)
        if lsse:
            bKWCWERule      = self.bLSSEDeployment.bCWERule
            data = [
                DescribeData(bKWCWERule, "Arn={}".format(bKWCWERule.arn)),
            ]
            DescribeData.logList("LAMBDA SWITCH: SCHEDULED EVENT", data)

    # ---------------------- RUN -------------------------------------------- #
    @logFunc()
    def invoke(self, **kwargs):  # pylint: disable=W0221,W0102
        """
        Invoke object
        """
        return self.bLSDeployment.invoke(**kwargs)

    # ---------------------- LOGS ------------------------------------------- #
    @logFunc()
    def tail(self):  # pylint: disable=W0221
        """
        Tail object
        """
        self.bLSDeployment.tailLambda()

    @logFunc()
    def dumpStreamToFile(self, **cwlgDumpStreamToFileKwargs):  # pylint: disable=W0221
        """
        Dump stream to file
        """
        self.bLSDeployment.dumpStreamToFile(**cwlgDumpStreamToFileKwargs)
