#!/usr/bin/env python3
"""
NAME:
 bastion.py

DESCRIPTION
 Bastion Host Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from jinja2 import Environment, FileSystemLoader
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bKeyPair import BeblsoftKeyPair
from base.aws.python.EC2.bInstance import BeblsoftInstance
from cicd.manager.common import CommonManager
from cicd.commands.describe import DescribeData


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------ BASTION MANAGER OBJECT --------------------------- #
class BastionManager(CommonManager):
    """
    Bastion Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bKeyPair              = BeblsoftKeyPair(
            name                          = "BeblsoftCICDKeyPair")
        self.bBastionSecurityGroup = BeblsoftSecurityGroup(
            name                          = "BeblsoftCICDBastionSecurityGroup",
            bVPC                          = self.mgr.network.bVPC,
            ipIngressPermissions          = [{
                "FromPort": 22,       # SSH
                "ToPort": 22,         # SSH
                "IpProtocol": "tcp",  # tcp
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
            }],
            ipEgressPermissions           = [{
                # All ports
                "IpProtocol": "-1",      # tcp,udp,icmp
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
            }])
        self.bSubnet               = self.mgr.network.bPublicSubList[0] # Must be in us-east-1a
        self.instanceName          = "BeblsoftCICDInstance"
        self.bInstance             = BeblsoftInstance(
            name                          = self.instanceName,
            imageId                       = self.cfg.bastion.ami,
            bSubnet                       = self.bSubnet,
            bKeyPair                      = self.bKeyPair,
            bSecurityGroup                = self.bBastionSecurityGroup,
            instanceType                  = self.cfg.bastion.instanceType,
            userDataFunc                  = self.getUserData)

    # ---------------------- USER DATA FUNCTION ----------------------------- #
    @logFunc()
    def getUserData(self):
        """
        Get MIME user data string
        Underlying mime file is actually a Jinja Template
        """
        userDataMimeTemplatePath   = os.path.join(self.cfg.dirs.bastion, 'userData.mime')
        templateDirectory          = os.path.dirname(userDataMimeTemplatePath)
        templateFile               = os.path.basename(userDataMimeTemplatePath)
        jinjaEnv                   = Environment(
            loader                            = FileSystemLoader(templateDirectory),
            trim_blocks                       = True)
        template                   = jinjaEnv.get_template(templateFile)
        templateStr                = template.render(
            GITLAB_RUNNER_AWS_ACCESS_KEY      = os.getenv('GITLAB_RUNNER_AWS_ACCESS_KEY'),
            GITLAB_RUNNER_AWS_SECRET_KEY      = os.getenv('GITLAB_RUNNER_AWS_SECRET_KEY'),
            GITLAB_RUNNER_REGISTRATION_TOKEN  = os.getenv('GITLAB_RUNNER_REGISTRATION_TOKEN'),
            GITLAB_RUNNER_CACHE_BUCKET_NAME   = self.mgr.bucket.bBucket.name,
            GITLAB_RUNNER_CACHE_BUCKET_REGION = self.mgr.bucket.bBucket.bRegion.name,
            GITLAB_RUNNER_EC2_REGION          = self.cfg.aws.bRegion.name,
            GITLAB_RUNNER_VPC_ID              = self.mgr.network.bVPC.id,
            GITLAB_RUNNER_SUBNET_ID           = self.bSubnet.id,
            GITLAB_RUNNER_AVAILABILITY_ZONE   = self.cfg.network.subnetAZ.replace(self.cfg.aws.bRegion.name, ""),
            GITLAB_RUNNER_INSTANCE_TYPE       = self.cfg.bastion.runnerInstanceType)
        return templateStr

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self, support=False, instance=False,
        gitlabRunnerAWSAccessKey=None, gitlabRunnerAWSSecretKey=None, gitlabRunnerAWSRegistrationToken=None):
        """
        Create objects
        """
        if support:
            self.bKeyPair.create()
            self.bBastionSecurityGroup.create()
        if instance:
            assert gitlabRunnerAWSAccessKey is not None
            assert gitlabRunnerAWSSecretKey is not None
            assert gitlabRunnerAWSRegistrationToken is not None
            os.environ["GITLAB_RUNNER_AWS_ACCESS_KEY"]     = gitlabRunnerAWSAccessKey
            os.environ["GITLAB_RUNNER_AWS_SECRET_KEY"]     = gitlabRunnerAWSSecretKey
            os.environ["GITLAB_RUNNER_REGISTRATION_TOKEN"] = gitlabRunnerAWSRegistrationToken
            self.bInstance.create()

    @logFunc()
    def delete(self, support=False, instance=False):
        """
        Delete objects
        """
        if instance:
            self.bInstance.delete()
        if support:
            self.bBastionSecurityGroup.delete()
            self.bKeyPair.delete()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bKeyPair, "Fingerprint={}".format(self.bKeyPair.fingerprint)),
            DescribeData(self.bBastionSecurityGroup, "ID={}".format(self.bBastionSecurityGroup.id)),
            DescribeData(self.bInstance, "ID={} Domain={} State={}".format(
                         self.bInstance.id, self.bInstance.domainName, self.bInstance.state)),
        ]
        DescribeData.logList("BASTION", data)

    # ---------------------- START / STOP ----------------------------------- #
    @logFunc()
    def start(self):
        """
        Start objects
        """
        self.bInstance.start()

    @logFunc()
    def stop(self):
        """
        Stop objects
        """
        self.bInstance.stop()
