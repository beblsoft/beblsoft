Content-Type: multipart/mixed; boundary="==BOUNDARY==================================================================="
MIME-Version: 1.0

--==BOUNDARY===================================================================
MIME-Version: 1.0
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash

echo "START CICD INSTALLATION ------------------------------------------------"
set -x # Print Commands
set -e # Abort on non-zero returns

function gitLabRunner_install() {
  curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
  apt-get install -y gitlab-runner
}

function docker_install() {
  apt-get update -y
  apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  apt-key fingerprint 0EBFCD88
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  apt-get update -y
  apt-get install -y docker-ce docker-ce-cli containerd.io
}

function dockerMachine_install() {
  local base=https://github.com/docker/machine/releases/download/v0.16.0
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine
  install /tmp/docker-machine /usr/local/bin/docker-machine
}

function nano_install() {
  apt-get install -y nano
}

function htop_install() {
  apt-get install -y htop
}

nano_install;
docker_install;
dockerMachine_install;
gitLabRunner_install;

echo "END CICD INSTALLATION --------------------------------------------------"

--==BOUNDARY===================================================================
MIME-Version: 1.0
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash

echo "START GITLAB RUNNER REGISTER -------------------------------------------"
set -x # Print Commands
set -e # Abort on non-zero returns

gitlab-runner register                                          \
	  --non-interactive                                           \
	  --url "https://gitlab.com/"                                 \
	  --registration-token {{ GITLAB_RUNNER_REGISTRATION_TOKEN }} \
	  --executor "docker+machine"                                 \
    --docker-image ubuntu:bionic                                \
	  --description "AWS-Docker-Machine"                          \
	  --tag-list "docker,aws,privileged"                          \
	  --run-untagged                                              \
	  --locked="false"

echo "END GITLAB RUNNER REGISTER ---------------------------------------------"

--==BOUNDARY===================================================================
MIME-Version: 1.0
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash

echo "START GITLAB RUNNER CONFIGURE ------------------------------------------"
set -x # Print Commands
set -e # Abort on non-zero returns

# Grab token from previous registration
token=$(cat /etc/gitlab-runner/config.toml | grep token | awk '{print $3}')

cat > /etc/gitlab-runner/config.toml << EOL
# Global Options
concurrent = 80                                                         # Total number of jobs for whole file, tune to bastion size
check_interval = 0                                                      # Uses default value
log_level = "debug"

[session_server]
  session_timeout = 1800                                                # How long in seconds the session can stay active after the
                                                                        # job completes (which will block job from finishing)
# Docker Machine Runner
[[runners]]
  name = "AWS-Docker-Machine"
  url = "https://gitlab.com/"
  token = GITLAB_RUNNER_TOKEN
  executor = "docker+machine"
  limit = 50
  environment = ["DOCKER_DRIVER=overlay2"]                              # Overlay is faster than AUFS
  [runners.docker]
    tls_verify = false
    image = "ubuntu:bionic"
    privileged = true
    disable_cache = true
    volumes = []
    pull_policy = "always"
  [runners.cache]
    Type = "s3"
    Path = ""
    Shared = true
    [runners.cache.s3]
      ServerAddress = "s3.amazonaws.com"
      AccessKey = "{{ GITLAB_RUNNER_AWS_ACCESS_KEY }}"
      SecretKey = "{{ GITLAB_RUNNER_AWS_SECRET_KEY }}"
      BucketName = "{{ GITLAB_RUNNER_CACHE_BUCKET_NAME }}"
      BucketLocation = "{{ GITLAB_RUNNER_CACHE_BUCKET_REGION }}"        # "us-east-1"
      Insecure = false
  [runners.machine]
    IdleCount = 0
    IdleTime = 3
    MaxBuilds = 4
    MachineDriver = "amazonec2"
    MachineName = "%s"
    MachineOptions = [
      "amazonec2-access-key={{ GITLAB_RUNNER_AWS_ACCESS_KEY }}",
      "amazonec2-secret-key={{ GITLAB_RUNNER_AWS_SECRET_KEY }}",
      "amazonec2-region={{ GITLAB_RUNNER_EC2_REGION }}",                # us-east-1
      "amazonec2-vpc-id={{ GITLAB_RUNNER_VPC_ID }}",
      "amazonec2-subnet-id={{ GITLAB_RUNNER_SUBNET_ID }}",
      "amazonec2-zone={{ GITLAB_RUNNER_AVAILABILITY_ZONE }}",           # a
      "amazonec2-instance-type={{ GITLAB_RUNNER_INSTANCE_TYPE }}",
      "amazonec2-tags=runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true"]
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0
EOL

# Replace token with generated token
sed -i "s/GITLAB_RUNNER_TOKEN/$token/g" /etc/gitlab-runner/config.toml

echo "END GITLAB RUNNER CONFIGURE --------------------------------------------"
