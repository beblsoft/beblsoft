#!/usr/bin/env python3
"""
NAME:
 start.py

DESCRIPTION
 Start Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- START --------------------------------------------- #
@click.command()
@click.option('--bastion', is_flag=True, default=False,
              help="Start bastion objects")
@click.pass_context
def start(ctx, bastion):
    """
    Start Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    if bastion:
        mgr.bastion.start()
