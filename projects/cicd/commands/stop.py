#!/usr/bin/env python3
"""
NAME:
 stop.py

DESCRIPTION
 Stop Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- STOP ---------------------------------------------- #
@click.command()
@click.option('--bastion', is_flag=True, default=False,
              help="Stop bastion objects")
@click.pass_context
def stop(ctx, bastion):
    """
    Stop Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    if bastion:
        mgr.bastion.stop()
