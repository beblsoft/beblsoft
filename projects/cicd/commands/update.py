#!/usr/bin/env python3
"""
NAME:
 update.py

DESCRIPTION
 Update Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- UPDATE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False, help="Update all objects")
@click.option('--ls', is_flag=True, default=False, help="Update lambda switch base objects")
@click.option('--lsse', is_flag=True, default=False, help="Update lambda switch scheduled event objects")
@click.pass_context
def update(ctx, allobjs, ls, lsse):
    """
    Update Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    mgr.ls.update(
        ls   = ls or allobjs,
        lsse = lsse or allobjs)
