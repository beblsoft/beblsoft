#!/usr/bin/env python3
"""
NAME:
 delete.py

DESCRIPTION
 Delete Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- DELETE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False, help="Delete all objects")
@click.option('--netbase', is_flag=True, default=False, help="Delete network base objects")
@click.option('--netpub', is_flag=True, default=False, help="Delete network subnets public")
@click.option('--netprinat', is_flag=True, default=False, help="Delete network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False, help="Delete network subnets private")
@click.option('--netnat', is_flag=True, default=False, help="Delete network NAT objects")
@click.option('--bucket', is_flag=True, default=False, help="Delete bucket objects")
@click.option('--bastionsupport', is_flag=True, default=False, help="Delete bastion support objects")
@click.option('--bastioninstance', is_flag=True, default=False, help="Delete bastion instance objects")
@click.option('--ls', is_flag=True, default=False, help="Delete lambda switch base objects")
@click.option('--lsse', is_flag=True, default=False, help="Delete lambda switch scheduled event objects")
@click.pass_context
def delete(ctx,
           allobjs,
           netbase, netpub, netprinat, netpri, netnat,
           bucket, bastionsupport, bastioninstance, ls, lsse):
    """
    Delete Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    mgr.ls.delete(
        ls   = ls or allobjs,
        lsse = lsse or allobjs)

    mgr.bastion.delete(
        support  = bastionsupport or allobjs,
        instance = bastioninstance or allobjs)

    if bucket or allobjs:
        mgr.bucket.delete()

    mgr.network.delete(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)
