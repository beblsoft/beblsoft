#!/usr/bin/env python3
"""
NAME:
 create.py

DESCRIPTION
 Create Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- CREATE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False, help="Create all objects")
@click.option('--netbase', is_flag=True, default=False, help="Create network base objects")
@click.option('--netpub', is_flag=True, default=False, help="Create network subnets public")
@click.option('--netprinat', is_flag=True, default=False, help="Create network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False, help="Create network subnets private")
@click.option('--netnat', is_flag=True, default=False, help="Create network NAT objects")
@click.option('--bucket', is_flag=True, default=False, help="Create bucket objects")
@click.option('--bastionsupport', is_flag=True, default=False, help="Create bastion support objects")
@click.option('--bastioninstance', is_flag=True, default=False, help="Create bastion instance objects")
@click.option('--gitlabrunnerawsaccesstoken', default=None, help="Gitlab runner aws access token")
@click.option('--gitlabrunnerawssecrettoken', default=None, help="Gitlab runner aws secret token")
@click.option('--gitlabrunnerregistrationtoken', default=None, help="Gitlab runner registration token")
@click.option('--ls', is_flag=True, default=False, help="Create lambda switch base objects")
@click.option('--lsse', is_flag=True, default=False, help="Create lambda switch scheduled event objects")
@click.pass_context
def create(ctx,
           allobjs,
           netbase, netpub, netprinat, netpri, netnat,
           bucket,
           bastionsupport, bastioninstance,
           gitlabrunnerawsaccesstoken, gitlabrunnerawssecrettoken, gitlabrunnerregistrationtoken,
           ls, lsse):
    """
    Create Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    mgr.network.create(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)

    if bucket or allobjs:
        mgr.bucket.create()

    mgr.bastion.create(
        support                          = bastionsupport or allobjs,
        instance                         = bastioninstance or allobjs,
        gitlabRunnerAWSAccessKey         = gitlabrunnerawsaccesstoken,
        gitlabRunnerAWSSecretKey         = gitlabrunnerawssecrettoken,
        gitlabRunnerAWSRegistrationToken = gitlabrunnerregistrationtoken)

    mgr.ls.create(
        ls   = ls or allobjs,
        lsse = lsse or allobjs)
