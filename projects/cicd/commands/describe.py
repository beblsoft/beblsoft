#!/usr/bin/env python3
"""
NAME:
 describe.py

DESCRIPTION
 Describe Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click
from base.bebl.python.color.bColor import BeblsoftColor


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- DESCRIBE DATA ------------------------------------- #
class DescribeData():
    """
    Class to organize describe data
    """

    def __init__(self, obj, objStr):
        """
        Initialize OBject
        Args
          obj:
            Beblsoft Object
            Note: must have an "exists" method
          objStr:
            String describing object state
        """
        self.obj    = obj
        self.objStr = objStr

    @property
    def existStr(self):
        """
        Return exists str
        """
        existStr = ""
        if self.obj.exists:
            existStr = BeblsoftColor.str2Color("EXISTS", BeblsoftColor.GREEN)
        else:
            existStr = BeblsoftColor.str2Color("DNE", BeblsoftColor.RED)
        return existStr

    @staticmethod
    def logList(titleStr, dataList, titleLength=120, titleStartDashes=30):
        """
        Log Describe Data
        Args
          title:
            String title
          dataList:
            List of DescribeData objects
        """
        logger.info("\n{} {} {}".format(
            "-" * titleStartDashes, titleStr,
            "-" * (titleLength - titleStartDashes - len(titleStr))))
        for d in dataList:
            logger.info("{} {} {}".format(
                str(d.obj).ljust(110), d.existStr.ljust(20), d.objStr))


# ----------------------- DESCRIBE ------------------------------------------ #
@click.command()
@click.option('--allobjs', is_flag=True, default=False, help="Describe all objects")
@click.option('--netbase', is_flag=True, default=False, help="Describe network base objects")
@click.option('--netpub', is_flag=True, default=False, help="Describe network subnets public")
@click.option('--netprinat', is_flag=True, default=False, help="Describe network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False, help="Describe network subnets private")
@click.option('--netnat', is_flag=True, default=False, help="Describe network NAT objects")
@click.option('--bucket', is_flag=True, default=False, help="Describe bucket objects")
@click.option('--bastion', is_flag=True, default=False, help="Describe bastion objects")
@click.option('--ls', is_flag=True, default=False, help="Describe lambda switch base objects")
@click.option('--lsse', is_flag=True, default=False, help="Describe lambda switch scheduled event objects")
@click.pass_context
def describe(ctx,
             allobjs,
             netbase, netpub, netprinat, netpri, netnat,
             bucket, bastion, ls, lsse):
    """
    Describe Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.mgr

    mgr.network.describe(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)

    if bucket or allobjs:
        mgr.bucket.describe()

    if bastion or allobjs:
        mgr.bastion.describe()

    mgr.ls.describe(
        ls   = ls or allobjs,
        lsse = lsse or allobjs)
