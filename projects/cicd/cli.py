#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 CICD CLI
"""

# ------------------------ IMPORTS ------------------------------------------ #
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from cicd.gc import gc
from cicd.commands.create import create
from cicd.commands.delete import delete
from cicd.commands.update import update
from cicd.commands.describe import describe
from cicd.commands.start import start
from cicd.commands.stop import stop
from cicd.manager import CICDManager
from cicd.config import Config


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/cicdCLI.log", type=click.Path(writable=True),
              help="Specify log file. Default=/tmp/cicdCLI.log")
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
@click.pass_context
def cli(ctx, logfile, appendlog, verbose):
    """
    CICD Command Line Interface
    """
    ctx.obj["gc"] = gc
    gc.cfg        = Config()
    gc.mgr        = CICDManager(cfg=gc.cfg)
    gc.bLog       = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- COMMANDS ------------------------------------------ #
cli.add_command(create)
cli.add_command(update)
cli.add_command(delete)
cli.add_command(describe)
cli.add_command(start)
cli.add_command(stop)



# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
