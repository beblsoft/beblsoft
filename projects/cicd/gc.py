#!/usr/bin/env python
"""
 NAME:
  gc.py

DESCRIPTION
 Smeckn AWS Global Context
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Objects --------------------------------
        self.mgr           = None
        self.cfg           = None

        # Log ------------------------------------
        self.bLog          = None
        self.cLogFilterMap = {
            "0": logging.CRITICAL,
            "1": logging.WARNING, # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime     = None
        self.endTime       = None


# ----------------------- GLOBALS ------------------------------------------- #
gc = GlobalContext()
