#!/usr/bin/env python3
"""
NAME:
 config.py

DESCRIPTION
 Configuration Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
from pathlib import Path
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.aws.python.EC2.bRegion import BeblsoftRegion


# ------------------------ CONSTANT CONFIG OBJECT --------------------------- #
class Config():
    """
    Configuration Class
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Directories
        self.dirs                       = BeblsoftAttrDict()
        self.dirs.bebl                  = Path(__file__).parents[2]  # beblosft
        self.dirs.base                  = os.path.join(self.dirs.bebl, "base")
        self.dirs.projects              = os.path.join(self.dirs.bebl, "projects")
        self.dirs.cicd                  = Path(__file__).parents[0]
        self.dirs.lambdaSwitch          = os.path.join(self.dirs.cicd, "lambdaSwitch")
        self.dirs.bastion               = os.path.join(self.dirs.cicd, "bastion")

        # AWS Regions
        self.aws                        = BeblsoftAttrDict()
        self.aws.bRegion                = BeblsoftRegion(name="us-east-1")
        self.aws.AZLetters              = ["a", "b", "c", "d", "e", "f"] # Note: a must be first as bastion must go there
        self.aws.AZList                 = ["{}{}".format(self.aws.bRegion.name, l) for l in self.aws.AZLetters]

        # Network
        self.network                    = BeblsoftAttrDict()
        self.network.vpcName            = "BeblsoftCICDVPC"
        self.network.subnetAZ           = "us-east-1a"

        # Bucket
        self.bucket                     = BeblsoftAttrDict()
        self.bucket.name                = "beblsoft-cicd"

        # Bastion Host
        # Ubuntu AMI: https://cloud-images.ubuntu.com/locator/ec2/
        # Gitlab Runner Support for Ubuntu Version: https://docs.gitlab.com/runner/install/linux-repository.html
        self.bastion                    = BeblsoftAttrDict()
        self.bastion.ami                = "ami-07d0cf3af28718ef8"
        self.bastion.instanceType       = "t3.small"
        self.bastion.runnerInstanceType = "t3.medium"
