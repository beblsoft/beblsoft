# Beblsoft CICD Project Documentation

This project implements autoscalling GitLab CI jobs on AWS Virtual machines.

One of the biggest advantages of GitLab Runner is its ability to automatically spin up and down VMs
to make sure builds are processed immediately while still having a cost-effective and scalable solution.

Relevant URLS, Documents:

- [GitLab Autoscaling Runner on AWS](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/)
- __Beblsoft GitLab Docs__: `beblsoft/tools/gitLab/*`
- [Docker-Machine AWS Driver](https://docs.docker.com/machine/drivers/aws/)

Monitoring URLs:

- [Live EC2 Instances](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:sort=instanceState)
- [CICD Lambda Switch Logs](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logStream:group=/aws/lambda/BeblsoftCICDLSDeployment;streamFilter=typeLogStreamPrefix)
- [CICD Lambda Switch](https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/BeblsoftCICDLSDeployment?tab=graph)

## Architecture

### Components

- __GitLab CI Server__: The GitLab Server which holds sources, i.e. [gitlab.com](https://gitlab.com/).

- __Bastion Host__: AWS VM that runs the GitLab Runner binary. GitLab runner connects to GitLab.com and listens
  for incoming CI jobs. When a new CI job arrives, the runner spawns (or reuses) AWS instances to handle the job.

- __Instance__: AWS VM that runs GitLab CI jobs. Spawned and culled by GitLab runner based on number of active
  CI jobs

- __Lambda Switch__: There is a [bug](https://gitlab.com/gitlab-org/gitlab-runner/issues/4634) in the
  gitlab runner where it occasionally orphans instances on AWS. To work around this issue, a lambda
  is periodically fired to teardown any orphans created by the gitlab runner.

### Picture

```text
   -------------
   | GitLab CI |
   | Server    |
   -------------
             |
  ------------------------------------------------------
  | AWS VPC  |                          Public Subnet  |
  |          |                                         |
  |          |                                         |
  |        --|-----------------------                  |
  |        | Beblsoft Bastion Host  |                  |
  |        | GitLab Runner          |                  |
  |        | Docker-Machine         |                  |
  |        --------------------------                  |
  |                    |                               |
  |                    |                               |
  |           -----------------------                  |
  |           |                     |                  |
  |     --------------        --------------           |
  |     | Instance 0 |  ...   | Instance N |           |
  |     | Docker     |        | Docker     |           |
  |     --------------        --------------           |
  |                                                    |
  |----------------------------------------------------|
  |                                    Private Subnet  |
  |    -----------------                               |
  |    | Lambda Switch |                               |
  |    -----------------                               |
  |                                                    |
  ------------------------------------------------------
```

## Setup

```bash
# Help --------------------------------------------------------------------------------------------
$CICD/bin/cicd -h
$CICD/bin/cicd [command] -h

# Install Dependencies ----------------------------------------------------------------------------
$CICD/bin/install all

# Stack Crud --------------------------------------------------------------------------------------
# Create
# Note1: Get the necessary tokens before executing command
# Note2: Bastion is completely configured at boot time via the cloud-init mime file ./bastion/userData.mime
$CICD/bin/cicd create --allobjs                                                                       \
                  --gitlabrunnerawsaccesstoken="AWS access token for gitlab runner"                   \
                  --gitlabrunnerawssecrettoken="AWS secret token for gitlab runner"                   \
                  --gitlabrunnerregistrationtoken="Gitlab runner registration token with Gitlab.com"
# Delete
$CICD/bin/cicd delete --allobjs
# Delete
$CICD/bin/cicd describe --allobjs

# Bastion -----------------------------------------------------------------------------------------
# Login
ssh -i <keyFile> ubuntu@<host>
```

## Bastion Host Debugging

Relevant Commands:

- `cloud-init analyze show`: Ensure cloud init ran successfully

Files to check:

- `/var/log/cloud-init-output.log`, `/var/log/cloud-init.log`: Cloud-init output log files

- `/var/lib/cloud/instances/i-00000XYZ/scripts/*`: Ensure scripts made it over properly

- `/etc/gitlab-runner/config.toml`: Ensure everything looks right, update any changes

- `/var/log/syslog`: Where gitlab-runner logs to. Increase verbosity in `config.toml` to `"debug"`
  for more verbose output

## `config.toml` Notes

- Availability Zones

  Tried running in us-east-1d zone and machines weren't properly spawned
  Ran successfully only when both bastion and slaves in us-east-1a

- Spot Instances

  Tried running with MachineOptions flag "amazonec2-request-spot-instance=true"
  AWS limited the number of concurrent machines to 2
  Only commit to GitLab (w/150 jobs) did not succeeed

- S3 Caching

  When changing the cache values, make sure to restart the runner

- `runners.machine` options

  `amazonec2-use-private-address=true`: Doesn't work, can't find spawned VMs

