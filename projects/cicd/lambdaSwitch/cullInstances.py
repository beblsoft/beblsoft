#!/usr/bin/env python3
"""
 NAME:
  cullInstances.py

 DESCRIPTION
  AWS Handler to Cull Orphaned Instances
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.EC2.bInstance import BeblsoftInstance


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleCullEvent(bRegionName,
                    vpcID,
                    bastionName,
                    **kwargs):  # pylint: disable=W0613
    """
    Handle AWS event to Check and Optionally Cull Orphaned Instances
    This Lambda function is periodically fired.

    Args
      bRegionName:
        Beblsoft Region name of where to cull instances
      vpcID:
        VPC ID where instances are located
        Ex. "vpc-0134661a86asdfasdf"
      bastionName:
        Name of bastion host that spawns runners. The bastion runs forever and is never terminated.
        Ex. "BeblsoftCICDInstance"
    """
    bRegion                 = BeblsoftRegion(name=bRegionName)
    instanceMetaList        = BeblsoftInstance.describeAll(bRegion=bRegion, vpcID=vpcID)
    terminateInstanceIDList = []
    for instanceMeta in instanceMetaList:
        if shouldTerminateInstance(instanceMeta, bastionName=bastionName):
            terminateInstanceIDList.append(instanceMeta.get("InstanceId"))
    BeblsoftInstance.deleteIDList(idList=terminateInstanceIDList, bRegion=bRegion)


# ----------------------- SHOULD TERMINATE ---------------------------------- #
@logFunc()
def shouldTerminateInstance(instanceMeta, bastionName, idleMaxS=120, runningMaxS=1800):
    """
    Make decision as to whether instance should be terminated

    Args:
      instanceMeta
        Instance metadata describing the instance in question
      bastionName
        Name of bastion host that spawns runners. The bastion runs forever and is never terminated.
        Ex. "BeblsoftCICDInstance"
      idleMaxS
        Number of seconds that EC2 runner can be idle for before being torn down
        Ex. 60
      runningMaxS
        Number of seconds that EC2 runner can be running for before being torn down
        Ex. 1800 (5 hours)

    Returns:
      True  - If instance should be terminated
      False - Otherwise
    """
    instanceID          = instanceMeta.get("InstanceId")
    state               = instanceMeta.get("State").get("Name")
    launchTimeUTC       = instanceMeta.get("LaunchTime")
    curTimeUTC          = datetime.utcnow().replace(tzinfo=timezone.utc)
    timeDeltaFromLaunch = curTimeUTC - launchTimeUTC
    tags                = instanceMeta.get("Tags", [])
    name                = None
    terminateInstance   = False

    # Load Name
    for tag in tags:
        if tag.get("Key") == "Name":
            name  = tag.get("Value")
            name  = name if name != "" else None

    # Instance State Cases
    # - Bastion
    #   Never terminate bastion
    # - Running
    #   Without a name: this is a failure by gitlab-runner, wait idleMaxS then kill
    #   Running too long: gitlab-runner forgot to teardown
    # - Hung pending, rebooting, stopping, stopped, or shutting down
    #   Wait idleMaxS before tearing down
    # - Terminated
    #   Already gone
    if name == bastionName:
        terminateInstance = False
    elif state == "running":
        noNameAndIdle     = name is None and timeDeltaFromLaunch.total_seconds() > idleMaxS
        activeAndIdle     = timeDeltaFromLaunch.total_seconds() > runningMaxS
        terminateInstance = noNameAndIdle or activeAndIdle
    elif state in ["pending", "rebooting", "stopping", "stopped", "shutting-down"]:
        terminateInstance = timeDeltaFromLaunch.total_seconds() > idleMaxS
    elif state == "terminated":
        terminateInstance = False
    else:
        logger.info("Unknown instance state {}".format(state))
        terminateInstance = True

    # Log Results
    resultStr = "Terminating Instance" if terminateInstance else "Leaving Instance"
    logger.info("{} InstanceID={} state='{}' name='{}' launchTimeUTC={} curTimeUTC={} timeDeltaFromLaunch={}".format(
        resultStr, instanceID, state, name, launchTimeUTC, curTimeUTC, timeDeltaFromLaunch))

    return terminateInstance
