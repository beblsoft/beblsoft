OVERVIEW
===============================================================================
Mojourney documentation


TODO
===============================================================================
- TODO's are in code formatted as such:
  #TODO: <Text describing todo>
- To see all TODO's execute command:
  grep -r --exclude-dir=venv TODO *


CLIENT
==============================================================================
- Enter client directory                : cd client;
- Install client                        : npm install;
- Start development server              : npm run dev
- Build for production                  : npm run build


SERVER
===============================================================================
- Enter server directory                : cd server;
- Setup virtual environment             : virtualenv -p /usr/bin/python3.5 venv
- Enter python virtual environment      : source venv/bin/activate
- Deactivate virtual environment        : deactivate
- Freeze the dependencies					      : pip3 freeze > requirements.txt
- Install from requirements file	      : pip3 install -r requirements.txt
- Set Mojourney environment variable    : export LASTPASS_PASSWORD=<Password>
- Run development server                : python3 cli.py runserver --port 5000
- Run unit tests                        : python3 cli.py test
- Test HelloWorld                       : curl -v --silent                                 \
                                          -X POST                                          \
                                          localhost:5000/v1.0/test/helloWorld         | jq
- Test Get                              : curl -v --silent                                 \
                                          -X GET                                           \
                                          localhost:5000/v1.0/test/get                | jq
- Test Post                             : curl -v --silent                                 \
                                          -H "Content-Type: application/json"              \
                                          -d '{"username":"xyz","password":"xyz"}'         \
                                          -X POST                                          \
                                          http://localhost:5000/v1.0/test/post        | jq
- Test Delete                           : curl -v --silent                                 \
                                          -X DELETE                                        \
                                          http://localhost:5000/v1.0/test/delete      | jq
- Test Error route                      : curl -v --silent                                 \
                                          localhost:5000/v1.0/test/error              | jq


MYSQL CONFIGURATION
===============================================================================
- Install Server                        : sudo apt-get install mysql-server
                                          sudo service mysql status
- Install Python Connector              : sudo apt-get install python3-dev libmysqlclient-dev
                                          (in virtual environment) pip3 install mysqlclient
- Create user with all
  permissions, no password              : sudo mysql --password;
                                          Enter password:
                                          mysql> uninstall plugin validate_password;
                                          mysql> CREATE USER 'mojourney'@'localhost';
                                          mysql> GRANT ALL PRIVILEGES ON *.* TO 'mojourney'@'localhost' WITH GRANT OPTION;
- Create databases                      : sudo mysql --password;
                                          Enter password:
                                          mysql> create database mojourneyDevelopment;
                                          mysql> create database mojourneyTest;
                                          mysql> create database mojourneyProduction;