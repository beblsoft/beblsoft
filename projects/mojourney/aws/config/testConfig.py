
#!/usr/bin/env python3
"""
NAME:
 testConifg.py

DESCRIPTION
 Test Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from mojourney.aws.config.config import Config
from base.aws.python.EC2.bKeyPair import BeblsoftKeyPair


# ------------------------ TEST CONFIG OBJECT ------------------------------- #
class TestConfig(Config):
    """
    Test Configuration Object
    """

    def __init__(self):
        """
        Initialize Object
        """
        super(TestConfig, self).__init__()

        # ------------- Network --------------------------------------------- #
        self.bKeyPair                                   = BeblsoftKeyPair(
            name                                                = "jbenssonKeyPair")
