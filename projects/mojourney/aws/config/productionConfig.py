#!/usr/bin/env python3
"""
NAME:
 productionConfig.py

DESCRIPTION
 Production Configuration Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
from pathlib import Path
from mojourney.aws.config.config import Config
from base.aws.python.EC2.bVPC import BeblsoftVPC
from base.aws.python.EC2.bSubnet import BeblsoftSubnet
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bInternetGateway import BeblsoftInternetGateway
from base.aws.python.EC2.RouteTable.bRouteTable import BeblsoftRouteTable
from base.aws.python.EC2.bKeyPair import BeblsoftKeyPair
from base.aws.python.EC2.bInstance import BeblsoftInstance
from base.aws.python.RDS.bSubnetGroup import BeblsoftDatabaseSubnetGroup
from base.aws.python.RDS.bDatabaseInstance import BeblsoftDatabaseInstance
from base.aws.python.S3.bBucket import BeblsoftBucket
from base.aws.python.S3.bBucketFile import BeblsoftBucketFile
from base.aws.python.ECS.bRegistry import BeblsoftContainerRegistry
from base.aws.python.ECS.bRepository import BeblsoftContainerRepository
from base.aws.python.ECS.bImage import BeblsoftContainerImage
from base.aws.python.ECS.bCluster import BeblsoftContainerCluster
from base.aws.python.ECS.bTaskDefinition import BeblsoftContainerTaskDefinition
from base.aws.python.ECS.bTask import BeblsoftContainerTask
from base.aws.python.AutoScaling.bLaunchConfiguration import BeblsoftLaunchConfiguration
from base.aws.python.AutoScaling.bAutoScalingGroup import BeblsoftAutoScalingGroup
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.IAM.bInstanceProfile import BeblsoftIAMInstanceProfile


# ------------------------ PRODUCTION CONFIG OBJECT ------------------------- #
class ProductionConfig(Config):
    """
    Production Configuration Object
    """

    def __init__(self):
        """
        Initialize Object
        """
        super(ProductionConfig, self).__init__()

        # ------------- Helper Variables ------------------------------------ #
        self.beblsoftDir          = Path(__file__).parents[3]
        self.baseDir              = "{}/base".format(self.beblsoftDir)
        self.playDir              = "{}/playground".format(self.beblsoftDir)
        self.configDir            = Path(__file__).parents[0]
        self.templateDir          = "{}/templates".format(self.configDir)

        # ------------- Network --------------------------------------------- #
        self.bKeyPair                                   = BeblsoftKeyPair(
            name                                                = "jbenssonKeyPair")
        self.bVPC                                       = BeblsoftVPC(
            name                                                = "mjVPC",
            cidrBlock                                           = "10.0.0.0/16",
            enableDnsHostnames                                  = True,
            enableDnsSupport                                    = True)
        self.bInternetGateway                           = BeblsoftInternetGateway(
            name                                                = "mjInternetGateway",
            bVPC                                                = self.bVPC)

        # ------------- Bastian Host ---------------------------------------- #
        self.bBastianHostSubnet                         = BeblsoftSubnet(
            name                                                = "mjBastianHostSubnet",
            cidrBlock                                           = "10.0.0.0/24",
            bVPC                                                = self.bVPC,
            availabiltyZone                                     = "us-east-1a")
        self.bBastianHostRouteTable                     = BeblsoftRouteTable(
            name                                                = "mjBastianHostRouteTable",
            bVPC                                                = self.bVPC,
            bSubnet                                             = self.bBastianHostSubnet,
            bInternetGateway                                    = self.bInternetGateway)
        self.bBastianHostSecurityGroup                  = BeblsoftSecurityGroup(
            name                                                = "mjBastianHostSecurityGroup",
            bVPC                                                = self.bVPC,
            description                                         = "Mojourney Bastian Host Security Group")
        self.bBastianHostInstance                       = BeblsoftInstance(
            name                                                = "mjBastianHostInstance",
            imageId                                             = "ami-fd8617eb",
            bSubnet                                             = self.bBastianHostSubnet,
            bKeyPair                                            = self.bKeyPair,
            bSecurityGroup                                      = self.bBastianHostSecurityGroup,
            instanceType                                        = "t2.nano")

        # ------------- Database -------------------------------------------- #
        self.bDatabaseSubnet1                           = BeblsoftSubnet(
            name                                                = "mjDatabaseSubnet1",
            cidrBlock                                           = "10.0.10.0/24",
            bVPC                                                = self.bVPC,
            availabiltyZone                                     = "us-east-1b")
        self.bDatabaseRouteTable1                       = BeblsoftRouteTable(
            name                                                = "mjDatabaseRouteTable1",
            bVPC                                                = self.bVPC,
            bSubnet                                             = self.bDatabaseSubnet1,
            bInternetGateway                                    = self.bInternetGateway)
        self.bDatabaseSubnet2                           = BeblsoftSubnet(
            name                                                = "mjDatabaseSubnet2",
            cidrBlock                                           = "10.0.11.0/24",
            bVPC                                                = self.bVPC,
            availabiltyZone                                     = "us-east-1c")
        self.bDatabaseRouteTable2                       = BeblsoftRouteTable(
            name                                                = "mjDatabaseRouteTable2",
            bVPC                                                = self.bVPC,
            bSubnet                                             = self.bDatabaseSubnet2,
            bInternetGateway                                    = self.bInternetGateway)
        self.bDatabaseSecurityGroup                     = BeblsoftSecurityGroup(
            name                                                = "mjDatabaseSecurityGroup",
            bVPC                                                = self.bVPC,
            description                                         = "Mojourney Database Security Group")
        self.bDatabaseSubnetGroup                       = BeblsoftDatabaseSubnetGroup(
            name                                                = "mjDBSubnetGroup",
            description                                         = "Mojourney Database subnet group",
            bSubnets                                            = [self.bDatabaseSubnet1, self.bDatabaseSubnet2])
        self.bDatabaseInstance                          = BeblsoftDatabaseInstance(
            name                                                = "mj",
            instanceId                                          = "mjDBInstance",
            instanceClass                                       = "db.t1.micro",
            engine                                              = "mysql",
            engineVersion                                       = "5.6.35",
            port                                                = 3306,  # MySQL default
            masterUsername                                      = "testUser",
            masterUserPassword                                  = "12345678",
            bSecurityGroup                                      = self.bDatabaseSecurityGroup,
            bDatabaseSubnetGroup                                = self.bDatabaseSubnetGroup,
            allocatedStorageGB                                  = 5,  # GB,
            storageType                                         = "standard",
            backupRetentionPeriod                               = 0,  # Disable automated backups
            multiAZ                                             = False,
            publiclyAccessible                                  = True,
            skipFinalSnapshot                                   = True)

        # ------------- Container Metadata ---------------------------------- #
        self.bContainerRegistry                         = BeblsoftContainerRegistry()
        self.bContainerRepositoryFlask                  = BeblsoftContainerRepository(
            bRegistry                                           = self.bContainerRegistry,
            name                                                = "mojourney/flask")
        self.bContainerImageFlask                       = BeblsoftContainerImage(
            path                                                = "{}/docker/helloFlask".format(
                self.playDir),
            bRepo                                               = self.bContainerRepositoryFlask,
            shortTag                                            = "latest")
        self.bContainerDefinitionFlask                  = BeblsoftContainerTaskDefinition(
            family                                              = "mjFlaskContainerTaskDefinition",
            containerDefinitions                                = [{"name": "mjFlaskContainer1",
                                                                    "bContainerImage": self.bContainerImageFlask,
                                                                    "memory": 124,
                                                                    "portMappings": [{
                                                                        "containerPort": 5000,
                                                                        "hostPort": 5000,
                                                                        "protocol": "tcp"
                                                                    }, ],
                                                                    "essential": True,
                                                                    "environment": [{
                                                                        "name": "FOO",
                                                                        "value": "BAR"
                                                                    }, ],
                                                                    }])

        # ------------- Container Cluster ----------------------------------- #
        self.bContainerClusterSubnet1                   = BeblsoftSubnet(
            name                                                = "mjContainerClusterSubnet1",
            cidrBlock                                           = "10.0.50.0/24",
            bVPC                                                = self.bVPC,
            availabiltyZone                                     = "us-east-1e")
        self.bContainerClusterRouteTable1               = BeblsoftRouteTable(
            name                                                = "mjContainerClusterRouteTable1",
            bVPC                                                = self.bVPC,
            bSubnet                                             = self.bContainerClusterSubnet1,
            bInternetGateway                                    = self.bInternetGateway)
        self.bContainerClusterSubnet2                   = BeblsoftSubnet(
            name                                                = "mjContainerClusterSubnet2",
            cidrBlock                                           = "10.0.51.0/24",
            bVPC                                                = self.bVPC,
            availabiltyZone                                     = "us-east-1f")
        self.bContainerClusterRouteTable2               = BeblsoftRouteTable(
            name                                                = "mjContainerClusterRouteTable2",
            bVPC                                                = self.bVPC,
            bSubnet                                             = self.bContainerClusterSubnet2,
            bInternetGateway                                    = self.bInternetGateway)
        self.bContainerClusterSecurityGroup             = BeblsoftSecurityGroup(
            name                                                = "mjContainerClusterSecurityGroup",
            bVPC                                                = self.bVPC,
            description                                         = "mj Web Server Security Group")
        self.bContainerCluster                          = BeblsoftContainerCluster(
            name                                                = "mjContainerCluster")
        self.bContainerInstanceIAMRole                  = BeblsoftIAMRole(
            name                                                = "mjContantainerInstanceIAMRole",
            trustPolicyPath                                     = "{}/containerInstanceIAMRoleTrustPolicy.json".format(
                                                                  self.configDir))
        self.bContainerInstanceIAMRolePolicy            = BeblsoftIAMRolePolicy(
            name                                                = "mjContantainerInstanceIAMRolePolicy",
            bIAMRole                                            = self.bContainerInstanceIAMRole,
            permissionPolicyPath                                = "{}/containerInstanceIAMRolePermissionPolicy.json".format(
                                                                  self.configDir))
        self.bContainerInstanceIAMProfile               = BeblsoftIAMInstanceProfile(
            name                                                = "mjContainerInstanceIAMProfile",
            bIAMRole                                            = self.bContainerInstanceIAMRole)
        self.bContainerLaunchConfiguration              = BeblsoftLaunchConfiguration(
            name                                                = "mjContainerClusterLaunchConfiguration",
            imageId                                             = "ami-9eb4b1e5",
            instanceType                                        = "t2.nano",
            bKeyPair                                            = self.bKeyPair,
            bIAMInstanceProfile                                 = self.bContainerInstanceIAMProfile,
            bSecurityGroups                                     = [
                self.bContainerClusterSecurityGroup],
            bContainerCluster                                   = self.bContainerCluster)
        self.bContainerInstanceAutoScalingGroup         = BeblsoftAutoScalingGroup(
            name                                                = "mjContainerInstanceAutoScalingGroup",
            bLaunchConfiguration                                = self.bContainerLaunchConfiguration,
            bSubnets                                            = [self.bContainerClusterSubnet1,
                                                                   self.bContainerClusterSubnet2],
            minSize                                             = 1,
            maxSize                                             = 3,
            desiredCapacity                                     = 1)
        self.bContainerTaskFlask                        = BeblsoftContainerTask(
            startedBy                                           = "mjContainerTaskFlask",
            bContainerCluster                                   = self.bContainerCluster,
            bTaskDefinition                                     = self.bContainerDefinitionFlask)

        # ------------- Buckets --------------------------------------------- #
        self.bBucket                                    = BeblsoftBucket(
            name                                                = "jbensson-bucket1",
            acl                                                 = "public-read-write")
        self.bBucketFile                                = BeblsoftBucketFile(
            key                                                 = "requirements.txt",
            bBucket                                             = self.bBucket,
            localPath                                           = "requirements.txt",
            downloadPath                                        = "/tmp/requirements.txt")
