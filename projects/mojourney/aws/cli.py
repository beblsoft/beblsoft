#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 Mojourney Command Line Interface Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import click
from base.aws.python.EC2.bVPC import BeblsoftVPC
from base.aws.python.EC2.bSubnet import BeblsoftSubnet
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bInternetGateway import BeblsoftInternetGateway
from base.aws.python.EC2.RouteTable.bRouteTable import BeblsoftRouteTable
from base.aws.python.EC2.bKeyPair import BeblsoftKeyPair
from base.aws.python.RDS.bSubnetGroup import BeblsoftDatabaseSubnetGroup
from base.aws.python.RDS.bDatabaseInstance import BeblsoftDatabaseInstance
from mojourney.aws.config.productionConfig import ProductionConfig
from mojourney.aws.config.testConfig import TestConfig


# ----------------------- GLOBAL CONTEXT CLASS ------------------------------ #
class GlobalContext():
    """
    Global context for command line interface.
    """

    def __init__(self):
        self.ctx_settings   = dict(help_option_names=["-h", "--help"])
        self.configMap      = {
            "production": ProductionConfig,
            "test": TestConfig
        }
        self.cfgClass        = None
        self.cfg             = None

        # Options
        self.all             = None
        self.network         = None
        self.instances       = None
        self.services        = None
        self.tasks           = None
        self.repos           = None
        self.localimages     = None
        self.remoteimages    = None
        self.taskdefinitions = None


# ----------------------- GLOBALS ------------------------------------------- #
g      = GlobalContext()
logger = logging.getLogger(__file__)


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=g.ctx_settings, options_metavar="[options]")
@click.option("-c", "--configclass", default="production", type=click.Choice(g.configMap.keys()),
              help="Configuration Type")
def cli(configclass):
    """Mojourney Command Line Interface"""

    g.cfgClass = g.configMap[configclass]
    g.cfg = g.cfgClass()
    logging.basicConfig(format="%(asctime)-15s: %(levelname)s %(message)s",
                        level=logging.INFO)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('boto3').setLevel(logging.CRITICAL)


# ------------------------ BASTIAN HOST INSTANCE ---------------------------- #
@cli.group()
def bastian():
    """Bastian Host Instance Interface"""
    pass


@bastian.command()
def run():
    """Run instance"""
    g.cfg.bBastianHostSubnet.create()
    g.cfg.bBastianHostRouteTable.create()
    g.cfg.bBastianHostSecurityGroup.create()
    g.cfg.bBastianHostInstance.run()


@bastian.command()
def terminate():
    """Terminate instance"""
    g.cfg.bBastianHostInstance.terminate()
    g.cfg.bBastianHostSecurityGroup.delete()
    g.cfg.bBastianHostRouteTable.delete()
    g.cfg.bBastianHostSubnet.delete()


@bastian.command()
def describe():
    """Describe Instance"""
    g.cfg.bBastianHostSubnet.describe()
    g.cfg.bBastianHostRouteTable.describe()
    g.cfg.bBastianHostSecurityGroup.describe()
    g.cfg.bBastianHostInstance.describe()


# ------------------------ DATABASE ----------------------------------------- #
@cli.group()
def database():
    """Database Interface"""
    pass


@database.command()
def create():
    """Create database"""
    g.cfg.bDatabaseSubnet1.create()
    g.cfg.bDatabaseRouteTable1.create()
    g.cfg.bDatabaseSubnet2.create()
    g.cfg.bDatabaseRouteTable2.create()
    g.cfg.bDatabaseSecurityGroup.create()
    g.cfg.bDatabaseSubnetGroup.create()
    g.cfg.bDatabase.create()


@database.command()
def delete():
    """Delete database"""
    g.cfg.bDatabase.delete()
    g.cfg.bDatabaseSubnetGroup.delete()
    g.cfg.bDatabaseSecurityGroup.delete()
    g.cfg.bDatabaseRouteTable2.delete()
    g.cfg.bDatabaseSubnet2.delete()
    g.cfg.bDatabaseRouteTable1.delete()
    g.cfg.bDatabaseSubnet1.delete()


@database.command()
def describe(): #pylint: disable=E0102
    """Describe database"""
    g.cfg.bDatabaseSubnet1.describe()
    g.cfg.bDatabaseRouteTable1.describe()
    g.cfg.bDatabaseSubnet2.describe()
    g.cfg.bDatabaseRouteTable2.describe()
    g.cfg.bDatabaseSecurityGroup.describe()
    g.cfg.bDatabaseSubnetGroup.describe()
    g.cfg.bDatabase.describe()


@database.command()
def describe_all(): #pylint: disable=E0102
    """Describe all databases"""
    BeblsoftDatabaseInstance.describeAll()


# ------------------------ CONTAINER CLUSTER -------------------------------- #
@cli.group()
@click.option('--allobjs', is_flag=True, default=False, help="Modify all objects")
@click.option('--network', is_flag=True, default=False, help="Modify network objects")
@click.option('--instances', is_flag=True, default=False, help="Modify instance objects")
@click.option('--services', is_flag=True, default=False, help="Modify services")
@click.option('--tasks', is_flag=True, default=False, help="Modify tasks")
def container_cluster(allobjs, network, instances, tasks, services): #pylint: disable=W0621
    """Container Cluster Interface"""
    g.all       = allobjs
    g.network   = network
    g.instances = instances
    g.services  = services
    g.tasks     = tasks


@container_cluster.command()
def create(): #pylint: disable=E0102
    """Create container cluster objects"""
    if g.network or g.all:
        g.cfg.bContainerClusterSubnet1.create()
        g.cfg.bContainerClusterRouteTable1.create()
        g.cfg.bContainerClusterSubnet2.create()
        g.cfg.bContainerClusterRouteTable2.create()
        g.cfg.bContainerClusterSecurityGroup.create()
    if g.instances or g.all:
        g.cfg.bContainerCluster.create()
        g.cfg.bContainerInstanceIAMRole.create()
        g.cfg.bContainerInstanceIAMRolePolicy.create()
        g.cfg.bContainerInstanceIAMProfile.create()
        g.cfg.bContainerLaunchConfiguration.create()
        g.cfg.bContainerInstanceAutoScalingGroup.create()
    if g.services or g.all:
        pass
    if g.tasks or g.all:
        g.cfg.bContainerTaskFlask.run()


@container_cluster.command()
def delete(): #pylint: disable=E0102
    """Delete container cluster objects"""
    if g.tasks or g.all:
        g.cfg.bContainerTaskFlask.stop()
    if g.services or g.all:
        pass
    if g.instances or g.all:
        g.cfg.bContainerInstanceAutoScalingGroup.delete()
        g.cfg.bContainerLaunchConfiguration.delete()
        g.cfg.bContainerInstanceIAMProfile.delete()
        g.cfg.bContainerInstanceIAMRolePolicy.delete()
        g.cfg.bContainerInstanceIAMRole.delete()
        g.cfg.bContainerCluster.delete()
    if g.network or g.all:
        g.cfg.bContainerClusterSecurityGroup.delete()
        g.cfg.bContainerClusterRouteTable2.delete()
        g.cfg.bContainerClusterSubnet2.delete()
        g.cfg.bContainerClusterRouteTable1.delete()
        g.cfg.bContainerClusterSubnet1.delete()


@container_cluster.command()
def describe(): #pylint: disable=E0102
    """Describe container cluster objects"""
    if g.network or g.all:
        g.cfg.bContainerClusterSubnet1.describe()
        g.cfg.bContainerClusterRouteTable1.describe()
        g.cfg.bContainerClusterSubnet2.describe()
        g.cfg.bContainerClusterRouteTable2.describe()
        g.cfg.bContainerClusterSecurityGroup.describe()
    if g.instances or g.all:
        g.cfg.bContainerCluster.describe()
        g.cfg.bContainerInstanceIAMRole.describe()
        g.cfg.bContainerInstanceIAMRolePolicy.describe()
        g.cfg.bContainerInstanceIAMProfile.describe()
        g.cfg.bContainerLaunchConfiguration.describe()
        g.cfg.bContainerInstanceAutoScalingGroup.describe()
        g.cfg.bContainerInstanceAutoScalingGroup.describeInstances()
    if g.services or g.all:
        pass
    if g.tasks or g.all:
        g.cfg.bContainerTaskFlask.describe()


# ------------------------ CONTAINER IMAGE ---------------------------------- #
@cli.group()
@click.option('--allobjs', is_flag=True, default=False, help="Modify all container metadat objects")
@click.option('--repos', is_flag=True, default=False, help="Modify remote repsitories")
@click.option('--localimages', is_flag=True, default=False, help="Modify local docker images")
@click.option('--remoteimages', is_flag=True, default=False, help="Modify remote docker images")
@click.option('--taskdefinitions', is_flag=True, default=False, help="Modify task definitions")
def container_metadata(allobjs, repos, localimages, remoteimages, taskdefinitions):
    """Container Metadata Interface"""
    g.all             = allobjs
    g.repos           = repos
    g.localimages     = localimages
    g.remoteimages    = remoteimages
    g.taskdefinitions = taskdefinitions

@container_metadata.command()
def create(): #pylint: disable=E0102
    """Create container metadata"""
    if g.repos or g.all:
        g.cfg.bContainerRepositoryFlask.create()
    if g.localimages or g.all:
        g.cfg.bContainerImageFlask.build()
    if g.remoteimages or g.all:
        g.cfg.bContainerImageFlask.push()
    if g.taskdefinitions or g.all:
        g.cfg.bContainerDefinitionFlask.register()


@container_metadata.command()
def delete(): #pylint: disable=E0102
    """Delete container metadata"""
    if g.taskdefinitions or g.all:
        g.cfg.bContainerDefinitionFlask.deregister()
    if g.remoteimages or g.all:
        g.cfg.bContainerImageFlask.remove_remote()
    if g.localimages or g.all:
        g.cfg.bContainerImageFlask.remove_local()
    if g.repos or g.all:
        g.cfg.bContainerRepositoryFlask.delete()


@container_metadata.command()
def describe(): #pylint: disable=E0102
    """Describe container metadata"""
    if g.repos or g.all:
        g.cfg.bContainerRepositoryFlask.describe()
    if g.localimages or g.all:
        g.cfg.bContainerImageFlask.describe()
    if g.remoteimages or g.all:
        g.cfg.bContainerRepositoryFlask.describe_images()
    if g.taskdefinitions or g.all:
        g.cfg.bContainerDefinitionFlask.describe()



# ----------------------- NETWORK ------------------------------------------- #
@cli.group()
def network():
    """Network Interface"""
    pass


@network.command()
def create(): #pylint: disable=E0102
    """Create network"""
    g.cfg.bVPC.create()
    g.cfg.bInternetGateway.create()


@network.command()
def delete(): #pylint: disable=E0102
    """Delete network"""
    g.cfg.bInternetGateway.delete()
    g.cfg.bVPC.delete()


@network.command()
def describe(): #pylint: disable=E0102
    """Describe network"""
    g.cfg.bVPC.describe()
    g.cfg.bInternetGateway.describe()


@network.command()
def describe_all(): #pylint: disable=E0102
    """Describe all network components"""
    BeblsoftVPC.describeAll()
    BeblsoftInternetGateway.describeAll()
    BeblsoftSubnet.describeAll()
    BeblsoftRouteTable.describeAll()
    BeblsoftSecurityGroup.describeAll()
    BeblsoftDatabaseSubnetGroup.describeAll()


# ------------------------ KEY PAIR ----------------------------------------- #
@cli.group()
def keypair():
    """Key Pair Interface"""
    pass


@keypair.command()
def create(): #pylint: disable=E0102
    """Create Key Pair"""
    g.cfg.bKeyPair.create()


@keypair.command()
def delete(): #pylint: disable=E0102
    """Delete Key Pair"""
    g.cfg.bKeyPair.delete()


@keypair.command()
def info():
    """Retrieve Key Pair Info"""
    logger.info("{}:\nFingerprint:{}".format(
        g.cfg.bKeyPair, g.cfg.bKeyPair.fingerprint))


@keypair.command()
def describe_all(): #pylint: disable=E0102
    """Describe all key pairs"""
    BeblsoftKeyPair.describeAll()


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    cli() #pylint: disable=E1120
