#!/usr/bin/env python3
"""
 NAME:
  test.py

 DESCRIPTION
  Test Routes
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Blueprint, request
from mojourney.server.app.kernel.response import JSONResponse
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
bp = Blueprint(__name__, __name__)
logger = logging.getLogger(__name__)


# ------------------------ TEST ROUTES -------------------------------------- #
@bp.route("/helloWorld", methods=["GET"])
def helloWorld():
    """
    Hello World Endpoint
    """
    data = {
        "msg": "HelloWorld!"
    }
    return JSONResponse(data=data)


@bp.route("/get", methods=["GET"])
def get():
    """
    Sample Get Endpoint
    """
    data = {
        "people": [
            {
                "first": "John",
                "last": "Smith",
            },
        ]
    }
    return JSONResponse(data=data)


@bp.route("/post", methods=["POST"])
def post():
    """
    Sample Post Endpoint
    """
    data = request.get_json()
    return JSONResponse(data=data)


@bp.route("/delete", methods=["DELETE"])
def delete():
    """
    Sample Delete Endpoint
    """
    return JSONResponse()


@bp.route("/error", methods=["GET"])
def error():
    """
    Error Endpoint
    """
    raise BeblsoftError(code=BeblsoftErrorCode.DEFAULT_ERROR)
