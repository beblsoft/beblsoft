#!/usr/bin/env python3
"""
 NAME:
  errors.py

 DESCRIPTION
  Error Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import sys
import logging
from mojourney.server.app.app import fApp
from mojourney.server.app.kernel.response import JSONResponse
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ERROR LOGGING ------------------------------------ #
def logError(error):  # pylint: disable=W0613
    """
    Log error
    Args
      error: Excetion instance
    """
    # TODO: Potentially log at higher error levels
    exc_type, exc_value, exc_traceback = sys.exc_info()
    logger.debug("Error thrown", exc_info=(exc_type, exc_value, exc_traceback))


# ------------------------ ERROR HANDLERS ----------------------------------- #
@fApp.errorhandler(BeblsoftError)
def handleServerError(error):
    """
    Handle ServerError
    Args
      error: ServerError instance
    """
    logError(error)
    return JSONResponse(code=error.extData.code)


@fApp.errorhandler(Exception)
def handleException(error):
    """
    Handle generic Exception
    Args
      error: Exception instance
    """
    logError(error)
    return JSONResponse(code=BeblsoftErrorCode.UNKNOWN_ERROR)
