#!/usr/bin/env python3
"""
 NAME:
  test_test.py

 DESCRIPTION
  Test V1 Test Route Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import json
import pprint
import logging
import unittest
from flask_api import status
from mojourney.server.app.app import fApp, db
from mojourney.server.app.kernel.response import JSONResponse
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------- CLASSES ------------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class TestTest(unittest.TestCase):
    """
    Test Test Routes
    """
    BASE_ROUTE        = "/v1.0.0/test"
    HELLO_WORLD_ROUTE = "{}/helloWorld".format(BASE_ROUTE)
    GET_ROUTE         = "{}/get".format(BASE_ROUTE)
    POST_ROUTE        = "{}/post".format(BASE_ROUTE)
    DELETE_ROUTE      = "{}/delete".format(BASE_ROUTE)
    ERROR_ROUTE       = "{}/error".format(BASE_ROUTE)
    POST_DATA         = {
        "cities": [
            {
                "name": "Boston",
                "Country": "USA"
            },
        ]
    }

    @classmethod
    def setUpClass(cls):
        db.create()

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.destroy()

    def setUp(self):
        self.testClient = fApp.test_client()

    def tearDown(self):
        pass

    # ------------------------ HELPER FUNTIONS -------------------- #
    def verifyHTTPResponse(self, resp, httpCode):
        """
        Verify test response
        Args
          resp: response object
          httpCode: expected http code. Ex. HTTP_200_OK
        """
        self.assertEqual(resp.status_code, httpCode)
        logger.debug("Response Dict:\n{}".format(pprint.pformat(resp.__dict__)))

    def verifyBeblsoftResponse(self, resp, bCode, bData):
        """
        Verify test response
        Args
          resp: response object
          httpCode: expected http code. Ex. HTTP_200_OK
          bCode: beblsoft error code. Ex. BeblsoftErrorCode.SUCCESS
          bMsg: beblsoft error message. Ex "Success"
          bData: dictionary of data
        """
        logger.debug("Response Dict:\n{}".format(pprint.pformat(resp.__dict__)))
        rData   = json.loads(resp.get_data().decode("ascii"))
        rBCode  = rData.get("code", None)
        rBErr   = BeblsoftError(code=rBCode)
        rBMsg   = rData.get("msg", None)
        rBMagic = rData.get("magic", None)
        rBData  = rData.get("data", None)
        logger.debug("Response Data:\n{}".format(pprint.pformat(rBData)))
        self.assertEqual(resp.status_code, rBErr.httpStatus)
        self.assertEqual(rBCode, bCode)
        self.assertEqual(rBMagic, JSONResponse.MAGIC)
        self.assertEqual(rBMsg, rBErr.extMsg)
        self.assertEqual(rBData, bData)

    # ------------------------ TESTS ------------------------------ #
    @logFunc()
    def test_invalidMethod(self):
        """
        Test invalid methods
        """
        resp = self.testClient.get(TestTest.POST_ROUTE)
        self.verifyHTTPResponse(resp, status.HTTP_405_METHOD_NOT_ALLOWED)

        resp = self.testClient.post(TestTest.GET_ROUTE)
        self.verifyHTTPResponse(resp, status.HTTP_405_METHOD_NOT_ALLOWED)

        resp = self.testClient.put(TestTest.GET_ROUTE)
        self.verifyHTTPResponse(resp, status.HTTP_405_METHOD_NOT_ALLOWED)

        resp = self.testClient.delete(TestTest.GET_ROUTE)
        self.verifyHTTPResponse(resp, status.HTTP_405_METHOD_NOT_ALLOWED)

    @logFunc()
    def test_helloWorld(self):
        """
        Test hello world route
        """
        resp = self.testClient.get(TestTest.HELLO_WORLD_ROUTE)
        self.verifyBeblsoftResponse(resp, bCode=BeblsoftErrorCode.SUCCESS,
                                    bData={"msg": "HelloWorld!"})

    @logFunc()
    def test_get(self):
        """
        Test get route
        """
        resp = self.testClient.get(TestTest.GET_ROUTE)
        self.verifyBeblsoftResponse(resp, bCode=BeblsoftErrorCode.SUCCESS,
                                    bData={"people": [{"first": "John", "last": "Smith"}]})

    @logFunc()
    def test_post(self):
        """
        Test post route
        """
        resp = self.testClient.post(TestTest.POST_ROUTE, data=json.dumps(TestTest.POST_DATA),
                                    content_type="application/json")
        self.verifyBeblsoftResponse(resp, bCode=BeblsoftErrorCode.SUCCESS,
                                    bData=TestTest.POST_DATA)

    @logFunc()
    def test_delete(self):
        """
        Test delete route
        """
        resp = self.testClient.delete(TestTest.DELETE_ROUTE)
        self.verifyBeblsoftResponse(resp, bCode=BeblsoftErrorCode.SUCCESS,
                                    bData={})

    @logFunc()
    def test_error(self):
        """
        Test error route
        """
        resp = self.testClient.get(TestTest.ERROR_ROUTE)
        self.verifyBeblsoftResponse(resp, bCode=BeblsoftErrorCode.DEFAULT_ERROR,
                                    bData={})
