#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Route Module Initialization
"""


# ---------------------------- IMPORTS -------------------------------------- #
from . import user
from . import test
