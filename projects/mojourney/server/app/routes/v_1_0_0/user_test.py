#!/usr/bin/env python3
"""
 NAME:
  test_user.py

 DESCRIPTION
  Test V1 User Route Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import unittest
import json
import logging
from mojourney.server.app.app import fApp, db, ljwt
from mojourney.server.app.models.user import User
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------- CLASSES ------------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class UserTest(unittest.TestCase):
    """
    Test User Routes
    """
    EMAIL           = "darrylpblack@gmail.com"
    PASSWORD        = "1234"
    LOGIN_ROUTE     = "/v1.0.0/user/login"
    GET_USER_ROUTE  = "/v1.0.0/user/darrylpblack@gmail.com"

    @classmethod
    def setUpClass(cls):
        db.create()

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.destroy()

    def setUp(self):
        user = User(email=self.EMAIL, password=self.PASSWORD)
        db.session.add(user)
        db.session.commit()
        self.testClient = fApp.test_client()

    def tearDown(self):
        user = User.getByEmail(self.EMAIL)
        db.session.delete(user)
        db.session.commit()

    # ------------------------ HELPER FUNCTIONS --------------------- #
    @logFunc()
    def postJSON(self, route, d, headers=None):
        """
        Post dictionary of json data to route
        Args
          route: route to post to
          d: dictionary of data to post
          headers: header dictionary to post
        Returns
          response object
        """
        resp = self.testClient.post(route, data=json.dumps(d), headers=headers,
                                    content_type="application/json")
        return resp

    @logFunc()
    def getLoginToken(self, email, password):
        """
        Retrieve login token for specific email and password
        Args
          email: user email address
          password: user password
        Returns
          login token
        """
        inData   = dict(email=email, password=password)
        resp     = self.postJSON(self.LOGIN_ROUTE, inData)
        respDict = json.loads(resp.data.decode("utf-8"))
        data     = json.loads(respDict.get("data"))
        return data.get("token")

    # ------------------------ TEST LOGIN --------------------------- #
    @logFunc()
    def test_loginValid(self):
        """
        Test valid login
        """
        token   = self.getLoginToken(email=self.EMAIL, password=self.PASSWORD)
        payload = ljwt.getPayloadFromJWT(token)
        user    = ljwt.getUserFromPayload(payload)
        self.assertEqual(user.email, self.EMAIL)

    @logFunc()
    def test_loginNoData(self):
        """
        Test login with no data
        """
        resp     = self.testClient.post(self.LOGIN_ROUTE)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.GEN_JSON_REQUIRED)
        self.assertRegexpMatches("JSON required", msg) #pylint: disable=W1505

    @logFunc()
    def test_loginNoEmail(self):
        """
        Test login with no email
        """
        inData   = {}
        resp     = self.postJSON(self.LOGIN_ROUTE, inData)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.AUTH_NO_EMAIL)
        self.assertRegexpMatches("JSON contains no email field", msg) #pylint: disable=W1505

    @logFunc()
    def test_loginNoPassword(self):
        """
        Test login with no password
        """
        inData   = dict(email=self.EMAIL)
        resp     = self.postJSON(self.LOGIN_ROUTE, inData)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.AUTH_NO_PASSWORD)
        self.assertRegexpMatches("JSON contains no password field", msg) #pylint: disable=W1505

    @logFunc()
    def test_loginBadEmail(self):
        """
        Test login with invalid email
        """

        inData   = dict(email="foo@gmail.com", password=self.PASSWORD)
        resp     = self.postJSON(self.LOGIN_ROUTE, inData)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.AUTH_INVALID_EMAIL)
        self.assertRegexpMatches("Invalid credentials", msg) #pylint: disable=W1505

    @logFunc()
    def test_loginBadPassword(self):
        """
        Test login with invalid password
        """
        inData   = dict(email=self.EMAIL, password="5678")
        resp     = self.postJSON(self.LOGIN_ROUTE, inData)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.AUTH_INVALID_PASSWORD)
        self.assertRegexpMatches("Invalid credentials", msg) #pylint: disable=W1505


    # ------------------------ TEST API ACCESS ---------------------- #
    @logFunc()
    def test_APIValidJWT(self):
        """
        Test that login JWT can be used to query API
        """
        token    = self.getLoginToken(email=self.EMAIL, password=self.PASSWORD)
        headers  = dict(Authorization=token)
        resp     = self.testClient.get(self.GET_USER_ROUTE, headers=headers)
        respDict = json.loads(resp.data.decode("utf-8"))
        data     = json.loads(respDict.get("data"))
        self.assertEqual(self.EMAIL, data.get("email"))

    @logFunc()
    def test_APINoJWT(self):
        """
        Test that API can't be queried without JWT
        """
        resp     = self.testClient.get(self.GET_USER_ROUTE)
        respDict = json.loads(resp.data.decode("utf-8"))
        code     = respDict.get("code", None)
        msg      = respDict.get("msg", None)
        self.assertEqual(code, BeblsoftErrorCode.JWT_NO_TOKEN)
        self.assertRegexpMatches("No token attached to request object", msg) #pylint: disable=W1505
