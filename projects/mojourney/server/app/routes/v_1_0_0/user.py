#!/usr/bin/env python3
"""
 NAME:
  user.py

 DESCRIPTION
  User Routes
"""


# ------------------------ IMPORTS ------------------------------------------ #
import json
import logging
from flask import Blueprint, request
from mojourney.server.app.app import ljwt
from mojourney.server.app.models.user import User, UserEncoder
from mojourney.server.app.kernel.response import JSONResponse
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.decorators.bDecorators import jsonRequired


# ------------------------ GLOBALS ------------------------------------------ #
bp = Blueprint(__name__, __name__)
logger = logging.getLogger(__name__)


# ------------------------ AUTHENTICATION ROUTES ---------------------------- #
# Login Workflow Timeline:
# [Client] www.mojourney.com/not-logged-in/account/log-in
#          User inputs credentials, client forwards data to server.
# [Server] app.mojourney.com/v1.0/user/login
#          data {"email": <email>, "password": <password>}
#          Server verifies credentials, on success returns LoginJWT to client
#          LoginJWT encodes User.uid
# [Client] Caches token for future API requests
# [Server] Protects API endpoints with @ljwt.required()
#          @ljwt.required enforces that request Authentication header is set with
#          a valid token, and then loads the appropriate user into g.user
#
@bp.route("/login", methods=["POST"])
@jsonRequired
def login():
    """
    Login to Server API
    JSON Args
      email: user email
      password: user password
    Raises
      AuthError
    """
    data = request.get_json()
    email = data.get("email", None)
    password = data.get("password", None)

    if email is None:
        raise(BeblsoftError(code=BeblsoftErrorCode.AUTH_NO_EMAIL))
    if password is None:
        raise(BeblsoftError(code=BeblsoftErrorCode.AUTH_NO_PASSWORD))

    user = User.getByEmail(email)
    if user is None:
        raise(BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_EMAIL))
    if not user.verifyPassword(password):
        raise(BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_PASSWORD))

    payload = ljwt.getPayloadFromUserId(user.uid)
    token = ljwt.getJWTFromPayload(payload)
    data = json.dumps({"token": token})
    return JSONResponse(data=data)


# Signup Workflow Timeline:
# [Client] www.mojourney.com/not-logged-in/account/start-sign-up
#          User inputs credentials, client forwards data to server
# [Server] app.mojourney.com/v1.0/user/startSignUp
#          data {"email": <email>, "password": <password>}
#          Server sends client email with SignUpJWT
#          SignUpJWT encodes User.email User.password
# [Client] Clicks link in email
#          www.mojourney.com/not-logged-in/account/finish-sign-up?token=<token>
#          Client immediately forwards token to server
# [Server] app.mojourney.com/v1.0/user/finishSignUp
#          data {"token":<token>}
#          Server decodes SignUpJWT, adds user to database, returns LoginJWT
@bp.route("/startSignUp", methods=["POST"])
@jsonRequired
def startSignUp():
    """
    Start signing up a user
    """
    pass


@bp.route("/finishSignUp", methods=["POST"])
@jsonRequired
def finishSignUp():
    """
    Finish signing up a user
    """
    pass


# Reset Password Workflow Timeline:
# [Client] www.mojourney.com/not-logged-in/account/start-reset-password
#          User inputs email, client forwards data to server
# [Server] app.mojourney.com/v1.0/user/start-reset-password
#          data {"email": <email>}
#          Server sends client email with ResetPasswordJWT
#          ResetPasswordJWT encodes User.email
# [Client] Clicks link in email
#          www.mojourney.com/not-logged-in/account/finish-reset-password?token=<token>
#          User enters new password, client forwards token and password to server
# [Server] app.mojourney.com/v1.0/user/finishResetPassword
#          data {"token":<token>, "password":<password>}
#          Server decodes ResetPasswordJWT, updates the user's password
@bp.route("/startResetPassword", methods=["POST"])
@jsonRequired
def startResetPassword():
    """
    Start reseting user"s password
    """
    pass


@bp.route("/finishResetPassword", methods=["POST"])
@jsonRequired
def finishResetPassword():
    """
    Finish reseting user"s password
    """
    pass


# ------------------------ CRUD ROUTES -------------------------------------- #
@bp.route("/<string:email>", methods=["GET"])
@ljwt.required()
def getByEmail(email):
    """
    Return user specified by email
    """
    user = User.getByEmail(email)
    data = json.dumps(user, cls=UserEncoder)
    return JSONResponse(data=data)
