#!/usr/bin/env python3
"""
 NAME
  user.py

 DESCRIPTION
  User Model
"""

# ----------------------------- IMPORTS ------------------------------------- #
import enum
import json
import random
from datetime import datetime
from sqlalchemy import Column, String, Integer, DateTime, Enum
from sqlalchemy.orm import relationship
import forgery_py
from werkzeug.security import generate_password_hash, check_password_hash
from mojourney.server.app.app import db


# ----------------------------- ENUMERATIONS -------------------------------- #
class Sex(enum.Enum):
    """
    Sex Enumeration
    """
    NONE   = 0
    MALE   = 1
    FEMALE = 2


class AccountType(enum.Enum):
    """
    Account Type Enumeration
    """
    ADMIN      = 1
    INSTRUCTOR = 2
    REGULAR    = 3


# ----------------------------- USER CLASS ---------------------------------- #
class User(db.base):
    """
    User Table
    Relationships
      User-UserSession           : One-To-Many
      User-UserExercise-Exercise : Many-To-Many

    """
    __tablename__ = "user"
    uid           = Column(Integer, primary_key=True)
    email         = Column(String(256), unique=True)
    passwordHash  = Column(String(256))
    sex           = Column(Enum(Sex), default=Sex.NONE)
    accountType   = Column(Enum(AccountType), default=AccountType.REGULAR)
    creationDate  = Column(DateTime, default=datetime.utcnow())
    sessions      = relationship("UserSession", cascade="all, delete-orphan",
                                 back_populates="user")
    exercises     = relationship("UserExercise", back_populates="user")

    def __repr__(self):
        return "User[uid={}, email={}]".format(self.uid, self.email)

    @property
    def password(self):
        """
        User password
        """
        raise AttributeError("Password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.passwordHash = generate_password_hash(password)

    def verifyPassword(self, password):
        """
        Returns
          True if correct password
        """
        return check_password_hash(self.passwordHash, password)

    @staticmethod
    def addFakeUsers(nUsers=100):
        """
        Add fake users to the database
        Args
          nUsers: number of fake users to add to the database
        """
        for i in range(nUsers):  # pylint: disable=W0612
            u = User(email=forgery_py.internet.email_address(),
                     password=forgery_py.lorem_ipsum.word(),
                     sex=random.choice(list(Sex)),
                     accountType=random.choice(list(AccountType)))
            db.session.add(u)
            db.session.commit()

    @staticmethod
    def getByEmail(email):
        """
        Get user from email
        """
        # TODO: catch one_or_none() exception
        return db.session.query(User).filter(User.email == email).one_or_none()

    @staticmethod
    def getById(uid):
        """
        Get user from uid
        """
        # TODO: catch one_or_none() exception
        return db.session.query(User).filter(User.uid == uid).one_or_none()

    @staticmethod
    def getAll():
        """
        Retrieve all users
        """
        return db.session.query(User).all()


# ----------------------------- USER ENCODER -------------------------------- #
class UserEncoder(json.JSONEncoder):
    """
    Encode User class into JSON
    """
    ENCODE_FIELDS = ["email", "sex", "accountType", "creationDate"]

    def default(self, o): #pylint: disable=E0202
        """
        Encode user into JSON data
        """
        fields = {}
        for field in UserEncoder.ENCODE_FIELDS:
            data = o.__getattribute__(field)
            fields[field] = str(data)
        return fields
