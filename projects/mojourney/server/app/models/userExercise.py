#!/usr/bin/env python3
"""
 NAME
  userExercise.py

 DESCRIPTION
  User Exercise Model
"""


# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import Column, Integer, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from mojourney.server.app.app import db


# ----------------------- USER EXERCISE CLASS ------------------------------- #
class UserExercise(db.base):
    """
    User Exercise Association Table
    """
    __tablename__ = "user_exercise"
    ueid          = Column(Integer, primary_key=True)
    userId        = Column(Integer, ForeignKey("user.uid"))
    exerciseId    = Column(Integer, ForeignKey("exercise.eid"))
    user          = relationship("User", back_populates="exercises")
    exercise      = relationship("Exercise", back_populates="users")

    following     = Column(Boolean, default=False)
    nLikes        = Column(Integer, default=0)
    ndisLikes     = Column(Integer, default=0)
