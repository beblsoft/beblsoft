#!/usr/bin/env python3
"""
 NAME:
  test_user.py

 DESCRIPTION
  User data model tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import unittest
import logging
from mojourney.server.app.app import db
from mojourney.server.app.models.user import User


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class UserTestCase(unittest.TestCase):
    """
    Test User Functionality
    """
    @classmethod
    def setUpClass(cls):
        db.create()

    @classmethod
    def tearDownClass(cls):
        db.session.remove()
        db.destroy()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_userPassword(self):
        """
        Test user password functionality
        """
        # Test user password setting and verification
        u1 = User(email="darrylpblack@gmail.com", password="1234")
        u2 = User(email="bensson.james@gmail.com", password="1234")
        db.session.add_all([u1, u2])
        db.session.commit()

        with self.assertRaises(AttributeError):
            logger.info(u1.password)

        self.assertTrue(u1.verifyPassword("1234"))
        self.assertFalse(u1.verifyPassword("5678"))

        self.assertNotEqual(u1.passwordHash, u2.passwordHash,
                            "Password hashes same for same password")
