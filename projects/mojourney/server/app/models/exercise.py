#!/usr/bin/env python3
"""
 NAME
  exercise.py

 DESCRIPTION
  Exercise Model
"""


# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import Column, Integer
from sqlalchemy.orm import relationship
from mojourney.server.app.app import db


# ----------------------------- USER CLASS ---------------------------------- #
class Exercise(db.base):
    """
    Exercise Table
    """
    __tablename__ = "exercise"
    eid           = Column(Integer, primary_key=True)
    users         = relationship("UserExercise", back_populates="exercise", )
