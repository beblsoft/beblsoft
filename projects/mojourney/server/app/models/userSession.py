#!/usr/bin/env python3
"""
 NAME
  userSession.py

 DESCRIPTION
  User Session
"""


# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from mojourney.server.app.app import db


# ----------------------- USER EXERCISE CLASS ------------------------------- #
class UserSession(db.base):
    """
    User Session Assosiation Table
    """
    __tablename__ = "user_session"
    usid          = Column(Integer, primary_key=True)
    userId        = Column(Integer, ForeignKey("user.uid"))
    user          = relationship("User", back_populates="sessions")
