#!/usr/bin/env python3
"""
 NAME:
  app.py

 DESCRIPTION
  Application Functionality
"""


# --------------------------- GLOBALS --------------------------------------- #
log       = None
bLastpass = None
db        = None
ljwt      = None
fApp      = None
bEmail    = None


# -------------------------- PACKAGE FACTORY -------------------------------- #
def factory(cfg, **kwargs):
    """
    Factory function creating all package objects
    Args
      cfg: configuration object. See config.py
      kwargs: arbitrary dictionary args to pass through
    Note
      Order matters here
    """
    logFactory(logFile=kwargs.get("logFile", None),
               cVerbose=kwargs.get("cVerbose", False))
    lpFactory(cfg, login=kwargs.get("login", False))
    dbFactory(cfg)
    ljwtFactory(cfg)
    appFactory(cfg)
    mailFactory(cfg)


# -------------------------- LOG FACTORY ------------------------------------ #
def logFactory(logFile, cVerbose):
    """
    Log Factory function
    Initialize the package's log instance
    Args
      cfg: configuration class
      logFile: file to log to
      cVerbose: if True, verbose console printing
    """
    from mojourney.server.app.kernel.log import Log
    global log

    log = Log(logFile, cVerbose)


# -------------------------- LASTPASS FACTORY ------------------------------- #
def lpFactory(cfg, login=False):
    """
    LastPass Factory function
    Initializes the package's lastpass instance
    Args
      cfg: configuration class
      login: explicitly login to lastpass
    """
    from base.lastpass.python.bLastpass import BeblsoftLastpass
    global bLastpass

    bLastpass = BeblsoftLastpass(cfg.LASTPASS_MASTER, cfg.LASTPASS_PASSWORD,
                                 cfg.LASTPASS_ACCOUNTS, login)
    bLastpass.loadAllPasswords()


# -------------------------- LOGIN JWT FACTORY ------------------------------ #
def ljwtFactory(cfg):
    """
    Login JWT Factory function
    Initializes the package's login jwt instance
    Args
      cfg: configuration class
    """
    from mojourney.server.app.kernel.jwt import LoginJWT
    global ljwt

    ljwt = LoginJWT(secret=cfg.JWT_SECRET, algorithm=cfg.JWT_ALGORITHM,
                    expDelta=cfg.JWT_EXP_DELTA, nbfDelta=cfg.JWT_NBF_DELTA)


# -------------------------- DATABASE FACTORY ------------------------------- #
def dbFactory(cfg):
    """
    Database factory function
    Initializes the package's database instance
    Args
      cfg: configration class
    """
    from mojourney.server.app.kernel.database import Database
    global db

    db = Database(engine=cfg.DB_ENGINE, echo=cfg.DB_ECHO)

    # Register all models with the DB
    from mojourney.server.app.models import user  #pylint: disable=W0612,W0611
    from mojourney.server.app.models import userExercise  #pylint: disable=W0612,W0611
    from mojourney.server.app.models import userSession  #pylint: disable=W0612,W0611
    from mojourney.server.app.models import exercise  #pylint: disable=W0612,W0611


# -------------------------- APPLICATION FACTORY ---------------------------- #
def appFactory(cfg):
    """
    Application factory function
    Initializes the package's application instance
    Args
      cfg: configuration class
      database: database
    """
    from flask import Flask
    from flask_cors import CORS
    global fApp

    fApp       = Flask(__name__)
    fApp.debug = cfg.DEBUG
    CORS(fApp)

    # Register all application blueprints
    from mojourney.server.app.routes.v_1_0_0.user import bp as userBP
    fApp.register_blueprint(userBP, url_prefix="/v1.0.0/user")

    from mojourney.server.app.routes.v_1_0_0.test import bp as testBP
    fApp.register_blueprint(testBP, url_prefix="/v1.0.0/test")

    # Register error handlers
    import mojourney.server.app.routes.v_1_0_0.errors   #pylint: disable=C0415,W0611


# ---------------------------- MAIL FACTORY --------------------------------- #
def mailFactory(cfg):
    """
    Mail Factory function
    Initializes the package's mail instance
    """
    from base.bebl.python.email.bEmail import BeblsoftEmail
    global bEmail

    bEmail = BeblsoftEmail(account=cfg.MAIL_FROM,
                           password=bLastpass.getPassword("google.com"))
