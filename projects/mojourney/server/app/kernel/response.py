#!/usr/bin/env python3
"""
 NAME:
  response.py

 DESCRIPTION
  Response Classes
"""


# ------------------------ IMPORTS ------------------------------------------ #
import json
from flask import Response
from base.bebl.python.error.bData import BeblsoftErrorData
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ JSON RESPONSE ------------------------------------ #
class JSONResponse(Response):
    """JSON Response Class"""
    MAGIC = 8152020


    def __init__(self, code=BeblsoftErrorCode.SUCCESS, data=None, **kwargs):
        """
        Initialize respone object
        Args
          status: HTTP status code
          code: Beblsoft error code
          data: String of data
        """
        super().__init__(self, **kwargs)
        self.mimetype    = "application/json"
        self.errorData   = BeblsoftErrorData.getByCode(code)
        self.status_code = self.errorData.httpStatus
        if not data:
            data         = {}
        dataDict         = dict(magic=JSONResponse.MAGIC, msg=self.errorData.extMsg, code=code, data=data)
        self.data        = json.dumps(dataDict)
