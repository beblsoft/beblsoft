#!/usr/bin/env python3
"""
 NAME:
  jwt.py

 DESCRIPTION
  JSON Web Token Classes
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from functools import wraps
from flask import g, request
import jwt #pylint: disable=W0406
from mojourney.server.app.models.user import User
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- BASE JWT CLASS ------------------------------- #
class BaseJWT():
    """Base JWT Class"""

    def __init__(self, secret, algorithm, expDelta,
                 nbfDelta=timedelta(seconds=0),
                 leeway=timedelta(seconds=0)):
        """
        Initialize JWT Class
        Args
          secret: secret used to encode JWT
          algorithm: algorithm used to encode JWT. ex. "HS256", "HS384", "HS512"
          expDelta: expiration delta, time after current time that JWT should expire
          nbfDelta: not before delta, time after current time where JWT should not be allowed
          leeway: time after expDelta where JWT allowed
        Other JWT terminology:
          iat = issued at
          iss = issuer
          aud = audience
        """
        self.secret            = secret
        self.algorithm         = algorithm
        self.expDelta          = expDelta
        self.nbfDelta          = nbfDelta
        self.authHeaderName    = "Authorization"
        self.authHeaderPrefix  = "JWT"
        self.verifyClaims      = ["signature", "exp", "nbf", "iat"]
        self.dontVerifyClaims  = ["aud"]
        self.requiredClaims    = ["exp", "iat", "nbf"]
        self.leeway            = leeway


# ---------------------------- LOGIN JWT CLASS ------------------------------ #
class LoginJWT(BaseJWT):
    """Login JWT Class"""

    def __init__(self, *args, **kwargs):
        BaseJWT.__init__(self, *args, **kwargs)

    def required(self, perms=None):
        """
        Route decorator that requires a login JWT.
        On success, g.user will be set to the user specified in the JWT.
        Args
          perms: user permissions required to access the route
        """
        def wrapper(func):  # pylint: disable=C0111
            @wraps(func)
            def decorator(*args, **kwargs):  # pylint: disable=C0111
                self._required(perms)
                return func(*args, **kwargs)
            return decorator
        return wrapper

    @logFunc()
    def _required(self, perms=None):
        """
        Do the work of verifying the request JWT and user permissions.
        On success, set g.user
        Args
          perms: user permissions required to access the route
        """
        token = self.getJWTFromRequest(request)
        if token is None:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_TOKEN)

        payload = self.getPayloadFromJWT(token)
        g.user = self.getUserFromPayload(payload)
        if perms:
            # TODO: check if user has correct permissions
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_INVALID_PERMISSIONS)

    @logFunc()
    def getJWTFromRequest(self, req):
        """
        Get a JWT from a request
        Args
          req: Flask Request
        Returns
          JWT
        """
        authHeader = req.headers.get(self.authHeaderName, None)
        if authHeader is None:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_TOKEN)

        parts = authHeader.split()
        if len(parts) > 1:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_TOKEN_SPACES)
        return parts[0]

    @logFunc()
    def getPayloadFromJWT(self, token):
        """
        Decode JWT and return payload
        Args
          token: JWT
        Returns
          payload
        """
        options = {}
        options.update({
            "verify_" + claim: True
            for claim in self.verifyClaims
        })
        options.update({
            "verify_" + claim: False
            for claim in self.dontVerifyClaims
        })
        options.update({
            "require_" + claim: True
            for claim in self.requiredClaims
        })
        try:
            payload = jwt.decode(str(token), key=self.secret, options=options, #pylint: disable=E1101
                                 algorithms=[self.algorithm], leeway=self.leeway)
            return payload
        except jwt.InvalidTokenError as e: #pylint: disable=E1101
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_INVALID_TOKEN,
                                originalError=e)

    @logFunc()
    def getUserFromPayload(self, payload):  # pylint: disable=R0201
        """
        Decode payload and return user
        Args
          payload: JWT payload
        Returns
          user associated with JWT
        """
        uid = payload.get("uid", None)
        if uid is None:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_PAYLOAD)
        user = User.getById(uid)
        if user is None:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_ASSOCIATED_USER)
        return user

    @logFunc()
    def getPayloadFromUserId(self, uid):
        """
        Create payload from user id
        Args
          uid: user id
        Returns
          payload
        """
        iat = datetime.now()
        exp = iat + self.expDelta
        nbf = iat + self.nbfDelta
        return {"uid": uid, "iat": iat, "exp": exp, "nbf": nbf}

    @logFunc()
    def getJWTFromPayload(self, payload):
        """
        Create JWT from payload
        Args
          payload: payload of data to encode
        Returns
          token
        """
        return jwt.encode(payload, key=self.secret, #pylint: disable=E1101
                          algorithm=self.algorithm).decode("utf-8")
