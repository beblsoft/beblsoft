#!/usr/bin/env python3
"""
 NAME:
  log.py

 DESCRIPTION
  Log Class
"""


# ---------------------------- IMPORTS -------------------------------------- #
import sys
import logging
from datetime import datetime


# ---------------------------- IMPORTS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- LOG CLASS ------------------------------------ #
class Log():
    """Log Class"""

    def __init__(self, logFile, cVerbose, nHeaderStars=90):
        """
        Initialize Mojourney Server Logging Capabilities
        Args
          logFile: log file to write to
          cVerbose: if True, verbose printing to console
        """
        # TODO: Add remote logging capabilities
        self.logFile      = logFile
        self.verbose      = cVerbose
        self.nHeaderStars = nHeaderStars
        self.startTime    = None
        self.endTime      = None

        # Console Logging
        cHandler = logging.StreamHandler()
        if cVerbose:
            cLevel = logging.DEBUG
        else:
            cLevel = logging.INFO
        cHandler.setLevel(cLevel)
        cFormatter = logging.Formatter(fmt="%(message)s")
        cHandler.setFormatter(cFormatter)

        # File Logging
        fHandler = logging.FileHandler(logFile)
        fHandler.setLevel(logging.DEBUG)
        fFormatter = logging.Formatter(fmt="%(asctime)s - %(levelname)s %(message)s",
                                       datefmt="%a, %d %b %Y %H:%M:%S")
        fHandler.setFormatter(fFormatter)

        # Add Handlers to root logger
        logging.basicConfig(level=logging.DEBUG, handlers=[cHandler, fHandler])
        self.logHeader()

    def logHeader(self):
        """
        Log Header
        """
        self.startTime = datetime.now()
        logger.debug("*" * self.nHeaderStars)
        logger.debug("Command Start: {}".format(" ".join(sys.argv)))
        logger.info("Logfile: {}".format(self.logFile))
        logger.debug("*" * self.nHeaderStars)

    def logFooter(self):
        """
        Log Footer
        """
        self.endTime = datetime.now()
        logger.debug("*" * self.nHeaderStars)
        logger.debug("Command End: {}".format(" ".join(sys.argv)))
        if self.startTime:
            logger.debug("Start Time: {}".format(self.startTime))
        if self.endTime:
            logger.debug("End Time: {}".format(self.endTime))
        if self.startTime and self.endTime:
            logger.debug("Elapsed Time: {}".format(self.endTime - self.startTime))
        logger.debug("*" * self.nHeaderStars)
