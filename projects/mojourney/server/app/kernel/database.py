#!/usr/bin/env python3
"""
 NAME
  database.py

 DESCRIPTION
  Database Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# --------------------------- DATABASE CLASS -------------------------------- #
class Database():
    """
    Database class
    Encapsulate all the details regarding database access
    Args
      engine:
        database engine
      echo:
        If True, echo all Database commands
    """

    def __init__(self, engine, echo=False):
        self.engine = create_engine(engine, echo=echo)
        self.session = scoped_session(sessionmaker(autocommit=False,
                                                   autoflush=False, bind=self.engine))
        self.base = declarative_base()
        self.base.query = self.session.query_property()

    def initApp(self, app=None):
        """
        Initialize Flask Application
        Args
          app: Flask application
        """
        @app.teardown_appcontext
        def shutdownSession(exception=None):  # pylint: disable=W0612,W0613
            """
            Remove the session after ever web request finishes
            """
            self.session.remove()

    def create(self):
        """
        Create all database tables
        """
        self.base.metadata.create_all(self.engine)

    def destroy(self):
        """
        Destroy all database tables
        """
        self.base.metadata.drop_all(self.engine)

    def populate(self, nUsers=100):  # pylint: disable=R0201
        """
        Populate database
        Args
          nUsers: number of users to add to database
        """
        from mojourney.server.app.models.user import User
        User.addFakeUsers(nUsers)
        logger.info("All users: {}".format(User.getAll()))
