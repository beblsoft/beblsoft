Dear {{ user.name }},

Please confirm your account by clicking on this link: {{ link }}.

Thanks!
Mojourney