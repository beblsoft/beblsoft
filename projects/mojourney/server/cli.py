#!/usr/bin/env python3
"""
 NAME:
  cli.py

 DESCRIPTION
  Mojourney Server Command Line Interface
"""


# ----------------------- IMPORTS ------------------------------------------- #
import sys
import traceback
import unittest
import logging
import code
import click
from werkzeug.local import LocalProxy
import mojourney.server.app.app as app #pylint: disable=C0414
from mojourney.server.config.developmentConfig import DevelopmentConfig
from mojourney.server.config.productionConfig import ProductionConfig


# ----------------------- GLOBALS ------------------------------------------- #
class GlobalContext():
    """Global Context object for CLI"""

    def __init__(self):
        self.cfgMap    = {
            'development': DevelopmentConfig,
            'production': ProductionConfig,
        }
        self.cfg       = None
        self.verbose   = False
        self.logfile   = None
        self.db        = LocalProxy(lambda: getattr(app, 'db', None))
        self.fApp      = LocalProxy(lambda: getattr(app, 'fApp', None))
        self.lp        = LocalProxy(lambda: getattr(app, 'lp', None))
        self.log       = LocalProxy(lambda: getattr(app, 'log', None))

gc            = GlobalContext()
logger        = logging.getLogger(__file__)
CLICK_CONTEXT = dict(help_option_names=["-h", "--help"])


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=CLICK_CONTEXT, options_metavar="[options]")
@click.option("-c", "--cfg", default="development",
              type=click.Choice(gc.cfgMap.keys()),
              help="Configuration Type. Default development")
@click.option("-v", "--verbose", is_flag=True, default=False, help="Enable verbose printing")
@click.option("--logfile", default="/tmp/mjserver.log", type=click.Path(), help="Specify log file")
@click.option("--login/--nologin", default=True, help="Login to LastPass")
def cli(cfg, logfile, verbose, login):
    """Mojourney Server Command Line Interface"""
    gc.cfg = gc.cfgMap[cfg]()
    app.factory(gc.cfg, logFile=logfile, cVerbose=verbose, login=login)


# ----------------------- RUNSERVER ----------------------------------------- #
@cli.command()
@click.option("--host", default="localhost", help="Host Name")
@click.option("-p", "--port", default=5000, help="Port")
@click.option("--debug", is_flag=True, default=False, help="Enable the Werkzeug Debugger")
@click.option("--threaded", is_flag=True, default=False, help="Multiple threads")
@click.option("--processes", default=1, help="Number of processes")
@click.option("--passthrough_errors", default=False, help="Set this to True to disable the error catching")
@click.option("--noreload", is_flag=True, default=False, help="Reload server on module change?")
def runserver(host, port, threaded, processes, debug, passthrough_errors, noreload):
    """Run the server"""

    # run(host=None, port=None, debug=None, **options)
    # **options are forwarded to the following function:
    # werkzeug.serving.run_simple(hostname, port, application, use_reloader=False,
    #                             use_debugger=False, use_evalex=True, extra_files=None,
    #                             reloader_interval=1, reloader_type='auto', threaded=False,
    #                             processes=1, request_handler=None, static_files=None,
    #                             passthrough_errors=False, ssl_context=None)
    gc.fApp.run(host=host, port=port, debug=debug, processes=processes,
                threaded=threaded, passthrough_errors=passthrough_errors,
                use_reloader=(not noreload))


# ----------------------- DATABASE ------------------------------------------ #
@cli.group()
def db():
    """Database Control"""
    pass


@db.command()
def init():
    """Create Database Tables"""
    gc.db.create()


@db.command()
def destroy():
    """Destroy Database Tables"""
    gc.db.destroy()


@db.command()
@click.option("--nusers", default=10, help="Number of users to add to database")
def populate(nusers):
    """Populate Database"""
    gc.db.populate(nUsers=nusers)


# ------------------------ TEST --------------------------------------------- #
@cli.command()
def test():
    """Discover and run application unit tests"""

    # Load specific tests
    # tests = unittest.TestLoader().loadTestsFromNames(
    #     ["tests.routes.v_1_0.test_test"])

    # Load all tests
    tests = unittest.TestLoader().discover(start_dir='app', pattern="*_test.py")

    unittest.TextTestRunner(verbosity=2).run(tests)


# ----------------------- LASTPASS ------------------------------------------ #
@cli.group()
def lp():
    """LastPass Control"""
    pass


@lp.command()
def dump():
    """Dump LastPass Passwords"""
    logger.info(gc.lp)


@lp.command()
def logout():
    """Logout of LastPass"""
    gc.lp.logout()


# ----------------------- SHELL --------------------------------------------- #
@cli.command()
def shell():
    """Create interactive python shell"""
    ctx = dict(gc=gc)
    code.interact(banner="", local=ctx)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == '__main__':
    try:
        cli()  # pylint: disable=E1120
    except Exception as e: #pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.log:
            gc.log.logFooter()
