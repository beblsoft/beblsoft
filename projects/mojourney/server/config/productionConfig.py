#!/usr/bin/env python3
"""
 NAME:
  productionConfig.py

 DESCRIPTION
   Server Production Configuration
"""


# --------------------------- IMPORTS --------------------------------------- #
from mojourney.server.config.baseConfig import BaseConfig



# --------------------------- CLASSES --------------------------------------- #
class ProductionConfig(BaseConfig):
    """
    Production configuration
    """
    PRODUCTION        = True
    DB_ENGINE         = "mysql://mojourney:@localhost/mojourneyProduction"
