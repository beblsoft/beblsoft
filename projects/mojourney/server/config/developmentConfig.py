#!/usr/bin/env python3
"""
 NAME:
  developmentConfig.py

 DESCRIPTION
   Server Development Configuration
"""


# --------------------------- IMPORTS --------------------------------------- #
from mojourney.server.config.baseConfig import BaseConfig



# --------------------------- CLASSES --------------------------------------- #
class DevelopmentConfig(BaseConfig):
    """
    Development configuration
    """
    DEBUG             = True
    DB_ENGINE         = "mysql://mojourney:@localhost/mojourneyDevelopment"
