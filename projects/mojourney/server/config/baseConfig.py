#!/usr/bin/env python3
"""
 NAME:
  baseConfig.py

 DESCRIPTION
   Server Base Configuration
"""


# ---------------------------- IMPORTS -------------------------------------- #
import os
import datetime


# --------------------------- CLASSES --------------------------------------- #
class BaseConfig():
    """
    Base configuration class
    """
    DEBUG             = False
    TESTING           = False
    PRODUCTION        = False

    DB_ENGINE         = None
    DB_ECHO           = False

    MAIL_FROM         = "beblsoftmanager@gmail.com"
    MAIL_SERVER       = "smtp.googlemail.com"
    MAIL_PORT         = 587

    LASTPASS_MASTER   = "beblsoftmanager@gmail.com"
    LASTPASS_ACCOUNTS = ["test1", "test2", "google.com"]
    LASTPASS_PASSWORD = os.environ["LASTPASS_PASSWORD"]

    JWT_SECRET        = "Beblsoft472801"  # TODO: Move JWT_SECRET to LastPass
    JWT_ALGORITHM     = "HS256"
    JWT_EXP_DELTA     = datetime.timedelta(days=1)
    JWT_NBF_DELTA     = datetime.timedelta(seconds=10)
