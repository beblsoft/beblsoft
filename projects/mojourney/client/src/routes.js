
import LoginView from './components/not-logged-in/Login.vue'
import NotLoggedInHomeView from './components/not-logged-in/Home.vue'
import SignupView from './components/not-logged-in/Signup.vue'

import DashView from './components/Dash.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import HomeView from './components/views/Home.vue'
import DailyView from './components/views/Daily.vue'
import BrowseView from './components/views/Browse.vue'
import InstructorView from './components/views/Instructors.vue'
import ExercisesView from './components/views/Exercises.vue'
import WorkoutsView from './components/views/Workouts.vue'
import PlansView from './components/views/Plans.vue'

// Routes, use auth = true to prevent access to any path!!!
const routes = [
  {
    path: '/not-logged-in/login',
    component: LoginView
  },
  {
    path: '/not-logged-in/home',
    component: NotLoggedInHomeView
  },
  {
    path: '/not-logged-in/signup',
    component: SignupView
  },
  {
    path: '/',
    component: DashView,
    children: [{
      path: 'home',
      alias: '',
      component: HomeView,
      name: 'Home',
      meta: { description: 'Overview of MoJourney', auth: true }
    },
    {
      path: 'daily',
      component: DailyView,
      name: 'Daily',
      meta: { description: 'Daily Programming for MoJourney', auth: true }
    },
    {
      path: 'browse',
      component: BrowseView,
      name: 'Browse',
      meta: { description: 'Browse Mojourney', auth: true }
    },
    {
      path: 'instructor',
      component: InstructorView,
      name: 'Instructors',
      meta: { description: 'View Instructors at MoJourney', auth: true }
    },
    {
      path: 'exercises',
      component: ExercisesView,
      name: 'Exercises',
      meta: { description: 'Mojourney Exercises', auth: true }
    },
    {
      path: 'workouts',
      component: WorkoutsView,
      name: 'Workouts',
      meta: { description: 'Mojourney Workouts', auth: true }
    },
    {
      path: 'plans',
      component: PlansView,
      name: 'Plans',
      meta: { description: 'Mojourney Plans', auth: true }
    }]
  },
  {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
