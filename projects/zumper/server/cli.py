#!/usr/bin/env python
"""
 NAME:
  cli.py

DESCRIPTION
 Zumper Server CLI
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click
from zumper.server.manager import ServerManager
from zumper.server.config import ServerConfig


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)
cfg    = ServerConfig()
mgr    = ServerManager(cfg=cfg)


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]), options_metavar="[options]")
def cli():
    """
    Zumper Server Command Line Interface
    """
    pass


# ----------------------- DATABASE COMMANDS --------------------------------- #
@cli.command()
def database_create():
    """
    Create Database
    """
    mgr.db.create()
    mgr.db.createTables()


@cli.command()
def database_delete():
    """
    Delete Database
    """
    mgr.db.deleteTables()
    mgr.db.delete()


# ----------------------- WEB API COMMANDS ---------------------------------- #
@cli.command()
def webapi_start():
    """
    Start Web API
    """
    mgr.fApp.run(
        host               = cfg.FLASK_HOST,
        port               = cfg.FLASK_PORT,
        debug              = cfg.FLASK_DEBUG,
        threaded           = cfg.FLASK_THREADED,
        use_reloader       = cfg.FLASK_USE_RELOADER,
        use_debugger       = cfg.FLASK_USE_DEBUGGER)


# ----------------------- PARSING COMMANDS ---------------------------------- #
@cli.command()
def parsing_start():
    """
    Start Parsing
    """
    mgr.parser.start()


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    cli(obj={})  # pylint: disable=E1120,E1123
