#!/usr/bin/env python3
"""
NAME:
 manager.py

DESCRIPTION
 Server Manager Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from flask import Flask
from zumper.server.db import Database
from zumper.server.parser.manager import ParsingManager
from zumper.server.webAPI.todo import bp as todoBP
from zumper.server.webAPI.listing import bp as listingBP
from zumper.server.webAPI.image import bp as imageBP
from base.mysql.python.bServer import BeblsoftMySQLServer



# ------------------------ SERVER MANAGER OBJECT ---------------------------- #
class ServerManager():
    """
    Server Manager Class
    """

    def __init__(self, cfg):  # pylint: disable=R0915
        """
        Initialize Object
        Args
          cfg:
            Server Configuration Object
        """
        self.cfg               = cfg

        # Database
        self.dbServer          = BeblsoftMySQLServer(
            domainNameFunc          = lambda : self.cfg.DB_SERVER,
            user                    = self.cfg.DB_USER,
            password                = self.cfg.DB_PASSWORD)
        self.db                = Database(
            bServer                 = self.dbServer,
            name                    = self.cfg.DB_NAME,
            echo                    = self.cfg.DB_ECHO)

        # Flask REST API
        self.fApp              = Flask(__name__)
        self.fApp.mgr          = self
        self.fApp.register_blueprint(todoBP, url_prefix="/todo")
        self.fApp.register_blueprint(listingBP, url_prefix="/listing")
        self.fApp.register_blueprint(imageBP, url_prefix="/image")

        # Parser
        self.parser            = ParsingManager(mgr=self)
