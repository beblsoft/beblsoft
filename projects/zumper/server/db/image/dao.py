#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Image Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from zumper.server.db.image.model import ImageModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- IMAGE DAO CLASS ----------------------------------- #
class ImageDAO():
    """
    Image Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(session, listingModel, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        imageModel = ImageModel(listingModel=listingModel)
        for k, v in kwargs.items():
            setattr(imageModel, k, v)
        session.add(imageModel)
        return imageModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(session, imageModel):  # pylint: disable=W0221
        """
        Delete object
        """
        session.delete(imageModel)

    @staticmethod
    @logFunc()
    def deleteByZumperID(session, zumperID):  # pylint: disable=W0221
        """
        Delete object
        """
        session.query(ImageModel).filter(ImageModel.zumperID == zumperID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(session):
        """
        Delete all objects
        """
        session.query(ImageModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(session, imageModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(imageModel, k, v)
        session.add(imageModel)
        return imageModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getByZumperID(session, zumperID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        imageModel = session.query(ImageModel)\
            .filter(ImageModel.zumperID == zumperID)\
            .one_or_none()
        if not imageModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return imageModel

    @staticmethod
    @logFunc()
    def getAll(session, listingID=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = session.query(ImageModel)

        #Filters
        if listingID is not None:
            q = q.filter(ImageModel.listingZumperID == listingID)

        return q.count() if count else q.all()
