#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Image Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import Column, BigInteger, Text, ForeignKey
from sqlalchemy.orm import relationship
from zumper.server.db import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- IMAGE MODEL CLASS --------------------------------- #
class ImageModel(base):
    """
    Image Table
    """
    __tablename__     = "Image"
    zumperID          = Column(BigInteger, primary_key=True)  # Zumper ID
    url               = Column(Text)                          # Ex. s3.amazonaws.com/...

    # Parent Listing
    # 1 Listing : N Images
    listingZumperID   = Column(BigInteger,
                               ForeignKey("Listing.zumperID", onupdate="CASCADE", ondelete="CASCADE"),
                               nullable=False)
    listingModel      = relationship("ListingModel", back_populates="imageModelList")

    def __repr__(self):
        return "[{} zumperID={}]".format(self.__class__.__name__, self.zumperID)
