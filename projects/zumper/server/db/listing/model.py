#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Listing Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import Column, BigInteger, Text, Boolean
from sqlalchemy.orm import relationship
from zumper.server.db import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- LISTING MODEL CLASS ------------------------------- #
class ListingModel(base):
    """
    Listing Table

    Describes an Apartment Listing
    """
    __tablename__     = "Listing"
    zumperID          = Column(BigInteger, primary_key=True)  # Zumper ID
    address           = Column(Text)                          # 1 Westwood Park Circle, Attleboro MA 02703
    priceUSCPerMonth  = Column(BigInteger)                    # US Cents Per Month Ex 100000 = $1000.00/month
    hasLaundry        = Column(Boolean, default=False)        # True if apartment has laundry
    squareFootage     = Column(BigInteger)                    # Ex. 200

    # Child Images
    # 1 Listing : N Images
    imageModelList    = relationship("ImageModel", back_populates="listingModel",
                                      passive_deletes=True)

    def __repr__(self):
        return "[{} zumperID={}]".format(self.__class__.__name__, self.zumperID)
