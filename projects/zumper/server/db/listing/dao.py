#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Listing Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from zumper.server.db.listing.model import ListingModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- LISTING DAO CLASS --------------------------------- #
class ListingDAO():
    """
    Listing Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(session, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        listingModel = ListingModel()
        for k, v in kwargs.items():
            setattr(listingModel, k, v)
        session.add(listingModel)
        return listingModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(session, listingModel):  # pylint: disable=W0221
        """
        Delete object
        """
        session.delete(listingModel)

    @staticmethod
    @logFunc()
    def deleteByZumperID(session, zumperID):  # pylint: disable=W0221
        """
        Delete object
        """
        session.query(ListingModel).filter(ListingModel.zumperID == zumperID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(session):
        """
        Delete all objects
        """
        session.query(ListingModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(session, listingModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(listingModel, k, v)
        session.add(listingModel)
        return listingModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getByZumperID(session, zumperID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        listingModel = session.query(ListingModel)\
            .filter(ListingModel.zumperID == zumperID)\
            .one_or_none()
        if not listingModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return listingModel

    @staticmethod
    @logFunc()
    def getAll(session, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = session.query(ListingModel)

        #Filters
        return q.count() if count else q.all()
