#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Todo Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import Column, BigInteger, Text, Boolean
from zumper.server.db import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- TODO MODEL CLASS ---------------------------------- #
class TodoModel(base):
    """
    Todo Table
    """
    __tablename__ = "Todo"

    # IDs
    zumperID      = Column(BigInteger, primary_key=True)  # Zumper ID
    message       = Column(Text)                          # Message. Ex. "Take out the trash"
    important     = Column(Boolean, default=False)        # True if TODO is important
    complete      = Column(Boolean, default=False)        # True if TODO is complete

    def __repr__(self):
        return "[{} zumperID={}]".format(self.__class__.__name__, self.zumperID)
