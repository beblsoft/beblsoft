#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Todo Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from zumper.server.db.todo.model import TodoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- TODO DAO CLASS ------------------------------------ #
class TodoDAO():
    """
    Todo Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(session, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        todoModel = TodoModel()
        for k, v in kwargs.items():
            setattr(todoModel, k, v)
        session.add(todoModel)
        return todoModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(session, todoModel):  # pylint: disable=W0221
        """
        Delete object
        """
        session.delete(todoModel)

    @staticmethod
    @logFunc()
    def deleteByZumperID(session, zumperID):  # pylint: disable=W0221
        """
        Delete object
        """
        session.query(TodoModel).filter(TodoModel.zumperID == zumperID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(session):
        """
        Delete all objects
        """
        session.query(TodoModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(session, todoModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(todoModel, k, v)
        session.add(todoModel)
        return todoModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getByZumperID(session, zumperID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        todoModel = session.query(TodoModel)\
            .filter(TodoModel.zumperID == zumperID)\
            .one_or_none()
        if not todoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return todoModel

    @staticmethod
    @logFunc()
    def getAll(session, iLikeMessage=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = session.query(TodoModel)

        #Filters
        if iLikeMessage:
            q = q.filter(TodoModel.message.ilike("%{}%".format(iLikeMessage)))

        return q.count() if count else q.all()
