#!/usr/bin/env python3
"""
 NAME
  __init__.py

 DESCRIPTION
  Database Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from pathlib import Path
import logging
import pprint
import uuid
from datetime import datetime
from sqlalchemy.sql.expression import func
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
base   = declarative_base()


# ----------------------------- TABLE, DAO IMPORTS -------------------------- #
from zumper.server.db.todo.model import TodoModel #pylint: disable=C0413
from zumper.server.db.listing.model import ListingModel #pylint: disable=C0413
from zumper.server.db.image.model import ImageModel #pylint: disable=C0413


# ----------------------------- DATABASE CLASS ------------------------------ #
class Database(BeblsoftMySQLDatabase):
    """
    Database Class
    """

    # --------------------- CREATE/DELETE TABLES ---------------------------- #
    @logFunc()
    def createTables(self, checkFirst=True):
        """
        Create all database tables
        Args
          checkFirst:
            If True, don't issue CREATEs for tables already present
        """
        base.metadata.create_all(self.engine, checkfirst=checkFirst)

    @logFunc()
    def deleteTables(self, checkFirst=True):
        """
        Destroy all database tables
        Args
          checkFirst:
            If True, don't issue DELETE's for tables not present
        """
        base.metadata.drop_all(self.engine, checkfirst=checkFirst)
