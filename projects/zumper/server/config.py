#!/usr/bin/env python3
"""
NAME:
 config.py

DESCRIPTION
 Server Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import getpass


# ------------------------ SERVER MANAGER OBJECT ---------------------------- #
class ServerConfig():
    """
    Server Config Class
    """

    def __init__(self):  # pylint: disable=R0915
        """
        Initialize Object
        """

        # Database
        self.DB_SERVER              = "localhost"
        self.DB_USER                = getpass.getuser()
        self.DB_PASSWORD            = ""
        self.DB_NAME                = "ZumperDB"
        self.DB_ECHO                = False

        # Flask
        self.FLASK_HOST             = "localhost"
        self.FLASK_PORT             = 5123
        self.FLASK_DEBUG            = True
        self.FLASK_USE_RELOADER     = True
        self.FLASK_USE_DEBUGGER     = True
        self.FLASK_THREADED         = True
