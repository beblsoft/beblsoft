#!/usr/bin/env python3
"""
NAME:
 manager.py

DESCRIPTION
 Parsing Manager Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import boto
import requests
from boto.s3.connection import S3Connection
from xml.sax.xmlreader import XMLReader, IncrementalParser
import xml.etree.ElementTree as ET
from zumper.server.db.listing.dao import ListingDAO


# ------------------------ PARSING MANAGER OBJECT --------------------------- #
class ParsingManager():
    """
    Parsing Manager Class
    """

    def __init__(self, mgr):
        """
        Initialize Object
        """
        self.mgr = mgr

    def start(self):
        """
        Start Parsing
        """
        filePath = "/home/jbensson/Downloads/958379.xml"
        tree     = ET.parse(filePath)
        root     = tree.getroot()
        db       = self.mgr.db

        # Iterate through all children in tree
        # Find listings
        # Add listings to database
        for child in root:
            isListing = child.find("propertytype") is not None
            if isListing:
                country = child.find("country").text
                address = country

                with db.sessionScope() as session:
                    ListingDAO.create(session=session, address=address)
