#!/usr/bin/env python3
"""
 NAME:
  todo.py

 DESCRIPTION
  Todo Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Blueprint, jsonify, current_app, request, abort, make_response
from zumper.server.db.todo.dao import TodoDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
bp     = Blueprint("todo", __name__)


# ------------------------ MARSHALING --------------------------------------- #
def todoModelToDict(todoModel):
    """
    Convert TodoModel to Dict
    """
    keys = ["zumperID", "message", "important", "complete"]
    return {k: getattr(todoModel, k) for k in keys}


def todoModelToJSON(todoModel): #pylint: disable=C0116
    return jsonify(todoModelToDict(todoModel=todoModel))

def todoModelListJSON(todoModelList): #pylint: disable=C011,C0116
    return jsonify({"data" : [todoModelToDict(todoModel=todoModel) for todoModel in todoModelList]})


# ------------------------ ROUTES ------------------------------------------- #
@bp.route("", methods=["POST"])
def create():
    """
    Create object
    """
    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload   = request.get_json()

    # Parse Arguments
    message   = payload.get("message", None)
    if message is None:
        abort(make_response(jsonify(message="Must specify TODO message"), 400))
    important = payload.get("important", False)
    complete  = payload.get("complete", False)

    # Create Object
    db        = current_app.mgr.db
    todoModel = None
    with db.sessionScope() as session:
        with db.transactionScope(session=session):
            todoModel = TodoDAO.create(session=session, message=message, important=important, complete=complete)
        return todoModelToJSON(todoModel=todoModel)


@bp.route("/<int:zumperID>", methods=["DELETE"])
def delete(zumperID):
    """
    Delete object
    """
    db = current_app.mgr.db
    with db.sessionScope() as session:
        TodoDAO.deleteByZumperID(session=session, zumperID=zumperID)
    return ('', 200)


@bp.route("/<int:zumperID>", methods=["PUT"])
def update(zumperID):
    """
    Update object
    """
    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload = request.get_json()

    # Update Object
    db        = current_app.mgr.db
    todoModel = None
    with db.sessionScope() as session:
        todoModel = TodoDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if todoModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        with db.transactionScope(session=session):
            todoModel = TodoDAO.update(session=session, todoModel=todoModel, **payload)
        return todoModelToJSON(todoModel=todoModel)


@bp.route("", methods=["GET"])
def getList():
    """
    List objects
    """
    db            = current_app.mgr.db
    todoModelList = []
    with db.sessionScope() as session:
        todoModelList = TodoDAO.getAll(session=session)
        return todoModelListJSON(todoModelList=todoModelList)


@bp.route("/<int:zumperID>", methods=["GET"])
def getByID(zumperID):
    """
    Get object
    """
    db        = current_app.mgr.db
    todoModel = None
    with db.sessionScope() as session:
        todoModel = TodoDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if todoModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        return todoModelToJSON(todoModel=todoModel)
