#!/usr/bin/env python3
"""
 NAME:
  listing.py

 DESCRIPTION
  Listing Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Blueprint, jsonify, current_app, request, abort, make_response
from zumper.server.db.listing.dao import ListingDAO
from zumper.server.db.image.dao import ImageDAO
from zumper.server.webAPI.image import imageModelListJSON


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
bp     = Blueprint("listing", __name__)


# ------------------------ MARSHALING --------------------------------------- #
def listingModelToDict(listingModel):
    """
    Convert ListingModel to Dict
    """
    keys = ["zumperID", "address", "priceUSCPerMonth",  "hasLaundry"]
    return {k: getattr(listingModel, k) for k in keys}


def listingModelToJSON(listingModel): #pylint: disable=C0116
    return jsonify(listingModelToDict(listingModel=listingModel))

def listingModelListJSON(listingModelList): #pylint: disable=C011,C0116
    return jsonify({"data" : [listingModelToDict(listingModel=listingModel) for listingModel in listingModelList]})


# ------------------------ ROUTES ------------------------------------------- #
@bp.route("", methods=["POST"])
def create():
    """
    Create object
    """
    db = current_app.mgr.db

    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload   = request.get_json()

    # Create
    with db.sessionScope() as session:
        listingModel = ListingDAO.create(session=session, **payload)
        return listingModelToJSON(listingModel=listingModel)


@bp.route("/<int:zumperID>", methods=["DELETE"])
def delete(zumperID):
    """
    Delete object
    """
    db = current_app.mgr.db
    with db.sessionScope() as session:
        ListingDAO.deleteByZumperID(session=session, zumperID=zumperID)
    return ('', 200)


@bp.route("/<int:zumperID>", methods=["PUT"])
def update(zumperID):
    """
    Update object
    """
    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload = request.get_json()

    # Update Object
    db        = current_app.mgr.db
    listingModel = None
    with db.sessionScope() as session:
        listingModel = ListingDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if listingModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        listingModel = ListingDAO.update(session=session, listingModel=listingModel, **payload)
        return listingModelToJSON(listingModel=listingModel)


@bp.route("", methods=["GET"])
def getList():
    """
    List objects
    """
    db = current_app.mgr.db
    listingModelList = []
    with db.sessionScope() as session:
        listingModelList = ListingDAO.getAll(session=session)
        return listingModelListJSON(listingModelList=listingModelList)


@bp.route("/<int:zumperID>", methods=["GET"])
def getByID(zumperID):
    """
    Get object
    """
    db        = current_app.mgr.db
    listingModel = None
    with db.sessionScope() as session:
        listingModel = ListingDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if listingModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        return listingModelToJSON(listingModel=listingModel)

@bp.route("/<int:zumperID>/image", methods=["GET"])
def getImages(zumperID):
    """
    Get object
    """
    db = current_app.mgr.db
    listingModel = None
    with db.sessionScope() as session:
        listingModel = ListingDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if listingModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        imageModelList = ImageDAO.getAll(session=session, listingID=listingModel.zumperID)
        return imageModelListJSON(imageModelList=imageModelList)
