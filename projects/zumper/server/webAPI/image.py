#!/usr/bin/env python3
"""
 NAME:
  image.py

 DESCRIPTION
  Image Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Blueprint, jsonify, current_app, request, abort, make_response
from zumper.server.db.listing.dao import ListingDAO
from zumper.server.db.image.dao import ImageDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
bp     = Blueprint("image", __name__)


# ------------------------ MARSHALING --------------------------------------- #
def imageModelToDict(imageModel):
    """
    Convert ImageModel to Dict
    """
    keys = ["zumperID", "url"]
    return {k: getattr(imageModel, k) for k in keys}


def imageModelToJSON(imageModel): #pylint: disable=C0116
    return jsonify(imageModelToDict(imageModel=imageModel))

def imageModelListJSON(imageModelList): #pylint: disable=C011,C0116
    return jsonify({"data" : [imageModelToDict(imageModel=imageModel) for imageModel in imageModelList]})


# ------------------------ ROUTES ------------------------------------------- #
@bp.route("", methods=["POST"])
def create():
    """
    Create object
    """
    db = current_app.mgr.db

    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload   = request.get_json()

    # Parse Arguments
    listingID = payload.get("listingID", None)
    if listingID is None:
        abort(make_response(jsonify(message="Must specify Listing ID"), 400))
    url = payload.get("url", None)
    if url is None:
        abort(make_response(jsonify(message="Must specify url"), 400))

    # Find underlying listing and create image
    with db.sessionScope() as session:
        listingModel = ListingDAO.getByZumperID(session=session, zumperID=listingID, raiseOnNone=False)
        if not listingModel:
            abort(make_response(jsonify(message="Must specify valid listing ID"), 400))
        with db.transactionScope(session=session):
            imageModel = ImageDAO.create(session=session, listingModel=listingModel, url=url)

        return imageModelToJSON(imageModel=imageModel)


@bp.route("/<int:zumperID>", methods=["DELETE"])
def delete(zumperID):
    """
    Delete object
    """
    db = current_app.mgr.db
    with db.sessionScope() as session:
        ImageDAO.deleteByZumperID(session=session, zumperID=zumperID)
    return ('', 200)


@bp.route("/<int:zumperID>", methods=["PUT"])
def update(zumperID):
    """
    Update object
    """
    # Retrieve JSON
    if not request.is_json:
        abort(make_response(jsonify(message="Must specify JSON"), 400))
    payload = request.get_json()

    # Update Object
    db        = current_app.mgr.db
    imageModel = None
    with db.sessionScope() as session:
        imageModel = ImageDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if imageModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        with db.transactionScope(session=session):
            imageModel = ImageDAO.update(session=session, imageModel=imageModel, **payload)
        return imageModelToJSON(imageModel=imageModel)


@bp.route("", methods=["GET"])
def getList():
    """
    List objects
    """
    db = current_app.mgr.db
    imageModelList = []
    with db.sessionScope() as session:
        imageModelList = ImageDAO.getAll(session=session)
        return imageModelListJSON(imageModelList=imageModelList)


@bp.route("/<int:zumperID>", methods=["GET"])
def getByID(zumperID):
    """
    Get object
    """
    db        = current_app.mgr.db
    imageModel = None
    with db.sessionScope() as session:
        imageModel = ImageDAO.getByZumperID(session=session, zumperID=zumperID, raiseOnNone=False)
        if imageModel is None:
            abort(make_response(jsonify(message="Object does not exist"), 404))
        return imageModelToJSON(imageModel=imageModel)
