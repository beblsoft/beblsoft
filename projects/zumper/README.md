# Zumper Interview Application

This is a demo flask REST application for the Zumper interview.

## Server

### To Run

```bash
# Help
$ZUMPER/bin/server --help

# Create Database
$ZUMPER/bin/server database-create

# Start Application
$ZUMPER/bin/server webapi-start

# Delete Database
$ZUMPER/bin/server database-delete
```

### TODO CRUD

```bash
# Create TODO
curl --request POST \
     --header "Content-Type: application/json" \
     --data   '{"message": "Hello World!", "important": true, "complete": false}' \
      http://localhost:5123/todo

# Update
curl --request PUT \
     --header "Content-Type: application/json" \
     --data   '{"message": "Updated Message", "important": false, "complete": true}' \
      http://localhost:5123/todo/1

# List
curl --request GET http://localhost:5123/todo

# Get
curl --request GET http://localhost:5123/todo/1

# Delete
curl --request DELETE http://localhost:5123/todo/1

```

### Listing CRUD

```bash
# Create TODO
curl --request POST \
     --header "Content-Type: application/json" \
     --data   '{"address": "1 Westwood Park Circle Attleboro MA, 02703", "priceUSCPerMonth": 100000, "hasLaundry": true, "squareFootage": 100}' \
      http://localhost:5123/listing

# Update
curl --request PUT \
     --header "Content-Type: application/json" \
     --data   '{"priceUSCPerMonth": 10022}' \
      http://localhost:5123/listing/1

# List
curl --request GET http://localhost:5123/listing

# Get
curl --request GET http://localhost:5123/listing/1

# Get Listing Images
curl --request GET http://localhost:5123/listing/2/image

# Delete
curl --request DELETE http://localhost:5123/listing/1

```

### Image CRUD

```bash
# Create TODO
curl --request POST \
     --header "Content-Type: application/json" \
     --data   '{"listingID": 2, "url": "s3.amazonaws.com/helloworld"}' \
      http://localhost:5123/image

# Update
curl --request PUT \
     --header "Content-Type: application/json" \
     --data   '{"url": "s3.amazonaws.com/update"}' \
      http://localhost:5123/image/1

# List
curl --request GET http://localhost:5123/image

# Get
curl --request GET http://localhost:5123/image/1

# Delete
curl --request DELETE http://localhost:5123/image/1

```
