/**
 * @file Smeckn API Index
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let API = require('./src/api.js');
let Errors = require('./src/error.js');


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = {
  API,
  Errors
};
