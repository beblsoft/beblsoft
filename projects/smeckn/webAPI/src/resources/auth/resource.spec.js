/**
 * @file Account Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Auth Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = '123092873450982345';
  let newPassword = 'asdfkljaysdfamksfdh';
  let reCAPTCHAToken = process.env.RECAPTCHA_PASSTHROUGH_TOKEN;

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeDeleteInputData: { accountEmail: email }
    });
  });

  /* -------------------- CREATE ACCOUNT ----------------------------------- */
  it('Should start create account process', async function () {
    await api.auth.createAccount({ data: { email, password, reCAPTCHAToken } });
  });

  /* -------------------- CONFIRM ACCOUNT ---------------------------------- */
  it('Should confirm account', async function () {
    // First get createAccount token
    let tokenData = await api.account.createTokens({ data: { email, password } });
    let createAccountToken = tokenData.createAccountToken;
    assert.typeOf(tokenData.authToken, 'string');
    assert.typeOf(tokenData.accountToken, 'string');
    assert.typeOf(tokenData.createAccountToken, 'string');
    assert.typeOf(tokenData.forgotPasswordToken, 'string');
    await api.auth.confirmAccount({ data: { createAccountToken } });
  });

  /* -------------------- FORGOT PASSWORD ---------------------------------- */
  it('Should start forgot password process', async function () {
    await api.auth.forgotPassword({ data: { email: email, reCAPTCHAToken } });
  });

  /* -------------------- RESET PASSWORD ----------------------------------- */
  it('Should reset password', async function () {
    // First get forgotPasswordToken token
    let tokenData = await api.account.createTokens({ data: { email, password } });
    let forgotPasswordToken = tokenData.forgotPasswordToken;
    await api.auth.resetPassword({ data: { forgotPasswordToken, password: newPassword } });
  });

  /* -------------------- LOGIN -------------------------------------------- */
  it('Should login', async function () {
    let loginData = await api.auth.login({
      data: { email, password: newPassword, reCAPTCHAToken }
    });
    let account = loginData.account;
    assert.isAtLeast(account.id, 0);
    assert.strictEqual(account.email, email);
    assert.strictEqual(account.type, 'TEST');
    assert.strictEqual(account.active, true);
    assert.typeOf(account.creationDate, 'string');
  });

  /* -------------------- LOGOUT ------------------------------------------- */
  it('Should logout', async function () {
    let logoutData = await api.auth.logout();
    assert.isEmpty(logoutData);
  });

  /* -------------------- GET STATUS --------------------------------------- */
  it('Should get status', async function () {
    // Logged In
    let tokenData = await api.account.createTokens({ data: { email, password } });
    let statusData = await api.auth.getStatus({ accountToken: tokenData.accountToken });
    assert.strictEqual(statusData.status, 'LOGGED_IN');
    assert.strictEqual(statusData.account.email, email);

    // Logged Out
    statusData = await api.auth.getStatus({ accountToken: '' });
    assert.strictEqual(statusData.status, 'LOGGED_OUT');
    assert.strictEqual(statusData.account, null);
  });

  /* -------------------- DELETE ACCOUNT ----------------------------------- */
  it('Should delete account', async function () {
    let accountList = await api.account.list({ queryParams: { email } });
    await api.account.delete({ urlParams: { id: accountList[0].id } });
  });
});
