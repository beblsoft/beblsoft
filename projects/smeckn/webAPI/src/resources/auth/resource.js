/**
 * @file Auth Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Auth Class
 * @extends {Resource}
 */
class AuthResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/auth' });

    // CREATE ACCOUNT
    this.registerMethod({
      name: 'createAccount',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/createAccount' }
    });

    // CONFIRM ACCOUNT
    this.registerMethod({
      name: 'confirmAccount',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/confirmAccount' }
    });

    // FORGOT PASSWORD
    this.registerMethod({
      name: 'forgotPassword',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/forgotPassword' }
    });

    // RESET PASSWORD
    this.registerMethod({
      name: 'resetPassword',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/resetPassword' }
    });

    // LOGIN
    this.registerMethod({
      name: 'login',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/login' }
    });

    // LOGOUT
    this.registerMethod({
      name: 'logout',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/logout' }
    });

    // GET STATUS
    this.registerMethod({
      name: 'getStatus',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/status' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = AuthResource;
