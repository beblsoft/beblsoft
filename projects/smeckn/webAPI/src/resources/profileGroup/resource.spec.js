/**
 * @file Profile Group Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Profile Group Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let profileGroupName = 'Johnny';
  let profileGroupNewName = 'Fred';
  let profileGroup = null;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create a profile group', async function () {
    profileGroup = await api.profileGroup.create({ data: { name: profileGroupName } });
    assert.equal(profileGroup.name, profileGroupName);
    assert.isNumber(profileGroup.id);
  });

  it('Should list all profile groups', async function () {
    let profileGroupList = await api.profileGroup.list();
    assert.isArray(profileGroupList);
    assert.lengthOf(profileGroupList, 1);
    assert.deepEqual(profileGroup, profileGroupList[0]);
  });

  it('Should get a profile group by id', async function () {
    let dupProfileGroup = await api.profileGroup.get({ urlParams: { id: profileGroup.id } });
    assert.deepEqual(profileGroup, dupProfileGroup);
  });

  it('Should update profile group name', async function () {
    let newProfileGroup = await api.profileGroup.update({
      urlParams: { id: profileGroup.id },
      data: { name: profileGroupNewName }
    });
    assert.equal(newProfileGroup.name, profileGroupNewName);
    assert.equal(newProfileGroup.id, profileGroup.id);
  });

  it('Should delete profile group', async function () {
    let profileGroupList = [];
    await api.profileGroup.delete({ urlParams: { id: profileGroup.id } });
    profileGroupList = await api.profileGroup.list();
    assert.isEmpty(profileGroupList);
  });
});
