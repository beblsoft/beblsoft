/**
 * @file Profile Group Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * ProfileGroupResource Class
 * @extends {Resource}
 */
class ProfileGroupResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/profileGroup' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST', urlTemplate: '' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET', urlTemplate: '' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{id}' }
    });

    // UPDATE
    this.registerMethod({
      name: 'update',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/{id}' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE', urlTemplate: '/{id}' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = ProfileGroupResource;
