/**
 * @file Facebook Profile Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Facebook Profile Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        facebookProfiles: true,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get facebook profile', async function () {
    let waFacebookProfile = await api.facebookProfile.get({
      urlParams: { smID: populater.waFake.popFBPSMID }
    });
    assert.strictEqual(waFacebookProfile.smID, populater.waFake.popFBPSMID);
  });
});
