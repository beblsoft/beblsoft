/**
 * @file Facebook Profile Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * FacebookProfileResource Class
 * @extends {Resource}
 */
class FacebookProfileResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/facebookProfile' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });

    // UPDATE
    this.registerMethod({
      name: 'update',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/{smID}' }
    });

  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = FacebookProfileResource;
