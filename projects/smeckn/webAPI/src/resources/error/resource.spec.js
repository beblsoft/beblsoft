/**
 * @file Error Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Error Resource and Flags', function () {
  let api = test.api;

  /* -------------------- TEST RESOURCE ------------------------------------ */
  it('Should get error from error resource', function(done) {
    api.error.get({ logError: false })
      .then((rval) => { done(rval); })
      .catch((err) => {
        done();
      });
  });

  /* -------------------- TEST API-ERROR-HTTP-STATUS ----------------------- */
  let errorHttpStatusMap = {
    400: 'BadRequestError',
    401: 'AuthError',
    402: 'FailedRequestError',
    404: 'NotFoundError',
    405: 'CodeError',
    429: 'TooManyRequestsError',
    // 500: 'ServerError',  // 500s cause development server to print
    // 510: 'ServerError'
  };
  let errorMapStatuses = Object.keys(errorHttpStatusMap);
  errorMapStatuses.forEach((status) => {
    it(`API-ERROR-HTTP-STATUS ${status} should generate ${errorHttpStatusMap[status]}`, function (done) {
      api.constant.list({ headers: { 'API-ERROR-HTTP-STATUS': status }, logError: false })
        .then((rval) => { done(rval); })
        .catch((err) => {
          assert.equal(err.name, errorHttpStatusMap[status]);
          assert.equal(err.serverCode, 4);
          assert.equal(err.serverMessage, 'Header Error');
          assert.equal(err.httpStatus, status);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  /* -------------------- TEST API-ERROR-CODE ------------------------------ */
  let errorCodeMap = {
    1: { httpStatus: 400, serverMessage: 'Default Error', errorName: 'BadRequestError' },
    1140: { httpStatus: 401, serverMessage: 'Invalid credentials', errorName: 'AuthError' }
  };
  let errorMapCodes = Object.keys(errorCodeMap);
  errorMapCodes.forEach((code) => {
    it(`API-ERROR-CODE ${code} should generate ${errorCodeMap[code].errorName}`, function (done) {
      api.constant.list({ headers: { 'API-ERROR-CODE': code }, logError: false })
        .then((rval) => { done(rval); })
        .catch((err) => {
          assert.equal(err.name, errorCodeMap[code].errorName);
          assert.equal(err.serverCode, code);
          assert.equal(err.serverMessage, errorCodeMap[code].serverMessage);
          assert.equal(err.httpStatus, errorCodeMap[code].httpStatus);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
});
