/**
 * @file Errror Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Error Resource Class
 * @extends {Resource}
 */
class ErrorResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/error' });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = ErrorResource;
