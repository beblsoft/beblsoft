/**
 * @file Profile Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * ProfileResource Class
 * @extends {Resource}
 */
class ProfileResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/profile' });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE', urlTemplate: '/{smID}' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = ProfileResource;
