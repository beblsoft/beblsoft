/**
 * @file Profile Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Profile Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        profiles: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should list profiles', async function () {
    let waProfileList = await api.profile.list({ urlParams: { profileGroupID: populater.waFake.popPGID } });
    assert.isAtLeast(waProfileList.length, 1);
    for (let i = 0; i < waProfileList.length; i++) {
      let waProfile = waProfileList[i];
      assert.isNumber(waProfile.smID);
      assert.strictEqual(waProfile.profileGroupID, populater.waFake.popPGID);
      assert.isString(waProfile.type);
    }
  });

  it('Should get profile by smID', async function () {
    let waProfile = await api.profile.get({ urlParams: { smID: populater.waFake.popProfileSMID } });
    assert.strictEqual(waProfile.smID, populater.waFake.popProfileSMID);
  });

  it('Should delete profile by smID', async function () {
    let errorThrown = false;
    let waProfile = await api.profile.get({ urlParams: { smID: populater.waFake.popProfileSMID } });
    assert.strictEqual(waProfile.smID, populater.waFake.popProfileSMID);
    await api.profile.delete({ urlParams: { smID: populater.waFake.popProfileSMID } });
    try {
      await api.profile.get({ urlParams: { smID: populater.waFake.popProfileSMID }, logError: false });
    } catch (err) { // NOT FOUND
      errorThrown = true;
    }
    assert.isTrue(errorThrown);
  });
});
