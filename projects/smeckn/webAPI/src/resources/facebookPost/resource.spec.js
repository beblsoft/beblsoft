/**
 * @file Facebook Post Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Facebook Post Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        facebookProfiles: true,
        facebookPostsFull: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should list facebook posts', async function () {
    let waFacebookPostPaginationModel = await api.facebookPost.list({
      queryParams: { profileSMID: populater.waFake.popFBPSMID }
    });
    assert.isString(waFacebookPostPaginationModel.nextCursor);
    assert.isAtLeast(waFacebookPostPaginationModel.data.length, 1);
    for (let i = 0; i < waFacebookPostPaginationModel.data.length; i++) {
      let waFacebookPost = waFacebookPostPaginationModel.data[0];
      assert.isNumber(waFacebookPost.smID);
    }
  });

  it('Should get facebook post by smID', async function () {
    let waFacebookPostPaginationModel = await api.facebookPost.list({
      queryParams: { profileSMID: populater.waFake.popFBPSMID }
    });
    let waFacebookPost = waFacebookPostPaginationModel.data[0];
    let waDupFacebookPost = await api.facebookPost.get({ urlParams: { smID: waFacebookPost.smID } });
    assert.strictEqual(waDupFacebookPost.smID, waFacebookPost.smID);
  });
});
