/**
 * @file Facebook Post Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * FacebookPostResource Class
 * @extends {Resource}
 */
class FacebookPostResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/facebookPost' });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = FacebookPostResource;
