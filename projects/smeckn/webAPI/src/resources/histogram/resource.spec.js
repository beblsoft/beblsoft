/**
 * @file Histogram Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');
let moment = require('moment');


/* ------------------------ MAIN ------------------------------------------- */
describe('Histogram Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        profiles: true,
        facebookProfiles: true,
        facebookPostsFull: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get histogram', async function () {
    let contentType = 'POST';
    let contentAttrType = 'FACEBOOK_POST_SMID';
    let groupByType = 'FACEBOOK_POST_CREATE_DATE';
    let intervalStartDate = moment('1995-12-25T00:00:00+00:00');
    let intervalEndDate = moment();
    let waHistogramList = await api.histogram.get({
      queryParams: {
        profileSMID: populater.waFake.popFBPSMID,
        contentType,
        contentAttrType,
        groupByType,
        intervalStartDate: intervalStartDate.toISOString(),
        intervalEndDate: intervalEndDate.toISOString(),
      }
    });
    assert.isArray(waHistogramList);
    assert.isAtLeast(waHistogramList.length, 1);
    for (let i = 0; i < waHistogramList.length; i++) {
      let waHistogram = waHistogramList[i];
      let waHistogramDataList = waHistogram.dataModelList;
      assert.strictEqual(waHistogram.profileSMID, populater.waFake.popFBPSMID);
      assert.strictEqual(waHistogram.contentType, contentType);
      assert.strictEqual(waHistogram.contentAttrType, contentAttrType);
      assert.strictEqual(waHistogram.groupByType, groupByType);
      for (let j = 0; j < waHistogramDataList; j++) {
        let waHistogramData = waHistogramDataList[j];
        assert.isString(waHistogramData.groupByDate);
        assert.isNumber(waHistogramData.count);
        assert.isNumber(waHistogramData.min);
        assert.isNumber(waHistogramData.max);
        assert.isNumber(waHistogramData.sum);
        assert.isNumber(waHistogramData.average);
        assert.isNumber(waHistogramData.stddev);
      }
    }
  });
});
