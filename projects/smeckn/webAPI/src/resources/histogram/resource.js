/**
 * @file Histogram Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * HistogramResource Class
 * @extends {Resource}
 */
class HistogramResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/histogram' });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = HistogramResource;
