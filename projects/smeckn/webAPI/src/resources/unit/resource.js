/**
 * @file Unit Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Unit Class
 * @extends {Resource}
 */
class UnitResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/unit' });

    // GET PRICE
    this.registerMethod({
      name: 'getPrice',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/price' }
    });

    // GET BALANCE
    this.registerMethod({
      name: 'getBalance',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/balance' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = UnitResource;
