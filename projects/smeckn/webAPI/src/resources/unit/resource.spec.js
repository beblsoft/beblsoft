/**
 * @file Unit Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Unit Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        unitLedger: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get balance', async function () {
    let waUnitBalanceList = await api.unit.getBalance();
    assert.isAtLeast(waUnitBalanceList.length, 1);
    waUnitBalanceList.forEach(function (waUnitBalance) {
      assert.isString(waUnitBalance.unitType);
      assert.isNumber(waUnitBalance.nAvailable);
      assert.isNumber(waUnitBalance.nHeld);
      assert.isNumber(waUnitBalance.nTotal);
    });
  });

  it('Should get balance of a specific unit', async function () {
    let unitType = 'TEXT_ANALYSIS';
    let waUnitBalanceList = await api.unit.getBalance({ queryParams: { unitType: unitType } });
    let waUnitBalance = waUnitBalanceList[0];
    assert.equal(waUnitBalanceList.length, 1);
    assert.strictEqual(waUnitBalance.unitType, unitType);
  });

  it('Should get price', async function () {
    let waUnitPriceList = await api.unit.getPrice();
    assert.isAtLeast(waUnitPriceList.length, 1);
    waUnitPriceList.forEach(function (waUnitPrice) {
      assert.exists(waUnitPrice.unitType);
      assert.exists(waUnitPrice.minPurchaseCount);
      assert.exists(waUnitPrice.purchaseIncrement);
      assert.exists(waUnitPrice.unitCost);
      assert.exists(waUnitPrice.bCurrencyCode);
    });
  });

  it('Should get price of a specific unit', async function () {
    let unitType = 'TEXT_ANALYSIS';
    let waUnitPriceList = await api.unit.getPrice({ queryParams: { unitType: unitType } });
    let waUnitPrice = waUnitPriceList[0];
    assert.equal(waUnitPriceList.length, 1);
    assert.strictEqual(waUnitPrice.unitType, unitType);
  });
});
