/**
 * @file Statistic Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Statistic Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();


  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        profiles: true,
        facebookProfiles: true,
        facebookPostsFull: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get statistic', async function () {
    let contentType = 'POST';
    let contentAttrType = 'FACEBOOK_POST_SMID';
    let waStatisticList = await api.statistic.get({
      queryParams: { profileSMID: populater.waFake.popFBPSMID, contentType, contentAttrType }
    });
    assert.isArray(waStatisticList);
    assert.isAtLeast(waStatisticList.length, 1);
    for (let i = 0; i < waStatisticList.length; i++) {
      let waStatistic = waStatisticList[i];
      assert.strictEqual(waStatistic.profileSMID, populater.waFake.popFBPSMID);
      assert.strictEqual(waStatistic.contentType, contentType);
      assert.strictEqual(waStatistic.contentAttrType, contentAttrType);
      assert.isNumber(waStatistic.count);
    }
  });
});
