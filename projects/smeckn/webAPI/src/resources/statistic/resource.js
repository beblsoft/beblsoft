/**
 * @file Statistic Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * StatisticResource Class
 * @extends {Resource}
 */
class StatisticResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/statistic' });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = StatisticResource;
