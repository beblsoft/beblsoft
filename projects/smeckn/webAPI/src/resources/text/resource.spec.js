/**
 * @file Text Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Text Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let text = 'Happiness, Joy, Contentment, Satisfaction!';
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        unitLedger: true,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create text', async function () {
    let waText = await api.text.create({ data: { text } });
    assert.isNumber(waText.smID);
    assert.isString(waText.createDate);
    assert.strictEqual(waText.text, text);
    assert.strictEqual(waText.origLength, text.length);
    assert.isNull(waText.compLangAnalysisModel);
    assert.isNull(waText.compSentAnalysisModel);
  });

  it('Should create chinese text', async function () {
    let chineseText = 'いが同じではない。字体や用字法は地域ごとに異なる点が見られ、1980年代以降、それ以前に活字でよく見ら';
    let waText = await api.text.create({ data: { text: chineseText } });
    assert.isNumber(waText.smID);
    assert.isString(waText.createDate);
    assert.strictEqual(waText.text, chineseText);
    assert.strictEqual(waText.origLength, chineseText.length);
    assert.isNull(waText.compLangAnalysisModel);
    assert.isNull(waText.compSentAnalysisModel);
  });

  it('Should create swedish text', async function () {
    let chineseText = 'Envar har rätt till undervisning. Undervisningen skall vara kostnadsfri, åtminstone på de elementära och grundläggande stadierna. Den elementära undervisningen skall vara obligatorisk. Yrkesundervisning och teknisk undervisning skall vara allmänt tillgänglig. Den högre undervisningen skall stå öppen i lika mån för alla på grundval av deras duglighet.';
    let waText = await api.text.create({ data: { text: chineseText } });
    assert.isNumber(waText.smID);
    assert.isString(waText.createDate);
    assert.strictEqual(waText.text, chineseText);
    assert.strictEqual(waText.origLength, chineseText.length);
    assert.isNull(waText.compLangAnalysisModel);
    assert.isNull(waText.compSentAnalysisModel);
  });

  it('Should list text', async function () {
    let waTextPaginationModel = await api.text.list();
    assert.isString(waTextPaginationModel.nextCursor);
    assert.isArray(waTextPaginationModel.data);
    for (let i = 0; i < waTextPaginationModel.length; i++) {
      let waText = waTextPaginationModel[i];
      assert.isObject(waText);
    }
  });

  it('Should get text by smID', async function () {
    let waTextPaginationModel = await api.text.list();
    let waText = waTextPaginationModel.data[0];
    let waDupText = await api.text.get({ urlParams: { smID: waText.smID } });
    assert.strictEqual(waDupText.smID, waText.smID);
  });

  it('Should analyze text by smID', async function () {
    // Careful here... we pay for every analysis.
    let waTextPaginationModel = await api.text.list();
    let waText = waTextPaginationModel.data[0];
    let waAnalyzedText = await api.text.analyze({
      urlParams: { smID: waText.smID },
      queryParams: { analysisType: 'COMPREHEND_SENTIMENT' }
    });
    assert.strictEqual(waAnalyzedText.smID, waText.smID);
    assert.isObject(waAnalyzedText.compLangAnalysisModel);
    assert.isObject(waAnalyzedText.compSentAnalysisModel);
  });

  it('Should delete text by smID', async function () {
    let waTextPaginationModel = await api.text.list();
    let waText = waTextPaginationModel.data[0];
    await api.text.delete({ urlParams: { smID: waText.smID } });
    let errorThrown = false;
    try {
      await api.text.get({ urlParams: { smID: waText.smID }, logError: false });
    } catch (err) { // NOT FOUND
      errorThrown = true;
    }
    assert.isTrue(errorThrown);
  });

});
