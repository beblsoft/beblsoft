/**
 * @file Text Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * TextResource Class
 * @extends {Resource}
 */
class TextResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/text' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE', urlTemplate: '/{smID}' }
    });

    // ANALYZE
    this.registerMethod({
      name: 'analyze',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/{smID}/analysis' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = TextResource;
