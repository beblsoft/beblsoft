/**
 * @file Charge Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe.skip('Charge Resource', function () {
  // Skipping these tests as they are causing stripe to send back rate limit errors in CI

  let api = test.api;
  let populater = test.populater;
  let token = 'tok_visa';
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      loadConstants: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create a charge', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      // Create credit card
      let waCreditCard = await api.creditCard.create({ data: { token } });

      // Create cart
      let waCartPostOutputModel = await api.cart.create({
        data: {
          cartUnitCountList: [
            { unitType: 'TEXT_ANALYSIS', count: 500 },
            { unitType: 'PHOTO_ANALYSIS', count: 500 }
          ],
          bCurrencyCode: 'USD'
        }
      });

      // Create charge
      let waCharge = await api.charge.create({
        data: { cartJWT: waCartPostOutputModel.cartJWT, stripeCardID: waCreditCard.id }
      });
      assert.isString(waCharge.id);
      assert.isString(waCharge.createdDate);
      assert.isString(waCharge.description);
      assert.isString(waCharge.bCurrencyCode);
      assert.isNumber(waCharge.amount);
    }
  });

  it('Should list charges', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let chargePaginationModel = await api.charge.list();
      assert.strictEqual(chargePaginationModel.nextCursor, '');
      assert.isArray(chargePaginationModel.data);
      assert.isAtLeast(chargePaginationModel.data.length, 1);
    }
  });
});
