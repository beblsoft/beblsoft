/**
 * @file Charge Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * ChargeResource Class
 * @extends {Resource}
 */
class ChargeResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/charge' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST', urlTemplate: '' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET', urlTemplate: '' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = ChargeResource;
