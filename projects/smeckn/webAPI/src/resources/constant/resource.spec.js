/**
 * @file Constant Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Constant Resource', function () {
	let api = test.api;

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get constants', async function () {
    let waConstants = await api.constant.list({});
    assert.typeOf(waConstants.MAX_PROFILEGROUPS, 'number');
  });

});
