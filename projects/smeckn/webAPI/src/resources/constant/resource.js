/**
 * @file Constant Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Constant Class
 * @extends {Resource}
 */
class ConstantResource extends Resource {

  /**
   * @constructor
   * @param  {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/constant' });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = ConstantResource;
