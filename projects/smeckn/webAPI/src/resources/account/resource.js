/**
 * @file Account Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Account Class
 * @extends {Resource}
 */
class AccountResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/account' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST', urlTemplate: '' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{id}' }
    });

    // CREATE TOKENS
    this.registerMethod({
      name: 'createTokens',
      methodArgs: { requestMethod: 'POST', urlTemplate: '/tokens' }
    });

    // UPDATE
    this.registerMethod({
      name: 'update',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/{id}' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE', urlTemplate: '/{id}' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = AccountResource;
