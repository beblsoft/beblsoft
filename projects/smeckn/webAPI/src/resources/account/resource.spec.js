/**
 * @file Account Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Account Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();
  let type = 'TEST';
  let id = null;

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({ waitPlatformOnline: true });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create an account', function (done) {
    api.account.create({ data: { email, type, password } })
      .then((data) => {
        id = data.id;
        assert.typeOf(data.id, 'number');
        assert.equal(data.email, email);
        assert.equal(data.type, type);
        assert.equal(data.active, true);
        done();
      })
      .catch((err) => {
        console.log(err.serverStack);
        done(err);
      });
  });

  it('Should get account by email', function (done) {
    api.account.list({ queryParams: { email } })
      .then((data) => {
        let account = data[0];
        id = account.id;
        assert.typeOf(account.id, 'number');
        assert.equal(account.email, email);
        assert.equal(account.type, type);
        assert.equal(account.active, true);
        done();
      })
      .catch((err) => { done(err); });
  });

  it('Should get accout by id', function (done) {
    api.account.get({ urlParams: { id } })
      .then((data) => { done(); })
      .catch((err) => { done(err); });
  });

  it('Should get accout tokens', function (done) {
    api.account.createTokens({ data: { email, password } })
      .then((data) => {
        assert.typeOf(data.authToken, 'string');
        assert.typeOf(data.accountToken, 'string');
        assert.typeOf(data.createAccountToken, 'string');
        assert.typeOf(data.forgotPasswordToken, 'string');
        done();
      })
      .catch((err) => { done(err); });
  });

  it('Should update account active', function (done) {
    api.account.update({ urlParams: { id }, data: { active: false } })
      .then((data) => {
        assert.equal(data.active, false);
        done();
      })
      .catch((err) => { done(err); });
  });

  it('Should delete account', function (done) {
    api.account.delete({ urlParams: { id } })
      .then((data) => { done(); })
      .catch((err) => { done(err); });
  });
});
