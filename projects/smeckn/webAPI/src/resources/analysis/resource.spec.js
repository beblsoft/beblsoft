/**
 * @file Analysis Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');
let moment = require('moment');


/* ------------------------ MAIN ------------------------------------------- */
describe('Analysis Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let contentType = 'POST';
  let analysisType = 'COMPREHEND_SENTIMENT';
  let analysisSMID = null;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        unitLedger: true,
        profileGroups: true,
        profiles: true,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create analysis', async function () {
    // This test creates an analysis which in turn spawns analysis workers
    // See: smeckn/server/profileStub/post/compSentAnalysis.py
    // These analysis workers race the tests:
    // - If the tests go fast: analysis workers could hit exceptions when the
    //   underlying account database is deleted in the next spec.js file
    // - If the analysis workers go fast: the analysis will be completed before
    //   other 'it' statements run.
    //
    // To ensure that the analysis workers complete before the next .spec file
    // the last test waits for the analysis to complete.
    let waAnalysis = await api.analysis.create({
      data: { profileSMID: populater.waFake.popProfileSMID, contentType, analysisType },
    });
    assert.strictEqual(waAnalysis.profileSMID, populater.waFake.popProfileSMID);
    analysisSMID = waAnalysis.smID;
  });

  it('Should list analyses', async function () {
    // Race prevention: We must call analysis.list twice as we don't know when
    // the analysis will be completed on the back end.
    let waAnalysisInProgressList = await api.analysis.list({
      queryParams: { profileSMID: populater.waFake.popProfileSMID, status: 'IN_PROGRESS' }
    });
    let waAnalysisSuccessList = await api.analysis.list({
      queryParams: { profileSMID: populater.waFake.popProfileSMID, status: 'SUCCESS' }
    });
    let waAnalysisList = waAnalysisInProgressList.concat(waAnalysisSuccessList);
    assert.isArray(waAnalysisList);
    assert.isAtLeast(waAnalysisList.length, 1);
    for (let i = 0; i < waAnalysisList; i++) {
      let waAnalysis = waAnalysisList[i];
      assert.isNumber(waAnalysis.smID);
      assert.strictEqual(waAnalysis.smID, analysisSMID);
      assert.strictEqual(waAnalysis.profileSMID, populater.waFake.popProfileSMID);
      assert.isString(waAnalysis.createDate);
      assert.isString(waAnalysis.contentType);
      assert.isString(waAnalysis.analysisType);
      assert.isString(waAnalysis.status);
      assert.isBoolean(waAnalysis.errorSeen);
    }
  });

  it('Should get analysis count', async function () {
    let profileSMID = populater.waFake.popProfileSMID;
    let waAnalysisCount = await api.analysis.getCount({
      queryParams: { profileSMID, contentType, analysisType }
    });
    assert.strictEqual(waAnalysisCount.profileSMID, profileSMID);
    assert.strictEqual(waAnalysisCount.contentType, contentType);
    assert.strictEqual(waAnalysisCount.analysisType, analysisType);
    assert.isNumber(waAnalysisCount.countDone);
    assert.isNumber(waAnalysisCount.countNotDone);
    assert.isNumber(waAnalysisCount.countCant);
  });

  it('Should get analysis by smID', async function () {
    let waAnalysis = await api.analysis.get({ urlParams: { smID: analysisSMID } });
    assert.strictEqual(waAnalysis.smID, analysisSMID);
  });

  it('Should update analysis by smID', async function () {
    let errorSeenList = [true, false];
    for (let i = 0; i < errorSeenList.length; i++) {
      let errorSeen = errorSeenList[i];
      let waAnalysis = await api.analysis.update({
        urlParams: { smID: analysisSMID },
        data: { errorSeen: errorSeen }
      });
      assert.strictEqual(waAnalysis.smID, analysisSMID);
      assert.strictEqual(waAnalysis.errorSeen, errorSeen);
    }
  });

  it('Should get analysis progress by smID', async function () {
    let waAnalysisProgress = await api.analysis.getProgress({ urlParams: { smID: analysisSMID } });
    assert.strictEqual(waAnalysisProgress.contentType, contentType);
    assert.strictEqual(waAnalysisProgress.analysisType, analysisType);
    assert.isNumber(waAnalysisProgress.countAnalyzed);
    assert.isNumber(waAnalysisProgress.countTotal);
    assert.isString(waAnalysisProgress.status);
  });

  it('Should wait until the analysis is complete', async function () {
    let startMoment = moment();
    let curMoment = moment();
    let maxWaitDuration = moment.duration(60, 'seconds');
    let waitDuration = null;
    let checkInterval = moment.duration(5, 'seconds');
    let complete = false;
    let keepChecking = true;

    while (keepChecking) {
      let waAnalysisProgress = await api.analysis.getProgress({ urlParams: { smID: analysisSMID } });
      complete = waAnalysisProgress.status === 'SUCCESS';
      if (!complete) {
        await new Promise((resolve) => setTimeout(resolve, checkInterval.asMilliseconds()));
      }
      curMoment = moment();
      waitDuration = curMoment.diff(startMoment);
      keepChecking = !complete && waitDuration < maxWaitDuration;
    }
    assert.isTrue(complete);
  });
});
