/**
 * @file Analysis Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * AnalysisResource Class
 * @extends {Resource}
 */
class AnalysisResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/analysis' });

    // CREATE (and START)
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET COUNT
    this.registerMethod({
      name: 'getCount',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/count' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });

    // UPDATE
    this.registerMethod({
      name: 'update',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/{smID}' }
    });

    // GET PROGRESS
    this.registerMethod({
      name: 'getProgress',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}/progress' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = AnalysisResource;
