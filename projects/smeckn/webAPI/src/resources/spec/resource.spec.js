/**
 * @file Spec Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Spec Resource', function () {
  let api = test.api;

  /* -------------------- CREATE ------------------------------------------- */
  it('Should get the spec', async function () {
    let waSpec = await api.spec.get();
    assert.hasAllKeys(waSpec,
      ['basePath', 'consumes', 'definitions', 'info', 'paths', 'produces', 'responses', 'security',
       'securityDefinitions', 'swagger', 'tags']);
  });
});
