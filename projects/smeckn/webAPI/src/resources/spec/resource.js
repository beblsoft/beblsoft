/**
 * @file Spec Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Spec Resource Class
 * @extends {Resource}
 */
class SpecResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/spec' });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = SpecResource;
