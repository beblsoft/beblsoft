/**
 * @file Credit Card Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe.skip('Credit Card Resource', function () {
  // Skipping these tests as they are causing stripe to send back rate limit errors in CI

  let api = test.api;
  let populater = test.populater;
  let tokenList = ['tok_visa', 'tok_mastercard'];
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      loadConstants: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create credit card', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      for (let i = 0; i < tokenList.length; i++) {
        let token = tokenList[i];
        let waCreditCard = await api.creditCard.create({ data: { token } });
        assert.isString(waCreditCard.id);
        assert.isString(waCreditCard.brand);
        assert.isString(waCreditCard.last4);
      }
    }
  });

  it('Should list credit cards', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let waCreditCardList = await api.creditCard.list();
      assert.strictEqual(waCreditCardList.length, tokenList.length);
      for (let i = 0; i < waCreditCardList.length; i++) {
        let waCreditCard = waCreditCardList[i];
        assert.isString(waCreditCard.id);
      }
    }
  });

  it('Should get default credit card', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let waCreditCard = await api.creditCard.getDefault();
      assert.isString(waCreditCard.id);
    }
  });

  it('Should set and get default credit card', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let waCreditCardList = await api.creditCard.list();
      assert.strictEqual(waCreditCardList.length, tokenList.length);
      for (let i = 0; i < waCreditCardList.length; i++) {
        let waCreditCard = waCreditCardList[i];
        let _ = await api.creditCard.setDefault({ data: { id: waCreditCard.id } });
        let waDefaultCreditCard = await api.creditCard.getDefault();
        assert.strictEqual(waDefaultCreditCard.id, waCreditCard.id);
      }
    }
  });

  it('Should get credit card by id', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let waCreditCardList = await api.creditCard.list();
      assert.strictEqual(waCreditCardList.length, tokenList.length);
      for (let i = 0; i < waCreditCardList.length; i++) {
        let waCreditCard = waCreditCardList[i];
        let waCurrentCreditCard = await api.creditCard.get({ urlParams: { id: waCreditCard.id } });
        assert.strictEqual(waCurrentCreditCard.id, waCreditCard.id);
      }
    }
  });

  it('Should delete credit card by id', async function () {
    if (populater.waConstants.ENVIRONMENT === 'Production') { this.skip(); } else {
      let waCreditCardList = await api.creditCard.list();
      assert.strictEqual(waCreditCardList.length, tokenList.length);
      for (let i = 0; i < waCreditCardList.length; i++) {
        let waCreditCard = waCreditCardList[i];
        await api.creditCard.delete({ urlParams: { id: waCreditCard.id } });
      }
      waCreditCardList = await api.creditCard.list();
      assert.strictEqual(waCreditCardList.length, 0);
    }
  });
});
