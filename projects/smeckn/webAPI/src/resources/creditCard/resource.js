/**
 * @file Credit Card Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * CreditCardResource Class
 * @extends {Resource}
 */
class CreditCardResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/creditCard' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST', urlTemplate: '' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET', urlTemplate: '' }
    });

    // SET DEFAULT
    this.registerMethod({
      name: 'setDefault',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/default' }
    });

    // GET DEFAULT
    this.registerMethod({
      name: 'getDefault',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/default' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE', urlTemplate: '/{id}' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{id}' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = CreditCardResource;
