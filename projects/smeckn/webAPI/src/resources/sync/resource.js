/**
 * @file Sync Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * SyncResource Class
 * @extends {Resource}
 */
class SyncResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/sync' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST' }
    });

    // LIST
    this.registerMethod({
      name: 'list',
      methodArgs: { requestMethod: 'GET' }
    });

    // GET
    this.registerMethod({
      name: 'get',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}' }
    });

    // UPDATE
    this.registerMethod({
      name: 'update',
      methodArgs: { requestMethod: 'PUT', urlTemplate: '/{smID}' }
    });


    // GET PROGRESS
    this.registerMethod({
      name: 'getProgress',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/{smID}/progress' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = SyncResource;
