/**
 * @file Sync Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Sync Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
        profileGroups: true,
        profiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it.skip('Should create sync', async function () {
    // Cannot create sync as Facebook limits API requests
    let waSync = await api.sync.create({ data: { profileSMID: populater.waFake.popProfileSMID } });
    assert.isObject(waSync);
  });

  it('Should list syncs', async function () {
    let waSyncList = await api.sync.list({
      queryParams: {
        profileSMID: populater.waFake.popProfileSMID,
        latest: true
      }
    });
    assert.isAtLeast(waSyncList.length, 1);
    for (let i = 0; i < waSyncList.length; i++) {
      let waSync = waSyncList[i];
      assert.isNumber(waSync.smID);
      assert.strictEqual(waSync.profileSMID, populater.waFake.popProfileSMID);
      assert.isString(waSync.createDate);
      assert.isString(waSync.restartDate);
      assert.isString(waSync.status);
      assert.isBoolean(waSync.errorSeen);
    }
  });

  it('Should get sync by smID', async function () {
    let waSyncList = await api.sync.list({
      queryParams: {
        profileSMID: populater.waFake.popProfileSMID,
        latest: true
      }
    });
    let waSync = await api.sync.get({ urlParams: { smID: waSyncList[0].smID } });
    assert.strictEqual(waSync.smID, waSyncList[0].smID);
  });

  it('Should update sync by smID', async function () {
    let waSyncList = await api.sync.list({
      queryParams: {
        profileSMID: populater.waFake.popProfileSMID,
        latest: true
      }
    });
    let errorSeenList = [true, false];
    for (let i = 0; i < errorSeenList.length; i++) {
      let errorSeen = errorSeenList[i];
      await api.sync.update({ urlParams: { smID: waSyncList[0].smID }, data: { errorSeen } });
      let waSync = await api.sync.get({ urlParams: { smID: waSyncList[0].smID } });
      assert.strictEqual(waSync.errorSeen, errorSeen);
    }
  });

  it('Should get sync progress by smID', async function () {
    let waSyncList = await api.sync.list({
      queryParams: {
        profileSMID: populater.waFake.popProfileSMID,
        latest: true
      }
    });
    let waSyncProgress = await api.sync.getProgress({ urlParams: { smID: waSyncList[0].smID } });
    assert.isObject(waSyncProgress);
    assert.isString(waSyncProgress.status);
  });
});
