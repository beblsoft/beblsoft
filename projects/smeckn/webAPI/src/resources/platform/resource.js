/**
 * @file Platform Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Platform Class
 * @extends {Resource}
 */
class PlatformResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/platform' });

    // GET GDBS STATUS
    this.registerMethod({
      name: 'getGDBSStatus',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/gdbsStatus' }
    });

    // GET ADBS STATUS
    this.registerMethod({
      name: 'getADBSStatus',
      methodArgs: { requestMethod: 'GET', urlTemplate: '/adbsStatus' }
    });
  }

  /**
   * Loop until API platform comes online
   * @param  {number} options.reCheckS  How frequently to check if platform is online
   * @param  {Object} options.maxWaitS} Total time to wait
   */
  async waitOnline({ reCheckS = 1, maxWaitS = 120 } = {}) {
    let online = false;
    let startDateS = new Date().getTime() / 1000;
    let curDateS = null;
    const sleep = (secs) => { /* eslint-disable-line */
      return new Promise((resolve) => { setTimeout(resolve, secs * 1000); });
    };

    /* Iterate until platform online or time is exceeded */
    do {
      try {
        await this.getGDBSStatus({ logError: false });
        await this.getADBSStatus({ logError: false });
        online = true;
      } catch (err) {
        curDateS = new Date().getTime() / 1000;
        if (curDateS - startDateS > maxWaitS) {
          throw (err);
        } else {
          await sleep(reCheckS);
        }
      }
    } while (!online);
    return online;
  }

}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = PlatformResource;
