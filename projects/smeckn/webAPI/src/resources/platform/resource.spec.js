/**
 * @file Platform Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Platform Resource', function () {
	let api = test.api;
  let populater = test.populater;

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({ waitPlatformOnline: true });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should get GDBS Status', async function () {
    let waGDBSStatus = await api.platform.getGDBSStatus({});
    assert.exists(waGDBSStatus.domainName);
    assert.equal(waGDBSStatus.state, 'ONLINE');
  });

  it('Should get ADBS Status', async function () {
    let waADBSStatus = await api.platform.getADBSStatus({});
    let waADBStatusData = waADBSStatus.data;
    assert.isAtLeast(waADBStatusData.length, 1);
    assert.exists(waADBStatusData[0].domainName);
    assert.equal(waADBStatusData[0].state, 'ONLINE');
  });
});
