/**
 * @file Cart Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe.skip('Cart Resource', function () {
  // Skipping these tests as they are causing stripe to send back rate limit errors in CI

  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();

  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({
      waitPlatformOnline: true,
      fakeCreateInputData: {
        accountEmail: email,
        accountPassword: password,
      },
      initAccountToken: true
    });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create cart', async function () {
    let inputData = {
      cartUnitCountList: [
        { unitType: 'TEXT_ANALYSIS', count: 500 },
        { unitType: 'PHOTO_ANALYSIS', count: 500 }
      ],
      bCurrencyCode: 'USD'
    };
    let waCartPostOutputModel = await api.cart.create({ data: inputData });
    assert.isObject(waCartPostOutputModel.cartModel);
    assert.isString(waCartPostOutputModel.cartJWT);
  });
});
