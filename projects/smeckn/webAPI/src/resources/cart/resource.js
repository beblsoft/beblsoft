/**
 * @file Cart Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * CartResource Class
 * @extends {Resource}
 */
class CartResource extends Resource {

  /**
   * @constructor
   * @param {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/cart' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST', urlTemplate: '' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = CartResource;
