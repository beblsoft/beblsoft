/**
 * @file Fake Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Resource = require('../../common/resource');


/* ------------------------------------------------------------------------- */
/**
 * Fake Resource Class
 * @extends {Resource}
 */
class FakeResource extends Resource {

  /**
   * @constructor
   * @param  {API} api
   */
  constructor({ api = null }) {
    super({ api: api, basePath: '/fake' });

    // CREATE
    this.registerMethod({
      name: 'create',
      methodArgs: { requestMethod: 'POST' }
    });

    // DELETE
    this.registerMethod({
      name: 'delete',
      methodArgs: { requestMethod: 'DELETE' }
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = FakeResource;
