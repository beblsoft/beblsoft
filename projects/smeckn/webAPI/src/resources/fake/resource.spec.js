/**
 * @file Fake Resource Tests
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let test = require('../../../test/main');


/* ------------------------ MAIN ------------------------------------------- */
describe('Fake Resource', function () {
  let api = test.api;
  let populater = test.populater;
  let email = populater.getTestEmail({ test: this });
  let password = populater.getTestPassword();


  /* -------------------- HOOKS -------------------------------------------- */
  before(async function () {
    await populater.init({ waitPlatformOnline: true });
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  it('Should create fake data', async function () {
    let fakeInputData = {
      accountEmail: email,
      accountPassword: password,
      profileGroups: true,
      profiles: true,
      syncsForAllProfilesSuccess: true,
      textFull: true,
      unitLedger: true,
      facebookProfiles: true,
      facebookPostsFull: true
    };
    let waFake = await api.fake.create({ data: fakeInputData });
    assert.exists(waFake.popAccountID);
    assert.strictEqual(waFake.popAccountEmail, email);
    assert.exists(waFake.popAuthToken);
    assert.exists(waFake.popAccountToken);
    assert.exists(waFake.popPGID);
    assert.exists(waFake.popProfileSMID);
    assert.exists(waFake.popFBPSMID);
  });

  it('Should delete fake data', async function () {
    let waAccountList = [];
    let fakeInputData = { accountEmail: email };
    let errorThrown = false;
    try {
      await api.fake.delete({ queryParams: fakeInputData });
      waAccountList = await api.account.list({ queryParams: { email }, logError: false });
    } catch (err) { /* NOT FOUND */
      errorThrown = true;
    }
    assert.isEmpty(waAccountList);
    assert.isTrue(errorThrown);
  });
});
