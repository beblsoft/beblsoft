/**
 * @file API Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let https = require('https');
let SpecResource = require('./resources/spec/resource.js');
let ConstantResource = require('./resources/constant/resource.js');
let ErrorResource = require('./resources/error/resource.js');
let PlatformResource = require('./resources/platform/resource.js');
let AuthResource = require('./resources/auth/resource.js');
let AccountResource = require('./resources/account/resource.js');
let FakeResource = require('./resources/fake/resource.js');
let UnitResource = require('./resources/unit/resource.js');
let CreditCardResource = require('./resources/creditCard/resource.js');
let CartResource = require('./resources/cart/resource.js');
let ChargeResource = require('./resources/charge/resource.js');
let ProfileGroupResource = require('./resources/profileGroup/resource.js');
let ProfileResource = require('./resources/profile/resource.js');
let SyncResource = require('./resources/sync/resource.js');
let AnalysisResource = require('./resources/analysis/resource.js');
let StatisticResource = require('./resources/statistic/resource.js');
let HistogramResource = require('./resources/histogram/resource.js');
let TextResource = require('./resources/text/resource.js');
let FacebookProfileResource = require('./resources/facebookProfile/resource.js');
let FacebookPostResource = require('./resources/facebookPost/resource.js');


/* ------------------------------------------------------------------------- */
/**
 * API Class
 */
class API {
  /**
   * @constructor
   * @param {String} baseDomain - Base domain to reach api. Ex.'https://api.smeckn.com'
   * @param {String} version - API Version
   * @param {number} requestTimeoutMS - Number of milliseconds before requests are timedout
   * @param {String} authToken
   * @param {String} accountToken
   * @param {Boolean} logErrors If true, logErrors
   * @param {Boolean} rejectUnauthorized If true, reject unauthorized certificates
   * @param {Boolean} withCredentials If true, send Cookie credentials to server
   */
  constructor({
    baseDomain = 'https://localhost:5100',
    version = 'v1',
    requestTimeoutMS = 60000,
    authToken = null,
    accountToken = null,
    logErrors = true,
    rejectUnauthorized = true,
    withCredentials = true,
  } = {}) {
    /* API */
    this.baseDomain = baseDomain;
    this.version = version;
    this.authToken = authToken;
    this.accountToken = accountToken;
    this.requestTimeoutMS = requestTimeoutMS;
    this.logErrors = logErrors;
    this.httpsAgent = new https.Agent({ rejectUnauthorized });
    this.withCredentials = withCredentials;

    /* Resources */
    this.spec = new SpecResource({ api: this });
    this.constant = new ConstantResource({ api: this });
    this.error = new ErrorResource({ api: this });
    this.platform = new PlatformResource({ api: this });
    this.auth = new AuthResource({ api: this });
    this.account = new AccountResource({ api: this });
    this.fake = new FakeResource({ api: this });
    this.unit = new UnitResource({ api: this });
    this.creditCard = new CreditCardResource({ api: this });
    this.cart = new CartResource({ api: this });
    this.charge = new ChargeResource({ api: this });
    this.profileGroup = new ProfileGroupResource({ api: this });
    this.profile = new ProfileResource({ api: this });
    this.sync = new SyncResource({ api: this });
    this.analysis = new AnalysisResource({ api: this });
    this.statistic = new StatisticResource({ api: this });
    this.histogram = new HistogramResource({ api: this });
    this.text = new TextResource({ api: this });
    this.facebookProfile = new FacebookProfileResource({ api: this });
    this.facebookPost = new FacebookPostResource({ api: this });
  }

  /**
   * @return {string} baseURL
   */
  get baseURL() {
    return `${this.baseDomain}/${this.version}`;
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = API;
