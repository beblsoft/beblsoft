/**
 * @file Error Functionality
 */


/* ------------------------------------------------------------------------- */
/**
 * APIError Class
 * @extends {Error}
 */
class APIError extends Error {
  /**
   * @constructor
   * @param  {String} message - Error message
   * @param  {Number} code - Smeckn error code
   * @param  {Number} httpStatus - http error status Ex. 500
   */
  constructor({ message = '', httpStatus = null, serverCode = null, serverMessage = null, serverStack = null, axiosErr = null } = {}) {
    super(message);
    // Properly capture the stack trace
    // Note: this doesn't work in the browser
    // Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.httpStatus = httpStatus;
    this.serverCode = serverCode;
    this.serverMessage = serverMessage || message;
    this.serverStack = serverStack;
    this.axiosErr = axiosErr;

  }

  /**
   * Return APIError given Axios error
   * @param {error} axiosErr axios error structure
   * @return {APIError} subclasses shown below
   */
  static fromAxiosResponse(axiosErr) {
    let apiError = null;
    let response = axiosErr.response;
    let data = response ? (response.data || null) : null;
    let httpStatus = response ? (response.status || null) : null;
    let errorArgs = null;

    // 3 Cases
    // 1: Request made, but server responded with status code outside of 2xx range
    // 2: The request was made but no response was received
    // 3: Something happened in setting up the request that went wrong
    if (axiosErr.response) { // ---------------------------------------- CASE 1
      errorArgs = {
        message: response.statusText || null,
        httpStatus: httpStatus,
        serverCode: data.code || null,
        serverStack: data.stack || null,
        serverMessage: data.message || 'Unknown Server Error',
        axiosErr
      };
      // 400 Errors
      if (400 <= httpStatus && httpStatus < 500) {
        switch (httpStatus) {
        case 400:
          apiError = new BadRequestError(errorArgs);
          break;
        case 401:
          apiError = new AuthError(errorArgs);
          break;
        case 402:
          apiError = new FailedRequestError(errorArgs);
          break;
        case 404:
          apiError = new NotFoundError(errorArgs);
          break;
        case 429:
          apiError = new TooManyRequestsError(errorArgs);
          break;
        default:
          apiError = new CodeError(errorArgs);
          break;
        }
        // 500 Errors
      } else if (500 <= httpStatus && httpStatus < 600) {
        apiError = new ServerError(errorArgs);
        // Everything else
      } else {
        apiError = new CodeError({ axiosErr });
      }
    } else if (axiosErr.request) { // ---------------------------------- CASE 2
      apiError = new RequestTimeoutError({ axiosErr });
    } else { // -------------------------------------------------------- CASE 3
      apiError = new CodeError({ axiosErr });
    }

    return apiError;
  }
}

/* ------------------------ ERROR TYPES ------------------------------------ */
/**
 * Invalid request
 */
class BadRequestError extends APIError {
  constructor(...args) { super(...args); this.name = 'BadRequestError'; } /* eslint-disable-line */
}

/**
 * Invalid Authentication Credentials
 */
class AuthError extends APIError {
  constructor(...args) { super(...args); this.name = 'AuthError'; } /* eslint-disable-line */
}

/**
 * Endpoint not found
 */
class NotFoundError extends APIError {
  constructor(...args) { super(...args); this.name = 'NotFoundError'; } /* eslint-disable-line */
}

/**
 * Request formatted correctly but failed
 */
class FailedRequestError extends APIError {
  constructor(...args) { super(...args); this.name = 'FailedRequestError'; } /* eslint-disable-line */

}

/**
 * Client sending too many requests
 */
class TooManyRequestsError extends APIError {
  constructor(...args) { super(...args); this.name = 'TooManyRequestsError'; } /* eslint-disable-line */

}

/**
 * Request timed out
 */
class RequestTimeoutError extends APIError {
  constructor(...args) { super(...args); this.name = 'RequestTimeoutError'; } /* eslint-disable-line */

}

/**
 * Server Error
 */
class ServerError extends APIError {
  constructor(...args) { super(...args); this.name = 'ServerError'; } /* eslint-disable-line */

}

/**
 * Code Error
 */
class CodeError extends APIError {
  constructor(...args) { super(...args); this.name = 'CodeError'; } /* eslint-disable-line */
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = {
  APIError,
  BadRequestError,
  AuthError,
  FailedRequestError,
  NotFoundError,
  TooManyRequestsError,
  RequestTimeoutError,
  ServerError,
  CodeError
};
