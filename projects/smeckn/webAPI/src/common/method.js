/**
 * @file Method Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let axios = require('axios');
let SmecknError = require('../error.js');
let assert = require('assert');


/* ------------------------------------------------------------------------- */
/**
 * Method Class
 */
class Method {

  /**
   * Create an API method from config
   * @constructs
   * @param {String} requestMethod - HTTP Verb One of: 'GET', 'PUT', 'POST', 'DELETE'
   * @param {String} urlTemplate - HTTP Path, Ex. 'account/{accountID}'
   */
  constructor({ resource = null, requestMethod = 'GET', urlTemplate = '' } = {}) {
    this.resource = resource;
    this.api = resource.api;
    this.requestMethod = requestMethod;
    this.urlTemplate = urlTemplate;
  }

  /**
   * Get method url
   * @param  {string} options.urlParams
   * @return {string} method url
   */
  getURL({ urlParams = '' } = {}) {
    let match = null;
    let regex = (/\{(\w+)\}/g);
    let urlString = this.urlTemplate;

    // Replace names in template with parameters
    while ((match = regex.exec(this.urlTemplate)) !== null) {
      let param = match[1];
      assert(param in urlParams, `Must specify ${param}`);
      urlString = urlString.replace(`{${param}}`, urlParams[param]);
    }
    return `${this.api.baseURL}${this.resource.basePath}${urlString}`;
  }

  /**
   * Update request headers
   * @param {Object} options.headers headers from caller
   * @param {String} options.authToken Auth Token Override
   * @param {String} options.accountToken Account Token Override
   * @return {Object} updated headers
   */
  updateHeaders({ headers = {}, authToken = null, accountToken = null } = {}) {
    let newHeaders = { 'Content-Type': 'application/json; charset=utf-8' };

    // Auth
    let _authToken = (authToken === null) ? this.api.authToken : authToken;
    if (_authToken !== null) { newHeaders['API-AUTH-TOKEN'] = _authToken; }

    // Account
    let _accountToken = (accountToken === null) ? this.api.accountToken : accountToken;
    if (_accountToken !== null) { newHeaders['API-ACCOUNT-TOKEN'] = _accountToken; }

    return Object.assign({}, newHeaders, headers);
  }

  /**
   * Call a method
   * @param  {object} urlParms - Data encoded in url fields Ex. {accountID: 10}
   * @param  {object} queryParams -  Data to be encoded in query string Ex. {sort : 'byHappiness'}
   * @param  {object} data - object payload of data Ex. {username : 'foo@goo.com', password: 'pAsSwOrd'}
   * @param  {object} headers - headers to add to payload Ex. {FOO_HEADER: 'FOO_VALUE'}
   * @return {Promise} Promise
   *
   * See ./error.js for errors thrown
   */
  call({
    urlParams = {},
    queryParams = null,
    data = null,
    headers = {},
    logError = null,
    authToken = null,
    accountToken = null
  } = {}) {
    let axiosConfig = {
      url: this.getURL({ urlParams: urlParams }),
      method: this.requestMethod,
      headers: this.updateHeaders({ headers: headers, authToken, accountToken }),
      timeout: this.api.requestTimeoutMS,
      httpsAgent: this.api.httpsAgent,
      withCredentials: this.api.withCredentials
    };
    // Only add data and query params if they are needed
    if (data !== null) {
      axiosConfig.data = JSON.stringify(data);
    }
    if (queryParams !== null) {
      axiosConfig.params = queryParams;
    }
    return new Promise((resolve, reject) => {
      axios.request(axiosConfig)
        .then((resp) => { resolve(resp.data); })
        .catch((err) => {
          let apiError = SmecknError.APIError.fromAxiosResponse(err);
          let _logError = (logError === null) ? this.api.logErrors : logError;
          if (_logError) {
            console.log(apiError);
            console.log(apiError.serverStack);
          }
          reject(apiError);
        });
    });
  } /* End call */
} /* End Method */


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = Method;
