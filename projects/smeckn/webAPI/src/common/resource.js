/**
 * @file Resource Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Method = require('./method');


/* ------------------------------------------------------------------------- */
/**
 * Resource Class
 */
class Resource {

  /**
   * @constructor
   *
   * @param {API} api
   * @param {string} basePath - base path for resource. Ex. '/account'
   */
  constructor({ api = null, basePath = '' }) {
    this.api = api;
    this.basePath = basePath;
  }

  /**
   * Register a method on the resource
   * @param  {string} options.name        Method Name. Ex. 'create'
   * @param  {Object} options.methodArgs  Arguments to initialize method with (excluding resource)
   *                                      Ex. { requestMethod: 'POST', urlTemplate: ''}
   */
  registerMethod({ name = '', methodArgs = {} } = {}) {
    let methodInstance = `${name}Method`;
    let methodCall = name;
    this[methodInstance] = new Method({ resource: this, ...methodArgs });
    this[methodCall] = (...args) => { return this[methodInstance].call(...args); };
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = Resource;
