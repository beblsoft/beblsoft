/**
 * @file Smeckn API Build
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let rimraf = require('rimraf');
let path = require('path');
let webpack = require('webpack');
let wpConfigs = require('./webpack.config.js');
let wpConfigKeys = Object.keys(wpConfigs);


/* ------------------------ GLOBALS ---------------------------------------- */
let distDir = path.resolve(__dirname, '..', 'dist');
let distGlob = `${distDir}/*`;


/* ------------------------ MAIN ------------------------------------------- */
rimraf.sync(distGlob);
wpConfigKeys.forEach((key) => {
  webpack(wpConfigs[key], function (error, stats) {
    if (error) { throw error; }
    process.stdout.write(
      `${stats.toString({ colors: true, modules: false, children: false, chunks: false, chunkModules: false })}\n\n`);
  });
});
