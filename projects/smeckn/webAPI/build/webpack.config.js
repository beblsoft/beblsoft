/**
 * @file Webpack Build Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('idempotent-babel-polyfill');
let webpack = require('webpack');
let path = require('path');
let distDir = path.resolve(__dirname, '..', 'dist');
let indexFile = path.resolve(__dirname, '..', 'index.js');


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Generate a config
 * @param {string} name build name
 * @param {string} mode
 * @return {WebpackConfigObject}
 */
function generateConfig({ name = 'SmecknAPI', mode = 'production' } = {}) {
  let config = {
    entry: ['idempotent-babel-polyfill', indexFile],
    mode: mode,
    output: {
      path: `${distDir}`,
      filename: `${name}.js`,
      sourceMapFilename: `${name}.map`,
      library: name,
      libraryTarget: 'umd'
    },
    watch: (process.env.WATCH !== undefined),
    watchOptions: {
      poll: 1000,
      ignored: /node_modules/
    },
    devtool: 'source-map',
    module: {
      rules: [{
          enforce: 'pre',
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
        },
        {
          test: /\.m?js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            }
          }
        }
      ],
    },
    node: {
      process: false
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }),
    ]
  };

  return config;
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = {
  config: generateConfig(),
};
