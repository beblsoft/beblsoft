#!/usr/bin/env python
"""
 NAME:
  cli.py

DESCRIPTION
 Smeckn Web API CLI
"""


# ----------------------- IMPORTS ------------------------------------------- #
import os
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.bebl.python.run.bRun import run
from base.bebl.python.error.bError import BeblsoftError
from smeckn.common.secret import SecretType
from smeckn.webAPI.gc import gc
from smeckn.common.constant import Constant


# ----------------------- GLOBALS ------------------------------------------- #
logger         = logging.getLogger(__name__)
const          = Constant()
curDir         = os.path.dirname(os.path.realpath(__file__))
defaultLogFile = os.path.join(curDir, "cli.log")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--secret", "-s", type=click.Choice(st.name for st in list(SecretType)), default="Test",
              help="Specify secret type. Default=Test")
@click.option("--logfile", default=defaultLogFile, type=click.Path(writable=True),
              help="Specify log file. Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(secret, logfile, appendlog, verbose):
    """
    Smeckn Web API Command Line Interface
    """
    gc.webAPI.cfg.bSecret = SecretType[secret].getBSecret()
    gc.bLog               = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- BUILD --------------------------------------------- #
@cli.command()
def build():
    """
    Build Web API
    """
    run(cmd=["npm run build"], cwd=gc.webAPI.cfg.const.dirs.webAPI, shell=True, raiseOnStatus=True,
        bufferedLogFunc=logger.info)


# ----------------------- ESLINT -------------------------------------------- #
@cli.command()
def eslint():
    """
    Eslint
    """
    cmd = "npx eslint --ext .js ."
    (_, output) = run(cmd=[cmd], cwd=gc.webAPI.cfg.const.dirs.webAPI, shell=True,
                      raiseOnStatus=True, bufferedLogFunc=logger.info)
    if output.splitlines():
        raise BeblsoftError(output=output)


# ----------------------- TEST ---------------------------------------------- #
@cli.command()
@click.option("--apidomain", default="https://localhost:5100",
              help="API domain. Note: when running locally, set allowunauthorized or requests hang")
@click.option("--timeoutms", default=None, help="Test timeout in milliseconds")
@click.option("--totaljobs", default=1, help="Total number of jobs running in parallel")
@click.option("--jobidx", default=0, help="Job index of total jobs running in parallel [0:totaljobs-1]")
@click.option("--allowunauthorized", is_flag=True, default=False, help="Allow unauthorized server certificates (useful for localhost)")
def test(apidomain, timeoutms, totaljobs, jobidx, allowunauthorized):
    """
    Test Web API
    """
    cmd = """node test/main.js                                                 \
            --base-domain='{}' --admin-auth-token='{}' {}                      \
            --recaptcha-passthrough-token='{}' --total-jobs={} --job-idx={} {} \
          """.format(apidomain,
                     gc.webAPI.cfg.bSecret.DBP_ADMIN_AUTH_TOKEN,
                     "--timeout={}".format(timeoutms) if timeoutms is not None else "",
                     gc.webAPI.cfg.bSecret.RECAPTCHA_PASSTHROUGH_TOKEN,
                     totaljobs,
                     jobidx,
                     "--allowUnauthorized" if allowunauthorized else "")
    (_, output) = run(cmd=[cmd], cwd=gc.webAPI.cfg.const.dirs.webAPI, shell=True,
                      raiseOnStatus=True, bufferedLogFunc=logger.info)
    for line in output.splitlines():
        if "failing" in line:
            raise BeblsoftError(msg="Test Failed output={}".format(output))


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as e:  # pylint: disable=W0703
        # Raise so exit code is set
        raise e
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
