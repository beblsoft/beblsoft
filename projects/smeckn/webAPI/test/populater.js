/**
 * @file API Test Populater
 */


/* ------------------------------------------------------------------------- */
/**
 * Populater Class
 */
class Populater {
  /**
   * @constructor
   * @param {API} api - Assumes Admin Auth Token has already been set
   */
  constructor({
    api = null,
    adminAuthToken = '',
  } = {}) {
    this.api = api;
    this.api.authToken = adminAuthToken;
    this.waConstants = null;
    this.waFake = null;
  }

  /* ------------------------ INIT ----------------------------------------- */
  /**
   * Initialize Data
   * @param  {boolean} options.waitPlatformOnline  If true, wait for platform to come online
   * @param  {boolean} options.loadConstants       If true, laod server constants
   * @param  {object}  options.fakeDeleteInputData Data to send to api.fake.delete
   * @param  {object}  options.fakeCreateInputData Data to send to api.fake.create
   * @param  {boolean} options.initAccountToken    If true, initialize account token based on fake data
   */
  async init({
    waitPlatformOnline = false,
    loadConstants = false,
    fakeDeleteInputData = null,
    fakeCreateInputData = null,
    initAccountToken = false
  } = {}) {

    // Wait for the platform to come online
    if (waitPlatformOnline) {
      await this.api.platform.waitOnline();
    }

    // Load constants
    if (loadConstants) {
      this.waConstants = await this.api.constant.list();
    }

    // Delete fake data
    if (fakeDeleteInputData) {
      await this.api.fake.delete({ queryParams: fakeDeleteInputData });
    }

    // Create fake data, will delete existing
    if (fakeCreateInputData) {
      this.waFake = await this.api.fake.create({ data: fakeCreateInputData });
    }

    // Initialize account token
    if (initAccountToken) {
      this.api.accountToken = this.waFake.popAccountToken;
    }
  }

  /* ------------------------ GET TEST EMAIL ------------------------------- */
  /**
   * Emails are Namespaced for the particular test
   * @param  {Object} test mocha test object
   * @return {String} Namespaced test email
   */
  getTestEmail({ test } = {}) {
    let fullTitleAlphaNumeric = test.fullTitle().replace(/[\W]+/g, '_');
    let jobIdx = process.env.JOB_IDX;
    let email = `success+webClientTest-${jobIdx}-${fullTitleAlphaNumeric}@simulator.amazonses.com`;
    return email;
  }

  /* ------------------------ GET TEST PASSWORD ---------------------------- */
  /**
   * @return {String} Password
   */
  getTestPassword() {
    return process.env.TEST_PASSWORD;
  }


} /* End Populater */


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = Populater;
