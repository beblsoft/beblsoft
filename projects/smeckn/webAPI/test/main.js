/**
 * @file Test Functionality
 *
 * Description
 *   Program looks for and executes all webAPI/src/*.spec.js files.
 *
 * Atomic Spec Files
 *   Each spec.js file must be run as an atomic unit.
 *   i.e. `it` statements in a single spec.js file depend on each other.
 *   This is contrary to our python tests where each "test_*" function can be
 *   run independently
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let program = require('commander');
let path = require('path');
let API = require('../src/api');
let Populater = require('./populater');
let Mocha = require('mocha');
let assert = require('assert');
let glob = require('glob');
let njstrace = require('njstrace').inject(); // Must be last after imports


/* ------------------------ PARSE TEST OPTIONS ----------------------------- */
program
  .version('1.0.0')
  .option('-b, --base-domain <baseDomain>', 'Base Domain')
  .option('--api-version [apiVersion]', 'API Version', 'v1')
  .option('--admin-auth-token <token>', 'Admin Auth Token')
  .option('--recaptcha-passthrough-token <token>', 'Recaptcha Passthrough Token')
  .option('--test-password [password]', 'Test Password', '983427uksdfjhasaw')
  .option('--timeout [timeout]', 'Test Timeout in Milliseconds', 120000)
  .option('--allowUnauthorized', 'Allow Unauthorized Server Certificate')
  .option('--total-jobs [nJobs]', 'Total number of jobs running in parallel', 1)
  .option('--job-idx [idx]', 'This job\'s index [0:totalJobs-1]', 0)
  .parse(process.argv);

assert(program.baseDomain, 'Specify base domain');
assert(program.adminAuthToken, 'Specify admin auth token');
assert(program.recaptchaPassthroughToken, 'Specify ReCAPTCHA PassThrough Token');


/* ------------------------ EXPORTS ---------------------------------------- */
let api = new API({
  baseDomain: program.baseDomain,
  version: program.apiVersion,
  requestTimeoutMS: program.timeout,
  authToken: program.adminAuthToken,
  rejectUnauthorized: Boolean(!program.allowUnauthorized)
});
let populater = new Populater({
  api: api,
  adminAuthToken: program.adminAuthToken,
});
process.env.RECAPTCHA_PASSTHROUGH_TOKEN = program.recaptchaPassthroughToken;
process.env.TEST_PASSWORD = program.testPassword;
process.env.JOB_IDX = program.jobIdx;
module.exports = {
  api,
  populater
};


/* ------------------------ MAIN ------------------------------------------- */
// Find all test files, and then filter them for this particular job
let totalTestFiles = glob.sync('**/*.*spec.js', {
  cwd: path.resolve(__dirname, '..', 'src'),
  absolute: true
});
let jobTestFiles = totalTestFiles.filter((value, index, array) => {
  return ((index - program.jobIdx) % program.totalJobs) === 0;
});

// Execute Mocha
let m = new Mocha({ timeout: program.timeout });
jobTestFiles.forEach((file) => { m.addFile(file); });
m.run();
