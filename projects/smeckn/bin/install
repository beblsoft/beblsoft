#!/bin/bash
#
# File:
#  install
#
# Description
#  Install Command Line Interface


###############################################################################
#                              IMPORTS                                        #
###############################################################################
. ${TOOLS}/bash/lib.sh
. ${TOOLS}/bash/python.sh
. ${TOOLS}/bash/nodejs.sh
. ${TOOLS}/bash/docker.sh
. ${TOOLS}/bash/dockerCompose.sh
. ${TOOLS}/bash/mysql.sh
. ${TOOLS}/bash/aws.sh


###############################################################################
#                              MAIN                                           #
###############################################################################
[ -z $1 ] && echo "install <target>" && exit 1;
TARGET=$1

${SMECKN}/bin/permission;

case $TARGET in

  machine_python)  printf -- "\n---- Installing Machine Python ------------- \n"
                   python3_ubuntu_install;;

  machine_nodejs)  printf -- "\n---- Installing Machine Node JS ------------ \n"
                   nodejs_install;
                   npm_install;;

  machine_docker)  printf -- "\n---- Installing Machine Docker ------------- \n"
                   docker_install;
                   dockerCompose_install;;

  machine_mysql)   printf -- "\n---- Installing Machine MySQL -------------- \n"
                   mysqlClient_install;
                   mysqlServer_install;
                   mysql_createUser $(whoami);;

  machine_aws)     printf -- "\n---- Installing Machine AWS ---------------- \n"
                   aws_install;
                   aws_configure;;

  server)          printf -- "\n---- Installing Server --------------------- \n"
                   pushd ${SMECKN}/server > /dev/null;
                   python3_setupVenv
                   popd > /dev/null;;

  webAPI)          printf -- "\n---- Installing WebAPI --------------------- \n"
                   pushd ${SMECKN}/webAPI > /dev/null;
                   python3_setupVenv
                   npm install
                   sudo npm link
                   popd > /dev/null;;

  webClient)       printf -- "\n---- Installing WebClient ------------------ \n"
                   pushd ${SMECKN}/webClient > /dev/null;
                   sudo apt-get install -y build-essential # NPM Fibers
                   python3_setupVenv
                   npm install
                   # Cypress Hack: Prevents Verification from Timing Out
                   # https://github.com/cypress-io/cypress/issues/4624
                   sed -i 's/smokeTestTimeout: 10000/smokeTestTimeout: 60000/' node_modules/cypress/lib/tasks/verify.js
                   npm link smeckn-web-api
                   popd > /dev/null;;

  email)           printf -- "\n---- Installing Email ---------------------- \n"
                   pushd ${SMECKN}/email > /dev/null;
                   python3_setupVenv
                   npm install
                   popd > /dev/null;;

  aws)             printf -- "\n---- Installing AWS ------------------------ \n"
                   pushd ${SMECKN}/aws > /dev/null;
                   python3_setupVenv
                   popd > /dev/null;;

  docs)            printf -- "\n---- Installing Docs ----------------------- \n"
                   pushd ${SMECKN}/docs > /dev/null;
                   npm install
                   popd > /dev/null;;

  licenseFinder)   printf -- "\n---- Installing License Finder ------------- \n"
                   pushd ${SMECKN}/licenseFinder > /dev/null;
                   sudo gem install license_finder
                   python3_setupVenv
                   popd > /dev/null;;

  machine)         ${SMECKN}/bin/install machine_python
                   ${SMECKN}/bin/install machine_nodejs
                   ${SMECKN}/bin/install machine_docker
                   ${SMECKN}/bin/install machine_mysql
                   ${SMECKN}/bin/install machine_aws;;

  all)             ${SMECKN}/bin/install server
                   ${SMECKN}/bin/install webAPI
                   ${SMECKN}/bin/install webClient
                   ${SMECKN}/bin/install aws;
                   ${SMECKN}/bin/install docs;
                   ${SMECKN}/bin/install licenseFinder;;
esac
