#!/bin/bash
#
# File:
#  uninstall
#
# Description
#  Uninstall Command Line Interface


###############################################################################
#                              IMPORTS                                        #
###############################################################################
. ${TOOLS}/bash/lib.sh
. ${TOOLS}/bash/mysql.sh


###############################################################################
#                              MAIN                                           #
###############################################################################
[ -z $1 ] && echo "uninstall <target>" && exit 1;

TARGET=$1

case $TARGET in
  server)          printf -- "\n---- Uninstalling Server ------------------- \n"
                   pushd ${SMECKN}/server > /dev/null
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  webAPI)          printf -- "\n---- Uninstalling WebAPI ------------------- \n"
                   pushd ${SMECKN}/webAPI > /dev/null;
                   [ -d node_modules ] && rm -rf node_modules
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  webClient)       printf -- "\n---- Uninstalling WebClient ---------------- \n"
                   pushd ${SMECKN}/webClient > /dev/null;
                   [ -d node_modules ] && rm -rf node_modules
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  email)           printf -- "\n---- Uninstalling Email -------------------- \n"
                   pushd ${SMECKN}/email > /dev/null;
                   [ -d node_modules ] && rm -rf node_modules
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  aws)             printf -- "\n---- Uninstalling AWS ---------------------- \n"
                   pushd ${SMECKN}/aws > /dev/null
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  docs)            printf -- "\n---- Uninstalling Docs --------------------- \n"
                   pushd ${SMECKN}/docs > /dev/null
                   [ -d node_modules ] && rm -rf node_modules
                   popd > /dev/null;;

  licenseFinder)   printf -- "\n---- Uninstalling License Finder ----------- \n"
                   pushd ${SMECKN}/licenseFinder > /dev/null
                   sudo gem uninstall --executables license_finder
                   [ -d venv ] && rm -rf venv
                   popd > /dev/null;;

  all)             ${SMECKN}/bin/uninstall server
                   ${SMECKN}/bin/uninstall webAPI
                   ${SMECKN}/bin/uninstall webClient
                   ${SMECKN}/bin/uninstall aws
                   ${SMECKN}/bin/uninstall docs
                   ${SMECKN}/bin/uninstall licenseFinder;;
esac
