#!/bin/bash
#
# File:
#  compose
#
# Description
#  Compose Command Line Interface


###############################################################################
#                              IMPORTS                                        #
###############################################################################
. ${TOOLS}/bash/lib.sh


###############################################################################
#                              UP                                             #
###############################################################################
function up() {
  pushd $SMECKN;
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline create --gdb --dbpadmin --dbpaccounts
  docker-compose up
  popd;
}

function up_server() {
  pushd $SMECKN;
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline create --gdb --dbpadmin --dbpaccounts
  docker-compose run --service-ports --use-aliases server
  popd;
}


###############################################################################
#                              TEST                                           #
###############################################################################
function test_server() {
  pushd $SMECKN;
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline test --module=ALL
  docker-compose down --volumes
  popd;
}

function test_webAPI() {
  pushd $SMECKN;
  local API_DOMAIN=${1:-"https://server:5100"}
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline create --gdb --dbpadmin --dbpaccounts
  docker-compose run webAPI test --apidomain=${API_DOMAIN} --allowunauthorized
  docker-compose down --volumes
  popd;
}

function test_webClient() {
  pushd $SMECKN;
  local CLIENT_DOMAIN=${1:-"https://localhost:8021"}
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline create --gdb --dbpadmin --dbpaccounts
  docker-compose up -d
  ./bin/webClient test --clientdomain=${CLIENT_DOMAIN} --console
  docker-compose down --volumes
  popd;
}

function test_webClientHeadless() {
  pushd $SMECKN;
  local CLIENT_DOMAIN=${1:-"https://localhost:8021"}
  docker-compose down --volumes
  docker-compose run server --gdbswaitonline create --gdb --dbpadmin --dbpaccounts
  docker-compose run webClientCypress test --clientdomain=${CLIENT_DOMAIN}
  docker-compose down --volumes
  popd;
}


###############################################################################
#                              MAIN                                           #
###############################################################################
[ -z $1 ] && echo "compose <command> ..." && exit 1;
TARGET=$1
shift # Removes $1

case $TARGET in
  up)                      up $@;;
  up_server)               up_server $@;;
  test_server)             test_server $@;;
  test_webAPI)             test_webAPI $@;;
  test_webClient)          test_webClient $@;;
  test_webClientHeadless)  test_webClientHeadless $@;;
esac
