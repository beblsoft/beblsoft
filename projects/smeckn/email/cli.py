#!/usr/bin/env python
"""
 NAME:
  cli.py

DESCRIPTION
 Smeckn Email CLI
"""


# ----------------------- IMPORTS ------------------------------------------- #
import os
import logging
from datetime import datetime
import click
from base.bebl.python.run.bRun import run
from base.bebl.python.log.bLog import BeblsoftLog
from smeckn.email.gc import gc
from smeckn.common.constant import Constant


# ----------------------- GLOBALS ------------------------------------------- #
logger         = logging.getLogger(__name__)
const          = Constant()
curDir         = os.path.dirname(os.path.realpath(__file__))
defaultLogFile = os.path.join(curDir, "cli.log")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default=defaultLogFile, type=click.Path(writable=True),
              help="Specify log file. Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Smeckn Email Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- CLEAN --------------------------------------------- #
@cli.command()
def clean():
    """
    Clean
    """
    cmd = "rm *"
    run(cmd=[cmd], cwd=gc.email.cfg.const.dirs.emailHTML, shell=True,
        raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- GENERATE ------------------------------------------ #
@cli.command()
@click.option("--validationlevel", default="strict", type=click.Choice(["strict", "soft", "skip"]),
              help="Specify validation level")
@click.option("--watch", is_flag=True, default=False, help="If True, watch")
def generate(validationlevel, watch):
    """
    Generate HTML files from MJML
    """
    cmd = "npx mjml --config.validationLevel={} {} ./mjml/* -o ./html".format(
        validationlevel,
        "-w" if watch else "")
    run(cmd=[cmd], cwd=gc.email.cfg.const.dirs.email, shell=True,
        raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- VALIDATE ------------------------------------------ #
@cli.command()
def validate():
    """
    Generate MJML files
    """
    cmd = "npx mjml --validate ./mjml/*"
    run(cmd=[cmd], cwd=gc.email.cfg.const.dirs.email, shell=True,
        raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as e:  # pylint: disable=W0703
        # Raise so exit code is set
        raise e
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
