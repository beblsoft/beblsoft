/**
 * @file Vue Press Configuration
 */

module.exports = {
  title: 'Smeckn Developer Docs',
  description: 'Internal Use Only',
  head: [
    ['link', { rel: 'icon', href: '/img/logo.png' }]
  ],
  port: 8099,
  base: '',
  host: 'localhost',
  dest: 'dist/',
  themeConfig: {
    logo: '/img/logo.png',
    lastUpdated: 'Last Updated', // string | boolean
    displayAllHeaders: true, // Default: false

    // Navbar
    nav: [
      { text: 'Intro', link: '/intro/GettingStarted' },
      { text: 'Components', link: '/components/Databases' },
      { text: 'Topics', link: '/topics/Facebook' },
      { text: 'Deployment', link: '/deployment/AWS' }
    ],
    // Sidebar
    sidebar: {
      '/intro/': [
        'GettingStarted',
        'Basics',
        'Docker',
      ],
      '/components/': [
        'Databases',
        'Server',
        'WebAPI',
        'WebClient',
        'UserGuide',
      ],
      '/topics/': [
        'Facebook',
        'Email',
        'LicenseFinder',
        'ThirdParty',
        'Security',
      ],
      '/deployment/': [
        'AWS',
        'Monitoring',
      ],
      // fallback, should be last
      '/': []
    }
  },
  markdown: {
    lineNumbers: false
  }
};
