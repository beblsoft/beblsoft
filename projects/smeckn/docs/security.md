# Security

Security is covered here as it spans the full stack. Most of Smeckn's security is implemented
inside the server.

## Types of Users

Smeckn supports 3 types of users:

1. __REGULAR__: This is an average Joe user who simply creates an account.

2. __ADMIN__: This user type has enhanced API privileges. It is used in CI/Deployment pipeline to
   create and delete TEST users.

   The ADMIN user can also make requests on behalf of any REGULAR user. Therefore, it we must protect
   ADMIN tokens from falling into the wrong hands.

3. __TEST__: This user type is used by the tests. TEST users are frequently created and then deleted.

The user enumeration can be found in `$SMECKN/server/gDB/account/model.py`.

## API Tokens

Most server endpoints require two tokens:

1. __Authentication__: This token encodes the bearer's Account ID and whether the bearer has
   ADMIN privileges or not.

   Header Name: `API-AUTH-TOKEN`

   Cookie Name: `__smeckn_auth_token`

2. __Account__: This token encodes the bearer's Account ID and Account database location.

   Header Name: `API-ACCOUNT-TOKEN`

   Cookie Name: `__smeckn_account_token`

In order for a request to access a protected account endpoint, it must contain the proper
__Authentication__ and __Account__ tokens.

## API Auth Endpoints

### `/auth/login`

This endpoint is the gateway to authentication.

It takes a `email`, `password`, and `reCAPTCHAToken`.

If the credentials are successful, the server sets `__smeckn_auth_token` and `__smeckn_admin_token`
cookies in the browser.

Note: These cookies are `secure`, `httpOnly`, and `samesite='Strict'` for maximum security. This
means that no thirdparty JavaScript running inside the browser can see or interact with the cookies.

### `/auth/status`

Since the browser cannot dectect if the authentiation cookies exist. It posts to `/auth/status`
to determine the login state.

The browser sends the cookies to `/auth/status` where the server can see them.

The server returns `status:'LOGGED_IN'` or `status:'LOGGED_OUT'`.

### `/auth/logout`

When the user logs out of the application, the app posts to `/auth/logout`.

The server recieves the post and clears the `__smeckn_auth_token` and `__smeckn_admin_token`
browser cookies.

## CORS

The Smeckn server implements the
[Cross Origin Resource Sharing](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) browser APIs
which prohibits content served from locations other than `*.smeckn.com` from accessing the smeckn API.

This is implemented using [Flask-CORS](https://flask-cors.corydolphin.com/en/2.1.0/)
in `$SMECKN/server/manager/application.py`.

## WebAPI

In order to cooperate with the CORS API. The webClient must tag all requests with the
`withCredentials=true` flag. This forwards the relevant cookies to the smeckn API.

See `$SMECKN/webAPI/src/common/method.js` for more details.

## WebClient

The webClient leverages the server API endpoints described above to securely store the API cookies
in the browser.

This allows for persistent login sessions. On arriving to `smeckn.com`, the banner page will post
to `/auth/status` to determine if the user is already logged in. If the user is logged in, the
application bypasses the login screen, and takes the user directly to the logged in home page.

