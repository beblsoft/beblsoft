# User Guide

The Smeckn UserGuide is a static website with helpful user content. Smeckn uses
[VuePress](https://vuepress.vuejs.org/) to format its User Guide. This code is located in
`$SMECKN/webClient/userGuide`.

The UserGuide is deployed with the webclient at `smeckn.com/userGuide/index.html`.

## File Tree

```bash
$ cd $SMECKN/webClient/userGuide;
$ tree -L 2 -a
.
├── blog
│   ├── BuildYourBrand-blank-branding-identity-business-cards-6372.jpg
│   ├── BuildYourBrand.md
│   ├── HelpOthers-black-and-white-close-up-dark-167964.jpg
│   ├── HelpOthers.md
│   ├── SocialMedia.md
│   ├── ouUp.md
│   ├── UseSMToLiftYouUp-youth-active-jump-happy-40815.jpeg
│   └── WhatIsSmeckn-asphalt-automobile-blur-2705754.jpg
├── company
│   ├── About.md
│   ├── Contact.md
│   ├── Jobs.md
│   ├── Legal.md
│   └── Press.md
├── overview
│   ├── AccountManagement.md
│   ├── Facebook.md
│   ├── Feedback.md
│   ├── GettingStarted.md
│   └── WhatIsSmeckn.md
├── README.md
└── .vuepress
    ├── config.js
    ├── override.styl
    ├── public
    └── style.styl
```

## Command Reference

| Description             | Command                                                                              |
|:------------------------|:-------------------------------------------------------------------------------------|
| Start user guide server | `cd $SMECKN; docker-compose run --service-ports webClientUserGuide start-user-guide` |

