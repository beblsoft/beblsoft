# Databases

The database layer persists application state.

![DBs](./SmecknDBs.jpg)

## Global Database

The Global Database holds all Global Smeckn State. Most importantly, it holds the mappings between
Account ID and Account Database Server.

The relevant global database directories are as follows:
```bash
cd $SMECKN/server;
tree -L 2;
├── gDB                    : SQLAlchemy Models. Each subdirectory is its own table
│   ├── account
│   ├── accountDBServer
│   ├── gJob
├── gDBMigrations          : Global Database Migrations
    ├── alembic.ini
    ├── env.py
    ├── migration_test.py
    ├── script.py.mako
    └── versions           : Directory holding database migration scripts
```

Below is a snapshot of the global database from [DB Diagram](https://dbdiagram.io/d): ![GDB](./SmecknGDB.jpg)

## Account Database

As its name implies an Account Database holds all account information for an individual user.
When a user creates an account a new Account Database is allocated on one of the Account
Database Servers.

The relevant account database directories are as follows:
```bash
cd $SMECKN/server;
tree -L 2;
├── aDB                    : SQLAlchemy models. Each subdirectory is its own table.
│   ├── accountInfo
│   ├── aJob
│   ├── analysis
│   ├── compLang
│   ├── compLangAnalysis
│   ├── compSentAnalysis
|    ...
│   ├── sort
│   ├── statistic
│   ├── sync
│   ├── syncContent
│   ├── text
│   ├── unitLedger
│   └── video
├── aDBMigrations          : Account Database Migrations
    ├── alembic.ini
    ├── env.py
    ├── migration_test.py
    ├── __pycache__
    ├── script.py.mako
    └── versions           : Directory holding database migration scripts
```

To create a new aDB revision:
- Change code: Make desired changes to sqlalchemy models
- Consolidate revision scripts: If revisions haven't been pushed to AWS yet, consolidate old.
  `rm -rf $SMECKN/server/aDBMigrations/versions/<migration script>`
- Autogenerate revision scripts: Create new revision script that reconciles the difference between
  the current sqlalchemy models and the underlying db tables.
  `docker-compose run server revise --ardb --ardbmessage=Sample_Revision`

Upgrading AWS ADBs:
- Deploy MLS: `docker-compose run aws --env Staging deploy --mls`
- Upgrade all adbs: `docker-compose run aws --env Staging upgrade --adb --adbversion=1s239082asd3423`

Downgrading AWS ADBs:
- Be careful!! : Should avoid downgrading databases unless absolutely necessary.
  Ensure that SLS is not using any fields.
- Deploy MLS: `docker-compose run aws --env Staging deploy --mls`
- Downgrade adb: `docker-compose run aws --env Staging downgrade --adb --adbversion=1s239082asd3423`
- Delete stale revision file: `rm $SMECKN/server/aDBMigrations/versions/234238_23498_Asda.py`

Lock Hierarchy:
- Account(Highest)
- ProfileGroup
- Profile
- FacebookProfile
- Sync
- Analysis
- UnitLedger(Lowest)
- I.e. Account Table cannot be locked after UnitLedger Table

Below is a snapshot of the account database from [DB Diagram](https://dbdiagram.io/d): ![ADB](./SmecknADB.jpg)

## Command reference

| Description             | Command                                                                            |
|:------------------------|:-----------------------------------------------------------------------------------|
| DB: Create and populate | `cd $SMECKN; docker-compose run server create --gdb --dbpadmin --dbpaccounts`      |
| DB: Delete              | `cd $SMECKN; docker-compose run server delete --gdb`                               |
| DB: Describe            | `cd $SMECKN; docker-compose run server describe --gdb --dbpadmin --dbpaccounts`    |
| GDB: Revise             | `cd $SMECKN; docker-compose run server revise --grdb --grdbmessage=Sample_Message` |
| GDB: Upgrade            | `cd $SMECKN; docker-compose run server upgrade --gdb --grdbversion=head`           |
| GDB: Downgrade          | `cd $SMECKN; docker-compose run server downgrade --gdb --grdbversion=base`         |
| ADB: Revise             | `cd $SMECKN; docker-compose run server revise --ardb --ardbmessage=Sample_Message` |