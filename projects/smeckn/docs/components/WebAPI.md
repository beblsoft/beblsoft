# Web API

## Introduction

The WebAPI is JavaScript interface layer allowing JavaScript clients to access the Server's REST API.

## File Tree

```bash
cd $SMECKN/webAPI;
tree -L 2 -I node_modules;
├── cli.py                    : Main python CLI
├── cli.log
├── gc.py
├── index.js
├── package.json              : Defines all package dependencies
├── package-lock.json
├── Dockerfile
├── requirements.txt
├── build                     : Build configuration files
│   ├── build.js
│   └── webpack.config.js
├── dist                      : UMD build output
│   ├── SmecknAPI.js
│   └── SmecknAPI.map
├── src
│   ├── api.js                : Global API definition file, exported to Web Client
│   ├── common
│   ├── error.js
│   └── resources             : All resource routes to the server are listed here
├── test
│   ├── main.js               : Test Main
│   └── populater.js
```

## Building

The webAPI is built as a UMD module as follows:

| Description        | Command                                       |
|:-------------------|:----------------------------------------------|
| Application: Build | `cd $SMECKN; docker-compose run webAPI build` |

## Testing

The webAPI is tested as follows:

| Description        | Command                                                                                                       |
|:-------------------|:--------------------------------------------------------------------------------------------------------------|
| Test: Local server | `cd $SMECKN; docker-compose run webAPI test --apidomain=https://server:5100`                                  |
| Test: AWS server   | `cd $SMECKN; docker-compose run webAPI --secret Nightly test --apidomain=https://nightly-api.test.smeckn.com` |