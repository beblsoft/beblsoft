# Web Client

## Application

The Web Client [Vue CLI](https://cli.vuejs.org/) application is the customer-visible web site.

### File Tree

The application file tree is shown below. `cli.py` is the main entry point for all commands.

```bash
cd $SMECKN/webClient;
tree -L 2 -I node_modules;
.
├── babel.config.js
├── cli.log
├── cli.py                  : CLI Entrypoint
├── cypress                 : Cypress implementation directory
├── dist                    : Distribution directory
├── Dockerfile              : Dockerfile defining dependencies
├── gc.py
├── package.json
├── package-lock.json
├── public                  : Public assets
|   ├── index.html          : index.html file that loads when Smeckn loads
|   └── img                 : Public images. These are copied as-is into build output
|                             Useful for any assets needed in public index.html
├── src
│   ├── appComponents       : Application Components
│   ├── commonComponents    : Common Components
│   ├── img
│   ├── router              : Vue Router
│   ├── scss                : Global scss files
│   ├── userGuide
│   ├── server              : Interface to Smeckn Server
│   ├── store               : Vuex Store
│   ├── messages            : Internationalization Support
│   ├── App.vue
│   ├── googleAnalytics.js
│   ├── main.js             : Application Entrypoint
│   ├── sentry.js
│   ├── ui.js
│   ├── chartjs.js
│   └── utils.js
├── userGuide               : User Guide Implementation
└── vue.config.js           : Vue CLI Configuration file
```

### Environments

The application has various environment variables depending on where it is running. Per Vue CLI standard
the environments variables are defined in `.env` files.
```bash
cd $SMECKN/webClient;
tree -L 2 -I node_modules;
.
├── .env              : Base environment file
├── .env.aws          : AWS Environment File (VUE_APP_ENVIRONMENT = Nightly, Staging, and Production)
├── .env.development  : Local Environment File (VUE_APP_ENVIRONMENT = Local)
```

### Common Components

Common components are a way of factoring out common functionality that can then be reused in many
places of the application. Common components are located under `smeckn/webClient/src/commonComponents/*`.

Most common components have their own directory with the following files:
- `Smeckn<Name>.vue`: Component Implementation
- `Smeckn<Name>Test.vue`: Test Wrapper Component. It gives an example usage of the common component.
  It is registered in the `routes.js` under `__test__`
- `Smeckn<Name>Test.spec.js`: Cypress test harness

As an example see `smeckn/webClient/src/commonComponents/contentSection`

### Naming Conventions

| Rule                                                             | Example          |
|:-----------------------------------------------------------------|:-----------------|
| Route Names are prefixed with `rn`                               | `rnBanner`       |
| Web API Data Variables are prefixed with `wa`                    | `waProfileGroup` |
| Methods called by events are prefixed with 'on'.                 | `onLogin`        |
| Local Smeckn CSS classes are prefixed with `sm`                  | `sm-text`        |
| Global Smeckn SCSS classes and variables are prefided with `smg` | `smg-text`       |

### Command Reference

| Description               | Command                                                                                                          |
|:--------------------------|:-----------------------------------------------------------------------------------------------------------------|
| Start app on local server | `cd $SMECKN; docker-compose run --service-ports webClient start --apidomain=https://localhost:5100`              |
| Start app on AWS server   | `cd $SMECKN; docker-compose run --service-ports webClient start --apidomain=https://nightly-api.test.smeckn.com` |
| Start app opening browser | `cd $SMECKN; docker-compose run --service-ports webClient start --openbrowser`                                   |

### Debugging

Chrome console commands:
- Show store state: `app.$g.store.state`
- If in Cypress: `cy.win((win) => { cy.log(win.app.$g); });`

## HTML and CSS

The Web Client leverages
[Sass](https://sass-lang.com/),
[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/),
and [Bootstrap-Vue](https://bootstrap-vue.js.org/) to perform layout and styling.

### DO's

- __DO follow the `beblsoft/templates/vue.vue` for template ordering__

- __DO use BootstrapVue Components__

  Use [Bootstrap Vue Components](https://bootstrap-vue.js.org/docs/components/alert) for markup.

  ```html
  <b-container class="bv-example-row">
    <b-row>
      <b-col>1 of 3</b-col>
      <b-col>2 of 3</b-col>
      <b-col>3 of 3</b-col>
    </b-row>
  </b-container>
  ```

- __DO use Bootstrap utility classes over BootstrapVue props__

  Where possible, use [Bootstrap's Utility Classes](https://getbootstrap.com/docs/4.0/utilities) to perform
  styling.

  I.e use:
  ```html
  <b-button class="btn-info mx-1" v-on:click="onCancel">Cancel</b-button>
  ```
  instead of:
  ```html
  <b-button class="mx-1" variant="info" v-on:click="onCancel">Cancel</b-button>
  ```

  Utilitly classes force consistency across the application: colors, fonts, sizes, etc.

- __DO create and use global custom utility classes__

  If bootstrap doesn't have a particular style utility. Add it to `src/scss/_smeckn.scss`. For example:
  ```scss
  @each $size, $maxWidth, $maxHeight in $smg-img-sz-list {
    .smg-img-#{$size} {
      max-width: $maxWidth;
      max-height: $maxHeight;
    }
  }
  ```
  Then use it in HTML:
  ```html
  <img src="../../img/flaticon/smile-green.png" class="smg-img-6" alt="">
  ```

- __DO keep HTML DRY__

  Try to keep HTML DRY (Don't Repeat Yourself). I.e. if a piece of HTML is being used more than
  once, create a reusable component and put it under `src/commonComponents/`

- __DO put unique component styles at end of file__

  If a style is unique to a component. Style it at the end of the file.
  For example:
  ```html
  <style scoped lang="scss">
  .sm-background-splash {
    background-image: url('../../img/adult-communication.png');
    background-repeat: no-repeat;
    background-size: cover;
  }
  </style>
  ```

- __DO use `rem` for sizing__

  `rem` is relative to root element's font-size. In `_smeckn.scss` we set the root html `font-size`
  to be responsive based on the screen width. By sizing all elements with `rem` they too will have
  responsive sizes.

### Dont's

- __DON'T use inline styles__

  Unless you absolutely need to, don't use inline styles.
  Use either utility classes, or styles at the end of the file.

- __DON'T use absolute sizing units__

  Using absoluting sizing units such as `px`, will not allow the application to be responsive.

## Asset Minification

Smaller WebClient assets leads to faster application load time and happier customers.

### Measuring

Smeckn leverages the [Webpack Bundle Analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer)
to measure bundle sizes. To enable the bundle analyzer, add the following to `vue.config.js`.

```js
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// ...

module.exports = {
  // ...
  configureWebpack: {
    plugins: [new BundleAnalyzerPlugin()],
  },
  // ...
};
```

After building the web client for production (command shown below) the following image is generated.
Hovering over each box displays the size of each asset.
![Bundler Analyzer](./WebpackBundleAnalyzer.png)

### Current Strategies

- __Lazy Load Application Components__: Lazily loading application components allows each component
  to be loaded only when it is needed. This decreases the initial app payload size.
  Lazily loading components is performed using this
  [Vue Router Recipe](https://router.vuejs.org/guide/advanced/lazy-loading.html).

  For example, the following code in `webClient/src/router/routes.js` lazily loads the `ConfirmAccount`
  component as part of the `auth` webpack chunk.
  ```js
  {
    path: '/confirmAccount',
    name: 'rnConfirmAccount',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/ConfirmAccount'),
    props: (route) => { return { createAccountToken: route.query.createAccountToken }; }
  }
  ```

- __Small Images__: Where possible, use [Gimp](https://www.gimp.org/) or another image manipulation tool
  to compress images to below 80 kB. Keep the original uncompressed image around for future use.
  For example:
  ```bash
  jbensson [09/20/2019 07:01:07 AM] HOME/git/beblsoft/projects/smeckn/webClient/src/img
  $ ls screenshots/facebookLogin*
   60 -rw-rw-r-- 1 jbensson jbensson 50109 Sep 19 15:55 screenshots/facebookLogin-compressed.jpg
  104 -rw-rw-r-- 1 jbensson jbensson 94354 Sep 19 15:55 screenshots/facebookLogin.jpg
  ```

- __Keep `smeckn/webClient/scss/indexScoped.scss` small__: This file is injected into any component
  thats uses its own custom scoped styles, i.e. has `<style scoped lang="scss">...</style>`.
  Previously, we were injecting the full Boostrap SCSS implementation into each individual scoped style block.
  This caused our CSS asset sizes to bloat and page load times to lag. Keeping `indexScoped.scss`
  small fixes this issue.

- __Remove unused third party packages__: Previously we were importing packages (Ex. `vue-good-table`
  and `vue-chartkick`) that were never being used in the application. This causes unnecessary application
  bloat.

### Future Strategies

The following strategies have been considered, but not yet implemented.

- __Remove moment locales__: Moment locales gzipped take up roughly 31KB. This
  [medium article](https://medium.com/js-dojo/how-to-reduce-your-vue-js-bundle-size-with-webpack-3145bf5019b7)
  describes how to prevent importing each locale.

- __Selective sub-imports__: A good strategy to use is to selectively import small bits of functionality
  from large packages, instead of importing the large package in its entirety. This could be done with the
  following packages:

  * Lodash: Import individual lodash functions instead of the whole lodash package.
    See this [Stackoverflow question](https://stackoverflow.com/questions/43479464/how-to-import-a-single-lodash-function)
    for more details.

## Cypress Testing

Smeckn leverages [Cypress](https://www.cypress.io/) for its end-to-end testing.

### File Tree

All of the relevant testing files are shown below. `cli.py test` invokes `main.js` which discovers all the
application test spec files and executes them.

All the test spec files are located in `$SMECKN/webClient/src/**/*.spec.js` next to their corresponding
application files.

```bash
cd $SMECKN/webCLient;
tree -I node_modules -L 2
.
├── cli.py             : Python CLI
├── requirements.txt   : Python requirements
├── cypress
│   ├── app
│   ├── common
│   ├── main.js        : Cypress tests entry point
|   |                    File searches for all the *.spec.js files in the tree and executes them
│   ├── testGlobal.js  : Global object imported by other tests
│   ├── plugins
│   ├── server
│   ├── support
│   ├── screenshots
│   └── videos
├── cypress.json       : (Empty) Cypress configuration file

```

### Test File Structure

All `*.spec.js` have the following structure:

```js
describe('Wrapper', function () {

  /**
   * Runs before each test function
   */
  beforeEach(function () {
    tg.init(); // Test Global Initialization
    tg.server.init({ // Database initialization
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login(); // Login to the application
  });

  /**
   * Sample test
   */
  it('Test 1', function () {
    // ...
  });

  /**
   * Sample test
   */
  it('Test 2', function () {
    // ...
  });
});
```

### Environment Skipping

Various tests are skipped on production or other environments.

To skip a test, use the following code:

```js
tg.init();
cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
```

The following tests are skipped on production:
- `src/payments/**`: We don't want to dilute Stripe production data with our tests

### Command Reference

To use the test client, first start the local application via: `$SMECKN/bin/compose up`.

| Description                                 | Command                                                                                                           |
|:--------------------------------------------|:------------------------------------------------------------------------------------------------------------------|
| Test opening the console                    | `$SMECKN/bin/webClient test --console`                                                                            |
| Test specific file                          | `$SMECKN/bin/webClient test --noexit --fileregex="**/FacebookPostAll.spec.js"`                                    |
| Test overwriting snapshots                  | `$SMECKN/bin/webClient test --updatesnapshots`                                                                    |
| Test over non-local client                  | `$SMECKN/bin/webClient test --clientdomain=https://nightly.test.smeckn.com`                                       |
| Test in a loop, ensuring consistent passing | `for i in $(seq 1 1 10); do $SMECKN/bin/webClient test --fileregex="**/FacebookPostAll.spec.js"; done;`           |
| Run inside docker container, mimicing CICD  | `cd $SMECKN; docker-compose run --no-deps webClientCypress test --clientdomain="https://nightly.test.smeckn.com"` |

## Production Deployment

### File Tree

The web client builds all output to the `dist/` directory. This directory is uploaded to AWS for deployment.

```bash
cd $SMECKN/webClient;
tree -L 2 -I node_modules;
.
├── dist          : Production build output, this is what is uploaded to AWS
│   ├── app       : All application code built here. Available under smeckn.com/app/index.html
│   ├── userGuide : All user guide code built here. Available under smeckn.com/userGuide/index.html
│   ├── email     : All static email content. Available under smeckn.com/email/*
```

### Command Reference

| Description                            | Command                                            |
|:---------------------------------------|:---------------------------------------------------|
| Build the web client without deploying | `$SMECKN/bin/aws --env Nightly build --webclient`  |
| Build and Deploy to production         | `$SMECKN/bin/aws --env Nightly deploy --webclient` |
