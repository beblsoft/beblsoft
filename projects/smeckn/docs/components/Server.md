# Server

## Web Application

The server is a Flask WSGI application. It exports a Swagger REST API using Flask Restplus that handles
requests and returns JSON responses.

### File Tree

```bash
cd $SMECKN/server;
tree -L 2;
├── manager
│   ├── application.py     : Flask application manager
├── wAPIV1                 : Directory containing all web API models
    ├── account
    ...
    ├── default
    ├── error
    ├── facebookPost       : Example namespace. Each directory is its own namespace
    ...
    ├── __init__.py        : Instantiates the Flask Restplus namespaces
    └── video
```

### Command reference

| Description             | Command                                                                                                      |
|:------------------------|:-------------------------------------------------------------------------------------------------------------|
| Start Flask Application | `cd $SMECKN; docker-compose run --service-ports server start --app` Open Chrome to https://localhost:5100/v1 |

## Testing

Each portion of the server is thoroughly tested. Most python functionality files `<name>.py` have
a corresponding `<name>_test.py` test file that is run during CI to verify that the functionality
works correctly. Since python is a scripted language, thorough testing is __esential__ to code
quality and maintainability.

The server leverages the following modules to perform testing:

- Python [unittest](https://docs.python.org/3.6/library/unittest.html) standard library module provides
  the framework for running each unit test.
- [vcrp](https://github.com/kevin1024/vcrpy) records API request responses to prevent tests from overusing
  APIs
- [freeezegun](https://github.com/spulec/freezegun) freezes all time modules allowing tests to run
  with consistent time settings.

The test command entrypoint is `$SMECKN/bin/server test`.

### File Tree

```bash
cd $SMECKN/server;
tree -L 2;
├── cli.py
├── commands
│   ├── test.py          : CLI Test handler
├── manager
│   └── test.py          : Code that searches for all of the relevant server tests and then executes them
├── test
    ├── auth.py
    ├── cassettes        : VCR cassette files are held here
    ├── common.py        : Common Test class subclassed by most tests
    ├── __init__.py
    ├── __pycache__
    ├── route.py
    └── vcr.py
```

### Command Reference

| Description               | Command                                                                                                       |
|:--------------------------|:--------------------------------------------------------------------------------------------------------------|
| Test All modules          | `cd $SMECKN; docker-compose run server test --module=ALL`                                                     |
| Test Specific module name | `cd $SMECKN; docker-compose run server test --name=smeckn.server.gDB.account.model_test.AccountModelTestCase` |

## Logging

Smeckn leverages [AWS CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html)
to aggregate its server logs. After the log data has been captured, Smeckn uses
[AWS CloudWatch Logs Insights](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/AnalyzingLogData.html)
to analyze and process the log data. Insights parses and aggregates JSON record data to produce metrics
and visualizations. To support developer consistency and easy of use, Smeckn utilizes several standard
APIs (described below) to consistently collect and process logs.

### Goals

Smeckn has the following goals for its logging subsystem. The logging facilities must enable:

1. __Debugging error situations__

2. __Collecting, Aggregating, and Visualizing Arbitrary Metrics__

   For example: `# Logins`, `# Failed Logins`, `# Purchases`, `# Python Exceptions`, `# Database Failures`

3. __Tracking a user across multiple requests__

### Methodology

Because CloudWatch Logs charges based on the amount of logs injested and processed, the server uses
the following heuristics when logging:

- __Normal Case__ (i.e. API request that returns 2xx, 3xx, 4xx): __Log as little as possible__ (< 1KB). Only log
  key metric data to be processed later.

- __Abnormal Case__ (i.e. API request that returns 5xx): Dump all potential context to the logs.

- __Local Development Environments__: Log at `logging.DEBUG`. This includes `@logFunc()` wrappers
  for extra debugging.

- __AWS Environments__: Log at `logging.INFO`. This prevents debugging information to dilute the logs
  and costing Smeckn lots of money.

### API: bLogger.py:BeblsoftLogger

The `BeblsoftLogger` class contains functions to log messages containing strings and JSON content. It
logs standard JSON fields but also allows callers to add additional JSON key-value pairs. By
default, it has the following format:

```json
<message> {
  "bLogCode": 'AUTH_LOGIN_SUCCESS',  // Log Code name, see bLogCode.py
  "bLogValue": 10,                   // Log code value, useful for counters, guages
  "bError": {/*...*/ },              // Beblsoft Error Fields
  /* Caller defined key-value pairs */
}
```

Example usage:
```python
BeblsoftLogger.warning(bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_EVENT_NOT_HANDLED,
                       event=self.event, context=self.context)
```

All log events should have a `bLogCode` field containing a specific `BeblsoftLogCode`
from `bLogCode.py:BeblsoftLogCode`. This allows Cloud Watch Logs Insights to
aggregate statistics on how frequently a particular log message is being displayed.

### API: sLogger.py:SmecknLogger

The `SmecknLogger` API wraps over `BeblsoftLogger` to add Smeckn specific context information.

The `log()` function dumps the following record format:
```json
<message> {
  /* bLogger Fields */
  "aID": 234234,       // Relevant Account ID
  "gJob": { /*...*/},  // Smeckn Global Job Fields
  "aJob": { /*...*/},  // Smeckn Account Job Fields
  /* Caller defined key-value pairs */
}
```

The `dumpState()` function dumps all contextual Smeckn state. This should __ONLY__ be used in exceptional cases.
It dumps the following record format:

```json
<message> {
  /* sLogger Fields */
  "eCtx": { /*...*/},   // Execution Context State
}
```

### API: requestWrapper.py:logRequestComplete()

This function logs each API request completion. It wraps over `SmecknLogger` to dump the following
record fields:

```json
<message> {
  "bLogCode": 'API_REQUEST_COMPLETE',
  /* sLogger Fields */
  "request": {                     // Request Object
    "method": "GET",               // Request Method
    "urlRule": "/v1/profileGroup"  // URL Requested
    "duration": "0:00:00.218288"   // Request Duration
    /* More fields on error */
  },
  "response": {                    // Response Object
    "statusCode": 200              // Status code
    /* More fields on error */
  },
}
```

### Business Metric Queries

The following are sample CloudWatch Logs Insights queries that generate business metrics.

- API Requests
  ```
  filter bLogCode = "API_REQUEST_COMPLETE" |
  stats count(*) as APIRequests by bin(15m)
  ```

- Failed Logins
  ```
  filter bLogCode = "API_REQUEST_COMPLETE" and request.urlRule like /auth\/login/ and response.statusCode != 200 |
  stats count(*) as FailedLogins by bin(15m)
  ```

- Successful Logins
  ```
  filter bLogCode = "AUTH_LOGIN_SUCCESS"  |
  stats count(*) as SuccessfulLogins by bin(15m)
  ```

- Successful Logouts
  ```
  filter bLogCode = "AUTH_LOGOUT_SUCCESS"  |
  stats count(*) as SuccessfulLogouts by bin(15m)
  ```

- Active Users
  ```
  stats count_distinct(aID) as ActiveUsers by bin(15m)
  ```

- Display 5xx Errors
  ```
  fields @timestamp, request.method, request.urlRule, response.statusCode |
  filter response.statusCode >= 500 and bLogCode = "API_REQUEST_COMPLETE" |
  sort @timestamp asc
  ```

- Display Calls to a particular API
  ```
  fields @timestamp, request.method, request.urlRule, response.statusCode                  |
  filter bLogCode = "API_REQUEST_COMPLETE" and request.urlRule like /auth\/confirmAccount/ |
  sort @timestamp asc
  ```

### User Metric Queries

- Find aID from email
  ```
  fields aID |
  filter bLogCode = "AUTH_LOGIN_SUCCESS" and email = "james.bensson@smeckn.com"
  ```

- Find all activity from aID
  ```
  fields @timestamp, @message |
  filter aID = 2619 |
  sort @timestamp asc
  ```

- Find all requests from aID
  ```
  fields @timestamp, request.method, request.urlRule, response.statusCode |
  filter aID = 13848 and bLogCode = "API_REQUEST_COMPLETE" |
  sort @timestamp asc
  ```

### Debugging Queries

The following are sample CloudWatch Logs Insights queries used for debugging.

- Find logs from lambda requestID
  ```
  fields @timestamp, @message                                |
  filter @requestId = "49206f0a-ee0d-443d-896e-b81021940b33" |
  sort @timestamp asc
  ```

### Infrastructure Queries

The following are sample CloudWatch Logs Insights queries used for infrastructure metrics.

- Amount of Overprovisioned Memory
  ```text
  filter @type = "REPORT" |
  stats max(@memorySize / 1024 / 1024) as provisonedMemoryMB,
        min(@maxMemoryUsed / 1024 / 1024) as smallestMemoryRequestMB,
        avg(@maxMemoryUsed / 1024 / 1024) as avgMemoryUsedMB,
        max(@maxMemoryUsed / 1024 / 1024) as maxMemoryUsedMB,
        provisonedMemoryMB - maxMemoryUsedMB as overProvisionedMB
  ```

## Sentry

Smeckn leverages [Sentry](https://sentry.io) to track errors and other abnormal events.

### API: sentry.py:SentryManager

`SentryManager` is the server's interface to alerting sentry if any abnormal behavior occurs.
It exposes the following functions:

- `captureMessage()`: This function allows a developer to send a message event to Sentry alerting that something
  went wrong. By default, `captureMessage` will dump all Smeckn state to the logs. Example usage:
  ```python
  mgr.sentry.captureMessage(message="DeadLetter", bLogCode=BeblsoftLogCode.SQS_DEAD_LETTER_RECEIVED)
  ```

- `captureError()`: This function allows a developer to send an error event to Sentry.
  By default, `captureError` will dump all Smeckn state to the logs. Example usage:
  ```python
  mgr.sentry.captureError(error=bError, bLogCode=BeblsoftLogCode.API_ERROR,
                          logMsg="API Error: {}".format(bError.code))
  ```
