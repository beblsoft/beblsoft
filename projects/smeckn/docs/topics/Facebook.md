# Facebook

This document describes the facebook profile workflows and diagrams how they are implemented from
WebClient to the Server in the code.

## Profile: Login, Create

Create Facebook Profile is initiated on the WebClient when the user logs into facebook. The webclient
then sends the credentials down to the server where the profile is added to the database.

```text
WEBCLIENT: Path /app/profileGroupDrillDown/profileAdd
User Clicks "Facebook"
smeckn/webClient/src/appComponents/FacebookProfile.js:FacebookProfile.onAdd()
smeckn/webClient/src/store/facebookProfile.js:add()
  |
  Tell server to create new facebook profile
  |
SERVER: POST /facebookProfile
smeckn/server/wAPIV1/facebookProfile/facebookProfile.py:FacebookProfileResource.post()
smeckn/server/facebook/manager.py:FacebookProfileManager.create()
  - base/facebook/python/user/dao.py:BfacebookUserDAO.getByID()
  - smeckn/server/aDB/facebookProfile/dao.py:FacebookProfileDAO.createFromBFBUserModel()
```

## Profile: Sync Start

Any time the user enters the context of their facebook profile on the WebClient, the `ProfileWrapper.vue`
is displayed. `ProfileWrapper.vue` kicks off the `SyncBar` component which optionally syncs the profile.

```
WEBCLIENT
smeckn.webClient/src/appComponents/profile/ProfileWrapper.vue
smeckn.webClient/src/appComponents/sync/SyncBar.vue:refresh()
smeckn/webClient/src/store/sync.js:create()
  |
  Tell server to create and start sync
  |
SERVER: POST /sync
smeckn/server/wAPIV1/sync/sync.py:SyncResource.post()
smeckn/server/profile/sync/manager.py:SyncManager.start()
  - smeckn/server/facebook/manager.py:FacebookManager.sync()
  - smeckn/server/profile/sync/manager.py:SyncManager.kickContentWorkers()
    smeckn/server/profile/sync/worker.py:SyncWorkerManager.kick()
     |
     Kick async jobs
     |
SERVER: SyncWorkerJob
smeckn/server/aJob/syncWorker/__init__.py:SyncWorkerJob.execute()
smeckn/server/profile/sync/worker.py:SyncWorkerManager.main()
smeckn/server/facebook/post/sync.py:PostSyncManager.syncChunk()
  - smeckn/server/facebook/post/sync.py:PostSyncManager.getNextChunk()
    base/facebook/python/user/dao.py:BFacebookUserDAO.getPostList()
  - smeckn/server/facebook/post/sync.py:PostSyncManager.syncChunkToDB()
    smeckn/server/aDB/facebookPost/dao.py:FacebookPostDAO.createFromBFBPostModel()
```

## Profile: Sync Status

After the `SyncBar.vue` component creates the sync, it polls the sync status.

```
WEBCLIENT
smeckn.webClient/src/appComponents/profile/ProfileWrapper.vue
smeckn.webClient/src/appComponents/sync/SyncBar.vue:displayProgress()
smeckn/webClient/src/store/sync.js:getProgress()
  |
  Get sync progress from server
  |
SERVER: GET /sync/<smID>/progress
smeckn/server/wAPIV1/sync/progress.py:ProgressResource.get()
smeckn/server/profile/sync/progress.py:SyncProgressManager.get()
  - smeckn/server/profile/sync/manager.py:SyncManager.checkTimeoutAllContent()
  - smeckn/server/profile/sync/manager.py:SyncManager.checkComplete()
  - smeckn/server/facebook/post/sync.py:PostSyncManager.getProgress()
```

## Profile: Get

When the user navigates to the `FacebookProfileOverview` screen, the webclient retrieves the Facebook
Profile metadata.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/overview
smeckn.webClient/src/appComponents/facebookProfile/FacebookProfileOverview.vue
smeckn.webClient/src/appComponents/facebookProfile.vue:get()
  |
  Get Facebook Profile From Server
  |
SERVER: GET /facebookProfile/<smID>
smeckn/server/wAPIV1/facebookProfile/smID.py:SMIDResource.get()
smeckn/server/aDB/facebookProfile/dao.py:FacebookProfileDAO.getBySMID()
```

## Profile: Update Credentials

The user can update their facebook profile credentials via the `FacebookProfileManage` screen.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/manage
smeckn.webClient/src/appComponents/facebookProfile/FacebookProfileManage.vue
smeckn.webClient/src/store/facebookProfile.js:updateCredentials()
  |
  Update Facebook Profile Credentials on Server
  |
SERVER: PUT /facebookProfile/<smID>
smeckn/server/wAPIV1/facebookProfile/smID.py:SMIDResource.put()
smeckn/server/facebook/manager.py:FacebookProfileManager.updateAccessToken()
  - base/facebook/python/ltAccessToken/dao.py:BFacebookLTAccessTokenDAO.get()
  - smeckn/server/aDB/facebookProfile/dao.py:FacebookProfileDAO.update()
```

## Profile: Delete

The user can delete their facebook profile via the `FacebookProfileManage` screen.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/manage
smeckn.webClient/src/appComponents/facebookProfile/FacebookProfileManage.vue
smeckn.webClient/src/store/profile.js:delete()
  |
  Delete profile from server
  |
SERVER: DELETE /profile/<smID>
smeckn/server/wAPIV1/profile/smID.py:SMIDResource.delete()
smeckn/server/aDB/profile/dao.py:ProfileDAO.delete()
```

## Posts: Get

The user can search, sort, filter, and analyze their Facebook posts via the `FacebookPostAll` screen.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/postAll
smeckn.webClient/src/appComponents/facebookPost/FacebookPostAll.vue
smeckn.webClient/src/store/facebookPost.js:list()
  |
  Get posts from server
  |
SERVER: GET /facebookPost
smeckn/server/wAPIV1/facebookPost/facebookPost.py:FacebookPostResource.get()
smeckn/server/aDB/facebookPost/dao.py:FacebookPostDAO.getAll()
```

## Posts: Statistic

The user can view statistics on their posts via the `FacebookPostFrequency` screen.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/postFrequency
smeckn.webClient/src/appComponents/facebookPost/FacebookPostFrequency.vue
smeckn.webClient/src/store/statistic.js:get()
  |
  Get statistic from server
  |
SERVER: GET /statistic
smeckn/server/wAPIV1/statistic/statistic.py:StatisticResource.get()
smeckn/server/profile/statistic/manager.py:StatisticManager.getList()
smeckn/server/facebook/post/manager.py:PostManager.statisticDAO
smeckn/server/aDB/facebookPost/statistic.py:FacebookPostStatisticDAO.getList()
```

## Posts: Histogram

The user can view a histogram of their posts via the `FacebookPostFrequency` screen.

```
WEBCLIENT: Path /app/profileGroupDrillDown/facebook/postFrequency
smeckn.webClient/src/appComponents/facebookPost/FacebookPostFrequency.vue
smeckn.webClient/src/store/hitogram.js:get()
  |
  Get histogram from server
  |
SERVER: GET /histogram
smeckn/server/wAPIV1/histogram/histogram.py:HistogramResource.get()
smeckn/server/profile/histogram/manager.py:HistogramManager.getList()
smeckn/server/facebook/post/manager.py:PostManager.statisticDAO
smeckn/server/aDB/facebookPost/histogram.py:FacebookPostHistogramDAO.getList()
```
