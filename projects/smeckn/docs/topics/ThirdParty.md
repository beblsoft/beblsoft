# 3rd Party Platforms

## GitLab

GitLab is used for source hosting and CICD. [Beblsoft Code](https://gitlab.com/beblsoft/beblsoft),
[Beblsoft Pipelines](https://gitlab.com/beblsoft/beblsoft/pipelines)

## Stripe

Stripe is used for payment processing.
[Console](https://dashboard.stripe.com/dashboard)

## Facebook

Facebook is used as a platform to supply user data for analysis.
[Console](https://developers.facebook.com/apps/1184500005034053/dashboard/),
[Test Console](https://developers.facebook.com/apps/924804401063069/dashboard/).

## ReCAPTCHA

ReCAPTCHA is used to ensure robots cannot access website.
[Console](https://www.google.com/recaptcha/admin/site/342862129)

## Sentry

Sentry is used for error tracking:
- Server: [Console](https://sentry.io/organizations/beblsoft/issues/?project=1437814),
  Sentry Slug: `smeckn-server`
- Web Client: [Console](https://sentry.io/organizations/beblsoft/issues/?project=1437813),
  Sentry Slug: `smeckn-webclient`

## Google Analytics

Google Analytics is used to track client traffic on the website.
[Console](https://analytics.google.com/analytics/web/#/a138051825w198639795p193825780/admin)
- Account: `Smeckn`
- Property: `www.smeckn.com`
- Views: `All Data`, `Production`, `Staging`, `Nightly`