# Email

## Timeline

The email templating subsystem is used as follows:

1. At Development Time

   MJML email templates are converted to Jinja (Python) HTML templates. See generate HTML command below.

2. On Server Processing an Event

   The server renders the Jinja Email template and sends an email to a user.
   For example, see `$SMECKN/server/wAPIV1/auth/createAccount.py`

3. On User Recieving the Email

   The email renders in the user's email browser (ex. Gmail).
   The email may have embedded links to the Web Client Application.
   The email may also have embedded images that are hosted by the Web Client's CDN.

## Relevant Files

```bash
cd $SMECKN;
$ tree -L 3 -I 'webAPI|webClient|node_modules|__pycache__|venv'
.
├── email
│   ├── mjml                          : MJML email template files that are converted to HTML
│   │   ├── common-logo-header.mjml
│   │   ├── common-styles.mjml
│   │   ├── confirm-account.mjml
│   │   └── reset-password.mjml
│   ├── html                          : HTML Jinja Templates that are rendered and sent by the server
│   │   ├── common-logo-header.html
│   │   ├── common-styles.html
│   │   ├── confirm-account.html
│   │   └── reset-password.html
│   ├── public                        : Static assets that are hosted by the web client
│   │   └── img
│   ├── cli.log
│   ├── cli.py                        : Email entrypoint
│   ├── Dockerfile
│   ├── gc.py
│   ├── package.json
│   ├── package-lock.json
│   └── requirements.txt
├── aws
│   ├── manager
│       └── webClient.py              : Does the work of deploying email static assets to web client
└── server
    ├── manager
    │   ├── email.py                  : Common server code to render and send email templates
    ├── wAPIV1
        ├── auth
        │   ├── createAccount.py      : Server example of sending an email

```

## Public Assets

All files under `$SMECKN/email/public` are available to use inside the email using the `WEBCLIENT_EMAIL_URL`
jinja template variable.

For example:
```html
<mj-image src="{{ WEBCLIENT_EMAIL_URL }}/img/info-logo-title.png" alt="" align="center"
  height="40px" width="200px"></mj-image>
```

## Linking to Web Client

Link to the Web Client Application by using the `WEBCLIENT_APP_URL` jinja template variable.

```html
<mj-button href="{{ WEBCLIENT_APP_URL }}/confirmAccount?createAccountToken={{ CREATE_ACCOUNT_TOKEN }}">
  Confirm Account
</mj-button>
```

## Command Reference

| Description                                   | Command                                                 |
|:----------------------------------------------|:--------------------------------------------------------|
| Clean                                         | `cd $SMECKN; docker-compose run email clean`            |
| Generate HTML from MJML                       | `cd $SMECKN; docker-compose run email generate`         |
| Generate HTML from MJML and Watch for Changes | `cd $SMECKN; docker-compose run email generate --watch` |
| Validate MJML files                           | `cd $SMECKN; docker-compose run email validate`         |
