# License Finder

Smeckn leverages [license_finder](https://github.com/pivotal/LicenseFinder) to verify third party
software licenses. See the `beblsoft/tools/licenseFinder` for more information.

## Allowable Licenses

We use the following heuristics when vetting software:

- Development Software

  Any license can be used for development (non-production) only purposes.
  For example, if the dependency is listed in `package.json`'s `devDependencies` section.

- Must Avoid Licenses

  Avoid `Affero GPL` (`AGPL`) used anywhere in the codebase.
  AGPL used on the server will cause all of the client code to be AGPL as well.

- Client-side Licenses

  Since the client is distributed to the customer, it must avoid all _copyleft_ licenses.
  This includes, but not limited to: `AGPL`, `GPL`, `LGPL`, `MPL`, `EPL`, `CDDL`. Usage of any one of these
  licenses could force Smeckn to open source or pay fines.

- Server-side Licenses

  Since the server is NOT distributed to the customer, it can use all _copyleft_ licenses except
  for `AGPL`.

## File Tree

```bash
cd $SMECKN;
$ tree -L 2
.
├── bin
│   ├── licenseFinder               : Entrypoint to run license_finder commands
|
├── licenseFinder
│   ├── dependency_decisions.yml    : Where license_finder stores all its dependency
|   |                                 dependency_decisions I.e. 'MIT' license is OK
│   ├── Dockerfile                  : Dockerfile creates environment with Ruby and
|   |                                 license_file executable
│   ├── report.html                 : HTML license report
│   └── venv
```

## Usage

Use license finder as follows:

| Description                       | Command                                                              |
|:----------------------------------|:---------------------------------------------------------------------|
| Check licenses                    | `cd $SMECKN; docker-compose run licenseFinder check`                 |
| Generate HTML report              | `cd $SMECKN; docker-compose run licenseFinder reportHTML`            |
| Add rules to dependency_decisions | See `$SMECKN/bin/licenseFinder` and `$TOOLS/licenseFinder/README.md` |