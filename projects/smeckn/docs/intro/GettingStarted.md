# Getting Started

## Laptop

Each developer should have a Laptop running `Linux Mint Version >= 19 Tara`.

## Security

The following security standards are required for developers:

- All laptops container source code must have full hard drive encryption
- Secure Laptop Passwords (i.e. contain letters, numbers, non-alphanumeric, >= 10 digits)
- 2 Factor Authentication on Email

## Developer Accounts

The following accounts are needed for Smeckn Developers. Talk to jbensson about setting each one up.

- GSuite: GMail, Google Drive, etc.
- AWS
- GitLab
- Sentry
- Facebook Developer
- Google Analytics
