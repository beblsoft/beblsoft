# Application Basics

## Quickstart

Below are instructions to install the development environment on a laptop:
-  Machine dependencies: `$SMECKN/bin/install machine`
-  Smeckn software: `$SMECKN/bin/install all`

Then, to run the full stack locally:
- Export AWS credentials : `aws_exportCredentials`
- Login to Gitlab Registry: `docker_loginGitLab`
- Pull smeckn Docker images: `docker_pull smeckn`
- Start the full stack : `$SMECKN/bin/compose up`.
- After the stack is up: View the [Server API](https://localhost:5100/v1) and [WebClient](https://localhost:8021/app)
- Run Server tests: `$SMECKN/bin/compose test_server`
- Run WebAPI tests: `$SMECKN/bin/compose test_webAPI`
- Run WebClient tests: `$SMECKN/bin/compose test_webClient`
- Run Headless WebClient tests: `$SMECKN/bin/compose test_webClientHeadless`

## Environments

Smeckn uses various environments to test code before rolling it out to production. Each environment
is appropriately namespaced so that environments do not conflict with each other.

| Name       | Client Domain                          | API Domain                        |
|:-----------|:---------------------------------------|:----------------------------------|
| production | `smeckn.com/app`, `www.smeckn.com/app` | `api.smeckn.com`                  |
| staging    | `staging.test.smeckn.com/app`          | `staging-api.test.smeckn.com`     |
| nightly    | `nightly.test.smeckn.com/app`          | `nightly-api.test.smeckn.com`     |
| developer  | `{developer}.test.smeckn.com/app`      | `{developer}-api.test.smeckn.com` |

## Architecture

Smeckn is a client-server architecture with the following components:

- Global Database (GDB): Holds Global State for application.
- Account Database (ADB): Holds all account information. One ADB is created per user.
- Server: a Flask WSGI server that handles client API requests among other things.
- Web Client: the code that runs in the user's laptop when they open www.smeckn.com.
- Web API: the JavaScript interface layer enabling the web client to communicate
  with the server.

The file tree is as follows:
```text
smeckn/
  webClient/     : Web Client Application
  webAPI/        : Web Interface to Server, used by webClient to access server
  server/        : Complete server implementation (Flask Application, Database Models and DAOs,
                   Background Processsing, MLS, SLS, ..)
  aws/           : AWS Deployment Software
  bin/           : Command Line Interfaces
  common/        : Smeckn Common Code
  docs/          : Internal Documentation (part of what you are reading now)
  licenseFinder/ : Automated license management
```

## Development Flow

Smeckn leverages the principles of trunk based development.

### Branches

Below are the relevant branches:

- `master` branch: Central trunk branch, the single source of truth in the repository.
- `<developer>-<title>` (ex. `dblack-bannerFix`) branches: These are lightweight developer branches
  that are pushed to Gitlab for review and CI verification before they are merged into `master`.
- `smeckn-v#`: Release branch, number monatomically increases for each release.

### Commit Rules

The following rules are adhered to when committing:

- All new features must first be verified by CI in a development branches before merging to `master`.
- Changes that break the `master` pipeline must be fixed immediately as this will hinder all developers
  from making progress.
- When ready to ship to production, a release branch is created. A Release branch can then cherry pick
  changes from `master`.

  ```txt

            --------------------         ----------------------
            | dblack-bannerFix |         | jbensson-fixEslint |
            --------------------         ----------------------
                         |                           |
                         |                           |
                         v                           v
   ----   ----   ----   ----   ----   ----   ----   ----
   |c0|-->|c1|-->|c2|-->|c4|-->|c5|-->|c6|-->|c7|-->|c8|  Beblsoft master branch
   ----   ----   ----   ----   ----   ----   ----   ----
     |             |            |                     |
     |             |            |                     v
     |             |        CherryPick               HEAD
     |             |          /
     |             |         /
     |             v        v
     |          smeckn-v2 -->
     v
  smeckn-v1
  ```

### Pipelines

The following CI Pipelines are used to regress changes (see `$SMECKN/.gitlab-ci.yml` for full details):

- Every push: local tests
- Daily/Nightly pipeline: local tests, nightly env deploy, nightly env test
- Every release branch commit: local tests, deploy to staging, test staging, deploy to production,
  test production

### New Feature Workflow

When developing a new feature, fixing a bug, or otherwise adding to the codebase, the following
workflow should be used.

1. Create a new local branch. Name the branch as follows: `<developer>-<title>`.

   ```bash
   git checkout -b jbensson-photoAnalysis
   ```

2. Do development work on that branch until it is ready to test.

   ```bash
   # Make changes
   git add -A
   git commit -m "First commit to photo analysis"
   ```

3. Push branch to Gitlab, to have it reviewed and CI run.

   ```bash
   git push origin jbensson-photoAnalysis
   ```

4. Create a merge request on Gitlab, requesting to merge your newly created branch into
   `beblsoft/beblsoft:master`. Reference related issues and add reviewers.

5. Iterate changes locally based on reviewer feedback and CI.

6. When the branch is ready, maintainer will merge it into master.

7. After branch has been merged into main. Delete local and remote branches, and then pull the
   new master branch with the integrated changes.

   ```bash
   git push --delete origin jbensson-photoAnalysis # Remote Branch
   git branch -d jbensson-photoAnalysis            # Local Branch
   git checkout master
   git pull origin master
   ```
