# Docker

## Introduction

Each Smeckn component can be run inside of a container. This allows for ease of use and repeatability
across different environments. `docker-compose` orchestrates the containers together to run the
complete smeckn stack. `$SMECKN/docker-compose.yml` defines all of the container dependencies.

Each component Dockerfile derives itself from the smeckn Base Dockerfile. The dockerfiles are located here:

- Base dockerfile: `$SMECKN/Dockerfile`
- Component dockerfiles: `$SMECKN/<component>/Dockerfile`

Below are the files related to docker infrastructure:
```bash
.
├── docker-compose.yml      : Docker compose file for all smeckn. Defines how all containers are
|                             composed together.
├── Dockerfile              : Base Dockerfile
├── .env                    : docker compose environment variables
├── bin
│   ├── compose             : Helper script to compose different configurations
│   ├── gitlabRunner        : CI Script executed by GitLab runner that starts containers
├── aws
│   ├── Dockerfile
├── email
│   ├── Dockerfile
├── licenseFinder
│   ├── Dockerfile
├── server
│   ├── Dockerfile
├── webAPI
│   ├── Dockerfile
└── webClient
    ├── Dockerfile

```

## Changes

Dockerfile changes must be managed appropriately as folows:

- Non-backwards compatible base change
    * Increment base version: Increment `BASE_VERSION` in `$SMECKN/.env`
    * Increment component version: Increment `COMPONENT_VERSION` in `$SMECKN/.env`
    * Rebuild images: `docker_build smeckn`
    * Push images: `docker_push smeckn`

- Non-backwards compatible component change
    * Increment component version: Increment `COMPONENT_VERSION` in `$SMECKN/.env`
    * Rebuild images: `docker_build smeckn`
    * Push images: `docker_push smeckn`

- Backwards compatible component change
    * Rebuild images: `docker_build smeckn`
    * Push images: `docker_push smeckn`

## Local Laptop

Below is the docker stack running on local laptop (see `docker-compose.yml` for additional lightweight
components not listed):
![Docker Stack](./SmecknDockerStack.jpg)

## Gitlab CI

Below is the docker stack running on GitLab. Containers run nested inside of a priveliged docker container
![GitLab Stack](./SmecknGitLabStack.jpg)