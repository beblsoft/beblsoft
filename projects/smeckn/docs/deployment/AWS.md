# AWS

## Architecture

The Smeckn AWS Stack is shown below:

![AWS Stack](./SmecknAWSStack.jpg)

## File Tree

```bash
cd $SMECKN/aws;
tree -L 2
.
├── cli.py                       : Python Command Line Interface
├── commands                     : CLI Command Verbs
│   ├── create.py
│   ├── debug.py
|   ...
│   ├── test.py
│   ├── upgrade.py
│   └── wakeup.py
├── config                       : Configurations for different AWS environments
│   ├── __init__.py
│   ├── dblack.py
│   ├── environment.py
│   ├── jbensson.py
│   ├── nightly.py
│   ├── production.py
│   ├── staging.py
│   └── test.py
├── manager                      : Managers for each component of the AWS stack
|   |                              Managers are in charge of all the component CRUD
│   ├── __init__.py
│   ├── accountDatabase.py
│   ├── accountDatabaseServer.py
│   ├── bastion.py
│   ├── common.py
|   ...
│   ├── ses.py
│   ├── single.py
│   ├── templates
│   ├── test.py
│   └── webClient.py
├── requirements.txt
├── static                       : Static Content used by AWS lambdas
│   └── mysqlclient

```

## Components

### List

The AWS environments leverage the following components:

- __Route 53__: Domain Name Management Service
- __ACM__: SLS Certificate Manager
- __CloudFront__: Content Delivery Network for Speedy website downloads
- __S3__: Object storage for static data
- __API Gateway__: Provides REST interface which can invoke a Lambda Function
- __Lambda__: Serverless function solution. Smeckn uploads `$SMECKN/server` code into 2 different
  lambda functions:
    * __Manager Lambda Switch (MLS)__: Used for management operations
    * __Server Lambda Switch (SLS)__: Used to service client requests
- __EC2__: VM Hosting Service, where Bastion Host and GitLab CI jobs run
- __Secrets Manager__: Used to store environment secrets.
- __CloudWatch__: Used to monitor AWS services
- __SES__: Simple Email Service. Used to send and receive email
- __SNS__: Simple Notification Service. Used to invoke Lambda function from email
- __SQS__: Use for async offload of tasks to lambda handlers
- __Rekognition__: AI Service to analyze photos and videos
- __Comprehend__: AI Service to analyze text
- __RDS__: Database Hosting Service. Hosts GDBS and ADBS.

### Command Reference

The following commands allow the AWS stack to be manipulated from a local computer.

::: warning
Please use caution when modifying production resources.
:::

| Description           | Command                                                                                                                                               |
|:----------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------|
| Full Stack: Create    | `cd $SMECKN; docker-compose run aws --env <Env> create --allojbs`                                                                                     |
| Full Stack: Describe  | `cd $SMECKN; docker-compose run aws --env <Env> describe --allobjs`                                                                                   |
| Full Stack: Deploy    | `cd $SMECKN; docker-compose run aws --env <Env> deploy --allobjs`                                                                                     |
| Full Stack: Delete    | `cd $SMECKN; docker-compose run aws --env <Env> delete --allobjs`                                                                                     |
| MLS: Tail             | `cd $SMECKN; docker-compose run aws --env <Env> tail --mls`                                                                                           |
| Bastian: Start        | `cd $SMECKN; docker-compose run aws --env <Env> start --bastian`                                                                                      |
| Bastian: Stop         | `cd $SMECKN; docker-compose run aws --env <Env> stop --bastian`                                                                                       |
| GDBS: Wake up         | `cd $SMECKN; docker-compose run aws --env <Env> wakeup --gdbs`                                                                                        |
| GDB: Upgrade          | `cd $SMECKN; docker-compose run aws --env <Env> upgrade --gdb --gdbversion=afc3efafa5ef`                                                              |
| GDB: Downgrade        | `cd $SMECKN; docker-compose run aws --env <Env> upgrade --gdb --gdbversion=afc3efafa5ef`                                                              |
| ADBS: Wake up         | `cd $SMECKN; docker-compose run aws --env <Env> wakeup --adbs`                                                                                        |
| ADB:Describe By Email | `cd $SMECKN; docker-compose run aws --env <Env> describe --adb --adbemail=james.bensson@smeckn.com`                                                   |
| ADB:Describe By ID    | `cd $SMECKN; docker-compose run aws --env <Env> describe --adb --adbid=1`                                                                             |
| ADB:Upgrade All       | `cd $SMECKN; docker-compose run aws --env <Env> upgrade --adb --adbversion=d50d8daf5442`                                                              |
| ADB:Downgrade All     | `cd $SMECKN; docker-compose run aws --env <Env> downgrade --adb --adbversion=d50d8daf5442`                                                            |
| SLS: Tail             | `cd $SMECKN; docker-compose run aws --env <Env> tail --sls`                                                                                           |
| SLS: Dump Logs        | `cd $SMECKN; docker-compose run aws --env <Env> dump --sls --slslogstream=2018/11/06/[\$LATEST]6f3a5eed7bc74c45a36d042f3224d986 --slsfilter=32a82809` |

## Testing

AWS is tested using the [Python unittest library](https://docs.python.org/3/library/unittest.html).

### File Tree

```bash
cd $SMECKN/aws;
tree -L 2
.
├── cli.py
├── manager
│   ├── test.py            : Test manager
├── tests                  : All test files located here
│   ├── aSleepJob_test.py
│   ├── gSleepJob_test.py
│   ├── __init__.py
│   └── __pycache__
```

### Command Reference

| Description               | Command                                                                                      |
|:--------------------------|:---------------------------------------------------------------------------------------------|
| Test all modules          | `cd $SMECKN; docker-compose run aws --env <Env> test --module=ALL`                           |
| Test specific module name | `cd $SMECKN; docker-compose run aws --env <Env> test --name=smeckn.aws.tests.gSleepJob_test` |
