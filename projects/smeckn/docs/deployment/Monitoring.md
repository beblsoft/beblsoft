# Monitoring

## Websites

The environment websites are available here:

- [Production](https://www.smeckn.com)
- [Staging](https://www.staging.test.smeckn.com)
- [Nightly](https://www.nightly.test.smeckn.com)

## Google Analytics

View the google analytics console [here](https://analytics.google.com/analytics/web/#/report-home/a138051825w198639795p193825780).

Metrics to check:

- Active users on site
- Time on site
- Conversion Rate: Dashboard to Create Accout
- Conversion Rate: Create Account to Payment

## Cloud Watch

View the cloud watch production dashboard [here](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#dashboards:name=Smeckn-Production).

### Global Database

- Connections
- CPU Utilization
- Serverless Capacity
- Max Bytes Used

### Account Databases

- Connections
- CPU Utilization
- Serverless Capacity
- Max Bytes Used

### Server Lambda Switch Metrics

- Invocations
- Errors
- 4xx Error
- 5xx Error
- Number of messages sent, recieved, deleted

### Manager Lambda Switch Metrics

- Invocations
- Errors

### Nat Gateway

- Bytes in From Destination (Less than 45 GBPS)
- Bytes out From Destination (Less than 45 GBPS)
- Active Connection Count

### Web Client Cloud Front

- Requests
- Bytes Downloaded
- 4xx Error Rate
- 5xx Error Rate

## Gitlab Pipelines

View the gitlab pipelines dashboard [here](https://gitlab.com/beblsoft/beblsoft/pipelines).

## Stripe

View the stripe console [here](https://dashboard.stripe.com/dashboard).

## Facebook

View the facebook console [here](https://developers.facebook.com/apps/1184500005034053/dashboard/).

View the test console [here](https://developers.facebook.com/apps/924804401063069/dashboard/).

## ReCAPTCHA

View the reCAPTCHA console [here](https://www.google.com/recaptcha/admin/site/342862129).

## Sentry

View the sentry server console [here](https://sentry.io/organizations/beblsoft/issues/?project=1437814).

View the sentry web client console [here](https://sentry.io/organizations/beblsoft/issues/?project=1437813).

