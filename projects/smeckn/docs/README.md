---
home: true
heroImage: /img/logo.png
actionText: Get Started →
actionLink: /intro/GettingStarted
footer: Copyright © 2019-present Smeckn, Inc.
---