#!/usr/bin/env python3
"""
 NAME:
  vcr.py

 DESCRIPTION
  VCR Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import os
import sys
import logging
from vcr import VCR  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- VCR MIXIN CLASS ------------------------------ #
class VCRMixin():
    """
    VCR Mixin

    Bibliography: Modeled off of VCRPY-unittest
    https://github.com/agriffis/vcrpy-unittest/blob/master/vcr_unittest/testcase.py
    """

    def __init__(self, mgr):
        """
        Initialize object
        """
        self.mgr = mgr
        self.cfg = self.mgr.cfg

    # ------------------------ PROPERTIES ----------------------------------- #
    @property
    def cassetteName(self):
        """
        Return cassette name

        Ex. SleepJobTestCase.test_valid.yaml
        """
        return "{}.{}.yaml".format(self.__class__.__name__, self._testMethodName)

    @property
    def cassettePath(self):
        """
        Return cassette path
        """
        return os.path.join(self.cassetteLibraryDir, self.cassetteName)

    @property
    def cassetteLibraryDir(self):  # pylint: disable=W0613
        """
        Return cassette directory for test

        Returns
        smeckn/server/test/cassettes/<path relative to /smeckn/server>
        Ex. smeckn/server/test/cassettes/payments/charge/manager_test/
        """
        curPath           = os.path.realpath(__file__)                       # server/test/vcr.py
        curDir            = os.path.dirname(curPath)                         # server/test
        cassettesDir      = os.path.join(curDir, "cassettes")                # server/test/cassettes
        serverDir         = os.path.dirname(curDir)                          # server
        subClassFile      = sys.modules[self.__class__.__module__].__file__  # server/payments/cart/manager_test.py
        subClassDir       = os.path.dirname(subClassFile)                    # server/payments/cart
        subClassBase      = os.path.basename(subClassFile)                   # manager_test.py
        subClassBaseNoExt = os.path.splitext(subClassBase)[0]                # manager_test
        commonPrefix      = os.path.commonprefix([serverDir, subClassDir])   # server/
        relativePath      = os.path.relpath(subClassDir, commonPrefix)       # payments/cart
        libraryDir        = os.path.join(cassettesDir, relativePath,
                                         subClassBaseNoExt)                  # server/test/cassettes/payments/cart/manager_test
        return libraryDir

    # ------------------------ SETUP ---------------------------------------- #
    @logFunc()
    def setUp(self):
        """
        SetUp
        """
        if not getattr(self, "vcrSetup", False):
            recordMode     = self.cfg.VCR_RECORD_MODE
            self.cleanCassetteFile(recordMode=recordMode)
            vcr            = VCR(record_mode=recordMode, match_on=self.cfg.VCR_MATCH_ON,  # pylint: disable=W0612
                                 cassette_library_dir=self.cassetteLibraryDir)
            ctxManager     = vcr.use_cassette(self.cassetteName)
            vcrLogger      = logging.getLogger("vcr")
            vcrLogger.setLevel(self.cfg.VCR_LOG_LEVEL)
            cassette       = ctxManager.__enter__()  # pylint: disable=W0612
            self.addCleanup(ctxManager.__exit__, None, None, None)  # Executed by unittest after tearDown
            self.vcrSetup  = True

    # ------------------------ DELETE CASSETTE FILE ------------------------- #
    @logFunc()
    def cleanCassetteFile(self, recordMode):
        """
        Clean cassette file
        Desired behavior in record mode "all":
          - cassetteFile removed
          - cassetteFile rewritten
          - Note: without removing cassette, all mode will append to cassette file
        """
        cassettePath = self.cassettePath
        if recordMode == "all" and os.path.exists(cassettePath):
            os.remove(cassettePath)
