#!/usr/bin/env python3
"""
 NAME:
  auth.py

 DESCRIPTION
  Auth Mixin Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import unittest
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- AUTH MIXIN CLASS ----------------------------- #
class AuthMixin(unittest.TestCase):
    """
    Auth Mixin
    """

    @logFunc()
    def authTest(self, route, methodFunc, **methodKwargs): #pylint: disable=R0912
        """
        Test Route Authentication
        Verify that each route is protected against bad authentication credentials

        Args
          route:
            Route to send HTTP verb
            Ex. "/v1/profileGroup/1"
          methodFunc:
            Method to call on route
            One of: self.[get,post,put,delete]JSON
          methodKwargs:
            Extra kwargs to pass to methodFunc
            Ex. d={"name":"john"}
        """

        runConfigList = [
            # No Tokens
            {"code": 400},

            # One Token Non-existent
            {"authHeader": True, "code": 400},
            {"authCookie": True, "code": 400},
            {"accountHeader": True, "code": 400},
            {"accountCookie": True, "code": 400},

            # Good Auth Token -------------------------------------------------
            # Bad Account
            {"authHeader": True, "badAccountHeader": True, "code": 401, "bErrorCode": 1142},
            {"authHeader": True, "badAccountCookie": True, "code": 401, "bErrorCode": 1142},
            {"authCookie": True, "badAccountHeader": True, "code": 401, "bErrorCode": 1142},
            {"authCookie": True, "badAccountCookie": True, "code": 401, "bErrorCode": 1142},

            # Wrong Account
            {"authHeader": True, "wrongAccountHeader": True, "code": 401, "bErrorCode": 1187},
            {"authHeader": True, "wrongAccountCookie": True, "code": 401, "bErrorCode": 1187},
            {"authCookie": True, "wrongAccountHeader": True, "code": 401, "bErrorCode": 1187},
            {"authCookie": True, "wrongAccountCookie": True, "code": 401, "bErrorCode": 1187},

            # Good Account Token ----------------------------------------------
            # Bad Auth
            {"badAuthHeader": True, "accountHeader": True, "code": 401, "bErrorCode": 1142},
            {"badAuthHeader": True, "accountCookie": True, "code": 401, "bErrorCode": 1142},
            {"badAuthCookie": True, "accountHeader": True, "code": 401, "bErrorCode": 1142},
            {"badAuthCookie": True, "accountCookie": True, "code": 401, "bErrorCode": 1142},

            # Wrong Auth
            {"wrongAuthHeader": True, "accountHeader": True, "code": 401, "bErrorCode": 1187},
            {"wrongAuthHeader": True, "accountCookie": True, "code": 401, "bErrorCode": 1187},
            {"wrongAuthCookie": True, "accountHeader": True, "code": 401, "bErrorCode": 1187},
            {"wrongAuthCookie": True, "accountCookie": True, "code": 401, "bErrorCode": 1187},
        ]

        for runConfig in runConfigList:
            _methodKwargs = methodKwargs.copy()
            # Auth Tokens -----------------------------------------------------
            # Good Auth
            if runConfig.get("authHeader", False):
                _methodKwargs["authToken"] = self.popAuthToken
            if runConfig.get("authCookie", False):
                self.setTokenCookies(authToken=self.popAuthToken)

            # Bad Auth
            if runConfig.get("badAuthHeader", False):
                _methodKwargs["authToken"] = "asdflkjsadhfp09asdflakdsjhf"
            if runConfig.get("badAuthCookie", False):
                self.setTokenCookies(authToken="asdflkjsadhfp09asdflakdsjhfuntToken")

            # Wrong Auth
            if runConfig.get("wrongAuthHeader", False):
                _methodKwargs["authToken"] = self.nonPopAuthToken
            if runConfig.get("wrongAuthCookie", False):
                self.setTokenCookies(authToken=self.nonPopAuthToken)

            # Account Tokens --------------------------------------------------
            # Good Account
            if runConfig.get("accountHeader", False):
                _methodKwargs["accountToken"] = self.popAccountToken
            if runConfig.get("accountCookie", False):
                self.setTokenCookies(accountToken=self.popAccountToken)

            # Bad Account
            if runConfig.get("badAccountHeader", False):
                _methodKwargs["accountToken"] = "asdflkjsadhfp09asdflakdsjhfuntToken"
            if runConfig.get("badAccountCookie", False):
                self.setTokenCookies(accountToken="asdflkjsadhfp09asdflakdsjhfuntToken")

            # Wrong Account
            if runConfig.get("wrongAccountHeader", False):
                _methodKwargs["accountToken"] = self.nonPopAccountToken
            if runConfig.get("wrongAccountCookie", False):
                self.setTokenCookies(accountToken=self.nonPopAccountToken)

            (code, data) = methodFunc(route=route, **_methodKwargs)
            self.assertEqual(code, runConfig.get("code"))
            if runConfig.get("bErrorCode", False):
                self.assertEqual(data.get("code"), runConfig.get("bErrorCode"))
            self.deleteTokenCookies()
