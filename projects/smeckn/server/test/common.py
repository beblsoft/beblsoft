#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Test Case Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
import unittest
from datetime import datetime
from freezegun import freeze_time
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gc import gc
from smeckn.server.test.route import RouteMixin
from smeckn.server.test.auth import AuthMixin
from smeckn.server.test.vcr import VCRMixin
from smeckn.server.fake.data import FakeData
from smeckn.server.eCtx import ExecutionContext


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
mgr    = gc.server.mgr


# ---------------------------- COMMON TEST CASE ----------------------------- #
@freeze_time(datetime(2019, 6, 1, 0, 0, 0), tick=True)
class CommonTestCase(FakeData,
                     RouteMixin, AuthMixin,
                     VCRMixin,
                     unittest.TestCase):
    """
    Common Test Case
    """

    def __init__(self, *args, **kwargs):  # pylint: disable=W0231
        """
        Initialize object
        """
        FakeData.__init__(self, mgr=mgr)
        VCRMixin.__init__(self, mgr=mgr)
        unittest.TestCase.__init__(self, *args, **kwargs)

        self.testStartDate = None
        self.testClient    = self.fApp.test_client()

    @classmethod
    @logFunc()
    def setUpClass(cls):
        mgr.gdb.delete()
        mgr.gdb.create(addLocalADBS=True)

    @classmethod
    @logFunc()
    def tearDownClass(cls):
        mgr.gdb.delete()

    def setUp(self, setUpVCR=True, **fakeSetupKwargs):  # pylint: disable=W0221
        """
        SetUp Test Case
        """
        ExecutionContext.setLocal()
        self.testStartDate = datetime.utcnow() # Set here so it's Frozen
        if setUpVCR:
            VCRMixin.setUp(self)
        FakeData.setUp(self, **fakeSetupKwargs)

    def tearDown(self, **fakeTeardownKwargs):  # pylint: disable=W0221
        """
        TearDown Test Case
        """
        FakeData.tearDown(self, **fakeTeardownKwargs)
        ExecutionContext.set(eCtx=None)
