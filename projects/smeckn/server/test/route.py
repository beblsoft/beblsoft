#!/usr/bin/env python3
"""
 NAME:
  route.py

 DESCRIPTION
  Route Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import json
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.cookie.bCookie import cookies
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE


# ---------------------------- ROUTE MIXIN CLASS ---------------------------- #
class RouteMixin():
    """
    Route Mixin

    Note:
      Parent class needs to set self.testClient
    """

    routes                     = BeblsoftAttrDict()
    routes.error               = "/v1/error"
    routes.constant            = "/v1/constant"
    routes.fake                = "/v1/fake"
    routes.platform            = "/v1/platform"
    routes.platformGDBSStatus  = "/v1/platform/gdbsStatus"
    routes.platformADBSStatus  = "/v1/platform/adbsStatus"
    routes.login               = "/v1/auth/login"
    routes.logout              = "/v1/auth/logout"
    routes.authStatus          = "/v1/auth/status"
    routes.forgotPassword      = "/v1/auth/forgotPassword"
    routes.resetPassword       = "/v1/auth/resetPassword"
    routes.createAccount       = "/v1/auth/createAccount"
    routes.confirmAccount      = "/v1/auth/confirmAccount"
    routes.account             = "/v1/account"
    routes.unitBalance         = "/v1/unit/balance"
    routes.unitPrice           = "/v1/unit/price"
    routes.creditCard          = "/v1/creditCard"
    routes.creditCardDefault   = "/v1/creditCard/default"
    routes.cart                = "/v1/cart"
    routes.charge              = "/v1/charge"
    routes.profileGroup        = "/v1/profileGroup"
    routes.profile             = "/v1/profile"
    routes.sync                = "/v1/sync"
    routes.analysis            = "/v1/analysis"
    routes.analysisCount       = "/v1/analysis/count"
    routes.statistic           = "/v1/statistic"
    routes.histogram           = "/v1/histogram"
    routes.text                = "/v1/text"
    routes.facebookProfile     = "/v1/facebookProfile"
    routes.facebookPost        = "/v1/facebookPost"

    # ------------------------ ROUTE FUNCTIONS ------------------------------ #
    def getHeaders(self, headers=None, authToken=None, accountToken=None):  # pylint: disable=R0201
        """
        Get API Headers
        Args
          headers:
            Headers from user
        """
        if not headers:
            headers = {}
        if authToken:
            headers["API_AUTH_TOKEN"] = authToken
        if accountToken:
            headers["API_ACCOUNT_TOKEN"] = accountToken
        return headers

    @logFunc()
    def postJSON(self, route, d=None, headers=None, authToken=None, accountToken=None):
        """
        Send Post Request
        Args
          route:
            route to post to
          d:
            Dictionary of data to post
          headers:
            Dictionary of headers to send
        Returns
          Tuple(status code, response dictionary)
        """
        d    = {} if d is None else d
        resp = self.testClient.post(
            route,
            data         = json.dumps(d),
            headers      = self.getHeaders(headers, authToken, accountToken),
            content_type = "application/json")
        return resp.status_code, json.loads(resp.data)

    @logFunc()
    def postJSONFull(self, route, d=None, headers=None, authToken=None, accountToken=None):
        """
        Send Post Request and Return all details
        Args
          Same as postJSON
        Returns
          BeblsoftAttrDict
          {
            code:
              Status code of request. Ex. 200
            data:
              JSON Data
              Ex. {'foo': 1}
            headers:
              MuliDict of headers
              Ex. {"foo": "bar"}
            cookie:
              http.cookies SimpleCookie object
          }
        """
        # Send Request
        d    = {} if d is None else d
        resp = self.testClient.post(
            route,
            data         = json.dumps(d),
            headers      = self.getHeaders(headers, authToken, accountToken),
            content_type = "application/json")

        # Parse cookies
        cookie = cookies.SimpleCookie()
        for header in resp.headers.getlist('Set-Cookie'): # Headers MultiDict
            cookie.load(header)

        # Populate rval
        rval         = BeblsoftAttrDict()
        rval.code    = resp.status_code
        rval.data    = json.loads(resp.data)
        rval.headers = resp.headers
        rval.cookie  = cookie
        return rval

    @logFunc()
    def putJSON(self, route, d=None, headers=None, authToken=None, accountToken=None):
        """
        Send Put Request
        """
        d    = {} if d is None else d
        resp = self.testClient.put(
            route,
            data         = json.dumps(d),
            headers      = self.getHeaders(headers, authToken, accountToken),
            content_type = "application/json")
        return resp.status_code, json.loads(resp.data)

    @logFunc()
    def getJSON(self, route, headers=None, authToken=None, accountToken=None):  # pylint: disable=W0102
        """
        Send Get request
        """
        resp = self.testClient.get(
            route,
            headers      = self.getHeaders(headers, authToken, accountToken),
            content_type = "application/json")
        return resp.status_code, json.loads(resp.data)

    @logFunc()
    def deleteJSON(self, route, headers=None, authToken=None, accountToken=None):  # pylint: disable=W0102
        """
        Send Delete request
        """
        resp = self.testClient.delete(
            route,
            headers      = self.getHeaders(headers, authToken, accountToken),
            content_type = "application/json")
        return resp.status_code, json.loads(resp.data)


    # ------------------------ COOKIE FUNCTIONS ----------------------------- #
    def setTokenCookies(self, authToken=None, accountToken=None):
        """
        Set Token Cookies
        """
        if authToken:
            self.setCookie(key=API_AUTH_TOKEN_COOKIE, value=authToken)
        if accountToken:
            self.setCookie(key=API_ACCOUNT_TOKEN_COOKIE, value=accountToken)

    def deleteTokenCookies(self):
        """
        Set Token Cookies
        """
        self.deleteCookie(key=API_AUTH_TOKEN_COOKIE)
        self.deleteCookie(key=API_ACCOUNT_TOKEN_COOKIE)


    def setCookie(self, key, value=None, serverName="127.0.0.1", domain="127.0.0.1"):
        """
        Set cookie on the test client, these cookies will be sent to the server
        Args
          key:
            Cookie Name
            Ex: "__smeckn_auth_token"
          value:
            Cookie Value
            Ex: "asdflkjasdf;alkdsjf;adksflj"
          serverName:
            Cookie Server Name
          domain:
            Cookie Domain
            Ex. "localhost"
        """
        self.testClient.set_cookie(server_name=serverName, domain=domain, key=key, value=value)


    def deleteCookie(self, key, path="/", serverName="127.0.0.1", domain="127.0.0.1"):
        """
        Delete cookie in the test client
        Args
          key:
            Cookie Name
            Ex: "__smeckn_auth_token"
          path:
            Cookie Path
            Ex: "/"
          serverName:
            Cookie Server Name
          domain:
            Cookie Domain
            Ex. "localhost"
        """
        self.testClient.delete_cookie(server_name=serverName, key=key, path=path, domain=domain)
