#!/usr/bin/env python
"""
 NAME:
  gc.py

DESCRIPTION
 Smeckn Server Global Context
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context
    """

    def __init__(self):
        """
        Initialize Object

        Note:
          Configured by cli.py
          Can't be declared inside cli.py as importers shouldn't import from __main__
        """
        # Server ---------------------------------
        self.server        = BeblsoftAttrDict()
        self.server.cfg    = None
        self.server.mgr    = None

        # Log ------------------------------------
        self.bLog          = None
        self.cLogFilterMap = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime     = None
        self.endTime       = None


# ----------------------- GLOBALS ------------------------------------------- #
gc = GlobalContext()
