#!/usr/bin/env python3
"""
NAME:
 heartbeat.py

DESCRIPTION
 Job Heartbeat Thread Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import threading
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ JOB HEARTBEAT THREAD ----------------------------- #
class JobHeartbeatThread(threading.Thread):
    """
    Job Heartbeat Thread
    """

    def __init__(self, func, periodS, stopEvent):
        """
        Initialize object
        Args
          func:
            Function to execute heartbeat
          periodS:
            Period in seconds to hearbeat
            Ex. 15
          stopEvent:
            Threading Event to wait on to stop heartbeating
        """
        threading.Thread.__init__(self, group=None, name="JobHB")
        self.func      = func
        self.periodS   = periodS
        self.stopEvent = stopEvent

    # ------------------------ RUN ------------------------------------------ #
    @logFunc()
    def run(self):
        """
        Run thread
        """
        while True:
            self.func()
            self.stopEvent.wait(timeout=self.periodS)
            if self.stopEvent.is_set():
                break
