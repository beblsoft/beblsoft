#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
 ------------------------------------------------------
  Job Functionality. A job represents a unit of work that can be performed
    1. Synchronously     : Caller waits for job to complete
    2. Asynchronously    : Caller queues job and continues running


 RULES FOR CHANGING JOBS
 ------------------------------------------------------
  - Description          : Jobs are async, so anytime lambda switch is updated it must be backwards
                           compatible and able to handle old jobs that were queued at a previous time
  - DO NOT --------------: Change existing job class names or module
                           I.e. Don't change smeckn.server.gJob.helloWorld.job:HelloWorldJob location
                         : Remove fields from existing job class constructors
                           I.e. Don't remove HelloWorldJob.__init__ args
                         : Remove fields from existing job execute functions
                           I.e. Don't remove HelloWorldJob.execute args
  - UPDATING ------------: Only ADD arguments to class constuctors
                         : Only ADD arguments to class execute functions


 JOB CLASS HIERARCHY
 ------------------------------------------------------
                 Job
                /   \
               /     \
      GlobalJob      AccountJob
        |               |
        |               |
      SleepJob       SleepJob
      CleanJob       FacebookAnalysisJob


 SYNC FLOW
 ------------------------------------------------------
   HelloWorldJob.invokeSync
     |
   Job.executeFromMessage
     |
   HelloWorldJob.execute


 ASYNC LOCAL FLOW
 ------------------------------------------------------
   HelloWorldJob.invokeAsync
     |
   JobQueueLocalManager.queueMessage - queues message on local worker pool
     |
   JobQueueLocalManager.workerThreadMain
     |
   Job.executeFromMessage
     |
   HelloWorldJob.execute


 ASYNC AWS FLOW
 ------------------------------------------------------
   HelloWorldJob.invokeAsync
     |
   JobQueueAWSManager.queueMessage
     |
   BeblsoftSQSMessage.send - Sends message SQS queue
     |
   smeckn.server.aws.jobQueue:handleMessageReceived
     |
   Job.executeFromMessage
     |
   HelloWorldJob.execute

"""

# -------------------------- IMPORTS ---------------------------------------- #
import time
import abc
import logging
import threading
import importlib
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.json.bObjectEncoder import BeblsoftJSONObjectEncoder
from smeckn.server.job.message import JobMessage
from smeckn.server.job.heartbeat import JobHeartbeatThread
from smeckn.server.eCtx import ExecutionContext


# -------------------------- GLOBALS ---------------------------------------- #
logger      = logging.getLogger(__name__)
threadLocal = threading.local()


# -------------------------- JOB -------------------------------------------- #
class Job(BeblsoftJSONObjectEncoder, abc.ABC):
    """
    Job Class
    """
    def __init__(self, mgr, jobMsg, heartbeatPeriodS=15):  # pylint: disable=W0613
        """
        Initialize object
        Args
          mgr:
            Server manager object
          jobMsg:
            Job Message object
          heartbeatPeriodS:
            Heartbeat period in seconds
            Ex. 15
        """
        self.mgr              = mgr
        self.cfg              = mgr.cfg
        self.eCtx             = ExecutionContext.get()
        assert self.eCtx
        self.msgID            = self.eCtx.jobMsgID
        self.jobMsg           = jobMsg
        self.gDB              = mgr.gdb.gDB
        self.heartbeatPeriodS = heartbeatPeriodS

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # -------------------------- ABSTRACT METHODS --------------------------- #
    @abc.abstractmethod
    def setUp(self):
        """
        SetUp execution

        Returns:
          True: SetUp Succeeded, execute will be call, tearDown will be called
          False: SetUp Failed, execute will NOT be called, tearDown will NOT be called
        """
        pass

    @abc.abstractmethod
    def tearDown(self, exception=None):
        """
        TearDown execution
        Args:
          exception:
            Exception Object if any occurred

        Notes:
          Must NEVER throw an exception.
        """
        pass

    @abc.abstractmethod
    def execute(self, **kwargs):
        """
        Execute Job

        Note:
          Any exception thrown will be handled by handleException.
        """
        pass

    @abc.abstractmethod
    def heartbeat(self):
        """
        Perform a heartbeat

        Note:
          Should never throw an exception.
        """
        pass

    @abc.abstractmethod
    def handleException(self, exception):
        """
        Handle a job exception

        Args:
          exception:
            Exception Object if any occurred

        Note:
          Should never throw an exception.
        """
        pass

    # -------------------------- EXECUTE ------------------------------------ #
    @staticmethod
    @logFunc()
    def executeFromMessage(mgr, jobMsg):
        """
        Execute from message
        Args
          mgr:
            Server manager object
          jobMessage:
            Stringified message
        """
        (classModule, className) = jobMsg.classStr.split(":")
        moduleObj                = importlib.import_module(classModule)
        classObj                 = getattr(moduleObj, className)
        jobObj                   = classObj(mgr=mgr, jobMsg=jobMsg, **jobMsg.classKwargs)
        return jobObj.executeWrap(**jobMsg.executeKwargs)

    # -------------------------- EXECUTE WRAP ------------------------------- #
    def executeWrap(self, **executeKwargs):
        """
        Preform message execution by invoking abstract methods
        """
        exception = None
        execute   = False
        hbse      = threading.Event()
        hbt       = JobHeartbeatThread(func=self.heartbeat,
                                       periodS=self.heartbeatPeriodS, stopEvent=hbse)
        try:
            self.__class__.setCurrentJob(job=self)
            execute = self.setUp()
            if execute:
                hbt.start()
                self.execute(**executeKwargs)
        except Exception as e:  # pylint: disable=W0703
            exception = e
            self.handleException(exception=e)
        finally:
            if execute:
                hbse.set()
                hbt.join()
                self.tearDown(exception=exception)
            self.__class__.setCurrentJob(job=None)

    # -------------------------- INVOKE ------------------------------------- #
    @classmethod
    @logFunc()
    def invokeAsync(cls, mgr, delayS=0, **jobMsgKwargs):
        """
        Invoke Job Asynchronously
        Args
          mgr:
            Server manager object
          delayS:
            Seconds to delay message

        LOCAL returns:
          Daemon thread object that is executing the message
          Useful for testing jobs, but should NOT be used in production code

        AWS returns:
          None if job successfully queued
          Otherwise, exception raised
        """
        jobMsg = JobMessage.fromJobClass(jobClass=cls, **jobMsgKwargs)
        return mgr.jqueue.queueMessage(jobMsg=jobMsg, delayS=delayS)

    @classmethod
    @logFunc()
    def invokeSync(cls, mgr, delayS=0, **jobMsgKwargs):
        """
        Invoke Job Synchronously

        Returns:
          Result of job.execute()
        """
        jobMsg = JobMessage.fromJobClass(jobClass=cls, **jobMsgKwargs)
        if delayS:
            time.sleep(delayS)
        return cls.executeFromMessage(mgr=mgr, jobMsg=jobMsg)


    # -------------------------- CURRENT JOB -------------------------------- #
    # Name of job object in thread local storage
    _JOB_THREAD_NAME = "__job"

    @classmethod
    def setCurrentJob(cls, job):
        """
        Set current job in thread local storage
        """
        return setattr(threadLocal, cls._JOB_THREAD_NAME, job)

    @classmethod
    def getCurrentJob(cls):
        """
        Get current job
        """
        return getattr(threadLocal, cls._JOB_THREAD_NAME, None)
