#!/usr/bin/env python3
"""
 NAME:
  message.py

 DESCRIPTION
  Job Message Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
import json


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- JOB MESSAGE ------------------------------------ #
class JobMessage():
    """
    Job Message Class
    """

    def __init__(self,
                 classStr,
                 classKwargs = None,
                 executeKwargs = None,
                 parentMetaList = None,
                 sentTimeMS = None):
        """
        Initialize object
        Args
          classStr:
            Class String
            Ex. smeckn.server.gJob.helloWorld.job:HelloWorldJob
          classKwargs:
            Dictionary of kwargs pass to instantiate class
            Ex. {}
          executeKwargs:
            Dictionary of kwargs passed to class.execute()
            Ex. {}
          sentTimeMS:
            Epoch Timestamp (in milliseconds) when message was sent
            Ex. 1529104986221
          parentMetaList:
            Metadata from parent execution
            Useful for debugging async calls that invoke other async children

        Important Note:
          This structure is passed over the wire.
          Never remove or change field names
          Only adding fields is allowed!
        """
        self.classStr        = classStr
        self.classKwargs     = classKwargs if classKwargs else {}
        self.executeKwargs   = executeKwargs if executeKwargs else {}
        self.parentMetaList  = parentMetaList if parentMetaList else []
        self.sentTimeMS      = sentTimeMS

    # -------------------------- ALTERNATE CONSTUCTORS ---------------------- #
    @staticmethod
    def fromJobClass(jobClass, **jobMsgKwargs):
        """
        Alternate constructor
        Args
          jobClass:
            Job Class
          jobMsgKwargs:
            Args to pass to __init__
        """
        moduleName = jobClass.__module__
        className  = jobClass.__name__
        jobMsg     = JobMessage(classStr = "{}:{}".format(moduleName, className),
                                **jobMsgKwargs)
        return jobMsg

    @staticmethod
    def fromString(jobMsgStr, **overrideKwargs):
        """
        Alternate constructor
        Args
          jobMsgStr:
            Stringified message
          overrideKwargs:
            Arguments to override those in jobMsgStr
        """
        jobMsgDict = json.loads(jobMsgStr)
        jobMsgDict.update(overrideKwargs)
        return JobMessage(**jobMsgDict)

    # -------------------------- TO STRING ---------------------------------- #
    def toString(self):
        """
        Return message formatted as string
        """
        return json.dumps(self.__dict__)
