#!/usr/bin/env python3
"""
 NAME:
  job_test.py

 DESCRIPTION
  Sleep Job Test
"""


# ---------------------------- IMPORTS -------------------------------------- #
import time
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gJob.sleep import SleepJob
from smeckn.server.gDB.gJob.dao import GlobalJobDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SleepJobTestCase(CommonTestCase):
    """
    Test Sleep Job
    """

    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        secondHB = None
        firstHB  = None

        # Kick off job
        thread   = SleepJob.invokeAsync(
            mgr           = self.mgr,
            classKwargs   = {"heartbeatPeriodS": 1},
            executeKwargs = {"sleepS": 8}
        )
        time.sleep(2)

        # Ensure job in table
        with self.gDB.sessionScope() as gs:
            gJobModelList = GlobalJobDAO.getAll(gs=gs, className="SleepJob")
            self.assertEqual(len(gJobModelList), 1)
            firstHB       = gJobModelList[0].heartbeatDate
        time.sleep(2)

        # Ensure heartbeat is larger
        with self.gDB.sessionScope() as gs:
            gJobModelList = GlobalJobDAO.getAll(gs=gs, className="SleepJob")
            self.assertEqual(len(gJobModelList), 1)
            secondHB      = gJobModelList[0].heartbeatDate
            self.assertGreater(secondHB, firstHB)
        thread.join()

        # Ensure job deleted from table
        with self.gDB.sessionScope() as gs:
            nGJobModels = GlobalJobDAO.getAll(gs=gs, className="SleepJob", count=True)
            self.assertEqual(nGJobModels, 0)
