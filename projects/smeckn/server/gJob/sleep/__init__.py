#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Sleep Job Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import time
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gJob import GlobalJob


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SLEEP JOB -------------------------------------- #
class SleepJob(GlobalJob):
    """
    Sleep Job Class
    """

    # -------------------------- EXECUTE ------------------------------------ #
    @logFunc()
    def execute(self, sleepS=5, **kwargs): #pylint: disable=W0221
        """
        Execute job
        Args
          sleepS:
            Amount of time (in seconds) to sleep
            Ex. 2
        """
        time.sleep(sleepS)
