#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Global Job Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import re
import abc
import logging
from datetime import datetime
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.job import Job
from smeckn.server.gDB.gJob.model import GlobalJobState
from smeckn.server.gDB.gJob.dao import GlobalJobDAO
from smeckn.server.log.sLogger import SmecknLogger


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- GLOBAL JOB ------------------------------------- #
class GlobalJob(Job, abc.ABC):
    """
    Global Job Class
    """
    _JOB_THREAD_NAME = "__gJob"

    def __init__(self, groupURN="", parentSMID=0, incarn=0, **jobKwargs):
        """
        Initialize object
        Args
          groupURN:
            Job group urn
            Helps correllate many jobs that are related
            Ex. UUID1: urn:uuid:3ca8e6bc-44ab-3e06-818e-ee5f0a9cb9c8
          parentSMID:
            Parent Job Smeckn ID
            Ex. 5
          incarn:
            Job Incarnation
            Tracks how many times this job has respawned itself
            Ex. 0
        """
        super().__init__(**jobKwargs)
        self.groupURN   = groupURN
        self.parentSMID = parentSMID
        self.incarn     = incarn

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        return {
            "name": self.__class__.__name__,
            "msgID": self.msgID,
            "groupURN": self.groupURN,
            "parentSMID": self.parentSMID,
            "incarn": self.incarn,
            "functionRN": self.eCtx.functionRN,
            "requestID": self.eCtx.requestID
        }

    # -------------------------- ABSTRACT METHODS --------------------------- #
    @abc.abstractmethod
    def execute(self, **kwargs):
        """
        Execute Job
        """
        pass

    # -------------------------- HELPER FUNCTIONS --------------------------- #
    @logFunc()
    def setStateError(self):
        """
        Set State to Error
        """
        try:
            with self.gDB.sessionScope() as gs:
                gJobModel = GlobalJobDAO.getOne(gs=gs, msgID=self.msgID)
                gJobModel.update(gs=gs, state=GlobalJobState.ERROR)
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{}: Set State Error failed".format(self))

    # -------------------------- OVERRIDDEN METHODS ------------------------- #
    @logFunc()
    def setUp(self):
        """
        Setup Execution
        """
        rval = True
        try:
            SmecknLogger.info(msg="{} Setup".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.GJOB_SETUP)
            with self.gDB.sessionScope() as gs:
                GlobalJobDAO.create(
                    gs          = gs,
                    msgID       = self.msgID,
                    groupURN    = self.groupURN,
                    parentSMID  = self.parentSMID,
                    incarn      = self.incarn,
                    state       = GlobalJobState.START,
                    classModule = self.__class__.__module__,
                    className   = self.__class__.__name__,
                    functionRN  = self.eCtx.functionRN,
                    requestID   = self.eCtx.requestID,
                    logStream   = self.eCtx.logStream)
        except sqlalchemy.exc.IntegrityError as e:
            duplicate = re.search(r"Duplicate entry .* for key 'uq_GlobalJob_msgID'", str(e))
            if duplicate:
                SmecknLogger.info(msg="{} Setup Duplicate".format(self.__class__.__name__),
                                  bLogCode=BeblsoftLogCode.GJOB_SETUP_DUPLICATE)
                rval = False
            else:
                raise e
        return rval

    @logFunc()
    def tearDown(self, exception=None):
        """
        Teardown Execution
        """
        try:
            SmecknLogger.info(msg="{} Teardown".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.GJOB_TEARDOWN)
            with self.gDB.sessionScope() as gs:
                GlobalJobDAO.deleteByFilter(gs=gs, msgID=self.msgID)
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{} Teardown failed".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.GJOB_TEARDOWN_FAILED)

    @logFunc()
    def heartbeat(self):
        """
        Perform a heartbeat
        """
        try:
            with self.gDB.sessionScope() as gs:
                gJobModel = GlobalJobDAO.getOne(gs=gs, msgID=self.msgID)
                GlobalJobDAO.update(gs=gs, gJobModel=gJobModel, heartbeatDate=datetime.utcnow())
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{} Heartbeat failed".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.GJOB_HEARTBEAT_FAILED)

    @logFunc()
    def handleException(self, exception):
        """
        Handle an exception
        """
        self.mgr.sentry.captureError(error=exception, bLogCode=BeblsoftLogCode.GJOB_EXCEPTION,
                                     logMsg="{} Exception".format(self.__class__.__name__))
        self.setStateError()
