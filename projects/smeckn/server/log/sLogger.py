#!/usr/bin/env python3
"""
 NAME:
  sLogger.py

 DESCRIPTION
  Smeckn Logger Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import sys  # pylint: disable=W0611
import threading  # pylint: disable=W0611
import traceback  # pylint: disable=W0611
import logging
from base.bebl.python.log.bLogger import BeblsoftLogger


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- SMECKN LOGGER CLASS -------------------------- #
class SmecknLogger():
    """
    Smeckn Logger Class
    """

    # ------------------- LOG ----------------------------------------------- #
    @staticmethod
    def log(aID = None, **kwargs):
        """
        Smeckn log hook that tries to find and attach relevant Smeckn metadata
        (like the account ID) to each log record

        Args
          aID:
            Account ID
            Ex. 234897
          aJob:
            AccountJob Object
          _callerStackIndex:
            Caller Stack Index
            See BeblosftLog.log
          kwargs:
            Kwargs for BeblsoftLog.log

        Format:
          The following keys are available in many (but not all) smeckn logs
          <message> {
            gJob: {}
            aJob: {}
            aID: <number>
            bLogCode: <string>
            logValue: <number>|<string>
            bError: {}
          }
        """
        # Args ----------------------------------
        _kwargs = {}
        _kwargs.update(kwargs)

        # Global Job ----------------------------
        from smeckn.server.gJob import GlobalJob
        gJob = GlobalJob.getCurrentJob()
        if gJob:
            _kwargs["gJob"] = gJob

        # Account Job ---------------------------
        from smeckn.server.aJob import AccountJob
        aJob = AccountJob.getCurrentJob()
        if aJob:
            _kwargs["aJob"] = aJob

        # Account ID ----------------------------
        from smeckn.server.application.glob import ApplicationGlobal
        _aID = aID
        if _aID is None:
            _aID = ApplicationGlobal.getAccountID()
        if _aID is not None:
            _kwargs["aID"] = _aID

        # Call underlying logger ----------------
        BeblsoftLogger.log(logger=kwargs.pop("logger", logger), **_kwargs)

    # ------------------- LOG WRAPPERS -------------------------------------- #
    @staticmethod
    def debug(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(level=logging.DEBUG, **kwargs)

    @staticmethod
    def info(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(level=logging.INFO, **kwargs)

    @staticmethod
    def warning(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(level=logging.WARNING, **kwargs)

    @staticmethod
    def error(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(level=logging.ERROR, **kwargs)

    @staticmethod
    def critical(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(level=logging.CRITICAL, **kwargs)

    @staticmethod
    def exception(**kwargs):  # pylint: disable=C0111
        SmecknLogger.log(isException=True, **kwargs)

    # ------------------- DUMP STATE ---------------------------------------- #

    @staticmethod
    def dumpState(**kwargs):
        """
        Dump System State
        Should only be called in error circumstances
        """
        # Args ----------------------------------
        _kwargs = {}
        _kwargs.update(kwargs)

        # Execution Context ---------------------
        from smeckn.server.eCtx import ExecutionContext
        eCtx = ExecutionContext.get()
        if eCtx:
            _kwargs["eCtx"] = eCtx

        # Thread Information --------------------
        # threadJSONList                = []
        # threadList                    = threading.enumerate()
        # threadIDMap                   = {thread.ident: thread for thread in threadList}
        # # For Each Thread
        # for threadID, frame in sys._current_frames().items():  # pylint: disable=W0212
        #     thread                    = threadIDMap.get(threadID, None)
        #     threadName                = thread.name if thread else None
        #     threadJSONDict            = {}
        #     threadJSONDict["name"]    = threadName
        #     threadJSONDict["id"]      = threadID
        #     threadJSONStackList       = []
        #     # For each stack record
        #     for fileName, lineNo, funcName, funcLine in traceback.extract_stack(frame):
        #         jsonStack             = {}
        #         jsonStack["fileName"] = fileName
        #         jsonStack["lineNo"]   = lineNo
        #         jsonStack["funcName"] = funcName
        #         jsonStack["funcLine"] = funcLine
        #         threadJSONStackList.append(jsonStack)
        #     threadJSONDict["stack"]   = threadJSONStackList
        #     threadJSONList.append(threadJSONDict)
        # _kwargs["threadList"] = threadJSONList

        # Call underlying logger ----------------
        SmecknLogger.error(**_kwargs)
