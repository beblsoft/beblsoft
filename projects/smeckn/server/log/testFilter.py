#!/usr/bin/env python3
"""
NAME
 testFilter.py

DESCRIPTION
 Test Log Filter
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging

# ------------------------ TEST LOG FILTER ---------------------------------- #
class TestConsoleLogFilter(logging.Filter):
    """
    Test Log Filter
    """

    def filter(self, record):
        """
        Args
          record:
            Log Record
            See: https://docs.python.org/3.7/library/logging.html#logrecord-objects
        Returns
          True if record should be logged
          False otherwise
        """
        smecknLog    = "smeckn" in record.name.lower()
        baseLog      = "base" in record.name.lower()
        filterRecord = smecknLog or baseLog
        logRecord    = not filterRecord
        return logRecord
