#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Account Database Server Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import pprint
from sqlalchemy import func, asc
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.accountDBServer.model import AccountDBServerModel
from smeckn.server.gDB.account.model import AccountModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT DATABASE SERVER DAO CLASS ----------------- #
class AccountDBServerDAO():
    """
    Account DB Serve3r Access Object
    """

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(gs, **kwargs):
        """
        Create new object
        Args:
          gs:
            Global DB session
          kwargs:
            Args to pass to Account Database Server Constructor
        """
        aDBSModel = AccountDBServerModel(**kwargs)
        gs.add(aDBSModel)
        return aDBSModel

    # ---------------------- DELETE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByID(gs, aDBSID):
        """
        Delete object
        """
        gs.query(AccountDBServerModel).\
            filter(AccountDBServerModel.id == aDBSID).delete()

    @staticmethod
    @logFunc()
    def deleteByWriteDomain(gs, writeDomain):
        """
        Delete object
        """
        gs.query(AccountDBServerModel).\
            filter(AccountDBServerModel.writeDomain == writeDomain).delete()

    @staticmethod
    @logFunc()
    def delete(gs, aDBSModel):
        """
        Delete object
        """
        gs.delete(aDBSModel)

    @staticmethod
    @logFunc()
    def deleteAll(gs):
        """
        Delete all objects
        """
        gs.query(AccountDBServerModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(gs, aDBSModel, **kwargs):  # pylint: disable=W0613
        """
        Update object
        Args
          aDBSModel:
            Account DB Server to Update
          kwargs:
            Dictionary of updates
        """
        for k, v in kwargs.items():
            setattr(aDBSModel, k, v)
        gs.add(aDBSModel)
        return aDBSModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getWithFewestAccountDBs(gs):
        """
        Return object with fewest account DBs
        """
        aDBSWithFewestAccounts = gs.\
            query(AccountDBServerModel, func.count(AccountDBServerModel.accounts)).\
            outerjoin(AccountModel).\
            group_by(AccountDBServerModel).\
            order_by(asc(func.count(AccountDBServerModel.accounts))).\
            first()
        return aDBSWithFewestAccounts[0]

    @staticmethod
    @logFunc()
    def getAll(gs, readDomain=None, writeDomain=None, count=False):
        """
        Return object list
        """
        q = gs.query(AccountDBServerModel)

        if readDomain:
            q = q.filter(AccountDBServerModel.readDomain == readDomain)
        if writeDomain:
            q = q.filter(AccountDBServerModel.writeDomain == writeDomain)

        return q.count() if count else q.all()

    @staticmethod
    @logFunc()
    def getCount(gs):
        """
        Return object count
        """
        return gs.query(AccountDBServerModel).count()

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(gs):
        """
        Describe all objects
        """
        aDBSModelList = AccountDBServerDAO.getAll(gs=gs)
        logger.info(pprint.pformat(aDBSModelList))
