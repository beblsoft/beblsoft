#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Account DB Server Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import pprint
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.accountDBServer.dao import AccountDBServerDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT DB SERVER POPULATER CLASS ----------------- #
class AccountDBServerPopulater(CommonPopulater):
    """
    Account DB Server Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, readDomainList=[], writeDomainList=[],  # pylint: disable=W0102
                     userList=[], passwordList=[]):
        """
        Populate List

        Returns:
          aDBSModelList
        """
        aModelList = []

        with self.gDB.sessionScope(commit=False) as gs:
            for idx, readDomain in enumerate(readDomainList):
                writeDomain   = writeDomainList[idx]
                user          = userList[idx]
                password      = passwordList[idx]
                aDBSModelList = AccountDBServerDAO.getAll(gs=gs, readDomain=readDomain,
                                                          writeDomain=writeDomain)
                # Update existing
                if aDBSModelList:
                    for aDBSModel in aDBSModelList:
                        AccountDBServerDAO.update(gs=gs, aDBSModel=aDBSModel, user=user, password=password)
                # Create new
                else:
                    AccountDBServerDAO.create(
                        gs          = gs,
                        writeDomain = writeDomain,
                        readDomain  = readDomain,
                        user        = user,
                        password    = password)
            gs.commit()
            aModelList = AccountDBServerDAO.getAll(gs=gs)
        return aModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with self.gDB.sessionScope() as gs:
            AccountDBServerDAO.deleteAll(gs=gs)

    @logFunc()
    def depopulateWriteDomainList(self, writeDomainList):  # pylint: disable=W0221
        """
        Depopulate by write domain
        """
        with self.gDB.sessionScope() as gs:
            for writeDomain in writeDomainList:
                AccountDBServerDAO.deleteByWriteDomain(gs=gs, writeDomain=writeDomain)

    # ---------------------------- DESCRIBE --------------------------------- #
    @logFunc()
    def describeAll(self):  # pylint: disable=W0221
        """
        Describe All
        """
        with self.gDB.sessionScope(commit=False) as gs:
            aDBSModelList = AccountDBServerDAO.getAll(gs)
            logger.info("Account DB Servers:\n{}".format(
                pprint.pformat(aDBSModelList)))
