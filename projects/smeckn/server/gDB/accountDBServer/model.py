#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Account Database Server Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, String, BigInteger, Enum
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.gDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class AccountDBServerState(enum.Enum):
    """
    Account DB Server State Enumeration
    """
    ONLINE    = 1   # Service Reads and Writes
    ONLINE_RO = 2   # Service Read Only
    OFFLINE   = 3   # Service Offline


# ----------------------- ACCOUNT DATABASE SERVER MODEL CLASS --------------- #
class AccountDBServerModel(base):
    """
    Account DB Server Table
    """
    __tablename__ = "AccountDBServer"
    id            = Column(BigInteger, primary_key=True)        # ID
    writeDomain   = Column(String(256), unique=True)            # Writes go to this domain
    readDomain    = Column(String(256), unique=True)            # Reads go to this domain [Aurora read replicas]
    user          = Column(String(256))                         # Master username
    password      = Column(String(256))                         # Master password
    port          = Column(BigInteger, default=3306)            # Connection port
    state         = Column(Enum(AccountDBServerState),          # DB State
                           default=AccountDBServerState.ONLINE)
    creationDate  = Column(DATETIME(fsp=6),
                           default=datetime.utcnow)             # Creation Date

    # Child Accounts
    # 1 Account DB Server : N Accounts
    accounts      = relationship("AccountModel", back_populates="accountDBServer")

    def __repr__(self):
        return "[{} id={}, writeDomain={} readDomain={} port={}]".format(
            self.__class__.__name__, self.id, self.writeDomain,
            self.readDomain, self.port)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def isOnline(self):  # pylint: disable=C0111
        return self.state == AccountDBServerState.ONLINE

    @property
    def isOnlineRO(self):  # pylint: disable=C0111
        return self.state == AccountDBServerState.ONLINE_RO

    @property
    def isOffline(self):  # pylint: disable=C0111
        return self.state == AccountDBServerState.OFFLINE
