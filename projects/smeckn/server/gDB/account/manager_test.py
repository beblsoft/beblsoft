#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Account Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountDBState
from smeckn.server.test.common import CommonTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountManagerTestCase(CommonTestCase):
    """
    Test Account Manager
    """

    def test_create(self):
        """
        Test account state is set properly
        """
        self.dbp.createAdmin()
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=self.cfg.DBP_ADMIN_EMAIL)
            self.assertEqual(aModel.dbState, AccountDBState.CREATED)
