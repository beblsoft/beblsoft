#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Account Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import random
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.manager import AccountManager
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.dbp.common import CommonPopulater



# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT POPULATER CLASS --------------------------- #
class AccountPopulater(CommonPopulater):
    """
    Account Populater
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.aDBNamePrefix = self.cfg.ADB_NAME_PREFIX

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, nAccounts):
        """
        Populate N accounts
        """
        emailList    = []
        passwordList = []
        aTypeList    = []

        for _ in range(0, nAccounts):
            emailList.append(forgery_py.email.address())  # pylint: disable=E1101
            passwordList.append(forgery_py.basic.password())  # pylint: disable=E1101
            aTypeList.append(random.choice(list(AccountType)))
        return self.populateList(emailList=emailList,
                                 passwordList=passwordList, aTypeList=aTypeList)

    @logFunc()
    def populateList(self, emailList=[], passwordList=[], aTypeList=[]):  # pylint: disable=W0102
        """
        Populate many

        Returns:
          aModelList
        """
        aModelList = []
        for idx, email in enumerate(emailList):
            password = passwordList[idx]
            aType    = aTypeList[idx]
            aModel   = self.populateOne(email=email, password=password, aType=aType)
            aModelList.append(aModel)
        return aModelList

    @logFunc()
    def populateOne(self, email, password, aType=AccountType.REGULAR):
        """
        Populate one
        """
        aModel = None
        AccountManager.create(gDB=self.gDB, aDBNamePrefix=self.aDBNamePrefix,
                              email=email, password=password, aType=aType)
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=email)
        return aModel

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aType=None):  # pylint: disable=W0221
        """
        Depopulate all
        """
        if self.gDB.exists:
            AccountManager.deleteAll(gDB=self.gDB, aType=aType)

    @logFunc()
    def depopulateByEmail(self, email):  # pylint: disable=W0221
        """
        Depopulate
        """
        if self.gDB.exists:
            AccountManager.deleteByEmail(gDB=self.gDB, email=email)

    # ---------------------------- CLEAN ------------------------------------ #
    def cleanAll(self, aDBNamePrefix):
        """
        Clean all accounts databases
        Args
          aDBNamePrefix:
            Account Database Name Prefix
            Ex. "SmecknUnitTestADB"
        """
        if self.gDB.exists:
            AccountManager.cleanAll(self.gDB, aDBNamePrefix)

    # ---------------------------- DESCRIBE --------------------------------- #
    @logFunc()
    def describeAll(self, tree=True):  # pylint: disable=W0221
        """
        Describe all
        """
        with self.gDB.sessionScope(commit=False) as gs:
            AccountDAO.describeAll(gs=gs, tree=tree)

    @logFunc()
    def describeByEmail(self, email, tree=True):  # pylint: disable=W0221
        """
        Describe all
        """
        with self.gDB.sessionScope(commit=False) as gs:
            AccountDAO.describeByEmail(gs=gs, email=email, tree=tree)
