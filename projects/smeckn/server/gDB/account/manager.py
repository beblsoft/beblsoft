#!/usr/bin/env python3
"""
 NAME
  manager.py

 DESCRIPTION
  Account Manager Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.accountDBServer.dao import AccountDBServerDAO
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountDBState, AccountType
from smeckn.server.aDB import AccountDatabase


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT MANAGER CLASS ----------------------------- #
class AccountManager():
    """
    Account Manager

    Performs account management operations

    Manages the account database state machine:

           . Create Initiated
           |
        CREATING
           |
           | Physical Account Database Created and Tables Added
           |
        CREATED
           |
           | Delete Initiated
           |
        DELETING
           |
           | Physical Account Database Deleted
           |
        DELETED
           |
           . Account Databse Record Removed From Global Database
    """

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(gDB, aDBNamePrefix, email, password, aType=AccountType.REGULAR):
        """
        Create object
        Args
          aDBNamePrefix:
            Account DB Name Prefix
            Ex. "SmecknTestADB"
          email:
            Account Email
            Ex. "FooBar@gmail.com"
          password:
            Account Password
            Ex. "12430985dfsa"
          aType:
            Acount Type

        Returns
          Account model
        """
        with gDB.sessionScope(commit=False) as gs:
            # Global DB
            # See if account record already exists
            # If not,
            #   Find Account DB Server with fewest DBs
            #   Commit account record to creating
            aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)
            if not aModel:
                aDBSModel = AccountDBServerDAO.getWithFewestAccountDBs(gs)
                aModel    = AccountDAO.create(gs=gs, email=email, password=password,
                                              dbNamePrefix=aDBNamePrefix, aDBSModel=aDBSModel,
                                              aType=aType)
                gs.commit()
                aModel    = AccountDAO.getByEmail(gs=gs, email=email)

            # Account DB
            # If account record doesn't have a database associated with it
            # Create account database
            # Update the account record
            if aModel.dbState != AccountDBState.CREATED:
                version = AccountManager.createAccountDatabase(aModel=aModel)
                AccountDAO.update(gs, aModel, dbVersion=version, dbState=AccountDBState.CREATED)
                gs.commit()
                aModel = AccountDAO.getByEmail(gs=gs, email=email)
            return aModel

    @staticmethod
    @logFunc()
    def createAccountDatabase(aModel):
        """
        Create account database

        Returns
          Account Database Version
        """
        aDB = AccountDatabase.fromAModel(aModel)
        aDB.create()
        aDB.createTables()
        aDB.initializeTableData(aModel=aModel)
        return aDB.bDBMigrations.version

    # ---------------------- DELETE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByEmail(gDB, email):
        """
        Delete object
        """
        with gDB.sessionScope() as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)
            if aModel:
                # Global DB
                # Update account record to deleting
                AccountDAO.update(gs, aModel, dbState=AccountDBState.DELETING)
                gs.commit()
                aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)

                # Delete account database
                aDB = AccountDatabase.fromAModel(aModel)
                aDB.delete()

                # Global DB
                # Delete account record
                AccountDAO.delete(gs, aModel)

    @staticmethod
    @logFunc()
    def deleteByID(gDB, aID):
        """
        Delete object
        """
        email = None
        with gDB.sessionScope() as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=aID, raiseOnNone=False)
            email = aModel.email if aModel else None
        if email:
            AccountManager.deleteByEmail(gDB=gDB, email=email)

    @staticmethod
    @logFunc()
    def deleteAll(gDB, aType=None):
        """
        Delete all objects
        Args
          aType:
            Account Type
            Ex. AccountType.REGULAR
        """
        aModelList = []

        # Global DB
        # Query all accounts
        with gDB.sessionScope(commit=False) as gs:
            aModelList = AccountDAO.getAll(gs, aType=aType)

        # Both DBs
        # Delete each acount
        for aModel in aModelList:
            AccountManager.deleteByEmail(gDB=gDB, email=aModel.email)

    # ---------------------- UPGRADE ---------------------------------------- #
    @staticmethod
    @logFunc()
    def upgradeAIDToVersion(gDB, aID, toVersion):
        """
        Upgrade a single account database
        """
        aDB = AccountDatabase.fromID(gDB=gDB, aID=aID)
        aDB.bDBMigrations.upgrade(toVersion=toVersion)
        version = aDB.bDBMigrations.version
        with gDB.sessionScope() as gs:
            AccountDAO.updateByID(gs=gs, aID=aID, dbVersion=version)

    @staticmethod
    @logFunc()
    def upgradeToVersion(gDB, toVersion, aIDMod=None, aIDModEquals=None):
        """
        Upgrade account databases
        """
        aModelList = []
        with gDB.sessionScope(commit=False) as gs:
            aModelList = AccountDAO.getAll(gs=gs, notDBVersion=toVersion,
                                           aIDMod=aIDMod, aIDModEquals=aIDModEquals)

        for aModel in aModelList:
            AccountManager.upgradeAIDToVersion(gDB=gDB, aID=aModel.id,
                                               toVersion=toVersion)

    # ---------------------- DOWNGRADE -------------------------------------- #
    @staticmethod
    @logFunc()
    def downgradeAIDToVersion(gDB, aID, toVersion):
        """
        Downgrade a single account database
        """
        aDB = AccountDatabase.fromID(gDB=gDB, aID=aID)
        aDB.bDBMigrations.downgrade(toVersion=toVersion)
        version = aDB.bDBMigrations.version
        with gDB.sessionScope() as gs:
            AccountDAO.updateByID(gs=gs, aID=aID, dbVersion=version)

    @staticmethod
    @logFunc()
    def downgradeToVersion(gDB, toVersion, aIDMod=None, aIDModEquals=None):
        """
        Downgrade account databases
        """
        aModelList = []
        with gDB.sessionScope(commit=False) as gs:
            aModelList = AccountDAO.getAll(gs=gs, notDBVersion=toVersion,
                                           aIDMod=aIDMod, aIDModEquals=aIDModEquals)

        for aModel in aModelList:
            AccountManager.downgradeAIDToVersion(gDB=gDB, aID=aModel.id,
                                                 toVersion=toVersion)

    # ---------------------- CLEAN ------------------------------------------ #
    @staticmethod
    @logFunc()
    def cleanAll(gDB, aDBNamePrefix):
        """
        Clean all objects
        """
        with gDB.sessionScope(commit=False) as gs:
            # Global DB
            # Find all account db servers
            aDBSModelList = AccountDBServerDAO.getAll(gs)

            # Account DB
            # Find and delete all account dbs on each server
            for aDBSModel in aDBSModelList:
                aDBList = AccountDatabase.allFromADBSModel(aDBSModel, aDBNamePrefix)
                for aDB in aDBList:
                    aDB.delete()

            # Global DB
            # Delete all account records
            AccountDAO.deleteAll(gs)
