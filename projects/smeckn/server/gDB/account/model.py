#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Account Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, String, BigInteger, ForeignKey, Enum, Boolean
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash
from smeckn.server.gDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class AccountType(enum.Enum):
    """
    Account Type Enumeration
    """
    REGULAR    = 1
    ADMIN      = 2
    TEST       = 3


class AccountDBState(enum.Enum):
    """
    Account State Enumeration
    """
    CREATING  = 1
    CREATED   = 2
    DELETING  = 3
    DELETED   = 4


class AuthStatus(enum.Enum):
    """
    Authentication Status
    """
    LOGGED_OUT = 0
    LOGGED_IN  = 1


# ----------------------- ACCOUNT MODEL CLASS ------------------------------- #
class AccountModel(base):
    """
    Account Table
    """
    __tablename__      = "Account"

    # User specific
    id                 = Column(BigInteger, primary_key=True)            # ID
    email              = Column(String(256), unique=True)                # Ex. james.bensson@smeckn.com
    passwordHash       = Column(String(256))                             # pbkdf2:sha256:50000$k2Dyqv31...
    type               = Column(Enum(AccountType),
                                default=AccountType.REGULAR)             # Account Type: ADMIN, REGULAR, ...
    active             = Column(Boolean, default=True)                   # If True, Account is Active
    creationDate       = Column(DATETIME(fsp=6),
                                default=datetime.utcnow)                 # Creation Date

    # Account DB
    dbNamePrefix       = Column(String(128))                             # SmecknTest, SmecknProd
    dbState            = Column(Enum(AccountDBState),
                                default=AccountDBState.CREATING)         # Database State
    dbVersion          = Column(String(128), default='')                 # Database Version Ex. d03d78135411

    # Parent Account DB Server
    # 1 Account DB Server : N Accounts
    accountDBServerID  = Column(BigInteger,
                                ForeignKey("AccountDBServer.id"),
                                nullable=False)                          # Corresponding DB Server
    accountDBServer    = relationship("AccountDBServerModel",
                                      back_populates="accounts")

    def __repr__(self):
        return "[{} id={} email={}]".format(
            self.__class__.__name__, self.id, self.email)

    @property
    def password(self):
        """
        Account password
        """
        raise AttributeError("Password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.passwordHash = generate_password_hash(password)

    def verifyPassword(self, password):
        """
        Returns
          True if correct password
        """
        return check_password_hash(self.passwordHash, password)
