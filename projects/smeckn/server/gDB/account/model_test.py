#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Account Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.test.common import CommonTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountModelTestCase(CommonTestCase):
    """
    Test Account Model
    """

    def test_uniqueIDs(self):
        """
        Test all accounts have a unique id
        """
        nAccounts  = 10
        aModelList = []

        # Create accounts
        aModelList = self.dbp.accountP.populateN(nAccounts=nAccounts)

        # Double loop over aModels, ensuring all are unique
        for idx, aModel1 in enumerate(aModelList):
            for cmpIdx in range(0, idx):
                aModel2 = aModelList[cmpIdx]
                self.assertNotEqual(aModel1.id, aModel2.id,
                                    "ID's are the same {} {}".format(aModel1, aModel2))

    def test_password(self):
        """
        Test password functionality
        """
        emailList      = ["foo@goo.com", "bar@far.com"]
        passwordList   = ["1234", "1234"]
        passwordHashes = []

        # Create users
        for idx, email in enumerate(emailList):
            password = passwordList[idx]
            self.dbp.accountP.populateOne(email=email, password=password)

        for idx, email in enumerate(emailList):
            # Verify user attributes
            with self.gDB.sessionScope(commit=False) as gs:
                password = passwordList[idx]
                aModel   = AccountDAO.getByEmail(gs=gs, email=email)

                with self.assertRaises(AttributeError):
                    logger.info(aModel.password)
                self.assertTrue(aModel.verifyPassword(password))
                passwordHashes.append(aModel.passwordHash)

        # Double loop over password hashes, ensuring all are unique
        for idx, passwordHash in enumerate(passwordHashes):
            for cmpIdx in range(0, idx):
                passwordHashCmp = passwordHashes[cmpIdx]
                self.assertNotEqual(
                    passwordHash, passwordHashCmp,
                    "Password hashes same {} {} {}".format(passwordHash, idx, cmpIdx))
