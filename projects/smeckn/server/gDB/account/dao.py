#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Account Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import pprint
import logging
from sqlalchemy import not_, or_
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.model import AccountModel, AccountType, AccountDBState


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT DAO CLASS --------------------------------- #
class AccountDAO():
    """
    Account Data Access Object
    """

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(gs, email, password, dbNamePrefix, aDBSModel,
               aType=AccountType.REGULAR, dbState=AccountDBState.CREATING):
        """
        Create new object
        Args
          gs:
            Global DB session
          kwargs:
            Args to pass to Accout Database Constructor
        """
        aModel = AccountModel(email=email, password=password,
                              type=aType, dbNamePrefix=dbNamePrefix,
                              dbState=dbState, accountDBServerID=aDBSModel.id)
        gs.add(aModel)
        return aModel

    # ---------------------- DELETE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByID(gs, aID):
        """
        Delete object
        """
        gs.query(AccountModel).filter(AccountModel.id == aID).delete()

    @staticmethod
    @logFunc()
    def delete(gs, aModel):
        """
        Delete object
        """
        gs.delete(aModel)

    @staticmethod
    @logFunc()
    def deleteAll(gs):
        """
        Delete all objects
        """
        gs.query(AccountModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(gs, aModel, **kwargs):  # pylint: disable=W0613
        """
        Update object
        Args
          aModel:
            Account model
          kwargs:
            Dictionary of updates
        """
        for k, v in kwargs.items():
            setattr(aModel, k, v)
        gs.add(aModel)
        return aModel

    @staticmethod
    @logFunc()
    def updateByID(gs, aID, **kwargs):  # pylint: disable=W0613
        """
        Update object by id
        """
        aModel = AccountDAO.getByID(gs=gs, aID=aID)
        return AccountDAO.update(gs=gs, aModel=aModel, **kwargs)

    @staticmethod
    @logFunc()
    def updatePassword(gs, aModel, oldPassword, newPassword):
        """
        Update password
        Args
          oldPassword:
            Old password
            Ex. '123456'
          newPassword:
            New passwod
            Ex. '78910'
        """
        if not aModel.verifyPassword(password=oldPassword):
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_PASSWORD)
        aModel.password = newPassword
        gs.add(aModel)

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getByEmail(gs, email, raiseOnNone=True):
        """
        Return object by email
        """
        aModel = gs.query(AccountModel).filter(
            AccountModel.email == email).one_or_none()
        if not aModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_NO_ACCOUNT)
        return aModel

    @staticmethod
    @logFunc()
    def getByID(gs, aID, raiseOnNone=True):
        """
        Return object by id
        """
        aModel = gs.query(AccountModel).filter(AccountModel.id == aID).one_or_none()
        if not aModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_NO_ACCOUNT)
        return aModel

    @staticmethod
    @logFunc()
    def getAll(gs, aType=None, aIDMod=None, aIDModEquals=None, notDBVersion=None, count=False):
        """
        Get all following a specific criteria
        Args
          aType:
            Account Type
            Ex. AccountType.REGULAR
          aIDMod:
            Account ID Modulus
          aIDModEquals:
            Return account if account.id % aIDMod == this value
          notDBVersion:
            If specified, return all not at a specific db version
          count:
            If True, return count of all object having criteria
        """
        rval = None
        q    = gs.query(AccountModel)

        # Filters
        if aType is not None:
            q = q.filter(AccountModel.type == aType)
        if aIDMod:
            q = q.filter(AccountModel.id % aIDMod == aIDModEquals)
        if notDBVersion:
            q = q.filter(or_(AccountModel.dbVersion == None,
                             not_(AccountModel.dbVersion.contains(notDBVersion))))

        # Rval
        rval = q.count() if count else q.all()
        return rval

    @staticmethod
    @logFunc()
    def getCount(gs):
        """
        Return object count
        """
        return gs.query(AccountModel).count()

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(gs, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe all
        """
        aModelList = AccountDAO.getAll(gs=gs)
        for aModel in aModelList:
            AccountDAO.describeAModel(gs=gs, aModel=aModel,
                                      tree=tree, startSpaces=startSpaces)

    @staticmethod
    @logFunc()
    def describeByEmail(gs, email, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe by email
        """
        aModel = AccountDAO.getByEmail(gs=gs, email=email)
        AccountDAO.describeAModel(gs=gs, aModel=aModel,
                                  tree=tree, startSpaces=startSpaces)

    @staticmethod
    @logFunc()
    def describeAModel(gs, aModel, tree=True, startSpaces=0):  # pylint: disable=W0221 #pylint: disable=W0613
        """
        Describe A Model
        """
        # Import here to avoid circular imports
        from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
        from smeckn.server.aDB import AccountDatabase

        logger.info("{}{}".format(" " * startSpaces, pprint.pformat(aModel)))
        if tree:
            with AccountDatabase.sessionScopeFromAModel(aModel=aModel, commit=False) as aDBS:
                ProfileGroupDAO.describeAll(aDBS=aDBS, tree=tree,
                                            startSpaces=startSpaces + 2)
