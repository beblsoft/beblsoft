#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Global Job Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, String, BigInteger, Integer, Enum
from sqlalchemy.dialects.mysql import DATETIME
from smeckn.server.gDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class GlobalJobState(enum.Enum):
    """
    Global Job State Enumeration
    """
    START = enum.auto()
    ERROR = enum.auto()


# ----------------------- GLOBAL JOB MODEL CLASS ---------------------------- #
class GlobalJobModel(base):
    """
    Global Job Table
    """
    __tablename__ = "GlobalJob"

    # IDs
    smID          = Column(BigInteger, primary_key=True)  # Smeckn ID
    msgID         = Column(String(256), unique=True)     # Context message ID
    groupURN      = Column(String(128), default="")      # UUID1: urn:uuid:3ca8e6bc-44ab-3e06-818e-ee5f0a9cb9c8
    parentSMID    = Column(BigInteger, default=0)        # Parent Job smID
    incarn        = Column(Integer, default=0)           # Inc'd when job requeues itself

    # State
    state         = Column(Enum(GlobalJobState))         # GlobalJobState.START

    # Class
    classModule   = Column(String(256))                  # smeckn.server.gJob.helloWorld.job
    className     = Column(String(256))                  # HelloWorldJob

    # Times
    creationDate  = Column(DATETIME(fsp=6),
                           default=datetime.utcnow)
    heartbeatDate = Column(DATETIME(fsp=6),
                           default=None)

    # Context
    # arn:aws:lambda:us-east-1:225928776711:function:SmecknTestServerLSDeployment
    functionRN    = Column(String(128))
    requestID     = Column(String(128))                  # e3573efe-5b68-57e9-8b6b-6aa1d81407f1
    logStream     = Column(String(128))                  # 2018/11/09/[$LATEST]9b58df40035a44118e5574dd052fa7a6

    def __repr__(self):
        return "[{} smID={} className={}]".format(
            self.__class__.__name__, self.smID, self.className)
