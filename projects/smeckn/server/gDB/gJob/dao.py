#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Global Job Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.gJob.model import GlobalJobModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- GLOBAL JOB DAO CLASS ------------------------------ #
class GlobalJobDAO():
    """
    Global Job Data Access Object
    """

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(gs, **kwargs):
        """
        Create new object
        Args
          gs:
            Global DB session
          kwargs:
            Arguments
        """
        gJobModel = GlobalJobModel(**kwargs)
        gs.add(gJobModel)
        return gJobModel

    # ---------------------- DELETE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByFilter(gs, smID=None, msgID=None):
        """
        Delete object
        """
        query = gs.query(GlobalJobModel)
        if smID:
            query = query.filter(GlobalJobModel.smID == smID)
        if msgID:
            query = query.filter(GlobalJobModel.msgID == msgID)
        query.delete()

    @staticmethod
    @logFunc()
    def delete(gs, gJobModel):
        """
        Delete object
        """
        gs.delete(gJobModel)

    @staticmethod
    @logFunc()
    def deleteAll(gs):
        """
        Delete all objects
        """
        gs.query(GlobalJobModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(gs, gJobModel, **kwargs):  # pylint: disable=W0613
        """
        Update object
        Args
          gJobModel:
            Global Job Model
          kwargs:
            Dictionary of updates
        """
        for k, v in kwargs.items():
            setattr(gJobModel, k, v)
        gs.add(gJobModel)
        return gJobModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getOne(gs, smID=None, msgID=None, raiseOnNone=True):
        """
        Return one object
        Args
          smID:
            Smeckn ID

        """
        query = gs.query(GlobalJobModel)

        # Filters
        if smID:
            query = query.filter(GlobalJobModel.smID == smID)
        if msgID:
            query = query.filter(GlobalJobModel.msgID == msgID)

        # Execute
        gJobModel = query.one_or_none()
        if not gJobModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.DB_OBJECT_DOES_NOT_EXIST)
        return gJobModel

    @staticmethod
    @logFunc()
    def getAll(gs, className=None, count=False):
        """
        Get all following a specific criteria
        """
        rval  = None
        query = gs.query(GlobalJobModel)

        # Filters
        if className:
            query = query.filter(GlobalJobModel.className == className)

        # Rval
        rval = query.count() if count else query.all()
        return rval

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(gs):  # pylint: disable=W0221
        """
        Describe all
        """
        gJobModelList = GlobalJobDAO.getAll(gs=gs)
        for gJobModel in gJobModelList:
            GlobalJobDAO.describe(gs=gs, gJobModel=gJobModel)

    @staticmethod
    @logFunc()
    def describe(gs, gJobModel):  #pylint: disable=W0613
        """
        Describe object
        """
        logger.info("{}".format(gJobModel))
