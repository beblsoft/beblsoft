#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Global Job Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import re
import uuid
import logging
from datetime import datetime, timedelta
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.gJob.dao import GlobalJobDAO
from smeckn.server.gDB.gJob.model import GlobalJobState


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class GlobalJobModelTestCase(CommonTestCase):
    """
    Test Global Job Model
    """
    @logFunc()
    def test_crud(self):
        """
        Test crud operations
        """
        nJobs         = 10
        gJobModelList = []
        classModule   = "foo.bar.baz"
        className     = "ExampleJob"
        groupURN      = 10
        node          = uuid.getnode()
        groupURN      = uuid.uuid1(node=node).urn
        parentSMID    = 5
        incarn        = 100000000
        heartbeatDate = datetime.utcnow()
        msgIDPrefix   = "msgID"

        # Create
        with self.gDB.sessionScope() as gs:
            for idx in range(nJobs):
                gJobModel = GlobalJobDAO.create(
                    gs            = gs,
                    msgID         = "{}{}".format(msgIDPrefix, idx),
                    groupURN      = groupURN,
                    parentSMID    = parentSMID,
                    incarn        = incarn,
                    state         = GlobalJobState.START,
                    heartbeatDate = heartbeatDate,
                    classModule   = classModule,
                    className     = className)
                gJobModelList.append(gJobModel)

        # Read, Verify Creation
        with self.gDB.sessionScope() as gs:
            for idx in range(nJobs):
                msgID = "{}{}".format(msgIDPrefix, idx)
                gJobModel = GlobalJobDAO.getOne(gs=gs, msgID=msgID)
                self.assertEqual(gJobModel.msgID, msgID)
                self.assertEqual(gJobModel.groupURN, groupURN)
                self.assertEqual(gJobModel.parentSMID, parentSMID)
                self.assertEqual(gJobModel.incarn, incarn)
                self.assertEqual(gJobModel.state, GlobalJobState.START)
                self.assertLessEqual(heartbeatDate - gJobModel.heartbeatDate, timedelta(seconds=1))
                self.assertEqual(gJobModel.classModule, classModule)
                self.assertEqual(gJobModel.className, className)

        # Delete
        with self.gDB.sessionScope() as gs:
            for idx in range(nJobs):
                msgID = "{}{}".format(msgIDPrefix, idx)
                GlobalJobDAO.deleteByFilter(gs=gs, msgID=msgID)

        # Read, Verify Deletion
        with self.gDB.sessionScope() as gs:
            for idx in range(nJobs):
                msgID = "{}{}".format(msgIDPrefix, idx)
                gJobModel = GlobalJobDAO.getOne(gs=gs, msgID=msgID, raiseOnNone=False)
                self.assertEqual(gJobModel, None)

    @logFunc()
    def test_duplicateMsgID(self):
        """
        Test adding duplicate msgID
        """
        try:
            msgID = "msgID"
            with self.gDB.sessionScope() as gs:
                GlobalJobDAO.create(
                    gs      = gs,
                    msgID   = msgID)
                GlobalJobDAO.create(
                    gs      = gs,
                    msgID   = msgID)
        except sqlalchemy.exc.IntegrityError as e:
            match = re.search(r"Duplicate entry .* for key 'uq_GlobalJob_msgID'", str(e))
            self.assertNotEqual(match, None)
        else:
            raise Exception("No exception raised")
