#!/usr/bin/env python3
"""
 NAME:
  aws.py

DESCRIPTION
 AWS Execution Context Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
from smeckn.server.eCtx import ExecutionContext


# ----------------------- AWS EXECUTION CONTEXT ----------------------------- #
class AWSExecutionContext(ExecutionContext):
    """
    AWS Execution Context Class
    """

    def __init__(self, event, context):
        """
        Initialize object
        Args:
          event:
            AWS Lambda Function event
          context:
            AWS Lambda context object
        """
        self._event  = event
        self.context = context

    # ------------------- GENERIC ------------------------------------------- #
    @property
    def functionRN(self):
        """
        Ex. arn:aws:lambda:us-east-1:225928776711:function:SmecknTestServerLSDeployment
        """
        return self.context.invoked_function_arn

    @property
    def location(self):
        """
        Ex. "AWS:SmecknTestServerLSDeployment"
        """
        return "AWS:{}".format(self.context.function_name)

    @property
    def event(self):
        return self._event

    @property
    def contextDict(self):
        return self.context.__dict__

    @property
    def timeRemainingMS(self):
        return self.context.get_remaining_time_in_millis()

    @property
    def requestID(self):
        """
        Ex. "e3573efe-5b68-57e9-8b6b-6aa1d81407f1"
        """
        return self.context.aws_request_id

    @property
    def logGroup(self):
        """
        Ex. "/aws/lambda/SmecknTestServerLSDeployment6"
        """
        return self.context.log_stream_name

    @property
    def logStream(self):
        """
        Ex. "2018/11/09/[$LATEST]9b58df40035a44118e5574dd052fa7a6"
        """
        return self.context.log_stream_name


    # ------------------- JOB MESSAGES -------------------------------------- #
    # Example SQS Event:
    # {
    #   "Records": [
    #      {
    #         "messageId": "c80e8021-a70a-42c7-a470-796e1186f753",
    #         "receiptHandle": "AQEBJQ+/u6NsnT5t8Q/VbVxgdUl4TMKZ5FqhksRdIQvLBhwNvADoBxYSOVeCBXdnS9P+erlTtwEALHsnBXynkfPLH3BOUqmgzP25U8kl8eHzq6RAlzrSOfTO8ox9dcp6GLmW33YjO3zkq5VRYyQlJgLCiAZUpY2D4UQcE5D1Vm8RoKfbE+xtVaOctYeINjaQJ1u3mWx9T7tork3uAlOe1uyFjCWU5aPX/1OHhWCGi2EPPZj6vchNqDOJC/Y2k1gkivqCjz1CZl6FlZ7UVPOx3AMoszPuOYZ+Nuqpx2uCE2MHTtMHD8PVjlsWirt56oUr6JPp9aRGo6bitPIOmi4dX0FmuMKD6u/JnuZCp+AXtJVTmSHS8IXt/twsKU7A+fiMK01NtD5msNgVPoe9JbFtlGwvTQ==",
    #         "body": "{\"foo\":\"bar\"}",
    #         "attributes": {
    #             "ApproximateReceiveCount": "3",
    #             "SentTimestamp": "1529104986221",
    #             "SenderId": "594035263019",
    #             "ApproximateFirstReceiveTimestamp": "1529104986230"
    #         },
    #         "messageAttributes": {},
    #         "md5OfBody": "9bb58f26192e4ba00f01e2e7b136bbd8",
    #         "eventSource": "aws:sqs",
    #         "eventSourceARN": "arn:aws:sqs:us-west-2:123456789012:MyQueue",
    #         "awsRegion": "us-west-2"
    #      }
    #   ]
    # }
    @property
    def jobMsgID(self):
        """
        Ex  "c80e8021-a70a-42c7-a470-796e1186f753"
        """
        return self.event.get("Records")[0].get("messageId")
