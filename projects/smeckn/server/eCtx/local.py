#!/usr/bin/env python3
"""
 NAME:
  local.py

DESCRIPTION
 Local Execution Context Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import uuid
from smeckn.server.eCtx import ExecutionContext
from smeckn.server.gc import gc


# ----------------------- LOCAL EXECUTION CONTEXT --------------------------- #
class LocalExecutionContext(ExecutionContext):
    """
    Local Execution Context Class
    """

    def __init__(self):
        """
        Initialize object
        """
        self.node  = uuid.getnode()
        self.uuid1 = uuid.uuid1(node=self.node)

    # ------------------- GENERIC ------------------------------------------- #
    @property
    def functionRN(self):
        return "LocalFunction"

    @property
    def location(self):
        return "Local"

    @property
    def event(self):
        return {}

    @property
    def contextDict(self):
        return {}

    @property
    def timeRemainingMS(self):
        return 86400000 # 1 Day

    @property
    def requestID(self):
        return "LocalRequest"

    @property
    def logGroup(self):
        return "LocalLogGroup"

    @property
    def logStream(self):
        """
        Ex. "/tmp/smecknServerCLI.log"
        """
        return gc.bLog.logFile

    # ------------------- JOB MESSAGES -------------------------------------- #
    @property
    def jobMsgID(self):
        """
        Ex  "Local:urn:uuid:d2a2438a-7471-11e8-a264-6045cb64d38b"
        """
        return "Local:{}".format(self.uuid1.urn)
