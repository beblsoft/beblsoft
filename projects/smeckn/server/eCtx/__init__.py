#!/usr/bin/env python3
"""
 NAME:
  __init__.py

DESCRIPTION
 Execution Context Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import abc
import threading
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.json.bObjectEncoder import BeblsoftJSONObjectEncoder


# ----------------------- GLOBALS ------------------------------------------- #
threadLocal = threading.local()


# ----------------------- EXECUTION CONTEXT --------------------------------- #
class ExecutionContext(BeblsoftJSONObjectEncoder, abc.ABC):
    """
    Execution Context Class

    Class provides common abstraction interface for different execution environments
    The context manager should be used to setup and teardown a context

    Execution context should be set whenever the context is started. Here is a sampling
      server/wAPIV1/decorators/requestWrapper.py
      server/aws/error.py
      server/aws/jobDLQueue.py
      server/aws/jobQueue.py
      server/manager/jobQueueLocal.py
      server/test/common.py
    """

    # ------------------- CONTEXT MANAGER ---------------------------------- #
    # Context Manager implementation
    # This allows an execution context to be used as follows:
    #
    # with AWSExecutionContext(event=event, context=context):
    #     print("Inside execution context")
    #
    def __enter__(self):
        """
        Enter the execution context
        See: https://docs.python.org/3/library/contextlib.html#using-a-context-manager-as-a-function-decorator
        """
        ExecutionContext.set(eCtx=self)

    def __exit__(self, excType, exc, excTB):
        """
        Exit the execution context
        See: https://docs.python.org/3/library/contextlib.html#using-a-context-manager-as-a-function-decorator
        """
        ExecutionContext.set(eCtx=None)

    # ------------------- THREAD LOCAL STORAGE ------------------------------ #
    _ECTX_THREAD_NAME = "__eCtx"

    @staticmethod
    @logFunc()
    def set(eCtx):
        """
        Set context
        """
        setattr(threadLocal, ExecutionContext._ECTX_THREAD_NAME, eCtx)
        return eCtx

    @staticmethod
    @logFunc()
    def get():
        """
        Get context
        """
        return getattr(threadLocal, ExecutionContext._ECTX_THREAD_NAME, None)

    @staticmethod
    @logFunc()
    def setLocal():
        """
        Set local context
        """
        from smeckn.server.eCtx.local import LocalExecutionContext
        eCtx = LocalExecutionContext()
        return ExecutionContext.set(eCtx=eCtx)

    @staticmethod
    @logFunc()
    def setAWS(event, context):
        """
        Set AWS context
        """
        from smeckn.server.eCtx.aws import AWSExecutionContext
        eCtx = AWSExecutionContext(event=event, context=context)
        return ExecutionContext.set(eCtx=eCtx)

    @staticmethod
    @logFunc()
    def setFromWSGIEnviron(environ):
        """
        Return from WSGI Environ
        """
        eCtx       = None
        awsEvent   = environ.get("awsEvent", None)
        awsContext = environ.get("awsContext", None)

        if awsEvent:
            eCtx = ExecutionContext.setAWS(event=awsEvent, context=awsContext)
        else:
            eCtx = ExecutionContext.setLocal()
        return eCtx

    # ------------------- JSON ENCODING ------------------------------------- #
    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        keyList = ["event", "contextDict"]
        return self.attributeKeysToDict(keyList=keyList)

    # ------------------- GENERIC ------------------------------------------- #
    @property
    @abc.abstractmethod
    def functionRN(self):
        """
        Return function resource name
        """
        pass

    @property
    @abc.abstractmethod
    def location(self):
        """
        Return location where execution is occuring
        """
        pass

    @property
    @abc.abstractmethod
    def event(self):
        """
        Return event dictionary
        """
        pass

    @property
    @abc.abstractmethod
    def contextDict(self):
        """
        Return context dictionary
        """
        pass

    @property
    @abc.abstractmethod
    def timeRemainingMS(self):
        """
        Return time remaining in milliseconds
        """
        pass

    @property
    @abc.abstractmethod
    def requestID(self):
        """
        Return request identifier
        """
        pass

    @property
    @abc.abstractmethod
    def logGroup(self):
        """
        Return log group name
        """
        pass

    @property
    @abc.abstractmethod
    def logStream(self):
        """
        Return log stream name
        """
        pass

    # ------------------- JOB MESSAGES -------------------------------------- #
    @property
    @abc.abstractmethod
    def jobMsgID(self):
        """
        Return job message ID
        """
        pass
