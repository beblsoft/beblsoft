#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Account Job Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import re
import abc
import logging
from datetime import datetime
import sqlalchemy
from flask import g
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.job import Job
from smeckn.server.aDB import AccountDatabase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.aDB.aJob.model import AccountJobState
from smeckn.server.aDB.aJob.dao import AccountJobDAO
from smeckn.server.aDB.accountInfo.dao import AccountInfoDAO
from smeckn.server.application.glob import ApplicationGlobal
from smeckn.server.log.sLogger import SmecknLogger


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ACCOUNT JOB ------------------------------------ #
class AccountJob(Job, abc.ABC):
    """
    Account Job Class
    """
    _JOB_THREAD_NAME = "__aJob"

    def __init__(self, aID, aDBSReadDomain, aDBSWriteDomain,
                 groupURN="", parentSMID=0, incarn=0, **jobKwargs):
        """
        Initialize object
        Args
          aID:
            Account ID
            Ex. 128
          aDBSReadDomain:
            Account Database Server read domain
            Ex. "localhost"
          aDBSWriteDomain:
            Account Database Server write domain
            Ex. "localhost"
          groupURN:
            Job group urn
            Helps correllate many jobs that are related
            Ex. UUID1: urn:uuid:3ca8e6bc-44ab-3e06-818e-ee5f0a9cb9c8
          parentSMID:
            Parent Job Smeckn ID
            Ex. 5
          incarn:
            Job Incarnation
            Tracks how many times this job has respawned itself
            Ex. 0
        """
        super().__init__(**jobKwargs)
        self.aID             = aID
        self.aDBSReadDomain  = aDBSReadDomain
        self.aDBSWriteDomain = aDBSWriteDomain
        self.groupURN        = groupURN
        self.parentSMID      = parentSMID
        self.incarn          = incarn

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        return {
            "name": self.__class__.__name__,
            "msgID": self.msgID,
            "groupURN": self.groupURN,
            "parentSMID": self.parentSMID,
            "incarn": self.incarn,
            "functionRN": self.eCtx.functionRN,
            "requestID": self.eCtx.requestID,
            "aID": self.aID,
        }

    # -------------------------- ABSTRACT METHODS --------------------------- #
    @abc.abstractmethod
    def execute(self, **kwargs):
        """
        Execute Job
        """
        pass

    # -------------------------- HELPER FUNCTIONS --------------------------- #
    @staticmethod
    @logFunc()
    def getClassKwargsFromADBS(aDBS, **kwargs):
        """
        Return class kwargs dictionary
        Args
          aDBS:
            Account Database Session
          kwargs:
            Extra kwargs to pass to class
        """
        aDB       = AccountDatabase.fromSession(session=aDBS)
        aDBServer = aDB.bServer
        return {
            "aID": aDB.aID,
            "aDBSReadDomain": aDBServer.domainName,
            "aDBSWriteDomain": aDBServer.domainName,
            **kwargs
        }

    @staticmethod
    @logFunc()
    def getClassKwargsFromFlask(**kwargs):
        """
        Return class kwargs dictionary
        Args
          kwargs:
            Extra kwargs to pass to class

        Requires the following flask variables be set:
          ApplicationGlobal.getAccountAD
            aID
            readDB
            writeDB
        """
        accountAD = ApplicationGlobal.getAccountAD()
        return {
            "aID": accountAD.aID,
            "aDBSReadDomain": accountAD.readDB,
            "aDBSWriteDomain": accountAD.writeDB,
            **kwargs
        }

    @staticmethod
    @logFunc()
    def getClassKwargsFromAccount(gDB, aID=None, email=None, **kwargs):
        """
        Return class kwargs dictionary
        Args
          aID:
            Account ID
          email:
            Account email
        """
        with gDB.sessionScope(commit=False) as gs:
            aModel     = None
            if aID:
                aModel = AccountDAO.getByID(gs=gs, aID=aID)
            elif email:
                aModel = AccountDAO.getByEmail(gs=gs, email=email)
            else:
                raise BeblsoftError(msg="Must specify aID or email")
            aDBSModel  = aModel.accountDBServer
            return {
                "aID": aModel.id,
                "aDBSReadDomain": aDBSModel.readDomain,
                "aDBSWriteDomain": aDBSModel.writeDomain,
                **kwargs
            }

    @logFunc()
    def setStateError(self):
        """
        Set State to Error
        Needs try/catch semantics in case database or table has been deleted
        """
        try:
            with AccountDatabase.sessionScopeFromAJob(aJob=self) as aDBS:
                aJobModel = AccountJobDAO.getOne(aDBS=aDBS, msgID=self.msgID)
                aJobModel.update(aDBS=aDBS, state=AccountJobState.ERROR)
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{}: Set State Error failed".format(self))

    # -------------------------- OVERRIDDEN METHODS ------------------------- #
    @logFunc()
    def setUp(self):
        """
        Setup Execution
        """
        rval = True
        try:
            SmecknLogger.info(msg="{} Setup".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.AJOB_SETUP)
            with AccountDatabase.sessionScopeFromAJob(aJob=self) as aDBS:
                AccountJobDAO.create(
                    aDBS        = aDBS,
                    msgID       = self.msgID,
                    groupURN    = self.groupURN,
                    parentSMID  = self.parentSMID,
                    incarn      = self.incarn,
                    state       = AccountJobState.START,
                    classModule = self.__class__.__module__,
                    className   = self.__class__.__name__,
                    functionRN  = self.eCtx.functionRN,
                    requestID   = self.eCtx.requestID,
                    logStream   = self.eCtx.logStream)
        except sqlalchemy.exc.IntegrityError as e:
            duplicate = re.search(r"Duplicate entry .* for key 'uq_AccountJob_msgID'", str(e))
            if duplicate:
                SmecknLogger.info(msg="{} Setup Duplicate".format(self.__class__.__name__),
                                  bLogCode=BeblsoftLogCode.AJOB_SETUP_DUPLICATE)
                rval = False
            else:
                raise e
        return rval

    @logFunc()
    def tearDown(self, exception=None):
        """
        Teardown Execution
        """
        try:
            SmecknLogger.info(msg="{} Teardown".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.AJOB_TEARDOWN)
            with AccountDatabase.sessionScopeFromAJob(aJob=self) as aDBS:
                AccountJobDAO.deleteByFilter(aDBS=aDBS, msgID=self.msgID)
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{} Teardown failed".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.AJOB_TEARDOWN_FAILED)

    @logFunc()
    def heartbeat(self):
        """
        Perform a heartbeat
        """
        try:
            with AccountDatabase.sessionScopeFromAJob(aJob=self) as aDBS:
                aJobModel = AccountJobDAO.getOne(aDBS=aDBS, msgID=self.msgID)
                AccountJobDAO.update(aDBS=aDBS, aJobModel=aJobModel, heartbeatDate=datetime.utcnow())
        except Exception as _:  # pylint: disable=W0703
            SmecknLogger.info(msg="{} Heartbeat failed".format(self.__class__.__name__),
                              bLogCode=BeblsoftLogCode.AJOB_HEARTBEAT_FAILED)

    @logFunc()
    def handleException(self, exception):
        """
        Handle an exception
        """
        noTable = False
        noDB    = False

        # Was the underlying Account Database Deleted?
        if isinstance(exception, sqlalchemy.exc.ProgrammingError):
            noTable = re.match(r".*Table '.*' doesn't exist.*", str(exception))
        if isinstance(exception, sqlalchemy.exc.OperationalError):
            noDB = re.match(r".*Unknown database '.*'.*", str(exception))

        if not (noTable or noDB):
            self.mgr.sentry.captureError(error=exception, bLogCode=BeblsoftLogCode.AJOB_EXCEPTION,
                                         logMsg="{} Exception".format(self.__class__.__name__),)
        self.setStateError()
