#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Analysis Worker Job Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import time
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aJob import AccountJob
from smeckn.server.profile.analysis.context import AnalysisContext


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS WORKER JOB ---------------------------- #
class AnalysisWorkerJob(AccountJob):
    """
    Analysis Worker Job Class
    """

    # ---------------------- EXECUTE ---------------------------------------- #
    @logFunc()
    def execute(self, analysisSMID, analyzeOneKwargList):  # pylint: disable=W0221
        """
        Execute job
        Args
          analysisSMID
            Analysis SMID
            Ex. 234
          analyzeOneKwargList
            List of kwargs to pass to AnalysisManager.analyzeOne
            Ex. [{"smID": 2398}, {"smID": 239487}]
        """
        with AccountDatabase.sessionScopeFromAJob(aJob=self, commit=False) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS, aJob=self)
            self.mgr.profile.analysis.worker.main(analysisCtx=analysisCtx, analysisSMID=analysisSMID,
                                                  analyzeOneKwargList=analyzeOneKwargList)
