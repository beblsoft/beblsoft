#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Sync Worker Job Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import time
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aJob import AccountJob
from smeckn.server.profile.sync.context import SyncContext


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC WORKER JOB -------------------------------- #
class SyncWorkerJob(AccountJob):
    """
    Sync Worker Job Class
    """

    # -------------------------- EXECUTE ------------------------------------ #
    @logFunc()
    def execute(self, syncContentSMID, nextDict=None):  # pylint: disable=W0221
        """
        Execute job
        Args
          syncContentSMID:
            SyncContent SMID
            Ex. 123
          nextDict:
            Dictionary of arguments to pass to ContentSyncManager syncChunk
            Ex1. None
            Ex2. {"nextLink" : "https://graph.facebook.com/...."}
        """
        with AccountDatabase.sessionScopeFromAJob(aJob=self, commit=False) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS, aJob=self)
            self.mgr.profile.sync.worker.main(syncCtx=syncCtx, syncContentSMID=syncContentSMID,
                                              nextDict=nextDict)
