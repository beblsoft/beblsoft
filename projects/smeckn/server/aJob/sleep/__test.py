#!/usr/bin/env python3
"""
 NAME:
  __test.py

 DESCRIPTION
  Sleep Job Test
"""


# ---------------------------- IMPORTS -------------------------------------- #
import time
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.aJob.dao import AccountJobDAO
from smeckn.server.aJob.sleep import SleepJob


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SleepJobTestCase(CommonTestCase):
    """
    Test Sleep Job
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)

    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        secondHB = None
        firstHB  = None
        aDB      = AccountDatabase.fromID(gDB=self.gDB, aID=self.popAccountID)

        # Kick off job
        thread   = SleepJob.invokeAsync(
            mgr           = self.mgr,
            classKwargs   = SleepJob.getClassKwargsFromAccount(gDB=self.gDB, aID=self.popAccountID,
                                                               heartbeatPeriodS=1),
            executeKwargs = {"sleepS": 8})
        time.sleep(2)

        # Ensure job in table
        with aDB.sessionScope() as aDBS:
            aJobModelList = AccountJobDAO.getAll(aDBS=aDBS, className="SleepJob")
            self.assertEqual(len(aJobModelList), 1)
            firstHB       = aJobModelList[0].heartbeatDate
        time.sleep(2)

        # Ensure heartbeat is larger
        with aDB.sessionScope() as aDBS:
            aJobModelList = AccountJobDAO.getAll(aDBS=aDBS, className="SleepJob")
            self.assertEqual(len(aJobModelList), 1)
            secondHB      = aJobModelList[0].heartbeatDate
            self.assertGreater(secondHB, firstHB)
        thread.join()

        # Ensure job deleted from table
        with aDB.sessionScope() as aDBS:
            nAJobModels = AccountJobDAO.getAll(aDBS=aDBS, className="SleepJob", count=True)
            self.assertEqual(nAJobModels, 0)
