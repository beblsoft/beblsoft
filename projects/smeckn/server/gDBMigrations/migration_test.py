#!/usr/bin/env python3
"""
 NAME:
  migration_test.py

 DESCRIPTION
  GDB Migration Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import unittest
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gc import gc


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)
cfg    = gc.server.cfg
mgr    = gc.server.mgr


# ---------------------------- CLASSES -------------------------------------- #
class MigrationTestCase(unittest.TestCase):
    """
    Test Migrations
    """

    @classmethod
    def setUpClass(cls):
        mgr.gdb.delete()

    @classmethod
    def tearDownClass(cls):
        mgr.gdb.delete(deleteAccounts=False)

    @logFunc()
    def setUp(self):
        self.cfg = cfg
        self.gDB = mgr.gdb.gDB

    @logFunc()
    def test_downgrade(self):
        """
        Test downgrading database
        """
        mgr.gdb.create()
        mgr.gdb.downgrade(toVersion="base")
        self.assertEqual(self.gDB.bDBMigrations.version, '')
        self.assertEqual(self.gDB.tables[0], 'alembic_version')
        self.assertEqual(len(self.gDB.tables), 1)

    @logFunc()
    def test_upgrade(self):
        """
        Test upgrading database
        """
        mgr.gdb.create()
        mgr.gdb.downgrade(toVersion="base")
        for version in self.gDB.bDBMigrations.history:
            mgr.gdb.upgrade(toVersion=version)
            self.assertEqual(self.gDB.bDBMigrations.version, version)
