"""
Minor_changes

Revision ID: e40f2bfd511e
Revises: 264b6dcdf5dc
Create Date: 2019-02-21 09:48:58.431504-05:00
"""

# ----------------------------- IMPORTS ------------------------------------- #
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# ----------------------------- GLOBALS ------------------------------------- #
revision      = 'e40f2bfd511e'
down_revision = '264b6dcdf5dc'
branch_labels = None
depends_on    = None


# ----------------------------- UPGRADE ------------------------------------- #
def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('Account', 'accountDBServerID',
               existing_type=mysql.BIGINT(display_width=20),
               nullable=False)
    # ### end Alembic commands ###


# ----------------------------- DOWNGRADE ----------------------------------- #
def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('Account', 'accountDBServerID',
               existing_type=mysql.BIGINT(display_width=20),
               nullable=True)
    # ### end Alembic commands ###
