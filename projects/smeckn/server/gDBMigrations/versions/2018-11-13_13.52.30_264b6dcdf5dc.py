"""
Initial_tables

Revision ID: 264b6dcdf5dc
Revises: 
Create Date: 2018-11-13 13:52:30.211719-05:00
"""

# ----------------------------- IMPORTS ------------------------------------- #
from alembic import op
import sqlalchemy as sa



# ----------------------------- GLOBALS ------------------------------------- #
revision      = '264b6dcdf5dc'
down_revision = None
branch_labels = None
depends_on    = None


# ----------------------------- UPGRADE ------------------------------------- #
def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('AccountDBServer',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('writeDomain', sa.String(length=256), nullable=True),
    sa.Column('readDomain', sa.String(length=256), nullable=True),
    sa.Column('user', sa.String(length=256), nullable=True),
    sa.Column('password', sa.String(length=256), nullable=True),
    sa.Column('port', sa.BigInteger(), nullable=True),
    sa.Column('state', sa.Enum('ONLINE', 'ONLINE_RO', 'OFFLINE', name='accountdbserverstate'), nullable=True),
    sa.Column('creationDate', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_AccountDBServer')),
    sa.UniqueConstraint('readDomain', name=op.f('uq_AccountDBServer_readDomain')),
    sa.UniqueConstraint('writeDomain', name=op.f('uq_AccountDBServer_writeDomain'))
    )
    op.create_table('GlobalJob',
    sa.Column('smID', sa.BigInteger(), nullable=False),
    sa.Column('msgID', sa.String(length=256), nullable=True),
    sa.Column('groupURN', sa.String(length=128), nullable=True),
    sa.Column('parentSMID', sa.BigInteger(), nullable=True),
    sa.Column('incarn', sa.Integer(), nullable=True),
    sa.Column('state', sa.Enum('START', 'ERROR', name='globaljobstate'), nullable=True),
    sa.Column('classModule', sa.String(length=256), nullable=True),
    sa.Column('className', sa.String(length=256), nullable=True),
    sa.Column('creationDate', sa.DateTime(), nullable=True),
    sa.Column('heartbeatDate', sa.DateTime(), nullable=True),
    sa.Column('functionRN', sa.String(length=128), nullable=True),
    sa.Column('requestID', sa.String(length=128), nullable=True),
    sa.Column('logStream', sa.String(length=128), nullable=True),
    sa.PrimaryKeyConstraint('smID', name=op.f('pk_GlobalJob')),
    sa.UniqueConstraint('msgID', name=op.f('uq_GlobalJob_msgID'))
    )
    op.create_table('Account',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('email', sa.String(length=256), nullable=True),
    sa.Column('passwordHash', sa.String(length=256), nullable=True),
    sa.Column('type', sa.Enum('REGULAR', 'ADMIN', 'TEST', name='accounttype'), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('creationDate', sa.DateTime(), nullable=True),
    sa.Column('dbNamePrefix', sa.String(length=128), nullable=True),
    sa.Column('dbState', sa.Enum('CREATING', 'CREATED', 'DELETING', 'DELETED', name='accountdbstate'), nullable=True),
    sa.Column('dbVersion', sa.String(length=128), nullable=True),
    sa.Column('accountDBServerID', sa.BigInteger(), nullable=True),
    sa.ForeignKeyConstraint(['accountDBServerID'], ['AccountDBServer.id'], name=op.f('fk_Account_accountDBServerID_AccountDBServer')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_Account')),
    sa.UniqueConstraint('email', name=op.f('uq_Account_email'))
    )
    # ### end Alembic commands ###


# ----------------------------- DOWNGRADE ----------------------------------- #
def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('Account')
    op.drop_table('GlobalJob')
    op.drop_table('AccountDBServer')
    # ### end Alembic commands ###
