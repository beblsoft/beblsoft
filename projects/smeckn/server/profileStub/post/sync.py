#!/usr/bin/env python3
"""
 NAME:
  post.py

 DESCRIPTION
  Post Sync Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.profile.sync.common import CommonContentSyncManager
from smeckn.server.profile.sync.models import SyncContentProgressModel
from smeckn.server.aDB.sync.model import SyncStatus


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST SYNC MANAGER ------------------------------ #
class PostSyncManager(CommonContentSyncManager):
    """
    Post Sync Manager Class
    """

    # ---------------------- IS LIMIT REACHED ------------------------------- #
    @logFunc()
    def isLimitReached(self, syncCtx):
        """
        Return True if limit has been reached
        """
        return False

    # ---------------------- DELETE STALE ----------------------------------- #
    @logFunc()
    def deleteStale(self, syncCtx):
        """
        Delete stale posts
        """
        pass

    # ---------------------- GET PROGRESS ----------------------------------- #
    @logFunc()
    def getProgress(self, syncCtx):
        """
        Return sync progress
        """
        return SyncContentProgressModel(contentType=self.contentType,
                                        count=self.cfg.PROFILE_STUB_SYNC_POST_PROGRESS_COUNT,
                                        status=syncCtx.syncContentModel.status)

    # ---------------------- SYNC CHUNK ------------------------------------- #
    @logFunc()
    def syncChunk(self, syncCtx, nextDict):  # pylint: disable=W0221
        """
        Sync a chunk of content

        Returns:
          (SyncStatus, nextDict)
        """
        status   = SyncStatus.SUCCESS
        nextDict = {}

        return (status, nextDict)
