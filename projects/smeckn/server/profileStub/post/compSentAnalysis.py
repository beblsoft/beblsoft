#!/usr/bin/env python3
"""
 NAME:
  compSentAnalysis.py

 DESCRIPTION
  Post Comprehend Sentiment Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.profile.analysis.common import CommonContentAnalysisManager
from smeckn.server.profile.analysis.models import AnalysisProgressModel
from smeckn.server.profile.analysis.models import AnalysisCountModel
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.analysis.model import AnalysisType


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST COMP SENT ANALYSIS MANAGER ---------------- #
class PostCompSentAnalysisManager(CommonContentAnalysisManager):
    """
    Post Comp Sent Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.countDone              = 50
        self.countNotDone           = 40
        self.countCant              = 30
        self.countAnalyzed          = 50
        self.lenAnalyzeOneKwargList = 50

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def analysisType(self):
        return AnalysisType.COMPREHEND_SENTIMENT

    @property
    def unitType(self):
        return UnitType.TEXT_ANALYSIS

    @property
    def nAnalysesPerWorker(self):
        return self.cfg.PROFILE_STUB_POST_COMPSENTA_NPERWORKER

    # ---------------------- GET COUNT MODEL -------------------------------- #
    @logFunc()
    def getCountModel(self, analysisCtx):
        """
        Return AnalysisCountModel
        """
        return AnalysisCountModel(profileSMID=analysisCtx.profileSMID,
                                  contentType=self.contentType,
                                  analysisType=self.analysisType,
                                  countDone=self.countDone,
                                  countNotDone=self.countNotDone,
                                  countCant=self.countCant)

    # ---------------------- GET PROGRESS ----------------------------------- #
    @logFunc()
    def getProgress(self, analysisCtx):
        """
        Return AnalysisProgressModel
        """
        return AnalysisProgressModel(contentType=self.contentType,
                                     analysisType=self.analysisType,
                                     countAnalyzed=self.countAnalyzed,
                                     countTotal=analysisCtx.analysisModel.contentCount,
                                     status=analysisCtx.analysisModel.status)

    # ---------------------- GET ANALYZE ONE KWARG LIST LIST ---------------- #
    @logFunc()
    def getAnalyzeOneKwargList(self, analysisCtx):
        """
        Returns
          List of analyzeOne kwargs
        """
        return [{"postSMID": idx} for idx in range(self.lenAnalyzeOneKwargList)]

    # ---------------------- ANALYZE ONE ------------------------------------ #
    @logFunc()
    def analyzeOne(self, analysisCtx, postSMID):  # pylint: disable=W0221
        """
        Analyze one piece of content
        """
        pass
