#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Profile Stub Post Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.profile.content.common import CommonContentManager
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.profileStub.post.sync import PostSyncManager
from smeckn.server.profileStub.post.compSentAnalysis import PostCompSentAnalysisManager
from smeckn.server.aDB.statistic.stubDAO import StubStatisticDAO
from smeckn.server.aDB.histogram.stubDAO import StubHistogramDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST MANAGER ----------------------------------- #
class PostManager(CommonContentManager):
    """
    Post Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.sync             = PostSyncManager(mgr=self.mgr, contentMgr=self)
        self.compSentAnalysis = PostCompSentAnalysisManager(mgr=self.mgr, contentMgr=self)


    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def contentType(self):
        return ContentType.POST

    @property
    def syncManager(self):
        return self.sync

    @property
    def analysisManagerList(self):
        return [self.compSentAnalysis]

    @property
    def statisticDAO(self):
        return StubStatisticDAO

    @property
    def histogramDAO(self):
        return StubHistogramDAO
