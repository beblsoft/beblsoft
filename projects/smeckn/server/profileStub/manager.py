#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Profile Stub Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.profile.common import CommonProfileManager
from smeckn.server.aDB.profile.model import ProfileType
from smeckn.server.profileStub.post.manager import PostManager
from smeckn.server.aDB.sync.model import SyncStatus


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- PROFILE STUB MANAGER --------------------------- #
class ProfileStubManager(CommonProfileManager):
    """
    Profile Stub Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Content
        self.post = PostManager(mgr=self.mgr)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def profileType(self):
        return ProfileType.BASE

    @property
    def contentManagerList(self):
        return [self.post]

    # ---------------------- SYNC ------------------------------------------- #
    @property
    def syncRestartDelta(self):
        return self.cfg.PROFILE_STUB_SYNC_RESTART_TIMEDELTA

    @logFunc()
    def isSyncStartable(self, syncCtx):
        return (True, SyncStatus.IN_PROGRESS)

    @logFunc()
    def sync(self, syncCtx):
        return (True, SyncStatus.IN_PROGRESS)
