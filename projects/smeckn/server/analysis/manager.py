#!/usr/bin/env python3
"""
 NAME:
   manager.py

 DESCRIPTION
   Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.manager.common import CommonManager
from smeckn.server.analysis.text.manager import TextAnalysisManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS MANAGER ------------------------------- #
class AnalysisManager(CommonManager):
    """
    Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.text = TextAnalysisManager(mgr=self.mgr)
