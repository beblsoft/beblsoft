#!/usr/bin/env python3
"""
 NAME:
   compSent.py

 DESCRIPTION
   Comprehend Sentiment Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from base.bebl.python.language.bLanguage import BeblsoftLanguage
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.log.sLogger import SmecknLogger


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMP SENT ANALYSIS MANAGER --------------------- #
class CompSentAnalysisManager(CommonManager):
    """
    Comp Sent Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.bComprehendClient = BeblsoftComprehendClient(bRegion=self.cfg.AWS_BREGION)

    @logFunc()
    def analyze(self, aDBS, textSMID, unitLedgerHoldSMID=None):
        """
        Analyze text

        Returns
          TextModel
        """
        textModel                 = TextDAO.getBySMID(aDBS=aDBS, smID=textSMID)
        compLangAnalysisModel     = None
        compSentAnalysisModel     = textModel.compSentAnalysisModel
        bLanguage                 = None
        unitLedgerHoldModel       = None
        createKwargs              = {"aDBS": aDBS, "textModel": textModel}
        unitType                  = UnitType.TEXT_ANALYSIS
        keepAnalyzing             = True

        # Check if sentiment already successfully analyzed
        if keepAnalyzing:
            status                = compSentAnalysisModel.status if compSentAnalysisModel else None
            keepAnalyzing         = status != CompLangAnalysisStatus.SUCCESS

        # Analyze language
        if keepAnalyzing:
            textModel             = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=textSMID)
            compLangAnalysisModel = textModel.compLangAnalysisModel
            keepAnalyzing         = compLangAnalysisModel != None

        # Load/create UnitLedgerHoldModel
        if keepAnalyzing:
            if unitLedgerHoldSMID:
                unitLedgerHoldModel = UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=unitLedgerHoldSMID)
            else:
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    unitLedgerHoldModel = UnitLedgerDAO.createHold(aDBS=aDBS, unitType=unitType, ledgerCount=1)

        # Delete any existing unsuccessful sentiment
        if keepAnalyzing and compSentAnalysisModel:
            with AccountDatabase.transactionScope(session=aDBS):
                CompSentAnalysisDAO.deleteBySMID(aDBS=aDBS, smID=compSentAnalysisModel.smID)

        # Check for bad language
        if keepAnalyzing and compLangAnalysisModel.status != CompLangAnalysisStatus.SUCCESS:
            with AccountDatabase.transactionScope(session=aDBS):
                CompSentAnalysisDAO.create(**createKwargs, status=CompSentAnalysisStatus.COMPREHEND_FAILED)
                keepAnalyzing = False

        # Check for unsupported language
        if keepAnalyzing:
            bLanguageType = compLangAnalysisModel.dominantCompLangModel.bLanguageType
            bLanguage     = BeblsoftLanguage.fromType(bLanguageType=bLanguageType)
            if bLanguageType not in self.bComprehendClient.detectSentimentSupportedBLanguageTypeList:
                with AccountDatabase.transactionScope(session=aDBS):
                    CompSentAnalysisDAO.create(**createKwargs, status=CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE)
                    keepAnalyzing = False

        # Do analysis and commit to database
        if keepAnalyzing:
            with AccountDatabase.transactionScope(session=aDBS):
                try:
                    bCompDetectSentimentOutput = self.bComprehendClient.detectSentiment(text=textModel.text,
                                                                                        bLanguage=bLanguage)
                    SmecknLogger.info(msg="Comprehend Sentiment Analyzed",
                                      bLogCode=BeblsoftLogCode.COMPREHEND_SENTIMENT_ANALYZED,
                                      text=textModel.text, bCompDetectSentimentOutput=bCompDetectSentimentOutput)
                    CompSentAnalysisDAO.createFromBCompDetectSentimentOutput(
                        **createKwargs, bCompDetectSentimentOutput=bCompDetectSentimentOutput,
                        status=CompSentAnalysisStatus.SUCCESS)
                except Exception as e:  # pylint: disable=W0703
                    self.mgr.sentry.captureError(error=e, bLogCode=BeblsoftLogCode.COMPREHEND_SENTIMENT_FAILED)
                    CompSentAnalysisDAO.create(**createKwargs, status=CompSentAnalysisStatus.COMPREHEND_FAILED)

        # Create deduction
        if keepAnalyzing:
            with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType, ledgerCount=-1,
                                              holdSMID=unitLedgerHoldModel.smID)

        return textModel
