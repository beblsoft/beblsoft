#!/usr/bin/env python3
"""
 NAME:
  compSent_test.py

 DESCRIPTION
  Comprehend Sentiment Analysis Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus, CompSentType
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.aDB.compLang.dao import CompLangDAO
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CompSentAnalysisTestCase(CommonTestCase):
    """
    Test Comprehend Sentiment Analysis
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, text=True)

    @logFunc()
    def test_analyzeSuccess(self):
        """
        Test successful analysis
        """
        text = "Love, peace, kindness, joy are beautiful!"  # Positive

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                textModel = TextDAO.createFromText(aDBS=aDBS, text=text, fromUser=True)

            textModel = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=textModel.smID)
            compSentAnalysisModel = textModel.compSentAnalysisModel
            self.assertGreaterEqual(compSentAnalysisModel.createDate, self.testStartDate)
            self.assertEqual(compSentAnalysisModel.status, CompSentAnalysisStatus.SUCCESS)
            self.assertEqual(compSentAnalysisModel.dominantSentiment, CompSentType.POSITIVE)
            self.assertGreaterEqual(compSentAnalysisModel.positiveScore, .9)
            self.assertLessEqual(compSentAnalysisModel.negativeScore, .1)
            self.assertLessEqual(compSentAnalysisModel.neutralScore, .1)
            self.assertLessEqual(compSentAnalysisModel.mixedScore, .1)

    @logFunc()
    def test_analyzeSuccessExistingHold(self):
        """
        Test successful analysis with existing hold
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            unitLedgerHoldSMID  = self.unitTypeDict[UnitType.TEXT_ANALYSIS].unitLedgerHoldSMID
            unitLedgerHoldModel = UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=unitLedgerHoldSMID)
            oldHoldCount        = unitLedgerHoldModel.ledgerCount
            _                   = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                          unitLedgerHoldSMID=unitLedgerHoldSMID)
            self.assertEqual(unitLedgerHoldModel.ledgerCount, oldHoldCount - 1)

    @logFunc()
    def test_analyzeSuccessNoHold(self):
        """
        Test successful analysis with existing hold
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            unitType            = UnitType.TEXT_ANALYSIS
            oldUnitBalance      = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
            _                   = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=self.popTextSMID)
            newUnitBalance      = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
            self.assertEqual(newUnitBalance.nTotal, oldUnitBalance.nTotal - 1)
            self.assertEqual(newUnitBalance.nHeld, oldUnitBalance.nHeld)
            self.assertEqual(newUnitBalance.nAvailable, oldUnitBalance.nAvailable - 1)

    @logFunc()
    def test_analyzeBadTextSMID(self):
        """
        Test with bad text SMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with self.assertRaises(BeblsoftError) as cm:
                self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=23498)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)

    @logFunc()
    def test_analyzeAlreadyDone(self):
        """
        Test with analysis already done
        Verify that the successful compSentAnalysisModel isn't deleted
        """
        compSentAnalysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                compSentAnalysisModel = CompSentAnalysisDAO.create(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                   status=CompSentAnalysisStatus.SUCCESS)
            oldCompSentAnalysisSMID = compSentAnalysisModel.smID
            textModel = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=self.popTextSMID)

            compSentAnalysisModel = textModel.compSentAnalysisModel
            self.assertEqual(oldCompSentAnalysisSMID, compSentAnalysisModel.smID)

    @logFunc()
    def test_analyzeOldFailureDeleted(self):
        """
        Test that old failure is deleted
        """
        compSentAnalysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                compSentAnalysisModel = CompSentAnalysisDAO.create(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                   status=CompSentAnalysisStatus.COMPREHEND_FAILED)
            oldCompSentAnalysisSMID = compSentAnalysisModel.smID
            textModel = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=self.popTextSMID)

            compSentAnalysisModel = textModel.compSentAnalysisModel
            self.assertNotEqual(oldCompSentAnalysisSMID, compSentAnalysisModel.smID)

    @logFunc()
    def test_analyzeUnsupportedLanguage(self):
        """
        Test that old failure is deleted
        """
        compSentAnalysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                   status=CompLangAnalysisStatus.SUCCESS)
                _                     = CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                                                           bLanguageType=BeblsoftLanguageType.Chinese, score=.7)
            textModel = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=self.popTextSMID)
            compSentAnalysisModel = textModel.compSentAnalysisModel
            self.assertEqual(compSentAnalysisModel.status, CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE)

    @logFunc()
    def test_analyzeComprehendException(self):
        """
        Test analyze with comprehend exception
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.mgr.analysis.text.compSent.bComprehendClient, "detectSentiment", autospec=True) as detectSentMock:
            detectSentMock.side_effect = Exception

            textModel = self.mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=self.popTextSMID)
            compSentAnalysisModel = textModel.compSentAnalysisModel
            self.assertEqual(compSentAnalysisModel.status, CompSentAnalysisStatus.COMPREHEND_FAILED)
