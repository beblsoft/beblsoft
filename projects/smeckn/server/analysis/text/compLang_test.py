#!/usr/bin/env python3
"""
 NAME:
  compLang_test.py

 DESCRIPTION
  Comprehend Language Analysis Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.aDB.text.dao import TextDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CompLangAnalysisTestCase(CommonTestCase):
    """
    Test Comprehend Language Analysis
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True)

    @logFunc()
    def test_analyzeSuccess(self):
        """
        Test successful analysis
        """
        text = "This is the most english text ever!"
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                textModel = TextDAO.createFromText(aDBS=aDBS, text=text, fromUser=True)

            textModel = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=textModel.smID)

            compLangAnalysisModel = textModel.compLangAnalysisModel
            self.assertGreaterEqual(compLangAnalysisModel.createDate, self.testStartDate)
            self.assertEqual(compLangAnalysisModel.status, CompLangAnalysisStatus.SUCCESS)
            dominantCompLangModel = compLangAnalysisModel.dominantCompLangModel
            self.assertEqual(dominantCompLangModel.bLanguageType, BeblsoftLanguageType.English)


    @logFunc()
    def test_analyzeBadTextSMID(self):
        """
        Test with bad text SMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with self.assertRaises(BeblsoftError) as cm:
                self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=23498)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)

    @logFunc()
    def test_analyzeAlreadyDone(self):
        """
        Test with analysis already done
        Verify that the successful compLangAnalysisModel isn't deleted
        """
        compLangAnalysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                   status=CompLangAnalysisStatus.SUCCESS)
            oldCompLangAnalysisSMID = compLangAnalysisModel.smID
            textModel = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=self.popTextSMID)

            compLangAnalysisModel = textModel.compLangAnalysisModel
            self.assertEqual(oldCompLangAnalysisSMID, compLangAnalysisModel.smID)

    @logFunc()
    def test_analyzeOldFailureDeleted(self):
        """
        Test that old failure is deleted
        """
        compLangAnalysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textSMID=self.popTextSMID,
                                                                   status=CompLangAnalysisStatus.COMPREHEND_FAILED)
            oldCompLangAnalysisSMID = compLangAnalysisModel.smID
            textModel = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=self.popTextSMID)

            compLangAnalysisModel = textModel.compLangAnalysisModel
            self.assertNotEqual(oldCompLangAnalysisSMID, compLangAnalysisModel.smID)

    @logFunc()
    def test_analyzeComprehendException(self):
        """
        Test analyze with comprehend exception
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.mgr.analysis.text.compLang.bComprehendClient, "detectDominantLanguage", autospec=True) as detectLanguageMock:
            detectLanguageMock.side_effect=Exception

            textModel = self.mgr.analysis.text.compLang.analyze(aDBS=aDBS, textSMID=self.popTextSMID)
            compLangAnalysisModel = textModel.compLangAnalysisModel
            self.assertEqual(compLangAnalysisModel.status, CompLangAnalysisStatus.COMPREHEND_FAILED)
