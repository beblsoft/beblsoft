#!/usr/bin/env python3
"""
 NAME:
   manager.py

 DESCRIPTION
   Text Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.manager.common import CommonManager
from smeckn.server.analysis.text.compLang import CompLangAnalysisManager
from smeckn.server.analysis.text.compSent import CompSentAnalysisManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- TEXT ANALYSIS MANAGER -------------------------- #
class TextAnalysisManager(CommonManager):
    """
    Text Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.compLang = CompLangAnalysisManager(mgr=self.mgr)
        self.compSent = CompSentAnalysisManager(mgr=self.mgr)
