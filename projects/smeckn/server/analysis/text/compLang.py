#!/usr/bin/env python3
"""
 NAME:
   compLang.py

 DESCRIPTION
   Comprehend Language Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.log.sLogger import SmecknLogger


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMP LANG ANALYSIS MANAGER --------------------- #
class CompLangAnalysisManager(CommonManager):
    """
    Comp Lang Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.bComprehendClient = BeblsoftComprehendClient(bRegion=self.cfg.AWS_BREGION)

    @logFunc()
    def analyze(self, aDBS, textSMID):
        """
        Analyze text language and commit to database
         - Nothing done if analysis already successfully present
         - Language detected

        Args
          textSMID:
            Text Smeckn ID
          nestedTransaction:
            If True, do all database work inside a nested transaction

        Returns
          TextModel
        """
        textModel                 = TextDAO.getBySMID(aDBS=aDBS, smID=textSMID)
        keepAnalyzing             = True
        compLangAnalysisModel     = None

        # Check for previous successful CompLangAnalysis
        # If one exists, don't reanalyze
        if keepAnalyzing:
            compLangAnalysisModel = textModel.compLangAnalysisModel
            status                = compLangAnalysisModel.status if compLangAnalysisModel else None
            keepAnalyzing         = status != CompLangAnalysisStatus.SUCCESS

        # Delete any existing unsuccessful CompLangAnalysis
        if keepAnalyzing and compLangAnalysisModel:
            with AccountDatabase.transactionScope(session=aDBS):
                CompLangAnalysisDAO.deleteBySMID(aDBS=aDBS, smID=compLangAnalysisModel.smID)

        # Do analysis and commit
        if keepAnalyzing:
            with AccountDatabase.transactionScope(session=aDBS):
                try:
                    bCompDetectLanguageOutput = self.bComprehendClient.detectDominantLanguage(text=textModel.text)
                    SmecknLogger.info(msg="Comprehend Language Analyzed",
                                      bLogCode=BeblsoftLogCode.COMPREHEND_LANGUAGE_ANALYZED,
                                      text=textModel.text, bCompDetectLanguageOutput=bCompDetectLanguageOutput)
                    compLangAnalysisModel     = CompLangAnalysisDAO.createFromBCompDetectLanguageOutput(
                        aDBS=aDBS, bCompDetectLanguageOutput=bCompDetectLanguageOutput,
                        textModel=textModel, status=CompLangAnalysisStatus.SUCCESS)
                except Exception as e:  # pylint: disable=W0703
                    self.mgr.sentry.captureError(error=e, bLogCode=BeblsoftLogCode.COMPREHEND_LANGUAGE_FAILED)
                    status = CompLangAnalysisStatus.COMPREHEND_FAILED
                    compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                                                       status=status)

        return textModel
