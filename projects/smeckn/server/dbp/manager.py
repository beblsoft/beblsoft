#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Database Population Manager Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bServer import BeblsoftMySQLServer
from smeckn.server.manager.common import CommonManager
from smeckn.server.gDB import GlobalDatabase
from smeckn.server.gDB.accountDBServer.populater import AccountDBServerPopulater
from smeckn.server.gDB.account.populater import AccountPopulater
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.aDB.profileGroup.populater import ProfileGroupPopulater
from smeckn.server.aDB.profile.populater import ProfilePopulater
from smeckn.server.aDB.sync.populater import SyncPopulater
from smeckn.server.aDB.syncContent.populater import SyncContentPopulater
from smeckn.server.aDB.analysis.populater import AnalysisPopulater
from smeckn.server.aDB.facebookProfile.populater import FacebookProfilePopulater
from smeckn.server.aDB.facebookPost.populater import FacebookPostPopulater
from smeckn.server.aDB.facebookPhoto.populater import FacebookPhotoPopulater
from smeckn.server.aDB.text.populater import TextPopulater
from smeckn.server.aDB.photo.populater import PhotoPopulater
from smeckn.server.aDB.video.populater import VideoPopulater
from smeckn.server.aDB.link.populater import LinkPopulater
from smeckn.server.aDB.compLangAnalysis.populater import CompLangAnalysisPopulater
from smeckn.server.aDB.compLang.populater import CompLangPopulater
from smeckn.server.aDB.compSentAnalysis.populater import CompSentAnalysisPopulater
from smeckn.server.aDB.unitLedger.populater import UnitLedgerPopulater


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- DATABASE POPULATION MANAGER CLASS ----------- #
class DatabasePopulationManager(CommonManager):
    """
    Database Population Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.gDBS              = BeblsoftMySQLServer(
            domainNameFunc          = lambda: self.cfg.GDB_DOMAIN,
            user                    = self.cfg.GDB_USER,
            password                = self.cfg.GDB_PASSWORD)
        self.gDB               = GlobalDatabase(
            bServer                 = self.gDBS,
            name                    = self.cfg.GDB_NAME,
            echo                    = self.cfg.GDB_ECHO)
        self.aDBNamePrefix     = self.cfg.ADB_NAME_PREFIX

        # Populaters
        self.accountDBServerP  = AccountDBServerPopulater(mgr=self.mgr, gDB=self.gDB)
        self.accountP          = AccountPopulater(mgr=self.mgr, gDB=self.gDB)
        self.profileGroupP     = ProfileGroupPopulater(mgr=self.mgr, gDB=self.gDB)
        self.profileP          = ProfilePopulater(mgr=self.mgr, gDB=self.gDB)
        self.syncP             = SyncPopulater(mgr=self.mgr, gDB=self.gDB)
        self.syncContentP      = SyncContentPopulater(mgr=self.mgr, gDB=self.gDB)
        self.analysisP         = AnalysisPopulater(mgr=self.mgr, gDB=self.gDB)
        self.facebookProfileP  = FacebookProfilePopulater(mgr=self.mgr, gDB=self.gDB)
        self.facebookPostP     = FacebookPostPopulater(mgr=self.mgr, gDB=self.gDB)
        self.facebookPhotoP    = FacebookPhotoPopulater(mgr=self.mgr, gDB=self.gDB)
        self.textP             = TextPopulater(mgr=self.mgr, gDB=self.gDB)
        self.photoP            = PhotoPopulater(mgr=self.mgr, gDB=self.gDB)
        self.videoP            = VideoPopulater(mgr=self.mgr, gDB=self.gDB)
        self.linkP             = LinkPopulater(mgr=self.mgr, gDB=self.gDB)
        self.compLangAnalysisP = CompLangAnalysisPopulater(mgr=self.mgr, gDB=self.gDB)
        self.compLangP         = CompLangPopulater(mgr=self.mgr, gDB=self.gDB)
        self.compSentAnalysisP = CompSentAnalysisPopulater(mgr=self.mgr, gDB=self.gDB)
        self.unitLedgerP       = UnitLedgerPopulater(mgr=self.mgr, gDB=self.gDB)

    @logFunc()
    def create(self, admin=False, accounts=False):
        """
        Create objects
        """
        if admin:
            self.createAdmin()
        if accounts:
            self.createAccounts()

    @logFunc()
    def delete(self, admin=False, accounts=False, allAccounts=False):
        """
        Delete objects
        """
        if admin:
            self.deleteAdmin()
        if accounts:
            self.deleteAccounts()
        if allAccounts:
            self.deleteAllAccounts()

    @logFunc()
    def describe(self, admin=False, accounts=False, allAccounts=False):
        """
        Describe objects
        """
        if admin:
            self.describeAdmin()
        if accounts:
            self.describeAccounts()
        if allAccounts:
            self.describeAllAccounts()

    # ------------------------ ADMIN ---------------------------------------- #
    @logFunc()
    def createAdmin(self):
        """
        Create Admin
        """
        self.accountP.populateList(emailList=[self.cfg.DBP_ADMIN_EMAIL],
                                   passwordList=[self.cfg.DBP_ADMIN_PASSWORD],
                                   aTypeList=[AccountType.ADMIN])

    @logFunc()
    def deleteAdmin(self):
        """
        Delete Admin
        """
        self.accountP.depopulateByEmail(email=self.cfg.DBP_ADMIN_EMAIL)

    @logFunc()
    def describeAdmin(self):
        """
        Describe Admin
        """
        self.accountP.describeByEmail(email=self.cfg.DBP_ADMIN_EMAIL, tree=False)

    # ------------------------ PREPOPULATED ACCOUNTS ------------------------ #
    @logFunc()
    def createAccounts(self):
        """
        Create Accounts
        """
        self.accountP.populateList(emailList=self.cfg.DBP_ACCOUNT_EMAIL_LIST,
                                   passwordList=self.cfg.DBP_ACCOUNT_PASSWORD_LIST,
                                   aTypeList=self.cfg.DBP_ACCOUNT_TYPE_LIST)

    @logFunc()
    def deleteAccounts(self):
        """
        Delete Accounts
        """
        for email in self.cfg.DBP_ACCOUNT_EMAIL_LIST:
            self.accountP.depopulateByEmail(email=email)

    @logFunc()
    def describeAccounts(self):
        """
        Describe Accounts
        """
        for email in self.cfg.DBP_ACCOUNT_EMAIL_LIST:
            self.accountP.describeByEmail(email=email, tree=False)

    # ------------------------ ALL ACCOUNTS --------------------------------- #
    @logFunc()
    def deleteAllAccounts(self, aType=None):
        """
        Delete aal accounts
        """
        self.accountP.depopulateAll(aType=aType)
        self.accountP.cleanAll(aDBNamePrefix=self.cfg.ADB_NAME_PREFIX)

    @logFunc()
    def describeAllAccounts(self):
        """
        Describe all accounts
        """
        self.accountP.describeAll(tree=False)
