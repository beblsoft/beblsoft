#!/usr/bin/env python3
"""
 NAME
  common.py

 DESCRIPTION
  Common Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMMON POPULATER CLASS ---------------------------- #
class CommonPopulater():
    """
    Common Populater
    """

    def __init__(self, mgr, gDB):
        """
        Initialize object
        """
        self.mgr = mgr
        self.cfg = mgr.cfg
        self.gDB = gDB
