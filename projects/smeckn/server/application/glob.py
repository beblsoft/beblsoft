#!/usr/bin/env python3
"""
 NAME:
  glob.py

 DESCRIPTION
  Application Global Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- APPLICATION GLOBAL ----------------------------- #
class ApplicationGlobal(CommonManager):
    """
    Application Global class
    """

    # -------------------------- FLASK GLOBAL ------------------------------- #
    @staticmethod
    def getG():
        """
        Return flask global
        """
        try:
            from flask import g
            _ = getattr(g, "_", None) # Try to access g to see if it really exists
            return g
        except RuntimeError as _:
            return None

    # -------------------------- ACCOUNT AD --------------------------------- #
    @staticmethod
    def setAccountAD(accountAD):
        """
        Set Account AD
        """
        g = ApplicationGlobal.getG()
        g.accountAD = accountAD

    @staticmethod
    def getAccountAD():
        """
        Get Account AD
        """
        g = ApplicationGlobal.getG()
        return getattr(g, "accountAD", None)

    @staticmethod
    def clearAccountAD():
        """
        Clear Account AD
        """
        g = ApplicationGlobal.getG()
        if getattr(g, "accountAD", None) is not None:
            g.accountAD  = None


    # -------------------------- ACCOUNT ID --------------------------------- #
    @staticmethod
    def getAccountID():
        """
        Get Account ID from Context
        - Look at Account Job
        - Look in Flask G
        """
        aID = None

        # Flask Global
        accountAD = ApplicationGlobal.getAccountAD()
        if accountAD is not None:
            aID = accountAD.aID

        # Account Job
        from smeckn.server.aJob import AccountJob
        aJob  = AccountJob.getCurrentJob()
        if aJob and not aID:
            aID = aJob.aID

        return aID
