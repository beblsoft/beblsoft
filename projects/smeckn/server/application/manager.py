#!/usr/bin/env python3
"""
 NAME:
  application.py

 DESCRIPTION
  Application Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
import pprint
from flask import Flask
from flask_cors import CORS
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server import wAPIV1
from smeckn.server.wAPIV1.spec.spec import createSpec


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- APPLICATION MANAGER ---------------------------- #
class ApplicationManager(CommonManager):
    """
    Application Manager class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Flask Application -------------------
        self.fApp                                = Flask(__name__)
        self.fApp.cfg                            = self.cfg
        self.fApp.mgr                            = self.mgr
        # Cross Origin Resource Sharing
        # Only allow our web client to access server inside of a browser
        CORS(self.fApp,
             origins=self.cfg.WEBCLIENT_DOMAIN_LIST + self.cfg.WEBCLIENT_USERGUIDE_DOMAIN_LIST,
             supports_credentials=True)
        self.fApp.config["ENV"]                  = self.cfg.APP_ENV
        self.fApp.config["DEBUG"]                = self.cfg.APP_DEBUG
        self.fApp.config["TESTING"]              = self.cfg.APP_TESTING
        self.fApp.config["PROPAGATE_EXCEPTIONS"] = self.cfg.APP_PROPAGATE_EXCEPTIONS
        self.fApp.config["RESTPLUS_VALIDATE"]    = self.cfg.APP_RESTPLUS_VALIDATE
        self.fApp.config["ERROR_404_HELP"]       = self.cfg.APP_ERROR_404_HELP

        # APIs --------------------------------
        self.fApp.register_blueprint(wAPIV1.bp, url_prefix="/v1")

        # Misc --------------------------------
        self.process                             = None

    # -------------------------- START -------------------------------------- #
    @logFunc()
    def start(self, useReloader=False, useDebugger=True):
        """
        Start application
        """
        # Options passed to werkzeug.serving.run_simple
        # http://werkzeug.pocoo.org/docs/0.14/serving/
        self.fApp.run(
            host               = self.cfg.SERVER_IP,
            port               = self.cfg.SERVER_PORT,
            ssl_context        = "adhoc",
            debug              = self.cfg.APP_DEBUG,
            processes          = self.cfg.APP_N_PROCESSES,
            threaded           = self.cfg.APP_THREADED,
            passthrough_errors = self.cfg.APP_PASSTHROUGH_ERRORS,
            use_reloader       = useReloader,
            use_debugger       = useDebugger)

    # -------------------------- DESCRIBE ----------------------------------- #
    @logFunc()
    def describe(self):
        """
        Describe the application
        """
        logger.info(pprint.pformat(self.fApp.__dict__))

    # -------------------------- CREATE SPECS ------------------------------- #
    @logFunc()
    def createSpecs(self):  # pylint: disable=R0201
        """
        Generate swagger specs
        """
        createSpec(fApp=self.fApp)
