#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Profile Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import abc
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMMON PROFILE MANAGER ------------------------- #
class CommonProfileManager(CommonManager, abc.ABC):
    """
    Common Profile Manager Class
    """

    # ---------------------- GENERIC ---------------------------------------- #
    @property
    @abc.abstractmethod
    def profileType(self):
        """
        Return profile type
        """
        pass

    # ---------------------- CONTENT ---------------------------------------- #
    @property
    @abc.abstractmethod
    def contentManagerList(self):
        """
        Return list of content managers
        """
        pass

    @property
    def contentManagerDict(self):
        """
        Return dictionary mapping ContentType to ContentManager
        Ex. { ContentType.POST: PostManager }
        """
        return {contentManager.contentType: contentManager for contentManager in self.contentManagerList}

    @logFunc()
    def getContentManager(self, contentType, raiseOnNone=True):
        """
        Return content manager
        """
        contentManager = self.contentManagerDict.get(contentType, None)
        if not contentManager and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_CONTENT_MANAGER,
                                extMsgFormatDict={"profileType": self.profileType.name,
                                                  "contentType": contentType.name})
        return contentManager

    # ---------------------- SYNC ------------------------------------------- #
    @property
    def syncManagerDict(self):
        """
        Return dictionary mapping ContentType to SyncManager
        Ex. { ContentType.POST : PostSyncManager }
        All keys will have non-None values
        """
        d = {}
        for contentType, contentManager in self.contentManagerDict.items():
            syncManager = contentManager.syncManager
            if syncManager:
                d[contentType] = syncManager
        return d

    @logFunc()
    def getSyncManager(self, contentType, raiseOnNone=True):
        """
        Return sync manager
        """
        contentManager = self.getContentManager(contentType=contentType)
        syncManager    = contentManager.syncManager
        if not syncManager and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_CONTENT_SYNC_MANAGER,
                                extMsgFormatDict={"profileType": self.profileType.name,
                                                  "contentType": contentType.name})
        return syncManager

    @property
    @abc.abstractmethod
    def syncRestartDelta(self):
        """
        Return timedelta between when the last sync was created and the next
        sync can begin
        Ex. timedelta(hours=24)
        """
        pass

    @abc.abstractmethod
    def isSyncStartable(self, syncCtx):
        """
        Args
          syncCtx:
            SyncContext, with profileSMID

        Returns
          (startable, status)
          startable - True if sync can be started, false othersise
          status    - Unused if startable is True
                      if startable is False, status with which to fail sync
        """
        pass

    @abc.abstractmethod
    def sync(self, syncCtx):
        """
        Sync profile metadata
        This is intended to sync the high level profile metadata (ex. FacebookProfile name)
        This is NOT intended to sync profile content (ex. FacebookProfile posts)

        Sync the profile
          SyncContext, with profileSMID

        Returns
          (success, syncStatus)
          success    - True if profile sync succeeded
          syncStatus - Unused if success is True
                       if success is False, status witch which to fail sync
        """
        pass

    # ---------------------- ANALYSIS --------------------------------------- #
    @logFunc()
    def getAnalysisManager(self, contentType, analysisType, raiseOnNone=True):
        """
        Return analysis manager
        """
        contentManager  = self.getContentManager(contentType=contentType)
        analysisManager = contentManager.analysisManagerDict.get(analysisType, None)
        if not analysisManager and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_CONTENT_ANALYSIS_MANAGER,
                                extMsgFormatDict={"profileType": self.profileType.name,
                                                  "contentType": contentType.name,
                                                  "analysisType": analysisType.name})
        return analysisManager

    # ---------------------- STATISTIC -------------------------------------- #
    @logFunc()
    def getStatisticDAO(self, contentType, raiseOnNone=True):
        """
        Return statistic dao
        """
        contentManager = self.getContentManager(contentType=contentType)
        statisticDAO   = contentManager.statisticDAO
        if not statisticDAO and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_CONTENT_STATISTIC_DAO,
                                extMsgFormatDict={"profileType": self.profileType.name,
                                                  "contentType": contentType.name})
        return statisticDAO

    # ---------------------- HISTOGRAM -------------------------------------- #
    @logFunc()
    def getHistogramDAO(self, contentType, raiseOnNone=True):
        """
        Return histogram dao
        """
        contentManager = self.getContentManager(contentType=contentType)
        histogramDAO   = contentManager.histogramDAO
        if not histogramDAO and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_CONTENT_HISTOGRAM_DAO,
                                extMsgFormatDict={"profileType": self.profileType.name,
                                                  "contentType": contentType.name})
        return histogramDAO
