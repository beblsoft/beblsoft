#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Content Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import abc
import logging
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMMON CONTENT MANAGER ------------------------- #
class CommonContentManager(CommonManager, abc.ABC):
    """
    Common Content Manager Class
    """

    # ---------------------- CONTENT ---------------------------------------- #
    @property
    @abc.abstractmethod
    def contentType(self):
        """
        Return content type
        """
        pass

    # ---------------------- SYNC ------------------------------------------- #
    @property
    def syncManager(self):
        """
        Return ContentSyncManager
        Ex. PostSyncManager
        """
        return None

    # ---------------------- ANALYSIS --------------------------------------- #
    @property
    def analysisManagerList(self):
        """
        Return analysis manager list
        Ex. [PostMessageCompSentAnalysisManager]
        """
        return []

    @property
    def analysisManagerDict(self):
        """
        Return dictionary mapping AnalysisType to ContentAnalysisManager
        Ex2. { AnalysisType.COMPREHEND_SENTIMENT : PostMessageCompSentAnalysisManager }
        """
        return {analysisManager.analysisType: analysisManager for analysisManager in self.analysisManagerList}

    # ---------------------- COUNT ------------------------------------------ #
    @property
    def countManager(self):
        """
        Return CountManager
        Ex. PostCountManager
        """
        return None

    # ---------------------- STATISTIC -------------------------------------- #
    @property
    def statisticDAO(self):
        """
        Return statistic dao
        Ex. FacebookPostStatisticDAO
        """
        return None

    # ---------------------- HISTOGRAM -------------------------------------- #
    @property
    def histogramDAO(self):
        """
        Return histogram dao
        Ex. FacebookPostHistogramDAO
        """
        return None
