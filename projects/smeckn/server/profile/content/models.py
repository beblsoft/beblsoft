#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Content Models
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- CONTENT ANALYSIS MODEL ------------------------- #
class ContentAnalysisModel():
    """
    Content Analysis Model
    """

    def __init__(self, analysisType, countDone, countNotDone, countCantAnalyze):
        """
        Initialize object

        Args
          analysisType:
            Analysis Type
            Ex. AnalysisType.COMPREHEND_SENTIMENT
          countDone:
            Count of content analyzed
          countNotDone:
            Count of content not analyzed
          countCantAnalyze:
            Count of content that cannot be analyzed
        """
        self.analysisType     = analysisType
        self.countDone        = countDone
        self.countNotDone     = countNotDone
        self.countCantAnalyze = countCantAnalyze


# -------------------------- CONTENT MODEL ---------------------------------- #
class ContentModel():
    """
    Content Model
    """

    def __init__(self, contentType, analysisList, count):
        """
        Initialize object

        Args
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          analysisList:
            ContentAnalysisModel List
          count:
            Total count of content
        """
        self.contentType  = contentType
        self.analysisList = analysisList
        self.count        = count
