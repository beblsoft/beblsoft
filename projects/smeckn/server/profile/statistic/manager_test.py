#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Statistic Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.statistic.context import StatisticContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- STATISTIC MANAGER TEST CASE ------------------ #
class StatisticManagerTestCase(CommonTestCase):
    """
    Test Statistic Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)

    @logFunc()
    def test_getList(self):
        """
        Test getList
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            statisticCtx = StatisticContext(
                mgr=self.mgr,
                aDBS=aDBS,
                profileSMID=self.popProfileSMID,
                contentType=ContentType.POST,
                contentAttrType=ContentAttrType.FACEBOOK_POST_SMID)

            statisticModelList = self.mgr.profile.statistic.getList(statisticCtx=statisticCtx)
            self.assertEqual(len(statisticModelList), 1)
