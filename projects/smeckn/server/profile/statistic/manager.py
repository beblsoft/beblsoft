#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Statistic Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB.profile.dao import ProfileDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- STATISTIC MANAGER ------------------------------ #
class StatisticManager(CommonManager):
    """
    Statistic Manager Class
    """

    # ---------------------- GET -------------------------------------------- #
    @logFunc()
    def getList(self, statisticCtx):  # pylint: disable=R0201
        """
        Get statistic
        Args
          statisticCtx:
            StatisticContext, fully loaded

        Returns
          StatisticModel List
        """
        profileModel   = ProfileDAO.getBySMID(aDBS=statisticCtx.aDBS, smID=statisticCtx.profileSMID)
        profileManager = self.mgr.profile.getProfileManager(profileType=profileModel.type)
        statisticDAO   = profileManager.getStatisticDAO(contentType=statisticCtx.contentType)

        return statisticDAO.getList(statisticCtx=statisticCtx)
