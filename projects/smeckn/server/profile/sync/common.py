#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Content Sync Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import abc
import logging
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMMON CONTENT SYNC MANAGER -------------------- #
class CommonContentSyncManager(CommonManager, abc.ABC):
    """
    Common Content Sync Manager Class
    """

    def __init__(self, mgr, contentMgr, **kwargs):
        """
        Initialize object
        """
        super().__init__(mgr=mgr, **kwargs)
        self.contentMgr  = contentMgr
        self.contentType = contentMgr.contentType

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def minSyncChunkTimedelta(self):
        """
        Minimum timedelta for SyncChunk to complete
        This is used in SyncWorkerManager.main to determine when to kick another
        worker to continue the work
        """
        return self.cfg.PROFILE_SYNC_MIN_SYNCCHUNK_TIMEDELTA

    # ---------------------- METHODS ---------------------------------------- #
    @abc.abstractmethod
    def isLimitReached(self, syncCtx):
        """
        Determine if content limit has been reached

        Args
          syncCtx:
            SyncContext, SyncContent loaded

        Returns
          True if content limit has been reached, False otherwise
        """
        pass

    @abc.abstractmethod
    def syncChunk(self, syncCtx, nextDict):
        """
        Sync a chunk of data
        Will be potentially called multiple times as profile content is synced

        Args
          syncCtx:
            SyncContext object, SyncContent loaded
          nextDict:
            Dictionary of parameters that encodes next chunk of data
            On first call, {}

        Returns
          (SyncStatus, nextDict)
          SyncStatus - status of sync
                       SyncStatus.IN_PROGRESS if still going and syncChunk should be called
                       again with nextDict
          nextDict   - Dict to past next sync chunk (note, must be encodable)
        """
        pass

    @abc.abstractmethod
    def deleteStale(self, syncCtx):
        """
        Delete stale content after the sync has completed

        Args
          syncCtx:
            SyncContext object, SyncContent loaded

        Returns
          None
        """
        pass

    @abc.abstractmethod
    def getProgress(self, syncCtx):
        """
        Return sync progress

        Returns
          SyncContentProgressModel
        """
        pass
