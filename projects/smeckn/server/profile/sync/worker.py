#!/usr/bin/env python3
"""
 NAME:
  worker.py

 DESCRIPTION
  Sync Worker Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB.syncContent.dao import SyncContentDAO
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aJob.syncWorker import SyncWorkerJob
from smeckn.server.eCtx import ExecutionContext


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC WORKER MANAGER ---------------------------- #
class SyncWorkerManager(CommonManager):
    """
    Sync Worker Manager Class
    """

    # ---------------------- MAIN ------------------------------------------- #
    @logFunc()
    def main(self, syncCtx, syncContentSMID, nextDict):
        """
        Worker main
        Args
          syncCtx:
            SyncContext, not loaded
          syncContentSMID:
            Sync Content SMID
            Ex. 5
          nextDict:
            Dict to be passed to next invocation of ContentContentManager
        """
        eCtx            = ExecutionContext.get()
        continueLooping = True
        status          = None
        nextDict        = nextDict if nextDict else {}
        loadedSyncCtx   = syncCtx.tryLoadSyncContent(syncContentSMID=syncContentSMID)

        if loadedSyncCtx:
            # Loop doing the following:
            #  - Heartbeat
            #  - Check if content limit is reached
            #  - If not, sync another chunk
            while continueLooping:
                self.heartbeat(syncCtx=syncCtx)
                contentLimitReached      = syncCtx.syncManager.isLimitReached(syncCtx=syncCtx)
                if contentLimitReached:
                    status               = SyncStatus.CONTENT_LIMIT_REACHED
                else:
                    (status, nextDict)   = syncCtx.syncManager.syncChunk(syncCtx=syncCtx, nextDict=nextDict)
                timeRemainingDelta       = timedelta(milliseconds=eCtx.timeRemainingMS)
                enoughTime               = timeRemainingDelta > syncCtx.syncManager.minSyncChunkTimedelta
                continueLooping          = status.isInProgress and enoughTime

            # After loop do the following:
            #  - If successfull, delete stale content
            #  - If done, complete sync content, check and optionally complete sync
            #  - If not done, kick off worker to continue
            if status.isSuccessfullCompletion:
                syncCtx.syncManager.deleteStale(syncCtx=syncCtx)
            if not status.isInProgress:
                self.mgr.profile.sync.completeContent(syncCtx=syncCtx, status=status)
                self.mgr.profile.sync.checkComplete(syncCtx=syncCtx)
            else:
                self.kick(syncCtx=syncCtx, nextDict=nextDict)


    # ---------------------- HEARTBEAT -------------------------------------- #
    @logFunc()
    def heartbeat(self, syncCtx):  # pylint: disable=R0201
        """
        Heartbeat
        """
        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            SyncContentDAO.update(aDBS=syncCtx.aDBS, syncContentModel=syncCtx.syncContentModel,
                                  workerHeartbeatDate=datetime.utcnow())

    # ---------------------- KICK ------------------------------------------- #
    @logFunc()
    def kick(self, syncCtx, nextDict=None):  # pylint: disable=R0201
        """
        Kick Worker
        """
        assert syncCtx.syncContentModel

        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            # Lock SyncContent row
            # Kick worker
            # Update database with worker kick
            SyncContentDAO.lockSMID(aDBS=syncCtx.aDBS, smID=syncCtx.syncContentSMID)
            syncCtx.aDBS.refresh(syncCtx.syncContentModel)

            SyncWorkerJob.invokeAsync(
                mgr           = self.mgr,
                classKwargs   = SyncWorkerJob.getClassKwargsFromADBS(aDBS=syncCtx.aDBS),
                executeKwargs = {"syncContentSMID": syncCtx.syncContentSMID,
                                 "nextDict": nextDict})
            SyncContentDAO.update(aDBS=syncCtx.aDBS, syncContentModel=syncCtx.syncContentModel,
                                  workerKickDate=datetime.utcnow(),
                                  nWorkerKicks=syncCtx.syncContentModel.nWorkerKicks + 1)
