#!/usr/bin/env python3
"""
 NAME:
  progress.py

 DESCRIPTION
  Sync Progress Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server.profile.sync.models import SyncProgressModel


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC PROGRESS MANAGER -------------------------- #
class SyncProgressManager(CommonManager):
    """
    Sync Progress Manager Class
    """

    # ---------------------- GET -------------------------------------------- #
    @logFunc()
    def get(self, syncCtx, syncSMID):
        """
        Get progress

        Args:
          syncCtx:
            SyncContext, nothing loaded

        Returns
          SyncProgressModel
        """
        syncCtx.loadSync(syncSMID=syncSMID)

        # Do the following:
        #  - Optionally timeout sync content
        #  - Optionally complete the sync
        self.mgr.profile.sync.checkTimeoutAllContent(syncCtx=syncCtx)
        self.mgr.profile.sync.checkComplete(syncCtx=syncCtx)

        # Get progress
        contentProgressList   = []
        for syncContentModel in syncCtx.syncModel.syncContentModelList:
            syncCtx.loadSyncContent(syncContentSMID=syncContentModel.smID)
            contentProgressModel = syncCtx.syncManager.getProgress(syncCtx=syncCtx)
            contentProgressList.append(contentProgressModel)

        return SyncProgressModel(status=syncCtx.syncModel.status, contentProgressList=contentProgressList)
