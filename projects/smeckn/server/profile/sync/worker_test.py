#!/usr/bin/env python3
"""
 NAME:
  worker_test.py

 DESCRIPTION
  Sync Worker Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import threading
from unittest.mock import patch
from datetime import timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.eCtx import ExecutionContext


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SyncWorkerManagerTestCase(CommonTestCase):
    """
    Test Sync Worker Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.syncManager      = self.mgr.profileStub.getSyncManager(contentType=self.contentType)
        self.syncModel        = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                           status=SyncStatus.IN_PROGRESS)
        self.syncContentModel = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                  contentType=self.contentType,
                                                                  status = SyncStatus.IN_PROGRESS)

    @logFunc()
    def test_mainSuccess(self):
        """
        Test main success case
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.syncManager, attribute='deleteStale', autospec=True) as deleteStaleMock:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)

            self.mgr.profile.sync.worker.main(syncCtx=syncCtx, syncContentSMID=self.syncContentModel.smID, nextDict={})
            self.assertEqual(syncCtx.syncModel.status, SyncStatus.SUCCESS)
            self.assertEqual(syncCtx.syncContentModel.status, SyncStatus.SUCCESS)
            self.assertGreaterEqual(syncCtx.syncContentModel.workerHeartbeatDate, self.testStartDate)
            self.assertEqual(deleteStaleMock.call_count, 1)

    @logFunc()
    def test_mainLimitReached(self):
        """
        Test main with limit reached
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.syncManager, attribute='deleteStale', autospec=True) as deleteStaleMock, \
                patch.object(self.syncManager, attribute='isLimitReached', autospec=True, return_value=True) as isLimitReachedMock:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)

            self.mgr.profile.sync.worker.main(syncCtx=syncCtx, syncContentSMID=self.syncContentModel.smID, nextDict={})
            self.assertEqual(syncCtx.syncModel.status, SyncStatus.CONTENT_LIMIT_REACHED)
            self.assertEqual(syncCtx.syncContentModel.status, SyncStatus.CONTENT_LIMIT_REACHED)
            self.assertEqual(deleteStaleMock.call_count, 1)
            self.assertEqual(isLimitReachedMock.call_count, 1)

    @logFunc()
    def test_mainMultipleKicks(self):
        """
        Test main with multiple worker kicks
        Set PROFILE_SYNC_MIN_SYNCCHUNK_TIMEDELTA to a small value so multiple workers will be kicked
        The main thread waits for the last syncChunk thread to signal condition variable
        """
        eCtx = ExecutionContext.get()
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.syncManager, attribute='deleteStale', autospec=True) as deleteStaleMock, \
                patch.object(self.syncManager, attribute='syncChunk', autospec=True) as syncChunkMock, \
                patch.dict(self.cfg.__dict__, PROFILE_SYNC_MIN_SYNCCHUNK_TIMEDELTA=timedelta(milliseconds=eCtx.timeRemainingMS + 1)) as _:
            syncCtx    = SyncContext(mgr=self.mgr, aDBS=aDBS)
            counter    = 0
            totalCalls = 10
            condVar    = threading.Condition()

            def syncChunk(syncCtx, nextDict):  # pylint: disable=W0613
                """
                Sync chunk variant that checks nextDict and completes over various iterations
                """
                nonlocal counter
                nonlocal condVar
                newNextDict = {"foo": "foo", "bar": "bar"}
                counter    += 1
                if counter == 1:
                    assert nextDict == {}
                else:
                    assert nextDict == newNextDict
                status    = SyncStatus.SUCCESS if counter == totalCalls else SyncStatus.IN_PROGRESS
                allDone   = counter == totalCalls
                if allDone:
                    with condVar:
                        condVar.notifyAll()
                    nextDict = None
                else:
                    nextDict = newNextDict
                return (status, nextDict)
            syncChunkMock.side_effect = syncChunk

            # Kick off main thread which inturn will kick off more threads to complete work
            self.mgr.profile.sync.worker.main(syncCtx=syncCtx, syncContentSMID=self.syncContentModel.smID,
                                              nextDict={})

            # Wait for last syncChunk thread to signal completion
            with condVar:
                condVar.wait()
            self.mgr.jqueue.joinWorkers()
            aDBS.refresh(syncCtx.syncModel)
            self.assertEqual(syncCtx.syncModel.syncContentModelList[0].nWorkerKicks, totalCalls - 1)
            self.assertEqual(syncCtx.syncModel.status, SyncStatus.SUCCESS)
            self.assertEqual(syncChunkMock.call_count, totalCalls)
            self.assertEqual(deleteStaleMock.call_count, 1)

    @logFunc()
    def test_heartbeat(self):
        """
        Test heartbeat
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.syncSMID)
            self.mgr.profile.sync.worker.heartbeat(syncCtx=syncCtx)
            self.assertGreaterEqual(syncCtx.syncContentModel.workerHeartbeatDate, self.testStartDate)

    @logFunc()
    def test_kick(self):
        """
        Test kick
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.syncSMID)
            self.assertEqual(self.syncContentModel.nWorkerKicks, 0)

            # Kick worker
            self.mgr.profile.sync.worker.kick(syncCtx=syncCtx)

            # Wait for worker to complete
            self.mgr.jqueue.joinWorkers()
            aDBS.refresh(syncCtx.syncContentModel)
            self.assertEqual(syncCtx.syncContentModel.nWorkerKicks, 1)
            self.assertGreaterEqual(syncCtx.syncContentModel.workerKickDate, self.testStartDate)
