#!/usr/bin/env python3
"""
 NAME:
  progress_test.py

 DESCRIPTION
  Sync Progress Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.syncContent.dao import SyncContentDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SyncProgressManagerTestCase(CommonTestCase):
    """
    Test Sync Progress Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.syncManager      = self.mgr.profileStub.getSyncManager(contentType=self.contentType)
        self.syncModel        = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                           status=SyncStatus.IN_PROGRESS)
        self.syncContentModel = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                  contentType=self.contentType,
                                                                  status=SyncStatus.IN_PROGRESS)
        self.testStartDate    = datetime.utcnow()


    @logFunc()
    def test_getSuccess(self):
        """
        Test get success case
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)

            # Update worker heartbeat date so don't get timedout
            with AccountDatabase.transactionScope(session=aDBS):
                SyncContentDAO.update(aDBS=aDBS, syncContentModel=self.syncContentModel, workerHeartbeatDate=datetime.utcnow())

            syncProgressModel = self.mgr.profile.sync.progress.get(syncCtx=syncCtx, syncSMID=self.syncModel.smID)
            self.assertEqual(syncProgressModel.status, SyncStatus.IN_PROGRESS)
            contentProgressList = syncProgressModel.contentProgressList
            self.assertEqual(len(contentProgressList), 1)
            contentProgressModel = contentProgressList[0]
            self.assertEqual(contentProgressModel.contentType, self.contentType)
            self.assertGreater(contentProgressModel.count, 0)
            self.assertEqual(contentProgressModel.status,  SyncStatus.IN_PROGRESS)

    @logFunc()
    def test_getTimeout(self):
        """
        Test get case that times out the sync
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)

            # Cause sync content, and therefore sync to be timed out because worker wasn't kicked
            with AccountDatabase.transactionScope(session=aDBS):
                SyncContentDAO.update(aDBS=aDBS, syncContentModel=self.syncContentModel, workerHeartbeatDate=None, workerKickDate=None)

            syncProgressModel = self.mgr.profile.sync.progress.get(syncCtx=syncCtx, syncSMID=self.syncModel.smID)
            self.assertEqual(syncProgressModel.status, SyncStatus.TIMED_OUT)
            contentProgressList = syncProgressModel.contentProgressList
            self.assertEqual(len(contentProgressList), 1)
            contentProgressModel = contentProgressList[0]
            self.assertEqual(contentProgressModel.contentType, self.contentType)
            self.assertGreater(contentProgressModel.count, 0)
            self.assertEqual(contentProgressModel.status,  SyncStatus.TIMED_OUT)
