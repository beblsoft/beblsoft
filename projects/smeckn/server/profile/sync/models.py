#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Sync Models
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC CONTENT PROGRESS MODEL -------------------- #
class SyncContentProgressModel():
    """
    Sync Content Progress Model
    """

    def __init__(self, contentType, count, status):
        """
        Initialize object

        Args
          contentType:
            Content Type
            Ex. ContentType.POST
          count:
            Count of content synced
          status:
            Sync Status
            Ex. SyncStatus.IN_PROGRESS
        """
        self.contentType = contentType
        self.count       = count
        self.status      = status


# -------------------------- SYNC PROGRESS MODEL ---------------------------- #
class SyncProgressModel():
    """
    Sync Progress Model
    """

    def __init__(self, status, contentProgressList):
        """
        Initialize object

        Args
          status:
            Sync Status
            Ex. SyncStatus.IN_PROGRESS
          contentProgressList:
            SyncContentProgressModel List
        """
        self.status              = status
        self.contentProgressList = contentProgressList
