#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Sync Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.syncContent.dao import SyncContentDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- START TEST CASE ------------------------------ #
class SyncManagerStartTestCase(CommonTestCase):
    """
    Test Sync Manager Start
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType   = ContentType.POST
        self.syncManager   = self.mgr.profileStub.getSyncManager(contentType=self.contentType)

    @logFunc()
    def test_success(self):
        """
        Test success case
        """
        syncSMID = None
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx   = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncModel = self.mgr.profile.sync.start(syncCtx=syncCtx, profileSMID=self.popProfileSMID)
            syncSMID  = syncModel.smID

        # Wait for all threads
        self.mgr.jqueue.joinWorkers()

        # New session as underlying state was changed by threads
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncModel = SyncDAO.getBySMID(aDBS=aDBS, smID=syncSMID)
            self.assertEqual(syncModel.status, SyncStatus.SUCCESS)
            self.assertGreaterEqual(syncModel.restartDate, self.testStartDate +
                                    syncCtx.profileManager.syncRestartDelta)
            for syncContentModel in syncModel.syncContentModelList:
                self.assertEqual(syncContentModel.status, SyncStatus.SUCCESS)

    @logFunc()
    def test_notStartable(self):
        """
        Test not startable case
        """
        status = SyncStatus.CREDENTIALS_EXPIRED
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.mgr.profileStub, attribute='isSyncStartable', autospec=True, return_value=(False, status)) as isSyncStartableMock:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            self.mgr.profile.sync.start(syncCtx=syncCtx, profileSMID=self.popProfileSMID)
            self.assertEqual(syncCtx.syncModel.status, status)
            self.assertEqual(isSyncStartableMock.call_count, 1)

    @logFunc()
    def test_profileSyncFailure(self):
        """
        Test profile sync failing
        """
        status = SyncStatus.THIRDPARTY_API_FAILURE
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.mgr.profileStub, attribute='sync', autospec=True, return_value=(False, status)) as syncMock:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            self.mgr.profile.sync.start(syncCtx=syncCtx, profileSMID=self.popProfileSMID)
            self.assertEqual(syncMock.call_count, 1)
            self.assertEqual(syncCtx.syncModel.status, status)


# ---------------------------- CHECK COMPLETE TEST CASE --------------------- #
class SyncManagerCheckCompleteTestCase(CommonTestCase):
    """
    Test Sync Manager Check Complete
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.syncModel        = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                           status=SyncStatus.IN_PROGRESS)
        self.postSyncContent  = None
        self.photoSyncContent = None

    def verifyOutputStatus(self, postStatus=None, photoStatus=None, outputStatus=None):
        """
        Test that postStatus and photoStatus result in outputStatus
        """
        if postStatus:
            self.postSyncContent = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                     contentType=ContentType.POST, status=postStatus)
        if photoStatus:
            self.photoSyncContent = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                      contentType=ContentType.PHOTO, status=photoStatus)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSync(syncSMID=self.syncModel.smID)
            self.mgr.profile.sync.checkComplete(syncCtx=syncCtx)
            self.assertEqual(syncCtx.syncModel.status, outputStatus)

    @logFunc()
    def test_noContent(self):  # pylint: disable=C0111
        self.verifyOutputStatus(outputStatus=SyncStatus.TIMED_OUT)

    @logFunc()
    def test_inProgress(self):  # pylint: disable=C0111
        self.verifyOutputStatus(SyncStatus.IN_PROGRESS, SyncStatus.SUCCESS, outputStatus=SyncStatus.IN_PROGRESS)

    @logFunc()
    def test_success(self):  # pylint: disable=C0111
        self.verifyOutputStatus(SyncStatus.SUCCESS, SyncStatus.SUCCESS, outputStatus=SyncStatus.SUCCESS)

    @logFunc()
    def test_errorPriority(self):  # pylint: disable=C0111
        self.verifyOutputStatus(SyncStatus.CREDENTIALS_EXPIRED, SyncStatus.THIRDPARTY_API_FAILURE,
                                outputStatus=SyncStatus.CREDENTIALS_EXPIRED)


# ---------------------------- CHECK TIMEOUT CONTENT ------------------------ #
class SyncManagerCheckTimeoutContentTestCase(CommonTestCase):
    """
    Test Sync Manager Check Timeout Content
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.initialStatus    = SyncStatus.IN_PROGRESS
        self.syncManager      = self.mgr.profileStub.getSyncManager(contentType=self.contentType)
        self.syncModel        = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                           status=SyncStatus.IN_PROGRESS)
        self.syncContentModel = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                  contentType=self.contentType,
                                                                  status=self.initialStatus)

    def verifyTimeoutContent(self, workerKickDate=None, workerHeartbeatDate=None, outputStatus=None):
        """
        Verify that setting workerKickDate and workerHeartbeatDate changes output status
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                SyncContentDAO.update(aDBS=aDBS, syncContentModel=self.syncContentModel, workerKickDate=workerKickDate,
                                      workerHeartbeatDate=workerHeartbeatDate)
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSync(syncSMID=self.syncModel.smID)
            self.mgr.profile.sync.checkTimeoutAllContent(syncCtx=syncCtx)
            self.assertEqual(self.syncContentModel.status, outputStatus)

    @logFunc()
    def test_noTimeout(self):  # pylint: disable=C0111
        self.verifyTimeoutContent(workerKickDate=datetime.utcnow(), workerHeartbeatDate=datetime.utcnow(),
                                  outputStatus=self.initialStatus)

    @logFunc()
    def test_noKickNoHeartbeat(self):  # pylint: disable=C0111
        self.verifyTimeoutContent(workerKickDate=None, workerHeartbeatDate=None,
                                  outputStatus=SyncStatus.TIMED_OUT)

    @logFunc()
    def test_oldKickOldHeartbeat(self):  # pylint: disable=C0111
        oldKickDate      = self.testStartDate - self.cfg.PROFILE_SYNC_MAX_IDLE_WORKERKICK - timedelta(seconds=1)
        oldHeartbeatDate = self.testStartDate - self.cfg.PROFILE_SYNC_MAX_IDLE_WORKERHB - timedelta(seconds=1)
        self.verifyTimeoutContent(workerKickDate=oldKickDate, workerHeartbeatDate=oldHeartbeatDate,
                                  outputStatus=SyncStatus.TIMED_OUT)


# ---------------------------- TEST CASE ------------------------------------ #
class SyncManagerTestCase(CommonTestCase):
    """
    Test Sync Manager
    Catch all for rest of tests
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.syncManager      = self.mgr.profileStub.getSyncManager(contentType=self.contentType)
        self.syncModel        = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                           status=SyncStatus.IN_PROGRESS)
        self.syncContentModel = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                  contentType=self.contentType,
                                                                  status = SyncStatus.IN_PROGRESS)

    @logFunc()
    def test_complete(self):
        """
        Test complete
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSync(syncSMID=self.syncModel.smID)

            status = SyncStatus.SUCCESS
            self.mgr.profile.sync.complete(syncCtx=syncCtx, status=status)
            self.assertEqual(syncCtx.syncModel.status, status)
            self.assertGreaterEqual(syncCtx.syncModel.completeDate, self.testStartDate)

    @logFunc()
    def test_completeContent(self):
        """
        Test complete content
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.smID)

            status = SyncStatus.SUCCESS
            self.mgr.profile.sync.completeContent(syncCtx=syncCtx, status=status)
            self.assertEqual(syncCtx.syncContentModel.status, status)
            self.assertGreaterEqual(syncCtx.syncContentModel.completeDate, self.testStartDate)
