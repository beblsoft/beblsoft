#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Sync Manager Functionality

 START WORKFLOW -----------------------------------------------------
    POST /sync
      |
      v
    SyncManager.start
      SyncDAO.create
      SyncContentDAO.create
      SyncManager.kickContentWorkers
      SyncDAO.deleteOld
      |
      v
    SyncWorkerManager.kick---------------------<--
    SyncWorkerJob.execute                        |
    SyncWorkerManager.main                       |
      Iterate Looping                            ^
        ContentSyncManager.syncChunk             |
      If done                                    |
        SyncManager.completeSyncContent          |
        SyncManager.checkComplete                |
      If not done,                               |
        SyncWorkerManager.kick -------------------

 PROGRESS WORKFLOW --------------------------------------------------
   GET /sync/<smID>/progress
     |
     v
   SyncProgressManager.get
     SyncManager.checkTimeoutAllContent
     SyncManager.checkComplete
     Iterate through all ContentSyncManagers
       ContentSyncManager.getProgress
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server.profile.sync.progress import SyncProgressManager
from smeckn.server.profile.sync.worker import SyncWorkerManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.syncContent.dao import SyncContentDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC MANAGER ----------------------------------- #
class SyncManager(CommonManager):
    """
    Sync Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        self.progress = SyncProgressManager(mgr=self.mgr)
        self.worker   = SyncWorkerManager(mgr=self.mgr)

    # ---------------------- START ------------------------------------------ #
    @logFunc()
    def start(self, syncCtx, profileSMID):
        """
        Start sync
        Args
          syncCtx:
            SyncContext Object, Nothing loaded
          profileSMID:
            Profile Smeckn ID

        Returns
          SyncModel
        """
        syncModel  = None

        # Create Sync row and Content rows
        with AccountDatabase.lockTables(session=syncCtx.aDBS,
                                        lockStr="Profile READ, Sync WRITE, Analysis READ"):
            syncCtx.loadProfile(profileSMID=profileSMID)
            createDate  = datetime.utcnow()
            restartDate = createDate + syncCtx.profileManager.syncRestartDelta
            syncModel   = SyncDAO.create(aDBS=syncCtx.aDBS, profileSMID=profileSMID,
                                         status=SyncStatus.IN_PROGRESS,
                                         createDate=createDate, restartDate=restartDate)
        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            for contentType, _ in syncCtx.profileManager.syncManagerDict.items():
                SyncContentDAO.create(aDBS=syncCtx.aDBS, syncSMID=syncModel.smID,
                                      contentType=contentType, status=SyncStatus.IN_PROGRESS)

        # Do the following:
        #  - Check if the sync is startable
        #  - Sync the profile metadata
        #  - If both above succeeded, kick content workers
        #  - Else, complete sync
        #  - Delete old syncs
        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            syncCtx.loadSync(syncSMID=syncModel.smID)
            syncStartable                    = False
            profileSyncSuccess               = False
            (syncStartable, status)          = syncCtx.profileManager.isSyncStartable(syncCtx=syncCtx)
            if syncStartable:
                (profileSyncSuccess, status) = syncCtx.profileManager.sync(syncCtx=syncCtx)
            if syncStartable and profileSyncSuccess:
                self.kickContentWorkers(syncCtx=syncCtx)
            else:
                self.complete(syncCtx=syncCtx, status=status)
            SyncDAO.deleteOld(aDBS=syncCtx.aDBS)

        syncCtx.aDBS.refresh(syncModel)
        return syncModel

    # ---------------------- CHECK COMPLETE --------------------------------- #
    @logFunc()
    def checkComplete(self, syncCtx):
        """
        Check and complete sync
        """
        assert syncCtx.syncModel
        assert syncCtx.profileManager

        errorStatusList = []
        nSucs           = 0
        nInProg         = 0
        finalStatus     = None
        complete        = True

        # Do the following:
        # - Iterate through all content, tablulating content statuses
        # - If content still in progress, bag out
        # - If all content done, propagate appropriate status up to sync
        syncContentModelList = syncCtx.syncModel.syncContentModelList
        for syncContentModel in syncContentModelList:
            status = syncContentModel.status
            if status == SyncStatus.IN_PROGRESS:
                nInProg += 1
            elif status == SyncStatus.SUCCESS:
                nSucs   += 1
            else:
                errorStatusList.append(status)

        if not syncContentModelList:
            finalStatus = SyncStatus.TIMED_OUT
        elif nInProg > 0:
            complete = False
        elif nSucs == len(syncContentModelList):
            finalStatus = SyncStatus.SUCCESS
        else:
            finalStatus = SyncStatus.getHighestPriority(errorStatusList)

        if complete:
            self.complete(syncCtx=syncCtx, status=finalStatus)

    # ---------------------- COMPLETE --------------------------------------- #
    @logFunc()
    def complete(self, syncCtx, status):  # pylint: disable=R0201
        """
        Complete Sync
        """
        assert syncCtx.syncModel
        assert syncCtx.profileManager

        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            # Lock Sync row
            # Only update sync if it hasn't already been completed
            SyncDAO.lockSMID(aDBS=syncCtx.aDBS, smID=syncCtx.syncSMID)
            syncCtx.aDBS.refresh(syncCtx.syncModel)
            existingStatus = syncCtx.syncModel.status
            if existingStatus.isInProgress:
                SyncDAO.update(aDBS=syncCtx.aDBS, syncModel=syncCtx.syncModel, status=status,
                               completeDate=datetime.utcnow())

    # ---------------------- KICK CONTENT WORKERS --------------------------- #
    @logFunc()
    def kickContentWorkers(self, syncCtx):  # pylint: disable=R0201
        """
        Kick Content Workers
        """
        assert syncCtx.syncModel

        syncCtx.aDBS.refresh(syncCtx.syncModel)
        syncContentModelList = syncCtx.syncModel.syncContentModelList
        for syncContentModel in syncContentModelList:
            syncCtx.loadSyncContent(syncContentSMID=syncContentModel.smID)
            self.worker.kick(syncCtx=syncCtx)

    # ---------------------- COMPLETE CONTENT ------------------------------- #
    @logFunc()
    def completeContent(self, syncCtx, status):  # pylint: disable=R0201
        """
        Complete Sync Content
        """
        assert syncCtx.syncContentSMID
        assert syncCtx.syncContentModel

        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            # Lock SyncContent row
            # Only update sync content if it hasn't already been completed
            SyncContentDAO.lockSMID(aDBS=syncCtx.aDBS, smID=syncCtx.syncContentSMID)
            syncCtx.aDBS.refresh(syncCtx.syncContentModel)
            existingStatus = syncCtx.syncContentModel.status
            if existingStatus.isInProgress:
                SyncContentDAO.update(aDBS=syncCtx.aDBS, syncContentModel=syncCtx.syncContentModel,
                                      status=status, completeDate=datetime.utcnow())

    # ---------------------- TIMEOUT CONTENT -------------------------------- #
    @logFunc()
    def checkTimeoutAllContent(self, syncCtx):
        """
        Check and timeout all content
        Return
          SyncStatus for SyncModel
        """
        assert syncCtx.syncModel

        syncContentModelList = syncCtx.syncModel.syncContentModelList
        for syncContentModel in syncContentModelList:
            syncCtx.loadSyncContent(syncContentSMID=syncContentModel.smID)
            self.checkTimeoutContent(syncCtx=syncCtx)

    @logFunc()
    def checkTimeoutContent(self, syncCtx):
        """
        Check and timeout content
        """
        assert syncCtx.syncContentModel

        syncContentModel = syncCtx.syncContentModel
        status           = syncContentModel.status
        inProgress       = status.isInProgress
        now              = datetime.utcnow()
        noKick           = not syncContentModel.workerKickDate
        idleKick         = noKick or \
            (syncContentModel.workerKickDate + self.cfg.PROFILE_SYNC_MAX_IDLE_WORKERKICK < now)
        noHeartbeat      = not syncContentModel.workerHeartbeatDate
        idleHeartbeat    = noHeartbeat or \
            (syncContentModel.workerHeartbeatDate + self.cfg.PROFILE_SYNC_MAX_IDLE_WORKERHB < now)

        if inProgress and idleKick and idleHeartbeat:
            status = SyncStatus.TIMED_OUT
            self.completeContent(syncCtx=syncCtx, status=status)
