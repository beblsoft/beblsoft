#!/usr/bin/env python3
"""
 NAME:
  context.py

 DESCRIPTION
  Sync Context Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.syncContent.dao import SyncContentDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- SYNC CONTEXT ----------------------------------- #
class SyncContext():
    """
    Sync Context Class
    """

    def __init__(self, mgr, aDBS, aJob=None):
        """
        Initialize object
        """
        self.mgr              = mgr
        self.aDBS             = aDBS
        self.aJob             = aJob
        self.profileSMID      = None
        self.profileModel     = None
        self.profileManager   = None
        self.syncSMID         = None
        self.syncModel        = None
        self.syncContentSMID  = None
        self.syncContentModel = None
        self.syncManager      = None

    @logFunc()
    def loadProfile(self, profileSMID):
        """
        Load profile
        """
        self.profileSMID    = profileSMID
        self.profileModel   = ProfileDAO.getBySMID(aDBS=self.aDBS, smID=profileSMID)
        self.profileManager = self.mgr.profile.getProfileManager(profileType=self.profileModel.type)

    @logFunc()
    def loadSync(self, syncSMID):
        """
        Load sync
        """
        self.syncSMID  = syncSMID
        self.syncModel = SyncDAO.getBySMID(aDBS=self.aDBS, smID=syncSMID)
        self.loadProfile(profileSMID=self.syncModel.profileSMID)

    @logFunc()
    def loadSyncContent(self, syncContentSMID):
        """
        Load Sync Content
        """
        self.syncContentSMID  = syncContentSMID
        self.syncContentModel = SyncContentDAO.getBySMID(aDBS=self.aDBS, smID=syncContentSMID)
        self.loadSync(syncSMID=self.syncContentModel.syncSMID)
        self.syncManager      = self.profileManager.getSyncManager(contentType=self.syncContentModel.contentType)

    @logFunc()
    def tryLoadSyncContent(self, syncContentSMID):
        """
        Try loading Sync Content
        This could fail if any object in the sync context has been deleted

        Returns
          True on Success
          False otherwise
        """
        rval = False
        try:
            self.loadSyncContent(syncContentSMID=syncContentSMID)
            rval = True
        except BeblsoftError as e:
            if e.code == BeblsoftErrorCode.PROFILE_DOES_NOT_EXIST:
                pass
            elif e.code == BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST:
                pass
            else:
                raise e
        return rval
