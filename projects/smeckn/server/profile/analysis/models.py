#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Analysis Models
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS TYPE PROGRESS MODEL ------------------- #
class AnalysisProgressModel():
    """
    Analysis Progress Model
    """

    def __init__(self, contentType, analysisType, countAnalyzed, countTotal, status):
        """
        Initialize object

        Args
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          analysisType:
            Analysis Type
            Ex. AnalysisType.COMPREHEND_SENTIMENT
          countAnalyzed:
            Count of content analyzed [whether successfully or not] during the
            current analysis
          countTotal:
            Total count to be analyzed
          status:
            Analysis Status
            Ex. AnalysisStatus.IN_PROGRESS
        """
        self.contentType    = contentType
        self.analysisType   = analysisType
        self.countAnalyzed  = countAnalyzed
        self.countTotal     = countTotal
        self.status         = status


# -------------------------- ANALYSIS COUNT MODEL --------------------------- #
class AnalysisCountModel():
    """
    Analysis Count Model
    """

    def __init__(self, profileSMID, contentType, analysisType,
                 countDone, countCant, countNotDone):
        """
        Initialize object

        Args
          profileSMID:
            Profile Smeckn ID
            Ex. 1298
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          analysisType:
            Analysis Type
            Ex. AnalysisType.COMPREHEND_SENTIMENT
          countDone:
            Count of content analyzed successfully
          countCant:
            Count of content that cannot be analyzed
          countNotDone:
            Count of content that has not been analyzed
        """
        self.profileSMID   = profileSMID
        self.contentType   = contentType
        self.analysisType  = analysisType
        self.countDone     = countDone
        self.countCant     = countCant
        self.countNotDone  = countNotDone
