#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Analysis Manager Functionality

 START WORKFLOW -----------------------------------------------------
    POST /analysis
      |
      v
    AnalysisManager.start
      AnalysisDAO.create
      ContentAnalysisManager.getAnalyzeOneKwargList
      UnitLedgerDAO.createHold
      AnalysisManager.kickWorkers
      AnalysisWorkerManager.kick
      |
      v
    AnalysisWorkerJob.execute
    AnalysisWorkerManager.main
      Iterate over analyzeOneKwargList
        ContentAnalysisManager.analyzeOne

 PROGRESS WORKFLOW --------------------------------------------------
   GET /analysis/<smID>/progress
     |
     v
   AnalysisProgressManager.get
     AnalysisManager.checkComplete
     AnalysisManager.checkTimeout
     ContentSyncManager.getProgress
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.manager.common import CommonManager
from smeckn.server.profile.analysis.progress import AnalysisProgressManager
from smeckn.server.profile.analysis.worker import AnalysisWorkerManager
from smeckn.server.profile.analysis.count import AnalysisCountManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.analysis.model import AnalysisStatus


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS MANAGER ------------------------------- #
class AnalysisManager(CommonManager):
    """
    Analysis Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.progress = AnalysisProgressManager(mgr=self.mgr)
        self.worker   = AnalysisWorkerManager(mgr=self.mgr)
        self.count    = AnalysisCountManager(mgr=self.mgr)

    # ---------------------- START ------------------------------------------ #
    @logFunc()
    def start(self, analysisCtx, profileSMID, contentType, analysisType):
        """
        Start analysis
        Args
          analysisCtx:
            AnalysisContext Object, Nothing loaded
          profileSMID:
            Profile Smeckn ID

        Returns
          AnalysisModel
        """
        analysisCtx.loadProfile(profileSMID=profileSMID)
        analysisCtx.loadContentAnalysis(contentType=contentType, analysisType=analysisType)
        status              = AnalysisStatus.IN_PROGRESS
        unitType            = analysisCtx.analysisManager.unitType
        unitLedgerHoldModel = None

        # Create analysis in DB, this prevents underlying profile from being
        # synced and content, changing
        with AccountDatabase.lockTables(session=analysisCtx.aDBS,
                                        lockStr="Profile READ, Sync READ, Analysis WRITE"):
            analysisModel = AnalysisDAO.create(aDBS=analysisCtx.aDBS, profileSMID=profileSMID,
                                               contentType=contentType,
                                               analysisType=analysisType, createDate=datetime.utcnow(),
                                               status=status)

        # Retrieve analysis worker kwargs
        # Check that there is something to analyze
        analysisCtx.loadAnalysis(analysisSMID=analysisModel.smID)
        analyzeOneKwargList = analysisCtx.analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)
        contentCount        = len(analyzeOneKwargList)
        if contentCount == 0:
            self.delete(analysisCtx=analysisCtx)
            raise BeblsoftError(code=BeblsoftErrorCode.ANALYSIS_NOTHING_TO_ANALYZE)

        # Create a hold for analysis
        if status.isInProgress:
            try:
                with AccountDatabase.lockTables(session=analysisCtx.aDBS, lockStr="UnitLedger WRITE"):
                    unitLedgerHoldModel = UnitLedgerDAO.createHold(aDBS=analysisCtx.aDBS, unitType=unitType,
                                                                   ledgerCount=contentCount)
            except BeblsoftError as e:
                self.delete(analysisCtx=analysisCtx)
                raise e

        # Update DB with hold and contentCount
        # Kick workers to analyze content
        if status.isInProgress:
            with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
                AnalysisDAO.update(aDBS=analysisCtx.aDBS, analysisModel=analysisCtx.analysisModel,
                                   contentCount=contentCount, unitLedgerHoldSMID=unitLedgerHoldModel.smID)
        if status.isInProgress:
            self.kickWorkers(analysisCtx=analysisCtx, analyzeOneKwargList=analyzeOneKwargList)

        return analysisCtx.analysisModel

    # ---------------------- CHECK COMPLETE --------------------------------- #
    @logFunc()
    def checkComplete(self, analysisCtx):
        """
        Check and optionally complete analysis
        """
        assert analysisCtx.analysisModel
        assert analysisCtx.analysisManager

        analysisProgressModel = analysisCtx.analysisManager.getProgress(analysisCtx=analysisCtx)
        countAnalyzed         = analysisProgressModel.countAnalyzed
        countTotal            = analysisProgressModel.countTotal
        if countAnalyzed == countTotal:
            status = AnalysisStatus.SUCCESS
            self.complete(analysisCtx=analysisCtx, status=status)

    # ---------------------- CHECK TIMEOUT ---------------------------------- #
    @logFunc()
    def checkTimeout(self, analysisCtx):
        """
        Check and optionally timeout analysis
        """
        assert analysisCtx.analysisModel
        assert analysisCtx.analysisManager

        analysisModel      = analysisCtx.analysisModel
        analysisCtx.aDBS.refresh(analysisModel)
        now                = datetime.utcnow()
        noKick             = not analysisModel.workerKickDate
        idleKick           = noKick or \
            (analysisModel.workerKickDate + self.cfg.PROFILE_ANALYSIS_MAX_IDLE_WORKERKICK < now)
        noHeartbeat        = not analysisModel.workerHeartbeatDate
        idleHeartbeat      = noHeartbeat or \
            (analysisModel.workerHeartbeatDate + self.cfg.PROFILE_ANALYSIS_MAX_IDLE_WORKERHB < now)

        if idleKick and idleHeartbeat:
            status = AnalysisStatus.TIMED_OUT
            self.complete(analysisCtx=analysisCtx, status=status)

    # ---------------------- DELETE ----------------------------------------- #
    @logFunc()
    def delete(self, analysisCtx):  # pylint: disable=R0201
        """
        Delete Analysis

        Should only be called in the start code path, before any workers have been kicked
        """
        assert analysisCtx.analysisModel
        assert not analysisCtx.analysisModel.workerKickDate
        assert not analysisCtx.analysisModel.workerHeartbeatDate

        with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
            AnalysisDAO.deleteBySMID(aDBS=analysisCtx.aDBS, smID=analysisCtx.analysisModel.smID)

    # ---------------------- COMPLETE --------------------------------------- #
    @logFunc()
    def complete(self, analysisCtx, status):  # pylint: disable=R0201
        """
        Complete Analysis
        """
        assert analysisCtx.analysisModel

        with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
            # Lock analysis row
            # Only update if it hasn't already been completed
            AnalysisDAO.lockSMID(aDBS=analysisCtx.aDBS, smID=analysisCtx.analysisSMID)
            analysisModel  = analysisCtx.analysisModel
            analysisCtx.aDBS.refresh(analysisModel)
            existingStatus = analysisModel.status
            if existingStatus.isInProgress:
                AnalysisDAO.update(aDBS=analysisCtx.aDBS, analysisModel=analysisModel,
                                   status=status, completeDate=datetime.utcnow())

            # Delete corresponding hold
            unitLedgerHoldModel = analysisModel.unitLedgerHoldModel
            if unitLedgerHoldModel:
                UnitLedgerDAO.deleteBySMID(aDBS=analysisCtx.aDBS, smID=unitLedgerHoldModel.smID)

    # ---------------------- KICK WORKERS ----------------------------------- #
    @logFunc()
    def kickWorkers(self, analysisCtx, analyzeOneKwargList):
        """
        Kick Workers
        """
        assert analysisCtx.analysisModel
        assert analysisCtx.analysisManager

        workerList = []

        # Kick workers, giving each analysisManager.nAnalysesPerWorker units
        for analyzeOneKwarg in analyzeOneKwargList:
            workerList.append(analyzeOneKwarg)
            if len(workerList) >= analysisCtx.analysisManager.nAnalysesPerWorker:
                self.worker.kick(analysisCtx, workerList)
                workerList = []

        # If any remaining, kick worker with a partial load
        if workerList:
            self.worker.kick(analysisCtx=analysisCtx, analyzeOneKwargList=workerList)
