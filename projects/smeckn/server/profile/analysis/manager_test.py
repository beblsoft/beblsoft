#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Analysis Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import math
import logging
from datetime import datetime, timedelta
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO
from smeckn.server.aDB.analysis.dao import AnalysisDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- START TEST CASE ------------------------------ #
class AnalysisManagerStartTestCase(CommonTestCase):
    """
    Test Analysis Manager Start
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.status           = AnalysisStatus.IN_PROGRESS
        self.contentCount     = 100
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)

    @logFunc()
    def test_success(self):
        """
        Test success case
        """
        nAnalysesPerWorker = 7  # Ensure last worker getting partial load
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.analysisManager, attribute="analyzeOne", autospec=True) as analyzeOneMock, \
                patch.dict(self.analysisManager.__dict__, countAnalyzed=self.contentCount,
                           lenAnalyzeOneKwargList=self.contentCount) as _, \
                patch.dict(self.cfg.__dict__, PROFILE_STUB_POST_COMPSENTA_NPERWORKER=nAnalysesPerWorker) as _:

            # Start analysis, this will kick several worker threads
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisModel = self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=self.popProfileSMID,
                                                            contentType=self.contentType, analysisType=self.analysisType)

            # Wait for analysis to complete
            self.mgr.jqueue.joinWorkers()
            self.assertEqual(analysisCtx.analysisModel, analysisModel)
            self.assertIsNotNone(analysisModel)
            self.assertGreaterEqual(analysisModel.workerHeartbeatDate, self.testStartDate)
            self.assertGreaterEqual(analysisModel.workerKickDate, self.testStartDate)
            self.assertEqual(analyzeOneMock.call_count, self.contentCount)
            self.assertIsNotNone(analysisModel.status, AnalysisStatus.SUCCESS)

    @logFunc()
    def test_badProfile(self):
        """
        Test bad profile
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            with self.assertRaises(BeblsoftError) as cm:
                _ = self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=234987,
                                                    contentType=self.contentType, analysisType=self.analysisType)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.PROFILE_DOES_NOT_EXIST)

    @logFunc()
    def test_badContentType(self):
        """
        Test bad content type
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            with self.assertRaises(BeblsoftError) as cm:
                _ = self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=self.popProfileSMID,
                                                    contentType=ContentType.POST_MESSAGE, analysisType=self.analysisType)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.PROFILE_NO_CONTENT_MANAGER)

    @logFunc()
    def test_badAnalysisType(self):
        """
        Test bad analysis type
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            with self.assertRaises(BeblsoftError) as cm:
                _ = self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=self.popProfileSMID,
                                                    contentType=self.contentType, analysisType=AnalysisType.COMPREHEND_LANGUAGE)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.PROFILE_NO_CONTENT_ANALYSIS_MANAGER)

    @logFunc()
    def test_alreadyAnalyzed(self):
        """
        Test case where all content is already analyzed
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.analysisManager, attribute="analyzeOne", autospec=True) as analyzeOneMock, \
                patch.dict(self.analysisManager.__dict__, lenAnalyzeOneKwargList=0) as _:
            # Start analysis, verify error
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            with self.assertRaises(BeblsoftError) as cm:
                self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=self.popProfileSMID,
                                                contentType=self.contentType, analysisType=self.analysisType)

            self.assertEqual(cm.exception.code, BeblsoftErrorCode.ANALYSIS_NOTHING_TO_ANALYZE)
            self.assertEqual(analyzeOneMock.call_count, 0)

            # Verify analysis deleted
            analysisModelCount = AnalysisDAO.getAll(aDBS=aDBS, count=True)
            self.assertEqual(analysisModelCount, 0)

    @logFunc()
    def test_notEnoughUnits(self):
        """
        Test not having enough analysis units for analysis
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.analysisManager, attribute="analyzeOne", autospec=True) as analyzeOneMock:
            # Delete all units
            with AccountDatabase.transactionScope(session=aDBS):
                UnitLedgerDAO.deleteAll(aDBS=aDBS)

            # Start analysis, verify error
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            with self.assertRaises(BeblsoftError) as cm:
                self.mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=self.popProfileSMID,
                                                contentType=self.contentType, analysisType=self.analysisType)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.LEDGER_NOT_ENOUGH_UNITS_FOR_HOLD)
            self.assertEqual(analyzeOneMock.call_count, 0)

            # Verify analysis deleted
            analysisModelCount = AnalysisDAO.getAll(aDBS=aDBS, count=True)
            self.assertEqual(analysisModelCount, 0)


# ---------------------------- CHECK COMPLETE TEST CASE --------------------- #
class AnalysisManagerCheckCompleteTestCase(CommonTestCase):
    """
    Test Analysis Manager Check Complete
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.status           = AnalysisStatus.IN_PROGRESS
        self.contentCount     = 100
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType,
                                                               status=self.status, contentCount=self.contentCount)

    @logFunc()
    def verifyCheckCompleteStatus(self, countAnalyzed, status):
        """
        Verify check complete status after setting countAnalyzed and status
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.dict(self.analysisManager.__dict__, countAnalyzed=countAnalyzed) as _:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            self.mgr.profile.analysis.checkComplete(analysisCtx=analysisCtx)
            self.assertEqual(analysisCtx.analysisModel.status, status)

    @logFunc()
    def test_progressDone(self):  # pylint: disable=C0111
        self.verifyCheckCompleteStatus(countAnalyzed=self.contentCount, status=AnalysisStatus.SUCCESS)

    @logFunc()
    def test_progressNotDone(self):  # pylint: disable=C0111
        self.verifyCheckCompleteStatus(countAnalyzed=self.contentCount - 1, status=AnalysisStatus.IN_PROGRESS)


# ---------------------------- CHECK TIMEOUT TEST CASE ---------------------- #
class AnalysisManagerCheckTimeoutTestCase(CommonTestCase):
    """
    Test Analysis Manager Check Timeout
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.status           = AnalysisStatus.IN_PROGRESS
        self.profileManager   = self.mgr.profileStub
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType,
                                                               status=self.status)
        self.staleKickDate    = datetime.utcnow() - self.cfg.PROFILE_ANALYSIS_MAX_IDLE_WORKERKICK - timedelta(seconds=5)
        self.staleHBDate      = datetime.utcnow() - self.cfg.PROFILE_ANALYSIS_MAX_IDLE_WORKERHB - timedelta(seconds=5)

    @logFunc()
    def verifyCheckTimeoutStatus(self, workerKickDate, workerHeartbeatDate, status):
        """
        Verify check timeout status
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            # Update analysis with new dates
            with AccountDatabase.transactionScope(session=aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisCtx.analysisModel,
                                   workerKickDate=workerKickDate, workerHeartbeatDate=workerHeartbeatDate)

            # Check timeout and verify status
            self.mgr.profile.analysis.checkTimeout(analysisCtx=analysisCtx)
            self.assertEqual(analysisCtx.analysisModel.status, status)

    @logFunc()
    def test_noDates(self):  # pylint: disable=C0111
        self.verifyCheckTimeoutStatus(workerKickDate=None, workerHeartbeatDate=None,
                                      status=AnalysisStatus.TIMED_OUT)

    @logFunc()
    def test_idleDates(self):  # pylint: disable=C0111
        self.verifyCheckTimeoutStatus(workerKickDate=self.staleKickDate, workerHeartbeatDate=self.staleHBDate,
                                      status=AnalysisStatus.TIMED_OUT)

    @logFunc()
    def test_oneAliveDate(self):  # pylint: disable=C0111
        self.verifyCheckTimeoutStatus(workerKickDate=datetime.utcnow(), workerHeartbeatDate=self.staleHBDate,
                                      status=self.status)
        self.verifyCheckTimeoutStatus(workerKickDate=self.staleKickDate, workerHeartbeatDate=datetime.utcnow(),
                                      status=self.status)


# ---------------------------- COMPLETE TEST CASE --------------------------- #
class AnalysisManagerCompleteTestCase(CommonTestCase):
    """
    Test Analysis Manager Complete
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.profileManager   = self.mgr.profileStub
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType)

    @logFunc()
    def verifyComplete(self, unitLedgerHoldSMID, startStatus, completeStatus, endStatus):
        """
        Verify analysis successfully completed
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analysisModel = analysisCtx.analysisModel
            # Update analysis according to inputs
            with AccountDatabase.transactionScope(session=aDBS):
                completeDate = datetime.utcnow() if startStatus != AnalysisStatus.IN_PROGRESS else None
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisCtx.analysisModel,
                                   unitLedgerHoldSMID=unitLedgerHoldSMID, status=startStatus, completeDate=completeDate)
            self.assertEqual(analysisModel.unitLedgerHoldSMID, unitLedgerHoldSMID)

            # Complete analysis, verify hold and status appropriately
            self.mgr.profile.analysis.complete(analysisCtx=analysisCtx, status=completeStatus)
            self.assertIsNone(analysisModel.unitLedgerHoldSMID)
            self.assertEqual(analysisModel.status, endStatus)
            self.assertGreaterEqual(analysisModel.completeDate, self.testStartDate)

    @logFunc()
    def test_holdDeleted(self):  # pylint: disable=C0111
        self.verifyComplete(unitLedgerHoldSMID=self.unitTypeDict[UnitType.TEXT_ANALYSIS].unitLedgerHoldSMID,
                            startStatus=AnalysisStatus.IN_PROGRESS, completeStatus=AnalysisStatus.SUCCESS,
                            endStatus=AnalysisStatus.SUCCESS)

    @logFunc()
    def test_alreadyCompleted(self):
        """
        Verify second completion doesn't override the first
        """
        self.verifyComplete(unitLedgerHoldSMID=None,
                            startStatus=AnalysisStatus.SUCCESS, completeStatus=AnalysisStatus.TIMED_OUT,
                            endStatus=AnalysisStatus.SUCCESS)
        self.verifyComplete(unitLedgerHoldSMID=None,
                            startStatus=AnalysisStatus.TIMED_OUT, completeStatus=AnalysisStatus.SUCCESS,
                            endStatus=AnalysisStatus.TIMED_OUT)


# ---------------------------- KICK WORKERS TEST CASE ----------------------- #
class AnalysisManagerKickWorkersTestCase(CommonTestCase):
    """
    Test Analysis Manager Kick Workers
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType)

    @logFunc()
    def verifyNWorkerKicks(self, lenAnalyzeOneKwargList, nAnalysesPerWorker):
        """
        Verify the number of times worker main is called
        """
        nWorkerMainCalls = math.ceil(float(lenAnalyzeOneKwargList) / nAnalysesPerWorker)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.mgr.profile.analysis.worker, attribute="main", autospec=True) as workerMainMock, \
                patch.dict(self.analysisManager.__dict__, lenAnalyzeOneKwargList=lenAnalyzeOneKwargList) as _, \
                patch.dict(self.cfg.__dict__, PROFILE_STUB_POST_COMPSENTA_NPERWORKER=nAnalysesPerWorker) as _:
            # Kick workers
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analyzeOneKwargList = self.analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)
            self.mgr.profile.analysis.kickWorkers(analysisCtx=analysisCtx, analyzeOneKwargList=analyzeOneKwargList)

            # Wait for workers to execute
            self.mgr.jqueue.joinWorkers()
            self.assertEqual(workerMainMock.call_count, nWorkerMainCalls)

    @logFunc()
    def test_fullWorkerCount(self):  # pylint: disable=C0111
        self.verifyNWorkerKicks(lenAnalyzeOneKwargList=100, nAnalysesPerWorker=10)

    @logFunc()
    def test_lastWorkerGetsLess(self):  # pylint: disable=C0111
        self.verifyNWorkerKicks(lenAnalyzeOneKwargList=93, nAnalysesPerWorker=9)
