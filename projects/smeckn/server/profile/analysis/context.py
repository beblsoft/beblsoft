#!/usr/bin/env python3
"""
 NAME:
  context.py

 DESCRIPTION
  Analysis Context Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.aDB.analysis.dao import AnalysisDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS CONTEXT ------------------------------- #
class AnalysisContext():
    """
    Analysis Context Class
    """

    def __init__(self, mgr, aDBS, aJob=None):
        """
        Initialize object
        """
        self.mgr               = mgr
        self.aDBS              = aDBS
        self.aJob              = aJob
        self.profileSMID       = None
        self.profileModel      = None
        self.profileManager    = None
        self.contentType       = None
        self.contentManager    = None
        self.analysisManager   = None
        self.analysisSMID      = None
        self.analysisModel     = None

    @logFunc()
    def loadProfile(self, profileSMID):
        """
        Load profile
        """
        self.profileSMID       = profileSMID
        self.profileModel      = ProfileDAO.getBySMID(aDBS=self.aDBS, smID=profileSMID)
        self.profileManager    = self.mgr.profile.getProfileManager(profileType=self.profileModel.type)

    @logFunc()
    def loadContentAnalysis(self, contentType, analysisType):
        """
        Load content analysis
        """
        self.contentManager    = self.profileManager.getContentManager(contentType=contentType)
        self.analysisManager   = self.profileManager.getAnalysisManager(contentType=contentType,
                                                                             analysisType=analysisType)
    @logFunc()
    def loadAnalysis(self, analysisSMID):
        """
        Load analysis
        """
        self.analysisSMID      = analysisSMID
        self.analysisModel     = AnalysisDAO.getBySMID(aDBS=self.aDBS, smID=analysisSMID)
        self.loadProfile(profileSMID=self.analysisModel.profileSMID)
        self.loadContentAnalysis(contentType=self.analysisModel.contentType,
                                 analysisType=self.analysisModel.analysisType)

    @logFunc()
    def tryLoadAnalysis(self, analysisSMID):
        """
        Try loading analysis
        This could fail if profile or analysis has been deleted

        Returns
          True on Success
          False otherwise
        """
        rval = False
        try:
            self.loadAnalysis(analysisSMID=analysisSMID)
            rval = True
        except BeblsoftError as e:
            if e.code == BeblsoftErrorCode.PROFILE_DOES_NOT_EXIST:
                pass
            elif e.code == BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST:
                pass
            else:
                raise e
        return rval
