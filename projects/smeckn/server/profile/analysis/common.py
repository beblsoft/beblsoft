#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Content Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import abc
import logging
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- COMMON CONTENT ANALYSIS MANAGER ---------------- #
class CommonContentAnalysisManager(CommonManager, abc.ABC):
    """
    Common Content Analysis Manager Class
    """

    def __init__(self, mgr, contentMgr, **kwargs):
        """
        Initialize object
        """
        super().__init__(mgr=mgr, **kwargs)
        self.contentMgr  = contentMgr
        self.contentType = contentMgr.contentType

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    @abc.abstractmethod
    def analysisType(self):
        """
        Return analysis type
        Ex. AnalysisType.COMPREHEND_SENTIMENT
        """
        pass

    @property
    @abc.abstractmethod
    def unitType(self):
        """
        Return analysis unit
        Hold will be created with this unit type before the analysis begins
        Ex. UnitType.TEXT_ANALYSIS
        """
        pass

    @property
    @abc.abstractmethod
    def nAnalysesPerWorker(self):
        """
        Return number of analyses that an individual worker can perform
        Ex. 10
        """
        pass

    # ---------------------- GET COUNT MODEL -------------------------------- #
    @abc.abstractmethod
    def getCountModel(self, analysisCtx):
        """
        Return AnalysisCountModel
        Args
          analysisCtx:
            Analysis Context, profile loaded, managers not loaded, analysis not loaded

        Returns
          AnalysisCountModel
        """
        pass

    # ---------------------- GET PROGRESS ----------------------------------- #
    @abc.abstractmethod
    def getProgress(self, analysisCtx):
        """
        Return analysis progress model
        Args
          analysisCtx:
            Analysis Context, analysisSMID loaded

        Returns
          AnalysisProgressModel
        """
        pass

    # ---------------------- GET ANALYZE ONE KWARG LIST LIST ---------------- #
    @abc.abstractmethod
    def getAnalyzeOneKwargList(self, analysisCtx):
        """
        Return list of analyzeOne kwargs
        These will be passed to analyzeOne in a separate worker context
        The length of the list should equal AnalysisCountModel.countNotDone
        Args
          analysisCtx:
            Analysis Context, profile loaded

        Returns
          List of analyzeOne kwargs
          Must be encodable
          Ex. [{"smID": 98123}, {"smID": 120398}, ... ]
        """
        pass

    # ---------------------- ANALYZE ONE ------------------------------------ #
    @abc.abstractmethod
    def analyzeOne(self, analysisCtx, **kwargs):
        """
        Analyze one piece of content

        Exception Note
          This function should attempt to catch and handle all known exceptions
          and not propagate them up to the AnalysisWorkerManager.main layer

          These will include various SQLAlchemy exceptions that cannot be
          known appriori. Fill in after cloud sends an email.

        Exceptions include:
          - Content deleted
          - Analysis timed out and hold deleted, no analysis units left

        Args
          analysisCtx:
            Analysis Context, fully loaded
          kwargs:
            kwargs used to analyze
            Ex. { "smID": 23948 }

        Returns
          N/A
        """
        pass
