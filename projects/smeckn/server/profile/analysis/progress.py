#!/usr/bin/env python3
"""
 NAME:
  progress.py

 DESCRIPTION
  Analysis Progress Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS PROGRESS MANAGER ---------------------- #
class AnalysisProgressManager(CommonManager):
    """
    Analysis Progress Manager Class
    """

    # ---------------------- GET -------------------------------------------- #
    @logFunc()
    def get(self, analysisCtx, analysisSMID):
        """
        Get progress

        Args:
          analysisCtx:
            analysisCtx, nothing loaded

        Returns
          AnalysisProgressModel
        """
        analysisCtx.loadAnalysis(analysisSMID=analysisSMID)

        # Do the following:
        # - Optionally complete analysis
        # - Optionally timeout analysis
        self.mgr.profile.analysis.checkComplete(analysisCtx=analysisCtx)
        self.mgr.profile.analysis.checkTimeout(analysisCtx=analysisCtx)

        # Get progress
        return analysisCtx.analysisManager.getProgress(analysisCtx=analysisCtx)
