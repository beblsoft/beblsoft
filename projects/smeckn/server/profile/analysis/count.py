#!/usr/bin/env python3
"""
 NAME:
  progress.py

 DESCRIPTION
  Analysis Count Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS COUNT MANAGER ------------------------- #
class AnalysisCountManager(CommonManager):
    """
    Analysis Count Manager Class
    """

    # ---------------------- GET -------------------------------------------- #
    @logFunc()
    def get(self, analysisCtx, profileSMID, contentType, analysisType): #pylint: disable=R0201
        """
        Get progress

        Args:
          analysisCtx:
            analysisCtx, nothing loaded

        Returns
          AnalysisCountModel
        """
        analysisCtx.loadProfile(profileSMID=profileSMID)
        analysisCtx.loadContentAnalysis(contentType=contentType, analysisType=analysisType)
        return analysisCtx.analysisManager.getCountModel(analysisCtx=analysisCtx)
