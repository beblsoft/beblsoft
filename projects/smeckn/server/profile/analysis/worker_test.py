#!/usr/bin/env python3
"""
 NAME:
  worker_test.py

 DESCRIPTION
  Analysis Worker Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AnalysisWorkerManagerTestCase(CommonTestCase):
    """
    Test Analysis Worker Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.status           = AnalysisStatus.IN_PROGRESS
        self.contentCount     = 100
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType,
                                                               status=self.status, contentCount=self.contentCount)

    @logFunc()
    def test_mainSuccess(self):
        """
        Test main success case
        All content is analyzed, analysis status should be success
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.object(self.analysisManager, attribute="analyzeOne", autospec=True) as analyzeOneMock, \
                patch.dict(self.analysisManager.__dict__, countAnalyzed=self.contentCount,
                           lenAnalyzeOneKwargList=self.contentCount) as _:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)

            analyzeOneKwargList = self.analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)
            self.mgr.profile.analysis.worker.main(analysisCtx=analysisCtx, analysisSMID=self.analysisModel.smID,
                                                  analyzeOneKwargList=analyzeOneKwargList)
            self.assertEqual(analysisCtx.analysisModel.status, AnalysisStatus.SUCCESS)
            self.assertGreaterEqual(analysisCtx.analysisModel.workerHeartbeatDate, self.testStartDate)
            self.assertEqual(analyzeOneMock.call_count, self.contentCount)

    @logFunc()
    def test_heartbeat(self):
        """
        Test heartbeat
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)

            self.mgr.profile.analysis.worker.heartbeat(analysisCtx=analysisCtx)
            self.assertGreaterEqual(analysisCtx.analysisModel.workerHeartbeatDate, self.testStartDate)

    @logFunc()
    def test_kick(self):
        """
        Test kick
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.dict(self.analysisManager.__dict__, countAnalyzed=self.contentCount,
                           lenAnalyzeOneKwargList=self.contentCount) as _:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)

            self.assertEqual(analysisCtx.analysisModel.nWorkerKicks, 0)
            analyzeOneKwargList = self.analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)

            # Kick worker
            self.mgr.profile.analysis.worker.kick(analysisCtx=analysisCtx, analyzeOneKwargList=analyzeOneKwargList)

            # Wait for worker to complete
            # Analysis should be done successfully
            self.mgr.jqueue.joinWorkers()
            aDBS.refresh(analysisCtx.analysisModel)
            self.assertEqual(analysisCtx.analysisModel.nWorkerKicks, 1)
            self.assertGreaterEqual(analysisCtx.analysisModel.workerKickDate, self.testStartDate)
            self.assertEqual(analysisCtx.analysisModel.status, AnalysisStatus.SUCCESS)
