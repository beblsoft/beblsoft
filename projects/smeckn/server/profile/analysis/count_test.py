#!/usr/bin/env python3
"""
 NAME:
  count_test.py

 DESCRIPTION
  Analysis Count Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AnalysisCountManagerTestCase(CommonTestCase):
    """
    Test Analysis Count Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.contentCount     = 100
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,

                                                                       analysisType=self.analysisType)

    @logFunc()
    def test_get(self):
        """
        Test get
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCountModel = self.mgr.profile.analysis.count.get(analysisCtx=analysisCtx,
                                                                     profileSMID=self.popProfileSMID,
                                                                     contentType=self.contentType,
                                                                     analysisType=self.analysisType)

            self.assertEqual(analysisCountModel.profileSMID, analysisCtx.profileSMID)
            self.assertEqual(analysisCountModel.contentType, self.contentType)
            self.assertEqual(analysisCountModel.analysisType, self.analysisType)
            self.assertEqual(analysisCountModel.countDone, self.analysisManager.countDone)
            self.assertEqual(analysisCountModel.countNotDone, self.analysisManager.countNotDone)
            self.assertEqual(analysisCountModel.countCant, self.analysisManager.countCant)
