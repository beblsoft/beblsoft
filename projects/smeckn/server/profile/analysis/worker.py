#!/usr/bin/env python3
"""
 NAME:
  worker.py

 DESCRIPTION
  Analysis Worker Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server.aJob.analysisWorker import AnalysisWorkerJob
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.analysis.dao import AnalysisDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- ANALYSIS WORKER MANAGER ------------------------ #
class AnalysisWorkerManager(CommonManager):
    """
    Analysis Worker Manager Class
    """

    # ---------------------- MAIN ------------------------------------------- #
    @logFunc()
    def main(self, analysisCtx, analysisSMID, analyzeOneKwargList):
        """
        Worker main
        Args
          analysisCtx:
            AnalysisContext, not loaded
          analsisSMID:
            Analysis SMID
            Ex. 5
          analyzeOneKwargList:
            List of kwargs to pass to each analyze one
        """
        loadedAnalysisCtx = analysisCtx.tryLoadAnalysis(analysisSMID=analysisSMID)

        if loadedAnalysisCtx:
            # Loop, analyzing each kwarg
            for analyzeOneKwarg in analyzeOneKwargList:
                self.heartbeat(analysisCtx=analysisCtx)
                analysisCtx.analysisManager.analyzeOne(analysisCtx=analysisCtx, **analyzeOneKwarg)

            # Check Complete
            self.mgr.profile.analysis.checkComplete(analysisCtx)

    # ---------------------- HEARTBEAT -------------------------------------- #
    @logFunc()
    def heartbeat(self, analysisCtx):  # pylint: disable=R0201
        """
        Heartbeat
        """
        assert analysisCtx.analysisModel

        with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
            AnalysisDAO.update(aDBS=analysisCtx.aDBS, analysisModel=analysisCtx.analysisModel,
                               workerHeartbeatDate=datetime.utcnow())

    # ---------------------- KICK ------------------------------------------- #
    @logFunc()
    def kick(self, analysisCtx, analyzeOneKwargList):  # pylint: disable=R0201
        """
        Kick Worker
        """
        assert analysisCtx.analysisSMID
        assert analysisCtx.analysisModel

        with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
            # Lock analysis row
            # Kick worker
            # Update database with worker kick
            AnalysisDAO.lockSMID(aDBS=analysisCtx.aDBS, smID=analysisCtx.analysisSMID)
            analysisCtx.aDBS.refresh(analysisCtx.analysisModel)

            AnalysisWorkerJob.invokeAsync(
                mgr           = self.mgr,
                classKwargs   = AnalysisWorkerJob.getClassKwargsFromADBS(aDBS=analysisCtx.aDBS),
                executeKwargs = {"analysisSMID": analysisCtx.analysisSMID,
                                 "analyzeOneKwargList": analyzeOneKwargList})

            AnalysisDAO.update(aDBS=analysisCtx.aDBS, analysisModel=analysisCtx.analysisModel,
                               workerKickDate=datetime.utcnow(),
                               nWorkerKicks=analysisCtx.analysisModel.nWorkerKicks + 1)
