#!/usr/bin/env python3
"""
 NAME:
  progress_test.py

 DESCRIPTION
  Analysis Progress Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.aDB.analysis.dao import AnalysisDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AnalysisProgressManagerTestCase(CommonTestCase):
    """
    Test Analysis Progress Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.contentCount     = 100
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)
        self.analysisModel    = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                               contentType=self.contentType,
                                                               analysisType=self.analysisType,
                                                               contentCount=self.contentCount)

    @logFunc()
    def verifyGetProgress(self, countAnalyzed, workerKickDate, startStatus, endStatus):
        """
        Verify get progress
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.dict(self.analysisManager.__dict__, countAnalyzed=countAnalyzed) as _:
            analysisCtx = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analysisModel = analysisCtx.analysisModel

            # Update workerKickDate
            with AccountDatabase.transactionScope(session=aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisModel, workerKickDate=workerKickDate,
                                   status=startStatus)

            # Get progress
            analysisProgressModel = self.mgr.profile.analysis.progress.get(analysisCtx=analysisCtx,
                                                                           analysisSMID=self.analysisModel.smID)
            self.assertEqual(analysisProgressModel.contentType, self.contentType)
            self.assertEqual(analysisProgressModel.analysisType, self.analysisType)
            self.assertEqual(analysisProgressModel.countAnalyzed, countAnalyzed)
            self.assertEqual(analysisProgressModel.countTotal, self.contentCount)
            self.assertEqual(analysisProgressModel.status, endStatus)
            self.assertEqual(analysisModel.status, endStatus)

    @logFunc()
    def test_getInProgress(self): #pylint: disable=C0111
        self.verifyGetProgress(countAnalyzed=self.contentCount - 1, workerKickDate=datetime.utcnow(),
                               startStatus=AnalysisStatus.IN_PROGRESS, endStatus=AnalysisStatus.IN_PROGRESS)

    @logFunc()
    def test_getComplete(self): #pylint: disable=C0111
        self.verifyGetProgress(countAnalyzed=self.contentCount, workerKickDate=datetime.utcnow(),
                               startStatus=AnalysisStatus.IN_PROGRESS, endStatus=AnalysisStatus.SUCCESS)

    @logFunc()
    def test_getTimeout(self): #pylint: disable=C0111
        self.verifyGetProgress(countAnalyzed=self.contentCount - 1, workerKickDate=None,
                               startStatus=AnalysisStatus.IN_PROGRESS, endStatus=AnalysisStatus.TIMED_OUT)

    @logFunc()
    def test_getTimeoutAndComplete(self): #pylint: disable=C0111
        self.verifyGetProgress(countAnalyzed=self.contentCount, workerKickDate=None,
                               startStatus=AnalysisStatus.IN_PROGRESS, endStatus=AnalysisStatus.SUCCESS)
