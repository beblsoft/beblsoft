#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Histogram Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.histogram.context import HistogramContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.groupBy.model import GroupByType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- HISTOGRAM MANAGER TEST CASE ------------------ #
class HistogramManagerTestCase(CommonTestCase):
    """
    Test Histogram Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)

    @logFunc()
    def test_getList(self):
        """
        Test getList
        """
        groupByTypeList = [GroupByType.FACEBOOK_POST_CREATE_DATE,
                           GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT]
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for groupByType in groupByTypeList:
                histogramCtx = HistogramContext(
                    mgr=self.mgr, aDBS=aDBS,
                    profileSMID=self.popProfileSMID,
                    contentType=ContentType.POST,
                    contentAttrType=ContentAttrType.FACEBOOK_POST_SMID,
                    groupByType=groupByType,
                    intervalStartDate=self.testStartDate,
                    intervalEndDate=self.testStartDate + timedelta(seconds=10))

                histogramModelList = self.mgr.profile.histogram.getList(histogramCtx=histogramCtx)
                self.assertEqual(len(histogramModelList), 1)
