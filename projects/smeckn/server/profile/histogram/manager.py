#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Histogram Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB.profile.dao import ProfileDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- HISTOGRAM MANAGER ------------------------------ #
class HistogramManager(CommonManager):
    """
    Histogram Manager Class
    """

    # ---------------------- GET -------------------------------------------- #
    @logFunc()
    def getList(self, histogramCtx): #pylint: disable=R0201
        """
        Get histogram
        Args
          histogramCtx:
            HistogramContext, fully loaded

        Returns
          HistogramModelList
        """
        profileModel   = ProfileDAO.getBySMID(aDBS=histogramCtx.aDBS, smID=histogramCtx.profileSMID)
        profileManager = self.mgr.profile.getProfileManager(profileType=profileModel.type)
        histogramDAO   = profileManager.getHistogramDAO(contentType=histogramCtx.contentType)

        return histogramDAO.getList(histogramCtx=histogramCtx)
