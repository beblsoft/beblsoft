#!/usr/bin/env python3
"""
 NAME:
   manager.py

 DESCRIPTION
   Profile Manager Functionality

 PROFILE HIERARCHY
   Profiles are organized through a hierarchy of managers with defined interfaces.
   The hierarchy is organized as follows:
     1. ProfileType
     2. ContentType
     3. ActionType

 COMMON INTERFACES
   All profile actions are access through common interfaces:
   Sync start                  : mgr.profile.sync.start
   Sync get progress           : mgr.profile.sync.progress.get
   Analysis start              : mgr.profile.analysis.start
   Analysis get progress       : mgr.profile.analysis.progress.get
   Content count get           : mgr.profile.count.get
   Statistic create            : mgr.profile.statistic.create
   Histogram create            : mgr.profile.histogram.create

 EXAMPLE
   FacebookProfileManager(CommonProfileTypeManager)
    |
    |- PostManager(CommonContentManager)
       |- PostSyncManager(CommonContentSyncManager)
       |- PostCountManager(CommonContentCountManager)
       |- PostStatisticManager(CommonContentStatisticManager)
       |- PostHistogramManager(CommonContentHistogramManager)
    |
    |- PostMessageManager(CommonContentManager)
       |- PostMessageCompSentAnalysisManager(CommonAnalysisManager)
       |- PostMessageCountManager(CommonContentCountManager)
       |- PostMessageStatisticManager(CommonContentStatisticManager)
       |- PostMessageHistogramManager(CommonContentHistogramManager)
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.manager.common import CommonManager
from smeckn.server.profile.sync.manager import SyncManager
from smeckn.server.profile.analysis.manager import AnalysisManager
from smeckn.server.profile.statistic.manager import StatisticManager
from smeckn.server.profile.histogram.manager import HistogramManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- PROFILE MANAGER -------------------------------- #
class ProfileManager(CommonManager):
    """
    Profile Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Common Access Interfaces
        self.sync      = SyncManager(mgr=self.mgr)
        self.analysis  = AnalysisManager(mgr=self.mgr)
        self.statistic = StatisticManager(mgr=self.mgr)
        self.histogram = HistogramManager(mgr=self.mgr)

    # ---------------------- PROFILE ---------------------------------------- #
    @property
    def profileManagerDict(self):
        """
        Return dictionary mapping profileType to profileTypeManager
        Ex. { ProfileType.FACEBOOK : self.mgr.facebook }
        """
        l = [self.mgr.profileStub, self.mgr.facebook]
        return {profileManager.profileType: profileManager for profileManager in l}

    @logFunc()
    def getProfileManager(self, profileType, raiseOnNone=True):
        """
        Return profile manager for type
        """
        profileManager = self.profileManagerDict.get(profileType, None)
        if not profileManager and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_NO_MANAGER,
                                extMsgFormatDict={"profileType": profileType.name})
        return profileManager
