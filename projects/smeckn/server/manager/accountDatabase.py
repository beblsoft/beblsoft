#!/usr/bin/env python3
"""
 NAME:
  accountDatabase.py

 DESCRIPTION
  Account Database Manager Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from smeckn.server.manager.common import CommonManager
from smeckn.server.gDB.account.manager import AccountManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.gDB.account.dao import AccountDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- ACCOUNT DATABASE MANAGER CLASS --------------- #
class AccountDatabaseManager(CommonManager):
    """
    Account Database Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.dbp  = self.mgr.dbp
        self.gDB  = self.dbp.gDB

    # --------------------- HELPERS ----------------------------------------- #
    @logFunc()
    def getADB(self, aID=None, email=None):
        """
        Return ADB given input
        """
        aDB = None
        if aID:
            aDB = AccountDatabase.fromID(gDB=self.gDB, aID=aID)
        elif email:
            aDB = AccountDatabase.fromEmail(gDB=self.gDB, email=email)
        if not aDB:
            raise BeblsoftError(msg="Must specify aID or email")
        return aDB

    # --------------------- CRUD -------------------------------------------- #
    @logFunc()
    def describe(self, aID=None, email=None):
        """
        Describe
        """
        aDB = self.getADB(aID, email)
        return aDB.describe()

    # --------------------- VERSIONS ---------------------------------------- #
    @logFunc()
    def getNNotAtVersion(self, version, aIDMod=None, aIDModEquals=None):
        """
        Return number of databases not at version
        """
        with self.gDB.sessionScope(commit=False) as gs:
            return AccountDAO.getAll(gs=gs, notDBVersion=version,
                                     aIDMod=aIDMod, aIDModEquals=aIDModEquals,
                                     count=True)

    @logFunc()
    def upgrade(self, toVersion, aIDMod=None, aIDModEquals=None):
        """
        Upgrade
        """
        AccountManager.upgradeToVersion(self.gDB, toVersion=toVersion,
                                        aIDMod=aIDMod, aIDModEquals=aIDModEquals)

    @logFunc()
    def downgrade(self, toVersion, aIDMod=None, aIDModEquals=None):
        """
        Downgrade
        """
        AccountManager.downgradeToVersion(self.gDB, toVersion=toVersion,
                                          aIDMod=aIDMod, aIDModEquals=aIDModEquals)

    # --------------------- RUN SQL ----------------------------------------- #
    @logFunc()
    def runSQL(self, aID=None, email=None, sqlStatement=""):
        """
        Run SQL
        """
        aDB = self.getADB(aID, email)
        return aDB.runSQL(sqlStatement=sqlStatement)
