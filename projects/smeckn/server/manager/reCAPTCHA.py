#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 reCAPTCHA.py

DESCRIPTION
 ReCAPTCHA Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.server.manager.common import CommonManager
from base.recaptcha.python.bReCAPTCHA import BeblsoftReCAPTCHA



# ------------------------ THIRD PARTY MANAGER OBJECT ----------------------- #
class ReCAPTCHAManager(CommonManager):
    """
    ReCAPTCHA Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bReCAPTCHA  = BeblsoftReCAPTCHA(
            serverSecret      = self.cfg.RECAPTCHA_SECRET_KEY)
