#!/usr/bin/env python3
"""
 NAME:
  globalRevisionDatabase.py

 DESCRIPTION
  Global Revision Database Manager Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bServer import BeblsoftMySQLServer
from smeckn.server.manager.common import CommonManager
from smeckn.server.gDB import GlobalDatabase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- GLOBAL REVISION DATABASE MANAGER CLASS ------- #
class GlobalRevisionDatabaseManager(CommonManager):
    """
    Global Revision Database Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.gRDBS         = BeblsoftMySQLServer(
            domainNameFunc     = lambda: self.cfg.GRDB_DOMAIN,
            user               = self.cfg.GRDB_USER,
            password           = self.cfg.GRDB_PASSWORD)
        self.gRDB          = GlobalDatabase(
            bServer            = self.gRDBS,
            name               = self.cfg.GRDB_NAME,
            echo               = self.cfg.GRDB_ECHO)

    # --------------------- CRUD -------------------------------------------- #
    @logFunc()
    def create(self):  # pylint: disable=W0221
        """
        Create
        """
        self.gRDB.create()

    @logFunc()
    def delete(self):
        """
        Delete
        """
        self.gRDB.delete()

    @logFunc()
    def describe(self):
        """
        Describe
        """
        if self.gRDB.exists:
            self.gRDB.describe()

    # --------------------- MIGRATION --------------------------------------- #
    @logFunc()
    def revise(self, msg):
        """
        Revise
        """
        self.gRDB.delete()
        self.gRDB.create()
        self.gRDB.createTables(fromMigrations=True)
        self.gRDB.bDBMigrations.revise(msg=msg)
        self.gRDB.delete()

    @logFunc()
    def upgrade(self, toVersion):
        """
        Upgrade
        """
        self.gRDB.bDBMigrations.upgrade(toVersion=toVersion)

    @logFunc()
    def downgrade(self, toVersion):
        """
        Downgrade
        """
        self.gRDB.bDBMigrations.downgrade(toVersion=toVersion)
