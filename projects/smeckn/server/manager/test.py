#!/usr/bin/env python3
"""
 NAME:
  test.py

 DESCRIPTION
  Test Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import os
import enum
from pathlib import Path
import logging
import unittest
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------ TEST MODULES ------------------------------------- #
@enum.unique
class TestModule(enum.Enum):
    """
    Test type enumeration
    """
    ALL          = enum.auto()
    GDB          = enum.auto()
    GDBMIGRATION = enum.auto()
    GJOB         = enum.auto()
    ADB          = enum.auto()
    ADBMIGRATION = enum.auto()
    AJOB         = enum.auto()
    FACEBOOK     = enum.auto()
    PROFILE      = enum.auto()
    PAYMENTS     = enum.auto()
    ANALYSIS     = enum.auto()
    WAPIV1       = enum.auto()
    WEBTOKEN     = enum.auto()

    @property
    def startDirList(self):
        """
        Return list of TestModule directories
        """
        ttypeDict = {
            TestModule.ALL: [""],
            TestModule.GDB: ["gDB"],
            TestModule.GDBMIGRATION: ["gDBMigrations"],
            TestModule.GJOB: ["gJob"],
            TestModule.ADB: ["aDB"],
            TestModule.ADBMIGRATION: ["aDBMigrations"],
            TestModule.AJOB: ["aJob"],
            TestModule.FACEBOOK: ["facebook",
                                  "aDB/facebookProfile", "aDB/facebookPost", "aDB/facebookPhoto",
                                  "wAPIV1/facebookProfile", "wAPIV1/facebookPost"],
            TestModule.PROFILE: ["profile"],
            TestModule.PAYMENTS: ["payments"],
            TestModule.ANALYSIS: ["analysis"],
            TestModule.WAPIV1: ["wAPIV1"],
            TestModule.WEBTOKEN: ["webToken"]
        }
        serverDir    = Path(__file__).parents[1]  # smeckn/server
        startDirList = ttypeDict.get(self)
        return [os.path.join(serverDir, sd) for sd in startDirList]

    def getTestSuite(self):
        """
        Return corresponding test suite
        """
        pattern   = "*_test.py"
        testSuite = unittest.TestSuite()
        serverDir = Path(__file__).parents[1]  # smeckn/server
        for startDir in self.startDirList:
            curTestSuite = unittest.TestLoader().discover(start_dir=startDir, pattern=pattern,
                                                          top_level_dir=serverDir)
            testSuite.addTest(test=curTestSuite)
        return testSuite


# -------------------------- TEST MANAGER ----------------------------------- #
class TestManager(CommonManager):
    """
    Test Manager class
    """

    @logFunc()
    def test(self, module=None, name=None, vcrRecordMode="once",
             totalJobs=1, jobIDX=0):  # pylint: disable=R0201
        """
        Test Infrastructure
        Args
          module:
            Module to test
            Ex1. "WAPIV1"
          name:
            Path to module.
            Ex1. smeckn.server.sAPIV1
          vcrRecordMode:
            One of: "once", "new_episodes", "none", "all"
          totalJobs:
            Number of total jobs running tests in parallel
            Ex. 50
          jobIDX:
            Index of this job
            Used when multiple jobs are running the tests, this job only runs
            a specific subset of the total tests
            Range = [0, totalJobs - 1] inclusive
        Raises
          BeblsoftError if test failed
        """
        testLoaderSuite          = None
        self.cfg.VCR_RECORD_MODE = vcrRecordMode
        if name:
            testLoaderSuite      = unittest.TestLoader().loadTestsFromNames([name])
        if module:
            testLoaderSuite      = TestModule[module].getTestSuite()

        if not testLoaderSuite:
            raise BeblsoftError(msg="No tests specified")

        # Create list of all TestCases and then only run portion for
        # this specific job
        # Note:
        #  - unittest.TestSuite is an aggregation of individual TestCases and TestSuites
        #  - Divide all nested test suites into their c
        testList     = [test for test in testLoaderSuite] #pylint: disable=R1721
        testCaseList = []
        while testList:
            test = testList.pop()
            if isinstance(test, unittest.TestSuite):
                for t in test:
                    testList.append(t)
            else:  # TestCase
                testCaseList.append(test)
        jobTestCaseList = testCaseList[jobIDX::totalJobs]  # l[start:stop:step]
        jobtestSuite    = unittest.TestSuite(tests=jobTestCaseList)

        # Run the tests
        result = unittest.TextTestRunner(verbosity=2).run(jobtestSuite)
        if result.errors:
            raise BeblsoftError(msg="Test failed result={}".format(result.__dict__))
