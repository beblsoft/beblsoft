#!/usr/bin/env python3
"""
 NAME:
  jobQueueAWS.py

 DESCRIPTION
  Job Queue AWS Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.SQS.bQueue import BeblsoftSQSQueue
from base.aws.python.SQS.bMessage import BeblsoftSQSMessage
from smeckn.server.manager.jobQueue import JobQueueManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- JOB QUEUE AWS MANAGER -------------------------- #
class JobQueueAWSManager(JobQueueManager):
    """
    Job Queue AWS Manager class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.bSQSQueue = BeblsoftSQSQueue(
            name            = self.cfg.SQS_JOB_QUEUE_NAME,
            bRegion         = self.cfg.AWS_BREGION)

    # -------------------------- MESSAGES ----------------------------------- #
    @logFunc()
    def queueMessage(self, jobMsg, delayS=0): #pylint: disable=R1711
        """
        Queue message
        """
        BeblsoftSQSMessage.send(
            bSQSQueue = self.bSQSQueue,
            body      = jobMsg.toString(),
            delayS    = delayS)
        return None
