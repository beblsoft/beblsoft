#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 email.py

DESCRIPTION
 Email Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import boto3
from jinja2 import Environment, FileSystemLoader
from email_validator import validate_email, EmailNotValidError
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------ EMAIL MANAGER OBJECT ----------------------------- #
class EmailManager(CommonManager):
    """
    Email Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.fromDomain    = self.cfg.EMAIL_FROM_DOMAIN
        self.configSetName = self.cfg.EMAIL_CONFIG_SET_NAME
        self.sesClient     = boto3.client("ses")

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    @logFunc()
    def renderTemplate(self, templatePath, **templateKwargs):  # pylint: disable=R0201
        """
        Render Template

        Args
          templatePath:
            Full path to HTML jinja template to be rendered
            Ex. "/home/jbensson/git/beblsoft/projects/smeckn/email/html/confirm-account.html"
          templateKwargs:
            Parameters to pass to the jinja template render function

        Note
          WEBCLIENT_APP_URL and WEBCLIENT_EMAIL_URL are automatically added as template
          parameters

        Returns
          HTML Template rendered with Kwargs
        """
        templateDirectory = os.path.dirname(templatePath)
        templateFile      = os.path.basename(templatePath)
        jinjaEnv          = Environment(
            loader              = FileSystemLoader(templateDirectory),
            trim_blocks         = True)
        template          = jinjaEnv.get_template(templateFile)
        templateStr       = template.render(
            WEBCLIENT_APP_URL   = self.cfg.WEBCLIENT_APP_URL,
            WEBCLIENT_EMAIL_URL = self.cfg.WEBCLIENT_EMAIL_URL,
            **templateKwargs)
        return templateStr

    @logFunc()
    def sendEmail(self,  # pylint: disable=W0102
                  sourcePrefix,
                  subject,
                  textBody=None,
                  htmlBody=None,
                  toAddressList=[],
                  ccAddressList=[],
                  bccAddressList=[],
                  replyToAddressList=[],
                  returnPath=None,
                  tagList=[],
                  validate=True):
        """
        Send an Email
        Args
          sourcePrefix:
            Email address prefix
            Ex. "postmaster" will send from "postmaster@<fromDomain>"
          subject:
            Email subject
            Ex. "Hello World!"
          textBody:
            String of text
            Ex. "Hello Jack,\n ... Sincerley,\nJames"
          htmlBody:
            Strong of html text
            Ex. "<html><body>...</body></html>"
          toAddressList:
          ccAddressList
          bccAddressList:
            Email receiptient lists
          replyToAddressList:
            The reply to email addresses for the message
          returnPath:
            Email address that bounces and complaints will be forwarded
          validate:
            If True, validate addresses before sending
        """
        if validate:
            self.validateAddressList(toAddressList)
            self.validateAddressList(ccAddressList)
            self.validateAddressList(bccAddressList)

        kwargs = {}
        kwargs["Source"] = "{}@{}".format(sourcePrefix,
                                          self.fromDomain)
        # Message
        assert(textBody is None or htmlBody is None)
        assert(textBody is not None or htmlBody is not None)
        messageDict = {}
        messageDict["Subject"] = {}
        messageDict["Subject"]["Data"] = subject
        messageDict["Subject"]["Charset"] = "UTF-8"
        messageDict["Body"] = {}
        if textBody:
            messageDict["Body"]["Text"] = {}
            messageDict["Body"]["Text"]["Data"] = textBody
            messageDict["Body"]["Text"]["Charset"] = "UTF-8"
        if htmlBody:
            messageDict["Body"]["Html"] = {}
            messageDict["Body"]["Html"]["Data"] = htmlBody
            messageDict["Body"]["Html"]["Charset"] = "UTF-8"
        kwargs["Message"] = messageDict

        # Destination
        destinationDict = {}
        destinationDict["ToAddresses"] = toAddressList
        destinationDict["CcAddresses"] = ccAddressList
        destinationDict["BccAddresses"] = bccAddressList
        kwargs["Destination"] = destinationDict

        kwargs["ReplyToAddresses"] = replyToAddressList
        if returnPath:
            kwargs["ReturnPath"] = returnPath
        kwargs["Tags"] = tagList
        if self.configSetName:
            kwargs["ConfigurationSetName"] = self.configSetName

        self.sesClient.send_email(**kwargs)
        logger.debug("{} Sent Email From {}".format(self, kwargs["Source"]))

    @logFunc()
    def validateAddress(self, address):  # pylint: disable=R0201
        """
        Validate an email address
        Args
          address:
            Email Address
            Ex. "beblsofttest@gmail.com"
        """
        try:
            validate_email(address)
        except EmailNotValidError as e:
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_EMAIL,
                                originalError=e)

    @logFunc()
    def validateAddressList(self, addressList):  # pylint: disable=R0201
        """
        Validate each email in address list
        """
        for address in addressList:
            self.validateAddress(address)
