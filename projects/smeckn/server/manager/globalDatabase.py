#!/usr/bin/env python3
"""
 NAME:
  globalDatabase.py

 DESCRIPTION
  Global Database Manager Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.manager.common import CommonManager


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- GLOBAL DATABASE MANAGER CLASS ---------------- #
class GlobalDatabaseManager(CommonManager):
    """
    Global Database Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.dbp  = self.mgr.dbp
        self.gDBS = self.dbp.gDBS
        self.gDB  = self.dbp.gDB

    # --------------------- HELPERS ----------------------------------------- #
    @logFunc()
    def addLocalADBS(self):
        """
        Add local database server (same as global database server)
        """
        aDBS = self.gDBS
        self.dbp.accountDBServerP.populateList(
            readDomainList  = [aDBS.domainName],
            writeDomainList = [aDBS.domainName],
            userList        = [aDBS.user],
            passwordList    = [aDBS.password])

    # --------------------- CRUD -------------------------------------------- #
    @logFunc()
    def create(self, addLocalADBS=False, dbpAdmin=False, dbpAccounts=False):  # pylint: disable=W0221
        """
        Create
        """
        # Global Database
        self.gDB.create()
        self.gDB.createTables()

        # Local Only! ADB is on local server
        if addLocalADBS:
            self.addLocalADBS()

        # Create population
        self.mgr.dbp.create(admin=dbpAdmin, accounts=dbpAccounts)

    @logFunc()
    def delete(self, deleteAccounts=True):
        """
        Delete
        """
        if self.gDB.exists:
            if deleteAccounts:
                self.dbp.accountP.depopulateAll()
            self.gDB.delete()

    @logFunc()
    def reset(self, **kwargs):  # pylint: disable=W0221
        """
        Reset
        """
        self.delete()
        self.create(**kwargs)

    @logFunc()
    def describe(self):
        """
        Describe
        """
        rval = None
        if self.gDB.exists:
            rval = self.gDB.describe()
        return rval

    # --------------------- WAIT ONLINE ------------------------------------- #
    @logFunc()
    def waitServerOnline(self):
        """
        Wait for server to come online
        """
        self.gDB.bServer.waitOnline()

    # --------------------- VERSIONS ---------------------------------------- #
    @logFunc()
    def upgrade(self, toVersion):
        """
        Upgrade
        """
        self.gDB.bDBMigrations.upgrade(toVersion=toVersion)

    @logFunc()
    def downgrade(self, toVersion):
        """
        Downgrade
        """
        self.gDB.bDBMigrations.downgrade(toVersion=toVersion)

    # --------------------- RUN SQL ----------------------------------------- #
    @logFunc()
    def runSQL(self, sqlStatement=""):
        """
        Run SQL
        """
        return self.gDB.runSQL(sqlStatement=sqlStatement)
