#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 jwt.py

DESCRIPTION
 JWT Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.server.manager.common import CommonManager
from smeckn.server.webToken.auth import AuthJWT
from smeckn.server.webToken.account import AccountJWT
from smeckn.server.webToken.createAccount import CreateAccountJWT
from smeckn.server.webToken.forgotPassword import ForgotPasswordJWT
from smeckn.server.webToken.cart import CartJWT


# ------------------------ SINGLE MANAGER OBJECT ---------------------------- #
class JWTManager(CommonManager):
    """
    JWT Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.auth                     = AuthJWT(
            mgr                              = self.mgr,
            secret                           = self.cfg.AUTH_JWT_SECRET,
            algorithm                        = self.cfg.AUTH_JWT_ALGORITHM,
            defaultExpDelta                  = self.cfg.AUTH_JWT_EXPDELTA,
            defaultNBFDelta                  = self.cfg.AUTH_JWT_NBFDELTA,
            defaultLeeway                    = self.cfg.AUTH_JWT_LEEWAY)
        self.account                  = AccountJWT(
            mgr                              = self.mgr,
            secret                           = self.cfg.ACCOUNT_JWT_SECRET,
            algorithm                        = self.cfg.ACCOUNT_JWT_ALGORITHM,
            defaultExpDelta                  = self.cfg.ACCOUNT_JWT_EXPDELTA,
            defaultNBFDelta                  = self.cfg.ACCOUNT_JWT_NBFDELTA,
            defaultLeeway                    = self.cfg.ACCOUNT_JWT_LEEWAY)
        self.createAccount           = CreateAccountJWT(
            mgr                              = self.mgr,
            secret                           = self.cfg.CA_JWT_SECRET,
            algorithm                        = self.cfg.CA_JWT_ALGORITHM,
            defaultExpDelta                  = self.cfg.CA_JWT_EXPDELTA,
            defaultNBFDelta                  = self.cfg.CA_JWT_NBFDELTA,
            defaultLeeway                    = self.cfg.CA_JWT_LEEWAY)
        self.forgotPassword          = ForgotPasswordJWT(
            mgr                              = self.mgr,
            secret                           = self.cfg.FP_JWT_SECRET,
            algorithm                        = self.cfg.FP_JWT_ALGORITHM,
            defaultExpDelta                  = self.cfg.FP_JWT_EXPDELTA,
            defaultNBFDelta                  = self.cfg.FP_JWT_NBFDELTA,
            defaultLeeway                    = self.cfg.FP_JWT_LEEWAY)
        self.cart                    = CartJWT(
            mgr                              = self.mgr,
            secret                           = self.cfg.CART_JWT_SECRET,
            algorithm                        = self.cfg.CART_JWT_ALGORITHM,
            defaultExpDelta                  = self.cfg.CART_JWT_EXPDELTA,
            defaultNBFDelta                  = self.cfg.CART_JWT_NBFDELTA,
            defaultLeeway                    = self.cfg.CART_JWT_LEEWAY)
