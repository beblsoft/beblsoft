#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 common.py

DESCRIPTION
 Common Manager
"""


# ------------------------ COMMON MANAGER OBJECT ---------------------------- #
class CommonManager():
    """
    Common Manager
    """

    def __init__(self, mgr):  # pylint: disable=R0915
        """
        Initialize Object
        """
        self.mgr  = mgr
        self.cfg  = mgr.cfg

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)
