#!/usr/bin/env python3
"""
NAME:
 sentry.py

DESCRIPTION
 Sentry Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import sentry_sdk #pylint: disable=E0401
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration #pylint: disable=E0401
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration #pylint: disable=E0401,E0611
from sentry_sdk.integrations.logging import LoggingIntegration #pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from smeckn.server.manager.common import CommonManager
from smeckn.server.eCtx import ExecutionContext
from smeckn.server.log.sLogger import SmecknLogger


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SENTRY MANAGER OBJECT ---------------------------- #
class SentryManager(CommonManager):
    """
    Sentry Manager
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        sentry_sdk.init(
            dsn               = self.cfg.SENTRY_SERVER_DSN,
            with_locals       = self.cfg.SENTRY_WITH_LOCALS,
            max_breadcrumbs   = self.cfg.SENTRY_MAX_BREADCRUMBS,
            release           = self.cfg.GIT_COMMIT_SHA,
            environment       = self.cfg.ENVIRONMENT,
            shutdown_timeout  = self.cfg.SENTRY_SHUTDOWN_TIMEOUT,
            integrations      = [AwsLambdaIntegration(),
                                 SqlalchemyIntegration(),
                                 LoggingIntegration(level=logging.INFO, event_level=logging.CRITICAL)],
            request_bodies    = self.cfg.SENTRY_REQUEST_BODIES,
            before_send       = self.beforeSend,
            before_breadcrumb = self.beforeBreadcrumb,
            debug             = self.cfg.SENTRY_DEBUG)

    # -------------------- ERROR MARKING ------------------------------------ #
    # Not all errors generated by the Sentry SDK should be sent.
    # Therefore, the captureError() API marks the errors before they are sent so
    # that the errors are not filtered out by beforeSend()
    _ERROR_SEND_ATTRIBUTE = "_sendToSentry"

    @staticmethod
    def markErrorToSend(error):
        """
        Mark Error to send to sentry
        """
        setattr(error, SentryManager._ERROR_SEND_ATTRIBUTE, True)

    @staticmethod
    def shouldSendError(error):
        """
        Return True if Error should be sent to Sentry
        """
        rval = getattr(error, SentryManager._ERROR_SEND_ATTRIBUTE, False)
        return rval

    # -------------------- BEFORE HOOKS ------------------------------------- #
    def beforeSend(self, event, hint):  # pylint: disable=W0613,R0201
        """
        Callback before sending events
        Filter events here

        Reference:
          https://docs.sentry.io/error-reporting/configuration/filtering/?platform=python
        """
        send = False

        # Only send exceptions, not log messages
        if "exc_info" in hint:
            errorType, error, tb = hint["exc_info"]  # pylint: disable=W0612
            if isinstance(error, BeblsoftError):
                send = SentryManager.shouldSendError(error=error)
            else:
                send = True
        return event if send else None

    def beforeBreadcrumb(self, crumb, hint):  # pylint: disable=W0613,R0201
        """
        Callback before sending breadcrumbs
        Filter breadcrumbs here
        """
        return crumb

    # -------------------- CAPTURE ------------------------------------------ #
    @logFunc()
    def captureMessage(self, message, bLogCode, dumpState=True):
        """
        Capture Message and optionally Dump State to logs
        Args
          message:
            Message to send to Sentry and to Log
            Ex. "Deadletter"
          bLogCode:
            Beblsoft Log Code
            Ex. BeblsoftLogCode.AJOB_SETUP
          dumpState:
            If True, dump state to logs
        """
        # Send Message
        with sentry_sdk.push_scope() as scope:
            self.setScope(scope=scope, subject=bLogCode.name)
            sentry_sdk.capture_message(message)

        # Dump Logs
        if dumpState:
            SmecknLogger.dumpState(msg=message, bLogCode=bLogCode)

    @logFunc()
    def captureError(self, error, bLogCode, logMsg="", dumpState=True, isBError=False):
        """
        Capture Error and optionally Dump State to Logs
        Args
          error
            Error to report to Sentry
            Ex. BeblsoftError()
          bLogCode:
            Beblsoft Log Code
            Ex. BeblsoftLogCode.AJOB_SETUP
          logMsg:
            Message to log with state dump
            Ex. "Sleep Job Failed"
          dumpState:
            If True, dump state to logs
          isBError:
            If True, error is a Beblsoft Error

        Note:
          dumpState() call is after capture_exception() because previously dumpState()
          logging at error was triggering Sentry to send the exception without
          the added setScope() context.
        """
        # Mark and Send Error
        SentryManager.markErrorToSend(error=error)
        with sentry_sdk.push_scope() as scope:
            self.setScope(scope=scope, subject=bLogCode.name)
            sentry_sdk.capture_exception(error)

        # Dump Logs
        if dumpState:
            SmecknLogger.dumpState(msg=logMsg, bLogCode=bLogCode, isException=True,
                                   bError=error if isBError else None)

    # -------------------- SCOPE -------------------------------------------- #
    @logFunc()
    def setScope(self,  # pylint: disable=R0201
                 scope,
                 subject      = None,
                 level        = "error",
                 language     = "Python3.6",
                 fingerprint  = None):
        """
        Set Sentry Scope
        Args:
          scope:
            Sentry Scope Object to modify
          subject:
            Scope Subject, describes area of code
            Ex. "Flask", "Job", "LambdaSwitchError"
          level:
            Level of scope
            One of: "fatal", "error", "warning", "info", or "debug"
          language:
            Language running
            Ex. "Python3.6"
          fingerprint:
            Fingerprint to customize sentry groupings
            Ex. ["{{ default }}"]

        Reference:
          https://docs.sentry.io/enriching-error-data/context/?platform=python
          https://docs.sentry.io/data-management/rollups/?platform=python#custom-grouping
        """
        # Generic -------------------------------
        scope.set_tag("Subject", subject)
        scope.set_tag("Language", language)
        scope.level = level
        if fingerprint == None:
            scope.fingerprint = ["{{ default }}"]

        # Execution Context ---------------------
        eCtx = ExecutionContext.get()
        if eCtx:
            scope.set_extra("eCtx", eCtx.jsonDict)

        # Flask Request -------------------------
        from flask import request
        aIPAddress = None
        try:
            environ = request.environ
            scope.set_tag("Path", environ.get("PATH_INFO"))
            scope.set_tag("Method", environ.get("REQUEST_METHOD"))
            scope.set_tag("URL", request.url)
            scope.set_extra("flaskRequest", request.__dict__)
            if not aIPAddress:
                aIPAddress = environ.get("REMOTE_ADDR", None)
        except RuntimeError as _:  # Not in application context
            pass

        # User ----------------------------------
        from smeckn.server.application.glob import ApplicationGlobal
        userDict                   = {}
        aID                        = ApplicationGlobal.getAccountID()
        if aID is not None:
            userDict["id"]         = aID
        if aIPAddress is not None:
            userDict["ip_address"] = aIPAddress
        if userDict.keys():
            scope.user             = userDict
