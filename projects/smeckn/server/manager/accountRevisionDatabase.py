#!/usr/bin/env python3
"""
 NAME:
  accountRevisionDatabase.py

 DESCRIPTION
  Account Revision Database Manager Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bServer import BeblsoftMySQLServer
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB import AccountDatabase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- ACCOUNT REVISION DATABASE MANAGER CLASS ------ #
class AccountRevisionDatabaseManager(CommonManager):
    """
    Account Revision Database Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.aRDBS         = BeblsoftMySQLServer(
            domainNameFunc     = lambda: self.cfg.GRDB_DOMAIN,
            user               = self.cfg.ARDB_USER,
            password           = self.cfg.ARDB_PASSWORD)
        self.aRDB          = AccountDatabase(
            bServer            = self.aRDBS,
            name               = self.cfg.ARDB_NAME,
            echo               = self.cfg.ARDB_ECHO)

    # --------------------- CRUD -------------------------------------------- #
    @logFunc()
    def create(self):  # pylint: disable=W0221
        """
        Create
        """
        self.aRDB.create()

    @logFunc()
    def delete(self):
        """
        Delete
        """
        self.aRDB.delete()

    @logFunc()
    def describe(self):
        """
        Describe
        """
        if self.aRDB.exists:
            self.aRDB.describe()

    # --------------------- MIGRATION --------------------------------------- #
    @logFunc()
    def revise(self, msg):
        """
        Revise
        """
        self.aRDB.delete()
        self.aRDB.create()
        self.aRDB.createTables(fromMigrations=True)
        self.aRDB.bDBMigrations.revise(msg=msg)
        self.aRDB.delete()

    @logFunc()
    def upgrade(self, toVersion):
        """
        Upgrade
        """
        self.aRDB.bDBMigrations.upgrade(toVersion=toVersion)

    @logFunc()
    def downgrade(self, toVersion):
        """
        Downgrade
        """
        self.aRDB.bDBMigrations.downgrade(toVersion=toVersion)
