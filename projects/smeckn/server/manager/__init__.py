#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 Server Manager Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.server.manager.sentry import SentryManager
from smeckn.server.dbp.manager import DatabasePopulationManager
from smeckn.server.manager.globalDatabase import GlobalDatabaseManager
from smeckn.server.manager.globalRevisionDatabase import GlobalRevisionDatabaseManager
from smeckn.server.manager.accountDatabase import AccountDatabaseManager
from smeckn.server.manager.accountRevisionDatabase import AccountRevisionDatabaseManager
from smeckn.server.manager.jwt import JWTManager
from smeckn.server.analysis.manager import AnalysisManager
from smeckn.server.payments.manager import PaymentsManager
from smeckn.server.profileStub.manager import ProfileStubManager
from smeckn.server.facebook.manager import FacebookProfileManager
from smeckn.server.profile.manager import ProfileManager
from smeckn.server.manager.email import EmailManager
from smeckn.server.manager.reCAPTCHA import ReCAPTCHAManager
from smeckn.server.manager.jobQueue import JobQueueManager
from smeckn.server.application.manager import ApplicationManager
from smeckn.server.manager.test import TestManager


# ------------------------ SERVER MANAGER OBJECT ---------------------------- #
class ServerManager():
    """
    Server Manager Class
    """

    def __init__(self, cfg):  # pylint: disable=R0915
        """
        Initialize Object
        """
        self.cfg         = cfg
        self.sentry      = SentryManager(mgr=self)
        self.dbp         = DatabasePopulationManager(mgr=self)
        self.gdb         = GlobalDatabaseManager(mgr=self)
        self.grdb        = GlobalRevisionDatabaseManager(mgr=self)
        self.adb         = AccountDatabaseManager(mgr=self)
        self.ardb        = AccountRevisionDatabaseManager(mgr=self)
        self.jwt         = JWTManager(mgr=self)
        self.analysis    = AnalysisManager(mgr=self)
        self.payments    = PaymentsManager(mgr=self)
        self.profileStub = ProfileStubManager(mgr=self)
        self.facebook    = FacebookProfileManager(mgr=self)
        self.profile     = ProfileManager(mgr=self)
        self.email       = EmailManager(mgr=self)
        self.recaptcha   = ReCAPTCHAManager(mgr=self)
        self.jqueue      = JobQueueManager.fromConfig(mgr=self, cfg=cfg)
        self.app         = ApplicationManager(mgr=self)
        self.test        = TestManager(mgr=self)
