#!/usr/bin/env python3
"""
 NAME:
  jobQueue.py

 DESCRIPTION
  Job Queue Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import abc
import logging
from smeckn.server.manager.common import CommonManager
from smeckn.server.config.aws import AWSConfig
from smeckn.server.config.local import LocalConfig
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- JOB QUEUE MANAGER ------------------------------ #
class JobQueueManager(CommonManager, abc.ABC):
    """
    Job Queue Manager class
    """

    @staticmethod
    def fromConfig(mgr, cfg):
        """
        Alternate constructor

        Return correct JobQueueManager Class based on configuration
        """
        if isinstance(cfg, AWSConfig):
            from smeckn.server.manager.jobQueueAWS import JobQueueAWSManager
            return JobQueueAWSManager(mgr=mgr)
        elif isinstance(cfg, LocalConfig):
            from smeckn.server.manager.jobQueueLocal import JobQueueLocalManager
            return JobQueueLocalManager(mgr=mgr)
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_UNKNOWN_CONFIG,
                                msg="Config={}".format(cfg))

    # -------------------------- QUEUE MESSAGE ------------------------------ #
    @abc.abstractmethod
    def queueMessage(self, jobMsg, delayS=0):
        """
        Queue message
        """
        pass
