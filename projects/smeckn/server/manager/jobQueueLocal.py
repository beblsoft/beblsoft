#!/usr/bin/env python3
"""
 NAME:
  jobQueueLocal.py

 DESCRIPTION
  Job Queue Local Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import time
import logging
import threading
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.thread.bThread import joinThreadsByName
from smeckn.server.manager.jobQueue import JobQueueManager
from smeckn.server.job import Job
from smeckn.server.job.message import JobMessage
from smeckn.server.eCtx.local import LocalExecutionContext


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- JOB QUEUE LOCAL MANAGER ------------------------ #
class JobQueueLocalManager(JobQueueManager):
    """
    Job Queue Local Manager class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.threadIdx        = 0
        self.lock             = threading.Lock()
        self.threadNamePrefix = "jqThread"

    # -------------------------- HELPERS ------------------------------------ #
    def getIncThreadIdx(self):
        """
        Get and increment thread idx
        """
        self.lock.acquire()
        rval = self.threadIdx
        self.threadIdx += 1
        self.lock.release()
        return rval

    # -------------------------- QUEUE -------------------------------------- #
    @logFunc()
    def queueMessage(self, jobMsg, delayS=0):
        """
        Queue message

        Returns
          Daemon Thread spawned to handle the work
        """
        msg = {
            "body": jobMsg.toString(),
            "delayS": delayS,
            "sentTimeMS": int(round(time.time() * 1000))
        }
        thread = threading.Thread(name="{}{}".format(self.threadNamePrefix, self.getIncThreadIdx()),
                                  target=self.workerThreadMain, args=(msg, ))
        thread.setDaemon(True)
        thread.start()
        return thread

    # -------------------------- WORKER THREAD MAIN ------------------------- #
    @logFunc()
    def workerThreadMain(self, msg):  # pylint: disable=R0201
        """
        Worker Thread Main
        """
        time.sleep(msg["delayS"])
        with LocalExecutionContext():
            jobMsg = JobMessage.fromString(jobMsgStr=msg.get("body"), sentTimeMS=msg.get("sentTimeMS"))
            return Job.executeFromMessage(mgr=self.mgr, jobMsg=jobMsg)

    # -------------------------- JOIN WORKERS ------------------------------- #
    @logFunc()
    def joinWorkers(self):
        """
        Join all workers in progress
        """
        joinThreadsByName(namePrefix=self.threadNamePrefix)
