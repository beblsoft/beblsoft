#!/usr/bin/env python3
"""
NAME:
 create.py

DESCRIPTION
 Create Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- CREATE -------------------------------------------- #
@click.command()
@click.option('--gdb', is_flag=True, default=False,
              help="Create global database")
@click.option('--dbpadmin', is_flag=True, default=False,
              help="Create database population admin")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Create database population accounts")
@click.pass_context
def create(ctx,
           gdb,
           dbpadmin, dbpaccounts):
    """
    Create Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if gdb:
        mgr.gdb.create(addLocalADBS=True)

    mgr.dbp.create(
        admin    = dbpadmin,
        accounts = dbpaccounts)
