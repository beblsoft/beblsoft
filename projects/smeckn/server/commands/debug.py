#!/usr/bin/env python3
"""
NAME:
 debug.py

DESCRIPTION
 Debug Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- DEBUG --------------------------------------------- #
@click.command()
@click.option('--example', is_flag=True, default=False,
              help="Debug example")
@click.pass_context
def debug(ctx,
          example):
    """
    Debug Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if example:
        print("Hello World {}!".format(mgr))
