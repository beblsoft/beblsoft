#!/usr/bin/env python3
"""
NAME:
 delete.py

DESCRIPTION
 Delete Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- START --------------------------------------------- #
@click.command()
@click.option('--gdb', is_flag=True, default=False,
              help="Delete global database")
@click.option('--dbpadmin', is_flag=True, default=False,
              help="Delete database population admin")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Delete database population accounts")
@click.option('--dbpallaccounts', is_flag=True, default=False,
              help="Delete database population all accounts")
@click.pass_context
def delete(ctx,
           gdb,
           dbpadmin, dbpaccounts, dbpallaccounts):
    """
    Delete Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    mgr.dbp.delete(
        admin       = dbpadmin,
        accounts    = dbpaccounts,
        allAccounts = dbpallaccounts)

    if gdb:
        mgr.gdb.delete()
