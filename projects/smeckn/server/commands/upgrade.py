#!/usr/bin/env python3
"""
NAME:
 upgrade.py

DESCRIPTION
 Upgrade Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click
from click import UsageError


# ----------------------- UPGRADE ------------------------------------------- #
@click.command()
@click.option('--gdb', is_flag=True, default=False,
              help="Upgrade global database")
@click.option('--gdbversion', default=None,
              help="Global database version")
@click.option('--adb', is_flag=True, default=False,
              help="Upgrade all account databases")
@click.option('--adbversion', default=None,
              help="Account database version")
@click.pass_context
def upgrade(ctx,
            gdb, gdbversion,
            adb, adbversion):
    """
    Upgrade Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if gdb:
        if not gdbversion:
            raise UsageError("Please specify gdbversion")
        mgr.gdb.upgrade(toVersion=gdbversion)

    if adb:
        if not adbversion:
            raise UsageError("Please specify adbversion")
        mgr.adb.upgradeAll(toVersion=adbversion)
