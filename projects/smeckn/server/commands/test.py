#!/usr/bin/env python3
"""
NAME:
 test.py

DESCRIPTION
 Test Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click
from smeckn.server.manager.test import TestModule
from smeckn.server.log.testFilter import TestConsoleLogFilter


# ----------------------- TEST ---------------------------------------------- #
@click.command()
@click.option("--module", default=None,
              type=click.Choice(
                  [name for name, member in TestModule.__members__.items()]),
              help="Run Specific Test Module")
@click.option("--name", default=None, help="Run Specific Test Name\nEx:smeckn.server.bar.FooTestCase")
@click.option("--vcrrecordmode", default="once",
              type=click.Choice(["once", "new_episodes", "none", "all"]),
              help="VCR Record Mode")
@click.option("--totaljobs", default=1, help="Total number of jobs running in parallel")
@click.option("--jobidx", default=0, help="Job index of total jobs running in parallel [0:totaljobs-1]")
@click.pass_context
def test(ctx, module, name, vcrrecordmode, totaljobs, jobidx):
    """
    Test Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    gc.bLog.addConsoleFilter(TestConsoleLogFilter())
    mgr.test.test(module=module, name=name, vcrRecordMode=vcrrecordmode,
                  jobIDX=jobidx, totalJobs=totaljobs)
