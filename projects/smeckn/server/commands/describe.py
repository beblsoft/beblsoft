#!/usr/bin/env python3
"""
NAME:
 describe.py

DESCRIPTION
 Describe Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- DESCRIBE ------------------------------------------ #
@click.command()
@click.option('--gdb', is_flag=True, default=False,
              help="Describe Global database")
@click.option('--dbpadmin', is_flag=True, default=False,
              help="Describe database population admin")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Describe database population accounts")
@click.option('--dbpallaccounts', is_flag=True, default=False,
              help="Describe database population all accounts")
@click.option('--app', is_flag=True, default=False,
              help="Describe application")
@click.pass_context
def describe(ctx,
             gdb,
             app,
             dbpadmin, dbpaccounts, dbpallaccounts):
    """
    Describe Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if gdb:
        mgr.gdb.describe()

    mgr.dbp.describe(
        admin       = dbpadmin,
        accounts    = dbpaccounts,
        allAccounts = dbpallaccounts)

    if app:
        mgr.app.describe()
