#!/usr/bin/env python3
"""
NAME:
 start.py

DESCRIPTION
 Start Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import code
import click
from smeckn.server.log.startAppFilter import StartAppLogFilter


# ----------------------- START --------------------------------------------- #
@click.command()
@click.option('--shell', is_flag=True, default=False,
              help="Start shell with python variables")
@click.option('--app', is_flag=True, default=False,
              help="Start application")
@click.pass_context
def start(ctx, shell, app):
    """
    Start Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if shell:
        shellCtx = {"gc": gc}
        code.interact(banner="", local=shellCtx)

    if app:
        gc.bLog.addConsoleFilter(StartAppLogFilter())
        mgr.app.start(useReloader=True)
