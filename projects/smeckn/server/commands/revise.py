#!/usr/bin/env python3
"""
NAME:
 revise.py

DESCRIPTION
 Revise Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click
from click import UsageError



# ----------------------- REVISE -------------------------------------------- #
@click.command()
@click.option('--grdb', is_flag=True, default=False,
              help="Revise global revision database")
@click.option('--grdbmessage', default=None,
              help="Global revision database revision message")
@click.option('--ardb', is_flag=True, default=False,
              help="Revise account revision database")
@click.option('--ardbmessage', default=None,
              help="Account revision database revision message")
@click.pass_context
def revise(ctx,
           grdb, grdbmessage,
           ardb, ardbmessage):
    """
    Revise Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.server.mgr

    if grdb:
        if not grdbmessage:
            raise UsageError("Please specify grdbmessage")
        mgr.grdb.revise(msg=grdbmessage)

    if ardb:
        if not ardbmessage:
            raise UsageError("Please specify ardbmessage")
        mgr.ardb.revise(msg=ardbmessage)
