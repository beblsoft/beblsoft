#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Link Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.link.dao import LinkDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- LINK POPULATER CLASS ------------------------------ #
class LinkPopulater(CommonPopulater):
    """
    Link Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, count=5):  # pylint: disable=W0102
        """
        Populate N

        Returns
          Link Model List
        """
        linkModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(count):
                url = "https://www.example.com/{}".format(forgery_py.basic.text())
                LinkDAO.create(aDBS=aDBS, fromUser=True, linkURL=url)
            aDBS.commit()
            linkModelList = LinkDAO.getAll(aDBS=aDBS)
        return linkModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            LinkDAO.deleteAll(aDBS=aDBS)
