#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Link Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.link.model import LinkModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- LINK DAO CLASS ------------------------------------ #
class LinkDAO():
    """
    Link Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        linkModel = LinkModel()
        for k, v in kwargs.items():
            setattr(linkModel, k, v)
        aDBS.add(linkModel)
        return linkModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(aDBS, linkModel):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.delete(linkModel)

    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(LinkModel).filter(LinkModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(LinkModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, linkModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(linkModel, k, v)
        aDBS.add(linkModel)
        return linkModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        linkModel = aDBS.query(LinkModel)\
            .filter(LinkModel.smID == smID)\
            .one_or_none()
        if not linkModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return linkModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, linkURL=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(LinkModel)

        #Filters
        if linkURL:
            q = q.filter(LinkModel.linkURL == linkURL)

        return q.count() if count else q.all()
