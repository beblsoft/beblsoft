#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Link Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.link.dao import LinkDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class LinkModelTestCase(CommonTestCase):
    """
    Test Link Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, links=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            url            = "www.example.com/helloWorld.mp4"
            LinkDAO.create(aDBS=aDBS, fromUser=True, linkURL=url)
            aDBS.commit()

            # Get
            linkModelList = LinkDAO.getAll(aDBS, linkURL=url)
            self.assertEqual(len(linkModelList), 1)
            linkModel = linkModelList[0]
            self.assertEqual(linkModel.linkURL, url)

            # Delete
            LinkDAO.deleteBySMID(aDBS=aDBS, smID=linkModel.smID)
            aDBS.commit()

            # Verify Delete
            linkModelList = LinkDAO.getAll(aDBS, linkURL=url)
            self.assertEqual(len(linkModelList), 0)
