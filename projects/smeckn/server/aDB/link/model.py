#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Link Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, Text, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger               = logging.getLogger(__name__)
MAX_LINK_URL_LENGTH  = 4096
MAX_LINK_NAME        = 4096
MAX_LINK_CAPTION     = 4096
MAX_LINK_DESCRIPTION = 4096


# ----------------------- LINK MODEL CLASS ---------------------------------- #
class LinkModel(base):
    """
    Link Table
    """
    __tablename__       = "Link"
    smID                = Column(BigInteger, primary_key=True)   # ID
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    fromUser            = Column(Boolean, default=False)         # If True, link is directly from
                                                                 # user and did not originate from a profile
    imageHeightPX       = Column(Integer)                        # Ex. 500
    imageWidthPX        = Column(Integer)                        # Ex. 700
    imageURL            = Column(Text)                           # Scraped image from link
    linkURL             = Column(Text)                           # Link URL

    name                = Column(Text)
    caption             = Column(Text)
    description         = Column(Text)


    # Parent Facebook Post
    # 1 Facebook Post : 1 Link
    #
    # Field notes
    #  imageURL    - https://scontent.xx.fbcdn.net/v/t15.5256-10/50055905_102701040846939_7867706927457763328_n.jpg?_nc_cat=107&_nc_ht=scontent.xx&oh=4dfcf345f22f67180bfbaaa1b0c43816&oe=5CF7CD37
    #  linkURL     - https://www.disney.com/
    #  name        - "Disney.com | The official home for all things Disney"
    #  caption     - "disney.com"
    #  description - "The official website for all things Disney..."
    #
    fbPostSMID          = Column(BigInteger,
                                 ForeignKey("FacebookPost.smID", onupdate="CASCADE", ondelete="CASCADE"))
    fbPostModel         = relationship("FacebookPostModel", back_populates="linkModel")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
