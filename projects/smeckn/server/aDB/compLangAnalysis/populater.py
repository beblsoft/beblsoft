#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Comprehend Language Analysis Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import random
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND LANGUAGE ANALYSIS POPULATER CLASS ------ #
class CompLangAnalysisPopulater(CommonPopulater):
    """
    Comprehend Language Analysis Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, aID, textSMIDList=[]):  # pylint: disable=W0102
        """
        Populate List

        Returns
          CompLangAnalysisModel List
        """
        compLangAnalysisModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for textSMID in textSMIDList:
                CompLangAnalysisDAO.create(
                    aDBS          = aDBS,
                    textSMID      = textSMID,
                    status        = random.choice(list(CompLangAnalysisStatus)))
            aDBS.commit()
            compLangAnalysisModelList = CompLangAnalysisDAO.getAll(aDBS=aDBS)
        return compLangAnalysisModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            CompLangAnalysisDAO.deleteAll(aDBS=aDBS)
