#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Comprehend Language Analysis Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND LANGUAGE ANALYSIS DAO CLASS ------------ #
class CompLangAnalysisDAO():
    """
    Comprehend Language Analysis Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        compLangAnalysisModel = CompLangAnalysisModel()
        if "createDate" not in kwargs:
            kwargs["createDate"] = datetime.utcnow()
        for k, v in kwargs.items():
            setattr(compLangAnalysisModel, k, v)
        aDBS.add(compLangAnalysisModel)
        return compLangAnalysisModel

    @staticmethod
    @logFunc()
    def createFromBCompDetectLanguageOutput(aDBS, bCompDetectLanguageOutput, **kwargs):
        """
        Create from comprehend detect langauage output
        Args
          aDBS:
            Acount Database Session
          bCompDetectLanguageOutput:
            Output from BeblsoftComprehendClient.detectDominantLanguage
        """
        from smeckn.server.aDB.compLang.dao import CompLangDAO

        compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, **kwargs)
        for compLangScore in bCompDetectLanguageOutput:
            bLanguage     = compLangScore.get("bLanguage")
            score         = compLangScore.get("Score")
            CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                               bLanguageType=bLanguage.bLanguageType, score=score)
        aDBS.add(compLangAnalysisModel)
        return compLangAnalysisModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(CompLangAnalysisModel).filter(CompLangAnalysisModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(CompLangAnalysisModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, compLangAnalysisModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(compLangAnalysisModel, k, v)
        aDBS.add(compLangAnalysisModel)
        return compLangAnalysisModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        compLangAnalysisModel = aDBS.query(CompLangAnalysisModel)\
            .filter(CompLangAnalysisModel.smID == smID)\
            .one_or_none()
        if not compLangAnalysisModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return compLangAnalysisModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, smTextID=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(CompLangAnalysisModel)

        # Filters
        if smTextID:
            q = q.filter(CompLangAnalysisModel.smTextID == smTextID)

        return q.count() if count else q.all()
