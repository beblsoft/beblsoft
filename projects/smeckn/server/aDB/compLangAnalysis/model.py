#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Comprehend Language Analysis Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Enum, select
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class CompLangAnalysisStatus(enum.Enum):
    """
    Comprehend Language Analysis Status Enumeration
    """
    SUCCESS           = 0
    COMPREHEND_FAILED = 1  # Exception thrown


# ----------------------- COMPREHEND LANGUAGE ANALYSIS MODEL CLASS ---------- #
class CompLangAnalysisModel(base):
    """
    Comprehend Language Analysis Table
    """
    __tablename__       = "CompLangAnalysis"
    smID                = Column(BigInteger, primary_key=True)   # ID
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    status              = Column(Enum(CompLangAnalysisStatus),
                                 default=None)                   # Ex. CompLangAnalysisStatus.SUCCESS

    # Parent Text
    # 1 Text : 1 CompLangAnalysis
    textSMID            = Column(BigInteger,
                                 ForeignKey("Text.smID", onupdate="CASCADE", ondelete="CASCADE"))
    textModel           = relationship("TextModel", back_populates="compLangAnalysisModel")

    # Child CompLangs
    # 1 CompLangAnalysis: N CompLang
    compLangModelList   = relationship("CompLangModel", back_populates="compLangAnalysisModel",
                                       passive_deletes=True)

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)

    @hybrid_property
    def dominantCompLangModel(self):
        """
        Return the dominant (highest score) language comprehend model
        """
        highestScore     = 0
        domCompLangModel = None

        for compLangModel in self.compLangModelList:
            if compLangModel.score > highestScore:
                highestScore     = compLangModel.score
                domCompLangModel = compLangModel
        return domCompLangModel

   # -------------------- DOMINANT BLANGUAGE TYPE --------------------------- #
    @hybrid_property
    def dominantBLanguageType(self):
        """
        Return the dominant (highest score) Beblosft language type
        """
        dominantBLanguageType = None
        if self.dominantCompLangModel: #pylint: disable=W0125
            dominantBLanguageType = self.dominantCompLangModel.bLanguageType  # pylint: disable=E1101
        return dominantBLanguageType

    @dominantBLanguageType.expression
    def dominantBLanguageType(cls):  # pylint: disable=E0213
        """
        Used for filtering dominantBLanguageType in SQL queries
        """
        from smeckn.server.aDB.compLang.model import CompLangModel
        return select([CompLangModel.bLanguageType])\
            .where(CompLangModel.compLangAnalysisSMID == cls.smID)\
            .order_by(CompLangModel.score.desc())\
            .limit(1)\
            .as_scalar()

   # -------------------- DOMINANT BLANGUAGE SCORE -------------------------- #
    @hybrid_property
    def dominantBLanguageScore(self):
        """
        Return the dominant (highest) Beblosft language type score
        """
        dominantBLanguageScore = 0
        if self.dominantCompLangModel: #pylint: disable=W0125
            dominantBLanguageScore = self.dominantCompLangModel.score #pylint: disable=E1101
        return dominantBLanguageScore

    @dominantBLanguageScore.expression
    def dominantBLanguageScore(cls):  # pylint: disable=E0213
        """
        Used for filtering dominantBLanguageScore in SQL queries
        """
        from smeckn.server.aDB.compLang.model import CompLangModel
        return select([CompLangModel.score])\
            .where(CompLangModel.compLangAnalysisSMID == cls.smID)\
            .order_by(CompLangModel.score.desc())\
            .limit(1)\
            .as_scalar()
