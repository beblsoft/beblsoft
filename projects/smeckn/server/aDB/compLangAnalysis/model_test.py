#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Comprehend Language Analysis Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import random
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel, CompLangAnalysisStatus
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.compLang.dao import CompLangDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CompLangAnalysisTestCase(CommonTestCase):
    """
    Test Comprehend Language Analysis Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True, compLangAnalysis=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            compLangAnalysisModel = CompLangAnalysisDAO.create(
                aDBS          = aDBS,
                status        = random.choice(list(CompLangAnalysisStatus)))
            aDBS.commit()

            # Get
            compLangAnalysisModelList = CompLangAnalysisDAO.getAll(aDBS)
            initialCount              = len(compLangAnalysisModelList)

            # Delete
            CompLangAnalysisDAO.deleteBySMID(aDBS=aDBS, smID=compLangAnalysisModel.smID)
            aDBS.commit()

            # Verify Delete
            compLangAnalysisModelList = CompLangAnalysisDAO.getAll(aDBS)
            self.assertEqual(len(compLangAnalysisModelList), initialCount - 1)

    @logFunc()
    def test_textCascadeDelete(self):
        """
        Test text deletions cascade down
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            nCompLangAnalysis = CompLangAnalysisDAO.getAll(aDBS, count=True)
            self.assertGreater(nCompLangAnalysis, 0)

            TextDAO.deleteBySMID(aDBS=aDBS, smID=self.popTextSMID)
            aDBS.commit()

            nCompLangAnalysis = CompLangAnalysisDAO.getAll(aDBS, count=True)
            self.assertEqual(nCompLangAnalysis, 0)

    @logFunc()
    def test_dominantHybrids(self):
        """
        Test hybrid properties: dominantCompLangModel, dominantBLanguageType
        """
        scoreList            = [.33333, .33333, .50000]
        bLanguageTypeList    = [BeblsoftLanguageType.Spanish,
                                BeblsoftLanguageType.English,
                                BeblsoftLanguageType.Chinese]
        dominantBLangType    = None
        maxScore             = 0
        compLangAnalysisSMID = self.popCompLangAnalysisSMID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                for idx, score in enumerate(scoreList):
                    bLanguageType = bLanguageTypeList[idx]
                    CompLangDAO.create(
                        aDBS                 = aDBS,
                        compLangAnalysisSMID = compLangAnalysisSMID,
                        bLanguageType        = bLanguageType,
                        score                = score)
                    if score > maxScore:
                        maxScore          = score
                        dominantBLangType = bLanguageType

            compLangAnalysisModel = CompLangAnalysisDAO.getBySMID(aDBS=aDBS, smID=compLangAnalysisSMID)
            self.assertEqual(compLangAnalysisModel.dominantCompLangModel.bLanguageType, dominantBLangType)
            self.assertEqual(compLangAnalysisModel.dominantBLanguageType, dominantBLangType)
            self.assertEqual(compLangAnalysisModel.dominantBLanguageScore, maxScore)

            q = aDBS.query(CompLangAnalysisModel).filter()
            q = q.filter(CompLangAnalysisModel.dominantBLanguageType == dominantBLangType)
            compLangAnalysisModelList = q.all()
            self.assertEqual(compLangAnalysisModelList[0].dominantBLanguageType, dominantBLangType)
            self.assertEqual(len(compLangAnalysisModelList), 1)
