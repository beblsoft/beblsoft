#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Unit Ledger Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class UnitLedgerModelTestCase(CommonTestCase):
    """
    Test Unit Ledger Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True, analyses=True)

    @logFunc()
    def deleteInitialUnits(self, aDBS):  # pylint: disable=R0201
        """
        Delete initial account units
        """
        UnitLedgerDAO.deleteAll(aDBS=aDBS)
        aDBS.commit()

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        super().setUp(unitLedger=True)

        unitType             = UnitType.TEXT_ANALYSIS
        ledgerDeductionCount = -20
        holdSMID             = self.unitTypeDict[unitType]["unitLedgerHoldSMID"]

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                unitLedgerDeductionModel  = UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType,
                                                                          ledgerCount=ledgerDeductionCount, holdSMID=holdSMID)
            # Get List
            unitLedgerModelList = UnitLedgerDAO.getAll(aDBS, unitType=unitType)
            self.assertEqual(len(unitLedgerModelList), 3)

            # Get Balance
            unitBalanceModel = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
            nTotal           = self.unitTypeDict[unitType]["nInitialTotal"] + ledgerDeductionCount
            nHeld            = self.unitTypeDict[unitType]["nInitialHeld"] + ledgerDeductionCount
            nAvailable       = nTotal - nHeld
            self.assertEqual(unitBalanceModel.nTotal, nTotal)
            self.assertEqual(unitBalanceModel.nHeld, nHeld)
            self.assertEqual(unitBalanceModel.nAvailable, nAvailable)

            # Delete
            UnitLedgerDAO.deleteBySMID(aDBS=aDBS, smID=unitLedgerDeductionModel.smID)
            aDBS.commit()

            # Verify Delete
            unitLedgerModelList = UnitLedgerDAO.getAll(aDBS, unitType=unitType)
            self.assertEqual(len(unitLedgerModelList), 2)

    @logFunc()
    def test_decrementingBalance(self):
        """
        Test slowly decrementing a balance against a hold
        """
        super().setUp(unitLedger=True)

        unitType    = UnitType.TEXT_ANALYSIS
        holdSMID    = self.unitTypeDict[unitType]["unitLedgerHoldSMID"]
        initialHeld = self.unitTypeDict[unitType]["nInitialHeld"]

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for i in range(1, initialHeld + 1):
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType,
                                                  ledgerCount=-1, holdSMID=holdSMID)
                unitBalanceModel = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
                nTotal           = self.unitTypeDict[unitType]["nInitialTotal"] - i
                nHeld            = self.unitTypeDict[unitType]["nInitialHeld"] - i
                nAvailable       = nTotal - nHeld
                self.assertEqual(unitBalanceModel.nTotal, nTotal)
                self.assertEqual(unitBalanceModel.nHeld, nHeld)
                self.assertEqual(unitBalanceModel.nAvailable, nAvailable)

            # Verify Hold Deleted
            self.assertEqual(UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=holdSMID, raiseOnNone=False), None)

    @logFunc()
    def test_negativeAddition(self):
        """
        Test trying to create a negative addition
        """
        unitType        = UnitType.TEXT_ANALYSIS
        ledgerCount     = -5000
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                UnitLedgerDAO.createAddition(aDBS=aDBS, unitType=unitType, ledgerCount=ledgerCount)
                aDBS.commit()
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.LEDGER_ADDITION_MUST_BE_POSITIVE)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a negative ledger addition")

    @logFunc()
    def test_negativeHold(self):
        """
        Test trying to create a negative addition
        """
        unitType        = UnitType.TEXT_ANALYSIS
        ledgerCount     = -5000
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createHold(aDBS=aDBS, unitType=unitType, ledgerCount=ledgerCount)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.LEDGER_HOLD_MUST_BE_POSITIVE)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a negative ledger hold")

    @logFunc()
    def test_holdTooLarge(self):
        """
        Test trying to create a hold that is too large
        """
        unitType        = UnitType.TEXT_ANALYSIS
        ledgerCount     = 5000
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            self.deleteInitialUnits(aDBS=aDBS)
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createHold(aDBS=aDBS, unitType=unitType, ledgerCount=ledgerCount)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.LEDGER_NOT_ENOUGH_UNITS_FOR_HOLD)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a hold when not enough units were available")

    @logFunc()
    def test_positiveDeduction(self):
        """
        Test trying to create a positive deduction
        """
        unitType        = UnitType.TEXT_ANALYSIS
        ledgerCount     = 5000
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType,
                                                  ledgerCount=ledgerCount, holdSMID=None)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.LEDGER_DEDUCTION_MUST_BE_NEGATIVE)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a deduction with a positive ledger count")

    @logFunc()
    def test_deductionWithNoHold(self):
        """
        Test trying to create a deduction with no hold
        """
        unitType        = UnitType.TEXT_ANALYSIS
        ledgerCount     = -5000
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType,
                                                  ledgerCount=ledgerCount, holdSMID=None)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a deduction without a hold")

    @logFunc()
    def test_deductionWithWrongHoldUnitType(self):
        """
        Test trying to create a deduction with a hold of the wrong unit type
        """
        exceptionRaised = False
        ledgerCount     = 100

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:

            try:
                self.deleteInitialUnits(aDBS=aDBS)
                with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                    UnitLedgerDAO.createAddition(aDBS=aDBS, unitType=UnitType.TEXT_ANALYSIS,
                                                 ledgerCount=ledgerCount)
                    aDBS.commit()
                    unitLedgerHoldModel = UnitLedgerDAO.createHold(aDBS=aDBS, unitType=UnitType.TEXT_ANALYSIS,
                                                                   ledgerCount=ledgerCount)
                    aDBS.commit()
                    UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=UnitType.PHOTO_ANALYSIS,
                                                  ledgerCount=-ledgerCount, holdSMID=unitLedgerHoldModel.smID)
                    aDBS.commit()
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.LEDGER_DEDUCTION_INVALID_UNITTYPE)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Created a deduction with a hold of the wrong unit type")

    @logFunc()
    def test_deductionsDeleteHolds(self):
        """
        Test creating deductions that delete their holds because they are greater than or equal to hold
        """
        ledgerCount        = 100
        deductionCountList = [-ledgerCount, -ledgerCount - 1]
        unitType           = UnitType.TEXT_ANALYSIS

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            self.deleteInitialUnits(aDBS=aDBS)
            with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                for deductionCount in deductionCountList:
                    UnitLedgerDAO.createAddition(aDBS=aDBS, unitType=unitType,
                                                 ledgerCount=2 * ledgerCount)
                    aDBS.commit()
                    unitLedgerHoldModel = UnitLedgerDAO.createHold(aDBS=aDBS, unitType=unitType,
                                                                   ledgerCount=ledgerCount)
                    aDBS.commit()
                    holdSMID = unitLedgerHoldModel.smID
                    UnitLedgerDAO.createDeduction(aDBS=aDBS, unitType=unitType,
                                                  ledgerCount=deductionCount, holdSMID=holdSMID)
                    aDBS.commit()
                    self.assertEqual(UnitLedgerDAO.getBySMID(
                        aDBS=aDBS, smID=holdSMID, raiseOnNone=False), None)
