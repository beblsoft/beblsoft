#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Unit Ledger Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import timedelta, datetime
from sqlalchemy import or_, func, and_
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.model import UnitLedgerModel, UnitBalanceModel, LedgerAction, UnitType, LedgerTag
from smeckn.server.aDB.unitLedger.model import INITIAL_FREE_TEXT_ANALYSIS


# ----------------------- GLOBALS ------------------------------------------- #
logger        = logging.getLogger(__name__)
HOLD_INTERVAL = timedelta(days=1)


# ----------------------- UNIT LEDGER DAO CLASS ----------------------------- #
class UnitLedgerDAO():
    """
    Unit Ledger Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def createAddition(aDBS, unitType, ledgerCount, **kwargs):  # pylint: disable=W0221
        """
        Create addition to ledger
        """
        # Validate the following:
        # 1. ledgerCount is positive

        # Check 1
        if ledgerCount <= 0:
            raise BeblsoftError(code=BeblsoftErrorCode.LEDGER_ADDITION_MUST_BE_POSITIVE)

        return UnitLedgerDAO._create(aDBS=aDBS, unitType=unitType, ledgerAction=LedgerAction.ADDITION,
                                     ledgerCount=ledgerCount, **kwargs)

    @staticmethod
    @logFunc()
    def createHold(aDBS, unitType, ledgerCount, **kwargs):  # pylint: disable=W0221
        """
        Create hold on ledger
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="UnitLedger WRITE")

        # Validate the following:
        # 1. ledgerCount is negative
        # 2. Enough available balance to create hold

        # Check 1
        if ledgerCount <= 0:
            raise BeblsoftError(code=BeblsoftErrorCode.LEDGER_HOLD_MUST_BE_POSITIVE)

        # Check 2
        unitBalanceModel = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
        if unitBalanceModel.nAvailable < ledgerCount:
            raise BeblsoftError(code=BeblsoftErrorCode.LEDGER_NOT_ENOUGH_UNITS_FOR_HOLD,
                                msg="Available={} Desired={}".format(unitBalanceModel.nAvailable, ledgerCount))

        return UnitLedgerDAO._create(aDBS=aDBS, unitType=unitType, ledgerAction=LedgerAction.HOLD,
                                     ledgerCount=ledgerCount, **kwargs)

    @staticmethod
    @logFunc()
    def createDeduction(aDBS, unitType, ledgerCount, holdSMID, **kwargs):  # pylint: disable=W0221
        """
        Create deduction on ledger
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="UnitLedger WRITE")

        # Validate the following
        # 1. ledgerCount is negative
        # 2. Valid hold
        # 3. Hold has same unit type

        # Check 1
        if ledgerCount >= 0:
            raise BeblsoftError(code=BeblsoftErrorCode.LEDGER_DEDUCTION_MUST_BE_NEGATIVE)

        # Check 2
        unitLedgerHoldModel = UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=holdSMID)

        # Check 3
        if unitLedgerHoldModel.unitType != unitType:
            raise BeblsoftError(code=BeblsoftErrorCode.LEDGER_DEDUCTION_INVALID_UNITTYPE)

        # Update hold, optionally delete
        newHoldLedgerCount = unitLedgerHoldModel.ledgerCount + ledgerCount
        unitLedgerHoldModel.ledgerCount = newHoldLedgerCount
        if newHoldLedgerCount <= 0:
            aDBS.delete(unitLedgerHoldModel)

        return UnitLedgerDAO._create(aDBS=aDBS, unitType=unitType, ledgerAction=LedgerAction.DEDUCTION,
                                     ledgerCount=ledgerCount, **kwargs)

    @staticmethod
    @logFunc()
    def _create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        unitLedgerModel = UnitLedgerModel()
        for k, v in kwargs.items():
            setattr(unitLedgerModel, k, v)
        aDBS.add(unitLedgerModel)
        return unitLedgerModel

    @staticmethod
    @logFunc()
    def createInitial(aDBS):
        """
        Create initial units

        Only add units if they aren't already present
        """
        unitTypeList    = [UnitType.TEXT_ANALYSIS]
        ledgerCountList = [INITIAL_FREE_TEXT_ANALYSIS]

        for idx, unitType in enumerate(unitTypeList):
            ledgerCount = ledgerCountList[idx]
            nExisting   = UnitLedgerDAO.getAll(aDBS=aDBS, unitType=unitType,
                                               ledgerTag=LedgerTag.INITIAL_FREE_UNITS, count=True)
            if nExisting == 0:
                UnitLedgerDAO.createAddition(aDBS=aDBS, unitType=unitType,
                                             ledgerCount=ledgerCount,
                                             ledgerTag=LedgerTag.INITIAL_FREE_UNITS)

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(UnitLedgerModel).filter(UnitLedgerModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(UnitLedgerModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, unitLedgerModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(unitLedgerModel, k, v)
        aDBS.add(unitLedgerModel)
        return unitLedgerModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        unitLedgerModel = aDBS.query(UnitLedgerModel)\
            .filter(UnitLedgerModel.smID == smID)\
            .one_or_none()
        if not unitLedgerModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return unitLedgerModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, profileSMID=None, analysisSMID=None,
               unitType=None, ledgerAction=None, ledgerTag=None, analysisType=None,
               ledgerCount=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(UnitLedgerModel)

        # Filters
        if profileSMID:
            q = q.filter(UnitLedgerModel.profileSMID == profileSMID)
        if analysisSMID:
            q = q.filter(UnitLedgerModel.analysisSMID == analysisSMID)
        if unitType:
            q = q.filter(UnitLedgerModel.unitType == unitType)
        if ledgerAction:
            q = q.filter(UnitLedgerModel.ledgerAction == ledgerAction)
        if ledgerTag:
            q = q.filter(UnitLedgerModel.ledgerTag == ledgerTag)
        if analysisType:
            q = q.filter(UnitLedgerModel.analysisType == analysisType)
        if ledgerCount:
            q = q.filter(UnitLedgerModel.ledgerCount == ledgerCount)

        return q.count() if count else q.all()

    @staticmethod
    @logFunc()
    def getUnitBalance(aDBS, unitType):
        """
        Return unit balance

        Returns
          UnitBalanceModel
        """
        nTotal = aDBS.query(func.sum(UnitLedgerModel.ledgerCount))\
            .filter(UnitLedgerModel.unitType == unitType)\
            .filter(or_(UnitLedgerModel.ledgerAction == LedgerAction.DEDUCTION,
                        UnitLedgerModel.ledgerAction == LedgerAction.ADDITION))\
            .scalar()
        if not nTotal:
            nTotal = 0

        nHeld = aDBS.query(func.sum(UnitLedgerModel.ledgerCount))\
            .filter(UnitLedgerModel.unitType == unitType)\
            .filter(and_(UnitLedgerModel.ledgerAction == LedgerAction.HOLD,
                         UnitLedgerModel.createDate >= datetime.utcnow() - HOLD_INTERVAL))\
            .scalar()
        if not nHeld:
            nHeld = 0

        nAvailable = nTotal - nHeld

        return UnitBalanceModel(unitType=unitType, nTotal=nTotal, nHeld=nHeld, nAvailable=nAvailable)
