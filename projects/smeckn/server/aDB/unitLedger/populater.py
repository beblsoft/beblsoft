#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Unit Ledger Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- UNIT LEDGER POPULATER CLASS ----------------------- #
class UnitLedgerPopulater(CommonPopulater):
    """
    Unit Ledger Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateCounts(self, aID, unitType, profileSMID=None, totalCount=100, holdCount=50):  # pylint: disable=W0102
        """
        Populate Counts

        Returns
          [UnitLedgerAdditionModel, UnitLedgerHoldModel]
        """
        unitLedgerAdditionModel = None
        unitLedgerHoldModel     = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="UnitLedger WRITE"):
                unitLedgerAdditionModel = UnitLedgerDAO.createAddition(
                    aDBS        = aDBS,
                    unitType    = unitType,
                    ledgerCount = totalCount,
                    profileSMID = profileSMID)
                aDBS.commit()
                unitLedgerHoldModel = UnitLedgerDAO.createHold(
                    aDBS        = aDBS,
                    unitType    = unitType,
                    ledgerCount = holdCount,
                    profileSMID = profileSMID)
                aDBS.commit()
            unitLedgerAdditionModel = UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=unitLedgerAdditionModel.smID)
            unitLedgerHoldModel     = UnitLedgerDAO.getBySMID(aDBS=aDBS, smID=unitLedgerHoldModel.smID)

        return [unitLedgerAdditionModel, unitLedgerHoldModel]

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            UnitLedgerDAO.deleteAll(aDBS=aDBS)
