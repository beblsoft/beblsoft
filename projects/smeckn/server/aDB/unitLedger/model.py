#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Unit Ledger Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, BigInteger, Enum, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.analysis.model import AnalysisType


# ----------------------- GLOBALS ------------------------------------------- #
logger                     = logging.getLogger(__name__)
INITIAL_FREE_TEXT_ANALYSIS = 40


# ----------------------- ENUMERATIONS -------------------------------------- #
class UnitType(enum.Enum):
    """
    Unit Type Enumeration
    """
    TEXT_ANALYSIS  = 1
    PHOTO_ANALYSIS = 2

    def toDescriptionString(self):
        """
        Return unit description string
        """
        descStrDict = {
            UnitType.TEXT_ANALYSIS: "Text Analysis",
            UnitType.PHOTO_ANALYSIS: "Photo Analysis",
        }
        return descStrDict[self]


class LedgerAction(enum.Enum):
    """
    Ledger Action Enumeration
    """
    HOLD       = 1
    DEDUCTION  = 2
    ADDITION   = 3


class LedgerTag(enum.Enum):
    """
    Ledger Tag Enumeration
    """
    INITIAL_FREE_UNITS = 1


# ----------------------- UNIT LEDGER MODEL CLASS --------------------------- #
class UnitLedgerModel(base):
    """
    Unit Ledger Table
    """
    __tablename__       = "UnitLedger"
    smID                = Column(BigInteger, primary_key=True)
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)    # Ex. "2000-01-04 07:53:55.474912"

    unitType            = Column(Enum(UnitType))             # Ex. UnitType.TEXT_ANALYSIS
    ledgerAction        = Column(Enum(LedgerAction))         # Ex. LedgerAction.HOLD
    ledgerTag           = Column(Enum(LedgerTag))            # Ex. LedgerTag.INITIAL_FREE_UNITS
    analysisType        = Column(Enum(AnalysisType))         # Ex. AnalysisType.COMPREHEND_SENTIMENT
    ledgerCount         = Column(Integer)                    # Count of units added or removed. Ex. -5


    # Parent Analysis
    # 1 Analysis : 1 Unit Ledger Hold
    analysisModel       = relationship("AnalysisModel", back_populates="unitLedgerHoldModel",
                                       uselist=False, passive_deletes=True)

    def __repr__(self):
        return "[{} smID={} unitType={}]".format(self.__class__.__name__, self.smID, self.unitType)


# ----------------------- UNIT BALANCE MODEL CLASS -------------------------- #
class UnitBalanceModel():
    """
    Unit Balance Model Class
    """

    def __init__(self, unitType, nTotal, nHeld, nAvailable):
        """
        Initialize object
        Args
          unitType:
            Type of unit
            Ex. UnitType.TEXT_ANALYSIS
          nTotal:
            Number of total units
            Ex. 100
          nHeld:
            Number of held units
            Ex. 45
          nAvailable:
            Numver of available units
            Ex. 55
        """
        self.unitType   = unitType
        self.nTotal     = nTotal
        self.nHeld      = nHeld
        self.nAvailable = nAvailable
