#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Comprehend Sentiment Analysis Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel, CompSentType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND SENTIMENT ANALYSIS DAO CLASS ----------- #
class CompSentAnalysisDAO():
    """
    Comprehend Sentiment Analysis Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        compSentAnalysisModel = CompSentAnalysisModel()
        if "createDate" not in kwargs:
            kwargs["createDate"] = datetime.utcnow()
        for k, v in kwargs.items():
            setattr(compSentAnalysisModel, k, v)
        aDBS.add(compSentAnalysisModel)
        return compSentAnalysisModel

    @staticmethod
    @logFunc()
    def createFromBCompDetectSentimentOutput(aDBS, bCompDetectSentimentOutput, **kwargs):
        """
        Create from comprehend detect sentiment output
        Args
          aDBS:
            Acount Database Session
          bCompDetectSentimentOutput:
            Output from BeblsoftComprehendClient.detectSentiment
        """
        dominantSentiment     = CompSentType.fromCompSentStr(bCompDetectSentimentOutput.get("Sentiment"))
        sentimentScore        = bCompDetectSentimentOutput.get("SentimentScore")
        positiveScore         = sentimentScore.get("Positive")
        negativeScore         = sentimentScore.get("Negative")
        neutralScore          = sentimentScore.get("Neutral")
        mixedScore            = sentimentScore.get("Mixed")
        compSentAnalysisModel = CompSentAnalysisDAO.create(
            aDBS=aDBS, dominantSentiment=dominantSentiment, positiveScore=positiveScore,
            negativeScore=negativeScore, neutralScore=neutralScore, mixedScore=mixedScore, **kwargs)
        aDBS.add(compSentAnalysisModel)
        return compSentAnalysisModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(CompSentAnalysisModel).filter(CompSentAnalysisModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(CompSentAnalysisModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, compSentAnalysisModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(compSentAnalysisModel, k, v)
        aDBS.add(compSentAnalysisModel)
        return compSentAnalysisModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone = True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        compSentAnalysisModel = aDBS.query(CompSentAnalysisModel)\
            .filter(CompSentAnalysisModel.smID == smID)\
            .one_or_none()
        if not compSentAnalysisModel and raiseOnNone:
            raise BeblsoftError(code = BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return compSentAnalysisModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, textSMID = None, count = False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(CompSentAnalysisModel)

        # Filters
        if textSMID:
            q = q.filter(CompSentAnalysisModel.textSMID == textSMID)

        return q.count() if count else q.all()
