#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Comprehend Sentiment Analysis Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import random
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus, CompSentType
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND SENTIMENT ANALYSIS POPULATER CLASS ----- #
class CompSentAnalysisPopulater(CommonPopulater):
    """
    Comprehend Sentiment Analysis Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, aID, textSMIDList=[]):  # pylint: disable=W0102
        """
        Populate List

        Returns
          CompSentAnalysisModel List
        """
        compSentAnalysisModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for textSMID in textSMIDList:
                CompSentAnalysisDAO.create(
                    aDBS          = aDBS,
                    textSMID      = textSMID,
                    status        = random.choice(list(CompSentAnalysisStatus)),
                    compDomSent   = random.choice(list(CompSentType)),
                    positiveScore = random.uniform(0, 1),
                    negativeScore = random.uniform(0, 1),
                    neutralScore  = random.uniform(0, 1),
                    mixedScore    = random.uniform(0, 1))
            aDBS.commit()
            compSentAnalysisModelList = CompSentAnalysisDAO.getAll(aDBS=aDBS)
        return compSentAnalysisModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            CompSentAnalysisDAO.deleteAll(aDBS=aDBS)
