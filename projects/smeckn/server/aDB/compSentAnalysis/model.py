#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Comprehend Sentiment Analysis Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Float, Enum
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.contentAttr.common import CommonContentAttr
from smeckn.server.aDB.groupBy.model import GroupByType, GroupByResultType
from smeckn.server.aDB.groupBy.common import CommonGroupBy
from smeckn.server.aDB.sort.common import CommonSort
from smeckn.server.aDB.sort.type import SortType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class CompSentAnalysisStatus(enum.Enum):
    """
    Comprehend Sentiment Analysis Status Enumeration
    """
    SUCCESS              = 0
    UNSUPPORTED_LANGUAGE = 1
    COMPREHEND_FAILED    = 2


class CompSentType(enum.Enum):
    """
    Comprehend Sentiment Type Enumeration
    """
    POSITIVE             = 0
    NEGATIVE             = 1
    NEUTRAL              = 2
    MIXED                = 3

    @staticmethod
    def fromCompSentStr(string):
        """
        Return CompSentType from output strings of BeblsoftComprehendClient.detectSentiment
        """
        return CompSentType[string]

    @property
    def contentAttrType(self):
        """
        Return corresponding ContentAttrType
        """
        sentDict = {
            CompSentType.POSITIVE: ContentAttrType.COMP_SENT_TYPE_POSITIVE,
            CompSentType.NEGATIVE: ContentAttrType.COMP_SENT_TYPE_NEGATIVE,
            CompSentType.NEUTRAL: ContentAttrType.COMP_SENT_TYPE_NEUTRAL,
            CompSentType.MIXED: ContentAttrType.COMP_SENT_TYPE_MIXED,
        }
        return sentDict[self]


# ----------------------- COMPREHEND SENTIMENT ANALYSIS MODEL CLASS --------- #
class CompSentAnalysisModel(base):
    """
    Comprehend Sentiment Analysis Table
    """
    __tablename__       = "CompSentAnalysis"
    smID                = Column(BigInteger, primary_key=True)   # ID
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    status              = Column(Enum(CompSentAnalysisStatus),
                                 default=None)                   # Ex. CompSentAnalysisStatus.SUCCESS

    # From Detect Sentiment
    dominantSentiment   = Column(Enum(CompSentType), default=None)        # Ex. CompSentType.POSITIVE
    positiveScore       = Column(Float(precision="13,12"), default=None)  # Ex. .9992
    negativeScore       = Column(Float(precision="13,12"), default=None)  # Ex. .0002
    neutralScore        = Column(Float(precision="13,12"), default=None)  # Ex. .0002
    mixedScore          = Column(Float(precision="13,12"), default=None)  # Ex. .0002

    # Parent Text
    # 1 Text : 1 CompSentAnalysis
    textSMID            = Column(BigInteger,
                                 ForeignKey("Text.smID", onupdate="CASCADE", ondelete="CASCADE"))
    textModel           = relationship("TextModel", back_populates="compSentAnalysisModel")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)


# ----------------------- CONTENT ATTR CLASSES ------------------------------ #
class CompSentAnalysisDominantSentiment(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT
    column          = CompSentAnalysisModel.dominantSentiment
    numeric         = False


class CompSentAnalysisPositiveScore(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_POSITIVE_SCORE
    column          = CompSentAnalysisModel.positiveScore
    numeric         = True


class CompSentAnalysisNegativeScore(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE
    column          = CompSentAnalysisModel.negativeScore
    numeric         = True


class CompSentAnalysisNeutralScore(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE
    column          = CompSentAnalysisModel.neutralScore
    numeric         = True


class CompSentAnalysisMixedScore(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_MIXED_SCORE
    column          = CompSentAnalysisModel.mixedScore
    numeric         = True


# ----------------------- GROUP BY CLASSES ---------------------------------- #
class ComprehendDominantSentimentGroupBy(CommonGroupBy):  # pylint: disable=C0111
    groupByResultType = GroupByResultType.CONTENT_ATTR_TYPE
    groupByType       = GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT
    column            = CompSentAnalysisModel.dominantSentiment

    @classmethod
    def mapToMySQLExpression(cls, groupByDateInterval=None):
        return cls.column

    @classmethod
    def mapFromMySQLResult(cls, result, groupByDateInterval=None):
        rval = None
        if result != None:
            assert isinstance(result, CompSentType)
            rval = result.contentAttrType
        return rval


# ----------------------- SORT CLASSES -------------------------------------- #
class CompSentAnalysisPositiveScoreSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE
    pColumn  = CompSentAnalysisModel.positiveScore
    sColumn  = CompSentAnalysisModel.smID

class CompSentAnalysisNegativeScoreSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE
    pColumn  = CompSentAnalysisModel.negativeScore
    sColumn  = CompSentAnalysisModel.smID

class CompSentAnalysisNeutralScoreSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE
    pColumn  = CompSentAnalysisModel.neutralScore
    sColumn  = CompSentAnalysisModel.smID

class CompSentAnalysisScoreMixedSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.COMP_SENT_ANALYSIS_MIXED_SCORE
    pColumn  = CompSentAnalysisModel.mixedScore
    sColumn  = CompSentAnalysisModel.smID
