#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Comprehend Sentiment Analysis Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import random
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus, CompSentType
from smeckn.server.aDB.text.dao import TextDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CompSentAnalysisTestCase(CommonTestCase):
    """
    Test Comprehend Sentiment Analysis Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True, compSentAnalysis=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            compSentAnalysisModel = CompSentAnalysisDAO.create(
                aDBS          = aDBS,
                status        = random.choice(list(CompSentAnalysisStatus)),
                compDomSent   = random.choice(list(CompSentType)),
                positiveScore = random.uniform(0, 1),
                negativeScore = random.uniform(0, 1),
                neutralScore  = random.uniform(0, 1),
                mixedScore    = random.uniform(0, 1))
            aDBS.commit()

            # Get
            compSentAnalysisModelList = CompSentAnalysisDAO.getAll(aDBS)
            initialCount              = len(compSentAnalysisModelList)

            # Delete
            CompSentAnalysisDAO.deleteBySMID(aDBS=aDBS, smID=compSentAnalysisModel.smID)
            aDBS.commit()

            # Verify Delete
            compSentAnalysisModelList = CompSentAnalysisDAO.getAll(aDBS)
            self.assertEqual(len(compSentAnalysisModelList), initialCount - 1)

    @logFunc()
    def test_scoreOne(self):
        """
        Test getting a score of 1.
        Ensures that score floats properly hold 1.
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            score                 = 1.0
            compSentAnalysisModel = CompSentAnalysisDAO.create(
                aDBS          = aDBS,
                status        = random.choice(list(CompSentAnalysisStatus)),
                compDomSent   = random.choice(list(CompSentType)),
                positiveScore = score,
                negativeScore = score,
                neutralScore  = score,
                mixedScore    = score)
            aDBS.commit()
            self.assertEqual(compSentAnalysisModel.positiveScore, score)
            self.assertEqual(compSentAnalysisModel.negativeScore, score)
            self.assertEqual(compSentAnalysisModel.neutralScore, score)
            self.assertEqual(compSentAnalysisModel.mixedScore, score)

    @logFunc()
    def test_textCascadeDelete(self):
        """
        Test text deletions cascade down
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            nCompSentAnalysis = CompSentAnalysisDAO.getAll(aDBS, count=True)
            self.assertGreater(nCompSentAnalysis, 0)

            TextDAO.deleteBySMID(aDBS=aDBS, smID=self.popTextSMID)
            aDBS.commit()

            nCompSentAnalysis = CompSentAnalysisDAO.getAll(aDBS, count=True)
            self.assertEqual(nCompSentAnalysis, 0)
