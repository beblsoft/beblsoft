#!/usr/bin/env python3
"""
NAME
 filter.py

DESCRIPTION
 Comp Sent Analysis Filter Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import and_, not_, or_
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.aDB.text.model import TextModel
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel, CompSentAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMP SENT ANALYSIS FILTER ------------------------- #
class CompSentAnalysisFilter():
    """
    Comp Sent Analysis Filter Object

    Analyses in this section require
      - outerjoin(TextModel)
      - outerjoin(CompSentAnalysisModel)
      - outerjoin(CompLangAnalysisModel)

    These filters assumed that the TextModel exists, if this assumption is invalid
    additional filters may need to be applied. See FacebookPostDAO.getAll
    """

    done = (CompSentAnalysisModel.status == CompSentAnalysisStatus.SUCCESS)

    doneAndLanguageStillNotSupported = (
        and_(
            CompSentAnalysisModel.status == CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE,
            CompLangAnalysisModel.dominantBLanguageType.notin_(  # pylint: disable=E1101
                BeblsoftComprehendClient.detectSentimentSupportedBLanguageTypeList
            )
        )
    )

    cant = doneAndLanguageStillNotSupported

    notDone = (
        or_(
            TextModel.compSentAnalysisModel == None,
            and_(
                not_(done),
                not_(cant)
            )
        )
    )
