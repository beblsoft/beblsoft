#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Analysis Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, Enum, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.contentType.model import ContentType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class AnalysisStatus(enum.Enum):
    """
    Analysis Status Enumeration
    """
    IN_PROGRESS                    = 0
    SUCCESS                        = 1
    TIMED_OUT                      = 2

    @property
    def isInProgress(self):  # pylint: disable=C0111
        return self == AnalysisStatus.IN_PROGRESS


class AnalysisType(enum.Enum):
    """
    Analysis Type Enumeration
    """
    COMPREHEND_LANGUAGE            = 0   # 0  - 19 : Comprehend Reserve
    COMPREHEND_SENTIMENT           = 1
    COMPREHEND_SYNTAX              = 2
    REKOGNITION_FACES              = 20  # 20 - 39 : Rekognition Reserve
    REKOGNITION_ENTITIES           = 21
    REKOGNITION_MODERATION_LABELS  = 22


# ----------------------- ANALYSIS MODEL CLASS ------------------------------ #
class AnalysisModel(base):
    """
    Analysis Table
    """
    __tablename__          = "Analysis"
    smID                   = Column(BigInteger, primary_key=True)
    createDate             = Column(DATETIME(fsp=6),
                                    default=datetime.utcnow)    # Ex. "2000-01-04 07:53:55.474912"
    completeDate           = Column(DATETIME(fsp=6))            # Date completed
    workerKickDate         = Column(DATETIME(fsp=6))            # Date workers kicked
    workerHeartbeatDate    = Column(DATETIME(fsp=6))            # Date worker heartbeat, written to by multiple workers

    contentType            = Column(Enum(ContentType))          # Ex. ContentType.POST
    contentCount           = Column(BigInteger)                 # Count of content being analyzed
    analysisType           = Column(Enum(AnalysisType))         # Ex. AnalysisType.COMPREHEND_SENTIMENT
    status                 = Column(Enum(AnalysisStatus))       # Ex. AnalysisStatus.IN_PROGRESS
    errorSeen              = Column(Boolean, default=False)     # If True, client has seen error
    nWorkerKicks           = Column(Integer, default=0)         # Number of workers kicked


    # Parent Profile
    # 1 Profile : N Analysis
    profileSMID            = Column(BigInteger,
                                    ForeignKey("Profile.smID", onupdate="CASCADE", ondelete="CASCADE"),
                                    nullable=False)
    profileModel           = relationship("ProfileModel", back_populates="analysisModelList")

    # Child Unit Ledger Hold
    # 1 Analysis : N Unit Ledgers
    unitLedgerHoldSMID     = Column(BigInteger,
                                    ForeignKey("UnitLedger.smID", onupdate="SET NULL", ondelete="SET NULL"))
    unitLedgerHoldModel    = relationship("UnitLedgerModel", back_populates="analysisModel")

    def __repr__(self):
        return "[{} smID={} contentType={} status={}]".format(
            self.__class__.__name__, self.smID, self.contentType, self.status)
