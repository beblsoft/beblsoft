#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Analysis Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.analysis.model import AnalysisStatus, AnalysisType
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AnalysisModelTestCase(CommonTestCase):
    """
    Test Analysis Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True, analyses=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        contentType  = ContentType.POST_MESSAGE
        analysisType = AnalysisType.COMPREHEND_SENTIMENT
        status       = AnalysisStatus.IN_PROGRESS

        profileSMID = self.nonPopProfileSMID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                analysisModel = AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                                   analysisType=analysisType, status=status)

            # Get
            analysisModelList = AnalysisDAO.getAll(aDBS, profileSMID=profileSMID)
            self.assertEqual(len(analysisModelList), 1)
            analysisModel = analysisModelList[0]
            self.assertEqual(analysisModel.status, status)

            # Delete
            AnalysisDAO.deleteBySMID(aDBS=aDBS, smID=analysisModel.smID)
            aDBS.commit()

            # Verify Delete
            analysisModelList = AnalysisDAO.getAll(aDBS, profileSMID=profileSMID)
            self.assertEqual(len(analysisModelList), 0)

    @logFunc()
    def test_profileCascadeDelete(self):
        """
        Test deleting profile deletes analyses
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Get
            analysisModelList = AnalysisDAO.getAll(aDBS)
            self.assertGreater(len(analysisModelList), 0)

            # Delete
            ProfileDAO.deleteBySMID(aDBS=aDBS, smID=self.popProfileSMID)
            aDBS.commit()

            # Verify Delete
            analysisModelList = AnalysisDAO.getAll(aDBS)
            self.assertEqual(len(analysisModelList), 0)

    @logFunc()
    def test_unitLedgerHoldCascadeDelete(self):
        """
        Test unit ledger hold deletion cascades down but doesn't delete analysis
        """
        super().setUp(unitLedger=True)
        unitType     = UnitType.TEXT_ANALYSIS
        holdSMID     = self.unitTypeDict[unitType]["unitLedgerHoldSMID"]
        analysisSMID = self.popAnalysisSMID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisModel = AnalysisDAO.getBySMID(aDBS=aDBS, smID=analysisSMID)
            with AccountDatabase.transactionScope(session=aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisModel, unitLedgerHoldSMID=holdSMID)

            # Verify ID set
            self.assertEqual(analysisModel.unitLedgerHoldSMID, holdSMID)

            # Delete hold
            with AccountDatabase.transactionScope(session=aDBS):
                UnitLedgerDAO.deleteBySMID(aDBS=aDBS, smID=holdSMID)

            # Verify ID updated
            self.assertEqual(analysisModel.unitLedgerHoldSMID, None)

    @logFunc()
    def test_nullProfileSMID(self):
        """
        Test trying to create analysis with NULL profileSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                    AnalysisDAO.create(
                        aDBS=aDBS, profileSMID=None, contentType=ContentType.POST_MESSAGE,
                        analysisType=AnalysisType.COMPREHEND_SENTIMENT, status=AnalysisStatus.IN_PROGRESS)
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created analysis with null profileSMID")

    @logFunc()
    def test_badProfileSMID(self):
        """
        Test trying to create analysis with bad profileSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                    AnalysisDAO.create(
                        aDBS=aDBS, profileSMID=2398723948723, contentType=ContentType.POST_MESSAGE,
                        analysisType=AnalysisType.COMPREHEND_SENTIMENT, status=AnalysisStatus.IN_PROGRESS)
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created analysis with invalid profileSMID")

    @logFunc()
    def test_analysisDuringSync(self):
        """
        Test trying to create an analysis while a sync is in progress
        """
        profileSMID     = self.nonPopProfileSMID
        contentType     = ContentType.POST_MESSAGE
        analysisType    = AnalysisType.COMPREHEND_SENTIMENT
        status          = AnalysisStatus.IN_PROGRESS
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID, status=SyncStatus.IN_PROGRESS)
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                    AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                       analysisType=analysisType, status=status)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.ANALYSIS_NOT_ALLOWED_WITH_SYNC)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Able to create analysis while sync in progress")

    @logFunc()
    def test_twoInProgress(self):
        """
        Test trying to create two in progress analyses at the same time
        """
        profileSMID     = self.nonPopProfileSMID
        contentType     = ContentType.POST_MESSAGE
        analysisType    = AnalysisType.COMPREHEND_SENTIMENT
        status          = AnalysisStatus.IN_PROGRESS
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                    AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                       analysisType=analysisType, status=status)
                    aDBS.commit()
                    AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                       analysisType=analysisType, status=status)
                    aDBS.commit()
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.ANALYSIS_ALREADY_IN_PROGRESS)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Able to create two in progress analyses")
