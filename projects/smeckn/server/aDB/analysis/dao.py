#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Analysis Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from sqlalchemy import desc, asc
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.analysis.model import AnalysisModel, AnalysisStatus
from smeckn.server.aDB import AccountDatabase


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ANALYSIS DAO CLASS -------------------------------- #
class AnalysisDAO():
    """
    Analysis Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, profileSMID, contentType, analysisType,
               status=AnalysisStatus.IN_PROGRESS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE")

        # Validate Analysis Can Be Created
        AnalysisDAO.validateCreateNoProfileSyncInProgress(aDBS, profileSMID=profileSMID)
        AnalysisDAO.validateCreateNoProfileContentAnalysisInProgress(aDBS, profileSMID=profileSMID,
                                                                     contentType=contentType,
                                                                     analysisType=analysisType)

        # Create Analysis
        analysisModel = AnalysisModel(profileSMID=profileSMID, contentType=contentType,
                                      analysisType=analysisType, status=status)

        if "createDate" not in kwargs:
            kwargs["createDate"] = datetime.utcnow()
        for k, v in kwargs.items():
            setattr(analysisModel, k, v)
        aDBS.add(analysisModel)
        return analysisModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(AnalysisModel).filter(AnalysisModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(AnalysisModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, analysisModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(analysisModel, k, v)
        aDBS.add(analysisModel)
        return analysisModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        analysisModel = aDBS.query(AnalysisModel)\
            .filter(AnalysisModel.smID == smID)\
            .one_or_none()
        if not analysisModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return analysisModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, profileSMID=None, contentType=None, analysisType=None,
               status=None,
               orderCreateDateDesc=False, orderCreateDateAsc=False,
               count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(AnalysisModel)

        # Filters
        if profileSMID:
            q = q.filter(AnalysisModel.profileSMID == profileSMID)
        if contentType:
            q = q.filter(AnalysisModel.contentType == contentType)
        if analysisType:
            q = q.filter(AnalysisModel.analysisType == analysisType)
        if status:
            q = q.filter(AnalysisModel.status == status)

        # Order
        if orderCreateDateDesc:
            q = q.order_by(desc(AnalysisModel.createDate))
        if orderCreateDateAsc:
            q = q.order_by(asc(AnalysisModel.createDate))

        return q.count() if count else q.all()

    # ---------------------- VALIDATION ------------------------------------- #
    @staticmethod
    @logFunc()
    def validateCreateNoProfileSyncInProgress(aDBS, profileSMID):
        """
        Validate no sync in progress
        """
        from smeckn.server.aDB.sync.dao import SyncDAO
        from smeckn.server.aDB.sync.model import SyncStatus

        syncModelList = SyncDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                       status=SyncStatus.IN_PROGRESS)
        if syncModelList:
            raise BeblsoftError(BeblsoftErrorCode.ANALYSIS_NOT_ALLOWED_WITH_SYNC)

    @staticmethod
    @logFunc()
    def validateCreateNoProfileContentAnalysisInProgress(aDBS, profileSMID,
                                                         contentType=None, analysisType=None):
        """
        Validate profile doesn't have same content being currently analyzed
        """
        analysisModelList = AnalysisDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                               contentType=contentType, analysisType=analysisType,
                                               status=AnalysisStatus.IN_PROGRESS)
        if analysisModelList:
            raise BeblsoftError(BeblsoftErrorCode.ANALYSIS_ALREADY_IN_PROGRESS,
                                msg="ProfileSMID={} contentType={} analysisType={}".format(
                                    profileSMID, contentType, analysisType))

    # ---------------------- LOCKING ---------------------------------------- #
    @staticmethod
    @logFunc()
    def lockSMID(aDBS, smID, update=True):
        """
        Lock row
        Args
          update:
            If True, lock row for update
            Else, lock for read
        """
        aDBS.execute("SELECT * from Analysis where smID={} {}".format(
            smID, "FOR UPDATE" if update else "LOCK IN SHARE MODE"))
