#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Analysis Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.analysis.model import AnalysisStatus, AnalysisType
from smeckn.server.aDB.contentType.model import ContentType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ANALYSIS POPULATER CLASS -------------------------- #
class AnalysisPopulater(CommonPopulater):
    """
    Analysis Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateOne(self,
                    aID,
                    profileSMID,
                    contentType         = ContentType.POST,
                    analysisType        = AnalysisType.COMPREHEND_SENTIMENT,
                    status              = AnalysisStatus.IN_PROGRESS,
                    contentCount        = 0,
                    workerKickDate      = None,
                    workerHeartbeatDate = None):  # pylint: disable=W0102
        """
        Populate One

        Returns
          Analysis Model
        """
        analysisModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                analysisModel = AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                                   contentCount=contentCount, analysisType=analysisType,
                                                   status=status, workerKickDate=workerKickDate,
                                                   workerHeartbeatDate=workerHeartbeatDate)
            analysisModel = AnalysisDAO.getBySMID(aDBS=aDBS, smID=analysisModel.smID)
        return analysisModel

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            AnalysisDAO.deleteAll(aDBS=aDBS)
