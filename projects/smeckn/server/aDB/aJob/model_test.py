#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Account Job Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import re
import uuid
import logging
from datetime import datetime, timedelta
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.aJob.dao import AccountJobDAO
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.aJob.model import AccountJobState


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountJobModelTestCase(CommonTestCase):
    """
    Test Account Job Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)

    @logFunc()
    def test_crud(self):
        """
        Test crud operations
        """
        nJobs         = 10
        aJobModelList = []
        classModule   = "foo.bar.baz"
        className     = "ExampleJob"
        groupURN      = 10
        node          = uuid.getnode()
        groupURN      = uuid.uuid1(node=node).urn
        parentSMID    = 5
        incarn        = 100000000
        heartbeatDate = datetime.utcnow()
        msgIDPrefix   = "msgID"

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                for idx in range(nJobs):
                    aJobModel = AccountJobDAO.create(
                        aDBS          = aDBS,
                        msgID         = "{}{}".format(msgIDPrefix, idx),
                        groupURN      = groupURN,
                        parentSMID    = parentSMID,
                        incarn        = incarn,
                        state         = AccountJobState.START,
                        heartbeatDate = heartbeatDate,
                        classModule   = classModule,
                        className     = className,
                        functionRN    = "functionRN{}".format(idx),
                        requestID     = "requestID{}".format(idx),
                        logStream     = "logStream{}".format(idx))
                    aJobModelList.append(aJobModel)

            # Read, Verify Creation
            for idx in range(nJobs):
                msgID = "{}{}".format(msgIDPrefix, idx)
                aJobModel = AccountJobDAO.getOne(aDBS=aDBS, msgID=msgID)
                self.assertEqual(aJobModel.msgID, msgID)
                self.assertEqual(aJobModel.groupURN, groupURN)
                self.assertEqual(aJobModel.parentSMID, parentSMID)
                self.assertEqual(aJobModel.incarn, incarn)
                self.assertEqual(aJobModel.state, AccountJobState.START)
                self.assertLessEqual(heartbeatDate - aJobModel.heartbeatDate, timedelta(seconds=1))
                self.assertEqual(aJobModel.classModule, classModule)
                self.assertEqual(aJobModel.className, className)

            # Delete
            with AccountDatabase.transactionScope(session=aDBS):
                for idx in range(nJobs):
                    msgID = "{}{}".format(msgIDPrefix, idx)
                    AccountJobDAO.deleteByFilter(aDBS=aDBS, msgID=msgID)

            # Read, Verify Deletion
            for idx in range(nJobs):
                msgID = "{}{}".format(msgIDPrefix, idx)
                aJobModel = AccountJobDAO.getOne(aDBS=aDBS, msgID=msgID, raiseOnNone=False)
                self.assertEqual(aJobModel, None)

    @logFunc()
    def test_duplicateMsgID(self):
        """
        Test adding duplicate msgID
        """
        try:
            msgID = "MsgID"
            with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
                AccountJobDAO.create(
                    aDBS    = aDBS,
                    msgID   = msgID)
                AccountJobDAO.create(
                    aDBS    = aDBS,
                    msgID   = msgID)
        except sqlalchemy.exc.IntegrityError as e:
            match = re.search(r"Duplicate entry .* for key 'uq_AccountJob_msgID'", str(e))
            self.assertNotEqual(match, None)
        else:
            raise Exception("No exception raised")
