#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Account Job Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.aJob.model import AccountJobModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT JOB DAO CLASS ----------------------------- #
class AccountJobDAO():
    """
    Accout Job Data Access Object
    """

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):
        """
        Create new object
        Args
          aDBS:
            Account DB session
          kwargs:
            Arguments
        """
        if "creationDate" not in kwargs:
            kwargs["creationDate"] = datetime.utcnow()
        aJobModel = AccountJobModel(**kwargs)
        aDBS.add(aJobModel)
        return aJobModel

    # ---------------------- DELETE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByFilter(aDBS, smID=None, msgID=None):
        """
        Delete object
        """
        query = aDBS.query(AccountJobModel)
        if smID:
            query = query.filter(AccountJobModel.smID == smID)
        if msgID:
            query = query.filter(AccountJobModel.msgID == msgID)
        query.delete()

    @staticmethod
    @logFunc()
    def delete(aDBS, aJobModel):
        """
        Delete object
        """
        aDBS.delete(aJobModel)

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(AccountJobModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, aJobModel, **kwargs):  # pylint: disable=W0613
        """
        Update object
        Args
          aJobModel:
            Account Job Model
          kwargs:
            Dictionary of updates
        """
        for k, v in kwargs.items():
            setattr(aJobModel, k, v)
        aDBS.add(aJobModel)
        return aJobModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getOne(aDBS, smID=None, msgID=None, raiseOnNone=True):
        """
        Return one object
        Args
          smID:
            Smeckn ID
        """
        query = aDBS.query(AccountJobModel)

        # Filters
        if smID:
            query = query.filter(AccountJobModel.smID == smID)
        if msgID:
            query = query.filter(AccountJobModel.msgID == msgID)

        # Execute
        aJobModel = query.one_or_none()
        if not aJobModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.DB_OBJECT_DOES_NOT_EXIST)
        return aJobModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, className=None, count=False):
        """
        Get all following a specific criteria
        """
        rval  = None
        query = aDBS.query(AccountJobModel)

        # Filters
        if className:
            query = query.filter(AccountJobModel.className == className)

        # Rval
        rval = query.count() if count else query.all()
        return rval

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(aDBS):  # pylint: disable=W0221
        """
        Describe all
        """
        aJobModelList = AccountJobDAO.getAll(aDBS=aDBS)
        for aJobModel in aJobModelList:
            AccountJobDAO.describe(aDBS=aDBS, aJobModel=aJobModel)

    @staticmethod
    @logFunc()
    def describe(aDBS, aJobModel):  #pylint: disable=W0613
        """
        Describe object
        """
        logger.info("{}".format(aJobModel))
