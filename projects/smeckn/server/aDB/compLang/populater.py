#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Comprehend Language Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import random
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.compLang.dao import CompLangDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND LANGUAGE POPULATER CLASS --------------- #
class CompLangPopulater(CommonPopulater):
    """
    Comprehend Language Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, aID, compLangAnalysisSMID, nLanguages=5):  # pylint: disable=W0102
        """
        Populate List

        Returns
          CompLangModel List
        """
        compLangModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(nLanguages):
                CompLangDAO.create(
                    aDBS                 = aDBS,
                    compLangAnalysisSMID = compLangAnalysisSMID,
                    bLanguageType        = random.choice(list(BeblsoftLanguageType)),
                    score                = random.uniform(0, 1))
            aDBS.commit()
            compLangModelList = CompLangDAO.getAll(aDBS=aDBS)
        return compLangModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            CompLangDAO.deleteAll(aDBS=aDBS)
