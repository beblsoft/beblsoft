#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Comprehend Language Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import random
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.compLang.dao import CompLangDAO
from smeckn.server.aDB.text.dao import TextDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CompLangTestCase(CommonTestCase):
    """
    Test Comprehend Language Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True, compLangAnalysis=True, compLang=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            bLanguageType = random.choice(list(BeblsoftLanguageType))
            compLangModel = CompLangDAO.create(
                aDBS                 = aDBS,
                compLangAnalysisSMID = self.popCompLangAnalysisSMID,
                bLanguageType        = bLanguageType,
                score                = random.uniform(0, 1))
            aDBS.commit()
            self.assertEqual(compLangModel.bLanguageType, bLanguageType)

            # Get
            compLangModelList = CompLangDAO.getAll(aDBS)
            initialCount      = len(compLangModelList)

            # Delete
            CompLangDAO.deleteBySMID(aDBS=aDBS, smID=compLangModel.smID)
            aDBS.commit()

            # Verify Delete
            compLangModelList = CompLangDAO.getAll(aDBS)
            self.assertEqual(len(compLangModelList), initialCount - 1)

    @logFunc()
    def test_scoreOne(self):
        """
        Test getting a score of 1.
        Ensures that score float properly holds 1.
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            score         = 1.0
            bLanguageType = random.choice(list(BeblsoftLanguageType))
            compLangModel = CompLangDAO.create(
                aDBS                 = aDBS,
                compLangAnalysisSMID = self.popCompLangAnalysisSMID,
                bLanguageType        = bLanguageType,
                score                = score)
            aDBS.commit()
            self.assertEqual(compLangModel.score, score)

    @logFunc()
    def test_textCascadeDelete(self):
        """
        Test text deletions cascade down
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            nCompLang = CompLangDAO.getAll(aDBS, count=True)
            self.assertGreater(nCompLang, 0)

            TextDAO.deleteBySMID(aDBS=aDBS, smID=self.popTextSMID)
            aDBS.commit()

            nCompLang = CompLangDAO.getAll(aDBS, count=True)
            self.assertEqual(nCompLang, 0)

    @logFunc()
    def test_nullCompLangAnalysisSMID(self):
        """
        Test trying to create compLang with NULL compLangAnalysisSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                CompLangDAO.create(
                    aDBS                 = aDBS,
                    compLangAnalysisSMID = None)
                aDBS.commit()
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created compLang with null compLangAnalysisSMID")

    @logFunc()
    def test_badcompLangAnalysisSMID(self):
        """
        Test trying to create compLang with bad compLangAnalysisSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                CompLangDAO.create(
                    aDBS                 = aDBS,
                    compLangAnalysisSMID = 239487234987)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created analysis with invalid compLangAnalysisSMID")

    @logFunc()
    def test_cebuano(self):
        """
        Test trying to add Cebuano Language
        Cebuano was added 7/30/10
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            score         = 1.0
            bLanguageType = BeblsoftLanguageType.Cebuano
            compLangModel = None
            with AccountDatabase.transactionScope(session=aDBS):
                compLangModel = CompLangDAO.create(
                    aDBS                 = aDBS,
                    compLangAnalysisSMID = self.popCompLangAnalysisSMID,
                    bLanguageType        = bLanguageType,
                    score                = score)
            self.assertGreater(compLangModel.smID, 0)
            self.assertEqual(compLangModel.bLanguageType, bLanguageType)
