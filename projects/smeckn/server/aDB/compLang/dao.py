#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Comprehend Language Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.compLang.model import CompLangModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND LANGUAGE DAO CLASS --------------------- #
class CompLangDAO():
    """
    Comprehend Language Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        compLangModel = CompLangModel()
        for k, v in kwargs.items():
            setattr(compLangModel, k, v)
        aDBS.add(compLangModel)
        return compLangModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(CompLangModel).filter(CompLangModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(CompLangModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, compLangModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(compLangModel, k, v)
        aDBS.add(compLangModel)
        return compLangModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        compLangModel = aDBS.query(CompLangModel)\
            .filter(CompLangModel.smID == smID)\
            .one_or_none()
        if not compLangModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return compLangModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, compLangAnalysisSMID=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(CompLangModel)

        # Filters
        if compLangAnalysisSMID:
            q = q.filter(CompLangModel.compLangAnalysisSMID == compLangAnalysisSMID)

        return q.count() if count else q.all()
