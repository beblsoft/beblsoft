#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Comprehend Language Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Enum, Float
from sqlalchemy.orm import relationship
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMPREHEND LANGUAGE MODEL CLASS ------------------- #
class CompLangModel(base):
    """
    Comprehend Language Table
    """
    __tablename__         = "CompLangModel"
    smID                  = Column(BigInteger, primary_key=True)   # ID

    # From comprehend detect languages
    bLanguageType         = Column(Enum(BeblsoftLanguageType),
                                   default=None)                   # Ex. BeblsoftLanguageType.English
    score                 = Column(Float(precision="12,11"))       # Ex. .995


    # Parent CompLangAnalysis
    # 1 CompLangAnalysis : N CompLang
    compLangAnalysisSMID  = Column(BigInteger,
                                   ForeignKey("CompLangAnalysis.smID", onupdate="CASCADE", ondelete="CASCADE"),
                                   nullable=False)
    compLangAnalysisModel = relationship("CompLangAnalysisModel", back_populates="compLangModelList")

    def __repr__(self):
        return "[{} smID={} bLanguageType={} score={}]".format(
            self.__class__.__name__, self.smID, self.bLanguageType, self.score)
