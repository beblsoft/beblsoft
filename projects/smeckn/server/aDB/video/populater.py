#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Video Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.video.dao import VideoDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- VIDEO POPULATER CLASS ----------------------------- #
class VideoPopulater(CommonPopulater):
    """
    Video Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, count=5, heightPX=500, widthPX=500):  # pylint: disable=W0102
        """
        Populate N

        Returns
          Video Model List
        """
        videoModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(count):
                url = "https://www.example.com/{}.mp4".format(forgery_py.basic.text())
                VideoDAO.create(aDBS=aDBS, fromUser=True, videoURL=url,
                                heightPX=heightPX, widthPX=widthPX)
            aDBS.commit()
            videoModelList = VideoDAO.getAll(aDBS=aDBS)
        return videoModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            VideoDAO.deleteAll(aDBS=aDBS)
