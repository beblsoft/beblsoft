#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Video Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.video.dao import VideoDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class VideoModelTestCase(CommonTestCase):
    """
    Test Video Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, videos=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            url            = "www.example.com/helloWorld.mp4"
            heightPX       = 500
            widthPX        = 500
            VideoDAO.create(aDBS=aDBS, fromUser=True, videoURL=url,
                            heightPX=heightPX, widthPX=widthPX)
            aDBS.commit()

            # Get
            videoModelList = VideoDAO.getAll(aDBS, videoURL=url)
            self.assertEqual(len(videoModelList), 1)
            videoModel = videoModelList[0]
            self.assertEqual(videoModel.heightPX, heightPX)

            # Delete
            VideoDAO.deleteBySMID(aDBS=aDBS, smID=videoModel.smID)
            aDBS.commit()

            # Verify Delete
            videoModelList = VideoDAO.getAll(aDBS, videoURL=url)
            self.assertEqual(len(videoModelList), 0)
