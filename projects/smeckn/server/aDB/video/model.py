#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Video Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, Text, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger               = logging.getLogger(__name__)
MAX_VIDEO_URL_LENGTH = 4096


# ----------------------- VIDEO MODEL CLASS --------------------------------- #
class VideoModel(base):
    """
    Video Table
    """
    __tablename__       = "Video"
    smID                = Column(BigInteger, primary_key=True)   # ID
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    fromUser            = Column(Boolean, default=False)         # If True, video is directly from
                                                                 # user and did not originate from a profile
    imageURL            = Column(Text)
    videoURL            = Column(Text)
    heightPX            = Column(Integer, None)                  # Ex. 500
    widthPX             = Column(Integer, None)                  # Ex. 700
    lengthSec           = Column(Integer, None)                  # Ex. 120

    # Parent Facebook Post
    # 1 Facebook Post : 1 Video
    #
    # Field notes
    #  imageURL        - https://scontent.xx.fbcdn.net/v/t15.5256-10/p168x128/48549207_313296326193095_3923521292833128448_n.jpg?_nc_cat=103&_nc_ht=scontent.xx&oh=50e1453411368214aea4bce71c1d4af9&oe=5CCD4CD7
    #  videoURL        - Note: this exipires!!
    #                    https://scontent.xx.fbcdn.net/v/t42.1790-29/49725944_435383650331946_1071125295563538432_n.mp4?_nc_cat=100&efg=eyJybHIiOjYzMCwicmxhIjo1MTIsInZlbmNvZGVfdGFnIjoic2QifQ%3D%3D&rl=630&vabr=350&_nc_ht=scontent.xx&oh=c15e45bb7d3dac034ffa1944f6ad942f&oe=5C439917
    #  widthPX         - None
    #  heightPX        - None
    #  lengthSec       - 1239
    fbPostSMID          = Column(BigInteger,
                                 ForeignKey("FacebookPost.smID", onupdate="CASCADE", ondelete="CASCADE"))
    fbPostModel         = relationship("FacebookPostModel", back_populates="videoModel")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
