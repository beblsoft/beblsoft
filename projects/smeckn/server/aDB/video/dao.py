#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Video Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.video.model import VideoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- VIDEO DAO CLASS ----------------------------------- #
class VideoDAO():
    """
    Video Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        videoModel = VideoModel()
        for k, v in kwargs.items():
            setattr(videoModel, k, v)
        aDBS.add(videoModel)
        return videoModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(aDBS, videoModel):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.delete(videoModel)

    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(VideoModel).filter(VideoModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(VideoModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, videoModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(videoModel, k, v)
        aDBS.add(videoModel)
        return videoModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        videoModel = aDBS.query(VideoModel)\
            .filter(VideoModel.smID == smID)\
            .one_or_none()
        if not videoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return videoModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, videoURL=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(VideoModel)

        #Filters
        if videoURL:
            q = q.filter(VideoModel.videoURL == videoURL)

        return q.count() if count else q.all()
