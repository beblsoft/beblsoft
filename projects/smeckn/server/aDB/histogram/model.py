#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Histogram Model Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- HISTOGRAM DATA MODEL --------------------------- #
class HistogramDataModel():
    """
    Histogram Model
    """

    def __init__(self):  # pylint: disable=W0622
        """
        Initialize object

        Args
          groupByDate:
            GroupBy datetime
          groupByContentAttrType:
            GroupBy ContentAttrType
          count:
            Count of points in interval
            Ex. 30
          min:
            Minimum value of points in interval
            Ex. 3.33
          max:
            Maximum value of points in interval
            Ex. 20.11
          sum:
            Sum of points in interval
            Ex. 210
          average:
            Average value of points in interval
            Ex. 70
          stddev:
            Standard Deviation of points in interval
            Ex. 4.11

        Creation
          See HistogramDataDAO
        """
        self.groupByDate            = None
        self.groupByContentAttrType = None
        self.count                  = None
        self.min                    = None
        self.max                    = None
        self.sum                    = None
        self.average                = None
        self.stddev                 = None


# -------------------------- HISTOGRAM MODEL -------------------------------- #
class HistogramModel():
    """
    Histogram Model
    """

    def __init__(self):
        """
        Initialize object

        Args
          profileSMID:
            Profile Smeckn ID
            Ex. 1298
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          contentAttrType:
            ContentAttrType
            Ex. ContentAttrType.COMPREHEND_DOMINANT_SENTIMENT
          groupByType:
            GroupByType
            Ex. GroupByType.FACEBOOK_POST_CREATE_DATE
          groupByDateInterval:
            GroupByDateInterval
            Ex. GroupByDateInterval.MONTH
          intervalStartDate:
            Interval start date
          intervalEndDate:
            Interval end date
          dataModelList:
            HistogramDataModel List

        Creation
          See HistogramDAO
        """
        self.profileSMID         = None
        self.contentType         = None
        self.contentAttrType     = None
        self.groupByType         = None
        self.groupByDateInterval = None
        self.intervalStartDate   = None
        self.intervalEndDate     = None
        self.dataModelList       = []


# -------------------------- HISTOGRAM SUPPORT MODEL ------------------------ #
class HistogramSupportModel():
    """
    Histogram Support Model

    Used to determine if a histogram is supported for the arguments specified
    """
    def __init__(self, contentType, contentAttrType, groupByType):
        """
        Initialize object
        """
        self.contentType     = contentType
        self.contentAttrType = contentAttrType
        self.groupByType     = groupByType

    def __eq__(self, other):
        """
        Check for equality
        """
        argList = ["contentType", "contentAttrType", "groupByType"]
        equal   = True
        for arg in argList:
            if getattr(self, arg) != getattr(other, arg):
                equal = False
                break
        return equal
