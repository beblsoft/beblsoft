#!/usr/bin/env python3
"""
NAME
 stubDAO.py

DESCRIPTION
 Stub Histogram Data Access Object
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.histogram.dao import CommonHistogramDAO
from smeckn.server.aDB.histogram.dataDAO import HistogramDataDAO
from smeckn.server.aDB.groupBy.dateInterval import GroupByDateInterval
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.aDB.contentAttr.model import ContentAttrType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STUB HISTOGRAM DAO CLASS -------------------------- #
class StubHistogramDAO(CommonHistogramDAO):
    """
    Stub Histogram Data Access Object
    """
    count   = 75
    min     = 1
    max     = 17
    sum     = 1009
    average = 13.45
    stddev  = 3.2387

    @staticmethod
    @logFunc()
    def getList(histogramCtx):
        """
        Get HistogramModelList
        """
        startDate               = histogramCtx.intervalStartDate
        endDate                 = histogramCtx.intervalEndDate
        groupByType             = histogramCtx.groupByType
        histogramModelList      = []

        # GroupByResultType.CONTENT_ATTR_TYPE
        if groupByType == GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT:
            histogramModel      = StubHistogramDAO.createFromContext(histogramCtx=histogramCtx, groupByDateInterval=None)
            histogramDataModel  = HistogramDataDAO.create(groupByContentAttrType=ContentAttrType.COMP_SENT_TYPE_POSITIVE,
                                                          count=StubHistogramDAO.count)
            histogramModel.dataModelList.append(histogramDataModel)
            histogramModelList.append(histogramModel)

        # GroupByResultType.DATE
        elif groupByType == GroupByType.FACEBOOK_POST_CREATE_DATE:
            groupByDateInterval = GroupByDateInterval.getFromDateInterval(startDate=startDate, endDate=endDate)
            histogramModel      = StubHistogramDAO.createFromContext(histogramCtx=histogramCtx,
                                                                     groupByDateInterval=groupByDateInterval)
            firstDate           = groupByDateInterval.roundDown(date=startDate)
            histogramDataModel  = HistogramDataDAO.create(groupByDate=firstDate,
                                                          count=StubHistogramDAO.count,
                                                          min=StubHistogramDAO.min, max=StubHistogramDAO.max,
                                                          sum=StubHistogramDAO.sum, average=StubHistogramDAO.average,
                                                          stddev=StubHistogramDAO.stddev)
            histogramModel.dataModelList.append(histogramDataModel)
            StubHistogramDAO.zeroFillDates(histogramModel=histogramModel)
            histogramModelList.append(histogramModel)

        return histogramModelList
