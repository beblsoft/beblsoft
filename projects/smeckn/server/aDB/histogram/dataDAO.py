#!/usr/bin/env python3
"""
NAME
 dataDAO.py

DESCRIPTION
 Histogram Data Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.histogram.model import HistogramDataModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HISTOGRAM DATA DAO CLASS -------------------------- #
class HistogramDataDAO():
    """
    Histogram Data Data Access Object
    """
    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(**kwargs):
        """
        Create object
        """
        histogramDataModel = HistogramDataModel()
        return HistogramDataDAO.update(histogramDataModel=histogramDataModel, **kwargs)

    @staticmethod
    @logFunc()
    def createZeroFill(**kwargs):
        """
        Create object
        """
        histogramDataModel = HistogramDataModel()
        return HistogramDataDAO.update(histogramDataModel=histogramDataModel,
                                       count=0, sum=None, average=None, min=None, max=None, stddev=None,
                                       **kwargs)

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(histogramDataModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            assert k in histogramDataModel.__dict__
            setattr(histogramDataModel, k, v)
        return histogramDataModel
