#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Histogram Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import abc
import logging
from sqlalchemy import func
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.histogram.model import HistogramModel, HistogramSupportModel
from smeckn.server.aDB.histogram.dataDAO import HistogramDataDAO
from smeckn.server.aDB.contentAttr.common import CommonContentAttr
from smeckn.server.aDB.groupBy.common import CommonGroupBy
from smeckn.server.aDB.groupBy.model import GroupByResultType
from smeckn.server.aDB.groupBy.dateInterval import GroupByDateInterval


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- COMMON HISTOGRAM DAO CLASS ------------------------ #
class CommonHistogramDAO(abc.ABC):
    """
    Common Histogram Data Access Object
    """

    # ---------------------- ABSTRACT PROPERTIES ---------------------------- #
    # HistogramSupportModel list
    # Defines the attribute combinations that the class supports
    supportModelList = []

    # ---------------------- ABSTRACT METHODS ------------------------------- #
    @abc.abstractstaticmethod
    def getList(histogramCtx):  # pylint: disable=W0613
        """
        Main entry point for histogram generation, Wraps over get

        Allows client to map one ContentAttrType to multiple hisograms
        Ex. COMP_SENT_ANALYSIS_ALL_SCORE -> [COMP_SENT_ANALYSIS_POSITIVE_SCORE, COMP_SENT_ANALYSIS_NEGATIVE_SCORE, ...]

        Returns
          HistogramModelList
        """
        pass

    @staticmethod
    def getProfileSMIDColumn(histogramCtx):  # pylint: disable=W0613
        """
        Return profileSMID column
        Ex. FacebookPostModel.fbpSMID
        """
        return None

    @staticmethod
    def getCreateDateColumn(histogramCtx):  # pylint: disable=W0613
        """
        Return create date column
        Ex. FacebookPostModel.fbCreateDate
        """
        return None

    @staticmethod
    def getOuterJoinList(histogramCtx):  # pylint: disable=W0613
        """
        Return list of outer joins that will be used during query
        Ex. [(TextModel, FacebookPostModel.messageTextModel)]
        """
        return []

    @staticmethod
    def getFilterList(histogramCtx):  # pylint: disable=W0613
        """
        Return list of filters that will be applied to query
        Ex. FacebookPostModel.messageTextModel != None
        """
        return []

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(**kwargs):
        """
        Create object
        """
        histogramModel = HistogramModel()
        return CommonHistogramDAO.update(histogramModel=histogramModel, **kwargs)

    @staticmethod
    def createFromContext(histogramCtx, **kwargs):
        """
        Create object from context
        """
        return CommonHistogramDAO.create(
            profileSMID       = histogramCtx.profileSMID,
            contentType       = histogramCtx.contentType,
            groupByType       = histogramCtx.groupByType,
            contentAttrType   = histogramCtx.contentAttrType,
            intervalStartDate = histogramCtx.intervalStartDate,
            intervalEndDate   = histogramCtx.intervalEndDate,
            **kwargs)

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(histogramModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            assert k in histogramModel.__dict__
            setattr(histogramModel, k, v)
        return histogramModel

    # ---------------------- ZERO FILL DATES -------------------------------- #
    @staticmethod
    @logFunc()
    def zeroFillDates(histogramModel):
        """
        Zero Fill Histogram Dates
        """
        assert histogramModel.groupByDateInterval
        groupByDateInterval = histogramModel.groupByDateInterval
        startDate           = histogramModel.intervalStartDate
        endDate             = histogramModel.intervalEndDate
        firstDate           = groupByDateInterval.roundDown(date=startDate)
        lastDate            = groupByDateInterval.roundDown(date=endDate)
        curDate             = firstDate

        while curDate <= lastDate:
            matchList = [histogramDataModel
                         for histogramDataModel in histogramModel.dataModelList
                         if histogramDataModel.groupByDate == curDate]
            assert len(matchList) <= 1
            if not matchList:
                histogramDataModel = HistogramDataDAO.createZeroFill(groupByDate=curDate)
                histogramModel.dataModelList.append(histogramDataModel)
            curDate = groupByDateInterval.getNext(curDate=curDate)

        # Sort date in ascending order
        histogramModel.dataModelList.sort(key=lambda histogramDataModel: histogramDataModel.groupByDate,
                                          reverse=False)
        return histogramModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @classmethod
    @logFunc()
    def get(cls, histogramCtx):  # pylint: disable=W0221,R0914
        """
        Get object
        Args
          histogramCtx
        Returns
          HistogramModel
        """
        aDBS                = histogramCtx.aDBS
        contentType         = histogramCtx.contentType
        contentAttrType     = histogramCtx.contentAttrType
        contentAttrClass    = CommonContentAttr.getClassFromType(contentAttrType=contentAttrType)
        contentAttrColumn   = contentAttrClass.column
        startDate           = histogramCtx.intervalStartDate
        endDate             = histogramCtx.intervalEndDate
        groupByType         = histogramCtx.groupByType
        cls.verifySupported(contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType)
        groupByClass        = CommonGroupBy.getClassFromType(groupByType=groupByType)
        groupByResultType   = groupByClass.groupByResultType
        groupByDateInterval = GroupByDateInterval.getFromDateInterval(startDate=startDate, endDate=endDate)
        groupByDateInterval = groupByDateInterval if groupByResultType == GroupByResultType.DATE else None
        groupByExpression   = groupByClass.mapToMySQLExpression(groupByDateInterval=groupByDateInterval)
        profileSMIDColumn   = cls.getProfileSMIDColumn(histogramCtx=histogramCtx)  # pylint: disable=E1128
        createDateColumn    = cls.getCreateDateColumn(histogramCtx=histogramCtx)  # pylint: disable=E1128
        outerJoinList       = cls.getOuterJoinList(histogramCtx=histogramCtx)
        filterList          = cls.getFilterList(histogramCtx=histogramCtx)
        histogramModel      = CommonHistogramDAO.createFromContext(histogramCtx=histogramCtx,
                                                                   groupByDateInterval=groupByDateInterval)

        # Base query
        q = aDBS.query(groupByExpression, func.count(contentAttrColumn))
        if contentAttrClass.numeric:
            q = q.add_columns(func.min(contentAttrColumn), func.max(contentAttrColumn),
                              func.sum(contentAttrColumn), func.avg(contentAttrColumn),
                              func.stddev(contentAttrColumn))
        # Outer joins
        for oj in outerJoinList:
            q   = q.outerjoin(oj)

        # Filters
        for f in filterList:
            q   = q.filter(f)
        if profileSMIDColumn and histogramCtx.profileSMID:
            q   = q.filter(profileSMIDColumn == histogramCtx.profileSMID)
        if createDateColumn and startDate:
            q   = q.filter(createDateColumn >= startDate)
        if createDateColumn and endDate:
            q   = q.filter(createDateColumn <= endDate)

        # Group By
        q = q.group_by(groupByExpression)

        # Execute query
        # Decode groupByResult into its correct type
        # Populate dataModelList
        dataList = q.all()
        for data in dataList:
            if contentAttrClass.numeric:
                (groupByMySQLResult, count, min, max, sum, average, stddev) = data  # pylint: disable=W0622
            else:
                (groupByMySQLResult, count, min, max, sum, average, stddev) = (*data, None, None, None, None, None)

            groupByResult          = groupByClass.mapFromMySQLResult(result=groupByMySQLResult,
                                                                     groupByDateInterval=groupByDateInterval)
            groupByDate            = groupByResult if groupByResultType == GroupByResultType.DATE else None
            groupByContentAttrType = groupByResult if groupByResultType == GroupByResultType.CONTENT_ATTR_TYPE else None
            histogramDataModel     = HistogramDataDAO.create(groupByDate=groupByDate,
                                                             groupByContentAttrType=groupByContentAttrType,
                                                             count=count, min=min, max=max,
                                                             sum=sum, average=average,
                                                             stddev=stddev)
            histogramModel.dataModelList.append(histogramDataModel)

        # Zero filling
        if groupByDateInterval:
            cls.zeroFillDates(histogramModel=histogramModel)

        return histogramModel

    # ---------------------- VERIFY SUPPORTED ------------------------------- #
    @classmethod
    @logFunc()
    def verifySupported(cls, contentType, contentAttrType, groupByType):
        """
        Verify that contentType and contentAttrType are a supported combination
        """
        histogramSupportModel = HistogramSupportModel(contentType=contentType, contentAttrType=contentAttrType,
                                                      groupByType=groupByType)
        if histogramSupportModel not in cls.supportModelList:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_HISTOGRAM_NOTSUPPORTED,
                                extMsgFormatDict={"contentType": contentType.name,
                                                  "contentAttrType": contentAttrType.name,
                                                  "groupByType": groupByType.name})
