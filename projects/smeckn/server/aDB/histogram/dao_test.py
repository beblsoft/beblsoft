#!/usr/bin/env python3
"""
 NAME:
  dao_test.py

 DESCRIPTION
  Histogram DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.facebookPost.histogram import FacebookPostHistogramDAO
from smeckn.server.aDB.histogram.context import HistogramContext
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- HISTOGRAM DAO TEST CASE ---------------------- #
class HistogramDAOTestCase(CommonTestCase):
    """
    Test Histogram DAO
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)

    @logFunc()
    def test_zeroFillDates(self):
        """
        Test zero fill dates
        """
        startEndDateList = [
            (datetime.utcnow(), datetime.utcnow() + timedelta(seconds=60)),
            (datetime.utcnow(), datetime.utcnow() + timedelta(minutes=60)),
            (datetime.utcnow(), datetime.utcnow() + timedelta(hours=24)),
            (datetime.utcnow(), datetime.utcnow() + timedelta(days=100)),
            (datetime.utcnow(), datetime.utcnow() + timedelta(weeks=52)),
            (datetime.utcnow(), datetime.utcnow() + timedelta(weeks=52 * 3)),
        ]
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for (startDate, endDate) in startEndDateList:
                histogramCtx        = HistogramContext(mgr=self.mgr, aDBS=aDBS,
                                                       profileSMID=self.popFBPSMID,
                                                       contentType=ContentType.POST,
                                                       groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE,
                                                       contentAttrType=ContentAttrType.FACEBOOK_POST_NLIKES,
                                                       intervalStartDate=startDate,
                                                       intervalEndDate=endDate)
                histogramModel      = FacebookPostHistogramDAO.get(histogramCtx=histogramCtx)
                groupByDateInterval = histogramModel.groupByDateInterval
                firstDate           = groupByDateInterval.roundDown(date=startDate)
                lastDate            = groupByDateInterval.roundDown(date=endDate)
                curDate             = firstDate
                dateList            = [histogramDataModel.groupByDate
                                       for histogramDataModel in histogramModel.dataModelList]
                while curDate <= lastDate:
                    self.assertIn(curDate, dateList)
                    curDate = groupByDateInterval.getNext(curDate=curDate)
