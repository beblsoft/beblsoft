#!/usr/bin/env python3
"""
 NAME:
  context.py

 DESCRIPTION
  Histogram Context Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- HISTOGRAM CONTEXT ------------------------------ #
class HistogramContext():
    """
    Histogram Context Class
    """

    def __init__(self,
                 mgr,
                 aDBS,
                 profileSMID,
                 contentType,
                 contentAttrType,
                 groupByType,
                 intervalStartDate,
                 intervalEndDate):
        """
        Initialize object
        """
        self.mgr               = mgr
        self.aDBS              = aDBS
        self.profileSMID       = profileSMID
        self.contentType       = contentType
        self.contentAttrType   = contentAttrType
        self.groupByType       = groupByType
        self.intervalStartDate = intervalStartDate
        self.intervalEndDate   = intervalEndDate
