#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Content Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- CONTENT TYPE -------------------------------------- #
class ContentType(enum.Enum):
    """
    Content Type Enumeration
    """
    PROFILE                                    = 0  # 0 -  99 Basic Reserve
    PHOTO                                      = 1
    VIDEO                                      = 2
    MESSAGE                                    = 3
    LINK                                       = 4

    POST                                       = 100   # 100 - 199 Post Reserve
    POST_LINK                                  = 102
    POST_MESSAGE                               = 103
    POST_PHOTO                                 = 104
    POST_VIDEO                                 = 105

    TWEET                                      = 200   # 200 - 299 Tweet Reserve
    TWEET_TEXT                                 = 201
    TWEET_PHOTO                                = 202
    TWEET_VIDEO                                = 203
    TWEET_LINK                                 = 204
