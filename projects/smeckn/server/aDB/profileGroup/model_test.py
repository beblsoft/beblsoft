#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Profile Group Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class ProfileGroupModelTestCase(CommonTestCase):
    """
    Test Profile Group Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting a profile group
        """
        aID     = self.nonPopAccountID
        pgName  = "James"
        pgID    = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            # Create
            with AccountDatabase.lockTables(session=aDBS, lockStr="ProfileGroup WRITE"):
                pgModel = ProfileGroupDAO.create(aDBS=aDBS, name=pgName)
                aDBS.commit()
                self.assertEqual(pgModel.name, pgName)
                pgID = pgModel.id

            # Get
            pgModel = ProfileGroupDAO.getByID(aDBS=aDBS, pgID=pgID)
            self.assertEqual(pgModel.id, pgID)

            # Delete
            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=pgID)
            aDBS.commit()

            # Verify Delete
            pgModelList = ProfileGroupDAO.getAll(aDBS=aDBS)
            self.assertFalse(pgModelList)
