#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Profile Group Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import pprint
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.regex.bValidators import validateAlphaNumericDash
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.model import ProfileGroupModel
from smeckn.server.aDB.profileGroup.model import PG_NAME_MIN_LEN, PG_NAME_MAX_LEN
from smeckn.server.aDB.profileGroup.model import PG_MAX_COUNT
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PROFILE GROUP DAO CLASS --------------------------- #
class ProfileGroupDAO():
    """
    Profile Group Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, name):
        """
        Create object
        Args
          aDBS:
            Account Database Session
          name:
            Object Name
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="ProfileGroup WRITE")
        ProfileGroupDAO.validateCount(aDBS=aDBS, canCreate=True)
        ProfileGroupDAO.validateName(aDBS=aDBS, name=name)
        pgModel = ProfileGroupModel(name=name)
        aDBS.add(pgModel)
        return pgModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByID(aDBS, pgID):
        """
        Delete object
        """
        aDBS.query(ProfileGroupModel).filter(ProfileGroupModel.id == pgID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete object
        """
        aDBS.query(ProfileGroupModel).all()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def updateNameByID(aDBS, pgID, newName):
        """
        Update object name
        """
        pgModel = ProfileGroupDAO.getByID(aDBS=aDBS, pgID=pgID)
        if newName == pgModel.name:
            pass
        else:
            ProfileGroupDAO.validateName(aDBS=aDBS, name=newName)
            pgModel.name = newName
        return pgModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getByName(aDBS, name, raiseOnNone=True):
        """
        Retrieve object by name
        """
        pgModel = aDBS.query(ProfileGroupModel)\
            .filter(ProfileGroupModel.name == name).one_or_none()
        if not pgModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_GROUP_DOES_NOT_EXIST)
        return pgModel

    @staticmethod
    @logFunc()
    def getByID(aDBS, pgID, raiseOnNone=True):  # pylint: disable=W0622
        """
        Retrieve object by id
        """
        pgModel = aDBS.query(ProfileGroupModel)\
            .filter(ProfileGroupModel.id == pgID).one_or_none()
        if not pgModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_GROUP_DOES_NOT_EXIST)
        return pgModel

    @staticmethod
    @logFunc()
    def getAll(aDBS):
        """
        Return all objects
        """
        return aDBS.query(ProfileGroupModel).all()

    @staticmethod
    @logFunc()
    def getCount(aDBS):
        """
        Return count of all objects
        """
        return aDBS.query(ProfileGroupModel).count()

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(aDBS, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe all
        """
        pgModelList = ProfileGroupDAO.getAll(aDBS=aDBS)
        for pgModel in pgModelList:
            ProfileGroupDAO.describePGModel(aDBS=aDBS, pgModel=pgModel,
                                            tree=tree, startSpaces=startSpaces)

    @staticmethod
    @logFunc()
    def describePGModel(aDBS, pgModel, tree=True, startSpaces=0):  # pylint: disable=W0221 #pylint: disable=W0613
        """
        Describe Model
        """
        logger.info("{}{}".format(" " * startSpaces, pprint.pformat(pgModel)))
        if tree:
            FacebookProfileDAO.describeAll(aDBS=aDBS, pgID=pgModel.id,
                                           tree=tree, startSpaces=startSpaces + 2)

    # ---------------------- VALIDATION ------------------------------------- #
    @staticmethod
    @logFunc()
    def validateCount(aDBS, canCreate=False):
        """
        Validate ProfileGroup count

        Args
          canCreate:
            If True, Caller is trying to create object
            Check against future object count
        """
        validCount = PG_MAX_COUNT - 1 if canCreate else PG_MAX_COUNT
        tooMany    = validCount < ProfileGroupDAO.getCount(aDBS=aDBS)
        if tooMany:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_GROUP_EXCEED_MAX,
                                msg="Max={}, Valid={}".format(PG_MAX_COUNT, validCount))

    @staticmethod
    @logFunc()
    def validateName(aDBS, name):
        """
        Validate ProfileGroup name
        """
        # Check name length
        nameLen = len(name)
        badLen  = ((nameLen < PG_NAME_MIN_LEN) or (nameLen > PG_NAME_MAX_LEN))
        if badLen:
            raise BeblsoftError(
                code=BeblsoftErrorCode.PROFILE_GROUP_NAME_BAD_LEN,
                msg="name={} len={} min={} max={}".format(
                    name, nameLen, PG_NAME_MIN_LEN, PG_NAME_MAX_LEN))

        # Check for valid characters
        valid = validateAlphaNumericDash(string=name)
        if not valid:
            raise BeblsoftError(
                code=BeblsoftErrorCode.PROFILE_GROUP_NAME_INVALID,
                msg="name={}".format(name))

        # Check if name already exists
        alreadyExists = ProfileGroupDAO.getByName(aDBS, name, raiseOnNone=False) != None
        if alreadyExists:
            raise BeblsoftError(
                code=BeblsoftErrorCode.PROFILE_GROUP_NAME_EXISTS)
