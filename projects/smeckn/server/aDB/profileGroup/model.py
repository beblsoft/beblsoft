#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Profile Group Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from sqlalchemy import Column, String, BigInteger
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger          = logging.getLogger(__name__)
PG_MAX_COUNT    = 5   # Inclusive
PG_NAME_MIN_LEN = 1   # Inclusive
PG_NAME_MAX_LEN = 64  # Inclusive


# ----------------------- PROFILE GROUP MODEL CLASS ------------------------- #
class ProfileGroupModel(base):
    """
    Profile Group Table
    """
    __tablename__ = "ProfileGroup"
    id            = Column(BigInteger, primary_key=True)          # ID
    name          = Column(String(PG_NAME_MAX_LEN), unique=True)  # Ex. Johnny
    creationDate  = Column(DATETIME(fsp=6),
                           default=datetime.utcnow)               # Creation Date

    # 1 Profile Group : N Profiles
    profiles      = relationship("ProfileModel", back_populates="profileGroup",
                                 passive_deletes=True)

    def __repr__(self):
        return "[{} name={} id={}]".format(
            self.__class__.name, self.name, self.id)
