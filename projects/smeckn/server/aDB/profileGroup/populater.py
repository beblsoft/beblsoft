#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Profile Group Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import random
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PROFILE GROUP POPULATER CLASS --------------------- #
class ProfileGroupPopulater(CommonPopulater):
    """
    Profile Group Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, nPGs, maxRandInt=1000000):
        """
        Populate N profile groups

        Returns
          All Profile Group Model List
        """
        nameList = []
        for _ in range(0, nPGs):
            name = "{}-{}".format(forgery_py.name.first_name(), random.randint(0, maxRandInt))
            nameList.append(name)
        return self.populateList(aID=aID, nameList=nameList)

    @logFunc()
    def populateList(self, aID, nameList=[]):  # pylint: disable=W0102
        """
        Populate list
        Args
          aID:
            Account ID
          nameList:
            List of profile group names to add
        Returns
          All Profile Group Model List
        """
        pgModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="ProfileGroup WRITE"):
                for name in nameList:
                    ProfileGroupDAO.create(aDBS=aDBS, name=name)
                aDBS.commit()

            pgModelList = ProfileGroupDAO.getAll(aDBS=aDBS)
        return pgModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            ProfileGroupDAO.deleteAll(aDBS=aDBS)

    # ---------------------------- DESCRIBE --------------------------------- #
    @logFunc()
    def describeAllFromAID(self, aID, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            ProfileGroupDAO.describeAll(aDBS=aDBS, tree=tree,
                                        startSpaces=startSpaces)
