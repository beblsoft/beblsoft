#!/usr/bin/env python3
"""
NAME
 stubDAO.py

DESCRIPTION
 Stub Statistic Data Access Object
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.statistic.dao import CommonStatisticDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STUB STATISTIC DAO CLASS -------------------------- #
class StubStatisticDAO(CommonStatisticDAO):
    """
    Stub Statistic Data Access Object
    """
    count   = 75
    min     = 1
    max     = 17
    sum     = 1009
    average = 13.45
    stddev  = 3.2387

    @staticmethod
    @logFunc()
    def getList(statisticCtx):
        """
        Get StatisticModelList
        """
        statisticModel = StubStatisticDAO.createFromContext(
            statisticCtx=statisticCtx,
            count=StubStatisticDAO.count, min=StubStatisticDAO.min, max=StubStatisticDAO.max,
            sum=StubStatisticDAO.sum, average=StubStatisticDAO.average,
            stddev=StubStatisticDAO.stddev)
        return [statisticModel]
