#!/usr/bin/env python3
"""
 NAME:
  model.py

 DESCRIPTION
  Statistic Model Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- STATISTIC MODEL -------------------------------- #
class StatisticModel():
    """
    Statistic Model
    """

    def __init__(self):  # pylint: disable=W0622
        """
        Initialize object

        Args
          profileSMID:
            Profile Smeckn ID
            Ex. 1298
          contentType:
            Content Type
            Ex. ContentType.POST_MESSAGE
          contentAttrType:
            ContentAttrType
            Ex. ContentAttrType.COMPREHEND_DOMINANT_SENTIMENT
          intervalStartDate:
            Interval start date datetime
          intervalEndDate:
            Interval end date datetime
          count:
            Count of points in interval
            Ex. 30
          min:
            Minimum value of points in interval
            Ex. 3.33
          max:
            Maximum value of points in interval
            Ex. 20.11
          sum:
            Sum of points in interval
            Ex. 210
          average:
            Average value of points in interval
            Ex. 70
          stddev:
            Standard Deviation of points in interval
            Ex. 4.11

        Creation
          See StatisticDAO
        """
        self.profileSMID       = None
        self.contentType       = None
        self.contentAttrType   = None
        self.intervalStartDate = None
        self.intervalEndDate   = None
        self.count             = None
        self.min               = None
        self.max               = None
        self.sum               = None
        self.average           = None
        self.stddev            = None


# -------------------------- STATISTIC SUPPORT MODEL ------------------------ #
class StatisticSupportModel():
    """
    Statistic Support Model

    Used to determine statistic is supported for the arguments specified
    """

    def __init__(self, contentType, contentAttrType):
        """
        Initialize object
        """
        self.contentType     = contentType
        self.contentAttrType = contentAttrType

    def __eq__(self, other):
        """
        Check for equality
        """
        argList = ["contentType", "contentAttrType"]
        equal   = True
        for arg in argList:
            if getattr(self, arg) != getattr(other, arg):
                equal = False
                break
        return equal
