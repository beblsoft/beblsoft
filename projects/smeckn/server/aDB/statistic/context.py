#!/usr/bin/env python3
"""
 NAME:
  context.py

 DESCRIPTION
  Statistic Context Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- STATISTIC CONTEXT ------------------------------ #
class StatisticContext():
    """
    Statistic Context Class
    """

    def __init__(self,
                 mgr,
                 aDBS,
                 profileSMID,
                 contentType,
                 contentAttrType,
                 intervalStartDate = None,
                 intervalEndDate   = None):
        """
        Initialize object
        """
        self.mgr               = mgr
        self.aDBS              = aDBS
        self.profileSMID       = profileSMID
        self.contentType       = contentType
        self.contentAttrType   = contentAttrType
        self.intervalStartDate = intervalStartDate
        self.intervalEndDate   = intervalEndDate
