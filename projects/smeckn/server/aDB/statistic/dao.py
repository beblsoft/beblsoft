#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Statistic Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import abc
import logging
from sqlalchemy import func
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.statistic.model import StatisticModel, StatisticSupportModel
from smeckn.server.aDB.contentAttr.common import CommonContentAttr


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STATISTIC DAO CLASS ------------------------------- #
class CommonStatisticDAO(abc.ABC):
    """
    Common Statistic Data Access Object
    """

    # ---------------------- ABSTRACT PROPERTIES ---------------------------- #
    # StatisticSupportModel list
    # Defines the attribute combinations that the class supports
    supportModelList = []

    # ---------------------- ABSTRACT METHODS ------------------------------- #
    @abc.abstractstaticmethod
    def getList(statisticCtx): #pylint: disable=W0613
        """
        Main entry point for statistic generation, Wraps over get

        Allows client to map one ContentAttrType to multiple statistics
        Ex. COMP_SENT_ANALYSIS_ALL_SCORE -> [COMP_SENT_ANALYSIS_POSITIVE_SCORE, COMP_SENT_ANALYSIS_NEGATIVE_SCORE, ...]

        Returns
          StatisticModelList
        """
        pass

    @staticmethod
    def getProfileSMIDColumn(statisticCtx):  # pylint: disable=W0613
        """
        Return profileSMID column
        Ex. FacebookPostModel.fbpSMID
        """
        return None

    @staticmethod
    def getCreateDateColumn(statisticCtx):  # pylint: disable=W0613
        """
        Return create date column
        Ex. FacebookPostModel.fbCreateDate
        """
        return None

    @staticmethod
    def getOuterJoinList(statisticCtx):  # pylint: disable=W0613
        """
        Return list of outer joins that will be used during query
        Ex. [(TextModel, FacebookPostModel.messageTextModel)]
        """
        return []

    @staticmethod
    def getFilterList(statisticCtx):  # pylint: disable=W0613
        """
        Return list of filters that will be applied to query
        Ex. FacebookPostModel.messageTextModel != None
        """
        return []

    # ---------------------- CREATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def create(**kwargs):
        """
        Create object
        """
        statisticModel = StatisticModel()
        return CommonStatisticDAO.update(statisticModel=statisticModel, **kwargs)

    @staticmethod
    def createFromContext(statisticCtx, **kwargs):
        """
        Create object from context
        """
        return CommonStatisticDAO.create(
            profileSMID       = statisticCtx.profileSMID,
            contentType       = statisticCtx.contentType,
            contentAttrType   = statisticCtx.contentAttrType,
            intervalStartDate = statisticCtx.intervalStartDate,
            intervalEndDate   = statisticCtx.intervalEndDate,
            **kwargs)

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(statisticModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            assert k in statisticModel.__dict__
            setattr(statisticModel, k, v)
        return statisticModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @classmethod
    @logFunc()
    def get(cls, statisticCtx):  # pylint: disable=W0221
        """
        Get object
        Args
          statisticCtx:
            Statistic context

        Returns
          StatisticModel
        """
        aDBS              = statisticCtx.aDBS
        contentType       = statisticCtx.contentType
        contentAttrType   = statisticCtx.contentAttrType
        cls.verifySupported(contentType=contentType, contentAttrType=contentAttrType)
        contentAttrClass  = CommonContentAttr.getClassFromType(contentAttrType=contentAttrType)  # pylint: disable=E1128
        contentAttrColumn = contentAttrClass.column
        profileSMIDColumn = cls.getProfileSMIDColumn(statisticCtx=statisticCtx)  # pylint: disable=E1128
        createDateColumn  = cls.getCreateDateColumn(statisticCtx=statisticCtx)  # pylint: disable=E1128
        outerJoinList     = cls.getOuterJoinList(statisticCtx=statisticCtx)
        filterList        = cls.getFilterList(statisticCtx=statisticCtx)

        # Base query
        q  = aDBS.query(func.count(contentAttrColumn))

        if contentAttrClass.numeric:
            q  = q.add_columns(func.min(contentAttrColumn), func.max(contentAttrColumn),
                               func.sum(contentAttrColumn), func.avg(contentAttrColumn),
                               func.stddev(contentAttrColumn))

        # Outer joins
        for oj in outerJoinList:
            q   = q.outerjoin(oj)

        # Filters
        for f in filterList:
            q   = q.filter(f)
        if profileSMIDColumn and statisticCtx.profileSMID:
            q   = q.filter(profileSMIDColumn == statisticCtx.profileSMID)
        if createDateColumn and statisticCtx.intervalStartDate:
            q   = q.filter(createDateColumn >= statisticCtx.intervalStartDate)
        if createDateColumn and statisticCtx.intervalEndDate:
            q   = q.filter(createDateColumn <= statisticCtx.intervalEndDate)

        # Execute
        dataList = q.all()
        if contentAttrClass.numeric:
            (count, min, max, sum, average, stddev) = dataList[0]  # pylint: disable=W0622
        else:
            (count, min, max, sum, average, stddev) = (*dataList[0], None, None, None, None, None)

        statisticModel = CommonStatisticDAO.createFromContext(
            statisticCtx=statisticCtx, count=count, sum=sum, average=average,
            min=min, max=max, stddev=stddev)

        return statisticModel

    # ---------------------- VERIFY SUPPORTED ------------------------------- #
    @classmethod
    @logFunc()
    def verifySupported(cls, contentType, contentAttrType):
        """
        Verify that contentType and contentAttrType are a supported combination
        """
        statisticSupportModel = StatisticSupportModel(contentType=contentType, contentAttrType=contentAttrType)
        if statisticSupportModel not in cls.supportModelList:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_STATISTIC_CONTENT_NOTSUPPORTED,
                                extMsgFormatDict={"contentType": contentType.name,
                                                  "contentAttrType": contentAttrType.name})
