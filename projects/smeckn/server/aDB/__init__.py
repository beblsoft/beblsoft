#!/usr/bin/env python3
"""
 NAME
  __init__.py

 DESCRIPTION
  Account Database Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import re
import logging
import pprint
from pathlib import Path
from flask import current_app
from sqlalchemy.sql.expression import func
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.accountDBServer.model import AccountDBServerState
from smeckn.server.application.glob import ApplicationGlobal


# ----------------------------- GLOBALS ------------------------------------- #
logger   = logging.getLogger(__name__)
metaData = MetaData(naming_convention = {  # Constraint naming for migration
    # Index
    "ix": "ix_%(column_0_label)s",
    # Unique Constraint
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    # Check
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    # Foreign Key
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    # Primary Key
    "pk": "pk_%(table_name)s"
})
base     = declarative_base(metadata=metaData)


# ----------------------------- ACCOUNT DATABASE CLASS ---------------------- #
class AccountDatabase(BeblsoftMySQLDatabase):
    """
    Account Database Class
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        migrationsDir     = os.path.join(Path(__file__).parents[1], "aDBMigrations")
        alembicConfigPath = os.path.join(migrationsDir, "alembic.ini")
        alembicVersionDir = os.path.join(migrationsDir, "versions")
        super().__init__(*args, **kwargs, alembicConfigPath=alembicConfigPath,
                         alembicVersionDir=alembicVersionDir)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def aID(self):
        """
        Return account ID
        """
        match = re.match(r"(\D*)(\d*)$", self.name)
        aID   = int(match.group(2))
        return aID

    # ---------------------- ALTERNATE CONSTRUCTORS ------------------------- #
    @staticmethod
    @logFunc()
    def fromAModel(aModel, write=True, echo=False):
        """
        Alternate constructor
        Args
          aModel:
            Account Model
          write:
            If True, use write database server endpoint
          echo:
            If True, echo database commands
        """
        aDBSModel = aModel.accountDBServer
        aDBServer = AccountDatabase.serverFromADBSModel(aDBSModel, write=write)
        aDB       = AccountDatabase(
            bServer = aDBServer,
            name    = AccountDatabase.getDBName(aModel.dbNamePrefix, aModel.id),
            echo    = echo)
        return aDB

    @staticmethod
    @logFunc()
    def fromID(gDB, aID, write=True, echo=False):
        """
        Alternate constructor
        """
        aModel = None
        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=aID)
            return AccountDatabase.fromAModel(aModel=aModel, write=write, echo=echo)

    @staticmethod
    @logFunc()
    def fromEmail(gDB, email, write=True, echo=False):
        """
        Alternate constructor
        """
        aModel = None
        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=email)
            return AccountDatabase.fromAModel(aModel=aModel, write=write, echo=echo)

    @staticmethod
    @logFunc()
    def allFromADBSModel(aDBSModel, namePrefix, write=True):
        """
        Alternate constructor

        Returns
          List of all objects on account database server
        """
        aDBServer = AccountDatabase.serverFromADBSModel(aDBSModel, write=write)
        dbNames   = aDBServer.databases
        aDBList   = []
        for dbName in dbNames:
            if AccountDatabase.isAccountDBName(namePrefix, dbName):
                aDBList.append(AccountDatabase(
                    bServer = aDBServer,
                    name    = dbName))
        return aDBList

    @staticmethod
    @logFunc()
    def serverFromADBSModel(aDBSModel, write=True):
        """
        Return server object
        """
        aDBServer = None

        if aDBSModel.isOffline:
            raise BeblsoftError(code=BeblsoftErrorCode.DB_OFFLINE)
        elif aDBSModel.isOnlineRO and write:
            raise BeblsoftError(code=BeblsoftErrorCode.DB_ONLINE_RO)
        else:
            aDBServer = BeblsoftMySQLServer(
                domainNameFunc = lambda: aDBSModel.writeDomain if write else aDBSModel.readDomain,
                user           = aDBSModel.user,
                password       = aDBSModel.password,
                port           = aDBSModel.port)
        return aDBServer

    # ---------------------- HELPERS ---------------------------------------- #
    @staticmethod
    @logFunc()
    def getDBName(namePrefix, aID):
        """
        Return database name given account ID
        """
        return "{}{}".format(namePrefix, aID)

    @staticmethod
    @logFunc()
    def isAccountDBName(namePrefix, dbName):
        """
        Return True if database is an account database
        """
        pattern = r"^{}".format(namePrefix)
        return re.match(pattern, dbName) != None

    # ---------------------- CREATE/DELETE TABLES --------------------------- #
    @logFunc()
    def createTables(self, fromMigrations=True, checkFirst=True):
        """
        Create all database tables
        Args
          fromMigrations:
            If True, create database from migration scripts
          checkFirst:
            If True, don't issue CREATEs for tables already present
        """
        if fromMigrations:
            self.bDBMigrations.upgrade(toVersion="head")
        else:
            base.metadata.create_all(self.engine, checkfirst=checkFirst)
            self.bDBMigrations.stamp(version="head")

    @logFunc()
    def initializeTableData(self, aModel):  # pylint: disable=W0613
        """
        Create initial table data
        """
        with self.sessionScope() as aDBS:
            UnitLedgerDAO.createInitial(aDBS=aDBS)
            AccountInfoDAO.create(aDBS=aDBS, aID=aModel.id)
            aDBS.commit()

    @logFunc()
    def deleteTables(self, fromMigrations=False, checkFirst=True):
        """
        Destroy all database tables
        Args
          fromMigrations:
            If True, delete database from migration scripts
          checkFirst:
            If True, don't issue DELETE's for tables not present
        """
        if fromMigrations:
            self.bDBMigrations.downgrade(toVersion="base")
        else:
            base.metadata.drop_all(self.engine, checkfirst=checkFirst)
            self.bDBMigrations.stamp(version="base")

    # ---------------------- SESSION SCOPE ---------------------------------- #
    @staticmethod
    @logFunc()
    def sessionScopeFromAModel(aModel, write=True, commit=True):
        """
        Return session scope from account database server model
        Args
          aModel:
            Account Model
          write:
            If True, use database write endpoint
          commit:
            If True, commit DB session after it completes
        """
        aDB = AccountDatabase.fromAModel(aModel, write=write)
        return aDB.sessionScope(commit=commit)

    @staticmethod
    @logFunc()
    def sessionScopeFromAID(gDB, aID, write=True, commit=True):
        """
        Return session scope from account ID
        Args
          gDB:
            Global Database
          aID:
            Account ID
        """
        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=aID)
            return AccountDatabase.sessionScopeFromAModel(aModel=aModel, write=write, commit=commit)

    @staticmethod
    @logFunc()
    def sessionScopeFromFlask(write=True, commit=True):
        """
        Return session scope flask application state

        Requires the following flask variables be set:
          current_app.cfg
            ADB_PORT
            ADB_NAME_PREFIX
            ADB_USER
            ADB_PASSWORD
            ADB_ECHO
          ApplicationGlobal.getAccountAD
            aID
            readDB
            writeDB
        """
        cfg       = current_app.cfg
        accountAD = ApplicationGlobal.getAccountAD()
        aDB       = AccountDatabase.fromAModel
        aDBServer = BeblsoftMySQLServer(
            domainNameFunc = lambda: accountAD.writeDB if write else accountAD.readDB,
            user           = cfg.ADB_USER,
            password       = cfg.ADB_PASSWORD,
            port           = cfg.ADB_PORT)
        aDB       = AccountDatabase(
            bServer        = aDBServer,
            name           = AccountDatabase.getDBName(cfg.ADB_NAME_PREFIX, accountAD.aID),
            echo           = cfg.ADB_ECHO)
        return aDB.sessionScope(commit=commit)

    @staticmethod
    @logFunc()
    def sessionScopeFromAJob(aJob, write=True, commit=True):
        """
        Return session scope from account job
        """
        cfg       = aJob.cfg
        aDBServer = BeblsoftMySQLServer(
            domainNameFunc = lambda: aJob.aDBSWriteDomain if write else aJob.aDBSReadDomain,
            user           = cfg.ADB_USER,
            password       = cfg.ADB_PASSWORD,
            port           = cfg.ADB_PORT)
        aDB       = AccountDatabase(
            bServer = aDBServer,
            name    = AccountDatabase.getDBName(cfg.ADB_NAME_PREFIX, aJob.aID),
            echo    = cfg.ADB_ECHO)
        return aDB.sessionScope(commit=commit)


# ----------------------------- MODEL, DAO IMPORTS -------------------------- #
# Import all models before DAOs to avoid import errors
from smeckn.server.aDB.accountInfo.model import AccountInfoModel  # pylint: disable=C0413
from smeckn.server.aDB.profileGroup.model import ProfileGroupModel  # pylint: disable=C0413
from smeckn.server.aDB.profile.model import ProfileModel  # pylint: disable=C0413
from smeckn.server.aDB.sync.model import SyncModel  # pylint: disable=C0413
from smeckn.server.aDB.syncContent.model import SyncContentModel  # pylint: disable=C0413
from smeckn.server.aDB.analysis.model import AnalysisModel  # pylint: disable=C0413
from smeckn.server.aDB.facebookProfile.model import FacebookProfileModel  # pylint: disable=C0413
from smeckn.server.aDB.facebookPost.model import FacebookPostModel  # pylint: disable=C0413
from smeckn.server.aDB.facebookPhoto.model import FacebookPhotoModel  # pylint: disable=C0413
from smeckn.server.aDB.aJob.model import AccountJobModel  # pylint: disable=C0413
from smeckn.server.aDB.video.model import VideoModel  # pylint: disable=C0413
from smeckn.server.aDB.link.model import LinkModel  # pylint: disable=C0413
from smeckn.server.aDB.text.model import TextModel  # pylint: disable=C0413
from smeckn.server.aDB.photo.model import PhotoModel  # pylint: disable=C0413
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel  # pylint: disable=C0413
from smeckn.server.aDB.compLang.model import CompLangModel  # pylint: disable=C0413
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel  # pylint: disable=C0413
from smeckn.server.aDB.unitLedger.model import UnitLedgerModel  # pylint: disable=C0413
# from smeckn.server.aDB..model import Model  # pylint: disable=C0413

from smeckn.server.aDB.accountInfo.dao import AccountInfoDAO  # pylint: disable=C0413
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO  # pylint: disable=C0413
from smeckn.server.aDB.profile.dao import ProfileDAO  # pylint: disable=C0413
from smeckn.server.aDB.sync.dao import SyncDAO  # pylint: disable=C0413
from smeckn.server.aDB.syncContent.dao import SyncContentDAO  # pylint: disable=C0413
from smeckn.server.aDB.analysis.dao import AnalysisDAO  # pylint: disable=C0413
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO  # pylint: disable=C0413
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO  # pylint: disable=C0413
from smeckn.server.aDB.facebookPhoto.dao import FacebookPhotoDAO  # pylint: disable=C0413
from smeckn.server.aDB.aJob.dao import AccountJobDAO  # pylint: disable=C0413
from smeckn.server.aDB.video.dao import VideoDAO  # pylint: disable=C0413
from smeckn.server.aDB.link.dao import LinkDAO  # pylint: disable=C0413
from smeckn.server.aDB.text.dao import TextDAO  # pylint: disable=C0413
from smeckn.server.aDB.photo.dao import PhotoDAO  # pylint: disable=C0413
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO  # pylint: disable=C0413
from smeckn.server.aDB.compLang.dao import CompLangDAO  # pylint: disable=C0413
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO  # pylint: disable=C0413
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO  # pylint: disable=C0413
# from smeckn.server.aDB..dao import DAO  # pylint: disable=C0413
