#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Facebook Photo Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookPhoto.dao import FacebookPhotoDAO
from smeckn.server.aDB.photo.dao import PhotoDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK PHOTO POPULATER CLASS -------------------- #
class FacebookPhotoPopulater(CommonPopulater):
    """
    Facebook Photo Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, aID, fbpSMID, fbIDList=[]):  # pylint: disable=W0102
        """
        Populate list

        Returns
          Facebook Photo Model List
        """
        fbPhotoModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for fbID in fbIDList:
                url          = "https://www.example.com/{}.png".format(forgery_py.basic.text())
                name         = forgery_py.lorem_ipsum.paragraph()
                fbPhotoModel = FacebookPhotoDAO.create(aDBS=aDBS, fbpSMID=fbpSMID, fbID=fbID, name=name)
                aDBS.commit()
                PhotoDAO.create(aDBS=aDBS, fbPhotoSMID=fbPhotoModel.smID,
                                imageURL=url, heightPX=500, widthPX=500)

            fbPhotoModelList = FacebookPhotoDAO.getAll(aDBS=aDBS, fbpSMID=fbpSMID)
        return fbPhotoModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            FacebookPhotoDAO.deleteAll(aDBS=aDBS)
