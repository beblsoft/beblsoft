#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Facebook Photo Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.aDB.facebookPhoto.dao import FacebookPhotoDAO
from smeckn.server.aDB.photo.dao import PhotoDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class FacebookPhotoModelTestCase(CommonTestCase):
    """
    Test Facebook Photo Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True,
                      facebookPhotos=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        fbpSMID = self.popFBPSMID
        fbID    = 129812731928371
        name    = "Facebook Photo!"

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            FacebookPhotoDAO.create(aDBS, fbpSMID=fbpSMID, fbID=fbID, name=name)
            aDBS.commit()

            # Get
            fbPhotoModel = FacebookPhotoDAO.getByFBID(aDBS, fbID=fbID)
            self.assertEqual(fbPhotoModel.fbID, fbID)
            self.assertEqual(fbPhotoModel.name, name)

            # Delete
            FacebookPhotoDAO.deleteBySMID(aDBS=aDBS, smID=fbPhotoModel.smID)
            aDBS.commit()

            # Verify Delete
            fbPhotoModel = FacebookPhotoDAO.getByFBID(aDBS, fbID=fbID, raiseOnNone=False)
            self.assertFalse(fbPhotoModel)

    @logFunc()
    def test_nullFBPSMID(self):
        """
        Test trying to create facebook photo with NULL fbpSMID
        """
        fbID = 129812731928371

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                FacebookPhotoDAO.create(aDBS, fbpSMID=None, fbID=fbID)
                aDBS.commit()
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created photo with null fbpSMID")

    @logFunc()
    def test_badFBPSMID(self):
        """
        Test trying to create facebook photo with bad fbpSMID
        """
        fbID     = 129812731928371
        fbpSMID  = 23948230948

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                FacebookPhotoDAO.create(aDBS, fbpSMID=fbpSMID, fbID=fbID)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created photo with invalid fbpSMID")

    @logFunc()
    def test_pgCascadeDelete(self):
        """
        Test profile group deletions cascade down appropriately to delete the
        following objects:
          - Facebook Photos
          - Photos
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbPhotoModelList = FacebookPhotoDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            photoModelList   = PhotoDAO.getAll(aDBS=aDBS)
            self.assertGreater(len(fbPhotoModelList), 0)
            self.assertGreater(len(photoModelList), 0)

            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=self.popPGID)
            aDBS.commit()

            fbPhotoModelList = FacebookPhotoDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            photoModelList   = PhotoDAO.getAll(aDBS=aDBS)
            self.assertFalse(fbPhotoModelList)
            self.assertFalse(photoModelList)
