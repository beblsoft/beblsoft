#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Facebook Photo Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from sqlalchemy import Column, BigInteger, ForeignKey, Text
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger               = logging.getLogger(__name__)
MAX_FBPHOTO_NAME_LEN = 2048


# ----------------------- FACEBOOK PHOTO MODEL CLASS ------------------------- #
class FacebookPhotoModel(base):
    """
    Facebook Photo Table
    """
    __tablename__       = "FacebookPhoto"
    smID                = Column(BigInteger, primary_key=True)  # ID
    syncDate            = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)       # Ex. "2000-01-04 07:53:55.474912"

    # Facebook Data
    fbID                = Column(BigInteger, unique=True)       # Facebook ID
    fbCreateDate        = Column(DATETIME(fsp=6))               # Ex. "2000-01-04 07:53:55.474912"
    fbUpdateDate        = Column(DATETIME(fsp=6))               # Ex. "2000-01-04 07:53:55.474912"
    name                = Column(Text, default=None)            # Ex. "Harry throwing a football"
    nLikes              = Column(BigInteger)                    # Ex. 5
    nComments           = Column(BigInteger)                    # Ex. 6
    nReactions          = Column(BigInteger)                    # Ex. 7

    # Parent Facebook Profile
    # 1 Facebook Profile : N Facebook Profile Photos
    fbpSMID             = Column(BigInteger,
                                 ForeignKey("FacebookProfile.smID", onupdate="CASCADE", ondelete="CASCADE"),
                                 nullable=False)
    fbpModel            = relationship("FacebookProfileModel", back_populates="photoModelList")

    # Child Photo
    # 1 Facebook Photo: 1 Photo
    photoModel          = relationship("PhotoModel", back_populates="fbPhotoModel",
                                       uselist=False, passive_deletes=True)

    def __repr__(self):
        return "[{} smID={} fbID={}]".format(
            self.__class__.__name__, self.smID, self.fbID)
