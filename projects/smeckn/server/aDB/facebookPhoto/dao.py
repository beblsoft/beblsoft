#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Facebook Photo Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.facebookPhoto.model import FacebookPhotoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK PHOTO DAO CLASS -------------------------- #
class FacebookPhotoDAO():
    """
    Facebook Photo Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, fbpSMID, **kwargs):  # pylint: disable=W0221
        """
        Create object
        Args
          aDBS:
            Account Database Session
          smFBPID:
            Smeckn Facebook Profile ID
        """
        fbPhotoModel = FacebookPhotoModel(fbpSMID=fbpSMID)
        for k, v in kwargs.items():
            setattr(fbPhotoModel, k, v)
        aDBS.add(fbPhotoModel)
        return fbPhotoModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(FacebookPhotoModel).filter(FacebookPhotoModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(FacebookPhotoModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, fbPhotoModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(fbPhotoModel, k, v)
        aDBS.add(fbPhotoModel)
        return fbPhotoModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        fbPhotoModel = aDBS.query(FacebookPhotoModel)\
            .filter(FacebookPhotoModel.smID == smID)\
            .one_or_none()
        if not fbPhotoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbPhotoModel

    @staticmethod
    @logFunc()
    def getByFBID(aDBS, fbID, raiseOnNone=True):  # pylint: disable=W0622
        """
        Retrieve object by facebook id
        """
        fbPhotoModel = aDBS.query(FacebookPhotoModel)\
            .filter(FacebookPhotoModel.fbID == fbID)\
            .one_or_none()
        if not fbPhotoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbPhotoModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, fbpSMID=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        Args
          smFBPID:
            If specifed, return photos for smeckn facebook profile id
        """
        q = aDBS.query(FacebookPhotoModel)

        # Filters
        if fbpSMID:
            q = q.filter(FacebookPhotoModel.fbpSMID == fbpSMID)

        return q.count() if count else q.all()
