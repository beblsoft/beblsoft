#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Sync Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, Enum
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.profile.model import ProfileType


# ----------------------- GLOBALS ------------------------------------------- #
logger                       = logging.getLogger(__name__)
# Ex. Max 10 Facebook profiles can be synced per day (includes adds/deletes)
SYNC_DAILY_PROFILE_THRESHOLD = 10


# ----------------------- ENUMERATIONS -------------------------------------- #
class SyncStatus(enum.Enum):
    """
    Sync Status Enumeration
    """
    IN_PROGRESS             = 0
    SUCCESS                 = 1
    CONTENT_LIMIT_REACHED   = 2
    CREDENTIALS_EXPIRED     = 3
    THIRDPARTY_API_FAILURE  = 4
    PROFILE_DELETED         = 5
    TIMED_OUT               = 6

    @property
    def isSuccessfullCompletion(self):  # pylint: disable=C0111
        return self in [SyncStatus.SUCCESS, SyncStatus.CONTENT_LIMIT_REACHED]

    @property
    def isInProgress(self):  # pylint: disable=C0111
        return self == SyncStatus.IN_PROGRESS

    @staticmethod
    def getHighestPriority(statusList):
        """
        Return highest priority in list
        The higher priority, the more likely content status bubbles up from content to sync
        """
        priorityStatusList = [
            SyncStatus.CREDENTIALS_EXPIRED,     # Highest
            SyncStatus.THIRDPARTY_API_FAILURE,
            SyncStatus.TIMED_OUT,
            SyncStatus.PROFILE_DELETED,
            SyncStatus.CONTENT_LIMIT_REACHED,
            SyncStatus.SUCCESS,
            SyncStatus.IN_PROGRESS,             # Lowest
        ]
        rval = None
        for status in priorityStatusList:
            if status in statusList:
                rval = status
                break
        assert rval
        return rval


# ----------------------- SYNC MODEL CLASS ---------------------------------- #
class SyncModel(base):
    """
    Sync Table
    """
    __tablename__          = "Sync"
    smID                   = Column(BigInteger, primary_key=True)
    createDate             = Column(DATETIME(fsp=6),
                                    default=datetime.utcnow)    # Ex. "2000-01-04 07:53:55.474912"
    completeDate           = Column(DATETIME(fsp=6))            # Date completed
    restartDate            = Column(DATETIME(fsp=6))            # Date after which next profile sync can begin

    status                 = Column(Enum(SyncStatus))           # Ex. SyncStatus.IN_PROGRESS
    errorSeen              = Column(Boolean, default=False)     # If True, client has seen error

    # Parent Profile
    # 1 Profile : N Sync
    # Note: no cascade delete here. We need syncs to stay around to ensure an attacker cannot
    # keep syncing with third parties and exhaust smeckn's third party API usage
    profileSMID            = Column(BigInteger,
                                    ForeignKey("Profile.smID", onupdate="SET NULL", ondelete="SET NULL"))
    profileModel           = relationship("ProfileModel", back_populates="syncModelList")
    profileType            = Column(Enum(ProfileType))

    # Child Sync Content
    # 1 Sync : N SyncContent
    syncContentModelList   = relationship("SyncContentModel", back_populates="syncModel",
                                          passive_deletes=True)

    def __repr__(self):
        return "[{} smID={} status={}]".format(
            self.__class__.__name__, self.smID, self.status)
