#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Sync Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
from datetime import datetime, timedelta
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.sync.model import SyncStatus, SYNC_DAILY_PROFILE_THRESHOLD
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.analysis.model import AnalysisStatus, AnalysisType
from smeckn.server.aDB.contentType.model import ContentType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SyncModelTestCase(CommonTestCase):
    """
    Test Sync Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True, syncs=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        status      = SyncStatus.IN_PROGRESS
        profileSMID = self.nonPopProfileSMID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                syncModel = SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID, status=status)

            # Get
            syncModelList = SyncDAO.getAll(aDBS, profileSMID=profileSMID)
            self.assertEqual(len(syncModelList), 1)
            syncModel = syncModelList[0]
            self.assertEqual(syncModel.status, status)

            # Delete
            SyncDAO.deleteBySMID(aDBS=aDBS, smID=syncModel.smID)
            aDBS.commit()

            # Verify Delete
            syncModelList = SyncDAO.getAll(aDBS, profileSMID=profileSMID)
            self.assertEqual(len(syncModelList), 0)

    @logFunc()
    def test_profileCascadeUpdate(self):
        """
        Test profile deletion cascades but doesn't delete sync
        """
        profileSMID = self.popProfileSMID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncModelList = SyncDAO.getAll(aDBS)
            self.assertGreater(len(syncModelList), 0)

            # Delete profile
            ProfileDAO.deleteBySMID(aDBS=aDBS, smID=profileSMID)
            aDBS.commit()

            # Verify Sync Still Present, but foreign key set to NULL
            syncModelList = SyncDAO.getAll(aDBS)
            self.assertGreater(len(syncModelList), 0)
            syncModel = syncModelList[0]
            self.assertEqual(syncModel.profileSMID, None)

    @logFunc()
    def test_twoInProgress(self):
        """
        Test trying to create two in progress syncs
        """
        profileSMID     = self.nonPopProfileSMID
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID,
                                   status=SyncStatus.IN_PROGRESS)
                    aDBS.commit()
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID,
                                   status=SyncStatus.IN_PROGRESS)
                    aDBS.commit()
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.SYNC_ALREADY_IN_PROGRESS)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Able to create two in progress syncs")

    @logFunc()
    def test_restartable(self):
        """
        Test sync restarts
        """
        profileSMID     = self.nonPopProfileSMID
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID,
                                   restartDate = datetime.utcnow() + timedelta(days=1),
                                   status=SyncStatus.SUCCESS)
                    aDBS.commit()
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID,
                                   status=SyncStatus.IN_PROGRESS)
                    aDBS.commit()
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.SYNC_NOT_RESTARTABLE)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Was able to create sync before allowable restart")

    @logFunc()
    def test_dailyThreshold(self):
        """
        Test sync daily creation threshold
        """
        pgID            = self.nonPopPGID
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for _ in range(SYNC_DAILY_PROFILE_THRESHOLD + 1):
                try:
                    profileModel = ProfileDAO.create(aDBS=aDBS, profileGroupID=pgID)
                    aDBS.commit()
                    with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                        SyncDAO.create(aDBS=aDBS, profileSMID=profileModel.smID, status=SyncStatus.SUCCESS)
                    ProfileDAO.deleteBySMID(aDBS, smID=profileModel.smID)
                    aDBS.commit()
                except BeblsoftError as e:
                    self.assertEqual(e.code, BeblsoftErrorCode.SYNC_DAILY_THRESHOLD_REACHED)
                    exceptionRaised = True
                    break
            if not exceptionRaised:
                raise Exception("Was able to create more than sync daily threshold")

    @logFunc()
    def test_syncDuringAnalysis(self):
        """
        Test trying to create a sync while an analysis is in progress
        """
        profileSMID     = self.nonPopProfileSMID
        contentType     = ContentType.POST_MESSAGE
        analysisType    = AnalysisType.COMPREHEND_SENTIMENT
        status          = AnalysisStatus.IN_PROGRESS
        exceptionRaised = False

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync READ, Analysis WRITE"):
                    AnalysisDAO.create(aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
                                       analysisType=analysisType, status=status)
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                    SyncDAO.create(aDBS=aDBS, profileSMID=profileSMID,
                                   status=SyncStatus.IN_PROGRESS)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.SYNC_NOT_ALLOWED_WITH_ANALYSIS)
                exceptionRaised = True
            if not exceptionRaised:
                raise Exception("Able to create sync while analysis in progress")
