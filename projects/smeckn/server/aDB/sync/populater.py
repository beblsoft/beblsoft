#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Sync Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.dao import ProfileDAO
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.sync.model import SyncStatus


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- SYNC POPULATER CLASS ------------------------------ #
class SyncPopulater(CommonPopulater):
    """
    Sync Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateOne(self, aID, profileSMID, status=SyncStatus.IN_PROGRESS,
                    createDate=None):  # pylint: disable=W0102
        """
        Populate One

        Returns
          SyncModel
        """
        syncModel = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                syncModel = SyncDAO.create(
                    aDBS        = aDBS,
                    profileSMID = profileSMID,
                    status      = status,
                    createDate  = createDate if createDate else datetime.utcnow())
            aDBS.refresh(syncModel)
        return syncModel

    @logFunc()
    def populateForAllProfiles(self, aID, status=SyncStatus.SUCCESS, createDate=None, restartDelta=None):
        """
        Populate one sync for each profile

        Returns
          SyncModelList
        """
        syncModelList = []

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            # Create syncs
            profileModelList = ProfileDAO.getAll(aDBS=aDBS)
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                for profileModel in profileModelList:
                    createDate  = createDate if createDate else datetime.utcnow()
                    restartDate = createDate + restartDelta if restartDelta else createDate + timedelta(days=3)
                    syncModel = SyncDAO.create(
                        aDBS        = aDBS,
                        profileSMID = profileModel.smID,
                        status      = status,
                        createDate  = createDate,
                        restartDate = restartDate)
                    syncModelList.append(syncModel)

            # Refresh syncs
            for syncModel in syncModelList:
                aDBS.refresh(syncModel)
        return syncModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            SyncDAO.deleteAll(aDBS=aDBS)
