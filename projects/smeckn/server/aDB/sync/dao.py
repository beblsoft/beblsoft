#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Sync Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime, timedelta
import logging
from sqlalchemy import desc, asc
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.model import SyncModel, SyncStatus, SYNC_DAILY_PROFILE_THRESHOLD
from smeckn.server.aDB.profile.dao import ProfileDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- SYNC DAO CLASS ------------------------------------ #
class SyncDAO():
    """
    Sync Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, profileSMID, status=SyncStatus.IN_PROGRESS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ")
        # Validate sync can be created
        SyncDAO.validateCreateNoProfileSyncInProgress(aDBS, profileSMID=profileSMID)
        SyncDAO.validateCreateSyncIsRestartable(aDBS, profileSMID=profileSMID)
        SyncDAO.validateCreateDailyThreshold(aDBS, profileSMID=profileSMID)
        SyncDAO.validateCreateNoProfileAnalysisInProgress(aDBS, profileSMID=profileSMID)

        # Create Sync
        profileModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=profileSMID)
        syncModel    = SyncModel(status=status,
                                 profileSMID=profileSMID,
                                 profileType=profileModel.type)
        if "createDate" not in kwargs:
            kwargs["createDate"] = datetime.utcnow()
        for k, v in kwargs.items():
            setattr(syncModel, k, v)
        aDBS.add(syncModel)
        return syncModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(SyncModel).filter(SyncModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(SyncModel).delete()

    @staticmethod
    @logFunc()
    def deleteOld(aDBS, createdBeforeNowDelta=timedelta(days=30)):
        """
        Delete old syncs
        """
        createdBeforeDate = datetime.utcnow() - createdBeforeNowDelta
        aDBS.query(SyncModel)\
            .filter(SyncModel.createDate < createdBeforeDate)\
            .delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, syncModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(syncModel, k, v)
        aDBS.add(syncModel)
        return syncModel

    @staticmethod
    @logFunc()
    def handleProfileCredentialUpdate(aDBS, profileSMID):
        """
        Handle profile credential update

        Find the last profile sync that occurred.
        If if failed with SyncStatus.CREDENTIALS_EXPIRED, update the restart time to now
        """
        syncModel = SyncDAO.getAll(aDBS=aDBS, profileSMID=profileSMID, orderCreateDateDesc=True, first=True)
        if syncModel and syncModel.status == SyncStatus.CREDENTIALS_EXPIRED:
            SyncDAO.update(aDBS=aDBS, syncModel=syncModel, restartDate=datetime.utcnow())

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        syncModel = aDBS.query(SyncModel)\
            .filter(SyncModel.smID == smID)\
            .one_or_none()
        if not syncModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return syncModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, profileSMID=None, status=None,
               restartDateGreaterThan=None, profileType=None,
               createdAfterDate=None,
               orderCreateDateDesc=False, orderCreateDateAsc=False,
               first=False, count=False,):  # pylint: disable=W0221
        """
        Return all objects
        """
        rval = None
        q    = aDBS.query(SyncModel)

        # Filters
        if profileSMID:
            q = q.filter(SyncModel.profileSMID == profileSMID)
        if status:
            q = q.filter(SyncModel.status == status)
        if restartDateGreaterThan:
            q = q.filter(SyncModel.restartDate >= restartDateGreaterThan)
        if profileType:
            q = q.filter(SyncModel.profileType == profileType)
        if createdAfterDate:
            q = q.filter(SyncModel.createDate >= createdAfterDate)

        # Order
        if orderCreateDateDesc:
            q = q.order_by(desc(SyncModel.createDate))
        if orderCreateDateAsc:
            q = q.order_by(asc(SyncModel.createDate))

        # First, count, or all
        if first:
            rval = q.first()
        elif count:
            rval = q.count()
        else:
            rval = q.all()

        return rval

    # ---------------------- VALIDATION ------------------------------------- #
    @staticmethod
    @logFunc()
    def validateCreateNoProfileSyncInProgress(aDBS, profileSMID):
        """
        Validate sync creation status is allowable profile currently doesn't have a sync in progress
        """
        syncModelList = SyncDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                       status=SyncStatus.IN_PROGRESS)
        if syncModelList:
            raise BeblsoftError(BeblsoftErrorCode.SYNC_ALREADY_IN_PROGRESS)

    @staticmethod
    @logFunc()
    def validateCreateSyncIsRestartable(aDBS, profileSMID):
        """
        Validate that profile can restart sync
        """
        syncModelList = SyncDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                       restartDateGreaterThan=datetime.utcnow())
        if syncModelList:
            raise BeblsoftError(BeblsoftErrorCode.SYNC_NOT_RESTARTABLE)

    @staticmethod
    @logFunc()
    def validateCreateDailyThreshold(aDBS, profileSMID):
        """
        Validate that profile type hasn't exceeded max syncs per day
        This accounts for profiles being created and deleted
        """
        profileModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=profileSMID)
        nSyncsToday  = SyncDAO.getAll(aDBS=aDBS, profileType=profileModel.type,
                                      createdAfterDate=datetime.utcnow() - timedelta(days=1),
                                      count=True)
        if nSyncsToday >= SYNC_DAILY_PROFILE_THRESHOLD:
            raise BeblsoftError(BeblsoftErrorCode.SYNC_DAILY_THRESHOLD_REACHED)

    @staticmethod
    @logFunc()
    def validateCreateNoProfileAnalysisInProgress(aDBS, profileSMID):
        """
        Validate that profile currently doesn't have analysis in progress
        """
        from smeckn.server.aDB.analysis.dao import AnalysisDAO
        from smeckn.server.aDB.analysis.model import AnalysisStatus

        analysisModelList = AnalysisDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                               status=AnalysisStatus.IN_PROGRESS)
        if analysisModelList:
            raise BeblsoftError(BeblsoftErrorCode.SYNC_NOT_ALLOWED_WITH_ANALYSIS)

    # ---------------------- LOCKING ---------------------------------------- #
    @staticmethod
    @logFunc()
    def lockSMID(aDBS, smID, update=True):
        """
        Lock row
        Args
          update:
            If True, lock row for update
            Else, lock for read
        """
        aDBS.execute("SELECT * from Sync where smID={} {}".format(
            smID, "FOR UPDATE" if update else "LOCK IN SHARE MODE"))
