#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Photo Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.photo.model import PhotoModel



# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PHOTO DAO CLASS ----------------------------------- #
class PhotoDAO():
    """
    Photo Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        photoModel = PhotoModel()
        for k, v in kwargs.items():
            setattr(photoModel, k, v)
        aDBS.add(photoModel)
        return photoModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(aDBS, photoModel):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.delete(photoModel)

    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(PhotoModel).filter(PhotoModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(PhotoModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, photoModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(photoModel, k, v)
        aDBS.add(photoModel)
        return photoModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        photoModel = aDBS.query(PhotoModel)\
            .filter(PhotoModel.smID == smID)\
            .one_or_none()
        if not photoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return photoModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, imageURL=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(PhotoModel)

        #Filters
        if imageURL:
            q = q.filter(PhotoModel.imageURL == imageURL)

        return q.count() if count else q.all()
