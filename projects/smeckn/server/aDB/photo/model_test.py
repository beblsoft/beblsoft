#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Photo Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.photo.dao import PhotoDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class PhotoModelTestCase(CommonTestCase):
    """
    Test Photo Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, photos=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            url            = "www.example.com/helloWorld.png"
            heightPX       = 500
            widthPX        = 500
            PhotoDAO.create(aDBS=aDBS, fromUser=True, imageURL=url,
                            heightPX=heightPX, widthPX=widthPX)
            aDBS.commit()

            # Get
            photoModelList = PhotoDAO.getAll(aDBS, imageURL=url)
            self.assertEqual(len(photoModelList), 1)
            photoModel = photoModelList[0]
            self.assertEqual(photoModel.heightPX, heightPX)

            # Delete
            PhotoDAO.deleteBySMID(aDBS=aDBS, smID=photoModel.smID)
            aDBS.commit()

            # Verify Delete
            photoModelList = PhotoDAO.getAll(aDBS, imageURL=url)
            self.assertEqual(len(photoModelList), 0)
