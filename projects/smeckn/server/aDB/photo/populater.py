#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Photo Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.photo.dao import PhotoDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PHOTO POPULATER CLASS ----------------------------- #
class PhotoPopulater(CommonPopulater):
    """
    Photo Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, count=5, heightPX=500, widthPX=500):  # pylint: disable=W0102
        """
        Populate N

        Returns
          Photo Model List
        """
        photoModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(count):
                url = "https://www.example.com/{}.png".format(forgery_py.basic.text())
                PhotoDAO.create(aDBS=aDBS, fromUser=True, imageURL=url,
                                heightPX=heightPX, widthPX=widthPX)
            aDBS.commit()
            photoModelList = PhotoDAO.getAll(aDBS=aDBS)
        return photoModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            PhotoDAO.deleteAll(aDBS=aDBS)
