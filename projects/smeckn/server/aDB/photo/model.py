#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Photo Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, Text, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger               = logging.getLogger(__name__)
MAX_PHOTO_URL_LENGTH = 4096  # See: https://stackoverflow.com/questions/219569/best-database-field-type-for-a-url


# ----------------------- PHOTO MODEL CLASS --------------------------------- #
class PhotoModel(base):
    """
    Photo Table
    """
    __tablename__       = "Photo"
    smID                = Column(BigInteger, primary_key=True)   # ID
    createDate          = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    fromUser            = Column(Boolean, default=False)         # If True, photo is directly from
                                                                 # user and did not originate from a profile
    imageURL            = Column(Text)
    heightPX            = Column(Integer, default=None)          # Ex. 500
    widthPX             = Column(Integer, default=None)          # Ex. 700

    # Parent Facebook Post
    # 1 Facebook Post : 1 Photo
    #
    # Fields notes:
    #  imageURL   - "https://scontent.xx.fbcdn.net/v/t15.5256-10/50055905_102701040846939_7867706927457763328_n.jpg?_nc_cat=107&_nc_ht=scontent.xx&oh=4dfcf345f22f67180bfbaaa1b0c43816&oe=5CF7CD37"
    #  heightPX   - None
    #  widthPX    - None
    fbPostSMID          = Column(BigInteger,
                                 ForeignKey("FacebookPost.smID", onupdate="CASCADE", ondelete="CASCADE"))
    fbPostModel         = relationship("FacebookPostModel", back_populates="photoModel")

    # Parent Facebook Photo
    # 1 Facebook Photo : 1 Photo
    fbPhotoSMID         = Column(BigInteger,
                                 ForeignKey("FacebookPhoto.smID", onupdate="CASCADE", ondelete="CASCADE"))
    fbPhotoModel        = relationship("FacebookPhotoModel", back_populates="photoModel")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
