#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Text Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import threading
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB.compLang.dao import CompLangDAO
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus, CompSentType
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger      = logging.getLogger(__name__)
threadLocal = threading.local()


# ----------------------- TEXT POPULATER CLASS ------------------------------ #
class TextPopulater(CommonPopulater):
    """
    Text Populater
    """

    TEXT_FAKE_LIST = [
        "Hello World!",
        "The Sun will come out tomorrow.",
        "Joy to the world the Lord has come!",
        "Here comes the sun.",
        "You know you’re in love when you can’t fall asleep because reality is finally better than your dreams",
        "I’m selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can’t handle me at my worst, then you sure as hell don’t deserve me at my best.",
        "Get busy living or get busy dying.",
        "When one door of happiness closes, another opens; but often we look so long at the closed door that we do not see the one which has been opened for us",
        "Twenty years from now you will be more disappointed by the things that you didn’t do than by the ones you did do",
        "When I dare to be powerful – to use my strength in the service of my vision, then it becomes less and less important whether I am afraid.",
        "Great minds discuss ideas; average minds discuss events; small minds discuss people",
        "Those who dare to fail miserably can achieve greatly.",
        "Let us always meet each other with smile, for the smile is the beginning of love.",
        "Challenges are what make life interesting and overcoming them is what makes life meaningful.",
        "A successful man is one who can lay a firm foundation with the bricks others have thrown at him.",
        "If the Easter Bunny and the Tooth Fairy had babies would they take your teeth and leave chocolate for you? What was the person thinking when they discovered cow’s milk was fine for human consumption… and why did they do it in the first place!? He turned in the research paper on Friday; otherwise, he would have not passed the class. We have never been to Asia, nor have we visited Africa.",
        "The book is in front of the table. Someone I know recently combined Maple Syrup & buttered Popcorn thinking it would taste like caramel popcorn. It didn’t and they don’t recommend anyone else do it either. The lake is a long way from here. Writing a list of random sentences is harder than I initially thought it would be.",
        "I often see the time 11:11 or 12:34 on clocks. Abstraction is often one floor above you. The memory we used to share is no longer coherent. Joe made the sugar cookies; Susan decorated them.",
        "Sometimes, all you need to do is completely make an ass of yourself and laugh it off to realise that life isn’t so bad after all. I am never at home on Sundays. A glittering gem is not enough. Italy is my favorite country; in fact, I plan to spend two weeks there next year.",
        "If I don’t like something, I’ll stay away from it. I currently have 4 windows open up… and I don’t know why. Someone I know recently combined Maple Syrup & buttered Popcorn thinking it would taste like caramel popcorn. It didn’t and they don’t recommend anyone else do it either. I checked to make sure that he was still alive.",
        "Hurry! My Mum tries to be cool by saying that she likes all the same things that I do. The waves were crashing on the shore; it was a lovely sight. If I don’t like something, I’ll stay away from it.",
        "aosdifu8734rlkm!!!sahdc-asdfasdfkjh"
    ]

    @staticmethod
    def setUpThreadLocal():
        """
        SetUp Thread Locals
        """
        try:
            _ = threadLocal.randomTextIdx
        except AttributeError:
            threadLocal.randomTextIdx = 0

    @staticmethod
    def getRandomText():
        """
        Return random text
        """
        TextPopulater.setUpThreadLocal()
        text = TextPopulater.TEXT_FAKE_LIST[threadLocal.randomTextIdx % len(TextPopulater.TEXT_FAKE_LIST)]
        threadLocal.randomTextIdx += 1
        return text

    # ---------------------------- POPULATE N ------------------------------- #
    @logFunc()
    def populateN(self, aID, count=5, fromUser=True):  # pylint: disable=W0102
        """
        Populate N
        Text models have no child relationships

        Returns
          Text Model List
        """
        textModelList = []

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                for _ in range(count):
                    TextDAO.createFromText(aDBS=aDBS, text=TextPopulater.getRandomText(), fromUser=fromUser)
            textModelList = TextDAO.getAll(aDBS=aDBS)
        return textModelList

    # ---------------------------- POPULATE FULL ---------------------------- #
    @logFunc()
    def populateFull(self, aID, count=200):
        """
        Populate all relevant text combinations

        Returns
          Text Model List
        """
        textPopulaterFuncList = TextPopulater.getTextPopulaterFuncList()

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                for idx in range(count):
                    textModel = TextDAO.createFromText(aDBS=aDBS, text=TextPopulater.getRandomText(), fromUser=True)
                    textPopulaterFunc = textPopulaterFuncList[idx % len(textPopulaterFuncList)]
                    textPopulaterFunc(aDBS=aDBS, textModel=textModel)
            textModelList = TextDAO.getAll(aDBS=aDBS)
        return textModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            TextDAO.deleteAll(aDBS=aDBS)

    # ---------------------------- POPULATE TEXT FUNC LIST ------------------ #
    @staticmethod
    def getTextPopulaterFuncList():
        """
        Return text population function list
        Each function populates the text in a different way

        Function prototype
        def func(aDBS, textModel):
            pass
        """
        return [
            TextPopulater.populateTextCompLangSuccessSupported,
            TextPopulater.populateTextCompLangSuccessUnsupported,
            TextPopulater.populateTextCompLangFailure,
            TextPopulater.populateTextCompSentSuccess,
            TextPopulater.populateTextCompSentCurrentUnsupportedLanguage,
            TextPopulater.populateTextCompSentNowSupportedLanguage,
            TextPopulater.populateTextCompSentFailure,
        ]

    # ---------------------------- POPULATE TEXT NOTHING -------------------- #
    @staticmethod
    @logFunc()
    def populateTextNothing(aDBS, textModel):
        """
        Placeholder to not add anthing to text
        """
        pass

    # ---------------------------- POPULATE TEXT COMP LANG ------------------ #
    @staticmethod
    @logFunc()
    def populateTextCompLangSuccessSupported(aDBS, textModel):
        """
        Comp Lang Success with supported language
        """
        compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                                           status=CompLangAnalysisStatus.SUCCESS)
        _                     = CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                                                   bLanguageType=BeblsoftLanguageType.English, score=.99)
        _                     = CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                                                   bLanguageType=BeblsoftLanguageType.French, score=.1)

    @staticmethod
    @logFunc()
    def populateTextCompLangSuccessUnsupported(aDBS, textModel):
        """
        Comp Lang Success with unsupported language
        """
        compLangAnalysisModel = CompLangAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                                           status=CompLangAnalysisStatus.SUCCESS)
        _                     = CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                                                   bLanguageType=BeblsoftLanguageType.Chinese, score=.99)
        _                     = CompLangDAO.create(aDBS=aDBS, compLangAnalysisModel=compLangAnalysisModel,
                                                   bLanguageType=BeblsoftLanguageType.French, score=.1)

    @staticmethod
    @logFunc()
    def populateTextCompLangFailure(aDBS, textModel):
        """
        Comp Lang Failure
        """
        _ = CompLangAnalysisDAO.create(aDBS=aDBS, textModel=textModel, status=CompLangAnalysisStatus.COMPREHEND_FAILED)

    # ---------------------------- POPULATE TEXT COMP SENT ------------------ #
    @staticmethod
    @logFunc()
    def populateTextCompSentSuccess(aDBS, textModel):
        """
        Comp Sent Success
        """
        TextPopulater.populateTextCompLangSuccessSupported(aDBS=aDBS, textModel=textModel)
        _ = CompSentAnalysisDAO.create(aDBS=aDBS, textModel=textModel, status=CompSentAnalysisStatus.SUCCESS,
                                       dominantSentiment=CompSentType.POSITIVE, positiveScore=.99,
                                       negativeScore=.1, neutralScore=.1, mixedScore=.1)

    @staticmethod
    @logFunc()
    def populateTextCompSentCurrentUnsupportedLanguage(aDBS, textModel):
        """
        Comp Sent with currently unsupported language
        """
        TextPopulater.populateTextCompLangSuccessUnsupported(aDBS=aDBS, textModel=textModel)
        _ = CompSentAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                       status=CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE)

    @staticmethod
    @logFunc()
    def populateTextCompSentNowSupportedLanguage(aDBS, textModel):
        """
        Mimic scenario where Comp Sent language was origininally marked unsupported but is now supported
        """
        TextPopulater.populateTextCompLangSuccessSupported(aDBS=aDBS, textModel=textModel)
        _ = CompSentAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                       status=CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE)

    @staticmethod
    @logFunc()
    def populateTextCompSentFailure(aDBS, textModel):
        """
        Comp Sent Failure
        """
        TextPopulater.populateTextCompLangSuccessSupported(aDBS=aDBS, textModel=textModel)
        _ = CompSentAnalysisDAO.create(aDBS=aDBS, textModel=textModel,
                                       status=CompSentAnalysisStatus.COMPREHEND_FAILED)
