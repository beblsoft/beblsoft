#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Text Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Boolean, String, Text, Integer
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.sort.common import CommonSort
from smeckn.server.aDB.sort.type import SortType


# ----------------------- GLOBALS ------------------------------------------- #
logger                  = logging.getLogger(__name__)
MAX_TEXT_LENGTH         = 5000 # Comprehend
TEXT_DIGEST_BYTE_LENGTH = 32
MAX_TEXT_FROM_USER      = 65536


# ----------------------- TEXT MODEL CLASS ---------------------------------- #
class TextModel(base):
    """
    Text Table
    """
    __tablename__         = "Text"
    smID                  = Column(BigInteger, primary_key=True)   # ID
    createDate            = Column(DATETIME(fsp=6),
                                   default=datetime.utcnow)        # Ex. "2000-01-04 07:53:55.474912"
    fromUser              = Column(Boolean, default=False)         # If True, text is directly from user

    # Store text with hexdigest for easy comparisons
    # Use Blake2b Hexdigest of untruncated text
    # Ex. "8c3467e8a3f196bdd1493cf649591fe114ff3ccc55ee135ad9ae3de55a62024a"
    text                  = Column(Text)
    textHexdigest         = Column(String(TEXT_DIGEST_BYTE_LENGTH * 2))
    origLength            = Column(Integer)

    # Child CompLangAnalysis
    # 1 Text: 1 CompLangAnalysis
    compLangAnalysisModel = relationship("CompLangAnalysisModel", back_populates="textModel",
                                         uselist=False, passive_deletes=True)

    # Child CompSentAnalysis
    # 1 Text: 1 CompSentAnalysis
    compSentAnalysisModel = relationship("CompSentAnalysisModel", back_populates="textModel",
                                         uselist=False, passive_deletes=True)

    # Parent Facebook Post
    # 1 Facebook Post : 1 Message Text
    #
    # Field notes
    #  text - "My first post!"
    fbPostSMID            = Column(BigInteger,
                                   ForeignKey("FacebookPost.smID", onupdate="CASCADE", ondelete="CASCADE"))
    fbPostModel           = relationship("FacebookPostModel", back_populates="messageTextModel")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)

    def textEqual(self, text):
        """
        Return True if text is the same
        """
        from smeckn.server.aDB.text.dao import TextDAO
        textHexdigest             = TextDAO.getTextHexdigest(text)
        return self.textHexdigest == textHexdigest


# ----------------------- SORT CLASSES -------------------------------------- #
class TextCreateDateSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.TEXT_CREATEDATE
    pColumn  = TextModel.createDate
    sColumn  = TextModel.smID
