#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Text Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.text.dao import TextDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class TextModelTestCase(CommonTestCase):
    """
    Test Text Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            text           = "Hello World Text!!"
            textHexdigest  = TextDAO.getTextHexdigest(text)
            TextDAO.create(
                aDBS          = aDBS,
                fromUser      = True,
                text          = text,
                textHexdigest = textHexdigest,
                origLength    = len(text))
            aDBS.commit()

            # Get
            textModelList = TextDAO.getAll(aDBS, iLikeText=text)
            self.assertEqual(len(textModelList), 1)
            textModel = textModelList[0]
            self.assertEqual(textModel.text, text)
            self.assertEqual(textModel.textHexdigest, textHexdigest)

            # Delete
            TextDAO.deleteBySMID(aDBS=aDBS, smID=textModel.smID)
            aDBS.commit()

            # Verify Delete
            textModelList = TextDAO.getAll(aDBS, iLikeText=text)
            self.assertEqual(len(textModelList), 0)

    @logFunc()
    def test_chinese(self):
        """
        Test chinese text
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            text           = "+いが同じではない。字体や用字法は地域ごとに異なる点が見られ、1980年代以降、それ以前に活字でよく見ら"
            textHexdigest  = TextDAO.getTextHexdigest(text)
            TextDAO.create(
                aDBS          = aDBS,
                fromUser      = True,
                text          = text,
                textHexdigest = textHexdigest,
                origLength    = len(text))
            aDBS.commit()

            # Get
            textModelList = TextDAO.getAll(aDBS, iLikeText=text)
            self.assertEqual(len(textModelList), 1)
            textModel = textModelList[0]
            self.assertEqual(textModel.text, text)
            self.assertEqual(textModel.textHexdigest, textHexdigest)
