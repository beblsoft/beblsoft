#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Text Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from hashlib import blake2b  # pylint: disable=E0611
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.aDB.text.model import TextModel, MAX_TEXT_LENGTH, TEXT_DIGEST_BYTE_LENGTH, MAX_TEXT_FROM_USER
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel, CompSentAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel
from smeckn.server.aDB.compSentAnalysis.filter import CompSentAnalysisFilter
from smeckn.server.aDB.sort.common import CommonSort


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- TEXT DAO CLASS ------------------------------------ #
class TextDAO():
    """
    Text Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def createFromText(aDBS, text, **createKwargs):  # pylint: disable=W0221
        """
        Create object
        """
        kwargs                  = {}
        kwargs["text"]          = text
        kwargs["truncatedText"] = TextDAO.getTruncatedText(text)
        kwargs["textDigest"]    = TextDAO.getTextHexdigest(text)
        kwargs["origLength"]    = len(text)
        kwargs.update(createKwargs)
        return TextDAO.create(aDBS=aDBS, **kwargs)

    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        if kwargs.get("fromUser", False):
            TextDAO.validateCreateMaxTextFromUserLimit(aDBS=aDBS)

        textModel = TextModel()
        for k, v in kwargs.items():
            setattr(textModel, k, v)
        aDBS.add(textModel)
        return textModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(aDBS, textModel):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.delete(textModel)

    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID, validateFromUser=False):  # pylint: disable=W0221
        """
        Delete object
        Args:
          validateFromUser:
            If True, verify that the textModel.fromUser is True
        """
        if validateFromUser:
            TextDAO.validateDeleteFromUser(aDBS=aDBS, smID=smID)
        aDBS.query(TextModel).filter(TextModel.smID == smID).delete()


    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(TextModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, textModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(textModel, k, v)
        aDBS.add(textModel)
        return textModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        textModel = aDBS.query(TextModel)\
            .filter(TextModel.smID == smID)\
            .one_or_none()
        if not textModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return textModel

    @staticmethod
    @logFunc()
    def getAll(aDBS,
               joinCompSent=False,
               createdAfterDate=None, createdBeforeDate=None, fromUser=None, iLikeText=None,
               compSentDone=None, compSentNotDone=None, compSentCant=None,
               sortType=None, sortOrder=None, sortPLastValue=None, sortSLastValue=None,
               limit=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(TextModel)

        # Joins
        if joinCompSent:
            q = q.outerjoin(CompSentAnalysisModel, TextModel.compSentAnalysisModel)
            q = q.outerjoin(CompLangAnalysisModel, TextModel.compLangAnalysisModel)

        # Text Filters
        if createdAfterDate:
            q = q.filter(TextModel.createDate >= createdAfterDate)
        if createdBeforeDate:
            q = q.filter(TextModel.createDate <= createdBeforeDate)
        if fromUser is not None:
            q = q.filter(TextModel.fromUser == fromUser)
        if iLikeText:
            q = q.filter(TextModel.text.ilike("%{}%".format(iLikeText)))

        # Comp Sent Filters
        if compSentDone:
            q = q.filter(CompSentAnalysisFilter.done)
        if compSentNotDone:
            q = q.filter(CompSentAnalysisFilter.notDone)
        if compSentCant:
            q = q.filter(CompSentAnalysisFilter.cant)

        # Sorting
        if sortType:
            SortClass = CommonSort.getClassFromType(sortType=sortType)
            sortInst  = SortClass(pSortOrder=sortOrder, pLastValue=sortPLastValue,
                                  sLastValue=sortSLastValue)
            for f in sortInst.filterList:
                q = q.filter(f)
            q = q.order_by(*sortInst.orderByArgs)

        # Limit
        if limit:
            q = q.limit(limit)

        return q.count() if count else q.all()

    @staticmethod
    @logFunc()
    def getModelListCountAD(textModelList):
        """
        Return Counts from TextModelList
        Used to verify getAll behavior

        Returns
          BeblsoftAttrDict  aD.nCompSentDone
                            aD.nCompSentCant
                            aD.nCompSentNotDone
        """
        aD                  = BeblsoftAttrDict()
        aD.nCompSentDone    = 0
        aD.nCompSentCant    = 0
        aD.nCompSentNotDone = 0

        for textModel in textModelList:
            compLangAnalysisModel = textModel.compLangAnalysisModel
            bLanguageType         = compLangAnalysisModel.dominantBLanguageType if compLangAnalysisModel else None
            compSentAnalysisModel = textModel.compSentAnalysisModel
            compSentStatus        = compSentAnalysisModel.status if compSentAnalysisModel else None

            if compSentStatus == CompSentAnalysisStatus.SUCCESS:
                aD.nCompSentDone    += 1
            elif compSentStatus == CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE and \
                    bLanguageType not in BeblsoftComprehendClient.detectSentimentSupportedBLanguageTypeList:
                aD.nCompSentCant    += 1
            else:
                aD.nCompSentNotDone += 1
        return aD

    # ---------------------- HELPERS ---------------------------------------- #
    @staticmethod
    @logFunc()
    def getTruncatedText(text):
        """
        Get text truncated to correct size
        """
        return text[0:MAX_TEXT_LENGTH]

    @staticmethod
    @logFunc()
    def getTextHexdigest(text):
        """
        Get text hexdigest
        """
        digestSize = TEXT_DIGEST_BYTE_LENGTH
        h          = blake2b(digest_size=digestSize)
        textBytes  = bytearray(text, "utf-8")
        h.update(textBytes)
        return h.hexdigest()

    # ---------------------- VALIDATION ------------------------------------- #
    @staticmethod
    @logFunc()
    def validateCreateMaxTextFromUserLimit(aDBS):
        """
        Validate that another piece of text can be created from user without exceeding limit
        """
        nTextFromUser = TextDAO.getAll(aDBS=aDBS, fromUser=True, count=True)
        if nTextFromUser >= MAX_TEXT_FROM_USER:
            raise BeblsoftError(BeblsoftErrorCode.TEXT_FROM_USER_LIMIT_EXCEEDED)

    @staticmethod
    @logFunc()
    def validateDeleteFromUser(aDBS, smID):
        """
        Validate that smID corresponds to text fromUser
        """
        textModel = TextDAO.getBySMID(aDBS=aDBS, smID=smID, raiseOnNone=False)
        if textModel and not textModel.fromUser:
            raise BeblsoftError(BeblsoftErrorCode.TEXT_CANT_DELETE_NOT_FROM_USER)
