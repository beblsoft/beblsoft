#!/usr/bin/env python3
"""
 NAME:
  dao_test.py

 DESCRIPTION
  Text DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.text.dao import TextDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- TEXT DAO GET ALL TEST CASE ------------------- #
class TextDAOGetAllTestCase(CommonTestCase):
    """
    Test Text DAO Get All
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, textFull=True)

    @logFunc()
    def test_compSent(self):
        """
        Test compSent
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            textModelList        = TextDAO.getAll(aDBS=aDBS)
            textModelListCountAD = TextDAO.getModelListCountAD(textModelList=textModelList)
            nCompSentDoneDB      = TextDAO.getAll(aDBS=aDBS, joinCompSent=True, compSentDone=True, count=True)
            nCompSentNotDoneDB   = TextDAO.getAll(aDBS=aDBS, joinCompSent=True, compSentNotDone=True, count=True)
            nCompSentCantDB      = TextDAO.getAll(aDBS=aDBS, joinCompSent=True, compSentCant=True, count=True)
            allCountDB           = TextDAO.getAll(aDBS=aDBS, count=True)

            self.assertEqual(textModelListCountAD.nCompSentDone, nCompSentDoneDB)
            self.assertEqual(textModelListCountAD.nCompSentNotDone, nCompSentNotDoneDB)
            self.assertEqual(textModelListCountAD.nCompSentCant, nCompSentCantDB)
            self.assertEqual(len(textModelList), allCountDB)
