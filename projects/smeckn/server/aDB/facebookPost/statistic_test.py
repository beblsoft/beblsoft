#!/usr/bin/env python3
"""
 NAME:
  statistic_test.py

 DESCRIPTION
  Facebook Post Statistic DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import statistics
from datetime import timedelta
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.facebookPost.model import FacebookPostType
from smeckn.server.aDB.facebookPost.statistic import FacebookPostStatisticDAO
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.facebookPost.populater import FacebookPostPopulater
from smeckn.server.aDB.statistic.context import StatisticContext
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- FACEBOOK POST STATISTIC DAO TEST CASE -------- #
class FacebookPostStatisticDAOTestCase(CommonTestCase):
    """
    Test Facebook Statistic Post DAO
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)

        self.nLikes            = 10
        self.nComments         = 10
        self.nReactions        = 10
        self.nPosts            = 100

        self.intervalStartDate = self.testStartDate
        self.intervalEndDate   = self.testStartDate + timedelta(seconds=50)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for idx in range(self.nPosts):
                fbPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID, fbID=idx,
                                                     type=FacebookPostType.STATUS,
                                                     fbCreateDate=self.intervalStartDate,
                                                     nLikes=self.nLikes, nComments=self.nComments,
                                                     nReactions=self.nReactions)
                FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                              textPopulaterFuncIdx=idx)

    @logFunc()
    def test_smID(self):
        """
        Test smid statistic
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            statisticCtx = StatisticContext(
                mgr=self.mgr,
                aDBS=aDBS,
                profileSMID=self.popFBPSMID,
                contentType=ContentType.POST,
                contentAttrType=ContentAttrType.FACEBOOK_POST_SMID,
                intervalStartDate=self.intervalStartDate,
                intervalEndDate=self.intervalEndDate)

            statisticModelList = FacebookPostStatisticDAO.getList(statisticCtx=statisticCtx)
            self.assertEqual(len(statisticModelList), 1)
            statisticModel     = statisticModelList[0]
            numList        = range(1, self.nPosts + 1)
            statSum        = sum(numList)
            statStdev      = statistics.stdev(numList)
            statAverage    = statSum / len(numList)
            self.assertEqual(statisticModel.count, self.nPosts)
            self.assertEqual(statisticModel.max, self.nPosts)
            self.assertEqual(statisticModel.min, 1)
            self.assertEqual(statisticModel.average, statAverage)
            self.assertAlmostEqual(statisticModel.stddev, statStdev, delta=1)
            self.assertEqual(statisticModel.sum, statSum)

    @logFunc()
    def test_supported(self):
        """
        Test supported combinations
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for statisticSupportModel in FacebookPostStatisticDAO.supportModelList:
                statisticCtx = StatisticContext(
                    mgr=self.mgr,
                    aDBS=aDBS,
                    profileSMID=self.popFBPSMID,
                    contentType=statisticSupportModel.contentType,
                    contentAttrType=statisticSupportModel.contentAttrType,
                    intervalStartDate=self.intervalStartDate,
                    intervalEndDate=self.intervalEndDate)

                statisticModelList = FacebookPostStatisticDAO.getList(statisticCtx=statisticCtx)
                self.assertEqual(len(statisticModelList), 1)
                statisticModel     = statisticModelList[0]
                self.assertEqual(statisticModel.profileSMID, self.popFBPSMID)
                self.assertEqual(statisticModel.contentType, statisticSupportModel.contentType)
                self.assertEqual(statisticModel.contentAttrType, statisticSupportModel.contentAttrType)
                self.assertEqual(statisticModel.intervalStartDate, self.intervalStartDate)
                self.assertEqual(statisticModel.intervalEndDate, self.intervalEndDate)
                self.assertGreater(statisticModel.count, 0)
                self.assertIsNotNone(statisticModel.max)
                self.assertIsNotNone(statisticModel.min)
                self.assertIsNotNone(statisticModel.average)
                self.assertIsNotNone(statisticModel.stddev)
                self.assertIsNotNone(statisticModel.sum)

    @logFunc()
    def test_notSupported(self):
        """
        Test unsupported combinations
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            statisticCtx = StatisticContext(
                mgr=self.mgr,
                aDBS=aDBS,
                profileSMID=self.popFBPSMID,
                contentType=ContentType.POST,
                contentAttrType=ContentAttrType.FACEBOOK_POST,
                intervalStartDate=self.intervalStartDate,
                intervalEndDate=self.intervalEndDate)

            with self.assertRaises(BeblsoftError) as cm:
                _ = FacebookPostStatisticDAO.getList(statisticCtx=statisticCtx)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.PROFILE_STATISTIC_CONTENT_NOTSUPPORTED)

    @logFunc()
    def test_badTimeRange(self):
        """
        Test bad time interval
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for statisticSupportModel in FacebookPostStatisticDAO.supportModelList:
                statisticCtx = StatisticContext(
                    mgr=self.mgr,
                    aDBS=aDBS,
                    profileSMID=self.popFBPSMID,
                    contentType=statisticSupportModel.contentType,
                    contentAttrType=statisticSupportModel.contentAttrType,
                    intervalStartDate=self.intervalStartDate - timedelta(seconds=100),
                    intervalEndDate=self.intervalStartDate - timedelta(seconds=90))

                statisticModelList = FacebookPostStatisticDAO.getList(statisticCtx=statisticCtx)
                self.assertEqual(len(statisticModelList), 1)
                statisticModel     = statisticModelList[0]
                self.assertEqual(statisticModel.count, 0)
                self.assertIsNone(statisticModel.max)
                self.assertIsNone(statisticModel.min)
                self.assertIsNone(statisticModel.stddev)
                self.assertIsNone(statisticModel.sum)

    @logFunc()
    def test_compSentAnalysisAllScore(self):
        """
        Test CompSentAnalysisAllScore
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            statisticCtx = StatisticContext(
                mgr=self.mgr,
                aDBS=aDBS,
                profileSMID=self.popFBPSMID,
                contentType=ContentType.POST_MESSAGE,
                contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_ALL_SCORE,
                intervalStartDate=self.intervalStartDate,
                intervalEndDate=self.intervalEndDate)

            statisticModelList = FacebookPostStatisticDAO.getList(statisticCtx=statisticCtx)
            self.assertEqual(len(statisticModelList), 4)
