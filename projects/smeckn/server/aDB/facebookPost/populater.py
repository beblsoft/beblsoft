#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Facebook Post Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from random import randint
from datetime import datetime, timedelta
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.facebookPost.model import FacebookPostType
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.text.populater import TextPopulater
from smeckn.server.aDB.photo.dao import PhotoDAO
from smeckn.server.aDB.video.dao import VideoDAO
from smeckn.server.aDB.link.dao import LinkDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK POST POPULATER CLASS --------------------- #
class FacebookPostPopulater(CommonPopulater):
    """
    Facebook Post Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateList(self, aID, fbpSMID, fbIDList=[], postType=FacebookPostType.STATUS):  # pylint: disable=W0102
        """
        Populate list

        Returns
          Facebook Post Model List
        """
        fbPostModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                for fbID in fbIDList:
                    fbPostModel = FacebookPostPopulater.populateBasePost(aDBS=aDBS, fbpSMID=fbpSMID,
                                                                         fbID=fbID, type=postType)

                    # Status Posts
                    if postType == FacebookPostType.STATUS:
                        message = TextPopulater.getRandomText()
                        TextDAO.createFromText(aDBS=aDBS, text=message, fbPostModel=fbPostModel)

                    # Photo Posts
                    if postType == FacebookPostType.PHOTO:
                        url     = "https://www.example.com/{}.png".format(forgery_py.basic.text())
                        PhotoDAO.create(aDBS=aDBS, fbPostModel=fbPostModel, imageURL=url, heightPX=500, widthPX=500)

                    # Video Posts
                    if postType == FacebookPostType.VIDEO:
                        url = "https://www.example.com/{}.mp4".format(forgery_py.basic.text())
                        VideoDAO.create(aDBS=aDBS, fbPostModel=fbPostModel,
                                        videoURL=url, heightPX=500, widthPX=500)

                    # Link Posts
                    if postType == FacebookPostType.LINK:
                        url = "https://www.example.com/{}".format(forgery_py.basic.text())
                        LinkDAO.create(aDBS=aDBS, fbPostModel=fbPostModel, linkURL=url)

            fbPostModelList = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=fbpSMID)
        return fbPostModelList

    # ---------------------------- POPULATE FULL ---------------------------- #
    @logFunc()
    def populateFull(self, aID, fbpSMID, nStatus=100, nPhoto=30, nVideo=30, nLink=30):
        """
        Populate all relevant facebook post combinations

        Returns
          fbPostModelList
        """
        populateFullFuncCountList = [
            (FacebookPostPopulater.populateFullStatus, nStatus),
            (FacebookPostPopulater.populateFullPhoto, nPhoto),
            (FacebookPostPopulater.populateFullVideo, nVideo),
            (FacebookPostPopulater.populateFullLink, nLink)
        ]

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                baseFBID = 0
                for (func, count) in populateFullFuncCountList:
                    func(aDBS=aDBS, fbpSMID=fbpSMID, baseFBID=baseFBID, count=count)
                    baseFBID += count
            fbPostModelList = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=fbpSMID)
        return fbPostModelList

    # ---------------------------- POPULATE BASE ---------------------------- #
    @staticmethod
    @logFunc()
    def populateBasePost(aDBS, fbpSMID, fbID, type,  # pylint: disable=W0622
                         minLikes=0, maxLikes=30,
                         minComments=0, maxComments=30,
                         minReactions=0, maxReactions=30,
                         fbCreateDateAfter=datetime(year=2000, month=1, day=1),
                         permalinkURL="https://www.facebook.com/102691597514550/posts/102690844181292"):
        """
        Populate base post
        Returns
          FacebookPostModel
        """
        now          = datetime.utcnow()
        deltaSeconds = int((now - fbCreateDateAfter).total_seconds())
        fbCreateDate = fbCreateDateAfter + timedelta(seconds=randint(0, deltaSeconds))

        return FacebookPostDAO.create(
            aDBS         = aDBS,
            fbpSMID      = fbpSMID,
            fbID         = fbID,
            fbCreateDate = fbCreateDate,
            fbUpdateDate = fbCreateDate,
            type         = type,
            nLikes       = randint(minLikes, maxLikes),
            nComments    = randint(minComments, maxComments),
            nReactions   = randint(minReactions, maxReactions),
            permalinkURL = permalinkURL)

    # ---------------------------- POPULATE TYPES --------------------------- #
    @staticmethod
    @logFunc()
    def populateFullStatus(aDBS, fbpSMID, baseFBID, count=100):
        """
        Populate all relevant status posts
        """
        for idx in range(count):
            fbPostModel       = FacebookPostPopulater.populateBasePost(aDBS=aDBS, fbpSMID=fbpSMID,
                                                                       fbID=baseFBID + idx,
                                                                       type=FacebookPostType.STATUS)
            FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                          textPopulaterFuncIdx=idx)

    @staticmethod
    @logFunc()
    def populateFullPhoto(aDBS, fbpSMID, baseFBID, count=100):
        """
        Populate all relevant photo posts
        """
        for idx in range(count):
            fbPostModel       = FacebookPostPopulater.populateBasePost(aDBS=aDBS, fbpSMID=fbpSMID,
                                                                       fbID=baseFBID + idx,
                                                                       type=FacebookPostType.PHOTO)
            url               = "https://www.example.com/{}.png".format(forgery_py.basic.text())
            PhotoDAO.create(aDBS=aDBS, fbPostModel=fbPostModel, imageURL=url, heightPX=500, widthPX=500)
            if idx > 1:
                FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                              textPopulaterFuncIdx=idx)

    @staticmethod
    @logFunc()
    def populateFullVideo(aDBS, fbpSMID, baseFBID, count=100):
        """
        Populate all relevant video posts
        """
        for idx in range(count):
            fbPostModel       = FacebookPostPopulater.populateBasePost(aDBS=aDBS, fbpSMID=fbpSMID,
                                                                       fbID=baseFBID + idx,
                                                                       type=FacebookPostType.VIDEO)
            url              = "https://www.example.com/{}.mp4".format(forgery_py.basic.text())
            VideoDAO.create(aDBS=aDBS, fbPostModel=fbPostModel, videoURL=url, heightPX=500, widthPX=500)
            if idx > 1:
                FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                              textPopulaterFuncIdx=idx)

    @staticmethod
    @logFunc()
    def populateFullLink(aDBS, fbpSMID, baseFBID, count=100):
        """
        Populate all relevant link posts
        """
        for idx in range(count):
            fbPostModel       = FacebookPostPopulater.populateBasePost(aDBS=aDBS, fbpSMID=fbpSMID,
                                                                       fbID=baseFBID + idx,
                                                                       type=FacebookPostType.LINK)
            url               = "https://www.example.com/{}".format(forgery_py.basic.text())
            LinkDAO.create(aDBS=aDBS, fbPostSMID=fbPostModel.smID, linkURL=url)
            if idx > 1:
                FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                              textPopulaterFuncIdx=idx)

    @staticmethod
    @logFunc()
    def populateFullMessageText(aDBS, fbPostModel, textPopulaterFuncIdx):
        """
        Add message text
        """
        textPopulaterFuncList = TextPopulater.getTextPopulaterFuncList()
        textModel             = TextDAO.createFromText(aDBS=aDBS, text=TextPopulater.getRandomText(),
                                                       fbPostModel=fbPostModel)
        textPopulaterFunc     = textPopulaterFuncList[textPopulaterFuncIdx % len(textPopulaterFuncList)]
        textPopulaterFunc(aDBS=aDBS, textModel=textModel)

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            FacebookPostDAO.deleteAll(aDBS=aDBS)
