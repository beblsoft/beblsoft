#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Facebook Post Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.aDB.facebookPost.model import FacebookPostModel, FacebookPostType
from smeckn.server.aDB.text.model import TextModel
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel, CompSentAnalysisStatus
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel
from smeckn.server.aDB.compSentAnalysis.filter import CompSentAnalysisFilter
from smeckn.server.aDB.sort.common import CommonSort


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK POST DAO CLASS --------------------------- #
class FacebookPostDAO():
    """
    Facebook Post Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, fbpSMID, **kwargs):  # pylint: disable=W0221
        """
        Create object
        Args
          aDBS:
            Account Database Session
          fbpSMID:
            Smeckn Facebook Profile ID
        """
        fbPostModel = FacebookPostModel(fbpSMID=fbpSMID)
        for k, v in kwargs.items():
            setattr(fbPostModel, k, v)
        aDBS.add(fbPostModel)
        return fbPostModel

    @staticmethod
    @logFunc()
    def createFromBFBPostModel(aDBS, fbpSMID, bFBPostModel):
        """
        Create object from BFacebookPostModel
        """
        fbPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=fbpSMID, fbID=bFBPostModel.id)
        fbPostModel = FacebookPostDAO.updateFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel,
                                                             bFBPostModel=bFBPostModel)
        return fbPostModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteStale(aDBS, fbpSMID, syncStartDate):  # pylint: disable=W0221
        """
        Delete posts from facebookProfile that were not synced after syncStartDate
        """
        aDBS.query(FacebookPostModel)\
            .filter(FacebookPostModel.fbpSMID == fbpSMID)\
            .filter(FacebookPostModel.syncDate < syncStartDate)\
            .delete()

    @staticmethod
    @logFunc()
    def deleteByFBID(aDBS, fbpSMID, fbID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(FacebookPostModel)\
            .filter(FacebookPostModel.fbpSMID == fbpSMID)\
            .filter(FacebookPostModel.fbID == fbID)\
            .delete()

    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(FacebookPostModel).filter(FacebookPostModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(FacebookPostModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, fbPostModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(fbPostModel, k, v)
        aDBS.add(fbPostModel)
        return fbPostModel

    @staticmethod
    @logFunc()
    def updateFromBFBPostModel(aDBS, fbPostModel, bFBPostModel):  # pylint: disable=R0915,R0912
        """
        Update object from BFacebookPostModel
        """
        fbPostModel.fbID         = bFBPostModel.id
        fbPostModel.syncDate     = datetime.utcnow()
        fbPostModel.fbCreateDate = bFBPostModel.createDate.replace(tzinfo=None)
        fbPostModel.fbUpdateDate = bFBPostModel.updateDate.replace(tzinfo=None)
        fbPostModel.type         = FacebookPostType.fromFacebookString(bFBPostModel.type)
        fbPostModel.nLikes       = bFBPostModel.nLikes
        fbPostModel.nComments    = bFBPostModel.nComments
        fbPostModel.nReactions   = bFBPostModel.nReactions
        fbPostModel.permalinkURL = bFBPostModel.permalinkURL

        FacebookPostDAO.updateMessageFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel, bFBPostModel=bFBPostModel)
        FacebookPostDAO.updatePhotoFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel, bFBPostModel=bFBPostModel)
        FacebookPostDAO.updateLinkFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel, bFBPostModel=bFBPostModel)
        FacebookPostDAO.updateVideoFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel, bFBPostModel=bFBPostModel)
        aDBS.add(fbPostModel)
        return fbPostModel

    @staticmethod
    @logFunc()
    def updateMessageFromBFBPostModel(aDBS, fbPostModel, bFBPostModel):
        """
        Update message text
        """
        from smeckn.server.aDB.text.dao import TextDAO
        messageText       = bFBPostModel.message
        messageTextModel  = fbPostModel.messageTextModel
        deleteMessageText = messageTextModel and not messageTextModel.textEqual(messageText)
        createMessageText = messageText and (not messageTextModel or deleteMessageText)
        if deleteMessageText:
            TextDAO.delete(aDBS=aDBS, textModel=messageTextModel)
        if createMessageText:
            TextDAO.createFromText(aDBS=aDBS, text=messageText, fbPostModel=fbPostModel)

    @staticmethod
    @logFunc()
    def updatePhotoFromBFBPostModel(aDBS, fbPostModel, bFBPostModel):
        """
        Update photo
        """
        from smeckn.server.aDB.photo.dao import PhotoDAO
        isPhoto                  = fbPostModel.type == FacebookPostType.PHOTO
        photoURL                 = bFBPostModel.fullPicture
        photoModel               = fbPostModel.photoModel
        if isPhoto:
            deletePhoto          = photoModel and photoModel.imageURL != photoURL
            createPhoto          = not photoModel or deletePhoto
        else:
            deletePhoto          = photoModel != None
            createPhoto          = False
        if deletePhoto:
            PhotoDAO.delete(aDBS=aDBS, photoModel=photoModel)
        if createPhoto:
            PhotoDAO.create(aDBS=aDBS, imageURL=photoURL, fbPostModel=fbPostModel)

        aDBS.add(fbPostModel)
        return fbPostModel

    @staticmethod
    @logFunc()
    def updateLinkFromBFBPostModel(aDBS, fbPostModel, bFBPostModel):
        """
        Update link
        """
        from smeckn.server.aDB.link.dao import LinkDAO
        isLink          = fbPostModel.type == FacebookPostType.LINK
        linkURL         = bFBPostModel.link
        linkModel       = fbPostModel.linkModel
        if isLink:
            deleteLink  = linkModel and linkModel.linkURL != linkURL
            createLink  = not linkModel or deleteLink
        else:
            deleteLink  = linkModel != None
            createLink  = False
        if deleteLink:
            LinkDAO.delete(aDBS=aDBS, linkModel=linkModel)
        if createLink:
            LinkDAO.create(aDBS=aDBS, linkURL=bFBPostModel.link, imageURL=bFBPostModel.fullPicture,
                           name=bFBPostModel.name, caption=bFBPostModel.caption,
                           description=bFBPostModel.description, fbPostModel=fbPostModel)

        aDBS.add(fbPostModel)
        return fbPostModel

    @staticmethod
    @logFunc()
    def updateVideoFromBFBPostModel(aDBS, fbPostModel, bFBPostModel):
        """
        Update video

        Note:
          Facebook video link expires! So if the imageURL is the same, we update
          the video URL with the lastest video
        """
        from smeckn.server.aDB.video.dao import VideoDAO
        isVideo           = fbPostModel.type == FacebookPostType.VIDEO
        videoURL          = bFBPostModel.source
        photoURL          = bFBPostModel.fullPicture
        lengthSec         = bFBPostModel.videoLengthS
        videoModel        = fbPostModel.videoModel

        if isVideo:
            updateVideo   = videoModel and videoModel.imageURL == photoURL  # Get video URL
            deleteVideo   = videoModel and videoModel.imageURL != photoURL
            createVideo   = not videoModel or deleteVideo
        else:
            updateVideo   = False
            deleteVideo   = videoModel != None
            createVideo   = False
        if updateVideo:
            VideoDAO.update(aDBS=aDBS, videoModel=videoModel, videoURL=videoURL)
        if deleteVideo:
            VideoDAO.delete(aDBS=aDBS, videoModel=videoModel)
        if createVideo:
            VideoDAO.create(aDBS=aDBS, videoURL=videoURL, imageURL=photoURL, lengthSec=lengthSec,
                            fbPostModel=fbPostModel)
        aDBS.add(fbPostModel)
        return fbPostModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        fbPostModel = aDBS.query(FacebookPostModel)\
            .filter(FacebookPostModel.smID == smID)\
            .one_or_none()
        if not fbPostModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbPostModel

    @staticmethod
    @logFunc()
    def getByFBID(aDBS, fbID, raiseOnNone=True):  # pylint: disable=W0622
        """
        Retrieve object by facebook id
        """
        fbPostModel = aDBS.query(FacebookPostModel)\
            .filter(FacebookPostModel.fbID == fbID)\
            .one_or_none()
        if not fbPostModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbPostModel

    @staticmethod
    @logFunc()
    def getAll(aDBS,  # pylint: disable=R0912
               joinMessage=False,
               fbpSMID=None, postType=None, syncedAfterDate=None, fbIDList=None,
               createdAfterDate=None, createdBeforeDate=None,
               containsMessage=None, messageTextILike=None,
               containsCompSent=None, compSentAfterDate=None, compSentDone=None, compSentNotDone=None, compSentCant=None,
               sortType=None, sortOrder=None, sortPLastValue=None, sortSLastValue=None,
               limit=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(FacebookPostModel)

        # Joins
        if joinMessage:
            q = q.outerjoin(TextModel, FacebookPostModel.messageTextModel)
            q = q.outerjoin(CompSentAnalysisModel, TextModel.compSentAnalysisModel)
            q = q.outerjoin(CompLangAnalysisModel, TextModel.compLangAnalysisModel)

        # Gen Filters
        if fbpSMID:
            q = q.filter(FacebookPostModel.fbpSMID == fbpSMID)
        if postType:
            q = q.filter(FacebookPostModel.type == postType)
        if syncedAfterDate:
            q = q.filter(FacebookPostModel.syncDate >= syncedAfterDate)
        if fbIDList:
            q = q.filter(FacebookPostModel.fbID.in_(fbIDList))
        if createdAfterDate:
            q = q.filter(FacebookPostModel.fbCreateDate >= createdAfterDate)
        if createdBeforeDate:
            q = q.filter(FacebookPostModel.fbCreateDate <= createdBeforeDate)

        # Message Filters
        if containsMessage:
            q = q.filter(FacebookPostModel.messageTextModel != None)
        if messageTextILike:
            q = q.filter(TextModel.text.ilike("%{}%".format(messageTextILike)))

        # Comp Sent Filters
        if containsCompSent:
            q = q.filter(TextModel.compSentAnalysisModel != None)
        if compSentAfterDate:
            q = q.filter(CompSentAnalysisModel.createDate >= compSentAfterDate)
        if compSentDone:
            q = q.filter(CompSentAnalysisFilter.done)
        if compSentNotDone:
            q = q.filter(CompSentAnalysisFilter.notDone)
        if compSentCant:
            q = q.filter(CompSentAnalysisFilter.cant)

        # Sorting
        if sortType:
            SortClass = CommonSort.getClassFromType(sortType=sortType)
            sortInst  = SortClass(pSortOrder=sortOrder, pLastValue=sortPLastValue,
                                  sLastValue=sortSLastValue)
            for f in sortInst.filterList:
                q = q.filter(f)
            q = q.order_by(*sortInst.orderByArgs)

        # Limit
        if limit:
            q = q.limit(limit)

        return q.count() if count else q.all()

    @staticmethod
    @logFunc()
    def getModelListCountAD(fbPostModelList, compSentAfterDate=None):
        """
        Return Counts from FBPostModelList
        Used to verify getAll behavior

        Returns
          BeblsoftAttrDict  aD.nMsgCompSentDone
                            aD.nMsgCompSentCant
                            aD.nMsgCompSentNotDone
                            aD.nMsgCompSentAfterDate
        """
        aD                       = BeblsoftAttrDict()
        aD.nMsgCompSentDone      = 0
        aD.nMsgCompSentCant      = 0
        aD.nMsgCompSentNotDone   = 0
        aD.nMsgCompSentAfterDate = 0

        for fbPostModel in fbPostModelList:
            # Message Counts
            messageTextModel          = fbPostModel.messageTextModel
            if messageTextModel:
                compLangAnalysisModel = messageTextModel.compLangAnalysisModel
                bLanguageType         = compLangAnalysisModel.dominantBLanguageType if compLangAnalysisModel else None
                compSentAnalysisModel = messageTextModel.compSentAnalysisModel
                compSentStatus        = compSentAnalysisModel.status if compSentAnalysisModel else None
                compSentAnalyzedAfter = compSentAnalysisModel and compSentAfterDate and compSentAnalysisModel.createDate >= compSentAfterDate

                if compSentStatus == CompSentAnalysisStatus.SUCCESS:
                    aD.nMsgCompSentDone      += 1
                elif compSentStatus == CompSentAnalysisStatus.UNSUPPORTED_LANGUAGE and \
                        bLanguageType not in BeblsoftComprehendClient.detectSentimentSupportedBLanguageTypeList:
                    aD.nMsgCompSentCant      += 1
                else:
                    aD.nMsgCompSentNotDone   += 1
                if compSentAnalyzedAfter:
                    aD.nMsgCompSentAfterDate += 1
        return aD
