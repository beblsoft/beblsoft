#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Facebook Post Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, BigInteger, ForeignKey, Enum, String, func, Text
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.contentAttr.common import CommonContentAttr
from smeckn.server.aDB.groupBy.model import GroupByType, GroupByResultType
from smeckn.server.aDB.groupBy.common import CommonGroupBy
from smeckn.server.aDB.sort.common import CommonSort
from smeckn.server.aDB.sort.type import SortType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class FacebookPostType(enum.Enum):
    """
    Facebook Post Type Enumeration
    """
    LINK   = 0
    STATUS = 1
    PHOTO  = 2
    VIDEO  = 3
    OFFER  = 4

    @staticmethod
    def fromFacebookString(string):  # pylint: disable=C0111
        return FacebookPostType[string.upper()]


# ----------------------- FACEBOOK POST MODEL CLASS ------------------------- #
class FacebookPostModel(base):
    """
    Facebook Post Table
    """
    __tablename__       = "FacebookPost"
    smID                = Column(BigInteger, primary_key=True)  # ID
    syncDate            = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)       # Date last synced

    # Facebook Data
    fbID                = Column(String(256))                   # Ex. "102691597514550_102700544180322"
    fbCreateDate        = Column(DATETIME(fsp=6))               # Ex. "2000-01-04 07:53:55.474912"
    fbUpdateDate        = Column(DATETIME(fsp=6))               # Ex. "2000-01-04 07:53:55.474912"
    type                = Column(Enum(FacebookPostType))        # Ex. FacebookPostType.LINK
    nLikes              = Column(BigInteger)                    # Ex. 5
    nComments           = Column(BigInteger)                    # Ex. 6
    nReactions          = Column(BigInteger)                    # Ex. 7
    permalinkURL        = Column(Text, default=None)            # Ex. https://www.facebook.com/102691597514550/posts/102690844181292

    # Parent Facebook Profile
    # 1 Facebook Profile : N Facebook Profile Posts
    fbpSMID             = Column(BigInteger,
                                 ForeignKey("FacebookProfile.smID", onupdate="CASCADE", ondelete="CASCADE"),
                                 nullable=False)
    fbpModel            = relationship("FacebookProfileModel", back_populates="postModelList")

    # Child Message Text
    # 1 Facebook Post: 1 Message Text
    messageTextModel    = relationship("TextModel", back_populates="fbPostModel",
                                       uselist=False, passive_deletes=True)

    # Child Photo
    # 1 Facebook Post: 1 Photo
    photoModel          = relationship("PhotoModel", back_populates="fbPostModel",
                                       uselist=False, passive_deletes=True)

    # Child Video
    # 1 Facebook Post: 1 Video
    videoModel          = relationship("VideoModel", back_populates="fbPostModel",
                                       uselist=False, passive_deletes=True)

    # Child Link
    # 1 Facebook Post: 1 Link
    linkModel           = relationship("LinkModel", back_populates="fbPostModel",
                                       uselist=False, passive_deletes=True)

    def __repr__(self):
        return "[{} smID={} fbID={}]".format(
            self.__class__.__name__, self.smID, self.fbID)


# ----------------------- CONTENT ATTR CLASSES ------------------------------ #
class FacebookPostContentAttr(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.FACEBOOK_POST
    column          = FacebookPostModel
    numeric         = True


class FacebookPostSMIDContentAttr(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.FACEBOOK_POST_SMID
    column          = FacebookPostModel.smID
    numeric         = True


class FacebookPostNLikesContentAttr(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.FACEBOOK_POST_NLIKES
    column          = FacebookPostModel.nLikes
    numeric         = True


class FacebookPostNCommentsContentAttr(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.FACEBOOK_POST_NCOMMENTS
    column          = FacebookPostModel.nComments
    numeric         = True


class FacebookPostNReactionsContentAttr(CommonContentAttr):  # pylint: disable=C0111
    contentAttrType = ContentAttrType.FACEBOOK_POST_NREACTIONS
    column          = FacebookPostModel.nReactions
    numeric         = True


# ----------------------- GROUP BY CLASSES ---------------------------------- #
class FacebookPostCreatedDateGroupBy(CommonGroupBy):  # pylint: disable=C0111
    groupByResultType = GroupByResultType.DATE
    groupByType       = GroupByType.FACEBOOK_POST_CREATE_DATE
    column            = FacebookPostModel.fbCreateDate

    @classmethod
    def mapToMySQLExpression(cls, groupByDateInterval=None):
        return func.date_format(cls.column, groupByDateInterval.mysqlStrFormat)

    @classmethod
    def mapFromMySQLResult(cls, result, groupByDateInterval=None):
        return groupByDateInterval.mysqlDateStrToDatetime(result)


# ----------------------- SORT CLASSES -------------------------------------- #
class FacebookPostFBCreateDateSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.FACEBOOK_POST_FBCREATEDATE
    pColumn  = FacebookPostModel.fbCreateDate
    sColumn  = FacebookPostModel.smID


class FacebookPostNLikesSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.FACEBOOK_POST_NLIKES
    pColumn  = FacebookPostModel.nLikes
    sColumn  = FacebookPostModel.smID


class FacebookPostNCommentsSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.FACEBOOK_POST_NCOMMENTS
    pColumn  = FacebookPostModel.nComments
    sColumn  = FacebookPostModel.smID


class FacebookPostNReactionsSort(CommonSort):  # pylint: disable=C0111
    sortType = SortType.FACEBOOK_POST_NREACTIONS
    pColumn  = FacebookPostModel.nReactions
    sColumn  = FacebookPostModel.smID
