#!/usr/bin/env python3
"""
NAME
 statistic.py

DESCRIPTION
 Facebook Post Statistic Data Access Object
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.facebookPost.model import FacebookPostModel
from smeckn.server.aDB.text.model import TextModel
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.statistic.dao import CommonStatisticDAO
from smeckn.server.aDB.statistic.model import StatisticSupportModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK POST STATISTIC DAO CLASS ----------------- #
class FacebookPostStatisticDAO(CommonStatisticDAO):
    """
    Facebook Post Statistic Data Access Object
    """
    supportModelList = [
        # Post
        StatisticSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_SMID),

        StatisticSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NLIKES),

        StatisticSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NREACTIONS),

        StatisticSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NCOMMENTS),
        # Post Message
        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_SMID),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NLIKES),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NREACTIONS),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NCOMMENTS),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_POSITIVE_SCORE),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE),

        StatisticSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_MIXED_SCORE),
    ]

    @staticmethod
    @logFunc()
    def getList(statisticCtx):
        """
        Get StatisticModelList
        """
        statisticModelList = []
        contentType        = statisticCtx.contentType
        contentAttrType    = statisticCtx.contentAttrType

        # ContentAttrType.COMPREHEND_SENTIMENT
        # Shortcut allowing client to get all comprehend sentiment stats at once
        if contentType == ContentType.POST_MESSAGE and contentAttrType == ContentAttrType.COMP_SENT_ANALYSIS_ALL_SCORE:
            for contentAttrType in [ContentAttrType.COMP_SENT_ANALYSIS_POSITIVE_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_MIXED_SCORE]:
                statisticCtx.contentAttrType = contentAttrType
                statisticModel = FacebookPostStatisticDAO.get(statisticCtx=statisticCtx)
                statisticModelList.append(statisticModel)
        # Regular case
        else:
            statisticModel = FacebookPostStatisticDAO.get(statisticCtx=statisticCtx)
            statisticModelList.append(statisticModel)

        return statisticModelList

    @staticmethod
    @logFunc()
    def getProfileSMIDColumn(statisticCtx):
        return FacebookPostModel.fbpSMID

    @staticmethod
    @logFunc()
    def getCreateDateColumn(statisticCtx):
        return FacebookPostModel.fbCreateDate

    @staticmethod
    @logFunc()
    def getOuterJoinList(statisticCtx):
        contentType   = statisticCtx.contentType
        outerJoinList = []
        if contentType == ContentType.POST_MESSAGE:
            outerJoinList.append((TextModel, FacebookPostModel.messageTextModel))
            outerJoinList.append((CompSentAnalysisModel, TextModel.compSentAnalysisModel))
            outerJoinList.append((CompLangAnalysisModel, TextModel.compLangAnalysisModel))
        return outerJoinList

    @staticmethod
    @logFunc()
    def getFilterList(statisticCtx):
        contentType   = statisticCtx.contentType
        filterList    = []
        if contentType == ContentType.POST_MESSAGE:
            filterList.append(FacebookPostModel.messageTextModel != None)
        return filterList
