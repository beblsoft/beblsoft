#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Facebook Post Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.facebookPost.model import FacebookPostType
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.photo.dao import PhotoDAO
from smeckn.server.aDB.video.dao import VideoDAO
from smeckn.server.aDB.link.dao import LinkDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class FacebookPostModelTestCase(CommonTestCase):
    """
    Test Facebook Post Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True,
                      facebookPosts=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        fbpSMID  = self.popFBPSMID
        fbID     = "129812731928371_234987234933387"
        postType = FacebookPostType.STATUS

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            FacebookPostDAO.create(aDBS, fbpSMID=fbpSMID, fbID=fbID, type=postType)
            aDBS.commit()

            # Get
            fbPostModel = FacebookPostDAO.getByFBID(aDBS, fbID=fbID)
            self.assertEqual(fbPostModel.fbID, fbID)
            self.assertEqual(fbPostModel.type, postType)

            # Delete
            FacebookPostDAO.deleteBySMID(aDBS=aDBS, smID=fbPostModel.smID)
            aDBS.commit()

            # Verify Delete
            fbPostModel = FacebookPostDAO.getByFBID(aDBS, fbID=fbID, raiseOnNone=False)
            self.assertFalse(fbPostModel)

    @logFunc()
    def test_nullFBPSMID(self):
        """
        Test trying to create facebook post with NULL fbpSMID
        """
        fbID     = "129812731928371_234987234933387"
        postType = FacebookPostType.STATUS

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                FacebookPostDAO.create(aDBS, fbpSMID=None, fbID=fbID, type=postType)
                aDBS.commit()
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created post with null fbpSMID")

    @logFunc()
    def test_badFBPSMID(self):
        """
        Test trying to create facebook post with bad fbpSMID
        """
        fbID     = "129812731928371_234987234933387"
        fbpSMID  = 23948230948
        postType = FacebookPostType.STATUS

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                FacebookPostDAO.create(aDBS, fbpSMID=fbpSMID, fbID=fbID, type=postType)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created post with invalid fbpSMID")

    @logFunc()
    def test_pgCascadeDelete(self):
        """
        Test profile group deletions cascade down appropriately to delete the
        following objects:
          - Facebook Posts
          - Message text
          - Photos
          - Videos
          - Links
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbPostModelList = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            textModelList   = TextDAO.getAll(aDBS=aDBS)
            photoModelList  = PhotoDAO.getAll(aDBS=aDBS)
            videoModelList  = VideoDAO.getAll(aDBS=aDBS)
            linkModelList   = LinkDAO.getAll(aDBS=aDBS)
            self.assertGreater(len(fbPostModelList), 0)
            self.assertGreater(len(textModelList), 0)
            self.assertGreater(len(photoModelList), 0)
            self.assertGreater(len(videoModelList), 0)
            self.assertGreater(len(linkModelList), 0)

            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=self.popPGID)
            aDBS.commit()

            fbPostModelList = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            textModelList   = TextDAO.getAll(aDBS=aDBS)
            photoModelList  = PhotoDAO.getAll(aDBS=aDBS)
            videoModelList  = VideoDAO.getAll(aDBS=aDBS)
            linkModelList   = LinkDAO.getAll(aDBS=aDBS)
            self.assertFalse(fbPostModelList)
            self.assertFalse(textModelList)
            self.assertFalse(photoModelList)
            self.assertFalse(videoModelList)
            self.assertFalse(linkModelList)
