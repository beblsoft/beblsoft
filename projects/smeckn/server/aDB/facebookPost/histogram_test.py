#!/usr/bin/env python3
"""
 NAME:
  histogram_test.py

 DESCRIPTION
  Facebook Post Histogram DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.facebookPost.model import FacebookPostType
from smeckn.server.aDB.facebookPost.histogram import FacebookPostHistogramDAO
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO
from smeckn.server.aDB.compSentAnalysis.model import CompSentType
from smeckn.server.aDB.facebookPost.populater import FacebookPostPopulater
from smeckn.server.aDB.histogram.context import HistogramContext
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- FACEBOOK POST HISTOGRAM DAO TEST CASE -------- #
class FacebookPostHistogramDAOTestCase(CommonTestCase):
    """
    Test Facebook Histogram Post DAO
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)

        self.nLikes            = 10
        self.nComments         = 10
        self.nReactions        = 10
        self.nPosts            = 100
        self.intervalStartDate = self.testStartDate
        self.nSeconds          = 10
        self.secondList        = [self.intervalStartDate +
                                  timedelta(seconds=idx) for idx in range(1, self.nSeconds + 1)]
        self.nMinutes          = 10
        self.minuteList        = [self.intervalStartDate +
                                  timedelta(minutes=idx) for idx in range(1, self.nMinutes + 1)]
        self.nHours            = 10
        self.hourList          = [self.intervalStartDate + timedelta(hours=idx) for idx in range(1, self.nHours + 1)]
        self.nDays             = 10
        self.dayList           = [self.intervalStartDate + timedelta(days=idx) for idx in range(1, self.nDays + 1)]
        self.nMonths           = 10
        self.monthList         = [self.intervalStartDate +
                                  relativedelta(months=idx) for idx in range(1, self.nMonths + 1)]
        self.nYears            = 10
        self.yearList          = [self.intervalStartDate +
                                  relativedelta(years=idx) for idx in range(1, self.nYears + 1)]
        self.dateList          = (self.secondList + self.minuteList +
                                  self.hourList + self.dayList + self.monthList + self.yearList)

        # Create posts with different dates
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for idx, date in enumerate(self.dateList):
                fbPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                     fbID=idx,
                                                     type=FacebookPostType.STATUS,
                                                     fbCreateDate=date,
                                                     nLikes=self.nLikes, nComments=self.nComments,
                                                     nReactions=self.nReactions)
                FacebookPostPopulater.populateFullMessageText(aDBS=aDBS, fbPostModel=fbPostModel,
                                                              textPopulaterFuncIdx=idx)

    @logFunc()
    def test_smID(self):
        """
        Test smid statistic
        """
        contentType     = ContentType.POST
        contentAttrType = ContentAttrType.FACEBOOK_POST_SMID
        groupByType     = GroupByType.FACEBOOK_POST_CREATE_DATE

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for dateList in [self.secondList, self.minuteList, self.hourList, self.dayList, self.monthList, self.yearList]:
                startDate = dateList[0]
                endDate   = dateList[-1]
                histogramCtx = HistogramContext(
                    mgr=self.mgr, aDBS=aDBS, profileSMID=self.popFBPSMID,
                    contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType,
                    intervalStartDate=startDate, intervalEndDate=endDate)
                histogramModelList = FacebookPostHistogramDAO.getList(histogramCtx=histogramCtx)
                self.assertEqual(len(histogramModelList), 1)
                histogramModel = histogramModelList[0]
                self.assertEqual(histogramModel.profileSMID, self.popFBPSMID)
                self.assertEqual(histogramModel.contentType, contentType)
                self.assertEqual(histogramModel.contentAttrType, contentAttrType)
                self.assertEqual(histogramModel.groupByType, groupByType)
                self.assertIsNotNone(histogramModel.groupByDateInterval)
                self.assertEqual(histogramModel.intervalStartDate, startDate)
                self.assertEqual(histogramModel.intervalEndDate, endDate)
                for idx, histogramDataModel in enumerate(histogramModel.dataModelList):
                    self.assertEqual(histogramDataModel.groupByDate,
                                     histogramModel.groupByDateInterval.roundDown(dateList[idx]))
                    self.assertEqual(histogramDataModel.groupByContentAttrType, None)
                    self.assertEqual(histogramDataModel.count, 1)
                    self.assertGreater(histogramDataModel.sum, 0)
                    self.assertGreater(histogramDataModel.average, 0)
                    self.assertGreater(histogramDataModel.min, 0)
                    self.assertGreater(histogramDataModel.max, 0)
                    self.assertEqual(histogramDataModel.stddev, 0)

    @logFunc()
    def test_supported(self):
        """
        Test supported combinations
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            startDate = self.dateList[0]
            endDate   = self.dateList[-1]
            for histogramSupportModel in FacebookPostHistogramDAO.supportModelList:
                histogramCtx = HistogramContext(
                    mgr=self.mgr, aDBS=aDBS, profileSMID=self.popFBPSMID,
                    contentType=histogramSupportModel.contentType,
                    contentAttrType=histogramSupportModel.contentAttrType,
                    groupByType=histogramSupportModel.groupByType,
                    intervalStartDate=startDate, intervalEndDate=endDate)
                histogramModelList = FacebookPostHistogramDAO.getList(histogramCtx=histogramCtx)
                self.assertEqual(len(histogramModelList), 1)
                histogramModel = histogramModelList[0]
                self.assertEqual(histogramModel.profileSMID, self.popFBPSMID)
                self.assertEqual(histogramModel.contentType, histogramSupportModel.contentType)
                self.assertEqual(histogramModel.contentAttrType, histogramSupportModel.contentAttrType)
                self.assertEqual(histogramModel.intervalStartDate, startDate)
                self.assertEqual(histogramModel.intervalEndDate, endDate)
                self.assertGreater(len(histogramModel.dataModelList), 0)

    @logFunc()
    def test_notSupported(self):
        """
        Test unsupported combinations
        """
        contentType     = ContentType.POST
        contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT
        groupByType     = GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT
        startDate       = self.dateList[0]
        endDate         = self.dateList[-1]

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            histogramCtx = HistogramContext(
                mgr=self.mgr, aDBS=aDBS, profileSMID=self.popFBPSMID,
                contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType,
                intervalStartDate=startDate, intervalEndDate=endDate)

            with self.assertRaises(BeblsoftError) as cm:
                _ = FacebookPostHistogramDAO.getList(histogramCtx=histogramCtx)
            self.assertEqual(cm.exception.code, BeblsoftErrorCode.PROFILE_HISTOGRAM_NOTSUPPORTED)

    @logFunc()
    def test_compSentAnalysisAllScore(self):
        """
        Test CompSentAnalysisAllScore
        """
        contentType     = ContentType.POST_MESSAGE
        contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_ALL_SCORE
        groupByType     = GroupByType.FACEBOOK_POST_CREATE_DATE

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            startDate = self.dateList[0]
            endDate   = self.dateList[-1]
            histogramCtx = HistogramContext(
                mgr=self.mgr, aDBS=aDBS, profileSMID=self.popFBPSMID,
                contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType,
                intervalStartDate=startDate, intervalEndDate=endDate)
            histogramModelList = FacebookPostHistogramDAO.getList(histogramCtx=histogramCtx)
            self.assertEqual(len(histogramModelList), 4)


# ---------------------------- FACEBOOK POST HISTOGRAM COMP SENT DOMINANT --- #
class FacebookPostHistogramDAOCompSentDominantSentimentTestCase(CommonTestCase):
    """
    Test Facebook Post Histgram DAO Comp Sent Dominant Sentiment
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)
        self.intervalStartDate           = self.testStartDate
        self.intervalEndDate             = self.testStartDate + timedelta(seconds=20)
        self.nPosts                      = 100
        self.compSentTypeList            = list(CompSentType)
        self.compSentTypeContentAttrList = [ContentAttrType.COMP_SENT_TYPE_POSITIVE, ContentAttrType.COMP_SENT_TYPE_NEGATIVE,
                                            ContentAttrType.COMP_SENT_TYPE_NEUTRAL, ContentAttrType.COMP_SENT_TYPE_MIXED]

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            for idx in range(self.nPosts):
                fbPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                     fbID=idx,
                                                     type=FacebookPostType.STATUS,
                                                     fbCreateDate=datetime.utcnow())
                messageTextModel = TextDAO.createFromText(aDBS=aDBS, text="RandomText",
                                                          fbPostModel=fbPostModel)
                _ = CompSentAnalysisDAO.create(aDBS=aDBS, textModel=messageTextModel,
                                               dominantSentiment=self.compSentTypeList[idx % len(self.compSentTypeList)])

    @logFunc()
    def test_compSentAnalysisDominantSentiment(self):
        """
        Test comp sent analysis dominant sentiment
        """
        contentType     = ContentType.POST_MESSAGE
        contentAttrType = ContentAttrType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT
        groupByType     = GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            histogramCtx = HistogramContext(
                mgr=self.mgr, aDBS=aDBS, profileSMID=self.popFBPSMID,
                contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType,
                intervalStartDate=self.intervalStartDate, intervalEndDate=self.intervalEndDate)
            histogramModelList = FacebookPostHistogramDAO.getList(histogramCtx=histogramCtx)
            self.assertEqual(len(histogramModelList), 1)
            histogramModel = histogramModelList[0]
            for histogramDataModel in histogramModel.dataModelList:
                self.assertIn(histogramDataModel.groupByContentAttrType, self.compSentTypeContentAttrList)
                self.assertEqual(histogramDataModel.count, self.nPosts / len(self.compSentTypeList))
