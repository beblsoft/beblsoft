#!/usr/bin/env python3
"""
NAME
 Histogram.py

DESCRIPTION
 Facebook Post Histogram Data Access Object
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.facebookPost.model import FacebookPostModel
from smeckn.server.aDB.text.model import TextModel
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisModel
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisModel
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.histogram.dao import CommonHistogramDAO
from smeckn.server.aDB.histogram.model import HistogramSupportModel
from smeckn.server.aDB.groupBy.model import GroupByType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK POST HISTOGRAM DAO CLASS ----------------- #
class FacebookPostHistogramDAO(CommonHistogramDAO):
    """
    Facebook Post Histogram Data Access Object
    """
    supportModelList = [
        # Post
        HistogramSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_SMID,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NLIKES,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NREACTIONS,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NCOMMENTS,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),
        # Post Message
        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_SMID,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NLIKES,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NREACTIONS,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.FACEBOOK_POST_NCOMMENTS,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_POSITIVE_SCORE,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_MIXED_SCORE,
                              groupByType=GroupByType.FACEBOOK_POST_CREATE_DATE),

        HistogramSupportModel(contentType=ContentType.POST_MESSAGE,  # Dominant sentiment pie chart
                              contentAttrType=ContentAttrType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT,
                              groupByType=GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT),
    ]

    @staticmethod
    @logFunc()
    def getList(histogramCtx):
        """
        Return HistogramModelList
        """
        histogramModelList = []
        contentType        = histogramCtx.contentType
        contentAttrType    = histogramCtx.contentAttrType

        # ContentAttrType.COMP_SENT_ANALYSIS_ALL_SCORE
        # Shortcut allowing client to get all comprehend sentiment stats at once
        if contentType == ContentType.POST_MESSAGE and contentAttrType == ContentAttrType.COMP_SENT_ANALYSIS_ALL_SCORE:
            for contentAttrType in [ContentAttrType.COMP_SENT_ANALYSIS_POSITIVE_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE,
                                    ContentAttrType.COMP_SENT_ANALYSIS_MIXED_SCORE]:
                histogramCtx.contentAttrType = contentAttrType
                histogramModel = FacebookPostHistogramDAO.get(histogramCtx=histogramCtx)
                histogramModelList.append(histogramModel)
        # Regular case
        else:
            histogramModel = FacebookPostHistogramDAO.get(histogramCtx=histogramCtx)
            histogramModelList.append(histogramModel)

        return histogramModelList

    @staticmethod
    @logFunc()
    def getProfileSMIDColumn(histogramCtx):
        return FacebookPostModel.fbpSMID

    @staticmethod
    @logFunc()
    def getCreateDateColumn(histogramCtx):
        return FacebookPostModel.fbCreateDate

    @staticmethod
    @logFunc()
    def getOuterJoinList(histogramCtx):
        contentType   = histogramCtx.contentType
        outerJoinList = []
        if contentType == ContentType.POST_MESSAGE:
            outerJoinList.append((TextModel, FacebookPostModel.messageTextModel))
            outerJoinList.append((CompSentAnalysisModel, TextModel.compSentAnalysisModel))
            outerJoinList.append((CompLangAnalysisModel, TextModel.compLangAnalysisModel))
        return outerJoinList

    @staticmethod
    @logFunc()
    def getFilterList(histogramCtx):
        contentType   = histogramCtx.contentType
        groupByType   = histogramCtx.groupByType
        filterList    = []
        if contentType == ContentType.POST_MESSAGE:
            filterList.append(FacebookPostModel.messageTextModel != None)
        if groupByType == GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT:
            filterList.append(CompSentAnalysisModel.dominantSentiment != None)
        return filterList
