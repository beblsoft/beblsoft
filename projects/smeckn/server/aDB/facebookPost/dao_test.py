#!/usr/bin/env python3
"""
 NAME:
  dao_test.py

 DESCRIPTION
  Facebook Post DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta, timezone
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- FACEBOOK POST DAO TEST CASE ------------------ #
class FacebookPostDAOTestCase(CommonTestCase):
    """
    Test Facebook Post DAO
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True,
                      facebookTestUsers=True, facebookTestUserData=True,
                      facebookTestUserProfiles=True)
        fbTestUserName      = "small"
        self.fbTestUser     = self.fbTestUserDict[fbTestUserName]
        self.fbTestUserData = self.fbTestUserDataDict[fbTestUserName]

    @logFunc()
    def test_statusPostCRUD(self):
        """
        Test status post crud
        """
        bFBPostMediaModel = self.fbTestUserData.bFBStatusPostMediaModel
        bFBPostModel      = bFBPostMediaModel.bFBPostModel

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Create
            aDBS.refresh(fbPostModel)
            self.assertEqual(fbPostModel.fbID, bFBPostModel.id)
            self.assertGreaterEqual(fbPostModel.syncDate, datetime.utcnow() - timedelta(seconds=1))
            self.assertEqual(fbPostModel.fbCreateDate.replace(tzinfo=timezone.utc), bFBPostModel.createDate)
            self.assertEqual(fbPostModel.fbUpdateDate.replace(tzinfo=timezone.utc), bFBPostModel.updateDate)
            self.assertEqual(fbPostModel.nLikes, bFBPostModel.nLikes)
            self.assertEqual(fbPostModel.nComments, bFBPostModel.nComments)
            self.assertEqual(fbPostModel.nReactions, bFBPostModel.nReactions)
            self.assertEqual(fbPostModel.permalinkURL, bFBPostModel.permalinkURL)
            self.assertIsNotNone(fbPostModel.permalinkURL)
            messageTextModel = fbPostModel.messageTextModel
            self.assertEqual(messageTextModel.text, bFBPostModel.message)
            messageTextSMID  = messageTextModel.smID

            # Update
            bFBPostModel.message = "RandomTextMessage!!!"
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.updateFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel,
                                                                     bFBPostModel=bFBPostModel)

            # Verify Update
            aDBS.refresh(fbPostModel)
            messageTextModel = fbPostModel.messageTextModel
            self.assertEqual(messageTextModel.text, bFBPostModel.message)
            self.assertNotEqual(messageTextModel.smID, messageTextSMID)

    @logFunc()
    def test_photoPostCRUD(self):
        """
        Test photo post crud
        """
        bFBPostMediaModel  = self.fbTestUserData.bFBPhotoPostMediaModel
        bFBPostModel       = bFBPostMediaModel.bFBPostModel

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Create
            aDBS.refresh(fbPostModel)
            self.assertEqual(fbPostModel.fbID, bFBPostModel.id)
            photoModel = fbPostModel.photoModel
            self.assertEqual(photoModel.imageURL, bFBPostModel.fullPicture)
            self.assertEqual(photoModel.widthPX, None)
            self.assertEqual(photoModel.heightPX, None)
            photoSMID = photoModel.smID

            # Update
            bFBPostModel.fullPicture = "https://www.facebook.com/randompic"
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.updateFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Update
            aDBS.refresh(fbPostModel)
            photoModel = fbPostModel.photoModel
            self.assertEqual(photoModel.imageURL, bFBPostModel.fullPicture)
            self.assertEqual(photoModel.widthPX, None)
            self.assertEqual(photoModel.heightPX, None)
            self.assertNotEqual(photoModel.smID, photoSMID)

    @logFunc()
    def test_linkPostCRUD(self):
        """
        Test link post crud
        """
        bFBPostMediaModel = self.fbTestUserData.bFBLinkPostMediaModel
        bFBPostModel      = bFBPostMediaModel.bFBPostModel

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Create
            aDBS.refresh(fbPostModel)
            self.assertEqual(fbPostModel.fbID, bFBPostModel.id)
            linkModel = fbPostModel.linkModel
            self.assertEqual(linkModel.linkURL, bFBPostModel.link)
            self.assertEqual(linkModel.imageURL, bFBPostModel.fullPicture)
            linkSMID  = linkModel.smID

            # Update
            bFBPostModel.link = "https://www.facebook.com/randomlink"
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.updateFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Update
            aDBS.refresh(fbPostModel)
            linkModel = fbPostModel.linkModel
            self.assertEqual(linkModel.linkURL, bFBPostModel.link)
            self.assertNotEqual(linkModel.smID, linkSMID)

    @logFunc()
    def test_videoPostCRUD(self):
        """
        Test video post crud
        """
        bFBPostMediaModel  = self.fbTestUserData.bFBVideoPostMediaModel
        bFBPostModel       = bFBPostMediaModel.bFBPostModel

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Create
            aDBS.refresh(fbPostModel)
            self.assertEqual(fbPostModel.fbID, bFBPostModel.id)
            videoModel = fbPostModel.videoModel
            self.assertEqual(videoModel.videoURL, bFBPostModel.source)
            self.assertEqual(videoModel.imageURL, bFBPostModel.fullPicture)
            self.assertEqual(videoModel.lengthSec, bFBPostModel.videoLengthS)
            videoSMID = videoModel.smID

            # Update
            bFBPostModel.source      = "https://www.facebook.com/randomvideo"
            bFBPostModel.fullPicture = "https://www.facebook.com/randompicture"
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.updateFromBFBPostModel(aDBS=aDBS, fbPostModel=fbPostModel,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Update
            aDBS.refresh(fbPostModel)
            videoModel = fbPostModel.videoModel
            self.assertEqual(videoModel.videoURL, bFBPostModel.source)
            self.assertEqual(videoModel.imageURL, bFBPostModel.fullPicture)
            self.assertNotEqual(videoModel.smID, videoSMID)

    @logFunc()
    def test_deleteStale(self):
        """
        Test deleting stale posts
        """
        bFBPostMediaModel = self.fbTestUserData.bFBStatusPostMediaModel
        bFBPostModel      = bFBPostMediaModel.bFBPostModel

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=bFBPostModel)
            # Verify Create
            aDBS.refresh(fbPostModel)
            fbPostSMID = fbPostModel.smID

            # Try Deleting Stale (with old date, nothing deleted)
            syncStartDate = datetime.utcnow() - timedelta(seconds=10)
            with AccountDatabase.transactionScope(session=aDBS):
                FacebookPostDAO.deleteStale(aDBS=aDBS, fbpSMID=self.popFBPSMID, syncStartDate=syncStartDate)
            fbPostModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=fbPostSMID)

            # Delete Stale (with new date, should be deleted)
            syncStartDate = datetime.utcnow() + timedelta(seconds=10)
            with AccountDatabase.transactionScope(session=aDBS):
                FacebookPostDAO.deleteStale(aDBS=aDBS, fbpSMID=self.popFBPSMID, syncStartDate=syncStartDate)
            fbPostModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=fbPostSMID, raiseOnNone=False)
            self.assertIsNone(fbPostModel)


# ---------------------------- FACEBOOK POST DAO GET ALL TEST CASE ---------- #
class FacebookPostDAOGetAllTestCase(CommonTestCase):
    """
    Test Facebook Post DAO Get All
    """
    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True, facebookPostsFull=True)

    @logFunc()
    def test_compSent(self):
        """
        Test compSent
        """
        analyzedAfterDate = datetime.utcnow() - timedelta(minutes=1)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbPostModelList        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            fbPostModelListCountAD = FacebookPostDAO.getModelListCountAD(fbPostModelList=fbPostModelList,
                                                                         compSentAfterDate=analyzedAfterDate)
            nCompSentDoneDB        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, joinMessage=True,
                                                            containsMessage=True, compSentDone=True, count=True)
            nCompSentNotDoneDB     = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, joinMessage=True,
                                                            containsMessage=True, compSentNotDone=True, count=True)
            nCompSentCantDB        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, joinMessage=True,
                                                            containsMessage=True, compSentCant=True, count=True)
            nCompSentAfterDate     = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, joinMessage=True,
                                                            containsMessage=True, compSentAfterDate=analyzedAfterDate,
                                                            count=True)
            allCountDB             = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, count=True)

            self.assertEqual(fbPostModelListCountAD.nMsgCompSentDone, nCompSentDoneDB)
            self.assertEqual(fbPostModelListCountAD.nMsgCompSentNotDone, nCompSentNotDoneDB)
            self.assertEqual(fbPostModelListCountAD.nMsgCompSentCant, nCompSentCantDB)
            self.assertEqual(fbPostModelListCountAD.nMsgCompSentAfterDate, nCompSentAfterDate)
            self.assertEqual(len(fbPostModelList), allCountDB)
