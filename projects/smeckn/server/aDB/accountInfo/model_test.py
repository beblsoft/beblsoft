#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Account Info Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.accountInfo.dao import AccountInfoDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountInfoModelTestCase(CommonTestCase):
    """
    Test Account Info Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)

    @logFunc()
    def test_rud(self):
        """
        Test reading and deleting
        """
        stripeCustomerID = "customer_230498asdflku2039urjdlfkj"

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Read
            accountInfoModel = AccountInfoDAO.get(aDBS=aDBS)
            self.assertIsNotNone(accountInfoModel)

            # Update
            AccountInfoDAO.update(aDBS=aDBS, accountInfoModel=accountInfoModel,
                                  stripeCustomerID=stripeCustomerID)
            aDBS.commit()
            self.assertEqual(accountInfoModel.stripeCustomerID, stripeCustomerID)

            # Delete
            AccountInfoDAO.delete(aDBS=aDBS)
            aDBS.commit()

            # Verify Delete
            accountInfoModel = AccountInfoDAO.get(aDBS=aDBS, raiseOnNone=False)
            self.assertIsNone(accountInfoModel)

    @logFunc()
    def test_createTwo(self):
        """
        Test trying to create two rows in AccountInfoTable
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                AccountInfoDAO.create(aDBS=aDBS, aID=1)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Able to create two rows in account info table")
