#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Account Info Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from datetime import datetime
import logging
from sqlalchemy import Column, BigInteger, Enum, String
from sqlalchemy.dialects.mysql import DATETIME
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class AccountInfoSingleton(enum.Enum):
    """
    Account Info Singleton Enumeration
    """
    SINGLE = 0


# ----------------------- ACCOUNT INFO MODEL CLASS -------------------------- #
class AccountInfoModel(base):
    """
    AccountInfo Table

    Table only has a single row. The table's purpose is to keep data out of
    the global database so the global database is not swamped with requests.
    """
    __tablename__        = "AccountInfo"
    smID                 = Column(BigInteger, primary_key=True)  # ID
    accountInfoSingleton = Column(Enum(AccountInfoSingleton),
                                  default=AccountInfoSingleton.SINGLE,
                                  nullable=False, unique=True)   # Enforces only one row in table
    createDate           = Column(DATETIME(fsp=6),
                                  default=datetime.utcnow)       # Ex. "2000-01-04 07:53:55.474912"
    aID                  = Column(BigInteger, nullable=False)    # Global Database AccountModel.id
    stripeCustomerID     = Column(String(256))                   # Stripe Customer ID

    def __repr__(self):
        return "[{} aID={}]".format(self.__class__.__name__, self.aID)
