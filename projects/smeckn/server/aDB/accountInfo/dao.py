#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Account Info Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.accountInfo.model import AccountInfoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ACCOUNT INFO DAO CLASS ---------------------------- #
class AccountInfoDAO():
    """
    Account Info Data Access Object
    """

    @staticmethod
    @logFunc()
    def create(aDBS, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        accountInfoModel = AccountInfoModel()
        if "createDate" not in kwargs:
            kwargs["createDate"] = datetime.utcnow()
        for k, v in kwargs.items():
            setattr(accountInfoModel, k, v)
        aDBS.add(accountInfoModel)
        return accountInfoModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(aDBS):
        """
        Delete all objects
        """
        aDBS.query(AccountInfoModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, accountInfoModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(accountInfoModel, k, v)
        aDBS.add(accountInfoModel)
        return accountInfoModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def get(aDBS, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object
        """
        accountInfoModel = aDBS.query(AccountInfoModel).one_or_none()
        if not accountInfoModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return accountInfoModel
