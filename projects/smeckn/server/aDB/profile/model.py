#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Profile Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging
from datetime import datetime
from sqlalchemy import Column, BigInteger, ForeignKey, Enum
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class ProfileType(enum.Enum):
    """
    Profile Type Enumeration
    """
    BASE      = 0
    FACEBOOK  = 1
    TWITTER   = 2
    INSTAGRAM = 3
    LINKEDIN  = 4


# ----------------------- PROFILE MODEL CLASS ------------------------------- #
class ProfileModel(base):
    """
    Profile Table
    """
    __tablename__          = "Profile"
    smID                   = Column(BigInteger, primary_key=True)  # ID
    creationDate           = Column(DATETIME(fsp=6),
                                    default=datetime.utcnow)       # Creation Date
    type                   = Column(Enum(ProfileType))             # ProfileType: FACEBOOK, TWITTER, ...

    # Profile Joined Inheritance
    # - Profile serves as base table for all other profiles
    # - Each subclass (ex. FacebookProfileModel) will have an additional table to
    #   store subclass-specific attrs
    __mapper_args__        = {
        'polymorphic_on': type,
        'polymorphic_identity': ProfileType.BASE
    }

    # 1 Profile Group : N Profiles
    profileGroupID         = Column(BigInteger,
                                    ForeignKey("ProfileGroup.id", onupdate="CASCADE", ondelete="CASCADE"),
                                    nullable=False)
    profileGroup           = relationship("ProfileGroupModel", back_populates="profiles", single_parent=True)

    # Child Syncs
    # 1 Profile : N Syncs
    syncModelList          = relationship("SyncModel", back_populates="profileModel",
                                          passive_deletes=True)

    # Child Analyses
    # 1 Profile : N Analyses
    analysisModelList      = relationship("AnalysisModel", back_populates="profileModel",
                                          passive_deletes=True)

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
