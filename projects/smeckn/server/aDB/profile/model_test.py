#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Profile Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.aDB.profile.dao import ProfileDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class ProfileModelTestCase(CommonTestCase):
    """
    Test Profile Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)

    @logFunc()
    def test_cascadeDelete(self):
        """
        Test that profiles are deleted when parent profile group is deleted
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            pModelList = ProfileDAO.getAll(aDBS=aDBS, profileGroupID=self.popPGID)
            self.assertGreater(len(pModelList), 0)

            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=self.popPGID)
            aDBS.commit()

            pModelList = ProfileDAO.getAll(aDBS=aDBS, profileGroupID=self.popPGID)
            self.assertFalse(pModelList)

    @logFunc()
    def test_nullPGID(self):
        """
        Test trying to create profile with NULL pgID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                ProfileDAO.create(aDBS, profileGroupID=None)
                aDBS.commit()
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created profile with null pgID")

    @logFunc()
    def test_badPGID(self):
        """
        Test trying to create profile with bad pgID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                ProfileDAO.create(aDBS, profileGroupID=23987239487)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created profile with invalid pgID")
