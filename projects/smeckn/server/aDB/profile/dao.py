#!/usr/bin/env python3
"""
 NAME
  dao.py

 DESCRIPTION
  Profile Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.profile.model import ProfileModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PROFILE DAO CLASS --------------------------------- #
class ProfileDAO():
    """
    Profile Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, profileGroupID):
        """
        Create object
        """
        pModel = ProfileModel(profileGroupID=profileGroupID)
        aDBS.add(pModel)
        return pModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):
        """
        Delete object
        """
        aDBS.query(ProfileModel).filter(ProfileModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(ProfileModel).delete()

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):
        """
        Return object
        """
        pModel = aDBS.query(ProfileModel)\
            .filter(ProfileModel.smID == smID).one_or_none()
        if not pModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.PROFILE_DOES_NOT_EXIST)
        return pModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, profileGroupID=None, createdAfterDate=None, count=False):
        """
        Return all objects

        Args
          profileGroupID:
            If specifed, return profiles only in profile group
          createdAfterDate:
            If specified, return profiles created after date
        """
        q = aDBS.query(ProfileModel)

        # Apply filters
        if profileGroupID:
            q = q.filter(ProfileModel.profileGroupID == profileGroupID)
        if createdAfterDate:
            q = q.filter(ProfileModel.creationDate >= createdAfterDate)

        return q.count() if count else q.all()

    # ---------------------- LOCKING ---------------------------------------- #
    @staticmethod
    @logFunc()
    def lockSMID(aDBS, smID, update=True):
        """
        Lock row
        Args
          update:
            If True, lock row for update
            Else, lock for read
        """
        aDBS.execute("SELECT * from Profile where smID={} {}".format(
            smID, "FOR UPDATE" if update else "LOCK IN SHARE MODE"))
