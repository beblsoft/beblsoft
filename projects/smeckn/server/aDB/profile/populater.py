#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Profile Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- PROFILE POPULATER CLASS --------------------------- #
class ProfilePopulater(CommonPopulater):
    """
    Profile Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, pgID, nProfiles):
        """
        Populate N profiles

        Returns
          Profile Model List
        """
        pModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(0, nProfiles):
                ProfileDAO.create(aDBS=aDBS, profileGroupID=pgID)
            aDBS.commit()
            pModelList = ProfileDAO.getAll(aDBS=aDBS)
        return pModelList


    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            ProfileDAO.deleteAll(aDBS=aDBS)
