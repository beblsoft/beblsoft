#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Sync Content Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB.syncContent.model import SyncContentModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- SYNC CONTENT DAO CLASS ---------------------------- #
class SyncContentDAO():
    """
    Sync Content Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, syncSMID, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        syncContentModel = SyncContentModel(syncSMID=syncSMID)
        for k, v in kwargs.items():
            setattr(syncContentModel, k, v)
        aDBS.add(syncContentModel)
        return syncContentModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(SyncContentModel).filter(SyncContentModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(SyncContentModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, syncContentModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(syncContentModel, k, v)
        aDBS.add(syncContentModel)
        return syncContentModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        syncContentModel = aDBS.query(SyncContentModel)\
            .filter(SyncContentModel.smID == smID)\
            .one_or_none()
        if not syncContentModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return syncContentModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, syncSMID=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        """
        q = aDBS.query(SyncContentModel)

        # Filters
        if syncSMID:
            q = q.filter(SyncContentModel.syncSMID == syncSMID)

        return q.count() if count else q.all()

    # ---------------------- LOCKING ---------------------------------------- #
    @staticmethod
    @logFunc()
    def lockSMID(aDBS, smID, update=True):
        """
        Lock row
        Args
          update:
            If True, lock row for update
            Else, lock for read
        """
        aDBS.execute("SELECT * from SyncContent where smID={} {}".format(
            smID, "FOR UPDATE" if update else "LOCK IN SHARE MODE"))
