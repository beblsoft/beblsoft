#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Sync Content Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import random
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.syncContent.dao import SyncContentDAO
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class SyncContentModelTestCase(CommonTestCase):
    """
    Test Sync Content Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True,
                      syncs=True, syncContent=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            contentType      = random.choice(list(ContentType))
            status           = random.choice(list(SyncStatus))
            syncContentModel = SyncContentDAO.create(
                aDBS        = aDBS,
                syncSMID    = self.popSyncSMID,
                contentType = contentType,
                status      = status)
            aDBS.commit()

            # Get
            initialSyncContentCount = SyncContentDAO.getAll(aDBS, count=True)

            # Delete
            SyncContentDAO.deleteBySMID(aDBS=aDBS, smID=syncContentModel.smID)
            aDBS.commit()

            # Verify Delete
            syncContentCount = SyncContentDAO.getAll(aDBS, count=True)
            self.assertEqual(syncContentCount, initialSyncContentCount - 1)

    @logFunc()
    def test_syncCascadeDelete(self):
        """
        Test deleting sync cascades down to sync content
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Get
            syncContentCount = SyncContentDAO.getAll(aDBS, count=True)
            self.assertGreater(syncContentCount, 0)

            # Delete
            SyncDAO.deleteBySMID(aDBS=aDBS, smID=self.popSyncSMID)
            aDBS.commit()

            # Verify Delete
            syncContentCount = SyncContentDAO.getAll(aDBS, count=True)
            self.assertEqual(syncContentCount, 0)

    @logFunc()
    def test_nullSyncSMID(self):
        """
        Test trying to create syncContnet with NULL syncSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                SyncContentDAO.create(
                    aDBS        = aDBS,
                    syncSMID    = None)
                aDBS.commit()
            except sqlalchemy.exc.OperationalError as _:
                aDBS.rollback()
            else:
                raise Exception("Created sync with null syncSMID")

    @logFunc()
    def test_badSyncSMID(self):
        """
        Test trying to create syncContent with bad syncSMID
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                SyncContentDAO.create(
                    aDBS        = aDBS,
                    syncSMID    = 1232234987)
                aDBS.commit()
            except sqlalchemy.exc.IntegrityError as _:
                aDBS.rollback()
            else:
                raise Exception("Created syncContent with invalid syncSMID")
