#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Sync Content Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import random
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.dbp.common import CommonPopulater
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.syncContent.dao import SyncContentDAO
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- SYNC CONTENT POPULATER CLASS ---------------------- #
class SyncContentPopulater(CommonPopulater):
    """
    Sync Content Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, syncSMID, count=5):  # pylint: disable=W0102
        """
        Populate N

        Returns
          SyncContentModel List
        """
        syncContentModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            for _ in range(count):
                SyncContentDAO.create(
                    aDBS        = aDBS,
                    syncSMID    = syncSMID,
                    contentType = random.choice(list(ContentType)),
                    status      = random.choice(list(SyncStatus)))
            aDBS.commit()
            syncContentModelList = SyncContentDAO.getAll(aDBS=aDBS)
        return syncContentModelList

    # ---------------------------- POPULATE ONE ----------------------------- #
    @logFunc()
    def populateOne(self, aID, syncSMID, contentType, status=SyncStatus.IN_PROGRESS):
        """
        Populate One

        Returns
          SyncContentModel
        """
        syncContentModel = None
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            syncContentModel = SyncContentDAO.create(
                aDBS        = aDBS,
                syncSMID    = syncSMID,
                contentType = contentType,
                status      = status)
            aDBS.commit()
            aDBS.refresh(syncContentModel)
        return syncContentModel

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            SyncContentDAO.deleteAll(aDBS=aDBS)
