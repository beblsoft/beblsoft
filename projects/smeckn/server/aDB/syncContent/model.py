#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Sync Content Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from sqlalchemy import Column, BigInteger, ForeignKey, Integer, Enum
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB import base
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- SYNC CONTENT MODEL CLASS -------------------------- #
class SyncContentModel(base):
    """
    Sync Content Table
    """
    __tablename__       = "SyncContent"
    smID                = Column(BigInteger, primary_key=True)   # ID

    workerKickDate      = Column(DATETIME(fsp=6))                # Date worker was last kicked
    workerHeartbeatDate = Column(DATETIME(fsp=6))                # Worker heartbeat
    completeDate        = Column(DATETIME(fsp=6))                # Date completed

    contentType         = Column(Enum(ContentType))              # Ex. ContentType.POST
    status              = Column(Enum(SyncStatus),
                                 default=SyncStatus.IN_PROGRESS) # Ex. SyncStatus.IN_PROGRESS
    nWorkerKicks        = Column(Integer, default=0)             # Number of times worker has been spawned

    # Parent Sync
    # 1 Sync : N SyncContent
    syncSMID            = Column(BigInteger,
                                 ForeignKey("Sync.smID", onupdate="CASCADE", ondelete="CASCADE"),
                                 nullable=False)
    syncModel           = relationship("SyncModel", back_populates="syncContentModelList")

    def __repr__(self):
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
