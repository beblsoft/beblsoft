#!/usr/bin/env python3
"""
 NAME
  populater.py

 DESCRIPTION
  Facebook Profile Populater Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import random
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO
from smeckn.server.dbp.common import CommonPopulater


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK PROFILE POPULATER CLASS ------------------ #
class FacebookProfilePopulater(CommonPopulater):
    """
    Facebook Profile Populater
    """

    # ---------------------------- POPULATE --------------------------------- #
    @logFunc()
    def populateN(self, aID, pgID, nFBProfiles, maxFBID=1000000000):
        """
        Populate N facebook profiles

        Returns
          Facebook Profile Model List
        """
        fbIDList = []
        for _ in range(0, nFBProfiles):
            fbID = random.randint(0, maxFBID)
            fbIDList.append(fbID)
        return self.populateList(aID=aID, pgID=pgID, fbIDList=fbIDList)

    @logFunc()
    def populateOne(self, aID, pgID, fbID, namePrefix="FacebookProfileName",
                    email="foo@bar.com", gender="female", birthdayDate=None,
                    hometown="Attleboro, MA"):  # pylint: disable=W0102
        """
        Populate One

        Returns
          Facebook Profile Model
        """
        fbpModel     = None
        birthdayDate = birthdayDate if birthdayDate else datetime.utcnow()
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                FacebookProfileDAO.create(aDBS=aDBS, pgID=pgID, fbID=fbID, name="{}{}".format(namePrefix, fbID),
                                          email=email, gender=gender, birthdayDate=birthdayDate, hometown=hometown)
            fbpModel = FacebookProfileDAO.getByFBID(aDBS=aDBS, fbID=fbID)
        return fbpModel

    @logFunc()
    def populateList(self, aID, pgID, fbIDList=[], namePrefix="FacebookProfileName"):  # pylint: disable=W0102
        """
        Populate list
        Args
          aID:
            Account ID
          pgID:
            Profile Group ID
        Returns
          Facebook Profile Model List
        """
        fbpModelList = []
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                for fbID in fbIDList:
                    FacebookProfileDAO.create(aDBS=aDBS, pgID=pgID, fbID=fbID,
                                              name="{}{}".format(namePrefix, fbID))
            fbpModelList = FacebookProfileDAO.getAll(aDBS=aDBS, pgID=pgID)
        return fbpModelList

    # ---------------------------- DEPOPULATE ------------------------------- #
    @logFunc()
    def depopulateAll(self, aID):  # pylint: disable=W0221
        """
        Depopulate all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID) as aDBS:
            FacebookProfileDAO.deleteAll(aDBS=aDBS)

    # ---------------------------- DESCRIBE --------------------------------- #
    @logFunc()
    def describeAllFromAID(self, aID, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe all
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=aID, commit=False) as aDBS:
            FacebookProfileDAO.describeAll(aDBS=aDBS, tree=tree,
                                           startSpaces=startSpaces)
