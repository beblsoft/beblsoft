#!/usr/bin/env python3
"""
NAME
 dao.py

DESCRIPTION
 Facebook Profile Data Access Object
"""

# ----------------------- IMPORTS ------------------------------------------- #
import pprint
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookProfile.model import FacebookProfileModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK PROFILE DAO CLASS ------------------------ #
class FacebookProfileDAO():
    """
    Facebook Profile Data Access Object
    """

    # ---------------------- CREATION --------------------------------------- #
    @staticmethod
    @logFunc()
    def create(aDBS, pgID, fbID, **kwargs):  # pylint: disable=W0221
        """
        Create object
        Args
          aDBS:
            Account Database Session
          pgID:
            Profile Group ID
          fbID:
            Facebook ID
          kwargs:
            Dictionary of arguments

        Note:
          Profile Tables are locked for the duration of the transaction
        """
        AccountDatabase.lockAssert(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE")
        FacebookProfileDAO.validateNoFBPsInPG(aDBS=aDBS, pgID=pgID)
        FacebookProfileDAO.validateFBPDoesntExist(aDBS=aDBS, fbID=fbID)

        fbpModel = FacebookProfileModel(profileGroupID=pgID, fbID=fbID)
        for k, v in kwargs.items():
            setattr(fbpModel, k, v)
        aDBS.add(fbpModel)
        return fbpModel

    @staticmethod
    @logFunc()
    def createFromBFBUserModel(aDBS, pgID, bFBUserModel, longTermAccessToken):
        """
        Create from BFacebookUserModel
        """
        fbpModel = FacebookProfileDAO.create(aDBS=aDBS, pgID=pgID, fbID=bFBUserModel.id)
        FacebookProfileDAO.updateFromBFBUserModel(aDBS=aDBS, fbpModel=fbpModel, bFBUserModel=bFBUserModel)
        FacebookProfileDAO.update(aDBS=aDBS, fbpModel=fbpModel,
                                  longTermAccessToken=longTermAccessToken)
        return fbpModel

    # ---------------------- DELETION --------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteBySMID(aDBS, smID):  # pylint: disable=W0221
        """
        Delete object
        """
        aDBS.query(FacebookProfileModel).filter(FacebookProfileModel.smID == smID).delete()

    @staticmethod
    @logFunc()
    def deleteAll(aDBS):
        """
        Delete all objects
        """
        aDBS.query(FacebookProfileModel).delete()

    # ---------------------- UPDATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def update(aDBS, fbpModel, **kwargs):
        """
        Update object
        """
        for k, v in kwargs.items():
            setattr(fbpModel, k, v)
        aDBS.add(fbpModel)
        return fbpModel

    @staticmethod
    @logFunc()
    def updateFromBFBUserModel(aDBS, fbpModel, bFBUserModel):
        """
        Update object
        """
        fbpModel.syncDate     = datetime.utcnow()
        fbpModel.name         = bFBUserModel.name
        fbpModel.email        = bFBUserModel.email
        fbpModel.gender       = bFBUserModel.gender
        fbpModel.birthdayDate = bFBUserModel.birthday
        fbpModel.hometown     = bFBUserModel.hometown
        aDBS.add(fbpModel)
        return fbpModel

    # ---------------------- RETRIEVAL -------------------------------------- #
    @staticmethod
    @logFunc()
    def getBySMID(aDBS, smID, raiseOnNone=True):  # pylint: disable=W0221,W0622
        """
        Retrieve object by id
        """
        fbpModel = aDBS.query(FacebookProfileModel)\
            .filter(FacebookProfileModel.smID == smID)\
            .one_or_none()
        if not fbpModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbpModel

    @staticmethod
    @logFunc()
    def getByFBID(aDBS, fbID, raiseOnNone=True):  # pylint: disable=W0622
        """
        Retrieve object by facebook id
        """
        fbpModel = aDBS.query(FacebookProfileModel)\
            .filter(FacebookProfileModel.fbID == fbID)\
            .one_or_none()
        if not fbpModel and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return fbpModel

    @staticmethod
    @logFunc()
    def getAll(aDBS, pgID=None, createdAfterDate=None, count=False):  # pylint: disable=W0221
        """
        Return all objects
        Args
          pgID:
            If specifed, return profiles only in profile group
          createdAfterDate:
            If specified, return profiles created after date
        """
        q = aDBS.query(FacebookProfileModel)

        # Filters
        if pgID:
            q = q.filter(FacebookProfileModel.profileGroupID == pgID)
        if createdAfterDate:
            q = q.filter(FacebookProfileModel.creationDate >= createdAfterDate)

        return q.count() if count else q.all()

    # ---------------------- DESCRIBE --------------------------------------- #
    @staticmethod
    @logFunc()
    def describeAll(aDBS, pgID=None, tree=True, startSpaces=0):  # pylint: disable=W0221
        """
        Describe all
        """
        fbpModelList = FacebookProfileDAO.getAll(aDBS=aDBS, pgID=pgID)
        for fbpModel in fbpModelList:
            FacebookProfileDAO.describeFBPModel(aDBS=aDBS, fbpModel=fbpModel,
                                                tree=tree, startSpaces=startSpaces)

    @staticmethod
    @logFunc()
    def describeFBPModel(aDBS, fbpModel, tree=True, startSpaces=0):  # pylint: disable=W0613
        """
        Describe Object
        """
        logger.info("{}{}".format(" " * startSpaces, pprint.pformat(fbpModel)))

    # ---------------------- VALIDATION ------------------------------------- #
    @staticmethod
    @logFunc()
    def validateNoFBPsInPG(aDBS, pgID):
        """
        Validate that profile group isn't associated with any facebook profiles
        """
        existingFBPList = FacebookProfileDAO.getAll(aDBS=aDBS, pgID=pgID)
        if existingFBPList:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_PROFILE_ALREADY_IN_PG)

    @staticmethod
    @logFunc()
    def validateFBPDoesntExist(aDBS, fbID):
        """
        Validate that facebook profile doesn't already exist in a pg
        """
        existingFBP = FacebookProfileDAO.getByFBID(aDBS=aDBS, fbID=fbID, raiseOnNone=False)
        if existingFBP:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_PROFILE_ID_EXISTS_DIFPG)
