#!/usr/bin/env python3
"""
 NAME:
  model_test.py

 DESCRIPTION
  Facebook Profile Model Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class FacebookProfileModelTestCase(CommonTestCase):
    """
    Test Facebook Profile Model
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)

    @logFunc()
    def test_crud(self):
        """
        Test creating and deleting facebook profile
        """
        pgID                = self.nonPopPGID
        fbID                = 100000000002342
        name                = "Johnny"
        email               = "johnny@gmail.com"
        gender              = "male"
        birthdayDate        = datetime.utcnow()
        hometown            = "Attleboro, Massachusetts"
        longTermAccessToken = "EAANJGs3P5J0BADgWVQ3TM8HL2ytZBu0LIn8v132jvfaLzFHVNK6zq4WHtzcwuobl6qkCNTItLiaQXBBxELItQiSPPh4ZAT5fktJ2dViH4ZB4KZBMoGhmcMzZCvZCLs5qcCy6y8fgk46FLjjGrkqEF6oi9CrWsUFgH1PL3VutJzuJAZBdWMnWbf5"

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            # Create
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                FacebookProfileDAO.create(aDBS, pgID=pgID, fbID=fbID, name=name, email=email,
                                          gender=gender, birthdayDate=birthdayDate, hometown=hometown,
                                          longTermAccessToken=longTermAccessToken)

            # Get
            fbpModel = FacebookProfileDAO.getByFBID(aDBS, fbID=fbID)
            self.assertEqual(fbpModel.fbID, fbID)
            self.assertEqual(fbpModel.name, name)
            self.assertEqual(fbpModel.email, email)
            self.assertEqual(fbpModel.gender, gender)
            self.assertEqual(fbpModel.birthdayDate, birthdayDate)
            self.assertEqual(fbpModel.hometown, hometown)
            self.assertEqual(fbpModel.email, email)
            self.assertEqual(fbpModel.longTermAccessToken, longTermAccessToken)

            # Delete
            FacebookProfileDAO.deleteBySMID(aDBS=aDBS, smID=fbpModel.smID)
            aDBS.commit()

            # Verify Delete
            fbpModel = FacebookProfileDAO.getByFBID(aDBS, fbID=fbID, raiseOnNone=False)
            self.assertFalse(fbpModel)

    @logFunc()
    def test_pgCascadeDelete(self):
        """
        Test profile group deletions cascade down to facebook profile
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            pModelList = FacebookProfileDAO.getAll(aDBS=aDBS, pgID=self.popPGID)
            self.assertGreater(len(pModelList), 0)

            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=self.popPGID)
            aDBS.commit()

            fbpModelList = FacebookProfileDAO.getAll(aDBS=aDBS, pgID=self.popPGID)
            self.assertFalse(fbpModelList)

    @logFunc()
    def test_twoFBPsSamePG(self):
        """
        Test trying to create two FBPs in the same PG
        """
        fbID1 = 100000000002342
        fbID2 = 100000000002343
        pgID  = self.nonPopPGID

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                FacebookProfileDAO.create(aDBS, pgID=pgID, fbID=fbID1)
                aDBS.commit()
                try:
                    FacebookProfileDAO.create(aDBS, pgID=pgID, fbID=fbID2)
                    aDBS.commit()
                except BeblsoftError as e:
                    self.assertEqual(e.code, BeblsoftErrorCode.FACEBOOK_PROFILE_ALREADY_IN_PG)
                else:
                    raise Exception("Two fbp's created in same pg")

    @logFunc()
    def test_sameFBPDifPG(self):
        """
        Test trying to create the same FBP in two different PGs
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                fbpModelList = FacebookProfileDAO.getAll(aDBS, pgID=self.popPGID)
                self.assertEqual(len(fbpModelList), 1)
                fbpModel    = fbpModelList[0]
                with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                    FacebookProfileDAO.create(aDBS, pgID=self.nonPopPGID, fbID=fbpModel.fbID)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.FACEBOOK_PROFILE_ID_EXISTS_DIFPG)
            else:
                raise Exception("Same fbp in different pgs")
