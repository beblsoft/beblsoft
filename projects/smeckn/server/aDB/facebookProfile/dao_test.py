#!/usr/bin/env python3
"""
 NAME:
  dao_test.py

 DESCRIPTION
  Facebook Profile DAO Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class FacebookProfileDAOTestCase(CommonTestCase):
    """
    Test Facebook Profile DAO
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True,
                      facebookTestUsers=True, facebookTestUserData=True)
        fbTestUserName      = "small"
        self.fbTestUser     = self.fbTestUserDict[fbTestUserName]
        self.fbTestUserData = self.fbTestUserDataDict[fbTestUserName]

    @logFunc()
    def test_profileCRUD(self):
        """
        Test profile crud
        """
        bFBUserModel        = self.fbTestUserData.bFBUserModel
        longTermAccessToken = self.fbTestUser.accessToken

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
                fbpModel = FacebookProfileDAO.createFromBFBUserModel(aDBS=aDBS, pgID=self.popPGID, bFBUserModel=bFBUserModel,
                                                                     longTermAccessToken=longTermAccessToken)
            # Verify Create
            aDBS.refresh(fbpModel)
            self.assertEqual(fbpModel.fbID, bFBUserModel.id)
            self.assertEqual(fbpModel.name, bFBUserModel.name)
            self.assertEqual(fbpModel.email, bFBUserModel.email)
            self.assertEqual(fbpModel.gender, bFBUserModel.gender)
            self.assertEqual(fbpModel.birthdayDate, bFBUserModel.birthday)
            self.assertEqual(fbpModel.hometown, bFBUserModel.hometown)
            self.assertGreaterEqual(fbpModel.syncDate, datetime.utcnow() - timedelta(seconds=1))

            # Update
            bFBUserModel.name = "New Name!"
            with AccountDatabase.transactionScope(session=aDBS):
                fbpModel = FacebookProfileDAO.updateFromBFBUserModel(aDBS=aDBS, fbpModel=fbpModel,
                                                                     bFBUserModel=bFBUserModel)

            # Verify Update
            aDBS.refresh(fbpModel)
            self.assertEqual(fbpModel.name, bFBUserModel.name)
