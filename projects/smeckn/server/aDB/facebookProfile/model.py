#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Facebook Profile Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from sqlalchemy import Column, BigInteger, ForeignKey, String, Text
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.orm import relationship
from smeckn.server.aDB.profile.model import ProfileModel
from smeckn.server.aDB.profile.model import ProfileType


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- FACEBOOK PROFILE MODEL CLASS ---------------------- #
class FacebookProfileModel(ProfileModel):
    """
    Facebook Profile Table
    """
    __tablename__       = 'FacebookProfile'
    smID                = Column(ForeignKey('Profile.smID', onupdate="CASCADE", ondelete="CASCADE"),
                                 primary_key=True)
    syncDate            = Column(DATETIME(fsp=6),
                                 default=datetime.utcnow)    # Date last synced

    # Ineritance Mapping to Profile
    __mapper_args__     = {'polymorphic_identity': ProfileType.FACEBOOK}

    # Facebook Data
    fbID                = Column(BigInteger, unique=True)       # Profile ID from Facebook
    name                = Column(String(256), default=None)     # Ex. "John Williams"
    email               = Column(String(256), default=None)     # Ex. "johnwilliams@gmail.com"
    gender              = Column(String(256), default=None)     # Ex. 'male'
    birthdayDate        = Column(DATETIME(fsp=6), default=None) # Ex. "2000-01-04 07:53:55.474912"
    hometown            = Column(String(256), default=None)     # "Attleboro, MA"
    longTermAccessToken = Column(Text, default=None)            # Ex. EAAQ1SZBYJWEUBAHY8CxUcYk364SGXGMcM6FDQV...

    # Child Posts
    # 1 Facebook Profile : N Facebook Posts
    postModelList       = relationship("FacebookPostModel", back_populates="fbpModel",
                                       passive_deletes=True)

    # Child Photos
    # 1 Facebook Profile : N Facebook Photos
    photoModelList      = relationship("FacebookPhotoModel", back_populates="fbpModel",
                                       passive_deletes=True)

    def __str__(self):
        """
        Note: don't put FacebookProfile specific fields Here
        This will cause exceptions when the following happens during sync:
          - Profile Table locked (not FacebookProfileTable)
          - Profile is loaded
          - Profile is printed, FacebookProfile is queryed, causes select on non-locked table
        """
        return "[{} smID={}]".format(self.__class__.__name__, self.smID)
