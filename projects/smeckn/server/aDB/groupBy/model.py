#!/usr/bin/env python3
"""
NAME
 model.py

DESCRIPTION
 Group By Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum


# ------------------------- GROUP BY RESULT TYPE ---------------------------- #
class GroupByResultType(enum.Enum):
    """
    Group By Resilt Type
    """
    DATE              = 0  # datetime.datetime
    CONTENT_ATTR_TYPE = 1  # ContentAttrType


# ------------------------- GROUP BY TYPE ----------------------------------- #
class GroupByType(enum.Enum):
    """
    Group By Type
    """
    FACEBOOK_POST_CREATE_DATE              = 0   # 0   -  99 Facebook Post Reserve
    COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT  = 100 # 100 - 199 Comprehend Sentiment Reserve
