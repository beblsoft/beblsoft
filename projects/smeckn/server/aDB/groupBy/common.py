#!/usr/bin/env python3
"""
NAME
 common.py

DESCRIPTION
 Common Group By Functionality

HISTOGRAM USAGE
  CommonHistogramDAO.get
    groupByType         = histogramCtx.groupByType
    groupByClass        = CommonGroupBy.getClassFromType(groupByType=groupByType)
    groupByResultType   = groupByClass.groupByResultType
    groupByDateInterval = GroupByDateInterval.getFromDateInterval(startDate=startDate, endDate=endDate)
    groupByDateInterval = groupByDateInterval if groupByResultType == GroupByResultType.DATE else None
    ...
    q = aDBS.query(groupByExpression, ...)
    ...
    (groupByMySQLResult, ...) = data
    groupByResult = groupByClass.mapFromMySQLResult(result=groupByMySQLResult)
"""

# ------------------------- IMPORTS ----------------------------------------- #
import abc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- COMMON GROUP BY --------------------------------- #
class CommonGroupBy(abc.ABC):
    """
    Common Group By Class
    """

    # ------------------------- ABSTRACT DATA ------------------------------- #
    groupByResultType = None
    groupByType       = None
    column            = None

    @abc.abstractclassmethod
    def mapToMySQLExpression(cls, groupByDateInterval=None):
        """
        Map Model Attribute to MySQLExpression
        Args
          column:
            Column attribute
            Ex. FacebookPost.createDate

        Returns
          Expression that can be passed to sqlalchemy query.group_by()
          Ex. func.date_format(FacebookPost.createDate, "%Y")
        """
        pass

    @abc.abstractclassmethod
    def mapFromMySQLResult(cls, result, groupByDateInterval=None):
        """
        Map mysql result
        Args
          result:
            MySQL result
            Ex1. "2018"
            Ex2. ConpSentType.POSITIVE

        Returns
          Instance of GroupByType object
          Ex. if groupByType == GroupByType.DATE, returns datetime
        """
        pass

    # ------------------------- GET CLASS ----------------------------------- #
    @classmethod
    @logFunc()
    def getClassFromType(cls, groupByType, raiseOnNone=True):
        """
        Return GroupBy class from type
        """
        subCls = None
        for curSubCls in cls.__subclasses__():
            if curSubCls.groupByType == groupByType:
                subCls = curSubCls
                break
        if not subCls and raiseOnNone:
            raise BeblsoftError(BeblsoftErrorCode.PROFILE_INVALID_GROUP_BY,
                                extMsgFormatDict={"groupByType": groupByType.name})
        return subCls
