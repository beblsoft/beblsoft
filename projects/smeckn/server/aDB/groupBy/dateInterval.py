#!/usr/bin/env python3
"""
NAME
 model.py

DESCRIPTION
 Group By Date Interval Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum
import logging
from datetime import datetime, timedelta


# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)



# ------------------------- GROUP BY DATE INTERVAL -------------------------- #
class GroupByDateInterval(enum.Enum):
    """
    Group By Date Interval
    """
    SECOND = 1
    MINUTE = 2
    HOUR   = 3
    DAY    = 4
    # WEEK = 5 # Don't know how to roundDown
    MONTH  = 6
    YEAR   = 7

    @property
    def mysqlStrFormat(self):
        """
        Return mysql str format
        """
        formatDict = {
            # Dictionary mapping: GroupByDateInterval => mysqlDateFormat
            #   mysqlDateFormat:
            #     MySQL date format string
            #     Maps mysqlDate -> mysqlDateString
            #     See: https://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_date-format
            GroupByDateInterval.SECOND: "%Y-%m-%d %H:%i:%s",
            GroupByDateInterval.MINUTE: "%Y-%m-%d %H:%i",
            GroupByDateInterval.HOUR: "%Y-%m-%d %H",
            GroupByDateInterval.DAY: "%Y-%m-%d",
            GroupByDateInterval.MONTH: "%Y-%m",
            GroupByDateInterval.YEAR: "%Y"
        }
        return formatDict[self]

    @property
    def pythonDateFormat(self):
        """
        Return datetime from mysql
        """
        formatDict = {
            # Dictionary mapping: GroupByDateInterval => pythonDateFormat
            #   pythonDateFormat:
            #     Python date format string
            #     Maps mysqlDateString -> python datetime object
            #     See: https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
            GroupByDateInterval.SECOND: "%Y-%m-%d %H:%M:%S",
            GroupByDateInterval.MINUTE: "%Y-%m-%d %H:%M",
            GroupByDateInterval.HOUR: "%Y-%m-%d %H",
            GroupByDateInterval.DAY: "%Y-%m-%d",
            GroupByDateInterval.MONTH: "%Y-%m",
            GroupByDateInterval.YEAR: "%Y"
        }
        return formatDict[self]

    def mysqlDateStrToDatetime(self, mysqlDateStr):
        """
        Return datetime from mysqlDateStr.
        """
        return datetime.strptime(mysqlDateStr, self.pythonDateFormat)

    def roundDown(self, date):
        """
        Return first date in interval
        """
        rdDate = None
        if self == GroupByDateInterval.SECOND:
            rdDate = date.replace(microsecond=0)
        elif self == GroupByDateInterval.MINUTE:
            rdDate = date.replace(second=0, microsecond=0)
        elif self == GroupByDateInterval.HOUR:
            rdDate = date.replace(minute=0, second=0, microsecond=0)
        elif self == GroupByDateInterval.DAY:
            rdDate = date.replace(hour=0, minute=0, second=0, microsecond=0)
        elif self == GroupByDateInterval.MONTH:
            rdDate = date.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        elif self == GroupByDateInterval.YEAR:
            rdDate = date.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        assert rdDate
        return rdDate

    def getNext(self, curDate):
        """
        Get next datetime after curDate
        """
        nextDate = None
        if self == GroupByDateInterval.SECOND:
            nextDate = curDate + timedelta(seconds=1)
        elif self == GroupByDateInterval.MINUTE:
            nextDate = curDate + timedelta(minutes=1)
        elif self == GroupByDateInterval.HOUR:
            nextDate = curDate + timedelta(hours=1)
        elif self == GroupByDateInterval.DAY:
            nextDate = curDate + timedelta(days=1)
        elif self == GroupByDateInterval.MONTH:
            curMonth  = curDate.month
            curYear   = curDate.year
            nextMonth = 1 if curMonth == 12 else curMonth + 1
            nextYear  = curYear + 1 if curMonth == 12 else curYear
            nextDate  = datetime(year=nextYear, month=nextMonth, day=1)
        elif self == GroupByDateInterval.YEAR:
            nextDate = datetime(year=curDate.year + 1, month=1, day=1)
        assert nextDate
        return nextDate

    def getPrev(self, curDate):
        """
        Get previous datetime before curDate
        """
        prevDate = None
        if self == GroupByDateInterval.SECOND:
            prevDate = curDate - timedelta(seconds=1)
        elif self == GroupByDateInterval.MINUTE:
            prevDate = curDate - timedelta(minutes=1)
        elif self == GroupByDateInterval.HOUR:
            prevDate = curDate - timedelta(hours=1)
        elif self == GroupByDateInterval.DAY:
            prevDate = curDate - timedelta(days=1)
        elif self == GroupByDateInterval.MONTH:
            curMonth  = curDate.month
            curYear   = curDate.year
            prevMonth = 12 if curMonth == 1 else curMonth - 1
            prevYear  = curYear - 1 if curMonth == 1 else curYear
            prevDate  = datetime(year=prevYear, month=prevMonth, day=1)
        elif self == GroupByDateInterval.YEAR:
            prevDate = datetime(year=curDate.year - 1, month=1, day=1)
        assert prevDate
        return prevDate

    @staticmethod
    def getFromDateInterval(startDate, endDate):
        """
        Return GroupByDateInterval from date interval
        """
        delta               = endDate - startDate
        groupByDateInterval = None

        if delta < timedelta(seconds=120):
            groupByDateInterval = GroupByDateInterval.SECOND
        elif delta < timedelta(minutes = 120):
            groupByDateInterval = GroupByDateInterval.MINUTE
        elif delta < timedelta(hours = 48):
            groupByDateInterval = GroupByDateInterval.HOUR
        elif delta < timedelta(days = 120):
            groupByDateInterval = GroupByDateInterval.DAY
        elif delta < timedelta(weeks = 52 * 2):
            groupByDateInterval = GroupByDateInterval.MONTH
        else:
            groupByDateInterval = GroupByDateInterval.YEAR
        return groupByDateInterval
