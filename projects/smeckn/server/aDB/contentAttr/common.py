#!/usr/bin/env python3
"""
NAME
 common.py

DESCRIPTION
 Common Content Attr Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import abc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- COMMON CONTENT ATTR ----------------------------- #
class CommonContentAttr(abc.ABC):
    """
    Common Content Attr Class

    Creates an association between a content attribute and a column
    """

    # ------------------------- ABSTRACT DATA ------------------------------- #
    contentAttrType = None
    column          = None
    numeric         = None # If True, this is a Numeric type [sum, min, max, stddev, average all work]

    # ------------------------- GET CLASS ----------------------------------- #
    @classmethod
    @logFunc()
    def getClassFromType(cls, contentAttrType, raiseOnNone=True):
        """
        Return ContentAttrClass from contentAttrType
        """
        subCls = None
        for curSubCls in cls.__subclasses__():
            if curSubCls.contentAttrType == contentAttrType:
                subCls = curSubCls
                break
        if not subCls and raiseOnNone:
            raise BeblsoftError(BeblsoftErrorCode.PROFILE_INVALID_CONTENTATTR,
                                extMsgFormatDict={"contentAttrType": contentAttrType.name})
        return subCls
