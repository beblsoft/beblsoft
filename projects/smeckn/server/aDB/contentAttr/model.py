#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Content Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import logging


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- ENUMERATIONS -------------------------------------- #
class ContentAttrType(enum.Enum):
    """
    Content Attribute Type Enumeration
    """
    FACEBOOK_POST                              = 0     # 0 -  99 Facebook Post Reserve
    FACEBOOK_POST_SMID                         = 1
    FACEBOOK_POST_NLIKES                       = 2
    FACEBOOK_POST_NREACTIONS                   = 3
    FACEBOOK_POST_NCOMMENTS                    = 4

    COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT      = 100   # 100 - 199 Comprehend Sentiment Reserve
    COMP_SENT_ANALYSIS_ALL_SCORE               = 101
    COMP_SENT_ANALYSIS_POSITIVE_SCORE          = 102
    COMP_SENT_ANALYSIS_NEGATIVE_SCORE          = 103
    COMP_SENT_ANALYSIS_NEUTRAL_SCORE           = 104
    COMP_SENT_ANALYSIS_MIXED_SCORE             = 105
    COMP_SENT_TYPE_POSITIVE                    = 106
    COMP_SENT_TYPE_NEGATIVE                    = 107
    COMP_SENT_TYPE_NEUTRAL                     = 108
    COMP_SENT_TYPE_MIXED                       = 109
