#!/usr/bin/env python3
"""
NAME
 common.py

DESCRIPTION
 Common Sort Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import abc
from sqlalchemy.sql import or_, and_
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.sort.order import SortOrder


# ------------------------- COMMON SORT ------------------------------------- #
class CommonSort(abc.ABC):
    """
    Common Sort Class
    """
    # ------------------------- ABSTRACT ------------------------------------ #
    # Sort Type
    # Ex. SortType.FACEBOOK_POST_FBCREATEDATE
    sortType = None

    # Primary column to sort off
    # Ex. FacebookPostModel.fbCreateDate
    pColumn  = None

    # Secondary column to sort off [Not necessary]
    # Useful when pColumn is NOT unique
    # This prevents pagination from skipping or duplicating results
    # Ex. FacebookPostModel.smID
    sColumn  = None

    def __init__(self, pSortOrder, sSortOrder=SortOrder.ASCENDING, pLastValue=None, sLastValue=None,
                 pColumn=None, sColumn=None):
        """
        Initialize object
        Args
          pSortOrder:
            Primary Column Sort Order
            Ex. SortOrder.ASCENDING
          sSortOrder:
            Secondary Column Sort Order
            Ex. SortOrder.ASCENDING
          pLastValue:
            Primary Last Value
            Will be filtered out
          sLastValue:
            Secondary Last Value
            Will be filtered out
          pColumn:
            Primary Column
            To override class is query is using aliasing
          sColumn:
            Secondary Column
            To override class is query is using aliasing
        """
        self._pColumn   = pColumn if pColumn else self.__class__.pColumn  # Underscore to not set class variable
        self._sColumn   = sColumn if sColumn else self.__class__.sColumn
        self.pSortOrder = pSortOrder
        self.sSortOrder = sSortOrder
        self.pLastValue = pLastValue
        self.sLastValue = sLastValue

    @property
    def filterList(self):
        """
        Filter List

        Return list of query filters
        """
        filterList = [self._pColumn != None]

        if self.pLastValue is not None:
            assert self._pColumn
            if self._sColumn: # Sorting off two columns
                filterList.append(
                    or_(
                        and_(
                            self._pColumn == self.pLastValue,
                            self.sSortOrder.nextCompOperator(self._sColumn, self.sLastValue)
                        ),
                        self.pSortOrder.nextCompOperator(self._pColumn, self.pLastValue)
                    )
                )
            else:
                filterList.append(self.pSortOrder.nextCompOperator(self._pColumn, self.pLastValue))

        return filterList

    @property
    def orderByArgs(self):
        """
        Order By Args

        Return args pass to query.order_by(*args)
        """
        rval = [self.pSortOrder.func(self._pColumn)]
        if self._sColumn:
            rval.append(self.sSortOrder.func(self._sColumn))
        return rval

    # ------------------------- GET CLASS ----------------------------------- #
    @classmethod
    @logFunc()
    def getClassFromType(cls, sortType, raiseOnNone=True):
        """
        Return SortClass from sortType
        """
        subCls = None
        for curSubCls in cls.__subclasses__():
            if curSubCls.sortType == sortType:
                subCls = curSubCls
                break
        if not subCls and raiseOnNone:
            raise BeblsoftError(BeblsoftErrorCode.PROFILE_INVALID_SORTTYPE,
                                extMsgFormatDict={"sortType": sortType.name})
        return subCls
