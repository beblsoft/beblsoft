#!/usr/bin/env python3
"""
NAME
 type.py

DESCRIPTION
 Sort Type Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum


# ------------------------- SORT TYPE --------------------------------------- #
class SortType(enum.Enum):
    """
    Sort Types
    """
    FACEBOOK_POST_FBCREATEDATE            = 0     # 0 -  99 Facebook Post Reserve
    FACEBOOK_POST_NLIKES                  = 1
    FACEBOOK_POST_NREACTIONS              = 2
    FACEBOOK_POST_NCOMMENTS               = 3

    COMP_SENT_ANALYSIS_POSITIVE_SCORE     = 100   # 100 - 199 Comprehend Sentiment Reserve
    COMP_SENT_ANALYSIS_NEGATIVE_SCORE     = 101
    COMP_SENT_ANALYSIS_NEUTRAL_SCORE      = 102
    COMP_SENT_ANALYSIS_MIXED_SCORE        = 103

    TEXT_CREATEDATE                       = 200   # 200 - 299 Text Reserve
