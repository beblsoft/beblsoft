#!/usr/bin/env python3
"""
NAME
 order.py

DESCRIPTION
 Sort Order Functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum
import operator
from sqlalchemy import asc, desc


# ------------------------- SORT ORDER -------------------------------------- #
class SortOrder(enum.Enum):
    """
    Sort Orders
    """
    ASCENDING  = 1
    DESCENDING = 2

    @property
    def func(self):
        """
        Return corresponding sqlalchemy func
        """
        funcDict = {
            SortOrder.ASCENDING: asc,
            SortOrder.DESCENDING: desc,
        }
        return funcDict[self]

    @property
    def nextCompOperator(self):
        """
        Next Comparison Operator
        Return operator which must return True for next element in the set
        Assumes all elements are unique

        Ex. SortOrder.ASCENDING
            [1, 2, 3, 4, 5]
            Returns operator.__gt__ as 2 > 1
        """
        compOperatorDict = {
            SortOrder.ASCENDING: operator.__gt__,
            SortOrder.DESCENDING: operator.__lt__
        }
        return compOperatorDict[self]
