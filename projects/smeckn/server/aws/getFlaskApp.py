#!/usr/bin/env python3
"""
 NAME:
  getFlaskApp.py

DESCRIPTION
 AWS Handler to retrieve flask application
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aws import AWSRootObject


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleGetFlaskApp(**kwargs):  # pylint: disable=W0613
    """
    Handle request to return flask application
    """
    awsr = AWSRootObject.getSingleton()
    return awsr.mgr.app.fApp
