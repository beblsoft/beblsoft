#!/usr/bin/env python3
"""
 NAME:
  error.py

DESCRIPTION
 AWS Error Handler
"""

# ----------------------- IMPORTS ------------------------------------------- #
import sys
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.aws import AWSRootObject
from smeckn.server.eCtx.aws import AWSExecutionContext


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleError(eType, event, context):  # pylint: disable=W0613
    """
    Handle an error
    """
    awsr = AWSRootObject.getSingleton()
    mgr  = awsr.mgr

    with AWSExecutionContext(event=event, context=context):
        e = sys.exc_info()[1]
        mgr.sentry.captureError(error=e, bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_ERROR_HANDLER,
                                logMsg="LSError:{}".format(eType.name))
