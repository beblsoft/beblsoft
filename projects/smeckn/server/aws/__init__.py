#!/usr/bin/env python3
"""
 NAME:
  __init__.py

DESCRIPTION
 AWS Smeckn Server Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import os
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.config.aws import AWSConfig
from smeckn.common.secret import SecretType
from smeckn.server.manager import ServerManager


# ----------------------- AWS ROOT OBJECT ----------------------------------- #
class AWSRootObject():
    """
    AWS Root Object Class
    """
    _singletonInstance = None

    def __init__(self, **kwargs):
        """
        Initialize Object
        """
        cfg      = AWSConfig(secretType=SecretType[os.getenv("SECRET_TYPE")], **kwargs)
        self.mgr = ServerManager(cfg=cfg)

    @staticmethod
    def getSingleton(**kwargs):
        """
        Return singleton

        This prevents configuration values having to be continuously be sucked down
        """
        if not AWSRootObject._singletonInstance:
            AWSRootObject._singletonInstance = AWSRootObject(**kwargs)
        return AWSRootObject._singletonInstance
