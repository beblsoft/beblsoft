#!/usr/bin/env python3
"""
 NAME:
  emailReceived.py

DESCRIPTION
 AWS Handler to Receive an Email
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleEmailReceived(event, context):  # pylint: disable=W0613
    """
    Handle a received email
    """
    pass
