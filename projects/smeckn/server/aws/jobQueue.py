#!/usr/bin/env python3
"""
 NAME:
  jobQueue.py

DESCRIPTION
  AWS Job Queue Handler
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.aws import AWSRootObject
from smeckn.server.job import Job
from smeckn.server.job.message import JobMessage
from smeckn.server.eCtx.aws import AWSExecutionContext


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleMessageReceived(event, context): #pylint: disable=W0613
    """
    Handle message received
    Example SQS Event:
    {
      "Records": [
         {
            "messageId": "c80e8021-a70a-42c7-a470-796e1186f753",
            "receiptHandle": "AQEBJQ+/u6NsnT5t8Q/VbVxgdUl4TMKZ5FqhksRdIQvLBhwNvADoBxYSOVeCBXdnS9P+erlTtwEALHsnBXynkfPLH3BOUqmgzP25U8kl8eHzq6RAlzrSOfTO8ox9dcp6GLmW33YjO3zkq5VRYyQlJgLCiAZUpY2D4UQcE5D1Vm8RoKfbE+xtVaOctYeINjaQJ1u3mWx9T7tork3uAlOe1uyFjCWU5aPX/1OHhWCGi2EPPZj6vchNqDOJC/Y2k1gkivqCjz1CZl6FlZ7UVPOx3AMoszPuOYZ+Nuqpx2uCE2MHTtMHD8PVjlsWirt56oUr6JPp9aRGo6bitPIOmi4dX0FmuMKD6u/JnuZCp+AXtJVTmSHS8IXt/twsKU7A+fiMK01NtD5msNgVPoe9JbFtlGwvTQ==",
            "body": "{\"foo\":\"bar\"}",
            "attributes": {
                "ApproximateReceiveCount": "3",
                "SentTimestamp": "1529104986221",
                "SenderId": "594035263019",
                "ApproximateFirstReceiveTimestamp": "1529104986230"
            },
            "messageAttributes": {},
            "md5OfBody": "9bb58f26192e4ba00f01e2e7b136bbd8",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:us-west-2:123456789012:MyQueue",
            "awsRegion": "us-west-2"
         }
      ]
    }
    """
    awsr       = AWSRootObject.getSingleton()
    mgr        = awsr.mgr

    with AWSExecutionContext(event=event, context=context):
        # Too Many Records
        recordList = event.get("Records")
        if len(recordList) > 1:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_TOO_MANY_RECORDS)

        # Execute message
        record     = recordList[0]
        sentTimeMS = record.get("attributes").get("SentTimestamp")
        jobMsg     = JobMessage.fromString(jobMsgStr=record.get("body"), sentTimeMS=sentTimeMS)
        return Job.executeFromMessage(mgr=mgr, jobMsg=jobMsg)
