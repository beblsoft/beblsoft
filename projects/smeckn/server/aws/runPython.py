#!/usr/bin/env python3
"""
 NAME:
  runPython.py

DESCRIPTION
 AWS Handler to Run Python
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aws import AWSRootObject


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def runPython(gdbDomain, code):
    """
    Execute Python Code from Client
    """
    awsr      = AWSRootObject.getSingleton(gdbDomain=gdbDomain)
    localDict = {
        "mgr": awsr.mgr,
        "rval": None
    }

    logger.info("Executing Code:\n{}".format(code))
    exec(code, globals(), localDict)  # pylint: disable=W0122
    return localDict.get("rval")
