#!/usr/bin/env python3
"""
 NAME:
  configSetEvent.py

DESCRIPTION
 AWS Handler to handle config set events
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleConfigSetEvent(event, context):  # pylint: disable=W0613
    """
    Handle an email configuration set event
    """
    pass
