#!/usr/bin/env python3
"""
 NAME:
  jobDLQueue.py

DESCRIPTION
  AWS Job Dead Letter Queue Handler
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode

from smeckn.server.aws import AWSRootObject
from smeckn.server.eCtx.aws import AWSExecutionContext


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleMessageReceived(event, context):
    """
    Handle message received
    """
    awsr    = AWSRootObject.getSingleton()
    mgr     = awsr.mgr

    with AWSExecutionContext(event=event, context=context):
        mgr.sentry.captureMessage(message="DeadLetter", bLogCode=BeblsoftLogCode.SQS_DEAD_LETTER_RECEIVED)
