#!/usr/bin/env python3
"""
 NAME:
  keepWarm.py

DESCRIPTION
 AWS Handler to Keep Warm
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- HANDLERS ------------------------------------------ #
@logFunc()
def handleKeepWarm(**kwargs):  # pylint: disable=W0613
    """
    Handle a keep warm event
    """
    pass
