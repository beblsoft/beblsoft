#!/usr/bin/env python
"""
 NAME:
  cli.py

DESCRIPTION
 Smeckn Server CLI
"""

# ----------------------- IMPORTS ------------------------------------------- #
import os
import logging
from datetime import datetime
import git
import click
from base.bebl.python.log.bLog import BeblsoftLog
from smeckn.common.secret import SecretType
from smeckn.server.commands.create import create
from smeckn.server.commands.delete import delete
from smeckn.server.commands.describe import describe
from smeckn.server.commands.start import start
from smeckn.server.commands.test import test
from smeckn.server.commands.revise import revise
from smeckn.server.commands.upgrade import upgrade
from smeckn.server.commands.downgrade import downgrade
from smeckn.server.commands.debug import debug
from smeckn.server.manager import ServerManager
from smeckn.server.config.local import LocalConfig
from smeckn.server.gc import gc
from smeckn.common.constant import Constant


# ----------------------- GLOBALS ------------------------------------------- #
logger         = logging.getLogger(__name__)
const          = Constant()
curDir         = os.path.dirname(os.path.realpath(__file__))
defaultLogFile = os.path.join(curDir, "cli.log")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--secret", "-s", type=click.Choice(st.name for st in list(SecretType)), default="Test",
              help="Specify secret type. Default=Test")
@click.option("--logfile", default=defaultLogFile, type=click.Path(writable=True),
              help="Specify log file. Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
@click.option("--gdbswaitonline", is_flag=True, default=False,
              help="Wait for GDB Server to come online")
@click.pass_context
def cli(ctx, secret, logfile, appendlog, verbose, gdbswaitonline):
    """
    Smeckn Server Command Line Interface
    """
    ctx.obj["gc"] = gc
    gc.server.cfg = LocalConfig(secretType=SecretType[secret],
                                gitCommitSHA=git.Repo(search_parent_directories=True).head.object.hexsha)
    gc.server.mgr = ServerManager(cfg=gc.server.cfg)
    gc.bLog       = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()

    if gdbswaitonline:
        gc.server.mgr.gdb.waitServerOnline()


# ----------------------- COMMANDS ------------------------------------------ #
cli.add_command(create)
cli.add_command(delete)
cli.add_command(describe)
cli.add_command(start)
cli.add_command(test)
cli.add_command(revise)
cli.add_command(upgrade)
cli.add_command(downgrade)
cli.add_command(debug)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as e:  # pylint: disable=W0703
        # Raise so exit code is set
        raise e
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
