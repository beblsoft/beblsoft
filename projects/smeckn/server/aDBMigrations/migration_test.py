#!/usr/bin/env python3
"""
 NAME:
  migration_test.py

 DESCRIPTION
  ADB Migration Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.test.common import CommonTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class MigrationTestCase(CommonTestCase):
    """
    Test Migrations
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID)
            aDB    = AccountDatabase.fromAModel(aModel=aModel)
            self.versionList = aDB.bDBMigrations.history

    @logFunc()
    def test_downgrade(self):
        """
        Test downgrading all account databases
        """
        self.mgr.adb.downgradeAll(toVersion="base")

        # 2 checks
        # --------
        # Verify Global database has updated account model dbversion
        # Verify Account Database is correctly stamped
        with self.gDB.sessionScope(commit=False) as gs:
            aModelList = AccountDAO.getAll(gs=gs)
            for aModel in aModelList:
                self.assertEqual(aModel.dbVersion, '')
                aDB = AccountDatabase.fromAModel(aModel=aModel)
                self.assertEqual(aDB.bDBMigrations.version, '')
                self.assertEqual(aDB.tables[0], 'alembic_version')
                self.assertEqual(len(aDB.tables), 1)

    @logFunc()
    def test_upgrade(self):
        """
        Test upgrading all account databases
        """
        self.mgr.adb.downgradeAll(toVersion="base")

        # Iterate over all versions
        for version in self.versionList:
            self.mgr.adb.upgradeAll(toVersion=version)

            with self.gDB.sessionScope(commit=False) as gs:
                aModelList = AccountDAO.getAll(gs=gs)

                # For each account:
                # Verify global database correct
                # Verify account database correct
                for aModel in aModelList:
                    self.assertEqual(aModel.dbVersion, version)
                    aDB = AccountDatabase.fromAModel(aModel=aModel)
                    self.assertEqual(aDB.bDBMigrations.version, version)
