#!/usr/bin/env python3
"""
NAME:
 env.py

DESCRIPTION
 Alembic Environment
"""

# ------------------------ IMPORTS ------------------------------------------ #
from __future__ import with_statement
from alembic import context
from sqlalchemy import create_engine
from smeckn.server.aDB import base


# ------------------------ GLOBALS ------------------------------------------ #
config         = context.config #pylint: disable=E1101
targetMetadata = base.metadata
xArgDict       = context.get_x_argument(as_dictionary=True) #pylint: disable=E1101
engineStr      = xArgDict.get("engineStr")


# ------------------------ ONLINE MIGRATIONS -------------------------------- #
def runMigrationsOnline():
    """
    Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.
    """
    connectable = create_engine(engineStr)
    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=targetMetadata) #pylint: disable=E1101
        with context.begin_transaction(): #pylint: disable=E1101
            context.run_migrations() #pylint: disable=E1101


# ------------------------ MAIN --------------------------------------------- #
runMigrationsOnline()
