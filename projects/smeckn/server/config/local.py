#!/usr/bin/env python3
"""
 NAME
  local.py

 DESCRIPTION
   Local Configuration Functionality
"""


# --------------------------- IMPORTS --------------------------------------- #
import os
import getpass
from smeckn.server.config import Config
from base.aws.python.EC2.bRegion import BeblsoftRegion


# --------------------------- LOCAL CONFIGURATION CLASS --------------------- #
class LocalConfig(Config):
    """
    Local Configuration class
    """

    def __init__(self, secretType, gitCommitSHA):
        """
        Initialize object
        """
        super().__init__(secretType=secretType)

        # AWS
        self.AWS_BREGION                       = BeblsoftRegion.getDefault()

        # Release
        self.ENVIRONMENT                       = "Local"
        self.GIT_COMMIT_SHA                    = gitCommitSHA

        # Server Domain
        self.SERVER_PROTOCOL                   = "https"
        self.SERVER_IP                         = os.getenv("SERVER_IP", "localhost")
        self.SERVER_PORT                       = int(os.getenv("SERVER_PORT", "5100"))
        self.SERVER_DOMAIN                     = "{}://{}:{}".format(self.SERVER_PROTOCOL,
                                                                     self.SERVER_IP,
                                                                     self.SERVER_PORT)
        # Web Client Domain
        self.WEBCLIENT_IP                      = os.getenv("WEBCLIENT_IP", "localhost")
        self.WEBCLIENT_PORT                    = int(os.getenv("WEBCLIENT_PORT", "8021"))
        self.WEBCLIENT_DOMAIN_LIST             = ["{}://{}:{}".format(self.WEBCLIENT_PROTOCOL,
                                                                      self.WEBCLIENT_IP,
                                                                      self.WEBCLIENT_PORT)]

        # Web Client User Guide Domain
        self.WEBCLIENT_USERGUIDE_IP            = os.getenv("WEBCLIENT_USERGUIDE_IP", "localhost")
        self.WEBCLIENT_USERGUIDE_PORT          = int(os.getenv("WEBCLIENT_USERGUIDE_PORT", "8021"))
        self.WEBCLIENT_USERGUIDE_DOMAIN_LIST   = ["{}://{}:{}".format(self.WEBCLIENT_USERGUIDE_PROTOCOL,
                                                                      self.WEBCLIENT_USERGUIDE_IP,
                                                                      self.WEBCLIENT_USERGUIDE_PORT)]
        # Web Client Email Domain
        self.WEBCLIENT_EMAIL_PROTOCOL          = "https"
        self.WEBCLIENT_EMAIL_IP                = os.getenv("WEBCLIENT_EMAIL_IP", "nightly.test.smeckn.com")
        self.WEBCLIENT_EMAIL_DOMAIN_LIST       = ["{}://{}".format(self.WEBCLIENT_EMAIL_PROTOCOL,
                                                                   self.WEBCLIENT_EMAIL_IP)]

        # Global Database
        self.GDB_DOMAIN                        = os.getenv("MYSQL_DOMAIN", "localhost")
        self.GDB_USER                          = os.getenv("MYSQL_USER", getpass.getuser())
        self.GDB_PASSWORD                      = os.getenv("MYSQL_PASSWORD", "")

        # Global Revision Database
        self.GRDB_DOMAIN                       = os.getenv("MYSQL_DOMAIN", "localhost")
        self.GRDB_USER                         = os.getenv("MYSQL_USER", getpass.getuser())
        self.GRDB_PASSWORD                     = os.getenv("MYSQL_PASSWORD", "")

        # Account Database
        self.ADB_DOMAIN                        = os.getenv("MYSQL_DOMAIN", "localhost")
        self.ADB_USER                          = os.getenv("MYSQL_USER", getpass.getuser())
        self.ADB_PASSWORD                      = os.getenv("MYSQL_PASSWORD", "")

        # Account Revision Database
        self.ARDB_DOMAIN                       = os.getenv("MYSQL_DOMAIN", "localhost")
        self.ARDB_USER                         = os.getenv("MYSQL_USER", getpass.getuser())
        self.ARDB_PASSWORD                     = os.getenv("MYSQL_PASSWORD", "")

        # Email
        self.EMAIL_FROM_DOMAIN                 = "test.smeckn.com"
        self.EMAIL_CONFIG_SET_NAME             = None

        # Flask Application
        self.APP_ENV                           = "development"
        self.APP_DEBUG                         = True
