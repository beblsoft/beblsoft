#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
   Configuration Functionality
"""

# --------------------------- IMPORTS --------------------------------------- #
import os
import abc
import enum
import getpass
import logging
from datetime import timedelta
from base.bebl.python.error.bError import BeblsoftError
from base.aws.python.STS.bIdentity import BeblsoftIdentity
from base.aws.python.SecretsManager.bCachedSecret import BeblsoftCachedSecret
from smeckn.server.gDB.account.model import AccountType
from smeckn.common.constant import Constant
from smeckn.common.secret import SecretType


# --------------------------- CONFIGURATION MODES --------------------------- #
class ConfigMode(enum.Enum):
    """
    Configuration Mode Enumeration
    """
    Test       = enum.auto()
    Production = enum.auto()

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


# --------------------------- CONFIGURATION CLASS --------------------------- #
class Config(abc.ABC):
    """
    Configuration class
    """

    def __init__(self, secretType):  # pylint: disable=R0915
        """
        Initialize object
        Args:
          secretType:
            SecretType
            Ex. SecretType.Test
        """
        # Constant
        self.CONST                                    = Constant()

        # AWS
        self.AWS_BREGION                              = None
        self.AWS_BIDENTITY                            = BeblsoftIdentity.getCaller()

        # Release
        self.ENVIRONMENT                              = None
        self.GIT_COMMIT_SHA                           = None

        # Secrets
        self.SECRET_TYPE                              = secretType
        self.BSECRET                                  = secretType.getBSecret()

        # Sentry
        self.SENTRY_SERVER_DSN                        = self.BSECRET.SENTRY_SERVER_DSN
        self.SENTRY_REQUEST_BODIES                    = "always"
        self.SENTRY_MAX_BREADCRUMBS                   = 100
        self.SENTRY_SHUTDOWN_TIMEOUT                  = 2.0
        self.SENTRY_DEBUG                             = False
        self.SENTRY_WITH_LOCALS                       = True

        # Server Domain
        self.SERVER_PROTOCOL                          = "https"
        self.SERVER_IP                                = ""
        self.SERVER_PORT                              = 0
        self.SERVER_DOMAIN                            = ""

        # WebClient Domains
        # Needs to be a list because production hosts at "smeckn.com" and "www.smeckn.com"
        self.WEBCLIENT_PROTOCOL                       = "https"
        self.WEBCLIENT_IP                             = ""
        self.WEBCLIENT_PORT                           = 0
        self.WEBCLIENT_DOMAIN_LIST                    = []

        # WebClient User Guide Domains
        self.WEBCLIENT_USERGUIDE_PROTOCOL             = "https"
        self.WEBCLIENT_USERGUIDE_IP                   = ""
        self.WEBCLIENT_USERGUIDE_PORT                 = 0
        self.WEBCLIENT_USERGUIDE_DOMAIN_LIST          = []

        # WebClient Email Domains
        self.WEBCLIENT_EMAIL_PROTOCOL                 = "https"
        self.WEBCLIENT_EMAIL_IP                       = ""
        self.WEBCLIENT_EMAIL_PORT                     = 0
        self.WEBCLIENT_EMAIL_DOMAIN_LIST              = []

        # Cookie Domain
        self.COOKIE_DOMAIN                            = None

        # Global Database
        self.GDB_DOMAIN                               = ""
        self.GDB_USER                                 = ""
        self.GDB_PASSWORD                             = ""
        self.GDB_NAME                                 = "SmecknGDB"
        self.GDB_PORT                                 = 3306
        self.GDB_ECHO                                 = False

        # Global Revision Database
        self.GRDB_DOMAIN                              = ""
        self.GRDB_USER                                = ""
        self.GRDB_PASSWORD                            = ""
        self.GRDB_NAME                                = "SmecknGRDB"
        self.GRDB_PORT                                = 3306
        self.GRDB_ECHO                                = False

        # Account Database
        self.ADB_DOMAIN                               = ""
        self.ADB_USER                                 = ""
        self.ADB_PASSWORD                             = ""
        self.ADB_NAME_PREFIX                          = "SmecknADB"
        self.ADB_PORT                                 = 3306
        self.ADB_ECHO                                 = False

        # Account Revision Database
        self.ARDB_DOMAIN                              = ""
        self.ARDB_USER                                = ""
        self.ARDB_PASSWORD                            = ""
        self.ARDB_NAME                                = "SmecknARDB"
        self.ARDB_PORT                                = 3306
        self.ARDB_ECHO                                = False

        # Database Populator
        # DBP_ADMIN_AUTH_TOKEN (aID=-1, never expires) given to test clients
        self.DBP_ADMIN_EMAIL                          = self.BSECRET.DBP_ADMIN_EMAIL
        self.DBP_ADMIN_PASSWORD                       = self.BSECRET.DBP_ADMIN_PASSWORD
        self.DBP_ADMIN_AUTH_TOKEN                     = self.BSECRET.DBP_ADMIN_AUTH_TOKEN
        self.DBP_ACCOUNT_EMAIL_LIST                   = []
        self.DBP_ACCOUNT_PASSWORD_LIST                = []
        self.DBP_ACCOUNT_EMAIL_LIST                   = ["darryl.black@smeckn.com", "james.bensson@smeckn.com"]
        self.DBP_ACCOUNT_PASSWORD_LIST                = ["beblsoft", "beblsoft"]
        self.DBP_ACCOUNT_TYPE_LIST                    = [AccountType.REGULAR, AccountType.REGULAR]

        # Job Queues
        self.SQS_JOB_QUEUE_NAME                       = os.getenv("SQS_JOB_QUEUE_NAME", None)
        self.SQS_JOB_DLQUEUE_NAME                     = os.getenv("SQS_JOB_DLQUEUE_NAME", None)

        # JWTs
        self.AUTH_JWT_SECRET                          = self.BSECRET.AUTH_JWT_SECRET
        self.AUTH_JWT_ALGORITHM                       = "HS512"
        self.AUTH_JWT_EXPDELTA                        = timedelta(days=10)
        self.AUTH_JWT_NBFDELTA                        = timedelta(seconds=0)
        self.AUTH_JWT_LEEWAY                          = timedelta(seconds=0)
        self.ACCOUNT_JWT_SECRET                       = self.BSECRET.ACCOUNT_JWT_SECRET
        self.ACCOUNT_JWT_ALGORITHM                    = "HS512"
        self.ACCOUNT_JWT_EXPDELTA                     = timedelta(days=10)
        self.ACCOUNT_JWT_NBFDELTA                     = timedelta(seconds=0)
        self.ACCOUNT_JWT_LEEWAY                       = timedelta(seconds=0)
        self.FP_JWT_SECRET                            = self.BSECRET.FP_JWT_SECRET
        self.FP_JWT_ALGORITHM                         = "HS512"
        self.FP_JWT_EXPDELTA                          = timedelta(days=2)
        self.FP_JWT_NBFDELTA                          = timedelta(seconds=0)
        self.FP_JWT_LEEWAY                            = timedelta(seconds=0)
        self.CA_JWT_SECRET                            = self.BSECRET.CA_JWT_SECRET
        self.CA_JWT_ALGORITHM                         = "HS512"
        self.CA_JWT_EXPDELTA                          = timedelta(days=2)
        self.CA_JWT_NBFDELTA                          = timedelta(seconds=0)
        self.CA_JWT_LEEWAY                            = timedelta(seconds=0)
        self.CART_JWT_SECRET                          = self.BSECRET.CART_JWT_SECRET
        self.CART_JWT_ALGORITHM                       = "HS512"
        self.CART_JWT_EXPDELTA                        = timedelta(days=2)
        self.CART_JWT_NBFDELTA                        = timedelta(seconds=0)
        self.CART_JWT_LEEWAY                          = timedelta(seconds=0)

        # ReCAPTCHA
        self.RECAPTCHA_SECRET_KEY                     = self.BSECRET.RECAPTCHA_SECRET_KEY
        self.RECAPTCHA_TEST_TOKEN                     = "03ACgFB9uDQpkQnMORhKLCgzRDRPlpoPmuFzQwXQJbnJ4FiDo_47Ofx3dj2pQzkqB0xrrYDzh_ZAr8Yon3VFpCCZWBgXT8Gzp4OAtueIK"
        self.RECAPTCHA_PASSTHROUGH_TOKEN              = self.BSECRET.RECAPTCHA_PASSTHROUGH_TOKEN

        # VCR
        self.VCR_RECORD_MODE                          = "once"
        self.VCR_MATCH_ON                             = ["method", "uri"]
        self.VCR_LOG_LEVEL                            = logging.WARNING

        # Email
        self.EMAIL_FROM_DOMAIN                        = ""
        self.EMAIL_CONFIG_SET_NAME                    = None

        # Flask Application
        self.APP_ENV                                  = None
        self.APP_DEBUG                                = False
        self.APP_TESTING                              = False
        self.APP_PROPAGATE_EXCEPTIONS                 = False
        self.APP_RESTPLUS_VALIDATE                    = True
        self.APP_ERROR_404_HELP                       = False  # NO Flask RP 404 help messages
        self.APP_ERROR_STACKS                         = self.SECRET_TYPE != SecretType.Production  # Send back stacks
        self.APP_N_PROCESSES                          = 1
        self.APP_THREADED                             = True
        self.APP_PASSTHROUGH_ERRORS                   = False

        # Stripe
        self.STRIPE_PUBLISHABLE_KEY                   = self.BSECRET.STRIPE_PUBLISHABLE_KEY
        self.STRIPE_SECRET_KEY                        = self.BSECRET.STRIPE_SECRET_KEY
        self.STRIPE_API_VERSION                       = "2018-11-08"
        self.STRIPE_LOG_LEVEL                         = "warning"

        # Facebook
        self.FACEBOOK_APP_ID                          = self.BSECRET.FACEBOOK_APP_ID
        self.FACEBOOK_APP_SECRET                      = self.BSECRET.FACEBOOK_APP_SECRET
        self.FACEBOOK_API_VERSION                     = "4.0"
        self.FACEBOOK_API_TIMEOUTS                    = 30
        self.FACEBOOK_SMECKN_TESTUSER_SMALL           = 325719254950802
        self.FACEBOOK_SMECKN_TESTUSER_MEDIUM          = 100033072498292
        self.FACEBOOK_SMECKN_TESTUSER_LARGE           = 100032988263383
        self.FACEBOOK_MAX_POSTS_PER_PROFILE           = 20000
        self.FACEBOOK_POST_SYNC_CHUNK_SIZE            = 100
        self.FACEBOOK_SYNC_RESTART_TIMEDELTA          = timedelta(hours=24)
        self.FACEBOOK_REQUIRED_PERMISSION_LIST        = ["public_profile", "user_posts"]
        self.FACEBOOK_DESIRED_PERMISSION_LIST         = ["public_profile", "user_posts"]
        self.FACEBOOK_POSTMSG_COMPSENTA_NPERWORKER    = 100

        # Profile Stub
        self.PROFILE_STUB_SYNC_RESTART_TIMEDELTA      = timedelta(hours=48)
        self.PROFILE_STUB_SYNC_POST_PROGRESS_COUNT    = 50
        self.PROFILE_STUB_POST_COMPSENTA_NPERWORKER   = 100

        # Profile
        self.PROFILE_SYNC_MAX_IDLE_WORKERKICK         = timedelta(minutes=2)
        self.PROFILE_SYNC_MAX_IDLE_WORKERHB           = timedelta(minutes=2)
        self.PROFILE_SYNC_MIN_SYNCCHUNK_TIMEDELTA     = timedelta(minutes=2)
        self.PROFILE_ANALYSIS_MAX_IDLE_WORKERKICK     = timedelta(minutes=2)
        self.PROFILE_ANALYSIS_MAX_IDLE_WORKERHB       = timedelta(minutes=2)

    @property
    def WEBCLIENT_APP_URL(self):  # pylint: disable=C0111
        return "{}{}".format(self.WEBCLIENT_DOMAIN_LIST[0], self.CONST.wcp.app)

    @property
    def WEBCLIENT_USERGUIDE_URL(self):  # pylint: disable=C0111
        return "{}{}".format(self.WEBCLIENT_USERGUIDE_DOMAIN_LIST[0], self.CONST.wcp.userGuide)

    @property
    def WEBCLIENT_EMAIL_URL(self):  # pylint: disable=C0111
        return "{}{}".format(self.WEBCLIENT_EMAIL_DOMAIN_LIST[0], self.CONST.wcp.email)
