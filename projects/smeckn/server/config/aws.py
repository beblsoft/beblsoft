#!/usr/bin/env python3
"""
NAME
 aws.py

DESCRIPTION
 AWS Configuration Functionality
"""


# --------------------------- IMPORTS --------------------------------------- #
import os
from smeckn.server.config import Config
from base.aws.python.EC2.bRegion import BeblsoftRegion


# --------------------------- AWS CONFIGURATION CLASS ----------------------- #
class AWSConfig(Config):
    """
    AWS Configuration class
    """

    def __init__(self, secretType, gdbDomain=None):
        """
        Initialize object
        """
        super().__init__(secretType=secretType)

        # AWS
        self.AWS_BREGION                       = BeblsoftRegion.getCurrent()

        # Release
        self.ENVIRONMENT                       = os.getenv("ENVIRONMENT")
        self.GIT_COMMIT_SHA                    = os.getenv("GIT_COMMIT_SHA")

        # Server Domain
        self.SERVER_DOMAIN                     = os.getenv("SERVER_DOMAIN", None)

        # WebCLient Domains: All the same on AWS
        self.WEBCLIENT_DOMAIN_LIST             = os.getenv("WEBCLIENT_DOMAIN_LIST", "").split(" ")
        self.WEBCLIENT_USERGUIDE_DOMAIN_LIST   = self.WEBCLIENT_DOMAIN_LIST
        self.WEBCLIENT_EMAIL_DOMAIN_LIST       = self.WEBCLIENT_DOMAIN_LIST

        # Cookie Domain
        self.COOKIE_DOMAIN                     = os.getenv("COOKIE_DOMAIN", None)

        # Global Database
        self.GDB_DOMAIN                        = os.getenv("GDB_DOMAIN", gdbDomain)
        self.GDB_USER                          = self.BSECRET.GDB_USER
        self.GDB_PASSWORD                      = self.BSECRET.GDB_PASSWORD
        self.GDB_ECHO                          = False

        # Account Database
        self.ADB_USER                          = self.BSECRET.ADB_USER
        self.ADB_PASSWORD                      = self.BSECRET.ADB_PASSWORD
        self.ADB_ECHO                          = False

        # Email
        self.EMAIL_FROM_DOMAIN                 = os.getenv("EMAIL_FROM_DOMAIN", None)
        self.EMAIL_CONFIG_SET_NAME             = os.getenv("EMAIL_CONFIG_SET_NAME", None)

        # Flask Application
        self.APP_ENV                           = "production"
        self.APP_DEBUG                         = False
