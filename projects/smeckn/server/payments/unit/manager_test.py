#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Unit Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.payments.unit.models import UnitCountModel
from smeckn.server.payments.cart.models import CartCostModel
from smeckn.server.aDB.unitLedger.model import UnitType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class UnitManagerTestCase(CommonTestCase):
    """
    Test Unit Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True)

    @logFunc()
    def test_increment(self):
        """
        Test inrementing unit balance
        """
        super().setUp(unitLedger=True)

        additionCount     = 10
        costAmount        = 100
        cartCostModelList = CartCostModel.getFakeList(count=additionCount, costAmount=costAmount)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            self.mgr.payments.unit.increment(aDBS=aDBS, cartCostModelList=cartCostModelList)
            aDBS.commit()
            for unitType in list(UnitType):
                unitTypeManager  = self.mgr.payments.unit.getUnitTypeManager(unitType=unitType)
                unitBalanceModel = unitTypeManager.getUnitBalanceModel(aDBS)
                self.assertEqual(unitBalanceModel.nTotal,
                                 self.unitTypeDict[unitType]["nInitialTotal"] + additionCount)
                self.assertEqual(unitBalanceModel.nHeld,
                                 self.unitTypeDict[unitType]["nInitialHeld"])

    @logFunc()
    def test_getUnitTypeManager(self):
        """
        Test that all unit types have a unit type manager
        """
        for unitType in list(UnitType):
            unitTypeManager = self.mgr.payments.unit.getUnitTypeManager(unitType=unitType)
            self.assertIsNotNone(unitTypeManager)

    @logFunc()
    def test_getUnitPriceModelList(self):
        """
        Test getUnitPriceModelList
        """
        bCurrencyCode      = BeblsoftCurrencyCode.USD
        unitPriceModelList = self.mgr.payments.unit.getUnitPriceModelList(bCurrencyCode=bCurrencyCode)

        # Iterate through all units verifying that price model is present for each
        for unitType in list(UnitType):
            foundUnitType = False
            for unitPriceModel in unitPriceModelList:
                if unitPriceModel.unitType == unitType:
                    foundUnitType = True
                    self.assertEqual(unitPriceModel.bCurrencyCode, bCurrencyCode)
                    self.assertGreater(unitPriceModel.unitCost, 0)
                    self.assertGreater(unitPriceModel.minPurchaseCount, 0)
                    self.assertGreater(unitPriceModel.purchaseIncrement, 0)
            self.assertTrue(foundUnitType)

    @logFunc()
    def test_getUnitPriceModelListInvalidCurrency(self):
        """
        Test getUnitPriceModelList with an invalid currency
        """
        bCurrencyCode = BeblsoftCurrencyCode.ALL
        try:
            _ = self.mgr.payments.unit.getUnitPriceModelList(bCurrencyCode=bCurrencyCode)
        except BeblsoftError as e:
            self.assertEqual(e.code, BeblsoftErrorCode.STRIPE_UNSUPPORTED_CURRENCY)
        else:
            raise Exception("Got price models for invalid currency")

    @logFunc()
    def test_getUnitBalanceModelList(self):
        """
        Test getUnitBalanceModelList
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            unitBalanceModelList = self.mgr.payments.unit.getUnitBalanceModelList(aDBS=aDBS)
            for unitBalanceModel in unitBalanceModelList:
                unitType = unitBalanceModel.unitType
                nTotal     = self.unitTypeDict[unitType]["nInitialTotal"]
                nHeld      = self.unitTypeDict[unitType]["nInitialHeld"]
                nAvailable = nTotal - nHeld
                self.assertEqual(unitBalanceModel.nTotal, nTotal)
                self.assertEqual(unitBalanceModel.nHeld, nHeld)
                self.assertEqual(unitBalanceModel.nAvailable, nAvailable)

    @logFunc()
    def test_getCartCostModelList(self):
        """
        Test getCartCostModelList
        """
        bCurrencyCode      = BeblsoftCurrencyCode.USD
        count              = 500
        unitCountModelList = UnitCountModel.getFakeList(count=count)
        cartCostModelList  = self.mgr.payments.unit.getCartCostModelList(bCurrencyCode=bCurrencyCode,
                                                                         unitCountModelList=unitCountModelList)
        for cartCostModel in cartCostModelList:
            foundUnitType = False
            for unitCountModel in unitCountModelList:
                unitType        = unitCountModel.unitType
                unitTypeManager = self.mgr.payments.unit.getUnitTypeManager(unitType=unitType)
                if cartCostModel.unitType == unitType:
                    costAmount = unitCountModel.count * unitTypeManager.currencyCostDict[bCurrencyCode]
                    self.assertEqual(cartCostModel.costAmount, costAmount)
                    foundUnitType = True
            self.assertTrue(foundUnitType)

    @logFunc()
    def test_getCartCostModelListInvalidCount(self):
        """
        Test getCartCostModelList with an invalid count
        """
        bCurrencyCode      = BeblsoftCurrencyCode.USD
        unitCountModelList = UnitCountModel.getFakeList(count=5)
        try:
            _ = self.mgr.payments.unit.getCartCostModelList(bCurrencyCode=bCurrencyCode,
                                                            unitCountModelList=unitCountModelList)
        except BeblsoftError as e:
            self.assertEqual(e.code, BeblsoftErrorCode.STRIPE_AMOUNT_UNDER_MIN_PURCHASE_COUNT)
        else:
            raise Exception("Created cart cost model with count under minimum")

    @logFunc()
    def test_getCartCostModelListInvalidCurrency(self):
        """
        Test getCartCostModelList with an invalid currency
        """
        bCurrencyCode      = BeblsoftCurrencyCode.ALL
        unitCountModelList = UnitCountModel.getFakeList(count=600)
        try:
            _ = self.mgr.payments.unit.getCartCostModelList(bCurrencyCode=bCurrencyCode,
                                                            unitCountModelList=unitCountModelList)
        except BeblsoftError as e:
            self.assertEqual(e.code, BeblsoftErrorCode.STRIPE_UNSUPPORTED_CURRENCY)
        else:
            raise Exception("Got cart cost models for invalid currency")
