#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Unit Models Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.unitLedger.model import UnitType


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- UNIT PRICE MODEL ------------------------------- #
class UnitPriceModel():
    """
    Unit Price Model Class
    """

    def __init__(self, unitType, bCurrencyCode, unitCost,
                 minPurchaseCount=1, purchaseIncrement=1):
        """
        Initialize object
        Args
          unitType:
            Type of unit
            Ex. UnitType.TEXT_ANALYSIS
          bCurrencyCode:
            Beblsoft Currency Code
            Ex. BeblsoftCurrencyCode.USD
          unitCost:
            Unit cost in the minimum dimension of the currency
            Ex. If currency is USD, mimumdimension is a penny
                100 = $1
          minPurchaseCount:
            Minimum count of unit that can be purchased at once
            Ex. 500
          purchaseIncrement:
            Incremental amount of units that can be purchased after minimum reached
            Ex. 1
        """
        self.unitType          = unitType
        self.bCurrencyCode     = bCurrencyCode
        self.unitCost          = unitCost
        self.minPurchaseCount  = minPurchaseCount
        self.purchaseIncrement = purchaseIncrement


# -------------------------- UNIT COUNT MODEL ------------------------------- #
class UnitCountModel():
    """
    Unit Count Model Class
    """

    def __init__(self, unitType, count):
        """
        Initialize object
        Args
          unitType:
            Type of unit
            Ex. UnitType.TEXT_ANALYSIS
          count:
            Unit Count
            Ex. 50
        """
        self.unitType   = unitType
        self.count      = count

    @staticmethod
    @logFunc()
    def getFakeList(count=5):
        """
        Return UnitCountModel list with all UnitTypes
        Args
          count:
            Count for each UnitType in list
        """
        unitCountModelList = []
        for unitType in list(UnitType):
            unitCountModel = UnitCountModel(unitType=unitType, count=count)
            unitCountModelList.append(unitCountModel)
        return unitCountModelList
