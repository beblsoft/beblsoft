#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Unit Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.manager.common import CommonManager
from smeckn.server.payments.unit.typeManager import UnitTypeManager
from smeckn.server.aDB.unitLedger.model import UnitType


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- UNIT MANAGER ----------------------------------- #
class UnitManager(CommonManager):
    """
    Unit Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Register all unit type managers here
        self.unitTypeManagerList  = [
            UnitTypeManager(
                mgr               = self.mgr,
                unitType          = UnitType.TEXT_ANALYSIS,
                minPurchaseCount  = 100,
                purchaseIncrement = 50,
                currencyCostDict  = {BeblsoftCurrencyCode.USD: 5}),
            UnitTypeManager(
                mgr               = self.mgr,
                unitType          = UnitType.PHOTO_ANALYSIS,
                minPurchaseCount  = 50,
                purchaseIncrement = 25,
                currencyCostDict  = {BeblsoftCurrencyCode.USD: 10}),
        ]

    # ---------------------- INCREMENT -------------------------------------- #
    @logFunc()
    def increment(self, aDBS, cartCostModelList):
        """
        Increment the unit balance for all unitCountModelList
        """
        for cartCostModel in cartCostModelList:
            unitType        = cartCostModel.unitType
            count           = cartCostModel.count
            unitTypeManager = self.getUnitTypeManager(unitType=unitType)
            unitTypeManager.increment(aDBS=aDBS, count=count)

    # ---------------------- GETTERS ---------------------------------------- #
    @logFunc()
    def getUnitTypeManager(self, unitType):
        """
        Return unit type manager for specific unitType
        Args
          unitType:
            Unit Type
            Ex. UnitType.TEXT_ANALYSIS
        """
        utm = None
        for curUTM in self.unitTypeManagerList:
            if curUTM.unitType == unitType:
                utm = curUTM
                break
        if not utm:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_NO_UNIT_MANAGER_EXISTS,
                                msg = "unitType={}".format(unitType))
        return utm

    @logFunc()
    def getUnitPriceModelList(self, bCurrencyCode, unitType=None):
        """
        Return UnitPriceModelList
        Args
          unitType:
            Unit Type, None for all unit types
        """
        unitPriceModelList = []
        for curUTM in self.unitTypeManagerList:
            if unitType is None or curUTM.unitType == unitType:
                unitPriceModel = curUTM.getUnitPriceModel(bCurrencyCode)
                unitPriceModelList.append(unitPriceModel)
        return unitPriceModelList

    @logFunc()
    def getUnitBalanceModelList(self, aDBS, unitType=None):
        """
        Return UnitPriceBalanceList
        Args
          unitType:
            Unit Type, None for all unit types
        """
        unitBalanceModelList = []
        for curUTM in self.unitTypeManagerList:
            if unitType is None or curUTM.unitType == unitType:
                unitBalanceModel = curUTM.getUnitBalanceModel(aDBS=aDBS)
                unitBalanceModelList.append(unitBalanceModel)
        return unitBalanceModelList

    @logFunc()
    def getCartCostModelList(self, bCurrencyCode, unitCountModelList):
        """
        Return CartCostModelList
        """
        cartCostModelList = []
        for unitCountModel in unitCountModelList:
            unitType        = unitCountModel.unitType
            count           = unitCountModel.count
            unitTypeManager = self.getUnitTypeManager(unitType=unitType)
            cartCostModel   = unitTypeManager.getCartCostModelModel(bCurrencyCode=bCurrencyCode, count=count)
            cartCostModelList.append(cartCostModel)
        return cartCostModelList
