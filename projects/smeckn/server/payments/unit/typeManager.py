#!/usr/bin/env python3
"""
 NAME:
  common.py

 DESCRIPTION
  Common Unit Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.payments.unit.models import UnitPriceModel
from smeckn.server.payments.cart.models import CartCostModel
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- UNIT TYPE MANAGER ------------------------------ #
class UnitTypeManager():
    """
    Unit Type Manager Class
    """

    def __init__(self, mgr, unitType, minPurchaseCount, purchaseIncrement, currencyCostDict):
        """
        Initialize object
        Args
          mgr:
            Smeckn Server Manager object
          unitType:
            Unit Type
            Ex. UnitType.TEXT_ANALYSIS
          minPurchaseCount:
            Minimum count of units that can be purchased at once
            Ex. 500
          purchaseIncrement:
            Incremental amount of units that can be purchased after minimum reached
            Ex. 10
          currencyCostDict:
            Dictionary mapping BeblsoftCurrencyCode to cost
            Ex. {
                BeblsoftCurrencyCode.USD: 10
            }

        """
        self.mgr               = mgr
        self.cfg               = mgr.cfg
        self.unitType          = unitType
        self.minPurchaseCount  = minPurchaseCount
        self.purchaseIncrement = purchaseIncrement
        self.currencyCostDict  = currencyCostDict

    # ---------------------- INCREMENT -------------------------------------- #
    def increment(self, aDBS, count):
        """
        Increment the unit balance
        Args
          count:
            Count of units to increment
        """
        return UnitLedgerDAO.createAddition(aDBS=aDBS, unitType=self.unitType, ledgerCount=count)

    # ---------------------- GETTERS ---------------------------------------- #
    def getUnitPriceModel(self, bCurrencyCode):
        """
        Return UnitPriceModel

        Args
          bCurrencyCode:
            BeblsoftCurrencyCode
            Ex. BeblsoftCurrencyCode.USD
        """
        unitCost = self.currencyCostDict.get(bCurrencyCode, None)
        if unitCost is None:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_UNSUPPORTED_CURRENCY)

        unitPriceModel = UnitPriceModel(
            unitType          = self.unitType,
            bCurrencyCode     = bCurrencyCode,
            unitCost          = unitCost,
            minPurchaseCount  = self.minPurchaseCount,
            purchaseIncrement = self.purchaseIncrement)
        return unitPriceModel

    def getUnitBalanceModel(self, aDBS):
        """
        Return UnitBalanceModel

        Args
          aDBS:
            Account Database Session

        Returns
          UnitBalanceModel
        """
        return UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=self.unitType)

    def getCartCostModelModel(self, bCurrencyCode, count):
        """
        Return CartCostModel

        Args
          bCurrencyCode:
            BeblsoftCurrencyCode
          count:
            Count of unitType units
        """
        self.validatePurchaseCount(count)
        unitCost   = self.currencyCostDict.get(bCurrencyCode, None)
        if unitCost is None:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_UNSUPPORTED_CURRENCY)

        costAmount = count * unitCost
        return CartCostModel(
            unitType   = self.unitType,
            count      = count,
            costAmount = costAmount)

    # ---------------------- VALIDATE --------------------------------------- #
    def validatePurchaseCount(self, count):
        """
        Validate purchase amount
        Args
          count:
            Amount being purchased
            Ex. 550

        Note:
          Keeping it simple and not checking purchase increment here
        """
        countOverMin = count - self.minPurchaseCount
        if countOverMin < 0:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_AMOUNT_UNDER_MIN_PURCHASE_COUNT)
