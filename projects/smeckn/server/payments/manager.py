#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Payments Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.stripe.python.bStripe import BeblsoftStripe
from smeckn.server.manager.common import CommonManager
from smeckn.server.payments.unit.manager import UnitManager
from smeckn.server.payments.cart.manager import CartManager
from smeckn.server.payments.customer.manager import CustomerManager
from smeckn.server.payments.creditCard.manager import CreditCardManager
from smeckn.server.payments.charge.manager import ChargeManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- PAYMENTS MANAGER ------------------------------- #
class PaymentsManager(CommonManager):
    """
    Payments Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Stripe Hook
        self.bStripe    = BeblsoftStripe(secretKey=self.cfg.STRIPE_SECRET_KEY,
                                         version=self.cfg.STRIPE_API_VERSION,
                                         logLevel=self.cfg.STRIPE_LOG_LEVEL)

        # Managers
        self.unit       = UnitManager(mgr=self.mgr)
        self.cart       = CartManager(mgr=self.mgr)
        self.customer   = CustomerManager(mgr=self.mgr)
        self.creditCard = CreditCardManager(mgr=self.mgr)
        self.charge     = ChargeManager(mgr=self.mgr)
