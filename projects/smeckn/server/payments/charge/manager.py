#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Charge Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.stripe.python.charge.dao import StripeChargeDAO
from smeckn.server.aDB import AccountDatabase
from smeckn.server.manager.common import CommonManager
from smeckn.server.log.sLogger import SmecknLogger


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- CHARGE MANAGER --------------------------------- #
class ChargeManager(CommonManager):
    """
    Charge Manager Class
    """

    # -------------------------- CREATE ------------------------------------- #
    @logFunc()
    def create(self, aDBS, cartJWT, stripeCustomerID, stripeCardID):
        """
        Create a charge
        Args
          aDBS:
            Account Database Session
          cartJWT:
            Cart JavaScript Web Token
          stripeCustomerID:
            Stripe Customer ID
          stripeCardID:
            Beblsoft Stripe Credit Card

        Returns
          StripeChargeModel
        """
        cartModel         = self.mgr.jwt.cart.decodeJWT(token=cartJWT)
        cartCostModelList = cartModel.cartCostModelList
        chargeDescription = self.createChargeDescription(cartModel=cartModel)
        stripeCardModel   = self.mgr.payments.creditCard.getByID(stripeCustomerID=stripeCustomerID,
                                                                 stripeCardID=stripeCardID)

        # Phase 1: Verify card's customer
        stripeCardModel.verifyCustomer(stripeCustomerID=stripeCustomerID)

        # Phase 2: Authorize the charge
        stripeChargeModel = StripeChargeDAO.create(bStripe=self.mgr.payments.bStripe,
                                                   amount=cartModel.totalCost,
                                                   stripeCustomerID=stripeCustomerID,
                                                   currency=cartModel.bCurrencyCode.toStripeCode(),
                                                   sourceID=stripeCardID,
                                                   capture=False,
                                                   description=chargeDescription)
        # Phase 3: Increment units
        with AccountDatabase.transactionScope(session=aDBS):
            self.mgr.payments.unit.increment(aDBS, cartCostModelList=cartCostModelList)

        # Phase 4: capture the charge
        stripeChargeModel = StripeChargeDAO.capture(bStripe=self.mgr.payments.bStripe,
                                                    stripeChargeID=stripeChargeModel.id)
        SmecknLogger.info(msg="Payment Charged", bLogCode=BeblsoftLogCode.PAYMENT_CHARGED_USD,
                          logValue=cartModel.totalCost, cartModel=cartModel)

        return stripeChargeModel

    @logFunc()
    def createChargeDescription(self, cartModel):  # pylint: disable=R0201
        """
        Return description for charge
        """
        description       = ""
        cartCostModelList = cartModel.cartCostModelList
        for cartCostModel in cartCostModelList:
            description  += "{} {} Unit{}, ".format(cartCostModel.count,
                                                    cartCostModel.unitType.toDescriptionString(),
                                                    "s" if cartCostModel.count > 0 else "")
        description       = description.rstrip(", ")  # Remove last comma and space
        return description

    # -------------------------- RETRIEVE ----------------------------------- #
    @logFunc()
    def getList(self, stripeCustomerID, startingAfterID=None, limit=100):
        """
        Get list of charges in order from most recent to least recent

        Returns
          (StripeChargeModel List, hasMore)
        """
        return StripeChargeDAO.getList(bStripe=self.mgr.payments.bStripe,
                                       stripeCustomerID=stripeCustomerID,
                                       startingAfterID=startingAfterID,
                                       limit=limit)
