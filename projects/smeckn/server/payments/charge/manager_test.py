#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Charge Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.payments.unit.models import UnitCountModel


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class ChargeManagerTestCase(CommonTestCase):
    """
    Test Charge Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, stripeCustomers=True, stripeCards=True, stripeCharges=True)

    @logFunc()
    def test_create(self):
        """
        Test creating a charge. Verify that all units are added
        """
        count                = 1000
        unitCountModelList   = UnitCountModel.getFakeList(count=count)
        bCurrencyCode        = BeblsoftCurrencyCode.USD
        (cartModel, cartJWT) = self.mgr.payments.cart.createCart(bCurrencyCode=bCurrencyCode,
                                                                 unitCountModelList=unitCountModelList)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeChargeModel = self.mgr.payments.charge.create(aDBS=aDBS, cartJWT=cartJWT,
                                                                stripeCustomerID=self.popStripeCustomerID,
                                                                stripeCardID=self.popStripeCardID)


            # Ensure units incremented properly
            for unitType in list(UnitType):
                unitTypeManager  = self.mgr.payments.unit.getUnitTypeManager(unitType=unitType)
                unitBalanceModel = unitTypeManager.getUnitBalanceModel(aDBS)
                self.assertEqual(unitBalanceModel.nTotal,
                                 self.unitTypeDict[unitType]["nInitialTotal"] + count)
                self.assertEqual(unitBalanceModel.nHeld,
                                 self.unitTypeDict[unitType]["nInitialHeld"])

            # Ensure charge looks correct
            self.assertEqual(stripeChargeModel.sourceID, self.popStripeCardID)
            self.assertEqual(cartModel.totalCost, stripeChargeModel.amount)
            self.assertEqual(bCurrencyCode, stripeChargeModel.bCurrencyCode)
            self.assertTrue(stripeChargeModel.captured)

    @logFunc()
    def test_getList(self):
        """
        Test getting a list of charges

        Verify in order from most recent to least recent
        """
        limit             = 3
        hasMore           = True
        total             = 0
        startingAfterID   = None

        while hasMore:
            (stripeChargeModelList, hasMore) = self.mgr.payments.charge.getList(
                stripeCustomerID=self.popStripeCustomerID, startingAfterID=startingAfterID, limit=limit)

            countThisRound  = len(stripeChargeModelList)
            firstDate       = stripeChargeModelList[0].createdDate
            lastDate        = stripeChargeModelList[-1].createdDate
            total          += countThisRound
            startingAfterID = stripeChargeModelList[-1].id

            self.assertGreaterEqual(firstDate, lastDate)
            self.assertLessEqual(countThisRound, limit)
        self.assertEqual(len(self.stripeChargeModelList), total)
