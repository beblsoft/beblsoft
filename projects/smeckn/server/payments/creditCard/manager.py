#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Credit Card Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.card.dao import StripeCardDAO
from base.stripe.python.customer.dao import StripeCustomerDAO
from smeckn.server.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger                   = logging.getLogger(__name__)
MAX_ACCOUNT_CREDIT_CARDS = 5


# -------------------------- CREDIT CARD MANAGER ---------------------------- #
class CreditCardManager(CommonManager):
    """
    Credit Card Manager Class
    """

    # ---------------------- CREATE ----------------------------------------- #
    @logFunc()
    def create(self, stripeCustomerID, source):  # pylint: disable=R0201
        """
        Create credit card
        Args
          stripeCustomerID:
            Stripe customer ID
          source:
            Source token
            Ex. "tok_visa"

        Returns
          Stripe Card Model
        """
        stripeCardModelList = StripeCardDAO.getAll(bStripe=self.mgr.payments.bStripe,
                                                   stripeCustomerID=stripeCustomerID)
        if len(stripeCardModelList) >= MAX_ACCOUNT_CREDIT_CARDS:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_CREDIT_CARD_COUNT_REACHED)

        stripeCardModel = StripeCardDAO.create(bStripe=self.mgr.payments.bStripe,
                                               stripeCustomerID=stripeCustomerID, source=source)
        return stripeCardModel

    # ---------------------- DELETE ----------------------------------------- #
    @logFunc()
    def deleteByID(self, stripeCustomerID, stripeCardID):  # pylint: disable=R0201
        """
        Delete credit card
        """
        StripeCardDAO.delete(bStripe=self.mgr.payments.bStripe,
                             stripeCustomerID=stripeCustomerID, stripeCardID=stripeCardID)

    # ---------------------- DEFAULT ---------------------------------------- #
    @logFunc()
    def setDefault(self, stripeCustomerID, stripeCardID):  # pylint: disable=R0201
        """
        Set credit card as customer default
        """
        StripeCustomerDAO.setDefaultCard(bStripe=self.mgr.payments.bStripe,
                                         stripeCustomerID=stripeCustomerID, stripeCardID=stripeCardID)
        return StripeCardDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                     stripeCustomerID=stripeCustomerID, stripeCardID=stripeCardID)

    @logFunc()
    def getDefault(self, stripeCustomerID):  # pylint: disable=R0201
        """
        Get customer default credit card
        """
        stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                                        stripeCustomerID=stripeCustomerID)
        stripeCardID        = stripeCustomerModel.defaultSourceID
        if not stripeCardID:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST)
        return StripeCardDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                     stripeCustomerID=stripeCustomerID, stripeCardID=stripeCardID)

    # ---------------------- RETRIEVE --------------------------------------- #
    @logFunc()
    def getByID(self, stripeCustomerID, stripeCardID):  # pylint: disable=R0201
        """
        Get StripeCardModel by ID
        """
        return StripeCardDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                     stripeCustomerID=stripeCustomerID, stripeCardID=stripeCardID)

    @logFunc()
    def getAll(self, stripeCustomerID):  # pylint: disable=R0201
        """
        Get list of all credit cards
        """
        return StripeCardDAO.getAll(bStripe=self.mgr.payments.bStripe, stripeCustomerID=stripeCustomerID)
