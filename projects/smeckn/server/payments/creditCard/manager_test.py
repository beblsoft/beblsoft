#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Credit Card Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.customer.dao import StripeCustomerDAO
from smeckn.server.test.common import CommonTestCase
from smeckn.server.payments.creditCard.manager import MAX_ACCOUNT_CREDIT_CARDS


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CreditCardManagerTestCase(CommonTestCase):
    """
    Test Credit Card Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True)

    @logFunc()
    def test_crud(self):
        """
        Test credit card crud
        """
        # Create and verify
        stripeCardModel  = self.mgr.payments.creditCard.create(stripeCustomerID=self.popStripeCustomerID,
                                                               source="tok_visa")
        self.assertEqual(stripeCardModel.brand, "Visa")

        # Delete and verify
        self.mgr.payments.creditCard.deleteByID(stripeCustomerID=self.popStripeCustomerID,
                                                stripeCardID=stripeCardModel.id)
        stripeCardModelList = self.mgr.payments.creditCard.getAll(stripeCustomerID=self.popStripeCustomerID)
        self.assertEqual(len(stripeCardModelList), 0)

    @logFunc()
    def test_createCardTooMany(self):
        """
        Test creating too many credit cards
        """
        source          = "tok_visa"
        exceptionRaised = False
        try:
            for i in range(MAX_ACCOUNT_CREDIT_CARDS + 1):
                _  = self.mgr.payments.creditCard.create(stripeCustomerID=self.popStripeCustomerID,
                                                         source=source)
        except BeblsoftError as e:
            self.assertEqual(e.code, BeblsoftErrorCode.STRIPE_CREDIT_CARD_COUNT_REACHED)
            self.assertEqual(i, MAX_ACCOUNT_CREDIT_CARDS)
            exceptionRaised = True
        if not exceptionRaised:
            raise Exception("Too many stripe cards added")

    @logFunc()
    def test_getAllCards(self):
        """
        Test retrieving all cards
        """
        super().setUp(stripeCards=True)
        stripeCardModelList = self.mgr.payments.creditCard.getAll(stripeCustomerID=self.popStripeCustomerID)
        self.assertEqual(len(stripeCardModelList), len(self.stripeCardModelList))

    @logFunc()
    def test_getSetDefault(self):
        """
        Test getting and setting customer default cards
        """
        super().setUp(stripeCards=True)
        for stripeCardModel in self.stripeCardModelList:
            self.mgr.payments.creditCard.setDefault(stripeCustomerID=self.popStripeCustomerID,
                                                    stripeCardID=stripeCardModel.id)
            defaultStripeCardModel = self.mgr.payments.creditCard.getDefault(stripeCustomerID=self.popStripeCustomerID)
            self.assertEqual(stripeCardModel.id, defaultStripeCardModel.id)
            stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                                            stripeCustomerID=self.popStripeCustomerID)
            self.assertEqual(stripeCustomerModel.defaultSourceID, stripeCardModel.id)
