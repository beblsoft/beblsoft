#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Customer Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.customer.dao import StripeCustomerDAO
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CustomerManagerTestCase(CommonTestCase):
    """
    Test Customer Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)

    @logFunc()
    def test_getCustomerNone(self):
        """
        Test having get customer return None
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerID = self.mgr.payments.customer.getCustomerID(aDBS=aDBS, raiseOnNone=False)
            self.assertIsNone(stripeCustomerID)

    @logFunc()
    def test_getCustomerRaise(self):
        """
        Test having get customer raise exception
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                _ = self.mgr.payments.customer.getCustomerID(aDBS=aDBS)
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.STRIPE_CUSTOMER_DOES_NOT_EXIST)
            else:
                raise Exception("Customer found when shouldn't exist")

    @logFunc()
    def test_getCustomerSuccess(self):
        """
        Test get customer success case
        """
        super().setUp(stripeCustomers=True)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerID = self.mgr.payments.customer.getCustomerID(aDBS=aDBS)
            self.assertEqual(stripeCustomerID, self.popStripeCustomerID)

    @logFunc()
    def test_getOrCreateCustomerPreexisting(self):
        """
        Test get or create customer when there is a preexisting customer
        """
        super().setUp(stripeCustomers=True)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerModel = self.mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            self.assertEqual(stripeCustomerModel.id, self.popStripeCustomerID)

    @logFunc()
    def test_getOrCreateCustomerNonexisting(self):
        """
        Test get or create customer when there is not an existing customer
        """
        stripeCustomerModel = None
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerModel = self.mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerID = self.mgr.payments.customer.getCustomerID(aDBS=aDBS)
            self.assertEqual(stripeCustomerModel.id, stripeCustomerID)
            StripeCustomerDAO.delete(bStripe=self.mgr.payments.bStripe,
                                     stripeCustomerID=stripeCustomerModel.id)
