#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Customer Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.customer.dao import StripeCustomerDAO
from smeckn.server.manager.common import CommonManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.accountInfo.dao import AccountInfoDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- CUSTOMER MANAGER ------------------------------- #
class CustomerManager(CommonManager):
    """
    Customer Manager Class
    """

    @logFunc()
    def getCustomerID(self, aDBS, raiseOnNone=True):  # pylint: disable=R0201
        """
        Return
          stripeCustomerID or None
        """
        stripeCustomerID  = None
        accountInfoModel  = AccountInfoDAO.get(aDBS=aDBS)
        stripeCustomerID  = accountInfoModel.stripeCustomerID

        if not stripeCustomerID and raiseOnNone:
            raise BeblsoftError(BeblsoftErrorCode.STRIPE_CUSTOMER_DOES_NOT_EXIST)

        return stripeCustomerID

    @logFunc()
    def getOrCreateStripeCustomerModel(self, aDBS):
        """
        Get or Create Customer Model
        Customer ID is committed to database

        Returns
          StripeCustomerModel
        """
        with AccountDatabase.lockTables(session=aDBS, lockStr="AccountInfo WRITE"):
            accountInfoModel        = AccountInfoDAO.get(aDBS=aDBS)
            aID                     = accountInfoModel.aID
            stripeCustomerID        = accountInfoModel.stripeCustomerID
            stripeCustomerModel     = None
            if stripeCustomerID:
                stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=self.mgr.payments.bStripe,
                                                                stripeCustomerID=stripeCustomerID)
            else:
                stripeCustomerModel = StripeCustomerDAO.create(bStripe=self.mgr.payments.bStripe,
                                                               metadata = {"aID": aID})
                AccountInfoDAO.update(aDBS=aDBS, accountInfoModel=accountInfoModel,
                                      stripeCustomerID=stripeCustomerModel.id)

            return stripeCustomerModel
