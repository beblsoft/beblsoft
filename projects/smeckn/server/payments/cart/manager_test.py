#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Cart Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.payments.unit.models import UnitCountModel


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CartManagerTestCase(CommonTestCase):
    """
    Test Cart Manager
    """

    @logFunc()
    def test_createCart(self):
        """
        Test creating cart
        """
        count              = 1000
        unitCountModelList = UnitCountModel.getFakeList(count=count)
        bCurrencyCode      = BeblsoftCurrencyCode.USD
        (cartModel, _)     = self.mgr.payments.cart.createCart(bCurrencyCode=bCurrencyCode,
                                                               unitCountModelList=unitCountModelList)

        totalCost            = 0
        for cartCostModel in cartModel.cartCostModelList:
            totalCost += cartCostModel.costAmount
        self.assertEqual(len(unitCountModelList), len(cartModel.cartCostModelList))
        self.assertEqual(totalCost, cartModel.totalCost)
