#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Cart Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.manager.common import CommonManager
from smeckn.server.payments.cart.models import CartModel


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- CART MANAGER ----------------------------------- #
class CartManager(CommonManager):
    """
    Cart Manager Class
    """

    @logFunc()
    def createCart(self, bCurrencyCode, unitCountModelList):
        """
        Create a cart and associated cart token
        Args
          bCurrencyCode:
            Beblsoft Currency Code
            Ex. BeblsoftCurrencyCode.USD
          unitCountModelList:
            UnitCountModel List

        Returns
          Tuple (CartModel, CartJWT)
        """
        if not unitCountModelList:
            raise BeblsoftError(BeblsoftErrorCode.STRIPE_MUST_SPECIFY_CART_UNITS)

        cartCostModelList = self.mgr.payments.unit.getCartCostModelList(bCurrencyCode=bCurrencyCode,
                                                                        unitCountModelList=unitCountModelList)

        totalCost         = sum(cartCostModel.costAmount for cartCostModel in cartCostModelList)
        cartModel         = CartModel(cartCostModelList=cartCostModelList, totalCost=totalCost,
                                      bCurrencyCode=bCurrencyCode)
        cartJWT           = self.mgr.jwt.cart.encodeJWT(cartModel)
        return (cartModel, cartJWT)
