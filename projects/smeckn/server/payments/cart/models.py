#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Cart Models Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.json.bObjectEncoder import BeblsoftJSONObjectEncoder
from smeckn.server.aDB.unitLedger.model import UnitType


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- CART COST MODEL -------------------------------- #
class CartCostModel(BeblsoftJSONObjectEncoder):
    """
    Cart Cost Model Class
    """

    def __init__(self, unitType, count, costAmount):
        """
        Initialize object
        Args
          unitType:
            Unit Type
            Ex. UnitType.TEXT_ANALYSIS
          count:
            Count of how many units
            Ex. 550
          costAmount:
            Cost Amount in cart currency units
            Ex. 100

        Note!
          This structure is encoded and passed over the wire.
          Special care must be taken when adding new fields.
        """
        self.unitType   = unitType
        self.count      = count
        self.costAmount = costAmount

    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        keyList = ["unitType", "count", "costAmount"]
        return self.attributeKeysToDict(keyList=keyList)

    @staticmethod
    @logFunc()
    def getFakeList(count=5, costAmount=10):
        """
        Return CartCostModel list with all UnitTypes
        Args
          count:
            Count for each UnitType in list
          costAmount:
            Cost amount for unit
        """
        cartCostModelList = []
        for unitType in list(UnitType):
            cartCostModel = CartCostModel(unitType=unitType, count=count, costAmount=costAmount)
            cartCostModelList.append(cartCostModel)
        return cartCostModelList

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


# -------------------------- CART MODEL ------------------------------------- #
class CartModel(BeblsoftJSONObjectEncoder):
    """
    Cart Model Class
    """

    def __init__(self, cartCostModelList, totalCost, bCurrencyCode):
        """
        Initialize object
        Args
          cartCostModelList:
            CartCostModel List
          totalCost:
            Total Cost in bCurrencyCode units
            Ex. 100
          bCurrencyCode:
            Beblsoft Currency Code
            Ex. BeblsoftCurrencyCode.USD

        Note!
          This structure is encoded and passed over the wire.
          Special care must be taken when adding new fields.
        """
        self.cartCostModelList = cartCostModelList
        self.totalCost         = totalCost
        self.bCurrencyCode     = bCurrencyCode

    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        keyList = ["cartCostModelList", "totalCost", "bCurrencyCode"]
        return self.attributeKeysToDict(keyList=keyList)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
