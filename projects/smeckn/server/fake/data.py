#!/usr/bin/env python3
"""
 NAME:
  data.py

 DESCRIPTION
  Fake Data Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.fake.mixins.account import FakeAccountMixin
from smeckn.server.fake.mixins.profileGroup import FakeProfileGroupMixin
from smeckn.server.fake.mixins.profile import FakeProfileMixin
from smeckn.server.fake.mixins.sync import FakeSyncMixin
from smeckn.server.fake.mixins.syncContent import FakeSyncContentMixin
from smeckn.server.fake.mixins.analysis import FakeAnalysisMixin
from smeckn.server.fake.mixins.text import FakeTextMixin
from smeckn.server.fake.mixins.compLangAnalysis import FakeCompLangAnalysisMixin
from smeckn.server.fake.mixins.compLang import FakeCompLangMixin
from smeckn.server.fake.mixins.compSentAnalysis import FakeCompSentAnalysisMixin
from smeckn.server.fake.mixins.photo import FakePhotoMixin
from smeckn.server.fake.mixins.video import FakeVideoMixin
from smeckn.server.fake.mixins.link import FakeLinkMixin
from smeckn.server.fake.mixins.unitLedger import FakeUnitLedgerMixin
from smeckn.server.fake.mixins.facebookProfile import FakeFacebookProfileMixin
from smeckn.server.fake.mixins.facebookPost import FakeFacebookPostMixin
from smeckn.server.fake.mixins.facebookPhoto import FakeFacebookPhotoMixin
from smeckn.server.fake.mixins.facebookTestUser import FakeFacebookTestUserMixin
from smeckn.server.fake.mixins.facebookTestUserData import FakeFacebookTestUserDataMixin
from smeckn.server.fake.mixins.facebookTestUserProfile import FakeFacebookTestUserProfileMixin
from smeckn.server.fake.mixins.stripeCustomer import FakeStripeCustomerMixin
from smeckn.server.fake.mixins.stripeCard import FakeStripeCardMixin
from smeckn.server.fake.mixins.stripeCharge import FakeStripeChargeMixin


# ---------------------------- FAKE DATA CLASS ------------------------------ #
class FakeData(
        FakeAccountMixin, FakeProfileGroupMixin, FakeProfileMixin,
        FakeFacebookProfileMixin, FakeFacebookPostMixin, FakeFacebookPhotoMixin,
        FakeFacebookTestUserMixin, FakeFacebookTestUserDataMixin, FakeFacebookTestUserProfileMixin,
        FakeSyncMixin, FakeSyncContentMixin, FakeAnalysisMixin,
        FakeTextMixin, FakeCompLangAnalysisMixin, FakeCompLangMixin, FakeCompSentAnalysisMixin,
        FakePhotoMixin,
        FakeVideoMixin,
        FakeLinkMixin,
        FakeUnitLedgerMixin,
        FakeStripeCustomerMixin, FakeStripeCardMixin, FakeStripeChargeMixin):
    """
    Fake Data
    """

    def __init__(self, mgr):
        """
        Initialize object
        """
        self.mgr   = mgr
        self.cfg   = self.mgr.cfg
        self.fApp  = self.mgr.app.fApp
        self.gDB   = self.mgr.gdb.gDB
        self.dbp   = self.mgr.dbp

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUp(self,  # pylint: disable=W0221,R0912, R0915
              accounts=False,
              profileGroups=False,
              profiles=False,
              facebookProfiles=False,
              facebookPosts=False,
              facebookPostsFull=False,
              facebookPhotos=False,
              facebookTestUsers=False,
              facebookTestUserData=False,
              facebookTestUserProfiles=False,
              syncs=False,
              syncsForAllProfilesSuccess=False,
              syncContent=False,
              analyses=False,
              text=False,
              textFull=False,
              compLangAnalysis=False,
              compLang=False,
              compSentAnalysis=False,
              photos=False,
              videos=False,
              links=False,
              unitLedger=False,
              stripeCustomers=False,
              stripeCards=False,
              stripeCharges=False):
        """
        SetUp Fake Data
        """
        if accounts:
            self.setUpAccounts()
        if profileGroups:
            self.setUpProfileGroups()
        if profiles:
            self.setUpProfiles()
        if facebookProfiles:
            self.setUpFacebookProfiles()
        if facebookPosts:
            self.setUpFacebookPosts()
        if facebookPostsFull:
            self.setUpFacebookPostsFull()
        if facebookPhotos:
            self.setUpFacebookPhotos()
        if facebookTestUsers:
            self.setUpFacebookTestUsers()
        if facebookTestUserData:
            self.setUpFacebookTestUserData()
        if facebookTestUserProfiles:
            self.setUpFacebookTestUserProfiles()
        if syncs:
            self.setUpSyncs()
        if syncsForAllProfilesSuccess:
            self.setUpSyncForAllProfilesSuccess()
        if syncContent:
            self.setUpSyncContent()
        if analyses:
            self.setUpAnalyses()
        if text:
            self.setUpText()
        if textFull:
            self.setUpTextFull()
        if compLangAnalysis:
            self.setUpCompLangAnalysis()
        if compLang:
            self.setUpCompLang()
        if compSentAnalysis:
            self.setUpCompSentAnalysis()
        if photos:
            self.setUpPhotos()
        if videos:
            self.setUpVideos()
        if links:
            self.setUpLinks()
        if unitLedger:
            self.setUpUnitLedger()
        if stripeCustomers:
            self.setUpStripeCustomers()
        if stripeCards:
            self.setUpStripeCards()
        if stripeCharges:
            self.setUpStripeCharges()

    # ---------------------------- TEARDOWN --------------------------------- #
    @logFunc()
    def tearDown(self):
        """
        TearDown Fake Data
        """
        self.tearDownStripeCreditCards()
        self.tearDownStripeCustomers()
        self.tearDownAccounts()
