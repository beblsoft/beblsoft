#!/usr/bin/env python3
"""
 NAME:
  photo.py

 DESCRIPTION
  Fake Photo Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE PHOTO MIXIN CLASS ----------------------- #
class FakePhotoMixin():
    """
    Fake Photo Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpPhotos(self, popPhotoIdx=0, nPhotos=5):
        """
        Setup Photos
        Dependency Graph: Accounts
                            |
                          Photos
        """
        self.photoModelList            = self.dbp.photoP.populateN(aID=self.popAccountID,
                                                                   count=nPhotos)
        self.popPhotoIdx               = popPhotoIdx
        self.popPhotoSMID              = self.photoModelList[self.popPhotoIdx].smID
        self.nonPopPhotoIdx            = ((self.popPhotoIdx + 1) %
                                          len(self.photoModelList))
        self.nonPopPhotoSMID           = self.photoModelList[self.nonPopPhotoIdx].smID
