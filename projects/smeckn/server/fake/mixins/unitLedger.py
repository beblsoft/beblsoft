#!/usr/bin/env python3
"""
 NAME:
  unitLedger.py

 DESCRIPTION
  Fake Unit Ledger Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from smeckn.server.aDB.unitLedger.model import UnitType


# ---------------------------- FAKE UNIT LEDGER MIXIN CLASS ----------------- #
class FakeUnitLedgerMixin():
    """
    Fake Unit Ledger Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpUnitLedger(self, nInitialTotal=1000, nInitialHeld=100):
        """
        Setup Unit Ledger
        Dependency Graph: Accounts
                            |
                          UnitLedger

        Resultant Object:
          self.unitTypeDict[UnitType].nInitialTotal
                                     .nInitialHeld
                                     .unitLedgerAdditionModel
                                     .unitLedgerAdditionSMID
                                     .unitLedgerHoldModel
                                     .nInitialHeld
                                     .unitLedgerHoldSMID
        """
        self.unitTypeDict = {}
        self.dbp.unitLedgerP.depopulateAll(aID=self.popAccountID)  # Delete account initial units
        for unitType in list(UnitType):
            [additionModel, holdModel] = self.dbp.unitLedgerP.populateCounts(aID=self.popAccountID,
                                                                             unitType=unitType,
                                                                             totalCount=nInitialTotal,
                                                                             holdCount=nInitialHeld)
            bAttrDict                         = BeblsoftAttrDict()
            bAttrDict.nInitialTotal           = nInitialTotal
            bAttrDict.nInitialHeld            = nInitialHeld
            bAttrDict.unitLedgerAdditionModel = additionModel
            bAttrDict.unitLedgerAdditionSMID  = additionModel.smID
            bAttrDict.unitLedgerHoldModel     = holdModel
            bAttrDict.unitLedgerHoldSMID      = holdModel.smID
            self.unitTypeDict[unitType]       = bAttrDict
