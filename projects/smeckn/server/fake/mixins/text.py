#!/usr/bin/env python3
"""
 NAME:
  text.py

 DESCRIPTION
  Fake Text Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE TEXT MIXIN CLASS ------------------------ #
class FakeTextMixin():
    """
    Fake Text Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpText(self, popTextIdx=0, nText=5):
        """
        Setup Text
        Dependency Graph: Accounts
                            |
                          Text
        """
        self.textModelList             = self.dbp.textP.populateN(aID=self.popAccountID,
                                                                  count=nText)
        self.popTextIdx                = popTextIdx
        self.popTextSMID               = self.textModelList[self.popTextIdx].smID
        self.nonPopTextIdx             = ((self.popTextIdx + 1) %
                                          len(self.textModelList))
        self.nonPopTextSMID            = self.textModelList[self.nonPopTextIdx].smID

    @logFunc()
    def setUpTextFull(self, nText=200):
        """
        Setup Text Full
        Dependency Graph: Accounts
                            |
                          Text Full
        """
        self.textModelList             = self.dbp.textP.populateFull(aID=self.popAccountID,
                                                                     count=nText)
