#!/usr/bin/env python3
"""
 NAME:
  facebookTestUserData.py

 DESCRIPTION
  Fake Facebook Test User Data Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.facebook.python.user.dao import BFacebookUserDAO
from base.facebook.python.postMedia.dao import BFacebookPostMediaDAO


# ---------------------------- FAKE FACEBOOK TEST USER DATA MIXIN CLASS ----- #
class FakeFacebookTestUserDataMixin():
    """
    Fake Facebook Test User Data Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookTestUserData(self):
        """
        Setup Facebook Test User Data
        Dependency Graph: Facebook Test Users
                            |
                          Facebook Test User Data
        Resultant Object:
          self.fbTestUserDataDict[name].bFBUserModel
                                       .bFBPostModelList
                                       .bFBPostMediaModelList
                                       .bFBStatusPostMediaModel
                                       .bFBPhotoPostMediaModel
                                       .bFBLinkPostMediaModel
                                       .bFBVideoPostMediaModel
                                       .bFBPhotoModelList
                                       .bFBVideoModelList
        """
        self.fbTestUserDataDict = BeblsoftAttrDict()

        for name, fbTestUser in self.fbTestUserDict.items():
            fbTestUserData                = BeblsoftAttrDict()
            self.fbTestUserDataDict[name] = fbTestUserData

            # User ------------------------------------------------------------
            bFBUserModel   = BFacebookUserDAO.getByID(
                bFBGraph         = self.mgr.facebook.bFBGraph,
                userID           = fbTestUser.id,
                accessToken      = fbTestUser.accessToken)
            fbTestUserData.bFBUserModel = bFBUserModel

            # Post ------------------------------------------------------------
            bFBPostModelList = BFacebookUserDAO.getAllPosts(
                bFBGraph         = self.mgr.facebook.bFBGraph,
                userID           = fbTestUser.id,
                accessToken      = fbTestUser.accessToken,
                type             = "posts")
            fbTestUserData.bFBPostModelList = bFBPostModelList

            # Post Media ------------------------------------------------------
            bFBPostMediaModelList = []
            for bFBPostModel in bFBPostModelList:
                bFBPostMediaModel = BFacebookPostMediaDAO.getFromBFBPostModel(
                    bFBGraph         = self.mgr.facebook.bFBGraph,
                    bFBPostModel     = bFBPostModel,
                    accessToken      = fbTestUser.accessToken)
                bFBPostMediaModelList.append(bFBPostMediaModel)
            fbTestUserData.bFBPostMediaModelList = bFBPostMediaModelList


            # Status Post Media -----------------------------------------------
            fbTestUserData.bFBStatusPostMediaModel = None
            for bFBPostMediaModel in bFBPostMediaModelList:
                if bFBPostMediaModel.bFBPostModel.type == "status":
                    fbTestUserData.bFBStatusPostMediaModel = bFBPostMediaModel
                    break

            # Photo Post Media ------------------------------------------------
            fbTestUserData.bFBPhotoPostMediaModel = None
            for bFBPostMediaModel in bFBPostMediaModelList:
                if bFBPostMediaModel.bFBPostModel.type == "photo":
                    fbTestUserData.bFBPhotoPostMediaModel = bFBPostMediaModel
                    break

            # Link Post Media -------------------------------------------------
            fbTestUserData.bFBLinkPostMediaModel = None
            for bFBPostMediaModel in bFBPostMediaModelList:
                if bFBPostMediaModel.bFBPostModel.type == "link":
                    fbTestUserData.bFBLinkPostMediaModel = bFBPostMediaModel
                    break

            # Video Post Media ------------------------------------------------
            fbTestUserData.bFBVideoPostMediaModel = None
            for bFBPostMediaModel in bFBPostMediaModelList:
                if bFBPostMediaModel.bFBPostModel.type == "video":
                    fbTestUserData.bFBVideoPostMediaModel = bFBPostMediaModel
                    break

            # Photos ----------------------------------------------------------
            bFBPhotoModelList = BFacebookUserDAO.getAllPhotos(
                bFBGraph         = self.mgr.facebook.bFBGraph,
                userID           = fbTestUser.id,
                accessToken      = fbTestUser.accessToken,
                type             = "uploaded")
            fbTestUserData.bFBPhotoModelList = bFBPhotoModelList

            # Videos ----------------------------------------------------------
            bFBVideoModelList = BFacebookUserDAO.getAllVideos(
                bFBGraph         = self.mgr.facebook.bFBGraph,
                userID           = fbTestUser.id,
                accessToken      = fbTestUser.accessToken,
                type             = "uploaded")
            fbTestUserData.bFBVideoModelList = bFBVideoModelList
