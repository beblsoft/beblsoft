#!/usr/bin/env python3
"""
 NAME:
  compLang.py

 DESCRIPTION
  Fake Comp Lang Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE COMP LANG MIXIN CLASS ------------------- #
class FakeCompLangMixin():
    """
    Fake Comp Lang Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpCompLang(self, popCompLangIdx=0):
        """
        Setup Comprehend Language
        Dependency Graph: Accounts
                            |
                          Text
                            |
                          CompLangAnalysis
                            |
                          CompLang
        """
        self.compLangModelList         = self.dbp.compLangP.populateList(aID=self.popAccountID,
                                                                         compLangAnalysisSMID=self.popCompLangAnalysisSMID,
                                                                         nLanguages=5)
        self.popCompLangIdx            = popCompLangIdx
        self.popCompLangSMID           = self.compLangModelList[self.popCompLangIdx].smID
        self.nonPopCompLangIdx         = ((self.popCompLangIdx + 1) %
                                          len(self.compLangModelList))
        self.nonPopCompLangSMID        = self.compLangModelList[self.nonPopCompLangIdx].smID
