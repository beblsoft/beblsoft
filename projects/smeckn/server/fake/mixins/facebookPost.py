#!/usr/bin/env python3
"""
 NAME:
  facebookPost.py

 DESCRIPTION
  Fake Facebook Post Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.facebookPost.model import FacebookPostType


# ---------------------------- FAKE FACEBOOK POST MIXIN CLASS --------------- #
class FakeFacebookPostMixin():
    """
    Fake Facebook Post Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookPosts(self, popFBPostIdx=0,
                           nStatusPosts=5, nPhotoPosts=5, nVideoPosts=5, nLinkPosts=5):
        """
        Setup Facebook Posts
        Dependency Graph: Accounts
                            |
                          ProfileGroups
                            |
                          Facebook Profiles
                            |
                          Facebook Posts
        """
        curFBPostID                         = 100000000000000
        if nStatusPosts > 0:
            fbPostIDList                    = list(range(curFBPostID, curFBPostID + nStatusPosts))
            self.fbStatusPostModelList      = self.dbp.facebookPostP.populateList(aID=self.popAccountID,
                                                                                  fbpSMID=self.popFBPSMID,
                                                                                  fbIDList=fbPostIDList,
                                                                                  postType=FacebookPostType.STATUS)
            self.popFBStatusPostIdx         = popFBPostIdx
            self.popFBStatusPostSMID        = self.fbStatusPostModelList[self.popFBStatusPostIdx].smID
            self.nonPopFBStatusPostIdx      = ((self.popFBStatusPostIdx + 1) %
                                               len(self.fbStatusPostModelList))
            self.nonPopFBStatusPostSMID     = self.fbStatusPostModelList[self.nonPopFBStatusPostIdx].smID
            curFBPostID += nStatusPosts

        if nPhotoPosts > 0:
            fbPostIDList                    = list(range(curFBPostID, curFBPostID + nPhotoPosts))
            self.fbPhotoPostModelList       = self.dbp.facebookPostP.populateList(aID=self.popAccountID,
                                                                                  fbpSMID=self.popFBPSMID,
                                                                                  fbIDList=fbPostIDList,
                                                                                  postType=FacebookPostType.PHOTO)
            self.popFBPhotoPostIdx          = popFBPostIdx
            self.popFBPhotoPostSMID         = self.fbPhotoPostModelList[self.popFBPhotoPostIdx].smID
            self.nonPopFBPhotoPostIdx       = ((self.popFBPhotoPostIdx + 1) %
                                               len(self.fbPhotoPostModelList))
            self.nonPopFBPhotoPostSMID      = self.fbPhotoPostModelList[self.nonPopFBPhotoPostIdx].smID
            curFBPostID += nPhotoPosts

        if nVideoPosts > 0:
            fbPostIDList                    = list(range(curFBPostID, curFBPostID + nVideoPosts))
            self.fbVideoPostModelList       = self.dbp.facebookPostP.populateList(aID=self.popAccountID,
                                                                                  fbpSMID=self.popFBPSMID,
                                                                                  fbIDList=fbPostIDList,
                                                                                  postType=FacebookPostType.VIDEO)
            self.popFBVideoPostIdx          = popFBPostIdx
            self.popFBVideoPostSMID         = self.fbVideoPostModelList[self.popFBVideoPostIdx].smID
            self.nonPopFBVideoPostIdx       = ((self.popFBVideoPostIdx + 1) %
                                               len(self.fbVideoPostModelList))
            self.nonPopFBVideoPostSMID      = self.fbVideoPostModelList[self.nonPopFBVideoPostIdx].smID
            curFBPostID += nVideoPosts

        if nLinkPosts > 0:
            fbPostIDList                    = list(range(curFBPostID, curFBPostID + nLinkPosts))
            self.fbLinkPostModelList        = self.dbp.facebookPostP.populateList(aID=self.popAccountID,
                                                                                  fbpSMID=self.popFBPSMID,
                                                                                  fbIDList=fbPostIDList,
                                                                                  postType=FacebookPostType.LINK)
            self.popFBLinkPostIdx           = popFBPostIdx
            self.popFBLinkPostSMID          = self.fbLinkPostModelList[self.popFBLinkPostIdx].smID
            self.nonPopFBLinkPostIdx        = ((self.popFBLinkPostIdx + 1) %
                                               len(self.fbLinkPostModelList))
            self.nonPopFBLinkPostSMID       = self.fbLinkPostModelList[self.nonPopFBLinkPostIdx].smID
            curFBPostID += nLinkPosts

    @logFunc()
    def setUpFacebookPostsFull(self, nStatus=100, nPhoto=30, nVideo=30, nLink=30):
        """
        Setup FacebookPosts Full
        Dependency Graph: Accounts
                            |
                          ProfileGroups
                            |
                          Facebook Profiles
                            |
                          Facebook Posts Full
        """
        self.fbPostModelList = self.dbp.facebookPostP.populateFull(aID=self.popAccountID, fbpSMID=self.popFBPSMID,
                                                                   nStatus=nStatus, nPhoto=nPhoto,
                                                                   nVideo=nVideo, nLink=nLink)
