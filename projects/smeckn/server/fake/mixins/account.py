#!/usr/bin/env python3
"""
 NAME:
  account.py

 DESCRIPTION
  Fake Account Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from datetime import timedelta
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType


# ---------------------------- FAKE ACCOUNT MIXIN CLASS --------------------- #
class FakeAccountMixin():
    """
    Fake Account Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpAccounts(self, popAccountIdx=0):
        """
        Setup Accounts
        Dependency Graph: N/A
        """
        # Admin
        self.adminEmail                = "success+smecknServerAdmin@simulator.amazonses.com"  # SES Mailbox simulator
        self.adminPassword             = "12390854skdvfjh3d"
        self.adminModel                = self.dbp.accountP.populateOne(email=self.adminEmail, password=self.adminPassword,
                                                                       aType=AccountType.ADMIN)
        self.adminAccountID            = self.adminModel.id
        (authT, accountT)              = self.getAccountTokens(email=self.adminEmail)
        self.adminAuthToken            = authT
        self.adminAccountToken         = accountT

        # Regular
        self.emailList                 = ["foo@goo.com", "bar@far.com"]
        self.passwordList              = ["asd9f8ukjmh39os8df", "32309rs,dfkqewfkldf"]
        self.aTypeList                 = [AccountType.TEST, AccountType.TEST]
        self.aModelList                = self.dbp.accountP.populateList(emailList=self.emailList,
                                                                        passwordList=self.passwordList,
                                                                        aTypeList=self.aTypeList)
        self.accountIDList             = [aModel.id for aModel in self.aModelList]
        self.authTokenList             = []
        self.accountTokenList          = []
        for email in self.emailList:
            (authT, accountT)          = self.getAccountTokens(email)
            self.authTokenList.append(authT)
            self.accountTokenList.append(accountT)

        # Populated/Non Populated
        self.popAccountIdx             = popAccountIdx
        self.popAccountEmail           = self.emailList[popAccountIdx]
        self.popAccountPassword        = self.passwordList[popAccountIdx]
        self.popAccountID              = self.accountIDList[popAccountIdx]
        self.popAuthToken              = self.authTokenList[popAccountIdx]
        self.popAccountToken           = self.accountTokenList[popAccountIdx]
        self.nonPopAccountIdx          = ((self.popAccountIdx + 1) %
                                          len(self.accountIDList))
        self.nonPopAccountID           = self.accountIDList[self.nonPopAccountIdx]
        self.nonPopAuthToken           = self.authTokenList[self.nonPopAccountIdx]
        self.nonPopAccountToken        = self.accountTokenList[self.nonPopAccountIdx]

    @logFunc()
    def setUpAccount(self, email, password, popAccountIdx=0):
        """
        Setup Account
        Dependency Graph: N/A
        """
        # Regular
        self.aModel                    = self.dbp.accountP.populateOne(email=email, password=password,
                                                                       aType=AccountType.TEST)
        self.aModelList                = [self.aModel]
        (authT, accountT)              = self.getAccountTokens(email)

        # Populated/Non Populated
        self.popAccountIdx             = popAccountIdx
        self.popAccountEmail           = email
        self.popAccountPassword        = password
        self.popAccountID              = self.aModel.id
        self.popAuthToken              = authT
        self.popAccountToken           = accountT

    @logFunc()
    def getAccountTokens(self, email, authTokenExpDays=30, accountTokenExpDays=30):  # pylint: disable=R0201
        """
        Return Login Credentials
        Returns
          Tuple (authToken, accountToken)
        """
        with self.gDB.sessionScope(commit=False) as gs:
            aModel    = AccountDAO.getByEmail(gs=gs, email=email)
            aDBSModel = aModel.accountDBServer

            # Create Auth Token
            expDelta  = timedelta(days=authTokenExpDays)
            aAdmin    = (aModel.type == AccountType.ADMIN)
            authToken = self.mgr.jwt.auth.encodeJWT(aID=aModel.id, aAdmin=aAdmin, expDelta=expDelta)

            # Create Account Token
            expDelta     = timedelta(days=accountTokenExpDays)
            accountToken = self.mgr.jwt.account.encodeJWT(aID=aModel.id, readDB=aDBSModel.readDomain,
                                                          writeDB=aDBSModel.writeDomain, expDelta=expDelta)
        return (authToken, accountToken)

    # ---------------------------- TEARODWN --------------------------------- #
    @logFunc()
    def tearDownAccounts(self):  # pylint: disable=R0201
        """
        TearDown Accounts
        """
        self.mgr.dbp.accountP.depopulateAll()

    @logFunc()
    def tearDownAccount(self, email, ensureTestAccount=True):  # pylint: disable=R0201
        """
        TearDown Account
        Args:
          email:
            Account email to teardown
          ensureTestAccount:
            If True, Ensure that account is a TEST account before tearing down
        """
        if ensureTestAccount:
            aModel = None
            with self.gDB.sessionScope(commit=False) as gs:
                aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)

            if aModel and aModel.type != AccountType.TEST:
                raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED)

        # Acutally delete account
        self.mgr.dbp.accountP.depopulateByEmail(email=email)
