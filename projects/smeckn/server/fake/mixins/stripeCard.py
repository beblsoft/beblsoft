#!/usr/bin/env python3
"""
 NAME:
  stripeCard.py

 DESCRIPTION
  Fake Stripe Card Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.run.bParallel import runFuncOnList
from base.stripe.python.card.dao import StripeCardDAO


# ---------------------------- FAKE STRIPE CARD MIXIN CLASS ----------------- #
class FakeStripeCardMixin():
    """
    Fake Stripe Card Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpStripeCards(self, popStripeCardIdx=0, nCards=2, source="tok_visa"):
        """
        Setup Stripe Cards
        Dependency Graph: Accounts
                            |
                          Stripe Customers
                            |
                          Stripe Cards
        """
        self.stripeCardModelList = []

        # Create credit cards in parallel
        def createCard(_):  # pylint: disable=C0111
            stripeCardModel = self.mgr.payments.creditCard.create(stripeCustomerID=self.popStripeCustomerID,
                                                                  source=source)
            self.stripeCardModelList.append(stripeCardModel)
        runFuncOnList(func=createCard, lst=list(range(0, nCards)), parallel=False)

        self.popStripeCardIdx          = popStripeCardIdx
        self.popStripeCardID           = self.stripeCardModelList[popStripeCardIdx].id
        self.nonPopStripeCardIdx       = ((self.popStripeCardIdx + 1) %
                                          len(self.stripeCardModelList))
        self.nonPopStripeCardID        = self.stripeCardModelList[self.nonPopStripeCardIdx].id


    # ---------------------------- TEARDOWN --------------------------------- #
    @logFunc()
    def tearDownStripeCreditCards(self):
        """
        Teardown Stripe Credit Cards
        """
        # Delete credit cards in parallel
        def deleteCreditCard(stripeCardModel): #pylint: disable=C0111
            StripeCardDAO.delete(bStripe=self.mgr.payments.bStripe,
                                 stripeCustomerID=self.popStripeCustomerID,
                                 stripeCardID=stripeCardModel.id)

        stripeCardModelList = getattr(self, "stripeCreditCardModelList", [])
        if stripeCardModelList:
            runFuncOnList(func=deleteCreditCard, lst=stripeCardModelList, parallel=True)
