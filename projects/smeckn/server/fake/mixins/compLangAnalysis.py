#!/usr/bin/env python3
"""
 NAME:
  compLangAnalysis.py

 DESCRIPTION
  Fake Comp Lang Analysis Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE COMP LANG ANALYSIS MIXIN CLASS ---------- #
class FakeCompLangAnalysisMixin():
    """
    Fake Comp Lang Analysis Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpCompLangAnalysis(self, popCompLangAnalysisIdx=0):
        """
        Setup Comprehend Language Analysis
        Dependency Graph: Accounts
                            |
                          Text
                            |
                          CompLangAnalysis
        """
        self.compLangAnalysisModelList = self.dbp.compLangAnalysisP.populateList(aID=self.popAccountID,
                                                                                 textSMIDList=[self.popTextSMID])
        self.popCompLangAnalysisIdx    = popCompLangAnalysisIdx
        self.popCompLangAnalysisSMID   = self.compLangAnalysisModelList[self.popCompLangAnalysisIdx].smID
