#!/usr/bin/env python3
"""
 NAME:
  facebookTestUserProfile.py

 DESCRIPTION
  Fake Facebook Test User Profile Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase


# ---------------------------- FAKE FACEBOOK TEST USER PROFILE MIXIN CLASS -- #
class FakeFacebookTestUserProfileMixin():
    """
    Fake Facebook Test User Profile Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookTestUserProfiles(self, fbTestUserName="small", popFBPIdx=0):
        """
        Setup Facebook Test Users
        Dependency Graph: Accounts
                            |
                          ProfileGroups      Facebook Test Users
                            |                  |
                          Facebook Test User Profiles
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbTestUser            = self.fbTestUserDict[fbTestUserName]
            fbpModel              = self.mgr.facebook.create(aDBS=aDBS, pgID=self.popPGID, fbpID=fbTestUser.id,
                                                             shortTermAccessToken=fbTestUser.accessToken)
            self.fbpModelList     = [fbpModel]
            self.popFBPIdx        = popFBPIdx
            self.popFBPSMID       = self.fbpModelList[self.popFBPIdx].smID
