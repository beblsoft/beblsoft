#!/usr/bin/env python3
"""
 NAME:
  analysis.py

 DESCRIPTION
  Fake Analysis Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE ANALYSIS MIXIN CLASS -------------------- #
class FakeAnalysisMixin():
    """
    Fake Analysis Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpAnalyses(self, popAnalysisIdx=0):
        """
        Setup Analyses
        Dependency Graph: Accounts
                            |
                          Profile Groups
                            |
                          Profiles
                            |
                          Analyses
        """
        self.analysisModel     = self.dbp.analysisP.populateOne(aID=self.popAccountID,
                                                                profileSMID=self.popProfileSMID)
        self.analysisModelList = [self.analysisModel]
        self.popAnalysisIdx    = popAnalysisIdx
        self.popAnalysisSMID   = self.analysisModelList[self.popAnalysisIdx].smID
