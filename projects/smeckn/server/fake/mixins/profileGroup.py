#!/usr/bin/env python3
"""
 NAME:
  profileGroup.py

 DESCRIPTION
  Fake Profile Group Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE PROFILE GROUP MIXIN CLASS --------------- #
class FakeProfileGroupMixin():
    """
    Fake Profile Group Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpProfileGroups(self, popPGIdx=0):
        """
        Setup Profile Groups
        Dependency Graph: Accounts
                            |
                          Profile Groups
        """
        self.pgNameList                = ["Sarah", "Ben", "John", "Randy", "Judith"]
        self.pgModelList               = self.dbp.profileGroupP.populateList(aID=self.popAccountID,
                                                                             nameList=self.pgNameList)
        self.popPGIdx                  = popPGIdx
        self.popPGID                   = self.pgModelList[self.popPGIdx].id
        self.nonPopPGIdx               = (self.popPGIdx + 1) % len(self.pgModelList)
        self.nonPopPGID                = self.pgModelList[self.nonPopPGIdx].id
