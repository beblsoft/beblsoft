#!/usr/bin/env python3
"""
 NAME:
  stripeCharge.py

 DESCRIPTION
  Fake Stripe Charge Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import time
import random
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.run.bParallel import runFuncOnList
from base.stripe.python.charge.dao import StripeChargeDAO
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode


# ---------------------------- FAKE STRIPE CHARGE MIXIN CLASS --------------- #
class FakeStripeChargeMixin():
    """
    Fake Stripe Charge Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpStripeCharges(self, popStripeChargeIdx=0, nCharges=10, maxSleepSecs=2):
        """
        Setup Stripe Charges
        Dependency Graph: Accounts
                            |
                          Stripe Customers
                            |
                          Stripe Cards
                            |
                          Stripe Charges
        """
        self.stripeChargeModelList     = []
        bCurrencyCode                  = BeblsoftCurrencyCode.USD
        amount                         = 100

        # Create charges in parallel
        # Sleep randomly so not all charges are at the same time
        def createCharge(_):  # pylint: disable=C0111
            time.sleep(random.randint(0, maxSleepSecs))
            stripeChargeModel = StripeChargeDAO.create(
                bStripe=self.mgr.payments.bStripe, stripeCustomerID=self.popStripeCustomerID,
                amount=amount, currency=bCurrencyCode.toStripeCode())
            self.stripeChargeModelList.append(stripeChargeModel)
        runFuncOnList(func=createCharge, lst=list(range(0, nCharges)), parallel=False)

        self.popStripeChargeIdx        = popStripeChargeIdx
        self.popStripeChargeID         = self.stripeChargeModelList[popStripeChargeIdx].id
        self.nonPopStripeChargeIdx     = ((self.popStripeChargeIdx + 1) %
                                          len(self.stripeChargeModelList))
        self.nonPopStripeChargeID      = self.stripeChargeModelList[self.nonPopStripeChargeIdx].id
