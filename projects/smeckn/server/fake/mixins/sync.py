#!/usr/bin/env python3
"""
 NAME:
  sync.py

 DESCRIPTION
  Fake Sync Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.sync.model import SyncStatus


# ---------------------------- FAKE SYNC MIXIN CLASS ------------------------ #
class FakeSyncMixin():
    """
    Fake Sync Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpSyncs(self, popSyncIdx=0):
        """
        Setup Syncs
        Dependency Graph: Accounts
                            |
                          Profile Groups
                            |
                          Profiles
                            |
                          Syncs
        """
        self.syncModel     = self.dbp.syncP.populateOne(aID=self.popAccountID,
                                                        profileSMID=self.popProfileSMID)
        self.syncModelList = [self.syncModel]
        self.popSyncIdx    = popSyncIdx
        self.popSyncSMID   = self.syncModelList[self.popSyncIdx].smID

    @logFunc()
    def setUpSyncForAllProfilesSuccess(self, popSyncIdx=0):
        """
        SetUp successfull syncs for all profiles
        """
        self.syncModelList = self.dbp.syncP.populateForAllProfiles(aID=self.popAccountID,
                                                                   status=SyncStatus.SUCCESS)
        self.popSyncIdx    = popSyncIdx
        self.popSyncSMID   = self.syncModelList[self.popSyncIdx].smID
