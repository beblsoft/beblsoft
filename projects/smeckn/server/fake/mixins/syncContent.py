#!/usr/bin/env python3
"""
 NAME:
  syncContent.py

 DESCRIPTION
  Fake Sync Content Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE SYNC CONTENT MIXIN CLASS ---------------- #
class FakeSyncContentMixin():
    """
    Fake Sync Content Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpSyncContent(self, popSyncContentIdx=0, nSyncContent=5):
        """
        Setup SyncContent
        Dependency Graph: Accounts
                            |
                          Syncs
                            |
                          Sync Content
        """
        self.syncContentModelList      = self.dbp.syncContentP.populateN(aID=self.popAccountID,
                                                                         syncSMID=self.popSyncSMID,
                                                                         count=nSyncContent)
        self.popSyncContentIdx         = popSyncContentIdx
        self.popSyncContentSMID        = self.syncContentModelList[self.popSyncContentIdx].smID
        self.nonPopSyncContentIdx      = ((self.popSyncContentIdx + 1) %
                                          len(self.syncContentModelList))
        self.nonPopSyncContentSMID     = self.syncContentModelList[self.nonPopSyncContentIdx].smID
