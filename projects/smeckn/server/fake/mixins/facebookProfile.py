#!/usr/bin/env python3
"""
 NAME:
  facebookProfile.py

 DESCRIPTION
  Fake Facebook Profile Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE FACEBOOK PROFILE MIXIN CLASS ------------ #
class FakeFacebookProfileMixin():
    """
    Fake Facebook Profile Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookProfiles(self, popFBPIdx=0):
        """
        Setup Facebook Profiles
        Dependency Graph: Accounts
                            |
                          ProfileGroups
                            |
                          Facebook Profiles
        """
        self.fbID                      = 100000000000000
        fbpModel                       = self.dbp.facebookProfileP.populateOne(aID=self.popAccountID,
                                                                               pgID=self.popPGID, fbID=self.fbID)
        self.fbpModelList              = [fbpModel]
        self.popFBPIdx                 = popFBPIdx
        self.popFBPSMID                = self.fbpModelList[self.popFBPIdx].smID
