#!/usr/bin/env python3
"""
 NAME:
  facebookTestUser.py

 DESCRIPTION
  Fake Facebook Test User Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.facebook.python.testUser.dao import BFacebookTestUserDAO


# ---------------------------- FAKE FACEBOOK TEST USER MIXIN CLASS ---------- #
class FakeFacebookTestUserMixin():
    """
    Fake Facebook Test User Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookTestUsers(self, fbTestUserName=None):
        """
        Setup Facebook Test Users
        Dependency Graph: Facebook Test Users

        Resultant Object:
          self.fbTestUserDict[name].id
                                   .accessToken
        """
        nameList            = ["small"]
        idList              = [self.cfg.FACEBOOK_SMECKN_TESTUSER_SMALL]
        self.fbTestUserDict = BeblsoftAttrDict()

        for idx, name in enumerate(nameList):
            if not fbTestUserName or fbTestUserName == name:
                fbTestUserID                          = idList[idx]
                self.fbTestUserDict[name]             = BeblsoftAttrDict()
                self.fbTestUserDict[name].id          = fbTestUserID
                self.fbTestUserDict[name].accessToken = BFacebookTestUserDAO.getAccessToken(
                    bFBGraph                             = self.mgr.facebook.bFBGraph,
                    appID                                = self.mgr.facebook.bFBGraph.appID,
                    testUserID                           = fbTestUserID,
                    accessToken                          = self.mgr.facebook.bFBGraph.appAccessToken)
