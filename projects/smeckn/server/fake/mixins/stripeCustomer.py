#!/usr/bin/env python3
"""
 NAME:
  stripeCustomer.py

 DESCRIPTION
  Fake Stripe Customer Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.run.bParallel import runFuncOnList
from base.stripe.python.customer.dao import StripeCustomerDAO
from smeckn.server.aDB import AccountDatabase


# ---------------------------- FAKE STRIPE CUSTOMER MIXIN CLASS ------------- #
class FakeStripeCustomerMixin():
    """
    Fake Stripe Customer Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpStripeCustomers(self, popStripeCustomerIdx=0):
        """
        Setup Stripe Customers
        Dependency Graph: Accounts
                            |
                          Stripe Customers
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            stripeCustomerModel          = self.mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            self.stripeCustomerModelList = [stripeCustomerModel]
            self.popStripeCustomerModel  = self.stripeCustomerModelList[popStripeCustomerIdx]
            self.popStripeCustomerIdx    = popStripeCustomerIdx
            self.popStripeCustomerID     = self.stripeCustomerModelList[popStripeCustomerIdx].id

    # ---------------------------- TEARDOWN --------------------------------- #
    @logFunc()
    def tearDownStripeCustomers(self):
        """
        Teardown Stripe Customers
        """
        # Delete customers in parallel
        def deleteCustomer(stripeCustomerModel):  # pylint: disable=C0111
            StripeCustomerDAO.delete(bStripe=self.mgr.payments.bStripe,
                                     stripeCustomerID=stripeCustomerModel.id)

        stripeCustomerModelList = getattr(self, "stripeCustomerModelList", [])
        if stripeCustomerModelList:
            runFuncOnList(func=deleteCustomer, lst=stripeCustomerModelList, parallel=True)
