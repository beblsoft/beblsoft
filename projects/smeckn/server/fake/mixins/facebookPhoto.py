#!/usr/bin/env python3
"""
 NAME:
  facebookPhoto.py

 DESCRIPTION
  Fake Facebook Photo Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE FACEBOOK PHOTO MIXIN CLASS -------------- #
class FakeFacebookPhotoMixin():
    """
    Fake Facebook Photo Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpFacebookPhotos(self, popFBPhotoIdx=0, nPhotos=5):
        """
        Setup Facebook Photos
        Dependency Graph: Accounts
                            |
                          ProfileGroups
                            |
                          Facebook Profiles
                            |
                          Facebook Photos
        """
        baseFBID                       = 100000000000000
        fbIDList                       = list(range(baseFBID, baseFBID + nPhotos))
        self.fbPhotoModelList          = self.dbp.facebookPhotoP.populateList(aID=self.popAccountID,
                                                                              fbpSMID=self.popFBPSMID,
                                                                              fbIDList=fbIDList)
        self.popFBPhotoIdx             = popFBPhotoIdx
        self.popFBPhotoSMID            = self.fbPhotoModelList[self.popFBPhotoIdx].smID
        self.nonPopFBPhotoIdx          = ((self.popFBPhotoIdx + 1) %
                                          len(self.fbPhotoModelList))
        self.nonPopFBPhotoSMID         = self.fbPhotoModelList[self.nonPopFBPhotoIdx].smID
