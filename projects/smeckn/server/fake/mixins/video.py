#!/usr/bin/env python3
"""
 NAME:
  video.py

 DESCRIPTION
  Fake Video Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE VIDEO MIXIN CLASS ----------------------- #
class FakeVideoMixin():
    """
    Fake Video Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpVideos(self, popVideoIdx=0, nVideos=5):
        """
        Setup Videos
        Dependency Graph: Accounts
                            |
                          Videos
        """
        self.videoModelList            = self.dbp.videoP.populateN(aID=self.popAccountID,
                                                                   count=nVideos)
        self.popVideoIdx               = popVideoIdx
        self.popVideoSMID              = self.videoModelList[self.popVideoIdx].smID
        self.nonPopVideoIdx            = ((self.popVideoIdx + 1) %
                                          len(self.videoModelList))
        self.nonPopVideoSMID           = self.videoModelList[self.nonPopVideoIdx].smID
