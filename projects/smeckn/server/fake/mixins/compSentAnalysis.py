#!/usr/bin/env python3
"""
 NAME:
  compSentAnalysis.py

 DESCRIPTION
  Fake Comp Sent Analysis Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE COMP SENT ANALYSIS MIXIN CLASS ---------- #
class FakeCompSentAnalysisMixin():
    """
    Fake Comp Sent Analysis Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpCompSentAnalysis(self, popCompSentAnalysisIdx=0):
        """
        Setup Comprehend Sentiment Analysis
        Dependency Graph: Accounts
                            |
                          Text
                            |
                          CompSentAnalysis
        """
        self.compSentAnalysisModelList = self.dbp.compSentAnalysisP.populateList(aID=self.popAccountID,
                                                                                 textSMIDList=[self.popTextSMID])
        self.popCompSentAnalysisIdx    = popCompSentAnalysisIdx
        self.popCompSentAnalysisSMID   = self.compSentAnalysisModelList[self.popCompSentAnalysisIdx].smID
