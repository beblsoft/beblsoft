#!/usr/bin/env python3
"""
 NAME:
  link.py

 DESCRIPTION
  Fake Link Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE LINK MIXIN CLASS ------------------------ #
class FakeLinkMixin():
    """
    Fake Link Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpLinks(self, popLinkIdx=0, nLinks=5):
        """
        Setup Links
        Dependency Graph: Accounts
                            |
                          Links
        """
        self.linkModelList             = self.dbp.linkP.populateN(aID=self.popAccountID,
                                                                  count=nLinks)
        # Populated/Non Populated
        self.popLinkIdx                = popLinkIdx
        self.popLinkSMID               = self.linkModelList[self.popLinkIdx].smID
        self.nonPopLinkIdx             = ((self.popLinkIdx + 1) %
                                          len(self.linkModelList))
        self.nonPopLinkSMID            = self.linkModelList[self.nonPopLinkIdx].smID
