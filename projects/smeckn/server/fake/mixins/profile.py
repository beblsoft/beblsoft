#!/usr/bin/env python3
"""
 NAME:
  profile.py

 DESCRIPTION
  Fake Profile Mixin Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- FAKE PROFILE MIXIN CLASS --------------------- #
class FakeProfileMixin():
    """
    Fake Profile Mixin
    """

    # ---------------------------- SETUP ------------------------------------ #
    @logFunc()
    def setUpProfiles(self, popProfileIdx=0, nProfiles=5):
        """
        Setup Profiles
        Dependency Graph: Accounts
                            |
                          Profile Groups
                            |
                          Profiles
        """
        self.profileModelList          = self.dbp.profileP.populateN(aID=self.popAccountID,
                                                                     pgID=self.popPGID,
                                                                     nProfiles=nProfiles)
        self.popProfileIdx             = popProfileIdx
        self.popProfileSMID            = self.profileModelList[self.popProfileIdx].smID
        self.nonPopProfileIdx          = ((self.popProfileIdx + 1) %
                                          len(self.profileModelList))
        self.nonPopProfileSMID         = self.profileModelList[self.nonPopProfileIdx].smID
