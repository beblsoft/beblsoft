#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Facebook Profile Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from datetime import timedelta
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.ltAccessToken.dao import BFacebookLTAccessTokenDAO
from base.facebook.python.debugAccessToken.dao import BFacebookDebugAccessTokenDAO
from base.facebook.python.user.dao import BFacebookUserDAO
from base.facebook.python.bGraph import BeblsoftFacebookGraph
from smeckn.server.profile.common import CommonProfileManager
from smeckn.server.facebook.post.manager import PostManager
from smeckn.server.facebook.postMessage.manager import PostMessageManager
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profile.model import ProfileType
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- FACEBOOK PROFILE MANAGER ----------------------- #
class FacebookProfileManager(CommonProfileManager):
    """
    Facebook Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)

        # Graph
        self.bFBGraph    = BeblsoftFacebookGraph(appID=self.cfg.FACEBOOK_APP_ID,
                                                 appSecret=self.cfg.FACEBOOK_APP_SECRET,
                                                 version=self.cfg.FACEBOOK_API_VERSION,
                                                 timeoutS=self.cfg.FACEBOOK_API_TIMEOUTS)
        # Content
        self.post        = PostManager(mgr=self.mgr)
        self.postMessage = PostMessageManager(mgr=self.mgr)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def profileType(self):
        return ProfileType.FACEBOOK

    @property
    def contentManagerList(self):
        return [self.post, self.postMessage]

    # ---------------------- CREATE ----------------------------------------- #
    @logFunc()
    def create(self, aDBS, pgID, fbpID, shortTermAccessToken):
        """
        Create object
        Args
          pgID:
            Profile Group ID
          fbpID:
            Facebook profile ID
          shortTermAccessToken:
            Facebook Profile Access token retrieved on the client
          verify:
            If True, verify the generated access token
            Tests set this value to false, as VCR'd access tokens will eventually time out

        Returns
          FacebookProfileModel
        """
        # Do the following:
        # - Verify pgID exists
        # - Retrieve and verify long term access token
        # - Retrieve facebook user
        # - Create and return fbpModel
        _                     = ProfileGroupDAO.getByID(aDBS=aDBS, pgID=pgID)
        bFBLTAccessTokenModel = BFacebookLTAccessTokenDAO.get(bFBGraph=self.bFBGraph,
                                                              shortTermAccessToken=shortTermAccessToken)
        self.verifyAccessToken(fbpID=fbpID, accessToken=bFBLTAccessTokenModel.token)
        bFBUserModel          = BFacebookUserDAO.getByID(bFBGraph=self.bFBGraph, userID=fbpID,
                                                         accessToken=bFBLTAccessTokenModel.token)
        fbpModel = None
        with AccountDatabase.lockTables(session=aDBS, lockStr="Profile WRITE, FacebookProfile WRITE"):
            fbpModel = FacebookProfileDAO.createFromBFBUserModel(aDBS=aDBS, pgID=pgID,
                                                                 bFBUserModel=bFBUserModel,
                                                                 longTermAccessToken=bFBLTAccessTokenModel.token)
        aDBS.refresh(fbpModel)
        return fbpModel

    # ---------------------- UPDATE ACCESS TOKEN ---------------------------- #
    @logFunc()
    def updateAccessToken(self, aDBS, fbpModel, shortTermAccessToken):
        """
        Update access token
        Args
          fbpModel:
            Facebook profile model
          shortTermAccessToken:
            Facebook Profile Access token retrieved on the client

        Returns
          FacebookProfileModel
        """
        # Do the following:
        # - Retrieve and verify long term access token
        # - Update fbpModel in database
        # - Update sync if last one failed due to credential errors
        bFBLTAccessTokenModel = BFacebookLTAccessTokenDAO.get(bFBGraph=self.bFBGraph,
                                                              shortTermAccessToken=shortTermAccessToken)
        self.verifyAccessToken(fbpID=fbpModel.fbID, accessToken=bFBLTAccessTokenModel.token)

        with AccountDatabase.transactionScope(session=aDBS):
            FacebookProfileDAO.update(aDBS=aDBS, fbpModel=fbpModel,
                                      longTermAccessToken=bFBLTAccessTokenModel.token)
        with AccountDatabase.transactionScope(session=aDBS):
            SyncDAO.handleProfileCredentialUpdate(aDBS=aDBS, profileSMID=fbpModel.smID)

        aDBS.refresh(fbpModel)
        return fbpModel

    # ---------------------- VERIFY ACCESS TOKEN ---------------------------- #
    @logFunc()
    def verifyAccessToken(self, fbpID, accessToken, remainingDelta=timedelta(days=1)):
        """
        Verify access token
        """
        bFBDebugAccessToken = BFacebookDebugAccessTokenDAO.get(bFBGraph=self.bFBGraph, inputToken=accessToken)
        bFBDebugAccessToken.verifyValid()
        bFBDebugAccessToken.verifyUserID(userID=fbpID)
        bFBDebugAccessToken.verifyTimeRemaining(remainingDelta=remainingDelta)
        bFBDebugAccessToken.verifyPermListGranted(permStrList=self.cfg.FACEBOOK_REQUIRED_PERMISSION_LIST)

    # ---------------------- SYNC ------------------------------------------- #
    @property
    def syncRestartDelta(self):
        return self.cfg.FACEBOOK_SYNC_RESTART_TIMEDELTA

    @logFunc()
    def isSyncStartable(self, syncCtx):
        assert syncCtx.profileModel

        startable     = True
        status        = SyncStatus.IN_PROGRESS
        fbpModel      = FacebookProfileDAO.getBySMID(aDBS=syncCtx.aDBS, smID=syncCtx.profileSMID)
        try:
            self.verifyAccessToken(fbpID=fbpModel.fbID, accessToken=fbpModel.longTermAccessToken)
        except BeblsoftError as _:
            startable = False
            status    = SyncStatus.CREDENTIALS_EXPIRED
        return (startable, status)

    @logFunc()
    def sync(self, syncCtx):  # pylint: disable=W0221
        """
        Sync profile

        Returns:
          (success, syncStatus)
        """
        status   = SyncStatus.IN_PROGRESS
        fbpModel = FacebookProfileDAO.getBySMID(aDBS=syncCtx.aDBS, smID=syncCtx.profileSMID, raiseOnNone=False)
        status   = status if fbpModel else SyncStatus.PROFILE_DELETED

        # Do the following:
        #  - Get user model from facebook
        #  - Sync model to underlying database
        if status.isInProgress:
            try:
                bFBUserModel = BFacebookUserDAO.getByID(bFBGraph=self.mgr.facebook.bFBGraph, userID=fbpModel.fbID,
                                                        accessToken=fbpModel.longTermAccessToken)
            except BeblsoftError as e:
                if e.code == BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID:
                    status = SyncStatus.CREDENTIALS_EXPIRED
                else:
                    self.mgr.sentry.captureError(error=e, bLogCode=BeblsoftLogCode.FACEBOOK_USER_RECORD_SYNC_FAILED)
                    status = SyncStatus.THIRDPARTY_API_FAILURE
        if status.isInProgress:
            try:
                with AccountDatabase.transactionScope(session=syncCtx.aDBS):
                    fbpModel = FacebookProfileDAO.updateFromBFBUserModel(aDBS=syncCtx.aDBS, fbpModel=fbpModel,
                                                                         bFBUserModel=bFBUserModel)
            except sqlalchemy.orm.exc.StaleDataError as _:
                status = SyncStatus.PROFILE_DELETED

        return (status == SyncStatus.IN_PROGRESS, status)
