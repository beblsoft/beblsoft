#!/usr/bin/env python3
"""
 NAME:
  sync.py

 DESCRIPTION
  Post Sync Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
import sqlalchemy
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.user.dao import BFacebookUserDAO
from smeckn.server.profile.sync.common import CommonContentSyncManager
from smeckn.server.profile.sync.models import SyncContentProgressModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.aDB.syncContent.dao import SyncContentDAO
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST SYNC MANAGER ------------------------------ #
class PostSyncManager(CommonContentSyncManager):
    """
    Post Sync Manager Class
    """

    # ---------------------- IS LIMIT REACHED ------------------------------- #
    @logFunc()
    def isLimitReached(self, syncCtx):
        """
        Return True if limit has been reached
        """
        assert syncCtx.profileSMID

        count = FacebookPostDAO.getAll(aDBS=syncCtx.aDBS, fbpSMID=syncCtx.profileSMID, count=True)
        return count > self.cfg.FACEBOOK_MAX_POSTS_PER_PROFILE

    # ---------------------- DELETE STALE ----------------------------------- #
    @logFunc()
    def deleteStale(self, syncCtx):
        """
        Delete stale posts
        """
        assert syncCtx.profileSMID
        assert syncCtx.syncModel

        with AccountDatabase.transactionScope(session=syncCtx.aDBS):
            FacebookPostDAO.deleteStale(aDBS=syncCtx.aDBS, fbpSMID=syncCtx.profileSMID,
                                        syncStartDate=syncCtx.syncModel.createDate)

    # ---------------------- GET PROGRESS ----------------------------------- #
    @logFunc()
    def getProgress(self, syncCtx):
        """
        Return sync progress
        """
        assert syncCtx.profileSMID
        assert syncCtx.syncContentModel

        count = FacebookPostDAO.getAll(aDBS=syncCtx.aDBS, fbpSMID=syncCtx.profileSMID,
                                       syncedAfterDate=syncCtx.syncModel.createDate, count=True)
        return SyncContentProgressModel(contentType=self.contentType,
                                        count=count, status=syncCtx.syncContentModel.status)

    # ---------------------- SYNC CHUNK ------------------------------------- #
    @logFunc()
    def syncChunk(self, syncCtx, nextDict):  # pylint: disable=W0221
        """
        Sync a chunk of content

        Returns:
          (SyncStatus, nextDict)
        """
        status   = SyncStatus.IN_PROGRESS
        fbpModel = FacebookProfileDAO.getBySMID(aDBS=syncCtx.aDBS, smID=syncCtx.profileSMID,
                                                raiseOnNone=False)
        status   = status if fbpModel else SyncStatus.PROFILE_DELETED

        # Do the following:
        #  - Get the next chunk of posts from facebook
        #  - Catch any facebook failures
        #  - Sync the posts to the underlying database
        #  - Return the link to sync the next chunk (if it exists)
        if status.isInProgress:
            (status, nextDict, bFBPostModelList) = self.getNextChunk(
                syncCtx=syncCtx, fbpModel=fbpModel, nextDict=nextDict)
        if status.isInProgress:
            status = self.syncChunkToDB(syncCtx=syncCtx, bFBPostModelList=bFBPostModelList)
        if status.isInProgress and not nextDict:
            status = SyncStatus.SUCCESS

        return (status, nextDict)

    # ---------------------- GET NEXT CHUNK --------------------------------- #
    @logFunc()
    def getNextChunk(self, syncCtx, fbpModel, nextDict):  # pylint: disable=W0613
        """
        Get next chunk

        Returns
          (status, nextDict, bFBPostModelList)
          status           - status of sync
          nextDict         - dictionary to encode access to next chunk
          bFBPostModelList - List of beblsoft facebook post objects
        """
        bFBPostModelList = []
        status           = SyncStatus.IN_PROGRESS
        nextDict         = nextDict if nextDict else {}
        nextLink         = nextDict.get("nextLink", None)
        pageLimit        = self.cfg.FACEBOOK_POST_SYNC_CHUNK_SIZE

        # Get chunk of facebook posts
        # Filter out any empty posts
        try:
            (bFBPostModelList, nextLink) = BFacebookUserDAO.getPostList(
                bFBGraph                     = self.mgr.facebook.bFBGraph,
                userID                       = fbpModel.fbID,
                accessToken                  = fbpModel.longTermAccessToken,
                nextLink                     = nextLink,
                pageLimit                    = pageLimit)
            bFBPostModelList = list(filter(lambda bFBPostModel: not bFBPostModel.isEmpty, bFBPostModelList))
        except BeblsoftError as e:
            if e.code == BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID:
                status = SyncStatus.CREDENTIALS_EXPIRED
            else:
                self.mgr.sentry.captureError(error=e, bLogCode=BeblsoftLogCode.FACEBOOK_POST_SYNC_FAILED,
                                             isBError=True)
                status = SyncStatus.THIRDPARTY_API_FAILURE
            nextDict = {}
        else:
            nextDict = {"nextLink": nextLink} if nextLink else {}

        return (status, nextDict, bFBPostModelList)

    # ---------------------- SYNC CHUNK TO DB ------------------------------- #
    @logFunc()
    def syncChunkToDB(self, syncCtx, bFBPostModelList):  # pylint: disable=R0201
        """
        Sync bFBPostModelList to database
        """
        assert syncCtx.profileSMID

        bFBPostIDDict     = {}
        addList           = []  # [BFacebookPostModel]
        updateList        = []  # [(FacebookPostModel, BFacebookPostModel)]
        bFBPostIDDict     = {bFBPostModel.id: bFBPostModel for bFBPostModel in bFBPostModelList}
        fbIDList          = bFBPostIDDict.keys()
        fbPostModelList   = FacebookPostDAO.getAll(aDBS=syncCtx.aDBS, fbpSMID=syncCtx.profileSMID, fbIDList=fbIDList)
        fbPostModelIDDict = {fbPostModel.fbID: fbPostModel for fbPostModel in fbPostModelList}

        # Posts fall into two categories
        # 1. Already in database
        #    updateList - update database contents
        #
        # 2. Not in database
        #    addList    - add to database
        for fbPostID, bFBPostModel in bFBPostIDDict.items():
            fbPostModel = fbPostModelIDDict.get(fbPostID, None)
            if fbPostModel:
                updateList.append((fbPostModel, bFBPostModel))
            else:
                addList.append(bFBPostModel)

        # Do the following:
        # - Lock SyncContent row, this prevents syncing after timeout
        # - Ensure SyncContent is still in progress
        # - Add content
        # - Update content
        # - Catch any errors and update status appropriately
        try:
            with AccountDatabase.transactionScope(session=syncCtx.aDBS):
                SyncContentDAO.lockSMID(aDBS=syncCtx.aDBS, smID=syncCtx.syncContentSMID, update=False)
                syncCtx.aDBS.refresh(syncCtx.syncContentModel)
                status = syncCtx.syncContentModel.status
                if status.isInProgress:
                    # Add
                    with AccountDatabase.transactionScope(session=syncCtx.aDBS, nested=True):
                        for bFBPostModel in addList:
                            FacebookPostDAO.createFromBFBPostModel(aDBS=syncCtx.aDBS,
                                                                   fbpSMID=syncCtx.profileSMID,
                                                                   bFBPostModel=bFBPostModel)
                    # Update
                    with AccountDatabase.transactionScope(session=syncCtx.aDBS, nested=True):
                        for (fbPostModel, bFBPostModel) in updateList:
                            FacebookPostDAO.updateFromBFBPostModel(aDBS=syncCtx.aDBS,
                                                                   fbPostModel=fbPostModel,
                                                                   bFBPostModel=bFBPostModel)

        except sqlalchemy.exc.IntegrityError as e:
            profileDeleted = "foreign key constraint fails" in e.orig and "Profile" in e.orig
            if profileDeleted:
                status = SyncStatus.PROFILE_DELETED
            else:
                raise e

        return status
