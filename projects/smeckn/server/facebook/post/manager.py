#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Facebook Post Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.profile.content.common import CommonContentManager
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.facebook.post.sync import PostSyncManager
from smeckn.server.aDB.facebookPost.statistic import FacebookPostStatisticDAO
from smeckn.server.aDB.facebookPost.histogram import FacebookPostHistogramDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST MANAGER ----------------------------------- #
class PostManager(CommonContentManager):
    """
    Post Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.sync  = PostSyncManager(mgr=self.mgr, contentMgr=self)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def contentType(self):
        return ContentType.POST

    @property
    def syncManager(self):
        return self.sync

    @property
    def statisticDAO(self):
        return FacebookPostStatisticDAO

    @property
    def histogramDAO(self):
        return FacebookPostHistogramDAO
