#!/usr/bin/env python3
"""
 NAME:
  post_test.py

 DESCRIPTION
  Facebook Post Sync Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.sync.model import SyncStatus


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class PostSyncManagerTestCase(CommonTestCase):
    """
    Test Facebook Post Sync Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookTestUsers=True, facebookTestUserData=True,
                      facebookTestUserProfiles=True)
        fbTestUserName         = "small"
        self.contentType       = ContentType.POST
        self.fbTestUser        = self.fbTestUserDict[fbTestUserName]
        self.fbTestUserData    = self.fbTestUserDataDict[fbTestUserName]
        self.bFBPostMediaModel = self.fbTestUserData.bFBStatusPostMediaModel
        self.bFBPostModel      = self.bFBPostMediaModel.bFBPostModel
        self.syncManager       = self.mgr.facebook.getSyncManager(contentType=self.contentType)
        self.syncModel         = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popFBPSMID,
                                                            status = SyncStatus.IN_PROGRESS)
        self.syncContentModel  = self.dbp.syncContentP.populateOne(aID=self.popAccountID, syncSMID=self.syncModel.smID,
                                                                   contentType=self.contentType,
                                                                   status = SyncStatus.IN_PROGRESS)

    @logFunc()
    def test_isLimitReached(self):
        """
        Test hitting the post limit
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.dict(self.cfg.__dict__, FACEBOOK_MAX_POSTS_PER_PROFILE=0) as _:

            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.smID)

            # Create single post
            with AccountDatabase.transactionScope(session=aDBS):
                _ = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                           bFBPostModel=self.bFBPostModel)
            isLimitReached = self.syncManager.isLimitReached(syncCtx=syncCtx)
            self.assertTrue(isLimitReached)

    @logFunc()
    def test_deleteStale(self):
        """
        Test deleting stale posts
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.smID)

            # Create
            fbPostModel = None
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=self.bFBPostModel)

            aDBS.refresh(fbPostModel)
            fbPostSMID = fbPostModel.smID

            # Try deleting stale (with old date, nothing should be deleted)
            with AccountDatabase.transactionScope(session=aDBS):
                self.syncManager.deleteStale(syncCtx=syncCtx)
            fbPostModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=fbPostSMID)

            # Update sync date so now it will be deleted
            staleDate = datetime.utcnow() - timedelta(seconds=10)
            with AccountDatabase.transactionScope(session=aDBS):
                FacebookPostDAO.update(aDBS=aDBS, fbPostModel=fbPostModel, syncDate=staleDate)
            with AccountDatabase.transactionScope(session=aDBS):
                self.syncManager.deleteStale(syncCtx=syncCtx)
            fbPostModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=fbPostSMID, raiseOnNone=False)
            self.assertIsNone(fbPostModel)

    @logFunc()
    def test_getProgress(self):
        """
        Test getting progress
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.smID)

            # Create single post
            with AccountDatabase.transactionScope(session=aDBS):
                fbPostModel = FacebookPostDAO.createFromBFBPostModel(aDBS=aDBS, fbpSMID=self.popFBPSMID,
                                                                     bFBPostModel=self.bFBPostModel)
            # Verify content progress
            syncContentProgressModel = self.syncManager.getProgress(syncCtx=syncCtx)
            self.assertEqual(syncContentProgressModel.contentType, self.contentType)
            self.assertEqual(syncContentProgressModel.count, 1)
            self.assertEqual(syncContentProgressModel.status, SyncStatus.IN_PROGRESS)

            # Set post sync date back
            with AccountDatabase.transactionScope(session=aDBS):
                aDBS.refresh(fbPostModel)
                _ = FacebookPostDAO.update(aDBS=aDBS, fbPostModel=fbPostModel,
                                           syncDate=datetime.utcnow() - timedelta(seconds=100))

            # Verify not in content progress
            syncContentProgressModel = self.syncManager.getProgress(syncCtx=syncCtx)
            self.assertEqual(syncContentProgressModel.count, 0)

    @logFunc()
    def test_syncChunk(self):
        """
        Test syncing chunk
        """
        nextDict        = {}
        counter         = 0
        continueLooping = True
        minPosts        = 3

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS, \
                patch.dict(self.cfg.__dict__, FACEBOOK_POST_SYNC_CHUNK_SIZE=1) as _:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadSyncContent(syncContentSMID=self.syncContentModel.smID)

            while continueLooping:
                counter            += 1
                (status, nextDict)  = self.syncManager.syncChunk(syncCtx=syncCtx, nextDict=nextDict)
                continueLooping     = status == SyncStatus.IN_PROGRESS

            syncContentProgressModel = self.syncManager.getProgress(syncCtx=syncCtx)

            self.assertEqual(status, SyncStatus.SUCCESS)
            self.assertGreaterEqual(syncContentProgressModel.count, minPosts)
            self.assertGreaterEqual(counter, minPosts)
