#!/usr/bin/env python3
"""
 NAME:
  manager_test.py

 DESCRIPTION
  Facebook Profile Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.debugAccessToken.dao import BFacebookDebugAccessTokenDAO
from base.facebook.python.user.dao import BFacebookUserDAO
from base.facebook.python.testUser.dao import BFacebookTestUserDAO
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.sync.context import SyncContext


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CREATE TEST CASE ----------------------------- #
class FacebookProfileCreateTestCase(CommonTestCase):
    """
    Test Facebook Profile Creation
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookTestUsers=True)

    @logFunc()
    def test_valid(self):
        """
        Test valid test case
        """
        fbTestUserName        = "small"
        fbTestUserID          = self.fbTestUserDict[fbTestUserName].id
        fbTestUserAccessToken = self.fbTestUserDict[fbTestUserName].accessToken
        bFBUserModel          = BFacebookUserDAO.getByID(bFBGraph=self.mgr.facebook.bFBGraph,
                                                         userID=fbTestUserID,
                                                         accessToken=fbTestUserAccessToken)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbpModel = self.mgr.facebook.create(aDBS=aDBS, pgID=self.popPGID, fbpID=fbTestUserID,
                                                shortTermAccessToken=fbTestUserAccessToken)
            self.assertEqual(fbpModel.name, bFBUserModel.name)
            self.assertEqual(fbpModel.email, bFBUserModel.email)
            self.assertEqual(fbpModel.gender, bFBUserModel.gender)
            self.assertEqual(fbpModel.birthdayDate, bFBUserModel.birthday)
            self.assertEqual(fbpModel.hometown, bFBUserModel.hometown)

            bFBDebugAccessToken = BFacebookDebugAccessTokenDAO.get(bFBGraph=self.mgr.facebook.bFBGraph,
                                                                   inputToken=fbpModel.longTermAccessToken)
            bFBDebugAccessToken.verifyValid()

    @logFunc()
    def test_badAccessToken(self):
        """
        Test bad access token
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                _ = self.mgr.facebook.create(aDBS=aDBS, pgID=self.popPGID, fbpID=239487234918237,
                                             shortTermAccessToken="InvalidAccessToken")
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID)


# ---------------------------- UPDATE TOKEN TEST CASE ----------------------- #
class FacebookProfileUpdateTokenTestCase(CommonTestCase):
    """
    Test Facebook Profile Token Update
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookTestUsers=True, facebookTestUserProfiles=True)

    @logFunc()
    def test_valid(self):
        """
        Test valid test case

        Profile access token and sync restart date should both be updated
        """
        createDate  = datetime.utcnow()
        restartDate = createDate + timedelta(hours=24)
        syncModel   = None

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            fbpModel       = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=self.popFBPSMID)

            with AccountDatabase.lockTables(session=aDBS, lockStr="Profile READ, Sync WRITE, Analysis READ"):
                syncModel = SyncDAO.create(aDBS=aDBS, profileSMID=fbpModel.smID, status=SyncStatus.CREDENTIALS_EXPIRED,
                                           createDate=createDate, restartDate=restartDate)
                self.assertEqual(syncModel.restartDate, restartDate)

            with AccountDatabase.transactionScope(session=aDBS):
                accessToken    = BFacebookTestUserDAO.getAccessToken(
                    bFBGraph                            = self.mgr.facebook.bFBGraph,
                    appID                               = self.mgr.facebook.bFBGraph.appID,
                    testUserID                          = fbpModel.fbID,
                    accessToken                         = self.mgr.facebook.bFBGraph.appAccessToken)
                oldAccessToken = fbpModel.longTermAccessToken
                fbpModel       = self.mgr.facebook.updateAccessToken(aDBS=aDBS, fbpModel=fbpModel,
                                                                     shortTermAccessToken=accessToken)
                self.assertNotEqual(fbpModel.longTermAccessToken, oldAccessToken)

            syncModel = SyncDAO.getBySMID(aDBS=aDBS, smID=syncModel.smID)
            self.assertNotEqual(syncModel.restartDate, restartDate)

    @logFunc()
    def test_badAccessToken(self):
        """
        Test bad access token
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            try:
                fbpModel       = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=self.popFBPSMID)
                _ = self.mgr.facebook.updateAccessToken(aDBS=aDBS, fbpModel=fbpModel,
                                                        shortTermAccessToken="InvalidAccessToken")
            except BeblsoftError as e:
                self.assertEqual(e.code, BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID)


# ---------------------------- SYNC TEST CASE ------------------------------- #
class FacebookProfileSyncTestCase(CommonTestCase):
    """
    Test Facebook Profile Sync
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookTestUsers=True,
                      facebookTestUserProfiles=True)

    @logFunc()
    def test_isSyncStartable(self):
        """
        Test isSyncStarable
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx  = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadProfile(profileSMID=self.popFBPSMID)

            # Good token should be startable
            (startable, status) = self.mgr.facebook.isSyncStartable(syncCtx=syncCtx)
            self.assertTrue(startable)

            # Update with bad token
            with AccountDatabase.transactionScope(session=aDBS):
                fbpModel = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=self.popFBPSMID)
                FacebookProfileDAO.update(aDBS=aDBS, fbpModel=fbpModel, longTermAccessToken="badToken")

            # Should no longer be startable
            (startable, status) = self.mgr.facebook.isSyncStartable(syncCtx=syncCtx)
            self.assertFalse(startable)
            self.assertEqual(status, SyncStatus.CREDENTIALS_EXPIRED)

    @logFunc()
    def test_sync(self):
        """
        Test sync
        """
        oldSyncDate = datetime.utcnow() - timedelta(days=1)

        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            syncCtx = SyncContext(mgr=self.mgr, aDBS=aDBS)
            syncCtx.loadProfile(profileSMID=self.popFBPSMID)

            # Set syncDate to be old
            with AccountDatabase.transactionScope(session=aDBS):
                fbpModel = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=self.popFBPSMID)
                FacebookProfileDAO.update(aDBS=aDBS, fbpModel=fbpModel, syncDate=oldSyncDate)

            (success, _) = self.mgr.facebook.sync(syncCtx=syncCtx)
            self.assertTrue(success)

            # Verify syncDate updated
            with AccountDatabase.transactionScope(session=aDBS):
                fbpModel = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=self.popFBPSMID)
                self.assertGreater(fbpModel.syncDate, oldSyncDate)
