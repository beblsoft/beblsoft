#!/usr/bin/env python3
"""
 NAME:
  compSentAnalysis_test.py

 DESCRIPTION
  Facebook Post Message Comprehend Sentiment Analysis Manager Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime, timedelta
from freezegun import freeze_time
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.text.populater import TextPopulater


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class PostMessageCompSentAnalysisManagerTestCase(CommonTestCase):
    """
    Test Facebook Post Message Comp Sent Analysis Manager
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True, facebookPostsFull=True)
        self.profileManager  = self.mgr.facebook
        self.contentType     = ContentType.POST_MESSAGE
        self.analysisType    = AnalysisType.COMPREHEND_SENTIMENT
        self.status          = AnalysisStatus.IN_PROGRESS
        self.contentCount    = 0
        self.analysisModel   = self.dbp.analysisP.populateOne(aID=self.popAccountID, profileSMID=self.popFBPSMID,
                                                              contentType=self.contentType,
                                                              analysisType=self.analysisType,
                                                              status=self.status,
                                                              contentCount=self.contentCount)

    @logFunc()
    def test_getCountModel(self):
        """
        Test getCountModel
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx            = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analysisManager        = analysisCtx.analysisManager
            analysisCountModel     = analysisManager.getCountModel(analysisCtx=analysisCtx)
            fbPostModelList        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            fbPostModelListCountAD = FacebookPostDAO.getModelListCountAD(fbPostModelList=fbPostModelList)
            self.assertEqual(analysisCountModel.analysisType, self.analysisType)
            self.assertEqual(analysisCountModel.countDone, fbPostModelListCountAD.nMsgCompSentDone)
            self.assertEqual(analysisCountModel.countCant, fbPostModelListCountAD.nMsgCompSentCant)
            self.assertEqual(analysisCountModel.countNotDone, fbPostModelListCountAD.nMsgCompSentNotDone)

    @logFunc()
    def test_getProgress(self):
        """
        Test getProgress
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx            = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analysisModel          = analysisCtx.analysisModel

            # Turn clock back on analysis, verify analyze count
            backDate               = datetime.utcnow() - timedelta(minutes=1)
            with AccountDatabase.transactionScope(session=aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisModel, createDate=backDate)

            fbPostModelList        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            fbPostModelListCountAD = FacebookPostDAO.getModelListCountAD(fbPostModelList=fbPostModelList,
                                                                         compSentAfterDate=backDate)
            analysisProgressModel  = analysisCtx.analysisManager.getProgress(analysisCtx=analysisCtx)
            self.assertEqual(analysisProgressModel.contentType, self.contentType)
            self.assertEqual(analysisProgressModel.analysisType, self.analysisType)
            self.assertEqual(analysisProgressModel.countAnalyzed, fbPostModelListCountAD.nMsgCompSentAfterDate)
            self.assertEqual(analysisProgressModel.countTotal, self.contentCount)
            self.assertEqual(analysisProgressModel.status, self.status)

            # Turn clock forward on analysis, verify nothing analyzed
            forwardDate            = datetime.utcnow() + timedelta(minutes=1)
            with AccountDatabase.transactionScope(session=aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisModel, createDate=forwardDate)
            analysisProgressModel  = analysisCtx.analysisManager.getProgress(analysisCtx=analysisCtx)
            self.assertEqual(analysisProgressModel.countAnalyzed, 0)

    @logFunc()
    def test_getAnalyzeOneKwargList(self):
        """
        Test getAnalyzeOneKwargList
        """
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx            = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            fbPostModelList        = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID)
            fbPostModelListCountAD = FacebookPostDAO.getModelListCountAD(fbPostModelList=fbPostModelList)
            analyzeOneKwargList    = analysisCtx.analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)
            self.assertEqual(len(analyzeOneKwargList), fbPostModelListCountAD.nMsgCompSentNotDone)


    @logFunc()
    @freeze_time(datetime.utcnow(), tick=True) # Overwrite common as Comprehend likes current time when recording
    def test_analyzeOne(self):
        """
        Test analyzeOne

        VCR issue:
          Both Comp Lang and Sent POST requests to  https://comprehend.us-east-1.amazonaws.com/>
          VCR cannot distinguish the difference and sometimes returns the wrong cassette results
          This results in spurious diffs

        VCR solution:
          The solution is to create 30 posts where all the comp language analysis
          has already been completed
        """
        super().setUp(unitLedger=True)

        nFacebookPosts = 30
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            analysisCtx             = AnalysisContext(mgr=self.mgr, aDBS=aDBS)
            analysisCtx.loadAnalysis(analysisSMID=self.analysisModel.smID)
            analysisManager         = analysisCtx.analysisManager
            analysisModel           = analysisCtx.analysisModel

            # Delete post full, setup posts with all language analyzed
            with AccountDatabase.transactionScope(session=aDBS):
                FacebookPostDAO.deleteAll(aDBS=aDBS)
                for idx in range(nFacebookPosts):
                    fbPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID, fbID=idx)
                    textModel   = TextDAO.createFromText(aDBS=aDBS, text=TextPopulater.getRandomText(),
                                                         fbPostModel=fbPostModel)
                    TextPopulater.populateTextCompLangSuccessSupported(aDBS=aDBS, textModel=textModel)

            # Load list to analyze and check counts
            analyzeOneKwargList     = analysisManager.getAnalyzeOneKwargList(analysisCtx=analysisCtx)
            analyzeOneCount         = len(analyzeOneKwargList)
            analysisCountModel      = analysisManager.getCountModel(analysisCtx=analysisCtx)
            self.assertEqual(analysisCountModel.countDone, 0)
            self.assertEqual(analysisCountModel.countCant, 0)
            self.assertEqual(analysisCountModel.countNotDone, nFacebookPosts)
            self.assertEqual(analysisCountModel.countNotDone, analyzeOneCount)

            # Create hold and update analysis
            with AccountDatabase.lockTables(session=analysisCtx.aDBS, lockStr="UnitLedger WRITE"):
                unitLedgerHoldModel = UnitLedgerDAO.createHold(aDBS=analysisCtx.aDBS,
                                                               unitType=analysisManager.unitType,
                                                               ledgerCount=analyzeOneCount)
            with AccountDatabase.transactionScope(session=analysisCtx.aDBS):
                AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisModel, createDate=datetime.utcnow(),
                                   unitLedgerHoldModel=unitLedgerHoldModel)

            # Do analysis
            for analyzeOneKwarg in analyzeOneKwargList:
                analysisManager.analyzeOne(analysisCtx=analysisCtx, **analyzeOneKwarg)

            # Verify that all are done
            analysisCountModel      = analysisManager.getCountModel(analysisCtx=analysisCtx)
            self.assertEqual(analysisCountModel.countDone, nFacebookPosts)
            self.assertEqual(analysisCountModel.countCant, 0)
            self.assertEqual(analysisCountModel.countNotDone, 0)
