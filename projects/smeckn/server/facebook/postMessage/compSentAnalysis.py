#!/usr/bin/env python3
"""
 NAME:
  compSentAnalysis.py

 DESCRIPTION
  Post Message Comprehend Sentiment Analysis Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.profile.analysis.common import CommonContentAnalysisManager
from smeckn.server.profile.analysis.models import AnalysisProgressModel
from smeckn.server.profile.analysis.models import AnalysisCountModel
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.analysis.model import AnalysisType


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST MESSAGE COMP SENT ANALYSIS MANAGER -------- #
class PostMessageCompSentAnalysisManager(CommonContentAnalysisManager):
    """
    Post Message Comp Sent Analysis Manager Class
    """

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def analysisType(self):
        return AnalysisType.COMPREHEND_SENTIMENT

    @property
    def unitType(self):
        return UnitType.TEXT_ANALYSIS

    @property
    def nAnalysesPerWorker(self):
        return self.cfg.FACEBOOK_POSTMSG_COMPSENTA_NPERWORKER

    # ---------------------- GET COUNT MODEL -------------------------------- #
    @logFunc()
    def getCountModel(self, analysisCtx):
        """
        Return AnalysisCountModel
        """
        assert analysisCtx.profileSMID
        aDBS         = analysisCtx.aDBS
        profileSMID  = analysisCtx.profileSMID
        countDone    = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=profileSMID, joinMessage=True,
                                              containsMessage=True, compSentDone=True, count=True)
        countNotDone = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=profileSMID, joinMessage=True,
                                              containsMessage=True, compSentNotDone=True, count=True)
        countCant    = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=profileSMID, joinMessage=True,
                                              containsMessage=True, compSentCant=True, count=True)
        return AnalysisCountModel(profileSMID=analysisCtx.profileSMID, contentType=self.contentType,
                                  analysisType=self.analysisType, countDone=countDone,
                                  countCant=countCant, countNotDone=countNotDone)

    # ---------------------- GET PROGRESS ----------------------------------- #
    @logFunc()
    def getProgress(self, analysisCtx):
        """
        Return AnalysisProgressModel
        """
        assert analysisCtx.analysisModel
        aDBS               = analysisCtx.aDBS
        profileSMID        = analysisCtx.profileSMID
        analysisModel      = analysisCtx.analysisModel
        countAnalyzedAfter = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=profileSMID, joinMessage=True,
                                                    containsMessage=True,
                                                    compSentAfterDate=analysisModel.createDate,
                                                    count=True)
        countTotal         = analysisModel.contentCount
        return AnalysisProgressModel(contentType=self.contentType,
                                     analysisType=self.analysisType,
                                     countAnalyzed=countAnalyzedAfter, countTotal=countTotal,
                                     status=analysisModel.status)

    # ---------------------- GET ANALYZE ONE KWARG LIST LIST ---------------- #
    @logFunc()
    def getAnalyzeOneKwargList(self, analysisCtx):
        """
        Returns
          List of analyzeOne kwargs
          Ex. [{"fbPostSMID": 98123}, {"fbPostSMID": 120398}, ... ]
        """
        assert analysisCtx.profileSMID
        aDBS        = analysisCtx.aDBS
        profileSMID = analysisCtx.profileSMID

        # Retrieve all posts that haven't been analyzed
        fbPostModelList = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=profileSMID, joinMessage=True,
                                                 containsMessage=True, compSentNotDone=True)

        fbPostSMIDList      = [fbPostModel.smID for fbPostModel in fbPostModelList]
        analyzeOneKwargList = [{"fbPostSMID": smID} for smID in fbPostSMIDList]
        return analyzeOneKwargList

    # ---------------------- ANALYZE ONE ------------------------------------ #
    @logFunc()
    def analyzeOne(self, analysisCtx, fbPostSMID):  # pylint: disable=W0221
        """
        Analyze one piece of content
        """
        assert analysisCtx.analysisModel

        analysisModel      = analysisCtx.analysisModel
        unitLedgerHoldSMID = analysisModel.unitLedgerHoldSMID
        fbPostModel        = FacebookPostDAO.getBySMID(aDBS=analysisCtx.aDBS, smID=fbPostSMID)
        textModel          = fbPostModel.messageTextModel
        if textModel:
            self.mgr.analysis.text.compSent.analyze(aDBS=analysisCtx.aDBS,
                                                    textSMID=textModel.smID,
                                                    unitLedgerHoldSMID=unitLedgerHoldSMID)
