#!/usr/bin/env python3
"""
 NAME:
  manager.py

 DESCRIPTION
  Facebook Post Message Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import logging
from smeckn.server.profile.content.common import CommonContentManager
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.facebook.postMessage.compSentAnalysis import PostMessageCompSentAnalysisManager
from smeckn.server.aDB.facebookPost.statistic import FacebookPostStatisticDAO
from smeckn.server.aDB.facebookPost.histogram import FacebookPostHistogramDAO


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# -------------------------- POST MESSAGE MANAGER --------------------------- #
class PostMessageManager(CommonContentManager):
    """
    Post Message Manager Class
    """

    def __init__(self, **kwargs):
        """
        Initialize object
        """
        super().__init__(**kwargs)
        self.compSentAnalysis = PostMessageCompSentAnalysisManager(mgr=self.mgr, contentMgr=self)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def contentType(self):
        return ContentType.POST_MESSAGE

    @property
    def analysisManagerList(self):
        return [self.compSentAnalysis]

    @property
    def statisticDAO(self):
        return FacebookPostStatisticDAO

    @property
    def histogramDAO(self):
        return FacebookPostHistogramDAO
