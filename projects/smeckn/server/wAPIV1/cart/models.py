#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Cart Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.fields import BCurrencyCodeField
from smeckn.server.wAPIV1.unit.fields import UnitTypeField
from smeckn.server.wAPIV1.cart.fields import CountField, CostAmountField, CartJWTField
from smeckn.server.wAPIV1.cart import ns
from smeckn.server.aDB.unitLedger.model import UnitType


# ------------------------ MODELS ------------------------------------------- #
cartUnitCountModel = ns.model("CartUnitCountModel", {
    "unitType": UnitTypeField(),
    "count": CountField()
})

cartPostInputModel = ns.model("CartPostInputModel", {
    "cartUnitCountList": fields.List(fields.Nested(cartUnitCountModel)),
    "bCurrencyCode": BCurrencyCodeField(),
})

cartCostModel = ns.model("CartCostModel", {
    "unitType": UnitTypeField(),
    "count": CountField(),
    "costAmount": CostAmountField()
})

cartModel = ns.model("CartModel", {
    "cartCostModelList": fields.List(fields.Nested(cartCostModel)),
    "totalCost": CostAmountField(),
    "bCurrencyCode": BCurrencyCodeField()
})

cartPostOutputModel = ns.model("CartPostOutputModel", {
    "cartModel": fields.Nested(cartModel),
    "cartJWT": CartJWTField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyCartModel(apiModel, bCurrencyCode):
    """
    Verify cart model
    """
    totalCost = 0
    for _cartCostModel in apiModel.get("cartCostModelList"):
        totalCost += _cartCostModel.get("costAmount")
        _ = UnitType[_cartCostModel.get("unitType")]
        assert _cartCostModel.get("count") >= 0
    assert totalCost == apiModel.get("totalCost")
    assert bCurrencyCode.name == apiModel.get("bCurrencyCode")
