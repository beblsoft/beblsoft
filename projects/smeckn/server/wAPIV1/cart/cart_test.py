#!/usr/bin/env python3
"""
 NAME:
  cart_test.py

 DESCRIPTION
  Cart Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.cart.models import verifyCartModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- CART POST TEST CASE ----------------------------- #
class CartPostTestCase(CommonTestCase):
    """
    Cart Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL      = self.routes.cart
        self.bCurrencyCode = BeblsoftCurrencyCode.USD
        self.validPayload  = {
            "bCurrencyCode": self.bCurrencyCode.name,
            "cartUnitCountList": [
                {
                    "unitType": UnitType.TEXT_ANALYSIS.name,
                    "count": 500,
                },
                {
                    "unitType": UnitType.PHOTO_ANALYSIS.name,
                    "count": 500,
                },
            ]
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Test valid case
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload,
                                     authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        verifyCartModel(apiModel=data.get("cartModel"), bCurrencyCode=self.bCurrencyCode)
        self.assertGreater(len(data.get("cartJWT")), 0)

    @logFunc()
    def test_invalidNoBCurrency(self):
        """
        Test no BCurrency
        """
        payload = self.validPayload.copy()
        payload.pop("bCurrencyCode")
        (code, _) = self.postJSON(self.validURL, d=payload,
                                  authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_invalidBCurrency(self):
        """
        Test invalid BCurrency
        """
        payload = self.validPayload.copy()
        payload["bCurrencyCode"] = "FOO_BAR"
        (code, _) = self.postJSON(self.validURL, d=payload,
                                  authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noUnitCounts(self):
        """
        Test with no unit counts
        """
        payload = self.validPayload.copy()
        payload["cartUnitCountList"] = []
        (code, data) = self.postJSON(self.validURL, d=payload,
                                     authToken=self.popAuthToken, accountToken=self.popAccountToken)
        print(code)
        print(data)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), BeblsoftErrorCode.STRIPE_MUST_SPECIFY_CART_UNITS.value)

    @logFunc()
    def test_invalidCartUnitCountTooFew(self):
        """
        Test no too few in cart unit count
        """
        payload = self.validPayload.copy()
        payload["cartUnitCountList"] = [{
            "unitType": UnitType.PHOTO_ANALYSIS.name,
            "count": 1,
        }]
        (code, data) = self.postJSON(self.validURL, d=payload,
                                     authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), BeblsoftErrorCode.STRIPE_AMOUNT_UNDER_MIN_PURCHASE_COUNT.value)

    @logFunc()
    def test_badUnitType(self):
        """
        Test cart unit count with bad unit type
        """
        payload = self.validPayload.copy()
        payload["cartUnitCountList"] = [{
            "unitType": "FOO_BAR",
            "count": 1,
        }]
        (code, _) = self.postJSON(self.validURL, d=payload,
                                  authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
