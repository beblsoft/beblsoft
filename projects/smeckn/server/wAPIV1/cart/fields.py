#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Cart Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class CountField(fields.Integer):
    """
    Count Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count",
            description = "Count",
            example     = 500,
            required    = required,
            **kwargs)


class CostAmountField(fields.Integer):
    """
    Cost Amount Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Cost Amount",
            description = "Cost amount of particular entity",
            example     = 500,
            required    = required,
            **kwargs)


class CartJWTField(fields.String):
    """
    Cart JWT Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Cart JWT",
            description = "Token to be presented at charge time.",
            example     = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o",
            required    = required,
            **kwargs)
