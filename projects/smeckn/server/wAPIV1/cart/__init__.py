#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Cart Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('cart', description="Cart Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .cart import *  # pylint: disable=C0413,W0401
