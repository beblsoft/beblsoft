#!/usr/bin/env python3
"""
 NAME:
  cart.py

 DESCRIPTION
  cart Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.cart.models import cartPostOutputModel, cartPostInputModel
from smeckn.server.payments.unit.models import UnitCountModel
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.cart import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CART RESOURCE ------------------------------------ #
@ns.route("")
class CartResource(Resource):
    """
    Cart Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(cartPostInputModel)
    @ns.marshal_with(cartPostOutputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create cart
        """
        inData               = request.json
        cartUnitCountList    = inData.get("cartUnitCountList")
        mgr                  = current_app.mgr
        unitCountModelList   = []
        bCurrencyCode        = BeblsoftCurrencyCode[inData.get("bCurrencyCode")]
        for cartUnitCount in cartUnitCountList:
            unitType         = UnitType[cartUnitCount.get("unitType")]
            count            = cartUnitCount.get("count")
            unitCountModel   = UnitCountModel(unitType=unitType, count=count)
            unitCountModelList.append(unitCountModel)

        (cartModel, cartJWT) = mgr.payments.cart.createCart(bCurrencyCode=bCurrencyCode,
                                                            unitCountModelList=unitCountModelList)
        return {
            "cartModel": cartModel,
            "cartJWT": cartJWT
        }
