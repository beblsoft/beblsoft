#!/usr/bin/env python3
"""
 NAME:
  smID_test.py

 DESCRIPTION
  Test Sync SMID Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.sync.models import verifySyncModel

# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- SMID GET TEST CASE ------------------------------ #
class SMIDGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True, syncs=True)
        self.validURL = "{}/{}".format(self.routes.sync, self.popSyncSMID)

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifySyncModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url          = "{}/{}".format(self.routes.sync, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- SMID PUT TEST CASE ------------------------------ #
class SMIDPutTestCase(CommonTestCase):
    """
    Put Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True, syncs=True)
        self.validURL = "{}/{}".format(self.routes.sync, self.popSyncSMID)
        self.validPayload = {
            "errorSeen": True,
        }

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_validErrorSeen(self):
        """
        Test setting errorSeen
        """
        (code, data) = self.putJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("errorSeen"), True)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifySyncModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badErrorSeen(self):
        """
        Test with bad error seen
        """
        payload              = self.validPayload.copy()
        payload["errorSeen"] = "adsfkjh"
        (code, _)            = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                            accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noErrorSeen(self):
        """
        Test with no error seen, should be fine
        """
        payload = self.validPayload.copy()
        payload.pop("errorSeen")
        (code, data) = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifySyncModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url          = "{}/{}".format(self.routes.sync, 1293842398)
        (code, data) = self.putJSON(url, d=self.validPayload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.putJSON, d=self.validPayload)
