#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Sync Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields, inputs  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.sync.fields import SMIDField, SyncStatusField, CreateDateField, RestartDateField
from smeckn.server.wAPIV1.sync.fields import ErrorSeenField, ContentCountField
from smeckn.server.wAPIV1.common.fields import ContentTypeField
from smeckn.server.wAPIV1.account import ns
from smeckn.server.aDB.sync.dao import SyncDAO


# ------------------------ PARSERS ------------------------------------------ #
syncGetParser = ns.parser()
syncGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
syncGetParser.add_argument(
    "latest",
    type         = inputs.boolean,
    required     = False,
    default      = True,
    location     = "args")


# ------------------------ MODELS ------------------------------------------- #
syncModel = ns.model("SyncModel", {
    "smID": SMIDField(),
    "profileSMID": ProfileSMIDField(),
    "createDate": CreateDateField(),
    "restartDate": RestartDateField(),
    "status": SyncStatusField(),
    "errorSeen": ErrorSeenField()
})

syncPostInputModel = ns.model("SyncPostInputModel", {
    "profileSMID": ProfileSMIDField(),
})

syncPutInputModel = ns.model("SyncPutInputModel", {
    "errorSeen": ErrorSeenField(required=False),
})

syncContentProgressModel = ns.model("SyncContentProgressModel", {
    "contentType": ContentTypeField(),
    "count": ContentCountField(),
    "status": SyncStatusField()
})


syncProgressModel = ns.model("SyncProgressModel", {
    "contentProgressList": fields.List(fields.Nested(syncContentProgressModel)),
    "status": SyncStatusField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifySyncModel(aDBS, apiModel, smID):
    """
    Verify sync model
    """
    syncDBModel = SyncDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == syncDBModel.smID
    assert apiModel.get("profileSMID") == syncDBModel.profileSMID
    assert apiModel.get("createDate") == syncDBModel.createDate.isoformat()
    if syncDBModel.restartDate:
        assert apiModel.get("restartDate") == syncDBModel.restartDate.isoformat()
    else:
        assert apiModel.get("restartDate") == syncDBModel.restartDate
    assert apiModel.get("status") == syncDBModel.status.name
    assert apiModel.get("errorSeen") == syncDBModel.errorSeen
