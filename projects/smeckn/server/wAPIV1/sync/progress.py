#!/usr/bin/env python3
"""
 NAME:
  progress.py

 DESCRIPTION
  Sync Progress Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.sync.models import syncProgressModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.sync import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ PROGRESS RESOURCE -------------------------------- #
@ns.route("/<int:smID>/progress")
@ns.doc(params={"smID": "Sync Smeckn ID"})
class ProgressResource(Resource):
    """
    Progress Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(syncProgressModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get sync progress
        """
        mgr = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            syncCtx = SyncContext(mgr=mgr, aDBS=aDBS)
            return mgr.profile.sync.progress.get(syncCtx, syncSMID=smID)
