#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Sync Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('sync', description="Sync Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .sync import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
from .progress import * #pylint: disable=C0413,W0401
