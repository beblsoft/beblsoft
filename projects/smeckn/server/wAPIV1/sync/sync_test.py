#!/usr/bin/env python3
"""
 NAME:
  sync_test.py

 DESCRIPTION
  Test Sync Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
import urllib
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.model import SyncStatus
from smeckn.server.wAPIV1.sync.models import verifySyncModel



# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- SYNC GET TEST CASE ------------------------------ #
class SyncGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validURLArgs  = urllib.parse.urlencode({"profileSMID": self.popProfileSMID, "latest": "true"})
        self.validURL      = "{}?{}".format(self.routes.sync, self.validURLArgs)
        self.syncModelOld  = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                        status=SyncStatus.SUCCESS,
                                                        createDate=datetime.utcnow() - timedelta(days=2))
        self.syncModel     = self.dbp.syncP.populateOne(aID=self.popAccountID, profileSMID=self.popProfileSMID,
                                                        status=SyncStatus.IN_PROGRESS, createDate=datetime.utcnow())
        self.syncModelList = [self.syncModelOld, self.syncModel]

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_trueLatest(self):
        """
        True latest should return one sync
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifySyncModel(aDBS=aDBS, apiModel=data[0], smID=data[0].get("smID"))
        self.assertEqual(len(data), 1)

    @logFunc()
    def test_falseLatest(self):
        """
        False latest should return all syncs
        """
        urlArgs  = urllib.parse.urlencode({"profileSMID": self.popProfileSMID, "latest": "false"})
        url      = "{}?{}".format(self.routes.sync, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            for apiModel in data:
                verifySyncModel(aDBS=aDBS, apiModel=apiModel, smID=apiModel.get("smID"))
        self.assertEqual(len(data), len(self.syncModelList))

    @logFunc()
    def test_noLatest(self):
        """
        Test no latest in url args, should default to True
        """
        urlArgs  = urllib.parse.urlencode({"profileSMID": self.popProfileSMID})
        url      = "{}?{}".format(self.routes.sync, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifySyncModel(aDBS=aDBS, apiModel=data[0], smID=data[0].get("smID"))
        self.assertEqual(len(data), 1)

    @logFunc()
    def test_noSyncs(self):
        """
        Test with no syncs in db
        """
        self.dbp.syncP.depopulateAll(aID=self.popAccountID)
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_noProfileSMID(self):
        """
        Test No ProfileSMID
        """
        url       = self.routes.sync
        (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badProfileSMID(self):
        """
        Test Bad ProfileSMID
        """
        urlArgs  = urllib.parse.urlencode({"profileSMID": 129812})
        url      = "{}?{}".format(self.routes.sync, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- SYNC POST TEST CASE ----------------------------- #
class SyncPostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validURL      = self.routes.sync
        self.validPayload  = {
            "profileSMID": self.popProfileSMID
        }

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        # Start sync and wait for all workers to complete
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        syncSMID = data.get("smID")
        self.assertEqual(data.get("status"), SyncStatus.IN_PROGRESS.name)
        self.mgr.jqueue.joinWorkers()

        # Get sync, it should now be successfully complete
        url          = "{}/{}".format(self.routes.sync, syncSMID)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("smID"), syncSMID)
        self.assertEqual(data.get("status"), SyncStatus.SUCCESS.name)

    @logFunc()
    def test_badProfileSMID(self):
        """
        Test bad profileSMID
        """
        payload                = self.validPayload.copy()
        payload["profileSMID"] = 2039483
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1601)

    @logFunc()
    def test_noProfileSMID(self):
        """
        Test no profile SMID
        """
        payload = self.validPayload.copy()
        payload.pop("profileSMID")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
