#!/usr/bin/env python3
"""
 NAME:
  sync.py

 DESCRIPTION
  Sync Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import request, current_app
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.sync.models import syncModel, syncGetParser, syncPostInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.profile.sync.context import SyncContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.wAPIV1.sync import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SYNC RESOURCE ------------------------------------ #
@ns.route("")
class SyncResource(Resource):
    """
    Sync Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(syncGetParser)
    @ns.marshal_list_with(syncModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get syncs
        """
        args          = syncGetParser.parse_args()
        profileSMID   = args.get("profileSMID")
        latest        = args.get("latest")
        syncModelList = []


        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            getAllKwargs                            = {}
            getAllKwargs["aDBS"]                    = aDBS
            getAllKwargs["profileSMID"]             = profileSMID
            if latest:
                getAllKwargs["orderCreateDateDesc"] = True
                getAllKwargs["first"]               = True
            rval                                    = SyncDAO.getAll(**getAllKwargs)

            # Convert rval into list
            if rval is None:
                syncModelList = []
            elif isinstance(rval, list):
                syncModelList = rval
            else:
                syncModelList = [rval]
            return syncModelList

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(syncPostInputModel)
    @ns.marshal_with(syncModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create and start sync
        """
        inData      = request.json
        profileSMID = inData.get("profileSMID")
        mgr         = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            syncCtx      = SyncContext(mgr=mgr, aDBS=aDBS)
            profileModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=profileSMID)  # Verify exists
            return mgr.profile.sync.start(syncCtx=syncCtx, profileSMID=profileModel.smID)
