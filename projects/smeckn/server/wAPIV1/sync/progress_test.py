#!/usr/bin/env python3
"""
 NAME:
  progress_test.py

 DESCRIPTION
  Test Sync SMID Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.sync.model import SyncStatus


# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- PROGRESS GET TEST CASE -------------------------- #
class ProgressGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        # Start sync and wait for it to complete
        (code, data) = self.postJSON(self.routes.sync, d={"profileSMID": self.popProfileSMID}, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        syncSMID = data.get("smID")
        self.mgr.jqueue.joinWorkers()

        # Get sync progress
        url = "{}/{}/progress".format(self.routes.sync, syncSMID)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("status"), SyncStatus.SUCCESS.name)
        for syncContentProgressModel in data.get("contentProgressList"):
            self.assertEqual(syncContentProgressModel.get("status"), SyncStatus.SUCCESS.name)
            self.assertGreater(syncContentProgressModel.get("count"), 0)
        self.assertEqual(len(data.get("contentProgressList")), len(self.mgr.profileStub.syncManagerDict))

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url          = "{}/{}/progress".format(self.routes.sync, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        url = "{}/2387/progress".format(self.routes.sync)
        self.authTest(route=url, methodFunc=self.getJSON)
