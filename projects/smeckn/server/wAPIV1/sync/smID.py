#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Sync SMID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import request
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.sync.models import syncModel, syncPutInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sync.dao import SyncDAO
from smeckn.server.wAPIV1.sync import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Sync Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(syncModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get sync
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return SyncDAO.getBySMID(aDBS=aDBS, smID=smID)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(syncPutInputModel)
    @ns.marshal_with(syncModel)
    @logFunc()
    def put(self, smID):  # pylint: disable=R0201
        """
        Update sync
        """
        inData    = request.json
        errorSeen = inData.get("errorSeen", None)

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            syncDBModel = SyncDAO.getBySMID(aDBS=aDBS, smID=smID)
            with AccountDatabase.transactionScope(session=aDBS):
                if errorSeen is not None:
                    syncDBModel = SyncDAO.update(aDBS=aDBS, syncModel=syncDBModel, errorSeen=errorSeen)
            aDBS.refresh(syncDBModel)
            return syncDBModel
