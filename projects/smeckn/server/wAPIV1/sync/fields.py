#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Sync Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.sync.model import SyncStatus


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class SyncStatusField(fields.String):
    """
    Sync Status Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Sync Status",
            description = "Sync Status",
            example     = SyncStatus.IN_PROGRESS.name,
            enum        = [str(status.name) for status in list(SyncStatus)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, SyncStatus):
            return value.name
        else:
            return None


class CreateDateField(fields.DateTime):
    """
    Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Date",
            description = "Sync Creation Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class RestartDateField(fields.DateTime):
    """
    Restart Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Restart Date",
            description = "Date after which next sync can be started",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class ErrorSeenField(fields.Boolean):
    """
    Error Seen Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Error Seen",
            description = "If True, Error has been seen by the client",
            example     = False,
            required    = required,
            **kwargs)

class ContentCountField(fields.Integer):
    """
    Content Count Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Content Count",
            description = "Count of content synced.",
            example     = 398,
            required    = required,
            **kwargs)
