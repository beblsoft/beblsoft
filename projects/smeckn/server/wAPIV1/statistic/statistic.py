#!/usr/bin/env python3
"""
 NAME:
  statistic.py

 DESCRIPTION
  Statistic Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.statistic.context import StatisticContext
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.statistic.models import statisticGetParser, statisticModel
from smeckn.server.wAPIV1.statistic import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ STATISTIC RESOURCE ------------------------------- #
@ns.route("")
class StatisticResource(Resource):
    """
    Statistic Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(statisticGetParser)
    @ns.marshal_list_with(statisticModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get statistics
        """
        mgr                   = current_app.mgr
        args                  = statisticGetParser.parse_args()
        profileSMID           = args.get("profileSMID")
        contentType           = ContentType[args.get("contentType")]
        contentAttrType       = ContentAttrType[args.get("contentAttrType")]
        intervalStartDate     = args.get("intervalStartDate", None)
        if intervalStartDate:
            intervalStartDate = intervalStartDate.replace(tzinfo=None)
        intervalEndDate       = args.get("intervalEndDate", None)
        if intervalEndDate:
            intervalEndDate   = intervalEndDate.replace(tzinfo=None)
        statisticModelList    = []

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            statisticCtx = StatisticContext(
                mgr=mgr,
                aDBS=aDBS,
                profileSMID=profileSMID,
                contentType=contentType,
                contentAttrType=contentAttrType,
                intervalStartDate=intervalStartDate,
                intervalEndDate=intervalEndDate)
            statisticModelList  = mgr.profile.statistic.getList(statisticCtx=statisticCtx)

        return statisticModelList
