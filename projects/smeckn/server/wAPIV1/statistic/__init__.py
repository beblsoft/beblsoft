#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Statistic Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("statistic", description="Statistic Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .statistic import *  #pylint: disable=C0413,W0401
