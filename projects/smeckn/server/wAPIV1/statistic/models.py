#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Statistic Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import inputs
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.wAPIV1.common.fields import ContentTypeField, ContentAttrTypeField
from smeckn.server.wAPIV1.statistic.fields import IntervalStartDateField, IntervalEndDateField
from smeckn.server.wAPIV1.statistic.fields import CountField, MinField, MaxField, SumField
from smeckn.server.wAPIV1.statistic.fields import AverageField, StandardDeviationField
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.statistic import ns
from smeckn.server.aDB.statistic.context import StatisticContext


# ------------------------ PARSERS ------------------------------------------ #
statisticGetParser = ns.parser()
statisticGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
statisticGetParser.add_argument(
    "contentType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [contentType.name for contentType in list(ContentType)])
statisticGetParser.add_argument(
    "contentAttrType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [contentAttrType.name for contentAttrType in list(ContentAttrType)])
statisticGetParser.add_argument(
    "intervalStartDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")
statisticGetParser.add_argument(
    "intervalEndDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")


# ------------------------ MODELS ------------------------------------------- #
statisticModel = ns.model("StatisticModel", {
    "profileSMID": ProfileSMIDField(),
    "contentType": ContentTypeField(),
    "contentAttrType": ContentAttrTypeField(),
    "intervalStartDate": IntervalStartDateField(required=False),
    "intervalEndDate": IntervalEndDateField(required=False),
    "count": CountField(),
    "min": MinField(),
    "max": MaxField(),
    "sum": SumField(),
    "average": AverageField(),
    "stddev": StandardDeviationField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyStatisticModelList(aDBS, mgr, apiModel, profileSMID, contentType, contentAttrType,
                             intervalStartDate=None, intervalEndDate=None):
    """
    Verify statistic model list
    """
    statisticCtx = StatisticContext(
        mgr=mgr, aDBS=aDBS, profileSMID=profileSMID, contentType=contentType,
        contentAttrType=contentAttrType, intervalStartDate=intervalStartDate, intervalEndDate=intervalEndDate)
    statisticModelList  = mgr.profile.statistic.getList(statisticCtx=statisticCtx)

    # Iterate over all StatisticModels
    for idx, _statisticModel in enumerate(statisticModelList):
        statisticAPIModel = apiModel[idx]
        assert _statisticModel.profileSMID == statisticAPIModel.get("profileSMID")
        assert _statisticModel.contentType.name == statisticAPIModel.get("contentType")
        assert _statisticModel.contentAttrType.name == statisticAPIModel.get("contentAttrType")
        assert _statisticModel.count == statisticAPIModel.get("count")
        assert _statisticModel.min == statisticAPIModel.get("min")
        assert _statisticModel.max == statisticAPIModel.get("max")
        assert _statisticModel.sum == statisticAPIModel.get("sum")
        assert _statisticModel.average == statisticAPIModel.get("average")
        assert _statisticModel.stddev == statisticAPIModel.get("stddev")
        if _statisticModel.intervalStartDate:
            assert _statisticModel.intervalStartDate.isoformat() == statisticAPIModel.get("intervalStartDate")
        if _statisticModel.intervalEndDate:
            assert _statisticModel.intervalEndDate.isoformat() == statisticAPIModel.get("intervalEndDate")
