#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Facebook Post Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields, inputs
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.facebookPost.fields import SMIDField, FBIDField
from smeckn.server.wAPIV1.facebookPost.fields import FBCreateDateField, FBUpdateDateField
from smeckn.server.wAPIV1.facebookPost.fields import TypeField, NLikesField, NCommentsField
from smeckn.server.wAPIV1.facebookPost.fields import NReactionsField, PermalinkURLField
from smeckn.server.wAPIV1.text.models import textModel, verifyTextModel
from smeckn.server.wAPIV1.photo.models import photoModel, verifyPhotoModel
from smeckn.server.wAPIV1.link.models import linkModel, verifyLinkModel
from smeckn.server.wAPIV1.video.models import videoModel, verifyVideoModel
from smeckn.server.wAPIV1.pagination.models import paginationParser, paginationModel
from smeckn.server.wAPIV1.facebookPost import ns
from smeckn.server.aDB.sort.order import SortOrder
from smeckn.server.aDB.sort.type import SortType
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO


# ------------------------ PARSERS ------------------------------------------ #
facebookPostGetParser = paginationParser.copy()
facebookPostGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
facebookPostGetParser.add_argument(
    "createdAfterDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")
facebookPostGetParser.add_argument(
    "createdBeforeDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")
facebookPostGetParser.add_argument(
    "searchString",
    type         = str,
    required     = False,
    location     = "args")
facebookPostGetParser.add_argument(
    "containsMessage",
    type         = inputs.boolean,
    required     = False,
    default      = False,
    location     = "args")
facebookPostGetParser.add_argument(
    "sortType",
    type         = str,
    required     = False,
    location     = "args",
    default      = SortType.FACEBOOK_POST_FBCREATEDATE.name,
    choices      = [SortType.FACEBOOK_POST_FBCREATEDATE.name,
                    SortType.FACEBOOK_POST_NLIKES.name,
                    SortType.FACEBOOK_POST_NCOMMENTS.name,
                    SortType.FACEBOOK_POST_NREACTIONS.name,
                    SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_MIXED_SCORE.name])
facebookPostGetParser.add_argument(
    "sortOrder",
    type         = str,
    required     = False,
    location     = "args",
    default      = SortOrder.DESCENDING.name,
    choices      = [sortOrder.name for sortOrder in list(SortOrder)])


# ------------------------ MODELS ------------------------------------------- #
facebookPostModel = ns.model("FacebookPostModel", {
    "smID": SMIDField(),
    "fbID": FBIDField(),
    "fbCreateDate": FBCreateDateField(),
    "fbUpdateDate": FBUpdateDateField(),
    "type": TypeField(),
    "nLikes": NLikesField(),
    "nComments": NCommentsField(),
    "nReactions": NReactionsField(),
    "fbpSMID": ProfileSMIDField(),
    "permalinkURL": PermalinkURLField(),
    "messageTextModel": fields.Nested(textModel, allow_null=True),
    "photoModel": fields.Nested(photoModel, allow_null=True),
    "linkModel": fields.Nested(linkModel, allow_null=True),
    "videoModel": fields.Nested(videoModel, allow_null=True)
})

facebookPostPaginationModel = ns.inherit(
    "FacebookPostPaginationModel",
    paginationModel,
    {
        "data": fields.List(fields.Nested(facebookPostModel)),
    }
)


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyFacebookPostModel(aDBS, apiModel, smID):
    """
    Verify facebook post model
    """
    fbPostModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == fbPostModel.smID
    assert apiModel.get("fbID") == fbPostModel.fbID
    if fbPostModel.fbCreateDate:
        assert apiModel.get("fbCreateDate") == fbPostModel.fbCreateDate.isoformat()
    if fbPostModel.fbUpdateDate:
        assert apiModel.get("fbUpdateDate") == fbPostModel.fbUpdateDate.isoformat()
    assert apiModel.get("type") == fbPostModel.type.name
    assert apiModel.get("nLikes") == fbPostModel.nLikes
    assert apiModel.get("nComments") == fbPostModel.nComments
    assert apiModel.get("nReactions") == fbPostModel.nReactions
    assert apiModel.get("fbpSMID") == fbPostModel.fbpSMID
    assert apiModel.get("permalinkURL") == fbPostModel.permalinkURL

    messageTextModel = apiModel.get("messageTextModel")
    if messageTextModel:
        verifyTextModel(aDBS=aDBS, apiModel=messageTextModel, smID=messageTextModel.get("smID"))

    _photoModel = apiModel.get("photoModel")
    if _photoModel:
        verifyPhotoModel(aDBS=aDBS, apiModel=_photoModel, smID=_photoModel.get("smID"))

    _linkModel = apiModel.get("linkModel")
    if _linkModel:
        verifyLinkModel(aDBS=aDBS, apiModel=_linkModel, smID=_linkModel.get("smID"))

    _videoModel = apiModel.get("videoModel")
    if _videoModel:
        verifyVideoModel(aDBS=aDBS, apiModel=_videoModel, smID=_videoModel.get("smID"))
