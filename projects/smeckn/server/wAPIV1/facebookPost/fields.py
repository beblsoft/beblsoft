#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Facebook Post Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.facebookPost.model import FacebookPostType


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)


class FBIDField(fields.String):
    """
    FBID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "FBID",
            description = "Facebook Identification Number",
            example     = "102691597514550_102700544180322",
            required    = required,
            **kwargs)


class FBCreateDateField(fields.DateTime):
    """
    Facebook Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Create Date",
            description = "Facebook Create Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class FBUpdateDateField(fields.DateTime):
    """
    Facebook Update Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Update Date",
            description = "Facebook Update Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class TypeField(fields.String):
    """
    Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Post Type",
            description = "Facebook Post Type",
            example     = FacebookPostType.STATUS.name,
            enum        = [facebookPostType.name for facebookPostType in list(FacebookPostType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, FacebookPostType):
            return value.name
        else:
            return None


class NLikesField(fields.Integer):
    """
    N Likes Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Number of Likes",
            description = "Number of Likes",
            example     = 500,
            required    = required,
            **kwargs)


class NCommentsField(fields.Integer):
    """
    N Comments Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Number of Comments",
            description = "Number of Comments",
            example     = 500,
            required    = required,
            **kwargs)


class NReactionsField(fields.Integer):
    """
    N Reactions Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Number of Reactions",
            description = "Number of Reactions",
            example     = 500,
            required    = required,
            **kwargs)

class PermalinkURLField(fields.String):
    """
    Permalink URL Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Permalink URL",
            description = "Permant Link to underlying facebook post",
            example     = "https://www.facebook.com/102691597514550/posts/102690844181292",
            required    = required,
            **kwargs)
