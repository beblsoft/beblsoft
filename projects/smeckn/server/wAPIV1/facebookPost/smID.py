#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Facebook Post SMID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.facebookPost.models import facebookPostModel
from smeckn.server.wAPIV1.facebookPost import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Facebook Post Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(facebookPostModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get facebook post
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            fbPostDBModel = FacebookPostDAO.getBySMID(aDBS=aDBS, smID=smID)
            return marshal(fbPostDBModel, facebookPostModel)
