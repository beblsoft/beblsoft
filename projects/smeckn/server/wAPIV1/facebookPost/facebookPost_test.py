#!/usr/bin/env python3
"""
 NAME:
  facebookPost_test.py

 DESCRIPTION
  Test Facebook Post Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.facebookPost.models import verifyFacebookPostModel
from smeckn.server.wAPIV1.facebookPost.models import facebookPostGetParser
from smeckn.server.wAPIV1.facebookPost.facebookPost import FacebookPostResource
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sort.order import SortOrder
from smeckn.server.aDB.sort.type import SortType
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.text.dao import TextDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- FACEBOOK POST GET TEST CASE --------------------- #
class FacebookPostGetTestCase(CommonTestCase):
    """
    Facebook Post Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)
        self.baseURL           = self.routes.facebookPost
        self.createdAfterDate  = self.testStartDate
        self.createdBeforeDate = datetime.utcnow()
        self.limit             = 17
        self.cursor            = ""
        self.searchString      = "The fox is in the basement"
        self.validArgs         = {
            "profileSMID": self.popFBPSMID,
            # "createdAfterDate"
            # "createdBeforeDate"
            # "searchString"
            # "containsMessage"
            # "sortType"
            # "sortOrder"
            "limit": self.limit,
            "cursor": self.cursor
        }
        self.validURL          = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    @logFunc()
    def test_apiModel(self):
        """
        Test facebook api model is accurate
        """
        super().setUp(facebookPostsFull=True)
        args              = self.validArgs.copy()
        keepLooping       = True
        nextCursor        = ""
        while keepLooping:
            args["cursor"]    = nextCursor
            url               = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, data)      = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            dataList          = data.get("data")
            nextCursor        = data.get("nextCursor")
            keepLooping       = nextCursor != ""
            with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
                for apiModel in dataList:
                    verifyFacebookPostModel(aDBS=aDBS, apiModel=apiModel, smID=apiModel.get("smID"))

    @logFunc()
    def test_filters(self):
        """
        Test filters
        """
        filterList = [
            ("createdAfterDate", self.createdAfterDate.isoformat(), 2),
            ("createdAfterDate", (self.createdAfterDate + timedelta(seconds=4)).isoformat(), 0),
            ("createdBeforeDate", (self.createdAfterDate + timedelta(seconds=4)).isoformat(), 2),
            ("createdBeforeDate", (self.createdAfterDate - timedelta(seconds=4)).isoformat(), 0),
            ("searchString", "blah", 1),
            ("searchString", "Blah", 1),
            ("searchString", "Hello", 0),
            ("containsMessage", True, 1),
            ("containsMessage", False, 2)
        ]

        # Create posts
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            facebookPostModel = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID, fbID=1,
                                                       fbCreateDate=self.createdAfterDate)
            _                 = TextDAO.createFromText(aDBS=aDBS, fbPostModel=facebookPostModel,
                                                       text="Blah, Blah, Blah")
            _                 = FacebookPostDAO.create(aDBS=aDBS, fbpSMID=self.popFBPSMID, fbID=2,
                                                       fbCreateDate=self.createdAfterDate)

        # Verify filters
        for fitlerName, value, count in filterList:
            args             = self.validArgs.copy()
            args[fitlerName] = value
            url              = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, data)     = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            dataList         = data.get("data")
            self.assertEqual(len(dataList), count)

    @logFunc()
    def test_getParserSortTypesAllSupported(self):
        """
        Test that all facebookPostGetParser sortTypes are supported in the resoure's getSortTypeDict
        """
        # Get allowable sortType choices
        supportSortTypeNameList = []
        for arg in facebookPostGetParser.args:
            if arg.name == "sortType":
                supportSortTypeNameList = arg.choices
        self.assertTrue(supportSortTypeNameList)

        # Verify each choice supported in getSortTypeList
        for supportTypeName in supportSortTypeNameList:
            self.assertIn(supportTypeName, [k.name for k, _ in FacebookPostResource.getSortTypeDict.items()])

    @logFunc()
    def test_sorts(self):
        """
        Test sorting by different entities
        """
        sortDictList = [
            {
                "sortType": SortType.FACEBOOK_POST_FBCREATEDATE,
                "getValFunc": lambda aM: aM.get("fbCreateDate"),
                "totalCheck": True
            },
            {
                "sortType": SortType.FACEBOOK_POST_NLIKES,
                "getValFunc": lambda aM: aM.get("nLikes"),
                "totalCheck": True
            },
            {
                "sortType": SortType.FACEBOOK_POST_NCOMMENTS,
                "getValFunc": lambda aM: aM.get("nComments"),
                "totalCheck": True
            },
            {
                "sortType": SortType.FACEBOOK_POST_NREACTIONS,
                "getValFunc": lambda aM: aM.get("nReactions"),
                "totalCheck": True
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE,
                "getValFunc": lambda aM: aM.get("messageTextModel").get("compSentAnalysisModel").get("positiveScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE,
                "getValFunc": lambda aM: aM.get("messageTextModel").get("compSentAnalysisModel").get("mixedScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE,
                "getValFunc": lambda aM: aM.get("messageTextModel").get("compSentAnalysisModel").get("neutralScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_MIXED_SCORE,
                "getValFunc": lambda aM: aM.get("messageTextModel").get("compSentAnalysisModel").get("mixedScore"),
                "totalCheck": False
            },
        ]
        super().setUp(facebookPostsFull=True)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            nPostsTotal = FacebookPostDAO.getAll(aDBS=aDBS, fbpSMID=self.popFBPSMID, count=True)

        for sd in sortDictList:
            for sortOrder in [SortOrder.ASCENDING, SortOrder.DESCENDING]:
                args              = self.validArgs.copy()
                args["sortType"]  = sd.get("sortType").name
                args["sortOrder"] = sortOrder.name
                getValFunc        = sd.get("getValFunc")
                loopCompFunc      = self.assertGreaterEqual if sortOrder == SortOrder.DESCENDING else self.assertLessEqual
                keepLooping       = True
                nextCursor        = ""
                total             = 0
                lastCallAPIModel  = None

                while keepLooping:
                    args["cursor"]    = nextCursor
                    url               = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
                    (code, data)      = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
                    self.assertEqual(code, 200)
                    dataList          = data.get("data")
                    nextCursor        = data.get("nextCursor")
                    keepLooping       = nextCursor != ""
                    total            += len(dataList)
                    self.assertLessEqual(len(dataList), self.limit)
                    for idx, apiModel in enumerate(dataList):
                        # Check ordering between each element in this api call
                        if idx + 1 < len(dataList):
                            nextAPIModel = dataList[idx + 1]
                            loopCompFunc(getValFunc(apiModel), getValFunc(nextAPIModel))

                        # Check ordering between last api call and this one
                        if lastCallAPIModel:
                            loopCompFunc(getValFunc(lastCallAPIModel), getValFunc(apiModel))
                    if dataList:
                        lastCallAPIModel = dataList[-1]

                if sd.get("totalCheck"):
                    self.assertEqual(total, nPostsTotal)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test removing required arguments, verifying invalid
        """
        argList = ["profileSMID"]
        for arg in argList:
            args      = self.validArgs.copy()
            args.pop(arg)
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test bad args
        """
        argBadCodeList = [("profileSMID", "Bad Argument", 400),
                          ("profileSMID", 23423, 404),
                          ("createdAfterDate", "BadDateFormat", 400),
                          ("createdBeforeDate", "BadDateFormat", 400),
                          ("containsMessage", "NotABoolean", 400),
                          ("sortType", "FACEBOOK_POST", 400),
                          ("sortType", "COMP_SENT_ANALYSIS_POSITIVE", 400),
                          ("sortType", "COMP_SENT_ANALYSIS_NEGATIVE", 400),
                          ("sortOrder", "DESCENDINGGG", 400),
                          ("sortOrder", "ASCENDINGGG", 400),
                          ("limit", 200, 400),
                          ("cursor", "BadCursorValue", 400)]
        for (arg, bad, outCode) in argBadCodeList:
            args      = self.validArgs.copy()
            args[arg] = bad
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
