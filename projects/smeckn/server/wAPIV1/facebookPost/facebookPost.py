#!/usr/bin/env python3
"""
 NAME:
  facebookPost.py

 DESCRIPTION
  Facebook Post Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.facebookPost.models import facebookPostGetParser, facebookPostPaginationModel
from smeckn.server.wAPIV1.pagination.cursorSort import SortPaginationCursor
from smeckn.server.wAPIV1.facebookPost import ns
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sort.type import SortType
from smeckn.server.aDB.sort.order import SortOrder
from smeckn.server.aDB.facebookPost.dao import FacebookPostDAO
from smeckn.server.aDB.profile.dao import ProfileDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ FACEBOOK POST RESOURCE --------------------------- #
@ns.route("")
class FacebookPostResource(Resource):
    """
    Facebook Post Routes
    """

    # -------------------- GET SORT TYPE DICT ------------------------------- #
    getSortTypeDict = {
        # Each SortType maps to:
        #    pDecodeCursorFunc - Function to decode (cast) cursor pLastValue into usable value
        #    pEncodeCursorFunc - Function to retrieve encodable pLastValue from FacebookPostModel
        #    sDecodeCursorFunc - Function to decode (cast) cursor sLastValue into usable value
        #    sEncodeCursorFunc - Function to retrieve encodable sLastValue from FacebookPostModel
        SortType.FACEBOOK_POST_FBCREATEDATE: {
            "pDecodeCursorFunc": lambda pStr: datetime.fromtimestamp(pStr),  # pylint: disable=W0108
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.fbCreateDate.timestamp(),
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.FACEBOOK_POST_NLIKES: {
            "pDecodeCursorFunc": int,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.nLikes,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.FACEBOOK_POST_NCOMMENTS: {
            "pDecodeCursorFunc": int,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.nComments,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.FACEBOOK_POST_NREACTIONS: {
            "pDecodeCursorFunc": int,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.nReactions,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.messageTextModel.compSentAnalysisModel.positiveScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.messageTextModel.compSentAnalysisModel.negativeScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.messageTextModel.compSentAnalysisModel.neutralScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_MIXED_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda fbPostModel: fbPostModel.messageTextModel.compSentAnalysisModel.mixedScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda fbPostModel: fbPostModel.smID
        },
    }

    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(facebookPostGetParser)
    @ns.marshal_with(facebookPostPaginationModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get facebook posts
        """
        args                  = facebookPostGetParser.parse_args()
        profileSMID           = args.get("profileSMID")
        createdAfterDate      = args.get("createdAfterDate", None)
        if createdAfterDate:
            createdAfterDate  = createdAfterDate.replace(tzinfo=None)
        createdBeforeDate     = args.get("createdBeforeDate", None)
        if createdBeforeDate:
            createdBeforeDate = createdBeforeDate.replace(tzinfo=None)
        searchString          = args.get("searchString", None)
        containsMessage       = args.get("containsMessage")
        limit                 = args.get("limit")
        sortType              = SortType[args.get("sortType")]
        sortOrder             = SortOrder[args.get("sortOrder")]
        cursor                = SortPaginationCursor.fromParserArgs(args)
        pDecodeCursorFunc     = self.getSortTypeDict[sortType]["pDecodeCursorFunc"]
        pEncodeCursorFunc     = self.getSortTypeDict[sortType]["pEncodeCursorFunc"]
        sDecodeCursorFunc     = self.getSortTypeDict[sortType]["sDecodeCursorFunc"]
        sEncodeCursorFunc     = self.getSortTypeDict[sortType]["sEncodeCursorFunc"]
        pLastValue            = None
        sLastValue            = None
        nextCursor            = None

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            # Verify profile
            _ = ProfileDAO.getBySMID(aDBS=aDBS, smID=profileSMID)

            # Retrieve last values from cursor
            if cursor:
                pLastValue = cursor.castPLastValue(castFunc=pDecodeCursorFunc)
                sLastValue = cursor.castSLastValue(castFunc=sDecodeCursorFunc)

            # Get Data
            fbPostModelList = FacebookPostDAO.getAll(
                aDBS=aDBS, joinMessage=True, fbpSMID=profileSMID,
                createdAfterDate=createdAfterDate, createdBeforeDate=createdBeforeDate,
                containsMessage=containsMessage, messageTextILike=searchString,
                sortType=sortType, sortOrder=sortOrder,
                sortPLastValue=pLastValue, sortSLastValue=sLastValue,
                limit=limit)

            # Encode next cursor
            if len(fbPostModelList) == limit:
                lastFBPostModel = fbPostModelList[-1]
                nextCursor      = SortPaginationCursor(pLastValue=pEncodeCursorFunc(lastFBPostModel),
                                                       sLastValue=sEncodeCursorFunc(lastFBPostModel))

            # Marshal data inside db scope
            return marshal({
                "nextCursor": nextCursor.toString() if nextCursor else "",
                "data": fbPostModelList
            }, facebookPostPaginationModel)
