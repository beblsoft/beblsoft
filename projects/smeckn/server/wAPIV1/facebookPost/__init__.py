#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Facebook Post Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('facebookPost', description="Facebook Post Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .facebookPost import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
