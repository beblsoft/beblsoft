#!/usr/bin/env python3
"""
 NAME:
  smID_test.py

 DESCRIPTION
  Test SMID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.facebookPost.models import verifyFacebookPostModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- SMID GET TEST CASE --------------------------- #
class SMIDGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True, facebookPosts=True)
        self.validURL = "{}/{}".format(self.routes.facebookPost, self.popFBStatusPostSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyFacebookPostModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badSMID(self):
        """
        Test with a bad SMID
        """
        url          = "{}/{}".format(self.routes.facebookPost, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
