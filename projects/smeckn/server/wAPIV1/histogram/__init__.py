#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Histogram Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("histogram", description="Histogram Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .histogram import *  #pylint: disable=C0413,W0401
