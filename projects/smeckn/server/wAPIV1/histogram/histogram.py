#!/usr/bin/env python3
"""
 NAME:
  histogram.py

 DESCRIPTION
  Histogram Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.aDB.histogram.context import HistogramContext
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.histogram.models import histogramGetParser, histogramModel
from smeckn.server.wAPIV1.histogram import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ HISTOGRAM RESOURCE ------------------------------- #
@ns.route("")
class HistogramResource(Resource):
    """
    Histogram Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(histogramGetParser)
    @ns.marshal_list_with(histogramModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get histograms
        """
        # Note:
        #   Strip tzinfo on intervalStartDate, intervalEndDate
        #   Python throws an exception when datetimes are compared and 1 has tzinfo and the other doesn't
        mgr                = current_app.mgr
        args               = histogramGetParser.parse_args()
        profileSMID        = args.get("profileSMID")
        contentType        = ContentType[args.get("contentType")]
        contentAttrType    = ContentAttrType[args.get("contentAttrType")]
        groupByType        = GroupByType[args.get("groupByType")]
        intervalStartDate  = args.get("intervalStartDate").replace(tzinfo=None)
        intervalEndDate    = args.get("intervalEndDate").replace(tzinfo=None)
        histogramModelList = []

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            histogramCtx = HistogramContext(
                mgr=mgr,
                aDBS=aDBS,
                profileSMID=profileSMID,
                contentType=contentType,
                contentAttrType=contentAttrType,
                groupByType=groupByType,
                intervalStartDate=intervalStartDate,
                intervalEndDate=intervalEndDate)
            histogramModelList  = mgr.profile.histogram.getList(histogramCtx=histogramCtx)

        return histogramModelList
