#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Histogram Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class GroupByDateField(fields.DateTime):
    """
    Group By Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Group By Date",
            description = "Group By Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class IntervalStartDateField(fields.DateTime):
    """
    Interval Start Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Interval Start Date",
            description = "Interval Start Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class IntervalEndDateField(fields.DateTime):
    """
    Interval End Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Interval End Date",
            description = "Interval End Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class CountField(fields.Integer):
    """
    Count Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count",
            description = "Count",
            example     = 500,
            required    = required,
            **kwargs)


class MinField(fields.Float):
    """
    Min Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Min",
            description = "Min",
            example     = 1,
            required    = required,
            **kwargs)


class MaxField(fields.Float):
    """
    Max Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Max",
            description = "Max",
            example     = 500,
            required    = required,
            **kwargs)


class SumField(fields.Float):
    """
    Sum Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Sum",
            description = "Sum",
            example     = 2909,
            required    = required,
            **kwargs)


class AverageField(fields.Float):
    """
    Average Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Average",
            description = "Average",
            example     = 239.23,
            required    = required,
            **kwargs)


class StandardDeviationField(fields.Float):
    """
    Standard Deviation Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Standard Deviation",
            description = "Standard Deviation",
            example     = 32.23,
            required    = required,
            **kwargs)
