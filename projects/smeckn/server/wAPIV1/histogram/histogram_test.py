#!/usr/bin/env python3
"""
 NAME:
  histogram_test.py

 DESCRIPTION
  Test Histogram Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.histogram.models import verifyHistogramModelList


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- HISTOGRAM GET TEST CASE ------------------------- #
class HistogramGetTestCase(CommonTestCase):
    """
    Histogram Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.baseURL           = self.routes.histogram
        self.contentType       = ContentType.POST
        self.contentAttrType   = ContentAttrType.FACEBOOK_POST_SMID
        self.groupByType       = GroupByType.FACEBOOK_POST_CREATE_DATE
        self.intervalStartDate = datetime.utcnow()
        self.intervalEndDate   = datetime.utcnow() + timedelta(seconds=10)
        self.validArgs         = {
            "profileSMID": self.popProfileSMID,
            "contentType": self.contentType.name,
            "contentAttrType": self.contentAttrType.name,
            "groupByType": self.groupByType.name,
            "intervalStartDate": self.intervalStartDate.isoformat(),
            "intervalEndDate": self.intervalEndDate.isoformat()
        }
        self.validURL          = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_success(self):
        """
        Test success case
        """
        groupByTypeList = [GroupByType.FACEBOOK_POST_CREATE_DATE,
                           GroupByType.COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT]
        for groupByType in groupByTypeList:
            args = self.validArgs.copy()
            args["groupByType"] = groupByType.name
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
                verifyHistogramModelList(aDBS=aDBS, mgr=self.mgr, apiModel=data, profileSMID=self.popProfileSMID,
                                         contentType=self.contentType,
                                         contentAttrType=self.contentAttrType,
                                         groupByType=groupByType,
                                         intervalStartDate=self.intervalStartDate,
                                         intervalEndDate=self.intervalEndDate)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test having no arg produces 400
        """
        argList = ["profileSMID",
                   "contentType",
                   "contentAttrType",
                   "groupByType",
                   "intervalStartDate",
                   "intervalEndDate"]
        for arg in argList:
            args = self.validArgs.copy()
            args.pop(arg)
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test having bad arg produces out code
        """
        argBadCodeList = [("profileSMID", "Bad Argument", 400),
                          ("profileSMID", 23423, 404),
                          ("contentType", "Bad Argument", 400),
                          ("contentAttrType", "Bad Argument", 400),
                          ("groupByType", "Bad Argument", 400),
                          ("intervalStartDate", "Bad Argument", 400),
                          ("intervalEndDate", "Bad Argument", 400)]
        for (arg, bad, outCode) in argBadCodeList:
            args = self.validArgs.copy()
            args[arg] = bad
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
