#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Content Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields
from flask_restplus.inputs import datetime_from_iso8601
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.common.fields import ContentTypeField, ContentAttrTypeField
from smeckn.server.wAPIV1.common.fields import GroupByTypeField, GroupByDateIntervalField
from smeckn.server.wAPIV1.histogram.fields import GroupByDateField
from smeckn.server.wAPIV1.histogram.fields import IntervalStartDateField, IntervalEndDateField
from smeckn.server.wAPIV1.histogram.fields import CountField, MinField, MaxField, SumField
from smeckn.server.wAPIV1.histogram.fields import AverageField, StandardDeviationField
from smeckn.server.wAPIV1.histogram import ns
from smeckn.server.aDB.histogram.context import HistogramContext


# ------------------------ PARSERS ------------------------------------------ #
histogramGetParser = ns.parser()
histogramGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
histogramGetParser.add_argument(
    "contentType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [contentType.name for contentType in list(ContentType)])
histogramGetParser.add_argument(
    "contentAttrType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [contentAttrType.name for contentAttrType in list(ContentAttrType)])
histogramGetParser.add_argument(
    "groupByType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [groupByType.name for groupByType in list(GroupByType)])
histogramGetParser.add_argument(
    "intervalStartDate",
    type         = datetime_from_iso8601,
    required     = True,
    location     = "args")
histogramGetParser.add_argument(
    "intervalEndDate",
    type         = datetime_from_iso8601,
    required     = True,
    location     = "args")


# ------------------------ MODELS ------------------------------------------- #
histogramDataModel = ns.model("HistogramDataModel", {
    "groupByDate": GroupByDateField(),
    "groupByContentAttrType": ContentAttrTypeField(),
    "count": CountField(),
    "min": MinField(),
    "max": MaxField(),
    "sum": SumField(),
    "average": AverageField(),
    "stddev": StandardDeviationField()
})

histogramModel = ns.model("HistogramModel", {
    "profileSMID": ProfileSMIDField(),
    "contentType": ContentTypeField(),
    "contentAttrType": ContentAttrTypeField(),
    "groupByType": GroupByTypeField(),
    "groupByDateInterval": GroupByDateIntervalField(),
    "intervalStartDate": IntervalStartDateField(),
    "intervalEndDate": IntervalEndDateField(),
    "dataModelList": fields.List(fields.Nested(histogramDataModel))
})

# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyHistogramModelList(aDBS, mgr, apiModel, profileSMID, contentType, contentAttrType, groupByType,
                             intervalStartDate, intervalEndDate):
    """
    Verify histogram model list
    """
    histogramCtx = HistogramContext(
        mgr=mgr, aDBS=aDBS, profileSMID=profileSMID,
        contentType=contentType, contentAttrType=contentAttrType, groupByType=groupByType,
        intervalStartDate=intervalStartDate, intervalEndDate=intervalEndDate)
    histogramModelList = mgr.profile.histogram.getList(histogramCtx=histogramCtx)

    # Iterate over all HistogramModels
    for idx, _histogramModel in enumerate(histogramModelList):
        histogramAPIModel = apiModel[idx]
        assert _histogramModel.profileSMID == histogramAPIModel.get("profileSMID")
        assert _histogramModel.contentType.name == histogramAPIModel.get("contentType")
        assert _histogramModel.contentAttrType.name == histogramAPIModel.get("contentAttrType")
        assert _histogramModel.groupByType.name == histogramAPIModel.get("groupByType")
        assert _histogramModel.intervalStartDate.isoformat() == histogramAPIModel.get("intervalStartDate")
        assert _histogramModel.intervalEndDate.isoformat() == histogramAPIModel.get("intervalEndDate")
        if _histogramModel.groupByDateInterval:
            assert _histogramModel.groupByDateInterval.name, histogramAPIModel.get("groupByDateInterval")

        # Iterate over all HistogramDataModels
        for jdx, _histogramDataModel in enumerate(_histogramModel.dataModelList):
            histogramDataAPIModel = histogramAPIModel.get("dataModelList")[jdx]
            if _histogramDataModel.groupByDate:
                assert _histogramDataModel.groupByDate.isoformat() == histogramDataAPIModel.get("groupByDate")
            if _histogramDataModel.groupByContentAttrType:
                assert _histogramDataModel.groupByContentAttrType.name == histogramDataAPIModel.get(
                    "groupByContentAttrType")
            assert _histogramDataModel.count == histogramDataAPIModel.get("count")
            assert _histogramDataModel.count == histogramDataAPIModel.get("count")
            assert _histogramDataModel.min == histogramDataAPIModel.get("min")
            assert _histogramDataModel.max == histogramDataAPIModel.get("max")
            assert _histogramDataModel.sum == histogramDataAPIModel.get("sum")
            assert _histogramDataModel.average == histogramDataAPIModel.get("average")
            assert _histogramDataModel.stddev == histogramDataAPIModel.get("stddev")
