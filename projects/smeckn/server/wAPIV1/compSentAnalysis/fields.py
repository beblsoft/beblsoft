#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Comprehend Sentiment Analysis Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.compSentAnalysis.model import CompSentAnalysisStatus, CompSentType


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class CompSentAnalysisStatusField(fields.String):
    """
    Comprehend Sentiment Analysis Status Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Comprehend Sentiment Analysis Status",
            description = "Comprehend Sentiment Analysis Status",
            example     = CompSentAnalysisStatus.SUCCESS.name,
            enum        = [str(compSentAnalysisStatus.name) for compSentAnalysisStatus in list(CompSentAnalysisStatus)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, CompSentAnalysisStatus):
            return value.name
        else:
            return None

class CompSentTypeField(fields.String):
    """
    Comprehend Sentiment Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Comprehend Sentiment Type Field",
            description = "Comprehend Sentiment Type Field",
            example     = CompSentType.POSITIVE.name,
            enum        = [str(compSentType.name) for compSentType in list(CompSentType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, CompSentType):
            return value.name
        else:
            return None


class PositiveScoreField(fields.Float):
    """
    Positive Score Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Positive Score",
            description = "Positive Score",
            example     = .23,
            required    = required,
            **kwargs)


class NegativeScoreField(fields.Float):
    """
    Negative Score Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Negative Score",
            description = "Negative Score",
            example     = .23,
            required    = required,
            **kwargs)


class NeutralScoreField(fields.Float):
    """
    Neutral Score Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Neutral Score",
            description = "Neutral Score",
            example     = .23,
            required    = required,
            **kwargs)


class MixedScoreField(fields.Float):
    """
    Mixed Score Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Mixed Score",
            description = "Mixed Score",
            example     = .23,
            required    = required,
            **kwargs)
