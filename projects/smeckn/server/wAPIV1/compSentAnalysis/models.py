#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Comp Sent Analysis Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.compSentAnalysis.fields import SMIDField, CompSentAnalysisStatusField, CompSentTypeField
from smeckn.server.wAPIV1.compSentAnalysis.fields import PositiveScoreField, NegativeScoreField, NeutralScoreField, MixedScoreField
from smeckn.server.wAPIV1.default import ns
from smeckn.server.aDB.compSentAnalysis.dao import CompSentAnalysisDAO


# ------------------------ MODELS ------------------------------------------- #
compSentAnalysisModel = ns.model("CompSentAnalysisModel", {
    "smID": SMIDField(),
    "status": CompSentAnalysisStatusField(),
    "dominantSentiment": CompSentTypeField(),
    "positiveScore": PositiveScoreField(),
    "negativeScore": NegativeScoreField(),
    "neutralScore": NeutralScoreField(),
    "mixedScore": MixedScoreField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyCompSentAnalysisModel(aDBS, apiModel, smID):
    """
    Verify comp sent analysis model
    """
    _compSentAnalysisModel = CompSentAnalysisDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _compSentAnalysisModel.smID
    assert apiModel.get("status") == _compSentAnalysisModel.status.name
    if _compSentAnalysisModel.dominantSentiment:
        assert apiModel.get("dominantSentiment") == _compSentAnalysisModel.dominantSentiment.name
    else:
        assert apiModel.get("dominantSentiment") == _compSentAnalysisModel.dominantSentiment
    assert apiModel.get("positiveScore") == _compSentAnalysisModel.positiveScore
    assert apiModel.get("negativeScore") == _compSentAnalysisModel.negativeScore
    assert apiModel.get("neutralScore") == _compSentAnalysisModel.neutralScore
    assert apiModel.get("mixedScore") == _compSentAnalysisModel.mixedScore
