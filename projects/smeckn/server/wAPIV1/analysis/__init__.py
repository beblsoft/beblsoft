#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Analysis Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('analysis', description="Analysis Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .analysis import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
from .progress import * #pylint: disable=C0413,W0401
from .count import * #pylint: disable=C0413,W0401
