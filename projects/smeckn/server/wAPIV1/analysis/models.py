#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Analysis Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisStatus, AnalysisType
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.analysis.fields import SMIDField, AnalysisTypeField, AnalysisStatusField, CreateDateField
from smeckn.server.wAPIV1.analysis.fields import CountAnalyzedField, CountTotalField, ErrorSeenField
from smeckn.server.wAPIV1.analysis.fields import CountDoneField, CountNotDoneField, CountCantField
from smeckn.server.wAPIV1.common.fields import ContentTypeField
from smeckn.server.wAPIV1.analysis import ns
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.profile.analysis.context import AnalysisContext


# ------------------------ PARSERS ------------------------------------------ #
analysisGetParser = ns.parser()
analysisGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
analysisGetParser.add_argument(
    "status",
    type         = str,
    required     = False,
    default      = AnalysisStatus.IN_PROGRESS.name,
    location     = "args",
    choices      = [analysisStatus.name for analysisStatus in list(AnalysisStatus)])


analysisCountGetParser = ns.parser()
analysisCountGetParser.add_argument(
    "profileSMID",
    type         = int,
    required     = True,
    location     = "args")
analysisCountGetParser.add_argument(
    "contentType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [contentType.name for contentType in list(ContentType)])
analysisCountGetParser.add_argument(
    "analysisType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [analysisType.name for analysisType in list(AnalysisType)])


# ------------------------ MODELS ------------------------------------------- #
analysisModel = ns.model("AnalysisModel", {
    "smID": SMIDField(),
    "profileSMID": ProfileSMIDField(),
    "createDate": CreateDateField(),
    "contentType": ContentTypeField(),
    "analysisType": AnalysisTypeField(),
    "status": AnalysisStatusField(),
    "errorSeen": ErrorSeenField()
})

analysisPostInputModel = ns.model("AnalysisPostInputModel", {
    "profileSMID": ProfileSMIDField(required=True),
    "contentType": ContentTypeField(required=True),
    "analysisType": AnalysisTypeField(required=True),
})

analysisPutInputModel = ns.model("AnalysisPutInputModel", {
    "errorSeen": ErrorSeenField(required=False),
})


analysisProgressModel = ns.model("AnalysisProgressModel", {
    "contentType": ContentTypeField(),
    "analysisType": AnalysisTypeField(),
    "countAnalyzed": CountAnalyzedField(),
    "countTotal": CountTotalField(),
    "status": AnalysisStatusField()
})

analysisCountModel = ns.model("AnalysisCountModel", {
    "profileSMID": ProfileSMIDField(),
    "contentType": ContentTypeField(),
    "analysisType": AnalysisTypeField(),
    "countDone": CountDoneField(),
    "countNotDone": CountNotDoneField(),
    "countCant": CountCantField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyAnalysisModel(aDBS, apiModel, smID):
    """
    Verify analysis model
    """
    analysisDBModel = AnalysisDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == analysisDBModel.smID
    assert apiModel.get("profileSMID") == analysisDBModel.profileSMID
    assert apiModel.get("createDate") == analysisDBModel.createDate.isoformat()
    assert apiModel.get("contentType") == analysisDBModel.contentType.name
    assert apiModel.get("analysisType") == analysisDBModel.analysisType.name
    assert apiModel.get("status") == analysisDBModel.status.name
    assert apiModel.get("errorSeen") == analysisDBModel.errorSeen


@logFunc()
def verifyAnalysisCountModel(aDBS, mgr, apiModel, profileSMID, contentType, analysisType):
    """
    Verify analysis count model
    """
    analysisCtx = AnalysisContext(mgr=mgr, aDBS=aDBS)
    _analysisCountModel = mgr.profile.analysis.count.get(analysisCtx=analysisCtx,
                                                        profileSMID=profileSMID, contentType=contentType,
                                                        analysisType=analysisType)
    assert apiModel.get("profileSMID") == _analysisCountModel.profileSMID
    assert apiModel.get("contentType") == _analysisCountModel.contentType.name
    assert apiModel.get("analysisType") == _analysisCountModel.analysisType.name
    assert apiModel.get("countDone") == _analysisCountModel.countDone
    assert apiModel.get("countNotDone") == _analysisCountModel.countNotDone
    assert apiModel.get("countCant") == _analysisCountModel.countCant
