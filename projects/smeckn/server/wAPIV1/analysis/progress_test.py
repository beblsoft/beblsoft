#!/usr/bin/env python3
"""
 NAME:
  progress_test.py

 DESCRIPTION
  Test Analysis Progress Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus


# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- PROGRESS GET TEST CASE -------------------------- #
class ProgressGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, profileGroups=True, profiles=True)
        self.contentType         = ContentType.POST
        self.analysisType        = AnalysisType.COMPREHEND_SENTIMENT
        self.status              = AnalysisStatus.IN_PROGRESS
        self.contentCount        = 200
        self.profileManager      = self.mgr.profileStub
        self.analysisManager     = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                          analysisType=self.analysisType)

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        with patch.dict(self.analysisManager.__dict__, countAnalyzed=self.contentCount,
                        lenAnalyzeOneKwargList=self.contentCount) as _:

            # Start analysis and wait for it
            payload      = {"profileSMID": self.popProfileSMID, "contentType": self.contentType.name,
                            "analysisType": self.analysisType.name}
            (code, data) = self.postJSON(self.routes.analysis, d=payload, authToken=self.popAuthToken,
                                         accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            analysisSMID = data.get("smID")
            self.mgr.jqueue.joinWorkers()

            # Get analysis progress, should be completed by workers
            url = "{}/{}/progress".format(self.routes.analysis, analysisSMID)
            (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            self.assertEqual(data.get("contentType"), self.contentType.name)
            self.assertEqual(data.get("analysisType"), self.analysisType.name)
            self.assertEqual(data.get("countAnalyzed"), self.contentCount)
            self.assertEqual(data.get("countTotal"), self.contentCount)
            self.assertEqual(data.get("status"), AnalysisStatus.SUCCESS.name)

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url          = "{}/{}/progress".format(self.routes.analysis, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)


    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        url = "{}/2387/progress".format(self.routes.analysis)
        self.authTest(route=url, methodFunc=self.getJSON)
