#!/usr/bin/env python3
"""
 NAME:
  count.py

 DESCRIPTION
  Analysis Count Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType
from smeckn.server.wAPIV1.analysis.models import analysisCountModel, analysisCountGetParser
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.wAPIV1.analysis import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ COUNT RESOURCE ----------------------------------- #
@ns.route("/count")
class CountResource(Resource):
    """
    Count Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(analysisCountGetParser)
    @ns.marshal_with(analysisCountModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get analysis count
        """
        mgr          = current_app.mgr
        args         = analysisCountGetParser.parse_args()
        profileSMID  = args.get("profileSMID")
        contentType  = ContentType[args.get("contentType")]
        analysisType = AnalysisType[args.get("analysisType")]

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            analysisCtx = AnalysisContext(mgr=mgr, aDBS=aDBS)
            return mgr.profile.analysis.count.get(analysisCtx=analysisCtx, profileSMID=profileSMID,
                                                  contentType=contentType, analysisType=analysisType)
