#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Analysis SMID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import request
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.analysis.models import analysisModel, analysisPutInputModel
from smeckn.server.wAPIV1.analysis import ns
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.analysis.dao import AnalysisDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Analysis Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(analysisModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get analysis
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return AnalysisDAO.getBySMID(aDBS=aDBS, smID=smID)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(analysisPutInputModel)
    @ns.marshal_with(analysisModel)
    @logFunc()
    def put(self, smID):  # pylint: disable=R0201
        """
        Update analysis
        """
        inData    = request.json
        errorSeen = inData.get("errorSeen", None)

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            analysisDBModel = AnalysisDAO.getBySMID(aDBS=aDBS, smID=smID)
            with AccountDatabase.transactionScope(session=aDBS):
                if errorSeen is not None:
                    analysisDBModel = AnalysisDAO.update(aDBS=aDBS, analysisModel=analysisDBModel, errorSeen=errorSeen)
            aDBS.refresh(analysisDBModel)
            return analysisDBModel
