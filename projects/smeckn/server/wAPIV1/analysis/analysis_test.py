#!/usr/bin/env python3
"""
 NAME:
  analysis_test.py

 DESCRIPTION
  Test Analysis Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
import urllib
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.wAPIV1.analysis.models import verifyAnalysisModel


# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- ANALYSIS GET TEST CASE -------------------------- #
class AnalysisGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validArgs              = {"profileSMID": self.popProfileSMID, "status": AnalysisStatus.IN_PROGRESS.name}
        self.validURL               = "{}?{}".format(self.routes.analysis, urllib.parse.urlencode(self.validArgs))
        self.contentType            = ContentType.POST
        self.analysisType           = AnalysisType.COMPREHEND_SENTIMENT
        self.analysisModelInSuccess = self.dbp.analysisP.populateOne(aID=self.popAccountID,
                                                                     profileSMID=self.popProfileSMID,
                                                                     contentType=self.contentType,
                                                                     analysisType=self.analysisType,
                                                                     status=AnalysisStatus.SUCCESS)
        self.analysisModelInProg    = self.dbp.analysisP.populateOne(aID=self.popAccountID,
                                                                     profileSMID=self.popProfileSMID,
                                                                     contentType=self.contentType,
                                                                     analysisType=self.analysisType,
                                                                     status=AnalysisStatus.IN_PROGRESS)

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_successStatusInProgress(self):
        """
        Test retrieving in progress analyses
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyAnalysisModel(aDBS=aDBS, apiModel=data[0], smID=self.analysisModelInProg.smID)
        self.assertEqual(len(data), 1)

    @logFunc()
    def test_successStatusInProgressNoArg(self):
        """
        Test retrieving in progress analyses, without passing status
        """
        args = self.validArgs.copy()
        args.pop("status")
        url  = "{}?{}".format(self.routes.analysis, urllib.parse.urlencode(args))
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyAnalysisModel(aDBS=aDBS, apiModel=data[0], smID=self.analysisModelInProg.smID)
        self.assertEqual(len(data), 1)

    @logFunc()
    def test_successStatusSuccess(self):
        """
        Test retrieving in successfully completed analyses
        """
        args = self.validArgs.copy()
        args["status"] = AnalysisStatus.SUCCESS.name
        url  = "{}?{}".format(self.routes.analysis, urllib.parse.urlencode(args))
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyAnalysisModel(aDBS=aDBS, apiModel=data[0], smID=self.analysisModelInSuccess.smID)
        self.assertEqual(len(data), 1)

    @logFunc()
    def test_noAnalyses(self):
        """
        Test with no analyses
        """
        self.dbp.analysisP.depopulateAll(aID=self.popAccountID)
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_noProfileSMID(self):
        """
        Test with no profileSMID
        """
        url       = self.routes.analysis
        (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badProfileSMID(self):
        """
        Test with bad profileSMID
        """
        urlArgs  = urllib.parse.urlencode({"profileSMID": 129812})
        url      = "{}?{}".format(self.routes.analysis, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- ANALYSIS POST TEST CASE ------------------------- #
class AnalysisPostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, profileGroups=True, profiles=True)
        self.validURL         = self.routes.analysis
        self.contentType      = ContentType.POST
        self.analysisType     = AnalysisType.COMPREHEND_SENTIMENT
        self.validPayload     = {
            "profileSMID": self.popProfileSMID,
            "contentType": self.contentType.name,
            "analysisType": self.analysisType.name
        }
        self.profileManager   = self.mgr.profileStub
        self.analysisManager  = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                       analysisType=self.analysisType)

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        contentCount = 100
        with patch.dict(self.analysisManager.__dict__, countAnalyzed=contentCount, lenAnalyzeOneKwargList=contentCount) as _:
            # Start analysis and wait for all workers to complete
            (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                         accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            analysisSMID = data.get("smID")
            self.assertEqual(data.get("status"), AnalysisStatus.IN_PROGRESS.name)
            self.mgr.jqueue.joinWorkers()

            # Get analysis, it should be complete
            url          = "{}/{}".format(self.routes.analysis, analysisSMID)
            (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            self.assertEqual(data.get("smID"), analysisSMID)
            self.assertEqual(data.get("status"), AnalysisStatus.SUCCESS.name)

    @logFunc()
    def test_badProfileSMID(self):
        """
        Test bad profileSMID
        """
        payload                = self.validPayload.copy()
        payload["profileSMID"] = 2039483
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1601)

    @logFunc()
    def test_noProfileSMID(self):
        """
        Test no profile SMID
        """
        payload = self.validPayload.copy()
        payload.pop("profileSMID")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badContentType(self):
        """
        Test with bad content type
        """
        # Not in choice array
        payload                = self.validPayload.copy()
        payload["contentType"] = "adlkjasdf;lkj"
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

        # Not supported by profile
        payload                = self.validPayload.copy()
        payload["contentType"] = ContentType.POST_MESSAGE.name
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1603)

    @logFunc()
    def test_noContentType(self):
        """
        Test with no content type
        """
        payload = self.validPayload.copy()
        payload.pop("contentType")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badAnalysisType(self):
        """
        Test with bad analysis type
        """
        # Not in choice array
        payload                 = self.validPayload.copy()
        payload["analysisType"] = "adlkjasdf;lkj"
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

        # Not supported by profile
        payload                 = self.validPayload.copy()
        payload["analysisType"] = AnalysisType.COMPREHEND_LANGUAGE.name
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1605)

    @logFunc()
    def test_noAnalysisType(self):
        """
        Test with no analysis type
        """
        payload = self.validPayload.copy()
        payload.pop("analysisType")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
