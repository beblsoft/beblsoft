#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Analysis Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class AnalysisTypeField(fields.String):
    """
    Analysis Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Analysis Type",
            description = "Analysis Type",
            example     = AnalysisType.COMPREHEND_SENTIMENT.name,
            enum        = [str(analysisType.name) for analysisType in list(AnalysisType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, AnalysisType):
            return value.name
        else:
            return None


class AnalysisStatusField(fields.String):
    """
    Analysis Status Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Analysis Status",
            description = "Analysis Status",
            example     = AnalysisStatus.IN_PROGRESS.name,
            enum        = [str(status.name) for status in list(AnalysisStatus)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, AnalysisStatus):
            return value.name
        else:
            return None


class CreateDateField(fields.DateTime):
    """
    Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Date",
            description = "Analysis Creation Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class ErrorSeenField(fields.Boolean):
    """
    Error Seen Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Error Seen",
            description = "If True, Error has been seen by the client",
            example     = False,
            required    = required,
            **kwargs)

class CountAnalyzedField(fields.Integer):
    """
    Count Analyzed Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count Analyzed",
            description = "Content count analyzed since the analysis began.",
            example     = 398,
            required    = required,
            **kwargs)

class CountTotalField(fields.Integer):
    """
    Count Total Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count Total",
            description = "Total content count being analyzed.",
            example     = 398,
            required    = required,
            **kwargs)

class CountDoneField(fields.Integer):
    """
    Count Done Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count Done",
            description = "Total content with analysis completed.",
            example     = 398,
            required    = required,
            **kwargs)

class CountNotDoneField(fields.Integer):
    """
    Count Not Done Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count Not Done",
            description = "Total content with analysis not performed.",
            example     = 398,
            required    = required,
            **kwargs)

class CountCantField(fields.Integer):
    """
    Count Cant Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Count Cant",
            description = "Total content that cannot be analyzed.",
            example     = 398,
            required    = required,
            **kwargs)
