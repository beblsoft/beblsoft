#!/usr/bin/env python3
"""
 NAME:
  progress.py

 DESCRIPTION
  Analysis Progress Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import current_app
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.analysis.models import analysisProgressModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.analysis import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ PROGRESS RESOURCE -------------------------------- #
@ns.route("/<int:smID>/progress")
@ns.doc(params={"smID": "Analysis Smeckn ID"})
class ProgressResource(Resource):
    """
    Progress Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(analysisProgressModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get analysis progress
        """
        mgr = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            analysisCtx = AnalysisContext(mgr=mgr, aDBS=aDBS)
            return mgr.profile.analysis.progress.get(analysisCtx=analysisCtx, analysisSMID=smID)
