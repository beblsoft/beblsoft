#!/usr/bin/env python3
"""
 NAME:
  count_test.py

 DESCRIPTION
  Test Analysis SMID Functionality
"""


# ------------------------- IMPORTS ----------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.wAPIV1.analysis.models import verifyAnalysisCountModel


# ------------------------- GLOBALS ----------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- COUNT GET TEST CASE ----------------------------- #
class CountTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, profileGroups=True, profiles=True)
        self.contentType         = ContentType.POST
        self.analysisType        = AnalysisType.COMPREHEND_SENTIMENT
        self.status              = AnalysisStatus.IN_PROGRESS
        self.contentCount        = 200
        self.profileManager      = self.mgr.profileStub
        self.analysisManager     = self.profileManager.getAnalysisManager(contentType=self.contentType,
                                                                          analysisType=self.analysisType)
        self.validArgs           = {
            "profileSMID": self.popProfileSMID,
            "contentType": self.contentType.name,
            "analysisType": self.analysisType.name,
        }
        self.baseURL             = self.routes.analysisCount
        self.validURL            = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    # --------------------- TEST FUNCTIONS ---------------------------------- #
    @logFunc()
    def test_success(self):
        """
        Test success case
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyAnalysisCountModel(aDBS=aDBS, mgr=self.mgr, apiModel=data, profileSMID=self.popProfileSMID,
                                     contentType=self.contentType, analysisType=self.analysisType)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test having no arg produces 400
        """
        argList = ["profileSMID",
                   "contentType",
                   "analysisType"]
        for arg in argList:
            args = self.validArgs.copy()
            args.pop(arg)
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test having bad arg produces out code
        """
        argBadCodeList = [("profileSMID", "Bad Argument", 400),
                          ("profileSMID", 23423, 404),
                          ("contentType", "Bad Argument", 400),
                          ("analysisType", "Bad Argument", 400)]
        for (arg, bad, outCode) in argBadCodeList:
            args = self.validArgs.copy()
            args[arg] = bad
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(self.validURL, methodFunc=self.getJSON)
