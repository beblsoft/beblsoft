#!/usr/bin/env python3
"""
 NAME:
  analysis.py

 DESCRIPTION
  Analysis Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from flask import request, current_app
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.analysis.models import analysisModel, analysisGetParser, analysisPostInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.analysis import ns
from smeckn.server.profile.analysis.context import AnalysisContext
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.analysis.dao import AnalysisDAO
from smeckn.server.aDB.analysis.model import AnalysisType, AnalysisStatus
from smeckn.server.aDB.profile.dao import ProfileDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ANALYSIS RESOURCE -------------------------------- #
@ns.route("")
class AnalysisResource(Resource):
    """
    Analysis Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(analysisGetParser)
    @ns.marshal_list_with(analysisModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get analyses
        """
        args        = analysisGetParser.parse_args()
        profileSMID = args.get("profileSMID")
        status      = AnalysisStatus[args.get("status")]

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return AnalysisDAO.getAll(aDBS=aDBS, profileSMID=profileSMID,
                                      status=status, orderCreateDateDesc=True)

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(analysisPostInputModel)
    @ns.marshal_with(analysisModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create and start analysis
        """
        inData       = request.json
        profileSMID  = inData.get("profileSMID")
        contentType  = ContentType[inData.get("contentType")]
        analysisType = AnalysisType[inData.get("analysisType")]
        mgr          = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            analysisCtx  = AnalysisContext(mgr=mgr, aDBS=aDBS)
            profileModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=profileSMID)  # Verify exists
            return mgr.profile.analysis.start(analysisCtx=analysisCtx, profileSMID=profileModel.smID,
                                              contentType=contentType, analysisType=analysisType)
