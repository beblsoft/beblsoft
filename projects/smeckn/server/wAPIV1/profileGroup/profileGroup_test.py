#!/usr/bin/env python3
"""
 NAME:
  profileGroup_test.py

 DESCRIPTION
  Test Profile Group Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.model import PG_MAX_COUNT
from smeckn.server.wAPIV1.profileGroup.models import verifyProfileGroupModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- PROFILE GROUP GET TEST CASE --------------------- #
class ProfileGroupGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)
        self.validURL = self.routes.profileGroup

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validMany(self):
        """
        Valid Test Returning Many
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyProfileGroupModel(aDBS=aDBS, apiModel=data[0], pgID=data[0].get("id"))
        self.assertEqual(len(data), len(self.pgNameList))

    @logFunc()
    def test_validNone(self):
        """
        Valid Test Returning None
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.nonPopAuthToken,
                                    accountToken=self.nonPopAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- PROFILE GROUP POST TEST CASE -------------------- #
class ProfileGroupPostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=False)
        self.validURL     = self.routes.profileGroup
        self.newPGName    = "Dennis"
        self.validPayload = {
            "name": self.newPGName,
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.nonPopAuthToken,
                                     accountToken=self.nonPopAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.nonPopAccountID, commit=False) as aDBS:
            verifyProfileGroupModel(aDBS=aDBS, apiModel=data, pgID=data.get("id"))

    @logFunc()
    def test_badName(self):
        """
        Test creation with malformed names
        """
        nameErrorList = [
            ("", 1500),
            ("01234567890123456789012345678901234567890123456789012345678901234", 1500),
            ("asdfkashsdfsadfd!", 1501),
            ("!", 1501),
            ("@", 1501),
            ("#", 1501),
            ("$", 1501),
            ("%", 1501),
            ("^", 1501),
            ("&", 1501),
            ("*", 1501),
            ("(", 1501),
            (")", 1501)
        ]
        for name,err in nameErrorList:
            payload      = {"name": name}
            (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.nonPopAuthToken,
                                         accountToken=self.nonPopAccountToken)
            self.assertEqual(code, 400)
            self.assertEqual(data.get("code"), err)

    @logFunc()
    def test_tooMany(self):
        """
        Test trying to create too many
        """
        for idx in range(0, PG_MAX_COUNT + 1):
            payload      = {"name": "johnny-{}".format(idx)}
            (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.nonPopAuthToken,
                                         accountToken=self.nonPopAccountToken)
            if idx == PG_MAX_COUNT:
                self.assertEqual(code, 400)
                self.assertEqual(data.get("code"), 1503)

    @logFunc()
    def test_duplicateName(self):
        """
        Test creating a duplicate name
        """
        for idx in range(0, 2):
            payload      = {"name": "johnny"}
            (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.nonPopAuthToken,
                                         accountToken=self.nonPopAccountToken)
            if idx == 2:
                self.assertEqual(code, 400)
                self.assertEqual(data.get("code"), 1503)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
