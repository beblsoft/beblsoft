#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Profile Group Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("profileGroup", description="Profile Group Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .profileGroup import *  #pylint: disable=C0413,W0401
from .id import * #pylint: disable=C0413,W0401
