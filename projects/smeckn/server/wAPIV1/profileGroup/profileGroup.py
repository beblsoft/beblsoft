#!/usr/bin/env python3
"""
 NAME:
  profileGroup.py

 DESCRIPTION
  Profile Group Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profileGroup.models import profileGroupPostInputModel, profileGroupModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.profileGroup import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ PROFILE GROUP RESOURCE --------------------------- #
@ns.route("")
class ProfileGroupResource(Resource):
    """
    Profile Group Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_list_with(profileGroupModel)
    @logFunc()
    def get(self): #pylint: disable=R0201
        """
        Get profile groups
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return ProfileGroupDAO.getAll(aDBS=aDBS)

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(profileGroupPostInputModel)
    @ns.marshal_with(profileGroupModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create profile group
        """
        inData = request.json
        name   = inData.get("name")

        # Create new profile group
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            with AccountDatabase.lockTables(session=aDBS, lockStr="ProfileGroup WRITE"):
                ProfileGroupDAO.create(aDBS=aDBS, name=name)
            return ProfileGroupDAO.getByName(aDBS=aDBS, name=name)
