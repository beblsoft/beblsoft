#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Profile Group Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class IDField(fields.Integer):
    """
    ID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "ID",
            description = "Profile Group Identification Number",
            example     = 1398,
            required    = required,
            **kwargs)


class NameField(fields.String):
    """
    Name Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Name",
            description = "Profile Group Name",
            example     = "Susan",
            required    = required,
            **kwargs)
