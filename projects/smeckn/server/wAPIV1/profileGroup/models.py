#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Profile Group Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profileGroup.fields import IDField, NameField
from smeckn.server.wAPIV1.profileGroup import ns
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO


# ------------------------ MODELS ------------------------------------------- #
profileGroupModel = ns.model("ProfileGroupModel", {
    "id": IDField(),
    "name": NameField()
})


profileGroupPostInputModel = ns.model("ProfileGroupPostInputModel", {
    "name": NameField(required=True)
})

profileGroupPutInputModel = ns.model("ProfileGroupPostInputModel", {
    "name": NameField(required=True)
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyProfileGroupModel(aDBS, apiModel, pgID):
    """
    Verify profile group model
    """
    pgModel = ProfileGroupDAO.getByID(aDBS=aDBS, pgID=pgID)
    assert apiModel.get("id") == pgModel.id
    assert apiModel.get("name") == pgModel.name
