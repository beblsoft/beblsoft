#!/usr/bin/env python3
"""
 NAME:
  id.py

 DESCRIPTION
  Profile Group ID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profileGroup.models import profileGroupPutInputModel, profileGroupModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.profileGroup import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ID RESOURCE -------------------------------------- #
@ns.route("/<int:id>")
@ns.doc(params={"id": "Profile Group ID"})
class IDResource(Resource):
    """
    ID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(profileGroupModel)
    @logFunc()
    def get(self, id):  # pylint: disable=R0201,W0622
        """
        Get profile group
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return ProfileGroupDAO.getByID(aDBS=aDBS, pgID=id)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(profileGroupPutInputModel)
    @ns.marshal_with(profileGroupModel)
    def put(self, id):  # pylint: disable=R0201,W0622
        """
        Update profile group
        """
        inData = request.json
        name   = inData.get("name")

        # Update profile group
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            ProfileGroupDAO.updateNameByID(aDBS=aDBS, pgID=id, newName=name)
            aDBS.commit()
            return ProfileGroupDAO.getByName(aDBS=aDBS, name=name)


    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @logFunc()
    def delete(self, id):  # pylint: disable=R0201,W0622
        """
        Delete profile group
        """
        with AccountDatabase.sessionScopeFromFlask() as aDBS:
            ProfileGroupDAO.deleteByID(aDBS=aDBS, pgID=id)
