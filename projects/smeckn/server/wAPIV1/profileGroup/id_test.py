#!/usr/bin/env python3
"""
 NAME:
  id_test.py

 DESCRIPTION
  Test ID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profileGroup.dao import ProfileGroupDAO
from smeckn.server.wAPIV1.profileGroup.models import verifyProfileGroupModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- ID GET TEST CASE ----------------------------- #
class IDGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)
        self.validURL = "{}/{}".format(self.routes.profileGroup, self.popPGID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyProfileGroupModel(aDBS=aDBS, apiModel=data, pgID=data.get("id"))

    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url          = "{}/{}".format(self.routes.profileGroup, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1504)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ---------------------------- ID PUT TEST CASE ----------------------------- #
class IDPutTestCase(CommonTestCase):
    """
    Put Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)
        self.validURL = "{}/{}".format(self.routes.profileGroup, self.popPGID)
        self.validPayload = {
            "name": "JeffreyK"
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test changing name
        """
        (code, data) = self.putJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyProfileGroupModel(aDBS=aDBS, apiModel=data, pgID=data.get("id"))

    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url          = "{}/{}".format(self.routes.profileGroup, 1293842398)
        (code, data) = self.putJSON(url, d=self.validPayload,
                                    authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1504)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.putJSON, d=self.validPayload)


# ---------------------------- ID DELETE TEST CASE -------------------------- #
class IDDeleteTestCase(CommonTestCase):
    """
    Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)
        self.validURL = "{}/{}".format(self.routes.profileGroup, self.popPGID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test changing name
        """
        (code, _) = self.deleteJSON(self.validURL,
                                    authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID,
                                                 write=False) as aDBS:
            pgModel = ProfileGroupDAO.getByID(aDBS=aDBS, pgID=self.popPGID, raiseOnNone=False)
            self.assertEqual(pgModel, None)

    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url       = "{}/{}".format(self.routes.profileGroup, 1293842398)
        (code, _) = self.deleteJSON(url, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.deleteJSON)
