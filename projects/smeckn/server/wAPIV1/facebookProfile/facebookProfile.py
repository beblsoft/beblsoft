#!/usr/bin/env python3
"""
 NAME:
  facebookProfile.py

 DESCRIPTION
  Facebook Profile Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request, current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.facebookProfile.models import facebookProfileModel, facebookProfilePostInputModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.facebookProfile import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ FACEBOOK PROFILE RESOURCE ------------------------ #
@ns.route("")
class FacebookProfileResource(Resource):
    """
    Facebook Profile Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(facebookProfilePostInputModel)
    @ns.marshal_with(facebookProfileModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create facebook profile
        """
        inData               = request.json
        pgID                 = inData.get("profileGroupID")
        fbID                 = inData.get("fbID")
        shortTermAccessToken = inData.get("shortTermAccessToken")
        mgr                  = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            fbpModel = mgr.facebook.create(aDBS=aDBS, pgID=pgID, fbpID=fbID,
                                           shortTermAccessToken=shortTermAccessToken)
            return fbpModel
