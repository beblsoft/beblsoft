#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Facebook Profile Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('facebookProfile', description="Facebook Profile Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .facebookProfile import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
