#!/usr/bin/env python3
"""
 NAME:
  smID_test.py

 DESCRIPTION
  Test smID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.facebookProfile.models import verifyFacebookProfileModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- SMID GET TEST CASE --------------------------- #
class SMIDGetTestCase(CommonTestCase):
    """
    SMID Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookProfiles=True)
        self.validURL = "{}/{}".format(self.routes.facebookProfile, self.popFBPSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyFacebookProfileModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badSMID(self):
        """
        Test with a bad SMID
        """
        url          = "{}/{}".format(self.routes.facebookProfile, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ---------------------------- SMID PUT TEST CASE --------------------------- #
class SMIDPutTestCase(CommonTestCase):
    """
    Put Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, facebookTestUsers=True,
                      facebookTestUserProfiles=True)
        self.validURL       = "{}/{}".format(self.routes.facebookProfile, self.popFBPSMID)
        self.fbTestUserName = "small"
        self.validPayload   = {
            "shortTermAccessToken": self.fbTestUserDict[self.fbTestUserName].accessToken
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validUpdateShortTermAccessToken(self):
        """
        Valid updateing short term access token
        """
        (code, data) = self.putJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyFacebookProfileModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))


    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url          = "{}/{}".format(self.routes.facebookProfile, 1293842398)
        (code, data) = self.putJSON(url, d=self.validPayload, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_badShortTermAccessToken(self):
        """
        Test with a bad short term access token
        """
        payload                         = self.validPayload.copy()
        payload["shortTermAccessToken"] = "BlahBlahBlah"
        (code, data)                    = self.putJSON(self.validURL, d=payload,
                                                       authToken=self.popAuthToken,
                                                       accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1712)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.putJSON, d=self.validPayload)
