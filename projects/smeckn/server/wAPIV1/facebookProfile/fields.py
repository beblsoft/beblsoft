#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Facebook Profile Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class FBIDField(fields.String):
    """
    FB ID Field

    Note: this is not a string because certain client languages like JavaScript
    don't have native support 64-bit numbers
    """

    def __init__(self, required=True, **kwargs):

        super().__init__(
            title       = "Facebook ID",
            description = "Facebook Profile Identification Number",
            example     = "39823948732498",
            required    = required,
            **kwargs)

    def parse(self, value):  # pylint: disable=R0201
        """
        Parse string into integer
        """
        rval = None
        try:
            rval = int(value)
        except Exception as _:
            raise ValueError('Unsupported integer format')
        return rval


class NameField(fields.String):
    """
    Name Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Name",
            description = "Facebook Profile Name",
            example     = "Mara Bensson",
            required    = required,
            **kwargs)


class EmailField(fields.String):
    """
    Email Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Email",
            description = "Facebook Profile Email",
            example     = "johnwilliams@gmail.com",
            required    = required,
            **kwargs)


class GenderField(fields.String):
    """
    Gender Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Gender",
            description = "Facebook Profile Gender",
            example     = "male",
            required    = required,
            **kwargs)


class BirthdayDateField(fields.DateTime):
    """
    Birthday Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Birthday Date",
            description = "Facebook Profile Birthday Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)


class HometownField(fields.String):
    """
    Hometown Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Hometown",
            description = "Facebook Profile Hometown",
            example     = "Attleboro, MA",
            required    = required,
            **kwargs)


class AccessTokenField(fields.String):
    """
    Access Token Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Facebook Profile Access Token",
            description = "Facebook Profile Access Token",
            example     = "EAAbXqc4Yv3IBAFnn0WX0bkNL6AXSjpxRiyl6JwZBcb2Foj81RDwhWJnWE2ecWIKhZAPJuleC0SdSFhDfldUgvpx843FTCF92dbjDKwEwmZCDxSmRJrml8ZCsRHbFazKQK6rNm1c9vaH4l0hOlwBWvLHzhLq3JB5ntHRkfy39pFB7vE00BR4v",
            required    = required,
            **kwargs)
