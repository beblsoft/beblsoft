#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Facebook Profile ID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request, current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.facebookProfile.models import facebookProfileModel, facebookProfilePutInputModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.facebookProfile import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Facebook Profile Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(facebookProfileModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201,W0622
        """
        Get facebook profile
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=smID)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(facebookProfilePutInputModel)
    @ns.marshal_with(facebookProfileModel)
    def put(self, smID):  # pylint: disable=R0201,W0622
        """
        Update facebook profile
        """
        inData               = request.json
        shortTermAccessToken = inData.get("shortTermAccessToken", None)
        mgr                  = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            fbpModel = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=smID)

            if shortTermAccessToken:
                fbpModel = mgr.facebook.updateAccessToken(aDBS=aDBS, fbpModel=fbpModel,
                                                          shortTermAccessToken=shortTermAccessToken)
            return fbpModel
