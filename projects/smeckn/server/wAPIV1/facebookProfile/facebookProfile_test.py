#!/usr/bin/env python3
"""
 NAME:
  facebookProfile_test.py

 DESCRIPTION
  Test Facebook Profile Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.facebookProfile.models import verifyFacebookProfileModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- FACEBOOK PROFILE POST TEST CASE ----------------- #
class FacebookProfilePostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True)
        self.validURL       = self.routes.facebookProfile
        self.badPayload     = {
            "profileGroupID": self.popPGID,
            "fbID": str(self.cfg.FACEBOOK_SMECKN_TESTUSER_SMALL),
            "shortTermAccessToken": "InvalidAccessToken"
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        super().setUp(facebookTestUsers=True)
        fbTestUserName = "small"
        validPayload = {
            "profileGroupID": self.popPGID,
            "fbID": str(self.fbTestUserDict[fbTestUserName].id),
            "shortTermAccessToken": self.fbTestUserDict[fbTestUserName].accessToken
        }
        (code, data) = self.postJSON(self.validURL, d=validPayload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyFacebookProfileModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badPGID(self):
        """
        Test bad profile group ID
        """
        payload                   = self.badPayload.copy()
        payload["profileGroupID"] = 799
        (code, _)                 = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 404)

    @logFunc()
    def test_noPGID(self):
        """
        Test no profile group ID
        """
        payload = self.badPayload.copy()
        payload.pop("profileGroupID")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noFBID(self):
        """
        Test no FBID
        """
        payload   = self.badPayload.copy()
        payload.pop("fbID")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badFBID(self):
        """
        Test bad FBID
        """
        payload   = self.badPayload.copy()
        payload["fbID"] = int(payload["fbID"])
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badShortTermAccessToken(self):
        """
        Test bad short term access token
        """
        payload         = self.badPayload.copy()
        payload["shortTermAccessToken"] = "InvalidToken"
        (code, data)    = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                        accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1712)

    @logFunc()
    def test_noShortTermAccessToken(self):
        """
        Test no short term access token
        """
        payload   = self.badPayload.copy()
        payload.pop("shortTermAccessToken")
        (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                  accountToken=self.popAccountToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.badPayload)
