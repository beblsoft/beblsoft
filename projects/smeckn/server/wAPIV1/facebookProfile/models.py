#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Facebook Profile Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.fields import SMIDField
from smeckn.server.wAPIV1.facebookProfile.fields import FBIDField, NameField, EmailField, GenderField
from smeckn.server.wAPIV1.facebookProfile.fields import BirthdayDateField, HometownField, AccessTokenField
from smeckn.server.wAPIV1.profileGroup.fields import IDField as ProfileGroupIDField
from smeckn.server.wAPIV1.facebookProfile import ns
from smeckn.server.aDB.facebookProfile.dao import FacebookProfileDAO


# ------------------------ MODELS ------------------------------------------- #
facebookProfileModel = ns.model("FacebookProfileModel", {
    "smID": SMIDField(),
    "profileGroupID": ProfileGroupIDField(),
    "fbID": FBIDField(),
    "name": NameField(),
    "email": EmailField(),
    "gender": GenderField(),
    "birthdayDate": BirthdayDateField(),
    "hometown": HometownField(),
})

facebookProfilePostInputModel = ns.model("FacebookProfilePostInputModel", {
    "profileGroupID": ProfileGroupIDField(),
    "fbID": FBIDField(),
    "shortTermAccessToken": AccessTokenField()
})

facebookProfilePutInputModel = ns.model("FacebookProfilePutInputModel", {
    "shortTermAccessToken": AccessTokenField(required=False)
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyFacebookProfileModel(aDBS, apiModel, smID):
    """
    Verify facebook profile model
    """
    fbpModel = FacebookProfileDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == fbpModel.smID
    assert apiModel.get("profileGroupID") == fbpModel.profileGroupID
    assert apiModel.get("fbID") == str(fbpModel.fbID)
    assert apiModel.get("name") == fbpModel.name
    assert apiModel.get("email") == fbpModel.email
    assert apiModel.get("gender") == fbpModel.gender
    assert apiModel.get("hometown") == fbpModel.hometown
