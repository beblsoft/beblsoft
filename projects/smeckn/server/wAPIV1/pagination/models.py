#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Pagination Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.server.wAPIV1.pagination.fields import NextCursorField
from smeckn.server.wAPIV1.default import ns


# ------------------------ PARSERS ------------------------------------------ #
paginationParser = ns.parser()
paginationParser.add_argument("cursor",
                              type        = str,
                              required    = False,
                              default     = None,
                              location    = "args")
paginationParser.add_argument("limit",
                              type        = int,
                              required    = False,
                              default     = 80,
                              choices     = list(range(1, 101)),
                              location    = "args")


# ------------------------ MODELS ------------------------------------------- #
paginationModel = ns.model(
    "PaginationModel",
    {
        "nextCursor": NextCursorField()
        # "data": List overridden by subclass
    }
)
