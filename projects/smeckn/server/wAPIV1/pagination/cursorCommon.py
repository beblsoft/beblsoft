#!/usr/bin/env python3
"""
NAME
 cursorCommon.py

DESCRIPTION
 Common pagination cursor functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import json
import base64
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------- COMMON PAGINATION CURSOR ------------------------ #
class CommonPaginationCursor():
    """
    Common Pagination Cursor. Represents a base64 encoded value.

    Encoding:
      Class Dictionary    -> JSON encoded string
      JSON encoded string -> Bytes
      Bytes               -> Base64 string

    Decoding:
      Base64 string       -> Bytes
      Bytes               -> JSON encoded string
      JSON encoded string -> Class Constructor
    """

    # --------------------- ALTERNATE CONSTRUCTORS -------------------------- #
    @classmethod
    @logFunc()
    def fromParserArgs(cls, parserArgs):
        """
        Return cursor from parser args
        """
        cursorB64Str = parserArgs.get("cursor", None)
        cursor       = None
        if cursorB64Str:
            cursor = cls.fromString(b64String=cursorB64Str)
        return cursor

    @classmethod
    @logFunc()
    def fromString(cls, b64String):
        """
        Return Cursor from Base64 encoded string
        """
        try:
            stringBytes = base64.b64decode(b64String).decode("utf-8")
            metaDict    = json.loads(stringBytes)
            cursor      = cls(**metaDict)
            return cursor
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="Cursor={}".format(b64String))

    # --------------------- TO STRING --------------------------------------- #
    @logFunc()
    def toString(self):
        """
        Return Base64 encoded string
        """
        string      = json.dumps(self.__dict__)
        stringBytes = bytes(string, "utf-8")
        b64String   = base64.b64encode(stringBytes).decode("utf-8")
        return b64String
