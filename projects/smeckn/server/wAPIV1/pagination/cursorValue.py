#!/usr/bin/env python3
"""
NAME
 cursorCommon.py

DESCRIPTION
 Common pagination cursor functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.wAPIV1.pagination.cursorCommon import CommonPaginationCursor


# ------------------------- VALUE PAGINATION CURSOR ------------------------- #
class ValuePaginationCursor(CommonPaginationCursor):
    """
    Value Pagination Cursor

    Creating:
      nextCursor = ValuePaginationCursor(val=fooStr).toString()
      return {
        "nextCursor": nextCursor,
        "data": [thing1, thing2, thing3, ...]
      }

    Retrieving:
      args   = fooGetParser.parse_args()
      fooStr = ValuePaginationCursor.castedValFromParserArgs(args, str)
    """

    def __init__(self, val=None):
        """
        Initialize Object
        """
        self.val = val

    # --------------------- VALUE CASTING ----------------------------------- #
    @classmethod
    @logFunc()
    def castedValFromParserArgs(cls, parserArgs, castFunc):
        """
        Return casted value from parser args

        Returns
          Casted value or None
        """
        cursor    = cls.fromParserArgs(parserArgs)
        castedVal = cursor.castVal(castFunc) if cursor else None
        return castedVal

    @logFunc()
    def castVal(self, castFunc):
        """
        Cast value
        Args
          castFunc:
            Function to cast value
            Ex. str, int, json.loads

        Returns
          Casted val or None if val is None
        """
        rval = None
        try:
            if self.val is not None:
                rval = castFunc(self.val)
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="Cursor={}".format(self.val))
        return rval
