#!/usr/bin/env python3
"""
NAME
 sortCursor.py

DESCRIPTION
 Pagination sort cursor functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.wAPIV1.pagination.cursorCommon import CommonPaginationCursor


# ------------------------- SORT PAGINATION CURSOR -------------------------- #
class SortPaginationCursor(CommonPaginationCursor):
    """
    Sort Pagination Cursor

    Used with SortType and SortOrder classes
    Stores and encodes primary and secondary last values

    Creating:
      nextCursor = SortPaginationCursor(pLastValue=1, sLastValue=2).toString()
      return {
        "nextCursor": nextCursor,
        "data": [thing1, thing2, thing3, ...]
      }

    Retrieving:
      args       = fooGetParser.parse_args()
      cursor     = SortPaginationCursor.fromParserArgs(args)
      pLastValue = cursor.castPLastValue(castFunc=int)
      sLastValue = cursor.castSLastValue(castFunc=int)
    """

    def __init__(self, pLastValue=None, sLastValue=None, **kwargs):
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.pLastValue = pLastValue
        self.sLastValue = sLastValue

    # --------------------- VALUE CASTING ----------------------------------- #
    @logFunc()
    def castPLastValue(self, castFunc):
        """
        Cast pLastValue
        Args
          castFunc:
            Function to cast value
            Ex. str, int, json.loads

        Returns
          Casted pLastValue or None if pLastValue is None
        """
        rval = None
        try:
            if self.pLastValue is not None:
                rval = castFunc(self.pLastValue)
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="pLastValue={}".format(self.pLastValue))
        return rval

    @logFunc()
    def castSLastValue(self, castFunc):
        """
        Cast sLastValue
        Args
          castFunc:
            Function to cast value
            Ex. str, int, json.loads

        Returns
          Casted sLastValue or None if sLastValue is None
        """
        rval = None
        try:
            if self.sLastValue is not None:
                rval = castFunc(self.sLastValue)
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="sLastValue={}".format(self.sLastValue))
        return rval
