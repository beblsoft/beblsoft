#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Pagination Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class NextCursorField(fields.String):
    """
    Next Cursor Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Next opaque cursor string",
            description = ("Next opaque cursor string to pass on susequent pagination request. "
                           "If '', no more next"),
            example     = "",
            required    = required,
            ** kwargs)
