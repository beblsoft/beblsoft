#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Comp Lang Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.compLang.fields import SMIDField, ScoreField
from smeckn.server.wAPIV1.common.fields import BLanguageTypeField
from smeckn.server.wAPIV1.default import ns
from smeckn.server.aDB.compLang.dao import CompLangDAO


# ------------------------ MODELS ------------------------------------------- #
compLangModel = ns.model("CompLangModel", {
    "smID": SMIDField(),
    "bLanguageType": BLanguageTypeField(),
    "score": ScoreField(),
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyCompLangModel(aDBS, apiModel, smID):
    """
    Verify comp lang model
    """
    _compLangModel = CompLangDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _compLangModel.smID
    assert apiModel.get("bLanguageType") == _compLangModel.bLanguageType.name
    assert apiModel.get("score") == _compLangModel.score
