#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Comprehend Language Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class ScoreField(fields.Float):
    """
    Score Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Score",
            description = "Score",
            example     = .2133,
            required    = required,
            **kwargs)
