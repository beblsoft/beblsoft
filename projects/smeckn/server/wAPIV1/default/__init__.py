#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Default Routes

 NOTE
   Namespace is used to register objects that do not have associated routes
   For example decorators
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('default', description="Default Operations")
