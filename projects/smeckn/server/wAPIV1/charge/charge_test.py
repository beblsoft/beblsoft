#!/usr/bin/env python3
"""
 NAME:
  charge_test.py

 DESCRIPTION
  Test Charge Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO
from smeckn.server.test.common import CommonTestCase
from smeckn.server.payments.unit.models import UnitCountModel
from smeckn.server.wAPIV1.charge.models import verifyChargeModel



# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- CHARGE GET TEST CASE ---------------------------- #
class ChargeGetTestCase(CommonTestCase):
    """
    Charge Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = self.routes.charge

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_badLimitValues(self):
        """
        Test with bad limit values
        """
        for limit in [-1, 0, 101]:
            urlArgs   = urllib.parse.urlencode({"limit": limit})
            url       = "{}?{}".format(self.validURL, urlArgs)
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badCursorValues(self):
        """
        Test with bad cursor values
        """
        for cursor in [234, "I am a bad cursor"]:
            urlArgs   = urllib.parse.urlencode({"cursor": cursor})
            url       = "{}?{}".format(self.validURL, urlArgs)
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_validNoPaginationValues(self):
        """
        Test valid with no pagination values
        """
        super().setUp(stripeCustomers=True, stripeCards=True, stripeCharges=True)
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        for apiModel in data.get("data"):
            verifyChargeModel(mgr=self.mgr, apiModel=apiModel)

    @logFunc()
    def test_validNoCharges(self):
        """
        Test with no charges
        """
        super().setUp(stripeCustomers=True)
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("nextCursor"), "")
        self.assertEqual(len(data.get("data")), 0)

    @logFunc()
    def test_validPagination(self):
        """
        Test valid pagination case
        """
        limit        = 2
        nextCursor   = None
        keepGoing    = True
        totalCharges = 0

        super().setUp(stripeCustomers=True, stripeCards=True, stripeCharges=True)
        while keepGoing:
            urlArgs                   = {}
            if nextCursor:
                urlArgs["cursor"]     = nextCursor
            urlArgs["limit"]          = limit
            urlArgs                   = urllib.parse.urlencode(urlArgs)
            url                       = "{}?{}".format(self.validURL, urlArgs)
            (code, data)              = self.getJSON(url, authToken=self.popAuthToken,
                                                     accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            nextCursor                = data.get("nextCursor")
            nCharges                  = len(data.get("data"))
            totalCharges             += nCharges
            if nextCursor:
                self.assertEqual(nCharges, limit)
            else:
                keepGoing             = False
        self.assertEqual(totalCharges, len(self.stripeChargeModelList))

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- CHARGE POST TEST CASE --------------------------- #
class ChargePostTestCase(CommonTestCase):
    """
    Charge Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True, stripeCustomers=True, stripeCards=True, stripeCharges=True)
        self.validURL           = self.routes.charge
        self.count              = 500
        self.unitCountModelList = UnitCountModel.getFakeList(count=self.count)
        self.bCurrencyCode      = BeblsoftCurrencyCode.USD
        (cartModel, cartJWT)    = self.mgr.payments.cart.createCart(bCurrencyCode=self.bCurrencyCode,
                                                                    unitCountModelList=self.unitCountModelList)
        self.cartModel          = cartModel
        self.cartJWT            = cartJWT
        self.validPayload       = {
            "cartJWT": self.cartJWT,
            "stripeCardID": self.popStripeCardID
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_badCartJWT(self):
        """
        Test with bad cart jwt
        """
        for invalidCartJWT in ["blah", "2349087sdaf098sdf"]:
            payload = self.validPayload.copy()
            payload["cartJWT"] = invalidCartJWT
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                      accountToken=self.popAccountToken)
            self.assertEqual(code, 401)

    def test_badCardID(self):
        """
        Test with bad card id
        """
        for invalidCardID in ["bad_card", "card_asdflakjsdflkj", ""]:
            payload = self.validPayload.copy()
            payload["stripeCardID"] = invalidCardID
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                      accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    def test_valid(self):
        """
        Test valid
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        verifyChargeModel(mgr=self.mgr, apiModel=data)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            for unitCountModel in self.unitCountModelList:
                unitType         = unitCountModel.unitType
                unitBalanceModel = UnitLedgerDAO.getUnitBalance(aDBS, unitType=unitType)
                self.assertEqual(unitBalanceModel.nTotal,
                                 self.unitTypeDict[unitType]["nInitialTotal"] + unitCountModel.count)

    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
