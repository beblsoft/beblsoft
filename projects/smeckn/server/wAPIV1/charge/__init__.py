#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Charge Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("charge", description="Charge Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .charge import *  #pylint: disable=C0413,W0401
