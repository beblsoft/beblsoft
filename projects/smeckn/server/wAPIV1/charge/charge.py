#!/usr/bin/env python3
"""
 NAME:
  charge.py

 DESCRIPTION
  Charge Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request, current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.charge.models import chargeGetParser, chargePostInputModel, chargeModel, chargePaginationModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.pagination.cursorValue import ValuePaginationCursor
from smeckn.server.wAPIV1.charge import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CHARGE RESOURCE ---------------------------------- #
@ns.route("")
class ChargeResource(Resource):
    """
    Charge Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(chargeGetParser)
    @ns.marshal_with(chargePaginationModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get charges
        """
        args             = chargeGetParser.parse_args()
        startingAfterID  = ValuePaginationCursor.castedValFromParserArgs(args, str)
        limit            = args.get("limit")
        nextCursor       = ""
        mgr              = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel              = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            (stripeChargeModelList, hasMore) = mgr.payments.charge.getList(stripeCustomerID=stripeCustomerModel.id,
                                                                           startingAfterID=startingAfterID,
                                                                           limit=limit)
            if hasMore:
                lastStripeChargeID           = stripeChargeModelList[-1].id if stripeChargeModelList else None
                nextCursor                   = ValuePaginationCursor(val=lastStripeChargeID).toString()

            return {
                "nextCursor": nextCursor,
                "data": stripeChargeModelList
            }

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(chargePostInputModel)
    @ns.marshal_with(chargeModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create charge
        """
        inData       = request.json
        cartJWT      = inData.get("cartJWT")
        stripeCardID = inData.get("stripeCardID")
        mgr          = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            stripeChargeModel   = mgr.payments.charge.create(aDBS=aDBS, cartJWT=cartJWT,
                                                             stripeCustomerID=stripeCustomerModel.id,
                                                             stripeCardID=stripeCardID)
            return stripeChargeModel
