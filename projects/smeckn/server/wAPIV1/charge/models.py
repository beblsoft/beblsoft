#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Charge Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.charge.dao import StripeChargeDAO
from smeckn.server.wAPIV1.common.fields import BCurrencyCodeField
from smeckn.server.wAPIV1.creditCard.fields import StripeCardIDField
from smeckn.server.wAPIV1.cart.fields import CostAmountField, CartJWTField
from smeckn.server.wAPIV1.charge.fields import StripeChargeIDField, CreationDateField, DescriptionField
from smeckn.server.wAPIV1.pagination.models import paginationModel, paginationParser
from smeckn.server.wAPIV1.charge import ns


# ------------------------ PARSERS ------------------------------------------ #
chargeGetParser = paginationParser.copy()


# ------------------------ MODELS ------------------------------------------- #
chargePostInputModel = ns.model("ChargePostInputModel", {
    "cartJWT": CartJWTField(),
    "stripeCardID": StripeCardIDField(),
})

chargeModel = ns.model("ChargeModel", {
    "id": StripeChargeIDField(),
    "createdDate": CreationDateField(),
    "description": DescriptionField(),
    "bCurrencyCode": BCurrencyCodeField(),
    "amount": CostAmountField()
})

chargePaginationModel = ns.inherit("ChargePaginationModel", paginationModel, {
    "data": fields.List(fields.Nested(chargeModel))
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyChargeModel(mgr, apiModel):
    """
    Verify charge model
    """
    stripeChargeModel = StripeChargeDAO.getByID(bStripe=mgr.payments.bStripe,
                                                stripeChargeID=apiModel.get("id"))
    assert stripeChargeModel.id == apiModel.get("id")
    assert len(apiModel.get("createdDate")) >= 0
    assert stripeChargeModel.description == apiModel.get("description")
    assert stripeChargeModel.bCurrencyCode.name == apiModel.get("bCurrencyCode")
    assert stripeChargeModel.amount == apiModel.get("amount")
