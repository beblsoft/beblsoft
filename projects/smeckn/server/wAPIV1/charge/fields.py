#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Charge Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class StripeChargeIDField(fields.String):
    """
    Stripe Charge ID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Stripe Charge ID",
            description = "Stripe Charge ID",
            example     = "ch_1Db9dpECEJ911IPj8vKWXCMv",
            required    = required,
            ** kwargs)

class CreationDateField(fields.DateTime):
    """
    Creation Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Creation Date",
            description = "Charge Creation Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class DescriptionField(fields.String):
    """
    Description ID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Charge Description",
            description = "Charge Description",
            example     = "1000 Text Analysis Units, 500 Photo Analysis Units",
            required    = required,
            ** kwargs)
