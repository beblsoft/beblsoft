#!/usr/bin/env python3
"""
 NAME:
  error.py

 DESCRIPTION
  Error Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.error import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ERROR RESOURCE ----------------------------------- #
@ns.route("")
class ErrorResource(Resource):
    """
    Error Routes
    """
    @requestWrapper()
    @ns.doc(security=None)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Trigger exception
        """
        raise BeblsoftError(code=BeblsoftErrorCode.TEST_ERROR)
