#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Error Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('error', description="Error Namespace")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .error import *  #pylint: disable=C0413,W0401
