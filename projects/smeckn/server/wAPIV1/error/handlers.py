#!/usr/bin/env python3
"""
 NAME:
  errors.py

 DESCRIPTION
  Error Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import traceback
from flask import current_app
from flask_restplus import fields
import werkzeug.exceptions
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.log.sLogger import SmecknLogger


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ REGISTER HANDLERS -------------------------------- #
def registerErrorHandlers(api):
    """
    Register error handlers on the api
    """

    # -------------------- MODEL -------------------------------------------- #
    errorModel = api.model("ErrorModel", {
        "code": fields.Integer(
            title       = "Server Error Code",
            example     = 1140),
        "message": fields.String(
            title       = "Server Error Message",
            example     = "No token attached to request object"),
        "stack": fields.String(
            title       = "Server Error Message",
            example     = "No token attached to request object"),
    })

    # -------------------- HELPERS ------------------------------------------ #
    @logFunc()
    def __handleBeblsoftError(bError):
        """
        Internal. Handle beblsoft error and convert it to a flask result.
        """
        mgr        = current_app.mgr
        cfg        = mgr.cfg
        httpStatus = bError.httpStatus
        data       = {
            "code": bError.code.value,
            "message": bError.extMsg,
            "stack": traceback.format_exc() if cfg.APP_ERROR_STACKS else ""
        }

        # Cases to handle
        # ------------------
        # 1. Normal Case: Log very little information
        # 2. Abnormal Case: Something baaaad happened, report to Sentry
        if 200 <= httpStatus < 500:
            SmecknLogger.info(msg="API Error:{}".format(bError.code),
                              bLogCode=BeblsoftLogCode.API_ERROR, bError=bError)
        elif bError.originalError is not None:
            mgr.sentry.captureError(error=bError.originalError, bLogCode=BeblsoftLogCode.API_ERROR,
                                    logMsg="API Error:{}".format(bError.code), isBError=False)
        else:
            mgr.sentry.captureError(error=bError, bLogCode=BeblsoftLogCode.API_ERROR,
                                    logMsg="API Error:{}".format(bError.code), isBError=True)

        return data, httpStatus

    # -------------------- ERROR HANDLERS ----------------------------------- #
    @api.errorhandler(BeblsoftError)
    @api.marshal_with(errorModel)
    @logFunc()
    def handleBeblsoftError(bError):  # pylint: disable=W0612
        """
        Handle BeblsoftError
        """
        return __handleBeblsoftError(bError=bError)

    @api.errorhandler(werkzeug.exceptions.BadRequest)
    @logFunc()
    def handleBadRequestException(error):  # pylint: disable=W0612
        """
        Handle BadRequest

        Reference:
          https://werkzeug.palletsprojects.com/en/0.15.x/exceptions/#werkzeug.exceptions.BadRequest
        """
        bError = BeblsoftError(code=BeblsoftErrorCode.GEN_JSON_VALIDATION_ERROR,
                               extMsg=error.description, originalError=error)
        return __handleBeblsoftError(bError=bError)

    @api.errorhandler(werkzeug.exceptions.MethodNotAllowed)
    @logFunc()
    def handleMethodNotAllowedException(error):  # pylint: disable=W0612
        """
        Handle MethodNotAllowed

        Reference:
          https://werkzeug.palletsprojects.com/en/0.16.x/exceptions/#werkzeug.exceptions.MethodNotAllowed
        """
        bError = BeblsoftError(code=BeblsoftErrorCode.GEN_REQUEST_METHOD_NOT_ALLOWED,
                               originalError=error)
        return __handleBeblsoftError(bError=bError)

    @api.errorhandler(Exception)
    @logFunc()
    def handleException(error):  # pylint: disable=W0612
        """
        Handle generic exception
        """
        bError = BeblsoftError(code=BeblsoftErrorCode.UNKNOWN_ERROR, originalError=error)
        return __handleBeblsoftError(bError=bError)
