#!/usr/bin/env python3
"""
 NAME:
  error_test.py

 DESCRIPTION
  Test Error Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class ErrorGetTestCase(CommonTestCase):
    """
    Error Post Test Case
    """
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, data) = self.getJSON(self.routes.error)
        self.assertEqual(code, 500)
        self.assertEqual(data.get("code"), 2)


class ErrorHTTPStatusTestCase(CommonTestCase):
    """
    Error HTTP Status Test Case
    """
    @logFunc()
    def test_valid(self):
        """
        Valid Test

        Note:
          Putting 5xx in httpStatusList triggers Flask exceptions and sends
          errors to Sentry
        """

        httpStatusList = [200, 400, 401, 402, 403, 404, 405, 429, 500, 501, 502, 503, 504]
        for httpStatus in httpStatusList:
            headers = {"API-ERROR-HTTP-STATUS": httpStatus}
            (code, data) = self.getJSON(self.routes.profileGroup, headers=headers)
            self.assertEqual(data.get("code"), 1)
            self.assertEqual(code, httpStatus)


class ErrorCodeTestCase(CommonTestCase):
    """
    Error Code Test Case

    Note:
      Putting errorCodes that correspond to 5xx httpStatuses in errorCodeList
      triggers Flask exceptions and sends errors to Sentry
    """
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        errorCodeList = [1, 1000, 1001]

        for errorCode in errorCodeList:
            headers = {"API-ERROR-CODE": errorCode}
            (_, data) = self.getJSON(self.routes.profileGroup, headers=headers)
            self.assertEqual(data.get("code"), errorCode)
