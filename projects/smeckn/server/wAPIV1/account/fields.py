#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Account Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.gDB.account.model import AccountType


# ------------------------ FIELDS ------------------------------------------- #
class IDField(fields.Integer):
    """
    ID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "ID",
            description = "Account Identification Number",
            example     = 1398,
            required    = required,
            **kwargs)


class TypeField(fields.String):
    """
    Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Type",
            description = "Account Type",
            example     = AccountType.REGULAR.name,
            enum        = [str(name) for name, _ in AccountType.__members__.items()],
            default     = AccountType.TEST.name,
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, AccountType):
            return value.name
        else:
            return None


class ActiveField(fields.Boolean):
    """
    Active Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Active",
            description = "If True, Account is active",
            example     = True,
            default     = True,
            required    = required,
            ** kwargs)


class CreationDateField(fields.DateTime):
    """
    Creation Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Creation Date",
            description = "Account Creation Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)
