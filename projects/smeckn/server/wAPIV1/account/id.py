#!/usr/bin/env python3
"""
 NAME:
  id.py

 DESCRIPTION
  Account ID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.gDB.account.manager import AccountManager
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.common.auth import verifyAccessFunc
from smeckn.server.wAPIV1.account.models import accountPutInputModel, accountModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.account import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ID RESOURCE -------------------------------------- #
@ns.route("/<int:id>")
@ns.doc(params={"id": "Account ID"})
class IDResource(Resource):
    """
    ID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper()
    @ns.marshal_with(accountModel)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @logFunc()
    def get(self, id):  # pylint: disable=R0201,W0622
        """
        Get account
        """
        gDB = current_app.mgr.gdb.gDB

        verifyAccessFunc(aID=id)
        with gDB.sessionScope(commit=False) as gs:
            return AccountDAO.getByID(gs=gs, aID=id)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper()
    @ns.expect(accountPutInputModel)
    @ns.marshal_with(accountModel)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    def put(self, id):  # pylint: disable=R0201,W0622
        """
        Update account
        """
        inData      = request.json
        gDB         = current_app.mgr.gdb.gDB
        active      = inData.get("active", None)
        oldPassword = inData.get("oldPassword", "")
        newPassword = inData.get("newPassword", None)

        verifyAccessFunc(aID=id)
        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=id)
            # Password updates
            if newPassword is not None:
                AccountDAO.updatePassword(gs=gs, aModel=aModel, oldPassword=oldPassword,
                                          newPassword=newPassword)
            # Active updates
            if active is not None:
                AccountDAO.update(gs=gs, aModel=aModel, active=active)
            gs.commit()
            return AccountDAO.getByID(gs=gs, aID=id)

    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @logFunc()
    def delete(self, id):  # pylint: disable=R0201,W0622
        """
        Delete account

        Only TEST accounts allowed.
        """
        gDB    = current_app.mgr.gdb.gDB
        aModel = None

        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByID(gs=gs, aID=id, raiseOnNone=False)
        if aModel and aModel.type != AccountType.TEST:
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED)
        elif aModel:
            AccountManager.deleteByID(gDB, aID=id)
