#!/usr/bin/env python3
"""
 NAME:
  tokens_test.py

 DESCRIPTION
  Test Tokens Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.account.models import verifyAccountTokenModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- TOKENS POST TEST CASE ------------------------ #
class TokensPostTestCase(CommonTestCase):
    """
    Tokens Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = "{}/tokens".format(self.routes.account)
        self.validPayload = {
            "email": self.popAccountEmail,
            "password": self.popAccountPassword
        }

    @logFunc()
    def test_valid(self):
        """
        Valid Test.
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            verifyAccountTokenModel(gs=gs, mgr=self.mgr, apiModel=data, aID=self.popAccountID)

    @logFunc()
    def test_onlyTestAccountsAllowed(self):
        """
        Test trying to get tokens for non-test account
        """
        payload = {
            "email": self.adminEmail,
            "password": "asdfaasdlfkajsdflkj"
        }
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1191)

    @logFunc()
    def test_noEmail(self):
        """
        Test no email
        """
        inData = self.validPayload.copy()
        inData.pop("email")
        (code, _) = self.postJSON(self.validURL, d=inData, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noPassword(self):
        """
        Test no password
        """
        inData = self.validPayload.copy()
        inData.pop("password")
        (code, _) = self.postJSON(self.validURL, d=inData, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badEmail(self):
        """
        Test bad email
        """
        inData = self.validPayload.copy()
        email  = "12098234@gmail.com"
        inData["email"] = email
        (code, data) = self.postJSON(self.validURL,
                                     d=inData, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("authToken"), "")
        self.assertEqual(data.get("accountToken"), "")
        self.assertEqual(data.get("forgotPasswordToken"), "")
        createAccountAD  = self.mgr.jwt.createAccount.decodeJWT(data.get("createAccountToken"))
        self.assertEqual(email, createAccountAD.email)
        self.assertEqual(self.popAccountPassword, createAccountAD.password)

    @logFunc()
    def test_noAuthToken(self):
        """
        Test with no auth token
        """
        (code, _) = self.postJSON(self.validURL, d=self.validPayload)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badAuthToken(self):
        """
        Test bad auth token
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload,
                                     authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
