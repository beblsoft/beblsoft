#!/usr/bin/env python3
"""
 NAME:
  tokens.py

 DESCRIPTION
  Account Tokens Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.account.models import accountTokenPostInputModel, accountTokenPostOutputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.account import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ TOKEN RESOURCE ----------------------------------- #
@ns.route("/tokens")
class TokensResource(Resource):
    """
    Tokens Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.expect(accountTokenPostInputModel)
    @ns.marshal_with(accountTokenPostOutputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201,W0622
        """
        Create tokens associated with account

        Only TEST account access allowed.
        """
        inData                = request.json
        gDB                   = current_app.mgr.gdb.gDB
        email                 = inData.get("email")
        password              = inData.get("password")
        authToken             = ""
        accountToken          = ""
        createAccountToken    = ""
        forgotPasswordToken   = ""

        with gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)
            if aModel and aModel.type != AccountType.TEST:
                raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED)
            if aModel:
                aDBSModel            = aModel.accountDBServer
                aAdmin               = (aModel.type == AccountType.ADMIN)
                accountToken         = current_app.mgr.jwt.account.encodeJWT(aID=aModel.id,
                                                                             readDB=aDBSModel.readDomain,
                                                                             writeDB=aDBSModel.writeDomain)
                authToken            = current_app.mgr.jwt.auth.encodeJWT(aID=aModel.id, aAdmin=aAdmin)
                forgotPasswordToken  = current_app.mgr.jwt.forgotPassword.encodeJWT(aID=aModel.id)
            createAccountToken       = current_app.mgr.jwt.createAccount.encodeJWT(email=email,
                                                                                   password=password,
                                                                                   aType=AccountType.TEST)
        return {"authToken": authToken,
                "accountToken": accountToken,
                "createAccountToken": createAccountToken,
                "forgotPasswordToken": forgotPasswordToken}
