#!/usr/bin/env python3
"""
 NAME:
  account_test.py

 DESCRIPTION
  Test Account Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.wAPIV1.account.models import verifyAccountModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- ACCOUNT GET TEST CASE ------------------------ #
class AccountGetTestCase(CommonTestCase):
    """
    Account Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.urlArgs  = urllib.parse.urlencode({"email": self.popAccountEmail})
        self.validURL = "{}?{}".format(self.routes.account, self.urlArgs)

    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            verifyAccountModel(gs=gs, apiModel=data[0], aID=self.popAccountID)

    @logFunc()
    def test_noEmail(self):
        """
        Test with no email
        """
        url = "{}".format(self.routes.account)
        (code, _) = self.getJSON(url, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_nonExistentEmail(self):
        """
        Test with email that is not in the database
        """
        urlArgs = urllib.parse.urlencode({"email": "1234923847"})
        url = "{}?{}".format(self.routes.account, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.adminAuthToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1185)

    @logFunc()
    def test_noAdmin(self):
        """
        Test with non admin token
        """
        (code, data) = self.getJSON(self.validURL,
                                    authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)


# ---------------------------- ACCOUNT POST TEST CASE ----------------------- #
class AccountPostTestCase(CommonTestCase):
    """
    Account Post Test Case
    """

    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.newEmail     = "32908543gkjshdf@foo.com"
        self.newPassword  = "129082743524"
        self.newType      = AccountType.TEST
        self.validPayload = {
            "email": self.newEmail,
            "password": self.newPassword,
            "type": self.newType.name
        }

    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        newAccountID = None
        (code, data) = self.postJSON(self.routes.account, d=self.validPayload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            newAccountID = AccountDAO.getByEmail(gs=gs, email=self.newEmail).id
            verifyAccountModel(gs=gs, apiModel=data, aID=newAccountID)

    @logFunc()
    def test_onlyTestAccountsAllowed(self):
        """
        Test trying a non-test account
        """
        for aType in list(AccountType):
            if aType == AccountType.TEST:
                continue
            payload = self.validPayload.copy()
            payload["type"] = aType.name
            (code, data) = self.postJSON(self.routes.account, d=payload, authToken=self.adminAuthToken)
            self.assertEqual(code, 400)
            self.assertEqual(data.get("code"), 1191)

    @logFunc()
    def test_noEmail(self):
        """
        Test with no email
        """
        inData = self.validPayload.copy()
        inData.pop("email")
        (code, _) = self.postJSON(self.routes.account, d=inData,
                                  authToken=self.adminAuthToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noPassword(self):
        """
        Test with no password
        """
        inData = self.validPayload.copy()
        inData.pop("password")
        (code, _) = self.postJSON(self.routes.account, d=inData,
                                  authToken=self.adminAuthToken)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noType(self):
        """
        Test with no type
        """
        newAccountID = None
        inData = self.validPayload.copy()
        inData.pop("type")
        (code, data) = self.postJSON(self.routes.account, d=inData,
                                     authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            newAccountID = AccountDAO.getByEmail(gs=gs, email=self.newEmail).id
            verifyAccountModel(gs=gs, apiModel=data, aID=newAccountID)

    @logFunc()
    def test_noAdmin(self):
        """
        Test with non admin token
        """
        (code, data) = self.postJSON(self.routes.account, d=self.validPayload,
                                     authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
