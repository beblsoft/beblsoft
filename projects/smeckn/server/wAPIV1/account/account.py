#!/usr/bin/env python3
"""
 NAME:
  account.py

 DESCRIPTION
  Account Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.manager import AccountManager
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.account.models import accountGetParser, accountPostInputModel, accountModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.account import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ACCOUNT RESOURCE --------------------------------- #
@ns.route("")
class AccountResource(Resource):
    """
    Account Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.expect(accountGetParser)
    @ns.marshal_list_with(accountModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get account by email
        """
        args   = accountGetParser.parse_args()
        email  = args.get("email", None)
        gDB    = current_app.mgr.gdb.gDB
        rval   = []

        with gDB.sessionScope(commit=False) as gs:
            rval = [AccountDAO.getByEmail(gs=gs, email=email)]
        return rval

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.expect(accountPostInputModel)
    @ns.marshal_with(accountModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create account

        Only TEST accounts allowed.
        """
        inData    = request.json
        gDB       = current_app.mgr.gdb.gDB
        cfg       = current_app.cfg
        email     = inData.get("email")
        password  = inData.get("password")
        aTypeName = inData.get("type", AccountType.TEST.name)
        aType     = AccountType[aTypeName]

        if aType != AccountType.TEST:
            raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED)

        return AccountManager.create(gDB=gDB, aDBNamePrefix=cfg.ADB_NAME_PREFIX,
                                     email=email, password=password, aType=aType)
