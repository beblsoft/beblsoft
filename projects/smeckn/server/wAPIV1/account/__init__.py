#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Account Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("account", description="Account Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .account import *  #pylint: disable=C0413,W0401
from .id import *  #pylint: disable=C0413,W0401
from .tokens import *  #pylint: disable=C0413,W0401
