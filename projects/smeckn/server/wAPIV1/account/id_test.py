#!/usr/bin/env python3
"""
 NAME:
  id_test.py

 DESCRIPTION
  Test ID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.account.models import verifyAccountModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- ID GET TEST CASE ----------------------------- #
class IDGetTestCase(CommonTestCase):
    """
    ID Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = "{}/{}".format(self.routes.account, self.popAccountID)

    @logFunc()
    def test_validRegToken(self):
        """
        Valid Test. Regular User Sending Request
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            verifyAccountModel(gs=gs, apiModel=data, aID=self.popAccountID)

    @logFunc()
    def test_validAdminToken(self):
        """
        Valid Test. Admin User Sending Request
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            verifyAccountModel(gs=gs, apiModel=data, aID=self.popAccountID)

    @logFunc()
    def test_noToken(self):
        """
        Test with no token
        """
        (code, _) = self.getJSON(self.validURL)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badToken(self):
        """
        Test bad token
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.nonPopAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1187)

    @logFunc()
    def test_badIDRegToken(self):
        """
        Test bad id with regular token
        """
        route = "{}/12329494849887".format(self.routes.account)
        (code, data) = self.getJSON(route, authToken=self.nonPopAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1187)

    @logFunc()
    def test_badIDAdminToken(self):
        """
        Test bad id with admin token
        """
        route = "{}/12329494849887".format(self.routes.account)
        (code, data) = self.getJSON(route, authToken=self.adminAuthToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1185)


# ---------------------------- ID PUT TEST CASE ----------------------------- #
class IDPutTestCase(CommonTestCase):
    """
    ID Put Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL    = "{}/{}".format(self.routes.account, self.popAccountID)
        self.oldPassword = self.popAccountPassword
        self.newPassword = "12394857425222"
        self.passwordPayload = {"oldPassword": self.oldPassword, "newPassword": self.newPassword}

    @logFunc()
    def test_validRegTokenActive(self):
        """
        Valid Test. Regular User Sending Request. Disable Active
        """
        active       = False
        (code, data) = self.putJSON(self.validURL, d={"active": active}, authToken=self.popAuthToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("active"), active)
        with self.gDB.sessionScope(commit=False) as gs:
            regModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID)
            self.assertEqual(regModel.active, active)

    @logFunc()
    def test_validAdminTokenActive(self):
        """
        Valid Test. Admin User Sending Request. Disable Active
        """
        active       = False
        (code, data) = self.putJSON(self.validURL, d={"active": active}, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        self.assertEqual(data.get("active"), active)
        with self.gDB.sessionScope(commit=False) as gs:
            regModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID)
            self.assertEqual(regModel.active, active)

    @logFunc()
    def test_validRegTokenPassword(self):
        """
        Valid Test. Regular User Sending Request. Change password
        """
        (code, _)    = self.putJSON(self.validURL, d=self.passwordPayload, authToken=self.popAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            regModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID)
            self.assertTrue(regModel.verifyPassword(self.newPassword))

    @logFunc()
    def test_validAdminTokenPassword(self):
        """
        Valid Test. Admin User Sending Request. Change password
        """
        (code, _)    = self.putJSON(self.validURL, d=self.passwordPayload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            regModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID)
            self.assertTrue(regModel.verifyPassword(self.newPassword))

    @logFunc()
    def test_noOldPassword(self):
        """
        Valid Test. Regular User Sending Request. Change password
        """
        payload = self.passwordPayload.copy()
        payload.pop("oldPassword")
        (code, data) = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1183)

    @logFunc()
    def test_noToken(self):
        """
        Test with no token
        """
        active    = False
        (code, _) = self.putJSON(self.validURL, d={"active": active})
        self.assertEqual(code, 400)

    @logFunc()
    def test_badToken(self):
        """
        Test bad token
        """
        active       = False
        (code, data) = self.putJSON(self.validURL, d={"active": active}, authToken=self.nonPopAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1187)

    @logFunc()
    def test_badIDRegToken(self):
        """
        Test bad id with regular token
        """
        active = False
        route  = "{}/12329494849887".format(self.routes.account)
        (code, data) = self.putJSON(route, d={"active": active}, authToken=self.nonPopAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1187)

    @logFunc()
    def test_badIDAdminToken(self):
        """
        Test bad id with admin token
        """
        active = False
        route  = "{}/12329494849887".format(self.routes.account)
        (code, data) = self.putJSON(route,
                                    d={"active": active}, authToken=self.adminAuthToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1185)


# ---------------------------- ID DELETE TEST CASE -------------------------- #
class IDDeleteTestCase(CommonTestCase):
    """
    ID Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = "{}/{}".format(self.routes.account, self.popAccountID)

    @logFunc()
    def test_valid(self):
        """
        Valid Test.
        """
        (code, _) = self.deleteJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            regModel = AccountDAO.getByID(gs=gs, aID=self.popAccountID, raiseOnNone=False)
            self.assertEqual(regModel, None)

    @logFunc()
    def test_onlyTestAccountsAllowed(self):
        """
        Test trying to delete a non-test account
        """
        url          = "{}/{}".format(self.routes.account, self.adminAccountID)
        (code, data) = self.deleteJSON(url, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1191)
        with self.gDB.sessionScope(commit=False) as gs:
            adminModel = AccountDAO.getByID(gs=gs, aID=self.adminAccountID)
            self.assertEqual(adminModel.id, self.adminAccountID)

    @logFunc()
    def test_noToken(self):
        """
        Test with no token
        """
        (code, _) = self.deleteJSON(self.validURL)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badToken(self):
        """
        Test bad token
        """
        (code, data) = self.deleteJSON(
            self.validURL, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
