#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Account Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.account.fields import IDField, TypeField, ActiveField, CreationDateField
from smeckn.server.wAPIV1.auth.fields import EmailField, PasswordField
from smeckn.server.wAPIV1.auth.fields import AuthJWTField, AccountJWTField, CreateAccountJWTField, ForgotPasswordJWTField
from smeckn.server.wAPIV1.account import ns
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType


# ------------------------ PARSERS ------------------------------------------ #
accountGetParser = ns.parser()
accountGetParser.add_argument(
    "email",
    type         = str,
    required     = True,
    location     = "args")


# ------------------------ MODELS ------------------------------------------- #
accountModel = ns.model("AccountModel", {
    "id": IDField(),
    "email": EmailField(),
    "type": TypeField(),
    "active": ActiveField(),
    "creationDate": CreationDateField()
})

accountPostInputModel = ns.model("AccountPostInputModel", {
    "email": EmailField(),
    "password": PasswordField(),
    "type": TypeField(required=False)
})

accountPutInputModel = ns.model("AccountPutInputModel", {
    "active": ActiveField(required=False),
    "oldPassword": PasswordField(required=False),
    "newPassword": PasswordField(required=False),
})

accountTokenPostInputModel = ns.model("AccountTokenPostInputModel", {
    "email": EmailField(),
    "password": PasswordField(),
})

accountTokenPostOutputModel = ns.model("AccountTokenPostOutputModel", {
    "authToken": AuthJWTField(),
    "accountToken": AccountJWTField(),
    "createAccountToken": CreateAccountJWTField(),
    "forgotPasswordToken": ForgotPasswordJWTField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyAccountModel(gs, apiModel, aID):
    """
    Verify account model
    Args
      apiModel:
        account model returned from API
      aID:
        account database id
    """
    aModel = AccountDAO.getByID(gs=gs, aID=aID)
    assert apiModel.get("id") == aModel.id
    assert apiModel.get("email") == aModel.email
    assert apiModel.get("type") == aModel.type.name
    assert apiModel.get("active") == aModel.active


@logFunc()
def verifyAccountTokenModel(gs, mgr, apiModel, aID):
    """
    Verify account token model
    """
    authAD           = mgr.jwt.auth.decodeJWT(apiModel.get("authToken"))
    accountAD        = mgr.jwt.account.decodeJWT(apiModel.get("accountToken"))
    createAccountAD  = mgr.jwt.createAccount.decodeJWT(apiModel.get("createAccountToken"))
    forgotPasswordAD = mgr.jwt.forgotPassword.decodeJWT(apiModel.get("forgotPasswordToken"))
    aModel           = AccountDAO.getByID(gs=gs, aID=aID)
    aDBSModel        = aModel.accountDBServer
    assert aModel.id == authAD.aID
    assert (aModel.type == AccountType.ADMIN) == authAD.aAdmin
    assert aModel.id == accountAD.aID
    assert aDBSModel.readDomain == accountAD.readDB
    assert aDBSModel.writeDomain == accountAD.writeDB
    assert aModel.email == createAccountAD.email
    assert aModel.id == forgotPasswordAD.aID
