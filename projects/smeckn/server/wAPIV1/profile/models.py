#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Profile Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.fields import SMIDField, TypeField
from smeckn.server.wAPIV1.profileGroup.fields import IDField as ProfileGroupIDField
from smeckn.server.wAPIV1.profile import ns
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.aDB.profile.model import ProfileType


# ------------------------ PARSERS ------------------------------------------ #
profileGetParser = ns.parser()
profileGetParser.add_argument(
    "profileGroupID",
    type         = int,
    required     = False,
    location     = "args")

# ------------------------ MODELS ------------------------------------------- #
profileModel = ns.model("ProfileModel", {
    "smID": SMIDField(),
    "profileGroupID": ProfileGroupIDField(),
    "type": TypeField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyProfileModel(aDBS, apiModel, smID):
    """
    Verify profile model
    """
    pModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == pModel.smID
    assert apiModel.get("profileGroupID") == pModel.profileGroupID
    assert apiModel.get("type") == ProfileType.BASE.name
