#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Profile SMID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.models import profileModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.profile import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Profile Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """

    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_list_with(profileModel)
    @logFunc()
    def get(self, smID): #pylint: disable=R0201,W0622
        """
        Get profile
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return ProfileDAO.getBySMID(aDBS=aDBS, smID=smID)


    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @logFunc()
    def delete(self, smID):  # pylint: disable=R0201,W0622
        """
        Delete profile
        """
        with AccountDatabase.sessionScopeFromFlask() as aDBS:
            ProfileDAO.deleteBySMID(aDBS=aDBS, smID=smID)
