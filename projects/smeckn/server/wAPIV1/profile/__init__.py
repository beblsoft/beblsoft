#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Profile Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('profile', description="Profile Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .profile import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
