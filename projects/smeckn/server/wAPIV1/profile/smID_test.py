#!/usr/bin/env python3
"""
 NAME:
  smID_test.py

 DESCRIPTION
  Test SMID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.wAPIV1.profile.models import verifyProfileModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- SMID GET TEST CASE --------------------------- #
class SMIDGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validURL = "{}/{}".format(self.routes.profile, self.popProfileSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyProfileModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url          = "{}/{}".format(self.routes.profile, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1601)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ---------------------------- SMID DELETE TEST CASE ------------------------ #
class SMIDDeleteTestCase(CommonTestCase):
    """
    Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validURL = "{}/{}".format(self.routes.profile, self.popProfileSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test changing name
        """
        (code, _) = self.deleteJSON(self.validURL,
                                    authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID,
                                                 write=False) as aDBS:
            pModel = ProfileDAO.getBySMID(aDBS=aDBS, smID=self.popProfileSMID, raiseOnNone=False)
            self.assertEqual(pModel, None)

    @logFunc()
    def test_badID(self):
        """
        Test with a bad ID
        """
        url       = "{}/{}".format(self.routes.profile, 1293842398)
        (code, _) = self.deleteJSON(url, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.deleteJSON)
