#!/usr/bin/env python3
"""
 NAME:
  profile_test.py

 DESCRIPTION
  Test Profile Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.profile.models import verifyProfileModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- PROFILE GET TEST CASE --------------------------- #
class ProfileGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, profileGroups=True, profiles=True)
        self.validURL = self.routes.profile

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validMany(self):
        """
        Valid Test Returning Many
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken,
                                    accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyProfileModel(aDBS=aDBS, apiModel=data[0], smID=data[0].get("smID"))
        self.assertEqual(len(data), len(self.pgNameList))

    @logFunc()
    def test_validNone(self):
        """
        Valid Test Returning None
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.nonPopAuthToken,
                                    accountToken=self.nonPopAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
