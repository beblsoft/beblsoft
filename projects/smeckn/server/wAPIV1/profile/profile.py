#!/usr/bin/env python3
"""
 NAME:
  profile.py

 DESCRIPTION
  Profile Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.profile.models import profileModel, profileGetParser
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.profile.dao import ProfileDAO
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.profile import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ PROFILE RESOURCE --------------------------------- #
@ns.route("")
class ProfileResource(Resource):
    """
    Profile Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(profileGetParser)
    @ns.marshal_list_with(profileModel)
    @logFunc()
    def get(self): #pylint: disable=R0201
        """
        Get profiles
        """
        args           = profileGetParser.parse_args()
        profileGroupID = args.get("profileGroupID", None)

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return ProfileDAO.getAll(aDBS=aDBS, profileGroupID=profileGroupID)
