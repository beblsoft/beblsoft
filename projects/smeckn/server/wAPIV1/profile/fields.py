#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Profile Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.profile.model import ProfileType


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Profile Identification Number",
            example     = 398,
            required    = required,
            **kwargs)


class TypeField(fields.String):
    """
    Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Type",
            description = "Profile Type",
            example     = ProfileType.FACEBOOK.name,
            enum        = [str(name) for name, _ in ProfileType.__members__.items()],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, ProfileType):
            return value.name
        else:
            return None
