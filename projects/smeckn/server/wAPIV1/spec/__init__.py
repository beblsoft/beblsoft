#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Spec Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('spec', description="Spec Namespace")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .spec import *  #pylint: disable=C0413,W0401
