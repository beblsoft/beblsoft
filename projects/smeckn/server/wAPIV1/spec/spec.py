#!/usr/bin/env python3
"""
 NAME:
  spec.py

 DESCRIPTION
  Spec Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import json
from flask import jsonify
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.spec import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SPEC RESOURCE ------------------------------------ #
@ns.route("")
class SpecResource(Resource):
    """
    Spec Routes
    """
    @requestWrapper()
    @ns.doc(security=None)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Return Swagger Spec
        """
        from smeckn.server.wAPIV1 import api
        return jsonify(api.__schema__)


# -------------------------- CREATE SWAGGER SPEC ---------------------------- #
def createSpec(fApp,
               dirName=os.path.dirname(os.path.realpath(__file__)),
               fileName="swagger.json",
               indent=4):
    """
    Create Swagger Spec
    Args
      fApp:
        Flask application
      dirName:
        Directory where to create the swagger spec
      fileName:
        Swagger Spec file name
    """
    from smeckn.server.wAPIV1 import api
    pathName = "{}/{}".format(dirName, fileName)
    with open(pathName, "w") as f:
        with fApp.test_request_context("/"):
            f.write(json.dumps(api.__schema__, indent=indent))
