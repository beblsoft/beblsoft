#!/usr/bin/env python3
"""
 NAME:
  smID_test.py

 DESCRIPTION
  Test SMID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.text.models import verifyTextModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- SMID GET TEST CASE ------------------------------ #
class SMIDGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, text=True)
        self.validURL = "{}/{}".format(self.routes.text, self.popTextSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyTextModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url          = "{}/{}".format(self.routes.text, 1293842398)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1005)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- SMID DELETE TEST CASE --------------------------- #
class SMIDDeleteTestCase(CommonTestCase):
    """
    Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)

        # SetUp text fromUser and not fromUser
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                textModelFromUser    = TextDAO.createFromText(aDBS=aDBS, text="Blah1", fromUser=True)
                textModelNotFromUser = TextDAO.createFromText(aDBS=aDBS, text="Blah2", fromUser=False)
            self.textFromUserSMID    = textModelFromUser.smID
            self.textNotFromUserSMID = textModelNotFromUser.smID

        self.validURL = "{}/{}".format(self.routes.text, self.textFromUserSMID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, _) = self.deleteJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            textModel = TextDAO.getBySMID(aDBS=aDBS, smID=self.textFromUserSMID, raiseOnNone=False)
            self.assertIsNone(textModel)

    @logFunc()
    def test_smIDNotFromUser(self):
        """
        Test with SMID not from user
        """
        url = "{}/{}".format(self.routes.text, self.textNotFromUserSMID)
        (code, data) = self.deleteJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 2401)

    @logFunc()
    def test_badSMID(self):
        """
        Test with bad SMID
        """
        url = "{}/{}".format(self.routes.text, 12098)
        (code, _) = self.deleteJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.deleteJSON)
