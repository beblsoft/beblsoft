#!/usr/bin/env python3
"""
 NAME:
  smID.py

 DESCRIPTION
  Text SMID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.text.models import textModel
from smeckn.server.wAPIV1.text import ns
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ SMID RESOURCE ------------------------------------ #
@ns.route("/<int:smID>")
@ns.doc(params={"smID": "Text Smeckn ID"})
class SMIDResource(Resource):
    """
    SMID Routes
    """

    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(textModel)
    @logFunc()
    def get(self, smID):  # pylint: disable=R0201
        """
        Get text
        """
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            textDBModel = TextDAO.getBySMID(aDBS=aDBS, smID=smID)
            return marshal(textDBModel, textModel)

    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @logFunc()
    def delete(self, smID):  # pylint: disable=R0201
        """
        Delete text
        """
        with AccountDatabase.sessionScopeFromFlask() as aDBS:
            TextDAO.deleteBySMID(aDBS=aDBS, smID=smID, validateFromUser=True)
