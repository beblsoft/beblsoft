#!/usr/bin/env python3
"""
 NAME:
  text.py

 DESCRIPTION
  Text Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
from flask import request
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.text.models import textPostInputModel, textGetParser, textModel, textPaginationModel
from smeckn.server.wAPIV1.pagination.cursorSort import SortPaginationCursor
from smeckn.server.wAPIV1.text import ns
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.sort.type import SortType
from smeckn.server.aDB.sort.order import SortOrder


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ TEXT RESOURCE ------------------------------------ #
@ns.route("")
class TextResource(Resource):
    """
    Text Routes
    """

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(textPostInputModel)
    @ns.marshal_with(textModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create text
        """
        inData = request.json
        text   = inData.get("text")

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                textDBModel = TextDAO.createFromText(aDBS=aDBS, text=text, fromUser=True)
            return marshal(textDBModel, textModel)

    # -------------------- GET SORT TYPE DICT ------------------------------- #
    # Same concept FacebookPost.getSortTypeDict
    getSortTypeDict = {
        SortType.TEXT_CREATEDATE: {
            "pDecodeCursorFunc": lambda pStr: datetime.fromtimestamp(pStr),  # pylint: disable=W0108
            "pEncodeCursorFunc": lambda textModel: textModel.createDate.timestamp(),
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda textModel: textModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda textModel: textModel.compSentAnalysisModel.positiveScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda textModel: textModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda textModel: textModel.compSentAnalysisModel.negativeScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda textModel: textModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda textModel: textModel.compSentAnalysisModel.neutralScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda textModel: textModel.smID
        },
        SortType.COMP_SENT_ANALYSIS_MIXED_SCORE: {
            "pDecodeCursorFunc": float,
            "pEncodeCursorFunc": lambda textModel: textModel.compSentAnalysisModel.mixedScore,
            "sDecodeCursorFunc": int,
            "sEncodeCursorFunc": lambda textModel: textModel.smID
        },
    }

    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(textGetParser)
    @ns.marshal_with(textPaginationModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get text
        """
        args                  = textGetParser.parse_args()
        createdAfterDate      = args.get("createdAfterDate", None)
        if createdAfterDate:
            createdAfterDate  = createdAfterDate.replace(tzinfo=None)
        createdBeforeDate     = args.get("createdBeforeDate", None)
        if createdBeforeDate:
            createdBeforeDate = createdBeforeDate.replace(tzinfo=None)
        searchString          = args.get("searchString", None)
        fromUser              = args.get("fromUser", None)
        limit                 = args.get("limit")
        sortType              = SortType[args.get("sortType")]
        sortOrder             = SortOrder[args.get("sortOrder")]
        cursor                = SortPaginationCursor.fromParserArgs(args)
        pDecodeCursorFunc     = self.getSortTypeDict[sortType]["pDecodeCursorFunc"]
        pEncodeCursorFunc     = self.getSortTypeDict[sortType]["pEncodeCursorFunc"]
        sDecodeCursorFunc     = self.getSortTypeDict[sortType]["sDecodeCursorFunc"]
        sEncodeCursorFunc     = self.getSortTypeDict[sortType]["sEncodeCursorFunc"]
        pLastValue            = None
        sLastValue            = None
        nextCursor            = None

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            # Retrieve last values from cursor
            if cursor:
                pLastValue = cursor.castPLastValue(castFunc=pDecodeCursorFunc)
                sLastValue = cursor.castSLastValue(castFunc=sDecodeCursorFunc)

            # Get Data
            textModelList = TextDAO.getAll(
                aDBS=aDBS, joinCompSent=True, iLikeText=searchString, fromUser=fromUser,
                createdAfterDate=createdAfterDate, createdBeforeDate=createdBeforeDate,
                sortType=sortType, sortOrder=sortOrder, sortPLastValue=pLastValue, sortSLastValue=sLastValue,
                limit=limit)

            # Encode next cursor
            if len(textModelList) == limit:
                lastTextModel = textModelList[-1]
                nextCursor    = SortPaginationCursor(pLastValue=pEncodeCursorFunc(lastTextModel),
                                                     sLastValue=sEncodeCursorFunc(lastTextModel))

            # Marshal data inside db scope
            return marshal({
                "nextCursor": nextCursor.toString() if nextCursor else "",
                "data": textModelList
            }, textPaginationModel)
