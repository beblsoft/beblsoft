#!/usr/bin/env python3
"""
 NAME:
  analysis_test.py

 DESCRIPTION
  Test SMID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.text.models import verifyTextModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.analysis.model import AnalysisType


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- ANALYSIS POST TEST CASE ------------------------- #
class AnalysisPostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True)

        # SetUp text with no analysis
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            with AccountDatabase.transactionScope(session=aDBS):
                textModel = TextDAO.createFromText(aDBS=aDBS, text="I am English text!")
            self.textSMID = textModel.smID

        self.analysisType = AnalysisType.COMPREHEND_SENTIMENT
        self.validArgs    = {"analysisType": self.analysisType.name}
        self.baseURL      = "{}/{}/analysis".format(self.routes.text, self.textSMID)
        self.validURL     = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_compSentValid(self):
        """
        Test compSent valid
        """
        args                 = self.validArgs.copy()
        args["analysisType"] = AnalysisType.COMPREHEND_SENTIMENT.name
        url                  = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
        (code, data) = self.postJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertIsNotNone(data.get("compLangAnalysisModel"))
        self.assertIsNotNone(data.get("compSentAnalysisModel"))
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyTextModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test removing required arguments, verifying invalid
        """
        argList = ["analysisType"]
        for arg in argList:
            args      = self.validArgs.copy()
            args.pop(arg)
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.postJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test bad args
        """
        argBadCodeList = [("analysisType", "Bad Argument", 400)]
        for (arg, bad, outCode) in argBadCodeList:
            args      = self.validArgs.copy()
            args[arg] = bad
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.postJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON)
