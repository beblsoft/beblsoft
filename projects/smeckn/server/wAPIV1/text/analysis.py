#!/usr/bin/env python3
"""
 NAME:
  analysis.py

 DESCRIPTION
  Text SMID Analysis Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.text.models import textAnalysisPostParser, textModel
from smeckn.server.wAPIV1.text import ns
from smeckn.server.aDB.analysis.model import AnalysisType
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.text.dao import TextDAO


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ANALYSIS RESOURCE -------------------------------- #
@ns.route("/<int:smID>/analysis")
@ns.doc(params={"smID": "Text Smeckn ID"})
class AnalysisResource(Resource):
    """
    Analysis Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(textAnalysisPostParser)
    @ns.marshal_with(textModel)
    @logFunc()
    def post(self, smID):  # pylint: disable=R0201
        """
        Preform text analysis
        """
        args         = textAnalysisPostParser.parse_args()
        analysisType = AnalysisType[args.get("analysisType")]
        mgr          = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            # Verify text exists
            textDBModel = TextDAO.getBySMID(aDBS=aDBS, smID=smID)

            # Perform Analysis
            if analysisType == AnalysisType.COMPREHEND_SENTIMENT:
                textDBModel = mgr.analysis.text.compSent.analyze(aDBS=aDBS, textSMID=smID)

            # Return
            return marshal(textDBModel, textModel)
