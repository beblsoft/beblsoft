#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Text Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class CreateDateField(fields.DateTime):
    """
    Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Date",
            description = "Create Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class TextField(fields.String):
    """
    Text Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Text",
            description = "Text",
            example     = "The other day the the dog jumped over the moon!",
            required    = required,
            **kwargs)

class OriginalLengthField(fields.Integer):
    """
    Original Length Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Original Length before truncated",
            description = "Original Length (in characters) before it was truncated",
            example     = 200,
            required    = required,
            **kwargs)
