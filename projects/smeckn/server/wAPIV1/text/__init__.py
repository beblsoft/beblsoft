#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Text Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('text', description="Text Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .text import *  #pylint: disable=C0413,W0401
from .smID import * #pylint: disable=C0413,W0401
from .analysis import * #pylint: disable=C0413,W0401
