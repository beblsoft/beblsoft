#!/usr/bin/env python3
"""
 NAME:
  text_test.py

 DESCRIPTION
  Test Text Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from unittest.mock import patch
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.text.models import verifyTextModel
from smeckn.server.wAPIV1.text.models import textGetParser
from smeckn.server.wAPIV1.text.text import TextResource
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.sort.order import SortOrder
from smeckn.server.aDB.sort.type import SortType
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.text.model import MAX_TEXT_FROM_USER


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- TEXT POST TEST CASE ----------------------------- #
class TextPostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL     = self.routes.text
        self.validPayload = {"text": "Hello World!!"}

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyTextModel(aDBS=aDBS, apiModel=data, smID=data.get("smID"))

    @logFunc()
    def test_tooMany(self):
        """
        Test too many text created
        """
        with patch.object(TextDAO, attribute="getAll", return_value=MAX_TEXT_FROM_USER) as getAllMock:
            (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken,
                                         accountToken=self.popAccountToken)
            self.assertEqual(code, 400)
            self.assertEqual(data.get("code"), 2400)
            self.assertEqual(getAllMock.call_count, 1)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test removing required arguments, verifying invalid
        """
        argList = ["text"]
        for arg in argList:
            payload = self.validPayload.copy()
            payload.pop(arg)
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                      accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test bad args
        """
        argBadCodeList = [("text", 12343, 400)]
        for (arg, bad, outCode) in argBadCodeList:
            payload = self.validPayload.copy()
            payload[arg] = bad
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                      accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)


# ------------------------- TEXT GET TEST CASE ------------------------------ #
class TextGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.baseURL           = self.routes.text
        self.createdAfterDate  = self.testStartDate
        self.createdBeforeDate = datetime.utcnow()
        self.limit             = 9
        self.cursor            = ""
        self.searchString      = "The fox is in the basement"
        self.validArgs         = {
            # "createdAfterDate"
            # "createdBeforeDate"
            # "searchString"
            # "fromUser"
            # "sortType"
            # "sortOrder"
            "limit": self.limit,
            "cursor": self.cursor
        }
        self.validURL          = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_apiModel(self):
        """
        Test text model is accurate
        """
        super().setUp(textFull=True)
        args              = self.validArgs.copy()
        keepLooping       = True
        nextCursor        = ""
        while keepLooping:
            args["cursor"]    = nextCursor
            url               = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, data)      = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            dataList          = data.get("data")
            nextCursor        = data.get("nextCursor")
            keepLooping       = nextCursor != ""
            with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
                for apiModel in dataList:
                    verifyTextModel(aDBS=aDBS, apiModel=apiModel, smID=apiModel.get("smID"))

    @logFunc()
    def test_filters(self):
        """
        Test filters
        """
        filterList = [
            ("createdAfterDate", self.createdAfterDate.isoformat(), 2),
            ("createdAfterDate", (self.createdAfterDate + timedelta(seconds=4)).isoformat(), 0),
            ("createdBeforeDate", (self.createdAfterDate + timedelta(seconds=4)).isoformat(), 2),
            ("createdBeforeDate", (self.createdAfterDate - timedelta(seconds=4)).isoformat(), 0),
            ("searchString", "blah", 2),
            ("searchString", "Blah", 2),
            ("searchString", "Hello", 0),
            ("fromUser", True, 1),
            ("fromUser", False, 1)
        ]

        # Create posts
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID) as aDBS:
            _                 = TextDAO.createFromText(aDBS=aDBS, text="Blah, Blah, Blah",
                                                       fromUser=True, createDate=self.createdAfterDate)
            _                 = TextDAO.createFromText(aDBS=aDBS, text="Blah, Blah, Blah",
                                                       fromUser=False, createDate=self.createdAfterDate)

        # Verify filters
        for fitlerName, value, count in filterList:
            args             = self.validArgs.copy()
            args[fitlerName] = value
            url              = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, data)     = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            dataList         = data.get("data")
            self.assertEqual(len(dataList), count)

    def test_getParserSortTypesAllSupported(self):
        """
        Test that all textGetParser sortTypes are supported in the resoure's getSortTypeDict
        """
        # Get allowable sortType choices
        supportSortTypeNameList = []
        for arg in textGetParser.args:
            if arg.name == "sortType":
                supportSortTypeNameList = arg.choices
        self.assertTrue(supportSortTypeNameList)

        # Verify each choice supported in getSortTypeList
        for supportTypeName in supportSortTypeNameList:
            self.assertIn(supportTypeName, [k.name for k, _ in TextResource.getSortTypeDict.items()])

    @logFunc()
    def test_sorts(self):
        """
        Test sorting by different entities
        """
        sortDictList = [
            {
                "sortType": SortType.TEXT_CREATEDATE,
                "getValFunc": lambda aM: aM.get("createDate"),
                "totalCheck": True
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE,
                "getValFunc": lambda aM: aM.get("compSentAnalysisModel").get("positiveScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE,
                "getValFunc": lambda aM: aM.get("compSentAnalysisModel").get("mixedScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE,
                "getValFunc": lambda aM: aM.get("compSentAnalysisModel").get("neutralScore"),
                "totalCheck": False
            },
            {
                "sortType": SortType.COMP_SENT_ANALYSIS_MIXED_SCORE,
                "getValFunc": lambda aM: aM.get("compSentAnalysisModel").get("mixedScore"),
                "totalCheck": False
            },
        ]
        super().setUp(textFull=True)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            nTextTotal = TextDAO.getAll(aDBS=aDBS, count=True)

        for sd in sortDictList:
            for sortOrder in [SortOrder.ASCENDING, SortOrder.DESCENDING]:
                args              = self.validArgs.copy()
                args["sortType"]  = sd.get("sortType").name
                args["sortOrder"] = sortOrder.name
                getValFunc        = sd.get("getValFunc")
                loopCompFunc      = self.assertGreaterEqual if sortOrder == SortOrder.DESCENDING else self.assertLessEqual
                keepLooping       = True
                nextCursor        = ""
                total             = 0
                lastCallAPIModel  = None

                while keepLooping:
                    args["cursor"]    = nextCursor
                    url               = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
                    (code, data)      = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
                    self.assertEqual(code, 200)
                    dataList          = data.get("data")
                    nextCursor        = data.get("nextCursor")
                    keepLooping       = nextCursor != ""
                    total            += len(dataList)
                    self.assertLessEqual(len(dataList), self.limit)
                    for idx, apiModel in enumerate(dataList):
                        # Check ordering between each element in this api call
                        if idx + 1 < len(dataList):
                            nextAPIModel = dataList[idx + 1]
                            loopCompFunc(getValFunc(apiModel), getValFunc(nextAPIModel))
                        # Check ordering between last api call and this one
                        if lastCallAPIModel:
                            loopCompFunc(getValFunc(lastCallAPIModel), getValFunc(apiModel))
                    if dataList:
                        lastCallAPIModel = dataList[-1]

                if sd.get("totalCheck"):
                    self.assertEqual(total, nTextTotal)

    def test_badArgInvalid(self):
        """
        Test bad args
        """
        argBadCodeList = [("createdAfterDate", "BadDateFormat", 400),
                          ("createdBeforeDate", "BadDateFormat", 400),
                          ("fromUser", "NotABoolean", 400),
                          ("sortType", "FACEBOOK_POST_NLIKES", 400),
                          ("sortType", "COMP_SENT_ANALYSIS_POSITIVE", 400),
                          ("sortType", "COMP_SENT_ANALYSIS_NEGATIVE", 400),
                          ("sortOrder", "DESCENDINGGG", 400),
                          ("sortOrder", "ASCENDINGGG", 400),
                          ("limit", 200, 400),
                          ("cursor", "BadCursorValue", 400)]
        for (arg, bad, outCode) in argBadCodeList:
            args = self.validArgs.copy()
            args[arg] = bad
            url = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
