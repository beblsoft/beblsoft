#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Text Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields, inputs
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.text.fields import SMIDField, CreateDateField, TextField, OriginalLengthField
from smeckn.server.wAPIV1.compLangAnalysis.models import compLangAnalysisModel, verifyCompLangAnalysisModel
from smeckn.server.wAPIV1.compSentAnalysis.models import compSentAnalysisModel, verifyCompSentAnalysisModel
from smeckn.server.wAPIV1.pagination.models import paginationParser, paginationModel
from smeckn.server.wAPIV1.text import ns
from smeckn.server.aDB.analysis.model import AnalysisType
from smeckn.server.aDB.text.dao import TextDAO
from smeckn.server.aDB.sort.order import SortOrder
from smeckn.server.aDB.sort.type import SortType


# ------------------------ PARSERS ------------------------------------------ #
textGetParser = paginationParser.copy()
textGetParser.add_argument(
    "createdAfterDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")
textGetParser.add_argument(
    "createdBeforeDate",
    type         = inputs.datetime_from_iso8601,
    required     = False,
    location     = "args")
textGetParser.add_argument(
    "searchString",
    type         = str,
    required     = False,
    location     = "args")
textGetParser.add_argument(
    "fromUser",
    type         = inputs.boolean,
    required     = False,
    location     = "args")
textGetParser.add_argument(
    "sortType",
    type         = str,
    required     = False,
    location     = "args",
    default      = SortType.TEXT_CREATEDATE.name,
    choices      = [SortType.TEXT_CREATEDATE.name,
                    SortType.COMP_SENT_ANALYSIS_POSITIVE_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_NEGATIVE_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_NEUTRAL_SCORE.name,
                    SortType.COMP_SENT_ANALYSIS_MIXED_SCORE.name])
textGetParser.add_argument(
    "sortOrder",
    type         = str,
    required     = False,
    location     = "args",
    default      = SortOrder.DESCENDING.name,
    choices      = [sortOrder.name for sortOrder in list(SortOrder)])


textAnalysisPostParser = ns.parser()
textAnalysisPostParser.add_argument(
    "analysisType",
    type         = str,
    required     = True,
    location     = "args",
    choices      = [AnalysisType.COMPREHEND_SENTIMENT.name])


# ------------------------ MODELS ------------------------------------------- #
textPostInputModel = ns.model("TextPostInputModel", {
    "text": TextField(),
})

textModel = ns.model("TextModel", {
    "smID": SMIDField(),
    "createDate": CreateDateField(),
    "text": TextField(),
    "origLength": OriginalLengthField(),
    "compLangAnalysisModel": fields.Nested(compLangAnalysisModel, allow_null=True),
    "compSentAnalysisModel": fields.Nested(compSentAnalysisModel, allow_null=True)
})

textPaginationModel = ns.inherit(
    "TextPaginationModel",
    paginationModel,
    {
        "data": fields.List(fields.Nested(textModel)),
    }
)


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyTextModel(aDBS, apiModel, smID):
    """
    Verify text model
    """
    _textModel = TextDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _textModel.smID
    assert apiModel.get("createDate") == _textModel.createDate.isoformat()
    assert apiModel.get("text") == _textModel.text
    assert apiModel.get("origLength") == _textModel.origLength

    _compLangAnalysisModel = apiModel.get("compLangAnalysisModel")
    if _compLangAnalysisModel:
        verifyCompLangAnalysisModel(aDBS=aDBS, apiModel=_compLangAnalysisModel,
                                    smID=_compLangAnalysisModel.get("smID"))

    _compSentAnalysisModel = apiModel.get("compSentAnalysisModel")
    if _compSentAnalysisModel:
        verifyCompSentAnalysisModel(aDBS=aDBS, apiModel=_compSentAnalysisModel,
                                    smID=_compSentAnalysisModel.get("smID"))
