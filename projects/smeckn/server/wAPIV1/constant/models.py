#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Default Models
"""


# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.wAPIV1.constant import ns
from smeckn.server.aDB.profileGroup.model import PG_MAX_COUNT
from smeckn.server.payments.creditCard.manager import MAX_ACCOUNT_CREDIT_CARDS
from smeckn.server.aDB.text.model import MAX_TEXT_LENGTH


# ------------------------ MODELS ------------------------------------------- #
constantModel = ns.model("ConstantModel", {
    "MAX_PROFILEGROUPS": fields.Integer(
        required    = True,
        title       = "Maximum profile groups per accout",
        description = "Maximum profile groups per accout. Attempts to add any more will fail",
        example     = 5
    ),
    "MAX_ACCOUNT_CREDIT_CARDS": fields.Integer(
        required    = True,
        title       = "Maximum number of credit cards per account.",
        description = "Maximum number of credit cards per account. Attempts to add any more will fail",
        example     = 5
    ),
    "TEXT_MAX_LENGTH": fields.Integer(
        required    = True,
        title       = "Maximum number of text characters supported.",
        description = "Maximum number of text characters supported. Longer pieces of text will be truncated.",
        example     = 5000
    ),
    "FBPROFILE_REQURIED_PERMISSION_LIST": fields.List(fields.String(
        required    = True,
        title       = "Facebook required permission",
        description = "Permission required by Smeckn to example Facebook data",
        example     = "user_posts"
    )),
    "FBPROFILE_DESIRED_PERMISSION_LIST": fields.List(fields.String(
        required    = True,
        title       = "Facebook desired permission",
        description = "Permission desired by Smeckn to example Facebook data",
        example     = "user_posts"
    )),
    "ENVIRONMENT": fields.String(
        required    = True,
        title       = "API Environment.",
        description = "API Environment.",
        example     = "Production"
    ),
    "GIT_COMMIT_SHA": fields.String(
        required    = True,
        title       = "API Git Commit SHA",
        description = "API Git Commit SHA",
        example     = "32ea7d9030f53a8aaf9a5dfcb5da181b87e5d9c3"
    ),
    "COMPREHEND_SENTIMENT_SUPPORTED_BLANGUAGE_LIST": fields.List(fields.String(
        required    = True,
        title       = "Supported Comprehend Language",
        description = "Supported Comprehend Beblsoft Language",
        example     = "ENGLISH"
    )),
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyConstantModel(mgr, apiModel):
    """
    Verify constant model
    """
    cfg = mgr.cfg
    assert apiModel.get("MAX_PROFILEGROUPS")                             == PG_MAX_COUNT
    assert apiModel.get("MAX_ACCOUNT_CREDIT_CARDS")                      == MAX_ACCOUNT_CREDIT_CARDS
    assert apiModel.get("TEXT_MAX_LENGTH")                               == MAX_TEXT_LENGTH
    assert apiModel.get("FBPROFILE_REQURIED_PERMISSION_LIST")            == cfg.FACEBOOK_REQUIRED_PERMISSION_LIST
    assert apiModel.get("FBPROFILE_DESIRED_PERMISSION_LIST")             == cfg.FACEBOOK_DESIRED_PERMISSION_LIST
    assert apiModel.get("ENVIRONMENT")                                   == cfg.ENVIRONMENT
    assert apiModel.get("GIT_COMMIT_SHA")                                == cfg.GIT_COMMIT_SHA
    assert apiModel.get("COMPREHEND_SENTIMENT_SUPPORTED_BLANGUAGE_LIST") == list(map(
        lambda bLanguageType: bLanguageType.name, BeblsoftComprehendClient.detectSentimentSupportedBLanguageTypeList))
