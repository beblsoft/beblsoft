#!/usr/bin/env python3
"""
 NAME:
  constant_test.py

 DESCRIPTION
  Test Constant Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.constant.models import verifyConstantModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CONSTANT GET TEST CASE ----------------------- #
class ConstantGetTestCase(CommonTestCase):
    """
    Constant Get Test Case
    """
    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.getJSON(self.routes.constant)
        self.assertEqual(code, 200)
        verifyConstantModel(mgr=self.mgr, apiModel=data)
