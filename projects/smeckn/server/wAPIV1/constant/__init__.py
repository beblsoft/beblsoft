#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Constant Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('constant', description="Constant Namespace")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .constant import *  #pylint: disable=C0413,W0401
