#!/usr/bin/env python3
"""
 NAME:
  constant.py

 DESCRIPTION
  Constant Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.constant.models import constantModel
from smeckn.server.aDB.profileGroup.model import PG_MAX_COUNT
from smeckn.server.payments.creditCard.manager import MAX_ACCOUNT_CREDIT_CARDS
from smeckn.server.aDB.text.model import MAX_TEXT_LENGTH
from smeckn.server.wAPIV1.constant import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CONSTANT RESOURCE -------------------------------- #
@ns.route("")
class ConstantResource(Resource):
    """
    Constant Routes
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.marshal_with(constantModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Return Constant Values
        """
        mgr = current_app.mgr
        cfg = mgr.cfg

        return {
          "MAX_PROFILEGROUPS" : PG_MAX_COUNT,
          "MAX_ACCOUNT_CREDIT_CARDS" : MAX_ACCOUNT_CREDIT_CARDS,
          "TEXT_MAX_LENGTH" : MAX_TEXT_LENGTH,
          "FBPROFILE_REQURIED_PERMISSION_LIST": cfg.FACEBOOK_REQUIRED_PERMISSION_LIST,
          "FBPROFILE_DESIRED_PERMISSION_LIST": cfg.FACEBOOK_DESIRED_PERMISSION_LIST,
          "ENVIRONMENT": cfg.ENVIRONMENT,
          "GIT_COMMIT_SHA": cfg.GIT_COMMIT_SHA,
          "COMPREHEND_SENTIMENT_SUPPORTED_BLANGUAGE_LIST":
          list(map(lambda bLanguageType: bLanguageType.name, BeblsoftComprehendClient.detectSentimentSupportedBLanguageTypeList))
        }

        # Future?
        # TEXT_ANALYSIS_LANGUAGES         : ComprehendLanguageEnum []
