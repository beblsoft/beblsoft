#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Link Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('link', description="Link Operations")
