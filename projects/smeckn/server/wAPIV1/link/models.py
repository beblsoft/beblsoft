#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Link Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.link.fields import SMIDField, CreateDateField
from smeckn.server.wAPIV1.link.fields import ImageURLField, LinkURLField
from smeckn.server.wAPIV1.link.fields import NameField, CaptionField, DescriptionField
from smeckn.server.wAPIV1.link import ns
from smeckn.server.aDB.link.dao import LinkDAO


# ------------------------ MODELS ------------------------------------------- #
linkModel = ns.model("LinkModel", {
    "smID": SMIDField(),
    "createDate": CreateDateField(),
    "imageURL": ImageURLField(),
    "linkURL": LinkURLField(),
    "name": NameField(),
    "caption": CaptionField(),
    "description": DescriptionField(),
})

# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyLinkModel(aDBS, apiModel, smID):
    """
    Verify link model
    """
    _linkModel = LinkDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _linkModel.smID
    assert apiModel.get("createDate") == _linkModel.createDate.isoformat()
    assert apiModel.get("imageURL") == _linkModel.imageURL
    assert apiModel.get("linkURL") == _linkModel.linkURL
    assert apiModel.get("name") == _linkModel.name
    assert apiModel.get("caption") == _linkModel.caption
    assert apiModel.get("description") == _linkModel.description
