#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Link Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class CreateDateField(fields.DateTime):
    """
    Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Date",
            description = "Create Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class ImageURLField(fields.String):
    """
    Image URL Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Image URL",
            description = "Image URL",
            example     = "https://www.gstatic.com/webp/gallery/1.sm.jpg",
            required    = required,
            **kwargs)

class LinkURLField(fields.String):
    """
    Link URL Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Link URL",
            description = "Link URL",
            example     = "https:www.disney.com",
            required    = required,
            **kwargs)

class NameField(fields.String):
    """
    Name Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Name",
            description = "Name",
            example     = "Disney.com | The official home for all things Disney",
            required    = required,
            **kwargs)

class CaptionField(fields.String):
    """
    Caption Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Caption",
            description = "Caption",
            example     = "disney.com",
            required    = required,
            **kwargs)

class DescriptionField(fields.String):
    """
    Description Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Description",
            description = "Description",
            example     = "The official website for all things Disney...",
            required    = required,
            **kwargs)
