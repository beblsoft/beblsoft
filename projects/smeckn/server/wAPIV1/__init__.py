#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Web API V1 Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import os
import logging
import json
from flask import Blueprint, jsonify, request
from flask_restplus import Api, apidoc  # pylint: disable=E0401
from smeckn.server.wAPIV1.default import ns as defaultNS
from smeckn.server.wAPIV1.spec import ns as specNS
from smeckn.server.wAPIV1.constant import ns as constantNS
from smeckn.server.wAPIV1.error import ns as errorNS
from smeckn.server.wAPIV1.platform import ns as platformNS
from smeckn.server.wAPIV1.auth import ns as authNS
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER, API_ACCOUNT_TOKEN_HEADER
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE
from smeckn.server.wAPIV1.account import ns as accountNS
from smeckn.server.wAPIV1.fake import ns as fakeNS
from smeckn.server.wAPIV1.unit import ns as unitNS
from smeckn.server.wAPIV1.creditCard import ns as creditCardNS
from smeckn.server.wAPIV1.cart import ns as cartNS
from smeckn.server.wAPIV1.charge import ns as chargeNS
from smeckn.server.wAPIV1.profileGroup import ns as profileGroupNS
from smeckn.server.wAPIV1.profile import ns as profileNS
from smeckn.server.wAPIV1.sync import ns as syncNS
from smeckn.server.wAPIV1.analysis import ns as analysisNS
from smeckn.server.wAPIV1.statistic import ns as statisticNS
from smeckn.server.wAPIV1.histogram import ns as histogramNS
from smeckn.server.wAPIV1.text import ns as textNS
from smeckn.server.wAPIV1.photo import ns as photoNS
from smeckn.server.wAPIV1.link import ns as linkNS
from smeckn.server.wAPIV1.video import ns as videoNS
from smeckn.server.wAPIV1.facebookProfile import ns as facebookProfileNS
from smeckn.server.wAPIV1.facebookPost import ns as facebookPostNS
from smeckn.server.wAPIV1.error.handlers import registerErrorHandlers


# -------------------------- GLOBALS ---------------------------------------- #
logger            = logging.getLogger(__name__)
bp                = Blueprint("v1", __name__)
api               = Api(bp,
                        title          = "Smeckn API",
                        version        = "1.0",
                        description    = "Social Media Checkin API",
                        security       = [API_AUTH_TOKEN_HEADER, API_ACCOUNT_TOKEN_HEADER,
                                          API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE],
                        authorizations = {
                            API_AUTH_TOKEN_HEADER: {"type": "apiKey", "in": "header", "name": API_AUTH_TOKEN_HEADER},
                            API_ACCOUNT_TOKEN_HEADER: {"type": "apiKey", "in": "header", "name": API_ACCOUNT_TOKEN_HEADER},
                            API_AUTH_TOKEN_COOKIE: {"type": "apiKey", "in": "cookie", "name": API_AUTH_TOKEN_COOKIE},
                            API_ACCOUNT_TOKEN_COOKIE: {"type": "apiKey", "in": "cookie", "name": API_ACCOUNT_TOKEN_COOKIE},
                        })

# Add all namespaces here. attach api to all namespaces so it is accessible during routes
api.add_namespace(defaultNS)
defaultNS.api = api
api.add_namespace(specNS)
specNS.api = api
api.add_namespace(constantNS)
constantNS.api = api
api.add_namespace(errorNS)
errorNS.api = api
api.add_namespace(platformNS)
platformNS.api = api
api.add_namespace(authNS)
authNS.api = api
api.add_namespace(accountNS)
accountNS.api = api
api.add_namespace(fakeNS)
fakeNS.api = api
api.add_namespace(unitNS)
unitNS.api = api
api.add_namespace(creditCardNS)
creditCardNS.api = api
api.add_namespace(cartNS)
cartNS.api = api
api.add_namespace(chargeNS)
chargeNS.api = api
api.add_namespace(profileGroupNS)
profileGroupNS.api = api
api.add_namespace(profileNS)
profileNS.api = api
api.add_namespace(syncNS)
syncNS.api = api
api.add_namespace(analysisNS)
analysisNS.api = api
api.add_namespace(statisticNS)
statisticNS.api = api
api.add_namespace(histogramNS)
histogramNS.api = api
api.add_namespace(textNS)
textNS.api = api
api.add_namespace(photoNS)
photoNS.api = api
api.add_namespace(linkNS)
linkNS.api = api
api.add_namespace(videoNS)
videoNS.api = api
api.add_namespace(facebookProfileNS)
facebookProfileNS.api = api
api.add_namespace(facebookPostNS)
facebookPostNS.api = api


# Register Error Handlers
registerErrorHandlers(api)
