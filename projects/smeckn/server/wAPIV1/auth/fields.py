#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Auth Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.gDB.account.model import AuthStatus


# ------------------------ FIELDS ------------------------------------------- #
class AuthStatusField(fields.String):
    """
    Authentication Status Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Authentication Status",
            description = "Authentication Status",
            example     = AuthStatus.LOGGED_IN.name,
            enum        = [authStatus.name for authStatus in list(AuthStatus)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, AuthStatus):
            return value.name
        else:
            return None

class ReCAPTCHATokenField(fields.String):
    """
    ReCAPTCHA Token Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "ReCAPTCHA Response Token",
            description = "ReCAPTCHA Response Token, returned in g-recaptcha-response",
            example     = "03ACgFB9uDQpkQnMORhKLCgzRDRPlpoPmuFzQwXQJbnJ4FiDo_47Ofx3dj2pQzkqB0xrrYDzh_ZAr8Yon3VFpCCZWBgXT8Gzp4OAtueIK-Do6ZK2KIcmA-qlYJN4Q3STiIkkZnT0gs_5CjZlQfx76cN888QG6_UWgQBIp9HMw7Eqkjct3dGBjEzy0H42Q99_bmu62xAEEQC5M-dMr_aHPItOA8jHaq3EEss-Y51-5ok3njsDDUYKXKepqccrzngxFlegyv-0pTVbv9K4jlpfjvLTZw4PGh2Y7JaZibPA1YjxsEkwl6j559PfexNrJfktaVGvbh6ypbt_rKcUkOwnTJ4-PtRXQYu5eBjbSc9Sf7THfCUk_sZkpF4b_7P8PuicE_f7bJwOLNPX64K5ijqT_XRNFlGmmW4wHiXmHLNlmVKsXnN3xS0I3ZCM4iJkPmMiu0B0n6QzrwiRPznXaQ_D0bqZCJCjCThm2BLjJi-0PfRWoVW1-9V5BU66rdxXu83DeOKx0IL_Sp-TfUHTvPS6fn4i1Mb5QTJHzh7yGTjNg_h1vGPj-9U0z_XAj5nKUQGBhs7TI1IrtjKgFUkiVHsX3Akjdi8nx_VZBWdcJb3O_fPsqmHMYDap1WpkX0v4HgL0FwuU53VQ4OagEyjzkwQVgVSCUtENDnIGcNAlFu9Io6RR57rDZnpMcYNzKCV0TjY5zky1Vcw2D6fA5i0iOGp1JDOezcyOHkIsEEeUSPczx7xJF5qrA728_QkRyoG2jqtqKKLswCyCtDrzTjIMR-pDpZb-zUfU8jUYNwESuXQCozmwHtukZX5b-SVJVZpRAPocUVTEcIHfhLmfYfkRnqlo--jEugxq0LemD-6yFWwtkWDOWB0DcHsItY3EedlEG73o7RGNrl1uYGNjjlN-ENDRshpYmVb4pjd79NsKVct8b59pAudS_o7sJDPy5U-nk2ztC2duVnL8GWh8VUpTfIpk7PeqGlkRzFrrGlVQ",
            required    = required,
            **kwargs)


class AuthJWTField(fields.String):
    """
    Auth JWT Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Authentication JWT",
            description = "Token to be presented on API Requests",
            example     = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o",
            required    = required,
            **kwargs)


class AccountJWTField(fields.String):
    """
    Account JWT Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Account JWT",
            description = "Token to be presented on API Requests",
            example     = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o",
            required    = required,
            **kwargs)


class CreateAccountJWTField(fields.String):
    """
    Create Account JWT Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Account JWT",
            description = "Token to be passed between Create Account and Confirm Account APIs",
            example     = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o",
            required    = required,
            **kwargs)


class ForgotPasswordJWTField(fields.String):
    """
    Forgot Password JWT Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Forgot Password JWT",
            description = "Token passed between Forgot Password and Reset Password APIs",
            example     = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o",
            required    = required,
            **kwargs)


class EmailField(fields.String):
    """
    Email Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Email",
            description = "Account Email Address",
            example     = "john.smith@gmail.com",
            required    = required,
            **kwargs)


class PasswordField(fields.String):
    """
    Password Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Password",
            description = "Account Password",
            example     = "pAsSwOrD123",
            required    = required,
            **kwargs)
