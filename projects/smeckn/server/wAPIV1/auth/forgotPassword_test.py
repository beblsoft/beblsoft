#!/usr/bin/env python3
"""
 NAME:
  forgotPassword_test.py

 DESCRIPTION
  Test Forgot Password Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class ForgotPasswordTestCase(CommonTestCase):
    """
    Forgot Password Test Case
    """

    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.email                   = self.adminEmail
        self.password                = self.adminPassword
        self.validForgotPasswordDict = {
            "email": self.email,
            "reCAPTCHAToken": self.cfg.RECAPTCHA_TEST_TOKEN,
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, _) = self.postJSON(self.routes.forgotPassword, self.validForgotPasswordDict)
        self.assertEqual(code, 200)

    @logFunc()
    def test_reCAPTCHAPassThrough(self):
        """
        Test recaptcha pass through
        """
        inData                   = self.validForgotPasswordDict.copy()
        inData["reCAPTCHAToken"] = self.cfg.RECAPTCHA_PASSTHROUGH_TOKEN
        (code, _)                = self.postJSON(self.routes.forgotPassword, inData)
        self.assertEqual(code, 200)

    @logFunc()
    def test_noData(self):
        """
        Test with no data
        """
        (code, _) = self.postJSON(self.routes.forgotPassword, {})
        self.assertEqual(code, 400)

    @logFunc()
    def test_noEmail(self):
        """
        Test with no email
        """
        inData = self.validForgotPasswordDict.copy()
        inData.pop("email")
        (code, _) = self.postJSON(self.routes.forgotPassword, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noReCAPTCHA(self):
        """
        Test with no ReCAPTCHA
        """
        inData = self.validForgotPasswordDict.copy()
        inData.pop("reCAPTCHAToken")
        (code, _) = self.postJSON(self.routes.forgotPassword, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badEmail(self):
        """
        Test with bad email
        """
        inData = self.validForgotPasswordDict.copy()
        inData["email"] = "noemailaccount@gmail.com"
        (code, data) = self.postJSON(self.routes.forgotPassword, inData)
        self.assertEqual(code, 404)
        self.assertEqual(data.get("code"), 1185)

    @logFunc()
    def test_badReCAPTCHA(self):
        """
        Test with bad ReCAPTCHA
        """
        with patch.dict(self.mgr.recaptcha.bReCAPTCHA.__dict__, serverSecret="badServerSecret") as _:
            # Now the "valid" payload should fail
            (code, data) = self.postJSON(self.routes.forgotPassword, self.validForgotPasswordDict)
            self.assertEqual(code, 401)
            self.assertEqual(data.get("code"), 1400)
