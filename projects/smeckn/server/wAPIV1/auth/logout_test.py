#!/usr/bin/env python3
"""
 NAME:
  logout_test.py

 DESCRIPTION
  Test Logout Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class LogoutTestCase(CommonTestCase):
    """
    Logout Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp()
        self.validLogoutDict = {"blah": "blah"}

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        inData = self.validLogoutDict.copy()
        resp   = self.postJSONFull(self.routes.logout, inData)
        data   = resp.data
        self.assertEqual(resp.code, 200)
        self.assertEqual(bool(data), False)

        # Auth Token Cookie
        authMorsel   = resp.cookie[API_AUTH_TOKEN_COOKIE]
        authToken    = authMorsel.value
        self.assertEqual(authToken, "")
        self.assertEqual(authMorsel["httponly"], True)
        self.assertEqual(authMorsel["samesite"], "Strict")
        self.assertEqual(authMorsel["secure"], True)
        self.assertEqual(float(authMorsel["max-age"]), 0)

        # Account Token Cookie
        accountMorsel = resp.cookie[API_ACCOUNT_TOKEN_COOKIE]
        accountToken = accountMorsel.value
        self.assertEqual(accountToken, "")
        self.assertEqual(accountMorsel["httponly"], True)
        self.assertEqual(accountMorsel["samesite"], "Strict")
        self.assertEqual(accountMorsel["secure"], True)
        self.assertEqual(float(accountMorsel["max-age"]), 0)
