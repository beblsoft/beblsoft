#!/usr/bin/env python3
"""
 NAME:
  login.py

 DESCRIPTION
  Login Route
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import timedelta
from flask import request, current_app
from flask_restplus import Resource, marshal  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.log.sLogger import SmecknLogger
from smeckn.server.wAPIV1.auth.models import loginOutputModel, loginInputModel
from smeckn.server.wAPIV1.auth.models import AUTH_TOKEN_EXP_DAYS_DEFAULT, ACCOUNT_TOKEN_EXP_DAYS_DEFAULT
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE
from smeckn.server.wAPIV1.auth import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ LOGIN RESOURCE ----------------------------------- #
# Login Workflow:
# --------------------------
# [Client] www.smeckn.com/<Login URL>
#          User inputs credentials, client forwards data to server.
# [Server] api.smeckn.com/v1/auth/Login
#          data {"email": <email>, "password": <password>}
#          Server verifies credentials, on success sents authentication cookies to Client
# [Client] Sends authentication cookies on all subsequent requests
# [Server] Protects API endpoints
@ns.route('/login')
class LoginResource(Resource):
    """
    Login Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(loginInputModel)
    @ns.response(200, 'Success', loginOutputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Login. On success set browser authentication cookies.
        """
        inData               = request.json
        email                = inData.get("email")
        password             = inData.get("password")
        authTokenExpDelta    = timedelta(days=inData.get("authTokenExpDays", AUTH_TOKEN_EXP_DAYS_DEFAULT))
        accountTokenExpDelta = timedelta(days=inData.get("accountTokenExpDays", ACCOUNT_TOKEN_EXP_DAYS_DEFAULT))
        gDB                  = current_app.mgr.gdb.gDB
        cfg                  = current_app.cfg
        api                  = ns.api

        # Validate reCAPTCHA
        current_app.mgr.recaptcha.bReCAPTCHA.validate(inData.get("reCAPTCHAToken"),
                                                      passThroughToken=cfg.RECAPTCHA_PASSTHROUGH_TOKEN)

        with gDB.sessionScope(commit=False) as gs:
            # Locate, verify account
            aModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)
            if aModel is None:
                raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_EMAIL)
            if not aModel.verifyPassword(password):
                raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_PASSWORD)

            # Create Auth and Account Tokens
            aDBSModel              = aModel.accountDBServer
            aAdmin                 = (aModel.type == AccountType.ADMIN)
            authToken              = current_app.mgr.jwt.auth.encodeJWT(aID=aModel.id, aAdmin=aAdmin,
                                                                        expDelta=authTokenExpDelta)
            accountToken           = current_app.mgr.jwt.account.encodeJWT(aID=aModel.id,
                                                                           readDB=aDBSModel.readDomain,
                                                                           writeDB=aDBSModel.writeDomain,
                                                                           expDelta=accountTokenExpDelta)

            # Create response with data and cookies
            browserRememberCookies = inData.get("browserRememberCookies")
            data                   = marshal({"account": aModel}, loginOutputModel)
            resp                   = api.make_response(data, code=200)
            resp.set_cookie(API_AUTH_TOKEN_COOKIE, authToken,
                            max_age=authTokenExpDelta.total_seconds() if browserRememberCookies else None,
                            domain=cfg.COOKIE_DOMAIN, httponly=True, samesite="Strict", secure=True)
            resp.set_cookie(API_ACCOUNT_TOKEN_COOKIE, accountToken,
                            max_age=accountTokenExpDelta.total_seconds() if browserRememberCookies else None,
                            domain=cfg.COOKIE_DOMAIN, httponly=True, samesite="Strict", secure=True)
            SmecknLogger.info(msg="aID={} Successfully logged In".format(aModel.id),
                              bLogCode=BeblsoftLogCode.AUTH_LOGIN_SUCCESS, aID=aModel.id, email=aModel.email)
            return resp
