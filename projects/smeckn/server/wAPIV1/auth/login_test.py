#!/usr/bin/env python3
"""
 NAME:
  login_test.py

 DESCRIPTION
  Test Login Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import timedelta
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.wAPIV1.account.models import verifyAccountModel
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE
from smeckn.server.wAPIV1.auth.models import AUTH_TOKEN_EXP_DAYS_DEFAULT, ACCOUNT_TOKEN_EXP_DAYS_DEFAULT


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class LoginTestCase(CommonTestCase):
    """
    Login Test Case
    """

    @logFunc()
    def setUp(self): #pylint: disable=W0221
        super().setUp(accounts=True)
        self.email               = self.emailList[0]
        self.password            = self.passwordList[0]
        self.validLoginDict      = {
            "email": self.email,
            "password": self.password,
            "reCAPTCHAToken": self.cfg.RECAPTCHA_TEST_TOKEN,
            "browserRememberCookies": True,
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validDoRememberCookies(self):
        """
        Valid Test
        """
        inData                           = self.validLoginDict.copy()
        inData["browserRememberCookies"] = True
        resp                             = self.postJSONFull(self.routes.login, inData)
        data                             = resp.data
        self.assertEqual(resp.code, 200)

        # Auth Token Cookie
        authMorsel   = resp.cookie[API_AUTH_TOKEN_COOKIE]
        authToken    = authMorsel.value
        authAD       = self.mgr.jwt.auth.decodeJWT(authToken)
        self.assertEqual(authMorsel["httponly"], True)
        self.assertEqual(authMorsel["samesite"], "Strict")
        self.assertEqual(authMorsel["secure"], True)
        self.assertEqual(float(authMorsel["max-age"]), timedelta(days=AUTH_TOKEN_EXP_DAYS_DEFAULT).total_seconds())

        # Account Token Cookie
        accountMorsel= resp.cookie[API_ACCOUNT_TOKEN_COOKIE]
        accountToken = accountMorsel.value
        accountAD    = self.mgr.jwt.account.decodeJWT(accountToken)
        self.assertEqual(accountMorsel["httponly"], True)
        self.assertEqual(accountMorsel["samesite"], "Strict")
        self.assertEqual(accountMorsel["secure"], True)
        self.assertEqual(float(accountMorsel["max-age"]), timedelta(days=ACCOUNT_TOKEN_EXP_DAYS_DEFAULT).total_seconds())

        # Verify with database
        with self.gDB.sessionScope(commit=False) as gs:
            accountModel = AccountDAO.getByEmail(gs=gs, email=self.email)
            aDBSModel    = accountModel.accountDBServer
            # Check Auth Token
            self.assertEqual(authAD.aID, accountModel.id)
            self.assertFalse(authAD.aAdmin)
            # Check Account Token
            self.assertEqual(accountAD.aID, accountModel.id)
            self.assertEqual(accountAD.readDB, aDBSModel.readDomain)
            self.assertEqual(accountAD.readDB, aDBSModel.writeDomain)
            # Check Account Model
            self.assertEqual(accountModel.email, self.email)
            self.assertTrue(accountModel.verifyPassword(self.password))
        with self.gDB.sessionScope(commit=False) as gs:
            verifyAccountModel(gs=gs, apiModel=data.get("account"), aID=data.get("account").get("id"))

    @logFunc()
    def test_validDontRememberCookies(self):
        """
        Valid test not remembering cookies
        """
        inData                           = self.validLoginDict.copy()
        inData["browserRememberCookies"] = False
        resp                             = self.postJSONFull(self.routes.login, inData)
        self.assertEqual(resp.code, 200)

        # Auth Token Cookie
        authMorsel   = resp.cookie[API_AUTH_TOKEN_COOKIE]
        self.assertEqual(authMorsel["max-age"], "")

        # Account Token Cookie
        accountMorsel= resp.cookie[API_ACCOUNT_TOKEN_COOKIE]
        self.assertEqual(accountMorsel["max-age"], "")

    @logFunc()
    def test_reCAPTCHAPassThrough(self):
        """
        Test recaptcha pass through
        """
        inData                   = self.validLoginDict.copy()
        inData["reCAPTCHAToken"] = self.cfg.RECAPTCHA_PASSTHROUGH_TOKEN
        (code, _)                = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 200)

    @logFunc()
    def test_noData(self):
        """
        Test with no data
        """
        (code, _) = self.postJSON(self.routes.login, {})
        self.assertEqual(code, 400)

    @logFunc()
    def test_noEmail(self):
        """
        Test with no email
        """
        inData = self.validLoginDict.copy()
        inData.pop("email")
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noPassword(self):
        """
        Test with no password
        """
        inData = self.validLoginDict.copy()
        inData.pop("password")
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noReCAPTCHA(self):
        """
        Test with no ReCAPTCHA
        """
        inData = self.validLoginDict.copy()
        inData.pop("reCAPTCHAToken")
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badEmail(self):
        """
        Test with bad email
        """
        inData = self.validLoginDict.copy()
        inData["email"] = "foo@gmail.com"
        (code, data) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1182)

    @logFunc()
    def test_badPassword(self):
        """
        Test with bad password
        """
        inData = self.validLoginDict.copy()
        inData["password"] = "asdflkadsjhf"
        (code, data) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1183)

    @logFunc()
    def test_badReCAPTCHA(self):
        """
        Test with bad ReCAPTCHA
        """
        with patch.dict(self.mgr.recaptcha.bReCAPTCHA.__dict__, serverSecret="badServerSecret") as _:
            # Now our "validLoginDict" should fail
            inData = self.validLoginDict.copy()
            (code, data) = self.postJSON(self.routes.login, inData)
            self.assertEqual(code, 401)
            self.assertEqual(data.get("code"), 1400)

    @logFunc()
    def test_badBrowserRememberCookies(self):
        """
        Test with bad browser remeber cookies
        """
        for val in [1, "sadflkj"]:
            inData = self.validLoginDict.copy()
            inData["browserRememberCookies"] = val
            (code, _) = self.postJSON(self.routes.login, inData)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badAuthTokenExp(self):
        """
        Test with bad auth token expiration
        """
        inData = self.validLoginDict.copy()
        inData["authTokenExpDays"] = 31
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

        inData["authTokenExpDays"] = -1
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badAccountTokenExp(self):
        """
        Test with bad account token expiration
        """
        inData = self.validLoginDict.copy()
        inData["accountTokenExpDays"] = 31
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)

        inData["accountTokenExpDays"] = -1
        (code, _) = self.postJSON(self.routes.login, inData)
        self.assertEqual(code, 400)
