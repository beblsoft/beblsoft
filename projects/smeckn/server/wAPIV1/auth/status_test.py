#!/usr/bin/env python3
"""
 NAME:
  status_test.py

 DESCRIPTION
  Test Auth Status Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.account.model import AuthStatus
from smeckn.server.wAPIV1.auth.models import verifyAuthStatusModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class AuthStatusTestCase(CommonTestCase):
    """
    Auth Status Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validAuthStatusDict = {"Foo": "Bar"}

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validLoggedIn(self):
        """
        Valid User Logged In
        """
        runConfigList = [
            # All successful combinations of header and cookie
            {"authHeader": True, "accountHeader": True},
            {"authHeader": True, "accountCookie": True},
            {"authCookie": True, "accountHeader": True},
            {"authCookie": True, "accountCookie": True}
        ]

        for runConfig in runConfigList:
            postKwargs = {}
            # Auth
            if runConfig.get("authHeader", False):
                postKwargs["authToken"] = self.popAuthToken
            if runConfig.get("authCookie", False):
                self.setTokenCookies(authToken=self.popAuthToken)
            # Account
            if runConfig.get("accountHeader", False):
                postKwargs["accountToken"] = self.popAccountToken
            if runConfig.get("accountCookie", False):
                self.setTokenCookies(accountToken=self.popAccountToken)

            resp = self.postJSONFull(self.routes.authStatus, self.validAuthStatusDict, **postKwargs)
            data = resp.data
            self.assertEqual(resp.code, 200)
            self.assertEqual(data.get("status"), AuthStatus.LOGGED_IN.name)
            with self.gDB.sessionScope(commit=False) as gs:
                verifyAuthStatusModel(gs=gs, apiModel=data, aID=self.popAccountID)

            self.deleteTokenCookies()

    @logFunc()
    def test_validLoggedOut(self):  # pylint: disable=R0912
        """
        Valid User Logged Out
        """
        runConfigList = [
            # No Tokens
            {"code": 400},

            # One Token Non-existent
            {"authHeader": True, "code": 400},
            {"authCookie": True, "code": 400},
            {"accountHeader": True, "code": 400},
            {"accountCookie": True, "code": 400},

            # Good Auth Token -------------------------------------------------
            # Bad Account
            {"authHeader": True, "badAccountHeader": True},
            {"authHeader": True, "badAccountCookie": True},
            {"authCookie": True, "badAccountHeader": True},
            {"authCookie": True, "badAccountCookie": True},

            # Wrong Account
            {"authHeader": True, "wrongAccountHeader": True},
            {"authHeader": True, "wrongAccountCookie": True},
            {"authCookie": True, "wrongAccountHeader": True},
            {"authCookie": True, "wrongAccountCookie": True},

            # Good Account Token ----------------------------------------------
            # Bad Auth
            {"badAuthHeader": True, "accountHeader": True},
            {"badAuthHeader": True, "accountCookie": True},
            {"badAuthCookie": True, "accountHeader": True},
            {"badAuthCookie": True, "accountCookie": True},

            # Wrong Auth
            {"wrongAuthHeader": True, "accountHeader": True},
            {"wrongAuthHeader": True, "accountCookie": True},
            {"wrongAuthCookie": True, "accountHeader": True},
            {"wrongAuthCookie": True, "accountCookie": True},
        ]

        for runConfig in runConfigList:
            postKwargs = {}
            # Auth Tokens -----------------------------------------------------
            # Good Auth
            if runConfig.get("authHeader", False):
                postKwargs["authToken"] = self.popAuthToken
            if runConfig.get("authCookie", False):
                self.setTokenCookies(authToken=self.popAuthToken)

            # Bad Auth
            if runConfig.get("badAuthHeader", False):
                postKwargs["authToken"] = "asdflkjsadhfp09asdflakdsjhf"
            if runConfig.get("badAuthCookie", False):
                self.setTokenCookies(authToken="asdflkjsadhfp09asdflakdsjhfuntToken")

            # Wrong Auth
            if runConfig.get("wrongAuthHeader", False):
                postKwargs["authToken"] = self.nonPopAuthToken
            if runConfig.get("wrongAuthCookie", False):
                self.setTokenCookies(authToken=self.nonPopAuthToken)

            # Account Tokens --------------------------------------------------
            # Good Account
            if runConfig.get("accountHeader", False):
                postKwargs["accountToken"] = self.popAccountToken
            if runConfig.get("accountCookie", False):
                self.setTokenCookies(accountToken=self.popAccountToken)

            # Bad Account
            if runConfig.get("badAccountHeader", False):
                postKwargs["accountToken"] = "asdflkjsadhfp09asdflakdsjhfuntToken"
            if runConfig.get("badAccountCookie", False):
                self.setTokenCookies(accountToken="asdflkjsadhfp09asdflakdsjhfuntToken")

            # Wrong Account
            if runConfig.get("wrongAccountHeader", False):
                postKwargs["accountToken"] = self.nonPopAccountToken
            if runConfig.get("wrongAccountCookie", False):
                self.setTokenCookies(accountToken=self.nonPopAccountToken)

            resp = self.postJSONFull(self.routes.authStatus, self.validAuthStatusDict, **postKwargs)
            data = resp.data
            self.assertEqual(resp.code, 200)
            self.assertEqual(data.get("status"), AuthStatus.LOGGED_OUT.name)
            self.assertEqual(data.get("account"), None)
            self.deleteTokenCookies()
