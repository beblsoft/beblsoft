#!/usr/bin/env python3
"""
 NAME:
  status.py

 DESCRIPTION
  Status Route
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.wAPIV1.auth import ns
from smeckn.server.gDB.account.model import AuthStatus
from smeckn.server.wAPIV1.auth.models import authStatusOutputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.common.auth import verifyAccessFunc
from smeckn.server.wAPIV1.common.auth import getAccountAD


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ STATUS RESOURCE ---------------------------------- #
@ns.route('/status')
class StatusResource(Resource):
    """
    Status Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.marshal_with(authStatusOutputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Get authentication status
        """
        gDB    = current_app.mgr.gdb.gDB
        status = None
        aModel = None

        try:
            verifyAccessFunc()
            accountAD = getAccountAD()
            with gDB.sessionScope(commit=False) as gs:
                aModel = AccountDAO.getByID(gs=gs, aID=accountAD.aID)
        except BeblsoftError as _:
            status = AuthStatus.LOGGED_OUT
        else:
            status = AuthStatus.LOGGED_IN

        return {
            "status": status,
            "account": aModel
        }
