#!/usr/bin/env python3
"""
 NAME:
  resetPassword_test.py

 DESCRIPTION
  Test Reset Password Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.account.dao import AccountDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class ResetPasswordTestCase(CommonTestCase):
    """
    Reset Password Test Case
    """

    @logFunc()
    def setUp(self): #pylint: disable=W0221
        super().setUp(accounts=True)
        self.aID                    = self.accountIDList[0]
        self.email                  = self.emailList[0]
        self.oldPassword            = self.passwordList[0]
        self.newPassword            = "0123asdf1211111111"
        self.forgotPasswordJWT      = self.mgr.jwt.forgotPassword.encodeJWT(
                aID                   = self.aID)
        self.validResetPasswordDict = {
            "forgotPasswordToken": self.forgotPasswordJWT,
            "password": self.newPassword,
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, _) = self.postJSON(self.routes.resetPassword,
                                  self.validResetPasswordDict)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=self.email)
            self.assertTrue(aModel.verifyPassword(self.newPassword))

    @logFunc()
    def test_noData(self):
        """
        Test with no data
        """
        (code, _) = self.postJSON(self.routes.resetPassword, {})
        self.assertEqual(code, 400)

    @logFunc()
    def test_noToken(self):
        """
        Test with no token
        """
        inData = self.validResetPasswordDict.copy()
        inData.pop("forgotPasswordToken")
        (code, _) = self.postJSON(self.routes.resetPassword, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noPassword(self):
        """
        Test with no password
        """
        inData = self.validResetPasswordDict.copy()
        inData.pop("password")
        (code, _) = self.postJSON(self.routes.resetPassword, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badToken(self):
        """
        Test with bad token
        """
        inData = self.validResetPasswordDict.copy()
        inData["forgotPasswordToken"] = "1204972435skdfhade832"
        (code, data) = self.postJSON(self.routes.resetPassword, inData)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1142)
