#!/usr/bin/env python3
"""
 NAME:
  confirmAccount.py

 DESCRIPTION
  Account Confirmation Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.manager import AccountManager
from smeckn.server.wAPIV1.auth.models import confirmAccountInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.auth import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CONFIRM ACCOUNT RESOURCE ------------------------- #
# See CreateAccount Workflow.
@ns.route('/confirmAccount')
class ConfirmAccountResource(Resource):
    """
    Confirm Account Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(confirmAccountInputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Confirm an account
        """
        inData = request.json
        token  = inData.get("createAccountToken")
        gDB    = current_app.mgr.gdb.gDB
        cfg    = current_app.mgr.cfg

        # Decode JWT
        createAccountAD = current_app.mgr.jwt.createAccount.decodeJWT(token)

        # Add account to database
        AccountManager.create(gDB=gDB, aDBNamePrefix=cfg.ADB_NAME_PREFIX,
                              email=createAccountAD.email, password=createAccountAD.password,
                              aType=createAccountAD.aType)
