#!/usr/bin/env python3
"""
 NAME:
  createAccount_test.py

 DESCRIPTION
  Test Create Account Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from unittest.mock import patch
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class CreateAccountTestCase(CommonTestCase):
    """
    Create Account Test Case
    """

    def setUp(self):  # pylint: disable=W0221
        super().setUp()
        self.email                  = "beblsofttest@gmail.com"  # Must be valid email
        self.password               = "0123456789"
        self.validCreateAccountDict = {
            "email": self.email,
            "password": self.password,
            "reCAPTCHAToken": self.cfg.RECAPTCHA_TEST_TOKEN,
        }
        self.badEmailList           = [
            "plainaddress",
            "#@%^%#$@#$@#.com",
            "@domain.com",
            "Joe Smith <email@domain.com>",
            "email.domain.com",
            "email@domain@domain.com",
            ".email@domain.com",
            "email.@domain.com",
            "email..email@domain.com",
            "email@domain.com (Joe Smith)",
            "email@domain",
            "email@-domain.com",
            "email@domain.web",
            "email@111.222.333.44444",
            "email@domain..com"
        ]

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, _) = self.postJSON(self.routes.createAccount, self.validCreateAccountDict)
        self.assertEqual(code, 200)

    @logFunc()
    def test_reCAPTCHAPassThrough(self):
        """
        Test recaptcha pass through
        """
        inData                   = self.validCreateAccountDict.copy()
        inData["reCAPTCHAToken"] = self.cfg.RECAPTCHA_PASSTHROUGH_TOKEN
        (code, _)                = self.postJSON(self.routes.createAccount, inData)
        self.assertEqual(code, 200)

    @logFunc()
    def test_noData(self):
        """
        Test with no data
        """
        (code, _) = self.postJSON(self.routes.createAccount, {})
        self.assertEqual(code, 400)

    @logFunc()
    def test_noEmail(self):
        """
        Test with no email
        """
        inData = self.validCreateAccountDict.copy()
        inData.pop("email")
        (code, _) = self.postJSON(self.routes.createAccount, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noPassword(self):
        """
        Test with no password
        """
        inData = self.validCreateAccountDict.copy()
        inData.pop("password")
        (code, _) = self.postJSON(self.routes.createAccount, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_noReCAPTCHA(self):
        """
        Test with no ReCAPTCHA
        """
        inData = self.validCreateAccountDict.copy()
        inData.pop("reCAPTCHAToken")
        (code, _) = self.postJSON(self.routes.createAccount, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badEmail(self):
        """
        Test with bad email
        """
        for email in self.badEmailList:
            inData = self.validCreateAccountDict.copy()
            inData["email"] = email
            (code, data) = self.postJSON(self.routes.createAccount, inData)
            self.assertEqual(code, 401,
                             "InvalidEmail={} was validated".format(email))
            self.assertEqual(data.get("code"), 1182,
                             "InvalidEmail={} was validated".format(email))

    @logFunc()
    def test_badReCAPTCHA(self):
        """
        Test with bad ReCAPTCHA
        """
        with patch.dict(self.mgr.recaptcha.bReCAPTCHA.__dict__, serverSecret="badServerSecret") as _:
            # Now the "valid" payload should fail
            (code, data) = self.postJSON(
                self.routes.createAccount, self.validCreateAccountDict)
            self.assertEqual(code, 401)
            self.assertEqual(data.get("code"), 1400)
