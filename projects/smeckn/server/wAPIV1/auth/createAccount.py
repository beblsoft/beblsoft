#!/usr/bin/env python3
"""
 NAME:
  createAccount.py

 DESCRIPTION
  Account Creation Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from flask import current_app, request
from flask_restplus import Resource
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType
from smeckn.server.wAPIV1.auth.models import createAccountInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.auth import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CREATE ACCOUNT RESOURCE -------------------------- #
# Create Account Workflow:
# --------------------------
# [Client] www.smeckn.com/<CreateAccount URL>
#          User inputs credentials, client posts data to server
# [Server] app.smeckn.com/v1/auth/CreateAccount
#          data {"email": <email>, "password": <password>}
#          Server sends client email with CreateAccountJWT
#          CreateAccountJWT encodes email, password
# [Client] Clicks link in email
#          www.smeckn.com/<ConfirmAccount URL>
#          Client immediately forwards token to server
# [Server] app.smeckn.com/v1/auth/ConfirmAccount
#          data {"token":<token>}
#          Server decodes CreateAccountJWT, adds user to database
@ns.route('/createAccount')
class CreateAccountResource(Resource):
    """
    Create Account Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(createAccountInputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create an account, send user email to verify email address
        """
        inData   = request.json
        email    = inData.get("email")
        password = inData.get("password")
        gDB      = current_app.mgr.gdb.gDB
        cfg      = current_app.cfg

        # Validate reCAPTCHA, email
        current_app.mgr.recaptcha.bReCAPTCHA.validate(inData.get("reCAPTCHAToken"),
                                                      passThroughToken=cfg.RECAPTCHA_PASSTHROUGH_TOKEN)
        current_app.mgr.email.validateAddress(address=email)

        # Check to See the Account Doesn't Exist
        with gDB.sessionScope(commit=False) as gs:
            accountModel = AccountDAO.getByEmail(gs=gs, email=email, raiseOnNone=False)
            if accountModel is not None:
                raise BeblsoftError(code=BeblsoftErrorCode.AUTH_EMAIL_ALREADY_EXISTS)

        # Make Create Account JWT
        token = current_app.mgr.jwt.createAccount.encodeJWT(email=email,
                                                            password=password,
                                                            aType=AccountType.REGULAR)

        # Create Email HTML From Template
        templatePath = os.path.join(cfg.CONST.dirs.emailHTML, 'confirm-account.html')
        templateStr  = current_app.mgr.email.renderTemplate(
            templatePath         = templatePath,
            CREATE_ACCOUNT_TOKEN = token)

        # Send Confirmation Email
        current_app.mgr.email.sendEmail(
            sourcePrefix  = "donotreply",
            subject       = "Confirm Smeckn Account",
            htmlBody      = templateStr,
            toAddressList = [email])
