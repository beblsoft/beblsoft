#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Auth Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.account.models import accountModel, verifyAccountModel
from smeckn.server.wAPIV1.auth.fields import AuthStatusField, ReCAPTCHATokenField
from smeckn.server.wAPIV1.auth.fields import PasswordField, EmailField
from smeckn.server.wAPIV1.auth.fields import CreateAccountJWTField, ForgotPasswordJWTField
from smeckn.server.gDB.account.model import AuthStatus
from smeckn.server.wAPIV1.auth import ns


# ------------------------ CONSTANTS ---------------------------------------- #
AUTH_TOKEN_EXP_DAYS_DEFAULT = 2
ACCOUNT_TOKEN_EXP_DAYS_DEFAULT = 2


# ------------------------ MODELS ------------------------------------------- #
loginInputModel = ns.model("LoginInputModel", {
    "email": EmailField(
        required    = True),
    "password": PasswordField(
        required    = True),
    "reCAPTCHAToken": ReCAPTCHATokenField(
        required    = True),
    "browserRememberCookies": fields.Boolean(
        required    = False,
        title       = "Browser Remember Cookies",
        descrption  = "If True, Browser will remember cookies. If False, cookies will NOT persist past a browser session",
        default     = False,
        example     = False),
    "authTokenExpDays": fields.Integer(
        required    = False,
        title       = "Auth Token Expiration in Days",
        min         = 1,
        max         = 30,
        default     = AUTH_TOKEN_EXP_DAYS_DEFAULT,
        example     = 5),
    "accountTokenExpDays": fields.Integer(
        required    = False,
        title       = "Account Token Expiration in Days",
        min         = 1,
        max         = 30,
        default     = ACCOUNT_TOKEN_EXP_DAYS_DEFAULT,
        example     = 5)
})

loginOutputModel = ns.model("LoginOutputModel", {
    "account": fields.Nested(accountModel),
})

authStatusOutputModel = ns.model("AuthStatusOutputModel", {
    "status": AuthStatusField(),
    "account": fields.Nested(accountModel, allow_null=True),
})

createAccountInputModel = ns.model("CreateAccountInputModel", {
    "email": EmailField(),
    "password": PasswordField(),
    "reCAPTCHAToken": ReCAPTCHATokenField(),
})

confirmAccountInputModel = ns.model("ConfirmAccountInputModel", {
    "createAccountToken": CreateAccountJWTField(),
})

forgotPasswordInputModel = ns.model("ForgotPasswordInputModel", {
    "email": EmailField(),
    "reCAPTCHAToken": ReCAPTCHATokenField(),
})

resetPasswordInputModel = ns.model("ResetPasswordInputModel", {
    "forgotPasswordToken": ForgotPasswordJWTField(),
    "password": PasswordField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyAuthStatusModel(gs, apiModel, aID):
    """
    Verify auth status model
    Args
      apiModel:
        auth status model returned from API
      aID:
        account database id
    """
    assert AuthStatus[apiModel.get("status")] in list(AuthStatus)
    verifyAccountModel(gs=gs, apiModel=apiModel.get("account"), aID=aID)
