#!/usr/bin/env python3
"""
 NAME:
  logout.py

 DESCRIPTION
  Logout Route
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from smeckn.server.log.sLogger import SmecknLogger
from smeckn.server.wAPIV1.auth import ns
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_COOKIE, API_ACCOUNT_TOKEN_COOKIE


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ LOGOUT RESOURCE ---------------------------------- #
@ns.route('/logout')
class LogoutResource(Resource):
    """
    Logout Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.response(200, 'Success')
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Logout. Clear browser authentication cookies.
        """
        cfg  = current_app.cfg
        resp = ns.api.make_response({}, code=200)
        resp.set_cookie(API_AUTH_TOKEN_COOKIE, "", max_age=0,
                        domain=cfg.COOKIE_DOMAIN, httponly=True, samesite="Strict", secure=True)
        resp.set_cookie(API_ACCOUNT_TOKEN_COOKIE, "", max_age=0,
                        domain=cfg.COOKIE_DOMAIN, httponly=True, samesite="Strict", secure=True)
        SmecknLogger.info(bLogCode=BeblsoftLogCode.AUTH_LOGOUT_SUCCESS)
        return resp
