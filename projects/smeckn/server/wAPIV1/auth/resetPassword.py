#!/usr/bin/env python3
"""
 NAME:
  auth.py

 DESCRIPTION
  Authorization Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request, current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.wAPIV1.auth.models import resetPasswordInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.auth import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ RESET PASSWORD RESOURCE -------------------------- #
# See forgot password workflow
@ns.route('/resetPassword')
class ResetPasswordResource(Resource):
    """
    Reset Password Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(resetPasswordInputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Complete reset password process
        """
        inData   = request.json
        token    = inData.get("forgotPasswordToken")
        password = inData.get("password")
        gDB      = current_app.mgr.gdb.gDB

        # Decode JWT
        forgotPasswordAD = current_app.mgr.jwt.forgotPassword.decodeJWT(token)

        # Update account password
        with gDB.sessionScope() as gs:
            AccountDAO.updateByID(gs=gs, aID=forgotPasswordAD.aID, password=password)
