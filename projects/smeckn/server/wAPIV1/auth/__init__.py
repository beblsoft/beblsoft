#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Authorization Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('auth', description="Authorization Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .createAccount import *  #pylint: disable=C0413,W0401
from .confirmAccount import *  #pylint: disable=C0413,W0401
from .login import *  #pylint: disable=C0413,W0401
from .logout import *  #pylint: disable=C0413,W0401
from .status import *  #pylint: disable=C0413,W0401
from .forgotPassword import *  #pylint: disable=C0413,W0401
from .resetPassword import *  #pylint: disable=C0413,W0401
