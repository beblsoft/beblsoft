#!/usr/bin/env python3
"""
 NAME:
  confirmAccount_test.py

 DESCRIPTION
  Test Confirm Account Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.gDB.account.model import AccountType


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CLASSES -------------------------------------- #
class ConfirmAccountTestCase(CommonTestCase):
    """
    Confirm Account Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp()
        self.email                   = "foo@gmail.com"
        self.password                = "0123456789"
        self.aType                   = AccountType.REGULAR
        self.createAccountToken      = self.mgr.jwt.createAccount.encodeJWT(self.email, self.password,
                                                                            aType=self.aType)
        self.validConfirmAccountDict = {
            "createAccountToken": self.createAccountToken
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Test
        """
        (code, _) = self.postJSON(self.routes.confirmAccount, self.validConfirmAccountDict)
        self.assertEqual(code, 200)
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=self.email)
            self.assertNotEqual(aModel, None)
            self.assertEqual(aModel.email, self.email)
            self.assertEqual(aModel.type, self.aType)
            self.assertTrue(aModel.verifyPassword(self.password))

    @logFunc()
    def test_noToken(self):
        """
        Test with no data
        """
        inData = self.validConfirmAccountDict.copy()
        inData.pop("createAccountToken")
        (code, _) = self.postJSON(self.routes.confirmAccount, inData)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badToken(self):
        """
        Test with bad token
        """
        inData = self.validConfirmAccountDict.copy()
        inData["createAccountToken"] = "ads928rudf,0234"
        (code, data) = self.postJSON(self.routes.confirmAccount, inData)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1142)
