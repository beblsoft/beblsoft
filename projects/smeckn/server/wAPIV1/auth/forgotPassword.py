#!/usr/bin/env python3
"""
 NAME:
  auth.py

 DESCRIPTION
  Authorization Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.gDB.account.dao import AccountDAO
from smeckn.server.wAPIV1.auth.models import forgotPasswordInputModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.auth import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ FORGOT PASSWORD RESOURCE ------------------------- #
# Forgot Password Workflow:
# --------------------------
# [Client] www.smeckn.com/<Forgot Password URL>
#          User inputs email, client forwards data to server
# [Server] app.smeckn.com/v1/ForgotPassword
#          data {"email": <email>}
#          Server sends client email with ForgotPasswordJWT
# [Client] Clicks link in email
#          www.smeckn.com/<Reset Password URL>
#          User enters new password, client forwards token and password to server
# [Server] app.smeckn.com/v1/ResetPassword
#          data {"token":<token>, "password":<password>}
#          Server decodes ForgotPasswordJWT, updates the user's password
@ns.route('/forgotPassword')
class ForgotPasswordResource(Resource):
    """
    Forgot Password Route
    """
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(forgotPasswordInputModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Begin reset password process, send user email with token
        """
        inData = request.json
        email  = inData.get("email")
        gDB    = current_app.mgr.gdb.gDB
        cfg    = current_app.cfg

        # Validate reCAPTCHA, email
        current_app.mgr.recaptcha.bReCAPTCHA.validate(inData.get("reCAPTCHAToken"),
                                                      passThroughToken=cfg.RECAPTCHA_PASSTHROUGH_TOKEN)
        with gDB.sessionScope(commit=False) as gs:
            # Load account, create jwt
            aModel = AccountDAO.getByEmail(gs=gs, email=email)
            token  = current_app.mgr.jwt.forgotPassword.encodeJWT(aID=aModel.id)

            # Create Email HTML From Template
            templatePath = os.path.join(cfg.CONST.dirs.emailHTML, 'reset-password.html')
            templateStr  = current_app.mgr.email.renderTemplate(
                templatePath          = templatePath,
                FORGOT_PASSWORD_TOKEN = token)

            # Send Confirmation Email
            current_app.mgr.email.sendEmail(
                sourcePrefix  = "donotreply",
                subject       = "Reset Smeckn Account Password",
                htmlBody      = templateStr,
                toAddressList = [email])
