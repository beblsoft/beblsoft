#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Comp Lang Analysis Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.compLang.models import compLangModel, ScoreField
from smeckn.server.wAPIV1.compLangAnalysis.fields import SMIDField, CompLangAnalysisStatusField
from smeckn.server.wAPIV1.common.fields import BLanguageTypeField
from smeckn.server.wAPIV1.compLang.models import verifyCompLangModel
from smeckn.server.wAPIV1.default import ns
from smeckn.server.aDB.compLangAnalysis.dao import CompLangAnalysisDAO


# ------------------------ MODELS ------------------------------------------- #
compLangAnalysisModel = ns.model("CompLangAnalysisModel", {
    "smID": SMIDField(),
    "status": CompLangAnalysisStatusField(),
    "compLangModelList": fields.List(fields.Nested(compLangModel)),
    "dominantBLanguageType": BLanguageTypeField(),
    "dominantBLanguageScore": ScoreField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyCompLangAnalysisModel(aDBS, apiModel, smID):
    """
    Verify comp lang analysis model
    """
    _compLangAnalysisModel = CompLangAnalysisDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _compLangAnalysisModel.smID
    assert apiModel.get("status") == _compLangAnalysisModel.status.name
    if _compLangAnalysisModel.dominantBLanguageType:
        assert apiModel.get("dominantBLanguageType") == _compLangAnalysisModel.dominantBLanguageType.name
    else:
        assert apiModel.get("dominantBLanguageType") == _compLangAnalysisModel.dominantBLanguageType
    assert apiModel.get("dominantBLanguageScore") == _compLangAnalysisModel.dominantBLanguageScore

    for _compLangModel in apiModel.get("compLangModelList"):
        verifyCompLangModel(aDBS=aDBS, apiModel=_compLangModel, smID=_compLangModel.get("smID"))
