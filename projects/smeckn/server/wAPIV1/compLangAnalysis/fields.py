#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Comprehend Language Analysis Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.compLangAnalysis.model import CompLangAnalysisStatus


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class CompLangAnalysisStatusField(fields.String):
    """
    Comprehend Language Analysis Status Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Comprehend Language Analysis Status",
            description = "Comprehend Language Analysis Status",
            example     = CompLangAnalysisStatus.SUCCESS.name,
            enum        = [str(compLangAnalysisStatus.name) for compLangAnalysisStatus in list(CompLangAnalysisStatus)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, CompLangAnalysisStatus):
            return value.name
        else:
            return None
