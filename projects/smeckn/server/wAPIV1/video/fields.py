#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Video Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class SMIDField(fields.Integer):
    """
    SMID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "SMID",
            description = "Smeckn Identification Number",
            example     = 398,
            required    = required,
            **kwargs)

class CreateDateField(fields.DateTime):
    """
    Create Date Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Create Date",
            description = "Create Date",
            dt_format   = "iso8601",
            required    = required,
            **kwargs)

class ImageURLField(fields.String):
    """
    Image URL Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Image URL",
            description = "Image URL",
            example     = "https://www.gstatic.com/webp/gallery/1.sm.jpg",
            required    = required,
            **kwargs)

class VideoURLField(fields.String):
    """
    Video URL Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Link URL",
            description = "Link URL",
            example     = "https:www.disney.com",
            required    = required,
            **kwargs)

class LengthSecField(fields.Integer):
    """
    Length Sec Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Length in seconds",
            description = "Length in seconds",
            example     = 398,
            required    = required,
            **kwargs)
