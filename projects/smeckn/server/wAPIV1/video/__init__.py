#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Video Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('video', description="Video Operations")
