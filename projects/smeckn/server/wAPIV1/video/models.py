#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Video Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.video.fields import SMIDField, CreateDateField
from smeckn.server.wAPIV1.video.fields import ImageURLField, VideoURLField, LengthSecField
from smeckn.server.wAPIV1.video import ns
from smeckn.server.aDB.video.dao import VideoDAO


# ------------------------ MODELS ------------------------------------------- #
videoModel = ns.model("VideoModel", {
    "smID": SMIDField(),
    "createDate": CreateDateField(),
    "imageURL": ImageURLField(),
    "videoURL": VideoURLField(),
    "lengthSec": LengthSecField(),
})

# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyVideoModel(aDBS, apiModel, smID):
    """
    Verify video model
    """
    _videoModel = VideoDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _videoModel.smID
    assert apiModel.get("createDate") == _videoModel.createDate.isoformat()
    assert apiModel.get("imageURL") == _videoModel.imageURL
    assert apiModel.get("videoURL") == _videoModel.videoURL
    assert apiModel.get("lengthSec") == _videoModel.lengthSec
