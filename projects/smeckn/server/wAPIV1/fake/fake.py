#!/usr/bin/env python3
"""
 NAME:
  fake.py

 DESCRIPTION
  Fake Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import request, current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc

from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.fake.models import fakeModel, fakeDeleteParser
from smeckn.server.wAPIV1.fake.models import fakePostInputModel, fakePostInputModelSetUpKeys
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.fake import ns
from smeckn.server.fake.data import FakeData


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ FAKE RESOURCE ------------------------------------ #
@ns.route("")
class FakeResource(Resource):
    """
    Fake Routes
    """

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.expect(fakePostInputModel)
    @ns.marshal_with(fakeModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        SetUp fake data

        Only allow TEST accounts
        """
        inData      = request.json
        mgr         = current_app.mgr
        fakeData    = FakeData(mgr=mgr)
        email       = inData.get("accountEmail")
        password    = inData.get("accountPassword")
        setUpKwargs = {setUpKey: inData.get(setUpKey) for setUpKey in fakePostInputModelSetUpKeys}

        fakeData.tearDownAccount(email=email, ensureTestAccount=True)
        fakeData.setUpAccount(email=email, password=password)
        fakeData.setUp(**setUpKwargs)
        return fakeData

    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.expect(fakeDeleteParser)
    @logFunc()
    def delete(self):  # pylint: disable=R0201
        """
        TearDown fake data

        Only allow TEST accounts
        """
        args     = fakeDeleteParser.parse_args()
        email    = args.get("accountEmail")
        mgr      = current_app.mgr
        fakeData = FakeData(mgr=mgr)

        fakeData.tearDownAccount(email=email, ensureTestAccount=True)
