#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Fake Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import inputs
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.auth.fields import EmailField, PasswordField
from smeckn.server.wAPIV1.account.fields import IDField as AccountIDField
from smeckn.server.wAPIV1.auth.fields import AuthJWTField, AccountJWTField
from smeckn.server.wAPIV1.profileGroup.fields import IDField as ProfileGroupIDField
from smeckn.server.wAPIV1.profile.fields import SMIDField as ProfileSMIDField
from smeckn.server.wAPIV1.fake.fields import SetUpField
from smeckn.server.wAPIV1.fake import ns


# ------------------------ PARSERS ------------------------------------------ #
fakeDeleteParser = ns.parser()
fakeDeleteParser.add_argument(
    "accountEmail",
    type         = inputs.email(),
    required     = True,
    location     = "args")


# ------------------------ MODELS ------------------------------------------- #
fakePostInputModel = ns.model("FakePostInputModel", {
    # Fields processed directly by FakeResource
    "accountEmail": EmailField(),
    "accountPassword": PasswordField(),

    # Fields sent to FakeData.setUp()
    "profileGroups": SetUpField(required=False),
    "profiles": SetUpField(required=False),
    "syncsForAllProfilesSuccess": SetUpField(required=False),
    "analyses": SetUpField(required=False),
    "textFull": SetUpField(required=False),
    "unitLedger": SetUpField(required=False),
    "facebookProfiles": SetUpField(required=False),
    "facebookPostsFull": SetUpField(required=False),
})
fakePostInputModelResourceKeys = ["accountEmail", "accountPassword"]
fakePostInputModelSetUpKeys    = [key for key in list(fakePostInputModel.keys())
                                  if key not in fakePostInputModelResourceKeys]


fakeModel = ns.model("FakeModel", {
    "popAccountID": AccountIDField(),
    "popAccountEmail": EmailField(),
    "popAuthToken": AuthJWTField(),
    "popAccountToken": AccountJWTField(),
    "popPGID": ProfileGroupIDField(),
    "popProfileSMID": ProfileSMIDField(),
    "popFBPSMID": ProfileSMIDField(),
})
fakeModelKeys = list(fakeModel.keys())


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyFakeModel(apiModel, allSet=False):
    """
    Verify fake model
    """
    for key in fakeModelKeys:
        assert key in apiModel
        if allSet:
            assert apiModel.get(key) is not None
