#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Fake Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class SetUpField(fields.Boolean):
    """
    SetUp Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Active",
            description = "If True, setUp the desired field",
            example     = True,
            default     = False,
            required    = required,
            ** kwargs)
