#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Fake Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('fake', description="Fake Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .fake import * #pylint: disable=C0413,W0401
