#!/usr/bin/env python3
"""
 NAME:
  fake_test.py

 DESCRIPTION
  Test Fake Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.fake.models import verifyFakeModel, fakePostInputModelSetUpKeys
from smeckn.server.gDB.account.dao import AccountDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- FAKE POST TEST CASE ----------------------------- #
class FakePostTestCase(CommonTestCase):
    """
    Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL     = self.routes.fake
        self.email        = "far@bar.com"
        self.password     = "23098asdlfki7usaj/div>"
        self.validPayload = {
            "accountEmail": self.email,
            "accountPassword": self.password,
        }

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        verifyFakeModel(apiModel=data)

    @logFunc()
    def test_full(self):
        """
        Test full setUp
        """
        payload = self.validPayload.copy()
        for key in fakePostInputModelSetUpKeys:
            payload[key] = True
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        verifyFakeModel(apiModel=data, allSet=True)

    @logFunc()
    def test_onlyTestAccountsAllowed(self):
        """
        Test trying to create fake TEST account with ADMIN email that already exists
        """
        payload = self.validPayload.copy()
        payload["accountEmail"] = self.adminEmail
        (code, data) = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1191)
        with self.gDB.sessionScope(commit=False) as gs:
            adminModel = AccountDAO.getByEmail(gs=gs, email=self.adminEmail)
            self.assertEqual(adminModel.id, self.adminAccountID)

    @logFunc()
    def test_unknownPayloadValues(self):
        """
        Test passing unknown payload values
        """
        payload           = self.validPayload.copy()
        payload["BadArg"] = "BadVal"
        (code, data)      = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        verifyFakeModel(apiModel=data)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test having no arg produces 400
        """
        argList = ["accountEmail",
                   "accountPassword"]
        for arg in argList:
            payload = self.validPayload.copy()
            payload.pop(arg)
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test having bad arg produces out code
        """
        argBadCodeList = [("accountEmail", 1212, 400),
                          ("accountPassword", 23423, 400)]
        for (arg, bad, outCode) in argBadCodeList:
            payload      = self.validPayload.copy()
            payload[arg] = bad
            (code, _)    = self.postJSON(self.validURL, d=payload, authToken=self.adminAuthToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_noAuthToken(self):
        """
        Test with no auth token
        """
        (code, _) = self.postJSON(self.validURL, d=self.validPayload)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badAuthToken(self):
        """
        Test bad auth token
        """
        (code, data) = self.postJSON(self.validURL, d=self.validPayload, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)


# ------------------------- FAKE DELETE TEST CASE --------------------------- #
class FakeDeleteTestCase(CommonTestCase):
    """
    Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.email     = self.popAccountEmail
        self.baseURL   = self.routes.fake
        self.validArgs = {"accountEmail": self.email}
        self.validURL  = "{}?{}".format(self.baseURL, urllib.parse.urlencode(self.validArgs))

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid test
        """
        # Account Exists
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=self.email)
            self.assertIsNotNone(aModel)

        # Delete Account
        (code, _) = self.deleteJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)

        # Account Doesn't Exist
        with self.gDB.sessionScope(commit=False) as gs:
            aModel = AccountDAO.getByEmail(gs=gs, email=self.email, raiseOnNone=False)
            self.assertIsNone(aModel)

    @logFunc()
    def test_onlyTestAccountsAllowed(self):
        """
        Test trying to delete ADMIN email
        """
        args         = {"accountEmail": self.adminEmail}
        url          = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
        (code, data) = self.deleteJSON(url, authToken=self.adminAuthToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), 1191)
        with self.gDB.sessionScope(commit=False) as gs:
            adminModel = AccountDAO.getByEmail(gs=gs, email=self.adminEmail)
            self.assertEqual(adminModel.id, self.adminAccountID)

    @logFunc()
    def test_noArgInvalid(self):
        """
        Test having no arg produces 400
        """
        argList = ["accountEmail"]
        for arg in argList:
            args      = self.validArgs.copy()
            args.pop(arg)
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.deleteJSON(url, authToken=self.adminAuthToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_badArgInvalid(self):
        """
        Test having bad arg produces out code
        """
        argBadCodeList = [("accountEmail", 1212, 400),
                          ("acccountEmail", "unknownemail@whereever.com", 200)]
        for (arg, bad, outCode) in argBadCodeList:
            args      = self.validArgs.copy()
            args[arg] = bad
            url       = "{}?{}".format(self.baseURL, urllib.parse.urlencode(args))
            (code, _) = self.deleteJSON(url, authToken=self.adminAuthToken)
            self.assertEqual(code, outCode)

    @logFunc()
    def test_noAuthToken(self):
        """
        Test with no auth token
        """
        (code, _) = self.deleteJSON(self.validURL)
        self.assertEqual(code, 400)

    @logFunc()
    def test_badAuthToken(self):
        """
        Test bad auth token
        """
        (code, data) = self.deleteJSON(self.validURL, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
