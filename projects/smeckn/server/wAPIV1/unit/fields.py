#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Unit Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from smeckn.server.aDB.unitLedger.model import UnitType


# ------------------------ FIELDS ------------------------------------------- #
class UnitTypeField(fields.String):
    """
    Unit Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Unit Type",
            description = "Unit Type",
            example     = UnitType.TEXT_ANALYSIS.name,
            enum        = [unitType.name for unitType in list(UnitType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, UnitType):
            return value.name
        else:
            return None


class NAvailableField(fields.Integer):
    """
    N Available Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Number of Available Units",
            description = "Number of units available for use",
            example     = 398,
            required    = required,
            **kwargs)


class NHeldField(fields.Integer):
    """
    N Held Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Numer of Held Units",
            description = "Number of units held",
            example     = 398,
            required    = required,
            **kwargs)


class NTotalField(fields.Integer):
    """
    N Total Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Numer of Total Units",
            description = "Number of total units",
            example     = 398,
            required    = required,
            **kwargs)


class MinPurchaseCountField(fields.Integer):
    """
    Min Purchase Count Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Minimum number of units that can be purchased",
            description = "Minimum number of units that can be purchased",
            example     = 500,
            required    = required,
            **kwargs)

class PurchaseIncrementField(fields.Integer):
    """
    Purchase Increment Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Purchase Increment",
            description = "Increment with which units are purchased",
            example     = 50,
            required    = required,
            **kwargs)

class UnitCostField(fields.Integer):
    """
    Unit Cost Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Unit Cost",
            description = "Unit cost in specified currency",
            example     = 10,
            required    = required,
            **kwargs)
