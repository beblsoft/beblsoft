#!/usr/bin/env python3
"""
 NAME:
  price.py

 DESCRIPTION
  Price Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.unit.models import priceGetParser, unitPriceModel
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.unit import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ PRICE RESOURCE ----------------------------------- #
@ns.route("/price")
class PriceResource(Resource):
    """
    Price Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper()
    @ns.doc(security=None)
    @ns.expect(priceGetParser)
    @ns.marshal_list_with(unitPriceModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get Price
        """
        args          = priceGetParser.parse_args()
        unitTypeStr   = args.get("unitType", None)
        unitType      = UnitType[unitTypeStr] if unitTypeStr else None
        bCurrencyCode = BeblsoftCurrencyCode[args.get("bCurrencyCode")]
        mgr           = current_app.mgr

        # Get prices[s]
        return mgr.payments.unit.getUnitPriceModelList(bCurrencyCode=bCurrencyCode, unitType=unitType)
