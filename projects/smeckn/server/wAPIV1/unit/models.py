#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Unit Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.wAPIV1.common.fields import BCurrencyCodeField
from smeckn.server.wAPIV1.unit.fields import UnitTypeField, NAvailableField, NHeldField, NTotalField
from smeckn.server.wAPIV1.unit.fields import MinPurchaseCountField, PurchaseIncrementField, UnitCostField
from smeckn.server.wAPIV1.unit import ns
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.aDB.unitLedger.dao import UnitLedgerDAO


# ------------------------ PARSERS ------------------------------------------ #
balanceGetParser = ns.parser()
balanceGetParser.add_argument(
    "unitType",
    type         = str,
    required     = False,
    location     = "args",
    choices      = [unitType.name for unitType in list(UnitType)])


priceGetParser = ns.parser()
priceGetParser.add_argument(
    "unitType",
    type         = str,
    required     = False,
    location     = "args",
    choices      = [unitType.name for unitType in list(UnitType)])
priceGetParser.add_argument(
    "bCurrencyCode",
    type         = str,
    required     = False,
    location     = "args",
    default      = BeblsoftCurrencyCode.USD.name,
    choices      = [bCurrencyCode.name for bCurrencyCode in list(BeblsoftCurrencyCode)])


# ------------------------ MODELS ------------------------------------------- #
unitBalanceModel = ns.model("UnitBalanceModel", {
    "unitType": UnitTypeField(),
    "nAvailable": NAvailableField(),
    "nHeld": NHeldField(),
    "nTotal": NTotalField()
})

unitPriceModel = ns.model("UnitPriceModel", {
    "unitType": UnitTypeField(),
    "minPurchaseCount": MinPurchaseCountField(),
    "purchaseIncrement": PurchaseIncrementField(),
    "unitCost": UnitCostField(),
    "bCurrencyCode": BCurrencyCodeField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyUnitBalanceModel(aDBS, apiModel):
    """
    Verify unit balance model
    """
    unitType = UnitType[apiModel.get("unitType")]
    _unitBalanceModel = UnitLedgerDAO.getUnitBalance(aDBS=aDBS, unitType=unitType)
    assert apiModel.get("nAvailable") == _unitBalanceModel.nAvailable
    assert apiModel.get("nHeld") == _unitBalanceModel.nHeld
    assert apiModel.get("nTotal") == _unitBalanceModel.nTotal


@logFunc()
def verifyUnitPriceModel(mgr, apiModel):
    """
    Verify unit price model
    """
    unitType        = UnitType[apiModel.get("unitType")]
    bCurrencyCode   = BeblsoftCurrencyCode[apiModel.get("bCurrencyCode")]
    _unitPriceModel = mgr.payments.unit.getUnitPriceModelList(bCurrencyCode=bCurrencyCode,
                                                             unitType=unitType)[0]
    assert apiModel.get("unitCost") == _unitPriceModel.unitCost
    assert apiModel.get("minPurchaseCount") == _unitPriceModel.minPurchaseCount
    assert apiModel.get("purchaseIncrement") == _unitPriceModel.purchaseIncrement
