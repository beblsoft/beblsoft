#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Unit Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('unit', description="Unit Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .price import *  #pylint: disable=C0413,W0401
from .balance import *  #pylint: disable=C0413,W0401
