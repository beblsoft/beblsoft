#!/usr/bin/env python3
"""
 NAME:
  balance.py

 DESCRIPTION
  Balance Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.unit.models import balanceGetParser, unitBalanceModel
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.unit import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BALANCE RESOURCE --------------------------------- #
@ns.route("/balance")
class BalanceResource(Resource):
    """
    Balance Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(balanceGetParser)
    @ns.marshal_list_with(unitBalanceModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get Balance
        """
        args         = balanceGetParser.parse_args()
        unitTypeStr  = args.get("unitType", None)
        unitType     = UnitType[unitTypeStr] if unitTypeStr else None
        mgr          = current_app.mgr

        # Get balance[s]
        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            return mgr.payments.unit.getUnitBalanceModelList(aDBS=aDBS, unitType=unitType)
