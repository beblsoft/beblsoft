#!/usr/bin/env python3
"""
 NAME:
  price_test.py

 DESCRIPTION
  Test Price Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.unit.models import verifyUnitPriceModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- PRICE GET TEST CASE ----------------------------- #
class PriceGetTestCase(CommonTestCase):
    """
    Price Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp()
        self.validURL = self.routes.unitPrice

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_invalidUnitType(self):
        """
        Test passing invalid unit type
        """
        invalidUnitTypeList = ["", "FOO_ANALYSIS", "BAR_ANALYSIS", "ANALYSIS"]

        for invalidUnitType in invalidUnitTypeList:
            urlArgs   = urllib.parse.urlencode({"unitType": invalidUnitType})
            url       = "{}?{}".format(self.validURL, urlArgs)
            (code, _) = self.getJSON(url)
            self.assertEqual(code, 400)

    @logFunc()
    def test_invalidBCurrencyCodeList(self):
        """
        Test passing invalid beblsoft currency code
        """
        invalidBCurrencyCodeList = ["", "*AS", "FASS", "XJD"]

        for invalidBCurrencyCode in invalidBCurrencyCodeList:
            urlArgs   = urllib.parse.urlencode({"bCurrencyCode": invalidBCurrencyCode})
            url       = "{}?{}".format(self.validURL, urlArgs)
            (code, _) = self.getJSON(url)
            self.assertEqual(code, 400)

    @logFunc()
    def test_noUnitTypeNoBCurrencyCode(self):
        """
        Test passing no unit type and no currency code
        """
        (code, data) = self.getJSON(self.validURL)
        self.assertEqual(code, 200)
        self.assertGreater(len(data), 1)
        for apiModel in data:
            verifyUnitPriceModel(mgr=self.mgr, apiModel=apiModel)

    @logFunc()
    def test_validUnitTypeBCurrencyCode(self):
        """
        Test passing invalid unitType and bCurrencyCode
        """
        unitType      = UnitType.TEXT_ANALYSIS
        bCurrencyCode = BeblsoftCurrencyCode.USD
        urlArgs       = urllib.parse.urlencode({"unitType": unitType.name,
                                                "bCurrencyCode": bCurrencyCode.name})
        url           = "{}?{}".format(self.validURL, urlArgs)
        (code, data)  = self.getJSON(url)
        apiModel      = data[0]
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 1)
        self.assertEqual(apiModel.get("unitType"), unitType.name)
        self.assertEqual(apiModel.get("bCurrencyCode"), bCurrencyCode.name)
        verifyUnitPriceModel(mgr=self.mgr, apiModel=apiModel)
