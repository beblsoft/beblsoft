#!/usr/bin/env python3
"""
 NAME:
  balance_test.py

 DESCRIPTION
  Test Balance Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import urllib
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB import AccountDatabase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.wAPIV1.unit.models import verifyUnitBalanceModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- BALANCE GET TEST CASE --------------------------- #
class BalanceGetTestCase(CommonTestCase):
    """
    Balance Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, unitLedger=True)
        self.validURL = self.routes.unitBalance

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_invalidUnitType(self):
        """
        Test passing invalid unit type
        """
        invalidUnitTypeList = ["", "FOO_ANALYSIS", "BAR_ANALYSIS", "ANALYSIS"]

        for invalidUnitType in invalidUnitTypeList:
            urlArgs   = urllib.parse.urlencode({"unitType": invalidUnitType})
            url       = "{}?{}".format(self.validURL, urlArgs)
            (code, _) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_noUnitType(self):
        """
        Test passing no unit type
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertGreater(len(data), 1)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            for apiModel in data:
                verifyUnitBalanceModel(aDBS=aDBS, apiModel=apiModel)

    @logFunc()
    def test_validUnitType(self):
        """
        Test passing valid unit type
        """
        unitType     = UnitType.TEXT_ANALYSIS
        urlArgs      = urllib.parse.urlencode({"unitType": unitType.name})
        url          = "{}?{}".format(self.validURL, urlArgs)
        (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        apiModel     = data[0]
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 1)
        self.assertEqual(apiModel.get("unitType"), unitType.name)
        with AccountDatabase.sessionScopeFromAID(gDB=self.gDB, aID=self.popAccountID, commit=False) as aDBS:
            verifyUnitBalanceModel(aDBS=aDBS, apiModel=apiModel)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)
