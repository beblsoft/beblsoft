#!/usr/bin/env python3
"""
 NAME:
  errorHeaders.py

 DESCRIPTION
  Error Header Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.wAPIV1.default import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger                = logging.getLogger(__name__)
API_ERROR_HTTP_STATUS = "API-ERROR-HTTP-STATUS"
API_ERROR_CODE        = "API-ERROR-CODE"


# ------------------------ MODELS ------------------------------------------- #
errorhttpStatusParser = ns.parser()
errorhttpStatusParser.add_argument(API_ERROR_HTTP_STATUS,
                                   type     = int,
                                   required = False,
                                   location = "headers")
errorCodeParser = ns.parser()
errorCodeParser.add_argument(API_ERROR_CODE,
                             type     = int,
                             required = False,
                             location = "headers")


# ------------------------ HEADER EVALUATORS -------------------------------- #
def evaluateErrorHttpStatusHeader():
    """
    Evaluate error http status header
    """
    errorhttpStatusArgs = errorhttpStatusParser.parse_args()
    errorHttpStatus     = errorhttpStatusArgs.get(API_ERROR_HTTP_STATUS, None)
    if errorHttpStatus:
        raise BeblsoftError(code=BeblsoftErrorCode.HEADER_ERROR,
                            httpStatus=errorHttpStatus)


def evaluateErrorCodeHeader():
    """
    Evaluate error code header
    """
    errorCodeArgs = errorCodeParser.parse_args()
    errorCode     = errorCodeArgs.get(API_ERROR_CODE, None)
    bErrorCode    = None
    if errorCode:
        try:
            bErrorCode = BeblsoftErrorCode(errorCode)
        except ValueError as _:
            pass
        else:
            raise BeblsoftError(code=bErrorCode)
