#!/usr/bin/env python3
"""
 NAME:
  helpers.py

 DESCRIPTION
  Auth Decorators

 NOTES
  Ran into trouble with circular imports when this functionality was placed in
  'wAPIV1/auth' directory.
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.wAPIV1.default import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger                   = logging.getLogger(__name__)
API_AUTH_TOKEN_HEADER    = "API-AUTH-TOKEN"
API_ACCOUNT_TOKEN_HEADER = "API-ACCOUNT-TOKEN"
API_AUTH_TOKEN_COOKIE    = "__smeckn_auth_token"
API_ACCOUNT_TOKEN_COOKIE = "__smeckn_account_token"


# ------------------------ MODELS ------------------------------------------- #
authParser = ns.parser()
authParser.add_argument(API_AUTH_TOKEN_HEADER,
                        type     = str,
                        required = False,
                        default  = None,
                        location = "headers")
authParser.add_argument(API_ACCOUNT_TOKEN_HEADER,
                        type     = str,
                        required = False,
                        default  = None,
                        location = "headers")
authParser.add_argument(API_AUTH_TOKEN_COOKIE,
                        type     = str,
                        required = False,
                        default  = None,
                        location = "cookies")
authParser.add_argument(API_ACCOUNT_TOKEN_COOKIE,
                        type     = str,
                        required = False,
                        default  = None,
                        location = "cookies")


# ------------------------ AUTH TOKEN --------------------------------------- #
@logFunc()
def getAuthToken():
    """
    Get Auth Token from Header or Cookie
    Header takes priority over cookie

    Raises
      Excetion if no token

    Returns
      AuthToken
    """
    token       = None
    authArgs    = authParser.parse_args()
    headerToken = authArgs.get(API_AUTH_TOKEN_HEADER)
    cookieToken = authArgs.get(API_AUTH_TOKEN_COOKIE)

    if cookieToken is not None:
        token = cookieToken
    elif headerToken is not None:
        token = headerToken
    else:
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_TOKEN_NOT_PRESENT)

    return token


@logFunc()
def getAuthAD(authToken = None):
    """
    Get Auth Attribute Dictionary
    """
    if not authToken:
        authToken = getAuthToken()
    authAD = current_app.mgr.jwt.auth.decodeJWT(authToken)
    return authAD


# ------------------------ ACCOUNT TOKEN ------------------------------------ #
@logFunc()
def getAccountToken():
    """
    Get Account Token from Header or Cookie
    Header takes priority over cookie

    Raises
      Excetion if no token

    Returns
      AuthToken
    """
    token       = None
    authArgs    = authParser.parse_args()
    headerToken = authArgs.get(API_ACCOUNT_TOKEN_HEADER)
    cookieToken = authArgs.get(API_ACCOUNT_TOKEN_COOKIE)

    if cookieToken is not None:
        token = cookieToken
    elif  headerToken is not None:
        token = headerToken
    else:
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ACCOUNT_TOKEN_NOT_PRESENT)

    return token


@logFunc()
def getAccountAD(accountToken=None):
    """
    Get Account Attribute Dictionary
    """
    if not accountToken:
        accountToken = getAccountToken()
    accountAD = current_app.mgr.jwt.account.decodeJWT(accountToken)
    return accountAD


# ------------------------ VERIFY ACCESS ------------------------------------ #
@logFunc()
def verifyAccessFunc(authToken=None, accountToken=None, aID=None):
    """
    Verify auth token has access to acount token via one of the following:
      1) Auth Token Encodes Same Account ID
      2) Auth Token Encodes Admin
    Args
      authToken:
        Auth token
        If None, will be parsed from API_AUTH_TOKEN
      accountToken:
        Account token
        If None, will be parsed from API_ACCOUNT_TOKEN
    """
    # Get Auth AD
    authAD = getAuthAD(authToken=authToken)

    # Get Account AD
    if not aID:
        accountAD = getAccountAD(accountToken=accountToken)
        aID       = accountAD.aID

    # Verify Access
    if (authAD.aID != aID) and (not authAD.aAdmin):
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_TOKEN_FOR_ID)


# ------------------------ VERIFY ADMIN ------------------------------------- #
@logFunc()
def verifyAdminFunc(authToken=None):
    """
    Verify client is an administrator
    """
    if not authToken:
        authToken = getAuthToken()

    # Decode Login JWT
    authAD = current_app.mgr.jwt.auth.decodeJWT(token=authToken)
    if not authAD.aAdmin:
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_ADMIN_REQUIRED)
