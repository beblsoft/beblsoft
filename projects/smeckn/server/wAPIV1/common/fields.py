#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Common Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from base.bebl.python.language.bType import BeblsoftLanguageType
from smeckn.server.aDB.contentType.model import ContentType
from smeckn.server.aDB.contentAttr.model import ContentAttrType
from smeckn.server.aDB.groupBy.model import GroupByType
from smeckn.server.aDB.groupBy.dateInterval import GroupByDateInterval


# ------------------------ FIELDS ------------------------------------------- #
class BCurrencyCodeField(fields.String):
    """
    BCurrency Code Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Beblsoft Currency Code",
            description = "Beblsoft Currency Code",
            example     = BeblsoftCurrencyCode.USD.name,
            enum        = [bCurrencyCode.name for bCurrencyCode in list(BeblsoftCurrencyCode)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, BeblsoftCurrencyCode):
            return value.name
        else:
            return None

class BLanguageTypeField(fields.String):
    """
    BLanguage Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Beblsoft Language Type",
            description = "Beblsoft Language Type",
            example     = BeblsoftLanguageType.English.name,
            enum        = [bLanguageType.name for bLanguageType in list(BeblsoftLanguageType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, BeblsoftLanguageType):
            return value.name
        else:
            return None


class ContentTypeField(fields.String):
    """
    Content Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Content Type",
            description = "Content Type",
            example     = ContentType.POST.name,
            enum        = [str(contentType.name) for contentType in list(ContentType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, ContentType):
            return value.name
        else:
            return None


class ContentAttrTypeField(fields.String):
    """
    Content Attr Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Content Attr Type",
            description = "Content Attr Type",
            example     = ContentAttrType.FACEBOOK_POST_SMID.name,
            enum        = [str(contentAttrType.name) for contentAttrType in list(ContentAttrType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, ContentAttrType):
            return value.name
        else:
            return None


class GroupByTypeField(fields.String):
    """
    Group By Type Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Group By Type",
            description = "Group By Type",
            example     = GroupByType.FACEBOOK_POST_CREATE_DATE.name,
            enum        = [str(groupByType.name) for groupByType in list(GroupByType)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, GroupByType):
            return value.name
        else:
            return None


class GroupByDateIntervalField(fields.String):
    """
    Group By Date Interval Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Group By Date Interval",
            description = "Group By Date Interval",
            example     = GroupByDateInterval.SECOND.name,
            enum        = [str(groupByDateInterval.name) for groupByDateInterval in list(GroupByDateInterval)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, GroupByDateInterval):
            return value.name
        else:
            return None
