#!/usr/bin/env python3
"""
 NAME:
  requestWrapper.py

 DESCRIPTION
  Request Wrapper Decorator
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
from functools import wraps
from flask import request, after_this_request
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from smeckn.server.eCtx import ExecutionContext
from smeckn.server.wAPIV1.common.errorHeaders import evaluateErrorHttpStatusHeader, evaluateErrorCodeHeader
from smeckn.server.wAPIV1.common.auth import verifyAdminFunc, verifyAccessFunc, getAccountAD
from smeckn.server.application.glob import ApplicationGlobal
from smeckn.server.log.sLogger import SmecknLogger


# ------------------------ GLOBALS ------------------------------------------ #
logger                = logging.getLogger(__name__)
API_ERROR_HTTP_STATUS = "API-ERROR-HTTP-STATUS"
API_ERROR_CODE        = "API-ERROR-CODE"


# ------------------------ REQUEST WRAPPER DECORATOR ------------------------ #
def requestWrapper(verifyAdmin=False, verifyAccess=False, loadAccountAttrDict=False):
    """
    Request Wrapper
    """
    def requestWrapperInner(func):
        """
        Real Decorator
        """
        @wraps(func)
        def wrapper(*args, **kwargs):  # pylint: disable=C0111
            """
            Evaluate headers, set execution context
            """
            startDateTime = datetime.now()

            # Execution Context and Verification --------------------
            ExecutionContext.setFromWSGIEnviron(environ=request.environ)
            if verifyAdmin:
                verifyAdminFunc()
            if verifyAccess:
                verifyAccessFunc()
            if loadAccountAttrDict:
                accountAD = getAccountAD()
                ApplicationGlobal.setAccountAD(accountAD=accountAD)

            # Register Handler For After Request --------------------
            @after_this_request
            def _requestComplete(response):
                return requestComplete(response=response, startDateTime=startDateTime)

            # Execute Headers ---------------------------------------
            evaluateErrorHttpStatusHeader()
            evaluateErrorCodeHeader()

            # Execute Wrapped Function ------------------------------
            logRequestStart()
            rval = func(*args, **kwargs)
            return rval

        return wrapper
    return requestWrapperInner


# ------------------------ REQUEST COMPLETE --------------------------------- #
def requestComplete(response, startDateTime):
    """
    Handle Request Completion

    Args
      response:
        Flask Response
        See: https://flask.palletsprojects.com/en/1.1.x/api/#flask.Response
      startDateTime:
        Datetime when the request started
    """
    logRequestComplete(response=response, startDateTime=startDateTime, endDateTime=datetime.now())
    ApplicationGlobal.clearAccountAD()
    ExecutionContext.set(eCtx=None)
    return response


# ------------------------ LOG REQUEST START -------------------------------- #
def logRequestStart():  # pylint: disable=W0102
    """
    Log Request Start

    Globals
      request:
        Flask Request Object
        See: https://flask.palletsprojects.com/en/1.1.x/api/#flask.Request
    """
    SmecknLogger.debug(msg="API Request Start:{}".format(request.url_rule), bLogCode=BeblsoftLogCode.API_REQUEST_START)


# ------------------------ LOG REQUEST COMPLETE ----------------------------- #
def logRequestComplete(response, startDateTime, endDateTime):
    """
    Log Request Complemetion
    Args
      response:
        Flask Response
        See: https://flask.palletsprojects.com/en/1.1.x/api/#flask.Response
      startDateTime:
        Datetime when the request started
      endDateTime:
        Datetime when the request completed
    """
    statusCode               = response.status_code
    urlRule                  = request.url_rule
    _request                 = BeblsoftAttrDict()
    _response                = BeblsoftAttrDict()
    _request.method          = request.method
    _request.urlRule         = urlRule
    _request.duration        = str(endDateTime - startDateTime)
    _response.statusCode     = response.status_code

    # Cases to handle
    # ------------------
    # 1. Normal Case: Log very little information
    # 2. Abnormal Case: Something bad happened, add more context to logs
    if 200 <= statusCode < 500:
        SmecknLogger.info(msg="API Request Complete:{}".format(urlRule), bLogCode=BeblsoftLogCode.API_REQUEST_COMPLETE,
                          request=_request, response=_response)
    else:
        # Request
        _request.url         = request.url
        _request.args        = request.args
        _request.contentType = request.content_type
        _request.cookies     = request.cookies
        _request.json        = request.get_json(silent=True)
        _request.headers     = str(request.headers)
        _request.host        = request.host
        _request.referrer    = request.referrer
        _request.remoteAddr  = request.remote_addr

        # Response
        _response.headers    = response.headers
        _response.status     = response.status
        _response.statusCode = response.status_code
        _response.json       = response.get_json(silent=True)
        _response.mimetype   = response.mimetype

        SmecknLogger.warning(msg="API Request Complete:{}".format(urlRule),
                             bLogCode=BeblsoftLogCode.API_REQUEST_COMPLETE,
                             request=_request, response=_response)
