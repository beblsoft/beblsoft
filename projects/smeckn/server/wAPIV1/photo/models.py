#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Photo Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.photo.fields import SMIDField, CreateDateField, ImageURLField
from smeckn.server.wAPIV1.photo import ns
from smeckn.server.aDB.photo.dao import PhotoDAO


# ------------------------ MODELS ------------------------------------------- #
photoModel = ns.model("PhotoModel", {
    "smID": SMIDField(),
    "createDate": CreateDateField(),
    "imageURL": ImageURLField()
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyPhotoModel(aDBS, apiModel, smID):
    """
    Verify photo model
    """
    _photoModel = PhotoDAO.getBySMID(aDBS=aDBS, smID=smID)
    assert apiModel.get("smID") == _photoModel.smID
    assert apiModel.get("createDate") == _photoModel.createDate.isoformat()
    assert apiModel.get("imageURL") == _photoModel.imageURL
