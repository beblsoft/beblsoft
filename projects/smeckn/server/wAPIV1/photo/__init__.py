#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Photo Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('photo', description="Photo Operations")
