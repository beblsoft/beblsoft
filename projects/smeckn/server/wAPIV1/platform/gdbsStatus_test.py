#!/usr/bin/env python3
"""
 NAME:
  gdbsStatus_test.py

 DESCRIPTION
  Test Global Database Server Status Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.platform.models import verifyMySQLServerStatusModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- GLOBAL DATABASE SERVER STATUS GET TEST CASE ----- #
class GlobalDatabaseServerStatusGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = self.routes.platformGDBSStatus

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Case
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        verifyMySQLServerStatusModel(apiModel=data, bServer=self.gDB.bServer)

    @logFunc()
    def test_nonAdmin(self):
        """
        Valid without admin credentials
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
