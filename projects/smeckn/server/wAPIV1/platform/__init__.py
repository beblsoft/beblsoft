#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Platform Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace("platform", description="Platform Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .gdbsStatus import *  #pylint: disable=C0413,W0401
from .adbsStatus import *  #pylint: disable=C0413,W0401
