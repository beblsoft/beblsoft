#!/usr/bin/env python3
"""
 NAME:
  adbsStatus_test.py

 DESCRIPTION
  Test Account Database Server Status Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.platform.models import verifyMySQLServerStatusModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- ACCOUNT DATABASE SERVER STATUS GET TEST CASE ---- #
class AccountDatabaseServerStatusGetTestCase(CommonTestCase):
    """
    Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = self.routes.platformADBSStatus

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_valid(self):
        """
        Valid Case
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.adminAuthToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data.get("data")), 1) # Only one ADBS on localhost
        apiModel = data.get("data")[0]
        verifyMySQLServerStatusModel(apiModel=apiModel, bServer=self.gDB.bServer) # ADBS == GDBS

    @logFunc()
    def test_nonAdmin(self):
        """
        Valid without admin credentials
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken)
        self.assertEqual(code, 401)
        self.assertEqual(data.get("code"), 1186)
