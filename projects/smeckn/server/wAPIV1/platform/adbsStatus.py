#!/usr/bin/env python3
"""
 NAME:
  adbsStatus.py

 DESCRIPTION
  Account Database Server Status Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.run.bParallel import runFuncOnList
from smeckn.server.gDB.accountDBServer.dao import AccountDBServerDAO
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.platform.models import accountDatabaseServerStatusGetOutputModel
from smeckn.server.wAPIV1.platform import ns
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper

# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ACCOUNT DATABASE SERVER STATUS RESOURCE ---------- #
@ns.route("/adbsStatus")
class AccountDatabaseServerStatusResource(Resource):
    """
    Account Database Server Status Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.marshal_with(accountDatabaseServerStatusGetOutputModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201,W0622
        """
        Get account database server statuses
        """
        gDB = current_app.mgr.gdb.gDB

        with gDB.sessionScope(commit=False) as gs:
            aDBSModelList     = AccountDBServerDAO.getAll(gs=gs)
            idxADBSServerList = [(idx, AccountDatabase.serverFromADBSModel(aDBSModel=aDBSModel))
                                 for idx, aDBSModel in enumerate(aDBSModelList)]
            statusList        = [{} for _ in idxADBSServerList]

            def getADBServerStatusFunc(idxADBServer):
                """
                Args
                  idxADBServer:
                    Tuple (idx, aDBServer)
                    Idx specifies index into status list to put result
                """
                idx       = idxADBServer[0]
                aDBServer = idxADBServer[1]
                statusList[idx] = {
                    "domainName": aDBServer.domainName,
                    "state": aDBServer.state
                }
            runFuncOnList(func=getADBServerStatusFunc, lst=idxADBSServerList, parallel=True)
            return {"data": statusList}
