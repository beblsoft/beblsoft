#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Platform Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401
from base.mysql.python.bServerState import BeblsoftMySQLServerState


# ------------------------ FIELDS ------------------------------------------- #
class MySQLServerDomainNameField(fields.String):
    """
    MySQL Server Domain Name Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "MySQL Server Domain Name",
            description = "MySQL Server Domain Name",
            example     = "smeckntestgdbcluster.cluster-cwmxk4jdpud3.us-east-1.rds.amazonaws.com",
            required    = required,
            ** kwargs)


class MySQLDatabaseServerStateField(fields.String):
    """
    MySQL Database Server State Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "MySQL Server State",
            description = "MySQL Server State",
            example     = BeblsoftMySQLServerState.ONLINE.name,
            enum        = [bmss.name for bmss in list(BeblsoftMySQLServerState)],
            default     = BeblsoftMySQLServerState.ONLINE.name,
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, BeblsoftMySQLServerState):
            return value.name
        else:
            return None
