#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Platform Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.platform.fields import MySQLServerDomainNameField, MySQLDatabaseServerStateField
from smeckn.server.wAPIV1.platform import ns


# ------------------------ MODELS ------------------------------------------- #
mySQLServerStatusModel = ns.model("MySQLServerStateModel", {
    "domainName": MySQLServerDomainNameField(),
    "state": MySQLDatabaseServerStateField(),
})

accountDatabaseServerStatusGetOutputModel = ns.model("AccountDatabaseServerStatusGetOutputModel", {
    "data": fields.List(fields.Nested(mySQLServerStatusModel))
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyMySQLServerStatusModel(apiModel, bServer):
    """
    Verify global status model
    Args
      apiModel:
        mySQLServerStatusModel
      bServer:
        BeblsoftMySQLServer object
    """
    assert apiModel.get("domainName") == bServer.domainName
    assert apiModel.get("state") != bServer.state
