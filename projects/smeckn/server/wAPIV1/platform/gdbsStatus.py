#!/usr/bin/env python3
"""
 NAME:
  gdbsStatus.py

 DESCRIPTION
  Global Database Server Status Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.wAPIV1.common.auth import API_AUTH_TOKEN_HEADER
from smeckn.server.wAPIV1.platform.models import mySQLServerStatusModel
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.platform import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ GLOBAL DATABASE SERVER STATUS RESOURCE ----------- #
@ns.route("/gdbsStatus")
class GlobalDatabaseServerStatusResource(Resource):
    """
    Global Database Server Status Routes
    """
    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAdmin=True)
    @ns.doc(security=[API_AUTH_TOKEN_HEADER])
    @ns.marshal_with(mySQLServerStatusModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201,W0622
        """
        Get global database server status
        """
        gDB  = current_app.mgr.gdb.gDB
        gDBS = gDB.bServer

        return {
            "domainName": gDBS.domainName,
            "state": gDBS.state
        }
