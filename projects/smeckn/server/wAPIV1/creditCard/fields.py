#!/usr/bin/env python3
"""
 NAME:
  fields.py

 DESCRIPTION
  Credit Card Fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import fields  # pylint: disable=E0401


# ------------------------ FIELDS ------------------------------------------- #
class StripeCardIDField(fields.String):
    """
    Stripe Card ID Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Stripe Card ID",
            description = "Stripe Card ID",
            example     = "card_1DbDR9ECEJ911IPjLYhle4UK",
            required    = required,
            ** kwargs)


class StripeCardTokenField(fields.String):
    """
    Stripe Card Token Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Stripe Card Token",
            description = "Stripe Card Token",
            example     = "tok_1Daq2jECEJ911IPjs97nrvet",
            required    = required,
            ** kwargs)


class BrandField(fields.String):
    """
    Brand Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Card Brand",
            description = "Card Brand",
            example     = "Visa",
            required    = required,
            ** kwargs)


class Last4Field(fields.String):
    """
    Last 4 Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Last 4 digits of credit card",
            description = "Last 4 digits of credit card",
            example     = "7272",
            required    = required,
            ** kwargs)
