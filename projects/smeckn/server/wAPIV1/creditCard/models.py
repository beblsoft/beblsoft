#!/usr/bin/env python3
"""
 NAME:
  models.py

 DESCRIPTION
  Credit Card Models
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.card.dao import StripeCardDAO
from smeckn.server.wAPIV1.creditCard.fields import StripeCardIDField, StripeCardTokenField, BrandField, Last4Field
from smeckn.server.wAPIV1.creditCard import ns


# ------------------------ MODELS ------------------------------------------- #
creditCardModel = ns.model("CreditCardModel", {
    "id": StripeCardIDField(),
    "brand": BrandField(),
    "last4": Last4Field()
})

creditCardPostInputModel = ns.model("CreditCardPostInputModel", {
    "token": StripeCardTokenField(required=True)
})

creditCardDefaultPutInputModel = ns.model("CreditCardDefaultPutInputModel", {
    "id": StripeCardIDField(required=True)
})


# ------------------------ VERIFY ------------------------------------------- #
@logFunc()
def verifyCreditCardModel(mgr, apiModel, stripeCustomerID):
    """
    Verify creditCard model
    """
    stripeCardModel = StripeCardDAO.getByID(bStripe=mgr.payments.bStripe,
                                            stripeCustomerID=stripeCustomerID,
                                            stripeCardID=apiModel.get("id"))
    assert apiModel.get("brand") == stripeCardModel.brand
    assert apiModel.get("last4") == stripeCardModel.last4
