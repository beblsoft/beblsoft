#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Credit Card Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask_restplus import Namespace  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
ns = Namespace('creditCard', description="Credit Card Operations")


# ------------------------ ROUTE IMPORTS ------------------------------------ #
from .creditCard import *  # pylint: disable=C0413,W0401
from .default import *  # pylint: disable=C0413,W0401
from .id import *  # pylint: disable=C0413,W0401
