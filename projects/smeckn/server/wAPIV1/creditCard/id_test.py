#!/usr/bin/env python3
"""
 NAME:
  id_test.py

 DESCRIPTION
  ID Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.creditCard.models import verifyCreditCardModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- ID GET TEST CASE -------------------------------- #
class IDGetTestCase(CommonTestCase):
    """
    ID Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True, stripeCards=True)
        self.validURL = "{}/{}".format(self.routes.creditCard, self.popStripeCardID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_invalidID(self):
        """
        Test with invalid card IDs
        """
        invalidIDList = ["badID", "malformedid", "card_2948dsaflkh", "sdsd"]
        for invalidID in invalidIDList:
            url          = "{}/{}".format(self.routes.creditCard, invalidID)
            (code, data) = self.getJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)
            self.assertTrue("No such source" in data.get("message"))

    @logFunc()
    def test_valid(self):
        """
        Test valid case
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        verifyCreditCardModel(mgr=self.mgr, apiModel=data, stripeCustomerID=self.popStripeCustomerID)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- ID DELETE TEST CASE ----------------------------- #
class IDDeleteTestCase(CommonTestCase):
    """
    ID Delete Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True, stripeCards=True)
        self.validURL = "{}/{}".format(self.routes.creditCard, self.popStripeCardID)

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_invalidID(self):
        """
        Test with invalid card IDs
        """
        invalidIDList = ["badID", "malformedid", "card_2948dsaflkh", "sdsd"]
        for invalidID in invalidIDList:
            url          = "{}/{}".format(self.routes.creditCard, invalidID)
            (code, data) = self.deleteJSON(url, authToken=self.popAuthToken, accountToken=self.popAccountToken)
            self.assertEqual(code, 400)
            self.assertTrue("No such source" in data.get("message"))

    @logFunc()
    def test_valid(self):
        """
        Test valid case
        """
        (code, _) = self.deleteJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        stripeCardModelList = self.mgr.payments.creditCard.getAll(stripeCustomerID=self.popStripeCustomerID)
        for stripeCardModel in stripeCardModelList:
            self.assertNotEqual(self.popStripeCardID, stripeCardModel.id)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.deleteJSON)
