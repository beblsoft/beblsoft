#!/usr/bin/env python3
"""
 NAME:
  default.py

 DESCRIPTION
  Default Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.creditCard.models import creditCardModel, creditCardDefaultPutInputModel
from smeckn.server.wAPIV1.creditCard import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ DEFAULT RESOURCE --------------------------------- #
@ns.route("/default")
class DefaultResource(Resource):
    """
    Default Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(creditCardModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get default credit card
        """
        mgr = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            return mgr.payments.creditCard.getDefault(stripeCustomerID=stripeCustomerModel.id)

    # -------------------- PUT ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(creditCardDefaultPutInputModel)
    @ns.marshal_with(creditCardModel)
    @logFunc()
    def put(self):  # pylint: disable=R0201
        """
        Set default credit card
        """
        inData       = request.json
        stripeCardID = inData.get("id")
        mgr          = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask() as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            return mgr.payments.creditCard.setDefault(stripeCustomerID=stripeCustomerModel.id, stripeCardID=stripeCardID)
