#!/usr/bin/env python3
"""
 NAME:
  id.py

 DESCRIPTION
  ID Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.creditCard.models import creditCardModel
from smeckn.server.wAPIV1.creditCard import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ ID RESOURCE -------------------------------------- #
@ns.route("/<string:id>")
class IDResource(Resource):
    """
    ID Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_with(creditCardModel)
    @logFunc()
    def get(self, id):  # pylint: disable=R0201,W0622
        """
        Get credit card
        """
        mgr = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            return mgr.payments.creditCard.getByID(stripeCustomerID=stripeCustomerModel.id,
                                                   stripeCardID=id)

    # -------------------- DELETE ------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @logFunc()
    def delete(self, id):  # pylint: disable=R0201,W0622
        """
        Delete credit card
        """
        mgr = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            mgr.payments.creditCard.deleteByID(stripeCustomerID=stripeCustomerModel.id,
                                               stripeCardID=id)
