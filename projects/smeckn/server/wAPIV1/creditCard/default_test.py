#!/usr/bin/env python3
"""
 NAME:
  default_test.py

 DESCRIPTION
  Default Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.creditCard.models import verifyCreditCardModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- DEFAULT GET TEST CASE --------------------------- #
class DefaultGetTestCase(CommonTestCase):
    """
    Default Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True)
        self.validURL = self.routes.creditCardDefault

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validNoCards(self):
        """
        Test valid case with no cards
        """
        (code, _) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 404)

    @logFunc()
    def test_validManyCards(self):
        """
        Test valid case with many cards
        """
        super().setUp(stripeCards=True)

        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        stripeCardModel = self.mgr.payments.creditCard.getDefault(stripeCustomerID=self.popStripeCustomerID)
        self.assertEqual(stripeCardModel.id, data.get("id"))
        verifyCreditCardModel(mgr=self.mgr, apiModel=data, stripeCustomerID=self.popStripeCustomerID)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- DEFAULT PUT TEST CASE --------------------------- #
class DefaultPutTestCase(CommonTestCase):
    """
    Default Put Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True, stripeCards=True)
        self.validURL     = self.routes.creditCardDefault
        self.validPayload = {"id": self.popStripeCardID}

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_invalidCardID(self):
        """
        Test invalid card ids
        """
        invalidIDList = ["badID", "malformedid", "card_2948dsaflkh"]
        for invalidID in invalidIDList:
            payload      = {"id": invalidID}
            (code, data) = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                     accountToken=self.popAccountToken)
            self.assertTrue("No such source" in data.get("message"))
            self.assertEqual(code, 400)

    @logFunc()
    def test_emptyCardID(self):
        """
        Test empty card id
        """
        payload      = {"id": ""}
        (code, data) = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                 accountToken=self.popAccountToken)
        self.assertEqual(code, 400)
        self.assertEqual(data.get("code"), BeblsoftErrorCode.STRIPE_CARDID_TOO_SHORT.value)


    @logFunc()
    def test_valid(self):
        """
        Test valid case
        """
        super().setUp(stripeCards=True)

        for stripeCardModel in self.stripeCardModelList:
            payload = {"id": stripeCardModel.id}
            (code, data) = self.putJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                        accountToken=self.popAccountToken)
            self.assertEqual(code, 200)
            defaultStripeCardModel = self.mgr.payments.creditCard.getDefault(
                stripeCustomerID=self.popStripeCustomerID)
            self.assertEqual(data.get("id"), defaultStripeCardModel.id)
            verifyCreditCardModel(mgr=self.mgr, apiModel=data, stripeCustomerID=self.popStripeCustomerID)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.putJSON, d=self.validPayload)
