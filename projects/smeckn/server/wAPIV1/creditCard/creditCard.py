#!/usr/bin/env python3
"""
 NAME:
  creditCard.py

 DESCRIPTION
  Credit Card Routes
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import current_app, request
from flask_restplus import Resource  # pylint: disable=E0401
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.aDB import AccountDatabase
from smeckn.server.wAPIV1.common.requestWrapper import requestWrapper
from smeckn.server.wAPIV1.creditCard.models import creditCardModel, creditCardPostInputModel
from smeckn.server.wAPIV1.creditCard import ns


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ CREDIT CARD RESOURCE ----------------------------- #
@ns.route("")
class CreditCardResource(Resource):
    """
    Credit Card Routes
    """
    # -------------------- GET ---------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.marshal_list_with(creditCardModel)
    @logFunc()
    def get(self):  # pylint: disable=R0201
        """
        Get credit cards
        """
        mgr                 = current_app.mgr
        stripeCardModelList = []

        with AccountDatabase.sessionScopeFromFlask(commit=False) as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            stripeCardModelList = mgr.payments.creditCard.getAll(stripeCustomerID=stripeCustomerModel.id)
        return stripeCardModelList

    # -------------------- POST --------------------------------------------- #
    @requestWrapper(verifyAccess=True, loadAccountAttrDict=True)
    @ns.expect(creditCardPostInputModel)
    @ns.marshal_with(creditCardModel)
    @logFunc()
    def post(self):  # pylint: disable=R0201
        """
        Create credit card
        """
        inData    = request.json
        cardToken = inData.get("token")
        mgr       = current_app.mgr

        with AccountDatabase.sessionScopeFromFlask() as aDBS:
            stripeCustomerModel = mgr.payments.customer.getOrCreateStripeCustomerModel(aDBS=aDBS)
            stripeCardModel     = mgr.payments.creditCard.create(stripeCustomerID=stripeCustomerModel.id,
                                                                 source=cardToken)
            return stripeCardModel
