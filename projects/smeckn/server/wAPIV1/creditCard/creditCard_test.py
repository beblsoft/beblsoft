#!/usr/bin/env python3
"""
 NAME:
  creditCard_test.py

 DESCRIPTION
  Credit Card Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.wAPIV1.creditCard.models import verifyCreditCardModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------- CREDIT CARD GET TEST CASE ----------------------- #
class CreditCardGetTestCase(CommonTestCase):
    """
    Credit Card Get Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True)
        self.validURL = self.routes.creditCard

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validNoCards(self):
        """
        Test valid case no cards
        """
        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), 0)

    @logFunc()
    def test_validManyCards(self):
        """
        Test valid case with many cards
        """
        super().setUp(stripeCustomers=True, stripeCards=True)

        (code, data) = self.getJSON(self.validURL, authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        self.assertEqual(len(data), len(self.stripeCardModelList))
        for apiModel in data:
            verifyCreditCardModel(mgr=self.mgr, apiModel=apiModel, stripeCustomerID=self.popStripeCustomerID)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.getJSON)


# ------------------------- CREDIT CARD POST TEST CASE ---------------------- #
class CreditCardPostTestCase(CommonTestCase):
    """
    Credit Card Post Test Case
    """

    @logFunc()
    def setUp(self):  # pylint: disable=W0221
        super().setUp(accounts=True, stripeCustomers=True)
        self.validURL = self.routes.creditCard
        self.validPayload = {"token": "tok_visa"}

    # ------------------------- TEST FUNCTIONS ------------------------------ #
    @logFunc()
    def test_validCardToken(self):
        """
        Test with valid card token
        """

        (code, data) = self.postJSON(self.validURL, d=self.validPayload,
                                     authToken=self.popAuthToken, accountToken=self.popAccountToken)
        self.assertEqual(code, 200)
        verifyCreditCardModel(mgr=self.mgr, apiModel=data, stripeCustomerID=self.popStripeCustomerID)

    @logFunc()
    def test_invalidCardToken(self):
        """
        Test with invalid card token
        """
        invalidTokenList = ["", "blah", "blahblah", "tok_cvcCheckFail"]
        for invalidToken in invalidTokenList:
            payload   = {"token": invalidToken}
            (code, _) = self.postJSON(self.validURL, d=payload, authToken=self.popAuthToken,
                                      accountToken=self.popAccountToken)
            self.assertEqual(code, 400)

    @logFunc()
    def test_auth(self):  # pylint: disable=C0111
        self.authTest(route=self.validURL, methodFunc=self.postJSON, d=self.validPayload)
