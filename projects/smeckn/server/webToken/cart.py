#!/usr/bin/env python3
"""
 NAME:
  cart.py

 DESCRIPTION
  Cart JSON Web Token Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
import pickle
from datetime import datetime
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.webToken import BaseJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CART JWT CLASS ------------------------------- #
class CartJWT(BaseJWT):
    """
    Cart JWT Class

    Encryption Process (Decryption is opposite)
      - Pickle into bytes
      - Encode bytes into latin1 string
        latin1 is used as it maps bytes one-to-one to unicode codepoints
        https://stackoverflow.com/questions/17303266/how-to-pickle-unicodes-and-save-them-in-utf-8-databases
      - Encrypt payload into JWT

    VERSION_1 fields
      version
      pickleStr
    """
    VERSION_1 = 1

    @logFunc()
    def decodeJWT(self, token):  # pylint: disable=R0201
        """
        Decode token
        Args
          token: JWT

        Returns
          CartModel
        """
        payload = self.decodePayloadFromJWT(token)
        try:
            pickleStr   = payload.get("pickleStr")
            pickleBytes = pickleStr.encode("latin1")
            cartModel   = pickle.loads(pickleBytes)
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_PAYLOAD,
                                msg="payload={}".format(payload),
                                originalError=e)
        return cartModel

    @logFunc()
    def encodeJWT(self, cartModel, expDelta=None):
        """
        Create token
        Args
          cartModel:
            Cart Model Object
          expDelta:
            expiration delta to override default
            Ex. timedelta(days=30)
        Returns
          payload
        """
        iat          = datetime.utcnow()
        exp          = iat + self.defaultExpDelta
        if expDelta:
            exp      = iat + expDelta
        nbf          = iat + self.defaultNBFDelta
        pickleBytes  = pickle.dumps(cartModel)
        pickleStr    = pickleBytes.decode("latin1")
        payload      = {
            "version": CartJWT.VERSION_1,
            "pickleStr": pickleStr,
            "iat": iat,
            "exp": exp,
            "nbf": nbf
        }

        return self.encodePayloadToJWT(payload)
