#!/usr/bin/env python3
"""
 NAME:
  forgotPassword.py

 DESCRIPTION
  Forgot Password JSON Web Token Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.webToken import BaseJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- FORGOT PASSWORD JWT CLASS -------------------- #
class ForgotPasswordJWT(BaseJWT):
    """
    Forgot Password JWT Class

    VERSION_1 fields
      version
      aID
    """
    VERSION_1 = 1

    @logFunc()
    def decodeJWT(self, token):  # pylint: disable=R0201
        """
        Decode token
        Args
          token: JWT
        Returns
          Attr Dict, see below
        """
        payload  = self.decodePayloadFromJWT(token)
        try:
            ad         = BeblsoftAttrDict()
            ad.version = payload.get("version")
            ad.aID     = payload.get("aID")
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_PAYLOAD,
                                msg="payload={}".format(payload),
                                originalError=e)
        return ad

    @logFunc()
    def encodeJWT(self, aID, expDelta=None):
        """
        Create token
        Args
          aID:
            account id
          expDelta:
            expiration delta to override default
            Ex. timedelta(days=30)
        Returns
          payload
        """
        iat     = datetime.utcnow()
        exp     = iat + self.defaultExpDelta
        if expDelta:
            exp = iat + expDelta
        nbf     = iat + self.defaultNBFDelta
        payload = {
            "version": ForgotPasswordJWT.VERSION_1,
            "aID": aID,
            "iat": iat,
            "exp": exp,
            "nbf": nbf
        }
        return self.encodePayloadToJWT(payload)
