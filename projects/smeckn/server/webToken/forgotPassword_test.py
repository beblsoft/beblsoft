#!/usr/bin/env python3
"""
 NAME:
  forgotPassword_test.py

 DESCRIPTION
  Forgot Password JWT Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.webToken.forgotPassword import ForgotPasswordJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class ForgotPasswordJWTTestCase(CommonTestCase):
    """
    Forgot Password JWT Test Case
    """
    @logFunc()
    def test_encodeDecode(self):
        """
        Test encoding and decoding
        """
        aID               = 5
        forgotPasswordJWT = self.mgr.jwt.forgotPassword.encodeJWT(aID=aID)
        forgotPasswordAD  = self.mgr.jwt.forgotPassword.decodeJWT(token=forgotPasswordJWT)
        self.assertEqual(forgotPasswordAD.version, ForgotPasswordJWT.VERSION_1)
        self.assertEqual(forgotPasswordAD.aID, aID)
