#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  Base JSON Web Token Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import timedelta
import jwt #pylint: disable=W0406
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- BASE JWT CLASS ------------------------------- #
class BaseJWT():
    """
    Base JWT Class

    Encoding Process
       Data Fields -------------> Payload -------------------> Token
       Ex.AccountID    Add JWT    AccountID         Encode
                       Fields     notBeforeDelta,
                                  expirationDelta

    Decoding Process
       Data Fields <------------- Payload <------------------- Token
                                                    Decode
    """

    def __init__(self, mgr, secret, algorithm,
                 defaultExpDelta,
                 defaultNBFDelta=timedelta(seconds=0),
                 defaultLeeway=timedelta(seconds=0)):
        """
        Initialize JWT Class
        Args
          mgr:
            Top Level Manager
          secret:
            secret used to encode JWT
          algorithm:
            algorithm used to encode JWT.
            ex. "HS256", "HS384", "HS512"
          defaultExpDelta:
            expiration delta, time after current time that JWT should expire
          defaultNBFDelta:
            not before delta, time after current time where JWT should not be allowed
          leeway:
            time after expDelta where JWT allowed
        Other JWT terminology:
          iat = issued at
          iss = issuer
          aud = audience
        """
        self.mgr               = mgr
        self.cfg               = mgr.cfg
        self.secret            = secret
        self.algorithm         = algorithm
        self.defaultExpDelta   = defaultExpDelta
        self.defaultNBFDelta   = defaultNBFDelta
        self.defaultLeeway     = defaultLeeway
        self.authHeaderName    = "Authorization"
        self.authHeaderPrefix  = "JWT"
        self.verifyClaims      = ["signature", "exp", "nbf", "iat"]
        self.dontVerifyClaims  = ["aud"]
        self.requiredClaims    = ["exp", "iat", "nbf"]

    @logFunc()
    def decodePayloadFromJWT(self, token):
        """
        Decode JWT and return payload
        Args
          token: JWT
        Returns
          payload
        """
        options = {}
        options.update({
            "verify_" + claim: True
            for claim in self.verifyClaims
        })
        options.update({
            "verify_" + claim: False
            for claim in self.dontVerifyClaims
        })
        options.update({
            "require_" + claim: True
            for claim in self.requiredClaims
        })
        try:
            payload = jwt.decode(str(token), key=self.secret, options=options, #pylint: disable=E1101
                                 algorithms=[self.algorithm], leeway=self.defaultLeeway)
            return payload
        except jwt.InvalidTokenError as e: #pylint: disable=E1101
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_INVALID_TOKEN,
                                originalError=e)

    @logFunc()
    def encodePayloadToJWT(self, payload):
        """
        Create JWT from payload
        Args
          payload: payload of data to encode
        Returns
          token
        """
        return jwt.encode(payload, key=self.secret, #pylint: disable=E1101
                          algorithm=self.algorithm).decode("utf-8")
