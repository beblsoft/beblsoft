#!/usr/bin/env python3
"""
 NAME:
  account_test.py

 DESCRIPTION
  Account JWT Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.webToken.account import AccountJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountJWTTestCase(CommonTestCase):
    """
    Account JWT Test Case
    """
    @logFunc()
    def test_encodeDecode(self):
        """
        Test encoding and decoding
        """
        aID         = 5
        readDB      = "WickedAwesomeReadDB@foo.com"
        writeDB     = "WickedAwesomeWriteDB@foo.com"
        accountJWT  = self.mgr.jwt.account.encodeJWT(aID=aID, readDB=readDB, writeDB=writeDB)
        accountAD   = self.mgr.jwt.account.decodeJWT(token=accountJWT)
        self.assertEqual(accountAD.version, AccountJWT.VERSION_1)
        self.assertEqual(accountAD.aID, aID)
        self.assertEqual(accountAD.readDB, readDB)
        self.assertEqual(accountAD.writeDB, writeDB)
