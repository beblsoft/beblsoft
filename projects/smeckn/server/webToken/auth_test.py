#!/usr/bin/env python3
"""
 NAME:
  auth_test.py

 DESCRIPTION
  Auth JWT Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.webToken.auth import AuthJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AuthJWTTestCase(CommonTestCase):
    """
    Auth JWT Test Case
    """
    @logFunc()
    def test_encodeDecode(self):
        """
        Test encoding and decoding
        """
        aID     = 5
        aAdmin  = True
        authJWT = self.mgr.jwt.auth.encodeJWT(aID=aID, aAdmin=aAdmin)
        authAD  = self.mgr.jwt.auth.decodeJWT(token=authJWT)
        self.assertEqual(authAD.version, AuthJWT.VERSION_1)
        self.assertEqual(authAD.aID, aID)
        self.assertEqual(authAD.aAdmin, aAdmin)

    @logFunc()
    def test_adminToken(self):
        """
        Test admin token
        """
        authAD  = self.mgr.jwt.auth.decodeJWT(token=self.cfg.DBP_ADMIN_AUTH_TOKEN)
        self.assertEqual(authAD.version, AuthJWT.VERSION_1)
        self.assertEqual(authAD.aID, AuthJWT.ADMIN_AID)
        self.assertEqual(authAD.aAdmin, True)
