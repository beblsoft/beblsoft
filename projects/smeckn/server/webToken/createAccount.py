#!/usr/bin/env python3
"""
 NAME:
  createAccount.py

 DESCRIPTION
  Create Account JSON Web Token Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from smeckn.server.webToken import BaseJWT
from smeckn.server.gDB.account.model import AccountType
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.log.bLogFunc import logFunc


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- CREATE ACCOUNT JWT CLASS --------------------- #
class CreateAccountJWT(BaseJWT):
    """
    Create Account JWT Class

    VERSION_1 fields
      version
      email
      password
      aType
    """
    VERSION_1 = 1

    @logFunc()
    def decodeJWT(self, token):  # pylint: disable=R0201
        """
        Decode JWT
        Args
          token: JWT
        Returns
          Attr Dict, see below
        """
        payload  = self.decodePayloadFromJWT(token)
        try:
            ad          = BeblsoftAttrDict()
            ad.version  = payload.get("version")
            ad.email    = payload.get("email")
            ad.password = payload.get("password")
            ad.aType    = AccountType[payload.get("aType")]
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_PAYLOAD,
                                msg="payload={}".format(payload),
                                originalError=e)
        return ad

    @logFunc()
    def encodeJWT(self, email, password, aType):
        """
        Create token
        Args
          email:
            account email
          password:
            account password
          aType:
            account type
            Ex. AccountType.TEST
        Returns
          payload
        """
        iat     = datetime.utcnow()
        exp     = iat + self.defaultExpDelta
        nbf     = iat + self.defaultNBFDelta
        payload = {
            "version": CreateAccountJWT.VERSION_1,
            "email": email,
            "password": password,
            "aType": aType.name,
            "iat": iat,
            "exp": exp,
            "nbf": nbf
        }
        return self.encodePayloadToJWT(payload)
