#!/usr/bin/env python3
"""
 NAME:
  createAccount_test.py

 DESCRIPTION
  Create Account JWT Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.test.common import CommonTestCase
from smeckn.server.webToken.createAccount import CreateAccountJWT
from smeckn.server.gDB.account.model import AccountType


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CreateAccountJWTTestCase(CommonTestCase):
    """
    Create Account JWT Test Case
    """
    @logFunc()
    def test_encodeDecode(self):
        """
        Test encoding and decoding
        """
        email            = "darryl.black@smeckn.com"
        password         = "WhuddaGuy"
        aType            = AccountType.TEST
        createAccountJWT = self.mgr.jwt.createAccount.encodeJWT(email=email, password=password, aType=aType)
        createAccountAD  = self.mgr.jwt.createAccount.decodeJWT(token=createAccountJWT)
        self.assertEqual(createAccountAD.version, CreateAccountJWT.VERSION_1)
        self.assertEqual(createAccountAD.email, email)
        self.assertEqual(createAccountAD.password, password)
        self.assertEqual(createAccountAD.aType, aType)
