#!/usr/bin/env python3
"""
 NAME:
  cart_test.py

 DESCRIPTION
  Cart JWT Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode
from smeckn.server.test.common import CommonTestCase
from smeckn.server.aDB.unitLedger.model import UnitType
from smeckn.server.payments.cart.models import CartCostModel, CartModel


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class CartJWTTestCase(CommonTestCase):
    """
    Cart JWT Test Case
    """
    @logFunc()
    def test_encodeDecode(self):
        """
        Test encoding and decoding
        """
        count              = 5
        costAmount         = 100
        bCurrencyCode      = BeblsoftCurrencyCode.USD
        totalCost          = 500

        cartCostModelList  = []
        for unitType in list(UnitType):
            cartCostModel  = CartCostModel(unitType=unitType, count=count,
                                           costAmount = costAmount)
            cartCostModelList.append(cartCostModel)

        cartModel          = CartModel(cartCostModelList=cartCostModelList, totalCost=totalCost,
                                       bCurrencyCode=bCurrencyCode)
        cartJWT            = self.mgr.jwt.cart.encodeJWT(cartModel=cartModel)
        cartModelDecoded   = self.mgr.jwt.cart.decodeJWT(token=cartJWT)
        self.assertEqual(cartModel, cartModelDecoded)
