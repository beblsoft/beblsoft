#!/usr/bin/env python3
"""
 NAME:
  auth.py

 DESCRIPTION
  Authentication JSON Web Token Functionality
"""


# ---------------------------- IMPORTS -------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.server.webToken import BaseJWT


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__name__)


# ---------------------------- AUTH JWT CLASS ------------------------------- #
class AuthJWT(BaseJWT):
    """
    Auth JWT Class

    VERSION_1 fields
      version
      aID
      aAdmin
    """
    VERSION_1 = 1
    ADMIN_AID = -1

    @logFunc()
    def decodeJWT(self, token):  # pylint: disable=R0201
        """
        Decode token. Allow backdoor for DBP_ADMIN_AUTH_TOKEN
        Args
          token: JWT
        Returns
          Attr Dict, see below
        """
        ad      = BeblsoftAttrDict()
        payload=  None
        try:
            if token == self.cfg.DBP_ADMIN_AUTH_TOKEN:
                ad.version = AuthJWT.VERSION_1
                ad.aID     = AuthJWT.ADMIN_AID
                ad.aAdmin  = True
            else:
                payload    = self.decodePayloadFromJWT(token)
                ad.version = payload.get("version")
                ad.aID     = payload.get("aID")
                ad.aAdmin  = payload.get("aAdmin")
        except BeblsoftError as e: # Let BeblsoftErrors pass through
            raise e
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.JWT_NO_PAYLOAD,
                                msg="payload={}".format(payload),
                                originalError=e)
        return ad

    @logFunc()
    def encodeJWT(self, aID, aAdmin, expDelta=None):
        """
        Create token
        Args
          aID:
            account id
          aAdmin:
            True if account is admin
          expDelta:
            expiration delta to override default
            Ex. timedelta(days=30)
        Returns
          payload
        """
        iat     = datetime.utcnow()
        exp     = iat + self.defaultExpDelta
        if expDelta:
            exp = iat + expDelta
        nbf     = iat + self.defaultNBFDelta
        payload = {
            "version": AuthJWT.VERSION_1,
            "aID": aID,
            "aAdmin": aAdmin,
            "iat": iat,
            "exp": exp,
            "nbf": nbf
        }
        return self.encodePayloadToJWT(payload)
