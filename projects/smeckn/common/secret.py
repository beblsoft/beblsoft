#!/usr/bin/env python3
"""
NAME:
 secret.py

DESCRIPTION
 Secret Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.SecretsManager.bCachedSecret import BeblsoftCachedSecret


# ----------------------- SECRET TYPE --------------------------------------- #
class SecretType(enum.Enum):
    """
    Secret Type Enumeration
    """
    Test       = enum.auto()
    Production = enum.auto()

    def __str__(self):
        return "[{} code={}]".format(self.__class__.__name__, self.name)

    @property
    def awsName(self):
        """
        Return Secret name
        """
        return "Smeckn{}Secret".format(self.name)

    @logFunc()
    def getBSecret(self, **kwargs):
        """
        Return corresponding BeblsoftSecret Object
        Args
          **kwargs: Args
        """
        return BeblsoftCachedSecret(name=self.awsName, **kwargs)
