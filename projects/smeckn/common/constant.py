#!/usr/bin/env python3
"""
NAME:
 constant.py

DESCRIPTION
 Smeckn Constants
"""

# ----------------------- IMPORTS ------------------------------------------- #
import os
from pathlib import Path
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict


# ----------------------- CONSTANT ------------------------------------------ #
class Constant():
    """
    Constant
    """

    def __init__(self):
        """
        Initialize Object
        """

        # Dirs -----------------------------------
        self.dirs                       = BeblsoftAttrDict()
        self.dirs.bebl                  = Path(__file__).parents[3]  # beblosft
        self.dirs.base                  = os.path.join(self.dirs.bebl, "base")
        self.dirs.tools                 = os.path.join(self.dirs.bebl, "tools")
        self.dirs.toolsBash             = os.path.join(self.dirs.tools, "bash")
        self.dirs.projects              = os.path.join(self.dirs.bebl, "projects")
        self.dirs.smeckn                = Path(__file__).parents[1]
        self.dirs.docs                  = os.path.join(self.dirs.smeckn, "docs")
        self.dirs.smecknCommon          = os.path.join(self.dirs.smeckn, "common")
        self.dirs.server                = os.path.join(self.dirs.smeckn, "server")
        self.dirs.webAPI                = os.path.join(self.dirs.smeckn, "webAPI")
        self.dirs.webClient             = os.path.join(self.dirs.smeckn, "webClient")
        self.dirs.webClientSRC          = os.path.join(self.dirs.webClient, "src")
        self.dirs.webClientCypress      = os.path.join(self.dirs.webClient, "cypress")
        self.dirs.webClientDist         = os.path.join(self.dirs.webClient, "dist")
        self.dirs.aws                   = os.path.join(self.dirs.smeckn, "aws")
        self.dirs.awsConfig             = os.path.join(self.dirs.aws, "config")
        self.dirs.awsManager            = os.path.join(self.dirs.aws, "manager")
        self.dirs.awsManagerTemplate    = os.path.join(self.dirs.awsManager, "templates")
        self.dirs.awsMySQLClient        = os.path.join(self.dirs.aws, "static", "mysqlclient")
        self.dirs.licenseFinder         = os.path.join(self.dirs.smeckn, "licenseFinder")
        self.dirs.email                 = os.path.join(self.dirs.smeckn, "email")
        self.dirs.emailHTML             = os.path.join(self.dirs.email, "html")
        self.dirs.emailPublic           = os.path.join(self.dirs.email, "public")

        # Web Client Paths -----------------------
        self.wcp                        = BeblsoftAttrDict()
        self.wcp.app                    = "/app"
        self.wcp.userGuide              = "/userGuide"
        self.wcp.email                  = "/email"
