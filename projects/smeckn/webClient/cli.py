#!/usr/bin/env python
"""
 NAME:
  cli.py

DESCRIPTION
 Smeckn Web Client CLI
"""


# ----------------------- IMPORTS ------------------------------------------- #
import os
import logging
from datetime import datetime
import git
import click
from base.bebl.python.run.bRun import run
from base.bebl.python.log.bLog import BeblsoftLog
from base.bebl.python.error.bError import BeblsoftError
from smeckn.common.secret import SecretType
from smeckn.webClient.gc import gc
from smeckn.common.constant import Constant


# ----------------------- GLOBALS ------------------------------------------- #
logger         = logging.getLogger(__name__)
const          = Constant()
curDir         = os.path.dirname(os.path.realpath(__file__))
defaultLogFile = os.path.join(curDir, "cli.log")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--secret", "-s", type=click.Choice(st.name for st in list(SecretType)), default="Test",
              help="Specify secret type. Default=Test")
@click.option("--logfile", default=defaultLogFile, type=click.Path(writable=True),
              help="Specify log file. Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(secret, logfile, appendlog, verbose):
    """
    Smeckn Web Client Command Line Interface
    """
    gc.webClient.cfg.bSecret = SecretType[secret].getBSecret()
    gc.bLog                  = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- INSPECT ------------------------------------------- #
@cli.command()
def inspect():
    """
    Inspect
    """
    cmd = "npx vue-cli-service inspect"
    run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient, shell=True,
        raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- BROWSERSLIST -------------------------------------- #
@cli.command()
def browserslist():
    """
    List Browsers
    """
    cmd = "npx browserslist"
    (_, _) = run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient, shell=True,
                 raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- ESLINT -------------------------------------------- #
@cli.command()
@click.option("--fix", is_flag=True, default=False, help="Fix error")
def eslint(fix):
    """
    Eslint
    """
    passed = False
    cmd = "npx vue-cli-service lint {}".format(
        "" if fix else "--no-fix")
    (_, output) = run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient, shell=True,
                      raiseOnStatus=True, bufferedLogFunc=logger.info)
    for line in output.splitlines():
        if "No lint errors found!" in line:
            passed = True
    if not passed:
        raise BeblsoftError(output=output)


# ----------------------- START --------------------------------------------- #
@cli.command()
@click.option("--apidomain", default="https://localhost:5100", help="API domain")
@click.option("--listenip", default="0.0.0.0", help="IP Address to listen on")
@click.option("--listenport", default=8021, help="Port to listen on")
@click.option("--openbrowser", is_flag=True, default=False, help="Open browser")
@click.option("--http", is_flag=True, default=False, help="Run over http")
def start(apidomain, listenip, listenport, openbrowser, http):
    """
    Start
    """
    cmd = """VUE_APP_ENVIRONMENT='Local'                            \
             VUE_APP_GIT_COMMIT_SHA='{}'                            \
             VUE_APP_SENTRY_WEBCLIENT_DSN='{}'                      \
             VUE_APP_GOOGLE_ANALYTICS_WEBCLIENT_ID='{}'             \
             VUE_APP_API_DOMAIN='{}'                                \
             VUE_APP_PORT='{}'                                      \
             VUE_APP_BASE_URL='{}'                                  \
             VUE_APP_USERGUIDE_BASE_URL='{}'                        \
             npx vue-cli-service serve --mode development {} {} {} {}""".format(
        git.Repo(search_parent_directories=True).head.object.hexsha,
        gc.webClient.cfg.bSecret.SENTRY_WEBCLIENT_DSN,
        gc.webClient.cfg.bSecret.GOOGLE_ANALYTICS_WEBCLIENT_ID,
        apidomain,
        listenport,
        gc.webClient.cfg.const.wcp.app,
        gc.webClient.cfg.const.wcp.userGuide,
        "--host={}".format(listenip),
        "--port={}".format(listenport),
        "--open" if openbrowser else "",
        "" if http else "--https")
    run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient, shell=True, bufferedLogFunc=logger.info)


@cli.command()
@click.option("--listenip", default="0.0.0.0", help="IP Address to listen on")
@click.option("--listenport", default=8022, help="Port to listen on")
def start_user_guide(listenip, listenport):
    """
    Start User Guide Server
    """
    cmd = """VUE_APP_USERGUIDE_BASE_URL='{}'    \
             npx vuepress dev {} {} userGuide""".format(
        gc.webClient.cfg.const.wcp.userGuide,
        "--host={}".format(listenip),
        "--port={}".format(listenport))
    run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient,
        shell=True, bufferedLogFunc=logger.info)


# ----------------------- TEST ---------------------------------------------- #
@cli.command()
@click.option("--clientdomain", default="https://localhost:8021", help="Client Domain")
@click.option("--console", is_flag=True, default=False, help="Run with cypress console")
@click.option("--noexit", is_flag=True, default=False, help="Don't exit runner")
@click.option("--fileregex", default="**/*.spec.js", help="File regex to run")
@click.option("--updatesnapshots", is_flag=True, default=False, help="Update all image snapshots")
@click.option("--totaljobs", default=1, help="Total number of jobs running in parallel")
@click.option("--jobidx", default=0, help="Job index of total jobs running in parallel [0:totaljobs-1]")
def test(clientdomain, console, noexit, fileregex, updatesnapshots, totaljobs, jobidx):
    """
    Test
    """
    cmd = """node cypress/main.js                                                    \
              --client-domain='{}' --client-public-path='{}' --admin-auth-token='{}' \
              --recaptcha-passthrough-token='{}' --file-regex='{}'                   \
              {} {} {}                                                               \
              --total-jobs={} --job-idx={}
              """.format(
        clientdomain,
        gc.webClient.cfg.const.wcp.app,
        gc.webClient.cfg.bSecret.DBP_ADMIN_AUTH_TOKEN,
        gc.webClient.cfg.bSecret.RECAPTCHA_PASSTHROUGH_TOKEN,
        fileregex,
        "--console" if console else "",
        "--no-exit" if noexit else "",
        "--update-snapshots" if updatesnapshots else "",
        totaljobs,
        jobidx)

    # Run tests and report results
    (_, output) = run(cmd=[cmd], cwd=gc.webClient.cfg.const.dirs.webClient,
                      shell=True, raiseOnStatus=True, bufferedLogFunc=logger.info)
    for line in output.splitlines():
        if "failed" in line:
            raise BeblsoftError(msg="Test Failed output={}".format(output))


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as e:  # pylint: disable=W0703
        # Raise so exit code is set
        raise e
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
