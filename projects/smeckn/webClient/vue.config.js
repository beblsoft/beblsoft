/**
 * @file Vue CLI Configuration
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = {
  publicPath: `${process.env.VUE_APP_BASE_URL}/`,
  outputDir: `dist${process.env.VUE_APP_BASE_URL}/`,
  lintOnSave: true,
  devServer: {
    // Since the publicPath isn't '/' we need to tell the browser where it
    // can reach the devServer for hot module reloading
    public: `https://localhost:${process.env.VUE_APP_PORT}`,
  },
  css: {
    loaderOptions: {
      sass: {
        // SCSS imported by all components with custom styles ['~@/' is an alias to 'src/']
        data: '@import "~@/scss/indexScoped.scss";'
      },
    }
  },
  transpileDependencies: [],
  chainWebpack: (config) => {
    config.stats({ warnings: false });
    config.resolve.symlinks(false);
    config.devtool('source-map');
  },
  // Control chunk sizes
  // See: https://stackoverflow.com/questions/51816020/how-to-break-the-js-files-into-chunks-in-vue-cli-3-with-webpack-performance-obje
  configureWebpack: {
    plugins: [/* new BundleAnalyzerPlugin() */],
    optimization: {
      splitChunks: { minSize: 10000, maxSize: 250000, }
    },
  },
  productionSourceMap: true
};
