# Social Media: The Good, Bad and Ugly

![Rearview Mirror](./WhatIsSmeckn-asphalt-automobile-blur-2705754.jpg)

## What is Social Media?

The definition of *Social Media* is pretty broad; social media is any website or application that
enables you to create and share digital content with others in a social network. Facebook, Twitter,
Instagram, LinkedIn, and Gmail are some of the most prevalent social media platforms.

Social media gives you a digital presence, wherever you are physically. Through your interactions with others
on social media, you accumulate a digital footprint over time. Your friends, family, work colleagues,
and everyday acquaintances interact with the virtual you through your social media. These real-time
electronic communications define your digital footprint. Whenever you post or tweet, your data
becomes available across the globe nearly instanteously.

Social media has revolutionized communication in the 21st century. It keeps us connected
with people across the planet, gives a voice to the voiceless, and works as a platform where anyone
can share their thoughts and ideas.

However, as with any new technology, social media is not without downsides. It has led to the
proliferation of negativity, criminal activity, and cyberbullying to name a few. As we share our
lives on social media, our personal data becomes more accessible not only to our friends
and family, but also to our prospective employers, customers, and even complete strangers.

### Positives

Social Media has the following positive aspects. It:

- enables communication with people across the globe,
- facilitates shared experiences with others,
- allows you to project an authentic image of yourself electronically,
- provides a professional network helping you find a job, and
- serves personalized and relevant content for your consumption.

### Negatives

Improper use of social media has led to the following negative aspects.

- Proliferation of negative data can cost you a job, a friend, or an opportunity to meet others.
- Misinterpreted posts can result in petty disagreement or hostility.
- Global chatrooms often faciliate cyber-bulling and criminal activity.
- Constantly viewing friend's highlight feeds can make you fall prey to comparison and feel down.
- Avoiding a real life in favor of a virtual one can lead you to loneliness.
- Careless use of your credentials on social media can result in identity theft.

## How much do you use Social Media?

Like most things, use of social media should be balanced with other aspects of your life.
It can be healthy to unplug for awhile and resist the temptation to check or engage everytime
your cell phone beeps.  Don’t become a slave to social media; if you feel like you are becoming
a social media indentured servant, consider putting yourself on a social media
diet. Be careful not to waste the time of your friends or family by posting your every move.

## Who looks at your Social Media?

Given the sophisticated search tools on the web, access to your social media content is more
prevalent than you may think.  In addition to your friends and family, complete strangers
may search and find your social media. Parents, relatives, perspective employers and school
administrators can discover disparaging aspects of your social media resulting in loss of face or
even loss of a perspective job. It is very common for employers to Google your name with the hopes of
digitally vetting you before making a potential hire.

## Defining Your Digital Footprint

Over time, you generate  lots of social media content including posts, tweets, photos, and email.
You also respond to the content of others. These actions result in the accumulation
of gigabytes of data linked directly back to you; this data becomes your digital footprint--how others
perceive and see you electronically.

In the same way that most companies use social media to market their products and attract customers,
your social media becomes how others see you electronically. Just as companies strive to project a positive,
inspirational image of themselves and their products, individuals can do the same.

Instead of trying to mimic the behavior of others or creating a false indentify, we recommend being
your authentic self online. Your unique and authentic digital footprint adds value that noone else can add.
That said, if you're anything like us, your identity has changed over the course of your social media
usage. Be mindful that you still can be held accountable for your digital past. In light of this,
we recommend that you review your complete history and make sure its fully consistent with who you
really are.

More specifically, there are many attributes of social media that shape your digital footprint.
These include:

- Frequency of use
- Circle of friends
- Data content: text, photos, and videos
- Likes and dislikes
- Reactions to your content and the content of others

Monitoring these attributes over time can help you improve your social media image.

## Be a Positive Force

Our recommendation is to use social media to surround yourself with an abundance of positivity.
Write uplifting posts and comments to energize others. Rather than getting into divisive political
debates, be a bridge builder who seeks to find common ground with all. Instead of posting drunk
pictures of yourself, post pictures of yourself simply having a good time without the alcohol.
In our world filled with negativity, help us shift the tide by becoming an an ambassador of positivity.
Positivity promotes more positivity!