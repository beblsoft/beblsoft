# Oprah:  Neutral With a Twist of Positivity

<style>.embed-container {position: relative; padding-bottom: 120%; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
<div class='embed-container'><iframe src='https://instagram.com/p/B2y0R8chZec/embed/' frameborder='0' scrolling='no' allowtransparency='true'></iframe></div>

Everyone knows who Oprah is. She is an amazing woman who lives life to the fullest and who is always
looking to give back. Oprah is truly inspirational and, at the same time, truly authentic. She has
millions of followers as she is revered by many.

At *Smeckn* we hypothesized what Oprah's true character is. We characterized Oprah as being like an
ambassor, never negative, but, at the same time, not a sticky sweet Pollyanna who is always
glowingly positive. Oprah seems real because her social media comes across as authentic and believable.

We decided to use our *Quick Analysis* Feature to test how good *Smeckn's* AI is.
We took a small sample size of recent Oprah tweets and cut and pasted them into our Quick Analysis
tool.  Oprah's tweets consistently had zero negativity, some positivity but lots of neutrality
(see tweets and screenshots below). We were encouraged by the results given such short snippets of text.

In a nutshell we found her social media tweets to match how we see Oprah.  The few tweets we
analyzed were void of negativity. A quick sample of Oprah's recent digital footprint re-enforced
why Oprah has become a role model for many--the digital Oprah is the authentic Oprah.

Given, our sample size was very small and a thorough scientist would spend more time analyzing
Oprah's tweets with a lot more randomly chosen samples.  That said, people don't see your
every post or tweet so make every one count! Stay neutral and positive!

Does your social media reflect the authentic you? If not, change your social media sentiment,
one post at a time and let the *digital you* become the *authentic you*.

----------------------------------------------------------------------------------------------------

Recent (9/19) Tweets from Oprah:

>Oprah Winfrey @Oprah Sep 13
>
>Tickets for #Oprahs2020VisionTour are officially on sale TODAY! Bring along your Gayle,
your siblings, your parents, your colleagues, and your loved ones so we can all collectively
make 2020 the year of transformation and triumph. Click here for tickets:
https://ticketmaster.com/oprah-winfrey-tickets/artist/712504

![Oprah Smeckn Analysis 1](./OprahSmecknAnalysis1.png)

>Oprah Winfrey @Oprah 23h
>
>The only thing more thrilling than being captivated by a book is being able to share it with others.
Which is why I’m excited to bring @oprahsbookclub to @apple starting TODAY! My first pick,
The Water Dancer by the brilliant Ta-Nehisi Coates. It will enthrall you. #ReadwithUs

![Oprah Smeckn Analysis 2](./OprahSmecknAnalysis2.png)

















