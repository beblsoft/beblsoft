# Helping Others

![Helping Hand](./HelpOthers-black-and-white-close-up-dark-167964.jpg)

We all have special gifts. Sharing your gifts with others is bound to put a smile on your face and help
others in the process. Here are some famous quotes on giving:

- “The meaning of life is to find your gift. The purpose of life is to give it away.” ―Pablo Picasso
- “Happiness doesn’t result from what we get, but from what we give.” ―Ben Carson
- “No one is useless in this world who lightens the burdens of another.” ―Charles Dickens
- “Those who are happiest are those who do the most for others.” —Booker T. Washington
- “Remember that the happiest people are not those getting more, but those giving more.” ―H.
Jackson Brown Jr.
- “Since you get more joy out of giving joy to others, you should put a good deal of thought into
the happiness that you are able to give.” ―Eleanor Roosevelt
- “We make a living by what we get. We make a life by what we give.” ―Winston S. Churchill

Use your social media to help others. Post entries that inspire and pictures that evoke joy.
Put the first smile of the day on your friends' faces.  Help others get a job or navigate through a
tough time. Make a difference, one post at a time.


