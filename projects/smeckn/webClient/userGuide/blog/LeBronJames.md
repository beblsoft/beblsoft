# LeBron James:  A True Inspiration

<style>.embed-container {position: relative; padding-bottom: 120%; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
<div class='embed-container'><iframe src='https://instagram.com/p/B3Sq3Z3ADu8/embed/' frameborder='0' scrolling='no' allowtransparency='true'></iframe></div>

If you're a basketball fan then you know who LeBron James is:  Top Laker player (maybe the greatest of
all time), philantropist, and promoter of good. LeBron is an inspiration for all.  James gives,
gives, gives.  He supports numereous non-profit organizations and helps others raise money for
various causes.  So we asked, what is his sentiment on twitter; what does he tweet?  The results
speak for themselves. His social media reflects the true LeBron--a amazing legend. We can all
learn from LeBron.

>On Oct 10, 2019 LeBron retweeted from @IPROMISESchool:  ...strategies, daily promise circles to check in, and teachers
>who provide constant love and support. Mental health matters, and family doesn’t let family walk
>alone. ❤️

![LeBron James Smeckn Analysis1](./LeBronJamesSmecknAnalysis1.png)

>On Oct 10, 2019 LeBron retweeted from @IPROMISESchool:  For many of our kids at IPS, life is a
rollercoaster. It’s important that they know, whether high or low, whatever they’re feeling is
perfectly ok. That’s why IPS works day in and day out to provide our family members with on-site
mental health supports, coping mechanisms and...

![LeBron James Smeckn Analysis1](./LeBronJamesSmecknAnalysis2.png)