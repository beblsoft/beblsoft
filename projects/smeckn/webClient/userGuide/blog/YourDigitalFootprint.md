# Your Digital Footprint

![Footprints at the Beach](./YourDigitalFootprint-photo-1430462708036-7ef5363d56d8.jpg)

The collection of all your social media content defines your digital footprint.  Your digital
footprint is how the world sees you electronically.  It is important that your digital footprint
matches the true, authentic you.

Are you familiar with an ocean's high tide and low tide?  In short, high tide is when the ocean comes
closer to the shore and, in doing so, washes away all the old footprints made just hours before.  High tide
happens twice a day giving way for a fresh, clean beach at low tide.

We all have digital footprints that we are not so proud of. You know, the ones that are negative or
hurtful to others. Use *Smeckn* to help you find these footprints so you can take them down.
Let *Smeckn* be like a high tide cleansing followed by a low tide clean beach, giving way to an
*authentic digital you*.  Moving forward, periodically check your beach of
social media content and "high tide away" any new negative footprints to keep your beach pristine.

