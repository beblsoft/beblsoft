# Tony Robbins:  Challenging Our AI

<style>.embed-container {position: relative; padding-bottom: 120%; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
<div class='embed-container'><iframe src='https://instagram.com/p/B3AqOZfH-wD/embed/' frameborder='0' scrolling='no' allowtransparency='true'></iframe></div>

No one would argue that Tony Robbins is one of the most successful motivational coaches out there
who consistently promotes the power of positivity--a true pillar of life's best practices. With
that, we decided to *smeck* a few of his recent twitter posts. As expected, we found some with
unprecedented positivity and but one tweet that was evaluated as somewhat negative. How could that be,
Tony Robbins, negative? Spoiler alert, AI is not perfect and, with very little context or complex
wording; sometimes AI gets things wrong.

>On Sep 21, 2019 Tony tweeted:  It's NOT what we GET in life that makes us truly FULFILLED.
>It’s what we GIVE!! After all, the secret to LIVING is GIVING!

![Tony Robbins Smeckn Analysis1](./TonyRobbinsSmecknAnalysis1.png)

Our AI algorithm misinterpreted the NOT phrase as negative. We humans know the phrase, "It's NOT
what we GET", is a way to strengthen the message. So we simplified Tony's tweet and re-analyzed it:
Tony paraphrased: "It’s what we GIVE in life that makes us truly FULFILLED!! After all, the secret
to LIVING is GIVING!"

![Tony Robbins Smeckn Analysis1](./TonyRobbinsSmecknAnalysis2.png)

Sure enough, our AI validated his intended positive message.

Our AI algorithm for sentiment analysis isn't perfect; however, we expect it will get better over
time. Don't always assume negative sentiment is really negative. Happy smeckn.
