# Let Social Media Lift You Up!

![Two Happy Youths Jumping](./UseSMToLiftYouUp-youth-active-jump-happy-40815.jpeg)

Interacting using social media is a way for you to socialize, have fun, stay current, and
build relationships with others. Even if you only use social media at work to promote your companies
products, it is best if your posts are positive, put a smile on your face and make others happy too.

## Does Social Media Make You Happy?

Does social media make you happy? If not, what aspects do not make you happy? Are there aspects of
social media that make you stressed, sad, or negative? If so, stop pursuing those aspects. Stop replying,
answering or promoting things in your life that make you stressed, sad, or negative!
Write positive posts and post joyful pictures. Make your social media infectiously positive bringing
joy to you and your friends.

## Take the High Road

Start taking the high road with your social media. Help others! Provide content-rich, thoughtful, happy
posts and pictures on your social media. If someone is feeling down, help build them back up!

As tough as it, look for the positive in everything that you do and post.  Think that noone is
intentially trying to be hurtful or do a bad job in life or relationships. Resist chiming into
negativity; and, if possible, turn a negative into a positive.

Use *Smeckn* to analyze, measure and improve your social media sentiment over time. See if you can
draw a correlation between your positive posts and feeling good about yourself and others.  Observe
whether you find more joy in life for life for you and your friends simply by taking that often
difficult high road with your social media.
