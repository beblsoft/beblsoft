# Building your Brand

![Blank Canvas Of Things to be Branded](./BuildYourBrand-blank-branding-identity-business-cards-6372.jpg)

It is no secret that companies across the globe use Social Media to build their brand. Social Media
is a great way to project a polished company image, spread the word, and get new customers.

Why not use social media to build your own brand? Create an authentic social media brand that
reflects your current self, attracts friends, and helps others.

To create your brand, follow the same guidelines used by companies:

- Be consistent across social media platforms. Don’t be Dr. Jekyll on Facebook and Mr. Hyde on
Twitter. Be yourself and use the same voice and style on all social media platforms.
- Add value to social media by telling thought provoking accounts that help educate and grow others.
- Resist gossiping and bringing others down. Build friends up; don't tear them down.
- Post relevant, simple, and valuable content. Don’t post your every move, just stuff that will make a difference.
- Practice a giving methodology. Give knowledge, insight, and help via your social media.
It has been said by many that those who focus on giving will implicitly receive abundance.

Let *Smeckn* help you sift through your myriad of social media data and help you remove the content
that is bad for your brand.

Keep monitoring and improving your brand over time ensuring that your authentic digital footprint
evolves with you.
