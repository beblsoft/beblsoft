# Taylor Swift:  Synonym for Positive

<style>.embed-container {position: relative; padding-bottom: 120%; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
<div class='embed-container'><iframe src='https://instagram.com/p/B1fgxY-DsvJ/embed/' frameborder='0' scrolling='no' allowtransparency='true'></iframe></div>

We all know her; teens love her.  She is an extremely talented singer/song writer--a music icon
with an unprecedented postive presence.  She exudes positivity in her digital footprint; her
social media represents the authentic Taylor.

Using totally non-judgemental AI algorithms, we analyzed a few recent tweets using
Smeckn's Quick Analysis Feature.  See for yourself, it just doesn't get any more positive
than Taylor.  Keep it up Taylor; we're all inspired by you.

----------------------------------------------------------------------------------------------------

Recent (9/19) Tweets from Taylor:

>Taylor Swift @taylorswift13 Aug 23
>
>This album is very much a celebration of love, in all its complexity, coziness, and chaos. It’s
the first album of mine that I’ve ever owned, and I couldn’t be more proud. I’m so excited that
#Lover is out NOW: https://taylorswift.lnk.to/Lover

![Taylor Smeckn Analysis1](./TaylorSmecknAnalysis1.png)

>Taylor Swift @taylorswift13 Sep 17
>
>The Lover album is open fields, sunsets, + SUMMER. I want to perform it in a way that feels
authentic. I want to go to some places I haven’t been and play festivals. Where we didn’t
have festivals, we made some. Introducing, Lover Fest East + West! http://TaylorSwift.com

![Taylor Smeckn Analysis2](./TaylorSmecknAnalysis2.png)












