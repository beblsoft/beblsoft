# Facebook Analysis

Serving billions of users, Facebook is one of the world’s premier social media platforms.
*Smeckn* enables Facebook users to search, filter, sort, and examine their data.

*Smeckn* analyzes all of your Facebook data and provides you with summary statistics including
your total likes and your posting frequency. We give you nice graphs a to help you visualize your
data over time. We also do sentiment analysis using AI algorithms to help you identify posts that
may be best deleted or edited. We provide you with direct links back to Facebook to help you make any
edits along the way.

## Adding Profile

Within your *Profile Group* go ahead and add your Facebook Profile. When you successfully add your
Facebook Profile to your *Profile Group*, *Smeckn* will sync your data.

![Add Facebook Profile](./AddFacebookProfile.png)

::: warning
Due to Facebook integration limitations, Smeckn can only sync your Facebook data once every 24-hours.
:::

## Visualizing Data

Once successfully added, you can individually review each of your your Facebook posts in the
"Facebook Post All" sidebar tab. This tab also allows you to search, sort, and filter your posts.

![Facebook Post All](./FacebookPostAll.png)

### Post Frequency

Facebook Post Frequency analysis allows you to see how often you are posting.

![Facebook Post Frequency](./FacebookPostFrequency.png)

### Post Likes

Facebook Post Like analysis shows you how frequently your posts are being liked by other users.

![Facebook Post Likes](./FacebookPostLikes.png)

### Post Comments

Facebook Post Comment analysis shows you how frequently your posts are being commented on by other users.

![Facebook Post Comments](./FacebookPostComments.png)

## Sentiment Analysis

You can do a *Sentiment Analysis* on individual posts or your entire Facebook profile. You will need
to have sufficient *Analysis Units* to do your analyses.

Analyze an individual post on the *Facebook Post All* screen:

![Facebook Post Sentiment Analyze One](./FacebookPostSentimentAnalyzeOne.png)

Analyze all remaining posts on the *Facebook Post Sentiment* screen:

![Facebook Post Sentiment Analyze All](./FacebookPostSentimentAnalyzeAll.png)

After you have analyzed some posts, the *Facebook Post Sentiment* screen shows you how your
sentiment trends over time.

![Facebook Post Sentiment](./FacebookPostSentiment.png)

## Cleaning up

In certain cases you may want to edit or delete a post. Just press the _View on Facebook_ link to view
the post within the context of your facebook profile where it can be edited and deleted.

![Facebook Post Edit](./FacebookPostEdit.png)
