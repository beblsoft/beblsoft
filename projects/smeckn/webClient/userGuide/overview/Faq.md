# FAQ

## How Does *Smeckn* work?

After creating your *Smeckn* account and adding a profile, *Smeckn* does an initial analysis of your
social media profile. This analysis goes through your social media and provides you with basic
statistics. You optionally can have *Smeckn* do a sentiment analysis helping you pinpoint opportunities
to make your social media footprint more positive.

## How often should you Smeckn?

Due to social media integration restrictions, *Smeckn* can only sync your data once a day. That said,
if you are frequently posting, wait at least a day before returning to *Smeckn* to analyze
your newly created data.

Establishing a regular analysis of your social media can be a healthy habit.  However, how often you
do your check-up (daily, weekly, monthly or yearly) is up to you.

## Once you analyze your data, what should you do?

*Smeckn* never writes or changes the contents of your social media; rather, *Smeckn* will
highlight which social media content to focus on. After running sentiment analysis, we recommend
taking the time to delete or edit negative content to make your digital footprint more positive.

## How can you track progress?

*Smeckn* provides graphs and charts that enable you to see your data over time. Keep an eye on those
data visualizations to monitor your progress.

## What data does *Smeckn* analyze?

*Smeckn* currently supports Facebook profile analysis; but hold tight, we will add more profile
types!