# Privacy and Security

![Privacy and Security](./PrivacyAndSecurity.png)

Here at Smeckn, we take your privacy and security very seriously. We believe that you, the data producer,
own your data and have the right to keep it private. To that end, we adhere to the following principles when
handling your data.

## On Demand Deletion

When Smeckn syncs your profile data, it makes a copy of the data within its own databases. This allows
you, for instance, to sort, filter, and search through your Facebook posts. When you are done analyzing
a specific profile, simply remove the profile from Smeckn. When this happens, Smeckn will permanently
delete the copy within its databases leaving no trace behind.

## No Third Party Sharing

Smeckn does not share your data with any third party entities. Once your data has been synced to our
databases, it is placed safely inside the Smeckn vault where only you can access it.

## Data Encryption

Smeckn encrypts all your data at rest and as it travels over the internet to your browser. This protects
you from malicious third parties attempting to access your data.

## Automated Analysis

Smeckn leverages automated AI algorithms, not humans, to analyze your data. AI attempts to be as objective
and judgement free as possible.

## Isolated from the Ground Up

When a new Smeckn user creates an account, Smeckn allocates a new database where that user’s
data is stored. This keeps the user’s dataset isolated in its own container where only that user
can access it.

