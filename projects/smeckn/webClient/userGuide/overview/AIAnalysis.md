# AI Analysis

## Text Sentiment Analysis

At the heart of *Smeckn*'s analysis is *Text Sentiment Analysis*. Text sentiment is divided into four
categories: **positive**, **negative**, **neutral**, and **mixed**. Using AI algorithms, *Smeckn*
scores individual social media text entries. Positive entries tend to have a higher positive
score whereas negative entries have a higher negative score.

Text Sentiment Analysis is supported for the following languages: German, English, Spanish,
French, Italian, and Portuguese. A single text sentiment analysis costs one text analysis unit
(explained below).

Although the Sentiment Analysis algorigthm works well, it certainly isn't perfect. It can yield
spurious results on short pieces of text or ambiguous phrases. The algorithm will improve over time
as it analyzes more data.

## Language Detection

*Smeckn* can detect the dominant language present in a piece of text. Language is done as
part of sentiment detection.

## Quick Text Analysis

Sometimes you just want to check out the sentiment of your text before actually posting it.
*Smeckn's* AI provides you with a non-judgmental second opinion.  Click on the **Text Analysis**
link in the middle of the menu bar.

![Text Analysis](./TextAnalysis.png)

## Analysis Units

*Analysis Units* are the currency that we use to perform our AI-based sentiment analysis. We charge
one *Text Analysis Unit* to analyze each piece up text up to 5000 characters long. We give you a few
free units to test out the application with payment required for additional units. The current
price of an *Analysis Unit* is given in the puchasing workflow.  Go to **Account->Purchase**
menu item to access the purchasing workflow.

![Purchase Analysis Units](./PurchaseAnalysisUnits.png)
