# Getting Started

Let's jump right in.

## How it works

![Getting Started](./GettingStarted.png)

### First you...

- Create a *Smeckn* Account.
- Create a Profile Group for yourself; give the Profile Group your name.  Profile Groups enable you
  to eventually check the Social Media accounts of others; this is particularly useful if you are a
  parent and want to monitor your kids activity.
- Add a Facebook Profile to your Profile Group.
- Wait for Smeckn to sync your Facebook data.
- Start viewing analytics on your data.

### After initial setup you...

- Periodically analyze new Social Media at your leisure.
- Perform Sentiment Analysis on some or all of *Smeckn* data.
- Edit your Social Media based on *Smeckn* results.
- Go back to the first bullet and repeat the process as you accumulate more Social Media.

## Account Hierarchy

*Smeckn* supports the following account hierarchy:

![Account Hierarchy](./AccountHierarchy.png)

### Smeckn Account

The Smeckn account is the top level in the hierarchy. You create and login to a *Smeckn* account
using your email address as your username.

### Profile Group

A *Profile Group* represents a group of social media profiles. One *Smeckn* account can contain multiple
profile groups each with their own profiles.

Profile groups allow you to organize a person’s social media profiles in a single group.
Profile groups help those who have many dependents keep their individual profiles organized.
For example, a parent may have multipe profile groups, one for each of their children.

Profile groups can be added via the profile group selection screen shown below. Profile groups
can be modified or deleted by clicking the **Manage Profile Groups** button.

![ProfileGroup Manage](./ProfileGroupManage.png)

When you run *Smeckn* for the first time, you will see that we have created a default
profile group for you.  You can rename this group to something more meaningful by clicking on the
**Manage Profile Groups** button. Note that if you have several profile groups, you can
quickly change the profile group in view with the top **Profile Group** menu item.

::: warning
Be careful when deleting a profile group as all of its underlying profiles and corresponding analyses
will be also deleted.
:::

### Profile

*Profiles* are your social media accounts that you use to log into each type of social media.
For example, you may have a Facebook profile, a Twitter profile and a LinkedIn profile.

