# Why *Smeckn*?

## From One of Our Founders

One day after accumulating years of Facebook Posts, I decided to scroll my Facebook home feed back to 2007.
As I journeyed back in digital time, the thought, *"who is this person?"*, immediately jumped into my head.

If you're anything like me and have time-traveled back through your social media, you probably have
had this same feeling. Most of us simply aren't the same people who we were 10 or 15 years ago when this
whole social media thing began.

## The Problem

A few of us have tried to clean up our digital past, but the process is painful and difficult.
It takes lots of energy to scroll through a long feed in the hopes of finding and deleting the
negative posts. In the end, we seem content to just ignore the problem.

Most of the time this seems to be an okay strategy... until you apply for that dream job and your new
employer finds those old, disparaging posts you made while at a bar with friends one night.

**What if there were an easier way to clean up our digital past?**

## Our Mission

Trying to answer that question gave birth to *Smeckn*. Our goal is to help you quickly analyze and
delete your digital past **so that no one can hold it against you**.

In a world filled with negativity, we believe that **everyone wins** when you clean up your digital
footprint. Let us help you quickly find and remove the negative needles in your social media haystack
and **make the digital you match the authentic you**.