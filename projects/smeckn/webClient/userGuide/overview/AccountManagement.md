# Account Management

On the upper right corner of the screen there is an **Account** menu.

![Account Menu](./AccountMenu.png)

## Changing your Password

Select **Account→Manage** to change your *Smeckn* Account Password.

![Account Update Password](./AccountUpdatePassword.png)

## Purchasing Analysis Units

Select **Account→Analysis Units→Purchase** to purchase analysis units.

![Account Purchase Analysis Units](./AccountPurchaseAnalysisUnits.png)

## Seeing Your Balance

Select **Account→Analysis Units→Balance** to see your remaining analysis units.

![Account Analysis Unit Balance](./AccountAnalysisUnitBalance.png)

## Adding Credit Cards

Select **Account→Payments→Credit Cards** to add one or more credit cards.

![Account Credit Cards](./AccountCreditCards.png)

## Reviewing Purchases

Select **Account→Payments→History** to see a history of your purchases.

![Account Payment History](./AccountPaymentHistory.png)
