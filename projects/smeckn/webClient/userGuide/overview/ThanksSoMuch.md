# Thanks so much!

Thank you so much for coming and using *Smeckn*.

The current version of *Smeckn* just scratches the surface on helping you improve your digital footprint.
Keep tuned into *Smeckn* as we continue to add new features to our product.

We sincerely hope that *Smeckn* helps you better manage your digital footprint.  We believe that
promoting positivity helps make our world a better place for all.

If you like *Smeckn*, we'd greatly appreciate it if you'd help us spread the word!

## Keep the Feedback Coming...

We are here to make *Smeckn* the most useful social media management tool for you. Please let us
know how to improve the application by sending email to <feedback@smeckn.com>.



