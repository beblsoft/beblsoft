# About

*Smeckn* stands for **Social Media Checking**.

*Smeckn*, Inc. is a Delaware-based company founded in 2019.

## Mission

Our mission is to stop the proliferation of negative social media and help people clean up their data
before it gets into the wrong hands. Here at *Smeckn*, we believe that everyone wins when one person
cleans up their social media.

*Smeckn* is a tool that uses AI to analyze your social media over time. Our goal is to fully equip
you with the tools to analyze, check, monitor, and clean up your social media. *Smeckn's* analyses
help you better manage your digital footprint and ensure that you are projecting an *authentic* you
to the world. Improving your digital image will undoubtedly influence how friends, family, employers,
and strangers view you online.

## Follow Us

You can us follow us on:

- [Twitter: @SmecknInc](https://twitter.com/SmecknInc)
- [LinkedIn](https://www.linkedin.com/company/smeckn)

