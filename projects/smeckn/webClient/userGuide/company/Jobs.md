# Jobs

## All Smeckn Employees

At Smeckn, we live and breathe our mission of helping our customers clean up their social media
data. We are looking for team members willing to embrace our mission and further our cause.
We want you to love coming into work every morning because you are make a tangible difference in our
world.

Specifically we're looking for team members with the following characteristics:

- integrity
- passion
- creativity
- commitment
- ability to work remotely and autonomously
- ability to learn and adapt quickly
- excellent verbal and written communication skills

As a member of the *Smeckn* team, you will be one of the few delivering solutions for *Smeckn* customers.
You will soon become a subject matter expert on providing the best social media analysis tools out there.

You will play a pivotal role in helping the *Smeckn* product scale globally.
With an estimated 4+ Billion social media users, we have a very large set of customers to help.
You’ll have exceptional problem solving skills, strong business judgment,
and demonstrated experience designing and delivering strong solutions on time, with quality.
You should be able to work both strategically and tactically, and be thrilled to take on new,
ambiguous projects that deliver on our worldwide mission to make social media content shine!

## Vue Developer

### Technology

Sitting at the top of the *Smeckn* stack is a *Vue* application. To create the next generation of
*Smeckn* features we are looking for a developer familiar with the following technologies:

- [JavaScript](https://www.javascript.com/) (both Node and Browser)
- [Vue](https://vuejs.org/)
- [Cypress](https://www.cypress.io/)
- [Git](https://git-scm.com/) and [Gitlab](https://gitlab.com/)
- [Google Analytics](https://developers.google.com/analytics/)
- [Sentry](https://sentry.io)

### Background

- 2+ years of JavaScript professional experience
- BS and/or MS in Computer Science or related field
- Exceptional organizational skills and attention to detail

### Interest

If interested, please send your resume and cover letter to <careers@smeckn.com>.

## Python AWS Developer

### Technology

Most of the *Smeckn* server logic is encapsulated in AWS Python Lambda Functions. These functions
leverage a pool of sharded MySQL servers. In light of this, we are looking for a developer familiar
with the following technologies:

- [Python](https://www.python.org/)
- [Docker](https://www.docker.com/)
- [AWS](https://aws.amazon.com/) and in particular [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html?id=docs_gateway)
- [MySQL](https://www.mysql.com/)
- [Git](https://git-scm.com/) and [Gitlab](https://gitlab.com/)
- [Sentry](https://sentry.io)

### Background

- 2+ years of Python and AWS professional experience
- BS and/or MS in Computer Science or related field
- Exceptional organizational skills and attention to detail

### Interest

If interested, please send your resume and cover letter to <careers@smeckn.com>.

## Customer Aquisition Expert

Hold tight, coming soon!

## Customer Service Expert (US)

Hold tight, coming soon!

## Customer Service Expert (Europe)

Hold tight, coming soon!



