# Contact

We welcome your feedback!  We want our users to absolutely *love* Smeckn and your feedback can help
us improve.

Please send feedback to: <feedback@smeckn.com>.

Thanks for your help!
