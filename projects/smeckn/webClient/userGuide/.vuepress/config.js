module.exports = {
  title: 'Smeckn User Guide',
  description: 'Social Media Analysis for Everyone',
  head: [
    ['link', { rel: 'icon', href: '/img/info-logo.png' }]
  ],
  host: 'localhost',
  port: 8022,
  base: `${process.env.VUE_APP_USERGUIDE_BASE_URL}/`,
  dest: `dist/${process.env.VUE_APP_USERGUIDE_BASE_URL}`,
  ga: 'UA-138051825-1',
  themeConfig: {
    logo: '/img/white-logo.png',
    lastUpdated: 'Last Updated', // string | boolean
    displayAllHeaders: false, // Only show subheaders when clicked on

    // Navbar
    nav: [
      { text: 'Overview', link: '/overview/WhySmeckn' },
      { text: 'Company', link: '/company/About' },
      { text: 'Blog', link: '/blog/SocialMedia' },
    ],

    // Sidebar
    sidebar: {
      '/company/': [
        'About',
        'Contact',
        'Jobs',
        'Legal',
        //'Press',
      ],
      '/overview/': [
        'WhySmeckn',
        'GettingStarted',
        'AIAnalysis',
        'Facebook',
        'AccountManagement',
        'PrivacyAndSecurity',
        'ThanksSoMuch',
        'Faq'
      ],
      '/blog/': [
        'SocialMedia',
        'Oprah',
        'BuildYourBrand',
        //'UseSMToLiftYouUp',
        //'HelpOthers',
        //'YourDigitalFootprint',
        //'TaylorSwift',
        //'TonyRobbins',
        //'LeBronJames',
      ],
      // fallback, should be last
      '/': []
    }
  }
}
