# Smeckn Image Documentation

## Colors

Bootstrap Colors from: https://www.color-hex.com/color-palette/5452

Pallete:
- <span style="background-color:#d9534f">#d9534f</span>:
  Use: emphasis, negative, cancel
- <span style="background-color:#f9f9f9">#f9f9f9</span>:
  Use: background
- <span style="background-color:#5bc0de">#5bc0de</span>:
  Use: buttons
- <span style="background-color:#5cb85c">#5cb85c</span>:
  Use: positive, check
- <span style="background-color:#428bca">#428bca</span>:
  Use: select/fill-in emphasis

## Logos

Logo images are duplicated throughout the Smeckn codebase.
When changing a file, make sure to update it everywhere it appears.

To see where it needs to be updated:
```bash
cd $SMECKN;
find . -name "white-logo-title.png"
```

## Flaticon Attribution

All images must be purchased before going live to production.

Icon made by [author link] from www.flaticon.com

| Image                                       | Link                                                                                                 | Description   |
|:--------------------------------------------|:-----------------------------------------------------------------------------------------------------|:--------------|
| facebook.png                                | https://www.flaticon.com/free-icon/facebook_145802                                                   |               |
| twitter.png                                 | https://www.flaticon.com/free-icon/twitter_145812                                                    |               |
| linkedin.png                                | https://www.flaticon.com/free-icon/linkedin_145807                                                   |               |
| instagram.png                               | https://www.flaticon.com/free-icon/instagram_145805                                                  |               |
| examination-blue.png, examination-white.png | https://www.flaticon.com/free-icon/analysing_69037                                                   | Used in logos |
| information.png                             | https://www.flaticon.com/free-icon/information_685815                                                |               |
| visa.png                                    | https://www.flaticon.com/free-icon/visa_196578#term=visa&page=1&position=1                           |               |
| mastercard.png                              | https://www.flaticon.com/free-icon/mastercard_196561#term=mastercard&page=1&position=2               |               |
| american-express.png                        | https://www.flaticon.com/free-icon/american-express_349228#term=american%20express&page=1&position=3 |               |
| discover.png                                | https://www.flaticon.com/free-icon/discover_349230#term=discover%20card&page=1&position=3            |               |
| diners-club.png                             | https://www.flaticon.com/free-icon/diners-club_196548#term=diners%20club&page=1&position=1           |               |
| jcb.png                                     | https://www.flaticon.com/free-icon/jcb_196559#term=jcb&page=1&position=1                             |               |
| unionpay.png                                | https://www.flaticon.com/free-icon/unionpay_825484#term=unionpay&page=1&position=3                   |               |
| credit-card.png                             | https://www.flaticon.com/free-icon/credit-card_633611#term=credit%20card&page=1&position=18          |               |
| up-arrow.png                                | https://www.flaticon.com/free-icon/chevron-arrow-down_25629#term=down%20arrow&page=1&position=21     |               |
| down-arrow.png                              | https://www.flaticon.com/free-icon/chevron-arrow-down_25629#term=down%20arrow&page=1&position=21     |               |
| left-arrow.png                              | https://www.flaticon.com/free-icon/chevron-arrow-down_25629#term=down%20arrow&page=1&position=21     |               |
| right-arrow.png                             | https://www.flaticon.com/free-icon/chevron-arrow-down_25629#term=down%20arrow&page=1&position=21     |               |
| double-up-arrow.png                         | https://www.flaticon.com/free-icon/double-left-chevron_25257#term=double%20arrow&page=1&position=3   |               |
| double-down-arrow.png                       | https://www.flaticon.com/free-icon/double-left-chevron_25257#term=double%20arrow&page=1&position=3   |               |
| double-left-arrow.png                       | https://www.flaticon.com/free-icon/double-left-chevron_25257#term=double%20arrow&page=1&position=3   |               |
| double-right-arrow.png                      | https://www.flaticon.com/free-icon/double-left-chevron_25257#term=double%20arrow&page=1&position=3   |               |
| home.png                                    | https://www.flaticon.com/free-icon/home_189083#term=home&page=1&position=31                          |               |
| plus-2.png                                  | https://www.flaticon.com/free-icon/add_148781#term=plus&page=1&position=10                           |               |
| calendar.png                                | https://www.flaticon.com/free-icon/calendar_660528                                                   |               |
| like.png                                    | https://www.flaticon.com/free-icon/like_179655#term=likes&page=1&position=24                         |               |
| chat.png                                    | https://www.flaticon.com/free-icon/chat_134808#term=comments&page=1&position=4                       |               |
| link.png                                    | https://www.flaticon.com/free-icon/web-link_3665#term=link&page=1&position=23                        |               |
| coolEmoji.png                               | https://www.flaticon.com/free-icon/cool_742814                                                       |               |
| sleepyEmoji.png                             | https://www.flaticon.com/free-icon/sleepy_742743                                                     |               |
| angryEmoji.png                              | https://www.flaticon.com/free-icon/angry_742744                                                      |               |
| x-button.png                                | https://www.flaticon.com/free-icon/x-button_458594#term=delete&page=1&position=15                    |               |
| encrpyted-file.png                          | https://www.flaticon.com/free-icon/encrpyted-file_1375115#term=encrpyted&page=1&position=3           |               |
| database.png                                | https://www.flaticon.com/free-icon/database_148825#term=database&page=1&position=7                   |               |
| analytics.png                               | https://www.flaticon.com/premium-icon/analytics_998331#term=analysis&page=1&position=1               |               |
| vault.png                                   | https://www.flaticon.com/free-icon/vault_1803103                                                     |               |
| shield.png                                  | https://www.flaticon.com/free-icon/shield_214343#term=security&page=2&position=14                    |               |
| security.png                                | https://www.flaticon.com/free-icon/security_1383398#term=security&page=2&position=4                  |               |
