/**
 * @file SmecknSortByDropDown Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import SmecknSortByDropDownTest from './SmecknSortByDropDownTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Sort By Drop Down', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknSortByDropDownTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknSortByDropDown]').should('be.visible');
      cy.get('[data-cy=sortOptionParagraph]').should('not.be.visible');
      cy.get('[data-cy=smecknSortByDropDownOption]').should('not.be.visible');
    }
  });

  it('Drop Down Open Close', function () {
    cy.get('[data-cy=smecknSortByDropDownOption]').should('not.be.visible');
    cy.get('[data-cy=smecknSortByDropDown]').click();
    cy.get('[data-cy=smecknSortByDropDownOption]').should('be.visible');
    cy.get('[data-cy=smecknSortByDropDown]').click();
    cy.get('[data-cy=smecknSortByDropDownOption]').should('not.be.visible');
  });

  it('Click all options', function () {
    // Iterate over all options
    cy.get('[data-cy=availableSortOption]').each(($availableSortOption) => {
      // Get option text
      cy.wrap($availableSortOption).within(() => {
        cy.get('[data-cy=availableWASortType]').invoke('text')
          .then((str) => str.trim()).as('curWASortType');
        cy.get('[data-cy=availableWASortOrder]').invoke('text')
          .then((str) => str.trim()).as('curWASortOrder');
      });
      // Find option in dropdown and click it
      cy.then(() => {
        SmecknSortByDropDownTest.selectSort({ waSortType: this.curWASortType, waSortOrder: this.curWASortOrder });
      });
      // Verify Selected
      cy.get('[data-cy=selectedWASortType]').invoke('text').then((text) => {
        expect(text.trim()).to.equal(this.curWASortType);
      });
      cy.get('[data-cy=selectedWASortOrder]').invoke('text').then((text) => {
        expect(text.trim()).to.equal(this.curWASortOrder);
      });
    });
  });
});
