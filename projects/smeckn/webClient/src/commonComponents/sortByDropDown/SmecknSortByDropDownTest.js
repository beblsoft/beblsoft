/**
 * @file SmecknSortByDropDown Tests
 */


/* ------------------------ SMECKN SORT BY DROP DOWN TEST ------------------ */
/**
 * Common Checkout Test Methods
 * @type {Object}
 */
const SmecknSortByDropDownTest = {

  /**
   * Select Sort
   * @param {String} waSortType
   * @param {String} waSortOrder
   */
  selectSort({ waSortType, waSortOrder }) {
    cy.then(() => {
      cy.get('[data-cy=smecknSortByDropDown]').click({ force: true });
      cy.then(() => {
        cy.get(`[data-cy=${waSortType}-${waSortOrder}]`).click({ force: true });
      });
    });
  },
};

/* ------------------------ EXPORTS ---------------------------------------- */
export default SmecknSortByDropDownTest;
