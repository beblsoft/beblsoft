/**
 * @file SmecknSidebarContainer Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Sidebar Container', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknSidebarContainerTestHome' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      if (viewport.width > 800) {
        cy.get('[data-cy=sidebarContainer]').should('be.visible');
        cy.get('[data-cy=sidebarContainerCollapsed]').should('not.be.visible');
      } else {
        cy.get('[data-cy=sidebarContainer]').should('not.be.visible');
        cy.get('[data-cy=sidebarContainerCollapsed]').should('be.visible');
      }
      cy.get('[data-cy=homeIcon]').should('be.visible');
      cy.get('[data-cy=disabledIcon]').should('be.visible');
      cy.get('[data-cy=level1Icon]').should('be.visible');
    }
  });

  it('Expand Collapse', function () {
    cy.get('[data-cy=sidebarContainer]').should('be.visible');
    cy.get('[data-cy=sidebarContainerCollapsed]').should('not.be.visible');
    cy.get('[data-cy=collapseSidebarButton]').click();
    cy.get('[data-cy=sidebarContainer]').should('not.be.visible');
    cy.get('[data-cy=sidebarContainerCollapsed]').should('be.visible');
    cy.get('[data-cy=expandSidebarButton]').click();
    cy.get('[data-cy=sidebarContainer]').should('be.visible');
    cy.get('[data-cy=sidebarContainerCollapsed]').should('not.be.visible');
  });

  it('Click Home', function () {
    cy.get('[data-cy=homeIcon]').click();
    cy.url().should('include', 'home');
  });

  it('Disabled', function () {
    cy.get('[data-cy=disabledIcon]').click();
    cy.url().should('include', 'home');
  });

  it('Badges', function () {
    cy.get('[data-cy=sidebarItemBadges]').click();
    cy.url().should('include', 'badges');
    cy.get('[data-cy=NewBadge]').should('be.visible');
    cy.get('[data-cy=1ItemBadge]').should('be.visible');
  });

  it('Levels', function () {
    cy.get('[data-cy=sidebarItemLevel1]').click();
    cy.get('[data-cy=sidebarItemLevel2]').click();
    cy.get('[data-cy=sidebarItemLevel3]').click();
    cy.url().should('include', 'level3');
  });

});
