/**
 * @file Common Mixin for SmecknSidebar*Item Components
 */


/* ---------------------- IMPORTS ------------------------------------------ */
import downArrowImgSrc from '../../img/flaticon/down-arrow.png';
import rightArrowImgSrc from '../../img/flaticon/right-arrow.png';


/* ---------------------- MIXIN OBJECT ------------------------------------- */
const SidebarItemMixin = {

  /* -------------------- EMITS -------------------------------------------- */
  /*
    Event
      Name
        'click'
      Description
        Emitted whenever the item is clicked
      Usage
        v-on:click="onClick"
        onClick() {}
   */

  /* -------------------- PROPS -------------------------------------------- */
  props: {
    item: {
      type: Object,
      required: true
    },
    minHeight: {
      // Height (px)
      type: Number,
      default: 40,
    },
    width: {
      // Width (px)
      type: Number,
      default: 275
    },
    widthExpanded: {
      // Expanded Width (px), used by collapsed item
      type: Number,
      default: 350,
    },
    showChildren: {
      // If True, show children
      // If False, only show children if one is active
      type: Boolean,
      default: false
    },
    showIcon: {
      // If False, do NOT show the icon
      type: Boolean,
      default: true
    },
    depthLevel: {
      // Depth level of nested item
      type: Number,
      default: 0
    },
    depthLevelPadMultiplier: {
      // Determines how far indended each nested sublevel is
      type: Number,
      default: 0.9
    },
    leftBorderWidthDescendantActive: {
      // Determines the left margin width (px) for items with active descendants
      type: Number,
      default: 5
    },
    collapsed: {
      // If True, Item is being shown in the sidebar collapsed state
      type: Boolean,
      default: false
    }
  },

  /* -------------------- DATA --------------------------------------------- */
  data() {
    return {
      // Tracks nested SidebarItems with open chilren, giving accordion affect
      openSubItemIndex: -1,

      // Should the CollapsedItem expand?
      expand: false,

      // Images
      downArrowImgSrc,
      rightArrowImgSrc,
    };
  },

  /* -------------------- COMPUTED ----------------------------------------- */
  computed: {

    /**
     * @return {Object} Object defining item style
     */
    itemStyle() {
      let styleObj = {
        'width': `${this.width}px`,
        'min-height': `${this.minHeight}px`,
        'padding-left': `${this.leftBorderWidthDescendantActive}px`
      };
      if (this.isDescendantRouteActive) {
        styleObj['padding-left'] = '0px';
        styleObj['border-left'] = `${this.leftBorderWidthDescendantActive}px solid ${this.$g.ui.colors.info}`;
      }
      return styleObj;
    },

    /**
     * @return {Object} Style object definining where collapsed expanse goes
     */
    collapsedItemExpandStyle() {
      let styleObj = {
        position: 'absolute',
        top: 0,
        left: `${this.width}px`,
      };
      if (this.isDescendantRouteActive) {
        styleObj.left = `${this.width - this.leftBorderWidthDescendantActive}px`;
      }
      return styleObj;
    },

    /**
     * @return {Boolean} True if Item has children
     */
    itemHasChildren() {
      return this.item.children !== undefined;
    },

    /**
     * @return {Boolean} True if item should drop down and show its children
     */
    dropDownChildren() {
      return (this.showChildren || this.isDescendantRouteActive) && this.itemHasChildren;
    },

    /**
     * @return {Object} Pad nested ChildItem titles
     */
    depthLevelPaddingStyle() {
      return { 'padding-left': `${this.depthLevel * this.depthLevelPadMultiplier}rem` };
    },

    /**
     * @return {Boolean} Return true if this item has an active route
     */
    isRouteActive() {
      return this.item.activeRouteName === this.$route.name;
    },

    /**
     * @return {Boolean} Return true if the item of any of its nested children
     * has an active route
     */
    isDescendantRouteActive() {
      let isItemRouteActive = (item) => { return item.activeRouteName === this.$route.name; };
      let isDescendantTreeRouteActive = (item) => {
        let rval = false;
        if (isItemRouteActive(item)) {
          rval = true;
        } else if (item.children) {
          for (let i = 0; i < item.children.length; i++) {
            let childItem = item.children[i];
            rval = isDescendantTreeRouteActive(childItem);
            if (rval) { break; }
          }
        }
        return rval;
      };
      let rval = isDescendantTreeRouteActive(this.item);
      return rval;
    }
  },

  /* -------------------- METHODS ------------------------------------------ */
  methods: {

    /**
     * Handle item being clicked
     *   Propagate event up to parent
     *   If enabled, execute item onClick handler
     */
    onItemClicked() {
      if (!this.item.disabled) {
        this.$emit('click');
        if (this.item.onClick) {
          this.item.onClick();
        }
      }
    },

    /**
     * Collapsed item handlers
     */
    onCollapsedItemClicked() {
      this.expand = true;
      this.onItemClicked();
    },
    onCollapsedItemMouseOver() {
      this.expand = true;
    },
    onCollapsedItemMouseLeave() {
      this.expand = false;
    },

    /**
     * Change the open sub item, invoke click callback
     * @param {Number} index item index that was clicked
     */
    async onSubItemClicked(index) {
      if (this.openSubItemIndex === index) {
        this.openSubItemIndex = -1;
      } else {
        this.openSubItemIndex = index;
      }
    }
  },
};


/* ---------------------- EXPORTS ------------------------------------------ */
export default SidebarItemMixin;
