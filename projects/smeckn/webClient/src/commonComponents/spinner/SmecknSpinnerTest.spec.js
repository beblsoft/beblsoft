/**
 * @file SmecknSpinner Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Spinner', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknSpinnerTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknSpinner]').should('be.visible');
      cy.get('[data-cy=smecknSpinnerText]').should('be.visible');
    }
  });

  it('ToggleLoading', function () {
    cy.get('[data-cy=smecknSpinnerText]').should('be.visible');
    cy.get('[data-cy=toggleLoadingButton]').click();
    cy.get('[data-cy=smecknSpinnerText]').should('not.be.visible');
    cy.get('[data-cy=toggleLoadingButton]').click();
    cy.get('[data-cy=smecknSpinnerText]').should('be.visible');
  });
});
