/*
  NAME
    SmecknDateRangeDataMixin.js

  DESCRIPTION
    Mixin class for components integrating with SmecknDateRangePicker

  USAGE
    See SmecknDateRangePickerTest.vue
*/

/* ------------------------ IMPORTS ---------------------------------------- */
import moment from 'moment';


/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /* ---------------------- PROPS ------------------------------------------ */
  props: {
    // Initial value for date range
    startDateRange: {
      type: Object,
      default () {
        return {
          startMoment: moment().subtract(20, 'years'),
          endMoment: moment()
        };
      },
    },
  },

  /* ---------------------- DATA ------------------------------------------- */
  data() {
    return {
      // Date range
      dateRange: {
        startMoment: null,
        endMoment: null,
      },
    };
  },

  /* ---------------------- WATCH ------------------------------------------ */
  watch: {
    startDateRange(val) {
      this.dateRange = this.startDateRange;
    },
  },

  /* ---------------------- LIFECYCLE -------------------------------------- */
  mounted() {
    this.dateRange = this.startDateRange;
  },
};
