/**
 * @file SmecknDateRangePicker Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import SmecknDateRangePickerTest from './SmecknDateRangePickerTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Date Range Picker', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknDateRangePickerTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknDateRangePicker]').should('be.visible');
      cy.get('[data-cy=smecknDateRangePickerCloseButton]').should('be.visible');
      cy.get('[data-cy=smecknDateRangePickerFromDate]').should('be.visible');
      cy.get('[data-cy=smecknDateRangePickerToDate]').should('be.visible');
      cy.get('[data-cy=startMoment]').should('be.visible');
      cy.get('[data-cy=endMoment]').should('be.visible');
    }
  });

  it('Should open when clicked', function () {
    cy.get('[data-cy=selectDateRangeButton]').should('not.be.visible');
    cy.get('[data-cy=smecknDateRangePicker]').click();
    cy.get('[data-cy=selectDateRangeButton]').should('be.visible');
  });

  it('Should close when x clicked', function () {
    cy.get('[data-cy=startMoment]').should('be.visible');
    cy.get('[data-cy=endMoment]').should('be.visible');
    cy.get('[data-cy=smecknDateRangePickerCloseButton]').should('be.visible');
    cy.get('[data-cy=smecknDateRangePickerCloseButton]').click();
    cy.get('[data-cy=smecknDateRangePickerCloseButton]').should('not.be.visible');
    cy.get('[data-cy=startMoment]').should('not.be.visible');
    cy.get('[data-cy=endMoment]').should('not.be.visible');
  });

  it('Should test preset date ranges buttons', function () {
    // Iterate through all preset date ranges
    cy.get('[data-cy=presetDateRange]').each(($p) => {
      // Get name
      cy.wrap($p).within(() => {
        cy.get('[data-cy=presetDateRangeName]').invoke('text').as('dateRangeName');
      });
      // Get start moment
      cy.wrap($p).within(() => {
        cy.get('[data-cy=presetDateRangeStartMoment]').invoke('text').then((startMomentStr) => {
          cy.wrap(Cypress.moment(startMomentStr)).as('startMoment');
        });
      });
      // Get end moment
      cy.wrap($p).within(() => {
        cy.get('[data-cy=presetDateRangeEndMoment]').invoke('text').then((endMomentStr) => {
          cy.wrap(Cypress.moment(endMomentStr)).as('endMoment');
        });
      });
      // Reset Widget
      cy.get('[data-cy=smecknDateRangePickerCloseButton]').click();
      // Open widget and click date range button
      cy.get('[data-cy=smecknDateRangePicker]').click();
      cy.then(() => {
        cy.get('[data-cy=selectDateRangeButton]').contains(this.dateRangeName).click();
      });
      // Verify selected date range matches
      cy.get('[data-cy=startMoment]').invoke('text').then((startMomentStr) => {
        expect(Cypress.moment(startMomentStr).diff(this.startMoment, 'day')).to.equal(0);
      });
      cy.get('[data-cy=endMoment]').invoke('text').then((endMomentStr) => {
        expect(Cypress.moment(endMomentStr).diff(this.endMoment, 'day')).to.equal(0);
      });
    });
  });

  it('Should select arbitrary dates', function () {
    let momentRangeList = [{
      startMoment: Cypress.moment([1990, 6, 10]),
      endMoment: Cypress.moment([2000, 7, 11])
    }, {
      startMoment: Cypress.moment([2000, 11, 7]),
      endMoment: Cypress.moment([2005, 8, 22])
    }, {
      startMoment: Cypress.moment([2020, 11, 7]),
      endMoment: Cypress.moment([2021, 8, 22])
    }, {
      startMoment: Cypress.moment([2000, 1, 1]),
      endMoment: Cypress.moment([2041, 11, 31])
    }];

    // Iterate through all dates verifing
    Cypress._.forEach(momentRangeList, (momentRange) => {
      cy.then(() => {
        SmecknDateRangePickerTest.selectDateRange({ ...momentRange });
        // Verify Start Moment
        cy.get('[data-cy=startMoment]').invoke('text').then((startMomentStr) => {
          return Cypress.moment(startMomentStr);
        }).as('startMoment');
        cy.then(() => { expect(this.startMoment.diff(momentRange.startMoment, 'day')).to.equal(0); });
        // Verify End Moment
        cy.get('[data-cy=endMoment]').invoke('text').then((endMomentStr) => {
          return Cypress.moment(endMomentStr);
        }).as('endMoment');
        cy.then(() => { expect(this.endMoment.diff(momentRange.endMoment, 'day')).to.equal(0); });
      });
    });
  });

});
