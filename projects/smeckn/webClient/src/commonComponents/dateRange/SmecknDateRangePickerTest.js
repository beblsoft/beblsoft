/**
 * @file SmecknDateRangePicker Common Tests
 */


/* ------------------------ SMECKN DATE RANGE PICKER TEST ------------------ */
/**
 * Common Checkout Test Methods
 * @type {Object}
 */
const SmecknDateRangePickerTest = {

  /**
   * Select Date Range
   * @param {Moment} startMoment starting moment to select
   * @param {Moment} endMoment end moment to select
   * @param {Number} waitMS milliseconds to wait
   */
  selectDateRange({ startMoment, endMoment, waitMS = 250, finalWait = true }) {
    cy.then(() => {
      // Open widget and click date range button
      cy.get('[data-cy=smecknDateRangePicker]').click();

      // Set start and end dates
      this.selectMoment({ start: true, moment: startMoment, waitMS });
      cy.wait(waitMS);
      this.selectMoment({ start: false, moment: endMoment, waitMS });
      if (finalWait) { cy.wait(waitMS); }
    });
  },

  /**
   * Select a moment
   * @param {Boolean} start If true, select from starting side of picker
   * @param {Moment} moment moment to select
   */
  selectMoment({ start = true, moment, waitMS }) {
    let year = moment.year();
    let month = moment.month();
    let day = moment.date();

    this.selectYear({ start, year });
    cy.wait(waitMS);
    this.selectMonth({ start, month });
    cy.wait(waitMS);
    this.selectDay({ start, day });
  },

  /**
   * Select a year
   */
  selectYear({ start = true, year }) {
    let direction = start ? 'left' : 'right';
    cy.get(`.${direction} .calendar-table .yearselect`).invoke('val', year).trigger('input');
  },

  /**
   * Select a month
   * @param {Number} month 0 indexed month
   */
  selectMonth({ start = true, month }) {
    let direction = start ? 'left' : 'right';
    let monthStr = Cypress.moment(month + 1, 'MM').format('MMM');
    cy.get(`.${direction} .calendar-table .monthselect`).select(monthStr);
  },

  /**
   * Select a day
   */
  selectDay({ start = true, day }) {
    let direction = start ? 'left' : 'right';
    let clicked = false;

    // Iterate through all elements matching one with correct value
    // Only click once
    cy.get(`.${direction} .calendar-table .table-condensed tbody td`).each(function ($elem) {
      let val = $elem.html();
      val = parseFloat(val);
      if (val === day && !clicked) {
        cy.wrap($elem).click();
        clicked = true;
      }
    });
  },
};

/* ------------------------ EXPORTS ---------------------------------------- */
export default SmecknDateRangePickerTest;
