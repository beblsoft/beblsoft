/**
 * @file SmecknFadeTransition Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Fade Transition', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknFadeTransitionTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=fadeParagraph]').should('be.visible');
    }
  });

  it('Toggle Fade', function () {
    cy.get('[data-cy=fadeParagraph]').should('be.visible');
    cy.get('[data-cy=toggleFade]').click();
    cy.get('[data-cy=fadeParagraph]').should('not.be.visible');
    cy.get('[data-cy=toggleFade]').click();
    cy.get('[data-cy=fadeParagraph]').should('be.visible');
  });
});
