/**
 * @file SmecknDropDownTransition Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Drop Down Transition', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknDropDownTransitionTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=dropDownParagraphs]').should('be.visible');
    }
  });

  it('Toggle Drop Down', function () {
    cy.get('[data-cy=dropDownParagraphs]').should('be.visible');
    cy.get('[data-cy=toggleDropDown]').click();
    cy.get('[data-cy=dropDownParagraphs]').should('not.be.visible');
    cy.get('[data-cy=toggleDropDown]').click();
    cy.get('[data-cy=dropDownParagraphs]').should('be.visible');
  });
});
