/**
 * @file SmecknContentSection Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Content Section', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknContentSectionTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknContentSection]').should('be.visible');
      cy.get('[data-cy=smecknContentSectionHeader]').should('be.visible');
    }
  });

  it('ToggleTitle', function () {
    cy.get('[data-cy=smecknContentSectionHeader]').should('be.visible');
    cy.get('[data-cy=toggleTitleButton]').click();
    cy.get('[data-cy=smecknContentSectionHeader]').should('not.be.visible');
    cy.get('[data-cy=toggleTitleButton]').click();
    cy.get('[data-cy=smecknContentSectionHeader]').should('be.visible');
  });
});
