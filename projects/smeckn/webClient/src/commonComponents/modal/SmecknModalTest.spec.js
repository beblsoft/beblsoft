/**
 * @file SmecknModal Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Modal', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknModalTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=showModalButton]').click({ force: true });
      cy.get('[data-cy=modalText]').should('be.visible');
      cy.get('[data-cy=modalFooterText]').should('be.visible');
    }
  });

  it('Toggle Modal', function () {
    cy.get('[data-cy=modalText]').should('not.be.visible');
    cy.get('[data-cy=showModalButton]').click();
    cy.get('[data-cy=modalText]').should('be.visible');
    cy.get('[data-cy=modalFooterText]').should('be.visible');
    cy.get('[data-cy=hideModalButton]').click({ force: true });
    cy.get('[data-cy=modalText]').should('not.be.visible');
  });
});
