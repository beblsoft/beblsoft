/**
 * @file SmecknScrollMixin Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Scroll Mixin', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknScrollMixinTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=randomIndex]').should('be.visible');
    }
  });

  it('Scroll To Bottom', function () {
    cy.get('[data-cy=nearBottomParagraph]').should('not.be.visible');
    cy.get('[data-cy=atBottomParagraph]').scrollIntoView();
    cy.get('[data-cy=nearBottomParagraph]').should('be.visible');
  });

  it('Go to top', function () {
    cy.get('[data-cy=goToTopButton]').click();
    cy.window().then((win) => {
      expect(win.scrollY).to.be.closeTo(0, 200);
    });
  });
});
