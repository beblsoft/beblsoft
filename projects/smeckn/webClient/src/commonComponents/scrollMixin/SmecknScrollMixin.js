/*
  NAME
    SmecknScrollMixin.js

  DESCRIPTION
    Scroll Mixin Component
    Watches Global Window Scroll Bar and Fires Callback Functions appropriately

  USAGE IN COMPONENT
    See SmecknScrollMixinTest.js
*/

/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /* ---------------------- PROPS ------------------------------------------ */
  props: {
    // Offset Height Near Bottom Multiplier
    // When scroll bar reaches within (this value) x (offsetHeight) close to the bottom
    // the onScrollNearEnd function will be invoked
    offsetHeightNearBottomMultiplier: {
      type: Number,
      default: 1.5
    },
  },

  /* ---------------------- LIFECYCLE -------------------------------------- */
  mounted() {
    window.addEventListener('scroll', this.onScroll);
  },
  destroyed() {
    window.removeEventListener('scroll', this.onScroll);
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {

    /**
     * Invoked when scrollbar is near the end of the screen
     * Optionally overridden by subclassing component
     * Note: will be called multiple times when near the bottom
     */
    onScrollNearEnd() {
      /* TO BE OVERRIDDEN */
    },

    /**
     * As the user scrolls towards the bottom of the page this method is invoked
     * @param  {Object} event Scroll event object
     */
    async onScroll(event) {
      /*
         Picture and variable name description
             -------------------
          ^  | Scrollable      | ^
          |  | Content         | |
          |  |                 | |
          sh |                 | sy
          |  |                 | |
          |  |                 | |
          |  |                 | v
          |  -------------------
       ^  |  | Scroll Visible  |
       |  |  | Windown         |
       |  |  | (What the user  |
       |  |  |  actually sees) |
       oh |  |                 |
       |  |  |                 |
       v  |  |                 |
          |  -------------------
          |  |                 |
          v  |                 |
             -------------------
       */

      let oh = window.innerHeight; // (px) height of container (constant value)
      let sh = document.body.scrollHeight; // (px) height of scrollable content (constant value)
      let sy = window.scrollY; // (px) Height of content at top of scroll pane (0 -> sh-oh)
      let nearBottom = sy > sh - this.offsetHeightNearBottomMultiplier * oh;
      if (nearBottom) {
        this.onScrollNearEnd();
      }
    },

    /**
     * Scroll back to top
     */
    scrollToTop() {
      window.scroll(0, 0);
    }
  }
};
