/**
 * @file SmecknSplashContainer Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Scroll Container', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknSplashContainerTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknSplashContainer]').should('be.visible');
      cy.get('[data-cy=splashContainerParagraph]').should('be.visible');
    }
  });
});
