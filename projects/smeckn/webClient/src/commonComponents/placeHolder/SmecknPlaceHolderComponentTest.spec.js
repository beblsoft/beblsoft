/**
 * @file SmecknPlaceHolderComponent Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Place Holder Component', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknPlaceHolderTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=placeHolderComponent]').should('be.visible');
    }
  });

  it('Route Name', function () {
    cy.get('[data-cy=placeHolderRouteNameSpan]').invoke('text').then((text) => {
      expect(text.trim()).to.equal('rnSmecknPlaceHolderTest');
    });
  });
});
