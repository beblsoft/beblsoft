/**
 * @file SmecknContentSection Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Content Section', function () {

  /**
   * Stub Text Analysis
   */
  function stubTextAnalysis() {
    cy.then(() => {
      cy.server();
      cy.route({
        method: 'POST',
        url: '*/text/*/analysis*',
        response: {
          smID: 398,
          createDate: '2019-08-06T15:45:25.180Z',
          text: 'The other day the the dog jumped over the moon!',
          origLength: 200,
          compLangAnalysisModel: {
            smID: 398,
            status: 'SUCCESS',
            compLangModelList: [{
              smID: 398,
              bLanguageType: 'English',
              score: 0.2133
            }],
            dominantBLanguageType: 'English'
          },
          compSentAnalysisModel: {
            smID: 398,
            status: 'SUCCESS',
            dominantSentiment: 'POSITIVE',
            positiveScore: 0.23,
            negativeScore: 0.23,
            neutralScore: 0.23,
            mixedScore: 0.23
          }
        }
      }).as('textAnalysis');
    });
  }

  /**
   * Stub Unit Balance
   */
  function stubUnitBalance({
    nAvailable = 0,
    nHeld = 0,
    nTotal = 0
  }) {
    cy.then(() => {
      cy.server();
      cy.route({
        method: 'GET',
        url: '*/unit/balance*',
        response: [{ unitType: 'TEXT_ANALYSIS', nAvailable, nHeld, nTotal }]
      }).as('getUnitBalance');
    });
  }

  /**
   * SetUp Tests
   */
  function setUp() {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknTextAnalysisTest' }); });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=textNotAnalyzedButton]').click({ force: true });
      cy.get('[data-cy=smecknTextAnalysisTest]').should('be.visible');
      cy.get('[data-cy=smecknTextAnalysisTestRadioButtons]').should('be.visible');
    }
  });

  it('Try Analysis: Success', function () {
    stubTextAnalysis();
    setUp();
    cy.get('[data-cy=textNotAnalyzedButton]').click({ force: true });
    cy.get('[data-cy=analyzeSentimentButton]').click({ force: true });
    cy.get('[data-cy=languageNotSupportedForSentimentPara]').should('not.be.visible');
    cy.get('[data-cy=errorMessagePara]').should('not.be.visible');
    cy.wait('@textAnalysis');
    // Verify Each Sentiment Present
    Cypress._.forEach(['positiveSentimentScore', 'negativeSentimentScore',
      'neutralSentimentScore', 'mixedSentimentScore'
    ], (sentimentDataCY) => {
      cy.get(`[data-cy=${sentimentDataCY}]`).then(($progress) => {
        let value = parseFloat($progress.text());
        expect(value).to.be.greaterThan(0);
      });
    });
    // Verify language
    cy.get('[data-cy=dominantBLanguageSpan]').should('be.visible');
    cy.get('[data-cy=dominantBLanguageScoreSpan]').should('be.visible');
  });

  it('Try Analysis: Not Enough Units', function () {
    stubUnitBalance({ nAvailable: 0, nTotal: 0, nHeld: 0 });
    setUp();
    cy.get('[data-cy=textNotAnalyzedButton]').click({ force: true });
    cy.get('[data-cy=analyzeSentimentButton]').click({ force: true });
    cy.wait('@getUnitBalance');
    cy.get('[data-cy=errorMessagePara]').should('be.visible');
  });

  it('Sentiment Language Not Supported', function () {
    setUp();
    cy.get('[data-cy=textUnsupportedLanguageButton]').click({ force: true });
    cy.get('[data-cy=languageNotSupportedForSentimentPara]').should('be.visible');
  });
});
