/**
 * @file Common Components Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import _ from 'lodash';


/* ------------------------ EXPORTS ---------------------------------------- */
/**
 * Dynamically load and register all Smeckn Common Components
 * Reference:
 *   https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components
 *   https://www.npmjs.com/package/require-context
 */
export default function registerCommonComponents() {
  const requireCommonComponent = require.context('.', true, /[A-Z]\w+\.(vue)$/);

  requireCommonComponent.keys().forEach((fileName) => {
    // Get component config
    // Get PascalCase name of component
    // Register component globally
    const componentConfig = requireCommonComponent(fileName);
    let componentName = fileName.split('/').pop().replace(/\.\w+$/, '');
    componentName = _.camelCase(componentName);
    componentName = _.upperFirst(componentName);
    Vue.component(componentName, componentConfig.default || componentConfig);
  });
}
