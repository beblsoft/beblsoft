/**
 * @file Smeckn Number Incrementer Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Number Incrementer', function () {
  let tooSmall = -1;
  let tooBig = 123456789012345678901234567890;
  let nClicks = 10;
  let startNumber = 5000;

  beforeEach(function () {
    tg.init();
    cy.then(async () => { await tg.$g.router.push({ name: 'rnSmecknNumberIncrementerTest' }); });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknNumberIncrementer]').should('be.visible');
      cy.get('[data-cy=smecknNumberIncrement]').should('be.visible');
      cy.get('[data-cy=smecknNumber]').should('be.visible');
      cy.get('[data-cy=smecknNumberDecrement]').should('be.visible');
    }
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  /**
   * Click incrementer/decrementer several times and verify that
   *   1. Parent gets new value
   *   2. Number moves appropriately
   * @param  {string} options.test             Test context
   * @param  {string} options.buttonDataCY     data-cy attribute to click
   * @param  {string} options.numberComparison Comparison operator for number before and after
   */
  function clickAndCheck({
    test,
    buttonDataCY = 'smecknNumberIncrement',
    numberComparison = 'be.greaterThan'
  } = {}) {
    cy.get('[data-cy=smecknNumber]').invoke('val', startNumber).trigger('input');
    for (let click = 0; click < nClicks; click++) {
      cy.get('[data-cy=smecknNumber]').then(($input) => {
        let val1 = parseFloat($input.val());
        cy.wrap(val1).as('val1');
      });
      cy.get(`[data-cy=${buttonDataCY}]`).click();
      cy.get('[data-cy=parentValue]').invoke('text').then((text) => {
        return parseFloat(text);
      }).as('parentValue');
      cy.get('[data-cy=smecknNumber]').then(($input) => {
        let val2 = parseFloat($input.val());
        cy.wrap(val2).should(`${numberComparison}`, test.val1);
        cy.wrap(val2).should('equal', test.parentValue);
      });
    }
  }

  it('Plus', function () {
    clickAndCheck({
      test: this,
      buttonDataCY: 'smecknNumberIncrement',
      numberComparison: 'be.greaterThan'
    });
  });

  it('Minus', function () {
    clickAndCheck({
      test: this,
      buttonDataCY: 'smecknNumberDecrement',
      numberComparison: 'be.lessThan'
    });
  });

  /**
   * Enter number and verify output converts it approprately
   * @param  {[type]} options.initialVal       Initial value to enter
   * @param  {Object} options.numberComparison Comparison operate for number before and after
   */
  function typeAndCheck({
    initialVal,
    numberComparison
  } = {}) {
    cy.get('[data-cy=smecknNumber]').type(`${initialVal}`);
    cy.get('[data-cy=smecknNumber]').then(($input) => {
      let val = parseFloat($input.val());
      cy.wrap(val).should(`${numberComparison}`, initialVal);
    });
  }

  it('Enter too Small', function () {
    typeAndCheck({ initialVal: tooSmall, numberComparison: 'be.greaterThan' });
  });

  it('Enter too high', function () {
    typeAndCheck({ initialVal: tooBig, numberComparison: 'be.lessThan' });
  });
});
