/*
  NAME
    SmecknChartMixin.js

  DESCRIPTION
    Mixin Chart Component
    Ensures that chart is properly setup and torn down
    Ensures chart reloads whenever its parameters change

  USAGE IN COMPONENT
    See SmecknChartMixinTest.vue
*/

/* ------------------------ IMPORTS ---------------------------------------- */
import Chart from 'chart.js';


/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /**
   * Render function
   * Reference: https://vuejs.org/v2/guide/render-function.html
   * Used because mixins can't have templates
   */
  render(createElement) {
    // <div>
    //   <canvas></canvas>
    // <div>
    let canvasElement = createElement('canvas', {
      ref: 'chart',
      attrs: { 'data-cy': 'smecknChartCanvas' }
    });
    let divElement = createElement('div', {
      class: this.chartDivClass,
      attrs: this.chartDivAttrObj
    }, [canvasElement]);
    return divElement;
  },

  /* ---------------------- PROPS ------------------------------------------ */
  props: {
    // Chart Div Class Object
    // Useful to set canvas height
    chartDivClass: {
      type: Object,
      default () { return { 'smg-mh-9': true }; }
    },
    // Chart Div Attribute Object
    chartDivAttrObj: {
      type: Object,
      default () { return { 'data-cy': 'smecknChartMixin' }; }
    }
  },

  /* ---------------------- DATA ------------------------------------------- */
  data() {
    return {
      // Chart Object
      chart: null
    };
  },

  /* ---------------------- COMPUTED --------------------------------------- */
  computed: {

    /**
     * Must be implemented by Parent Component!
     * @return {Object} Chartjs:Chart Constructor Parameters
     * {
     *   type:
     *     type: String
     *     description: Chart Type
     *
     *   data:
     *     type: Object
     *     description: chart data
     *
     *   options:
     *     type: Object
     *     description: chart options
     * }
     */
    chartParams() {
      throw new Error('Parent Component Must implement computed:chartParams');
    }
  },

  /* ---------------------- WATCH ------------------------------------------ */
  watch: {

    /**
     * Watch all nested changes to chartParams and force new chart to be created
     */
    chartParams: {
      deep: true,
      handler() {
        this.createChart();
      }
    }
  },

  /* ---------------------- LIFECYCLE -------------------------------------- */
  mounted() {
    this.$nextTick(() => { this.createChart(); });
  },
  destroyed() {
    this.destroyChart();
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {

    /**
     * (Re)Create the chart
     */
    createChart() {
      let ctx = this.$refs.chart.getContext('2d');
      this.destroyChart();
      this.chart = new Chart(ctx, this.chartParams);
    },

    /**
     * Destroy the chart
     */
    destroyChart() {
      if (this.chart) {
        this.chart.destroy();
        this.chart = null;
      }
    }
  }
};
