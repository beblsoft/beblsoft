/**
 * @file SmecknDropRightTransition Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Drop Right Transition', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknDropRightTransitionTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=dropRightParagraph]').should('be.visible');
    }
  });

  it('Toggle Drop Right', function () {
    cy.get('[data-cy=dropRightParagraph]').should('be.visible');
    cy.get('[data-cy=toggleDropRight]').click();
    cy.get('[data-cy=dropRightParagraph]').should('not.be.visible');
    cy.get('[data-cy=toggleDropRight]').click();
    cy.get('[data-cy=dropRightParagraph]').should('be.visible');
  });
});
