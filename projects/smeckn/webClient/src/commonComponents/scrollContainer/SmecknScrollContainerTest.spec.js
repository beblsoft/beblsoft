/**
 * @file SmecknScrollContainer Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Smeckn Scroll Container', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { tg.$g.router.push({ name: 'rnSmecknScrollContainerTest' }); });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknScrollContainer]').should('be.visible');
    }
  });

  it('Near Bottom', function () {
    cy.get('[data-cy=nearBottomPara]').should('not.be.visible');
    cy.get('[data-cy=smecknScrollContainer]').scrollTo('bottom', { duration: 2000 });
    cy.get('[data-cy=nearBottomPara]').should('be.visible');
  });

  it('Scroll To Top', function () {
    cy.get('[data-cy=smecknScrollContainer]').scrollTo('bottom', { duration: 2000 });
    cy.get('[data-cy=nearBottomPara]').should('be.visible');
    cy.get('[data-cy=smecknScrollContainer]').then(($div) => {
      cy.log($div.scrollTop());
      expect($div.scrollTop()).to.not.equal(0);
    });
    cy.get('[data-cy=scrollToTopButton]').click();
    cy.get('[data-cy=smecknScrollContainer]').then(($div) => {
      expect($div.scrollTop()).to.equal(0);
    });
  });
});
