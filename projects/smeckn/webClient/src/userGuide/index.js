/**
 * @file User Guide Functionality
 */


/* ------------------------ USER GUIDE CLASS ------------------------------- */
/**
 * Encapsulate user guide state
 */
export default class UserGuide {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
  }

  /**
   * @return {String} User Guide Domain
   *   Local: http://localhost:8022
   *   AWS  : https://www.smeckn.com
   */
  getDomain() {
    let domain = '';
    if (process.env.VUE_APP_ENVIRONMENT === 'Local') {
      domain += process.env.VUE_APP_USERGUIDE_DOMAIN;
    } else {
      domain += window.location.origin;
    }
    return domain;
  }

  /**
   * @return {String} User Guide Base URL
   *   AWS : https://www.smeckn.com/userGuide
   */
  getBaseURL() {
    // VUE_APP_USERGUIDE_BASE_URL includes starting '/'
    return `${this.getDomain()}${process.env.VUE_APP_USERGUIDE_BASE_URL}`;
  }

  /**
   * @return {String} Home URL
   */
  getHomeURL() {
    return `${this.getBaseURL()}/index.html`;
  }

  /**
   * @return {String} Terms and Conditions URL
   */
  getTermsAndConditionsURL() {
    return `${this.getBaseURL()}/company/Legal.html#terms-and-conditions`;
  }

  /**
   * @return {String} Privacy Policy URL
   */
  getPrivacyPolicyURL() {
    return `${this.getBaseURL()}/company/Legal.html#privacy-policy`;
  }

  /**
   * @return {String} Refund Policy URL
   */
  getRefundPolicyURL() {
    return `${this.getBaseURL()}/company/Legal.html#refund-policy`;
  }

  /**
   * @return {String} Cookie Policy URL
   */
  getCookiePolicyURL() {
    return `${this.getBaseURL()}/company/Legal.html#cookie-policy`;
  }

  /**
   * @return {String} Disclaimer URL
   */
  getDisclaimerURL() {
    return `${this.getBaseURL()}/company/Legal.html#disclaimer`;
  }
}
