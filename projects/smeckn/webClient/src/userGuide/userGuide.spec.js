/**
 * @file User Guide Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('User Guide', function () {

  /**
   * Before Each
   * Skip locally as User Guide is hosted on a different port and Cypress can't do cross-domain testing.
   * Skip on Production as we don't want to skew production analytics data
   */
  beforeEach(function () {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Local') { this.skip(); } });
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init();
  });

  /* ---------------------- TESTS ------------------------------------------ */
  it('Home', function () {
    cy.visit(tg.$g.userGuide.getHomeURL());
    cy.get('.site-name').contains('Smeckn User Guide');
  });

  it('Terms and Conditions', function () {
    cy.visit(tg.$g.userGuide.getTermsAndConditionsURL());
    cy.get('.page').contains('Terms and Conditions').should('be.visible');
  });

  it('Privacy Policy', function () {
    cy.visit(tg.$g.userGuide.getPrivacyPolicyURL());
    cy.get('.page').contains('Privacy Policy').should('be.visible');
  });

  it('Refund Policy', function () {
    cy.visit(tg.$g.userGuide.getRefundPolicyURL());
    cy.get('.page').contains('Refund Policy').should('be.visible');
  });

  it('Cookie Policy', function () {
    cy.visit(tg.$g.userGuide.getCookiePolicyURL());
    cy.get('.page').contains('Cookie Policy').should('be.visible');
  });

  it('Disclaimer', function () {
    cy.visit(tg.$g.userGuide.getDisclaimerURL());
    cy.get('.page').contains('Disclaimer').should('be.visible');
  });
});
