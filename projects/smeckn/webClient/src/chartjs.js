/**
 * @file Chart JS Global Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Chart from 'chart.js';
import color from 'color';


/* ------------------------ CHARTJS CLASS ---------------------------------- */
/**
 * Encapsulate ChartJS state
 */
export default class ChartJS {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
    this.backgroundColorAlpha = 0.4;

    /**
     * Configure Chart.js globals
     * Reference: https://www.chartjs.org/docs/latest/
     */
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;
    Chart.defaults.global.defaultFontColor = this.$g.ui.colors.dark;
    Chart.defaults.global.defaultFontSize = '14';
    Chart.defaults.global.defaultFontStyle = 'bold';
    Chart.defaults.global.title.display = true;
    Chart.defaults.global.title.position = 'top';
    Chart.defaults.global.title.fontSize = '16';
    Chart.defaults.global.title.fontStyle = 'bold';
    Chart.defaults.global.title.fontStyle = 'bold';
    Chart.defaults.global.legend.display = true;
    Chart.defaults.global.legend.position = 'top';
    Chart.defaults.global.legend.fullWidth = false;
    Chart.defaults.global.legend.labels.fontSize = 12;
    Chart.defaults.global.legend.labels.fontStyle = 'normal';
    Chart.defaults.global.legend.labels.fontColor = this.$g.ui.colors.dark;
    Chart.defaults.global.legend.labels.padding = 10;
    Chart.defaults.global.elements.rectangle.borderWidth = 2;
    Chart.defaults.global.elements.rectangle.borderColor = this.$g.ui.colors.info;
    Chart.defaults.global.elements.rectangle.backgroundColor = this.colorToBackgroundColor(this.$g.ui.colors.info);
    Chart.defaults.global.tooltips.enabled = true;
    Chart.defaults.global.tooltips.mode = 'nearest';
    Chart.defaults.global.tooltips.backgroundColor = this.$g.ui.colors.light;
    Chart.defaults.global.tooltips.titleFontSize = 16;
    Chart.defaults.global.tooltips.titleFontStyle = 'bold';
    Chart.defaults.global.tooltips.titleFontColor = this.$g.ui.colors.info;
    Chart.defaults.global.tooltips.titleSpacing = 4;
    Chart.defaults.global.tooltips.titleMarginBottom = 4;
    Chart.defaults.global.tooltips.bodyFontSize = 14;
    Chart.defaults.global.tooltips.bodyFontStyle = 'normal';
    Chart.defaults.global.tooltips.bodyFontColor = this.$g.ui.colors.dark;
    Chart.defaults.global.tooltips.bodySpacing = 4;
    Chart.defaults.global.tooltips.footerFontSize = 14;
    Chart.defaults.global.tooltips.footerFontStyle = 'normal';
    Chart.defaults.global.tooltips.footerFontColor = this.$g.ui.colors.info;
    Chart.defaults.global.tooltips.footerSpacing = 4;
    Chart.defaults.global.tooltips.footerMarginTop = 4;
    Chart.defaults.global.tooltips.xPadding = 10;
    Chart.defaults.global.tooltips.yPadding = 10;
    Chart.defaults.global.tooltips.caretPadding = 4;
    Chart.defaults.global.tooltips.caretSize = 10;
    Chart.defaults.global.tooltips.cornerRadius = 6;
    Chart.defaults.global.tooltips.multiKeyBackground = this.$g.ui.colors.dark;
    Chart.defaults.global.tooltips.displayColors = true;
    Chart.defaults.global.tooltips.borderColor = this.$g.ui.colors.dark;
    Chart.defaults.global.tooltips.borderWidth = 2;
  }

  /**
   * Add alpha to color to make it a background color
   * @param  {String} colorString Hex color string ex. this.$g.ui.colors.info
   * @return {String} backgroundColorString
   */
  colorToBackgroundColor(colorString) {
    return color(colorString).alpha(this.backgroundColorAlpha).string();
  }

}
