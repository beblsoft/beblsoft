/**
 * @file Cypress Global Functionality
 */

/* ------------------------ CYPRESS CLASS ---------------------------------- */
/**
 * Encapsulate Cypress state
 */
export default class Cypress {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;

  }

  /**
   * Cookie name that signifies that Cypress is testing the application
   * @return {String}
   */
  static get testClientCookie() {
    return '__SMECKN_CYPRESS_TEST_CLIENT_COOKIE';
  }

  /**
   * Return True if cypress is the client
   * @return {Boolean} [description]
   */
  get isTestClient() {
    return document.cookie.includes(Cypress.testClientCookie);
  }
}
