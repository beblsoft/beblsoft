/**
 * @file Error Handler Functionality
 */


/* ------------------------------------------------------------------------- */
/**
 * Error Handler
 */
export default class ErrorHandler {

  /**
   * @constructor
   * @param {Global} g
   * @param {Object} overrideErrorHandlers {errName: (apiError) => {}} object
   *                                       To override handler funcs
   */
  constructor({ $g = null, overrideErrorHandlers = {} } = {}) {
    this.$g = $g;

    this.defaultHandler = (apiError) => { /* eslint-disable-line */
      this.$g.store.dispatch('auth/logout', { whoops: true });
      this.$g.sentry.captureException({ err: apiError });
    };

    this.handlerFuncs = {
      BadRequestError: (apiError) => {
        this.$g.alertBus.show({ text: apiError.serverMessage });
      },
      AuthError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { alertMsg: this.$g.store.state.message.messages.AUTH_ERROR_CREDENTIALS_EXPIRED });
      },
      NotFoundError: (apiError) => {
        this.$g.alertBus.show({ text: apiError.serverMessage });
      },
      FailedRequestError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { whoops: true });
        this.$g.sentry.captureException({ err: apiError });
      },
      TooManyRequestsError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { whoops: true });
        this.$g.sentry.captureException({ err: apiError });
      },
      RequestTimeoutError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { whoops: true });
        this.$g.sentry.captureException({ err: apiError });
      },
      ServerError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { whoops: true });
        this.$g.sentry.captureException({ err: apiError });
      },
      CodeError: (apiError) => { /* eslint-disable-line */
        this.$g.store.dispatch('auth/logout', { whoops: true });
        this.$g.sentry.captureException({ err: apiError });
      },
      ...overrideErrorHandlers
    };
  }

  /**
   * Handle a Smeckn API Error
   * @param {APIError} err
   */
  handle({ apiError = null } = {}) {
    console.dir(apiError);
    if (apiError.name in this.handlerFuncs) {
      this.handlerFuncs[apiError.name](apiError);
    } else {
      this.defaultHandler(apiError);
    }
  }
}
