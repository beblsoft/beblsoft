/**
 * @file Server Interface Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import API from 'smeckn-web-api/src/api';


/* ------------------------ SERVER ----------------------------------------- */
/**
 * Encapsulate all interaction with smeckn server
 */
export default class Server {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
    this.api = new API({
      baseDomain: process.env.VUE_APP_API_DOMAIN,
      version: process.env.VUE_APP_API_VERSION,
      requestTimeoutMS: parseInt(process.env.VUE_APP_API_TIMEOUTMS, 60000)
    });
  }
}
