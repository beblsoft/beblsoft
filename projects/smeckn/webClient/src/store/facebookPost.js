/**
 * @file Vuex Facebook Post Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ FACEBOOK POST MODULE --------------------------- */
const FacebookPostModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {},
  actions: {

    /**
     * Get
     *
     * On Success:
     *   Get waFacebookPost
     *
     * On Failure:
     *   Throws error with message for user
     */
    async get({ _ }, { waFacebookPostSMID }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waFacebookPost = null;

      try {
        waFacebookPost = await this.$g.server.api.facebookPost.get({
          urlParams: { smID: waFacebookPostSMID },
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waFacebookPost;
    },

    /**
     * List
     *
     * On Success:
     *   Return waFacebookPostData
     *
     * On Failure:
     *   Throws error with message for user
     */
    async list({ _ }, {
      waProfile,
      createdAfterDate = null,
      createdBeforeDate = null,
      searchString = '',
      containsMessage = null,
      sortType = null,
      sortOrder = null,
      cursor = null,
      limit = 25
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waFacebookPostData = null;

      try {
        let queryParams = { profileSMID: waProfile.smID, limit };
        if (cursor) { queryParams.cursor = cursor; }
        if (createdAfterDate) { queryParams.createdAfterDate = createdAfterDate; }
        if (createdBeforeDate) { queryParams.createdBeforeDate = createdBeforeDate; }
        if (searchString !== '') { queryParams.searchString = searchString; }
        if (containsMessage !== null) { queryParams.containsMessage = containsMessage; }
        if (sortType !== null) { queryParams.sortType = sortType; }
        if (sortOrder !== null) { queryParams.sortOrder = sortOrder; }
        waFacebookPostData = await this.$g.server.api.facebookPost.list({ queryParams });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waFacebookPostData;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default FacebookPostModule;
