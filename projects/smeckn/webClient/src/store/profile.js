/**
 * @file Vuex Profile Module
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';
import _ from 'lodash';


/* ------------------------ PROFILE MODULE --------------------------------- */
const ProfileModule = {
  namespaced: true,
  state: {
    // profiles contained in active profile group
    waProfileList: [],
  },
  mutations: {
    setWAProfileList(state, waProfileList) {
      state.waProfileList = waProfileList;
    },
    clearWAProfileList(state) {
      state.waProfileList = [];
    }
  },
  getters: {

    /**
     * @return {Array} All waProfileTypes that are currently supported by the webClient
     */
    supportedWAProfileTypeList: (state) => {
      return ['FACEBOOK'];
    },

    /**
     * @return {Array} All waProfileTypes that do NOT exist in profile group
     */
    waProfileTypesNotYetAddedList: (state, getters) => {
      return getters.supportedWAProfileTypeList.filter((waProfileType) => {
        // If matchingWAProfile is undefined, it does NOT exist yet, and therefore passes filter test
        let matchingWAProfile = _.find(state.waProfileList, (waProfile) => waProfile.type === waProfileType);
        return matchingWAProfile === undefined;
      });
    },

    /**
     * @return {Object|undefined} Returns waProfile by type, undefined if one doesn't exist
     */
    getWAProfileByType: (state) => (waProfileType) => {
      return state.waProfileList.find((waProfile) => waProfile.type === waProfileType);
    }

  },
  actions: {

    /**
     * List
     *
     * On Success:
     *   Refreshes store profile list
     *
     * On Failure:
     *   Throws APIError
     */
    async list({ __ }, { waProfileGroup = null }) {
      // Handle case where waProfileGroup is null
      if (waProfileGroup) {
        this.$g.store.commit('profile/clearWAProfileList');
        let waProfileList = await this.$g.server.api.profile.list({
          queryParams: { profileGroupID: waProfileGroup.id }
        });
        this.$g.store.commit('profile/setWAProfileList', waProfileList);
      }
    },

    /**
     * Delete
     *
     * On Success:
     *   Deletes profile
     *
     * On Failure:
     *   Throws error with message for user
     */
    async delete({ __ }, { waProfile }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.profile.delete({ urlParams: { smID: waProfile.smID } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default ProfileModule;
