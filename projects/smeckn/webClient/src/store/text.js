/**
 * @file Vuex Text Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ TEXT MODULE ------------------------------------ */
const TextModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {},
  actions: {

    /**
     * Create text
     *
     * On Success:
     *   Uploads text to server
     *   Returns waText object
     *
     * On Failure:
     *   Throws error with message for user
     */
    async create({ _ }, {
      text
    }) {
      let waText = null;
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        waText = await this.$g.server.api.text.create({ data: { text } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waText;
    },

    /**
     * Analyze text
     *
     * On Success:
     *   Has server analyze text
     *   Returns waText object, containing analysis
     *
     * On Failure:
     *   Throws error with message for user
     */
    async analyze({ _ }, {
      waText,
      analysisType
    }) {
      let waAnalyzedText = null;
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        waAnalyzedText = await this.$g.server.api.text.analyze({
          urlParams: { smID: waText.smID },
          queryParams: { analysisType }
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalyzedText;
    },

    /**
     * Create and analyze text
     *
     * On Success:
     *   Uploads text to server
     *   Analyzes text
     *   Returns waText object, containing analysis
     *   Returns waText object
     *
     * On Failure:
     *   Throws error with message for user
     */
    async createAnalyze({ _ }, {
      text,
      analysisType
    }) {
      let waText = await this.$g.store.dispatch('text/create', { text });
      let waAnalyzedText = await this.$g.store.dispatch('text/analyze', { waText, analysisType });
      return waAnalyzedText;
    },
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default TextModule;
