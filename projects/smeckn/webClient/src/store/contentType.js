/**
 * @file Vuex Content Type Module
 */

/* ------------------------ CONTENT TYPE MODULE ---------------------------- */
const ContentTypeModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * Return string version of content type
     * @param {String} waContentType WebAPI content type
     * @param {Boolean} Boolean If true, return plural form
     * @return {String}
     */
    contentTypeToString: (state) => (waContentType, plural) => {
      let str = '';

      switch (waContentType) {
      case 'PROFILE':
        str = plural ? 'Profiles' : 'Profile';
        break;
      case 'PHOTO':
        str = plural ? 'Photos' : 'Photo';
        break;
      case 'VIDEO':
        str = plural ? 'Videos' : 'Video';
        break;
      case 'MESSAGE':
        str = plural ? 'Messages' : 'Message';
        break;
      case 'LINK':
        str = plural ? 'Links' : 'Link';
        break;
      case 'POST':
        str = plural ? 'Posts' : 'Post';
        break;
      case 'POST_LINK':
        str = plural ? 'Post Links' : 'Post Link';
        break;
      case 'POST_MESSAGE':
        str = plural ? 'Post Messages' : 'Post Message';
        break;
      case 'POST_PHOTO':
        str = plural ? 'Post Photos' : 'Post Photo';
        break;
      case 'POST_VIDEO':
        str = plural ? 'Post Videos' : 'Post Video';
        break;
      case 'TWEET':
        str = plural ? 'Tweets' : 'Tweet';
        break;
      case 'TWEET_TEXT':
        str = plural ? 'Tweet Texts' : 'Tweet Text';
        break;
      case 'TWEET_PHOTO':
        str = plural ? 'Tween Photos' : 'Tweet Photo';
        break;
      case 'TWEET_VIDEO':
        str = plural ? 'Tweet Videos' : 'Tweet Video';
        break;
      case 'TWEET_LINK':
        str = plural ? 'Tweet Links' : 'Tweet Link';
        break;
      default:
        throw new Error(`Invalid waContentType ${waContentType}`);
      }
      return str;
    }

  },
  actions: {} /* End action */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default ContentTypeModule;
