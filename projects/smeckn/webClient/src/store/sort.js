/**
 * @file Vuex Sort Module
 */

/* ------------------------ SORT MODULE ------------------------------------ */
const SortModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * Return string version of sort type
     * @param {String} waSortType WebAPI sort type
     * @param {Boolean} Boolean If true, return short version
     * @return {String}
     */
    sortTypeToString: (state) => (waSortType, short) => {
      let str = '';

      switch (waSortType) {
      case 'FACEBOOK_POST_FBCREATEDATE':
        str = 'Create Date';
        str = short ? str : `Facebook Post ${str}`;
        break;
      case 'FACEBOOK_POST_NLIKES':
        str = 'Likes';
        str = short ? str : `Facebook Post ${str}`;
        break;
      case 'FACEBOOK_POST_NREACTIONS':
        str = 'Reactions';
        str = short ? str : `Facebook Post ${str}`;
        break;
      case 'FACEBOOK_POST_NCOMMENTS':
        str = 'Comments';
        str = short ? str : `Facebook Post ${str}`;
        break;
      case 'COMP_SENT_ANALYSIS_POSITIVE_SCORE':
        str = 'Positive Sentiment';
        break;
      case 'COMP_SENT_ANALYSIS_NEGATIVE_SCORE':
        str = 'Negative Sentiment';
        break;
      case 'COMP_SENT_ANALYSIS_NEUTRAL_SCORE':
        str = 'Neutral Sentiment';
        break;
      case 'COMP_SENT_ANALYSIS_MIXED_SCORE':
        str = 'Mixed Sentiment';
        break;
      case 'TEXT_CREATEDATE':
        str = 'Create Date';
        str = short ? str : `Text ${str}`;
        break;
      default:
        throw new Error(`Invalid waSortType ${waSortType}`);
      }
      return str;
    },

    /**
     * Return string version of sort order
     * @param {String} waSortOrder WebAPI sort order
     * @return {String}
     */
    sortOrderToString: (state) => (waSortOrder) => {
      let str = '';

      switch (waSortOrder) {
      case 'ASCENDING':
        str = 'Ascending';
        break;
      case 'DESCENDING':
        str = 'Descending';
        break;
      default:
        throw new Error(`Invalid waSortOrder ${waSortOrder}`);
      }
      return str;
    },

    /**
     * Return waSortType from waContentAttrType
     * @param  {String} waContentAttrType
     * @return {String} waSortType
     */
    contentAttrTypeToSortType: (state) => (waContentAttrType) => {
      let waSortType = '';

      switch (waContentAttrType) {
      case 'COMP_SENT_TYPE_POSITIVE':
      case 'COMP_SENT_ANALYSIS_POSITIVE_SCORE':
        waSortType = 'COMP_SENT_ANALYSIS_POSITIVE_SCORE';
        break;
      case 'COMP_SENT_TYPE_NEGATIVE':
      case 'COMP_SENT_ANALYSIS_NEGATIVE_SCORE':
        waSortType = 'COMP_SENT_ANALYSIS_NEGATIVE_SCORE';
        break;
      case 'COMP_SENT_TYPE_NEUTRAL':
      case 'COMP_SENT_ANALYSIS_NEUTRAL_SCORE':
        waSortType = 'COMP_SENT_ANALYSIS_NEUTRAL_SCORE';
        break;
      case 'COMP_SENT_TYPE_MIXED':
      case 'COMP_SENT_ANALYSIS_MIXED_SCORE':
        waSortType = 'COMP_SENT_ANALYSIS_MIXED_SCORE';
        break;
      default:
        throw new Error(`Invalid waContentAttrType ${waContentAttrType}`);
      }
      return waSortType;
    }
  },

  actions: {} /* End action */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default SortModule;
