/**
 * @file Vuex Cart Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ CART MODULE ------------------------------------ */
const CartModule = {
  namespaced: true,
  state: {
    waCart: null,
    waCartCreditCard: null,
  },
  mutations: {
    setWACart(state, waCart) {
      state.waCart = waCart;
    },

    clearWACart(state) {
      state.waCart = null;
    },

    setWACartCreditCard(state, waCartCreditCard) {
      state.waCartCreditCard = waCartCreditCard;
    },

    clearWACartCreditCard(state) {
      state.waCartCreditCard = null;
    },
  },
  getters: {},
  actions: {

    /**
     * Create Cart
     *
     * On Success:
     *   Creates cart from server and stores it in the store
     *
     * On Failure:
     *   Throws Error
     */
    async create({ _ }, { cartUnitCountList, bCurrencyCode = 'USD' }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waCart = null;

      try {
        waCart = await this.$g.server.api.cart.create({ data: { cartUnitCountList, bCurrencyCode } });
        this.$g.store.commit('cart/setWACart', waCart);
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default CartModule;
