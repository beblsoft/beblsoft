/**
 * @file Vuex CreditCard Module
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';
import ErrorHandler from '../server/errorHandler';
import visaImgSrc from '../img/flaticon/visa.png';
import masterCardImgSrc from '../img/flaticon/mastercard.png';
import americanExpressImgSrc from '../img/flaticon/american-express.png';
import discoverImgSrc from '../img/flaticon/discover.png';
import dinersClubImgSrc from '../img/flaticon/diners-club.png';
import jcbImgSrc from '../img/flaticon/jcb.png';
import unionPayImgSrc from '../img/flaticon/unionpay.png';
import creditCardImgSrc from '../img/flaticon/credit-card.png';


/* ------------------------ CREDIT CARD MODULE ----------------------------- */
const CreditCardModule = {
  namespaced: true,
  state: {
    waCreditCardList: [],
    waDefaultCreditCard: null
  },
  mutations: {
    setWACreditCardList(state, waCreditCardList) {
      state.waCreditCardList = waCreditCardList;
    },

    clearWACreditCardList(state) {
      state.waCreditCardList = [];
    },

    setWADefaultCreditCard(state, waDefaultCreditCard) {
      state.waDefaultCreditCard = waDefaultCreditCard;
    },

    clearWADefaultCreditCard(state) {
      state.waDefaultCreditCard = null;
    },
  },
  getters: {

    /**
     * Get Credit Card Image from its brand
     * Reference: https://stripe.com/docs/api/cards/object#card_object-brand
     * @param {Object} state
     * @param  {String} Card brand (from Stripe)
     * @return {Object} Image
     */
    getCreditCardImage: (state) => (brand) => {
      let brandImageObj = {
        'Visa': visaImgSrc,
        'MasterCard': masterCardImgSrc,
        'American Express': americanExpressImgSrc,
        'Discover': discoverImgSrc,
        'Diners Club': dinersClubImgSrc,
        'JCB': jcbImgSrc,
        'UnionPay': unionPayImgSrc
      };
      let imgSrc = creditCardImgSrc;
      if (brand in brandImageObj) {
        imgSrc = brandImageObj[brand];
      }
      return imgSrc;
    },

    /**
     * Return index of default credit card in waCreditCardList
     * @param {Object} state
     * @return {Number} Index of defaultCreditCard, -1 if it doesn't exist
     */
    defaultCreditCardIndex: (state) => {
      let rval = _.findIndex(state.waCreditCardList, (waCreditCard) => {
        try {
          return waCreditCard.id === state.waDefaultCreditCard.id;
        } catch (error) {
          return false;
        }
      });
      return rval;
    },

  },
  actions: {

    /**
     * Create
     *
     * On Success:
     *   Creates card
     *
     * On Failure:
     *   Throws Error
     */
    async create({ __ }, { token }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waCreditCard = null;

      try {
        waCreditCard = await this.$g.server.api.creditCard.create({ data: { token } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waCreditCard;
    },

    /**
     * Delete
     *
     * On Success:
     *   Deletes card
     *
     * On Failure:
     *   Throws Error
     */
    async delete({ __ }, { waCreditCard }) {
      let errorHandler = new ErrorHandler({ $g: this.$g });

      try {
        await this.$g.server.api.creditCard.delete({ urlParams: { id: waCreditCard.id } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * List
     *
     * On Success:
     *   Lists cards and updates waCreditCardList
     *
     * On Failure:
     *   Throws Error
     */
    async list({ __ }) {
      let errorHandler = new ErrorHandler({ $g: this.$g });
      let waCreditCardList = null;

      try {
        waCreditCardList = await this.$g.server.api.creditCard.list();
        this.$g.store.commit('creditCard/setWACreditCardList', waCreditCardList);
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Set Default
     *
     * On Success:
     *   Sets card as default and updates waDefaultCreditCard
     *
     * On Failure:
     *   Throws Error
     */
    async setDefault({ __ }, { waCreditCard }) {
      let errorHandler = new ErrorHandler({ $g: this.$g });
      let waDefaultCreditCard = null;

      try {
        waDefaultCreditCard = await this.$g.server.api.creditCard.setDefault({
          data: { id: waCreditCard.id }
        });
        this.$g.store.commit('creditCard/setWADefaultCreditCard', waDefaultCreditCard);
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waDefaultCreditCard;
    },

    /**
     * Get Default
     *
     * On Success:
     *   Gets default card and updates waDefaultCreditCard
     *
     * On Failure:
     *   Throws Error
     */
    async getDefault({ __ }) {
      let overrideErrorHandlers = {
        NotFoundError: (apiError) => {
          this.$g.store.commit('creditCard/clearWADefaultCreditCard');
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waDefaultCreditCard = null;

      try {
        waDefaultCreditCard = await this.$g.server.api.creditCard.getDefault();
        this.$g.store.commit('creditCard/setWADefaultCreditCard', waDefaultCreditCard);
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waDefaultCreditCard;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default CreditCardModule;
