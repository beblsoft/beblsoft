/**
 * @file Vuex Message Module
 */

/* ------------------------ MESSAGE MODULE --------------------------------- */
const MessageModule = {
  namespaced: true,
  state: {
    messages: null,
  },
  mutations: {
    setMessages(state, messages) {
      state.messages = messages;
    },
  },
  getters: {},
  actions: {}
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default MessageModule;
