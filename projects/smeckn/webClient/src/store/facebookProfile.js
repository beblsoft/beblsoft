/**
 * @file Vuex Facebook Profile Module
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ FACEBOOK PROFILE MODULE ------------------------ */
const FacebookProfileModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * @return {Object|undefined} Return waProfile object or undefined
     */
    waProfile: (state, getters, rootState, rootGetters) => {
      return rootGetters['profile/getWAProfileByType']('FACEBOOK');
    },

    /**
     * @return {Boolean} Return true if there is a waProfile in profile group, false otherwise
     */
    waProfileExists: (state, getters) => {
      return getters.waProfile !== undefined;
    },
  },

  actions: {

    /* --------------- FACEBOOK COMPONENT FUNTIONS ------------------------- */
    /**
     * Add
     *
     * On Success:
     *   Prompts user for their facebook login details
     *   Creates facebook profile on backend
     *   Reloads the profile list
     *   Navigates to the facebook overview
     *
     * On Failure:
     *   Throws error with message for user
     *
     * Always:
     *   Ensure that user is logged out of Facebook, this guarantees that if
     *   the user logs in again, the login dialog will pop up
     */
    async add({ __ }, { waProfileGroup }) {
      let loginResp = await this.$g.store.dispatch('facebookProfile/login', {
        scope: this.$g.store.state.constant.waConstants.FBPROFILE_DESIRED_PERMISSION_LIST,
        authType: 'reauthorize'
      });
      try {
        if (loginResp.status === 'connected') {
          let authResp = loginResp.authResponse;
          await this.$g.store.dispatch('facebookProfile/create', {
            waProfileGroup,
            fbID: authResp.userID,
            shortTermAccessToken: authResp.accessToken
          });
          await this.$g.store.dispatch('profile/list', { waProfileGroup });
          await this.$g.router.push({ name: 'rnFacebookPostAll' });
        }
      } finally {
        await this.$g.store.dispatch('facebookProfile/logout');
      }
    },

    /**
     * Update Credentials
     *
     * On Success:
     *   Prompts user for their facebook login details
     *   Checks same FBID set
     *   Updates server credentials
     *
     * On Failure:
     *   Throws error with message for user
     *
     * Always:
     *   Ensure that user is logged out of Facebook, this guarantees that if
     *   the user logs in again, the login dialog will pop up
     */
    async updateCredentials({ __ }, { waProfile }) {
      let loginResp = await this.$g.store.dispatch('facebookProfile/login', {
        scope: this.$g.store.state.constant.waConstants.FBPROFILE_DESIRED_PERMISSION_LIST,
        authType: 'rerequest'
      });
      try {
        if (loginResp.status === 'connected') {
          let authResp = loginResp.authResponse;
          await this.$g.store.dispatch('facebookProfile/updateAccessToken', {
            waProfile,
            shortTermAccessToken: authResp.accessToken,
          });
          this.$g.alertBus.show({
            text: this.$g.store.state.message.messages.SUCCESSFULLY_UPDATED_FACEBOOK_PROFILE_CREDENTIALS,
            severity: 'success'
          });
        }
      } finally {
        await this.$g.store.dispatch('facebookProfile/logout');
      }
    },

    /* --------------- SMECKN SERVER FUNCTIONS ----------------------------- */
    /*
     * Create
     *
     * On Success:
     *   Creates a new facebook profile on smeckn server
     *
     * On Failure:
     *   Throws error with message for user
     */
    async create({ __ }, { waProfileGroup, fbID, shortTermAccessToken }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
        ServerError: (apiError) => { throw new Error(this.$g.store.state.message.messages.FACEBOOK_CANT_COMMUNICATE); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waFacebookProfile = null;

      try {
        waFacebookProfile = await this.$g.server.api.facebookProfile.create({
          data: { profileGroupID: waProfileGroup.id, fbID, shortTermAccessToken }
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waFacebookProfile;
    },


    /**
     * Get
     *
     * On Success:
     *   Get waFacebookProfile
     *
     * On Failure:
     *   Throws error with message for user
     */
    async get({ __ }, { waProfile }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waFacebookProfile = null;

      try {
        waFacebookProfile = await this.$g.server.api.facebookProfile.get({
          urlParams: { smID: waProfile.smID },
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waFacebookProfile;
    },

    /**
     * Update Access Token
     *
     * On Success:
     *   Update access token on server
     *
     * On Failure:
     *   Throws error with message for user
     */
    async updateAccessToken({ __ }, { waProfile, shortTermAccessToken }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
        ServerError: (apiError) => { throw new Error(this.$g.store.state.message.messages.FACEBOOK_CANT_COMMUNICATE); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waFacebookProfile = null;

      try {
        waFacebookProfile = await this.$g.server.api.facebookProfile.update({
          urlParams: { smID: waProfile.smID },
          data: { shortTermAccessToken }
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waFacebookProfile;
    },

    /* --------------- FACEBOOK API FUNTIONS ------------------------------- */
    /**
     * Get Login Status
     *
     * @return {Promise} Resloves to login status
     *                   If logged in:
     *                   {
     *                     status: 'connected',
     *                     authResponse: {
     *                       accessToken: '...',
     *                       expiresIn:'...',
     *                       signedRequest:'...',
     *                       userID:'...'
     *                     }
     *                   }
     *                   If not logged in:
     *                   {
     *                     status: 'not_authorized'
     *                   }
     *
     * On Success:
     *   Returns facbook login status
     *
     * On Failure:
     *   Throws error with message for user
     *
     * Reference:
     *   https://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus/
     */
    getLoginStatus({ __ }) {
      return new Promise(async (resolve, reject) => {
        try {
          await window.FB.getLoginStatus((resp) => { resolve(resp); });
        } catch (err) {
          this.$g.sentry.captureException({ err });
          reject(new Error(this.$g.store.state.message.messages.ERROR_CONNECTING_TO_FACEBOOK));
        }
      });
    },

    /**
     * Logout
     *
     * @return {Promise} Resloves logout
     *
     * On Success:
     *   Ensures that user is logged out of facebook
     *
     * On Failure:
     *   Throws error with message for user
     *
     * Reference:
     *   https://developers.facebook.com/docs/reference/javascript/FB.logout/
     */
    async logout({ __ }) {
      return new Promise(async (resolve, reject) => {
        try {
          let loginStatus = await this.$g.store.dispatch('facebookProfile/getLoginStatus');
          if (loginStatus.status === 'connected') {
            await window.FB.logout((resp) => { resolve(resp); });
          } else { resolve(); }
        } catch (err) {
          this.$g.sentry.captureException({ err });
          reject(new Error(this.$g.store.state.message.messages.ERROR_CONNECTING_TO_FACEBOOK));
        }
      });
    },

    /**
     * Login
     *
     * @param {Array} scope List of extended permissions
     * @param {String} authType Optional key, supports 3 values: 'rerequest', 'reauthenticate', 'reauthorize'
     * @return {Promise} Resloves to login object, same as getLoginStatus
     *
     * On Success:
     *   Prompts user to login
     *   Returns login response
     *
     * On Failure:
     *   Throws error with message for user
     *
     * Reference:
     *   https://developers.facebook.com/docs/reference/javascript/FB.login/v3.2
     */
    login({ __ }, { scope, authType = 'reauthorize' }) {
      return new Promise(async (resolve, reject) => {
        try {
          let options = { scope: scope.join(',') };
          if (authType !== null) { options.auth_type = authType; } /* eslint-disable-line */
          await window.FB.login((resp) => { resolve(resp); }, options);
        } catch (err) {
          this.$g.sentry.captureException({ err });
          reject(new Error(this.$g.store.state.message.messages.ERROR_CONNECTING_TO_FACEBOOK));
        }
      });
    }
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default FacebookProfileModule;
