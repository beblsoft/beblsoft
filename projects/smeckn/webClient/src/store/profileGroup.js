/**
 * @file Vuex Profile Group Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';
import ErrorHandler from '../server/errorHandler';


/* ------------------------ PROFILE GROUP MODULE --------------------------- */
const ProfileGroupModule = {
  namespaced: true,
  state: {
    waProfileGroupList: [],
    waProfileGroup: null,
  },
  mutations: {
    setWAProfileGroupList(state, waProfileGroupList) {
      state.waProfileGroupList = waProfileGroupList;
    },

    clearWAProfileGroupList(state) {
      state.waProfileGroupList = [];
    },

    setWAProfileGroup(state, waProfileGroup) {
      state.waProfileGroup = waProfileGroup;
    },

    clearWAProfileGroup(state) {
      state.waProfileGroup = null;
    },
  },
  getters: {
    waProfileGroupsExist(state) {
      return state.waProfileGroupList.length > 0;
    },

    waProfileGroupSet(state) {
      return state.waProfileGroup !== null;
    },

    /**
     * Return true if another profile group can be added
     */
    canAddProfileGroup(state, getters, rootState, rootGetters) {
      return state.waProfileGroupList.length < rootState.constant.waConstants.MAX_PROFILEGROUPS;
    },
  },
  actions: {

    /**
     * Create
     *
     * On Success:
     *   Creates a new profile group
     *   Navigates back to dashboard
     *
     * On Failure:
     *   Throws error with message for user
     */
    async create({ __ }, { name = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.profileGroup.create({ data: { name } });
        this.$g.router.push({ name: 'rnProfileGroupsDashboard' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Delete
     *
     * On Success:
     *   Deletes profile group
     *   Navigates correctly
     *
     * On Failure:
     *   Throws error with message for user
     */
    async delete({ __ }, { waProfileGroup = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
        NotFoundError: (apiError) => {
          this.$g.router.replace({ name: 'rnProfileGroupsManage' });
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.profileGroup.delete({ urlParams: { id: waProfileGroup.id } });

        // Did we delete the last profile group?
        // If yes: go back to dashboard
        // If no: Stay on manage page
        if (this.$g.store.state.profileGroup.waProfileGroupList.length === 1) {
          this.$g.router.push({ name: 'rnProfileGroupsDashboard' });
        } else {
          this.$g.router.push({ name: 'rnProfileGroupsManage' });
        }
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Update
     *
     * On Success:
     *   Updates profile group
     *   Navigates back to dashboard
     *
     * On Failure:
     *   If 404, navigates back to dashboard
     *   If 400, throws error with message for user
     */
    async update({ __ }, {
      waProfileGroup = null,
      name = ''
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
        NotFoundError: (apiError) => {
          this.$g.alertBus.show({ text: apiError.serverMessage });
          this.$g.router.replace({ name: 'rnProfileGroupsManage' });
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.profileGroup.update({
          urlParams: { id: waProfileGroup.id },
          data: { name }
        });
        this.$g.router.push({ name: 'rnProfileGroupsManage' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Clear All
     *
     * On Success:
     *   Clears all ProfileGroup state
     */
    async clearAll({ __ }) {
      this.$g.store.commit('profileGroup/clearWAProfileGroupList');
      await this.$g.store.dispatch('profileGroup/clearCurrent');
    },

    /**
     * Clear Current
     *
     * On Success:
     *   Clears current active ProfileGroup and associated Profiles
     */
    async clearCurrent({ __ }) {
      this.$g.store.commit('profileGroup/clearWAProfileGroup');
      this.$g.store.commit('profile/clearWAProfileList');
    },

    /**
     * List
     *
     * On Success:
     *   Refreshes store profile group list
     *
     * On Failure:
     *   Throws APIError
     */
    async list({ __ }) {
      let waProfileGroupList = await this.$g.server.api.profileGroup.list();
      let waProfileGroupListSorted = _.sortBy(waProfileGroupList, (waProfileGroup) => {
        return waProfileGroup.name;
      });
      this.$g.store.commit('profileGroup/setWAProfileGroupList', waProfileGroupListSorted);
    },

    /**
     * Dash Init
     *
     * On Success:
     *   Refreshes the store's profile groups
     *   If no profile groups exist, create first profile group
     */
    async dashInit({ __ }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { this.$g.store.dispatch('auth/logout', { alertMsg: apiError.serverMessage }); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.store.dispatch('profileGroup/clearAll');
        await this.$g.store.dispatch('profileGroup/list');

        // Create first profile group if it doesn't exist
        if (this.$g.store.state.profileGroup.waProfileGroupList.length === 0) {
          await this.$g.server.api.profileGroup.create({
            data: { name: this.$g.store.state.message.messages.DEFAULT_PROFILE_GROUP_NAME }
          });
          await this.$g.store.dispatch('profileGroup/list');
        }
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Manage Init
     *
     * On Success:
     *   Refreshes the store's profile groups
     *   Navigates appropriately
     */
    async manageInit({ __ }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { this.$g.store.dispatch('auth/logout', { alertMsg: apiError.serverMessage }); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.store.dispatch('profileGroup/clearAll');
        await this.$g.store.dispatch('profileGroup/list');

        // If no profile groups exist, navigate back to dash
        if (this.$g.store.state.profileGroup.waProfileGroupList.length === 0) {
          this.$g.router.replace({ name: 'rnProfileGroupsDashboard' });
        }
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Drill Down
     *
     * On Success:
     *   Drills Down into a particular profile group
     */
    async drillDown({ __ }, { waProfileGroup = null }) {
      this.$g.store.commit('profileGroup/clearWAProfileGroup');
      this.$g.store.commit('profileGroup/setWAProfileGroup', waProfileGroup);
      this.$g.router.push({ name: 'rnProfileGroupDrillDown' });
    },
  } /* End actions*/
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default ProfileGroupModule;
