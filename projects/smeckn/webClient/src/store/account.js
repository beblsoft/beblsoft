/**
 * @file Vuex Account Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ ACCOUNT MODULE --------------------------------- */
const AccountModule = {
  namespaced: true,
  state: {
    waAccount: null,
  },
  mutations: {
    setWAAccount(state, waAccount) {
      state.waAccount = waAccount;
    },
    clearWAAccount(state) {
      state.waAccount = null;
    },
  },
  getters: {
    isWALoggedIn(state) {
      return state.waAccount !== null;
    },
    isWAAccountAdmin(state) {
      return state.waAccount !== null && state.waAccount.type === 'ADMIN';
    },
  },
  actions: {

    /**
     * Set the active account
     */
    async set({ _ }, {
      waAccount = null,
    }) {
      this.$g.store.commit('account/setWAAccount', waAccount);
      this.$g.sentry.setUserScope({ aID: waAccount.id, aEmail: waAccount.email });
      this.$g.googleAnalytics.setUser({ aID: waAccount.id });

      // Clear associated profile group data
      await this.$g.store.dispatch('profileGroup/clearAll');
    },

    /**
     * Clear the active account
     */
    async clear({ _ }) {
      this.$g.googleAnalytics.clearUser();
      this.$g.sentry.clearUserScope();
      this.$g.store.commit('account/clearWAAccount');

      // Clear associated profile group data
      await this.$g.store.dispatch('profileGroup/clearAll');
    },

    /**
     * Update Password
     *
     * On Success:
     *   Updates account password
     *
     * On Failure:
     *   Throws error with message for user
     */
    async updatePassword({ _ }, {
      waAccount,
      oldPassword,
      newPassword
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
        AuthError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.account.update({
          urlParams: { id: waAccount.id },
          data: { oldPassword, newPassword }
        });
        this.$g.alertBus.show({ text: this.$g.store.state.message.messages.PASSWORD_SUCCESSFULLY_UPDATED, severity: 'success' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    }
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default AccountModule;
