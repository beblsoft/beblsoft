/**
 * @file Vuex Histogram Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ HISTOGRAM MODULE ------------------------------- */
const HistogramModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {},
  actions: {

    /**
     * Get
     *
     * On Success:
     *   Get waHistogramList
     *
     * On Failure:
     *   Throws error with message for user
     */
    async get({ _ }, {
      waProfile,
      contentType,
      contentAttrType,
      groupByType,
      intervalStartDate,
      intervalEndDate
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waHistogramList = [];

      try {
        let queryParams = {
          profileSMID: waProfile.smID,
          contentType,
          contentAttrType,
          groupByType,
          intervalStartDate,
          intervalEndDate
        };
        waHistogramList = await this.$g.server.api.histogram.get({ queryParams });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waHistogramList;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default HistogramModule;
