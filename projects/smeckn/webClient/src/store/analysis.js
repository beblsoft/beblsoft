/**
 * @file Vuex Analysis Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ ANALYSIS MODULE -------------------------------- */
const AnalysisModule = {
  namespaced: true,
  state: {},
  mutations: {},

  getters: {

    /**
     * Return error message for a failed analysis
     * @return {String} error message
     */
    getAnalysisErrorMessage: (state) => ($g, waAnalysisStatus) => {
      let msg = '';
      switch (waAnalysisStatus) {
      case 'TIMED_OUT':
        msg = $g.store.state.message.messages.ANALYSIS_FAILED_TIMED_OUT;
        break;
      default:
        msg = $g.store.state.message.messages.ANALYSIS_FAILED_UNKNOWN;
        break;
      }
      return msg;
    }
  },

  actions: {

    /**
     * Create
     *
     * On Success:
     *   Creates a new analysis
     *
     * On Failure:
     *   Throws error with message for user
     */
    async create({ _ }, { waProfile, waContentType, waAnalysisType }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waAnalysis = null;

      try {
        waAnalysis = await this.$g.server.api.analysis.create({
          data: {
            profileSMID: waProfile.smID,
            contentType: waContentType,
            analysisType: waAnalysisType
          }
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalysis;
    },

    /**
     * Update
     *
     * On Success:
     *   Updates analysis
     *
     * On Failure:
     *   Throws error with message for user
     */
    async update({ _ }, { waAnalysis, errorSeen = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waAnalysisUpdated = null;
      let data = {};
      if (errorSeen !== null) { data.errorSeen = errorSeen; }

      try {
        waAnalysisUpdated = await this.$g.server.api.analysis.update({
          urlParams: { smID: waAnalysis.smID },
          data
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalysisUpdated;
    },

    /**
     * List
     *
     * On Success:
     *   Lists analyses
     *
     * On Failure:
     *   Throws error with message for user
     */
    async list({ _ }, { waProfile, waAnalysisStatus = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waAnalysisList = [];
      let queryParams = { profileSMID: waProfile.smID };
      if (waAnalysisStatus !== null) { queryParams.status = waAnalysisStatus; }

      try {
        waAnalysisList = await this.$g.server.api.analysis.list({ queryParams });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalysisList;
    },

    /**
     * Get
     *
     * On Success:
     *   Get analysis
     *
     * On Failure:
     *   Throws error with message for user
     */
    async get({ _ }, { waAnalysis }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waDupAnalysis = null;

      try {
        waDupAnalysis = await this.$g.server.api.analysis.get({ urlParams: { smID: waAnalysis.smID }, });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waDupAnalysis;
    },

    /**
     * Get Count
     *
     * On Success:
     *   Get analysis counts
     *
     * On Failure:
     *   Throws error with message for user
     */
    async getCount({ _ }, { waProfile, waContentType, waAnalysisType }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waAnalysisCount = null;

      try {
        waAnalysisCount = await this.$g.server.api.analysis.getCount({
          queryParams: {
            profileSMID: waProfile.smID,
            contentType: waContentType,
            analysisType: waAnalysisType
          },
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalysisCount;
    },

    /**
     * Get Progress
     *
     * On Success:
     *   Get analysis progress
     *
     * On Failure:
     *   Throws error with message for user
     */
    async getProgress({ _ }, { waAnalysis }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waAnalysisProgress = null;

      try {
        waAnalysisProgress = await this.$g.server.api.analysis.getProgress({
          urlParams: { smID: waAnalysis.smID },
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waAnalysisProgress;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default AnalysisModule;
