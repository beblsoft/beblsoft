/**
 * @file Vuex Constant Module
 */

/* ------------------------ DEFAULT CONSTANTS ------------------------------ */
// Note: these should be kept in sync with $SMECKN/server/wAPIV1/constant/constant.py
const waConstantsDefault = {
  MAX_PROFILEGROUPS: 5,
  MAX_ACCOUNT_CREDIT_CARDS: 5,
  TEXT_MAX_LENGTH: 5000,
  FBPROFILE_REQURIED_PERMISSION_LIST: ['public_profile', 'user_posts'],
  FBPROFILE_DESIRED_PERMISSION_LIST: ['public_profile', 'user_posts'],
  ENVIRONMENT: 'Local',
  GIT_COMMIT_SHA: '1676b07fdcc8e8a28264043a2abf5c61c46e17bd',
  COMPREHEND_SENTIMENT_SUPPORTED_BLANGUAGE_LIST: [
    'English',
    'Spanish',
    'French',
    'Dutch',
    'Italian',
    'Portuguese'
  ]
};


/* ------------------------ CONSTANT MODULE -------------------------------- */
const ConstantModule = {
  namespaced: true,
  state: {
    waConstants: waConstantsDefault,
  },
  mutations: {
    setWAConstants(state, waConstants) {
      state.waConstants = waConstants;
    },
  },
  getters: {},
  actions: {

    /**
     * Load constants. If API fails, use defaults
     */
    async load({ _ }) {
      let waConstants = {};
      try {
        waConstants = await this.$g.server.api.constant.list();
      } catch (apiError) {
        waConstants = waConstantsDefault;
      } finally {
        this.$g.store.commit('constant/setWAConstants', waConstants);
      }
    }
  } /* End action */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default ConstantModule;
