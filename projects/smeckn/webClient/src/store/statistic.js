/**
 * @file Vuex Statistic Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ STATISTIC MODULE ------------------------------- */
const StatisticModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {},
  actions: {

    /**
     * Get
     *
     * On Success:
     *   Get waStatisticList
     *
     * On Failure:
     *   Throws error with message for user
     */
    async get({ _ }, {
      waProfile,
      contentType,
      contentAttrType,
      intervalStartDate = null,
      intervalEndDate = null
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waStatisticList = [];

      try {
        let queryParams = { profileSMID: waProfile.smID, contentType, contentAttrType };
        if (intervalStartDate) { queryParams.intervalStartDate = intervalStartDate; }
        if (intervalEndDate) { queryParams.intervalEndDate = intervalEndDate; }
        waStatisticList = await this.$g.server.api.statistic.get({ queryParams });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waStatisticList;
    },
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default StatisticModule;
