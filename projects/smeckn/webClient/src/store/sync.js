/**
 * @file Vuex Sync Module
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';
import moment from 'moment';


/* ------------------------ SYNC MODULE ------------------------------------ */
const SyncModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * @return True if sync is restartable
     */
    isSyncRestartable: (state) => (waSync) => {
      let restartMoment = moment(waSync.restartDate);
      return moment() > restartMoment;
    },

    /**
     * Return error message for a failed sync
     * @return {String} error message
     */
    getSyncErrorMessage: (state) => ($g, waSync) => {
      let msg = '';
      switch (waSync.status) {
      case 'CONTENT_LIMIT_REACHED':
        msg = $g.store.state.message.messages.SYNC_FAILED_CONTENT_LIMIT_REACHED;
        break;
      case 'CREDENTIALS_EXPIRED':
        msg = $g.store.state.message.messages.SYNC_FAILED_CREDENTIALS_EXPIRED;
        break;
      case 'THIRDPARTY_API_FAILURE':
        msg = $g.store.state.message.messages.SYNC_FAILED_THIRDPARTY_API_FAILURE;
        break;
      case 'PROFILE_DELETED':
        msg = $g.store.state.message.messages.SYNC_FAILED_PROFILE_DELETED;
        break;
      case 'TIMED_OUT':
        msg = $g.store.state.message.messages.SYNC_FAILED_TIMED_OUT;
        break;
      default:
        msg = $g.store.state.message.messages.SYNC_FAILED_UNKNOWN;
        break;
      }
      return msg;
    }

  },
  actions: {

    /**
     * Create
     *
     * On Success:
     *   Creates a new sync
     *
     * On Failure:
     *   Throws error with message for user
     */
    async create({ _ }, { waProfile }) {
      let errorHandler = new ErrorHandler({ $g: this.$g });
      let waSync = null;

      try {
        waSync = await this.$g.server.api.sync.create({ data: { profileSMID: waProfile.smID } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waSync;
    },

    /**
     * Update
     *
     * On Success:
     *   Updates sync
     *
     * On Failure:
     *   Throws error with message for user
     */
    async update({ _ }, { waSync, errorSeen = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waSyncUpdated = null;
      let data = {};
      if (errorSeen !== null) { data.errorSeen = errorSeen; }

      try {
        waSyncUpdated = await this.$g.server.api.sync.update({
          urlParams: { smID: waSync.smID },
          data
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waSyncUpdated;
    },

    /**
     * List
     *
     * On Success:
     *   Lists syncs
     *
     * On Failure:
     *   Throws error with message for user
     */
    async list({ _ }, { waProfile, latest = null }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waSyncList = [];
      let queryParams = { profileSMID: waProfile.smID };
      if (latest !== null) { queryParams.latest = latest; }

      try {
        waSyncList = await this.$g.server.api.sync.list({ queryParams });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waSyncList;
    },

    /**
     * Get Latest
     *
     * On Success:
     *   Returns the latest profile sync, null if None exists
     *
     * On Failure:
     *   Throws error with message for user
     */
    async getLatest({ _ }, { waProfile }) {
      let waSyncList = await this.$g.store.dispatch('sync/list', { waProfile, latest: true });
      return waSyncList.length > 0 ? waSyncList[0] : null;
    },

    /**
     * Get Progress
     *
     * On Success:
     *   Returns the sync progress
     *
     * On Failure:
     *   Returns null
     */
    async getProgress({ _ }, { waSync }) {
      let waSyncProgress = null;
      try {
        waSyncProgress = await this.$g.server.api.sync.getProgress({ urlParams: { smID: waSync.smID } });
      } catch (apiError) {
        console.warn('Sync Get Progress Call Failed');
      }
      return waSyncProgress;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default SyncModule;
