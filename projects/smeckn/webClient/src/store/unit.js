/**
 * @file Vuex Unit Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';


/* ------------------------ UNIT MODULE ------------------------------------ */
const UnitModule = {
  namespaced: true,
  state: {
    waUnitPriceList: [],
    waUnitBalanceList: []
  },
  mutations: {
    setWAUnitPriceList(state, waUnitPriceList) {
      state.waUnitPriceList = waUnitPriceList;
    },

    clearWAUnitPriceList(state) {
      state.waUnitPriceList = [];
    },

    setWAUnitBalanceList(state, waUnitBalanceList) {
      state.waUnitBalanceList = waUnitBalanceList;
    },

    clearWAUnitBalanceList(state) {
      state.waUnitBalanceList = [];
    },
  },
  getters: {
    getWAUnitPriceByType: (state) => (unitType) => {
      return state.waUnitPriceList.find((waUnitPrice) => waUnitPrice.unitType === unitType);
    },
    getWAUnitBalanceByType: (state) => (unitType) => {
      return state.waUnitBalanceList.find((waUnitBalance) => waUnitBalance.unitType === unitType);
    }
  },
  actions: {

    /**
     * Get Prices
     *
     * On Success:
     *   Refreshes store unit price list
     *
     * On Failure:
     *   Throws APIError
     */
    async getPrices({ state, commit, rootState }) {
      let waUnitPriceList = await this.$g.server.api.unit.getPrice();

      // Filter photo anlysis units if feature not enabled
      if (!rootState.feature.photoAnalysisUnits) {
        waUnitPriceList = _.filter(waUnitPriceList, (waUnitPrice) => {
          return waUnitPrice.unitType !== 'PHOTO_ANALYSIS';
        });
      }
      this.$g.store.commit('unit/setWAUnitPriceList', waUnitPriceList);
    },

    /**
     * Get Balances
     *
     * On Success:
     *   Refreshes store unit balance list
     *
     * On Failure:
     *   Throws APIError
     */
    async getBalances({ state, commit, rootState }) {
      let waUnitBalanceList = await this.$g.server.api.unit.getBalance();

      // Filter photo anlysis units if feature not enabled
      if (!rootState.feature.photoAnalysisUnits) {
        waUnitBalanceList = _.filter(waUnitBalanceList, (waUnitBalance) => {
          return waUnitBalance.unitType !== 'PHOTO_ANALYSIS';
        });
      }
      this.$g.store.commit('unit/setWAUnitBalanceList', waUnitBalanceList);
    }

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default UnitModule;
