/**
 * @file Vuex GroupByDateInterval Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';


/* ------------------------ GROUP BY DATE INTERVAL MODULE ------------------ */
const GroupByDateInterval = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * Map waGroupByDateInterval to corresponding ChartJS time unit
     *
     * Reference:
     *  https://www.chartjs.org/docs/latest/axes/cartesian/time.html#time-units
     *
     * @return {String}
     */
    getChartJSTimeUnit: () => (waGroupByDateInterval) => {
      let str = '';

      switch (waGroupByDateInterval) {
      case 'SECOND':
      case 'MINUTE':
      case 'HOUR':
      case 'DAY':
      case 'MONTH':
      case 'YEAR':
        str = _.toLower(waGroupByDateInterval);
        break;
      default:
        throw new Error(`Invalid waGroupByDateInterval ${waGroupByDateInterval}`);
      }
      return str;
    },

    /**
     * Map waGroupByDateInterval to corresponding Moment Duration Name
     *
     * Reference:
     *   https://momentjs.com/docs/#/durations/creating/
     *
     * @return {String} Moment duration name
     */
    getMomentDurationName: () => (waGroupByDateInterval) => {
      let str = '';

      switch (waGroupByDateInterval) {
      case 'SECOND':
      case 'MINUTE':
      case 'HOUR':
      case 'DAY':
      case 'MONTH':
      case 'YEAR':
        str = `${_.toLower(waGroupByDateInterval)}s`;
        break;
      default:
        throw new Error(`Invalid waGroupByDateInterval ${waGroupByDateInterval}`);
      }
      return str;
    },

    /**
     * Map waGroupByDateInterval to corresponding Moment format string
     * @return {String} Moment format string
     */
    getMomentFormatString: () => (waGroupByDateInterval) => {
      let str = '';

      switch (waGroupByDateInterval) {
      case 'SECOND':
        str = 'h:mm:ss a';
        break;
      case 'MINUTE':
        str = 'h:mm a';
        break;
      case 'HOUR':
        str = 'hA';
        break;
      case 'DAY':
        str = 'MMM D';
        break;
      case 'MONTH':
        str = 'MMM YYYY';
        break;
      case 'YEAR':
        str = 'YYYY';
        break;
      default:
        throw new Error(`Invalid waGroupByDateInterval ${waGroupByDateInterval}`);
      }
      return str;
    },

  },
  actions: {}
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default GroupByDateInterval;
