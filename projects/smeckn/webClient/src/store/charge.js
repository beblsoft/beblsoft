/**
 * @file Vuex Charge Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ CHARGE MODULE ---------------------------------- */
const ChargeModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * Return the amount formatted as a string in the correct currency
     * @param  {Object} state Vuex State Object
     * @param  {Number} amount Currency Amount
     * @param  {Number} bCurrencyCode Beblsoft Currency Code
     * @return {string} formatted currency
     */
    getFormattedCurrency: (state) => (amount, bCurrencyCode) => {
      let rval = amount;
      if (bCurrencyCode === 'USD') {
        rval = `$${(amount / 100).toFixed(2)}`;
      }
      return rval;
    }

  },
  actions: {

    /**
     * Create
     *
     * On Success:
     *   Creates charge
     *
     * On Failure:
     *   Throws Error
     */
    async create({ _ }, { cartJWT, waCreditCard }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waCharge = null;

      try {
        waCharge = await this.$g.server.api.charge.create({
          data: { cartJWT: cartJWT, stripeCardID: waCreditCard.id }
        });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waCharge;
    },

    /**
     * List
     *
     * On Success:
     *   Lists charges and updates waChargeList and waChargeNextCursor
     *
     * On Failure:
     *   Throws Error
     *
     * @param {Boolean} clear If true clear out existing waChargeList
     */
    async list({ _ }, { cursor = null, limit = 100 } = {}) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        }
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waChargeData = null;
      try {
        waChargeData = await this.$g.server.api.charge.list({ queryParams: { cursor, limit } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
      return waChargeData;
    },

  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default ChargeModule;
