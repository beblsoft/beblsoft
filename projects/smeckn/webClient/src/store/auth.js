/**
 * @file Vuex Auth Module
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import ErrorHandler from '../server/errorHandler';


/* ------------------------ AUTH MODULE ------------------------------------ */
const AuthModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {},
  actions: {

    /**
     * Login
     *
     * On Success:
     *   Sends login request to server, server sets authentication cookies
     *   Set account state in store
     *   Routes appropriately
     *
     * On Failure:
     *   Rejects with appropriate error message
     *
     */
    async login({ _ }, {
      email,
      password,
      reCAPTCHAToken,
      browserRememberCookies = true,
      authTokenExpDays = 14,
      accountTokenExpDays = 14,
    } = ({}, {})) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { throw new Error(apiError.serverMessage); },
        AuthError: (apiError) => { throw new Error(apiError.serverMessage); },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });
      let waLoginData = null;

      try {
        waLoginData = await this.$g.server.api.auth.login({
          data: {
            email,
            password,
            reCAPTCHAToken,
            browserRememberCookies,
            authTokenExpDays,
            accountTokenExpDays,
          }
        });
        await this.$g.store.dispatch('account/set', { waAccount: waLoginData.account });
        this.$g.router.replace({ name: 'rnProfileGroupsDashboard' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Try Login
     *
     * Does the following:
     *   Get authentication status
     *   If "LOGGED_IN":
     *     Correct cookies are available, set account state, and navigate appropriately
     *   If "LOGGED_OUT":
     *     Correct cookies are NOT available, do nothing
     */
    async tryLogin({ _ }) {
      let errorHandler = new ErrorHandler({ $g: this.$g });

      try {
        let waAuthStatus = await this.$g.server.api.auth.getStatus();
        if (waAuthStatus.status === 'LOGGED_IN') {
          await this.$g.store.dispatch('account/set', { waAccount: waAuthStatus.account });
          this.$g.router.replace({ name: 'rnProfileGroupsDashboard' });
        }
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Logout
     *
     * On Success:
     *   Clears account state, send logout message to server so it can clear cookies
     *   Routes appropriately
     */
    async logout({ _ }, {
      whoops = false,
      alertMsg = null
    } = ({}, {})) {

      try {
        // Route to Banner First. Without doing so, clearing the account causes many
        // potentially active components to throw errors.
        await this.$g.router.replace({ name: 'rnBanner', params: { tryLoginOnMount: false } });
        await this.$g.store.dispatch('account/clear');
        await this.$g.server.api.auth.logout();
      } catch (apiError) { /* Ignore errors here so we don't overwhelm user if server can't be reached */ }

      // Alert message
      if (alertMsg !== null) {
        this.$g.alertBus.show({ text: alertMsg });
      } else if (whoops) {
        this.$g.alertBus.show({ text: this.$g.store.state.message.messages.WHOOPS_SOMETHING_WENT_WRONG });
      }
    },

    /**
     * Confirm Account
     *
     * On Success:
     *   Routes to rnLogin
     */
    async confirmAccount({ _ }, { createAccountToken }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          this.$g.store.dispatch('auth/logout', { alertMsg: apiError.serverMessage });
        },
        AuthError: (apiError) => {
          this.$g.store.dispatch('auth/logout', { alertMsg: apiError.serverMessage });
        },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.auth.confirmAccount({ data: { createAccountToken } });
        this.$g.alertBus.show({ text: this.$g.store.state.message.messages.ACCOUNT_SUCCESSFULLY_CREATED, severity: 'success' });
        this.$g.router.push({ name: 'rnLogin' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Create Account
     *
     * On Success:
     *   Routes to CreateAccountEmail
     *
     * On Failure:
     *   Throws error with message for user
     */
    async createAccount({ _ }, {
      email,
      password,
      reCAPTCHAToken
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        },
        AuthError: (apiError) => { /* eslint-disable-line */
          throw new Error(apiError.serverMessage);
        },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.auth.createAccount({
          data: { email, password, reCAPTCHAToken },
        });
        this.$g.router.push({ name: 'rnCreateAccountEmail', params: { email } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Forgot Password
     *
     * On Success:
     *   Routes to ForgotPasswordEmail
     *
     * On Failure:
     *   Throws error with message for user
     */
    async forgotPassword({ _ }, {
      email,
      reCAPTCHAToken
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
        AuthError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
        NotFoundError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.auth.forgotPassword({
          data: { email, reCAPTCHAToken },
        });
        this.$g.router.push({ name: 'rnForgotPasswordEmail', params: { email } });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    },

    /**
     * Reset Password
     *
     * On Success:
     *   Updates account password
     *   Routes to Login In
     *
     * On Failure:
     *   Throws error with message for user
     *   Or navigates appropriately
     */
    async resetPassword({ _ }, {
      forgotPasswordToken,
      password
    }) {
      let overrideErrorHandlers = {
        BadRequestError: (apiError) => {
          throw new Error(apiError.serverMessage);
        },
        AuthError: (apiError) => {
          // Bad token, have user try again
          this.$g.alertBus.show({ text: this.$g.store.state.message.messages.PASSWORD_RESET_TRY_AGAIN });
          this.$g.router.push({ name: 'rnForgotPassword' });
        },
      };
      let errorHandler = new ErrorHandler({ $g: this.$g, overrideErrorHandlers });

      try {
        await this.$g.server.api.auth.resetPassword({ data: { forgotPasswordToken, password } });
        this.$g.alertBus.show({ text: this.$g.store.state.message.messages.PASSWORD_SUCCESSFULLY_UPDATED, severity: 'success' });
        this.$g.router.push({ name: 'rnLogin' });
      } catch (apiError) {
        errorHandler.handle({ apiError });
      }
    }
  } /* End actions */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default AuthModule;
