/**
 * @file Vuex Analysis Type Module
 */

/* ------------------------ ANALYSIS TYPE MODULE --------------------------- */
const AnalysisTypeModule = {
  namespaced: true,
  state: {},
  mutations: {},
  getters: {

    /**
     * Return string version of analysis type
     * @param {String} waAnalysisType WebAPI analysis type
     * @return {String}
     */
    analysisTypeToString: (state) => (waAnalysisType) => {
      let str = '';
      switch (waAnalysisType) {
      case 'COMPREHEND_LANGUAGE':
        str = 'Language';
        break;
      case 'COMPREHEND_SENTIMENT':
        str = 'Sentiment';
        break;
      case 'COMPREHEND_SYNTAX':
        str = 'Syntax';
        break;
      case 'REKOGNITION_FACES':
        str = 'Faces';
        break;
      case 'REKOGNITION_ENTITIES':
        str = 'Entities';
        break;
      case 'REKOGNITION_MODERATION_LABELS':
        str = 'Moderation Labels';
        break;
      default:
        throw new Error(`Invalid waAnalysisType ${waAnalysisType}`);
      }
      return str;
    }

  },
  actions: {} /* End action */
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default AnalysisTypeModule;
