/**
 * @file Vuex Store Functionality
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import Vuex from 'vuex';
import ConstantModule from './constant';
import MessageModule from './message';
import AuthModule from './auth';
import AccountModule from './account';
import UnitModule from './unit';
import CreditCardModule from './creditCard';
import CartModule from './cart';
import ChargeModule from './charge';
import ProfileGroupModule from './profileGroup';
import ProfileModule from './profile';
import FacebookProfileModule from './facebookProfile';
import FacebookPostModule from './facebookPost';
import SyncModule from './sync';
import AnalysisModule from './analysis';
import AnalysisTypeModule from './analysisType';
import StatisticModule from './statistic';
import HistogramModule from './histogram';
import GroupByDateIntervalModule from './groupByDateInterval';
import ContentTypeModule from './contentType';
import SortModule from './sort';
import TextModule from './text';
import FeatureModule from './feature';


/* ------------------------ STORE ------------------------------------------ */
export default class Store {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    Vue.use(Vuex);
    this.store = new Vuex.Store({
      modules: {
        constant: ConstantModule,
        message: MessageModule,
        auth: AuthModule,
        account: AccountModule,
        unit: UnitModule,
        creditCard: CreditCardModule,
        cart: CartModule,
        charge: ChargeModule,
        profileGroup: ProfileGroupModule,
        profile: ProfileModule,
        facebookProfile: FacebookProfileModule,
        facebookPost: FacebookPostModule,
        sync: SyncModule,
        analysis: AnalysisModule,
        analysisType: AnalysisTypeModule,
        statistic: StatisticModule,
        histogram: HistogramModule,
        groupByDateInterval: GroupByDateIntervalModule,
        contentType: ContentTypeModule,
        sort: SortModule,
        text: TextModule,
        feature: FeatureModule,
      },
      strict: process.env.VUE_APP_ENVIRONMENT !== 'Production'
    });
    this.store.$g = $g;


    /* -------------------- HOT MODULE REPLACEMENT ------------------------- */
    // Recipe Taken From Here: https://vuex.vuejs.org/guide/hot-reload.html
    if (module.hot) {
      module.hot.accept([
        './constant',
        './message',
        './auth',
        './account',
        './unit',
        './creditCard',
        './cart',
        './charge',
        './profileGroup',
        './profile',
        './facebookProfile',
        './facebookPost',
        './sync',
        './analysis',
        './analysisType',
        './statistic',
        './histogram',
        './groupByDateInterval',
        './contentType',
        './sort',
        './text',
        './feature',
      ], () => {
        const NewConstantModule = require('./constant').default; /* eslint-disable-line */
        const NewMessageModule = require('./message').default; /* eslint-disable-line */
        const NewAuthModule = require('./auth').default; /* eslint-disable-line */
        const NewAccountModule = require('./account').default; /* eslint-disable-line */
        const NewUnitModule = require('./unit').default; /* eslint-disable-line */
        const NewCreditCardModule = require('./creditCard').default; /* eslint-disable-line */
        const NewCartModule = require('./cart').default; /* eslint-disable-line */
        const NewChargeModule = require('./charge').default; /* eslint-disable-line */
        const NewProfileGroupModule = require('./profileGroup').default; /* eslint-disable-line */
        const NewProfileModule = require('./profile').default; /* eslint-disable-line */
        const NewFacebookProfileModule = require('./facebookProfile').default; /* eslint-disable-line */
        const NewFacebookPostModule = require('./facebookPost').default; /* eslint-disable-line */
        const NewSyncModule = require('./sync').default; /* eslint-disable-line */
        const NewAnalysisModule = require('./analysis').default; /* eslint-disable-line */
        const NewAnalysisTypeModule = require('./analysisType').default; /* eslint-disable-line */
        const NewStatisticModule = require('./statistic').default; /* eslint-disable-line */
        const NewHistogramModule = require('./histogram').default; /* eslint-disable-line */
        const NewGroupByDateIntervalModule = require('./groupByDateInterval').default /* eslint-disable-line */
        const NewContentTypeModule = require('./contentType').default; /* eslint-disable-line */
        const NewSortModule = require('./sort').default; /* eslint-disable-line */
        const NewTextModule = require('./text').default; /* eslint-disable-line */
        const NewFeatureModule = require('./feature').default; /* eslint-disable-line */

        this.store.hotUpdate({
          modules: {
            constant: NewConstantModule,
            message: NewMessageModule,
            auth: NewAuthModule,
            account: NewAccountModule,
            unit: NewUnitModule,
            creditCard: NewCreditCardModule,
            cart: NewCartModule,
            charge: NewChargeModule,
            profileGroup: NewProfileGroupModule,
            profile: NewProfileModule,
            facebookProfile: NewFacebookProfileModule,
            facebookPost: NewFacebookPostModule,
            sync: NewSyncModule,
            analysis: NewAnalysisModule,
            analysisType: NewAnalysisTypeModule,
            statistic: NewStatisticModule,
            histogram: NewHistogramModule,
            groupByDateInterval: NewGroupByDateIntervalModule,
            contentType: NewContentTypeModule,
            sort: NewSortModule,
            text: NewTextModule,
            feature: NewFeatureModule,
          }
        });
      });
    }

  }
}
