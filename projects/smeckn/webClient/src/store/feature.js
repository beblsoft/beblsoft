/**
 * @file Vuex Feature Module
 *
 * Implements feature toggling for the Web Client
 */


/* ------------------------ FEATURE MODULE --------------------------------- */
const FeatureModule = {
  namespaced: true,
  state: {
    // If true, display photo analysis units
    photoAnalysisUnits: false,
  },
  mutations: {},
  getters: {},
  actions: {}
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default FeatureModule;
