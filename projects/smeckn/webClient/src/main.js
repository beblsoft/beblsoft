/**
 * @file Application Main Entry Point
 * @description Binding for all application components, resources, and initialization
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import App from './App';
import Vue from 'vue';
import Cypress from './cypress'; // Fonts, charts, bootstrap vue
import UI from './ui'; // Fonts, charts, bootstrap vue
import ChartJS from './chartjs';
import UserGuide from './userGuide/index.js';
import SmecknSentry from './sentry';
import Utils from './utils.js';
import Store from './store'; // Contained Store Initialization, must be before router initialization
import Router from './router';
import Server from './server';
import AlertBus from './appComponents/alert/AlertBus.js';
import SyncBus from './appComponents/sync/SyncBus.js';
import AnalysisBus from './appComponents/analysis/AnalysisBus.js';
import Messages from './messages';
import GoogleAnalytics from './googleAnalytics';


/* ------------------------ GLOBAL CLASS ----------------------------------- */
/**
 * All global state goes here
 */
class Global {

  /**
   * @constructor
   */
  constructor() {
    // Process Env: Used by tests
    this.process = {};
    this.process.env = process.env;

    // Cypress
    this.cypress = new Cypress({ $g: this });

    // UI
    this.ui = new UI({ $g: this });

    // ChartJS
    this.chartjs = new ChartJS({ $g: this });

    // User Guide
    this.userGuide = new UserGuide({ $g: this });

    // Sentry
    this.sentry = new SmecknSentry({ $g: this });

    // Utils
    this.utils = new Utils({ $g: this });

    // Vuex Store: Must be before router
    let s = new Store({ $g: this });
    this.store = s.store;

    // Vue Router
    this.router = new Router({ $g: this });

    // Smeckn Server Interface
    this.server = new Server({ $g: this });

    // Alert Bus
    this.alertBus = new AlertBus();

    // Sync Bus
    this.syncBus = new SyncBus();

    // Analysis Bus
    this.analysisBus = new AnalysisBus();

    // Messages
    this.setMessages = new Messages({ $g: this });

    // Google Analytics
    this.googleAnalytics = new GoogleAnalytics({ $g: this });

    // App Backpointer
    this.app = null;
  }

  /**
   * Initialize that can be done asynchronously
   */
  async asyncInit() {
    await this.store.dispatch('constant/load');
  }
}

/* ------------------------ INITALIZATION ---------------------------------- */
let g = new Global();
g.asyncInit();
Vue.prototype.$g = g;

// Vue Instance
g.app = window.app = new Vue({ /* eslint-disable-line */
  store: g.store,
  router: g.router.router, // Actual Vue Router is embedded in wrapper
  render: (h) => { return h(App); }
}).$mount('#app');
