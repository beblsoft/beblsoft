/**
 * @file Sentry Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import * as SentryIntegrations from '@sentry/integrations';


/* ------------------------ SENTRY CLASS ----------------------------------- */
/**
 * Encapsulate sentry state
 */
export default class SmecknSentry {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;

    // Have Vue instance Log errors when they occur
    // If not present, Sentry will prevent errors from being logged
    Vue.config.errorHandler = (err, vm, info) => { console.error(err); };

    // Instantiate Sentry
    Sentry.init({
      dsn: process.env.VUE_APP_SENTRY_WEBCLIENT_DSN,
      release: process.env.VUE_APP_GIT_COMMIT_SHA,
      environment: process.env.VUE_APP_ENVIRONMENT,
      maxBreadcrumbs: 200,
      debug: false,
      sampleRate: 1.0,
      attachStacktrace: true,
      beforeSend: this.beforeSend,
      beforeBreadcrumb: this.beforeBreadcrumb,
      integrations: [
        new SentryIntegrations.Vue({ Vue, attachProps: true, }),
        new SentryIntegrations.ExtraErrorData()
      ],
    });
    this.setGlobalScope();
  }

  /* ------------------------ BEFORE HOOKS --------------------------------- */
  /**
   * Before sending event
   * @param  {Object} event Sentry event
   * @return {Object}       Revised event or null
   */
  beforeSend(event, hint) {
    let _event = event;
    _event.extra = _event.extra || {};

    // Assign enumerable error properties to extras
    // https://github.com/getsentry/sentry-javascript/issues/2210
    try {
      let originalException = hint.originalException;
      if (originalException !== null) {
        _event.extra.errorString = originalException.toString();
        _event.extra.errorStack = originalException.stack;
        let keys = Object.keys(originalException);
        if (keys.length) {
          _event.extra.errorProperties = {};
          keys.forEach((key) => { _event.extra.errorProperties[key] = originalException[key]; });
        }
      }
    } catch (error) {
      console.warn('SmecknSentry: Failed to assign enumerable error properties to extras', error);
    }

    // Don't send local events
    if (process.env.VUE_APP_ENVIRONMENT === 'Local') { _event = null; }

    return _event;
  }

  /**
   * Before each breadcrump
   * @param  {Object} breadcrumb Sentry breadcrumb
   * @param  {Object} hint       Sentry hint
   * @return {Object}            Revised breadcrumb or null
   */
  beforeBreadcrumb(breadcrumb, hint) {
    // console.log([breadcrumb, hint]);
    let rval = breadcrumb;
    if (process.env.VUE_APP_ENVIRONMENT === 'Local') {
      rval = null;
    }
    return rval;
  }

  /* ------------------------ CAPTURE -------------------------------------- */
  /**
   * Capture Message
   */
  captureMessage({ message } = {}) {
    Sentry.captureMessage(message);
  }

  /**
   * Capture Error Action
   */
  captureException({ err } = {}) {
    console.log('Exception!');
    console.log(err);
    Sentry.captureException(err);
  }

  /* ------------------------ SCOPE ---------------------------------------- */
  /**
   * Set global scope
   */
  setGlobalScope() {
    Sentry.configureScope((scope) => {
      scope.setTag('language', 'Vue');
      scope.setLevel('error');
      // scope.setFingerprint(['Fingerprint1']);
    });
  }

  /**
   * Set user scope
   */
  setUserScope({ aID, aEmail } = {}) {
    Sentry.configureScope((scope) => {
      scope.setUser({ email: aEmail, id: aID });
    });
  }

  /**
   * Clear user scope
   */
  clearUserScope() {
    Sentry.configureScope((scope) => { scope.setUser({}); });
  }

}
