/**
 * @file Messages Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import english from './english';


/* ------------------------------------------------------------------------- */
/**
 * Messages Class
 */
export default class Messages {

  /**
   * @constructor
   * @param  {Global} options.g
   * @param  {string} options.language
   */
  constructor({ $g = null, language = 'english' } = {}) {
    this.$g = $g;
    this.store = $g.store;

    this.languages = {
      english
    };

    this.assertLanguagesEqual();
    this.setLanguage({ language });

  }

  /**
   * Set the language used by the message infrastructure
   * @param {Object} options.language } [description]
   */
  setLanguage({ language } = {}) {
    this.store.commit('message/setMessages', this.languages[language]);
  }

  /**
   * Assert that languages have same message keys
   */
  assertLanguagesEqual() {
    let compKeyString = null;
    for (let language in self.languages) {
      if (self.languages.hasOwnProperty(language)) {
        let languageKeys = Object.keys(language).sort();
        let keyString = JSON.stringify(languageKeys);
        // Case 1: First language
        if (compKeyString === null) {
          compKeyString = keyString;
        // Case 2: Compare against first language
        } else if (compKeyString !== keyString) {
          throw new Error(`Message language keys mismatch!\n${compKeyString}\n${keyString}`);
        }
      }
    } /* End for */
  } /* End assertAllLanguagesEqual */

}
