/**
 * @file English Messages
 */

/* ------------------------ ENGLISH MESSAGES ------------------------------- */
const english = {
  SPECIFY_VALID_EMAIL: 'Please specify a valid email.',
  SPECIFY_VALID_PASSWORD: 'Please specify a valid password.',
  SPECIFY_VALID_PASSWORDS: 'Please specify two passwords that are the same.',
  SPECIFY_VALID_NEW_PASSWORDS: 'Please specify two new passwords that are the same.',
  SPECIFY_VALID_PROFILEGROUP_NAME: 'Please specify a profile group name.',
  SPECIFY_VALID_TEXT_TO_ANALYZE: 'Please specify text to analyze.',
  SPECIFY_UNITS_TO_PURCHASE: 'Please specify which units you would like to purchase',
  WHOOPS_SOMETHING_WENT_WRONG: 'Whoops, sorry! Something went wrong! Please try again later.',
  PASSWORD_RESET_TRY_AGAIN: 'Something went wrong resetting your password. Please try again.',
  ACCOUNT_SUCCESSFULLY_CREATED: 'Account successfully created!',
  PASSWORD_SUCCESSFULLY_UPDATED: 'Password successfully updated!',
  AUTH_ERROR_CREDENTIALS_EXPIRED: 'Your credentials have expired, please log in again.',
  TOO_MUCH_TEXT_ADDED: 'Too much text added, please decrease the number of characters.',
  LANGUAGE_NOT_SUPPORTED: 'This language is not currently supported for sentiment analysis.',
  DEFAULT_PROFILE_GROUP_NAME: 'DefaultProfileGroup',
  NO_MORE_TEXT_ANALYSIS_UNITS_ARE_AVAILABLE: 'No more text analysis units are available, please purchase more to continue.',
  NOT_ENOUGH_TEXT_ANALYSIS_UNITS_ARE_AVAILABLE: 'Not enough text analysis units are available to perform analysis, please purchase more to continue.',
  SPECIFY_A_CREDIT_CARD: 'Please specify a credit card to complete your purchase.',
  CLICK_REFUND_POLICY: 'You must agree to our refund policy before continuing.',
  ERROR_CONNECTING_TO_FACEBOOK: 'An error occurred when trying to reach Facebook, please try again later.',
  SUCCESSFULLY_UPDATED_FACEBOOK_PROFILE_CREDENTIALS: 'Facebook profile credentials have been successfully updated.',
  SYNC_FAILED_CONTENT_LIMIT_REACHED:'Profile sync failed to complete because the content limit was reached.',
  SYNC_FAILED_CREDENTIALS_EXPIRED:'Profile sync failed to complete because the profile credentials expired. Please refresh your profile credentials.',
  SYNC_FAILED_THIRDPARTY_API_FAILURE: 'Profile sync failed to complete because Smeckn cannot currently communicate with the relevant third party servers.',
  SYNC_FAILED_PROFILE_DELETED:'Profile sync failed to complete because the profile was deleted.',
  SYNC_FAILED_TIMED_OUT:'Profile sync failed to complete due to an internal Smeckn error.',
  SYNC_FAILED_UNKNOWN:'Profile sync failed to complete due to an internal Smeckn error.',
  ANALYSIS_FAILED_TIMED_OUT: 'because Smeckn\'s servers are currently too busy. Please try again later.',
  ANALYSIS_FAILED_UNKNOWN: 'because an internal Smeckn error occurred. Please try again later.',
  FACEBOOK_CANT_COMMUNICATE: 'Smeckn is experiencing difficulty communicating with Facebook\'s servers right now. Please try again later.',
};

/* ------------------------ IMPORTS ---------------------------------------- */
export default english;
