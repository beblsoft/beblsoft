/**
 * @file Router Routes
 *
 */
/* eslint space-in-parens: 0 */

/* ------------------------ ROUTES ----------------------------------------- */
/**
 * List of application routes
 * @type {Route}
 */
let routes = [

  /* ----------------- BASE APP ROUTES ------------------------------------- */
  /**
   * Route Requirements:
   *   N/A
   */
  {
    path: '/banner',
    name: 'rnBanner',
    component: () => import( /* webpackChunkName: "base" */ '../appComponents/base/Banner'),
    props: true,
    alias: '/'
  },

  /* ----------------- AUTHENTICATION ROUTES ------------------------------- */
  {
    path: '/login',
    name: 'rnLogin',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/Login'),
  },
  {
    path: '/createAccount',
    name: 'rnCreateAccount',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/CreateAccount'),
  },
  {
    path: '/createAccountEmail',
    name: 'rnCreateAccountEmail',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/CreateAccountEmail'),
    props: true
  },
  {
    path: '/confirmAccount',
    name: 'rnConfirmAccount',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/ConfirmAccount'),
    props: (route) => { return { createAccountToken: route.query.createAccountToken }; }
  },
  {
    path: '/forgotPassword',
    name: 'rnForgotPassword',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/ForgotPassword'),
  },
  {
    path: '/forgotPasswordEmail',
    name: 'rnForgotPasswordEmail',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/ForgotPasswordEmail'),
    props: true
  },
  {
    path: '/resetPassword',
    name: 'rnResetPassword',
    component: () => import( /* webpackChunkName: "auth" */ '../appComponents/auth/ResetPassword'),
    props: (route) => { return { forgotPasswordToken: route.query.forgotPasswordToken }; }
  },

  /* ----------------- ACCOUNT ROUTES -------------------------------------- */
  /**
   * Route Requirements:
   *   Logged In User
   */
  {
    path: '/accountManage',
    name: 'rnAccountManage',
    component: () => import( /* webpackChunkName: "account" */ '../appComponents/account/AccountManage'),
    meta: { requiresAuth: true }
  },

  /* ----------------- PAYMENT ROUTES -------------------------------------- */
  /**
   * Route Requirements:
   *   Logged In User
   */
  {
    path: '/unitBalance',
    name: 'rnUnitBalance',
    component: () => import( /* webpackChunkName: "payments" */ '../appComponents/payments/UnitBalance'),
    meta: { requiresAuth: true }
  },
  {
    path: '/paymentHistory',
    name: 'rnPaymentHistory',
    component: () => import( /* webpackChunkName: "payments" */ '../appComponents/payments/PaymentHistory'),
    meta: { requiresAuth: true }

  },
  {
    path: '/creditCards',
    name: 'rnCreditCards',
    component: () => import( /* webpackChunkName: "payments" */ '../appComponents/payments/CreditCards'),
    meta: { requiresAuth: true }

  },
  {
    path: '/unitCheckout',
    name: 'rnUnitCheckout',
    component: () => import( /* webpackChunkName: "payments" */ '../appComponents/payments/Checkout'),
    meta: { requiresAuth: true }

  },
  {
    path: '/paymentThankYou',
    name: 'rnPaymentThankYou',
    component: () => import( /* webpackChunkName: "payments" */ '../appComponents/payments/PaymentThankYou'),
    meta: { requiresAuth: true }

  },

  /* ----------------- ANALYSIS ROUTES ------------------------------------- */
  /**
   * Route Requirements:
   *   Logged In User
   */
  {
    path: '/customTextAnalysis',
    name: 'rnCustomTextAnalysis',
    component: () => import( /* webpackChunkName: "customAnalysis" */ '../appComponents/customAnalysis/CustomTextAnalysis'),
    meta: { requiresAuth: true }

  },

  /* ----------------- PROFILE GROUP ROUTES -------------------------------- */
  /**
   * Route Requirements:
   *   Logged In User
   */
  {
    path: '/profileGroupsDashboard',
    name: 'rnProfileGroupsDashboard',
    component: () => import( /* webpackChunkName: "profileGroup" */ '../appComponents/profileGroup/ProfileGroupsDashboard'),
    meta: { requiresAuth: true }

  },
  {
    path: '/profileGroupsManage',
    name: 'rnProfileGroupsManage',
    component: () => import( /* webpackChunkName: "profileGroup" */ '../appComponents/profileGroup/ProfileGroupsManage'),
    meta: { requiresAuth: true }

  },
  {
    path: '/profileGroupAdd',
    name: 'rnProfileGroupAdd',
    component: () => import( /* webpackChunkName: "profileGroup" */ '../appComponents/profileGroup/ProfileGroupAdd'),
    meta: { requiresAuth: true }

  },
  {
    path: '/profileGroupManage',
    name: 'rnProfileGroupManage',
    component: () => import( /* webpackChunkName: "profileGroup" */ '../appComponents/profileGroup/ProfileGroupManage'),
    props: true,
    meta: { requiresAuth: true }

  },

  /* ----------------- PROFILE GROUP DRILLDOWN ----------------------------- */
  /**
   * Route Requirements:
   *   Logged In User       - This is enforced by the router global setupBeforeEachGuard
   *   Active Profile Group - This is enforced in ProfileGroupDrillDown.vue
   */
  {
    path: '/profileGroupDrillDown/',
    name: 'rnProfileGroupDrillDown',
    component: () => import( /* webpackChunkName: "profileGroup" */ '../appComponents/profileGroup/ProfileGroupDrillDown'),
    meta: { requiresAuth: true },
    children: [{
        path: 'profileSelect',
        name: 'rnProfileSelect',
        component: () => import( /* webpackChunkName: "profile" */ '../appComponents/profile/ProfileSelect'),
      },
      {
        path: 'profileAdd',
        name: 'rnProfileAdd',
        component: () => import( /* webpackChunkName: "profile" */ '../appComponents/profile/ProfileAdd'),
      },

      /* ----------------- FACEBOOK PROFILE ROUTES ------------------------- */
      {
        path: 'facebook/',
        name: 'rnFacebookProfile',
        component: () => import( /* webpackChunkName: "profile" */ '../appComponents/profile/ProfileWrapper'),
        props: { waProfileType: 'FACEBOOK' },
        children: [{
            path: 'overview',
            name: 'rnFacebookProfileOverview',
            component: () => import( /* webpackChunkName: "facebookProfile" */ '../appComponents/facebookProfile/FacebookProfileOverview'),
          },
          {
            path: 'manage',
            name: 'rnFacebookProfileManage',
            component: () => import( /* webpackChunkName: "facebookProfile" */ '../appComponents/facebookProfile/FacebookProfileManage'),
          },
          {
            path: 'postAll',
            name: 'rnFacebookPostAll',
            component: () => import( /* webpackChunkName: "facebookPost" */ '../appComponents/facebookPost/FacebookPostAll'),
            props: true,
          },
          {
            path: 'postFrequency',
            name: 'rnFacebookPostFrequency',
            component: () => import( /* webpackChunkName: "facebookPost" */ '../appComponents/facebookPost/FacebookPostFrequency'),
          },
          {
            path: 'postLikes',
            name: 'rnFacebookPostLikes',
            component: () => import( /* webpackChunkName: "facebookPost" */ '../appComponents/facebookPost/FacebookPostLikes'),
          },
          {
            path: 'postComments',
            name: 'rnFacebookPostComments',
            component: () => import( /* webpackChunkName: "facebookPost" */ '../appComponents/facebookPost/FacebookPostComments'),
          },
          {
            path: 'postSentiment',
            name: 'rnFacebookPostSentiment',
            component: () => import( /* webpackChunkName: "facebookPost" */ '../appComponents/facebookPost/FacebookPostSentiment'),
          },
        ]
      }, /* End rnFacebookProfile */
    ]
  }, /* End rnProfileGroupDrillDown */

  /* ----------------- TEST ROUTES ----------------------------------------- */
  /**
   * Route Requirements: N/A
   * Test components in isolation
   * Note: all the components in this section are lazily loaded as to not add overhead to the app
   */
  {
    path: '/__internal_test__/',
    name: 'rnTest',
    component: () => import( /* webpackChunkName: "test" */ '../appComponents/base/Test'),
    meta: { requiresAuth: false },
    children: [{
      path: 'smecknChartMixinTest',
      name: 'rnSmecknChartMixinTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/chartMixin/SmecknChartMixinTest'),
    }, {
      path: 'smecknContentSectionTest',
      name: 'rnSmecknContentSectionTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/contentSection/SmecknContentSectionTest'),
    }, {
      path: 'smecknLogoCardTest',
      name: 'rnSmecknLogoCardTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/logoCard/SmecknLogoCardTest'),
    }, {
      path: 'smecknDateRangePickerTest',
      name: 'rnSmecknDateRangePickerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/dateRange/SmecknDateRangePickerTest.vue'),
    }, {
      path: 'smecknDropDownTransitionTest',
      name: 'rnSmecknDropDownTransitionTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/dropDownTransition/SmecknDropDownTransitionTest'),
    }, {
      path: 'smecknDropRightTransitionTest',
      name: 'rnSmecknDropRightTransitionTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/dropRightTransition/SmecknDropRightTransitionTest'),
    }, {
      path: 'smecknFadeTransitionTest',
      name: 'rnSmecknFadeTransitionTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/fadeTransition/SmecknFadeTransitionTest'),
    }, {
      path: 'smecknModalTest',
      name: 'rnSmecknModalTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/modal/SmecknModalTest'),
    }, {
      path: 'smecknNumberIncrementerTest',
      name: 'rnSmecknNumberIncrementerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/numberIncrementer/SmecknNumberIncrementerTest'),
    }, {
      path: 'smecknPlaceHolderTest',
      name: 'rnSmecknPlaceHolderTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/placeHolder/SmecknPlaceHolderComponentTest'),
    }, {
      path: 'smecknScrollContainerTest',
      name: 'rnSmecknScrollContainerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/scrollContainer/SmecknScrollContainerTest'),
    }, {
      path: 'smecknScrollMixinTest',
      name: 'rnSmecknScrollMixinTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/scrollMixin/SmecknScrollMixinTest'),
    }, {
      path: 'smecknSidebarContainerTest/',
      name: 'rnSmecknSidebarContainerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/sidebar/SmecknSidebarContainerTest'),
      children: [{
          path: 'home',
          name: 'rnSmecknSidebarContainerTestHome',
          component: () => import( /* webpackChunkName: "test" */ '../commonComponents/placeHolder/SmecknPlaceHolderComponent'),
          props: true,
          alias: ''
        },
        {
          path: 'disabled',
          name: 'rnSmecknSidebarContainerTestDisabled',
          component: () => import( /* webpackChunkName: "test" */ '../commonComponents/placeHolder/SmecknPlaceHolderComponent'),
          props: true,
        },
        {
          path: 'badges',
          name: 'rnSmecknSidebarContainerTestBadges',
          component: () => import( /* webpackChunkName: "test" */ '../commonComponents/placeHolder/SmecknPlaceHolderComponent'),
          props: true,
        },
        {
          path: 'level3',
          name: 'rnSmecknSidebarContainerTestLevel3',
          component: () => import( /* webpackChunkName: "test" */ '../commonComponents/placeHolder/SmecknPlaceHolderComponent'),
          props: true,
        },
      ]
    }, {
      path: 'smecknSortByDropDownTest',
      name: 'rnSmecknSortByDropDownTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/sortByDropDown/SmecknSortByDropDownTest.vue'),
    }, {
      path: 'smecknSpinnerTest',
      name: 'rnSmecknSpinnerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/spinner/SmecknSpinnerTest'),
    }, {
      path: 'smecknSplashContainerTest',
      name: 'rnSmecknSplashContainerTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/splashContainer/SmecknSplashContainerTest'),
    }, {
      path: 'smecknTextAnalysisTest',
      name: 'rnSmecknTextAnalysisTest',
      component: () => import( /* webpackChunkName: "test" */ '../commonComponents/textAnalysis/SmecknTextAnalysisTest'),
      meta: { requiresAuth: true }
    }]
  },

  /* ----------------- 404 CATCH ALL --------------------------------------- */
  {
    path: '*',
    name: 'rnComponent404',
    component: () => import( /* webpackChunkName: "base" */ '../appComponents/base/Component404'),
  },
];


/* ------------------------ EXPORTS ---------------------------------------- */
export default routes;
