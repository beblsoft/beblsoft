/**
 * @file Centralized router setup with all application routes
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';


/* ------------------------ ROUTER ----------------------------------------- */
/**
 * Encapsulate router logic
 * This wrapper class is needed so that the router guards can access $g
 */
export default class Router {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;

    Vue.use(VueRouter);
    this.router = new VueRouter({
      routes,
      base: `${process.env.VUE_APP_BASE_URL}/`,
      mode: 'history',
      scrollBehavior: function (to, from, savedPosition) {
        let rval = null;
        if (savedPosition) {
          rval = savedPosition;
        } else {
          rval = { x: 0, y: 0 };
        }
        return rval;
      }
    });
    this.setupBeforeEachGuard();
  }

  /* ---------------------- BEFORE EACH GUARD ----------------------------- */
  /**
   * Setup router before each guards
   * before has the following arguments:
   * @param  {Route} to - route being navigated to
   * @param  {Route} from - the current route being navigated away from
   * @param  {Function} next - resolves the next route
   */
  setupBeforeEachGuard() {
    this.router.beforeEach((to, from, next) => {
      // Auth Check
      let loggedIn = this.$g.store.getters['account/isWALoggedIn'];
      let requiresAuth = to.matched.some((record) => { return record.meta.requiresAuth; });
      if (requiresAuth && !loggedIn) {
        return next({ name: 'rnBanner' });
      }

      // All good here, continue you on your way!
      return next();
    });
  }

  /* ---------------------- NAVIGATION WRAPPERS ---------------------------- */
  /**
   * Push route
   * Enables navigation to go back to go back to current route
   */
  async push() {
    try {
      await this.router.push(...arguments);
    } catch (err) {
      this.handleRouterError(err);
    }
  }

  /**
   * Replace route
   * Prohibits navigation from going back to current route
   */
  async replace() {
    try {
      await this.router.replace(...arguments);
    } catch (err) {
      this.handleRouterError(err);
    }
  }

  /**
   * Go to route
   */
  async go(goArgs) {
    try {
      await this.router.go(...arguments);
    } catch (err) {
      this.handleRouterError(err);
    }
  }

  /**
   * Handler a router error
   * @param  {error} err Router error
   */
  handleRouterError(err) {
    if (!Boolean(err)) { /* eslint-disable-line */
      /* SMECKN-WEBCLIENT-3A: Error is undefined, null, or false */
    } else if (err.name === 'NavigationDuplicated') {
      /* Prevent needless NavigationDuplicated errors from going to sentry */
    } else {
      throw err;
    }
  }
}
