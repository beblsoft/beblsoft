/**
 * @file Global UI Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import _ from 'lodash';

/* Fonts */
import 'typeface-questrial';
import 'typeface-nunito';
import 'typeface-roboto';
import 'typeface-shadows-into-light';

/* UI Libs */
import BootstrapVue from 'bootstrap-vue';
import VueFormWizard from 'vue-form-wizard';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';
import registerCommonComponents from './commonComponents/index.js';
import SocialSharing from 'vue-social-sharing';

/* Smeckn SASS */
import './scss/index.scss';

/* ------------------------ UI CLASS --------------------------------------- */
/**
 * Encapsulate UI state
 */
export default class UI {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
    Vue.use(VueFormWizard);
    Vue.use(BootstrapVue);
    Vue.use(SocialSharing);
    registerCommonComponents();

    /**
     * Need colors in JavaScript for custom components that don't use SASS
     * These colors must match those defined in _bootstrapOverrides.scss!
     * @type {Object}
     */
    this.colors = {
      primary: '#428bca',
      success: '#5cb85c', // Positive Sentiment
      danger: '#d9534f', // Negative Sentiment
      info: '#17a2b8', // Neutral Sentiment
      secondary: '#989898', // Mixed Sentiment
      warning: '#ffc107',
      light: '#f9f9f9',
      dark: '#343a40',
      facebook: '#4267B2',
    };
  }
}
