/**
 * @file Utility Functionality
 */


/* ------------------------------------------------------------------------- */
/**
 * Encapsulate all Application Utilities
 */
export default class Utils {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
  }


  /**
   * Soft Assert a particular condition. Handled based on environment type
   *
   * @param  {[type]}   options.condition Condition to check [Should always be true]
   * @param  {[type]}   options.message   Error message thrown if condition fails in non production environment
   * @param  {Function} options.callback  Callback invoked if condition fails in production environment
   */
  softAssert({ condition = true, message = '', callback = () => {} } = {}) {
    if (!condition) {
      if (process.env.VUE_APP_ENVIRONMENT === 'Production') {
        callback(); /* eslint-disable-line */
      } else {
        throw new Error(message);
      }
    }
  }
}
