/**
 * @file Analysis Bus Implementation
 *
 * @description Facilatates communication between application and analysis bars
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';


/* ------------------------ ANALYSIS BUS CLASS ----------------------------- */
/**
 * Manage performing analysis
 */
export default class AnalysisBus {

  /**
   * @constructor
   */
  constructor() {
    this.bus = new Vue();
  }

  /**
   * Allows any component to trigger a profile analysis refresh
   *
   * Example component usage:
   *   this.$g.analysisBus.refresh();
   */
  refresh() {
    this.bus.$emit('refresh');
  }

  /**
   * Called when the active profile has an analysis complete
   */
  complete({ waContentType = '', waAnalysisType = '' } = {}) {
    this.bus.$emit('complete', waContentType, waAnalysisType);
  }
}
