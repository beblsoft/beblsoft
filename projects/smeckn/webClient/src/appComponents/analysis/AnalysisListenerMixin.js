/*
  NAME
    AnalysisListenerMixin.js

  DESCRIPTION
    Mixin Component
    Allows components to listen to when a analysis completes so they can reload
    their backing data.

    For example, FacebookPostSentiment component needs to reload its data after
    the facebook post sentiment analysis completes.

  USAGE IN COMPONENT
    export default {
      mixins: [AnalysisListenerMixin],
      methods: {
        onAnalysisComplete(waContentType, waAnalysisType) {...}
      }
    }
*/

/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /* ---------------------- LIFECYCLE -------------------------------------- */
  mounted() {
    this.$g.analysisBus.bus.$on('complete', this.onAnalysisComplete);
  },
  destroyed() {
    this.$g.analysisBus.bus.$off('complete', this.onAnalysisComplete);
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {

    /**
     * Handle Analysis Complete
     */
    onAnalysisComplete(waContentType, waAnalysisType) {
      this.$g.utils.softAssert({
        condition: false,
        message: 'onAnalysisComplete must be implemented!'
      });
    },
  }
};
