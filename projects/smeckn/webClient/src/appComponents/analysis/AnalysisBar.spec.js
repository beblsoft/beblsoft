/**
 * @file Analysis Bar Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('AnalysisBar', function () {

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * SetUp individual tests
   */
  beforeEach(function () {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      cy.then(() => { cy.spy(tg.$g.analysisBus, 'complete').as('analysisBusComplete'); });
    });
  });

  /**
   * Stub List Analysis
   */
  function stubAnalysisList({
    latestExists = true,
    smID = 20,
    profileSMID = 20,
    status = 'IN_PROGRESS',
    createDate = null,
    contentType = 'POST',
    analysisType = 'COMPREHEND_SENTIMENT',
    errorSeen = false,
    delayMS = 200
  } = {}) {

    let response = [];
    if (latestExists) {
      response = [{
        smID,
        profileSMID,
        createDate: createDate ? createDate : Cypress.moment.utc().subtract(2, 'seconds').toISOString(),
        contentType,
        analysisType,
        status: status,
        errorSeen
      }];
    }
    cy.server();
    cy.route({
      method: 'GET',
      url: '**/analysis*status*',
      response,
      delay: delayMS,
    }).as('listAnalysis');
  }

  /**
   * Stub Get Sync Progress
   */
  function stubAnalysisGetProgress({
    contentType = 'POST',
    analysisType = 'COMPREHEND_SENTIMENT',
    countAnalyzed = 398,
    countTotal = 400,
    status = 'IN_PROGRESS',
    delayMS = 200,
  } = {}) {
    cy.server();
    cy.route({
      method: 'GET',
      url: '**/analysis/*/progress',
      response: { contentType, analysisType, countAnalyzed, countTotal, status },
      delay: delayMS,
    }).as('getAnalysisProgress');
  }

  /**
   * Drill Down into a profile, and refresh analysis
   */
  function profileDrillDownRefreshAnalysis() {
    cy.then(() => {
      tg.$g.router.push({ name: 'rnFacebookProfileManage' });
      cy.url().should('include', 'facebook');
      cy.then(() => { tg.$g.analysisBus.refresh(); });
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('waProfile nulled out', function () {
    stubAnalysisList();
    stubAnalysisGetProgress({ status: 'TIMED_OUT' });
    profileDrillDownRefreshAnalysis();
    cy.get('[data-cy=analysisBarList]').should('be.visible');
    cy.then(() => { tg.$g.store.commit('profile/clearWAProfileList'); });
    cy.get('[data-cy=analysisBarList]').should('not.be.visible');
  });

  it('refresh: should list analyses', function () {
    stubAnalysisList();
    stubAnalysisGetProgress({ status: 'TIMED_OUT' });
    profileDrillDownRefreshAnalysis();
    cy.get('[data-cy=analysisBarList]').should('be.visible');
    cy.get('[data-cy=analysisBar]').should('be.visible');
    cy.get('[data-cy=analysisBarFailure]').should('be.visible');
  });

  it('in_progress: should continually poll in progress', function () {
    stubAnalysisList();
    stubAnalysisGetProgress();
    profileDrillDownRefreshAnalysis();
    let max = 10;
    for (let i = 0; i < max; i++) {
      stubAnalysisGetProgress({ countAnalyzed: i, countTotal: max });
      cy.get('[data-cy=analysisBarProgress]').should('contain', i / max * 100);
    }
    stubAnalysisGetProgress({ status: 'SUCCESS', countAnalyzed: max, countTotal: max });
    cy.get('[data-cy=analysisBarProgress]').should('not.be.visible');
    cy.get('[data-cy=analysisBarSuccess]').should('be.visible');
  });

  it('suceess: should show success for a few seconds', function () {
    stubAnalysisList();
    stubAnalysisGetProgress({ status: 'SUCCESS' });
    profileDrillDownRefreshAnalysis();
    cy.get('[data-cy=analysisBarSuccess]').should('be.visible');
    cy.get('[data-cy=analysisBarSuccess]').should('not.be.visible');
    cy.then(() => { let _ = expect(this.analysisBusComplete).to.be.called; });
  });

  it('failure: should show failure until x clicked', function () {
    stubAnalysisList();
    stubAnalysisGetProgress({ status: 'TIMED_OUT' });
    profileDrillDownRefreshAnalysis();
    cy.get('[data-cy=analysisBarFailure]').should('be.visible');
    cy.get('[data-cy=analysisErrorCloseButton]').click();
    cy.get('[data-cy=analysisBarFailure]').should('not.be.visible');
  });
});
