/**
 * @file Profile Wrapper Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Wrapper', function () {

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => {
      tg.$g.router.push({ name: 'rnFacebookProfileOverview' });
      cy.url().should('include', 'facebook');
      cy.get('[data-cy=facebookProfileID]').should('be.visible');
    });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=profileWrapper]').should('be.visible');
    }
  });

  it('Clear waProfileList', function () {
    cy.then(() => { tg.$g.store.commit('profile/clearWAProfileList'); });
    cy.url().should('include', 'profileAdd');
  });

});
