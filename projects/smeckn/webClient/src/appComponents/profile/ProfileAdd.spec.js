/**
 * @file Profile Add Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Add', function () {

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => {
      tg.$g.router.push({ name: 'rnProfileAdd' });
      cy.url().should('include', '/profileAdd');
    });
  });

  /**
   * Create Fake Profiles
   */
  function createFakeProfiles() {
    // Wait Hack: need to wait for ProfileAdd.vue to query profiles. However,
    // wait('@getProfileList') doesn't seem to work consistently
    cy.wait(2000);
    cy.then(() => {
      let fakeProfileList = [];
      let supportedWAProfileTypeList = tg.$g.store.getters['profile/supportedWAProfileTypeList'];
      for (let i = 0; i < supportedWAProfileTypeList.length; i++) {
        let waProfileType = supportedWAProfileTypeList[i];
        fakeProfileList.push({ smID: i, profileGroupID: i * i, type: waProfileType });
      }
      tg.$g.store.commit('profile/setWAProfileList', fakeProfileList);
    });
  }

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=profileAddCard]').should('be.visible');
    }
  });
  it('No profiles exist', function () {
    cy.get('[data-cy=profileTypeListSection]').should('be.visible');
    cy.get('[data-cy=noProfilesToAddPara]').should('not.be.visible');
  });
  it('All profiles already exist', function () {
    createFakeProfiles();
    cy.get('[data-cy=profileTypeListSection]').should('not.be.visible');
    cy.get('[data-cy=noProfilesToAddPara]').should('be.visible');
  });
});
