/**
 * @file Profile Interface
 */

/* ------------------------ PROFILE CLASS ---------------------------------- */
/**
 * Abstract Profile Class
 * Each Profile Class must implement its own
 */
class Profile {

  /**
   * Construct a profile
   * @param  {object} options.$g             Global
   * @param  {Object} options.waProfileGroup Active Web API Profile Group
   * @return {Profile}
   */
  constructor({ $g, waProfileGroup = null } = {}) {
    this.$g = $g;
    this.waProfileGroup = waProfileGroup;
  }

  /* ---------------------- PROPERTIES ------------------------------------- */
  /**
   * @returns {String} Web API Profile Type Ex. "FACEBOOK"
   */
  get waProfileType() {
    throw new Error('Not implemented!');
  }

  /**
   * @returns {String} Title to be displayed in application. Ex. "Facebook"
   */
  get title() {
    throw new Error('Not implemented!');
  }

  /**
   * @returns {Object} Icon source object
   */
  get iconSrc() {
    throw new Error('Not implemented!');
  }

  /**
   * @returns {String} Hex Color String to Color text Ex. "0x234908"
   */
  get textColor() {
    throw new Error('Not implemented!');
  }

  /**
   * @returns {Object} Attribute fields to add to HTML
   */
  get attributeObj() {
    return {};
  }

  /**
   * @returns {Object} Return sidebar item defined in SmecknSidebarContainer
   */
  get item() {
    throw new Error('Not implemented!');
  }

  /* ---------------------- METHODS ---------------------------------------- */
  /**
   * Function does the work of adding the profile
   *
   * On Success:
   *   Function creates profile on backend, updates store profile list, and navigates
   *   to profile
   *
   * On Neutral:
   *   I.e. users exits out of login dialog
   *   Function simply returns
   *
   * On Failure:
   *   Function throws error with reason which is then displayed to user
   *
   * @returns N/A
   */
  async onAdd() {
    throw new Error('Not Implemented!');
  }

  /**
   * Function that is invoked when the AddProfile page is mounted
   *
   * Background:
   *   To enable multiple Facebook profiles in different profile groups
   *   we needed to log the current Facebook user out before prompting a new
   *   user to login.
   *   When the user clicks the facebook icon to add a Facebook profile, we
   *   CANNOT logout the existing loggedin user as the first thing we do in
   *   response to the mouse click. This is because browsers will then block the
   *   async login request dialog as a popup. Therefore, we added this function
   *   allowing Profiles to do arbitrary setup work before they are added.
   *
   * @returns N/A
   */
  async onAddProfileMounted() {}

  /**
   * Function handles profile selection in the ProfileSelect component
   *
   * On Success:
   *   Function navigates to selected profile
   *
   * On Failure:
   *   Function throws error with reason which is then displayed to user
   *
   * @returns N/A
   */
  async onSelect() {
    throw new Error('Not Implemented!');
  }


}


/* ------------------------ EXPORT ----------------------------------------- */
export default Profile;
