/*
  NAME
    ProfileListMixin.js

  DESCRIPTION
    Mixin class for components needing access to all Profiles in a list

  USAGE
    See ProfileAdd.vue
*/

/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';
import Vuex from 'vuex';
import FacebookProfile from '../facebookProfile/FacebookProfile.js';


/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /* ---------------------- DATA ------------------------------------------- */
  data() {
    return {
      // List of supported profiles
      profileList: []
    };
  },

  /* ---------------------- COMPUTED --------------------------------------- */
  computed: {
    ...Vuex.mapState('profile', ['waProfileList']),
    ...Vuex.mapGetters('profile', ['supportedWAProfileTypeList']),

    /**
     * @return {Array[Profile]} List of profiles that have been added to profile group
     */
    profileAddedList() {
      return this.profileList.filter((profile) => {
        let matchingWAProfile = _.find(this.waProfileList, (waProfile) => { return profile.waProfileType === waProfile.type; });
        return matchingWAProfile !== undefined;
      });
    },

    /**
     * @return {Array[Profile]} List of profiles that have NOT been added yet to profile group
     */
    profileNotAddedList() {
      return this.profileList.filter((profile) => {
        let matchingWAProfile = _.find(this.waProfileList, (waProfile) => { return profile.waProfileType === waProfile.type; });
        return matchingWAProfile === undefined;
      });
    },
  },

  /* ---------------------- LIFECYCLE -------------------------------------- */
  /**
   * Instantiate Profiles Objects
   */
  mounted() {
    this.profileList = [
      new FacebookProfile({ $g: this.$g, waProfileGroup: this.waProfileGroup })
    ];
    this.$g.utils.softAssert({
      condition: this.profileList.length === this.supportedWAProfileTypeList.length,
      message: 'Profile List does not match supported profile list'
    });
  },

  /**
   * Clean up Profiles Objects
   */
  beforeDestroy() {
    for (let i = 0; i < this.profileList.length; i++) { delete this.profileList[i]; }
    this.profileList = [];
  },
};
