/**
 * @file Profile Select Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ GENERIC TESTS ---------------------------------- */
describe('Profile Select', function () {

  /**
   * SetUp Tests
   * @param {Boolean} options.profiles If True, create profiles
   */
  function setUp({ profiles = false } = {}) {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: profiles,
        syncsForAllProfilesSuccess: profiles
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => { tg.$g.router.push({ name: 'rnProfileSelect' }); });
  }

  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=profileSelectCard]').should('be.visible');
    }
  });
  it('No profiles exist', function () {
    setUp();
    cy.get('[data-cy=profileTypeListSection]').should('not.be.visible');
    cy.get('[data-cy=noProfilesAvailablePara]').should('be.visible');
  });
  it('One profile exists', function () {
    setUp({ profiles: true });
    cy.get('[data-cy=profileTypeListSection]').should('be.visible');
    cy.get('[data-cy=noProfilesAvailablePara]').should('not.be.visible');
  });
});


/* ------------------------ FACEBOOK --------------------------------------- */
describe('Profile Select: Facebook', function () {
  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => { tg.$g.router.push({ name: 'rnProfileSelect' }); });
  });

  it('Navigation', function() {
    cy.get('[data-cy=facebookCard]').click();
    cy.url().should('include', 'facebook');
  });
});
