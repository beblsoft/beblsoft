/**
 * @file Checkout Review Order Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import CheckoutTest from './CheckoutTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Checkout Review Order', function () {
  beforeEach(function () {
    CheckoutTest.setUp({ testObj: this, createNCards: 1, tab4: true });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=chargesSection]').should('be.visible');
      cy.get('[data-cy=totalCostSection]').should('be.visible');
      cy.get('[data-cy=creditCardSection]').should('be.visible');
      cy.get('[data-cy=refundPolicySection]').should('be.visible');
      cy.get('[data-cy=refundPolicyLink]').should('be.visible');
    }
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  /**
   * Test filling out and wizard
   */
  function testForm({
    clickRefundPolicy = true,
    clickNext = true,
    errorMsg = '',
  } = {}) {
    // Add Units
    if (clickRefundPolicy) {
      cy.get('[data-cy=refundPolicyCheckbox]').check({ force: true });
    }
    // Click Next
    if (clickNext) { CheckoutTest.clickNext(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  it('Should go back a tab', function () {
    CheckoutTest.clickBack();
    CheckoutTest.verifyTab({ number: 3 });
  });

  it('Should fail form submission because refund policy not checked', function () {
    testForm({
      clickRefundPolicy: false,
      errorMsg: tg.$g.store.state.message.messages.CLICK_REFUND_POLICY
    });
  });

  it('Should successfully create charge', function () {
    testForm({});
    cy.url().should('include', 'paymentThankYou');
  });

});
