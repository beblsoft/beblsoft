/**
 * @file Payment Thank You Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Payment Thank You', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => {
      await tg.$g.router.push({ name: 'rnPaymentThankYou' });
    });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('contain', 'Thank You!');
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Should navigate to unit balance', function () {
    cy.get('[data-cy=analysisBalanceLink]').click();
    cy.url().should('contain', 'unitBalance');
  });

});
