/**
 * @file Checkout Select Payment Method Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import CheckoutTest from './CheckoutTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Checkout Select Payment Method', function () {

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    CheckoutTest.setUp({ testObj: this, createNCards: 3, tab3: true });
    cy.then(() => {
      for (let i = 0; i < tg.viewports.length; i++) {
        let viewport = tg.viewports[i];
        cy.viewport(viewport.width, viewport.height);

        // Visibility
        cy.get('[data-cy=selectCardSection]').should('be.visible');
        cy.get('[data-cy=addNewCardSection]').should('be.visible');
        cy.get('[data-cy=defaultCreditCardBadge]').should('be.visible');
        cy.get('[data-cy=selectedCreditCardBadge]').should('be.visible');
        for (let j = 0; j < this.waCreditCardList.length; j++) {
          cy.get('[data-cy=creditCardList]').should('contain', this.waCreditCardList[j].last4);
        }
      }
    });
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  /**
   * Test filling out and wizard
   */
  function testForm({
    clickNext = true,
    errorMsg = '',
  } = {}) {
    // Click Next
    if (clickNext) { CheckoutTest.clickNext(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  it('Back', function () {
    CheckoutTest.setUp({ testObj: this, tab3: true });
    CheckoutTest.clickBack();
    CheckoutTest.verifyTab({ number: 2 });
  });

  it('No Cards', function () {
    CheckoutTest.setUp({ testObj: this, tab3: true });
    testForm({ errorMsg: tg.$g.store.state.message.messages.SPECIFY_A_CREDIT_CARD });
  });

  it('Selecting Different Cards', function () {
    CheckoutTest.setUp({ testObj: this, createNCards: 4, tab3: true });
    cy.get('[data-cy=creditCardListItem]').each(($elem) => {
      cy.wrap($elem).click();
      cy.wrap($elem).should('contain', 'Selected');
    });
  });

  it('Adding Max Cards', function () {
    CheckoutTest.setUp({ testObj: this, createNCards: 5, tab3: true });
    cy.get('[data-cy=addNewCardSection]').should('not.be.visible');
  });

  it('Next Success', function () {
    CheckoutTest.setUp({ testObj: this, createNCards: 1, tab3: true });
    cy.get('[data-cy=selectedCreditCardBadge]').should('be.visible');
    CheckoutTest.clickNext();
    CheckoutTest.verifyTab({ number: 4 });
  });

});
