/**
 * @file Checkout Verify Cost Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import CheckoutTest from './CheckoutTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Checkout Verify Cost', function () {
  beforeEach(function () {
    CheckoutTest.setUp({ testObj: this, tab2: true });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=chargeContentSection]').should('contain', 'Charges');
      cy.get('[data-cy=totalCostContentSection]').should('contain', 'Total Cost');
    }
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  it('Back', function () {
    CheckoutTest.clickBack();
    CheckoutTest.verifyTab({ number: 1 });
  });

  it('Next Success', function () {
    CheckoutTest.clickNext();
    CheckoutTest.verifyTab({ number: 3 });
  });

});
