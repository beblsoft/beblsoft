/**
 * @file Add Credit Card Tests
 *
 * Reference
 *   Testing Stripe: https://stripe.com/docs/testing
 *
 * Note
 *   Unfortunately, Cypress can't enter data into iFrames. The open issue is
 *   found here:https://github.com/cypress-io/cypress/issues/136
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';

/* ------------------------ TESTS ------------------------------------------ */
describe('Add Credit Card', function () {

  beforeEach(function() {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => { tg.$g.router.push({ name: 'rnCreditCards' }); });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=addNewCardSection]').should('be.visible');
      cy.get('[data-cy=addCardButton]').should('be.visible');
    }
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  it('Empty Form', function () {
    cy.get('[data-cy=addCardButton]').click();
    cy.get('[data-cy=errorMessagePara]').should('be.visible');
  });
});
