/**
 * @file Unit Balance Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Unit Balance', function () {
  beforeEach(function () {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        unitLedger: true,
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.server();
    cy.route('*/unit/balance').as('getBalance');
    cy.then(async () => {
      await tg.$g.router.push({ name: 'rnUnitBalance' });
      cy.wait('@getBalance');
    });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknCardTitle]').should('be.visible').should('contain', 'Analysis Unit Balance');
      cy.get('[data-cy=balanceTable]').should('be.visible');
      cy.get('[data-cy=unitTypeColumn]').should('be.visible');
      cy.get('[data-cy=nAvailableColumn]').should('be.visible');
    }
  });

  /* ---------------------- TABLE ------------------------------------------ */
  it('Verify Table', function () {
    cy.get('[data-cy=nAvailableColumn]').each(($td) => {
      assert.isAbove(parseFloat($td.html()), 0);
    });
  });

});
