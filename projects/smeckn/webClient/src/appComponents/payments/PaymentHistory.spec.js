/**
 * @file Payment History Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Payment History', function () {


  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  // Global variable used to ensure that all stubbed charges have unique ID
  let dataIndex = 0;

  /**
   * Stub GET Charges
   */
  function stubCharges({ nChargesPerPacket = 100, nextCursor = '' }) {
    cy.server();
    cy.route({
      method: 'GET',
      url: '*/charge*',
      response: (routeData) => {
        let data = [];
        for (let i = 0; i < nChargesPerPacket; i++) {
          data.push({
            id: `ch_1Db9dpECEJ911IPj8vKWXCMv${dataIndex++}`,
            createdDate: '2019-07-02T11:12:41.466Z',
            description: '1000 Text Analysis Units, 500 Photo Analysis Units',
            bCurrencyCode: 'USD',
            amount: 500
          });
        }
        return { nextCursor, data };
      } /* End Response */
    }).as('getCharges');
  }

  /**
   * Setup Test state
   */
  function setUp({ testObj = null } = {}) {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { testObj.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => { tg.$g.router.push({ name: 'rnPaymentHistory' }); });
  }

  /**
   * Get Number of rows in the table
   * @return {Object} Cypress Closure that resolve length of taqble
   */
  function getNTableRows() {
    return cy.get('td[data-cy=createDateColumn]').then(($tdList) => { return $tdList.length; });
  }

  /* ---------------------- TEST ------------------------------------------- */
  it('Viewports', function () {
    setUp({ testObj: this });
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('contain', 'Payment History');
      cy.get('[data-cy=smecknContentSection]').should('contain', 'Payments');
      cy.get('[data-cy=errorMessagePara]').should('not.be.visible');
    }
  });

  it('No Charges', function () {
    setUp({ testObj: this });
    cy.get('[data-cy=noChargesPara]').should('be.visible');
  });

  it('Should Paginate', function () {
    let nScrolls = 10;
    let nChargesPerPacket = 100;
    let nextCursor = 'this.will.cause.unlimited.pagination';

    stubCharges({ nChargesPerPacket, nextCursor });
    setUp({ testObj: this });
    cy.wait('@getCharges');
    for (let i = 0; i < nScrolls; i++) {
      stubCharges({ nChargesPerPacket, nextCursor });
      let minNCharges = nChargesPerPacket * (i + 2) - 1;
      cy.get('[data-cy=smecknScrollContainer]').scrollTo('bottom');
      cy.wait('@getCharges');
      getNTableRows().should('be.greaterThan', minNCharges);
    }
  });

  it('Should not paginate', function () {
    let nChargesPerPacket = 100;

    stubCharges({ nChargesPerPacket, nextCursor: '' });
    setUp({ testObj: this });
    cy.wait('@getCharges');
    getNTableRows().then((lengthOld) => {
      cy.get('[data-cy=smecknScrollContainer]').scrollTo('bottom');
      getNTableRows().should('be.equal', lengthOld);
    });
  });
});
