/**
 * @file Credit Card Tests
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Credit Cards', function () {

  /* ---------------------- SETUP ------------------------------------------ */
  /**
   * Setup Test
   * @param {Number} options.testObj
   * @param {Number} options.nCreditCards
   */
  function setUp({ testObj = null, nCreditCards = 0 } = {}) {
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { testObj.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();

    // Create Cards and navigate
    cy.then(async () => {
      for (let i = 0; i < nCreditCards; i++) {
        await tg.$g.store.dispatch('creditCard/create', { token: 'tok_visa' });
      }
    });
    cy.then(() => {
      cy.server();
      cy.route('*/creditCard').as('getCreditCard');
    });
    cy.then(async () => { tg.$g.router.push({ name: 'rnCreditCards' }); });
    cy.wait('@getCreditCard').then((xhr) => { cy.wrap(xhr.response.body).as('waCreditCardList'); });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    setUp({ testObj: this, nCreditCards: 1 });
    cy.then(() => {
      for (let i = 0; i < tg.viewports.length; i++) {
        let viewport = tg.viewports[i];
        cy.viewport(viewport.width, viewport.height);
        cy.get('[data-cy=availableCardsSection]').should('be.visible');
        cy.get('[data-cy=addNewCardSection]').should('be.visible');
      }
    });
  });

  it('No Cards', function () {
    setUp({ testObj: this, nCreditCards: 0 });
    cy.get('[data-cy=noCardsAdded]').should('be.visible');
  });

  it('Set Default Card', function () {
    setUp({ testObj: this, nCreditCards: 4 });
    cy.get('[data-cy=creditCardListItem]').each(($item) => {
      let $defaultButton = $item.find('[data-cy=setDefaultButton]');
      if ($defaultButton) {
        cy.wrap($defaultButton).click();
        cy.wrap($item).within(() => {
          cy.get('[data-cy=defaultBadge]').should('be.visible');
        });
      }
    });
  });

  it('Delete', function () {
    setUp({ testObj: this, nCreditCards: 4 });
    cy.get('[data-cy=creditCardListItem]').each(($item) => {
      cy.wrap($item).within(() => {
        cy.get('[data-cy=deleteButton]').click();
      });
    });
    cy.get('[data-cy=creditCardListItem]').should('not.be.visible');
  });

  it('Max Cards Added', function () {
    setUp({ testObj: this, nCreditCards: 5 });
    cy.get('[data-cy=addNewCardSection]').should('not.be.visible');
  });

});
