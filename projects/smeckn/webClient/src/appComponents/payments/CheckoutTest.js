/**
 * @file Checkout Common Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ COMMON ----------------------------------------- */
/**
 * Common Checkout Test Methods
 * @type {Object}
 */
const CheckoutTest = {

  /**
   * SetUp Checkout Form at Desired Location
   * @param {boolean} options.testObj - Test object, useful for skipping tests on production
   * @param {boolean} options.createCards
   * @param {boolean} options.tab1
   * @param {boolean} options.tab2
   * @param {boolean} options.tab3
   * @param {boolean} options.tab4
   */
  setUp: function ({
    testObj = null,
    createNCards = 0,
    tab1 = false,
    tab2 = false,
    tab3 = false,
    tab4 = false,
  } = {}) {
    // Usual Setup
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { testObj.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();

    // Checkout Setup
    cy.then(async () => {
      // Create Cards
      for (let i = 0; i < createNCards; i++) {
        cy.log(`Creating credit card ${i}`);
        let tokenArray = ['tok_visa', 'tok_mastercard', 'tok_amex', 'tok_discover', 'tok_diners'];
        let token = tokenArray[i % tokenArray.length];
        await tg.$g.store.dispatch('creditCard/create', { token });
      }

      // Listen On Routes
      cy.then(() => {
        cy.server();
        cy.route('*/unit/price').as('getPrice');
        cy.route('*/creditCard').as('getCreditCard');
        cy.route('*/creditCard/default').as('getDefaultCreditCard');
      });

      // 1. Setup Select Units
      if (tab1 || tab2 || tab3 || tab4) {
        cy.then(async () => { await tg.$g.router.push({ name: 'rnUnitCheckout' }); });
        cy.wait('@getPrice').then((xhr) => { cy.wrap(xhr.response.body).as('waUnitPriceList'); });
        cy.wait('@getCreditCard').then((xhr) => { cy.wrap(xhr.response.body).as('waCreditCardList'); });
        cy.wait('@getDefaultCreditCard');
        CheckoutTest.verifyTab({ number: 1 });
      }

      // 2. Setup Verify Cost
      if (tab2 || tab3 || tab4) {
        cy.get('[data-cy=smecknNumberIncrement]').click({ multiple: true });
        CheckoutTest.clickNext();
        CheckoutTest.verifyTab({ number: 2 });
      }

      // 3. Setup Select Payment Methods
      if (tab3 || tab4) {
        CheckoutTest.clickNext();
        CheckoutTest.verifyTab({ number: 3 });
      }

      // 4. Setup Review Order
      if (tab4) {
        cy.get('[data-cy=selectedCreditCardBadge]').should('be.visible');
        CheckoutTest.clickNext();
        CheckoutTest.verifyTab({ number: 4 });
      }
    });
  },

  /**
   * Click Next Button
   */
  clickNext: function () {
    cy.get('.wizard-footer-right > span > .wizard-btn').click();
  },

  /**
   * Click Back Button
   */
  clickBack: function () {
    cy.get('.wizard-footer-left > span > .wizard-btn').click();
  },

  /**
   * Verify on a particular tab of form wizard
   * @param  {number} options.number Tab index (1-based inded)
   */
  verifyTab: function ({ number = 1 } = {}) {
    switch (number) {
    case 1:
      { cy.get('[data-cy=selectUnitsTab]').should('be.visible'); break; }
    case 2:
      { cy.get('[data-cy=verifyCostTab]').should('be.visible'); break; }
    case 3:
      { cy.get('[data-cy=selectPaymentMethodTab]').should('be.visible'); break; }
    case 4:
      { cy.get('[data-cy=reviewOrderTab]').should('be.visible'); break; }
    default:
      { throw new Error(`Invalid number ${number}`); }
    }
  },
};


/* ------------------------ EXPORTS ---------------------------------------- */
export default CheckoutTest;
