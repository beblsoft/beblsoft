/*
  NAME
    CheckoutTabMixin.js

  DESCRIPTION
    Base Class for all CheckoutTab Components
*/

export default {


  /* ---------------------- PROPS ------------------------------------------ */
  props: {
    title: {
      // Component Title, used for locating it
      type: String,
      required: true
    },
  },

  /* ---------------------- DATA ------------------------------------------- */
  data() {
    return {};
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {

    /**
     * Called before tab is switched
     *
     * @return {boolean} (Sync)  If true move on, otherwise stay
     * @return {Promise} (Async) Promise that will resolve or reject appropriately
     */
    async onBeforeTabSwitch() {
      throw new Error('onBeforeTabSwitch not yet implemented!');
    }
  }
};
