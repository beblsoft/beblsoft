/**
 * @file Checkout Select Units Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import CheckoutTest from './CheckoutTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Checkout Select Units', function () {
  beforeEach(function () {
    CheckoutTest.setUp({ testObj: this, tab1: true });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=selectUnitsTab]').should('contain', 'Select Units');
      cy.then(() => {
        for (let j = 0; j < tg.$g.store.state.unit.waUnitPriceList.length; j++) {
          let waUnitPrice = tg.$g.store.state.unit.waUnitPriceList[j];
          cy.get('[data-cy=unitCostColumn]').should('contain', waUnitPrice.unitCost);
        }
      });
    }
  });

  /* ---------------------- FUNCTIONALITY ---------------------------------- */
  /**
   * Test filling out and wizard
   */
  function testForm({
    addUnits = true,
    clickNext = true,
    errorMsg = '',
  } = {}) {
    // Add Units
    if (addUnits) { cy.get('[data-cy=smecknNumberIncrement]').click({ multiple: true }); }
    // Click Next
    if (clickNext) { CheckoutTest.clickNext(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  it('No Units Selected', function () {
    testForm({ addUnits: false, errorMsg: tg.$g.store.state.message.messages.SPECIFY_UNITS_TO_PURCHASE });
  });

  it('Next Success', function () {
    testForm();
    CheckoutTest.verifyTab({ number: 2 });
  });

});
