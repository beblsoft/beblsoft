/**
 * @file Checkout Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import CheckoutTest from './CheckoutTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Checkout', function () {
  beforeEach(function () {
    CheckoutTest.setUp({ testObj: this, tab1: true });
  });

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknCardTitle]').should('contain', 'Purchase Analysis Units');
      cy.get('[data-cy=purchaseFormWizard]').should('be.visible')
        .should('contain', 'Select Units')
        .should('contain', 'Verify Cost')
        .should('contain', 'Select Payment Method')
        .should('contain', 'Review Order');
    }
  });
});
