/**
 * @file Custom Text Analysis Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Custom Text Analysis', function () {
  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        unitLedger: true,
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => {
      await tg.$g.router.push({ name: 'rnCustomTextAnalysis' });
    });
  });

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknCardTitle]').should('be.visible').should('contain', 'Text Analysis');
      cy.get('[data-cy=charactersRemainingPara]').should('be.visible');
      cy.get('[data-cy=analysisUnitsRemainingPara]').should('be.visible');
    }
  });

  /* ---------------------- INPUT CASES ------------------------------------ */
  /**
   * Test filling out, submitting form, and checking results
   */
  function testInput({
    text = '',
    analyzeSentiment = true,
    dominantLanguagePara = '',
    errorMsg = ''
  } = {}) {
    // TextArea
    cy.get('[data-cy=inputTextArea]').clear();

    // Text
    if (text) { cy.get('[data-cy=inputTextArea]').type(text); }

    // Analyze Sentiment
    if (analyzeSentiment) { cy.get('[data-cy=analyzeSentimentButton]').click(); }

    // Dominant Language
    if (dominantLanguagePara !== '') {
      cy.get('[data-cy=dominantLanguagePara]').should('contain', dominantLanguagePara);
      cy.get('[data-cy=dominantLanguageScorePara]').should('be.visible');
    }

    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- INVALID INPUT ---------------------------------- */
  it('Too few characters', function () {
    testInput({ text: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_TEXT_TO_ANALYZE });
  });

  it.skip('Too many characters', function () {
    // Skip as this test takes too long to type
    cy.then(() => {
      testInput({
        text: 'C'.repeat(tg.$g.store.state.constant.waConstants.TEXT_MAX_LENGTH + 1),
        errorMsg: tg.$g.store.state.message.messages.TOO_MUCH_TEXT_ADDED
      });
    });
  });

  it('No more analysis units', function () {
    cy.get('[data-cy=analysisUnitsRemainingPara]');
    cy.then(() => {
      tg.$g.store.commit('unit/setWAUnitBalanceList', [{ unitType: 'TEXT_ANALYSIS', nAvailable: 0 }]);
      testInput({
        text: 'Blah',
        errorMsg: tg.$g.store.state.message.messages.NO_MORE_TEXT_ANALYSIS_UNITS_ARE_AVAILABLE
      });
    });
  });

  /* ---------------------- SUCCESS CASES ---------------------------------- */
  it('Characters remaining countdown', function () {
    cy.then(() => {
      let nChars = 79;
      testInput({ text: 'C'.repeat(nChars), analyzeSentiment: false });
      cy.get('[data-cy=charactersRemainingPara]').should('contain',
        tg.$g.store.state.constant.waConstants.TEXT_MAX_LENGTH - nChars);
    });
  });

  it('Chinese - No sentiment supported', function () {
    testInput({
      text: '中國哲學書電子化計劃',
      dominantLanguagePara: 'Chinese'
    });
    cy.get('[data-cy=sentimentNotSupportedPara]').should('be.visible');
  });

  it('English - Success', function () {
    cy.then(() => {
      testInput({
        text: 'AWS is the bomb!',
        dominantLanguagePara: 'English'
      });
      cy.get('[data-cy=sentimentDataRow]').should('be.visible');
    });
  });

  it('Spanish - Success', function () {
    cy.then(() => {
      testInput({
        text: 'El aeropuerto se considera como un aeródromo para el tráfico regular de aviones.',
        dominantLanguagePara: 'Spanish'
      });
      cy.get('[data-cy=sentimentDataRow]').should('be.visible');
    });
  });
});
