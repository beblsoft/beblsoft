/**
 * @file Facebook Post Likes Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import SmecknDateRangePickerTest from '../../commonComponents/dateRange/SmecknDateRangePickerTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Post Likes', function () {

  /* ---------------------- HELPERS ---------------------------------------- */
  /**
   * Stub Get Histogram
   */
  function stubHistogramGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/histogram*').as('getHistogram');
    });
  }

  /**
   * Stub Get Histogram with Fake Data
   */
  function stubHistogramGetWithFakeData() {
    cy.then(() => {
      cy.server();
      let startMoment = Cypress.moment().utc().subtract(20, 'years');
      let endMoment = Cypress.moment().utc();
      let curMoment = startMoment.clone();
      let dataModelList = [];
      while (endMoment.diff(curMoment) >= 0) {
        dataModelList.push({
          groupByDate: curMoment.toISOString(),
          sum: 547,
          average: 54.7,
        });
        curMoment.add(1, 'year');
      }
      let response = [{
        profileSMID: 2,
        contentType: 'POST',
        contentAttrType: 'FACEBOOK_POST_NLIKES',
        groupByType: 'FACEBOOK_POST_CREATE_DATE',
        groupByDateInterval: 'YEAR',
        intervalStartDate: startMoment.toISOString(),
        intervalEndDate: endMoment.toISOString(),
        dataModelList
      }];
      cy.route({
        method: 'GET',
        url: '*/histogram*',
        response,
        delay: 200,
      }).as('getHistogram');
    });
  }

  /**
   * Stub Get Statistic
   */
  function stubStatisticGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/statistic*').as('getStatistic');
    });
  }

  /**
   * Get statistic number corresponding to dataCY span
   */
  function getStatisticSpanNumber({ dataCYAttr }) {
    return cy.get(`[data-cy=${dataCYAttr}]`).then(($span) => {
      let num = parseFloat($span.text());
      return num;
    });
  }

  /**
   * Before Each Test
   */
  function setUp({ histogramFakeData = false } = {}) {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true,
          facebookPostsFull: true
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      stubStatisticGet();
      if (histogramFakeData) { stubHistogramGetWithFakeData(); } else { stubHistogramGet(); }
      cy.then(() => { tg.$g.router.push({ name: 'rnFacebookPostLikes' }); });
      cy.wait('@getStatistic');
      cy.wait('@getHistogram');
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostLikesCard]').should('be.visible');
      cy.get('[data-cy=facebookPostLikesStatistic]').should('be.visible');
      cy.get('[data-cy=smecknChartMixin]').should('be.visible');
      cy.get('[data-cy=smecknDateRangePicker]').should('be.visible');
    }
  });

  it('Date Range: No Posts', function () {
    setUp();
    SmecknDateRangePickerTest.selectDateRange({
      startMoment: Cypress.moment().add(20, 'years'),
      endMoment: Cypress.moment().add(40, 'years'),
    });
    cy.wait('@getStatistic');
    cy.get('[data-cy=noPostsExistInDateRange]').should('be.visible');
    cy.get('[data-cy=facebookPostLikesStatistic]').should('not.be.visible');
    cy.get('[data-cy=smecknChartMixin]').should('not.be.visible');
  });

  it('Statistic Check', function () {
    setUp();
    SmecknDateRangePickerTest.selectDateRange({
      startMoment: Cypress.moment().subtract(20, 'years'),
      endMoment: Cypress.moment().add(20, 'years'),
    });
    cy.wait('@getStatistic');
    cy.get('[data-cy=noPostsExistInDateRange]').should('not.be.visible');
    cy.get('[data-cy=facebookPostLikesStatistic]').should('be.visible');
    getStatisticSpanNumber({ dataCYAttr: 'nFacebookPosts' }).should('be.greaterThan', 0);
  });

  it('Histogram Snapshot', function () {
    setUp({ histogramFakeData: true });
    cy.wait(2000); // Wait for histogram
    cy.get('canvas').matchImageSnapshot();
  });

  it('Sync Complete', function () {
    setUp();
    cy.then(() => {
      tg.$g.syncBus.complete();
      cy.wait('@getStatistic');
      cy.wait('@getHistogram');
    });
  });
});
