/**
 * @file Facebook Post Sentiment Count Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Post Sentiment Counts', function () {

  /* ---------------------- STUBBING --------------------------------------- */
  /**
   * Stub Analysis Count Get
   */
  function stubAnalysisCountGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/analysis/count*').as('getAnalysisCount');
    });
  }

  /**
   * Stub Analysis Count Get with Fake Data
   */
  function stubAnalysisCountGetWithFakeData({
    countDone = 398,
    countNotDone = 398,
    countCant = 398
  }) {
    cy.then(() => {
      cy.server();
      let response = {
        profileSMID: 398,
        contentType: 'POST',
        analysisType: 'COMPREHEND_SENTIMENT',
        countDone,
        countNotDone,
        countCant,
      };
      cy.route({
        method: 'GET',
        url: '*/analysis/count*',
        response,
        delay: 200,
      }).as('getAnalysisCount');
    });
  }

  /**
   * Stub Start Analysis
   */
  function stubAnalysisStart() {
    cy.then(() => {
      cy.server();
      cy.route('POST', '*/analysis*', {
        smID: 398,
        profileSMID: 398,
        createDate: '2019-08-06T14:36:03.423Z',
        contentType: 'POST',
        analysisType: 'COMPREHEND_SENTIMENT',
        status: 'IN_PROGRESS',
        errorSeen: true
      }).as('startAnalysis');
    });
  }

  /**
   * Stub Unit Balance
   */
  function stubUnitBalanceGet({
    nAvailable = 0,
    nHeld = 0,
    nTotal = 0
  } = {}) {
    cy.then(() => {
      cy.server();
      cy.route({
        method: 'GET',
        url: '*/unit/balance*',
        response: [{ unitType: 'TEXT_ANALYSIS', nAvailable, nHeld, nTotal }]
      }).as('getUnitBalance');
    });
  }

  /* ---------------------- SETUP ------------------------------------------ */
  /**
   * Before Each Test
   */
  function setUp({
    sentimentHistogramFakeData = false,
    dominantSentimentHistogramFakeData = false
  } = {}) {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true,
          facebookPostsFull: true
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      cy.then(() => { tg.$g.router.push({ name: 'rnFacebookPostSentiment' }); });
      cy.wait('@getAnalysisCount');
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    stubAnalysisCountGet();
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostSentimentCounts]').should('be.visible');
      cy.get('[data-cy=analyzeAllButton]').should('be.visible');
      cy.get('[data-cy=analyzeAllNotDoneCount]').should('be.visible');
      cy.get('[data-cy=countTotal]').should('be.visible');
      cy.get('[data-cy=countDone]').should('be.visible');
      cy.get('[data-cy=countNotDone]').should('be.visible');
      cy.get('[data-cy=countCant]').should('be.visible');
    }
  });

  it('Validate Counts', function () {
    stubAnalysisCountGet();
    cy.get('[data-cy=countCant]').then(($span) => {
      let value = parseFloat($span.text());
      cy.wrap(value).as('countCant');
      expect(value).to.be.at.least(0);
    });
    cy.get('[data-cy=countDone]').then(($span) => {
      let value = parseFloat($span.text());
      cy.wrap(value).as('countDone');
      expect(value).to.be.at.least(0);
    });
    cy.get('[data-cy=countNotDone]').then(($span) => {
      let value = parseFloat($span.text());
      cy.wrap(value).as('countNotDone');
      expect(value).to.be.at.least(0);
    });
    cy.get('[data-cy=countTotal]').then(($span) => {
      let value = parseFloat($span.text());
      expect(value).to.equal(this.countCant + this.countDone + this.countNotDone);
    });
  });

  it('All Posts Analyzed', function () {
    stubAnalysisCountGetWithFakeData({ countDone: 500, countNotDone: 0, countCant: 0 });
    setUp();
    cy.get('[data-cy=analyzeAllButton]').should('not.be.visible');
  });

  it('Start analysis: Not Enough Units', function () {
    stubAnalysisCountGet();
    stubUnitBalanceGet({ nAvailable: 0, nTotal: 0, nHeld: 0 });
    stubAnalysisStart();
    setUp();
    cy.get('[data-cy=analyzeAllButton]').click();
    cy.wait('@getUnitBalance');
    cy.get('[data-cy=errorMessagePara]').should('be.visible');
  });

  it('Start analysis: Success', function () {
    stubAnalysisCountGet();
    stubUnitBalanceGet({ nAvailable: 5000, nTotal: 5000, nHeld: 0 });
    stubAnalysisStart();
    setUp();
    cy.get('[data-cy=analyzeAllButton]').click();
    cy.wait('@getUnitBalance');
    cy.wait('@startAnalysis');
  });

  it('Sync Complete', function () {
    stubAnalysisCountGet();
    setUp();
    cy.then(() => {
      tg.$g.syncBus.complete();
      cy.wait('@getAnalysisCount');
    });
  });

  it('Analysis Complete', function () {
    stubAnalysisCountGet();
    setUp();
    cy.then(() => {
      tg.$g.analysisBus.complete();
      cy.wait('@getAnalysisCount');
    });
  });
});
