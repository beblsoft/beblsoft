/**
 * @file Facebook Post Sentiment Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import SmecknDateRangePickerTest from '../../commonComponents/dateRange/SmecknDateRangePickerTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Post Sentiment', function () {

  /* ---------------------- STATISTIC -------------------------------------- */
  /**
   * Stub Get Statistic
   */
  function stubStatisticGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/statistic*').as('getStatistic');
    });
  }

  /* ---------------------- SENTIMENT HISTOGRAM ---------------------------- */
  /**
   * Stub Get Sentiment Histogram
   */
  function stubSentimentHistogramGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/histogram*COMP_SENT_ANALYSIS_ALL_SCORE*').as('getSentimentHistogram');
    });
  }

  /**
   * Stub Get Sentiment Histogram with Fake Data
   */
  function stubSentimentHistogramGetWithFakeData() {
    cy.then(() => {
      cy.server();
      let startMoment = Cypress.moment().utc().subtract(20, 'years');
      let endMoment = Cypress.moment().utc();
      let curMoment = startMoment.clone();
      let dataModelList = [];
      while (endMoment.diff(curMoment) >= 0) {
        dataModelList.push({
          groupByDate: curMoment.toISOString(),
          sum: 547,
          average: 54.7,
          count: 10,
          min: 1,
          max: 100
        });
        curMoment.add(1, 'year');
      }
      let waHistogram = {
        profileSMID: 2,
        contentType: 'POST_MESSAGE',
        contentAttrType: 'COMP_SENT_ANALYSIS_POSITIVE_SCORE',
        groupByType: 'FACEBOOK_POST_CREATE_DATE',
        groupByDateInterval: 'YEAR',
        intervalStartDate: startMoment.toISOString(),
        intervalEndDate: endMoment.toISOString(),
        dataModelList
      };
      let response = [];
      Cypress._.forEach(['COMP_SENT_ANALYSIS_POSITIVE_SCORE', 'COMP_SENT_ANALYSIS_NEGATIVE_SCORE',
        'COMP_SENT_ANALYSIS_NEUTRAL_SCORE', 'COMP_SENT_ANALYSIS_MIXED_SCORE',
      ], (contentAttrType) => {
        let waHistogramClone = Cypress._.cloneDeep(waHistogram);
        waHistogramClone.contentAttrType = contentAttrType;
        response.push(waHistogramClone);
      });
      cy.route({
        method: 'GET',
        url: '*/histogram*COMP_SENT_ANALYSIS_ALL_SCORE*',
        response,
        delay: 200,
      }).as('getSentimentHistogram');
    });
  }

  /* ---------------------- DOMINANT SENTIMENT HISTOGRAM ------------------- */
  /**
   * Stub Get Dominant Sentiment Histogram
   */
  function stubDominantSentimentHistogramGet() {
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/histogram*COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT*').as('getDominantSentimentHistogram');
    });
  }

  /**
   * Stub Get Dominant Sentiment Histogram with Fake Data
   */
  function stubDominantSentimentHistogramGetWithFakeData() {
    cy.then(() => {
      cy.server();
      let startMoment = Cypress.moment().utc().subtract(20, 'years');
      let endMoment = Cypress.moment().utc();
      let dataModelList = [];
      Cypress._.forEach(['COMP_SENT_TYPE_POSITIVE', 'COMP_SENT_TYPE_NEGATIVE',
        'COMP_SENT_TYPE_NEUTRAL', 'COMP_SENT_TYPE_MIXED'
      ], (groupByContentAttrType) => {
        dataModelList.push({
          groupByContentAttrType,
          sum: 547,
          average: 54.7,
          count: 10,
          min: 1,
          max: 100
        });
      });
      let response = [{
        profileSMID: 2,
        contentType: 'POST_MESSAGE',
        contentAttrType: 'COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT',
        groupByType: 'COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT',
        groupByDateInterval: null,
        intervalStartDate: startMoment.toISOString(),
        intervalEndDate: endMoment.toISOString(),
        dataModelList
      }];
      cy.route({
        method: 'GET',
        url: '*/histogram*COMP_SENT_ANALYSIS_DOMINANT_SENTIMENT*',
        response,
        delay: 200,
      }).as('getDominantSentimentHistogram');
    });
  }


  /* ---------------------- WAIT ------------------------------------------- */
  /**
   * Wait for component to load data
   */
  function waitLoadData() {
    cy.wait('@getStatistic');
    cy.wait('@getSentimentHistogram');
    cy.wait('@getDominantSentimentHistogram');
  }

  /* ---------------------- SETUP ------------------------------------------ */
  /**
   * Before Each Test
   */
  function setUp({
    sentimentHistogramFakeData = false,
    dominantSentimentHistogramFakeData = false
  } = {}) {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true,
          facebookPostsFull: true
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();

      // Stubbing
      stubStatisticGet();
      if (sentimentHistogramFakeData) {
        stubSentimentHistogramGetWithFakeData();
      } else {
        stubSentimentHistogramGet();
      }
      if (dominantSentimentHistogramFakeData) {
        stubDominantSentimentHistogramGetWithFakeData();
      } else {
        stubDominantSentimentHistogramGet();
      }

      cy.then(() => { tg.$g.router.push({ name: 'rnFacebookPostSentiment' }); });
      waitLoadData();
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostSentimentCard]').should('be.visible');
      cy.get('[data-cy=facebookPostSentimentCounts]').should('be.visible');
      cy.get('[data-cy=facebookPostSentimentTable]').should('be.visible');
      cy.get('[data-cy=facebookPostSentimentHistogram]').should('be.visible');
      cy.get('[data-cy=facebookPostSentimentDoughnut]').should('be.visible');
      cy.get('[data-cy=noPostsExistInDateRange]').should('not.be.visible');
    }
  });

  it('Date Range: No Posts', function () {
    setUp();
    SmecknDateRangePickerTest.selectDateRange({
      startMoment: Cypress.moment().add(20, 'years'),
      endMoment: Cypress.moment().add(40, 'years'),
    });
    waitLoadData();
    cy.get('[data-cy=noPostsExistInDateRange]').should('be.visible');
    cy.get('[data-cy=facebookPostSentimentTable]').should('not.be.visible');
    cy.get('[data-cy=facebookPostSentimentHistogram]').should('not.be.visible');
    cy.get('[data-cy=facebookPostSentimentDoughnut]').should('not.be.visible');
  });

  it('Statistic Table', function () {
    setUp();
    cy.get('[data-cy=facebookPostSentimentTableCount]').each(($td) => {
      let val = parseFloat($td.text());
      expect(val).to.be.at.least(0);
    });
    cy.get('[data-cy=facebookPostSentimentTableAverageScore]').each(($td) => {
      let val = parseFloat($td.text());
      expect(val).to.be.at.least(0);
    });
    cy.get('[data-cy=facebookPostSentimentTableLowestScore]').each(($td) => {
      let val = parseFloat($td.text());
      expect(val).to.be.at.least(0);
    });
    cy.get('[data-cy=facebookPostSentimentTableHighestScore]').each(($td) => {
      let val = parseFloat($td.text());
      expect(val).to.be.at.least(0);
    });
  });

  it('Histogram Snapshot', function () {
    setUp({ sentimentHistogramFakeData: true });
    cy.wait(2000); // Wait for histogram
    cy.get('[data-cy=facebookPostSentimentHistogram]').matchImageSnapshot();
  });

  it('Doughnut Snapshot', function () {
    setUp({ dominantSentimentHistogramFakeData: true });
    cy.wait(2000); // Wait for histogram
    cy.get('[data-cy=facebookPostSentimentDoughnut]').matchImageSnapshot();
  });

  it('Sync Complete', function () {
    setUp();
    cy.then(() => {
      tg.$g.syncBus.complete();
      waitLoadData();
    });
  });

  it('Analysis Complete', function () {
    setUp();
    cy.then(() => {
      tg.$g.analysisBus.complete();
      waitLoadData();
    });
  });
});
