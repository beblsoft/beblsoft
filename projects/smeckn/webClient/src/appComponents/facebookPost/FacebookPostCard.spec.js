/**
 * @file Facebook Post Card Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';

/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Post Card', function () {

  /* ---------------------- MODELS ----------------------------------------- */
  let textExampleModel = {
    smID: 398,
    createDate: '2019-08-05T17:41:09.525Z',
    text: 'The other day the the dog jumped over the moon!',
    permalinkURL: 'https://www.facebook.com/102691597514550/posts/102690844181292',
    origLength: 200,
    compLangAnalysisModel: {
      smID: 398,
      status: 'SUCCESS',
      compLangModelList: [{
        smID: 398,
        bLanguageType: 'English',
        score: 0.2133
      }],
      dominantBLanguageType: 'English'
    },
    compSentAnalysisModel: {
      smID: 398,
      status: 'SUCCESS',
      dominantSentiment: 'POSITIVE',
      positiveScore: 0.23,
      negativeScore: 0.23,
      neutralScore: 0.23,
      mixedScore: 0.23
    }
  };
  let photoExampleModel = {
    smID: 398,
    createDate: '2019-08-05T17:41:09.525Z',
    imageURL: 'https://www.gstatic.com/webp/gallery/1.sm.jpg'
  };
  let linkExampleModel = {
    smID: 398,
    createDate: '2019-08-05T17:41:09.525Z',
    imageURL: 'https://www.gstatic.com/webp/gallery/1.sm.jpg',
    linkURL: 'https:www.disney.com',
    name: 'Disney.com | The official home for all things Disney',
    caption: 'disney.com',
    description: 'The official website for all things Disney...'
  };
  let videoExampleModel = {
    smID: 398,
    createDate: '2019-08-05T17:41:09.525Z',
    imageURL: 'https://www.gstatic.com/webp/gallery/1.sm.jpg',
    videoURL: 'https:www.disney.com',
    lengthSec: 398
  };

  /* ---------------------- HELPERS ---------------------------------------- */
  /**
   * Stub FacebookPost List
   */
  function stubFacebookPostList({
    smID = 300,
    fbCreateDate = null,
    type = 'STATUS',
    nLikes = 50,
    nComments = 23,
    nReactions = 21,
    messageTextModel = null,
    photoModel = null,
    linkModel = null,
    videoModel = null,
    delayMS = 200
  } = {}) {
    cy.then(() => {
      let response = {
        nextCursor: '',
        data: [{
          smID,
          fbCreateDate: fbCreateDate ? fbCreateDate : Cypress.moment.utc().subtract(2, 'seconds').toISOString(),
          nLikes,
          nComments,
          nReactions,
          messageTextModel,
          photoModel,
          linkModel,
          videoModel,
        }]
      };
      cy.server();
      cy.route({ method: 'GET', url: '*/facebookPost*', response, delay: delayMS, }).as('listFacebookPost');
    });
  }

  /**
   * SetUp Test
   */
  function setUp() {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true,
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      cy.then(() => { tg.$g.router.push({ name: 'rnFacebookPostAll' }); });
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Message Visible', function () {
    stubFacebookPostList({ messageTextModel: textExampleModel, });
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostCard]').should('be.visible');
      cy.get('[data-cy=facebookPostCreateDatePara]').should('be.visible');
      cy.get('[data-cy=facebookPostNLikesPara]').should('be.visible');
      cy.get('[data-cy=facebookPostNCommentsPara]').should('be.visible');
      cy.get('[data-cy=facebookPostMessageText]').should('be.visible');
      cy.get('[data-cy=smecknTextAnalysis]').should('be.visible');
      cy.get('[data-cy=facebookPostPermaLink]').should('be.visible');
    }
  });

  it('Photo Visible', function () {
    stubFacebookPostList({ photoModel: photoExampleModel, });
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostImage]').should('be.visible');
    }
  });

  it('Link Visible', function () {
    stubFacebookPostList({ linkModel: linkExampleModel, });
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookLinkImage]').should('be.visible');
      cy.get('[data-cy=facebookLinkName]').should('be.visible');
      cy.get('[data-cy=facebookLinkDescription]').scrollIntoView().should('be.visible');
    }
  });

  it('Video Visible', function () {
    stubFacebookPostList({ videoModel: videoExampleModel, });
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostVideoImage]').should('be.visible');
    }
  });
});
