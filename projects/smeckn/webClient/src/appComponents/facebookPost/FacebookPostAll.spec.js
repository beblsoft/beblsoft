/**
 * @file Facebook Post Card Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';
import SmecknDateRangePickerTest from '../../commonComponents/dateRange/SmecknDateRangePickerTest.js';
import SmecknSortByDropDownTest from '../../commonComponents/sortByDropDown/SmecknSortByDropDownTest.js';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Post All', function () {

  /* ---------------------- HELPERS ---------------------------------------- */
  /**
   * Subb FacebookPost List
   */
  function stubFacebookPostList() {
    // Listen On Routes
    cy.then(() => {
      cy.server();
      cy.route('GET', '*/facebookPost*').as('getFacebookPost');
    });
  }

  /**
   * Fill out search form
   */
  function fillForm({
    searchWord = '',
    waSortType = 'FACEBOOK_POST_FBCREATEDATE',
    waSortOrder = 'DESCENDING',
    startMoment = Cypress.moment().subtract(20, 'years'),
    endMoment = Cypress.moment().add(20, 'minutes'),
  } = {}) {
    // Search Word
    cy.get('[data-cy=facebookPostSearchBar]').clear();
    if (searchWord !== '') {
      cy.get('[data-cy=facebookPostSearchBar]').type(searchWord);
    }
    // Sort
    SmecknSortByDropDownTest.selectSort({ waSortType, waSortOrder });
    // Date Picker
    SmecknDateRangePickerTest.selectDateRange({ startMoment, endMoment });
    // Wait for request
    cy.wait('@getFacebookPost');
    cy.get('[data-cy=smecknSpinner]').should('not.be.visible');
  }

  /* ---------------------- BEFORE EACH ------------------------------------ */
  beforeEach(function () {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
          syncsForAllProfilesSuccess: true,
          facebookPostsFull: true
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      stubFacebookPostList();
      cy.then(() => { tg.$g.router.push({ name: 'rnFacebookPostAll' }); });
    });
  });

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookPostAllCard]').should('be.visible');
      cy.get('[data-cy=facebookPostSearchBar]').should('be.visible');
      cy.get('[data-cy=smecknSortByDropDown]').should('be.visible');
      cy.get('[data-cy=smecknDateRangePicker]').should('be.visible');
    }
  });

  it('Search: Results', function () {
    let searchWord = 'the';
    let searchRegEx = RegExp(searchWord, 'i');
    fillForm({ searchWord });
    cy.get('[data-cy=facebookPostMessageText]').each(($text) => {
      cy.wrap($text).contains(searchRegEx);
    });
    cy.get('[data-cy=noMatchingPostsPara]').should('not.be.visible');
  });

  it('Search: No Results', function () {
    let searchWord = 'qqqqqqqqqqqqqqqqqqqqqqqq';
    fillForm({ searchWord });
    cy.get('[data-cy=facebookPostMessageText]').should('not.be.visible');
    cy.get('[data-cy=noMatchingPostsPara]').should('be.visible');
  });

  it('Change date: No Results', function () {
    fillForm({
      startMoment: Cypress.moment().add(20, 'years'),
      endMoment: Cypress.moment().add(40, 'years')
    });
    cy.get('[data-cy=facebookPostMessageText]').should('not.be.visible');
    cy.get('[data-cy=noMatchingPostsPara]').should('be.visible');
  });

  it('Sort By: Create Date Ascending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_FBCREATEDATE', waSortOrder: 'ASCENDING', });
    cy.wrap(false).as('lastDate');
    cy.get('[data-cy=facebookPostCreateDatePara]').each(($p) => {
      let curDate = Cypress.moment($p.text());
      if (this.lastDate) {
        // CurDate - LastDate should be positive as CurDate is more recent
        expect(curDate.diff(this.lastDate)).to.be.at.least(0);
      }
      this.lastDate = curDate;
    });
  });

  it('Sort By: Create Date Descending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_FBCREATEDATE', waSortOrder: 'DESCENDING', });
    cy.wrap(false).as('lastDate');
    cy.get('[data-cy=facebookPostCreateDatePara]').each(($p) => {
      let curDate = Cypress.moment($p.text());
      if (this.lastDate) {
        // CurDate - LastDate should be negative as LastDate is more recent
        expect(curDate.diff(this.lastDate)).to.be.at.most(0);
      }
      this.lastDate = curDate;
    });
  });

  it('Sort By: Likes Ascending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_NLIKES', waSortOrder: 'ASCENDING', });
    cy.wrap(false).as('lastNLikes');
    cy.get('[data-cy=facebookPostNLikesPara]').each(($p) => {
      let curNLikes = parseFloat($p.text());
      if (this.lastNLikes) {
        expect(this.lastNLikes).to.be.at.most(curNLikes);
      }
      this.lastNLikes = curNLikes;
    });
  });

  it('Sort By: Likes Descending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_NLIKES', waSortOrder: 'DESCENDING', });
    cy.wrap(false).as('lastNLikes');
    cy.get('[data-cy=facebookPostNLikesPara]').each(($p) => {
      let curNLikes = parseFloat($p.text());
      if (this.lastNLikes) {
        expect(this.lastNLikes).to.be.at.least(curNLikes);
      }
      this.lastNLikes = curNLikes;
    });
  });

  it('Sort By: Comments Ascending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_NCOMMENTS', waSortOrder: 'ASCENDING', });
    cy.wrap(false).as('lastNComments');
    cy.get('[data-cy=facebookPostNCommentsPara]').each(($p) => {
      let curNComments = parseFloat($p.text());
      if (this.lastNComments) {
        expect(this.lastNComments).to.be.at.most(curNComments);
      }
      this.lastNComments = curNComments;
    });
  });

  it('Sort By: Comments Descending', function () {
    fillForm({ waSortType: 'FACEBOOK_POST_NCOMMENTS', waSortOrder: 'DESCENDING', });
    cy.wrap(false).as('lastNComments');
    cy.get('[data-cy=facebookPostNCommentsPara]').each(($p) => {
      let curNComments = parseFloat($p.text());
      if (this.lastNComments) {
        expect(this.lastNComments).to.be.at.least(curNComments);
      }
      this.lastNComments = curNComments;
    });
  });

  it('Sort By: Sentiment', function () {
    let sentimentList = [{
        waSortType: 'COMP_SENT_ANALYSIS_POSITIVE_SCORE',
        dataCY: 'positiveSentimentScore'
      },
      {
        waSortType: 'COMP_SENT_ANALYSIS_NEGATIVE_SCORE',
        dataCY: 'negativeSentimentScore'
      },
      {
        waSortType: 'COMP_SENT_ANALYSIS_NEUTRAL_SCORE',
        dataCY: 'neutralSentimentScore'
      },
      {
        waSortType: 'COMP_SENT_ANALYSIS_MIXED_SCORE',
        dataCY: 'mixedSentimentScore'
      }
    ];
    Cypress._.forEach(sentimentList, (sentimentObj) => {
      fillForm({ waSortType: sentimentObj.waSortType, waSortOrder: 'DESCENDING', });
      cy.wrap(false).as('lastValue');
      cy.get(`[data-cy=${sentimentObj.dataCY}]`).each(($progress) => {
        let curValue = parseFloat($progress.text());
        if (this.lastValue) {
          expect(this.lastValue).to.be.at.least(curValue);
        }
        this.lastValue = curValue;
      });
    });
  });

  it('Auto Scroll', function () {
    // Skipping this test as it itermittently fails
    // Add 3 scrolls to ensure that the second call to get facebookPostCard
    // gets more elements than the first call
    cy.get('[data-cy=facebookPostCard]').its('length').as('startLength');
    cy.get('[data-cy=facebookPostCard]').last().scrollIntoView({ duration: 1000 });
    cy.wait('@getFacebookPost');
    cy.get('[data-cy=facebookPostCard]').last().scrollIntoView({ duration: 1000 });
    cy.wait('@getFacebookPost');
    cy.get('[data-cy=facebookPostCard]').last().scrollIntoView({ duration: 1000 });
    cy.then(() => {
      cy.get('[data-cy=facebookPostCard]').its('length').as('endLength');
    });
    cy.then(() => {
      expect(this.endLength).to.be.greaterThan(this.startLength);
    });
  });

  it('Sync Complete', function () {
    cy.then(() => {
      tg.$g.syncBus.complete();
      cy.wait('@getFacebookPost');
    });
  });

  it('Analysis Complete', function () {
    cy.then(() => {
      tg.$g.analysisBus.complete();
      cy.wait('@getFacebookPost');
    });
  });
});
