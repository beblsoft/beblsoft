/**
 * @file Profile Group Manage Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Group Manage', function () {
  let waProfileGroup = null;

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    cy.then(async () => {
      waProfileGroup = await tg.server.api.profileGroup.create({ data: { name: 'Steve' } });
    });
    tg.app.login();
    cy.then(async () => {
      tg.$g.router.push({
        name: 'rnProfileGroupManage',
        params: { waProfileGroup }
      });
    });
    cy.url().should('include', '/profileGroupManage');
  });


  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting form
   */
  function testForm({
    name = '',
    errorMsg = '',
    click = true
  } = {}) {
    // Name
    cy.get('[data-cy=profileGroupNameInput]').clear();
    if (name !== '') { cy.get('[data-cy=profileGroupNameInput]').type(name); }
    // Click
    if (click) { cy.get('[data-cy=saveButton]').click(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=saveButton]').should('be.visible');
      cy.get('[data-cy=deleteModalButton]').should('be.visible');
    }
  });

  /* ---------------------- BAD NAMES -------------------------------------- */
  it('Bad names', function () {
    testForm({ errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PROFILEGROUP_NAME });
    testForm({ name: '12908(@#*%*@#%', errorMsg: 'Profile Group Name Contains Invalid Characters' });
    testForm({
      name: '01234567890123456789012345678901234567890123456789012345678901234567890123456789',
      errorMsg: 'Profile Group Name Is Not Properly Sized'
    });
  });

  /* ---------------------- SAVE ------------------------------------------- */
  it('Success', function () {
    let name = 'Bruce';
    testForm({ name });
    cy.url().should('include', '/profileGroupsManage');
    cy.get('[data-cy=profileGroupCard]').should('contain', name);
  });

  /* ---------------------- DELETE CANCEL ---------------------------------- */
  it('Delete Cancel', function () {
    cy.get('[data-cy=deleteModalButton]').click();
    cy.get('[data-cy=cancelDeleteButton]').click();
    cy.url().should('include', '/profileGroupManage');
  });

  /* ---------------------- DELETE ----------------------------------------- */
  it('Delete', function () {
    cy.get('[data-cy=deleteModalButton]').click();
    cy.get('[data-cy=deleteButton]').click();
    cy.url().should('include', '/profileGroupsDashboard');
    cy.get('[data-cy=profileGroupCard]').should('contain',
      tg.$g.store.state.message.messages.DEFAULT_PROFILE_GROUP_NAME);
  });

});
