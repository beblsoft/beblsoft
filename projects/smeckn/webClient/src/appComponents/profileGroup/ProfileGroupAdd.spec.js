/**
 * @file Profile Group Add Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Group Add', function () {
  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => {
      await tg.$g.router.push({ name: 'rnProfileGroupAdd' });
    });
  });

  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting form
   */
  function testForm({
    name = '',
    errorMsg = '',
    click = true,
    enter = false
  } = {}) {
    // Name
    cy.get('[data-cy=profileGroupNameInput]').clear();
    if (name !== '') { cy.get('[data-cy=profileGroupNameInput]').type(name); }
    // Click
    if (click) { cy.get('[data-cy=addProfileGroupButton]').click(); }
    // Enter
    if (enter) { cy.get('[data-cy=profileGroupNameInput]').type('{enter}'); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=profileGroupNameInput]').should('be.visible');
      cy.get('[data-cy=cancelButton]').should('be.visible');
      cy.get('[data-cy=addProfileGroupButton]').should('be.visible');
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Navigation', function () {
    // Cancel
    cy.get('[data-cy=cancelButton]').click();
    cy.url().should('include', '/profileGroupsDashboard');
  });

  /* ---------------------- BAD NAMES -------------------------------------- */
  it('Bad names', function () {
    testForm({ errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PROFILEGROUP_NAME });
    testForm({ name: '12908(@#*%*@#%', errorMsg: 'Profile Group Name Contains Invalid Characters' });
    testForm({
      name: '01234567890123456789012345678901234567890123456789012345678901234567890123456789',
      errorMsg: 'Profile Group Name Is Not Properly Sized'
    });
  });

  /* ---------------------- SUCCESS ---------------------------------------- */
  it('Success:Enter', function () {
    let name = 'Bruce';
    testForm({ name, click: false, enter: true });
    cy.url().should('include', '/profileGroupsDashboard');
    cy.get('[data-cy=profileGroupCard]').should('contain', name);
  });
  it('Success', function () {
    let name = 'Bruce';
    testForm({ name });
    cy.url().should('include', '/profileGroupsDashboard');
    cy.get('[data-cy=profileGroupCard]').should('contain', name);
  });
});
