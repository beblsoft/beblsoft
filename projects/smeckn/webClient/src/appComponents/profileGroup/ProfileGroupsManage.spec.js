/**
 * @file Profile Groups Manage Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Groups Manage', function () {
  let profileGroupPrefix = 'profileGroup';

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Setup state
   */
  function setUp({ nProfileGroups = 1 } = {}) {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    cy.then(async () => {
      for (let i = 0; i < nProfileGroups; i++) {
        await tg.server.api.profileGroup.create({ data: { name: `${profileGroupPrefix}${i}` } });
      }
    });
    tg.app.login();
    cy.get('[data-cy=profileGroupCard]').should('be.visible');
    cy.then(async () => {
      // Wait for profile groups to be displayed before routing (needed for 0 profile group case)
      // If nProfileGroups === 0, teardown the profile groups created by profileGroupDashboard
      if (nProfileGroups === 0) {
        let waProfileGroupList = await tg.server.api.profileGroup.list();
        for (let i = 0; i < waProfileGroupList.length; i++) {
          await tg.server.api.profileGroup.delete({ urlParams: { id: waProfileGroupList[i].id } });
        }
      }
    });
    cy.then(async () => { tg.$g.router.push({ name: 'rnProfileGroupsManage' }); });
    cy.url().should('include', '/profileGroupsManage');
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=profileGroupCard]').should('be.visible');
      cy.get('[data-cy=profileGroupCard]').should('be.visible');
      cy.get('[data-cy=doneEditsButton]').scrollIntoView().should('be.visible');
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Navigation', function () {
    setUp();
    // Done Edits
    cy.get('[data-cy=doneEditsButton]').click();
    cy.url().should('include', '/profileGroupsDashboard');

    // Go back to management page
    cy.go('back');
    cy.url().should('include', '/profileGroupsManage');

    // Add Profile Group
    cy.get('[data-cy=profileGroupCard]').click();
    cy.url().should('include', '/profileGroupManage');
  });

  /* ---------------------- MANY PROFILE GROUPS ---------------------------- */
  it('Many profile groups', function () {
    let nProfileGroups = 3;
    setUp({ nProfileGroups });
    // Verify all profile groups are listed
    for (let i = 0; i < nProfileGroups; i++) {
      cy.get('[data-cy=profileGroupCard]').eq(i).then(($card) => {
        expect($card).to.contain(`${profileGroupPrefix}${i}`);
      });
    }
  });

  /* ---------------------- NO GROUPS -------------------------------------- */
  it('No Groups', function () {
    setUp({ nProfileGroups: 0 });
    cy.url().should('include', '/profileGroupsDashboard');
  });
});
