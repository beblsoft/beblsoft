/**
 * @file Profile Groups Dashboard Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Profile Groups Dashboard', function () {
  let profileGroupPrefix = 'ProfileGroup';

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Log the user in and wait for first profile group
   */
  function setUp({ nProfileGroups = 1 } = {}) {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    cy.then(async () => {
      for (let i = 0; i < nProfileGroups; i++) {
        await tg.server.api.profileGroup.create({ data: { name: `${profileGroupPrefix}${i}` } });
      }
    });
    tg.app.login();
    cy.url().should('include', '/profileGroupsDashboard');
    cy.get('[data-cy=profileGroupCard]').should('be.visible');
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=addProfileGroupCard]').should('be.visible');
      cy.get('[data-cy=manageProfileGroupsButton]').should('be.visible');
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Navigation', function () {
    setUp();
    // Manage Profile Groups
    cy.get('[data-cy=manageProfileGroupsButton]').click();
    cy.url().should('include', '/profileGroupsManage');
    cy.go('back');

    // Add Profile Group
    cy.get('[data-cy=addProfileGroupCard]').click();
    cy.url().should('include', '/profileGroupAdd');
    cy.go('back');

    // DrillDown
    cy.get('[data-cy=profileGroupCard]').click();
    cy.url().should('include', '/profileGroupDrillDown');
    cy.then(() => {
      cy.get('[data-cy=profileGroupDropdownProfileGroup]')
        .should('contain', `${profileGroupPrefix}0`);
    });
  });

  /* ---------------------- DEFAULT GROUP ---------------------------------- */
  it('Default Group', function () {
    setUp({ nProfileGroups: 0 });
    cy.get('[data-cy=profileGroupCard]').should('be.visible');
    cy.then(() => {
      cy.get('[data-cy=profileGroupCard]')
        .should('contain', tg.$g.store.state.message.messages.DEFAULT_PROFILE_GROUP_NAME);
    });
  });

  /* ---------------------- MAX GROUPS ------------------------------------- */
  it('Max profile groups', function () {
    let nProfileGroups = null;
    tg.init(); /* Load constants */
    cy.then(() => {
      nProfileGroups = tg.$g.store.state.constant.waConstants.MAX_PROFILEGROUPS;
      setUp({ nProfileGroups });
    });

    // Verify all profile groups are listed: 1. In header, 2. On card
    for (let i = 0; i < nProfileGroups; i++) {
      cy.get('[data-cy=profileGroupDropdownProfileGroup]').eq(i).then(($elem) => {
        expect($elem).to.contain(`${profileGroupPrefix}${i}`);
      });
      cy.get('[data-cy=profileGroupCard]').eq(i).then(($card) => {
        expect($card).to.contain(`${profileGroupPrefix}${i}`);
      });
    }
    cy.get('[data-cy=addProfileGroupCard]').should('not.be.visible');
  });
});
