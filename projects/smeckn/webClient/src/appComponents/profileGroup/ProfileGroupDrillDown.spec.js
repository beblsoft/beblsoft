/**
 * @file Profile Group Drill Down Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ BASIC ------------------------------------------ */
describe('ProfileGroupDrillDown: Basic', function () {

  /* ---------------------- BEFORE EACH ------------------------------------ */
  /**
   * Setup Tests
   */
  beforeEach(function () {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
    });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=selectProfileIcon]').should('be.visible');
      cy.get('[data-cy=addProfileIcon]').should('be.visible');
    }
  });
  it('waProfileGroup Cleared', function () {
    cy.then(() => { tg.$g.store.commit('profileGroup/clearWAProfileGroup'); });
    cy.url().should('include', 'profileGroupsDashboard');
  });
  it('waProfileGroup Changed', function () {
    cy.then(() => {
      tg.$g.store.commit('profileGroup/setWAProfileGroup', { id: 3, name: 'loo' });
    });
    cy.wait('@getProfileList');
  });
  it('Expand', function () {
    cy.viewport(1000, 1000);
    cy.get('[data-cy=sidebarContainer]').should('be.visible');
    cy.get('[data-cy=sidebarContainerCollapsed]').should('not.be.visible');
  });
  it('Collapse', function () {
    cy.viewport(300, 600);
    cy.get('[data-cy=sidebarContainerCollapsed]').should('be.visible');
    cy.get('[data-cy=sidebarContainer]').should('not.be.visible');
  });
  it('No profiles added', function () {
    cy.then(() => { tg.$g.store.commit('profile/clearWAProfileList'); });
    cy.get('[data-cy=disabledSidebarItem] [data-cy=selectProfileIcon]').should('be.visible');
  });
  it('All profiles added', function () {
    // Wait for profile/add to query profiles before updating
    // Hack Alert: cy.wait('@getProfileList') is too flaky, so we wait a second
    cy.wait(1000);
    // Add fake profiles
    cy.then(() => {
      let fakeProfileList = [];
      let supportedWAProfileTypeList = tg.$g.store.getters['profile/supportedWAProfileTypeList'];
      for (let i = 0; i < supportedWAProfileTypeList.length; i++) {
        let waProfileType = supportedWAProfileTypeList[i];
        fakeProfileList.push({ smID: i, profileGroupID: i * i, type: waProfileType });
      }
      tg.$g.store.commit('profile/setWAProfileList', fakeProfileList);
    });
    cy.get('[data-cy=disabledSidebarItem] [data-cy=addProfileIcon]').should('be.visible');
  });
});


/* ------------------------ FACEBOOK SIDEBAR ------------------------------- */
describe('ProfileGroupDrillDown: Facebook Sidebar', function () {

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
  });

  it('Navigation', function () {
    // Note: Had to add {force:true} to clicks due to intermittent CI failures
    // 1st Level: Manage, Overview
    cy.get('[data-cy=facebookProfileSidebarItem]').should('be.visible');
    cy.get('[data-cy=facebookProfileSidebarItem]').click({ force: true });
    cy.get('[data-cy=facebookProfileOverviewSidebarItem]').click({ force: true });
    cy.get('[data-cy=facebookProfileOverviewCard]').should('be.visible');
    cy.get('[data-cy=facebookProfileManageSidebarItem]').click({ force: true });
    cy.get('[data-cy=facebookProfileManageCard]').should('be.visible');

    // 2nd Level: Posts
    cy.get('[data-cy=facebookPostsSidebarItem]').should('be.visible');
    cy.get('[data-cy=facebookPostsSidebarItem]').click({ force: true });
    cy.get('[data-cy=facebookPostAllSidebarItem]').click({ force: true }); /* All */
    cy.get('[data-cy=facebookPostAllCard]').should('be.visible');
    cy.get('[data-cy=facebookPostSentimentSidebarItem]').click({ force: true }); /* Sentiment */
    cy.get('[data-cy=facebookPostSentimentCard]').should('be.visible');
    cy.get('[data-cy=facebookPostFrequencySidebarItem]').click({ force: true }); /* Frequency */
    cy.get('[data-cy=facebookPostFrequencyCard]').should('be.visible');
    cy.get('[data-cy=facebookPostLikesSidebarItem]').click({ force: true }); /* Likes */
    cy.get('[data-cy=facebookPostLikesCard]').should('be.visible');
    cy.get('[data-cy=facebookPostCommentsSidebarItem]').click({ force: true }); /* Comments */
    cy.get('[data-cy=facebookPostCommentsCard]').should('be.visible');

  });
});
