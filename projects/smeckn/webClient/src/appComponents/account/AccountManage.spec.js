/**
 * @file Account Manage Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ ACCOUNT MANAGE --------------------------------- */
describe('Account Manage', function () {
  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => { await tg.$g.router.push({ name: 'rnAccountManage' }); });
  });

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknCardTitle]').should('be.visible').should('contain', 'Manage Account');
      cy.get('[data-cy=smecknContentSectionHeader]').should('be.visible').should('contain', 'Basic Information');
      cy.get('[data-cy=smecknContentSectionHeader]').should('be.visible').should('contain', 'Update Password');
    }
  });
});


/* ---------------------- UPDATE PASSWORD ---------------------------------- */
describe('Account Manage: Update Password', function () {
  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(async () => { await tg.$g.router.push({ name: 'rnAccountManage' }); });
  });

  /**
   * Test filling out and submitting create form
   */
  function testForm({
    password = Cypress.env('TEST_PASSWORD'),
    newPassword = Cypress.env('TEST_PASSWORD'),
    confirmNewPassword = Cypress.env('TEST_PASSWORD'),
    errorMsg = '',
    click = true,
    clear = false
  } = {}) {
    // Password
    cy.get('[data-cy=passwordInput]').clear();
    if (password !== '') { cy.get('[data-cy=passwordInput]').type(password); }
    // New Password
    cy.get('[data-cy=newPasswordInput]').clear();
    if (newPassword !== '') { cy.get('[data-cy=newPasswordInput]').type(newPassword); }
    // Confirm New Password
    cy.get('[data-cy=confirmNewPasswordInput]').clear();
    if (confirmNewPassword !== '') { cy.get('[data-cy=confirmNewPasswordInput]').type(confirmNewPassword); }
    // Click
    if (click) { cy.get('[data-cy=saveButton]').click(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
    // Clear
    if (clear) { cy.get('[data-cy=clearButton]').click(); }
  }

  it('No password', function () {
    testForm({ password: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORD });
  });
  it('No new password', function () {
    testForm({ newPassword: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_NEW_PASSWORDS });
  });
  it('New passwords different', function () {
    testForm({ newPassword: 'asdfasdf', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_NEW_PASSWORDS });
  });
  it('Invalid password', function () {
    testForm({ password: 'asdfasdfasdf', errorMsg: 'Invalid credentials' });
  });
  it('Clear', function () {
    testForm({ click: false, clear: true });
    cy.get('[data-cy=passwordInput]').invoke('val').should('have.lengthOf', 0);
    cy.get('[data-cy=newPasswordInput]').invoke('val').should('have.lengthOf', 0);
    cy.get('[data-cy=confirmNewPasswordInput]').invoke('val').should('have.lengthOf', 0);
  });
  it('Success', function () {
    testForm();
    tg.app.verifyAlert({ text: tg.$g.store.state.message.messages.PASSWORD_SUCCESSFULLY_UPDATED });
  });
});
