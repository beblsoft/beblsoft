/**
 * @file FacebookProfile Class
 */

/* ------------------------ IMPORT ----------------------------------------- */
import Profile from '../profile/Profile.js';
import facebookIcon from '../../img/flaticon/facebook.png';


/**
 * FacebookProfile Class
 */
class FacebookProfile extends Profile {

  /* ---------------------- PROPERTIES ------------------------------------- */
  get waProfileType() { /* eslint-disable-line */
    return 'FACEBOOK';
  }
  get title() { /* eslint-disable-line */
    return 'Facebook';
  }
  get iconSrc() { /* eslint-disable-line */
    return facebookIcon;
  }
  get textColor() { /* eslint-disable-line */
    return this.$g.ui.colors.facebook;
  }
  get attributeObj() { /* eslint-disable-line */
    return { 'data-cy': 'facebookCard' };
  }
  get sidebarItem() { /* eslint-disable-line */
    let item = {
      title: 'Facebook Profile',
      icon: { src: facebookIcon },
      attributeObj: { 'data-cy': 'facebookProfileSidebarItem' },
      children: [{
          title: 'Overview',
          onClick: () => { this.$g.router.push({ name: 'rnFacebookProfileOverview' }); },
          activeRouteName: 'rnFacebookProfileOverview',
          attributeObj: { 'data-cy': 'facebookProfileOverviewSidebarItem' }
        },
        {
          title: 'Posts',
          attributeObj: { 'data-cy': 'facebookPostsSidebarItem' },
          children: [{
            title: 'All',
            onClick: () => { this.$g.router.push({ name: 'rnFacebookPostAll' }); },
            activeRouteName: 'rnFacebookPostAll',
            attributeObj: { 'data-cy': 'facebookPostAllSidebarItem' }
          }, {
            title: 'Sentiment',
            onClick: () => { this.$g.router.push({ name: 'rnFacebookPostSentiment' }); },
            activeRouteName: 'rnFacebookPostSentiment',
            attributeObj: { 'data-cy': 'facebookPostSentimentSidebarItem' }
          }, {
            title: 'Frequency',
            onClick: () => { this.$g.router.push({ name: 'rnFacebookPostFrequency' }); },
            activeRouteName: 'rnFacebookPostFrequency',
            attributeObj: { 'data-cy': 'facebookPostFrequencySidebarItem' }
          }, {
            title: 'Likes',
            onClick: () => { this.$g.router.push({ name: 'rnFacebookPostLikes' }); },
            activeRouteName: 'rnFacebookPostLikes',
            attributeObj: { 'data-cy': 'facebookPostLikesSidebarItem' }
          }, {
            title: 'Comments',
            onClick: () => { this.$g.router.push({ name: 'rnFacebookPostComments' }); },
            activeRouteName: 'rnFacebookPostComments',
            attributeObj: { 'data-cy': 'facebookPostCommentsSidebarItem' }
          }],
        },
        {
          title: 'Manage',
          onClick: () => { this.$g.router.push({ name: 'rnFacebookProfileManage' }); },
          activeRouteName: 'rnFacebookProfileManage',
          attributeObj: { 'data-cy': 'facebookProfileManageSidebarItem' }
        }
      ]
    };
    return item;
  }

  /* ---------------------- METHODS ---------------------------------------- */
  async onAdd() { /* eslint-disable-line */
    await this.$g.store.dispatch('facebookProfile/add', { waProfileGroup: this.waProfileGroup });
  }
  async onAddProfileMounted() { /* eslint-disable-line */
    await this.$g.store.dispatch('facebookProfile/logout');
  }
  async onSelect() { /* eslint-disable-line */
    this.$g.router.push({ name: 'rnFacebookPostAll' });
  }
}


/* ------------------------ EXPORT ----------------------------------------- */
export default FacebookProfile;
