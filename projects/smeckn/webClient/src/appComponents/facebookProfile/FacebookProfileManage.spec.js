/**
 * @file Facebook Profile Manage Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Profile Manage', function () {

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => {
      tg.$g.router.push({ name: 'rnFacebookProfileManage' });
      cy.url().should('include', 'facebook');
    });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookProfileManageCard]').should('be.visible');
      cy.get('[data-cy=updateCredentialsButton]').should('be.visible');
      cy.get('[data-cy=deleteModalButton]').should('be.visible');
    }
  });
  it('Cancel Delete', function () {
    cy.get('[data-cy=deleteModalButton]').click();
    cy.get('[data-cy=cancelDeleteButton]').click();
    cy.url().should('contain', 'facebook');
  });
  it('Delete', function () {
    cy.get('[data-cy=deleteModalButton]').click();
    cy.get('[data-cy=deleteButton]').click();
    cy.url().should('contain', 'profileAdd');
    cy.then(() => {
      assert.equal(0, tg.$g.store.state.profile.waProfileList.length, 'No more profiles');
    });
  });
});
