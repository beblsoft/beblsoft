/**
 * @file Facebook Profile Overview Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Facebook Profile Overview', function () {

  beforeEach(function () {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
        profileGroups: true,
        facebookProfiles: true,
        syncsForAllProfilesSuccess: true
      },
      initAccountToken: true
    });
    tg.app.login();
    tg.app.profileGroupDrillDown();
    cy.then(() => {
      tg.$g.router.push({ name: 'rnFacebookProfileOverview' });
      cy.url().should('include', 'facebook');
    });
  });

  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=facebookProfileOverviewCard]').should('be.visible');
      cy.get('[data-cy=facebookProfileName]').should('be.visible');
      cy.get('[data-cy=facebookProfileEmail]').should('be.visible');
      cy.get('[data-cy=facebookProfileGender]').should('be.visible');
      cy.get('[data-cy=facebookProfileBirthdayDate]').should('be.visible');
      cy.get('[data-cy=facebookProfileHometown]').should('be.visible');
      cy.get('[data-cy=facebookProfileID]').should('be.visible');
    }
  });
});
