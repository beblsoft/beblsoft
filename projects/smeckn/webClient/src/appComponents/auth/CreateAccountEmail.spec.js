/**
 * @file Create Account Email Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Create Account Email', function () {
  const RouteType = {
    prop: 0,
    noProp: 1
  };

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Use tg.$g.router to navigate as CreateAccountEmail component requires
   * email prop to be passed
   */
  function setUp({ rType = RouteType.prop } = {}) {
    tg.init();
    cy.then(async () => {
      tg.$g.router.push({
        name: 'rnCreateAccountEmail',
        params: (rType === RouteType.prop) ? { email: Cypress.env('TEST_EMAIL') } : {}
      });
    });
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    setUp();
    cy.url().should('include', '/createAccountEmail');
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
    }
  });

  /* ---------------------- ELEMENTS --------------------------------------- */
  it('Element data', function () {
    setUp();
    cy.get('[data-cy=smecknLogoCard]').should('be.visible')
      .should('contain', 'Check your email')
      .should('contain', Cypress.env('TEST_EMAIL'));
  });

  /* ---------------------- FAILURE ---------------------------------------- */
  it('No prop', function () {
    setUp({ rType: RouteType.noProp });
    cy.url().should('include', '/banner');
    cy.then(() => { tg.app.verifyAlert({ whoops: true }); });
  });
});
