/**
 * @file Create Account Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Create Account', function () {
  beforeEach(function () {
    // Skip on production because we could potentially hit reCAPTCHA
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init();
    cy.visit('/createAccount');
  });

  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting create form
   */
  function testForm({
    email = null,
    password = null,
    confirmPassword = null,
    errorMsg = '',
    click = true
  } = {}) {
    let _email = email !== null ? email : Cypress.env('TEST_EMAIL');
    let _password = password !== null ? password : Cypress.env('TEST_PASSWORD');
    let _confirmPassword = confirmPassword !== null ? confirmPassword : Cypress.env('TEST_PASSWORD');

    // Email
    cy.get('[data-cy=emailInput]').clear();
    if (_email !== '') { cy.get('[data-cy=emailInput]').type(_email); }
    // Password
    cy.get('[data-cy=passwordInput]').clear();
    if (_password !== '') { cy.get('[data-cy=passwordInput]').type(_password); }
    // Confirm Password
    cy.get('[data-cy=confirmPasswordInput]').clear();
    if (_confirmPassword !== '') { cy.get('[data-cy=confirmPasswordInput]').type(_confirmPassword); }
    // Click. First wait for ReCAPTCHA to render
    if (click) {
      cy.get('[data-cy=reCAPTCHARendered]');
      cy.get('[data-cy=createAccountButton]').click();
    }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=createAccountButton]').should('be.visible');
      cy.get('[data-cy=termsAndConditionsButton]').should('be.visible');
      cy.get('[data-cy=privacyPolicyButton]').should('be.visible');
      cy.get('[data-cy=cookiePolicyButton]').should('be.visible');
      cy.get('[data-cy=disclaimerButton]').should('be.visible');
    }
  });

  /* ---------------------- FAILURE CASES ---------------------------------- */
  it('No email', function () {
    testForm({ email: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_EMAIL });
  });
  it('Bad email', function () {
    testForm({ email: 'I_AM_A_BAD_EMAIL', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_EMAIL });
  });
  it('Missing passwords', function () {
    testForm({ password: '', confirmPassword: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
    testForm({ confirmPassword: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
    testForm({ password: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
  });
  it('Different passwords', function () {
    testForm({ password: 'foo', confirmPassword: 'bar', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
  });
  it('Create existing account', function () {
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      }
    });
    testForm({ errorMsg: 'Account with email already exists' });
  });

  /* ---------------------- SUCCESS ---------------------------------------- */
  it('Success', function () {
    let email = Cypress.env('TEST_EMAIL');
    cy.then(async () => { await tg.server.api.fake.delete({ queryParams: { accountEmail: email } }); });
    cy.then(() => { testForm(); });
    cy.url().should('include', '/createAccountEmail');
    cy.get('[data-cy=createAccountEmailPara]').should('contain', Cypress.env('TEST_EMAIL'));
  });
});
