/**
 * @file Forgot Password Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Forgot Password', function () {
  beforeEach(function () {
    // Skip on production because we could potentially hit reCAPTCHA
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      }
    });
    cy.visit('/forgotPassword');
  });

  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting form
   */
  function testForm({
    email = Cypress.env('TEST_EMAIL'),
    errorMsg = '',
    click = true
  } = {}) {
    // Email
    cy.get('[data-cy=emailInput]').clear();
    if (email !== '') { cy.get('[data-cy=emailInput]').type(email); }
    // Click
    if (click) { cy.get('[data-cy=resetPasswordButton]').click(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=emailInput]').should('be.visible');
      cy.get('[data-cy=resetPasswordButton]').should('be.visible');
    }
  });

  /* ---------------------- FAILURE CASES ---------------------------------- */
  it('No email', function () {
    testForm({ email: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_EMAIL });
  });
  it('Bad email', function () {
    testForm({ email: 'I_AM_A_BAD_EMAIL', errorMsg: 'Account does not exist' });
  });

  /* ---------------------- SUCCESS ---------------------------------------- */
  it('Success', function () {
    testForm();
    cy.url().should('include', '/forgotPasswordEmail');
    cy.get('[data-cy=forgotPasswordEmailPara]').should('contain', Cypress.env('TEST_EMAIL'));
  });

});
