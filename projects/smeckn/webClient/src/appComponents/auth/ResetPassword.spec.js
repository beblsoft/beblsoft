/**
 * @file Reset Password Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Reset Password', function () {
  const TokenType = {
    none: 0,
    valid: 1,
    invalid: 2
  };

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Get url from token type
   */
  async function getURLFromTokenType({ tokenType = TokenType.valid } = {}) {
    let waTokenData = '';
    let urlPathStr = '/resetPassword';
    let urlArgsStr = '';

    switch (tokenType) {
    case TokenType.valid:
      waTokenData = await tg.server.api.account.createTokens({
        data: {
          email: Cypress.env('TEST_EMAIL'),
          password: Cypress.env('TEST_PASSWORD')
        }
      });
      urlArgsStr = `?forgotPasswordToken=${waTokenData.forgotPasswordToken}`;
      break;
    case TokenType.invalid:
      urlArgsStr = '?forgotPasswordToken=this.token.will.not.work';
      break;
    case TokenType.none: // Fall through
    default:
      break;
    }
    return `${urlPathStr}${urlArgsStr}`;
  }

  /**
   * SetUp based on token type
   */
  function setUp({ tokenType = TokenType.valid } = {}) {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    cy.then(async () => {
      cy.visit(await getURLFromTokenType({ tokenType }));
    });
  }

  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting form
   */
  function testForm({
    password = '',
    confirmPassword = '',
    errorMsg = '',
    click = true
  } = {}) {
    // Password
    cy.get('[data-cy=passwordInput]').clear();
    if (password !== '') { cy.get('[data-cy=passwordInput]').type(password); }
    // Confirm Password
    cy.get('[data-cy=confirmPasswordInput]').clear();
    if (confirmPassword !== '') { cy.get('[data-cy=confirmPasswordInput]').type(confirmPassword); }
    // Click
    if (click) { cy.get('[data-cy=resetPasswordButton]').click(); }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    setUp();
    cy.url().should('include', '/resetPassword');
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=resetPasswordButton]').should('be.visible');
    }
  });

  /* ---------------------- FAILURE CASES ---------------------------------- */
  it('Missing passwords', function () {
    setUp();
    testForm({ errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
    testForm({ password: 'foo', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
    testForm({ confirmPassword: 'foo', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
  });
  it('Bad passwords', function () {
    setUp();
    testForm({ password: 'foo', confirmPassword: 'bar', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORDS });
  });
  it('Bad token', function () {
    cy.visit('/resetPassword?forgotPasswordToken=badToken.will.notwork');
    let password = 'blahblahblah';
    testForm({ password, confirmPassword: password });
    tg.app.verifyAlert({ text: tg.$g.store.state.message.messages.PASSWORD_RESET_TRY_AGAIN });
    cy.url().should('include', '/forgotPassword');
  });
  it('No token', function () {
    cy.visit('/resetPassword');
    cy.then(() => { tg.app.verifyAlert({ whoops: true }); });
    cy.url().should('include', '/banner');
  });

  /* ---------------------- SUCCESS CASE ----------------------------------- */
  it('Success', function () {
    setUp();
    let password = 'blahblahblah';
    testForm({ password, confirmPassword: password });
    cy.url().should('include', '/login');
    // Verify password updated
    cy.then(async () => {
      let waLoginData = await tg.server.api.auth.login({
        data: {
          email: Cypress.env('TEST_EMAIL'),
          password,
          reCAPTCHAToken: Cypress.env('RECAPTCHA_PASSTHROUGH_TOKEN')
        }
      });
      expect(waLoginData.account).to.have.property('email');
    });
  });

});
