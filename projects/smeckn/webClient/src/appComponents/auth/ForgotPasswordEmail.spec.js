/**
 * @file Forgot Password Email Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Forgot Password Email', function () {
  const RouteType = {
    prop: 0,
    noProp: 1
  };

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Setup, optionally passing the correct route email prop
   */
  function setUp({ rType = RouteType.prop } = {}) {
    tg.init();
    cy.then(async () => {
      tg.$g.router.push({
        name: 'rnForgotPasswordEmail',
        params: (rType === RouteType.prop) ? { email: Cypress.env('TEST_EMAIL') } : {}
      });
    });
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=forgotPasswordEmailPara]').should('be.visible');
    }
  });

  /* ---------------------- ELEMENTS --------------------------------------- */
  it('Element data', function () {
    setUp();
    cy.url().should('include', '/forgotPasswordEmail');
    cy.get('[data-cy=smecknLogoCard]')
      .should('be.visible')
      .should('contain', 'Check your email');
    cy.get('[data-cy=forgotPasswordEmailPara]')
      .should('be.visible')
      .should('contain', Cypress.env('TEST_EMAIL'));
  });

  /* ---------------------- FAILURE ---------------------------------------- */
  it('No prop', function () {
    setUp({ rType: RouteType.noProp });
    cy.url().should('include', '/banner');
    cy.then(() => { tg.app.verifyAlert({ whoops: true }); });
  });
});
