/**
 * @file Confirm Account Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Confirm Account', function () {
  const TokenType = {
    none: 0,
    valid: 1,
    invalid: 2
  };

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Get url from token type
   * Returns
   *   '/confirmAccount?createAccountToken=<specific token type>'
   */
  async function getURLFromTokenType({ tokenType = TokenType.valid } = {}) {
    let waTokenData = '';
    let urlPathStr = '/confirmAccount';
    let urlArgsStr = '';

    switch (tokenType) {
    case TokenType.valid:
      waTokenData = await tg.server.api.account.createTokens({
        data: { email: Cypress.env('TEST_EMAIL'), password: Cypress.env('TEST_PASSWORD') }
      });
      urlArgsStr = `?createAccountToken=${waTokenData.createAccountToken}`;
      break;
    case TokenType.invalid:
      urlArgsStr = '?createAccountToken=this.token.will.not.work';
      break;
    case TokenType.none: // Fall through
    default:
      urlArgsStr = '';
      break;
    }
    return `${urlPathStr}${urlArgsStr}`;
  }

  /**
   * Setup based on token type
   */
  function setUp({ tokenType = TokenType.valid } = {}) {
    tg.init();
    tg.server.init();
    cy.then(async () => {
      let urlPath = await getURLFromTokenType({ tokenType });
      cy.log(`Visiting ${urlPath}`);
      cy.visit(urlPath);
    });
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it.skip('Viewports', function () {
    // Difficult to get the timing right for this test.
    // ConfirmAccount route is immediately navigated from when account is created.
  });

  /* ---------------------- FAILURE CASES ---------------------------------- */
  it('No token', function () {
    setUp({ tokenType: TokenType.none });
    cy.then(() => { tg.app.verifyAlert({ whoops: true }); });
    cy.url().should('include', '/banner');
  });
  it('Bad token', function () {
    setUp({ tokenType: TokenType.invalid });
    cy.then(() => { tg.app.verifyAlert({ text: 'Invalid credentials' }); });
    cy.url().should('include', '/banner');
  });

  /* ---------------------- SUCCESS ---------------------------------------- */
  it('Success', function () {
    setUp({ tokenType: TokenType.valid });
    cy.url().should('include', '/login');
    cy.then(() => { tg.app.verifyAlert({ text: tg.$g.store.state.message.messages.ACCOUNT_SUCCESSFULLY_CREATED }); });
  });

});
