/**
 * @file Login Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Login', function () {
  beforeEach(function () {
    // Skip on production because we could potentially hit reCAPTCHA
    tg.init();
    cy.then(() => { if (tg.$g.process.env.VUE_APP_ENVIRONMENT === 'Production') { this.skip(); } });
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      }
    });
    cy.visit('/login');
  });

  /* ---------------------- FORM COMPLETION -------------------------------- */
  /**
   * Test filling out and submitting form
   */
  function testForm({
    email = Cypress.env('TEST_EMAIL'),
    password = Cypress.env('TEST_PASSWORD'),
    errorMsg = '',
    click = true,
    verifySuccess = false,
  } = {}) {
    // Email
    cy.get('[data-cy=emailInput]').clear();
    if (email !== '') { cy.get('[data-cy=emailInput]').type(email); }
    // Password
    cy.get('[data-cy=passwordInput]').clear();
    if (password !== '') { cy.get('[data-cy=passwordInput]').type(password); }
    // Click. First wait for ReCAPTCHA to render
    if (click) {
      cy.get('[data-cy=reCAPTCHARendered]');
      cy.get('[data-cy=loginButton]').click();
    }
    // Error Message
    if (errorMsg !== '') { cy.get('[data-cy=errorMessagePara]').should('contain', errorMsg); }
    // Verify success
    if (verifySuccess) {
      cy.url().should('include', '/profileGroupsDashboard');
      cy.getCookie('__smeckn_auth_token').then((cookie) => {
          assert.equal(cookie.httpOnly, true);
          assert.equal(cookie.secure, true);
        });
      cy.getCookie('__smeckn_account_token').then((cookie) => {
        assert.equal(cookie.httpOnly, true);
        assert.equal(cookie.secure, true);
      });
    }
  }

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=loginButton]').should('be.visible');
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Navigation', function () {
    cy.get('[data-cy=forgotPasswordButton]').click();
    cy.url().should('include', '/forgotPassword');
  });

  /* ---------------------- FAILURE CASES ---------------------------------- */
  it('No email', function () {
    testForm({ email: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_EMAIL });
  });
  it('Bad email', function () {
    testForm({ email: 'I_AM_A_BAD_EMAIL', errorMsg: 'Invalid credentials' });
  });
  it('No password', function () {
    testForm({ password: '', errorMsg: tg.$g.store.state.message.messages.SPECIFY_VALID_PASSWORD });
  });
  it('Bad password', function () {
    testForm({ password: 'ThisWillNotWork', errorMsg: 'Invalid credentials' });
  });

  /* ---------------------- SUCCESS CASE ----------------------------------- */
  it('Success', function () {
    cy.then(() => { testForm({ verifySuccess: true }); });
  });
});
