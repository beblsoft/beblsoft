/**
 * @file Sync Bar Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ BASIC ------------------------------------------ */
describe('SyncBar', function () {

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * SetUp individual tests
   */
  beforeEach(function () {
    cy.then(() => {
      tg.init();
      tg.server.init({
        fakeCreateInputData: {
          accountEmail: Cypress.env('TEST_EMAIL'),
          accountPassword: Cypress.env('TEST_PASSWORD'),
          profileGroups: true,
          facebookProfiles: true,
        },
        initAccountToken: true
      });
      tg.app.login();
      tg.app.profileGroupDrillDown();
      cy.then(() => { cy.spy(tg.$g.syncBus, 'complete').as('syncBusComplete'); });
    });
  });

  /**
   * Stub Get Latest Sync
   */
  function stubSyncGetLatest({
    latestExists = true,
    smID = 20,
    profileSMID = 20,
    status = 'IN_PROGRESS',
    createDate = null,
    restartDate = null,
    errorSeen = false,
    delayMS = 200
  } = {}) {

    let response = [];
    if (latestExists) {
      response = [{
        smID,
        profileSMID,
        createDate: createDate ? createDate : Cypress.moment.utc().subtract(2, 'seconds').toISOString(),
        restartDate: restartDate ? restartDate : Cypress.moment.utc().add(2, 'days').toISOString(),
        status: status,
        errorSeen
      }];
    }
    cy.server();
    cy.route({
      method: 'GET',
      url: '**/sync*latest=true*',
      response,
      delay: delayMS,
    }).as('getLatest');
  }

  /**
   * Stub Get Sync Progress
   */
  function stubSyncGetProgress({
    status = 'IN_PROGRESS'
  } = {}) {
    cy.server();
    cy.route({
      method: 'GET',
      url: '**/sync/*/progress',
      response: {
        contentProgressList: [
          { contentType: 'POST', count: 398, status },
          { contentType: 'PHOTO', count: 398, status }
        ],
        status
      },
      delay: 200,
    }).as('getProgress');
  }

  /**
   * Stub Create Sync
   */
  function stubSyncCreate({
    smID = 20,
    profileSMID = 20,
    status = 'IN_PROGRESS',
    createDate = null,
    restartDate = null,
    errorSeen = false,
    delayMS = 200
  } = {}) {
    cy.server();
    cy.route({
      method: 'POST',
      url: '**/sync',
      response: {
        smID,
        profileSMID,
        createDate: createDate ? createDate : Cypress.moment.utc().subtract(2, 'seconds').toISOString(),
        restartDate: restartDate ? restartDate : Cypress.moment.utc().add(2, 'days').toISOString(),
        status: status,
        errorSeen
      },
      delay: delayMS,
    }).as('create');
  }

  /**
   * Stub Update Sync
   */
  function stubSyncUpdate() {
    cy.server();
    cy.route({
      method: 'PUT',
      url: '**/sync/*',
      response: {},
    }).as('update');
  }

  /**
   * Drill Down into a profile, this will trigger Sync Bar
   */
  function profileDrillDown() {
    cy.then(() => {
      tg.$g.router.push({ name: 'rnFacebookProfileManage' });
      cy.url().should('include', 'facebook');
    });
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('waProfile nulled out', function () {
    stubSyncGetLatest({ status: 'TIMED_OUT' });
    profileDrillDown();
    cy.get('[data-cy=syncBar]').should('be.visible');
    cy.get('[data-cy=syncErrorMessagePara]').should('be.visible');
    cy.then(() => { tg.$g.store.commit('profile/clearWAProfileList'); });
    cy.get('[data-cy=syncBar]').should('not.be.visible');

  });

  it('refresh: Should start sync if no sync', function () {
    stubSyncGetLatest({ latestExists: false });
    stubSyncCreate();
    stubSyncGetProgress();
    profileDrillDown();
    cy.wait('@create');
    cy.wait('@getProgress');
    cy.get('[data-cy=syncBarProgress]').should('be.visible');
  });

  it('refresh: Should start sync if sync restartable', function () {
    stubSyncGetLatest({ restartDate: Cypress.moment.utc().toISOString() });
    stubSyncCreate();
    stubSyncGetProgress();
    profileDrillDown();
    cy.wait('@create');
    cy.wait('@getProgress');
    cy.get('[data-cy=syncBarProgress]').should('be.visible');
  });

  it('refresh: Should display sync progress if in progress', function () {
    stubSyncGetLatest({ status: 'IN_PROGRESS' });
    stubSyncGetProgress();
    profileDrillDown();
    cy.wait('@getProgress');
    cy.get('[data-cy=syncBarProgress]').should('be.visible');
  });

  it('refresh: Should not display sync if sync succeeded', function () {
    stubSyncGetLatest({ status: 'SUCCESS' });
    profileDrillDown();
    cy.get('[data-cy=syncBar]').should('not.be.visible');
  });

  it('refresh: Should handle errors appropriately', function () {
    stubSyncGetLatest({ status: 'CREDENTIALS_EXPIRED', errorSeen: false });
    stubSyncUpdate();
    profileDrillDown();
    cy.get('[data-cy=syncErrorMessagePara]').should('be.visible');
    cy.get('[data-cy=syncErrorCloseButton]').should('be.visible');
    stubSyncGetLatest({ status: 'CREDENTIALS_EXPIRED', errorSeen: true });
    cy.get('[data-cy=syncErrorCloseButton]').click();
    cy.wait('@update');
    cy.wait('@getLatest');
    cy.get('[data-cy=syncErrorMessagePara]').should('not.be.visible');
    cy.get('[data-cy=syncErrorCloseButton]').should('not.be.visible');
  });

  it('displayProgress: Progress leading to success', function () {
    let minProgressAttempts = 10;
    stubSyncGetLatest({ status: 'IN_PROGRESS' });
    stubSyncGetProgress();
    profileDrillDown();
    for (let i = 0; i < minProgressAttempts; i++) {
      cy.wait('@getProgress');
      cy.get('[data-cy=syncBarProgress]').should('be.visible');
    }
    stubSyncGetLatest({ status: 'SUCCESS' });
    stubSyncGetProgress({ status: 'SUCCESS' });
    cy.get('[data-cy=syncBarSuccess]').should('be.visible');
    cy.wait('@getLatest');
    cy.get('[data-cy=syncBarSuccess]').should('not.be.visible');
    cy.get('[data-cy=syncBar]').should('not.be.visible');
    cy.then(() => { let _ = expect(this.syncBusComplete).to.be.called; });
  });

  it('displayProgress: Progress leading to failure', function () {
    stubSyncGetLatest({ status: 'IN_PROGRESS' });
    stubSyncGetProgress();
    profileDrillDown();
    cy.get('[data-cy=syncBarProgress]').should('be.visible');
    stubSyncGetLatest({ status: 'TIMED_OUT' });
    stubSyncGetProgress({ status: 'TIMED_OUT' });
    cy.wait('@getLatest');
    cy.get('[data-cy=syncBar]').should('be.visible');
    cy.get('[data-cy=syncBarFailure]').should('be.visible');
  });
});
