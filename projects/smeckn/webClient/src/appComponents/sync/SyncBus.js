/**
 * @file Sync Bus Implementation
 *
 * @description Facilatates communication between application and sync bar
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';


/* ------------------------ SYNC BUS CLASS --------------------------------- */
/**
 * Manage performing sync
 */
export default class SyncBus {

  /**
   * @constructor
   */
  constructor() {
    this.bus = new Vue();
  }

  /**
   * Allows any component to trigger a profile sync refresh
   *
   * Example component usage:
   *   this.$g.syncBus.refresh();
   */
  refresh() {
    this.bus.$emit('refresh');
  }

  /**
   * Called when the active profile sync is complete
   */
  complete() {
    this.bus.$emit('complete');
  }

}
