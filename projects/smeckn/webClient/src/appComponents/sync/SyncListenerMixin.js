/*
  NAME
    SyncListenerMixin.js

  DESCRIPTION
    Mixin Component
    Allows components to listen to when a sync completes so they can reload
    their backing data.

    For example, FacebookPostFrequency component needs to reload its data after
    the facebook profile sync completes.

  USAGE IN COMPONENT
    export default {
      mixins: [SyncListenerMixin],
      methods: {
        onSyncComplete() {...}
      }
    }
*/

/* ------------------------ MIXIN ------------------------------------------ */
export default {

  /* ---------------------- LIFECYCLE -------------------------------------- */
  mounted() {
    this.$g.syncBus.bus.$on('complete', this.onSyncComplete);
  },
  destroyed() {
    this.$g.syncBus.bus.$off('complete', this.onSyncComplete);
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {

    /**
     * Handle Sync Complete
     */
    onSyncComplete() {
      this.$g.utils.softAssert({
        condition: false,
        message: 'onSyncComplete must be implemented!'
      });
    },
  }
};
