/**
 * @file Alert Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Alert', function () {
  let text = 'Hello Alert!';
  let alertSeverityList = [
    'primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'
  ];

  beforeEach(function () {
    tg.init();
  });

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      let severity = 'success';
      let dismissSecs = 1;
      cy.viewport(viewport.width, viewport.height);
      cy.then(() => { tg.$g.alertBus.show({ text, severity, dismissSecs }); });
      cy.then(() => { tg.app.verifyAlert({ text, severity, dismissSecs }); });
    }
  });

  /* ---------------------- COLOR ------------------------------------------ */
  it('Color', function () {
    for (let i = 0; i < alertSeverityList.length; i++) {
      let severity = alertSeverityList[i];
      let dismissSecs = 1;
      cy.then(() => {
        cy.log(severity);
        tg.$g.alertBus.show({ text, severity, dismissSecs });
      });
      cy.then(() => { tg.app.verifyAlert({ text, severity, dismissSecs, waitNotVisible: true }); });
    }
  });

  /* ---------------------- COUNTDOWN -------------------------------------- */
  it('Countdown', function () {
    // Disabled countdown, but still having this test run in case it goes back in
    let severity = 'success';
    let dismissSecs = 3;
    cy.then(() => { tg.$g.alertBus.show({ text, severity, dismissSecs }); });
    cy.then(() => { tg.app.verifyAlert({ text, severity, dismissSecs, countdown: true, waitNotVisible: true }); });
  });
});
