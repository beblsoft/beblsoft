/**
 * @file Manage Alerts Communication
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';


/* ------------------------ ALERT BUS CLASS -------------------------------- */
/**
 * Manage communication between application and alert dialog
 */
export default class AlertBus {

  /**
   * @constructor
   */
  constructor() {
    this.bus = new Vue();
  }

  /**
   * Show an alert to the user
   * @param  {string} options.text        String to display to user
   * @param  {string} options.severity    Severity of message.
   *                                      One of: 'primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'
   * @param  {number} options.dismissSecs Duration in seconds to display message
   *
   * Example component usage:
   *   this.$g.alertBus.show({text: 'Demo alert', severity: 'info'});
   */
  show({ text = null, severity = 'warning', dismissSecs = 5 } = {}) {
    this.bus.$emit('show', { text, severity, dismissSecs });
  }
}
