/**
 * @file Banner Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Banner', function () {

  /* ---------------------- BEFORE EACH ------------------------------------ */
  beforeEach(function () {
    tg.init();
  });

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];

      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=tagline]').should('be.visible');
      cy.get('[data-cy=signUpForFreeButton]').should('be.visible');
    }
  });

  it('Document', function () {
    cy.document().its('contentType').should('eq', 'text/html');
  });

  it('Element text', function () {
    cy.get('[data-cy=tagline]').should('be.visible');
    cy.get('[data-cy=signUpForFreeButton]').should('be.visible');
    cy.get('[data-cy=loginButton]').should('be.visible');
  });

  it('Navigation', function () {
    cy.get('[data-cy=signUpForFreeButton]').click();
    cy.url().should('include', '/createAccount');

    cy.go('back');

    cy.get('[data-cy=loginButton]').click();
    cy.url().should('include', '/login');
  });

  it('TryLogin: Persists login state', function () {
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    tg.app.login();
    cy.then(() => {
      cy.visit('/');
      cy.url().should('include', 'profileGroupsDashboard');
    });
  });
});
