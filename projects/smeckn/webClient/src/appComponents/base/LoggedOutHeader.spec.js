/**
 * @file LoggedOutHeader Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ LOGGED OUT ------------------------------------- */
describe('Logged Out Header', function () {
  let collapsibleWidth = 600;

  /* ---------------------- VIEWPORTS -------------------------------------- */
  it('Viewports', function () {
    tg.init();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.then(() => {
        if (viewport.width > collapsibleWidth) {
          cy.get('[data-cy=goToUserGuideBtn]').should('be.visible');
          cy.get('[data-cy=createAccountBtn]').should('be.visible');
          cy.get('[data-cy=loginBtn]').should('be.visible');
        }
      });
    }
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Logged Out', function () {
    tg.init();
    // Create account
    cy.get('[data-cy=createAccountBtn]').click();
    cy.url().should('include', '/createAccount');

    // Login
    cy.get('[data-cy=loginBtn]').click();
    cy.url().should('include', '/login');

    // Home
    cy.get('[data-cy=homeLink]').click();
    cy.url().should('include', '/banner');
  });
});
