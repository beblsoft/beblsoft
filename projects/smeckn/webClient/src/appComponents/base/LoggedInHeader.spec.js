/**
 * @file LoggedInHeader Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ LOGGED OUT ------------------------------------- */
describe('Logged In Header', function () {
  let collapsibleWidth = 600;
  let profileGroupPrefix = 'ProfileGroup';

  /* ---------------------- HELPER FUNCTIONS ------------------------------- */
  /**
   * Log the user in and wait for first profile group
   */
  function setUp({ nProfileGroups = 1 } = {}) {
    tg.init();
    tg.server.init({
      fakeCreateInputData: {
        accountEmail: Cypress.env('TEST_EMAIL'),
        accountPassword: Cypress.env('TEST_PASSWORD'),
      },
      initAccountToken: true
    });
    cy.then(async () => {
      for (let i = 0; i < nProfileGroups; i++) {
        await tg.server.api.profileGroup.create({ data: { name: `${profileGroupPrefix}${i}` } });
      }
    });
    tg.app.login();
    cy.url().should('include', '/profileGroupsDashboard');
  }

  /* ---------------------- TESTS ------------------------------------------ */
  it('Viewports', function () {
    setUp();
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.then(() => {
        if (viewport.width > collapsibleWidth) {
          cy.get('[data-cy=profileGroupDropdown]').should('be.visible');
          cy.get('[data-cy=accountDropdown]').should('be.visible');
        }
      });
    }
  });

  it('Text Analysis Navigation', function () {
    setUp();
    cy.get('[data-cy=textAnalysisLink]').click();
    cy.url().should('include', '/customTextAnalysis');
  });

  it('Profile Group Navigation: 1 Profile Group', function () {
    // Add click({ force: true }) as occasionally the dropdowns don't open
    setUp();

    // Select Profile Group
    cy.get('[data-cy=homeLink]').click({ force: true });
    cy.url().should('include', '/profileGroupsDashboard');

    // Profile Group Dropdown
    cy.get('[data-cy=profileGroupDropdown]').click({ force: true });
    cy.get('[data-cy=profileGroupDropdownProfileGroup]').click({ force: true });
    cy.url().should('include', '/profileGroupDrillDown');
    cy.url().should('include', '/profileAdd');

    // Add Profile Group
    cy.get('[data-cy=profileGroupDropdown]').click({ force: true });
    cy.get('[data-cy=addProfileGroupLink]').click({ force: true });
    cy.url().should('include', '/profileGroupAdd');

    // Manage Profile Group
    cy.get('[data-cy=profileGroupDropdown]').click({ force: true });
    cy.get('[data-cy=manageProfileGroupsLink]').click({ force: true });
    cy.url().should('include', '/profileGroupsManage');
  });

  it('Profile Group Navigation: Max Profile Groups', function () {
    let nProfileGroups = null;
    tg.init(); /* Load constants */
    cy.then(() => {
      nProfileGroups = tg.$g.store.state.constant.waConstants.MAX_PROFILEGROUPS;
      setUp({ nProfileGroups });
    });

    // Verify Add Profile Group not visible
    cy.get('[data-cy=profileGroupDropdown]').click();
    cy.get('[data-cy=addProfileGroupLink]').should('not.be.visible');
  });

  it('Account Navigation', function () {
    setUp();

    // Links that stay within app
    let tagURLList = [
      { tag: 'manageAccountLink', url: '/accountManage', click: true },
      { tag: 'unitBalanceLink', url: '/unitBalance', click: true },
      { tag: 'purchaseUnitsLink', url: '/unitCheckout', click: true },
      { tag: 'creditCardsLink', url: '/creditCards', click: true },
      { tag: 'paymentHistoryLink', url: '/paymentHistory', click: true },
      { tag: 'userGuideLink', click: false }
    ];
    for (let i = 0; i < tagURLList.length; i++) {
      let tagURL = tagURLList[i];
      cy.wait(200); // Wait hack: Sometimes Aneed to fix accountDropdown not being visible
      cy.get('[data-cy=accountDropdown]').click();
      cy.get(`[data-cy=${tagURL.tag}]`).should('be.visible');
      if (tagURL.click) {
        // Click button and verify link
        cy.get(`[data-cy=${tagURL.tag}]`).click();
        cy.url().should('include', tagURL.url);
      } else {
        // Click out of dropdown. Used for links that go to user guide.
        cy.get('[data-cy=accountDropdown]').click();
      }
    }

    // Logout
    cy.get('[data-cy=accountDropdown]').click();
    cy.get('[data-cy=logoutLink]').click();
    cy.url().should('include', '/banner');
    cy.getCookie('__smeckn_auth_token').should('be', null);
    cy.getCookie('__smeckn_account_token').should('be', null);
  });
});
