/**
 * @file Footer Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Footer', function () {
  beforeEach(function () {
    tg.init();
  });

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=copyrightPara]').should('be.visible');

      cy.get('[data-cy=facebookNetwork]').should('be.visible');
      cy.get('[data-cy=facebookImage]').should('be.visible');
      cy.get('[data-cy=facebookNetwork]').should('contain', 'Facebook');

      cy.get('[data-cy=twitterNetwork]').should('be.visible');
      cy.get('[data-cy=twitterImage]').should('be.visible');
      cy.get('[data-cy=twitterNetwork]').should('contain', 'Twitter');

      cy.get('[data-cy=linkedinNetwork]').should('be.visible');
      cy.get('[data-cy=linkedinImage]').should('be.visible');
      cy.get('[data-cy=linkedinNetwork]').should('contain', 'LinkedIn');

      cy.get('[data-cy=pinterestNetwork]').should('be.visible');
      cy.get('[data-cy=pinterestImage]').should('be.visible');
      cy.get('[data-cy=pinterestNetwork]').should('contain', 'Pinterest');

      cy.get('[data-cy=redditNetwork]').should('be.visible');
      cy.get('[data-cy=redditImage]').should('be.visible');
      cy.get('[data-cy=redditNetwork]').should('contain', 'Reddit');

      cy.get('[data-cy=whatsappNetwork]').should('be.visible');
      cy.get('[data-cy=whatsappImage]').should('be.visible');
      cy.get('[data-cy=whatsappNetwork]').should('contain', 'Whatsapp');

      cy.get('[data-cy=emailNetwork]').should('be.visible');
      cy.get('[data-cy=emailImage]').should('be.visible');
      cy.get('[data-cy=emailNetwork]').should('contain', 'Email');
    }
  });

  /* ---------------------- ELEMENTS --------------------------------------- */
  it('Element text', function () {
    // Copyright
    let date = new Date();
    cy.get('[data-cy=copyrightPara]')
      .should('contain', 'Copyright')
      .should('contain', date.getFullYear())
      .should('contain', 'Smeckn, Inc.')
      .should('contain', 'All rights reserved')
      .should('contain', '(Beta Release)');
  });
});
