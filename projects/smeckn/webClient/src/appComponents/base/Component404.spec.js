/**
 * @file Component404 Tests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { tg } from '../../../cypress/testGlobal';


/* ------------------------ TESTS ------------------------------------------ */
describe('Component404', function () {
  beforeEach(function () {
    tg.init();
    cy.visit('/ThisPageDoesNotExist');
    cy.url().should('include', '/ThisPageDoesNotExist');
  });

  /* ---------------------- VIEWPORT --------------------------------------- */
  it('Viewports', function () {
    for (let i = 0; i < tg.viewports.length; i++) {
      let viewport = tg.viewports[i];
      cy.viewport(viewport.width, viewport.height);
      cy.get('[data-cy=smecknLogoCard]').should('be.visible');
      cy.get('[data-cy=goHomeButton]').should('be.visible');
    }
  });

  /* ---------------------- ELEMENTS --------------------------------------- */
  it('Element text', function () {
    cy.get('[data-cy=smecknLogoCard]')
      .should('contain', 'Whoops, well that\'s embarassing...')
      .should('contain', 'The page you are navigating to does not exist.')
      .should('contain', 'Let\'s get you back home.');
    cy.get('[data-cy=goHomeButton]')
      .should('contain', 'Go Home');
  });

  /* ---------------------- NAVIGATION ------------------------------------- */
  it('Navigation', function () {
    cy.get('[data-cy=goHomeButton]').click();
    cy.url().should('include', '/banner');
  });

});
