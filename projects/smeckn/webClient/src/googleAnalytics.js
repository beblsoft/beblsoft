/**
 * @file Google Analytics Functionality
 */


/* ------------------------ IMPORTS ---------------------------------------- */
import Vue from 'vue';
import VueAnalytics from 'vue-analytics';


/* ------------------------ GOOGLE ANALYTICS CLASS ------------------------- */
/**
 * Encapsulate google analytics state
 */
export default class GoogleAnalytics {

  /**
   * @constructor
   * @param {Global} g
   */
  constructor({ $g = null } = {}) {
    this.$g = $g;
    let isTestClient = this.$g.cypress.isTestClient;

    Vue.use(VueAnalytics, {
      id: process.env.VUE_APP_GOOGLE_ANALYTICS_WEBCLIENT_ID,
      router: this.$g.router.router,
      ignoreRoutes: [],
      debug: {
        enabled: false,
        trace: true,
        sendHitTask: !isTestClient, // Don't send Cypress clients
      },
      autoTracking: {
        page: true, // Enable page autotracking
        pageviewOnLoad: true, // Send pageview event on load
        screenview: false, // Don't track screens
        pageviewTemplate(route) { // Template to fill out Google Analytics pageview
          return {
            page: `${process.env.VUE_APP_BASE_URL}${route.path}`,
            title: document.title,
            location: window.location.href
          };
        },
        exception: true, // Enable exception tracking
        exceptionLogs: true // Log to console
      },
      beforeFirstHit: this.beforeFirstHit,
      ready: this.ready,
    });

    // setup for optimize
    Vue.$ga.require('GTM-5PPJSP7');
  }

  /* ------------------------ BEFORE HOOKS --------------------------------- */
  /**
   * Before first hit
   */
  beforeFirstHit() {
    // console.log('Google Analytics: Before first hit');
  }

  /**
   * Ready callback
   */
  ready() {
    // console.log('Google Analytics: Ready!');
  }

  /* ------------------------ USER ----------------------------------------- */
  /**
   * Set User
   * @param {int} options.aID Account ID
   */
  setUser({ aID }) {
    Vue.$ga.set({ userId: aID });
    this.sendEvent({ category: 'authentication', action: 'login' });
  }

  /**
   * Clear User
   */
  clearUser() {
    this.sendEvent({ category: 'authentication', action: 'logout' });
    Vue.$ga.set({ userId: '' });
  }

  /* ------------------------ EVENTS --------------------------------------- */
  /**
   * Send and event to google analytics
   * @param  {str} options.category Category
   * @param  {str} options.action   Action
   * @param  {str} options.label    Label
   * @param  {str} options.value   Value
   */
  sendEvent({ category, action, label, value = null } = {}) {
    Vue.$ga.event({
      eventCategory: category,
      eventAction: action,
      eventLabel: label,
      eventValue: value
    });
  }

  /* ------------------------ TRACKING ------------------------------------- */
  /**
   * Disable analytics
   */
  disableTracking() {
    Vue.$ga.disable();
  }

  /**
   * Enable analytics
   */
  enableTracking() {
    Vue.$ga.disable();
  }
}
