#!/usr/bin/env python
"""
 NAME:
  gc.py

DESCRIPTION
 Smeckn WebClient Global Context
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from smeckn.common.constant import Constant


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context
    """

    def __init__(self):
        """
        Initialize Object
        """
        # WebClient ------------------------------
        self.webClient             = BeblsoftAttrDict()
        self.webClient.cfg         = BeblsoftAttrDict()
        self.webClient.cfg.const   = Constant()
        self.webClient.cfg.bSecret = None

        # Log ------------------------------------
        self.bLog          = None
        self.cLogFilterMap = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime     = None
        self.endTime       = None


# ----------------------- GLOBALS ------------------------------------------- #
gc = GlobalContext()
