/**
 * @file Cypress Plugins
 */

/* ------------------------ IMPORTS ---------------------------------------- */
const { addMatchImageSnapshotPlugin } = require('cypress-image-snapshot/plugin');


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = (on, config) => {

  // Solves problem of chrome seeing 404's
  // https://github.com/cypress-io/cypress/issues/1872#issuecomment-450807452
  on('before:browser:launch', (browser = {}, args) => { /* eslint-disable-line */
    if (browser.name === 'chrome') {
      args.push('--proxy-bypass-list=<-loopback>');
      args.push('--disable-site-isolation-trials');
      return args;
    }
  });

  // cypress-image-snapshot
  addMatchImageSnapshotPlugin(on, config);

  // @cypress/browserify-preprocessor
  // This leverages the the browserify settings in package.json to convert the
  // webClient/cypress/*.js files to run in the browser.
  // Changing the package.json browserify settings could result in test errors

  // More plugins here...
};
