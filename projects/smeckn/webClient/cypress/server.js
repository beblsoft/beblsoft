/**
 * @file Server Functionality
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { API } from 'smeckn-web-api';


/* ------------------------------------------------------------------------- */
/**
 * Server Class
 */
export default class Server {
  /**
   * @constructor
   */
  constructor({
    tg = null,
  } = {}) {
    this.tg = tg;
    this.platformOnline = false;
    this.api = null;
    this.waFake = null;
  }

  /**
   * Initialize server
   *
   */
  init({
    fakeDeleteInputData = null,
    fakeCreateInputData = null,
    initAccountToken = false
  } = {}) {
    cy.then(async () => {

      // Setup Test API
      let appAPI = this.tg.$g.server.api;
      this.api = new API({ baseDomain: appAPI.baseDomain, version: appAPI.version, logErrors: true });
      this.api.authToken = Cypress.env('ADMIN_AUTH_TOKEN');

      // Wait for api to come online
      if (!this.platformOnline) {
        await this.api.platform.waitOnline();
        this.platformOnline = true;
      }
      cy.log('Server Platform Online');

      // Delete fake data
      if (fakeDeleteInputData) {
        await this.api.fake.delete({ queryParams: fakeDeleteInputData });
      }

      // Create fake data, will delete existing
      if (fakeCreateInputData) {
        cy.log(fakeCreateInputData);
        this.waFake = await this.api.fake.create({ data: fakeCreateInputData });
        cy.log('Server Fake Data Created');
      }

      // Initialize account token
      if (initAccountToken) {
        this.api.accountToken = this.waFake.popAccountToken;
      }
    })
  }
}
