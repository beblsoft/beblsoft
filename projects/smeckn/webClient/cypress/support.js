/**
 * @file Cypress Support File
 * This support file is automatically run before each test.
 * This support file is a great place to put custom commands or global overrides
 *
 * See Also:
 *   https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests.html#Support-file
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';
import _Cypress from '../src/cypress.js';


/* ------------------------ IMAGE SNAPSHOT --------------------------------- */
/**
 * Register cypress-image-snapshot matchImageSnapshot command
 * so that it can be used during tests
 */
addMatchImageSnapshotCommand({
  failureThreshold: 0.03, // threshold for entire image
  failureThresholdType: 'percent', // percent of image or number of pixels
  customDiffConfig: { threshold: 0.1 }, // threshold for each pixel
  capture: 'viewport', // capture viewport in screenshot
});


/* ------------------------ SET TEST EMAIL --------------------------------- */
/**
 * Set Namespaced Email for each test
 * This prevents tests from deleting each others' data
 *
 * @param {Object} currentTest Mocha Test Object
 */
function setTestEmail(currentTest) {
  let fullTitleAlphaNumeric = currentTest.fullTitle().replace(/[\W]+/g, '_');
  let email = `success+webClientTest-${fullTitleAlphaNumeric}@simulator.amazonses.com`;
  cy.log(`Email: ${email}`);
  Cypress.env('TEST_EMAIL', email);
}


/* ------------------------ GLBAL BEFORE EACH ------------------------------ */
/**
 * Runs before each test
 */
beforeEach(function () {
  // Set Cookie to tell the app that the we are a Cypress Client
  cy.setCookie(_Cypress.testClientCookie, 'true')

  // Setup namespaced test email
  setTestEmail(this.currentTest);
});

