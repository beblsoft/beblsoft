/**
 * @file Test Global Functionality
 */

/* ------------------------------------------------------------------------- */
/**
 * Application Class
 */
export default class Application {
  /**
   * @constructor
   */
  constructor({ tg = null } = {}) {
    this.tg = tg;
  }

  /**
   * Login
   *
   * On Success:
   *   Logs user into application
   */
  login() {
    cy.then(async () => {
      await this.tg.$g.store.dispatch('auth/login', {
        email: Cypress.env('TEST_EMAIL'),
        password: Cypress.env('TEST_PASSWORD'),
        reCAPTCHAToken: Cypress.env('RECAPTCHA_PASSTHROUGH_TOKEN')
      });
      cy.get('[data-cy=profileGroupCard]').should('be.visible');
      cy.log('Logged into application');
    });
  }

  /**
   * Profile Group DrillDown
   *
   * On Success:
   *   Drills down into profile group layer
   *
   * Note:
   *   Requires cy.server() to have been called
   */
  profileGroupDrillDown({ waProfileGroup = null } = {}) {
    cy.then(async () => {
      if (waProfileGroup === null) {
        let waProfileGroupList = await this.tg.server.api.profileGroup.list();
        waProfileGroup = waProfileGroupList[0]; /* eslint-disable-line */
      }
      cy.server();
      cy.route('GET', '*/profile*').as('getProfileList');
      await this.tg.$g.store.dispatch('profileGroup/drillDown', { waProfileGroup });
      cy.wait('@getProfileList');
      cy.url().should('include', '/profileGroupDrillDown');
      cy.log('Drilled down into profile group');
    });
  }

  /**
   * Verify Alert
   *
   * On Success:
   *   Verifies alert
   *
   * @param  {String}  options.text           Alert Text
   * @param  {String}  options.whoops         If true, whoops alert should be shown
   * @param  {String}  options.severity       Alert severity
   * @param  {Number}  options.dismissSecs    Amount of time alert should be shown
   * @param  {Number}  options.countDown      If true, verify that alert counts down
   * @param  {Boolean} options.waitNotVisible If true, verify that alert disappears
   */
  verifyAlert({
    text = null,
    whoops = false,
    severity = null,
    dismissSecs = null,
    countDown = false,
    waitNotVisible = false
  }) {
    let severityColors = {
      primary: 'rgb(217, 232, 244)',
      secondary: 'rgb(234, 234, 234)',
      success: 'rgb(222, 241, 222)',
      danger: 'rgb(247, 221, 220)',
      warning: 'rgb(255, 243, 205)',
      info: 'rgb(209, 236, 241)',
      light: 'rgb(254, 254, 254)',
      dark: 'rgb(214, 216, 217)'
    };

    cy.log('Verifying alert...');

    // Verify text
    cy.get('[data-cy=alertText]').should('be.visible');
    if (text !== null) {
      cy.get('[data-cy=alertText]').should('contain', text);
    }
    // Verify whoops
    if (whoops) {
      cy.get('[data-cy=alertText]').should('contain', this.tg.$g.store.state.message.messages.WHOOPS_SOMETHING_WENT_WRONG);
    }
    // Verify severity color
    if (severity !== null) {
      cy.get('[data-cy=alertBox]').then(($box) => {
        expect(Cypress.$($box).css('backgroundColor')).to.equal(severityColors[severity]);
      });
    }
    // Timer and countdown are disabled for now
    // if (dismissSecs !== null) {
    //   cy.get('[data-cy=alertText]').should('contain', dismissSecs);
    //   if (countDown) {
    //     for (let i = countDown - 1; i >= 1; i--) {
    //       cy.get('[data-cy=alertText]').should('contain', i);
    //     }
    //   }
    // }

    // Verify alert disappears
    if (waitNotVisible) {
      cy.get('[data-cy=alertText]').should('not.be.visible');
    }
  }
}
