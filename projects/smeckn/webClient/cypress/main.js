/**
 * @file Test Functionality
 *
 * Fixes:
 *   - Cypress Console Errors with ENOSPC
 *     See: https://stackoverflow.com/questions/22475849/node-js-what-is-enospc-error-and-how-to-solve
 *     Fix: echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let program = require('commander');
let assert = require('assert');
let cypress = require('cypress');
let path = require('path');
let glob = require('glob');
let waitPort = require('wait-port');


/* ------------------------ PARSE TEST OPTIONS ----------------------------- */
program
  .version('1.0.0')
  .option('--client-domain <domain>', 'Client Domain')
  .option('--client-public-path <path>', 'Client Domain')
  .option('--client-domain-wait-timeout <ms>', 'Time to wait for client before timing out (ms)', 30000)
  .option('--admin-auth-token <token>', 'Admin Auth Token')
  .option('--recaptcha-passthrough-token <token>', 'Recaptcha Passthrough Token')
  .option('--test-password [password]', 'Test Password', '12345678')
  .option('--console', 'Run with cypress console') // Fills program.console
  .option('--total-jobs [nJobs]', 'Total number of jobs running in parallel', 1)
  .option('--job-idx [idx]', 'This job\'s index [0:totalJobs-1]', 0)
  .option('--no-exit', 'Don\'t exit when complete')
  .option('--file-regex [regex]', 'Files to run', '**/*.spec.js')
  .option('--update-snapshots', 'Update all snapshots')
  .option('--no-fail-on-snapshot-diff', 'Update all snapshots')
  .parse(process.argv);

assert(program.clientDomain, 'Specify client domain');
assert(program.clientPublicPath, 'Specify client public path');
assert(program.adminAuthToken, 'Specify admin auth token');
assert(program.recaptchaPassthroughToken, 'Specify ReCAPTCHA PassThrough Token');

/* ------------------------ GLOBALS ---------------------------------------- */
// Find all test files, and then filter them for this particular job
let totalTestFiles = glob.sync(program.fileRegex, {
  cwd: path.resolve(__dirname, '..', 'src'),
  absolute: true,
  ignore: ['**/videos/**/*.spec.js', '**/screenshots/**/*.spec.js']
});
let jobTestFiles = totalTestFiles.filter((value, index, array) => {
  return ((index - program.jobIdx) % program.totalJobs) === 0;
});

// Timeouts: high incase DB needs to load
// Note: CI Timeouts of 180000 resulted in test flake
let consoleWaitTime = 30000; // 30 seconds.
let commandLineWaitTime = 240000; // 2 minutes.

// Find (local) client port
let portRegex = (/:(\d+)/);
let portMatch = portRegex.exec(program.clientDomain);
let port = portMatch ? parseInt(portMatch[1], 10) : null;

// Configure Cypress Module API Args
let args = {
  spec: jobTestFiles, // Controls files tested during cypress.run(args)
  browser: program.console ? 'electron' : 'electron',  // Back to electron as chrome 77 isn't supported
  headed: program.console, // Running headlessly in CI enables video recording
  exit: program.exit,
  env: {
    ADMIN_AUTH_TOKEN: program.adminAuthToken,
    RECAPTCHA_PASSTHROUGH_TOKEN: program.recaptchaPassthroughToken,
    TEST_EMAIL: null, // Set before each test
    TEST_PASSWORD: program.testPassword,
    JOB_IDX: program.jobIdx,
    VIEWPORTS: [
      { name: 'iphone-5', width: 320, height: 568 },
      { name: 'jbensson-android', width: 360, height: 660 },
      { name: 'ipad-2', width: 768, height: 1024 },
      { name: 'default', width: 1000, height: 660 },
      { name: 'macbook-11', width: 1366, height: 768 },
      { name: 'macbook-15', width: 1440, height: 900 }
    ],
  },
  config: {
    baseUrl: `${program.clientDomain}${program.clientPublicPath}`,
    chromeWebSecurity: false, // Display insecure HTTPS content
    fileServerFolder: path.resolve(__dirname, '..'),
    integrationFolder: path.resolve(__dirname, '..', 'src'),
    fixturesFolder: false,
    testFiles: '**/*.spec.js', // Controls console files appearing during cypress.open, (Rel to integrationFolder)
    pluginsFile: path.resolve(__dirname, 'plugins.js'),
    supportFile: path.resolve(__dirname, 'support.js'),
    watchForFileChanges: true,
    numTestsKeptInMemory: program.console ? 5 : 0,
    defaultCommandTimeout: program.console ? consoleWaitTime : commandLineWaitTime,
    execTimeout: program.console ? consoleWaitTime : commandLineWaitTime,
    taskTimeout: program.console ? consoleWaitTime : commandLineWaitTime,
    pageLoadTimeout: program.console ? consoleWaitTime : commandLineWaitTime,
    requestTimeout: program.console ? consoleWaitTime : commandLineWaitTime,
    responseTimeout: program.console ? consoleWaitTime : commandLineWaitTime,

    // Media
    trashAssetsBeforeRun: true,
    screenshotsFolder: path.resolve(__dirname, 'screenshots'),
    video: true,
    videosFolder: path.resolve(__dirname, 'videos'),
    videoCompression: 32, // Lower value results in better quality, but higher file size
    videoUploadOnPasses: false, // Only keep videos on failures
  }
};

// cypress-image-snapshot
// Strange implementation: https://github.com/palmerhq/cypress-image-snapshot/blob/master/src/command.js
if (program.updateSnapshots) { args.env.updateSnapshots = true; }
if (!program.failOnSnapshotDiff) { args.env.failOnSnapshotDiff = false; }


/* ------------------------ MAIN ------------------------------------------- */
/**
 * Cypress Main
 */
async function main() {
  // Wait for (local) port to come online
  // If this isn't done cypress will race with webClient and might not find baseURL
  if (port) {
    let isPortOpen = await waitPort({
      port: port,
      timeout: program.clientDomainWaitTimeout,
      output: 'silent'
    });
    if (!isPortOpen) {
      throw new Error(`Can't reach client domain ${program.clientDomain}`);
    }
  }

  // Execute Cypress
  if (program.console) {
    cypress.open(args);
  } else {
    cypress.run(args);
  }
}
main();
