/**
 * @file Test Global Functionality
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import Server from './server.js';
import Application from './app.js';


/* ------------------------------------------------------------------------- */
/**
 * Test Global Class
 */
class TestGlobal {
  /**
   * @constructor
   */
  constructor() {
    this.$g = null;
    this.app = new Application({ tg: this });
    this.server = new Server({ tg: this });
    this.viewports = Cypress.env('VIEWPORTS');
  }

  /**
   * Initialize global test state
   */
  init() {
    // Visit site
    cy.visit('/');

    // Retrieve app.$g from window
    cy.window().then(async (win) => {
      this.$g = win.app.$g;
      this.$g.googleAnalytics.disableTracking();
    });
  }
}


/* ------------------------ EXPORTS ---------------------------------------- */
export default {
  tg: new TestGlobal()
};
