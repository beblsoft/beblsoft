#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 Configuration Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import abc
import enum
import logging
from pathlib import Path
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.aws.python.EC2.bRegion import BeblsoftRegion
from smeckn.common.constant import Constant


# ------------------------ CONFIG OBJECT ------------------------------------ #
class Config(abc.ABC):
    """
    Configuration Class
    """

    def __init__(self, env):
        """
        Initialize Object
        """
        # Names
        self.env                     = env
        self.name                    = env.name
        self.lName                   = self.name.lower()
        self.uName                   = self.name.upper()

        # Constant
        self.const                   = Constant()

        # AWS Regions
        self.aws                     = BeblsoftAttrDict()
        self.aws.bRegion             = BeblsoftRegion(name="us-east-1")
        self.aws.AZLetters           = ["a", "b", "c", "d", "e", "f"]
        self.aws.AZList              = ["{}{}".format(self.aws.bRegion.name, l) for l in self.aws.AZLetters]

        # Secret
        self.secret                  = BeblsoftAttrDict()
        self.secret.type             = None

        # Buckets
        self.buckets                 = BeblsoftAttrDict()
        self.buckets.privData        = "smeckn-private-data"
        self.buckets.webClient       = None

        # Domains
        self.domains                 = BeblsoftAttrDict()
        self.domains.protocol        = "https"
        self.domains.main            = "smeckn.com"
        self.domains.cookie          = ".{}".format(self.domains.main)
        self.domains.clientList      = []
        self.domains.api             = None

        # Global Database Server
        self.gdbs                    = BeblsoftAttrDict()
        self.gdbs.minCapacity        = None
        self.gdbs.maxCapacity        = None
        self.gdbs.autoPause          = False
        self.gdbs.secUntilAutoPause  = 300

        # Account Database Server
        self.adbs                    = BeblsoftAttrDict()
        self.adbs.count              = 1
        self.adbs.minCapacity        = None
        self.adbs.maxCapacity        = None
        self.adbs.autoPause          = False
        self.adbs.secUntilAutoPause  = 300

        # Manager Lambda Switch
        self.mls                     = BeblsoftAttrDict()
        self.mls.logLevel            = logging.INFO

        # Server Lambda Switch
        self.sls                     = BeblsoftAttrDict()
        self.sls.logLevel            = logging.INFO

        # Email
        self.email                   = BeblsoftAttrDict()
        self.email.domain            = None
        self.email.recipientList     = None
