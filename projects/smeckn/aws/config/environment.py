#!/usr/bin/env python3
"""
NAME:
 environment.py

DESCRIPTION
 Environment Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import enum
from smeckn.aws.config.production import ProductionConfig
from smeckn.aws.config.staging import StagingConfig
from smeckn.aws.config.nightly import NightlyConfig
from smeckn.aws.config.dblack import DBlackConfig
from smeckn.aws.config.jbensson import JBenssonConfig


# ------------------------ ENVIRONMENT -------------------------------------- #
class Environment(enum.Enum):
    """
    Environment Enumeration
    """
    Production = enum.auto()
    Staging    = enum.auto()
    Nightly    = enum.auto()
    DBlack     = enum.auto()
    JBensson   = enum.auto()

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @property
    def config(self):
        """
        Return Config Instance from ConfigMode
        """
        cfgClsDict = {
            Environment.Production: ProductionConfig,
            Environment.Staging: StagingConfig,
            Environment.Nightly: NightlyConfig,
            Environment.DBlack: DBlackConfig,
            Environment.JBensson: JBenssonConfig
        }
        return cfgClsDict[self](env=self)
