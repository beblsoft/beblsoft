#!/usr/bin/env python3
"""
NAME:
 test.py

DESCRIPTION
 Test Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.aws.config import Config
from smeckn.common.secret import SecretType


# ------------------------ TEST CONFIG OBJECT ------------------------------- #
class TestConfig(Config):
    """
    Test Configuration Object
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Secret
        self.secret.type              = SecretType.Test

        # Buckets
        self.buckets.webClient              = "{}.test.{}".format(self.lName, self.domains.main)

        # Domains
        self.domains.clientList       = [self.buckets.webClient]
        self.domains.api              = "{}-api.test.{}".format(self.lName, self.domains.main)

        # Global Database
        self.gdbs.minCapacity         = 2    # 1 Fails in CI
        self.gdbs.maxCapacity         = 2
        self.gdbs.autoPause           = True
        self.gdbs.secUntilAutoPause   = 3600 # 1 hour

        # Account Database
        self.adbs.count               = 1
        self.adbs.minCapacity         = 2    # 1 Fails in CI
        self.adbs.maxCapacity         = 2
        self.adbs.autoPause           = True
        self.adbs.secUntilAutoPause   = 3600 # 1 hour

        # Email
        self.email.domain             = "{}.test.{}".format(self.lName, self.domains.main)
        self.email.recipientList      = ["{}.test.{}".format(self.lName, self.domains.main)]
