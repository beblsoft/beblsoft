#!/usr/bin/env python3
"""
NAME:
 dblack.py

DESCRIPTION
 DBlack Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.aws.config.test import TestConfig


# ------------------------ DBLACK CONFIG OBJECT ----------------------------- #
class DBlackConfig(TestConfig):
    """
    DBlack Configuration Object
    """
    pass
