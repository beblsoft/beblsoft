#!/usr/bin/env python3
"""
NAME:
 jbensson.py

DESCRIPTION
 JBensson Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.aws.config.test import TestConfig


# ------------------------ JBENSSON CONFIG OBJECT --------------------------- #
class JBenssonConfig(TestConfig):
    """
    JBensson Configuration Object
    """
    pass
