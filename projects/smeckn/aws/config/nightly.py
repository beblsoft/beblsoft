#!/usr/bin/env python3
"""
NAME:
 nightly.py

DESCRIPTION
 Nightly Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.aws.config.test import TestConfig


# ------------------------ NIGHTY CONFIG OBJECT ----------------------------- #
class NightlyConfig(TestConfig):
    """
    Nightly Configuration Object
    """
    pass
