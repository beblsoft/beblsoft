#!/usr/bin/env python3
"""
NAME:
 staging.py

DESCRIPTION
 Staging Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
from smeckn.aws.config.test import TestConfig


# ------------------------ STAGING CONFIG OBJECT ---------------------------- #
class StagingConfig(TestConfig):
    """
    Staging Configuration Object
    """
    pass
