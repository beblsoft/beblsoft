#!/usr/bin/env python3
"""
NAME:
 production.py

DESCRIPTION
 Production Configuration Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from smeckn.aws.config import Config
from smeckn.common.secret import SecretType


# ------------------------ PRODUCTION CONFIG OBJECT ------------------------- #
class ProductionConfig(Config):
    """
    Production Configuration Object
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Secret
        self.secret.type              = SecretType.Production

        # Buckets
        self.buckets.webClient        = "www.{}".format(self.domains.main)

        # Domains
        self.domains.clientList       = [self.domains.main, self.buckets.webClient]
        self.domains.api              = "api.{}".format(self.domains.main)

        # Global Database
        self.gdbs.minCapacity         = 2
        self.gdbs.maxCapacity         = 64    # Max is 256 = 488 GB Mem
        self.gdbs.autoPause           = True  # Never pause
        self.gdbs.secUntilAutoPause   = 3600  # 1 hour

        # Account Database
        self.adbs.count               = 2
        self.adbs.minCapacity         = 2
        self.adbs.maxCapacity         = 64    # Max is 256 = 488 GB Mem
        self.adbs.autoPause           = True  # Never pause
        self.adbs.secUntilAutoPause   = 3600  # 1 hour

        # Manager Lambda Switch
        self.mls.logLevel            = logging.INFO

        # Server Lambda Switch
        self.sls.logLevel            = logging.INFO

        # Email
        self.email.domain             = self.domains.main
        self.email.recipientList      = [self.domains.main]
