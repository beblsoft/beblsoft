#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 Manager Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import os
from smeckn.aws.manager.single import SingleManager
from smeckn.aws.manager.ses import SESManager
from smeckn.aws.manager.secret import SecretManager
from smeckn.aws.manager.bastion import BastionManager
from smeckn.aws.manager.managerLambdaSwitch import ManagerLambdaSwitchManager
from smeckn.aws.manager.globalDatabaseServer import GlobalDatabaseServerManager
from smeckn.aws.manager.globalDatabase import GlobalDatabaseManager
from smeckn.aws.manager.accountDatabaseServer import AccountDatabaseServerManager
from smeckn.aws.manager.accountDatabase import AccountDatabaseManager
from smeckn.aws.manager.databasePopulation import DatabasePopulationManager
from smeckn.aws.manager.serverLambdaSwitch import ServerLambdaSwitchManager
from smeckn.aws.manager.network import NetworkManager
from smeckn.aws.manager.webClient import WebClientManager
from smeckn.aws.manager.test import TestManager


# ------------------------ AWS MANAGER OBJECT ------------------------------- #
class AWSManager():
    """
    AWS Manager Class
    """

    def __init__(self, cfg):  # pylint: disable=R0915
        """
        Initialize Object
        """
        self.cfg       = cfg

        # Note: Order matters here, objects reference each other
        self.single    = SingleManager(mgr=self)
        self.ses       = SESManager(mgr=self)
        self.secret    = SecretManager(mgr=self)
        self.network   = NetworkManager(mgr=self)
        self.bastion   = BastionManager(mgr=self)
        self.mls       = ManagerLambdaSwitchManager(mgr=self)
        self.gdbs      = GlobalDatabaseServerManager(mgr=self)
        self.gdb       = GlobalDatabaseManager(mgr=self)
        self.adbs      = AccountDatabaseServerManager(mgr=self)
        self.adb       = AccountDatabaseManager(mgr=self)
        self.dbp       = DatabasePopulationManager(mgr=self)
        self.sls       = ServerLambdaSwitchManager(mgr=self)
        self.webClient = WebClientManager(mgr=self)
        self.test      = TestManager(mgr=self)
