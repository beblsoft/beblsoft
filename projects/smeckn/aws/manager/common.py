#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 common.py

DESCRIPTION
 Common Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from functools import wraps
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from smeckn.aws.config.environment import Environment


# ------------------------ COMMON MANAGER OBJECT ---------------------------- #
class CommonManager():
    """
    Common Manager
    """

    def __init__(self, mgr):  # pylint: disable=R0915
        """
        Initialize Object
        """
        self.mgr  = mgr
        self.cfg  = mgr.cfg

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------------ CONFIG ENV REQUIRED -------------------------- #
    @staticmethod
    def configEnvRequired(env):
        """
        Decorator to require environement
        Args
          env:
            Required environement
            Ex. Environment.Test
        """
        def configEnvRequiredInner(func):
            """
            Inner decorator to all args to be passed to outer decorator
            """
            @wraps(func)
            def wrapper(*args, **kwargs):  # pylint: disable=C0111
                from smeckn.aws.gc import gc
                if not gc.force and gc.aws.cfg.env != env:
                    raise BeblsoftError(code=BeblsoftErrorCode.AWS_ACTION_NOT_PERMITTED)
                return func(*args, **kwargs)
            return wrapper
        return configEnvRequiredInner

    # ------------------------ NOT ON PRODUCTION ---------------------------- #
    @staticmethod
    def notOnProduction(func):
        """
        Decorator to exclude production environment
        """
        @wraps(func)
        def wrapper(*args, **kwargs):  # pylint: disable=C0111
            from smeckn.aws.gc import gc
            if not gc.force and gc.aws.cfg.env == Environment.Production:
                raise BeblsoftError(code=BeblsoftErrorCode.AWS_ACTION_NOT_PERMITTED)
            return func(*args, **kwargs)
        return wrapper
