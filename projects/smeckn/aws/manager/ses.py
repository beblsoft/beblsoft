#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 ses.py

DESCRIPTION
 Simple Email Service Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.SES.ConfigurationSet.bSet import BeblsoftSESConfigurationSet
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ SES OBJECT --------------------------------------- #
class SESManager(CommonManager):
    """
    Single Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object

        Objects Created By Hand and Used for all Environments:
        """
        super().__init__(**kwargs)

        self.bSESConfigurationSet  = BeblsoftSESConfigurationSet(
            name                           = "Smeckn{}SESConfigurationSet".format(self.cfg.name))

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bSESConfigurationSet.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        self.bSESConfigurationSet.delete()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bSESConfigurationSet, "Name={}".format(
                self.bSESConfigurationSet.name)),
        ]
        DescribeData.logList("SES", data)
