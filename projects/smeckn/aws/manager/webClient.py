#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 webClient.py

DESCRIPTION
 Web Client Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import shutil
import git
from base.bebl.python.run.bRun import run
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Vue.bDeploymentV2 import BeblsoftVueDeploymentV2
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData

# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ WEB CLIENT MANAGER OBJECT ------------------------ #
class WebClientManager(CommonManager):
    """
    Web Client Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)
        self.codeDir             = self.cfg.const.dirs.webClient
        self.autoGenFilePath     = os.path.join(self.cfg.const.dirs.webClient, ".env.aws")
        self.environmentDict     = {
            "NODE_ENV": 'production',
            "VUE_APP_ENVIRONMENT": self.cfg.env.name,
            "VUE_APP_GIT_COMMIT_SHA":
            git.Repo(search_parent_directories=True).head.object.hexsha,
            "VUE_APP_SENTRY_WEBCLIENT_DSN": self.mgr.secret.bSecret.SENTRY_WEBCLIENT_DSN,
            "VUE_APP_GOOGLE_ANALYTICS_WEBCLIENT_ID": self.mgr.secret.bSecret.GOOGLE_ANALYTICS_WEBCLIENT_ID,
            "VUE_APP_API_DOMAIN": "{}://{}".format(self.cfg.domains.protocol,
                                                   self.cfg.domains.api),
            "VUE_APP_API_VERSION": "v1",
            "VUE_APP_API_TIMEOUTMS": 60000,
            "VUE_APP_RECAPTCHA_SITE_KEY": self.mgr.secret.bSecret.RECAPTCHA_SITE_KEY,
            "VUE_APP_BASE_URL": self.cfg.const.wcp.app,
            "VUE_APP_USERGUIDE_BASE_URL": self.cfg.const.wcp.userGuide,
            "VUE_APP_STRIPE_PUBLISHABLE_KEY": self.mgr.secret.bSecret.STRIPE_PUBLISHABLE_KEY,
            "VUE_APP_FACEBOOK_APP_ID": self.mgr.secret.bSecret.FACEBOOK_APP_ID
        }
        self.linkList            = ["smeckn-web-api"]
        self.bVueDeployment      = BeblsoftVueDeploymentV2(
            name                          = "Smeckn{}VueDeployment".format(self.cfg.name),
            distDir                       = self.cfg.const.dirs.webClientDist,
            installCodeFunc               = self.installCode,
            buildCodeFunc                 = self.buildCode,
            deleteCodeFunc                = self.deleteCode,
            bucketName                    = self.cfg.buckets.webClient,
            bHostedZone                   = self.mgr.single.bHostedZone,
            domainList                    = self.cfg.domains.clientList,
            bCertificate                  = self.mgr.single.bCertificate,
            indexDocument                 = "app/index.html",
            pathInvalidationList          = ["{}/index.html".format(self.cfg.const.wcp.app),
                                             "{}/index.html".format(self.cfg.const.wcp.userGuide)])

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bVueDeployment.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        self.bVueDeployment.delete()

    @logFunc()
    def update(self):
        """
        Update objects
        """
        self.bVueDeployment.update()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        bBucketWebsite  = self.bVueDeployment.bBucketWebsite
        bCFDistribution = self.bVueDeployment.bCFDistribution
        data = [
            DescribeData(bBucketWebsite, "DomainName={}".format(
                bBucketWebsite.domainName)),
            DescribeData(bCFDistribution, "DomainName={}".format(bCFDistribution.domainName))]
        data += [DescribeData(bRRS, "DomainName={}".format(bRRS.domainName))
                 for bRRS in self.bVueDeployment.bResourceRecordSetList]
        DescribeData.logList("WEB CLIENT", data)

    # ----------------------- CODE FUNCTIONS -------------------------------- #
    @logFunc()
    def installCode(self):
        """
        Install code dependencies
        """
        # Install
        run(cmd="npm install", cwd=self.codeDir, raiseOnStatus=True,
            bufferedLogFunc=logger.info)
        logger.info("{} Installed code dependencies".format(self))

        # Link
        for link in self.linkList:
            run(cmd="npm link {}".format(link), cwd=self.codeDir, raiseOnStatus=True,
                bufferedLogFunc=logger.info)
            logger.info("{} linked {}".format(self, link))

    @logFunc()
    def buildCode(self):
        """
        Build code
        """
        # Application
        self.autoGenerateEnvFile()
        cmd = "npx vue-cli-service build --mode aws"
        run(cmd=cmd, cwd=self.cfg.const.dirs.webClient, raiseOnStatus=True, bufferedLogFunc=logger.info)

        # User Guide
        cmd = """VUE_APP_USERGUIDE_BASE_URL='{}' \
                 npx vuepress build userGuide""".format(self.cfg.const.wcp.userGuide)
        run(cmd=[cmd], cwd=self.cfg.const.dirs.webClient, shell=True, raiseOnStatus=True,
            bufferedLogFunc=logger.info)

        # Email
        emailDistDir = os.path.join(self.cfg.const.dirs.webClientDist, self.cfg.const.wcp.email.replace("/", ""))
        shutil.copytree(src=self.cfg.const.dirs.emailPublic, dst=emailDistDir)

        logger.info("{} Built code".format(self))

    @logFunc()
    def autoGenerateEnvFile(self):
        """
        Auto generate environment file function
        """

        if os.path.exists(self.autoGenFilePath):
            os.remove(self.autoGenFilePath)
        with open(self.autoGenFilePath, "w") as f:
            for key, value in self.environmentDict.items():
                f.write("{}={}\n".format(key, value))

    @logFunc()
    def deleteCode(self):
        """
        Delete Local Code
        """
        run(cmd="rm -rf dist", cwd=self.codeDir, raiseOnStatus=True)
        logger.info("{} Deleted code".format(self))
