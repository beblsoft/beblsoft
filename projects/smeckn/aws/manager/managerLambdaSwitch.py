#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 managerLambdaSwitch.py

DESCRIPTION
 Manager Lambda Switch Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import git
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.Lambda.bPackage import BeblsoftLambdaPackage
from base.aws.python.LambdaSwitch.bDeployment import BeblsoftLSDeployment
from base.aws.python.LambdaSwitch.cases.Error.bDeployment import BeblsoftLSErrorDeployment
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ MANAGER LAMBDA SWITCH MANAGER OBJECT ------------- #
class ManagerLambdaSwitchManager(CommonManager):
    """
    Manager Lambda Switch Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bLFIAMRole           = BeblsoftIAMRole(
            name                          = "Smeckn{}ManagerLSRole".format(self.cfg.name),
            trustPolicyPath               = "{}/managerLSIAMRoleTrustPolicy.json".format(self.cfg.const.dirs.awsManagerTemplate))
        self.bLFIAMRolePolicy     = BeblsoftIAMRolePolicy(
            name                          = "Smeckn{}ManagerLSRP".format(self.cfg.name),
            bIAMRole                      = self.bLFIAMRole,
            permissionPolicyPath          = "{}/managerLSIAMRolePermissionPolicy.json".format(self.cfg.const.dirs.awsManagerTemplate))
        self.bLSSecurityGroup     = BeblsoftSecurityGroup(
            name                          = "Smeckn{}ManagerLSSecurityGroup".format(self.cfg.name),
            bVPC                          = self.mgr.network.bVPC)
        self.bLambdaPackage       = BeblsoftLambdaPackage(
            bucketName                    = self.cfg.buckets.privData,
            bucketKey                     = "Smeckn{}ManagerLS.zip".format(self.cfg.name),
            fromToPaths                   = [{"fromPath": "{}/*".format(self.cfg.const.dirs.server),
                                              "toPath": "smeckn/server",
                                              "excludeStrList": ["venv", "__pycache__"]},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.smecknCommon),
                                              "toPath": "smeckn/common"},
                                             {"fromPath": "{}/venv/lib/python3.6/site-packages/*".format(self.cfg.const.dirs.server),
                                              "toPath": "."},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.awsMySQLClient),
                                              "toPath": "."},
                                             {"fromPath": "{}/bebl/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/bebl/python"},
                                             {"fromPath": "{}/aws/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/aws/python"},
                                             {"fromPath": "{}/recaptcha/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/recaptcha/python"},
                                             {"fromPath": "{}/mysql/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/mysql/python"},
                                             {"fromPath": "{}/stripe/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/stripe/python"},
                                             {"fromPath": "{}/facebook/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/facebook/python"},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.email),
                                              "toPath": "smeckn/email",
                                              "excludeStrList": ["venv", "__pycache__", "node_modules"]},
                                             ],
            buildDir                      = "/tmp/Smeckn{}ManagerLSBuild".format(self.cfg.name),
            bRegion                       = self.cfg.aws.bRegion)
        self.bLSDeployment        = BeblsoftLSDeployment(
            name                          = "Smeckn{}ManagerLSDeployment".format(self.cfg.name),
            bLambdaPackage                = self.bLambdaPackage,
            bIAMRole                      = self.bLFIAMRole,
            logLevel                      = self.cfg.mls.logLevel,
            bSubnets                      = self.mgr.network.bPrivateNATSubList,
            bSecurityGroups               = [self.bLSSecurityGroup],
            envDictFunc                   = lambda: {"SECRET_TYPE": self.cfg.secret.type.name,
                                                     "ENVIRONMENT": self.cfg.env.name,
                                                     "GIT_COMMIT_SHA":
                                                     git.Repo(search_parent_directories=True).head.object.hexsha,
                                                     "SERVER_DOMAIN": "https://{}".format(self.cfg.domains.api),
                                                     "WEBCLIENT_DOMAIN_LIST": " ".join(
                                                         ["https://{}".format(d) for d in self.cfg.domains.clientList]
                                                     ),
                                                     "COOKIE_DOMAIN": self.cfg.domains.cookie,
                                                     "EMAIL_FROM_DOMAIN": self.cfg.email.domain,
                                                     "EMAIL_CONFIG_SET_NAME": self.mgr.ses.bSESConfigurationSet.name},
            description                   = "Smeckn {} Manager Lambda Switch".format(self.cfg.name),
            timeoutS                      = 600,  # 10 minutes
            memorySizeMB                  = 1028,
            logRetentionInDays            = 30)  # MB
        self.bLSErrorDeployment   = BeblsoftLSErrorDeployment(
            bLSDeployment                 = self.bLSDeployment,
            funcStr                       = "smeckn.server.aws.error:handleError")

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bLFIAMRole.create()
        self.bLFIAMRolePolicy.create()
        self.bLSSecurityGroup.create()
        self.bLSDeployment.create()

    @logFunc()
    @CommonManager.notOnProduction
    def delete(self):
        """
        Delete objects
        """
        self.bLSDeployment.delete()
        self.bLSSecurityGroup.delete()
        self.bLFIAMRolePolicy.delete()
        self.bLFIAMRole.delete()

    @logFunc()
    def update(self):
        """
        Update objects
        """
        self.bLSDeployment.update()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        bLFIAMRole      = self.bLFIAMRole
        bLambdaFunction = self.bLSDeployment.bLambdaFunction
        data = [
            DescribeData(bLFIAMRole, "ID={}".format(bLFIAMRole.id)),
            DescribeData(self.bLSSecurityGroup,
                         "ID={}".format(self.bLSSecurityGroup.id)),
            DescribeData(bLambdaFunction, "Arn={}".format(bLambdaFunction.arn)),
        ]
        DescribeData.logList("MANAGER LAMBDA SWITCH", data)

    # ---------------------- RUN -------------------------------------------- #
    @logFunc()
    def invoke(self, **kwargs):  # pylint: disable=W0221,W0102
        """
        Invoke object
        """
        return self.bLSDeployment.invoke(**kwargs)

    @logFunc()
    def runPython(self, code, **lambdaInvokeKwargs):  # pylint: disable=W0221,W0102
        """
        Run python code
        Args
          code:
            String of python code to run inside lambda
            Ex. 'print("Hello World!"); rval = 0;'

        Note:
          All newlines are stripped out of code; therefore, each command must
          be completed with a semicolon

        Returns
          Data placed in "rval" variable will be returned
        """
        code = code.strip("\n")
        return self.invoke(
            funcStr="smeckn.server.aws.runPython:runPython",
            funcKwargs={
                "gdbDomain": self.mgr.gdbs.bDBCluster.endpoint,
                "code": code
            },
            **lambdaInvokeKwargs)

    # ---------------------- LOGS ------------------------------------------- #
    @logFunc()
    def tail(self):  # pylint: disable=W0221
        """
        Tail object
        """
        self.bLSDeployment.tailLambda()

    @logFunc()
    def dumpStreamToFile(self, **cwlgDumpStreamToFileKwargs):  # pylint: disable=W0221
        """
        Dump stream to file
        """
        self.bLSDeployment.dumpStreamToFile(**cwlgDumpStreamToFileKwargs)
