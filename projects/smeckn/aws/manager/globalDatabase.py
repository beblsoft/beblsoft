#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 globalDatabase.py

DESCRIPTION
 Global Database Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.aws.manager.common import CommonManager


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ GLOBAL DATABASE MANAGER OBJECT ------------------- #
class GlobalDatabaseManager(CommonManager):
    """
    Global Database Manager
    """

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.mgr.mls.runPython(code="mgr.gdb.create();")

    @logFunc()
    @CommonManager.notOnProduction
    def delete(self):
        """
        Delete objects
        """
        self.mgr.mls.runPython(code="mgr.gdb.delete();")

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        rval = self.mgr.mls.runPython(code="rval = mgr.gdb.describe();")
        logger.info(rval)

    # ---------------------- VERSIONS --------------------------------------- #
    @logFunc()
    def upgradeToMLS(self):
        """
        Upgrade version to latest MLS version
        """
        history       = self.mgr.mls.runPython(code="""rval = mgr.gdb.gDB.bDBMigrations.history;""")
        latestVersion = history[-1]
        logger.info("{} History: {}".format(self, history))
        self.upgrade(toVersion=latestVersion)

    @logFunc()
    def upgrade(self, toVersion):
        """
        Upgrade
        """
        self.mgr.mls.runPython(
            code="""mgr.gdb.upgrade(toVersion="{}");""".format(toVersion))
        logger.info("{} Upgraded to {}".format(self, toVersion))

    @logFunc()
    @CommonManager.notOnProduction
    def downgrade(self, toVersion):
        """
        Downgrade
        """
        self.mgr.mls.runPython(
            code="""mgr.gdb.downgrade(toVersion="{}");""".format(toVersion))
        logger.info("{} Downgraded to {}".format(self, toVersion))

    # ---------------------- RUN SQL ---------------------------------------- #
    @logFunc()
    def runSQL(self, sqlStatement="", **lambdaInvokeKwargs):
        """
        Run SQL
        """
        return self.mgr.mls.runPython(
            code="""rval = mgr.gdb.runSQL(sqlStatement="{}");""".format(sqlStatement),
            **lambdaInvokeKwargs)
