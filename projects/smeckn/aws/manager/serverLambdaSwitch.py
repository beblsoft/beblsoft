#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 serverLambdaSwitch.py

DESCRIPTION
 Server Lambda Switch Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import git
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.Lambda.bPackage import BeblsoftLambdaPackage
from base.aws.python.LambdaSwitch.bDeployment import BeblsoftLSDeployment
from base.aws.python.LambdaSwitch.cases.Error.bDeployment import BeblsoftLSErrorDeployment
from base.aws.python.LambdaSwitch.cases.Flask.bDeployment import BeblsoftLSFlaskDeployment
from base.aws.python.LambdaSwitch.cases.ScheduledEvent.bDeployment import BeblsoftLSScheduledEventDeployment
from base.aws.python.LambdaSwitch.cases.SESConfigurationSetEvent.bDeployment import BeblsoftLSSESConfigurationEventDeployment
from base.aws.python.LambdaSwitch.cases.SQS.bDeployment import BeblsoftLSSQSDeployment
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ SERVER LAMBDA SWITCH MANAGER OBJECT -------------- #
class ServerLambdaSwitchManager(CommonManager):
    """
    Server Lambda Switch Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.sqsJobQueueName      = "Smeckn{}ServerLSJobQueue".format(self.cfg.name)
        self.sqsJobDLQueueName    = "Smeckn{}ServerLSJobDLQueue".format(self.cfg.name)
        self.bLFIAMRole           = BeblsoftIAMRole(
            name                          = "Smeckn{}ServerLSRole".format(self.cfg.name),
            trustPolicyPath               = "{}/serverLSIAMRoleTrustPolicy.json".format(self.cfg.const.dirs.awsManagerTemplate))
        self.bLFIAMRolePolicy     = BeblsoftIAMRolePolicy(
            name                          = "Smeckn{}ServerLSRP".format(self.cfg.name),
            bIAMRole                      = self.bLFIAMRole,
            permissionPolicyPath          = "{}/serverLSIAMRolePermissionPolicy.json".format(self.cfg.const.dirs.awsManagerTemplate))
        self.bLSSecurityGroup     = BeblsoftSecurityGroup(
            name                          = "Smeckn{}ServerLSSecurityGroup".format(self.cfg.name),
            bVPC                          = self.mgr.network.bVPC)
        self.bLambdaPackage       = BeblsoftLambdaPackage(
            bucketName                    = self.cfg.buckets.privData,
            bucketKey                     = "Smeckn{}ServerLS.zip".format(self.cfg.name),
            fromToPaths                   = [{"fromPath": "{}/*".format(self.cfg.const.dirs.server),
                                              "toPath": "smeckn/server",
                                              "excludeStrList": ["venv", "__pycache__"]},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.smecknCommon),
                                              "toPath": "smeckn/common"},
                                             {"fromPath": "{}/venv/lib/python3.6/site-packages/*".format(self.cfg.const.dirs.server),
                                              "toPath": "."},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.awsMySQLClient),
                                              "toPath": "."},
                                             {"fromPath": "{}/bebl/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/bebl/python"},
                                             {"fromPath": "{}/aws/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/aws/python"},
                                             {"fromPath": "{}/recaptcha/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/recaptcha/python"},
                                             {"fromPath": "{}/mysql/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/mysql/python"},
                                             {"fromPath": "{}/stripe/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/stripe/python"},
                                             {"fromPath": "{}/facebook/python/*".format(self.cfg.const.dirs.base),
                                              "toPath": "base/facebook/python"},
                                             {"fromPath": "{}/*".format(self.cfg.const.dirs.email),
                                              "toPath": "smeckn/email",
                                              "excludeStrList": ["venv", "__pycache__", "node_modules"]},
                                             ],
            buildDir                      = "/tmp/Smeckn{}ServerLSBuild".format(self.cfg.name),
            bRegion                       = self.cfg.aws.bRegion)
        self.bLSDeployment        = BeblsoftLSDeployment(
            name                          = "Smeckn{}ServerLSDeployment".format(self.cfg.name),
            bLambdaPackage                = self.bLambdaPackage,
            bIAMRole                      = self.bLFIAMRole,
            logLevel                      = self.cfg.sls.logLevel,
            bSubnets                      = self.mgr.network.bPrivateNATSubList,
            bSecurityGroups               = [self.bLSSecurityGroup],
            envDictFunc                   = lambda: {"SECRET_TYPE": self.cfg.secret.type.name,
                                                     "ENVIRONMENT": self.cfg.env.name,
                                                     "GIT_COMMIT_SHA":
                                                     git.Repo(search_parent_directories=True).head.object.hexsha,
                                                     "SERVER_DOMAIN": "https://{}".format(self.cfg.domains.api),
                                                     "WEBCLIENT_DOMAIN_LIST": " ".join(
                                                         ["https://{}".format(d) for d in self.cfg.domains.clientList]
                                                     ),
                                                     "COOKIE_DOMAIN": self.cfg.domains.cookie,
                                                     "GDB_DOMAIN": self.mgr.gdbs.bDBCluster.endpoint,
                                                     "EMAIL_FROM_DOMAIN": self.cfg.email.domain,
                                                     "EMAIL_CONFIG_SET_NAME": self.mgr.ses.bSESConfigurationSet.name,
                                                     "SQS_JOB_QUEUE_NAME": self.sqsJobQueueName,
                                                     "SQS_JOB_DLQUEUE_NAME": self.sqsJobDLQueueName},
            description                   = "Smeckn {} Server Lambda Switch".format(self.cfg.name),
            timeoutS                      = 600,  # 10 Minutes
            memorySizeMB                  = 1028,
            logRetentionInDays            = 30)
        self.bLSErrorDeployment   = BeblsoftLSErrorDeployment(
            bLSDeployment                 = self.bLSDeployment,
            funcStr                       = "smeckn.server.aws.error:handleError")
        self.bLSKWSEDeployment    = BeblsoftLSScheduledEventDeployment(
            bLSDeployment                 = self.bLSDeployment,
            index                         = 0,
            funcStr                       = "smeckn.server.aws.keepWarm:handleKeepWarm",
            expression                    = "rate(5 minutes)")
        self.bLSFlaskDeployment   = BeblsoftLSFlaskDeployment(
            bLSDeployment                 = self.bLSDeployment,
            bHostedZone                   = self.mgr.single.bHostedZone,
            domainName                    = self.cfg.domains.api,
            bCertificate                  = self.mgr.single.bCertificate,
            appFuncStr                    = "smeckn.server.aws.getFlaskApp:handleGetFlaskApp")
        self.bLSSESCEDeployment   = BeblsoftLSSESConfigurationEventDeployment(
            bLSDeployment                 = self.bLSDeployment,
            bSESConfigurationSet          = self.mgr.ses.bSESConfigurationSet,
            eventList                     = ["send", "reject", "bounce", "complaint", "delivery",
                                             "open", "click", "renderingFailure"],
            funcStr                       = "smeckn.server.aws.configSetEvent:handleConfigSetEvent")
        self.bLSSQSJobQDeployment = BeblsoftLSSQSDeployment(
            bLSDeployment                 = self.bLSDeployment,
            qName                         = self.sqsJobQueueName,
            qFuncStr                      = "smeckn.server.aws.jobQueue:handleMessageReceived",
            qMessageRetentionS            = 86400,  # 1 Day
            qVisibilityTimeoutS           = 601,  # 10 minutes, 1 second
            qMaxReceiveCount              = 2,
            dlqName                       = self.sqsJobDLQueueName,
            dlqFuncStr                    = "smeckn.server.aws.jobDLQueue:handleMessageReceived",
            dlqMessageRetentionS          = 604800,  # 7 Days
            dlqVisibilityTimeoutS         = 601,  # 10 minutes, 1 second
            fifoQueues                    = False)

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self, ls, lskw, lsf, lssesce, lsjq):
        """
        Create objects
        """
        if ls:
            self.bLFIAMRole.create()
            self.bLFIAMRolePolicy.create()
            self.bLSSecurityGroup.create()
            self.bLSDeployment.create()
        if lskw:
            self.bLSKWSEDeployment.create()
        if lsf:
            self.bLSFlaskDeployment.create(allobjs = True)
        if lssesce:
            self.bLSSESCEDeployment.create()
        if lsjq:
            self.bLSSQSJobQDeployment.create()

    @logFunc()
    def delete(self, ls, lskw, lsf, lssesce, lsjq):
        """
        Delete objects
        """
        if lsjq:
            self.bLSSQSJobQDeployment.delete()
        if lssesce:
            self.bLSSESCEDeployment.delete()
        if lsf == "partial":
            self.bLSFlaskDeployment.delete(
                api = True, resources = True, deployment = True, stage = True,
                edge = False, bp = True, alias = False)
        if lsf == "all":
            self.bLSFlaskDeployment.delete(
                api = True, resources = True, deployment = True, stage = True,
                edge = True, bp = True, alias = True)
        if lskw:
            self.bLSKWSEDeployment.delete()
        if ls:
            self.bLSDeployment.delete()
            self.bLSSecurityGroup.delete()
            self.bLFIAMRolePolicy.delete()
            self.bLFIAMRole.delete()

    @logFunc()
    def update(self, ls, lskw, lsf, lssesce, lsjq):
        """
        Update objects
        """
        if ls:
            self.bLSDeployment.update()
        if lskw:
            self.bLSKWSEDeployment.update()
        if lsf:
            self.bLSFlaskDeployment.update()
        if lssesce:
            self.bLSSESCEDeployment.update()
        if lsjq:
            self.bLSSQSJobQDeployment.update()

    @logFunc()
    def describe(self, ls, lskw, lsf, lssesce, lsjq):
        """
        Describe objects
        """
        if ls:
            bLFIAMRole      = self.bLFIAMRole
            bLambdaFunction = self.bLSDeployment.bLambdaFunction
            data = [
                DescribeData(bLFIAMRole, "ID={}".format(bLFIAMRole.id)),
                DescribeData(self.bLSSecurityGroup,
                             "ID={}".format(self.bLSSecurityGroup.id)),
                DescribeData(bLambdaFunction, "Arn={}".format(bLambdaFunction.arn)),
            ]
            DescribeData.logList("SERVER LAMBDA SWITCH", data)
        if lskw:
            bKWCWERule      = self.bLSKWSEDeployment.bCWERule
            data = [
                DescribeData(bKWCWERule, "Arn={}".format(bKWCWERule.arn)),
            ]
            DescribeData.logList("SERVER LAMBDA SWITCH: KEEP WARM", data)
        if lsf:
            bAGDeployment   = self.bLSFlaskDeployment.bAGDeployment
            bAGStage        = self.bLSFlaskDeployment.bAGStage
            bAGEdgeDomain   = self.bLSFlaskDeployment.bAGEdgeDomain
            bAliasRecord    = self.bLSFlaskDeployment.bAliasRecord
            data = [
                DescribeData(bAGDeployment, "Id={}".format(bAGDeployment.id)),
                DescribeData(bAGStage, "DomainName={}".format(bAGStage.url)),
                DescribeData(bAGEdgeDomain, "cfDomain={}".format(
                    bAGEdgeDomain.cfDomain)),
                DescribeData(bAliasRecord, "DomainName={}".format(
                    bAliasRecord.domainName))
            ]
            DescribeData.logList("SERVER LAMBDA SWITCH: FLASK", data)
        if lssesce:
            bSNSTopic              = self.bLSSESCEDeployment.bSNSTopic
            bSNSTopicSubscription  = self.bLSSESCEDeployment.bSNSTopicSubscription
            bSESConfigSetTopicDest = self.bLSSESCEDeployment.bSESConfigSetTopicDest
            data = [
                DescribeData(bSNSTopic, "Arn={}".format(bSNSTopic.arn)),
                DescribeData(bSNSTopicSubscription, "Arn={}".format(
                    bSNSTopicSubscription.arn)),
                DescribeData(bSESConfigSetTopicDest, "Enabled={}".format(
                    bSESConfigSetTopicDest.online))
            ]
            DescribeData.logList("SERVER LAMBDA SWITCH: SES CONFIG", data)
        if lsjq:
            bSQSQueue            = self.bLSSQSJobQDeployment.bSQSQueue
            bLambdaESMapping     = self.bLSSQSJobQDeployment.bLambdaESMapping
            bSQSDLQueue          = self.bLSSQSJobQDeployment.bSQSDLQueue
            bDLLambdaESMapping   = self.bLSSQSJobQDeployment.bDLLambdaESMapping
            data = [
                DescribeData(bSQSQueue, "url={} arn={}".format(
                    bSQSQueue.url, bSQSQueue.arn)),
                DescribeData(bLambdaESMapping, "state={} uuid={}".format(
                    bLambdaESMapping.state, bLambdaESMapping.uuid)),
                DescribeData(bSQSDLQueue, "url={} arn={}".format(
                    bSQSDLQueue.url, bSQSDLQueue.arn)),
                DescribeData(bDLLambdaESMapping, "state={} uuid={}".format(
                    bDLLambdaESMapping.state, bDLLambdaESMapping.uuid)),
            ]
            DescribeData.logList("SERVER LAMBDA SWITCH: JOB QUEUE", data)

    # ---------------------- RUN -------------------------------------------- #
    @logFunc()
    def invoke(self, **kwargs):  # pylint: disable=W0221,W0102
        """
        Invoke object
        """
        return self.bLSDeployment.invoke(**kwargs)

    @logFunc()
    def runPython(self, code, **lambdaInvokeKwargs):  # pylint: disable=W0221,W0102
        """
        Run python code
        See commands in ManagerLambdaSwitchManager.runPython
        """
        code = code.strip("\n")
        return self.invoke(
            funcStr = "smeckn.server.aws.runPython:runPython",
            funcKwargs = {
                "gdbDomain": self.mgr.gdbs.bDBCluster.endpoint,
                "code": code
            },
            **lambdaInvokeKwargs)

    # ---------------------- LOGS ------------------------------------------- #
    @logFunc()
    def tail(self):  # pylint: disable=W0221
        """
        Tail object
        """
        self.bLSDeployment.tailLambda()

    @logFunc()
    def dumpStreamToFile(self, **cwlgDumpStreamToFileKwargs):  # pylint: disable=W0221
        """
        Dump stream to file
        """
        self.bLSDeployment.dumpStreamToFile(**cwlgDumpStreamToFileKwargs)
