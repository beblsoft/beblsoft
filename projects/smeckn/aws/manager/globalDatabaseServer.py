#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 globalDatabaseServer.py

DESCRIPTION
 Global Database Server Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.RDS.bSubnetGroup import BeblsoftDatabaseSubnetGroup
from base.aws.python.Aurora.bCluster import BeblsoftDatabaseCluster
from base.aws.python.Aurora.bParameterGroup import BeblsoftDatabaseClusterParameterGroup
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ GLOBAL DATABASE SERVER MANAGER OBJECT ------------ #
class GlobalDatabaseServerManager(CommonManager):
    """
    Global Database Server Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bSecurityGroup       = BeblsoftSecurityGroup(
            name                          = "Smeckn{}GDBSecurityGroup".format(
                self.cfg.name),
            bVPC                          = self.mgr.network.bVPC)
        self.bDBSubnetGroup       = BeblsoftDatabaseSubnetGroup(
            name                          = "Smeckn{}GDBSubnetGroup".format(
                self.cfg.name),
            bSubnets                      = self.mgr.network.bPrivateSubList)
        self.bDBClusterParamGroup = BeblsoftDatabaseClusterParameterGroup(
            name                          = "Smeckn{}GDBClusterParamGroup".format(
                self.cfg.name),
            family                        = "aurora5.6",
            bRegion                       = self.cfg.aws.bRegion)
        self.bDBCluster           = BeblsoftDatabaseCluster(
            clusterID                     = "Smeckn{}GDBCluster".format(
                self.cfg.name),
            dbName                        = None,
            bSecurityGroup                = self.bSecurityGroup,
            bDBClusterParameterGroup      = self.bDBClusterParamGroup,
            bDBSubnetGroup                = self.bDBSubnetGroup,
            masterUsername                = self.mgr.secret.bSecret.GDB_USER,
            masterUserPassword            = self.mgr.secret.bSecret.GDB_PASSWORD,
            engine                        = "aurora",
            engineVersion                 = "5.6.10a",
            port                          = 3306,
            engineMode                    = "serverless",
            scalingConfiguration          = {"MinCapacity": self.cfg.gdbs.minCapacity,
                                             "MaxCapacity": self.cfg.gdbs.maxCapacity,
                                             "AutoPause": self.cfg.gdbs.autoPause,
                                             "SecondsUntilAutoPause": self.cfg.gdbs.secUntilAutoPause})

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bSecurityGroup.create()
        self.bDBSubnetGroup.create()
        self.bDBClusterParamGroup.create()
        self.bDBCluster.create()

    @logFunc()
    @CommonManager.notOnProduction
    def delete(self):
        """
        Delete objects
        """
        self.bDBCluster.delete()
        self.bDBClusterParamGroup.delete()
        self.bDBSubnetGroup.delete()
        self.bSecurityGroup.delete(deleteENIs=False)

    @logFunc()
    def update(self):
        """
        Update objects
        """
        self.bDBCluster.update()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bSecurityGroup,
                         "ID={}".format(self.bSecurityGroup.id)),
            DescribeData(self.bDBSubnetGroup,
                         "ARN={}".format(self.bDBSubnetGroup.arn)),
            DescribeData(self.bDBClusterParamGroup,
                         "ARN={}".format(self.bDBClusterParamGroup.arn)),
            DescribeData(self.bDBCluster,
                         "ARN={} Capacity={} Endpoint={}".format(
                             self.bDBCluster.arn, self.bDBCluster.capacity,
                             self.bDBCluster.endpoint))]
        DescribeData.logList("GLOBAL DATABASE SERVER", data)

    # ---------------------- WAKE UP ---------------------------------------- #
    @logFunc()
    def wakeUp(self):
        """
        WakeUp Global Database
        """
        self.mgr.mls.runPython(
            code =
            """import pprint;"""
            """from base.mysql.python.bServer import BeblsoftMySQLServer;"""
            """bServer = BeblsoftMySQLServer(domainNameFunc= lambda : '{}', user = '{}', password = '{}');"""
            """logger.info(pprint.pformat(bServer.versions));"""
            .format(
                self.bDBCluster.endpoint,
                self.mgr.secret.bSecret.GDB_USER,
                self.mgr.secret.bSecret.GDB_PASSWORD))
