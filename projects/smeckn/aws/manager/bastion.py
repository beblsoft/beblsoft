#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 bastion.py

DESCRIPTION
 Bastion Host Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bKeyPair import BeblsoftKeyPair
from base.aws.python.EC2.bInstance import BeblsoftInstance
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData



# ------------------------ BASTION MANAGER OBJECT --------------------------- #
class BastionManager(CommonManager):
    """
    Bastion Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bKeyPair             = BeblsoftKeyPair(
            name                          = "Smeckn{}KeyPair".format(self.cfg.name))
        self.bBastionSecurityGroup= BeblsoftSecurityGroup(
            name                          = "Smeckn{}BastionSecurityGroup".format(self.cfg.name),
            bVPC                          = self.mgr.network.bVPC,
            ipIngressPermissions          = [{
                                                "FromPort": 22,       # SSH
                                                "ToPort": 22,         # SSH
                                                "IpProtocol": "tcp",  # tcp
                                                "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
                                            }],
            ipEgressPermissions           = [{
                                                # All ports
                                                "IpProtocol": "-1",      # tcp,udp,icmp
                                                "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
                                            }])
        # Use Lambda AMI: https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html
        self.bInstance            = BeblsoftInstance(
            name                          = "Smeckn{}Instance".format(self.cfg.name),
            imageId                       = "ami-4fffc834",
            bSubnet                       = self.mgr.network.bPublicSubList[0],
            bKeyPair                      = self.bKeyPair,
            bSecurityGroup                = self.bBastionSecurityGroup,
            instanceType                  = "t2.nano")

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bKeyPair.create()
        self.bBastionSecurityGroup.create()
        self.bInstance.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        self.bInstance.delete()
        self.bBastionSecurityGroup.delete()
        self.bKeyPair.delete()


    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bKeyPair, "Fingerprint={}".format(
                self.bKeyPair.fingerprint)),
            DescribeData(self.bBastionSecurityGroup,
                         "ID={}".format(self.bBastionSecurityGroup.id)),
            DescribeData(self.bInstance, "ID={} Domain={} State={}".format(
                         self.bInstance.id, self.bInstance.domainName, self.bInstance.state)),
            ]
        DescribeData.logList("BASTION", data)

    # ---------------------- START / STOP ----------------------------------- #
    @logFunc()
    def start(self):
        """
        Start objects
        """
        self.bInstance.start()

    @logFunc()
    def stop(self):
        """
        Stop objects
        """
        self.bInstance.stop()
