#!/usr/bin/env python3
"""
 NAME:
  test.py

 DESCRIPTION
  Test Manager Functionality
"""

# -------------------------- IMPORTS ---------------------------------------- #
import enum
from pathlib import Path
import logging
import unittest
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from smeckn.aws.manager.common import CommonManager


# -------------------------- GLOBALS ---------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------ TEST MODULES ------------------------------------- #
@enum.unique
class TestModule(enum.Enum):
    """
    Test type enumeration
    """
    ALL   = enum.auto()
    GJOB  = enum.auto()  # Global Jobs
    AJOB  = enum.auto()  # Account Job


# -------------------------- TEST MANAGER ----------------------------------- #
class TestManager(CommonManager):
    """
    Test Manager class
    """

    @logFunc()
    def test(self, module=None, name=None): #pylint: disable=R0201
        """
        Test Infrastructure
        Args
          module:
            Module to test
            Ex1. "GJOB"
          name:
            Path to module.
            Ex1. smeckn.aws.test.aSleepJob_test

        Raises
          BeblsoftError if test failed
        """
        tests         = None
        if name:
            tests     = unittest.TestLoader().loadTestsFromNames([name])
        if module == TestModule.ALL.name:
            pattern   = "*_test.py"
            awsDir    = Path(__file__).parents[1]  # smeckn/aws
            tests     = unittest.TestLoader().discover(start_dir=awsDir, pattern=pattern)
        if module == TestModule.GJOB.name:
            tests     = unittest.TestLoader().loadTestsFromNames(["smeckn.aws.tests.gSleepJob_test"])
        if module == TestModule.AJOB.name:
            tests     = unittest.TestLoader().loadTestsFromNames(["smeckn.aws.tests.aSleepJob_test"])

        if not tests:
            raise BeblsoftError(msg="No tests specified")
        result = unittest.TextTestRunner(verbosity=2).run(tests)
        if result.errors:
            raise BeblsoftError(msg="Test failed result={}".format(result.__dict__))
