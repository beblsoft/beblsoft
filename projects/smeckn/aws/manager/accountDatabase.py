#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 accountDatabase.py

DESCRIPTION
 Account Database Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.run.bParallel import runFuncOnList
from smeckn.aws.manager.common import CommonManager


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ ACCOUNT DATABASE MANAGER OBJECT ------------------ #
class AccountDatabaseManager(CommonManager):
    """
    Account Database Manager
    """

    # ---------------------- HELPERS ---------------------------------------- #
    def emailAndAIDToArgStr(self, aID=None, email=None):  # pylint: disable=R0201
        """
        Format email and AID in function calls
        """
        return "aID={}, email={}".format(
            aID, "'{}'".format(email) if email else None)

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def describe(self, aID=None, email=None):
        """
        Describe objects
        """
        rval = self.mgr.mls.runPython(code = """rval = mgr.adb.describe({});""".format(
            self.emailAndAIDToArgStr(aID, email)))
        logger.info(rval)

    # ---------------------- VERSIONS --------------------------------------- #
    @logFunc()
    def getNNotAtVersion(self, version, aIDMod=None, aIDModEquals=None):
        """
        Return number of databases not at version
        """
        if version in ["head", "base"]:
            raise BeblsoftError(msg = "Must specify a slug version")
        nNotAtVersion = self.mgr.mls.runPython(
            code = """rval = mgr.adb.getNNotAtVersion(version="{}", aIDMod={}, aIDModEquals={});""".format(
                version, aIDMod, aIDModEquals))
        return int(nNotAtVersion)

    @logFunc()
    def upgradeToMLS(self, nThreads=10):
        """
        Upgrade version to latest version in MLS history
        """
        history        = self.mgr.mls.runPython(
            code=("""aDB = mgr.adb.getADB(email='{}'); """
                  """rval = aDB.bDBMigrations.history;""").format(
                self.mgr.secret.bSecret.DBP_ADMIN_EMAIL))
        latestVersion  = history[-1]
        logger.info("{} History: {}".format(self, history))
        self.upgrade(toVersion=latestVersion, nThreads=nThreads)

    @logFunc()
    def upgrade(self, toVersion, nThreads=10):
        """
        Spawn nThreads to upgrade account databases in parallel
        Threads upgrade specific databases according to server/gDB/account/dao.py:getAll
        """
        def upgradeThread(idx):
            """
            Single thread to upgrade multiple databases
            """
            iteration     = 0
            nNotAtVersion = -1
            while nNotAtVersion != 0:
                nNotAtVersion = self.getNNotAtVersion(version=toVersion, aIDMod=nThreads,
                                                      aIDModEquals=idx)
                logger.info("{} Thread={} Iteration={} nNotAtVersion={}".format(self, idx,
                                                                                iteration, nNotAtVersion))
                self.mgr.mls.runPython(
                    code = """mgr.adb.upgrade(toVersion="{}", aIDMod={}, aIDModEquals={});""".format(
                        toVersion, nThreads, idx))
                iteration += 1

        runFuncOnList(func=upgradeThread, lst=range(0, nThreads), parallel=True)
        logger.info("{} All Upgraded to {}".format(self, toVersion))

    @logFunc()
    @CommonManager.notOnProduction
    def downgrade(self, toVersion, nThreads=10):
        """
        Spawn nThreads to downgrade account databases in parallel
        Threads downgrade specific databases according to server/gDB/account/dao.py:getAll
        """
        def downgradeThread(idx):
            """
            Single thread to downgrade multiple databases
            """
            iteration     = 0
            nNotAtVersion = -1
            while nNotAtVersion != 0:
                nNotAtVersion = self.getNNotAtVersion(version=toVersion, aIDMod=nThreads,
                                                      aIDModEquals=idx)
                logger.info("{} Thread={} Iteration={} nNotAtVersion={}".format(self, idx,
                                                                                iteration, nNotAtVersion))
                self.mgr.mls.runPython(
                    code = """mgr.adb.downgrade(toVersion="{}", aIDMod={}, aIDModEquals={});""".format(
                        toVersion, nThreads, idx))
                iteration += 1

        runFuncOnList(func=downgradeThread, lst=range(0, nThreads), parallel=True)
        logger.info("{} All Downgraded to {}".format(self, toVersion))

    # --------------------- RUN SQL ----------------------------------------- #
    @logFunc()
    def runSQL(self, aID=None, email=None, sqlStatement=""):
        """
        Describe objects
        """
        return self.mgr.mls.runPython(code = """rval = mgr.adb.runSQL({}, sqlStatement="{}");""".format(
            self.emailAndAIDToArgStr(aID, email), sqlStatement))
