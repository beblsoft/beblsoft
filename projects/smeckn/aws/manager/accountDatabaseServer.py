#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 accountDatabaseServer.py

DESCRIPTION
 Account Database Server Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.run.bParallel import runFuncOnList
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.RDS.bSubnetGroup import BeblsoftDatabaseSubnetGroup
from base.aws.python.Aurora.bCluster import BeblsoftDatabaseCluster
from base.aws.python.Aurora.bParameterGroup import BeblsoftDatabaseClusterParameterGroup
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ ACCOUNT DATABASE SERVER MANAGER OBJECT ----------- #
class AccountDatabaseServerManager(CommonManager):
    """
    Account Database Server Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bSecurityGroupList       = []
        self.bDBSubnetGroupList       = []
        self.bDBClusterParamGroupList = []
        self.bDBClusterList           = []
        for idx in range(0, self.cfg.adbs.count):
            self.bSecurityGroupList.append(
                BeblsoftSecurityGroup(
                    name                          = "Smeckn{}ADBSecurityGroup{}".format(self.cfg.name, idx),
                    bVPC                          = self.mgr.network.bVPC))
            self.bDBSubnetGroupList.append(
                BeblsoftDatabaseSubnetGroup(
                    name                          = "Smeckn{}ADBSubnetGroup{}".format(self.cfg.name, idx),
                    bSubnets                      = self.mgr.network.bPrivateSubList))
            self.bDBClusterParamGroupList.append(
                BeblsoftDatabaseClusterParameterGroup(
                    name                          = "Smeckn{}ADBClusterParamGroup{}".format(self.cfg.name, idx),
                    family                        = "aurora5.6",
                    bRegion                       = self.cfg.aws.bRegion))
            self.bDBClusterList.append(
                BeblsoftDatabaseCluster(
                    clusterID                     = "Smeckn{}ADBCluster{}".format(self.cfg.name, idx),
                    dbName                        = None,
                    bSecurityGroup                = self.bSecurityGroupList[idx],
                    bDBClusterParameterGroup      = self.bDBClusterParamGroupList[idx],
                    bDBSubnetGroup                = self.bDBSubnetGroupList[idx],
                    masterUsername                = self.mgr.secret.bSecret.ADB_USER,
                    masterUserPassword            = self.mgr.secret.bSecret.ADB_PASSWORD,
                    engine                        = "aurora",
                    engineVersion                 = "5.6.10a",
                    port                          = 3306,
                    engineMode                    = "serverless",
                    scalingConfiguration          = {"MinCapacity": self.cfg.adbs.minCapacity,
                                                     "MaxCapacity": self.cfg.adbs.maxCapacity,
                                                     "AutoPause": self.cfg.adbs.autoPause,
                                                     "SecondsUntilAutoPause": self.cfg.adbs.secUntilAutoPause}))

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        for idx in range(0, self.cfg.adbs.count):
            self.bSecurityGroupList[idx].create()
            self.bDBSubnetGroupList[idx].create()
            self.bDBClusterParamGroupList[idx].create()
            self.bDBClusterList[idx].create()
            self.registerADB(self.bDBClusterList[idx])

    @logFunc()
    @CommonManager.notOnProduction
    def delete(self):
        """
        Delete objects
        """
        for idx in range(0, self.cfg.adbs.count):
            self.deregisterADB(self.bDBClusterList[idx])
            self.bDBClusterList[idx].delete()
            self.bDBClusterParamGroupList[idx].delete()
            self.bDBSubnetGroupList[idx].delete()
            self.bSecurityGroupList[idx].delete(deleteENIs=False)

    @logFunc()
    def update(self):
        """
        Update objects
        """
        for idx in range(0, self.cfg.adbs.count):
            self.bDBClusterList[idx].update()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = []
        for bSecurityGroup in self.bSecurityGroupList:
            data.append(DescribeData(bSecurityGroup,
                                     "ID={}".format(bSecurityGroup.id)))
        for bDBSubnetGroup in self.bDBSubnetGroupList:
            data.append(DescribeData(bDBSubnetGroup,
                                     "ARN={}".format(bDBSubnetGroup.arn)))
        for bDBClusterParamGroup in self.bDBClusterParamGroupList:
            data.append(DescribeData(bDBClusterParamGroup,
                                     "ARN={}".format(bDBClusterParamGroup.arn)))
        for bDBCluster in self.bDBClusterList:
            data.append(DescribeData(bDBCluster,
                                     "ARN={} Capacity={} Endpoint={}".format(
                                         bDBCluster.arn, bDBCluster.capacity,
                                         bDBCluster.endpoint)))
        DescribeData.logList("ACCOUNT DATABASE SERVER", data)

    # ---------------------- REGISTER / DEREGISTER -------------------------- #
    @logFunc()
    def registerADB(self, bDBCluster):
        """
        Register ADB
        """
        code = """mgr.dbp.accountDBServerP.populateList(readDomainList=["{}"],writeDomainList=["{}"], userList=["{}"],passwordList=["{}"]);
               """.format(bDBCluster.endpoint, bDBCluster.endpoint,
                          self.mgr.secret.bSecret.ADB_USER,
                          self.mgr.secret.bSecret.ADB_PASSWORD)
        self.mgr.mls.runPython(code=code)

    @logFunc()
    def deregisterADB(self, bDBCluster):
        """
        Deregister ADB
        """
        code = """mgr.dbp.accountDBServerP.depopulateWriteDomainList(writeDomainList=["{}"]);
               """.format(bDBCluster.endpoint)
        self.mgr.mls.runPython(code=code)

    # ---------------------- WAKE UP ---------------------------------------- #
    @logFunc()
    def wakeUp(self):
        """
        WakeUp
        """
        def wakeUpSingleADBS(endpoint):
            """
            Wakup a single ADB endpoint
            """
            self.mgr.mls.runPython(
                code =
                """import pprint;"""
                """from base.mysql.python.bServer import BeblsoftMySQLServer;"""
                """bServer = BeblsoftMySQLServer(domainNameFunc= lambda : '{}', user = '{}', password = '{}');"""
                """logger.info(pprint.pformat(bServer.versions));"""
                .format(
                    endpoint,
                    self.mgr.secret.bSecret.ADB_USER,
                    self.mgr.secret.bSecret.ADB_PASSWORD))
        runFuncOnList(
            func     = wakeUpSingleADBS,
            lst      = [bDBCluster.endpoint for bDBCluster in self.bDBClusterList],
            parallel = True)
