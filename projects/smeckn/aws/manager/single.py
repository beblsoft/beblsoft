#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 single.py

DESCRIPTION
 Single Manager

 File contains objects that are created a single time for all environments
 (sometimes by hand via the AWS console) and then not modified after that
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.S3.bBucket import BeblsoftBucket
from base.aws.python.ACM.bCertificate import BeblsoftACMCertificate
from base.aws.python.Route53.bHostedZone import BeblsoftHostedZone
from base.aws.python.KMS.bCMKey import BeblsoftKMSCMKey
from base.aws.python.SES.ReceiptRule.bRuleSet import BeblsoftSESReceiptRuleSet
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ SINGLE MANAGER OBJECT ---------------------------- #
class SingleManager(CommonManager):
    """
    Single Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bPrivDataBucket       = BeblsoftBucket(
            name                           = self.cfg.buckets.privData)
        self.bCertificate          = BeblsoftACMCertificate(
            name                           = "SmecknCert",
            domainName                     = "*.{}".format(self.cfg.domains.main),
            additionalDomains              = ["test.{}".format(self.cfg.domains.main), self.cfg.domains.main])
        self.bHostedZone           = BeblsoftHostedZone(
            domainName                     = self.cfg.domains.main)
        self.bSESReceiptRuleSet    = BeblsoftSESReceiptRuleSet(
            name                           = "BeblsoftReceiptRuleSet")
        self.bKMSCMKey             = BeblsoftKMSCMKey(
            name                           = "SmecknKMSCustomerMasterKey",
            bRegion                        = self.cfg.aws.bRegion)

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bPrivDataBucket.create()
        self.bCertificate.create()
        self.bHostedZone.create()
        self.bSESReceiptRuleSet.create()
        self.bKMSCMKey.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        # self.bKMSCMKey.delete()
        # self.bSESReceiptRuleSet.delete()
        # self.bHostedZone.delete()
        # self.bCertificate.delete()
        # self.bPrivDataBuck
        pass

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bPrivDataBucket, ""),
            DescribeData(self.bCertificate, "Arn={}".format(self.bCertificate.arn)),
            DescribeData(self.bHostedZone, "ID={}".format(self.bHostedZone.id)),
            DescribeData(self.bSESReceiptRuleSet, "Name={}".format(
                self.bSESReceiptRuleSet.name)),
            DescribeData(self.bKMSCMKey, "Arn={}".format(self.bKMSCMKey.arn)),
        ]
        DescribeData.logList("SINGLE", data)
