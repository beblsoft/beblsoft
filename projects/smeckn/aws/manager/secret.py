#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 secret.py

DESCRIPTION
 Secret Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.aws.manager.common import CommonManager
from smeckn.aws.commands.describe import DescribeData


# ------------------------ SECRET MANAGER OBJECT ---------------------------- #
class SecretManager(CommonManager):
    """
    Secret Manager
    """

    def __init__(self, **kwargs):  # pylint: disable=R0915
        """
        Initialize Object
        """
        super().__init__(**kwargs)

        self.bSecret              = self.cfg.secret.type.getBSecret(
            bKMSCustomerMasterKey         = self.mgr.single.bKMSCMKey)

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self):
        """
        Create objects
        """
        self.bSecret.create()

    @logFunc()
    def delete(self):
        """
        Delete objects
        """
        self.bSecret.delete()

    @logFunc()
    def describe(self):
        """
        Describe objects
        """
        data = [
            DescribeData(self.bSecret, ""),
        ]
        DescribeData.logList("SECRETS", data)
