#!/usr/bin/env python3 # pylint: disable=E1101
"""
NAME:
 databasePopulation.py

DESCRIPTION
 Database Population Manager
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.aws.manager.common import CommonManager



# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------ DATABASE POPULATION MANAGER OBJECT --------------- #
class DatabasePopulationManager(CommonManager):
    """
    Database Population Manager
    """

    @logFunc()
    def create(self, admin=False, accounts=False):
        """
        Create objects
        """
        if admin:
            self.deleteAdmin()
            self.createAdmin()
        if accounts:
            self.deleteAccounts()
            self.createAccounts()

    @logFunc()
    @CommonManager.notOnProduction
    def delete(self, admin=False, email=None, accounts=False, allTestAccounts=False, allAccounts=False):
        """
        Delete objects
        """
        if admin:
            self.deleteAdmin()
        if email:
            self.deleteAccount(email=email)
        if accounts:
            self.deleteAccounts()
        if allTestAccounts:
            self.deleteAllTestAccounts()
        if allAccounts:
            self.deleteAllAccounts()

    @logFunc()
    def describe(self, allAccounts=False):
        """
        Describe objects
        """
        if allAccounts:
            self.describeAllAccounts()

    # ------------------------ ADMIN ACCOUNT -------------------------------- #
    @logFunc()
    def createAdmin(self):
        """
        Create admin
        """
        code = """mgr.dbp.createAdmin()"""
        self.mgr.mls.runPython(code=code)

    @logFunc()
    def deleteAdmin(self):
        """
        Delete admin
        """
        code = """mgr.dbp.deleteAdmin()"""
        self.mgr.mls.runPython(code=code)

    # ------------------------ SINGLE ACCOUNT ------------------------------- #
    @logFunc()
    def createAccount(self, email, password):
        """
        Create account
        """
        code = """mgr.dbp.accountP.populateOne(email='{}', password='{}');""".format(email, password)
        self.mgr.mls.runPython(code=code)

    @logFunc()
    @CommonManager.notOnProduction
    def deleteAccount(self, email):
        """
        Delete Accounts
        """
        code = """mgr.dbp.accountP.depopulateByEmail(email='{}');""".format(email)
        self.mgr.mls.runPython(code=code)

    # ------------------------ STANDARD ACCOUNTS ---------------------------- #
    @logFunc()
    def createAccounts(self):
        """
        Create accounts
        """
        code = """mgr.dbp.createAccounts();"""
        self.mgr.mls.runPython(code=code)

    @logFunc()
    def deleteAccounts(self):
        """
        Delete Accounts
        """
        code = """mgr.dbp.deleteAccounts();"""
        self.mgr.mls.runPython(code=code)

    # ------------------------ TEST ACCOUNTS -------------------------------- #
    @logFunc()
    def deleteAllTestAccounts(self):
        """
        Delete all test accounts
        """
        code = ("""from smeckn.server.gDB.account.model import AccountType;"""
                """mgr.dbp.deleteAllAccounts(aType=AccountType.TEST);""")
        self.mgr.mls.runPython(code=code)

    # ------------------------ ALL ACCOUNTS --------------------------------- #
    @logFunc()
    @CommonManager.notOnProduction
    def deleteAllAccounts(self):
        """
        Delete all accounts
        """
        code = """mgr.dbp.deleteAllAccounts();"""
        self.mgr.mls.runPython(code=code)

    @logFunc()
    def describeAllAccounts(self):
        """
        Describe objects
        """
        nRegularAccounts = self.mgr.gdb.runSQL(sqlStatement="SELECT COUNT(email) FROM Account WHERE type='REGULAR'")
        nAdminAccounts   = self.mgr.gdb.runSQL(sqlStatement="SELECT COUNT(email) FROM Account WHERE type='ADMIN'")
        nTestAccounts    = self.mgr.gdb.runSQL(sqlStatement="SELECT COUNT(email) FROM Account WHERE type='TEST'")
        logger.info("Accounts: Regular={} Admin={} Test={}".format(nRegularAccounts, nAdminAccounts, nTestAccounts))
        regularEmailList = self.mgr.gdb.runSQL(sqlStatement="SELECT email FROM Account WHERE type='REGULAR'")
        logger.info("Regular Account Emails: {}".format(regularEmailList))
