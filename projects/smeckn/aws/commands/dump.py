#!/usr/bin/env python3
"""
NAME:
 dump.py

DESCRIPTION
 Dump Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click
from click import UsageError


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- DUMP ---------------------------------------------- #
@click.command()
@click.option('--mls', is_flag=True, default=False,
              help="Dump Manager Lambda Switch objects")
@click.option('--mlslogstream', default=None,
              help="Dump Manager Lambda Switch log stream")
@click.option('--mlsfilter', default="",
              help="Dump Manager Lambda Switch log filter")
@click.option('--mlsdumpfile', default=None,
              help="Dump Manager Lambda Switch to log file")
@click.option('--sls', is_flag=True, default=False,
              help="Dump Server Lambda Switch objects")
@click.option('--slslogstream', default=None,
              help="Dump Server Lambda Switch log stream")
@click.option('--slsfilter', default="",
              help="Dump Server Lambda Switch log filter")
@click.option('--slsdumpfile', default=None,
              help="Dump Server Lambda Switch to log file")
@click.pass_context
def dump(ctx,
         mls, mlslogstream, mlsfilter, mlsdumpfile,
         sls, slslogstream, slsfilter, slsdumpfile):
    """
    Dump Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if mls:
        if not mlslogstream:
            raise UsageError("Must specify mlslogstream")
        mgr.mls.dumpStreamToFile(lsName=mlslogstream,
                                 filterPattern=mlsfilter, filePath=mlsdumpfile)

    if sls:
        if not slslogstream:
            raise UsageError("Must specify slslogstream")
        mgr.sls.dumpStreamToFile(lsName=slslogstream,
                                 filterPattern=slsfilter, filePath=slsdumpfile)
