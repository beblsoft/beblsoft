#!/usr/bin/env python3
"""
NAME:
 tail.py

DESCRIPTION
 Tail Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- TAIL ---------------------------------------------- #
@click.command()
@click.option('--mls', is_flag=True, default=False,
              help="Tail Manager Lambda Switch objects")
@click.option('--sls', is_flag=True, default=False,
              help="Tail Server Lambda Switch objects")
@click.pass_context
def tail(ctx, mls, sls):
    """
    Tail Infrastructure Logs
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if mls:
        mgr.mls.tail()

    if sls:
        mgr.sls.tail()
