#!/usr/bin/env python3
"""
NAME:
 describe.py

DESCRIPTION
 Describe Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click
from click import UsageError
from base.bebl.python.color.bColor import BeblsoftColor


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- DESCRIBE DATA ------------------------------------- #
class DescribeData():
    """
    Class to organize describe data
    """

    def __init__(self, obj, objStr):
        """
        Initialize OBject
        Args
          obj:
            Beblsoft Object
            Note: must have an "exists" method
          objStr:
            String describing object state
        """
        self.obj    = obj
        self.objStr = objStr

    @property
    def existStr(self):
        """
        Return exists str
        """
        existStr = ""
        if self.obj.exists:
            existStr = BeblsoftColor.str2Color("EXISTS", BeblsoftColor.GREEN)
        else:
            existStr = BeblsoftColor.str2Color("DNE", BeblsoftColor.RED)
        return existStr

    @staticmethod
    def logList(titleStr, dataList, titleLength=120, titleStartDashes=30):
        """
        Log Describe Data
        Args
          title:
            String title
          dataList:
            List of DescribeData objects
        """
        logger.info("\n{} {} {}".format(
            "-" * titleStartDashes, titleStr,
            "-" * (titleLength - titleStartDashes - len(titleStr))))
        for d in dataList:
            logger.info("{} {} {}".format(
                str(d.obj).ljust(110), d.existStr.ljust(20), d.objStr))


# ----------------------- DESCRIBE ------------------------------------------ #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Describe all objects")
@click.option('--single', is_flag=True, default=False,
              help="Describe single objects")
@click.option('--ses', is_flag=True, default=False,
              help="Describe ses objects")
@click.option('--secret', is_flag=True, default=False,
              help="Describe secret objects")
@click.option('--netbase', is_flag=True, default=False,
              help="Describe network base objects")
@click.option('--netpub', is_flag=True, default=False,
              help="Describe network subnets public")
@click.option('--netprinat', is_flag=True, default=False,
              help="Describe network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False,
              help="Describe network subnets private")
@click.option('--netnat', is_flag=True, default=False,
              help="Describe network NAT objects")
@click.option('--mls', is_flag=True, default=False,
              help="Describe Manager Lambda Switch objects")
@click.option('--bastion', is_flag=True, default=False,
              help="Describe bastion objects")
@click.option('--gdbs', is_flag=True, default=False,
              help="Describe global database server objects")
@click.option('--gdb', is_flag=True, default=False,
              help="Describe global database objects")
@click.option('--adbs', is_flag=True, default=False,
              help="Describe account database servers")
@click.option('--adb', is_flag=True, default=False,
              help="Describe Account database")
@click.option('--adbemail', default=None,
              help="Describe Account database email")
@click.option('--adbid', default=None,
              help="Describe Account database id")
@click.option('--dbpallaccounts', is_flag=True, default=False,
              help="Describe database population all accounts")
@click.option('--sls', is_flag=True, default=False,
              help="Describe Server Lambda Switch Base objects")
@click.option('--slskw', is_flag=True, default=False,
              help="Describe Server Lambda Switch Keep Warm objects")
@click.option('--slsf', is_flag=True, default=False,
              help="Describe Server Lambda Switch Flask objects")
@click.option('--slssesce', is_flag=True, default=False,
              help="Describe Server Lambda Switch SES Configuration Event objects")
@click.option('--slsjq', is_flag=True, default=False,
              help="Describe Server Lambda Switch Job Queue objects")
@click.option('--webclient', is_flag=True, default=False,
              help="Describe Web Client objects")
@click.pass_context
def describe(ctx,
             allobjs, single,
             ses, secret,
             netbase, netpub, netprinat, netpri, netnat,
             mls,
             bastion,
             gdbs,
             gdb,
             adbs,
             adb, adbemail, adbid,
             dbpallaccounts,
             sls, slskw, slsf, slssesce, slsjq,
             webclient):
    """
    Describe Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if single or allobjs:
        mgr.single.describe()

    if ses or allobjs:
        mgr.ses.describe()

    if secret or allobjs:
        mgr.secret.describe()

    mgr.network.describe(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)

    if mls or allobjs:
        mgr.mls.describe()

    if bastion or allobjs:
        mgr.bastion.describe()

    if gdbs or allobjs:
        mgr.gdbs.describe()

    if gdb:
        mgr.gdb.describe()

    if adbs or allobjs:
        mgr.adbs.describe()

    if adb:
        if not adbid and not adbemail:
            raise UsageError("Specify adbid or adbemail")
        mgr.adb.describe(aID=adbid, email=adbemail)

    mgr.dbp.describe(
        allAccounts = dbpallaccounts)

    mgr.sls.describe(
        ls      = sls or allobjs,
        lskw    = slskw or allobjs,
        lsf     = slsf or allobjs,
        lssesce = slssesce or allobjs,
        lsjq    = slsjq or allobjs)

    if webclient or allobjs:
        mgr.webClient.describe()
