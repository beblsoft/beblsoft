#!/usr/bin/env python3
"""
NAME:
 delete.py

DESCRIPTION
 Delete Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- DELETE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Delete all objects [except for single, secret]")
@click.option('--single', is_flag=True, default=False,
              help="Delete single objects")
@click.option('--ses', is_flag=True, default=False,
              help="Delete ses objects")
@click.option('--secret', is_flag=True, default=False,
              help="Delete secret objects")
@click.option('--netbase', is_flag=True, default=False,
              help="Delete network base objects")
@click.option('--netpub', is_flag=True, default=False,
              help="Delete network subnets public")
@click.option('--netprinat', is_flag=True, default=False,
              help="Delete network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False,
              help="Delete network subnets private")
@click.option('--netnat', is_flag=True, default=False,
              help="Delete network NAT objects")
@click.option('--mls', is_flag=True, default=False,
              help="Delete Manager Lambda Switch objects")
@click.option('--bastion', is_flag=True, default=False,
              help="Delete bastion objects")
@click.option('--gdbs', is_flag=True, default=False,
              help="Delete global database server")
@click.option('--gdb', is_flag=True, default=False,
              help="Delete global database")
@click.option('--adbs', is_flag=True, default=False,
              help="Delete account database servers")
@click.option('--dbpadmin', is_flag=True, default=False,
              help="Delete database population admin")
@click.option('--dbpaccountemail', default=None,
              help="Delete database population account email")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Delete database population accounts")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Delete database population standard accounts")
@click.option('--dbpalltestaccounts', is_flag=True, default=False,
              help="Delete database population all test accounts")
@click.option('--dbpallaccounts', is_flag=True, default=False,
              help="Delete database population all accounts")
@click.option('--sls', is_flag=True, default=False,
              help="Delete Server Lambda Switch Base objects")
@click.option('--slskw', is_flag=True, default=False,
              help="Delete Server Lambda Switch Keep Warm objects")
@click.option('--slsf', type=click.Choice(['all', 'partial']), default=None,
              help="Delete Server Lambda Switch Flask objects")
@click.option('--slssesce', is_flag=True, default=False,
              help="Delete Server Lambda Switch SES Configuration Event objects")
@click.option('--slsjq', is_flag=True, default=False,
              help="Delete Server Lambda Switch Job Queue objects")
@click.option('--webclient', is_flag=True, default=False,
              help="Delete Web Client objects")
@click.pass_context
def delete(ctx,
           allobjs, single,
           ses, secret,
           netbase, netpub, netprinat, netpri, netnat,
           mls,
           bastion,
           gdbs,
           gdb,
           adbs,
           dbpadmin, dbpaccountemail, dbpaccounts, dbpalltestaccounts, dbpallaccounts,
           sls, slskw, slsf, slssesce, slsjq,
           webclient):
    """
    Delete Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if webclient or allobjs:
        mgr.webClient.delete()

    mgr.sls.delete(
        ls      = sls or allobjs,
        lskw    = slskw or allobjs,
        lsf     = "all" if allobjs else slsf,
        lssesce = slssesce or allobjs,
        lsjq    = slsjq or allobjs)

    if dbpadmin or dbpaccountemail or dbpaccounts or dbpalltestaccounts or dbpallaccounts or allobjs: #pylint: disable=R0916
        mgr.dbp.delete(
            admin           = dbpadmin or allobjs,
            email           = dbpaccountemail,
            accounts        = dbpaccounts or allobjs,
            allTestAccounts = dbpalltestaccounts or allobjs,
            allAccounts     = dbpallaccounts or allobjs)

    if adbs or allobjs:
        mgr.adbs.delete()

    if gdb or allobjs:
        mgr.gdb.delete()

    if gdbs or allobjs:
        mgr.gdbs.delete()

    if bastion or allobjs:
        mgr.bastion.delete()

    if mls or allobjs:
        mgr.mls.delete()

    mgr.network.delete(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)

    if secret:
        mgr.secret.delete()

    if ses or allobjs:
        mgr.ses.delete()

    if single:
        mgr.single.delete()
