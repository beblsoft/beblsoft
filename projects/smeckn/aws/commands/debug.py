#!/usr/bin/env python3
"""
NAME:
 debug.py

DESCRIPTION
 Debug Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import click


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- DEBUG --------------------------------------------- #
@click.command()
@click.option('--mls', is_flag=True, default=False,
              help="Debug Manager Lambda Switch objects")
@click.option('--mlscodeerror', is_flag=True, default=False,
              help="Debug Manager Lambda Switch code error")
@click.option('--mlstimeouterror', is_flag=True, default=False,
              help="Debug Manager Lambda Switch timeout error")
@click.option('--mlsmemoryerror', is_flag=True, default=False,
              help="Debug Manager Lambda Switch memory error")
@click.option('--gdb', is_flag=True, default=False,
              help="Debug global database")
@click.option('--adb', is_flag=True, default=False,
              help="Debug account database")
@click.option('--sls', is_flag=True, default=False,
              help="Debug Server Lambda Switch objects")
@click.option('--slsjq', is_flag=True, default=False,
              help="Debug Server Lambda Switch job queue objects")
@click.pass_context
def debug(ctx,
          mls, mlscodeerror, mlstimeouterror, mlsmemoryerror,
          gdb,
          adb,
          sls, slsjq):
    """
    Debug Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if mls:
        mgr.mls.runPython(
            code=("""print("Hello World!");"""
                  """rval = 0;"""))
    if mlscodeerror:
        mgr.mls.runPython(code=("""print("Hello World!;"""))

    if mlstimeouterror:
        mgr.mls.runPython(code=("""while True: pass;"""))

    if mlsmemoryerror:
        mgr.mls.runPython(code=("""lis = []; while 1: lis.append("a" * 10000000);"""))  # Doesn't work

    if gdb:
        # General
        # -----------------
        logger.info(mgr.gdb.runSQL(sqlStatement="SHOW TABLES;"))
        # logger.info(mgr.gdb.runSQL(sqlStatement="SELECT writeDomain from AccountDBServer;"))
        # logger.info(mgr.gdb.runSQL(sqlStatement="SELECT email FOM Account;"))
        logger.info(mgr.gdb.runSQL(sqlStatement="SELECT * FROM alembic_version;"))
        logger.info(mgr.gdb.runSQL(sqlStatement="SHOW COLUMNS FROM Account;"))
        logger.info(mgr.gdb.runSQL(sqlStatement="SELECT writeDomain, readDomain FROM AccountDBServer;"))

        # Collation Changes
        # -----------------
        # logger.info(mgr.gdb.runSQL(sqlStatement="show databases;"))
        # logger.info(mgr.gdb.runSQL(sqlStatement="ALTER DATABASE SmecknGDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"))
        # logger.info(mgr.gdb.runSQL(sqlStatement="""SELECT DEFAULT_CHARACTER_SET_NAME, DEFAULT_COLLATION_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'SmecknGDB' """))

    if adb:
        email = "success+webAPITest-1@simulator.amazonses.com"
        logger.info(mgr.adb.runSQL(email=email, sqlStatement="SHOW TABLES;"))
        logger.info(mgr.adb.runSQL(email=email, sqlStatement="""SHOW VARIABLES LIKE '%version%';"""))
        logger.info(mgr.adb.runSQL(email=email, sqlStatement="SELECT name FROM ProfileGroup;"))
        logger.info(mgr.adb.runSQL(email=email, sqlStatement="SELECT smID FROM Profile;"))

    if sls:
        mgr.sls.runPython(
            code=("""import os;"""
                  """import pprint;"""
                  """print(pprint.pformat(mgr.cfg.__dict__));"""
                  """print(os.getenv("SERVER_DOMAIN", None));"""
                  """rval = 0;"""))

    if slsjq:
        mgr.sls.runPython(
            code=("""from smeckn.server.gJob.helloWorld.job import HelloWorldJob;"""
                  """rval = HelloWorldJob.invoke(mgr=mgr);"""))
