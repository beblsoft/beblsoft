#!/usr/bin/env python3
"""
NAME:
 create.py

DESCRIPTION
 Create Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- CREATE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Create all objects [except for bastion]")
@click.option('--single', is_flag=True, default=False,
              help="Create single objects")
@click.option('--ses', is_flag=True, default=False,
              help="Create ses objects")
@click.option('--secret', is_flag=True, default=False,
              help="Create secret objects")
@click.option('--netbase', is_flag=True, default=False,
              help="Create network base objects")
@click.option('--netpub', is_flag=True, default=False,
              help="Create network subnets public")
@click.option('--netprinat', is_flag=True, default=False,
              help="Create network subnets private NAT")
@click.option('--netpri', is_flag=True, default=False,
              help="Create network subnets private")
@click.option('--netnat', is_flag=True, default=False,
              help="Create network NAT objects")
@click.option('--mls', is_flag=True, default=False,
              help="Create Manager Lambda Switch objects")
@click.option('--bastion', is_flag=True, default=False,
              help="Create bastion objects")
@click.option('--gdbs', is_flag=True, default=False,
              help="Create global database server")
@click.option('--gdb', is_flag=True, default=False,
              help="Create global database")
@click.option('--adbs', is_flag=True, default=False,
              help="Create account database servers")
@click.option('--dbpadmin', is_flag=True, default=False,
              help="Create database population admin")
@click.option('--dbpaccounts', is_flag=True, default=False,
              help="Create database population standard accounts")
@click.option('--sls', is_flag=True, default=False,
              help="Create Server Lambda Switch Base objects")
@click.option('--slskw', is_flag=True, default=False,
              help="Create Server Lambda Switch Keep Warm objects")
@click.option('--slsf', is_flag=True, default=False,
              help="Create Server Lambda Switch Flask objects")
@click.option('--slssesce', is_flag=True, default=False,
              help="Create Server Lambda Switch SES Configuration Event objects")
@click.option('--slsjq', is_flag=True, default=False,
              help="Create Server Lambda Switch Job Queue objects")
@click.option('--webclient', is_flag=True, default=False,
              help = "Create web client objects")
@click.pass_context
def create(ctx,
           allobjs, single,
           ses, secret,  # pylint: disable=R0915,R0912
           netbase, netpub, netprinat, netpri, netnat,
           mls,
           bastion,
           gdbs,
           gdb,
           adbs,
           dbpadmin, dbpaccounts,
           sls, slskw, slsf, slssesce, slsjq,
           webclient):
    """
    Create Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if single or allobjs:
        mgr.single.create()

    if ses or allobjs:
        mgr.ses.create()

    if secret or allobjs:
        mgr.secret.create()

    mgr.network.create(
        base   = netbase or allobjs,
        pub    = netpub or allobjs,
        prinat = netprinat or allobjs,
        pri    = netpri or allobjs,
        nat    = netnat or allobjs)

    if mls or allobjs:
        mgr.mls.create()

    if bastion:
        mgr.bastion.create()

    if gdbs or allobjs:
        mgr.gdbs.create()

    if gdb or allobjs:
        mgr.gdb.create()

    if adbs or allobjs:
        mgr.adbs.create()

    mgr.dbp.create(
        admin    = dbpadmin or allobjs,
        accounts = dbpaccounts or allobjs)

    mgr.sls.create(
        ls      = sls or allobjs,
        lskw    = slskw or allobjs,
        lsf     = slsf or allobjs,
        lssesce = slssesce or allobjs,
        lsjq    = slsjq or allobjs)

    if webclient or allobjs:
        mgr.webClient.create()
