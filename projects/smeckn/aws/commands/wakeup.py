#!/usr/bin/env python3
"""
NAME:
 wakeup.py

DESCRIPTION
 Wake Up Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ------------------------ WAKE UP ------------------------------------------ #
@click.command()
@click.option('--gdbs', is_flag=True, default=False,
              help="Wake Up global database server")
@click.option('--adbs', is_flag=True, default=False,
              help="Wake up account database servers")
@click.pass_context
def wakeup(ctx,
           gdbs,
           adbs):
    """
    Wake Up Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if gdbs:
        mgr.gdbs.wakeUp()

    if adbs:
        mgr.adbs.wakeUp()
