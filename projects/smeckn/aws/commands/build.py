#!/usr/bin/env python3
"""
NAME:
 build.py

DESCRIPTION
 Build Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- BUILD --------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Build all objects [except for bastion]")
@click.option('--webclient', is_flag=True, default=False,
              help = "Build web client objects")
@click.pass_context
def build(ctx,
          allobjs, webclient):
    """
    Build Infrastucture
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if webclient or allobjs:
        mgr.webClient.deleteCode()
        mgr.webClient.buildCode()
