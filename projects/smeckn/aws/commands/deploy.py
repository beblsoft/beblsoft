#!/usr/bin/env python3
"""
NAME:
 deploy.py

DESCRIPTION
 Deploy Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- CREATE -------------------------------------------- #
@click.command()
@click.option('--allobjs', is_flag=True, default=False,
              help="Deploy to all objects")
@click.option('--mls', is_flag=True, default=False,
              help="Deploy Manager Lambda Switch Base objects")
@click.option('--gdbs', is_flag=True, default=False,
              help="Deploy global database server")
@click.option('--gdb', is_flag=True, default=False,
              help="Deploy to global database")
@click.option('--adbs', is_flag=True, default=False,
              help="Deploy account database servers")
@click.option('--adb', is_flag=True, default=False,
              help="Deploy to account databases")
@click.option('--sls', is_flag=True, default=False,
              help="Deploy Server Lambda Switch Base objects")
@click.option('--slskw', is_flag=True, default=False,
              help="Deploy Server Lambda Switch Keep Warm objects")
@click.option('--slsf', is_flag=True, default=False,
              help="Deploy Server Lambda Switch Flask objects")
@click.option('--slssesce', is_flag=True, default=False,
              help="Deploy Server Lambda Switch SES Configuration Event objects")
@click.option('--slsjq', is_flag=True, default=False,
              help="Deploy Server Lambda Switch Job Queue objects")
@click.option('--webclient', is_flag=True, default=False,
              help="Deploy Web Client objects")
@click.pass_context
def deploy(ctx,
           allobjs,
           mls,
           gdbs, gdb,
           adbs, adb,
           sls, slskw, slsf, slssesce, slsjq,
           webclient):
    """
    Deploy the latest changes onto infrastucture.
    Assumes that all infrastructure has already been created.

    \b
    Database Field Additions
    * Stage 1
      Run deploy script [mls, gdb, adb's, sls, adb's]
      At this point sls is writing records correctly to new DB fields
      Must update adb's a second time after sls because sls might have created
      new accounts in the time period after adb's were first patched

    \b
    Database Field Removals
    * Stage 1
      Deprecate access to database fields in SQLAlchemy models
      Run deploy script [mls, gdb, adb's, sls, adb's]
    * State 2
      Remove DB field
      Run deploy script [mls, gdb, adb's, sls, adb's]
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if mls or allobjs:
        mgr.mls.update()

    if gdbs or allobjs:
        mgr.gdbs.update()

    if gdb or allobjs:
        mgr.gdb.upgradeToMLS()

    if adbs or allobjs:
        mgr.adbs.update()

    if adb or allobjs:
        mgr.adb.upgradeToMLS()

    mgr.sls.update(
        ls      = sls or allobjs,
        lskw    = slskw or allobjs,
        lsf     = slsf or allobjs,
        lssesce = slssesce or allobjs,
        lsjq    = slsjq or allobjs)

    # Must also update adb here, as SLS could have created more accounts
    if adb or allobjs:
        mgr.adb.upgradeToMLS()

    if webclient or allobjs:
        mgr.webClient.update()

    return ctx
