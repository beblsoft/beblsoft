#!/usr/bin/env python3
"""
NAME:
 downgrade.py

DESCRIPTION
 Downgrade Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click
from click import UsageError


# ----------------------- DOWNGRADE ----------------------------------------- #
@click.command()
@click.option('--gdb', is_flag=True, default=False,
              help="Downgrade global database")
@click.option('--gdbversion', default=None,
              help="Global database version")
@click.option('--adb', is_flag=True, default=False,
              help="Downgrade all account databases")
@click.option('--adbversion', default=None,
              help="Account database version")
@click.pass_context
def downgrade(ctx,
              gdb, gdbversion,
              adb, adbversion):
    """
    Downgrade Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    if gdb:
        if not gdbversion:
            raise UsageError("Please specify gdbversion")
        mgr.gdb.downgrade(toVersion=gdbversion)

    if adb:
        if not adbversion:
            raise UsageError("Please specify adbversion")
        mgr.adb.downgrade(toVersion=adbversion)
