#!/usr/bin/env python3
"""
NAME:
 test.py

DESCRIPTION
 Test Command
"""

# ----------------------- IMPORTS ------------------------------------------- #
import click
from smeckn.aws.manager.test import TestModule



# ----------------------- TEST ---------------------------------------------- #
@click.command()
@click.option("--module", default=None,
              type=click.Choice(
                  [name for name, member in TestModule.__members__.items()]),
              help="Run Specific Test Module")
@click.option("--name", default=None, help="Run Specific Test Name\nEx:smeckn.aws.test.a_test")
@click.pass_context
def test(ctx, module, name):
    """
    Test Infrastructure
    """
    gc  = ctx.obj["gc"]
    mgr = gc.aws.mgr

    mgr.test.test(module=module, name=name)
