#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 Smeckn AWS CLI
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from smeckn.aws.gc import gc
from smeckn.aws.config.environment import Environment
from smeckn.aws.commands.create import create
from smeckn.aws.commands.delete import delete
from smeckn.aws.commands.describe import describe
from smeckn.aws.commands.build import build
from smeckn.aws.commands.start import start
from smeckn.aws.commands.stop import stop
from smeckn.aws.commands.tail import tail
from smeckn.aws.commands.wakeup import wakeup
from smeckn.aws.commands.debug import debug
from smeckn.aws.commands.upgrade import upgrade
from smeckn.aws.commands.downgrade import downgrade
from smeckn.aws.commands.dump import dump
from smeckn.aws.commands.test import test
from smeckn.aws.commands.deploy import deploy
from smeckn.aws.manager import AWSManager
from smeckn.common.constant import Constant


# ----------------------- GLOBALS ------------------------------------------- #
logger         = logging.getLogger(__name__)
const          = Constant()
curDir         = os.path.dirname(os.path.realpath(__file__))
defaultLogFile = os.path.join(curDir, "cli.log")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--env", "-e", type=click.Choice(env.name for env in list(Environment)), required=True,
              help="Specify environment")
@click.option("--logfile", default=defaultLogFile, type=click.Path(writable=True),
              help="Specify log file. Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
@click.option("-f", "--force", is_flag=True, default=False,
              help="Force changes, overrides production barriers.")
@click.pass_context
def cli(ctx, env, logfile, appendlog, verbose, force):
    """
    Smeckn AWS Command Line Interface
    """
    ctx.obj["gc"] = gc
    gc.force      = force
    gc.aws.cfg    = Environment[env].config
    gc.aws.mgr    = AWSManager(cfg=gc.aws.cfg)
    gc.bLog       = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- COMMANDS ------------------------------------------ #
cli.add_command(create)
cli.add_command(delete)
cli.add_command(describe)
cli.add_command(build)
cli.add_command(start)
cli.add_command(stop)
cli.add_command(tail)
cli.add_command(wakeup)
cli.add_command(debug)
cli.add_command(upgrade)
cli.add_command(downgrade)
cli.add_command(dump)
cli.add_command(test)
cli.add_command(deploy)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as e:  # pylint: disable=W0703
        # Raise so exit code is set
        raise e
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
