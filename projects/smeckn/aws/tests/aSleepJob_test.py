#!/usr/bin/env python3
"""
 NAME:
  aSleepJob_test.py

 DESCRIPTION
  Account Sleep Job Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import time
import logging
from smeckn.aws.tests import AWSTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class AccountSleepJobTestCase(AWSTestCase):
    """
    Account Sleep Job Test Case
    """

    def setUp(self):  # pylint: disable=W0221
        super().setUp(gDBSWakeUp=True, aDBSWakeUp=True, setUpAccount=True)

    def test_valid(self):
        """
        Valid test
        """

        # Invoke async job
        code = ("""from smeckn.server.aJob.sleep import SleepJob;"""
                """gDB = mgr.gdb.gDB;"""
                """SleepJob.invokeAsync(mgr = mgr,"""
                """                     classKwargs = SleepJob.getClassKwargsFromAccount(gDB=gDB, email='{}', heartbeatPeriodS=1),"""
                """                     executeKwargs = {{"sleepS": 10}});""").format(self.email)
        self.mgr.sls.runPython(code=code)

        # Verify job is in DB
        time.sleep(5)
        aJobClassList = self.mgr.adb.runSQL(email=self.email, sqlStatement="""SELECT className FROM AccountJob""")
        self.assertEqual(len(aJobClassList), 1)
        self.assertEqual(aJobClassList[0][0], "SleepJob")

        # Verify job was removed from DB after completion
        time.sleep(10)
        aJobClassList = self.mgr.adb.runSQL(email=self.email, sqlStatement="""SELECT className FROM AccountJob""")
        self.assertEqual(len(aJobClassList), 0)
