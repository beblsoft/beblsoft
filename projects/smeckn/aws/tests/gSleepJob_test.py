#!/usr/bin/env python3
"""
 NAME:
  gSleepJob_test.py

 DESCRIPTION
  Global Sleep Job Tests
"""


# ---------------------------- IMPORTS -------------------------------------- #
import time
import logging
from smeckn.aws.tests import AWSTestCase


# ---------------------------- GLOBALS -------------------------------------- #
logger = logging.getLogger(__file__)


# ---------------------------- CLASSES -------------------------------------- #
class GlobalSleepJobTestCase(AWSTestCase):
    """
    Global Sleep Job Test Case
    """

    def setUp(self):  # pylint: disable=W0221
        super().setUp(gDBSWakeUp=True, aDBSWakeUp=True)

    def test_valid(self):
        """
        Valid test
        """
        # Invoke async job
        code = ("""from smeckn.server.gJob.sleep import SleepJob;"""
                """SleepJob.invokeAsync(mgr = mgr, classKwargs = {"heartbeatPeriodS": 1},"""
                """                     executeKwargs = {"sleepS": 10})""")
        self.mgr.sls.runPython(code=code)

        # Verify job is in DB
        time.sleep(5)
        gJobClassList = self.mgr.gdb.runSQL(sqlStatement="""SELECT className FROM GlobalJob""")
        self.assertEqual(len(gJobClassList), 1)
        self.assertEqual(gJobClassList[0][0], "SleepJob")

        # Verify job was removed from DB after completion
        time.sleep(10)
        gJobClassList = self.mgr.gdb.runSQL(sqlStatement="""SELECT className FROM GlobalJob""")
        self.assertEqual(len(gJobClassList), 0)
