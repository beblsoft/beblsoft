#!/usr/bin/env python3
"""
 NAME:
  __init__.py

 DESCRIPTION
  AWS Test Case Functionality
"""

# ---------------------------- IMPORTS -------------------------------------- #
import unittest
import logging
from base.bebl.python.log.bLogFunc import logFunc
from smeckn.aws.gc import gc


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
cfg    = gc.aws.cfg
mgr    = gc.aws.mgr


# ---------------------------- AWS TEST CASE -------------------------------- #
class AWSTestCase(unittest.TestCase):
    """
    AWS Test Case
    """

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    @logFunc()
    def setUp(self, gDBSWakeUp=False, aDBSWakeUp=False, setUpAccount=False): #pylint: disable=W0221
        self.cfg = cfg
        self.mgr = mgr

        if gDBSWakeUp:
            self.mgr.gdbs.wakeUp()
        if aDBSWakeUp:
            self.mgr.adbs.wakeUp()
        if setUpAccount:
            self.setUpAccount()

    @logFunc()
    def tearDown(self):
        pass

    # ------------------------ SETUP TEST ACCOUNT --------------------------- #
    def setUpAccount(self):
        """
        SetUp Test Account
        """
        self.email    = "beblsofttest@gmail.com"  # Must be a valid email!
        self.password = "2309487dsf093$@@$^@#W23e"
        self.mgr.dbp.createAccount(email=self.email, password=self.password)
