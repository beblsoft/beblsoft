alembic==1.0.2
aniso8601==2.0.0
boto3==1.9.27
botocore==1.12.27
certifi==2018.10.15
chardet==3.0.4
click==6.7
dnspython==1.15.0
docutils==0.14
email-validator==1.0.3
Flask==0.12.2
Flask-Cors==3.0.3
flask-restplus==0.10.1
flask-swagger==0.2.13
ForgeryPy3==0.3.1
gitdb2==2.0.5
GitPython==2.1.11
idna==2.7
itsdangerous==0.24
Jinja2==2.10
jmespath==0.9.3
jsonschema==2.6.0
Mako==1.0.7
MarkupSafe==1.0
multidict==4.5.2
mysqlclient==1.3.10
PyJWT==1.6.4
python-dateutil==2.7.2
python-editor==1.0.3
pytz==2017.3
PyYAML==3.12
requests==2.20.0
s3transfer==0.1.13
six==1.11.0
smmap2==2.0.5
SQLAlchemy==1.2.2
stripe==2.17.0
urllib3==1.23
vcrpy==2.0.1
Werkzeug==0.13
wrapt==1.11.1
yarl==1.3.0
