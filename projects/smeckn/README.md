# Smeckn Documentation

All Smeckn internal documentation is available through a VuePress web application.

To run the application:

```bash
cd $SMECKN/docs;

# Install dependencies
npm install

# Start server
npm run dev
```
