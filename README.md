# Beblsoft Source Code

## Getting Started

- [Linux Mint Setup](tools/linuxMint/README.md)
- [Sublime Setup](tools/sublime/README.md)

## Filesystem Tree

```bash
$ cd $BEBLSOFT; tree -L 1
.
├── projects      : Production Applications
├── playground    : Prototype software
├── base          : Common code for all repository clients
├── tools         : Code and documentation pertaining to environment setup
├── research      : Documented research
└── templates     : Common file templates
```

## Projects

The `projects` directory hosts all of Beblsoft's production code.

- CICD: [README](projects/cicd/README.md)
- Mojourney: [README](projects/mojourney/README.txt)
- Quotable Joy: [README](projects/quote/README.txt)
- Smeckn: [README](projects/smeckn/README.md)

## Playground

The `playground` directory is the place for prototyping.
Directories are named after the corresponding language or technology they describe.
Each directory should have an associated `README` and `README-projects`.

Playground Technologies:

- alembic: [README](playground/alembic/README.txt), [README-projects](playground/alembic/README-projects.txt)
- android: [README](playground/android/README.md)
- angular: [README](playground/angular/README.txt), [README-projects](playground/angular/README-projects.txt)
- bash: [README](playground/bash/README.md), [README-projects](playground/bash/README-projects.md)
- bootstrap: [README](playground/bootstrap/README.md), [README-projects](playground/bootstrap/README-projects.md)
- c: [README](playground/c/README.md), [README-projects](playground/c/README-projects.md)
- c++: [README](playground/c++/README.txt), [README-projects](playground/c++/README-projects.txt)
- css: [README](playground/css/README.md), [README-projects](playground/css/README-projects.md)
- cypress: [README](playground/cypress/README.md), [README-plugins](playground/cypress/README-plugins.md),
  [README-projects](playground/cypress/README-projects.md)
- docker: [README](playground/docker/README.txt), [README-projects](playground/docker/README-projects.txt)
- docker-compose: [README](playground/dockerCompose/README.txt), [README-projects](playground/dockerCompose/README-projects.txt)
- docker-machine: [README](playground/dockerMachine/README.txt)
- docker-swarm: [README](playground/dockerSwarm/README.txt)
- flask: [README](playground/flask/README.txt), [README-projects](playground/flask/README-projects.txt)
- flaskRestplus: [README](playground/flaskRestplus/README.txt), [README-projects](playground/flaskRestplus/README-projects.txt)
- html: [README](playground/html/README.md)
- java: [README](playground/java/README.md), [README-projects](playground/java/README-projects.md)
- javaScript: [README](playground/javaScript/README.md)
- kubernetes: [README](playground/kubernetes/README.txt)
- LaTex: [README](playground/LaTeX/README.md),
  [README-projects](playground/LaTeX/README-projects.md)
- markdown: [README](playground/markdown/README.md),
  [README-projects](playground/markdown/README-prjects.md),
  [github](playground/markdown/github.md),
  [gitlab](playground/markdown/gitlab.md)
- mjml: [README](playground/mjml/README.txt), [README-projects](playground/mjml/README-projects.txt)
  [core/README-projects](playground/node/core/README-projects.md),
- npm: [README](playground/npm/README.txt), [README-projects](playground/npm/README-projects.txt)
- perl: [README](playground/perl/README.md), [README-projects](playground/perl/README-projects.md)
- prometheus: [README](playground/prometheus/README.txt), [README-projects](playground/prometheus/README-projects.txt)
- python: [README](playground/python/README.md)
- redis: [README](playground/redis/README.txt)
- ruby: [README](playground/ruby/README.md), [README-projects](playground/ruby/README-projects.md)
- sass: [README](playground/sass/README.md), [README-projects](playground/sass/README-projects.md)
- sqlalchemy: [README](playground/sqlalchemy/README.txt), [README-projects](playground/sqlalchemy/README-projects.txt)
- statsD: [README](playground/statsD/README.md)
- vue: [README](playground/vue/README.md)
- webpack: [README](playground/webpack/README.txt), [README-projects](playground/webpack/README-projects.txt)

## Tools

The `tools` directory contains software for development setup, management, and testing.
Each directory should have an associated `README`.

Each new technology in the Beblsoft stack often requires install and management scripts.
Each script has the naming convention: `$TOOLS/bash/<technology-name>.sh`
Each script should prefix all of its functions with `<technology>_`
Ex. `$TOOLS/bash/docker.sh` has functions `docker_install`, `docker_login`, `docker_logout`.
The bash_profile, located at `$TOOLS/bash/bash_profile`, sources all of these technology
scripts on startup to load them into the terminal environment.

Tools:

- bash: [bash_profile](tools/bash/bash_profile)
- cron: [README](tools/cron/README.md)
- emacs: [README](tools/emacs/README.md)
- eslint: [README](tools/eslint/README.txt)
- git: [README](tools/git/README.txt)
- gitLab: [README](tools/gitLab/README.txt), [README-runner](tools/gitLab/README-runner.txt),
  [README-projects](tools/gitLab/README-projects.txt)
- licenseFinder: [README](tools/licenseFinder/README.md)
- linuxMint: [README](tools/linuxMint/README.txt)
- markdownlint
- nomachine: [README](tools/nomachine/README.txt)
- pylint
- realVNC: [README](tools/realVNC/README.md)
- screen: [RC File](tools/screen/screenrc)
- sublime: [README](tools/sublime/README.md)
- swagger: [README](tools/swagger/README.txt)
- thunderbird: [README](tools/thunderbird/README.txt)
- ubuntuDIND
- visual studio: [README](tools/visualStudio/README.txt)
- xpath: [README](tools/xpath/README.txt)

## Base

The `base` directory hosts code that is shared accross the repository.

The base subdirectories have the following naming convention:

- Library files: `base/<3rd party name>/<language>/<library code>` Ex. `base/facebook/python/*`
- Demos: `base/<3rd party name>/<name>-demo[s]/<demo files>` Ex. `base/facebook/js-login-demo/*`

Since the base is shared by different clients from the entire repository, the following rules
must be adhered to when modifying code:

- Modularity: Where possible keep 1 object/file
- Backwards compatible changes: Edit the object/file inline
- Backwards incompatible changes: Create a new `file<V2>` or `folder<V2>/object<V2>`. This allows new
  code to be added without breaking existing clients.
- Deleting code: Before deleting code, ensure that no other clients are using that code

Base Integrations:

- aws: [README](base/aws/README.txt)
- bebl: [README](base/bebl/README.md)
- facebook: [README](base/facebook/README.txt), [README-projects](base/facebook/README-projects.txt)
- googleMarketingPlatform: [README](base/googleMarketingPlatform/README.md)
- googleMaps: [README](base/googleMaps/README.txt), [README-projects](base/googleMaps/README-projects.txt)
- instagram: [README](base/instagram/README.md)
- lastpass: [README](base/lastpass/README.md)
- mysql: [README](base/mysql/README.md), [README-projects](base/mysql/README-projects.md)
- recaptcha: [README](base/recaptcha/README.txt), [README-projects](base/recaptcha/README-projects.txt)
- sentry: [README](base/sentry/README.txt), [README-projects](base/sentry/README-projects.txt)
- stripe: [README](base/stripe/README.txt), [README-projects](base/stripe/README-projects.txt)
- twitter: [README](base/twitter/README.md)
- vagrant: [README](base/vagrant/README.md), [README-plugins](playground/vagrant/README-plugins.md),
  [README-projects](playground/vagrant/README-projects.md)

## Research

The `research` directory documents different software, business, or random ideas.

- 12 Factor App: [README](research/12FactorApp/README.txt)
- Dev Ops: [README](research/devOps/README.txt)
- Semantic Versioning: [README](research/semver/README.txt)
- Trunk Based Development: [README](research/trunkBasedDevelopment/README.txt)
- Legal: [GDPR](./research/legal/gdpr.md), [websites](./research/legal/websites.md)
