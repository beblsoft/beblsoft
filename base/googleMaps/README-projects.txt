OVERVIEW
===============================================================================
Google Map Samples Documentation


PROJECT: JS DEMO
===============================================================================
- Description:
  * Display a google map
- Directory                       : ./js-demo
- Tutorial Documentation          : https://developers.google.com/maps/documentation/javascript/tutorial
- API Key Documentation           : https://developers.google.com/maps/documentation/javascript/get-api-key
- To Run
  * Ensure API index.html API
    key is valid
  * View file in chrome           : index.html

