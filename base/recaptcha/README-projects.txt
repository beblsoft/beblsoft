OVERVIEW
===============================================================================
ReCAPTCHA Samples Documentation


PROJECT: JS STANDALONE DEMO
===============================================================================
- Description:
  * Javascript Usage of Google Recatcha API
  * Keys Registered under label "Beblsoft Base" here: https://www.google.com/recaptcha/admin#list
- Directory                       : ./js-standalone-demo
- To Run
  * Setup python environment      : virtualenv -p /usr/bin/python3.6 venv
                                    source venv/bin/activate
                                    pip3 install -r requirements.txt
  * Start python server           : ./app.py
  * View in chrome                : localhost:5000/index
                                    Click submit button


PROJECT: VUE STANDALONE DEMO
===============================================================================
- Description:
  * Vue usage of Google Recatcha Invisible API without any 3rd party library assistance
  * Keys Registered under label "Beblsoft Base" here: https://www.google.com/recaptcha/admin#list
- Directory                       : ./vue-standalone-demo
- To Run
  * Setup python environment      : virtualenv -p /usr/bin/python3.6 venv
                                    source venv/bin/activate
                                    pip3 install -r requirements.txt
  * Start python server           : ./app.py
  * View in chrome                : localhost:5000/index
                                    Open Chrome Console
                                    Click submit button

PROJECT: VUE RECAPTCHA DEMO
===============================================================================
- Description:
  * Vue usage of Google Recatcha Invisible API
  * Leverages npm package vue-recpatcha to do the heavy lifting
  * Keys Registered under label "Beblsoft Base" here: https://www.google.com/recaptcha/admin#list
- vue-recaptcha
  * Home Page                     : https://www.npmjs.com/package/vue-recaptcha
  * Example Usage                 : https://github.com/DanSnow/vue-recaptcha/tree/b117fdf6466e4a492de8a902e00eb168181ebd62/example
  * Install (CDN)                 : <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.min.js"></script>
  * Install (NPM)                 : npm install --save vue-recaptcha
- Directory                       : ./vue-recaptcha-demo
- To Run
  * Setup python environment      : virtualenv -p /usr/bin/python3.6 venv
                                    source venv/bin/activate
                                    pip3 install -r requirements.txt
  * Start python server           : ./app.py
  * View in chrome                : localhost:5000/index
                                    Open Chrome Console
                                    Click submit button