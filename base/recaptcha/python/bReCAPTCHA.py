#!/usr/bin/env python3.6
"""
 NAME
  bReCAPTCHA.py

 DESCRIPTION
   Beblsoft ReCAPTCHA Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
import json
import requests
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- BEBLSOFT RECAPTCHA -------------------------- #
class BeblsoftReCAPTCHA():
    """
    Beblsoft ReCAPTCHA Class

    Server Side ReCAPTCHA object that validates client side token

    From https://developers.google.com/recaptcha/docs/faq
      With the following test keys, you will always get No CAPTCHA and all verification requests will pass.
        Site key:   6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI
        Secret key: 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe
    """
    SITEKEY_ALWAYS_PASS   = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
    SECRETKEY_ALWAYS_PASS = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"

    def __init__(self, serverSecret):
        """
        Initialize object
        Args
          serverSecret:
            Server ReCAPTCHA secret
        """
        self.serverSecret = serverSecret
        self.verifyURL    = "https://www.google.com/recaptcha/api/siteverify"

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    @logFunc()
    def validate(self, token, remoteIP=None, nRetries=5, timeoutS=.5, passThroughToken=None):
        """
        Validate client token
        Args
          token:
            The value of g-recaptcha-response on client side
          remoteIP:
            The end user's IP address
          nRetries:
            Number of times to try request
          timeoutS:
            Time in seconds to timeout hanging requests
          passThroughToken:
            Token value to let pass through
            Use case: Production testing, admin test client program can specify token to bypass validation
        Raises
          BeblsoftError if client token is NOT valud
        """
        keepTrying = True
        valid      = False
        data       = {
            "secret": self.serverSecret,
            "response": token,
        }
        if remoteIP:
            data["remoteip"] = remoteIP

        if token == passThroughToken:
            pass
        else:
            # Loop issuing request
            # Loop is present because sometimes requests to verifyURL were hanging and failing
            while keepTrying:
                try:
                    resp  = requests.post(self.verifyURL, data=data, timeout=timeoutS)
                    valid = json.loads(resp.content).get("success", None)
                except Exception as _:  # pylint: disable=W0703
                    nRetries -= 1
                    keepTrying = (nRetries > 0)
                else:
                    keepTrying = False
            if not valid:
                raise BeblsoftError(code=BeblsoftErrorCode.RECAPTCHA_TOKEN_INVALID)
