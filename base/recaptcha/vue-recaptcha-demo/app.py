#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Flask-RestPlus ReCaptcha Server
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from flask import Flask, request, Response
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401
from base.recaptcha.python.bReCAPTCHA import BeblsoftReCAPTCHA
from base.bebl.python.error.bError import BeblsoftError


# ------------------------ GLOBALS ------------------------------------------ #
app                = Flask(__name__)
api                = Api(app)
logger             = logging.getLogger()
logger.setLevel(
    level              = logging.NOTSET)
bReCPATCHA         = BeblsoftReCAPTCHA(
    serverSecret       = "6Lc3f3IUAAAAADEtOdw8FRi2IrhThIBI50wzoMAD")


# ------------------------ INDEX ROUTE -------------------------------------- #
@api.route("/index")
class Index(Resource):
    """
    Index Routes
    """

    @api.representation("text/html")
    def get(self):  # pylint: disable=R0201
        """
        Handle get
        """
        directory = os.path.dirname(os.path.realpath(__file__))
        indexFile = "{}/{}".format(directory, "index.html")
        data      = ""
        with open(indexFile, "r") as f:
            data = f.read()
        resp = Response(data, mimetype="text/html", headers=None)
        return resp


# ------------------------ VALIDATION ROUTE --------------------------------- #
validateInModel = api.model("ValidateInModel", {
    "recaptchaToken": fields.String(
        title       = "Recaptcha Response Token",
        description = "Recaptcha Response Token",
        required    = True,
        readonly    = True,
        example     = "03ACgFB9uDQpkQnMORhKLCgzRDRPlpoPmuFzQwXQJbnJ4FiDo_47Ofx3dj2pQzkqB0xrrYDzh_ZAr8Yon3VFpCCZWBgXT8Gzp4OAtueIK-Do6ZK2KIcmA-qlYJN4Q3STiIkkZnT0gs_5CjZlQfx76cN888QG6_UWgQBIp9HMw7Eqkjct3dGBjEzy0H42Q99_bmu62xAEEQC5M-dMr_aHPItOA8jHaq3EEss-Y51-5ok3njsDDUYKXKepqccrzngxFlegyv-0pTVbv9K4jlpfjvLTZw4PGh2Y7JaZibPA1YjxsEkwl6j559PfexNrJfktaVGvbh6ypbt_rKcUkOwnTJ4-PtRXQYu5eBjbSc9Sf7THfCUk_sZkpF4b_7P8PuicE_f7bJwOLNPX64K5ijqT_XRNFlGmmW4wHiXmHLNlmVKsXnN3xS0I3ZCM4iJkPmMiu0B0n6QzrwiRPznXaQ_D0bqZCJCjCThm2BLjJi-0PfRWoVW1-9V5BU66rdxXu83DeOKx0IL_Sp-TfUHTvPS6fn4i1Mb5QTJHzh7yGTjNg_h1vGPj-9U0z_XAj5nKUQGBhs7TI1IrtjKgFUkiVHsX3Akjdi8nx_VZBWdcJb3O_fPsqmHMYDap1WpkX0v4HgL0FwuU53VQ4OagEyjzkwQVgVSCUtENDnIGcNAlFu9Io6RR57rDZnpMcYNzKCV0TjY5zky1Vcw2D6fA5i0iOGp1JDOezcyOHkIsEEeUSPczx7xJF5qrA728_QkRyoG2jqtqKKLswCyCtDrzTjIMR-pDpZb-zUfU8jUYNwESuXQCozmwHtukZX5b-SVJVZpRAPocUVTEcIHfhLmfYfkRnqlo--jEugxq0LemD-6yFWwtkWDOWB0DcHsItY3EedlEG73o7RGNrl1uYGNjjlN-ENDRshpYmVb4pjd79NsKVct8b59pAudS_o7sJDPy5U-nk2ztC2duVnL8GWh8VUpTfIpk7PeqGlkRzFrrGlVQ"),
    "email": fields.String(
        default     = "john.smith@gmail.com",
        title       = "Account Email",
        description = "Account Email",
        required    = True,
        readonly    = True,
        example     = "john.smith@gmail.com")
})

validateOutModel = api.model("ValidateOutModel", {
    "valid": fields.Boolean(
        default     = False,
        attribute   = "valid",
        title       = "Valid Recaptcha",
        description = "True if recaptcha valid, false otherwise",
        required    = True,
        readonly    = True,
        example     = True),
})


@api.route("/validate")
class Validate(Resource):
    """
    Validate Routes
    """

    @api.expect(validateInModel)
    @api.marshal_with(validateOutModel)
    def post(self):  # pylint: disable=R0201
        """
        Handle post
        """
        inData = request.json
        bReCPATCHA.validate(token=inData.get("recaptchaToken"))
        return {"valid": True}


# ------------------------ EXCEPTION HANDLERS ------------------------------- #
errorModel = api.model("Error", {
    "code": fields.Integer(
        attribute   = "extData.code",
        title       = "Server Error Code",
        example     = 1140),
    "message": fields.String(
        attribute   = "extData.extMsg",
        title       = "Server Error Message",
        example     = "No token attached to request object"),
})


@api.errorhandler(BeblsoftError)
@api.marshal_with(errorModel)
def handleBeblsoftError(error):
    """
    Beblsoft Error Handler
    """
    return error, error.extData.httpStatus


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(debug=True)
