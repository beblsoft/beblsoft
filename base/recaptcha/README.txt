OVERVIEW
===============================================================================
reCAPTCHA Documentation
- Description
  * reCAPTCHA is a free service that protects your site from spam and abuse.
    It uses advanced risk analysis techniques to tell humans and bots apart. With
    the new API, a significant number of your valid human users will pass the
    reCAPTCHA challenge without having to solve a CAPTCHA.
  * reCAPTCHA comes in the form of a widget that you can easily add to your
    blog, forum, registration form, etc.
- Relevant URLs
  * Home                            : https://developers.google.com/recaptcha/
  * Codelab                         : https://codelabs.developers.google.com/codelabs/reCAPTCHA/index.html#0
  * API Registration                : https://www.google.com/recaptcha/admin#list
  * Invisible Recaptcha             : https://developers.google.com/recaptcha/docs/invisible


CODE LAB
===============================================================================
- Clone sources                     : git clone https://github.com/googlecodelabs/recaptcha-codelab.git
- Go to directory                   : cd recaptcha-codelab
- For python                        : cd final-python
                                      python server.py
                                      Open Chrome: localhost:8080


INVISIBLE
===============================================================================
- To invoke the invisible reCAPTCHA, you can either:
  * Automatically bind the challenge
    to a button                     : <button class="g-recaptcha" data-sitekey="your_site_key"
                                       data-callback='onSubmit'>Submit</button>
  * Programmatically bind the
    challenge to a button           : <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
                                      <script type="text/javascript">
                                         var onSubmit = function(token) {
                                           console.log('success!');
                                         };

                                         var onloadCallback = function() {
                                           grecaptcha.render('submit', {
                                             'sitekey' : 'your_site_key',
                                             'callback' : onSubmit
                                           });
                                         };
                                      </script>
  * Programmatically invoke the
    challenge                       : <div class="g-recaptcha"
                                        data-sitekey="your_site_key"
                                        data-callback="onSubmit"
                                        data-size="invisible">
                                      </div>
                                      grecaptcha.execute();