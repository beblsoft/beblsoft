#!/usr/bin/env python3
"""
NAME:
 bAWSExample.py

DESCRIPTION
 Beblsoft AWS Example Object Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT AWS EXAMPLE OBJECT ----------------------- #
class BeblsoftAWSExampleObject(BeblsoftAWSObject):
    """
    Beblsoft AWS Example Object
    """

    def __init__(self, name):
        """
        Initialize object
        """
        super().__init__()
        self.name = name

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)


    # -------------------------- VERBS -------------------------------------- #
    def _create(self, **kwargs):
        """
        Create object
        """
        pass

    def _delete(self, **kwargs):
        """
        Delete object
        """
        pass

    # -------------------------- METADATA ----------------------------------- #
    def _getMetadata(self):
        """
        Get object metadata
        """
        return None
