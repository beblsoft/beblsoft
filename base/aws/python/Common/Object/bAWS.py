#!/usr/bin/env python3
"""
NAME:
 bAWS.py

DESCRIPTION
 Beblsoft AWS Object Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import time
from datetime import datetime, timedelta
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT AWS OBJECT ------------------------------- #
class BeblsoftAWSObject(): #pylint: disable=R0904
    """
    Beblsoft AWS Object
    Basic CRUD methods for AWS objects to subclass
    """

    # ----------------------- PYTHON METHODS -------------------------------- #
    def __init__(self):
        """
        Initialize object
        """
        self._metadata   = None
        self._attributes = None

    def __str__(self):
        """
        Return string representation
        """
        return "[{}]".format(self.__class__.__name__)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def exists(self):
        """
        Return True if object exists
        """
        metadata = self.getMetadata()
        return (metadata != None)

    @property
    def online(self):
        """
        Return True is object is online and running.
        Most objects are online when they exist. Others, may override this functionality
        """
        return self.exists

    # ----------------------- METADATA PROPERTIES --------------------------- #
    def getValueFromMetadata(self, key, required=False, default=None):
        """
        Return value from metadata dictionary
        Nested values are allowed
        Args
          key:
            Key into object
            Ex1. 56
            Ex2. "Foo"
            Ex3. "Foo.bar.baz"
          required:
            If True, raise exception if not found
          default:
            Default value
        """
        value = None
        self.getMetadata()
        if isinstance(key, int):
            value = self.__getValueForKeyList(key, self._metadata, required, default)
        else:
            value = self.__getValueForKeyList(
                key.split('.'), self._metadata, required, default)
        return value

    def __getValueForKeyList(self, keyList, d, required, default):
        """
        Return value given a list of keys
        """
        value = None
        if len(keyList) == 1:
            value = self.__getValueForKey(keyList[0], d, required, default)
        else:
            value = self.__getValueForKeyList(keyList[1:],
                                              self.__getValueForKey(
                                                  keyList[0], d, required, default),
                                              required,
                                              default)
        return value

    def __getValueForKey(self, key, d, required, default):  # pylint: disable=R0201
        """
        Return object value for key
        """
        value = None
        try:
            value = d[key]
        except Exception as e:  # pylint: disable=W0703
            if required:
                raise BeblsoftError(
                    code=BeblsoftErrorCode.AWS_NO_ATTRIBUTE, originalError=e)
            else:
                value = default
        return value

    # ----------------------- METADATA -------------------------------------- #
    def getMetadata(self):
        """
        Get all AWS object metadata
        Returns:
          Valid Metadata dictionary
          None if object doesn't exist
        """
        self._metadata = self._getMetadata()  # pylint: disable=E1111
        return self._metadata

    def _getMetadata(self):
        """
        Internal getMetadata, to be overridden by subclass
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- WAIT CHANGE ----------------------------------- #
    def waitChange(self, changeFunc, waitIntervalS=15, waitTotalS=300):  # pylint: disable=R0201
        """
        Wait on a change to occur

        Args
          changeFunc:
            Function to be periodically evaluate
            If function returns True, change has occurred
            If function returns False, change has not occurred
            Ex. lambda : self.state == "Available"
          waitIntervalS:
            Seconds to wait before reexecuting changeFunc
            Ex. 15
          waitTotalS:
            Total time to wait before throwing exception
        """
        startTime = datetime.now()
        curTime   = None
        totalTime = timedelta(seconds=waitTotalS)
        idx       = 0

        while not changeFunc():
            time.sleep(waitIntervalS)
            curTime = datetime.now()
            if curTime - startTime > totalTime:
                raise BeblsoftError(code=BeblsoftErrorCode.AWS_WAIT_TIMED_OUT,
                                    msg="waitIntervalS={} waitTotalS={} idx={}".format(
                                        waitIntervalS, waitTotalS, idx))
            idx += 1

    # ----------------------- CREATE ---------------------------------------- #
    def create(self, **kwargs):
        """
        Create object
        """
        if self.exists:
            logger.info("{} Already exists".format(self))
        else:
            self._create(**kwargs)
            logger.info("{} Created".format(self))

    def _create(self, **kwargs):  # pylint: disable=W0613
        """
        Internal create, to be overridden by subclass
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- DELETE ---------------------------------------- #
    def delete(self, **kwargs):
        """
        Delete object
        """
        if self.exists:
            self._delete(**kwargs)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    def _delete(self, **kwargs):  # pylint: disable=W0613
        """
        Internal delete, to be overridden by subclass
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- START ----------------------------------------- #
    def start(self, **kwargs):
        """
        Start object
        """
        if self.exists:
            self._start(**kwargs)
            logger.info("{} Started".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _start(self, **kwargs):  # pylint: disable=W0613
        """
        Internal start, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- STOP ------------------------------------------ #
    def stop(self, **kwargs):
        """
        Stop object
        """
        if self.exists:
            self._stop(**kwargs)
            logger.info("{} Stopped".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _stop(self, **kwargs):  # pylint: disable=W0613
        """
        Internal stop, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- REBOOT ---------------------------------------- #
    def reboot(self, **kwargs):
        """
        Reboot object
        """
        if self.exists:
            self._start(**kwargs)
            logger.info("{} Rebooted".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _reboot(self, **kwargs):  # pylint: disable=W0613
        """
        Internal reboot, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- ENABLE ---------------------------------------- #
    def enable(self, **kwargs):
        """
        Enable object
        """
        if self.exists:
            self._enable(**kwargs)
            logger.info("{} Enabled".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _enable(self, **kwargs):  # pylint: disable=W0613
        """
        Internal enable, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- DISABLE --------------------------------------- #
    def disable(self, **kwargs):
        """
        Disable object
        """
        if self.exists:
            self._disable(**kwargs)
            logger.info("{} Enabled".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _disable(self, **kwargs):  # pylint: disable=W0613
        """
        Internal disable, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- UPDATE ---------------------------------------- #
    def update(self, **kwargs):
        """
        Update object
        """
        if self.exists:
            self._update(**kwargs)
            logger.info("{} Updated".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _update(self, **kwargs):  # pylint: disable=W0613
        """
        Internal update, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- MODIFY ---------------------------------------- #
    def modify(self, **kwargs):
        """
        Modify object
        """
        if self.exists:
            self._update(**kwargs)
            logger.info("{} Modified".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _modify(self, **kwargs):  # pylint: disable=W0613
        """
        Internal modify, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- RESTORE --------------------------------------- #
    def restore(self, **kwargs):
        """
        Restore object
        """
        if self.exists:
            self._update(**kwargs)
            logger.info("{} Restored".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _restore(self, **kwargs):  # pylint: disable=W0613
        """
        Internal restore, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- TAIL ------------------------------------------ #
    def tail(self, **kwargs):
        """
        Tail object
        """
        if self.exists:
            logger.info("Tailing {}".format(self))
            self._tail(**kwargs)
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _tail(self, **kwargs):  # pylint: disable=W0613
        """
        Internal tail, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- INVOKE ---------------------------------------- #
    def invoke(self, **kwargs):
        """
        Invoke object
        """
        if not self.exists:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))
        logger.debug("Invoking {}".format(self))
        return self._invoke(**kwargs)

    def _invoke(self, **kwargs):  # pylint: disable=W0613
        """
        Internal invoke, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- CONFIRM --------------------------------------- #
    def confirm(self, **kwargs):
        """
        Confirm object
        """
        if self.exists:
            self._confirm(**kwargs)
            logger.info("{} Confirmed".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _confirm(self, **kwargs):  # pylint: disable=W0613
        """
        Internal confirm, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- COPY ------------------------------------------ #
    def copy(self, **kwargs):
        """
        Copy object
        """
        if self.exists:
            self._copy(**kwargs)
            logger.info("{} Copied".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _copy(self, **kwargs):  # pylint: disable=W0613
        """
        Internal copy, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- PURGE ----------------------------------------- #
    def purge(self, **kwargs):
        """
        Purge object
        """
        if self.exists:
            self._purge(**kwargs)
            logger.info("{} Purged".format(self))
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST,
                                msg="{}".format(self))

    def _purge(self, **kwargs):  # pylint: disable=W0613
        """
        Internal purge, to be overridden by subclass if desired
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # ----------------------- DESCRIBE -------------------------------------- #
    def describe(self):
        """
        Describe object
        """
        meta = self.getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))
