#!/usr/bin/env python3
"""
NAME:
 bIdentity.py

DESCRIPTION
 Beblsoft Identity
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT IDENTITY --------------------------------- #
class BeblsoftIdentity():
    """
    Beblsoft Identity
    """

    def __init__(self, userID, accountID, arn):
        """
        Initialize Object
        Args
          userID:
            Unique ID of calling entity
          accountID:
            AWS account ID of account that owns the calling entity
          arn:
            AWS arn associated with calling entity
        """
        self.userID    = userID
        self.accountID = accountID
        self.arn       = arn
        self.stsClient = boto3.client("sts")

    def __str__(self):
        return "[{} userID={} accountID={} arn={}]".format(
            self.__class__.__name__, self.userID, self.accountID, self.arn)

    @staticmethod
    @logFunc()
    def getCaller():
        """
        Return default region
        """
        stsClient = boto3.client("sts")
        resp = stsClient.get_caller_identity()

        return BeblsoftIdentity(
            userID    = resp.get("UserId"),
            accountID = resp.get("Account"),
            arn       = resp.get("Arn"))
