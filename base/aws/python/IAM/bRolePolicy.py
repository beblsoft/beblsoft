#!/usr/bin/env python3
"""
NAME:
 bRolePolicy.py

DESCRIPTION
 Beblsoft Identity and Access Management Role Policy Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT IAM ROLE POLICY ------------------------- #
class BeblsoftIAMRolePolicy():
    """
    Beblsoft IAM Role Policy
    """

    def __init__(self, name, bIAMRole, permissionPolicyPath, description=""):
        """
        Initialize Object
        Args
          name:
            Role policy name
          bIAMRole:
            Beblsoft IAM Role object to attach policy
          permissionPolicyPath:
            Absolute path to JSON policy document
            Create policy here: https://console.aws.amazon.com/iam/home?region=us-east-1#/policies$new?step=edit
          description:
            Description of role policy
        Documentation
          http://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_terms-and-concepts.html
        """
        self.name                     = name
        self.bIAMRole                 = bIAMRole
        self.description              = description
        self.permissionPolicyData     = None
        with open(permissionPolicyPath, "r") as f:
            self.permissionPolicyData = f.read()
        self.iamClient                = boto3.client("iam")
        self.iamRes                   = boto3.resource("iam")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create role policy
        """
        self.iamClient.put_role_policy(RoleName=self.bIAMRole.name, PolicyName=self.name,
                                       PolicyDocument=self.permissionPolicyData)
        logger.info("{} created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete role policy
        """
        try:
            self.iamClient.delete_role_policy(
                RoleName=self.bIAMRole.name, PolicyName=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code == "NoSuchEntity":
                logger.info("{} Never existed".format(self))
            else:
                logger.exception(pprint.pformat(e.__dict__))
        else:
            logger.info("{} Deleted".format(self))

    @logFunc()
    def describe(self):
        """
        Describe role policy
        """
        meta = self.__getMetadata()
        logger.info("{} info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        resp = None
        try:
            resp = self.iamClient.get_role_policy(
                RoleName=self.bIAMRole.name, PolicyName=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code == "NoSuchEntity":
                pass
            else:
                logger.exception(pprint.pformat(e.__dict__))
        return resp
