#!/usr/bin/env python3
"""
NAME:
 bRole.py

DESCRIPTION
 Beblsoft Identity and Access Management Role Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT IAM ROLE -------------------------------- #
class BeblsoftIAMRole():
    """
    Beblsoft IAM Role
    """

    def __init__(self, name, trustPolicyPath, description=""):
        """
        Initialize Object
        Args
          name:
            Role name
          trustPolicyPath:
            Absolute path to trust relationship policy document that grants
            an entity permission to assume the role
           description:
            Description of role
        Documentation
          http://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_terms-and-concepts.html
        """
        self.name        = name
        self.description = description
        self.iamClient   = boto3.client("iam")
        with open(trustPolicyPath, "r") as f:
            self.trustPolicyData = f.read()

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @property
    def exists(self):
        """
        Return True if role exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def arn(self):
        """
        Return Amazon Resource Name
        """
        arn  = None
        meta = self.__getMetadata()
        if meta:
            arn = meta.get("Arn", None)
        return arn

    @property
    def id(self):
        """
        Return id
        """
        roleId = None
        meta   = self.__getMetadata()
        if meta:
            roleId = meta.get("RoleId", None)
        return roleId

    @logFunc()
    def create(self):
        """
        Create role
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            self.iamClient.create_role(RoleName=self.name, AssumeRolePolicyDocument=self.trustPolicyData,
                                       Description=self.description)
            logger.info("{} created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete role
        """
        if self.exists:
            self.iamClient.delete_role(RoleName=self.name)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe role
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        meta = None
        try:
            resp = self.iamClient.get_role(RoleName=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code == "NoSuchEntity":
                pass
            else:
                logger.exception(pprint.pformat(e.__dict__))
        else:
            meta = resp.get("Role", None)
        return meta
