#!/usr/bin/env python3
"""
NAME:
 bInstanceProfile.py

DESCRIPTION
 Beblsoft Identity and Access Management Instance Profile Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
import botocore
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT IAM INSTANCE PROFILE -------------------- #
class BeblsoftIAMInstanceProfile():
    """
    Beblsoft IAM Instance Profile
    """

    def __init__(self, name, bIAMRole):
        """
        Initialize Object
        Args
          name:
            Role name
          bIAMRole:
            Beblsoft IAM Role
        Documentation
          http://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_terms-and-concepts.html
        """
        self.name      = name
        self.bIAMRole  = bIAMRole
        self.iamClient = boto3.client("iam")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create instance profile
        Note
          Add sleep in here since the waiter wasn't properly waiting,
          and subsequent resources dependent on IAM Instance Profile (namely,
          BeblsoftLaunchConfiguration) were failing.
        """
        self.iamClient.create_instance_profile(InstanceProfileName=self.name)
        self.iamClient.add_role_to_instance_profile(InstanceProfileName=self.name,
                                                    RoleName=self.bIAMRole.name)
        waiter = self.iamClient.get_waiter("instance_profile_exists")
        waiter.wait(InstanceProfileName=self.name)
        logger.info("{} created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete instance profile
        """
        self.iamClient.remove_role_from_instance_profile(InstanceProfileName=self.name,
                                                         RoleName=self.bIAMRole.name)
        self.iamClient.delete_instance_profile(InstanceProfileName=self.name)
        logger.info("{} deleted".format(self))

    @logFunc()
    def describe(self):
        """
        Describe instance profile
        """
        meta = self.__getMetadata()
        logger.info("{} info:\n{}".format(self, pprint.pformat(meta)))

    @staticmethod
    def describeAll():
        """
        Describe all IAM Instance Profiles
        """
        iamClient = boto3.client("iam")
        resp = iamClient.list_instance_profiles()
        return resp.get("InstanceProfiles", None)

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        meta = None
        try:
            resp = self.iamClient.get_instance_profile(InstanceProfileName=self.name)
        except botocore.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "NoSuchEntity":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        else:
            meta = resp.get("InstanceProfile", None)
        return meta
