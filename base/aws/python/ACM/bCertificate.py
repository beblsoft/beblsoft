#!/usr/bin/env python3
"""
NAME
 bCerticiate.py

DESCRIPTION
 Beblsoft AWS Certificate Manager (ACM) Certificate Functionality

CREATING A CERTIFICATE
  - Request a certificate
  - Type in domain. Ex. "*.quotablejoy.com"
    Add Additional Domains
    Choose next
  - Choose DNS Validation
  - Confirm and Request
  - For validation, create a CNAME record that allows AWS to verify domain
    Ex.
    Type:  CNAME
    Name:  _b08d1e8cdadec08d9b246135f671b3b7.quoteablejoy.com.
    Value: _0c1ce1d963478f38832141ad7379cb3b.acm-validations.aws.
  - Validation may take several hours to complete
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT ACM CERTIFICATE ------------------------- #
class BeblsoftACMCertificate():
    """
    Beblsoft ACM Certificate
    """

    def __init__(self, name, domainName, additionalDomains, validation="DNS",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            Name for certificate. This is needed as same domainName can be
            used in multiple certificates.
            Ex. "QuoteCert"
          domainName:
            Fully Qualified Domain Name
            Ex1. www.quotablejoy.com
            Ex2. *.quotablejoy.com
            Note: "*" only protects one domain sublevel. In above example
            foo.quotablejoy.com is covered but foo.bar.quotablejoy.com is NOT
          additionalDomains:
            List of additional domains to cover
            Ex. ["quotablejoy.com", "quotablejoy.net"]
            See: https://stackoverflow.com/questions/37520110/aws-acm-wildcard-ssl-certificate-not-working-on-domain
          validation:
            Medthod to validate domain
            One of: "EMAIL" or "DNS"
        """
        self.name              = name
        self.nameTag           = {"Key": "Name",
                                  "Value": self.name}
        self.domainName        = domainName
        self.additionalDomains = additionalDomains
        self.validation        = validation
        self.bRegion           = bRegion
        self.acmClient         = boto3.client("acm", region_name=bRegion.name)

    def __str__(self):
        return "[{} name={} domainName={}]".format(
            self.__class__.__name__, self.name, self.domainName)

    @property
    def exists(self):
        """
        Return True if Certificate exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def arn(self):
        """
        Return certificate ARN
        """
        arn      = None
        certList = BeblsoftACMCertificate.__getAllCertificates(bRegion=self.bRegion)
        for curCert in certList:
            curArn = curCert.get("CertificateArn", None)
            resp   = self.acmClient.list_tags_for_certificate(CertificateArn=curArn)
            tags   = resp.get("Tags")
            for tag in tags:
                nameKey  = tag.get("Key")   == "Name"
                sameName = tag.get("Value") == self.name
                if nameKey and sameName:
                    arn = curArn
        return arn

    @logFunc()
    def create(self):
        """
        Create Certificate
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            resp = self.acmClient.request_certificate(
                DomainName=self.domainName,
                SubjectAlternativeNames=self.additionalDomains,
                ValidationMethod=self.validation)
            arn  = resp.get("CertificateArn", None)
            resp = self.acmClient.add_tags_to_certificate(
                CertificateArn=arn,
                Tags=[self.nameTag])
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete Certificate
        """
        if self.exists:
            self.acmClient.delete_certificate(
                CertificateArn=self.arn)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe certificate
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @staticmethod
    def __getAllCertificates(bRegion=BeblsoftRegion.getDefault()):
        """
        Get list of all certificates
        """
        certList  = None
        acmClient = boto3.client("acm", region_name=bRegion.name)
        resp      = acmClient.list_certificates()
        if resp:
            certList = resp.get("CertificateSummaryList", None)
        return certList

    @logFunc()
    def __getMetadata(self):
        """
        Return certificate metadata
        """
        meta = None
        arn  = self.arn
        if arn:
            resp = self.acmClient.describe_certificate(CertificateArn=arn)
        if resp:
            meta = resp.get("Certificate")
        return meta
