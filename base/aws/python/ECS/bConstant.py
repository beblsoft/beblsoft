#!/usr/bin/env python3
"""
NAME:
 bConstant.py

DESCRIPTION
 Beblsoft ECS Constant Functionality
"""


# ------------------------ BEBLSOFT ECS CONSTANT ---------------------------- #
class BeblsoftECSConstant():
    """
    Beblsoft ECS Constant
    """
    DOCKER_BASE_URL  = "unix://var/run/docker.sock"
