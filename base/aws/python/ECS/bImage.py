#!/usr/bin/env python3
"""
NAME:
 bImage.py

DESCRIPTION
 Beblsoft Container Image Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
import docker
from base.aws.python.ECS.bConstant import BeblsoftECSConstant
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER REPOSITORY -------------------- #
class BeblsoftContainerImage():
    """
    Beblsoft Container Image
    """

    def __init__(self, path, bRepo, shortTag):
        """
        Initialize Object
        Args
          path: path to docker file
          bRepo: beblsoft container repository to push image
          shortTag: short tag applied after repo name. Ex "latest"

        Note:
          Long tags look like:
          818838633047.dkr.ecr.us-east-1.amazonaws.com/mojourney/flask:latest
          |<-----------registryURL------------------->|<-----repo---->|<shorttag>|
        """
        self.path      = path
        self.bRepo     = bRepo
        self.bRegistry = bRepo.bRegistry
        self.shortTag  = shortTag
        self.longTag   = "{}:{}".format(bRepo.url, shortTag)
        if any(c.isupper() for c in self.longTag):
            raise BeblsoftError(
                msg="Tags must be lowercase: {}".format(self.longTag))
        self.bRegion         = bRepo.bRegion
        self.ecrClient       = boto3.client("ecr", region_name=self.bRegion.name)
        self.dockerClient    = docker.DockerClient(
            base_url=BeblsoftECSConstant.DOCKER_BASE_URL)
        self.dockerApiClient = docker.APIClient(
            base_url=BeblsoftECSConstant.DOCKER_BASE_URL)

    def __str__(self):
        return "[{} Repo={} ShortTag={}]".format(self.__class__.__name__, self.bRepo.name, self.shortTag)

    @logFunc()
    def build(self):
        """
        Build local docker image
        """
        for line in self.dockerApiClient.build(path=self.path, tag=self.longTag):
            logger.info("{}".format(line.decode("utf8").strip()))
        logger.info("{} Built".format(self))

    @logFunc()
    def remove_local(self):
        """
        Remove local docker image
        """
        self.dockerClient.images.remove(image=self.longTag, force=True)
        logger.info("{} removed from local".format(self))

    @logFunc()
    def remove_remote(self):
        """
        Remove remote docker image
        """
        self.ecrClient.batch_delete_image(repositoryName=self.bRepo.name,
                                          imageIds=[{"imageTag": self.shortTag}])
        logger.info("{} removed from remote".format(self))

    @logFunc()
    def push(self):
        """
        Push local image to remote repository
        """
        authConfig = {
            "username": self.bRegistry.username,
            "password": self.bRegistry.authToken
        }
        for line in self.dockerApiClient.push(repository=self.longTag, auth_config=authConfig,
                                              stream=True):
            logger.info("{}".format(line.decode("utf8").strip()))
        logger.info("{} Pushed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe image
        """
        image = None
        try:
            image = self.dockerClient.images.get(self.longTag)
        except docker.errors.ImageNotFound as e:  # pylint: disable=W0612
            logger.info("{} doesn't exit".format(self))
        else:
            logger.info("{} Info:\n{}".format(self, pprint.pformat(image)))
