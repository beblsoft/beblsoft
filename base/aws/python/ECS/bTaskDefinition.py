#!/usr/bin/env python3
"""
NAME
 bTaskDefinition.py

DESCRIPTION
 Beblsoft Container Task Definition Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER TASK DEFINITION --------------- #
class BeblsoftContainerTaskDefinition():
    """
    Beblsoft Container Task Definition
    """

    def __init__(self, family, containerDefinitions, networkMode="bridge",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          family:
            task definition family
            ex. mjFlask:1
          containerDefinitions:
            List of container definitions in JSON format
            See: http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.register_task_definition
            name:
              container name
            bContainerImage:
              BeblsoftContainerImage used to start a container
            memory:
              Hard limit in MiB to present to container
            portMappings:
              List of port mappings for the container
              containerPort
                port number on the container
              hostPort
                port number on the host
            essential:
              If True, and the container fails or stops, all other containers
              that are part of the task are stopped
            entryPoint:
              Entry point that is passed to the container
            environment:
              List of environment variables passed to the container
              name
                name of environment variable
              value
                value of environment variable
            workingDirectory
              working directory in which to run commands inside the container
            logConfiguration
              Log configuration specified for container
              logDriver
                Log dirver to use for container
                Valid Values. 'json-file'|'syslog'|'journald'|'gelf'|'fluentd'|'awslogs'|'splunk'
              options
                Options for the log driver
          networkMode:
            Docker networking mode for containers
            Valid values: 'bridge'|'host'|'none'

        """
        self.family               = family
        self.containerDefinitions = containerDefinitions
        self.networkMode          = networkMode
        self.bRegion              = bRegion
        self.ecsClient            = boto3.client("ecs", region_name=bRegion.name)

    def __str__(self):
        return "[{} Family={}]".format(self.__class__.__name__, self.family)

    @logFunc()
    def register(self):
        """
        Register task definition
        """
        # Convert containerDefinitions: "bContainerImage" -> "image"
        for containerDef in self.containerDefinitions:
            bContainerImage = containerDef.pop("bContainerImage", None)
            containerDef["image"] = bContainerImage.longTag

        self.ecsClient.register_task_definition(family=self.family, networkMode=self.networkMode,
                                           containerDefinitions=self.containerDefinitions)
        logger.info("{} registered".format(self))

    @logFunc()
    def deregister(self):
        """
        Deregister all revisions of task definition
        """
        resp = self.ecsClient.list_task_definitions(familyPrefix=self.family)
        taskDefinitionArns = resp.get("taskDefinitionArns", None)
        for taskDefinitionArn in taskDefinitionArns:
            self.ecsClient.deregister_task_definition(taskDefinition=taskDefinitionArn)
        logger.info("{} deregistered".format(self))

    @logFunc()
    def describe(self):
        """
        Describe task definition
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        meta = None
        try:
            resp = self.ecsClient.describe_task_definition(taskDefinition=self.family)
            meta = resp.get("taskDefinition", None)
        except ClientError as e:
            if e.response["Error"]["Message"] != "Unable to describe task definition.":
                raise e
        return meta
