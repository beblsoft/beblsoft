#!/usr/bin/env python3
"""
NAME
 bTask.py

DESCRIPTION
 Beblsoft Container Task Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER TASK -------------------------- #
class BeblsoftContainerTask():
    """
    Beblsoft Container Task
    """

    def __init__(self, startedBy, bContainerCluster, bTaskDefinition, group=None, count=1):
        """
        Initialize object
        Args
          startedBy:
            Tag to be used later to search for container tasks
          bContainerCluster:
            BeblsoftContainerCluster object
          bTaskDefinition:
            BeblsoftTaskDefinition object
          group:
            Name of task group to associate with task
          count:
            Number of instantiations of specified task
        """
        self.startedBy         = startedBy
        self.bContainerCluster = bContainerCluster
        self.bTaskDefinition   = bTaskDefinition
        self.count             = count
        if group:
            self.group         = group
        else:
            self.group         = bTaskDefinition.family
        self.bRegion           = bContainerCluster.bRegion
        self.ecsClient         = boto3.client("ecs", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} StartedBy={}]".format(self.__class__.__name__, self.startedBy)

    @logFunc()
    def run(self):
        """
        Run task
        """
        resp = self.ecsClient.run_task(cluster=self.bContainerCluster.arn,
                                       taskDefinition="{}".format(
                                           self.bTaskDefinition.family),
                                       count=self.count,
                                       group=self.group,
                                       startedBy=self.startedBy)

        waiter = self.ecsClient.get_waiter("tasks_running")
        waiter.wait(cluster=self.bContainerCluster.arn,
                    tasks=[task["taskArn"] for task in resp.get("tasks", None)])
        logger.info("{} running".format(self))

    @logFunc()
    def stop(self):
        """
        Stop task
        """
        clusterArn = self.bContainerCluster.arn
        tasks      = self.__getMetadata()
        if tasks:
            for task in tasks:
                taskArn = task.get("taskArn")
                self.ecsClient.stop_task(cluster=clusterArn, task=taskArn)
            waiter = self.ecsClient.get_waiter("tasks_stopped")
            waiter.wait(cluster=clusterArn,
                        tasks=[task.get("taskArn") for task in tasks])
            logger.info("{} stopped".format(self))
        else:
            logger.info("{} wasn't running".format(self))

    @logFunc()
    def describe(self):
        """
        Describe task
        """
        tasks = self.__getMetadata()
        logger.info("{} info:\n{}".format(self, pprint.pformat(tasks)))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        tasks      = None
        clusterArn = self.bContainerCluster.arn
        resp       = self.ecsClient.list_tasks(cluster=clusterArn,
                                               startedBy=self.startedBy)
        taskArns   = resp.get("taskArns", None)
        if taskArns:
            resp   = self.ecsClient.describe_tasks(
                cluster=clusterArn, tasks=taskArns)
            tasks  = resp.get("tasks", None)
        return tasks
