#!/usr/bin/env python3
"""
NAME:
 bRepository.py

DESCRIPTION
 Beblsoft Container Repository Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import re
import pprint
import logging
import boto3
import docker
from botocore.exceptions import ClientError
from base.aws.python.ECS.bConstant import BeblsoftECSConstant
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER REPOSITORY -------------------- #
class BeblsoftContainerRepository():
    """
    Beblsoft Container Repository

    Description
      Wrapper over AWS Elastic Container Registry (ECR) Repository
      ECR Repository is a storage for Docker images
    """

    def __init__(self, bRegistry, name):
        """
        Initialize Object
        Args
          name:
            repository name
            ex. "MojourneyRepository"
        """
        self.bRegistry       = bRegistry
        self.name            = name
        self.bRegion         = bRegistry.bRegion
        self.ecrClient       = boto3.client("ecr", region_name=self.bRegion.name)
        self.dockerApiClient = docker.APIClient(
            base_url           = BeblsoftECSConstant.DOCKER_BASE_URL)


    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @property
    def url(self):
        """
        Return <registry url>/<repository name> to specify repository URL
        """
        proxy = re.sub("https://", "", self.bRegistry.url)
        url = "{}/{}".format(proxy, self.name)
        return url

    @logFunc()
    def create(self):
        """
        Create Repository
        """
        self.ecrClient.create_repository(repositoryName=self.name)
        logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete Repository
        """
        self.ecrClient.delete_repository(repositoryName=self.name, force=False)
        logger.info("{} Deleted".format(self))

    @logFunc()
    def describe(self):
        """
        Describe repository
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def describe_images(self):
        """
        Describe repository images
        """
        images = self.__getImages()
        logger.info("{} Images:\n{}".format(self, pprint.pformat(images)))

    @staticmethod
    def descibeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all repositories in registry
        """
        repositories = None
        ecrClient    = boto3.client("ecr", region_name=bRegion.name)
        resp         = ecrClient.describe_repositories()
        if resp:
            repositories = resp.get("repositories", None)
        logger.info("Repositories:\n{}".format(pprint.pformat(repositories)))

    @logFunc()
    def __getImages(self):
        """
        Return repository images
        """
        images = None
        try:
            resp = self.ecrClient.describe_images(repositoryName=self.name)
            images = resp.get("imageDetails", None)
        except ClientError as e:
            if e.response["Error"]["Code"] != "RepositoryNotFoundException":
                raise e
        return images

    @logFunc()
    def __getMetadata(self):
        """
        Return repository metadata
        """
        meta         = None
        repositories = None
        try:
            resp = self.ecrClient.describe_repositories(repositoryNames=[self.name])
            repositories = resp.get("repositories", None)
        except ClientError as e:
            if e.response["Error"]["Code"] != "RepositoryNotFoundException":
                raise e
        if repositories:
            meta = repositories[0]
        return meta
