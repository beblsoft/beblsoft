#!/usr/bin/env python3
"""
NAME
 bRegistry.py

DESCRIPTION
 Beblsoft Container Registry Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import base64
import boto3
import docker
from base.aws.python.ECS.bConstant import BeblsoftECSConstant
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER REGISTRY ---------------------- #
class BeblsoftContainerRegistry():
    """
    Beblsoft Container Registry
    """

    def __init__(self, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        """
        self.bRegion         = bRegion
        self.ecrClient       = boto3.client("ecr", region_name=bRegion.name)
        self.dockerApiClient = docker.APIClient(
            base_url           = BeblsoftECSConstant.DOCKER_BASE_URL)
        self.username        = "AWS"

    def __str__(self):
        return "[{}:{}]".format(self.__class__.__name__, self.url)

    @property
    def url(self):
        """
        Return registry url
        """
        auth  = self.__getAuthToken()
        proxy = auth.get("proxyEndpoint", None)
        return proxy

    @property
    def authToken(self):
        """
        Return authentication token
        Note
          Token is base64 encoded
        """
        auth  = self.__getAuthToken()
        token = base64.b64decode(
            auth.get("authorizationToken", None)).decode("utf-8")
        token = token.replace("AWS:", "", 1)
        return token

    @logFunc()
    def login(self):
        """
        Login to AWS registry
        """
        resp = self.dockerApiClient.login(username=self.username, password=self.authToken,
                                          email=None, registry=self.url, reauth=True)
        status = resp["Status"]
        if status != "Login Succeeded":
            raise BeblsoftError(cmd="login", status=status)
        logger.info("{} logged in".format(self))

    @logFunc()
    def describe(self):
        """
        Describe registry
        """
        logger.info("{}:\nURL: {}\nAuthToken: {}".format(
            self, self.url, self.authToken))

    @logFunc()
    def __getAuthToken(self):  # pylint: disable=R0201
        """
        Return Docker authorization token

        This allows a local docker client to connect to the AWS repository via:
        docker login -u AWS -p <authorizationToken> -e none <proxyEndpoint>

        Example
        docker login -u AWS -p eyJwYXlsb2FkIjoiRTB4eVF2bH...5cGUiOiJEQVRBX0tFSJ9
                     -e none https://818838633047.dkr.ecr.us-east-1.amazonaws.com


        AWS CLI equivalent: ecr get-login
        """
        token    = None
        resp     = self.ecrClient.get_authorization_token()
        authData = resp.get("authorizationData", None)
        if authData:
            token = authData[0]
        return token
