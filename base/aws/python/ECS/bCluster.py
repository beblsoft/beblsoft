#!/usr/bin/env python3
"""
NAME:
 bCluster.py

DESCRIPTION
 Beblsoft Container Cluster Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CONTAINER CLUSTER ----------------------- #
class BeblsoftContainerCluster():
    """
    Beblsoft Container Cluster
    """

    def __init__(self, name, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            cluster name
            ex. "MojourneyCluster"
        """
        self.name      = name
        self.bRegion   = bRegion
        self.ecsClient = boto3.client("ecs", region_name=bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create cluster
        """
        self.ecsClient.create_cluster(clusterName=self.name)
        logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete cluster
        """
        self.ecsClient.delete_cluster(cluster=self.name)
        logger.info("{} Deleted".format(self))

    @property
    def arn(self):
        """
        Return instance arn
        """
        arn  = None
        meta = self.__getMetadata()
        if meta:
            arn = meta.get("clusterArn", None)
        return arn

    @logFunc()
    def describe(self):
        """
        Describe cluster
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all clusters
        """
        ecsClient = boto3.client("ecs", region_name=bRegion.name)
        resp      = ecsClient.describe_clusters()
        clusters  = resp.get("clusters", None)
        logger.info("Clusters:\n{}".format(pprint.pformat(clusters)))

    @logFunc()
    def __getMetadata(self):
        """
        Return cluster metadata
        """
        meta     = None
        resp     = self.ecsClient.describe_clusters(clusters=[self.name])
        clusters = resp.get("clusters", None)
        if clusters:
            meta = clusters[0]
        return meta
