OVERVIEW
===============================================================================
Amazon ElastiCache Technology
- Description
  * Amazon ElastiCache offers fully managed Redis and Memcached
  * Seamlessly deploy, operate, and scale popular open source compatible in-memory
    data stores
- Features
  * Extreme Performance             : In Memory Data Store and Cache Support
                                      Sub-millisecond response times
  * Fully Managed                   : No more management tasks, for example:
                                      Hardware provisioning, software patching,
                                      setup, configuration, monitoring, failure
                                      recovery, and backups
  * Scalable                        : Scale out, scale in, and scale up
                                      Write and memory scaling supported with sharding
                                      Replicas provide read scaling


RELEVANT URLS
===============================================================================
- Home                              : https://aws.amazon.com/elasticache/
- Documentation                     : https://aws.amazon.com/documentation/elasticache/
