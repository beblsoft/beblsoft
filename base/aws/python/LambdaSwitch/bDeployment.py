#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Deployment Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from jinja2 import Environment, FileSystemLoader
from base.aws.python.Lambda.bFunction import BeblsoftLambdaFunction
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS DEPLOYMENT ---------------------------- #
class BeblsoftLSDeployment():
    """
    Beblsoft LS Deployment
    """

    def __init__(self, name, bLambdaPackage, bIAMRole,  # pylint: disable=W0231
                 logLevel=logging.DEBUG, **lambdaKwargs):
        """
        Initialize Object
        Args
          name:
            Deployment Name
            Ex. "QuoteProdFlaskDeployment"
          bLambdaPackage:
            Beblsoft Lambda Package Object
          bIAMRole:
            Beblsoft IAM Role
          logLevel:
            Level to log at
            Ex. logging.DEBUG
        """
        # ---- Basic ------------------
        self.name                 = name
        self.logLevel             = logLevel
        self.bRegion              = bLambdaPackage.bRegion
        self.bIAMRole             = bIAMRole
        self.bDeployments         = []  # Child deployments are added here

        # ---- Lambda Function --------
        self.bLSDir               = os.path.dirname(os.path.realpath(__file__))
        # base/aws/python/LambdaSwitch
        self.bLSRelDir            = self.bLSDir.split("beblsoft/")[1]
        self.bLSHandlerDir        = os.path.join(self.bLSDir, "Handler")
        fromToPathList            = [{"fromPath": "{}/*".format(self.bLSHandlerDir),
                                      "toPath": "."},
                                     {"fromPath": "{}/*".format(self.bLSDir),
                                      "toPath": "{}".format(self.bLSRelDir)
                                      }]
        bLambdaPackage.fromToPaths.extend(fromToPathList)
        self.bLambdaFunction      = BeblsoftLambdaFunction(
            name                         = name,
            handler                      = "bHandler.handler",
            runTime                      = "python3.6",
            bLambdaPackage               = bLambdaPackage,
            bIAMRole                     = bIAMRole,
            **lambdaKwargs)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @property
    def settingsDict(self):
        """
        Return settings dictionary
        """
        settingsDict = {"GLOBAL_LOGLEVEL": self.logLevel}
        for bDeployment in self.bDeployments:
            settingsDict = bDeployment.updateSettingsDict(settingsDict)
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self, sleepS=1):
        """
        Create Deployment
        """
        self.createLambdaSettingsFile()
        self.bLambdaFunction.create(sleepS=sleepS)

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        self.createLambdaSettingsFile()
        self.bLambdaFunction.update()

    @logFunc()
    def delete(self):
        """
        Delete Deployment
        """
        self.bLambdaFunction.delete()

    @logFunc()
    def describe(self):
        """
        Describe Deployment
        """
        self.bLambdaFunction.describe()

    # ----------------------- LAMBDA FUNCTIONS ------------------------------ #
    @logFunc()
    def createLambdaSettingsFile(self):
        """
        Autogenerate flask deployment settings
        """
        templateFile        = "bSettingsTemplate.jinja"
        settingsPath        = "{}/bSettings.py".format(self.bLSHandlerDir)
        jinjaEnv            = Environment(
            loader             = FileSystemLoader(self.bLSHandlerDir),
            trim_blocks        = True)
        template            = jinjaEnv.get_template(templateFile)
        templateStr         = template.render(
            generatorFile      = __file__,
            settingsDict       = self.settingsDict)
        if os.path.exists(settingsPath):
            os.remove(settingsPath)
        with open(settingsPath, "w") as f:
            f.write(templateStr)

    @logFunc()
    def invoke(self, funcStr, funcKwargs={}, **lambdaInvokeKwargs):  # pylint: disable=W0102
        """
        Invoke flask deployment lambda
        Args
          funcStr:
            Function in lambda package to invoke
            Ex. "mod1.mod2.mod3:func10"
          funcKwargs:
            Dictionary of args to pass to funcStr
            Ex. { "foo": "bar", "laz": "baz" }
          lambdaInvokeKwargs:
            Args to pass to BeblsoftLambdaFunction.invoke
        """
        payloadJSON = {
            "type": "LambdaSwitchInvocation",
            "funcStr": funcStr,
            "kwargs": funcKwargs
        }

        return self.bLambdaFunction.invoke(
            payloadJSON=payloadJSON,
            **lambdaInvokeKwargs)

    @logFunc()
    def tailLambda(self):
        """
        Tail lambda function
        """
        self.bLambdaFunction.tail()

    @logFunc()
    def dumpStreamToFile(self, **cwlgDumpStreamToFileKwargs):
        """
        Dump log stream to file
        """
        self.bLambdaFunction.dumpStreamToFile(**cwlgDumpStreamToFileKwargs)
