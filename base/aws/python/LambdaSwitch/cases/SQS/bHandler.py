#!/usr/bin/env python3
"""
NAME:
 bHandler.py

DESCRIPTION
 Beblsoft Lambda Switch SQS Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT LS SQS HANDLER -------------------------- #
class BeblsoftLSSQSHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS SQS Handler

    Example SQS Event:
    {
      "Records": [
         {
            "messageId": "c80e8021-a70a-42c7-a470-796e1186f753",
            "receiptHandle": "AQEBJQ+/u6NsnT5t8Q/VbVxgdUl4TMKZ5FqhksRdIQvLBhwNvADoBxYSOVeCBXdnS9P+erlTtwEALHsnBXynkfPLH3BOUqmgzP25U8kl8eHzq6RAlzrSOfTO8ox9dcp6GLmW33YjO3zkq5VRYyQlJgLCiAZUpY2D4UQcE5D1Vm8RoKfbE+xtVaOctYeINjaQJ1u3mWx9T7tork3uAlOe1uyFjCWU5aPX/1OHhWCGi2EPPZj6vchNqDOJC/Y2k1gkivqCjz1CZl6FlZ7UVPOx3AMoszPuOYZ+Nuqpx2uCE2MHTtMHD8PVjlsWirt56oUr6JPp9aRGo6bitPIOmi4dX0FmuMKD6u/JnuZCp+AXtJVTmSHS8IXt/twsKU7A+fiMK01NtD5msNgVPoe9JbFtlGwvTQ==",
            "body": "{\"foo\":\"bar\"}",
            "attributes": {
                "ApproximateReceiveCount": "3",
                "SentTimestamp": "1529104986221",
                "SenderId": "594035263019",
                "ApproximateFirstReceiveTimestamp": "1529104986230"
            },
            "messageAttributes": {},
            "md5OfBody": "9bb58f26192e4ba00f01e2e7b136bbd8",
            "eventSource": "aws:sqs",
            "eventSourceARN": "arn:aws:sqs:us-west-2:123456789012:MyQueue",
            "awsRegion": "us-west-2"
         }
      ]
    }
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)
        self.queueNameToFuncDict = {}
        queueNameToFuncStrDict   = getattr(self.settings, "SQS_QUEUE_FUNCSTR_DICT", {})
        for queueName, funcStr in queueNameToFuncStrDict.items():
            self.queueNameToFuncDict[queueName] = self.getFuncObjFromFuncStr(funcStr)

    @property
    def enabled(self):
        """
        Return True if handler is enabled
        """
        return getattr(self.settings, "SQS_ENABLED", False)

    @logFunc()
    def shouldHandleEvent(self, event, context):
        """
        Return True if this handler should handle the event
        """
        shouldHandle = False
        try:
            record       = event.get("Records")[0]
            shouldHandle = record.get("eventSource", None) == "aws:sqs"
        except Exception as _:  # pylint: disable=W0703
            pass
        return shouldHandle

    @logFunc()
    def handleEvent(self, event, context):
        """
        Handle the Event

        Returns:
          funcStr response
        """
        recordList = event.get("Records")
        record     = recordList[0]
        qName      = record.get("eventSourceARN").split(":")[-1]
        funcStr    = self.queueNameToFuncDict[qName]

        return funcStr(event=event, context=context)
