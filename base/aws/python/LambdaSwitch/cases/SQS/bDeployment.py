#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch SQS Deployment Functionality
 Inbound messages are proccessed via picture below.
 The lambda event source mapping polls the source and dead letter queues for
 pending messages and then executes them

RELEVANT DOCUMENTATION
 Lambda Processing SQS  : https://docs.aws.amazon.com/lambda/latest/dg/with-sqs.html

PICTURE:
  1. Producer Queues Message
        |
  2. SQS Queue
        |
  3. Lambda Event Source Mapping
        |
  4. AWS Lambda Handler (Function A)
        |
        |---------------------------------------------------
        |                        |                         |
  5a. Success                5b. Failure i            5c. Failure maxReceive
        |                        |                        Exhausted
        |                    Message is invoked            |
        |                    again. State 3                |
    Lambda Handler                                         |
    Deletes Message from                              6. SQS Queue puts message
    Queue                                                on dead letter queue
                                                           |
                                                           |
       -----------------------------------------------------
       |
  7. SQS Dead-letter-queue
       |
       |
  8. Lambda Event Source Mapping
       |
       |
  9. AWS Lambda Handler (Function B)
       |
       |
     Lambda Handler must successfully dead letter message


"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment
from base.aws.python.Lambda.bEventSourceMapping import BeblsoftLambdaEventSourceMapping
from base.aws.python.SQS.bQueue import BeblsoftSQSQueue
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS SQS DEPLOYMENT ------------------------ #
class BeblsoftLSSQSDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS SQS Deployment
    """

    def __init__(self,
                 bLSDeployment,
                 # Regular Queue
                 qName,
                 qFuncStr,
                 qMessageRetentionS = 86400,  # 1 Day
                 qVisibilityTimeoutS = 20,
                 qMaxReceiveCount = 3,
                 qBatchSize = 1,

                 # Dead letter
                 dlqName = None,
                 dlqFuncStr = None,
                 dlqMessageRetentionS = 604800,  # 7 Days
                 dlqVisibilityTimeoutS = 20,
                 dlqBatchSize = 1,

                 # Both queues
                 fifoQueues = False,
                 contentBasedDeduplication = False,
                 bKMSCMKey = None):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          qName:
            Name of queue to be created
            Note: Assumed that queue is in same region as lambda
            Ex. "SmecknTestServerLSQueue"
          qFuncStr:
            Function to execute on received queue message(s)
            Must be in bLambdaPackage
            If function throws an error or times out, message(s) will be delivered again
            If function completes without an error, message(s) is deemed processed
            Must have prototpye: def handle(event, context)
            Ex. "smeckn.server.aws.jobQueue:handleMessage"
          dlqName:
            Name of dead letter queue to be created
            Note: Assumed that queue is in same region as lambda
            Ex. "SmecknTestServerLSDLQueue"
          dlqFuncStr:
            Function to execute on received queue message(s)
            Must be in bLambdaPackage
            If function throws an error, message(s) will be delivered again, resulting in infinite loop
            To avoid this, funcStr should NOT return error!
            Must have prototpye: def handle(event, context)
            Ex. "smeckn.server.aws.jobDLQueue:handleMessage"

          Other args:
            See BeblsoftSQSQueue, BeblsoftLambdaEventSourceMapping

        """
        super().__init__(bLSDeployment)
        # Lambda ---------------------------
        self.bLambdaFunction          = self.bLSDeployment.bLambdaFunction
        self.bRegion                  = self.bLambdaFunction.bRegion

        # Dead letter queue ----------------
        self.bSQSDLQueue              = None
        self.bDLLambdaESMapping       = None
        self.dlqFuncStr               = dlqFuncStr
        if dlqName:
            self.bSQSDLQueue          = BeblsoftSQSQueue(
                name                      = dlqName,
                messageRetentionS         = dlqMessageRetentionS,
                visibilityTimeoutS        = dlqVisibilityTimeoutS,
                fifoQueue                 = fifoQueues,
                contentBasedDeduplication = contentBasedDeduplication,
                bKMSCMKey                 = bKMSCMKey,
                bRegion                   = self.bRegion)
            self.bDLLambdaESMapping   = BeblsoftLambdaEventSourceMapping(
                bFunction                 = self.bLambdaFunction,
                eventSourceARNFunc        = lambda: self.bSQSDLQueue.arn,
                batchSize                 = dlqBatchSize)

        # Regular Queue --------------------
        self.qFuncStr                 = qFuncStr
        self.bSQSQueue                = BeblsoftSQSQueue(
            name                          = qName,
            messageRetentionS             = qMessageRetentionS,
            visibilityTimeoutS            = qVisibilityTimeoutS,
            fifoQueue                     = fifoQueues,
            contentBasedDeduplication     = contentBasedDeduplication,
            bSQSDeadLetterQueue           = self.bSQSDLQueue,
            maxReceiveCount               = qMaxReceiveCount,
            bKMSCMKey                     = bKMSCMKey,
            bRegion                       = self.bRegion)
        self.bLambdaESMapping         = BeblsoftLambdaEventSourceMapping(
            bFunction                     = self.bLambdaFunction,
            eventSourceARNFunc            = lambda: self.bSQSQueue.arn,
            batchSize                     = qBatchSize)

    def __str__(self):
        return "[{} qName={}]".format(self.__class__.__name__, self.bSQSQueue.name)

    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        sqsQueueFuncStrDictName                        = "SQS_QUEUE_FUNCSTR_DICT"
        sqsQueueFuncStrDict                            = settingsDict.get(sqsQueueFuncStrDictName, {})
        sqsQueueFuncStrDict[self.bSQSQueue.name]       = self.qFuncStr
        if self.bSQSDLQueue.name:
            sqsQueueFuncStrDict[self.bSQSDLQueue.name] = self.dlqFuncStr

        settingsDict.update({
            "SQS_ENABLED": True,
            sqsQueueFuncStrDictName: sqsQueueFuncStrDict
        })
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Deployment
        """
        if self.bSQSDLQueue:
            self.bSQSDLQueue.create()
            self.bDLLambdaESMapping.create()
        self.bSQSQueue.create()
        self.bLambdaESMapping.create()

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        if self.bSQSDLQueue:
            self.bSQSDLQueue.update()
            self.bDLLambdaESMapping.update()
        self.bSQSQueue.update()
        self.bLambdaESMapping.update()

    @logFunc()
    def delete(self):
        """
        Delete Deployment
        """
        self.bLambdaESMapping.delete()
        self.bSQSQueue.delete()
        if self.bSQSDLQueue:
            self.bDLLambdaESMapping.delete()
            self.bSQSDLQueue.delete()

    @logFunc()
    def describe(self):
        """
        Describe Deployment
        """
        if self.bSQSDLQueue:
            self.bSQSDLQueue.describe()
            self.bDLLambdaESMapping.describe()
        self.bSQSQueue.describe()
        self.bLambdaESMapping.describe()
