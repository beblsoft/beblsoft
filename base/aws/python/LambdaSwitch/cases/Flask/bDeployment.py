#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Flask Deployment Functionality
 Flask is deployed on AWS via the picture below.

PICTURE
  Client HTTP Request
        |
  Alias Recod                    api.quotablejoy.com
        |
  Cloud Front Distribution Edge  d2sxs4i9nlamdk.cloudfront.net
        |
  AWS API Gateway Rest API       https://6z25k1haj9.execute-api.us-east-1.amazonaws.com/Default
        |
  AWS Lambda Function Handler    SmecknProdLambdaSwitch
        |
  Flask Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment
from base.aws.python.APIGateway.bRestAPI import BeblsoftAPIGatewayRestAPI
from base.aws.python.APIGateway.bRootResource import BeblsoftAPIGatewayRootResource
from base.aws.python.APIGateway.bResource import BeblsoftAPIGatewayResource
from base.aws.python.APIGateway.bMethod import BeblsoftAPIGatewayMethod
from base.aws.python.APIGateway.bLambdaIntegration import BeblsoftAPIGatewayLambdaIntegration
from base.aws.python.APIGateway.bDeployment import BeblsoftAPIGatewayDeployment
from base.aws.python.APIGateway.bStage import BeblsoftAPIGatewayStage
from base.aws.python.APIGateway.bCustomEdgeDomain import BeblsoftAPIGatewayCustomEdgeDomain
from base.aws.python.APIGateway.bBasePathMapping import BeblsoftAPIGatewayBasePathMapping
from base.aws.python.Route53.ResourceRecordSet.bAlias import BeblsoftAliasResourceRecordSet
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS FLASK DEPLOYMENT ---------------------- #
class BeblsoftLSFlaskDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS Flask Deployment
    """

    def __init__(self, bLSDeployment, domainName, bHostedZone,
                 bCertificate, appFuncStr, disableFlaskAppLogging=True):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          domainName:
            Name to deploy flask api
            Ex. "api.quotablejoy.com"
          bHostedZone:
            Beblsoft Hosted Zone Object
          bCertificate:
            BeblsoftACMCertificate object
          appFuncStr:
            Python path to flask function that returns application object
            Ex: "mod1.mod2.mod3:appFunc"
          disableFlaskAppLogging:
            If True, disable flask app logging.
            This prevents flask from logging exception stack traces

        Notes
          Lambda event and context are added to the request context and can
          be retrieved as follows:

          from flask import request
          environ    = request.environ
          awsEvent   = environ.get("awsEvent", None)
          awsContext = environ.get("awsContext", None)
        """

        super().__init__(bLSDeployment)
        # ---- Basic ------------------
        self.domainName             = domainName
        self.bHostedZone            = bHostedZone
        self.bCertificate           = bCertificate
        self.appFuncStr             = appFuncStr
        self.disableFlaskAppLogging = disableFlaskAppLogging
        self.bRegion                = bLSDeployment.bRegion

        # ---- Rest API ---------------
        self.bAGRestAPI           = BeblsoftAPIGatewayRestAPI(
            name                          = "{}RestAPI".format(self.bLSDeployment.name),
            description                   = "{} Rest API".format(self.bLSDeployment.name),
            bRegion                       = self.bRegion)

        # ---- Root Resource = "/" ----
        self.bAGRootResource      = BeblsoftAPIGatewayRootResource(
            bAGRestAPI                    = self.bAGRestAPI)
        self.bAGRootMethod        = BeblsoftAPIGatewayMethod(
            bAGResource                   = self.bAGRootResource,
            httpMethod                    = "ANY")
        self.bAGRootIntegration   = BeblsoftAPIGatewayLambdaIntegration(
            bAGMethod                     = self.bAGRootMethod,
            bLambdaFunction               = self.bLSDeployment.bLambdaFunction,
            bIAMRole                      = self.bLSDeployment.bIAMRole)

        # ---- Proxy Resource = "/*" --
        self.bAGProxyResource     = BeblsoftAPIGatewayResource(
            bAGRestAPI                    = self.bAGRestAPI,
            bAGParentRes                  = self.bAGRootResource,
            pathPart                      = "{proxy+}")
        self.bAGProxyMethod       = BeblsoftAPIGatewayMethod(
            bAGResource                   = self.bAGProxyResource,
            httpMethod                    = "ANY")
        self.bAGProxyIntegration  = BeblsoftAPIGatewayLambdaIntegration(
            bAGMethod                     = self.bAGProxyMethod,
            bLambdaFunction               = self.bLSDeployment.bLambdaFunction,
            bIAMRole                      = self.bLSDeployment.bIAMRole)

        # ---- Deployment -------------
        self.bAGDeployment        = BeblsoftAPIGatewayDeployment(
            bAGRestAPI                    = self.bAGRestAPI)

        # ---- Stage ------------------
        self.bAGStage             = BeblsoftAPIGatewayStage(
            bAGDeployment                 = self.bAGDeployment,
            name                          = "Default")

        # ---- Custom Edge Domain -----
        self.bAGEdgeDomain        = BeblsoftAPIGatewayCustomEdgeDomain(
            domainName                    = self.domainName,
            bCertificate                  = self.bCertificate,
            bRegion                       = self.bRegion)

        # ---- Base Path Mapping ------
        self.bAGBasePathMapping   = BeblsoftAPIGatewayBasePathMapping(
            domainName                    = self.domainName,
            basePath                      = "",
            bAGStage                      = self.bAGStage)

        # ---- Alias Record -----------
        self.bAliasRecord         = BeblsoftAliasResourceRecordSet(
            bHostedZone                   = self.bHostedZone,
            domainName                    = self.domainName,
            recordType                    = "A",
            dnsNameFunc                   = lambda: self.edgeDomainName,
            cloudFrontAlias               = True)

    def __str__(self):
        return "[{} domainName={}]".format(self.__class__.__name__, self.domainName)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def edgeDomainName(self):
        """
        Return cloud front edge domain
        """
        return self.bAGEdgeDomain.cfDomain

    @property
    def stageDomainName(self):
        """
        Return API Gateway stage domain
        """
        return self.bAGStage.url

    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        settingsDict.update({
            "FLASK_ENABLED": True,
            "FLASK_APPFUNCSTR": self.appFuncStr,
            "FLASK_DISABLE_APP_LOG": self.disableFlaskAppLogging
        })
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self, api=False, resources=False, deployment=False, stage=False,
               edge=False, bp=False, alias=False, allobjs=False):
        """
        Create Deployment
        """
        if api or allobjs:
            self.bAGRestAPI.create()
        if resources or allobjs:
            self.bAGRootMethod.create()
            self.bAGRootIntegration.create()
            self.bAGProxyResource.create()
            self.bAGProxyMethod.create()
            self.bAGProxyIntegration.create()
        if deployment or allobjs:
            self.bAGDeployment.create()
        if stage or allobjs:
            self.bAGStage.create()
        if edge or allobjs:
            self.bAGEdgeDomain.create()
        if bp or allobjs:
            self.bAGBasePathMapping.create()
        if alias or allobjs:
            self.bAliasRecord.create()

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        pass

    @logFunc()
    def delete(self, api=False, resources=False, deployment=False, stage=False,
               edge=False, bp=False, alias=False, allobjs=False):
        """
        Delete Deployment
        """
        if alias or allobjs:
            self.bAliasRecord.delete()
        if bp or allobjs:
            self.bAGBasePathMapping.delete()
        if edge or allobjs:
            self.bAGEdgeDomain.delete()
        if stage or allobjs:
            self.bAGStage.delete()
        if deployment or allobjs:
            self.bAGDeployment.delete()
        if resources or allobjs:
            self.bAGProxyIntegration.delete()
            self.bAGProxyMethod.delete()
            self.bAGProxyResource.delete()
            self.bAGRootIntegration.delete()
            self.bAGRootMethod.delete()
        if api or allobjs:
            self.bAGRestAPI.delete()

    @logFunc()
    def describe(self, api=False, resources=False, deployment=False,
                 stage=False, edge=False, bp=False, alias=False, allobjs=False):
        """
        Describe Deployment
        """
        if api or allobjs:
            self.bAGRestAPI.describe()
        if resources or allobjs:
            self.bAGRootMethod.describe()
            self.bAGRootIntegration.describe()
            self.bAGProxyResource.describe()
            self.bAGProxyMethod.describe()
            self.bAGProxyIntegration.describe()
        if deployment or allobjs:
            self.bAGDeployment.describe()
        if stage or allobjs:
            self.bAGStage.describe()
        if edge or allobjs:
            self.bAGEdgeDomain.describe()
        if bp or allobjs:
            self.bAGBasePathMapping.describe()
        if alias or allobjs:
            self.bAliasRecord.describe()

    # ----------------------- INVOKE ---------------------------------------- #
    @logFunc()
    def invokeAPI(self, *args, **kwargs):
        """
        Invoke flask deployment api
        """
        self.bAGProxyResource.invoke(*args, **kwargs)
