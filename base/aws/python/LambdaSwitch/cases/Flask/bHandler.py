#!/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
  Beblsoft Lambda Switch Flask Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import sys
import json
import traceback
import logging
import pprint
from urllib.parse import urlencode
import six
from werkzeug.wsgi import ClosingIterator
from werkzeug.wrappers import Response
from werkzeug import urls
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT LS FLASK HANDLER ------------------------ #
class BeblsoftLSFlaskHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS Flask Handler

    Example API Gateway Event:
    {
      'body': None,
      'headers': {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.8',
        'CloudFront-Forwarded-Proto': 'https',
        'CloudFront-Is-Desktop-Viewer': 'true',
        'CloudFront-Is-Mobile-Viewer': 'false',
        'CloudFront-Is-SmartTV-Viewer': 'false',
        'CloudFront-Is-Tablet-Viewer': 'false',
        'CloudFront-Viewer-Country': 'US',
        'Host': 'api.test.smeckn.com',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '
        'AppleWebKit/537.36 (KHTML, like Gecko) '
        'Chrome/61.0.3163.100 Safari/537.36',
        'Via': '2.0 d7d969e3c6b32bf100efb5f21e71b14a.cloudfront.net '
        '(CloudFront)',
        'X-Amz-Cf-Id': 'vpGikfTos1XBudj0_pyUVArXv0c-XeKECLGJA8lnYzOjCRlup1kLGg==',
        'X-Amzn-Trace-Id': 'Root=1-5ad503b7-ea2a013c4b796554460e2440',
        'X-Forwarded-For': '209.17.40.44, 54.239.145.57',
        'X-Forwarded-Port': '443',
        'X-Forwarded-Proto': 'https',
        'upgrade-insecure-requests': '1'
      },
      'httpMethod': 'GET',
      'isBase64Encoded': False,
      'path': '/api/v1/',
      'pathParameters': {'proxy': 'api/v1'},
      'queryStringParameters': None,
      'requestContext': {
          'accountId': '225928776711',
          'apiId': 'ialdch9l1c',
          'extendedRequestId': 'Fc2EtHe8IAMFcuQ=',
          'httpMethod': 'GET',
          'identity': {
            'accessKey': None,
            'accountId': None,
            'caller': None,
            'cognitoAuthenticationProvider': None,
            'cognitoAuthenticationType': None,
            'cognitoIdentityId': None,
            'cognitoIdentityPoolId': None,
            'sourceIp': '209.17.40.44',
            'user': None,
            'userAgent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
            'userArn': None
          },
          'path': '/api/v1/',
          'protocol': 'HTTP/1.1',
          'requestId': '834b8a37-41b2-11e8-be31-ff62898f7c19',
          'requestTime': '16/Apr/2018:20:12:39 +0000',
          'requestTimeEpoch': 1523909559756,
          'resourceId': 'a97ppd',
          'resourcePath': '/{proxy+}',
          'stage': 'Default'
      },
      'resource': '/{proxy+}',
      'stageVariables': None
    }
    """

    def __init__(self, *args, **kwargs):  # pylint: disable=W0613
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)
        if self.enabled:
            appFuncStr          = self.settings.FLASK_APPFUNCSTR
            appFunc             = self.getFuncObjFromFuncStr(appFuncStr)
            appObject           = appFunc()
            self.flaskAppObject = FlaskMiddleware(appObject)
        else:
            self.flaskAppObject = None

        # Disable Flask Logging - This prevents flask from logging exceptions, among other things
        self.disableFlaskAppLogging = getattr(self.settings, "FLASK_DISABLE_APP_LOG", True)
        if self.disableFlaskAppLogging:
            logging.getLogger("flask.app").disabled = True

    @property
    def enabled(self):
        """
        Return True if enabled
        """
        return getattr(self.settings, "FLASK_ENABLED", False)

    @logFunc()
    def shouldHandleEvent(self, event, context):  # pylint: disable=R0201,W0613
        """
        Return True if API gateway event
        """
        httpMethod = event.get("httpMethod", None)
        return (httpMethod != None)

    @logFunc()
    def handleEvent(self, event, context):
        """
        Handle API gateway event by doing the following:
        - Invoke the underlying Flask application
        - Map the response back to API Gateway.

        Note on MultiValueHeaders:
          We use "multiValueHeaders" instead of "headers" as each cookie has a
          the same header name, "Set-Cookie". multiValueHeaders allow multiple
          cookies to be set without overwriting each other.

        Returns
          Lambda Proxy Response
          See https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-output-format
        """
        resp                   = {}
        environ                = self.createFlaskRequest(event, context)

        try:
            flaskResp          = Response.from_app(self.flaskAppObject, environ)
            resp["statusCode"] = flaskResp.status_code
            resp["body"]       = flaskResp.get_data(as_text=True)

            # MultiValueHeaders
            multiValueHeaders  = {}
            for key, value in flaskResp.headers:
                if key in multiValueHeaders:
                    multiValueHeaders[key].append(value)
                else:
                    multiValueHeaders[key] = [value]
            resp["multiValueHeaders"] = multiValueHeaders

            # Headers
            resp["headers"]                            = {}
            resp["headers"]["x-aws-lambda-request-id"] = context.aws_request_id
            resp["headers"]["x-aws-lambda-log-group"]  = context.log_group_name
            resp["headers"]["x-aws-lambda-log-stream"] = context.log_stream_name

        except Exception as e:  # pylint: disable=W0703
            exc_info = sys.exc_info()
            logger.exception(pprint.pformat(e.__dict__))
            resp["statusCode"] = 500
            body               = {
                "message": "Uncaught exception in flask app",
                "traceback": traceback.format_exception(*exc_info)
            }
            resp["body"]       = json.dumps(str(body), indent=4)
        return resp

    @logFunc()
    def createFlaskRequest(self, event, context):  # pylint: disable=R0201
        """
        Create a Flask request from an API Gateway event
        Args
          event:
            API Gateway Event
          context:
            Lambda context
        """
        httpMethod        = event.get("httpMethod")
        pathParams        = event.get("pathParameters")  # pylint: disable=W0612
        queryStringParams = event.get("queryStringParameters", None)
        headers           = event.get("headers", {})
        body              = event.get("body", None)
        bodyStr           = ""
        if body:
            bodyStr       = body.encode("utf-8")
            body          = six.BytesIO(bodyStr)
        path              = urls.url_unquote(event.get("path"))
        queryString       = ""
        if queryStringParams:
            queryString   = urlencode(queryStringParams)
        serverName        = "LambdaFlask"
        xForwardedPort    = headers.get("X-Forwarded-Port", "80")
        xForwardedFor     = headers.get("X-Forwarded-For", "")
        remoteAddr        = "127.0.0.1"
        if "," in xForwardedFor:
            # Last IP is cloudfront
            # 2nd to last IP is client
            remoteAddr    = xForwardedFor.split(", ")[-2]

        # Content-Type can come in either case
        contentType       = headers.get("Content-Type", None)
        if not contentType:
            contentType   = headers.get("content-type", "")
        contentLength     = "0"
        if body:
            contentLength = str(len(bodyStr))

        environ = {
            "HTTPS": "on",
            "wsgi.url_scheme": "https",
            "wsgi.version": (1, 0),
            "wsgi.input": body,
            "wsgi.errors": sys.stderr,
            "wsgi.multiprocess": False,
            "wsgi.multithread": False,
            "wsgi.run_once": False,
            "PATH_INFO": path,
            "QUERY_STRING": queryString,
            "REMOTE_ADDR": remoteAddr,
            "REQUEST_METHOD": httpMethod,
            "SERVER_NAME": serverName,
            "SERVER_PORT": xForwardedPort,
            "SERVER_PROTOCOL": str("HTTP/1.1"),
            "lambda.context": context,
            "CONTENT_TYPE": contentType,
            "CONTENT_LENGTH": contentLength,
            "awsEvent": event,
            "awsContext": context,
        }

        for header in headers:
            environHeader = "HTTP_{}".format(header.upper().replace("-", "_"))
            environ[environHeader] = str(headers[header])
        # logger.info("environ={} pathParams={} xForwardedFor={}".format(
        #     pprint.pformat(environ), pathParams, xForwardedFor))
        return environ


# ------------------------ FLASK MIDDLEWARE --------------------------------- #
class FlaskMiddleware():
    """
    Flask Middleware Class necessary for Application Deployment
    """

    def __init__(self, appObj):
        """
        Initialize object
        Args
          appObj:
            WSGI app object
        """
        self.appObj = appObj

    def __call__(self, environ, start_response):
        """
        Call Object
        """
        def encode_response(status, headers, exc_info=None):
            """
            Encode response
            """
            return start_response(status, headers, exc_info)

        response = self.appObj(environ, encode_response)
        return ClosingIterator(response)
