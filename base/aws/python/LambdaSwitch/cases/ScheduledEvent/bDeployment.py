#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Scheduled Event Deployment Functionality
 Scheduled Events are deployed on AWS via the picture below.

PICTURE
  Scheduled Events               Ex. Keep Warm
        |
  AWS Lambda Function Handler    SmecknProdLambdaSwitch
        |
  User Defied Callback
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment
from base.aws.python.CloudWatchEvents.bRule import BeblsoftCloudWatchEventsRule
from base.aws.python.CloudWatchEvents.bRuleLambdaTarget import BeblsoftCloudWatchEventsRuleLambdaTarget
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS SCHEDULED EVENT DEPLOYMENT ------------ #
class BeblsoftLSScheduledEventDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS Scheduled Event
    """
    MIN_INDEX = 0
    MAX_INDEX = 100

    def __init__(self, bLSDeployment, index, funcStr, expression,
                 kwargsFunc=None):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          index:
            Unique index [required]
            Should be unique for all scheduled events on lambda switch
            Ex. 0
            Must be between MIN_INDEX and MAX_INDEX
          funcStr:
            Function to execute scheduled event
            Must be in bLambdaPackage
            Must have prototpye: def handle(**kwargs)
            Where kwargs are passed via kwargsFunc below
            Ex. "quote.serverV2.aws:foo"
          expression:
            Rate expression at which to execute function [required]
            Ex. "rate(5 minutes)"
          kwargsFunc:
            Function to return dictionary of kwargs that will be passed to
            scheduled function [optional]
        """
        super().__init__(bLSDeployment)
        self.index                = index
        if index > BeblsoftLSScheduledEventDeployment. MAX_INDEX:
            raise(BeblsoftError("Index {} too high".format(index)))
        if index < BeblsoftLSScheduledEventDeployment.MIN_INDEX:
            raise(BeblsoftError("Index {} too low".format(index)))

        self.funcStr              = funcStr
        self.expression           = expression
        self.kwargsFunc           = lambda: {}
        if kwargsFunc:
            self.kwargsFunc       = kwargsFunc
        self.bRegion              = self.bLSDeployment.bRegion
        self.name                 = "ScheduledEvent-{}-{}".format(self.bLSDeployment.name,
                                                                  self.index)
        self.bCWERule             = BeblsoftCloudWatchEventsRule(
            name                 = self.name,
            bIAMRole             = self.bLSDeployment.bIAMRole,
            scheduleExpression   = self.expression,
            bRegion              = self.bRegion)
        self.bCWERuleTarget       = BeblsoftCloudWatchEventsRuleLambdaTarget(
            bLambdaFunction      = self.bLSDeployment.bLambdaFunction,
            bRule                = self.bCWERule,
            bIAMRole             = self.bLSDeployment.bIAMRole,
            tid                  = self.name,
            kwargs               = self.kwargsFunc())

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        settingsDict.update({
            "SCHEDEVENT_ENABLED": True,
            "SCHEDEVENT_FUNCSTR{}".format(self.index): "{}".format(self.funcStr),
        })
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Deployment
        """
        self.bCWERule.create()
        self.bCWERuleTarget.create()

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        self.delete()
        self.create()

    @logFunc()
    def delete(self):
        """
        Delete Deployment
        """
        self.bCWERuleTarget.delete()
        self.bCWERule.delete()

    @logFunc()
    def describe(self):
        """
        Describe Deployment
        """
        self.bCWERule.describe()
        self.bCWERuleTarget.describe()
