# !/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
 Beblsoft Lambda Switch Scheduled Event Handler Functionality

BACKGROUND
  LAMBDA EVENTS: https://docs.aws.amazon.com/lambda/latest/dg/invoking-lambda-function.html
"""


# ------------------------ IMPORTS ------------------------------------------ #
import re
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler
from base.aws.python.LambdaSwitch.cases.ScheduledEvent.bDeployment import BeblsoftLSScheduledEventDeployment


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()


# ------------------------ BEBLSOFT LS SCHEDULED EVENT HANDLER -------------- #
class BeblsoftLSScheduledEventHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS Scheduled Event Handler

    Example CloudWatchEvents Event:
    {
       'account': '225928776711',
       'detail': {},
       'detail-type': 'Scheduled Event',
       'id': '0e82ec82-f89c-75b4-861c-20eea3178e82',
       'kwargs': {'Hello': 'World!'},
       'region': 'us-east-1',
       'resources': ['arn:aws:events:us-east-1:225928776711:rule/QuoteTestFlaskDeployment-0-bHandler-keepWarm'],
       'source': 'aws.events',
       'time': '2018-03-16T21:13:54Z',
       'version': '0'
    }
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)

        # Each function callback has a unique index that is encoded in the settings file.
        # Create a map of index -> function object
        self.idxFuncDict  = {}
        for idx in range(BeblsoftLSScheduledEventDeployment.MIN_INDEX,
                         BeblsoftLSScheduledEventDeployment.MAX_INDEX + 1):
            funcStr = getattr(self.settings,
                              "SCHEDEVENT_FUNCSTR{}".format(idx), None)
            if funcStr:
                funcObj = self.getFuncObjFromFuncStr(funcStr)
                self.idxFuncDict[idx] = funcObj

    @property
    def enabled(self):
        """
        Return True if enabled
        """
        return getattr(self.settings, "SCHEDEVENT_ENABLED", False)

    @logFunc()
    def shouldHandleEvent(self, event, context):  # pylint: disable=R0201
        """
        Return True if event was scheduled
        """
        detailType = event.get("detail-type", None)
        source     = event.get("source", None)
        return ((detailType == "Scheduled Event") and (source == "aws.events"))

    @logFunc()
    def handleEvent(self, event, context):  # pylint: disable=R0201
        """
        Execute a scheduled event
        """
        ruleName  = None
        resources = event.get("resources", None)
        kwargs    = event.get("kwargs", {})
        resp      = None
        index     = None

        # Parse index from resources name
        # 'resources': ['arn:aws:events:us-east-1:225928776711:rule/QuoteTestFlaskDeployment-0-bHandler-keepWarm'],
        if resources:
            ruleName = resources[0]
        if ruleName:
            m = re.match(r".*-(\d*)$", ruleName)
            index = int(m.group(1)) if m else None

        # Invoke function
        if index != None:
            functionObj = self.idxFuncDict[index]
            resp        = functionObj(**kwargs)
        else:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                                msg="No handler for index={} idxFuncDict={}".format(index, self.idxFuncDict))

        return resp
