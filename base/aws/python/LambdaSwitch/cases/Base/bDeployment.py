#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Base Deployment Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import abc


# ------------------------ BEBLSOFT LS BASE DEPLOYMENT ---------------------- #
class BeblsoftLSBaseDeployment(abc.ABC):
    """
    Beblsoft Lambda Switch Base Deployment
    """

    def __init__(self, bLSDeployment):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
        """
        self.bLSDeployment = bLSDeployment
        bLSDeployment.bDeployments.append(self)

    @abc.abstractmethod
    def updateSettingsDict(self, settingsDict):
        """
        Return updated settings dictionary after adding own fields

        Example Return
          {
            "GENERAL_LOGGING"  : logging.NOTSET,
            "GENERAL_PASSWORD" : 12345678,
           }
        """
        pass
