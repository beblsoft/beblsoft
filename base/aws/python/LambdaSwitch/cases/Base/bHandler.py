#!/usr/bin/env python3
"""
NAME:
 bHandler.py

DESCRIPTION
 Beblsoft Lambda Switch Base Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import abc
import logging
import importlib
from base.bebl.python.log.bLogger import BeblsoftLogger
from base.bebl.python.log.bLogCode import BeblsoftLogCode


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT LS BASE HANDLER ------------------------- #
class BeblsoftLSBaseHandler(abc.ABC):
    """
    Beblsoft LS Base Handler
    """

    def __init__(self, settings):
        """
        Initialize object
        Args
          settings:
            dictionary of settings passed in deployment
            Ex. {
            "GENERAL_LOGGING"  : logging.NOTSET,
            "GENERAL_PASSWORD" : 12345678,
           }
        """
        self.settings = settings

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # -------------------- ABSTRACT PROPERTIES ------------------------------ #
    @property
    @abc.abstractmethod
    def enabled(self):
        """
        Return True if handler is enabled
        """
        pass

    # -------------------- ABSTRACT METHODS --------------------------------- #
    @abc.abstractmethod
    def shouldHandleEvent(self, event, context):
        """
        Return True if this handler should handle the event
        """
        pass

    @abc.abstractmethod
    def handleEvent(self, event, context):
        """
        Handle the Event

        Returns:
          Response object
        """
        pass

    # -------------------- GET FUNCTION OBJECT FROM FUNCTION STR ------------ #
    def getFuncObjFromFuncStr(self, funcStr):  # pylint: disable=R0201
        """
        Given function string, return the function object
        Args
          funcStr:
            Function String
            Ex. smeckn.server.aws:handleBounceComplaint

        Returns
          Function object
        """
        (funcModule, funcName) = funcStr.split(":")
        moduleObj              = importlib.import_module(funcModule)
        func                   = getattr(moduleObj, funcName)
        return func

    # -------------------- LOGGING ------------------------------------------ #
    def logEventStart(self, event, context):  # pylint: disable=R0201
        """
        Log Event Start
        """
        BeblsoftLogger.debug(msg="{} Request Start".format(self),
                             bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_REQUEST_START,
                             handlerClass=self.__class__.__name__, event=event, context=context)

    def logEventDone(self, event, context, resp):  # pylint: disable=R0201,W0613
        """
        Log Event Done
        """
        BeblsoftLogger.debug(msg="{} Request Done".format(self),
                             bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_REQUEST_DONE,
                             handlerClass=self.__class__.__name__, resp=resp)
