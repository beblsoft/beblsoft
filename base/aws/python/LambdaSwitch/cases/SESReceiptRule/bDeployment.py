#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Simple Email Service (SES) Receipt Rule Deployment Functionality
 Inbound emails are proccessed via picture below.


PICTURE:
  Email Sent By Sender
        |
  Domain MX Record
        |
  AWS Inbound SMTP
  AWS SES
        |
 ----------------------------------------------------------
 |  Receipt Rule 1                   SES Receipt Rule Set |
 |      |                                                 |
 |  Receipt Rule 2                                        |
 |      |                                                 |
 |     ...                                                |
 |                                                        |
 |  Receipt Rule j -------> SNS action ----> Stop Action  |
 |     ...                     |                          |
 |      |                      |                          |
 |  Receipt Rule n             |                          |
 ------------------------------|---------------------------
                               |
                          AWS SNS Topic
                          AWS SNS Topic Subscription
                                |
                          AWS Lambda Function Handler
                                |
                          User Email Received Callback
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment
from base.aws.python.SES.ReceiptRule.Action.bSNS import BeblsoftSESReceiptRuleSNSAction
from base.aws.python.SES.ReceiptRule.Action.bStop import BeblsoftSESReceiptRuleStopAction
from base.aws.python.SES.ReceiptRule.bRule import BeblsoftSESReceiptRule
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.SNS.bTopic import BeblsoftSNSTopic
from base.aws.python.SNS.bTopicSubscription import BeblsoftSNSTopicSubscription
from base.aws.python.Route53.ResourceRecordSet.bMailExchange import BeblsoftMailExchangeResourceRecordSet


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS SES RECEIPT RULE DEPLOYMENT ----------- #
class BeblsoftLSSESReceiptRuleDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS SES ReceiptRule Deployment
    """

    def __init__(self, bLSDeployment, bSESReceiptRuleSet,
                 bHostedZone, domainName, recipientList, funcStr):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          bSESReceiptRuleSet:
            Active Beblsoft SES Receipt Rule Set
          bHostedZone:
            Beblsoft Hosted Zone associated with domain below
          domainName:
            Domain name for which MX record will be created
            Forwards mail to AWS SES
          recipientList:
            List of addresses/domains to handle inbound email programmatically
            Ex1. ["smeckn.com"]
            Ex2. ["test.smeckn.com"]
          funcStr:
            Function to execute on receiving a new email
            Must be in bLambdaPackage
            Must have prototpye: def handle(event, context)
            Ex. "smeckn.server.aws.email.receiver:handleEmailReceived"
        """
        super().__init__(bLSDeployment)
        self.bSESReceiptRuleSet    = bSESReceiptRuleSet
        self.bHostedZone           = bHostedZone
        self.domainName            = domainName
        self.recipientList         = recipientList
        self.funcStr               = funcStr
        self.bLambdaFunction       = self.bLSDeployment.bLambdaFunction
        self.bRegion               = self.bLambdaFunction.bRegion

        # Mail Exchange Record -----------
        self.bMXRecord             = BeblsoftMailExchangeResourceRecordSet(
            bHostedZone               = self.bHostedZone,
            domainName                = self.domainName,
            valueListFunc             = lambda: ["10 inbound-smtp.{}.amazonaws.com".format(
                self.bRegion.name)])

        # SNS Topic ----------------------
        self.bSNSTopic             = BeblsoftSNSTopic(
            name                      = "{}-SESReceiptTopic".format(
                self.bLSDeployment.name),
            bRegion                   = self.bLSDeployment.bRegion)

        # SNS Topic Subscription ---------
        self.bSNSTopicSubscription = BeblsoftSNSTopicSubscription(
            bSNSTopic                 = self.bSNSTopic,
            protocol                  = "lambda",
            endpointFunc              = lambda: self.bLambdaFunction.arn)

        # Receipt Rule -------------------
        self.bSNSAction            = BeblsoftSESReceiptRuleSNSAction(
            bSNSTopic                 = self.bSNSTopic)
        self.bStopAction           = BeblsoftSESReceiptRuleStopAction(
            bSESReceiptRuleSet        = bSESReceiptRuleSet)
        self.bSESReceiptRule       = BeblsoftSESReceiptRule(
            bSESReceiptRuleSet        = bSESReceiptRuleSet,
            name                      = "{}-SESReceiptRule".format(
                self.bLSDeployment.name),
            recipientList             = recipientList,
            bActionList               = [self.bSNSAction, self.bStopAction])

    def __str__(self):
        return "[{} recipientList={}]".format(self.__class__.__name__, self.recipientList)

    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        settingsDict.update({
            "SESRR_ENABLED": True,
            "SESRR_FUNCSTR": self.funcStr,
            "SESRR_TOPICNAME": self.bSNSTopic.name,
        })
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Deployment
        """
        self.bMXRecord.create()
        self.bSNSTopic.create()
        self.bSNSTopicSubscription.create()
        self.bLambdaFunction.addPermission(statementId=self.bSNSTopic.name,
                                           principal="sns.amazonaws.com",
                                           sourceArn=self.bSNSTopic.arn)
        self.bSESReceiptRule.create()

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        pass

    @logFunc()
    def delete(self):
        """
        Delete Deployment
        """
        self.bSESReceiptRule.delete()
        self.bLambdaFunction.removePermission(statementId=self.bSNSTopic.name)
        self.bSNSTopicSubscription.delete()
        self.bSNSTopic.delete()
        self.bMXRecord.delete()

    @logFunc()
    def describe(self):
        """
        Describe Deployment
        """
        self.bMXRecord.describe()
        self.bSNSTopic.describe()
        self.bSNSTopicSubscription.describe()
        self.bSESReceiptRule.describe()
