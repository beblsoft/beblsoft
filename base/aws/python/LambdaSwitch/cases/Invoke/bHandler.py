#!/usr/bin/env python3
"""
NAME:
 bHandler.py

DESCRIPTION
 Beblsoft Lambda Switch Invoke Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()


# ------------------------ BEBLSOFT LS INVOKE HANDLER ----------------------- #
class BeblsoftLSInvokeHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS Invoke Event Handler
    """

    @property
    def enabled(self):
        """
        Return True as handler is always enabled
        """
        return True

    @logFunc()
    def shouldHandleEvent(self, event, context):
        """
        Return True if this handler should handle the event
        """
        return (event.get("type", None) == "LambdaSwitchInvocation")

    @logFunc()
    def handleEvent(self, event, context):
        """
        Handle the Event

        Returns:
          Response object
        """
        funcStr = event.get("funcStr")
        kwargs  = event.get("kwargs", {})
        func    = self.getFuncObjFromFuncStr(funcStr)
        return func(**kwargs)
