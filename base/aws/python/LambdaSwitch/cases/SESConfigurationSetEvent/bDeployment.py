#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Simple Email Service (SES) Configuration Event Deployment Functionality
 SES Configuration events are handled by lambda functions via the picture

PICTURE
  Configuration Set Event               (i.e. send, bounce, complaint)
  Configuration Set Topic Destination
        |
  SNS Topic
  SNS Topic Subscription
        |
  AWS Lambda Function Handler
        |
  User Callback
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.SES.ConfigurationSet.bTopicDestination import BeblsoftSESConfigurationSetTopicDestination
from base.aws.python.SNS.bTopic import BeblsoftSNSTopic
from base.aws.python.SNS.bTopicSubscription import BeblsoftSNSTopicSubscription


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS SES CONFIGURATION EVENT DEPLOYMENT ---- #
class BeblsoftLSSESConfigurationEventDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS SES Configuration Event Deployment
    """

    def __init__(self, bLSDeployment, bSESConfigurationSet, eventList,
                 funcStr):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          bSESConfigurationSet:
            Beblsoft SES Configuration Set Object
          eventList:
            List of configuration set to handle programmatically
            Ex. ["send", "reject", "bounce", "complaint", "delivery",
                 "open", "click", "renderingFailure"]
          funcStr:
            Function to execute on receiving a configuration set event
            Must be in bLambdaPackage
            Must have prototpye: def handle(event, context)
            Ex. "smeckn.server.aws.email.configSetEvent:handleConfigSetEvent"
        """
        super().__init__(bLSDeployment)
        self.bSESConfigurationSet      = bSESConfigurationSet
        self.eventList                 = eventList
        self.funcStr                   = funcStr
        self.bLambdaFunction           = self.bLSDeployment.bLambdaFunction

        # SNS Topic ----------------------------
        self.bSNSTopic                 = BeblsoftSNSTopic(
            name                          = "{}-SESConfigSetTopic".format(
                                            self.bLSDeployment.name),
            bRegion                       = self.bLSDeployment.bRegion)

        # SNS Topic Subscription ---------------
        self.bSNSTopicSubscription     = BeblsoftSNSTopicSubscription(
            bSNSTopic                     = self.bSNSTopic,
            protocol                      = "lambda",
            endpointFunc                  = lambda: self.bLambdaFunction.arn)

        # Configuration Set Destination --------
        self.bSESConfigSetTopicDest    = BeblsoftSESConfigurationSetTopicDestination(
            bSNSTopic                     = self.bSNSTopic,
            name                          = "{}-ConfigSetTopicDest".format(
                                            self.bLSDeployment.name),
            bSESConfigurationSet          = bSESConfigurationSet,
            matchingEventList             = eventList)

    def __str__(self):
        return "[{} configSet={}]".format(self.__class__.__name__, self.bSESConfigurationSet.name)

    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        settingsDict.update({
            "SESCE_ENABLED": True,
            "SESCE_FUNCSTR": self.funcStr,
            "SESCE_TOPICNAME": self.bSNSTopic.name,
        })
        return settingsDict

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Deployment
        """
        self.bSNSTopic.create()
        self.bSNSTopicSubscription.create()
        self.bLambdaFunction.addPermission(statementId=self.bSNSTopic.name,
                                           principal="sns.amazonaws.com",
                                           sourceArn=self.bSNSTopic.arn)
        self.bSESConfigSetTopicDest.create()

    @logFunc()
    def update(self):
        """
        Update Deployment
        """
        pass

    @logFunc()
    def delete(self):
        """
        Delete Deployment
        """
        self.bSESConfigSetTopicDest.delete()
        self.bLambdaFunction.removePermission(statementId=self.bSNSTopic.name)
        self.bSNSTopicSubscription.delete()
        self.bSNSTopic.delete()

    @logFunc()
    def describe(self):
        """
        Describe Deployment
        """
        self.bSNSTopic.delete()
        self.bSNSTopicSubscription.delete()
        self.bSESConfigSetTopicDest.delete()
