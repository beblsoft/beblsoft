#!/usr/bin/env python3
"""
NAME:
 bHandler.py

DESCRIPTION
 Beblsoft Lambda Switch SES Config Event Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler
from base.aws.python.SNS.bTopic import BeblsoftSNSTopic
from base.aws.python.EC2.bRegion import BeblsoftRegion


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()


# ------------------------ BEBLSOFT LS SES CONFIG EVENT HANDLER ------------- #
class BeblsoftLSSESConfigEventHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS SES Config Event Handler
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)
        # Config Set Events
        funcStr        = getattr(self.settings, "SESCE_FUNCSTR", None)
        self.func      = None
        if funcStr:
            self.func  = self.getFuncObjFromFuncStr(funcStr)
        self.topicName = getattr(self.settings, "SESCE_TOPICNAME", None)
        self.bSNSTopic = BeblsoftSNSTopic(
            name          = self.topicName,
            bRegion       = BeblsoftRegion.getCurrent())

    @property
    def enabled(self):
        """
        Return True if handler is enabled
        """
        return getattr(self.settings, "SESCE_ENABLED", False)

    @logFunc()
    def shouldHandleEvent(self, event, context):
        """
        Return True if this handler should handle the event
        """
        try:
            record       = event.get("Records")[0]
            correctTopic = self.topicName in record.get("Sns").get("TopicArn")
            return correctTopic
        except Exception as _: #pylint: disable=W0703
            return False

    @logFunc()
    def handleEvent(self, event, context):
        """
        Handle the Event

        Returns:
          Response object
        """
        return self.func(event=event, context=context)


"""
SNS Lambda Event Structure:
https://docs.aws.amazon.com/lambda/latest/dg/eventsources.html#eventsources-sns
{
  "Records": [
    {
      "EventVersion": "1.0",
      "EventSubscriptionArn": eventsubscriptionarn,
      "EventSource": "aws:sns",
      "Sns": {
        "SignatureVersion": "1",
        "Timestamp": "1970-01-01T00:00:00.000Z",
        "Signature": "EXAMPLE",
        "SigningCertUrl": "EXAMPLE",
        "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
        "Message": "Hello from SNS!",
        "MessageAttributes": {
          "Test": {
            "Type": "String",
            "Value": "TestString"
          },
          "TestBinary": {
            "Type": "Binary",
            "Value": "TestBinary"
          }
        },
        "Type": "Notification",
        "UnsubscribeUrl": "EXAMPLE",
        "TopicArn": topicarn,
        "Subject": "TestInvoke"
      }
    }
  ]
}


Bounce Notification Structure:
https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-examples.html#notification-examples-bounce
{
   "notificationType":"Bounce",
   "bounce":{
      "bounceType":"Permanent",
      "reportingMTA":"dns; email.example.com",
      "bouncedRecipients":[
         {
            "emailAddress":"jane@example.com",
            "status":"5.1.1",
            "action":"failed",
            "diagnosticCode":"smtp; 550 5.1.1 <jane@example.com>... User"
         }
      ],
      "bounceSubType":"General",
      "timestamp":"2016-01-27T14:59:38.237Z",
      "feedbackId":"00000138111222aa-33322211-cccc-cccc-cccc-ddddaaaa068a-000000",
      "remoteMtaIp":"127.0.2.0"
   },
   "mail":{
      "timestamp":"2016-01-27T14:59:38.237Z",
      "source":"john@example.com",
      "sourceArn": "arn:aws:ses:us-west-2:888888888888:identity/example.com",
      "sourceIp": "127.0.3.0",
      "sendingAccountId":"123456789012",
      "messageId":"00000138111222aa-33322211-cccc-cccc-cccc-ddddaaaa0680-000000",
      "destination":[
        "jane@example.com",
        "mary@example.com",
        "richard@example.com"],
      "headersTruncated":false,
      "headers":[
       {
         "name":"From",
         "value":"\"John Doe\" <john@example.com>"
       },
       {
         "name":"To",
         "value":"\"Jane Doe\" <jane@example.com>, \"Mary Doe\" <mary@example.com>, \"Richard Doe\" <richard@example.com>"
       },
       {
         "name":"Message-ID",
         "value":"custom-message-ID"
       },
       {
         "name":"Subject",
         "value":"Hello"
       },
       {
         "name":"Content-Type",
         "value":"text/plain; charset=\"UTF-8\""
       },
       {
         "name":"Content-Transfer-Encoding",
         "value":"base64"
       },
       {
         "name":"Date",
         "value":"Wed, 27 Jan 2016 14:05:45 +0000"
       }
      ],
      "commonHeaders":{
         "from":[
            "John Doe <john@example.com>"
         ],
         "date":"Wed, 27 Jan 2016 14:05:45 +0000",
         "to":[
            "Jane Doe <jane@example.com>, Mary Doe <mary@example.com>, Richard Doe <richard@example.com>"
         ],
         "messageId":"custom-message-ID",
         "subject":"Hello"
       }
    }
}


Complaint Notification Structure:
https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-examples.html#notification-examples-complaint
{
  "notificationType":"Complaint",
  "complaint":{
     "userAgent":"AnyCompany Feedback Loop (V0.01)",
     "complainedRecipients":[
        {
           "emailAddress":"richard@example.com"
        }
     ],
     "complaintFeedbackType":"abuse",
     "arrivalDate":"2016-01-27T14:59:38.237Z",
     "timestamp":"2016-01-27T14:59:38.237Z",
     "feedbackId":"000001378603177f-18c07c78-fa81-4a58-9dd1-fedc3cb8f49a-000000"
  },
  "mail":{
     "timestamp":"2016-01-27T14:59:38.237Z",
     "messageId":"000001378603177f-7a5433e7-8edb-42ae-af10-f0181f34d6ee-000000",
     "source":"john@example.com",
     "sourceArn": "arn:aws:ses:us-west-2:888888888888:identity/example.com",
     "sourceIp": "127.0.3.0",
     "sendingAccountId":"123456789012",
     "destination":[
        "jane@example.com",
        "mary@example.com",
        "richard@example.com"
     ],
      "headersTruncated":false,
      "headers":[
       {
         "name":"From",
         "value":"\"John Doe\" <john@example.com>"
       },
       {
         "name":"To",
         "value":"\"Jane Doe\" <jane@example.com>, \"Mary Doe\" <mary@example.com>, \"Richard Doe\" <richard@example.com>"
       },
       {
         "name":"Message-ID",
         "value":"custom-message-ID"
       },
       {
         "name":"Subject",
         "value":"Hello"
       },
       {
         "name":"Content-Type",
         "value":"text/plain; charset=\"UTF-8\""
       },
       {
         "name":"Content-Transfer-Encoding",
         "value":"base64"
       },
       {
         "name":"Date",
         "value":"Wed, 27 Jan 2016 14:05:45 +0000"
       }
     ],
     "commonHeaders":{
       "from":[
          "John Doe <john@example.com>"
       ],
       "date":"Wed, 27 Jan 2016 14:05:45 +0000",
       "to":[
          "Jane Doe <jane@example.com>, Mary Doe <mary@example.com>, Richard Doe <richard@example.com>"
       ],
       "messageId":"custom-message-ID",
       "subject":"Hello"
     }
  }
}


Delivery Notification Structure:
https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-examples.html#notification-examples-delivery
{
  "notificationType":"Delivery",
  "mail":{
     "timestamp":"2016-01-27T14:59:38.237Z",
     "messageId":"0000014644fe5ef6-9a483358-9170-4cb4-a269-f5dcdf415321-000000",
     "source":"john@example.com",
     "sourceArn": "arn:aws:ses:us-west-2:888888888888:identity/example.com",
     "sourceIp": "127.0.3.0",
     "sendingAccountId":"123456789012",
     "destination":[
        "jane@example.com"
     ],
      "headersTruncated":false,
      "headers":[
       {
          "name":"From",
          "value":"\"John Doe\" <john@example.com>"
       },
       {
          "name":"To",
          "value":"\"Jane Doe\" <jane@example.com>"
       },
       {
          "name":"Message-ID",
          "value":"custom-message-ID"
       },
       {
          "name":"Subject",
          "value":"Hello"
       },
       {
          "name":"Content-Type",
          "value":"text/plain; charset=\"UTF-8\""
       },
       {
          "name":"Content-Transfer-Encoding",
          "value":"base64"
       },
       {
          "name":"Date",
          "value":"Wed, 27 Jan 2016 14:58:45 +0000"
       }
      ],
      "commonHeaders":{
        "from":[
           "John Doe <john@example.com>"
        ],
        "date":"Wed, 27 Jan 2016 14:58:45 +0000",
        "to":[
           "Jane Doe <jane@example.com>"
        ],
        "messageId":"custom-message-ID",
        "subject":"Hello"
      }
   },
  "delivery":{
     "timestamp":"2016-01-27T14:59:38.237Z",
     "recipients":["jane@example.com"],
     "processingTimeMillis":546,
     "reportingMTA":"a8-70.smtp-out.amazonses.com",
     "smtpResponse":"250 ok:  Message 64111812 accepted",
     "remoteMtaIp":"127.0.2.0"
  }
}
""" #pylint: disable=W0105
