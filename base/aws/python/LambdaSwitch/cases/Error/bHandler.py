#!/usr/bin/env python3
"""
NAME:
 bHandler.py

DESCRIPTION
 Beblsoft Lambda Switch Error Handler Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import enum
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Base.bHandler import BeblsoftLSBaseHandler


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT LS ERRORS ------------------------------- #
class BeblsoftLSErrorType(enum.Enum):
    """
    LS Error Types
    """
    EXCEPTION           = enum.auto()  # Exception occurred while processing event
    TIMEOUT             = enum.auto()  # Going to timeout soon
    OUT_OF_MEMORY       = enum.auto()  # Almost out of memory
    EVENT_NOT_PROCESSED = enum.auto()  # Event was not processed


# ------------------------ BEBLSOFT LS ERROR HANDLER ------------------------ #
class BeblsoftLSErrorHandler(BeblsoftLSBaseHandler):
    """
    Beblsoft LS Error Event Handler
    """

    def __init__(self, *args, **kwargs):  # pylint: disable=W0613
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)
        funcStr          = getattr(self.settings, "ERROR_FUNCSTR", None)
        if funcStr:
            self.funcObj = self.getFuncObjFromFuncStr(funcStr)
        else:
            self.funcObj = None

    @property
    def enabled(self):
        return self.funcObj != None

    @logFunc()
    def shouldHandleEvent(self, event, context):
        return True

    @logFunc()
    def handleEvent(self, eType, event, context):  # pylint: disable=W0221
        """
        Handle the Error
        """
        rval = None
        if self.funcObj:
            rval = self.funcObj(eType, event, context)
        else:
            logger.error("Unhandled error eType={} event={} context={}".format(
                eType, event, context))
        return rval
