#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Lambda Switch Error Event Deployment Functionality

PICTURE
  AWS Event
        |
  AWS Lambda Function Handler
        |
  Something Goes wrong. See BeblsoftLSErrorType.
        |
  User Defined Callback Handles Error

NOTE
  This module allows developers to alert themselves to Lambda Switch Errors.
  Code in the handler should be able to send the developer and email with
  stack traces and log locations.
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.LambdaSwitch.cases.Base.bDeployment import BeblsoftLSBaseDeployment


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LS ERROR DEPLOYMENT ---------------------- #
class BeblsoftLSErrorDeployment(BeblsoftLSBaseDeployment):
    """
    Beblsoft LS Error Event
    """

    def __init__(self, bLSDeployment, funcStr):
        """
        Initialize Object
        Args
          bLSDeployment:
            Beblsoft Lambda Switch Deployment
          funcStr:
            Function to execute scheduled event
            Must be in bLambdaPackage
            Must have prototpye: def handle(eType, event, context)
            Ex. "smcekn.server.aws.error:handleError"

        FuncStr Args:
          eType:
            Error Type
            Ex. BeblsoftLSErrorType.EXCEPTION
          event:
            AWS Event Object
          context:
            AWS context object

         FuncStr Rval:
           Not processed

         Note:
           If error is due to exception. sys.exc_info() will hold exception
        """
        super().__init__(bLSDeployment)
        self.funcStr              = funcStr

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)


    # ----------------------- UPDATE SETTINGS DICT -------------------------- #
    def updateSettingsDict(self, settingsDict):
        """
        Update settings dict
        """
        settingsDict.update({
            "ERROR_FUNCSTR": "{}".format(self.funcStr),
        })
        return settingsDict
