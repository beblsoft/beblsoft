#!/usr/bin/env python3
"""
NAME:
 bTimeoutThread.py

DESCRIPTION
 Beblsoft Lambda Switch Timeout Thread Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import threading
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Error.bHandler import BeblsoftLSErrorType


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT LSH TIMEOUT THREAD ---------------------- #
class BeblsoftLSHTimeoutThread(threading.Thread):
    """
    Beblsoft Lambda Switch Handler Timeout Thread

    Thread periodically checks time remaining and invokes ErrorHandler
    if too little time is left
    """

    def __init__(self, bLSHandler, event, context, reCheckS=.5, criticalDelayMS=2000):
        """
        Initialize object
        Args
          bLSHandler:
            Beblsoft Lambda Switch Handler object
          event:
            AWS event
          context:
            AWS context
          reCheckS:
            Amount of time (in seconds) thread should wait before retrying
            Ex. .5
          criticalDelayMS:
            Critical amount of time remaining below which the Error handler will be invoked
            Ex. 2000
        """
        threading.Thread.__init__(self, group=None, name="BLSHTimeout")
        self.bLSHandler      = bLSHandler
        self.event           = event
        self.context         = context
        self.reCheckS        = reCheckS
        self.criticalDelayMS = criticalDelayMS
        self.stopEvent       = self.bLSHandler.stopEvent
        self.handledError    = False
        self.bErrorHandler   = bLSHandler.bErrorHandler

    # ------------------------ RUN ------------------------------------------ #
    def run(self):
        """
        Run thread
        """
        while not self.stopEvent.is_set():
            self.stopEvent.wait(timeout=self.reCheckS)
            self.checkTime()

    @logFunc()
    def checkTime(self):
        """
        Check time
        """
        if not self.handledError:
            timeRemainingMS = self.context.get_remaining_time_in_millis()

            if timeRemainingMS < self.criticalDelayMS:
                logger.error("Little Time Left: reminingMS={} criticalDelayMS={}".format(
                    timeRemainingMS, self.criticalDelayMS))
                self.bErrorHandler.handleEvent(eType=BeblsoftLSErrorType.TIMEOUT,
                                               event=self.event, context=self.context)
                self.handledError = True
