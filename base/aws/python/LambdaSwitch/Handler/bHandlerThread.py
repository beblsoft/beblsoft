#!/usr/bin/env python3
"""
NAME:
 bHandlerThread.py

DESCRIPTION
 Beblsoft Lambda Switch Handler Thread Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import threading
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.log.bLogger import BeblsoftLogger
from base.bebl.python.log.bLogCode import BeblsoftLogCode
from base.aws.python.LambdaSwitch.cases.Error.bHandler import BeblsoftLSErrorType


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT LSH HANDLER THREAD ---------------------- #
class BeblsoftLSHHandlerThread(threading.Thread):
    """
    Beblsoft Lambda Switch Handler Timeout Thread

    Thread invokes aws payload
    """

    def __init__(self, bLSHandler, event, context):
        """
        Initialize object
        Args
          bLSHandler:
            Beblsoft Lambda Switch Handler object
          event:
            AWS event
          context:
            AWS context
        """
        threading.Thread.__init__(self, group=None, name="BLSHHandler")
        self.bLSHandler     = bLSHandler
        self.bHandlers      = bLSHandler.bHandlers
        self.event          = event
        self.context        = context
        self.bErrorHandler  = bLSHandler.bErrorHandler

    # ------------------------ RUN ------------------------------------------ #
    @logFunc()
    def run(self):
        """
        Handler Thread
        Handle event and update bLSHandler resp and exception fields

        Note:
          Throw an error in the unhandled case...
          Services like SQS detect failure if an error is thrown
        """
        resp      = None
        processed = False
        try:
            # Find and execute handler for this request
            for h in self.bHandlers:
                if h.shouldHandleEvent(event=self.event, context=self.context):
                    h.logEventStart(event=self.event, context=self.context)
                    resp = h.handleEvent(event=self.event, context=self.context)
                    processed = True
                    h.logEventDone(event=self.event, context=self.context, resp=resp)
                    break

        except Exception as e:  # pylint: disable=W0703
            self.handleException(exception=e)
        else:
            if not processed:
                self.handleNotProcessed()
        self.bLSHandler.setResponse(resp)

    # ------------------------ HANDLE EXCEPTION ----------------------------- #
    @logFunc()
    def handleException(self, exception):
        """
        Handle Exception
        Exception occured in the handler thread
        """
        BeblsoftLogger.exception(bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_EXCEPTION,
                                 event=self.event, context=self.context)
        self.bLSHandler.setException(exception)
        self.bErrorHandler.handleEvent(eType=BeblsoftLSErrorType.EXCEPTION,
                                       event=self.event, context=self.context)

    # ------------------------ HANDLE NOT PROCESSED ------------------------- #
    @logFunc()
    def handleNotProcessed(self):
        """
        Handle Not Processed
        None of the handlers processed the event
        Thrown and error
        """
        BeblsoftLogger.warning(bLogCode=BeblsoftLogCode.LAMBDA_SWITCH_EVENT_NOT_HANDLED,
                               event=self.event, context=self.context)
        self.bErrorHandler.handleEvent(eType=BeblsoftLSErrorType.EVENT_NOT_PROCESSED)
        exception = BeblsoftError(code=BeblsoftErrorCode.AWS_LAMBDA_INVOCATION_UNHANDLED)
        self.bLSHandler.setException(exception)
