#!/usr/bin/env python3
"""
NAME:
 bMemoryThread.py

DESCRIPTION
 Beblsoft Lambda Switch Memory Thread Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import threading
import logging
import os
import psutil
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.LambdaSwitch.cases.Error.bHandler import BeblsoftLSErrorType


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT LSH MEMORY THREAD ----------------------- #
class BeblsoftLSHMemoryThread(threading.Thread):
    """
    Beblsoft Lambda Switch Handler Memory Thread

    Thread periodically checks memory usage and invokes ErrorHandler if too much
    memory is used
    """

    def __init__(self, bLSHandler, event, context, reCheckS=.5, memCriticalMB=400):
        """
        Initialize object
        Args
          bLSHandler:
            Beblsoft Lambda Switch Handler object
          event:
            AWS event
          context:
            AWS context
          reCheckS:
            Amount of time (in seconds) thread should wait before retrying
            Ex. .5
          memCriticalMB:
            Critical amount of memory remaining below which the Error handler will be invoked
            Ex. 400
        """
        threading.Thread.__init__(self, group=None, name="BLSHMemory")
        self.bLSHandler    = bLSHandler
        self.event         = event
        self.context       = context
        self.reCheckS      = reCheckS
        self.memCriticalMB = memCriticalMB
        self.stopEvent     = self.bLSHandler.stopEvent
        self.handledError  = False
        self.bErrorHandler = bLSHandler.bErrorHandler

    # ------------------------ RUN ------------------------------------------ #
    def run(self):
        """
        Run thread
        """
        while not self.stopEvent.is_set():
            self.stopEvent.wait(timeout=self.reCheckS)
            self.checkMemory()

    @logFunc()
    def checkMemory(self):
        """
        Check that there is enough memory to continue

        Using RSS instead of VMS because in actual trial run:
          - aws lambda reported max memory : 139 MB
          - rss reported                   : 96.42 MB
          - vms reported                   : 586.70 MB
        """
        if not self.handledError:
            p          = psutil.Process(os.getpid())
            memLimitMB = int(self.context.memory_limit_in_mb)
            memUsedMB  = int(p.memory_info().rss) / 1024 / 1024
            memLeftMB  = memLimitMB - memUsedMB

            if memLeftMB < self.memCriticalMB:
                logger.error("Low memory: limitMB={} usedMB={} leftMB={}".format(
                    memLimitMB, memUsedMB, memLeftMB))
                self.bErrorHandler.handleEvent(eType=BeblsoftLSErrorType.OUT_OF_MEMORY,
                                               event=self.event, context=self.context)
                self.handledError = True
