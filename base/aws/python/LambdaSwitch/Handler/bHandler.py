#!/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
 Beblsoft Lambda Switch Handler Functionality

BACKGROUND
  LAMBDA EVENTS: https://docs.aws.amazon.com/lambda/latest/dg/invoking-lambda-function.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import threading
import importlib
from base.aws.python.LambdaSwitch.Handler.bMemoryThread import BeblsoftLSHMemoryThread
from base.aws.python.LambdaSwitch.Handler.bTimeoutThread import BeblsoftLSHTimeoutThread
from base.aws.python.LambdaSwitch.Handler.bHandlerThread import BeblsoftLSHHandlerThread
from base.aws.python.LambdaSwitch.cases.Error.bHandler import BeblsoftLSErrorHandler
from base.aws.python.LambdaSwitch.cases.Flask.bHandler import BeblsoftLSFlaskHandler
from base.aws.python.LambdaSwitch.cases.ScheduledEvent.bHandler import BeblsoftLSScheduledEventHandler
from base.aws.python.LambdaSwitch.cases.SESConfigurationSetEvent.bHandler import BeblsoftLSSESConfigEventHandler
from base.aws.python.LambdaSwitch.cases.SESReceiptRule.bHandler import BeblsoftLSSESReceiptRuleHandler
from base.aws.python.LambdaSwitch.cases.Invoke.bHandler import BeblsoftLSInvokeHandler
from base.aws.python.LambdaSwitch.cases.SQS.bHandler import BeblsoftLSSQSHandler
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ BEBLSOFT LS HANDLER ------------------------------ #
class BeblsoftLSHandler():
    """
    Beblsoft Lambda Switch Handler
    """
    handlers = [BeblsoftLSFlaskHandler,
                BeblsoftLSScheduledEventHandler,
                BeblsoftLSSESConfigEventHandler,
                BeblsoftLSSESReceiptRuleHandler,
                BeblsoftLSInvokeHandler,
                BeblsoftLSSQSHandler]

    def __init__(self):
        """
        Initialize object
        """
        self.settingsName   = "bSettings"
        self.settings       = importlib.import_module(self.settingsName)
        self.logLevel       = None
        self.bHandlers      = None
        self.stopEvent      = None
        self.exception      = None
        self.resp           = None
        self.initLogging()
        self.initHandlers()
        self.initExecutionState()

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------------ INIT STATE ----------------------------------- #
    @logFunc()
    def initLogging(self):
        """
        Initialize logging
        """
        # Custom Log Formatting - Useful to discern where log messages are comming from
        # logFormat = "%(asctime)s - %(name)s - %(levelno)s - %(module)s - %(message)s"
        # for h in logger.handlers:
        #     h.setFormatter(logging.Formatter(logFormat))
        self.logLevel = int(self.settings.GLOBAL_LOGLEVEL)
        logger.setLevel(self.logLevel)

    @logFunc()
    def initHandlers(self):
        """
        Initialize handlers
        """
        self.bHandlers      = []
        for hCls in BeblsoftLSHandler.handlers:
            hInst = hCls(self.settings)
            if (hInst.enabled):
                self.bHandlers.append(hInst)
        self.bErrorHandler  = BeblsoftLSErrorHandler(self.settings)

    @logFunc()
    def initExecutionState(self):
        """
        Reset execution state
        """
        self.stopEvent  = threading.Event()  # Signal to threads to stop
        self.exception  = None
        self.resp       = None

    # ------------------------ SETTERS -------------------------------------- #
    def setResponse(self, resp):
        """
        Hook for handler thread to set response
        """
        self.resp = resp

    def setException(self, exception):
        """
        Hook for handler thread to set exception
        """
        self.exception = exception

    # ------------------------ MAIN THREAD ---------------------------------- #
    @logFunc()
    def mainThread(self, event, context):
        """
        Main thread
        Args
          event:
            AWS Event that triggered lambda
            https://docs.aws.amazon.com/lambda/latest/dg/eventsources.html
          context:
            Lambda context
            https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

        Main thread does the following
          - Spawns handler thread to handle work
          - Spawns timeoutThead to detect timeouts
          - Spawns memoryThread to detect memory overusage
          - Waits for handler thread to complete
          - Stops other threads
        """
        self.initExecutionState()
        handlerThread = BeblsoftLSHHandlerThread(
            bLSHandler     = self,
            event          = event,
            context        = context)
        timeoutThread = BeblsoftLSHTimeoutThread(
            bLSHandler     = self,
            event          = event,
            context        = context)
        memoryThread  = BeblsoftLSHMemoryThread(
            bLSHandler     = self,
            event          = event,
            context        = context)

        # Start Threads
        handlerThread.start()
        timeoutThread.start()
        memoryThread.start()

        # Wait for handler and then stop others
        handlerThread.join()
        self.stopEvent.set()
        timeoutThread.join()
        memoryThread.join()

        # Return
        if self.exception:
            raise self.exception  # pylint: disable=E0702
        return self.resp


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()  # Root Logger
lh     = BeblsoftLSHandler()


# ------------------------ MAIN ENTRY POINT --------------------------------- #
def handler(event, context):
    """
    Handle AWS Event
    """
    return lh.mainThread(event, context)
