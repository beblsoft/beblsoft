OVERVIEW
===============================================================================
AWS Comprehend Technology Documentation
- Description                       : Comprehend is a natural language processing (NLP) service that uses
                                      machine learning to find insights and relationships in text
                                    : Processes any text file in UTF-8 format
- Document Insights -----------------
  * Entities                        : People, places, and locations identified in a document
  * Key Phrases                     : Extract key phrases in a document
  * Language                        : Detect dominant language in document
  * Sentiment                       : Emotional sentiment of a document: positive, neutral, negative, or mixed
  * Syntax                          : Part of speech for each word in document
  * Custom Classification           : Train models, then send documents through classifier
  * Custom Entity Recognition       : Train models, then send documents through entity recognizer
  * Topic Modeling                  : Have comprehend analyze thousands of documents and find common clusters
- Supported Languages ---------------
  * English
  * Spanish
  * French
  * German
  * Italian
  * Portuguese
- URLS ------------------------------
 * Home                             : https://aws.amazon.com/comprehend/
 * Documentation                    : https://docs.aws.amazon.com/comprehend/latest/dg/what-is.html



DOCUMENT PROCESSING
===============================================================================
- Processing modes
  * Single-Document Processing      : Single doc, single response, synchronous
  * Multiple Synchronous Processing : Multiple documents (up to 25), synchronous
  * Asynchronous Batch Processing   : Multiple documents in S3 bucket processed asynchronously
