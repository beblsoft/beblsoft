#!/usr/bin/env python3.6
"""
NAME:
 bClient.py

DESCRIPTION
 Beblsoft Comprehend Client
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.language.bLanguage import BeblsoftLanguage
from base.bebl.python.language.bType import BeblsoftLanguageType


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT COMPREHEND CLIENT ------------------------ #
class BeblsoftComprehendClient():
    """
    Beblsoft Comprehend Client
    """

    def __init__(self, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        """
        self.compClient = boto3.client("comprehend", region_name=bRegion.name)

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ----------------------- VERIFICATION ---------------------------------- #
    def verifyTextLen(self, text, minLen=1, maxLen=5000):  # pylint: disable=R0201
        """
        Verify text length
        Args:
          text:
            Text String to be analyzed
          minLen:
            Minimum length of text
          maxLen:
            Maxiumum length of text
        """
        textLen = len(text)
        if textLen < minLen:
            raise BeblsoftError(code=BeblsoftErrorCode.COMPREHEND_TEXT_TOO_SMALL,
                                msg="TextLen={} MinLen={}".format(textLen, minLen))
        elif textLen > maxLen:
            raise BeblsoftError(code=BeblsoftErrorCode.COMPREHEND_TEXT_TOO_BIG,
                                msg="TextLen={} MaxLen={}".format(textLen, maxLen))

    # ----------------------- DOMINANT LANGUAGE ----------------------------- #
    @logFunc()
    def detectDominantLanguage(self, text, minLen=1, maxLen=5000):
        """
        Detect Dominant Language in Text
        Args
          text
            UTF-8 text string
            Ex. "What a beautiful day it is!"

        Reference
          Language Codes : https://tools.ietf.org/html/rfc5646#section-2

        Return from detect_dominant_language
          [{
            'LanguageCode': 'string',  # RFC 5646 language code for the dominant language
            'Score': ...               # Level of confidence that Amazon Comprehend has in accuracy of detection
           }, ...]

        Return from function
          [{
            'bLanguage': BeblsoftLanguage, # Beblsoft Language
            'Score': ...                   # Level of confidence that Amazon Comprehend has in accuracy of detection
           }, ...]
        """
        self.verifyTextLen(text, minLen, maxLen)
        resp         = self.compClient.detect_dominant_language(Text=text)
        languageList = resp.get("Languages")
        for language in languageList:
            code = language.pop("LanguageCode")
            language["bLanguage"] = BeblsoftLanguage.fromCode(code=code)
        return languageList

    # ----------------------- SENTIMENT ------------------------------------- #
    detectSentimentSupportedBLanguageTypeList = [
        BeblsoftLanguageType.English,
        BeblsoftLanguageType.Spanish,
        BeblsoftLanguageType.French,
        BeblsoftLanguageType.Dutch,
        BeblsoftLanguageType.Italian,
        BeblsoftLanguageType.Portuguese,
    ]

    @logFunc()
    def detectSentiment(self, text, bLanguage, minLen=1, maxLen=5000):
        """
        Detect Sentiment in Text
        Args
          text:
            UTF-8 text string
            Max bytes = 4,999
            Ex. "What a beautiful day it is!"
          bLanguage:
            Beblsoft Language

        Returns
          {                          # Dominant sentiment in text
            'Sentiment': 'POSITIVE'|'NEGATIVE'|'NEUTRAL'|'MIXED',
            'SentimentScore': {      # Scores for each sentiment
              'Positive': ...,
              'Negative': ...,
              'Neutral': ...,
              'Mixed': ...
            }
          }
        """
        self.verifyTextLen(text, minLen, maxLen)
        resp = self.compClient.detect_sentiment(Text=text, LanguageCode=bLanguage.twoLetterCode)
        resp.pop("ResponseMetadata")
        return resp

    # ----------------------- ENTITIES -------------------------------------- #
    @logFunc()
    def detectEntities(self, text, bLanguage, minLen=1, maxLen=5000):
        """
        Detect Sentiment in Text

        Returns
          [{
            'Score': ...,        # AWS Confidence
                                 # Entity Type
            'Type': 'PERSON'|'LOCATION'|'ORGANIZATION'|'COMMERCIAL_ITEM'|'EVENT'|'DATE'|'QUANTITY'|'TITLE'|'OTHER',
                                 # COMMERCIAL_ITEM - Branded product
                                 # DATE            - Full date (for example, 11/25/2017), day (Tuesday), month (May), or time (8:30 a.m.)
                                 # EVENT           - An event, such as a festival, concert, election, etc.
                                 # LOCATION        - A specific location, such as a country, city, lake, building, etc.
                                 # ORGANIZATION    - Large organizations, such as a government, company, religion, sports team, etc.
                                 # OTHER           - Entities that don't fit into any of the other entity categories
                                 # PERSON          - Individuals, groups of people, nicknames, fictional characters
                                 # QUANTITY        - A quantified amount, such as currency, percentages, numbers, bytes, etc.
                                 # TITLE           - An official name given to any creation or creative work, such as movies, books, songs, etc.
            'Text': 'string',    # Entity source text
            'BeginOffset': 123,  # Zero-based offset from beginning of text to first character
            'EndOffset': 123     # Zero-based offset from beginning of text to last character
          }, ...]
        """
        self.verifyTextLen(text, minLen, maxLen)
        resp = self.compClient.detect_entities(Text=text, LanguageCode=bLanguage.twoLetterCode)
        return resp.get("Entities")

    # ----------------------- SYNTAX ---------------------------------------- #
    @logFunc()
    def detectSyntax(self, text, bLanguage, minLen=1, maxLen=5000):
        """
        Detect Syntax in Text

        Returns
          [{
            'TokenId': 123,     # Unique ID of token
            'Text': 'string',   # Work in source text
            'BeginOffset': 123, # Zero-based offset from beginning of text to first character
            'EndOffset': 123,   # Zero-based offset from beginning of text to last character
            'PartOfSpeech': {
              'Tag': 'ADJ'|'ADP'|'ADV'|'AUX'|'CONJ'|'CCONJ'|'DET'|'INTJ'|'NOUN'|'NUM'|'O'|'PART'|'PRON'|'PROPN'|'PUNCT'|'SCONJ'|'SYM'|'VERB',
                                # ADJ   - Adjective, words that modify nouns
                                # ADP   - Adposition
                                # ADV   - Adverb
                                # AUX   - Auxiliary
                                # CCONJ - Coordinating conjunction
                                # DET   - Determiner
                                # INJ   - Interjection
                                # NOUN  - Noun
                                # NUM   - Numeral
                                # O     - Other
                                # PART  - Particle
                                # PRON  - Pronoun
                                # PROPN - Proper Noun
                                # PUNCT - Punctuation
                                # SCONJ - Subordinating conjunction
                                # SYM   - Symbol
                                # VERB  - Verb
              'Score': ...      # AWS Confidence
            }
          }, ...]
        """
        self.verifyTextLen(text, minLen, maxLen)
        resp = self.compClient.detect_syntax(Text=text, LanguageCode=bLanguage.twoLetterCode)
        return resp.get("SyntaxTokens")

    # ----------------------- KEY PHRASES ----------------------------------- #
    @logFunc()
    def detectKeyPhrases(self, text, bLanguage, minLen=1, maxLen=5000):
        """
        Detect Key Phrases in Text

        A key phrase is a string containing a noun phrase that describes a particular thing.

        Returns
          [{
            'Score': ...,          # AWS Confidence Score
            'Text': 'string',      # Text of key noun phrase
            'BeginOffset': 123,    # Zero-based offset from beginning of text to first character
            'EndOffset': 123       # Zero-based offset from beginning of text to last character
          }, ...]
        """
        self.verifyTextLen(text, minLen, maxLen)
        resp = self.compClient.detect_key_phrases(Text=text, LanguageCode=bLanguage.twoLetterCode)
        return resp.get("KeyPhrases")
