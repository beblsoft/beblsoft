#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Comprehend Samples
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import pprint
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.aws.python.Comprehend.bClient import BeblsoftComprehendClient
from base.bebl.python.language.bLanguage import BeblsoftLanguage
from base.bebl.python.language.bType import BeblsoftLanguageType


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Comprehend ------------------------------
        self.bComprehend       = BeblsoftComprehendClient()

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/comprehendSample.log", type=click.Path(),
              help="Specify log file Default=/tmp/comprehendSample.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Comprehend Samples
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- DETECT DOMINANT LANGUAGE -------------------------- #
@cli.command()
def detect_dominant_language():
    """
    Detect Dominant Language

    Languages Translated From https://translatr.varunmalhotra.xyz/
    """
    languageList = [
        {"locale": "en", "country": "English", "string": "2034983450983245aasdsad"},
        {"locale": "ar", "country": "Arabic", "string": "الشمس تسطع في هذا اليوم الجميل!"},
        {"locale": "de", "country": "German", "string": "Die Sonne scheint an diesem schönen Tag!"},
        {"locale": "el", "country": "Greek", "string": "Ο ήλιος λάμπει σε αυτή την όμορφη μέρα!"},
        {"locale": "en", "country": "English", "string": "The sun is shining on this beautiful day!"},
        {"locale": "ja", "country": "Japanese", "string": "太陽はこの美しい日に輝いています！"},
        {"locale": "nl", "country": "Dutch", "string": "De zon schijnt op deze mooie dag!"},
        {"locale": "no", "country": "Norwegian", "string": "Solen skinner på denne vakre dagen!"},
        {"locale": "pl", "country": "Polish", "string": "W ten piękny dzień świeci słońce!"},
        {"locale": "pt", "country": "Portuguese", "string": "O sol está brilhando neste lindo dia!"},
        {"locale": "ro", "country": "Romanian", "string": "Soarele strălucește în această zi frumoasă!"},
        {"locale": "ru", "country": "Russian", "string": "Солнце сияет в этот прекрасный день!"},
        {"locale": "sv", "country": "Swedish", "string": "Solen skiner på denna vackra dag!"},
        {"locale": "vi", "country": "Vietnamese", "string": "Mặt trời chiếu sáng vào ngày đẹp trời này!"},
        {"locale": "zh", "country": "Chinese", "string": "在这美好的一天，阳光灿烂！"},
    ]
    for langObj in languageList:
        resp = gc.bComprehend.detectDominantLanguage(text=langObj.get("string"))
        logger.info("local={} resp={}".format(langObj.get("locale"), resp))


# ----------------------- DETECT SENTIMENT ---------------------------------- #
@cli.command()
def detect_sentiment():
    """
    Detect Sentiment
    """
    neutralTextList = [
        "I",
        "FAIRBANKS, Alaska — It is the last great stretch of nothingness in the United States, a vast landscape of mosses, sedges and shrubs that is home to migrating caribou and the winter dens of polar bears. Aside from a Native village at its northern tip, civilization has not dented its 19 million acres, an area the size of South Carolina. There are no roads and no visitors beyond the occasional hunter and backpacker. But the Arctic National Wildlife Refuge — a federally protected place of austere beauty that during a recent flyover was painted white by heavy snowfall — is on the cusp of major change. The biggest untapped onshore trove of oil in North America is believed to lie beneath the refuge’s coastal plain along the Beaufort Sea. For more than a generation, opposition to drilling has left the refuge largely unscathed, but now the Trump administration, working with Republicans in Congress and an influential and wealthy Alaska Native corporation, is clearing the way for oil exploration along the coast."
    ]
    positiveTextList = [
        "Hello World!",
        "She lifted the edge of the picnic blanket and watched as the now free lady bird buzzed of into the nearby flowerbeds. After being assured that there were no more unfortunate guests beneath her, she put down her basket and sat down to soak up the needed rays of sunshine. The smell of sweet jasmine brought her back from her trance and she pulled off her glasses to assemble her herbal tea and mini blueberry pies. From above she could be mistaken for an angle with her halo of golden blonde ringlets surrounding her fine cheeks and unique eyes. Her face was soft, her lips thin and wide were frozen in a forever smile. Usually the first thing people noticed about her was the orange-brown patch in her milky green eye and her perfect complexion, they would assume she was a stereotypical cold hearted mean girl but if you got to know her she was a lot more than that. She was very empathic, a sweet girl the kind that one would imagine to fall out of movie, almost unreal but completely genuine. She finished her first cup of tea and observed the bird’s flutter from the old oak tree in the middle of the park to the ground below. After an hour of observation the bird’s flew off, and she decided it was time to go as well. After packing her picnic, her phone began to ring which surprised her since she mostly liked to keep to herself and prefered talking to people in person. She answered the call and realised it was her mother, who had decided to drop in for a surprise visit. Happy to hear the news, she pulled the keys out from her pocket gathered her basket and made her way out of the park to her apartment that lived just outside its boundaries."
    ]
    negativeTextList = [
        "I dislike you.",
        "Glancing up at the half broken clock on the kitchen wall i knew it was late. I slammed the lid of my computer down, instantly regretting it and lifted it back up to see if it broke. The crack on the top right corner looked a little worse which made my gloomy mood even darker. I got up to assemble my dinner – Dry bread and homemade cheese. After searching the fridge extensively i remembered that i had finished the remaining cheese for breakfast. Sighing i poured myself a glass of water and mentally prepared myself for the trip down to the basement – i hated it down there. I convinced myself that it was full off rodents, so i set rat traps which never caught anything but still went off with a loud snap at the most inconvenient times. The creaking door stubbornly refused to open, so i pulled on it with all my weight almost pulling off the latch in the process. With the small, dull flashlight gripped tightly between my teeth i slowly went down the freezing concrete stairs. It was a lot colder down here. Before fully descending, I double checked the room with the flashlight to make sure no monsters were waiting for me in the dark. As soon as the thought entered my head it wouldn’t get out. I felt my heart speed up as adrenaline filled my body. Cursing myself in my head i ran over to the shelf and grabbed a jar of cheese. This only provoked me further since the sudden sound of my bare feet slapping the ground spooked me so much i almost stepped onto one of the rat traps. I made a dash for it and jogged up the stairs as quickly as i could, slamming the door shut behind me. After a halfhearted dinner i made my way to bed, it was very late and i needed to wake up early for work tomorrow. Upon glancing at my reflection i realised i forgot to take a shower. My bright blonde hair was now darkened and the bags under my eyes looked worse than ever. I sighed as i flopped onto my bed and set my alarm clock to 4am, 1 hour earlier than usual so that i could get a quick shower in the morning.  My head touched the pillow and i went out like a light."
    ]
    bLanguage = BeblsoftLanguage.fromType(bLanguageType=BeblsoftLanguageType.English)
    for text in neutralTextList + positiveTextList + negativeTextList:
        resp = gc.bComprehend.detectSentiment(text=text, bLanguage=bLanguage)

        logger.info("Analysis Text={} Len={}".format(text, len(text)))
        logger.info(pprint.pformat(resp))


# ----------------------- DETECT ENTITIES ----------------------------------- #
@cli.command()
def detect_entities():
    """
    Detect Entities
    """
    textList = [
        "Mr Bush, who served as the 41st US president between 1989 and 1993, died late on Friday at the age of 94. Sully the dog will be travelling with the casket on the flight from Texas to Washington DC on Monday. Mr Bush's body is due to lie in state this week ahead of a day of national mourning. The coffin will be flown from Texas to DC on board Air Force One - temporarily renamed Special Air Mission 41, in homage to the late president - and then back on Wednesday, with Sully accompanying the body throughout.",
        "Down two scores and with about nine minutes remaining, the Vikings picked up what looked like a fourth-down conversion—but it was close. Minnesota hurried to the line to try to get the next play off before the Patriots could decide whether to challenge the ruling, but safety Patrick Chung dropped to the ground with an injury. Excellent timing! That gave New England time to challenge the play (and Bill Belichick might’ve needed every second, given that he forgot which sock his challenge flag was in), but Vikings receiver Adam Thielen decided to wander over to the Patriots sideline to tell Belichick exactly what he thought of Chung’s convenient owie."
    ]
    for text in textList:
        bLanguage = BeblsoftLanguage.fromType(bLanguageType=BeblsoftLanguageType.English)
        resp = gc.bComprehend.detectEntities(text=text, bLanguage=bLanguage)
        logger.info("Analysis Text={} Len={}".format(text, len(text)))
        logger.info(pprint.pformat(resp))


# ----------------------- DETECT SYNTAX ------------------------------------- #
@cli.command()
def detect_syntax():
    """
    Detect Syntax
    """
    textList = [
        "U.S. stock futures were posting substantial gains on Monday, Dec. 3, after Donald Trump proclaimed a 90-day \"truce\" in the still-simmering trade war between Washington and Beijing following this weekend's G-20 Summit in Argentina."
    ]
    bLanguage = BeblsoftLanguage.fromType(bLanguageType=BeblsoftLanguageType.English)
    for text in textList:
        resp = gc.bComprehend.detectSyntax(text=text, bLanguage=bLanguage)
        logger.info("Analysis Text={} Len={}".format(text, len(text)))
        logger.info(pprint.pformat(resp))


# ----------------------- DETECT KEY PHRASES -------------------------------- #
@cli.command()
def detect_key_phrases():
    """
    Detect Key Phrases
    """
    textList = [
        "U.S. stock futures were posting substantial gains on Monday, Dec. 3, after Donald Trump proclaimed a 90-day \"truce\" in the still-simmering trade war between Washington and Beijing following this weekend's G-20 Summit in Argentina."
    ]
    bLanguage = BeblsoftLanguage.fromType(bLanguageType=BeblsoftLanguageType.English)
    for text in textList:
        resp = gc.bComprehend.detectKeyPhrases(text=text,bLanguage=bLanguage)
        logger.info("Analysis Text={} Len={}".format(text, len(text)))
        logger.info(pprint.pformat(resp))


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
