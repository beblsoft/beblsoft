OVERVIEW
===============================================================================
Comprehend Samples Documentation



PROJECT: SAMPLES
===============================================================================
- Description                       : Comprehend Samples
- Directory                         : ./samples
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Commands --------------------------
  * Detect Dominant Language        : ./cli.py detect-dominant-language
  * Detect Sentiment                : ./cli.py detect-sentiment
  * Detect Entities                 : ./cli.py detect-entities
  * Detect Syntax                   : ./cli.py detect-syntax
  * Detect Key Phrases              : ./cli.py detect-key-phrases
