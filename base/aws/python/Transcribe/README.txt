OVERVIEW
===============================================================================
AWS Transcribe Technology Documentation
- Description                       : Amazon Transcribe is an automatic speech recognition (ASR) service
                                      that makes it easy for developers to add speech-to-text capability to their
                                      applications
- Features --------------------------
  * Easy-to-Read Transcriptions     : Outputs string text with punctuation
  * Timestamp Generation            : Timestamp for each word. Easily locate the audio in the original recording
  * Custom Vocabulary               : Expand and customize the speech recognition vocabulary
  * Recognize multiple speakers
  * Channel Identification          : Detect where each speaker is recorded on different channels
  * Streaming Transcription         : Transcribe in real time

- URLS ------------------------------
 * Home                             : https://aws.amazon.com/transcribe/
 * Documentation                    : https://docs.aws.amazon.com/transcribe/latest/dg/what-is-transcribe.html
