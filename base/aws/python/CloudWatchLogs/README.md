# CloudWatch Logs Documentation

CloudWatch Logs enables you to centralize the logs from all your systems, applications, and AWS
services that you use, in a single, highly scalable service. You can easily view them, search
them for specific error codes or patterns, filter them based on specific fields, or archive them
securely for future analysis.

CloudWatch Logs enables you to see all of your logs, regardless of their source, as a single and
consistent flow of events ordered by time, and you can query them and sort them based on other
dimensions, group them by specific fields, create custom computations with a powerful query language,
and visualize log data in dashboards.

Relevant URLs:
[AWS Documentation](https://docs.aws.amazon.com/en_pv/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html),
[Pricing](https://aws.amazon.com/cloudwatch/pricing/)

## Features

- Monitor logs from EC2 instances, AWS lambda functions, and other services
- Log Retention: by default, logs are kept indefinitely and never expire
- Archive Log Data: Store data in highly durable storage

## Insights

CloudWatch Logs Insights enables you to interactively search and analyze your log data in Amazon
CloudWatch Logs. You can perform queries to help you quickly and effectively respond to operational
issues. If an issue occurs, you can use Insights to identify potential causes and validate deployed
fixes.

Insights includes a purpose-built query language with a few simple but powerful commands.

Insights automatically discovers fields in logs from AWS services such as Route 53, Lambda, CloudTrail,
VPC, and any application or custom log that emits log events as JSON.

A single request can query up to 20 log groups.

### Supported Logs and Discovered Fields

Insights supports all types of logs. For every log sent to CloudWatch logs, three system fields are
automatically generated:

- `@message` contains the raw unparsed log event

- `@timestamp` contains the time when the log event was added to the CloudWatch Logs

- `@logStream` contains the name of the log stream that the log event was added to

For other types of logs with fields that Insights doesn't automatically discover, use the __parse__
command to extract and create ephemeral fields for use in that query.

If the name of a discovered log field starts with the `@` character, Insights displays it with an
additional `@` appended to the beginning. For example, if a log field name is `@example.com`,
this field name is displayed as `@@example.com`.

#### JSON Log Fields

Insights represents nested JSON fields using the dot notation. In the following JSON event,
the field `type` in the JSON object `userItentity` is represented as `userIdentity.type`.

JSON arrays are flattened into a list of field names and values. For example, to specify the value
of `instanceId` for the first item in `requestParameters.instancesSet`, use
`requestParameters.instancesSet.items.0.instanceId`.

```json
{
    "eventVersion": "1.0",
    "userIdentity":
    {
        "type": "IAMUser",
        "principalId": "EX_PRINCIPAL_ID",
        "arn": "arn: aws: iam: : 123456789012: user/Alice",
        "accessKeyId": "EXAMPLE_KEY_ID",
        "accountId": "123456789012",
        "userName": "Alice"
    },
    "eventTime": "2014-03-06T21: 22: 54Z",
    "eventSource": "ec2.amazonaws.com",
    "eventName": "StartInstances",
    "awsRegion": "us-east-2",
    "sourceIPAddress": "205.251.233.176",
    "userAgent": "ec2-api-tools1.6.12.2",
    "requestParameters":
    {
        "instancesSet":
        {
            "items": [
            {
                "instanceId": "i-abcde123"
            }]
        }
    },
}
```

### Query Syntax

Insights supports a query language you can use to perform queries on log groups.
Each query can include one or more query commands separated by Unix-style pipe characters (`|`).

Size query commands are supported, along with many supporting functions and operations, including
regular expressions, arithmetic operations, comparison operations, numeric functions, datetime
functions, string functions, and generic functions.

Comments are also supported. Lines in a query that start with the `#` character are ignored.

#### Command: fields

Retrieves the specifed fields from log events. You can use functions and operations within a fields
command.

```
fields `foo-bar`, action, abs(f3-f4)
```
retrieves the fields foo-bar, action, and the absolute
value of the difference between f3 and f4 for all log events in the log group.

Note: The backtick symbol \` is necessary around foo-bar because that field name includes a non-alphanumeric
character. Any log field named in a query that has characters other than the @ sign, the period (.),
and alphanumeric characters must be surrounded by backtick characters.

#### Command: filter

Filters the results of a query based on one or more conditions. You can use comparison operators
(`=`, `!=`, `<`, `<=`, `>`, `>=`), Boolean operators (`and`, `or`, and `not`) and regular expressions.

`fields f1, f2, f3 | filter (duration>2000)` retrieves the fields `f1`, `f2`, and `f3` for all log
events with a value over 2000 in the duration field.

`filter (duration>2000)` is also a valid query, but the results don't show separate fields.
Instead, the results show the `@timestamp` and all log data in the `@message` field for all log events
where duration is more than 2000.

`fields f1, f2 | filter (f1=10 or f3>25)` retrieves the fields `f1` and `f2` for all log events where
`f1` is 10 or `f3` is greater than 25.

`fields f1 | filter statusCode like /2\d\d/` returns log events where the field statusCode has a
value between 200 and 299.

You can use `like` or `=~` in the __filter__ query command to filter by substrings or regular expressions.
Enclose your match string with double or single quotation marks to perform substring matching. To
perform regular expression matching, enclose it with forward slashes. The query returns only log events
that match the criteria that you set.

The following threee examples return all events in which `f1` contains the word `Exception`.
```text
fields f1, f2, f3 | filter f1 like /Exception/
fields f1, f2, f3 | filter f1 =~ /Exception/
fields f1, f2, f3 | filter f1 like "Exception"
```

The following examples uses a regular expression and returns all events in whcih `f1` contains the
word `Exception`.
```text
fields f1, f2, f3 | filter f1 like /(?i)Exception/
```

The following example uses a regular expression and returns all events in which `f1` is exactly the
word Exception. The query isn't case sensitive.
```text
fields f1, f2, f3 | filter f1 =~ /^(?i)Exception$/
```

#### Command: stats

Calculates aggregate statistics based on the values of log fields. Several statistical operators
are supported, including `sum()`, `avg()`, `count()`, `min()`, and `max()`.

When you use __stats__, you can also use `by` to specify one or more criteria to use to group data
when calculating the statistics.

`stats avg (f1) by f2` calculates the average value of f1 for each unique value of f2.

#### Command: sort

Sorts the retrieved log events. Both ascending (`asc`) and descending (`desc`) order are supported.

`fields f1, f2, f3 | sort f1 desc` retrieves the fields `f1`, `f2`, and `f3` and sorts the returned
events in descending order based on the value of f1.

#### Command: limit

Limits the number of log events returned by the query.

`fields f1, f2 | sort @timestamp desc | limit 25` retrieves the fields `f1` and `f2`, sorts the events
in descending order based on the value of `@timestamp`, and returns the first 25 events by sort
order. In this case the sort order is by timestamp starting with the most recent, so the most recent
25 events are returned.

#### Command: parse

Extracts data from a log field, creating one or more ephemeral fields that you can process further
in the query. __parse__ accepts both glob expressions and regular expressions.

For glob expressions, provide the parse command with a constant string (characters enclosed in either
single or double quotation marks) where each variable piece of text is replaced with an asterisk
(`*`). These are extracted into ephemeral fields and given an alias after the keyword, in positional
order.

Enclose regular expressions in forward slashes (`/`). Within the expression, each part of the matched
string that is to be extracted is enclosed in a named capturing group. An example of a named capturing
group is `(?<name>.*)`, where name is the anem and `.*` is the pattern.

Using the single log line as an example:

```text
25 May 2019 10:24:39,474 [ERROR] {foo=2, bar=data} The error was: DataIntegrityException
```

The following two parse expressions each do the following: the ephemeral fields `level`, `config`,
and `exception` are extracted. `level` has a value of `ERROR`, `config` has a falue of `{foo=2, bar=data}`,
and `exception` has a value of `DataIntegrityException`. The first expression uses a glob expression,
and the second uses a regular expression.

```text
parse @message "[*] * The error was: *" as level, config, exception
```

```text
parse @message /\[(?<level>\S+)\]\s+(?<config>\{.*\})\s+The error was: (?<exception>\S+)/
```

#### Query Aliases

You can use `as` to create one or more aliases in a query. Aliases are supported in the __fields__,
__stats__, and __sort__ commands.

You can create aliases for log fields and for the results of operations and functions.

The following returns the absolute value of `myField` as `AbsoluteValueMyField` and also returns the
field `myField2`.

```text
fields abs(myField) as AbsoluteValueMyField, myField2
```

The following calculates the average of values of `f1` as `myAvgF1` and returns them in descending
order by that value.

```text
stats avg(f1) as myAvgF1 | sort myAvgF1 desc
```

#### Query Comments

Comment out lines in a query using the `#` character. Lines that start with the `#` character are ignored.
This can be useful to document your query or to temporarily ignore part of a complex query for one
call, without deleting that line.

```text
fields @timestamp, @message
# | filter @message like /delay/
| limit 20
```

#### Comparison Operators

You can use comparison operations in the __filter__ command and as arguments for other functions.
Comparison operations accept all data types as arguments and return a Boolean result.

```text
= != < <= > >=
```

#### Arithmetic Operations

You can use arithmetic operations in the __filter__ and __fields__ commands. Aruthmetic operations
accept numeric data types as arguments and return numeric results.

| Operation | Description                            |
|:----------|:---------------------------------------|
| a + b     | Addition                               |
| a - b     | Subtraction                            |
| a * b     | Multiplication                         |
| a / b     | Division                               |
| a ^ b     | Exponentiation. 2 ^ 3 returns 8        |
| a % b     | Remainder or modulus. 10 % 3 returns 1 |

#### Numeric Operations

You can use numeric operations in the __filter__ and __fields__ commands and as arguments for other
functions. Numeric operations accept numeric data types as arguments and return numeric results.

| Operation           | Description                                                                  |
|:--------------------|:-----------------------------------------------------------------------------|
| abs(a)              | Absolute value                                                               |
| ceil(a)             | Round to ceiling (the smallest integer that is greater than the value of a). |
| floor(a)            | Round to floor (the largest integer that is smaller than the value of a).    |
| greatest(a,b,... z) | Returns the largest value.                                                   |
| least(a, b, ... z)  | Returns the smallest value.                                                  |
| log(a)              | Natural log                                                                  |
| sqrt(a)             | Square root                                                                  |

#### General Functions

You can use general functions in the __filter__ and __fields__ commands as arguments for other functions.

| Function                                         | Arguments  | Result Type | Description                                     |
|:-------------------------------------------------|:-----------|:------------|:------------------------------------------------|
| ispresent(fieldname)                             | Log field  | Boolean     | Returns true if the field exists.               |
| coalesce(fieldname1, fieldname2, ... fieldnamex) | Log fields | Log field   | Returns the first non-null value from the list. |

#### String Functions

You can use string functions in the __filter__ and __fields__ commands and as arguments for other functions.

| Function                                     | Arguments                                | Result Type | Description                                                                                                                                                                                                                                               |
|:---------------------------------------------|:-----------------------------------------|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| isempty(fieldname)                           | String                                   | Boolean     | Returns true if the field is missing or is an empty string.                                                                                                                                                                                               |
| isblank(fieldname)                           | String                                   | Boolean     | Returns true if the field is missing, an empty string, or contains only white space.                                                                                                                                                                      |
| concat(string1, string2, ... stringz)        | Strings                                  | String      | Concatenates the strings.                                                                                                                                                                                                                                 |
| ltrim(string) or ltrim(string1, string2)     | String                                   | String      | Remove white space from the left of the string. If the function has a second string argument, it removes the characters of string2 from the left of string1. For example, ltrim("xyZfooxyZ","xyZ") returns "fooxyZ".                                      |
| rtrim(string) or rtrim(string1, string2)     | String                                   | String      | Remove white space from the right of the string. If the function has a second string argument, it removes the characters of string2 from the right of string1. For example, rtrim("xyZfooxyZ","xyZ") returns "xyZfoo".                                    |
| trim(string) or trim(string1, string2)       | String                                   | String      | Remove white space from both ends of the string. If the function has a second string argument, it removes the characters of string2 from both sides of string1. For example, trim("xyZfooxyZ","xyZ") returns "foo".                                       |
| strlen(string)                               | String                                   | Number      | Returns the length of the string in Unicode code points.                                                                                                                                                                                                  |
| toupper(string)                              | String                                   | String      | Converts the string to uppercase.                                                                                                                                                                                                                         |
| tolower(string)                              | String                                   | String      | Converts the string to lowercase.                                                                                                                                                                                                                         |
| substr(string1, x), or substr(string1, x, y) | String, number or string, number, number | String      | Returns a substring from the index specified by the number argument to the end of the string. If the function has a second number argument, it contains the length of the substring to be retrieved. For example, substr("xyZfooxyZ",3, 3) returns "foo". |
| replace(string1, string2, string3)           | String, string, string                   | String      | Replaces all instances of string2 in string1 with string3. For example: replace("foo","o","0") returns "f00".                                                                                                                                             |
| strcontains(string1, string2)                | String                                   | Number      | Returns 1 if string1 contains string2 and 0 otherwise.                                                                                                                                                                                                    |

#### Datetime Functions

You can use datetime functions in the __filter__ and __fields__ commands as arguments for other functions.
You can use these functions to create time buckets for queries with aggregate functions.

As part of datetime functions, you can use time periods that consist of a number and then either `m`
for minutes or `h` for hours. For example `10m` is 10 minutes, and `1h` is 1 hour.

| Function             | Arguments         | Result Type | Description                                                                                                                                                      |
|:---------------------|:------------------|:------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| datefloor(a, period) | Timestamp, period | Timestamp   | Truncates the timestamp to the given period. For example, datefloor(@timestamp, 1h) truncates all values of @timestamp to the bottom of the hour.                |
| dateceil(a, period)  | Timestamp, period | Timestamp   | Rounds up the timestamp to the given period and then truncates. For example, dateceil(@timestamp, 1h) truncates all values of @timestamp to the top of the hour. |
| bin(period)          | Period            | Timestamp   | Rounds the value of @timestamp to the given period and then truncates.                                                                                           |

#### IP Address Functions

You can use IP address functions in the __filter__ and __fields__ commands and as arguments for other
functions.

| Function                          | Arguments      | Result Type | Description                                                                                    |
|:----------------------------------|:---------------|:------------|:-----------------------------------------------------------------------------------------------|
| isValidIp(fieldname)              | String         | Boolean     | Returns true if the field is a valid v4 or v6 IP address.                                      |
| isValidIpV4(fieldname)            | String         | Boolean     | Returns true if the field is a valid v4 IP address.                                            |
| isValidIpV6(fieldname)            | String         | Boolean     | Returns true if the field is a valid v4 or v6 IP address.                                      |
| isValidIp(fieldname)              | String         | Boolean     | Returns true if the field is a valid v6 IP address.                                            |
| isIpInSubnet(fieldname, string)   | String, string | Boolean     | Returns true if the field is a valid v4 or v6 IP address within the specified v4 or v6 subnet. |
| isIpv4InSubnet(fieldname, string) | String, string | Boolean     | Returns true if the field is a valid v4 IP address within the specified v4 subnet.             |
| isIpv6InSubnet(fieldname, string) | String, string | Boolean     | Returns true if the field is a valid v6 IP address within the specified v6 subnet.             |

#### Stats Aggregation Functions

You can use aggregation functions in the __stats__ command and as arguments for other functions.

| Function                     | Arguments              | Result Type     | Description                                                                                                                                                                                                                                           |
|:-----------------------------|:-----------------------|:----------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| avg(NumericFieldname)        | Numeric log field      | Number          | The average of the values in the specified field.                                                                                                                                                                                                     |
| count(fieldname) or count(*) | Log field              | Number          | Counts the log records. count(*) counts all records in the log group, while count (fieldname) counts all records that include the specified field name.                                                                                               |
| count_distinct(fieldname)    | Log field              | Number          | Returns the number of unique values for the field. If the field has very high cardinality (contains many unique values), the value returned by count_distinct is just an approximation.                                                               |
| max(fieldname)               | Log field              | Log field value | The maximum of the values for this log field in the queried logs.                                                                                                                                                                                     |
| min(fieldname)               | Log field              | Log field value | The minimum of the values for this log field in the queried logs.                                                                                                                                                                                     |
| pct(fieldname, value)        | Log field value, value | Log field value | A percentile indicates the relative standing of a value in a dataset. For example, pct(@duration, 95) returns the @duration value at which 95 percent of the values of @duration are lower than this value, and 5 percent are higher than this value. |
| stddev(NumericFieldname)     | Numeric log field      | Number          | The standard deviation of the values in the specified field.                                                                                                                                                                                          |
| sum(NumericFieldname)        | Numeric log field      | Number          | The sum of the values in the specified field.                                                                                                                                                                                                         |

#### Stats Non-Aggregation Functions

You can use non-aggregation functions in the __stats__ command and as arguments for other functions.

| Function              | Arguments | Result Type | Description                                                                                             |
|:----------------------|:----------|:------------|:--------------------------------------------------------------------------------------------------------|
| earliest(fieldname)   | Log field | Log field   | Returns the value of fieldName from the log event that has the earliest time stamp in the queried logs. |
| latest(fieldname)     | Log field | Log field   | Returns the value of fieldName from the log event that has the latest time stamp in the queried logs.   |
| sortsFirst(fieldname) | Log field | Log field   | Returns the value of fieldName that sorts first in the queried logs.                                    |
| sortsLast(fieldname)  | Log field | Log field   | Returns the value of fieldName that sorts last in the queried logs.                                     |

### Visualizing Time Series Data

You can use visualizations to identify trends and patterns that occur over time within  your logs.
Insights generates visualizations for all queries with the following characteristics:

- The query contains one or more aggregation functions.
- The query uses the bin() function to group the data by one field.

Insights displays visualizations using line charts and stacked area charts.

#### Examples

```text
stats avg(myfield1) by bin(5m)

stats avg(myfield1), min(myfield2), max(myfield3) by bin(5m)
```

#### Common Mistakes

The following query doesn't generate a visualization because it contains more than one grouping
field. `stats avg(myfield1) by bin(5m), myfield4`

The following query doesn't generate a visualixation because the `bin()` grouping function isn't
used. `stats avg(myfield1) by myfield4`

### Sample Queries

#### Generic

Find the 25 most recently added log events:

```
fields @timestamp, @message | sort @timestamp desc | limit 25
```

Get a list of the number of exceptions per hour:

```
filter @message like /Exception/
| stats count(*) as exceptionCount by bin(1h)
| sort exceptionCount desc
```

Get a list of log events that aren't exceptions:

```
fields @message | filter @message not like /Exception/
```

#### AWS Lambda

Determine the amount of overprovisioned memory:

```
filter @type = "REPORT"
| stats max(@memorySize / 1024 / 1024) as provisonedMemoryMB,
    min(@maxMemoryUsed / 1024 / 1024) as smallestMemoryRequestMB,
    avg(@maxMemoryUsed / 1024 / 1024) as avgMemoryUsedMB,
    max(@maxMemoryUsed / 1024 / 1024) as maxMemoryUsedMB,
    provisonedMemoryMB - maxMemoryUsedMB as overProvisionedMB
```

Create a latency report

```
filter @type = "REPORT" |
stats avg(@duration), max(@duration), min(@duration) by bin(5m)
```

#### Parse Command

Use a glob expression to extract the ephemeral fields `@user`, `@method`, and `@latency` from the
log field `@message` and return the average latency for each unique combination of `@method`
and `@user`:

```
parse @message "user=*, method:*, latency := *" as @user,
@method, @latency | stats avg(@latency) by @method,
@user
```

### Add Query to Dashboard or Export Query Results

After you run a query, you can add the query to a CloudWatch dashboard, or copy the results to the
clipboard. Queries added to dashboards automatically re-run every time you load the dashboard
and every time the dashboard refreshes.

### View Running Querues or Query History

You can view the queries currently in progress as well as your recent query history.

## Log Groups and Log Streams

A __log stream__ is a sequence of log events that share the same source. Each separate source of
logs into CloudWatch Logs makes up a separate log stream.

A __log group__ is a group of log streams that share the same retention, monitoring, and access
control settings. You can define log groups and specify which streams to put into each group. There
is no limit on the number of log streams that can belong to one log group.

## Searching and Filtering Log Data

After the CloudWatch logs agent begins publishing log data to Amazon CloudWatch, you can begin
searching and filtering the log data by creating one or more metric filters. Metric filters define
the terms and patterns to look for in log data as it is sent to CloudWatch logs. CloudWatch Logs
uses these metric filters to turn log data into numerical CloudWatch metrics that you can graph
or set an alarm on.

### Concepts

#### filter Pattern

A symbolic description of how CloudWatch Logs should interpret the data in each log event.
For example, a log entry may contain timestamps, IP addresses, strings, and so on. You use the
pattern to specify what to look for in the log file.

#### metric name

The name of the CloudWatch metric to which the monitored log information should be published. For
exaple, you may publish a metric called ErrorCount.

#### metric namespace

The desintation of the new CloudWatch metric.

#### metric value

The numerical value to publish to the metric each time a matching log is found. For example, if you're
counting the occurences of a particular term like "Error", the value will be "1" for each occurence.
If you're counting the bytes transferred, you can increment by the actual number of bytes found
in the log event

#### default value

The value reported to the metric filter during a period when no matching logs are found. By setting
this to 0, you ensure that data is reported during every period, preventing "spotty" metrics with
periods of no data.

## Real-time Processing of Log Data with Subscriptions

You can use subscriptions to get access to real-time feed of log events from CloudWatch logs and
have it delivered to other services such as Amazon Kinesis stream, Amazone Kinesis Data Firehose
stream, or AWS Lambda for custom processing, analysis, or loading to other systems.

To begin subscribing to log events, create the receiving source, such as a Kinesis stream, where
the events will be delivered. A sibscription filter defines the filter pattern to use for filtering
which log events get delivered to your AWS resoruce, as well as information about where to send matching
log events.

## Sending Logs Directly to S3

Some AWS services can publish logs directly to Amazon S3. This way, if your main requirements for
logs is storage in Amazon S3, you can easily have the service producing the logs send them directly
to Amazon S3 without setting up additional infrastructure.

Logs published to Amazon S3 are published to an existing bucket that you specify. One or more log
files are created every five minutes in the specified bucket.

The following logs can be published directly to Amazon S3:

- VPC flow logs.
- AWS Global Accelerator flow logs.

## Exporting Log Data to Amazon S3

You can export log data from your log groups to an Amazon S3 bucket and use this data in custom
processing and analysis, or load onto other systems.

To begin the export process, you must create an S3 bucket to store the exported log data. You can
store the exported files in your Amazon S3 bucket and define Amazon S3 lifecycle ruls to archive
or delete exported files automatically.

Log data can take up to 12 hours to become available for export.

## Streaming Log Data to Amazon Elasticsearch Service

You can configure CloudWatch Logs to stream data it receives to your Amazon Elasticsearch Service
(Amazon ES) cluster in near real-time through a CloudWatch Logs subscription.

## Authentication and Access Control for CloudWatch Logs

Access to Amazon CloudWatch Logs requires credentials that AWS can use to authenticate your requests.
Those credentials must have permissions to access AWS resources, such as retrieve CloudWatch logs
data about your cloud resources.

## Using CloudWatch Logs with Interface VPC Endpoints

If you use Amazon VPC to host AWS resources, you can establish a private connection between your VPC
and CloudWatch logs. You can use this connection to send logs to CloudWatch Logs without sending
them through the internet.

## Agent Reference

The CloudWatch Logs agent provides an automated way to send log data to CloudWatch Logs from Amazon
EC2 instances. The agent is comprised of the following components:

- A plug-in to the AWS CLI that pushes log data to CloudWatch logs
- A script (daemon) that initiates the process to push data to CloudWatch logs
- A cron job that ensures that the daemon is always running

## Monitoring Usage with CloudWatch Metrics

CloudWatch Logs sends metrics to Amazon CloudWatch every minute.

The `AWS/Logs` namespace includes the following metrics

| Metric             | Description                                                                                                           |
|:-------------------|:----------------------------------------------------------------------------------------------------------------------|
| IncomingBytes      | The volume of log events in uncompressed bytes                                                                        |
| IncomingLogEvents  | The number of log events uploaded to CloudWatch Logs                                                                  |
| ForwardedBytes     | The volume of log events in compressed bytes forwarded to the subscription destination                                |
| ForwardedLogEvents | The number of log events forwarded to the subscription destination                                                    |
| DeliveryErrors     | The number of log events for which CloudWatch Logs received an error when forwarding to the subscription destination  |
| DeliveryThrottling | The number of log events for which CloudWatch Logs was throttled when forwarding data to the subscription destination |