#!/usr/bin/env python3
"""
NAME
 bLogGroup.py

DESCRIPTION
 Beblsoft Cloud Watch Log Group Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
import random
import string
import sys
import math
import time
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CLOUD WATCH LOG GROUP ------------------- #
class BeblsoftCloudWatchLogGroup(BeblsoftAWSObject):
    """
    Beblsoft Cloud Watch Log Group
    """

    def __init__(self, name, retentionInDays=30, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            log group name
            Ex. "/aws/lambda/SmecknTestLambdaSwitch"
          retentionInDays:
            Number of days to retain the log events in the specified group
            Possible Values: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653
        """
        super().__init__()
        self.name            = name
        self.retentionInDays = retentionInDays
        self.bRegion         = bRegion
        self.logClient       = boto3.client("logs", region_name=bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ------------------------ RETENTION POLICY ----------------------------- #
    @logFunc()
    def setRetentionPolicy(self):
        """
        Set retention policy
        """
        self.logClient.put_retention_policy(
            logGroupName    = self.name,
            retentionInDays = self.retentionInDays)
        logger.info("{} Retention policy set".format(self))

    @logFunc()
    def deleteRetentionPolicy(self):
        """
        Delete retention policy
        """
        self.logClient.delete_retention_policy(
            logGroupName    = self.name)
        logger.info("{} Retention policy deleted".format(self))

    # ------------------------ LOG EVENTS ----------------------------------- #
    @logFunc(logArgs=False, logRval=False)
    def _tail(self, sleepS=1): #pylint: disable=W0221
        """
        Tail logs in log group, printing to stdout
        Args
          sleepS:
            time to sleep before requerying logs (sec)
        """
        startTimeEMS = math.floor(time.time() * 1000)
        try:
            while True:
                endTimeEMS = math.floor(time.time() * 1000)
                logEvents  = self.getLogEvents(startTimeEMS=startTimeEMS,
                                               endTimeEMS=endTimeEMS)
                for logEvent in logEvents:
                    print(logEvent.get("message").rstrip())
                if logEvents:
                    startTimeEMS = logEvents[-1].get("timestamp") + 1
                time.sleep(sleepS)
        except KeyboardInterrupt:
            sys.exit(0)

    @logFunc()
    def dumpStreamToFile(self, lsName, filePath=None, filterPattern="",
                         startTimeEMS=0, endTimeEMS=None, nRandomLetters=6):
        """
        Dump log stream to a filePath
        Args
          lsName:
            Log stream name
            Ex. "2018/11/06/[$LATEST]65ef994ffa76417c83853e1b42014029"
          filePath:
            File path to dump logs
            If None, logs will be dumped to a random file
            Ex. /tmp/logs
          filterPatern:
            Filter pattern to use
            If not provided, all events are matched
          startTimeEMS:
            Start time in milliseconds since epoch
            Ex. 1541520557660
          endTimeEMS:
            End time in milliseconds since epoch
            Ex. 1541520557990
          nRandomLetters:
            If filePath not given, number of random letters used to construct
            file path
            Ex. 6
        """
        logger.info("Dumping Stream '{}'...".format(lsName))

        # Get events
        logEvents = self.getLogEvents(
            startTimeEMS  = startTimeEMS,
            endTimeEMS    = endTimeEMS,
            lsNameList    = [lsName],
            filterPattern = filterPattern)

        # Write to file
        if not filePath:
            fileName = ''.join(random.choice(string.ascii_letters)
                               for i in range(nRandomLetters))
            filePath = "/tmp/log/{}.txt".format(fileName)
        os.makedirs(os.path.dirname(filePath), exist_ok = True)
        with open(filePath, 'w') as f:
            for logEvent in logEvents:
                f.write(logEvent.get("message"))

        # Results
        logger.info("{} Dumped Logs:".format(self))
        logger.info("\tStream : {}".format(lsName))
        logger.info("\tFile   : {}".format(filePath))


    # ------------------------ GET LOG EVENTS ------------------------------- #
    @logFunc(logArgs=False, logRval=True)
    def getLogEvents(self, startTimeEMS=0, endTimeEMS=None, lsNameList=None,
                     filterPattern="", interleaved=True):
        """
        Retrieve log events
        Args
          startTimeEMS:
            start time (Epoch Milli Seconds)
          endTimeEMS:
            end time (Epoch Milli Seconds)
            If None, currentTime used
          lsNameList:
            List of log stream names
            If None, all log streams are queried
          filterPatern:
            Filter pattern to use
            If not provided, all events are matched
          interleaved:
            If True, return responses that contain events from multiple
            log streams within log group
            If False, all log events in first stream are returned first
        Returns
          List of logEvents sorted by timeStamp
          See here:
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/logs.html#CloudWatchLogs.Client.filter_log_events
        """
        resp           = {}
        nextToken      = None
        doneRetrieving = False
        logEvents      = []
        extraArgs      = {}
        if not lsNameList:
            lsMetaList = self.__getLogStreamMetadata()
            lsNameList = [sMeta.get("logStreamName") for sMeta in lsMetaList]

        while not doneRetrieving:
            __endTimeEMS = endTimeEMS
            if not __endTimeEMS:
                __endTimeEMS = math.floor(time.time() * 1000)
            resp = self.logClient.filter_log_events(
                logGroupName   = self.name,
                logStreamNames = lsNameList,
                startTime      = startTimeEMS,
                endTime        = __endTimeEMS,
                filterPattern  = filterPattern,
                interleaved    = interleaved,
                **extraArgs)
            logEvents += resp.get("events", None)
            nextToken  = resp.get("nextToken", None)
            if nextToken:
                extraArgs["nextToken"] = nextToken
            else:
                doneRetrieving = True

        return sorted(logEvents, key=lambda k: k.get("timestamp"))

    # ------------------------ METADATA ------------------------------------- #
    @logFunc(logArgs=False, logRval=True)
    def __getLogStreamMetadata(self, orderBy="LastEventTime", descending=True):
        """
        Return Log Stream Metadata
        Args
          orderBy:
            values: "LogStreamName" or "LastEventTime"
          descending:
            If True, results are returned in descending order
            If False, results returned in ascending order
        Returns
          None or metadata here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/logs.html#CloudWatchLogs.Client.describe_log_streams
        """
        resp = self.logClient.describe_log_streams(
            logGroupName=self.name, orderBy=orderBy, descending=descending)
        return resp.get("logStreams", None)

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return log group metadata
        Returns
          None or metadata object here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/logs.html#CloudWatchLogs.Client.describe_log_groups
        """
        meta = None
        logGroupMetaList = self.logClient.describe_log_groups(logGroupNamePrefix=self.name).get("logGroups")
        if logGroupMetaList:
            meta = logGroupMetaList[0]
        return meta
