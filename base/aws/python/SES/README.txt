OVERVIEW
===============================================================================
AWS Simple Email Service (SES) Documentation
- Description
  * Amazon Simple Email Service (SES) is a cloud-based email sending service
    designed to help application developers send marketing, notification, and
    transaction emails
- Relevant URLS
  * Product Details                 : https://aws.amazon.com/ses/details/
  * Whitepaper                      : https://media.amazonwebservices.com/AWS_Amazon_SES_Best_Practices.pdf
  * Common Email Account Names      : http://www.ietf.org/rfc/rfc2142.txt
  * URI Black List                  : http://uribl.com/
  * URI Reputation Data             : http://www.surbl.org/
  * Spam Assassin                   : https://spamassassin.apache.org/


TERMS
===============================================================================
- Deliverability                    : Likelihood that an email message you send
                                      will actually arrive at its intended destination.
                                      I.e. the recipient's inbox
                                      Best way to increase deliverability is to
                                      send high quality mail
- Email quality                     : Value to email recipient. ISPs try to predict
                                      email quality using various anti-spam metrics.
- Reputation                        : Trust with the recipient and ISP. Receiver
                                      ISPs use metrics to assess the sender's mail
                                      and create a sender reputation score
- Hard Bounce                       : Persistent delivery failures. Ex. "Mailbox
                                      does not exist"
                                      SES passes back sync and async hard bounces
- Soft Bounce                       : Transient failures. Ex. "Mailbox Full"
                                      SES retries soft bounces
- Bounce Rate                       : Rate of failed delivery. A high rate of hard
                                      bounces strongly indicates to email receivers that
                                      you don't know your recipients. Therefore, high
                                      bounce rates have a negative impact on deliverability.
- Complaint                         : When an email recipient marks a message as spam
                                      by clicking "mark as spam" button, the ISP records
                                      the event as a complaint. Some ISPs allow senders
                                      for more transparency by providing feedback loops.
                                      SES automatically forwards complaints from ISPs
                                      back to developer.
- Complaint Rate                    : Rate of complaints. As complaint rate goes
                                      up deliverability goes down.
- Spam Detection                    : Email receivers have cracked down on malicious
                                      communication from spammers (phishing, virus, or scams).
                                      Many content filters exist: Apache Spam Assassin,
                                      Google Postini, Symantex BrightMail
                                      SES using filtering technologies to help block
                                      messages containing viruses before they are sent.
- Spamtraps                         : Special addresses set up by ISPs to monitor unsolicited email.
- Simple Email Transfer Protocol    : (SMTP) Internet standard for email transmision
- Internet Message Access Protocol  : (IMAP) Internet standard protocol used by email clients
                                      to retrieve email messages from a mail server
                                      over a TCP/IP connection
- Post Office Protocol              : (POP3) Application layer internet standard protocol
                                      used by email clients to retreive email from a server
                                      in an IP network
- DomainKeys Identified Mail        : (DKIM) Allows senders to sign their email messages and ISPs
                                      to use those signatures to verify that those messages
                                      are legitimate and have not been modified by a third
                                      party in transit


PRODUCT DETAILS
===============================================================================
- High Deliverability               : Deliverability = likelihood that an email will
                                      arrive to recipient's inboxes is high.
                                      AWS ensures high quality content by actively filtering
                                      spam
- Content Personalization           : Create templates with email personalization
- Authentication                    : Supports industry standard authentication
                                      DomainKeys Identified Mail (DKIM),
                                      Sender Policy Framework (SPF), and
                                      Domain-based Message Authentication, Reporting
                                      and Conformance (DMARC)
- Dedicated IP Addresses            : Emails sent from a shared pool of SES customers
                                      to ensure high deliverability
                                      Can also create custom private sending IP address pools
- Monitoring                        : Monitor various email statistics: number of sends,
                                      deliveries, opens, clicks, bounces, complaints, and
                                      rejections
- Sender Reputation Management      : Track overall bounce and complaint rates for your account
                                      Take action when complaint rates hit a certain threshold
- Flexible Email Receiving          : Have complete control over emails you accept, and how
                                      they are processed.
- Multiple Email Sending Interfaces : SES Console, SMTP, SES API, AWS SDK
- Mailbox Simulator                 : Use mailbox simulator to test sending email
                                      to specificy addresses. Simulate successful deliveries,
                                      hard bounces, out-of-office responses, or complaints
- AWS Integration                   : AWS SES integrates seamlessly with other AWS
                                      services
- Pay Only For What You Use         : First 62,000 emails are free. Then a very low rate
                                      after that.


SENDING AN EMAIL
===============================================================================
- Email Sending Process:
  Sender -> Sender's Email Server -> Internet -> Receiver ISP -> Recipient
- Email Sending with SES
  Sender -> SES -------------------> Internet -> Receiver ISP -> Recipient


VERIFYING DOMAINS
===============================================================================
- Domain Confirmation               : SES requires that you verify your email or domain
                                      To confirm that you own it. Can verify a single
                                      email address or an entire domain
                                      If example.com is verified, can send from
                                      user1@example.com, user2@example.com, ...
- Verify a New Domain               : SES Console > Domains
                                      Verify a New Domain
                                      Enter Domain Name
                                      Check Generate DKIM Settings
                                      Verify this Domain
                                      Use Route 53 to generate records
- Generated Records
  * Text (TXT)                      : Used to verify domain
    Domain                            _amazonses.smeckn.com.
    Value                             "b44t/ypof549F/DGrk24gHeaul/sF4L2O4qmHIIMwUQ="
  * Mail Exchange (MX)              : Used for email receiving
    Domain                            smeckn.com
    Value                             10 inbound-smtp.us-east-1.amazonaws.com
  * Domain Key 1 (CNAME)            : Used to enable DKIM signing
    Domain                            4c4qnzq2put55ay7oroolgodjivz5m2x._domainkey.smeckn.com.
    Value                             4c4qnzq2put55ay7oroolgodjivz5m2x.dkim.amazonses.com
  * Domain Key 2 (CNAME)            : Used to enable DKIM signing
    Domain                            bw2owh5gbmqu6ikg7mbyxmfwkrl2v7cu._domainkey.smeckn.com.
    Value                             bw2owh5gbmqu6ikg7mbyxmfwkrl2v7cu.dkim.amazonses.com
  * Domain Key 3 (CNAME)            : Used to enable DKIM signing
    Domain                            f4gl2sfqu3yzowgshvs5gnqtcam64hxk._domainkey.smeckn.com.
    Value                             f4gl2sfqu3yzowgshvs5gnqtcam64hxk.dkim.amazonses.com
- Notes                             : Must reverify in each SES region
                                    : Can send from any subdomain of verified domain.
                                      I.e. if example.com is verified, can send from
                                      user@a.b.example.com
                                    : Domain names are case-insensitive
                                    : Can verify as many as 10,000 identities (domains
                                      and email addresses) in each AWS region


SANDBOX
===============================================================================
- Description                       : To help prevent fraud and abuse, and to help protect your
                                      reputation as a sender, SES applies certain restrictions on
                                      new SES accounts
                                    : All new accounts go in the SES sandbox.
- Rules in the sandbox              : Only send to verified email addresses and domains or
                                      to the SES mailbox simulator
                                    : Can only send from verified addresses and domains
                                    : Can send a max of 200 messages in 24 hour period
                                    : Can send a max of 1 message per second
- To move out of the sandbox
  AWS Console > SES Home
  Sending Statistics
  Request a Sending Limit Increase
  Fill out request per
  documentation                     : https://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html


MAILBOX SIMULATOR
===============================================================================
- Description                       : SES includes a mailbox simulator that you can use to test how
                                      your application handles various email sending scenarios without
                                      affecting your sending quota or your
                                      bounce and complaint metrics
- Test Emails
  * Success                         : success@simulator.amazonses.com
                                      SNS success notification sent
  * Bounce                          : bounce@simulator.amazonses.com
                                      ISP rejects email with SMPT 550 5.1.1 response "Unknonw User"
                                      Email placed on suppression list
                                      SNS notifcation: RFC 3464
  * Out of the office               : ooto@simulator.amazonses.com
                                      Responds with OOTO message
                                      SNS notification: RFC 3834
  * Compliant                       : complaint@simulator.amazonses.com
                                      User clicks "Mark as Spam" within email application
                                      ISP sends a complaint response to Amazon SES
                                      SNS notification: RFC 5965
  * Address suppression list        : suppressionlist@simulator.amazonses.com
                                      SES treats email as a hard bounce
  * Other                           : Any other addresses@simulator.amazonses.com
                                      will be added to the suppression list


BEST PRACTICES
===============================================================================
- General
  * Put self in recipient's shoes. Do I want this in my inbox?
  * Industries with bad reputation: Home mortgage, credit, pharmaceuticals, tobacco
    alcohol, adult entertainment, gambling, work-from-home programs
  * Think
  * Think carefully about SUBJECT, FROM, and DOMAIN. When navigating on
    example-foo.com, recipients want email from sender@example-foo.com NOT someone else.
  * Don't send from ISP-based address. I.e. don't use sender@hotmail.com
  * Supply correct WHOIS info
- Authentication
  * Use Sender Policy Framework (SPF) and SenderID
  * Use DomainKeys or DomainKeys Identified Mail (DKIM)
- Building and maintaining your list
  * Be careful about how you collect email addresses. Sometimes people will
    provide bogus email addresses that generate hard bounces
  * Require duplicate email address fields
  * Use third-party vendors to check email validity before you send to it
  * Check email syntax, validate it corresponds to MX domain.
  * Don't send to postmaster@, abuse@, noc@, etc...
  * Beware of third party email lists. ISPs setup spamtraps in these lists
    to try and catch unsolicited email. Only collect email addresses directly
    from the recipient
- Compliance
  * Follow email laws.
- Avoiding and Handling Bounces
  * Keep bounce rate below 5%
  * Before sending email on whole list, verify liveliness, login activity,
    purchases, etc.
  * Provide alternative means of communication for critical information, such
    as password resets. Ex. secret questions, potal mail, or SMS.
    Allow recipient to choose email to send to.
  * Do not resend to a hard bounce.
  * Don't point bounce submission address to a mailbox that bounces
  * Add X-header (Ex. X-MarketingCampaign3) in emails so you can better track
    what bounces
- Avoiding and Handling Complaints
  * Keep complaint rate below 0.1%
  * Don’t continue to send a recipient the same type of email that generated a
    complaint.
  * Ensure that recipients know why they're getting an email
- Creating Quality Content
  * Use Span Assassin or Return Path to help identify content issues
  * Check against blacklisted links: URIBL.com, SURBL.org
  * Avoid broken links in emails: unsubscribe, privacy, and terms all work
  * For high frequency email, ensure that content is different in each email


RECEIVING EMAIL
===============================================================================
- General
  * When SES is used as email receiver, you must tell the service what to do
    with your mail
- MX Records                        : Each domain that is using SES for email
                                      receiving must have an MX record forwarding
                                      email to SES
    Ex1 Domain                        smeckn.com
        Value                         10 inbound-smtp.us-east-1.amazonaws.com
    Ex2 Domain                        test.smeckn.com
        Value                         10 inbound-smtp.us-east-1.amazonaws.com
- Recipient-Based Control           : Handle mail based on its recipient
                                      Ex1. DO_NOT_REPLY@example.com -> bounce
                                      Ex2. admin@example.com        -> S3
- Receipt Rules                     : Specify how to handle mail when condition is satisfied
                                      Consists of a condition and an ordered list of actions
                                      If the condition is matched SES performs the rule action
- Receipt Rule Actions
  S3         Action                 : Delivers mail to S3 bucket, optional SNS notification
  SNS        Action                 : Publish mail to SNS topic
  Lambda     Action                 : Invoke lambda function
  Bounce     Action                 : Reject email by returning bounce to sender
  Stop       Action                 : Terminates the evaluation of the receipt rule set
  Add Header Action                 : Adds a header to the received mail
  WorkMail   Action                 : Handle mail with Amazon WorkMail
- Recipient Condition matching
  * Match a specific email address  : user@example.com, Also matches user+123@example.com
  * Match all addresses within
    domain but not subdomains       : example.com
  * Match all addresses within a
    subdomain, but not those within
    parent domain                   : subdomain.example.com
  * Match all addresses within all
    subdomains, but not those within
    the parent domain               : .example.com
  * Match all addresses in domain
    and subdomains                  : example.com, .example.com
  * Match all recipients in all
    verified domains                : [None]
- Receit Rule Sets                  : Group of receipt rules
                                      You can define multiple receipt rule sets for AWS account,
                                      but only one is active at any time

             Active Receipt Rule Set
             -------------------------------------------------------------
   Mail      |                                                           |
  -------    |     -------------------------------------------------     |
  | \_/ | -------> |Rule1                                          |     |
  |     |    |     |   Condition  --> Action1  --> ... --> ActionN |     |
  -------    |     |                                               |     |
             |     -------------------------------------------------     |
             |            |                                              |
             |            v                                              |
             |            |                                              |
             |     --------------------------------------------------    |
             |     |RuleM                                           |    |
             |     |   Condition  --> Action1  --> ... --> ActionN  |    |
             |     |                                                |    |
             |     --------------------------------------------------    |
             |                                                           |
             -------------------------------------------------------------
- IP Address-Based Control          : Can also control mail by setting up IP address filters
  Allow List                        : IP address to allow.
                                      Note: use this to unblock IPs that SES blocks (ex. EC2 IP Addresses)
  Block List                        : IP adresses to block
- Email Receiving Process           1.  Incoming Mail
                                    2.  Check IP Allowed/Block Filters
                                    3.  Examine Active Receipt Rule Set for Match
                                    3a. If no match, SES rejects mail
                                    3b. If match, SES accepts mail. All of the
                                        matching receipt rule sets are execute

CONFIGURATION SETS
===============================================================================
- General
  * Configuration sets are groups of rules that you can apply to emails that are sent
  * When you apply a configuration set to an email, all of the rules in that
    configuration set are applied to the email.
- Rule Types
  Event Publishing                  : Track the number of send, delivery, open, click,
                                      bounce, and complaint events for each email you send.
                                      Invoke actions based on events.
  IP Pool Management                : Lease dedicated IP addresses to use with SES
                                      I.e. have one IP pool for marketing emails,
                                      another for transactional emails.


