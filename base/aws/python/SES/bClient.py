#!/usr/bin/env python3.6
"""
NAME:
 bClient.py

DESCRIPTION
 Beblsoft SES Client
"""

# ------------------------ IMPORTS ------------------------------------------ #
from datetime import datetime
import socket
import logging
import boto3


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES CLIENT ------------------------------- #
class BeblsoftSESClient():
    """
    Beblsoft SES Client
    """

    def __init__(self):
        """
        Initialize Object
        """
        self.sesClient = boto3.client("ses")

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    def sendEmail(self,  # pylint: disable=W0102
                  source,
                  destinationDict = {
                      "ToAddresses": [],
                      "CcAddresses": [],
                      "BccAddresses": []
                  },
                  messageDict = {
                      "Subject": {
                          "Data": "",
                          "Charset": ""
                      },
                      "Body": {
                          "Text": {
                              "Data": "",
                              "Charset": ""
                          },
                          "Html": {
                              "Data": "",
                              "Charset": ""
                          }
                      }
                  },
                  replyToAddressList = [],
                  returnPath = None,
                  tagList=[],
                  configurationSetName = None):
        """
        Send an Email
        Args
          source:
            Email address that is sending the email
            Ex. "jbensson@smeckn.com"
          destinationDict:
            The destination of the email, composed of To, CC, and BCC fields
            Note: the message may not hold more than 50 recipients
          messageDict:
            The message to be sent
          replyToAddressList:
            The reply to email addresses for the message
          returnPath:
            Email address that bounces and complaints will be forwarded
          configurationSetName:
            Name of configuration set to use when sending an email
        """
        kwargs                             = {}
        kwargs["Source"]                   = source
        kwargs["Destination"]              = destinationDict
        kwargs["Message"]                  = messageDict
        kwargs["ReplyToAddresses"]         = replyToAddressList
        if returnPath:
            kwargs["ReturnPath"]           = returnPath
        kwargs["Tags"]                     = tagList
        if configurationSetName:
            kwargs["ConfigurationSetName"] = configurationSetName

        self.sesClient.send_email(**kwargs)
        logger.info("{} Sent Email From {}".format(self, source))


# ----------------------- TEST HARNESS -------------------------------------- #
if __name__ == "__main__":
    # Test harness for the SES client

    bSESClient = BeblsoftSESClient()
    bSESClient.sendEmail(
        source          = "jbensson@test.smeckn.com",
        destinationDict = {
            "ToAddresses": ["foo@test.smeckn.com"]
        },
        messageDict = {
            "Subject": {
                "Data": "Hello World from {}".format(socket.gethostname()),
                "Charset": "UTF-8"
            },
            "Body": {
                "Text": {
                    "Data": "Hello World!\nHost={}\nTime={}".format(
                        socket.gethostname(), datetime.now()),
                    "Charset": "UTF-8"
                },
                "Html": {
                    "Data": "<h1>Hello World!</h1><h3>Host:{}</h3><h3>Date:{}</h3>".format(
                        socket.gethostname(), datetime.now()),
                    "Charset": "UTF-8"
                }
            }
        },
        configurationSetName="SmecknTestSESConfigurationSet")
