#!/usr/bin/env python3
"""
NAME:
 bTopicDestination.py

DESCRIPTION
 Beblsoft SES Configuration Set Topic Destination Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.SES.ConfigurationSet.bEventDestination import BeblsoftSESConfigurationSetEventDestination
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES CONFIGURATION SET TOPIC DESTINATION -- #
class BeblsoftSESConfigurationSetTopicDestination(BeblsoftSESConfigurationSetEventDestination):
    """
    Beblsoft SES Configuration Set Event Destination
    Abstract Base Class, must be overridden.
    """

    def __init__(self, bSNSTopic, **kwargs):
        """
        Initialize Object
        Args
          bSNSTopic:
            Beblsoft SNS Topic Object
        """
        super().__init__(**kwargs)
        self.bSNSTopic = bSNSTopic

    logFunc()
    def getDestDict(self, enabled):
        """
        Return destination dictionary
        """
        destDict                       = {}
        destDict["Name"]               = self.name
        destDict["Enabled"]            = enabled
        destDict["MatchingEventTypes"] = self.matchingEventList
        destDict["SNSDestination"]     = {
            "TopicARN": self.bSNSTopic.arn
        }
        return destDict

    # -------------------------- VERBS -------------------------------------- #
    logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        self.sesClient.create_configuration_set_event_destination(
            ConfigurationSetName=self.bSESConfigurationSet.name,
            EventDestination=self.getDestDict(enabled=True))

    logFunc()
    def _update(self, enabled=True): #pylint: disable=W0221
        """
        Update object
        """
        self.sesClient.update_configuration_set_event_destination(
            ConfigurationSetName=self.bSESConfigurationSet.name,
            EventDestination=self.getDestDict(enabled=enabled))
