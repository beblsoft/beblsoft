#!/usr/bin/env python3
"""
NAME:
 bSet.py

DESCRIPTION
 Beblsoft SES Configuration Set Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES CONFIGURATION SET -------------------- #
class BeblsoftSESConfigurationSet(BeblsoftAWSObject):
    """
    Beblsoft SES Configuration Set
    """

    def __init__(self, name):
        """
        Initialize Object
        Args
          name:
            Configuration Set Name
            Ex. SmecknConfigurationSet
        """
        super().__init__()
        self.name      = name
        self.sesClient = boto3.client("ses")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        self.sesClient.create_configuration_set(
            ConfigurationSet = {
                "Name": self.name
            })

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.sesClient.delete_configuration_set(ConfigurationSetName=self.name)

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ses.html#SES.Client.describe_configuration_set
        """
        resp = None
        try:
            resp = self.sesClient.describe_configuration_set(
                ConfigurationSetName=self.name,
                ConfigurationSetAttributeNames=["eventDestinations", "trackingOptions", "reputationOptions"])
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "ConfigurationSetDoesNotExist":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return resp
