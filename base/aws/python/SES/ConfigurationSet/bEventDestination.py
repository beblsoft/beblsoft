#!/usr/bin/env python3
"""
NAME:
 bEventDestination.py

DESCRIPTION
 Beblsoft SES Configuration Set Event Destination Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES CONFIGURATION SET EVENT DESTINATION -- #
class BeblsoftSESConfigurationSetEventDestination(BeblsoftAWSObject):
    """
    Beblsoft SES Configuration Set Event Destination
    Abstract Base Class, must be overridden.
    """

    def __init__(self,  # pylint: disable=W0102
                 name,
                 bSESConfigurationSet,
                 matchingEventList=["send", "reject", "bounce", "complaint", "delivery",
                                    "open", "click", "renderingFailure"]):
        """
        Initialize Object
        Args
          name:
            Configuration Set Destination Name
            Ex. SmecknConfigurationSetSNSTopicDest
          bSESConfigurationSet:
            Beblsoft SES Configuration Set Object
          bSNSTopic:
            Beblsoft SNS Topic Object to forward events to
          matchingEventList:
            List of email events to forward to SNS topic
        """
        super().__init__()
        self.name                  = name
        self.bSESConfigurationSet  = bSESConfigurationSet
        self.bRegion               = bSESConfigurationSet
        self.matchingEventList     = matchingEventList
        self.sesClient             = boto3.client("ses")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def online(self):
        """
        Return True is object is online and running.
        """
        return self.getValueFromMetadata(key="Enabled")

    # -------------------------- VERBS -------------------------------------- #
    def _create(self, **kwargs):
        """
        Create configuration set destination
        Must be overridden by subclass
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete configuration set destination
        """
        self.sesClient.delete_configuration_set_event_destination(
            ConfigurationSetName=self.bSESConfigurationSet.name,
            EventDestinationName=self.name)

    def _update(self, enabled=True): #pylint: disable=W0613,W0221
        """
        Update configuration set destination.
        Must be overridden by subclass
        Args
          enabled:
            If True, enable the destination
            If False, disable the destination
        """
        raise BeblsoftError(code=BeblsoftErrorCode.AWS_NOT_IMPLEMENTED,
                            msg="{}".format(self))

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get AWS metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ses.html#SES.Client.describe_configuration_set
        """
        meta = None
        resp = self.sesClient.describe_configuration_set(
            ConfigurationSetName=self.bSESConfigurationSet.name,
            ConfigurationSetAttributeNames=["eventDestinations"])
        metaList = resp.get("EventDestinations", [])
        for curMeta in metaList:
            if curMeta.get("Name") == self.name:
                meta = curMeta
                break
        return meta
