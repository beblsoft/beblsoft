#!/usr/bin/env python3
"""
NAME:
 bRuleSet.py

DESCRIPTION
 Beblsoft SES Receipt Rule Set Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE SET --------------------- #
class BeblsoftSESReceiptRuleSet(BeblsoftAWSObject):
    """
    Beblsoft SES Receipt Rule Set

    Note:
      Only one active rule set per AWS account
    """

    def __init__(self, name):
        """
        Initialize Object
        Args
          name:
            Rule set name
            Ex. BeblsoftRuleSet
        """
        super().__init__()
        self.name      = name
        self.sesClient = boto3.client("ses")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        self.sesClient.create_receipt_rule_set(
            RuleSetName=self.name)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.sesClient.delete_receipt_rule_set(
            RuleSetName=self.name)

    @logFunc()
    def _enable(self, **kwargs):
        """
        Enable object
        Note: only one object can be enabled at a time
        """
        self.sesClient.set_active_receipt_rule_set(
            RuleSetName=self.name)

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ses.html#SES.Client.describe_receipt_rule_set
        """
        meta = None
        try:
            meta = self.sesClient.describe_receipt_rule_set(RuleSetName=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "RuleSetDoesNotExist":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
