#!/usr/bin/env python3
"""
NAME:
 bBase.py

DESCRIPTION
 Beblsoft SES Receipt Rule Base Action Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import abc
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE BASE ACTION ------------- #
class BeblsoftSESReceiptRuleBaseAction(abc.ABC):
    """
    Beblsoft SES Receipt Rule Base Action
    """

    @abc.abstractmethod
    def getCreateDict(self):
        """
        Return action's creation dictionary for sesClient.create_receipt_rule
        """
        pass
