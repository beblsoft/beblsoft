#!/usr/bin/env python3
"""
NAME:
 bSNS.py

DESCRIPTION
 Beblsoft SES Receipt Rule SNS Action Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.SES.ReceiptRule.Action.bBase import BeblsoftSESReceiptRuleBaseAction


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE SNS ACTION -------------- #
class BeblsoftSESReceiptRuleSNSAction(BeblsoftSESReceiptRuleBaseAction):
    """
    Beblsoft SES Receipt Rule SNS Action
    """

    def __init__(self, bSNSTopic, encoding="UTF-8"):
        """
        Initialize Object
        Args
          bSNSTopic:
            Beblsoft SNS Topic object
          Encoding:
            One of: "UTF-8" or "Base64"
            UTF-8 is easier to use, but may not preserve all characters
        """
        self.bSNSTopic = bSNSTopic
        self.encoding  = encoding

    def getCreateDict(self):
        return {
            "SNSAction": {
                "TopicArn": self.bSNSTopic.arn,
                "Encoding": self.encoding
            }
        }
