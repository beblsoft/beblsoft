#!/usr/bin/env python3
"""
NAME:
 bAddHeader.py

DESCRIPTION
 Beblsoft SES Receipt Rule Add Header Action Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.SES.ReceiptRule.Action.bBase import BeblsoftSESReceiptRuleBaseAction


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE ADD HEADER ACTION ------- #
class BeblsoftSESReceiptRuleAddHeaderAction(BeblsoftSESReceiptRuleBaseAction):
    """
    Beblsoft SES Receipt Rule Add Header Action
    """

    def __init__(self, headerName, headerValue):
        """
        Initialize Object
        Args
          headerName:
            Name of the header.
            Must be between 1 and 50 characters
          headerValue:
            Header value.
            Must be less than 2048 characters
        """
        self.headerName  = headerName
        self.headerValue = headerValue

    def getCreateDict(self):
        return {
            "AddHeaderAction": {
                "HeaderName": self.headerName,
                "HeaderValue": self.headerValue
            },
        }
