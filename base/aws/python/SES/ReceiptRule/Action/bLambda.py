#!/usr/bin/env python3
"""
NAME:
 bLambda.py

DESCRIPTION
 Beblsoft SES Receipt Rule Lambda Action Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.SES.ReceiptRule.Action.bBase import BeblsoftSESReceiptRuleBaseAction


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE LAMBDA ACTION ----------- #
class BeblsoftSESReceiptRuleLambdaAction(BeblsoftSESReceiptRuleBaseAction):
    """
    Beblsoft SES Receipt Rule Lambda Action
    """

    def __init__(self, bFunction, invocationType="Event", bSNSTopic=None):
        """
        Initialize Object
        Args
          bFunction:
            Beblsoft Lambda Function
          invocationType:
            One of "RequestResponse" or "Event"
            RequestResponse - the execution will immediatley result in a response
                              30 second timeout
            Event           - execution will happen asynchronously
          bSNSTopic:
            Beblsoft SNS Topic object
        """
        self.bFunction      = bFunction
        self.invocationType = invocationType
        self.bSNSTopic      = bSNSTopic

    def getCreateDict(self):
        d                                   = {}
        d["LambdaAction"]                   = {}
        d["LambdaAction"]["FunctionArn"]    = self.bFunction.arn
        d["LambdaAction"]["InvocationType"] = self.invocationType
        if self.bSNSTopic:
            d["LambdaAction"]["TopicArn"]   = self.bSNSTopic.arn
        return d
