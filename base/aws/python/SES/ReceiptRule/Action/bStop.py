#!/usr/bin/env python3
"""
NAME:
 bStop.py

DESCRIPTION
 Beblsoft SES Receipt Rule Stop Action Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.SES.ReceiptRule.Action.bBase import BeblsoftSESReceiptRuleBaseAction


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE STOP ACTION ------------- #
class BeblsoftSESReceiptRuleStopAction(BeblsoftSESReceiptRuleBaseAction):
    """
    Beblsoft SES Receipt Rule Stop Action
    """

    def __init__(self, bSESReceiptRuleSet, bSNSTopic=None):
        """
        Initialize Object
        Args
          bSESReceiptRuleSet:
            Beblsoft SES Receipt Rule Set Object
          bSNSTopic:
            Beblsoft SNS Topic Object
        """
        self.bSESReceiptRuleSet = bSESReceiptRuleSet
        self.bSNSTopic          = bSNSTopic

    def getCreateDict(self):
        d                               = {}
        d["StopAction"]                 = {}
        d["StopAction"]["Scope"]        = "RuleSet"
        if self.bSNSTopic:
            d["StopAction"]["TopicArn"] = self.bSNSTopic.arn
        return d
