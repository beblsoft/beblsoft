#!/usr/bin/env python3
"""
NAME:
 bRule.py

DESCRIPTION
 Beblsoft SES Receipt Rule Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SES RECEIPT RULE ------------------------- #
class BeblsoftSESReceiptRule(BeblsoftAWSObject):
    """
    Beblsoft SES Receipt Rule
    """

    def __init__(self, bSESReceiptRuleSet, name, recipientList,
                 bActionList, tlsPolicy="Optional", scanEnabled=False):
        """
        Initialize Object
        Args
          bSESReceiptRuleSet:
            Beblsoft SES Receipt Rule Set Object
          name:
            Rule name
            Ex. SmecknReceiptRule
          recipientList:
            List of email addresses domains that the rule should be applied to
            Ex1. ["smeckn.com"]
            Ex2. ["user1@smeckn.com", "user2@smeckn.com"]
          bActionList:
            List of BeblsoftReceiptRuleAction objects
          tlsPolicy:
            Require email encrypted over TLS
            One of "Optional" or "Require"
          scanEnabled:
            If True, messages that this receipt rule applies to are scanned
            for spam and viruses.
        """
        super().__init__()
        self.bSESReceiptRuleSet = bSESReceiptRuleSet
        self.name               = name
        self.recipientList      = recipientList
        self.bActionList        = bActionList
        self.tlsPolicy          = tlsPolicy
        self.scanEnabled        = scanEnabled
        self.sesClient          = boto3.client("ses")

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    def getRuleDict(self, enabled):
        """
        Return rule dictionary
        """
        ruleDict = {
            'Name': self.name,
            'Enabled': enabled,
            'TlsPolicy': self.tlsPolicy,
            'Recipients': self.recipientList,
            'Actions': [bA.getCreateDict() for bA in self.bActionList],
            'ScanEnabled': self.scanEnabled
        }
        return ruleDict

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def online(self):
        """
        Return True is object is online and running.
        """
        return self.getValueFromMetadata(key="Enabled")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, enabled=True, bAfterRule=None):  # pylint: disable=W0221
        """
        Create object
        """
        kwargs = {}
        kwargs["RuleSetName"] = self.bSESReceiptRuleSet.name
        if bAfterRule:
            kwargs["After"]   = bAfterRule.name
        kwargs["Rule"]        = self.getRuleDict(enabled)
        self.sesClient.create_receipt_rule(**kwargs)

    @logFunc()
    def _delete(self):  # pylint: disable=W0221
        """
        Delete object
        """
        if self.exists:
            self.sesClient.delete_receipt_rule(
                RuleSetName=self.bSESReceiptRuleSet.name,
                RuleName=self.name)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def _update(self, enabled=True):  # pylint: disable=W0221
        """
        Update object
        """
        self.sesClient.update_receipt_rule(
            RuleSetName = self.name,
            Rule        = self.getRuleDict(enabled))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ses.html#SES.Client.describe_receipt_rule
        """
        meta = None
        try:
            resp = self.sesClient.describe_receipt_rule(
                RuleSetName=self.bSESReceiptRuleSet.name,
                RuleName=self.name)
            meta = resp.get("Rule", None)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "RuleDoesNotExist":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
