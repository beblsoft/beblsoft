#!/usr/bin/env python3.6
"""
NAME:
 bImageClient.py

DESCRIPTION
 Beblsoft Rekognition Image Client
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT REKOGNITION IMAGE CLIENT ----------------- #
class BeblsoftRekognitionImageClient():
    """
    Beblsoft Rekognition Image Client
    """

    def __init__(self, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        """
        self.rekClient = boto3.client("rekognition", region_name=bRegion.name)

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ----------------------- FACES ----------------------------------------- #
    @logFunc()
    def detectFaces(self, imgBytes=None, attributes="ALL"):
        """
        Detect Faces in Image
        Args
          imgBytes:
            Blob of image bytes
            Max size = 5MB
            Formats supported: JPEG and PNG
          attributes:
            Array of facial attributes that should be returned
            One of "ALL" or "DEFAULT"
        Returns
          {
            'FaceDetails': [
                {
                  'BoundingBox': {
                      'Width': ...,
                      'Height': ...,
                      'Left': ...,
                      'Top': ...
                  },
                  'AgeRange': {
                      'Low': 123,
                      'High': 123
                  },
                  'Smile': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'Eyeglasses': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'Sunglasses': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'Gender': {
                      'Value': 'Male'|'Female',
                      'Confidence': ...
                  },
                  'Beard': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'Mustache': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'EyesOpen': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'MouthOpen': {
                      'Value': True|False,
                      'Confidence': ...
                  },
                  'Emotions': [
                      {
                          'Type': 'HAPPY'|'SAD'|'ANGRY'|'CONFUSED'|'DISGUSTED'|'SURPRISED'|'CALM'|'UNKNOWN',
                          'Confidence': ...
                      },
                  ],
                  'Landmarks': [
                      {
                          'Type': 'eyeLeft'|'eyeRight'|'nose'|'mouthLeft'|'mouthRight'|'leftEyeBrowLeft'|'leftEyeBrowRight'|'leftEyeBrowUp'|'rightEyeBrowLeft'|'rightEyeBrowRight'|'rightEyeBrowUp'|'leftEyeLeft'|'leftEyeRight'|'leftEyeUp'|'leftEyeDown'|'rightEyeLeft'|'rightEyeRight'|'rightEyeUp'|'rightEyeDown'|'noseLeft'|'noseRight'|'mouthUp'|'mouthDown'|'leftPupil'|'rightPupil'|'upperJawlineLeft'|'midJawlineLeft'|'chinBottom'|'midJawlineRight'|'upperJawlineRight',
                          'X': ...,
                          'Y': ...
                      },
                  ],
                  'Pose': {
                      'Roll': ...,
                      'Yaw': ...,
                      'Pitch': ...
                  },
                  'Quality': {
                      'Brightness': ...,
                      'Sharpness': ...
                  },
                  'Confidence': ...
                },
            ],
            'OrientationCorrection': 'ROTATE_0'|'ROTATE_90'|'ROTATE_180'|'ROTATE_270'
          }
        """
        resp = self.rekClient.detect_faces(
            Image={"Bytes": imgBytes},
            Attributes = [attributes])
        resp.pop("ResponseMetadata")
        return resp

    # ----------------------- ENTITIES -------------------------------------- #
    @logFunc()
    def detectEntities(self, imgBytes=None, maxLabels=None, minConfidence=None):
        """
        Detect Entities in Image
        Args
          imgBytes:
            Blob of image bytes
            Max size = 5MB
          maxLabels:
            Max number of labels that service should return
            Service retures specified number of highest confidence labels
          minConfidence:
            Min confidence of labels to return
            If not specified, operation returns labels with confidence values >= .5

        Returns
          {
            'Labels': [
               {
                 'Name': 'string',            # Name of object or scene
                 'Confidence': ...,           # Level of confidence
                 'Instances': [               # Bounding box for each instance of detected object
                     {
                         'BoundingBox': {
                             'Width': ...,
                             'Height': ...,
                             'Left': ...,
                             'Top': ...
                         },
                         'Confidence': ...
                     },
                 ],
                 'Parents': [                 # Parent labels for label
                     {
                         'Name': 'string'
                     },
                 ]
               },
            ],
            'OrientationCorrection': 'ROTATE_0'|'ROTATE_90'|'ROTATE_180'|'ROTATE_270',
            'LabelModelVersion': 'string'
          }
        """
        kwargs                      = {}
        kwargs["Image"]             = {"Bytes": imgBytes}
        if maxLabels:
            kwargs["MaxLabels"]     = maxLabels
        if minConfidence:
            kwargs["MinConfidence"] = minConfidence

        resp = self.rekClient.detect_labels(**kwargs)
        resp.pop("ResponseMetadata")
        return resp

    # ----------------------- MODERATION LABELS ----------------------------- #
    @logFunc()
    def detectModerationLabels(self, imgBytes=None, minConfidence=None):
        """
        Detect Moderation Labels in Image
        Args
          imgBytes:
            Blob of image bytes
            Max size = 5MB
          minConfidence:
            Min confidence of labels to return
            If not specified, operation returns labels with confidence values >= .5

            Two levels of taxonomy : https://docs.aws.amazon.com/rekognition/latest/dg/moderation.html
              1 Explicity Nudity   : Nudity
                                   : Graphic Male Nudity
                                   : Graphic Female Nudity
                                   : Sexual Activity
                                   : Partial Nudity
              2 Suggestive         : Female Swimwear Or Underwear
                                   : Male Swimwear Or Underwear
                                   : Revealing Clothes
        Returns
          [
            {
             'Confidence': ...,
             'Name': 'string',
             'ParentName': 'string'
            },
          ]
        """
        kwargs                      = {}
        kwargs["Image"]             = {"Bytes": imgBytes}
        if minConfidence:
            kwargs["MinConfidence"] = minConfidence

        resp = self.rekClient.detect_moderation_labels(**kwargs)
        return resp.get("ModerationLabels")

    # ----------------------- TEXT ------------------------------------------ #
    @logFunc()
    def detectText(self, imgBytes=None):
        """
        Detect Text in Image
        Args
          imgBytes:
            Blob of image bytes
            Max size = 5MB

        Returns
          [
            {
              'DetectedText': 'string',
              'Type': 'LINE'|'WORD',
              'Id': 123,
              'ParentId': 123,
              'Confidence': ...,
              'Geometry': {
                  'BoundingBox': {
                      'Width': ...,
                      'Height': ...,
                      'Left': ...,
                      'Top': ...
                  },
                  'Polygon': [
                      {
                          'X': ...,
                          'Y': ...
                      },
                  ]
              }
            },
          ]
        """
        resp = self.rekClient.detect_text(Image={"Bytes": imgBytes})
        return resp.get("TextDetections")

    # ----------------------- CELEBRITIES ----------------------------------- #
    @logFunc()
    def detectCelebrities(self, imgBytes=None):
        """
        Detect Celebrities in Image
        Args
          imgBytes:
            Blob of image bytes
            Max size = 5MB

        Returns
          {
            'CelebrityFaces': [
                {
                    'Urls': [
                        'string',
                    ],
                    'Name': 'string',
                    'Id': 'string',
                    'Face': {
                        'BoundingBox': {
                            'Width': ...,
                            'Height': ...,
                            'Left': ...,
                            'Top': ...
                        },
                        'Confidence': ...,
                        'Landmarks': [
                            {
                                'Type': 'eyeLeft'|'eyeRight'|'nose'|'mouthLeft'|'mouthRight'|'leftEyeBrowLeft'|'leftEyeBrowRight'|'leftEyeBrowUp'|'rightEyeBrowLeft'|'rightEyeBrowRight'|'rightEyeBrowUp'|'leftEyeLeft'|'leftEyeRight'|'leftEyeUp'|'leftEyeDown'|'rightEyeLeft'|'rightEyeRight'|'rightEyeUp'|'rightEyeDown'|'noseLeft'|'noseRight'|'mouthUp'|'mouthDown'|'leftPupil'|'rightPupil'|'upperJawlineLeft'|'midJawlineLeft'|'chinBottom'|'midJawlineRight'|'upperJawlineRight',
                                'X': ...,
                                'Y': ...
                            },
                        ],
                        'Pose': {
                            'Roll': ...,
                            'Yaw': ...,
                            'Pitch': ...
                        },
                        'Quality': {
                            'Brightness': ...,
                            'Sharpness': ...
                        }
                    },
                    'MatchConfidence': ...
                },
            ],
            'UnrecognizedFaces': [
                {
                    'BoundingBox': {
                        'Width': ...,
                        'Height': ...,
                        'Left': ...,
                        'Top': ...
                    },
                    'Confidence': ...,
                    'Landmarks': [
                        {
                            'Type': 'eyeLeft'|'eyeRight'|'nose'|'mouthLeft'|'mouthRight'|'leftEyeBrowLeft'|'leftEyeBrowRight'|'leftEyeBrowUp'|'rightEyeBrowLeft'|'rightEyeBrowRight'|'rightEyeBrowUp'|'leftEyeLeft'|'leftEyeRight'|'leftEyeUp'|'leftEyeDown'|'rightEyeLeft'|'rightEyeRight'|'rightEyeUp'|'rightEyeDown'|'noseLeft'|'noseRight'|'mouthUp'|'mouthDown'|'leftPupil'|'rightPupil'|'upperJawlineLeft'|'midJawlineLeft'|'chinBottom'|'midJawlineRight'|'upperJawlineRight',
                            'X': ...,
                            'Y': ...
                        },
                    ],
                    'Pose': {
                        'Roll': ...,
                        'Yaw': ...,
                        'Pitch': ...
                    },
                    'Quality': {
                        'Brightness': ...,
                        'Sharpness': ...
                    }
                },
            ],
            'OrientationCorrection': 'ROTATE_0'|'ROTATE_90'|'ROTATE_180'|'ROTATE_270'
          }
        """
        resp = self.rekClient.recognize_celebrities(Image={"Bytes": imgBytes})
        resp.pop("ResponseMetadata")
        return resp
