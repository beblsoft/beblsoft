#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Rekognition Samples
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback
import logging
import pprint
from datetime import datetime
import click
from PIL import Image, ImageDraw
from base.bebl.python.log.bLog import BeblsoftLog
from base.aws.python.Rekognition.bImageClient import BeblsoftRekognitionImageClient


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Rekognition -----------------------------
        self.bRekognition      = BeblsoftRekognitionImageClient()

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/rekognitionSample.log", type=click.Path(),
              help="Specify log file Default=/tmp/rekognitionSample.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Rekognition Samples
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- DETECT FACES -------------------------------------- #
@cli.command()
def detect_faces():
    """
    Detect Faces
    """
    filename            = "facialExpressions.jpg"
    resp                = None
    image               = Image.open(filename)
    imgWidth, imgHeight = image.size
    draw                = ImageDraw.Draw(image)

    with open(filename, "rb") as im:
        resp = gc.bRekognition.detectFaces(imgBytes=im.read())

    for faceDetail in resp["FaceDetails"]:
        # Draw Bounding Box
        box     = faceDetail["BoundingBox"]
        left    = imgWidth * box["Left"]
        top     = imgHeight * box["Top"]
        width   = imgWidth * box["Width"]
        height  = imgHeight * box["Height"]
        points  = ((left, top),
                   (left + width, top),
                   (left + width, top + height),
                   (left, top + height),
                   (left, top))
        draw.line(points, fill="#00d400", width=2)

        # Draw emotion
        emotions = faceDetail["Emotions"]
        emotions.sort(key = lambda obj: obj.get("Confidence"), reverse=True)
        draw.text((left, top), emotions[0].get("Type"))
    image.show()


# ----------------------- DETECT ENTITIES ----------------------------------- #
@cli.command()
def detect_entities():
    """
    Detect Entities
    """
    filename            = "hiker.jpg"
    resp                = None
    image               = Image.open(filename)
    imgWidth, imgHeight = image.size
    draw                = ImageDraw.Draw(image)

    with open(filename, "rb") as im:
        resp = gc.bRekognition.detectEntities(imgBytes=im.read())

    for label in resp["Labels"]:
        name       = label["Name"]
        confidence = label["Confidence"]
        for instance in label["Instances"]:
            # Draw Box over Each Instance
            box     = instance["BoundingBox"]
            left    = imgWidth * box["Left"]
            top     = imgHeight * box["Top"]
            width   = imgWidth * box["Width"]
            height  = imgHeight * box["Height"]
            points  = (
                (left, top),
                (left + width, top),
                (left + width, top + height),
                (left, top + height),
                (left, top)
            )
            draw.line(points, fill="#00d400", width=2)

            # Draw name
            draw.text((left, top), "{},{}".format(name, confidence))
    image.show()


# ----------------------- DETECT MODERATION LABELS -------------------------- #
@cli.command()
def detect_moderation_labels():
    """
    Detect Moderation Labels
    """
    filename = "shirtlessMan.jpg"
    resp     = None
    with open(filename, "rb") as im:
        resp = gc.bRekognition.detectModerationLabels(imgBytes=im.read())
    logger.info(pprint.pformat(resp))


# ----------------------- DETECT TEXT --------------------------------------- #
@cli.command()
def detect_text():
    """
    Detect Text
    """
    filename            = "inspirationalQuote.jpg"
    resp                = None
    image               = Image.open(filename)
    imgWidth, imgHeight = image.size
    draw                = ImageDraw.Draw(image)

    with open(filename, "rb") as im:
        resp = gc.bRekognition.detectText(imgBytes=im.read())

    for textObj in resp:
        text      = textObj["DetectedText"]
        ttype     = textObj["Type"] #pylint: disable=W0612
        tid       = textObj["Id"] #pylint: disable=W0612
        confidence= textObj["Confidence"] #pylint: disable=W0612

        # Draw Bounding Box
        box       = textObj["Geometry"]["BoundingBox"]
        left      = imgWidth * box["Left"]
        top       = imgHeight * box["Top"]
        width     = imgWidth * box["Width"]
        height    = imgHeight * box["Height"]
        points    = ((left, top),
                     (left + width, top),
                     (left + width, top + height),
                     (left, top + height),
                     (left, top))
        draw.line(points, fill="#00d400", width=2)

        # Draw name, info
        draw.text((left, top), "{}".format(text))
    image.show()


# ----------------------- DETECT CELEBRITIES -------------------------------- #
@cli.command()
def detect_celebrities():
    """
    Detect Celebrities
    """
    filename            = "markWahlberg.jpg"
    resp                = None
    image               = Image.open(filename)
    imgWidth, imgHeight = image.size
    draw                = ImageDraw.Draw(image)

    with open(filename, "rb") as im:
        resp = gc.bRekognition.detectCelebrities(imgBytes=im.read())

    for celeb in resp["CelebrityFaces"]:
        # Draw Bounding Box
        box       = celeb["Face"]["BoundingBox"]
        left      = imgWidth * box["Left"]
        top       = imgHeight * box["Top"]
        width     = imgWidth * box["Width"]
        height    = imgHeight * box["Height"]
        points    = ((left, top),
                     (left + width, top),
                     (left + width, top + height),
                     (left, top + height),
                     (left, top))
        draw.line(points, fill="#00d400", width=2)

        # Draw name, info
        name       = celeb["Name"]
        celebID    = celeb["Id"]
        confidence = celeb["Face"]["Confidence"]
        draw.text((left, top), "{},{},{}".format(name, celebID, confidence))
        draw.text((left, top+10), "{}".format(celeb.get("Urls")))
    image.show()


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
