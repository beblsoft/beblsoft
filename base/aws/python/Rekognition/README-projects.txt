OVERVIEW
===============================================================================
Rekognition Samples Documentation



PROJECT: IMG SAMPLES
===============================================================================
- Description                       : Rekognition Image Samples
- Directory                         : ./imgSamples
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Commands --------------------------
  * Detect faces                    : ./cli.py detect-faces
  * Detect entities                 : ./cli.py detect-entities
  * Detect moderation labels        : ./cli.py detect-moderation-labels
  * Detect text                     : ./cli.py detect-text
  * Detect celebrities              : ./cli.py detect-celebrities