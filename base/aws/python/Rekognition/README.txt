OVERVIEW
===============================================================================
- AWS Rekognition Technology Documentation
- Description                       : Rekognition makes it easy to add image and video analysis to applications
                                    : The service can identify objects, people, text, scenes, activities,
                                      and any inappropriate content
                                    : It can detect, analyze, and compare faces for a wide variety of user
                                      verification, cataloging, people counting, and public safety use cases
- Image Features --------------------
  * Object and Scene Detection      : Object(swimwear), scene(beach), or concept(outdoors) found in image.
                                      Includes hierarchical taxonomy with parents and children
  * Facial Reconition               : Detect faces in photos and videos
  * Facial Analysis                 : Analyze facial content
  * Face Comparison                 : Compare faces across images
  * Unsafe Image Detection          : Determine if an image or video contains explicit or suggestive adult content
  * Celebrity Recognition           : Identify celebrities in images
  * Text in Image
- Video Features --------------------
  * Person Identification and Pathng: How people move through vido
  * Face Recognition                : Search video for face
  * Objects, Scenes, and Activities : Detect things such as city, beach, dancing
  * Inapprorpiate Video Detection
  * Celebrity Recognition
- Relevant URLS ---------------------
  * Home                            : https://aws.amazon.com/rekognition/
  * Documentation                   : https://docs.aws.amazon.com/rekognition/latest/dg/what-is.html



WORKING WITH IMAGES
===============================================================================
- Images ----------------------------
  * Supported Formats               : .jpg or .png
  * Images bytes over HTTP          : Must be a base64-encoded string
                                      Following APIs automatically encode image: Java, JavaScript, Python, PHP
  * Correcting Orientation          : Allows reorientation for display
- Bounding Boxes --------------------
  * Properties
    Height                          : Height of bounding box as ratio of image height
    Width                           : Width of bounding box as ration of overall width
    Left                            : Left coordinate of the bounding box as ratio of overall image width
    Top                             : Top coordinate of bounding box as ratio of overall height
  * Picture                            0                                    1
                                      ---------------------------------------
                                     0| Image                |              |
                                      |                      |              |
                                      |                  Top |              |
                                      |                      |              |
                                      |                      v              |
                                      |                -------------        |
                                      |                |Bounding   |^       |
                                      |                |Box        ||       |
                                      |-----Left------>|           |Height  |
                                      |                |           ||       |
                                      |                |           |v       |
                                      |                -------------        |
                                      |                <--Width---->        |
                                      |                                     |
                                     1|                                     |
                                      ---------------------------------------
- Image Orientations -----------------
  * Exif in JPEG                     : Can contain image orientation in jpeg metadata
                                       If set, OrientationCorrection will not be set
  * Rotating Image                   : If OrientationCorrection is set, image will neet to be rotated
                                       Note, bounding box coordinates will also have to be rotated
  * Bounding Box Transformations
    ROTATE_0                         : left = image.width*BoundingBox.Left
                                       top  = image.height*BoundingBox.Top
    ROTATE_90                        : left = image.height * (1 - (<face>.BoundingBox.Top + <face>.BoundingBox.Height))
                                       top  = image.width * <face>.BoundingBox.Left
    ROTATE_180                       : left = image.width - (image.width*(<face>.BoundingBox.Left+<face>.BoundingBox.Width))
                                       top  = image.height * (1 - (<face>.BoundingBox.Top + <face>.BoundingBox.Height))
    ROTATE_270                       : left = image.height * BoundingBox.top
                                       top  = image.width * (1 - BoundingBox.Left - BoundingBox.Width)