#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Flask Deployment Functionality
 Flask is deployed on AWS via the picture below.

PICTURE
  Client HTTP Request
        |
  Alias Recod                    api.quotablejoy.com
        |
  Cloud Front Distribution Edge  d2sxs4i9nlamdk.cloudfront.net
        |
  AWS API Gateway Rest API       https://6z25k1haj9.execute-api.us-east-1.amazonaws.com/Default
        |
        |
        |        Scheduled Events (Ex. Keep Warm)
        |              |
        |              |
  AWS Lambda Function Handler    QuoteProdFlaskDeployment
        |              |
        |              |
  Flask Application   Other Callbacks
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from jinja2 import Environment, FileSystemLoader
from base.aws.python.Lambda.bFunction import BeblsoftLambdaFunction
from base.aws.python.APIGateway.bRestAPI import BeblsoftAPIGatewayRestAPI
from base.aws.python.APIGateway.bRootResource import BeblsoftAPIGatewayRootResource
from base.aws.python.APIGateway.bResource import BeblsoftAPIGatewayResource
from base.aws.python.APIGateway.bMethod import BeblsoftAPIGatewayMethod
from base.aws.python.APIGateway.bLambdaIntegration import BeblsoftAPIGatewayLambdaIntegration
from base.aws.python.APIGateway.bDeployment import BeblsoftAPIGatewayDeployment
from base.aws.python.APIGateway.bStage import BeblsoftAPIGatewayStage
from base.aws.python.APIGateway.bCustomEdgeDomain import BeblsoftAPIGatewayCustomEdgeDomain
from base.aws.python.APIGateway.bBasePathMapping import BeblsoftAPIGatewayBasePathMapping
from base.aws.python.Route53.ResourceRecordSet.bAlias import BeblsoftAliasResourceRecordSet
from base.aws.python.CloudWatchEvents.bRule import BeblsoftCloudWatchEventsRule
from base.aws.python.CloudWatchEvents.bRuleLambdaTarget import BeblsoftCloudWatchEventsRuleLambdaTarget
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT FLASK DEPLOYMENT ------------------------- #
class BeblsoftFlaskDeployment():
    """
    Beblsoft Flask Deployment
    """

    def __init__(self, name, domainName, bHostedZone, bCertificate,  # pylint: disable=W0102
                 appModObjPath, bLambdaPackage, bIAMRole,
                 kwrMin=5, scheduledEvents=[], logLevel=logging.DEBUG,
                 **kwargs):
        """
        Initialize Object
        Args
          name:
            Deployment Name
            Ex. "QuoteProdFlaskDeployment"
          domainName:
            Name to deploy flask api
            Ex. "api.quotablejoy.com"
          bHostedZone:
            Beblsoft Hosted Zone Object
          bCertificate:
            BeblsoftACMCertificate object
          appModObjPath:
            Python path to flask module and application object
            Ex: "mod1.mod2.mod3:appObj"
          bLambdaPackage:
            Beblsoft Lambda Package Object
          bIAMRole:
            Beblsoft IAM Role
          kwrMin:
            Keep Warm Rate (in minutes) at which to keep the server warm
          scheduledEvents:
            List of scheduled event dictionaries. Each dictionary has the
            following fields:
              idx:
                Unique index [required]
                Solves problem where multiple function names are registerd
                Ex. 0
              function:
                Function to execute in bLambdaPackage [required]
                quote.serverV2.aws:foo
              expression:
                Rate expression at which to execute function [required]
                Ex. "rate(5 minutes)"
              description:
                Scheduled event description [optional]
                Ex. "QuoteFoo Description"
              kwargsFunc:
                Function to return dictionary of kwargs that will be passed to
                scheduled function [optional]
          logLevel:
            Level to log at
            Ex. logging.DEBUG
        """
        # ---- Basic ------------------
        self.name                 = name
        self.domainName           = domainName
        self.bHostedZone          = bHostedZone
        self.bCertificate         = bCertificate
        (appMod, appObj)          = appModObjPath.rsplit(":", 1)
        self.appModule            = appMod
        self.appObject            = appObj
        self.logLevel             = logLevel
        self.bRegion              = bLambdaPackage.bRegion
        self.bIAMRole             = bIAMRole
        self.kwrMin               = kwrMin

        # ---- Lambda Function --------
        self.bLFDir               = os.path.dirname(os.path.realpath(__file__))
        fromToPath                = {"fromPath": "{}/*.py".format(self.bLFDir),
                                     "toPath": "."}
        bLambdaPackage.fromToPaths.append(fromToPath)
        self.bLambdaFunction      = BeblsoftLambdaFunction(
            name                         = name,
            handler                      = "bHandler.handler",
            runTime                      = "python3.6",
            bLambdaPackage               = bLambdaPackage,
            bIAMRole                     = bIAMRole,
            **kwargs)

        # ---- Scheduled Events -------
        self.scheduledEvents      = scheduledEvents
        self.bCWERules            = []
        self.bCWERuleTargets      = []
        self.initScheduledEvents()

        # ---- Rest API ---------------
        self.bAGRestAPI           = BeblsoftAPIGatewayRestAPI(
            name                          = "{}RestAPI".format(self.name),
            description                   = "{} Rest API".format(self.name),
            bRegion                       = self.bRegion)

        # ---- Root Resource = "/" ----
        self.bAGRootResource      = BeblsoftAPIGatewayRootResource(
            bAGRestAPI                    = self.bAGRestAPI)
        self.bAGRootMethod        = BeblsoftAPIGatewayMethod(
            bAGResource                   = self.bAGRootResource,
            httpMethod                    = "ANY")
        self.bAGRootIntegration   = BeblsoftAPIGatewayLambdaIntegration(
            bAGMethod                     = self.bAGRootMethod,
            bLambdaFunction               = self.bLambdaFunction,
            bIAMRole                      = self.bIAMRole)

        # ---- Proxy Resource = "/*" --
        self.bAGProxyResource     = BeblsoftAPIGatewayResource(
            bAGRestAPI                    = self.bAGRestAPI,
            bAGParentRes                  = self.bAGRootResource,
            pathPart                      = "{proxy+}")
        self.bAGProxyMethod       = BeblsoftAPIGatewayMethod(
            bAGResource                   = self.bAGProxyResource,
            httpMethod                    = "ANY")
        self.bAGProxyIntegration  = BeblsoftAPIGatewayLambdaIntegration(
            bAGMethod                     = self.bAGProxyMethod,
            bLambdaFunction               = self.bLambdaFunction,
            bIAMRole                      = self.bIAMRole)

        # ---- Deployment -------------
        self.bAGDeployment        = BeblsoftAPIGatewayDeployment(
            bAGRestAPI                    = self.bAGRestAPI)

        # ---- Stage ------------------
        self.bAGStage             = BeblsoftAPIGatewayStage(
            bAGDeployment                 = self.bAGDeployment,
            name                          = "Default")

        # ---- Custom Edge Domain -----
        self.bAGEdgeDomain        = BeblsoftAPIGatewayCustomEdgeDomain(
            domainName                    = self.domainName,
            bCertificate                  = self.bCertificate,
            bRegion                       = self.bRegion)

        # ---- Base Path Mapping ------
        self.bAGBasePathMapping   = BeblsoftAPIGatewayBasePathMapping(
            domainName                    = self.domainName,
            basePath                      = "",
            bAGStage                      = self.bAGStage)

        # ---- Alias Record -----------
        self.bAliasRecord         = BeblsoftAliasResourceRecordSet(
            bHostedZone                   = self.bHostedZone,
            domainName                    = self.domainName,
            recordType                    = "A",
            dnsNameFunc                   = lambda: self.edgeDomainName,
            cloudFrontAlias               = True)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def edgeDomainName(self):
        """
        Return cloud front edge domain
        """
        return self.bAGEdgeDomain.cfDomain

    @property
    def stageDomainName(self):
        """
        Return API Gateway stage domain
        """
        return self.bAGStage.url

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self, lam=False, se=False, api=False, resources=False,
               deployment=False, stage=False, edge=False, bp=False,
               alias=False, allobjs=False, sleepS=1):
        """
        Create Deployment
        """
        if lam or allobjs:
            self.createLambdaSettingsFile()
            self.bLambdaFunction.create(sleepS=sleepS)
        if se or allobjs:
            for cweRule in self.bCWERules:
                cweRule.create()
            for cweRuleTarget in self.bCWERuleTargets:
                cweRuleTarget.create()
        if api or allobjs:
            self.bAGRestAPI.create()
        if resources or allobjs:
            self.bAGRootMethod.create()
            self.bAGRootIntegration.create()
            self.bAGProxyResource.create()
            self.bAGProxyMethod.create()
            self.bAGProxyIntegration.create()
        if deployment or allobjs:
            self.bAGDeployment.create()
        if stage or allobjs:
            self.bAGStage.create()
        if edge or allobjs:
            self.bAGEdgeDomain.create()
        if bp or allobjs:
            self.bAGBasePathMapping.create()
        if alias or allobjs:
            self.bAliasRecord.create()

    @logFunc()
    def update(self, lam=False, se=False, allobjs=False):
        """
        Update Deployment
        """
        if lam or allobjs:
            self.createLambdaSettingsFile()
            self.bLambdaFunction.update()
        if se or allobjs:
            self.delete(se=True)
            self.create(se=True)

    @logFunc()
    def delete(self, lam=False, se=False, api=False, resources=False,
               deployment=False, stage=False, edge=False, bp=False,
               alias=False, allobjs=False):
        """
        Delete Deployment
        """
        if alias or allobjs:
            self.bAliasRecord.delete()
        if bp or allobjs:
            self.bAGBasePathMapping.delete()
        if edge or allobjs:
            self.bAGEdgeDomain.delete()
        if stage or allobjs:
            self.bAGStage.delete()
        if deployment or allobjs:
            self.bAGDeployment.delete()
        if resources or allobjs:
            self.bAGProxyIntegration.delete()
            self.bAGProxyMethod.delete()
            self.bAGProxyResource.delete()
            self.bAGRootIntegration.delete()
            self.bAGRootMethod.delete()
        if api or allobjs:
            self.bAGRestAPI.delete()
        if se or allobjs:
            for cweRuleTarget in self.bCWERuleTargets:
                cweRuleTarget.delete()
            for cweRule in self.bCWERules:
                cweRule.delete()
        if lam or allobjs:
            self.bLambdaFunction.delete()

    @logFunc()
    def describe(self, lam=False, se=False, api=False, resources=False,
                 deployment=False, stage=False, edge=False, bp=False,
                 alias=False, allobjs=False):
        """
        Describe Deployment
        """
        if lam or allobjs:
            self.bLambdaFunction.describe()
        if se or allobjs:
            for cweRule in self.bCWERules:
                cweRule.describe()
            for cweRuleTarget in self.bCWERuleTargets:
                cweRuleTarget.describe()
        if api or allobjs:
            self.bAGRestAPI.describe()
        if resources or allobjs:
            self.bAGRootMethod.describe()
            self.bAGRootIntegration.describe()
            self.bAGProxyResource.describe()
            self.bAGProxyMethod.describe()
            self.bAGProxyIntegration.describe()
        if deployment or allobjs:
            self.bAGDeployment.describe()
        if stage or allobjs:
            self.bAGStage.describe()
        if edge or allobjs:
            self.bAGEdgeDomain.describe()
        if bp or allobjs:
            self.bAGBasePathMapping.describe()
        if alias or allobjs:
            self.bAliasRecord.describe()

    # ----------------------- LAMBDA FUNCTIONS ------------------------------ #
    @logFunc()
    def createLambdaSettingsFile(self):
        """
        Autogenerate flask deployment settings
        """
        templateFile        = "bSettingsTemplate.jinja"
        settingsPath        = "{}/bSettings.py".format(self.bLFDir)
        jinjaEnv            = Environment(
            loader             = FileSystemLoader(self.bLFDir),
            trim_blocks        = True)
        template            = jinjaEnv.get_template(templateFile)
        templateStr         = template.render(
            generatorFile      = __file__,
            appModule          = self.appModule,
            appObject          = self.appObject,
            logLevel           = self.logLevel)
        if os.path.exists(settingsPath):
            os.remove(settingsPath)
        with open(settingsPath, "w") as f:
            f.write(templateStr)

    @logFunc()
    def invokeLambda(self, *args, **kwargs):
        """
        Invoke flask deployment lambda
        """
        self.bLambdaFunction.invoke(*args, **kwargs)

    @logFunc()
    def tailLambda(self):
        """
        Tail lambda function
        """
        self.bLambdaFunction.tail()

    # ----------------------- SCHEDULED EVENTS ------------------------------ #
    @logFunc()
    def initScheduledEvents(self):
        """
        Initialize scheduled events

        Each scheduled event is composed of two parts:
           - CloudWatchEvents Rule
           - CloudWatchEvents Rule Target
        """
        # Add Keep Warm Event
        if self.kwrMin > 0:
            scheduledEvent  = {"index": 0,
                               "function": "bHandlerCallbacks:keepWarm",
                               "expression": "rate({} minute{sStr})".format(
                                   self.kwrMin, sStr="" if self.kwrMin == 1 else "s"),
                               "kwargsFunc": lambda: {"Hello": "World!"}}
            self.scheduledEvents.append(scheduledEvent)

        for se in self.scheduledEvents:
            index           = se.get("index")
            function        = se.get("function")
            expression      = se.get("expression")
            kwargsFunc      = se.get("kwargsFunc", lambda: {})
            name            = "{}-{}-{}".format(self.name,
                                                index, function.replace(":", "-"))
            cweRule         = BeblsoftCloudWatchEventsRule(
                name                 = name,
                bIAMRole             = self.bIAMRole,
                scheduleExpression   = expression,
                bRegion              = self.bRegion)
            cweRuleTarget   = BeblsoftCloudWatchEventsRuleLambdaTarget(
                bLambdaFunction      = self.bLambdaFunction,
                bRule                = cweRule,
                bIAMRole             = self.bIAMRole,
                tid                  = name,
                kwargs               = kwargsFunc())
            self.bCWERules.append(cweRule)
            self.bCWERuleTargets.append(cweRuleTarget)

    # ----------------------- API GATEWAY ----------------------------------- #
    @logFunc()
    def invokeAPI(self, *args, **kwargs):
        """
        Invoke flask deployment api
        """
        self.bAGProxyResource.invoke(*args, **kwargs)
