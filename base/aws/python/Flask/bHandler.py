#!/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
 Beblsoft Flask Handler Functionality

BACKGROUND
  LAMBDA EVENTS: https://docs.aws.amazon.com/lambda/latest/dg/invoking-lambda-function.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re
import sys
import json
import traceback
import logging
import pprint
import importlib
from urllib.parse import urlencode
import six
from werkzeug.wsgi import ClosingIterator
from werkzeug.wrappers import Response
from werkzeug import urls
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()


# ------------------------ FLASK MIDDLEWARE --------------------------------- #
class FlaskMiddleware():
    """
    Flask Middleware Class necessary for Application Deployment
    """

    def __init__(self, appObj):
        """
        Initialize object
        Args
          appObj:
            WSGI app object
        """
        self.appObj = appObj

    def __call__(self, environ, start_response):
        """
        Call Object
        """
        def encode_response(status, headers, exc_info=None):
            """
            Encode response
            """
            return start_response(status, headers, exc_info)

        response = self.appObj(environ, encode_response)
        return ClosingIterator(response)


# ------------------------ LAMBDA HANDLER ----------------------------------- #
class LambdaHandler():
    """
    Singleton to avoid duplicate setup
    """
    __instance = None

    def __new__(cls):
        """
        Return singleton instance to avoid duplicate setup
        """
        if LambdaHandler.__instance is None:
            LambdaHandler.__instance = object.__new__(cls)
        return LambdaHandler.__instance

    def __init__(self):
        """
        Initialize object
        """
        self.settingsName   = "bSettings"
        self.settings       = importlib.import_module(self.settingsName)
        self.appModule      = importlib.import_module(self.settings.APP_MODULE)
        self.appObject      = getattr(self.appModule, self.settings.APP_OBJECT)
        self.logLevel       = self.settings.LOG_LEVEL
        self.flaskAppObject = FlaskMiddleware(self.appObject)
        logger.setLevel(self.logLevel)

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    @logFunc()
    def handler(self, event, context):
        """
        AWS Lambda Handler
        """
        resp = None
        if self.isScheduledEvent(event):
            resp    = self.executeScheduledEvent(event)
        elif self.isFlaskEvent(event):
            environ = self.createFlaskRequest(event, context)
            resp    = self.executeFlaskRequest(environ)
        else:
            logger.warning("No matching event")

        return resp

    # ---------------- SCHEDULED EVENTS ------------------------------------- #
    # Example Event from CloudWatch Events:
    # {
    #   'account': '225928776711',
    #   'detail': {},
    #   'detail-type': 'Scheduled Event',
    #   'id': '0e82ec82-f89c-75b4-861c-20eea3178e82',
    #   'kwargs': {'Hello': 'World!'},
    #   'region': 'us-east-1',
    #   'resources': ['arn:aws:events:us-east-1:225928776711:rule/QuoteTestFlaskDeployment-0-bHandler-keepWarm'],
    #   'source': 'aws.events',
    #   'time': '2018-03-16T21:13:54Z',
    #   'version': '0'
    # }
    @logFunc()
    def isScheduledEvent(self, event):  # pylint: disable=R0201
        """
        Return True if event was scheduled
        """
        detailType = event.get("detail-type", None)
        source     = event.get("source", None)
        return ((detailType == "Scheduled Event") and (source == "aws.events"))

    @logFunc()
    def executeScheduledEvent(self, event):  # pylint: disable=R0201
        """
        Execute a scheduled event
        """
        rval      = None
        ruleName  = None
        module    = None
        function  = None
        resources = event.get("resources", None)
        kwargs    = event.get("kwargs", {})

        # Parse module, function from resources name
        if resources:
            ruleName = resources[0]
        if ruleName:
            m = re.match(r".*-(\w*)-(\w*)$", ruleName)
            if m:
                module   = m.group(1)
                function = m.group(2)
        logger.info("module={} function={}".format(module, function))

        # Invoke module, function
        if module and function:
            moduleObj   = importlib.import_module(module)
            functionObj = getattr(moduleObj, function)
            rval = functionObj(**kwargs)
        return rval

    # ---------------- FLASK EVENTS ----------------------------------------- #
    # Example Event from API Gateway:
    # {
    #   'body': None,
    #   'headers': {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    #   'Accept-Encoding': 'gzip, deflate, br',
    #   'Accept-Language': 'en-US,en;q=0.8',
    #   'CloudFront-Forwarded-Proto': 'https',
    #   'CloudFront-Is-Desktop-Viewer': 'true',
    #   'CloudFront-Is-Mobile-Viewer': 'false',
    #   'CloudFront-Is-SmartTV-Viewer': 'false',
    #   'CloudFront-Is-Tablet-Viewer': 'false',
    #   'CloudFront-Viewer-Country': 'US',
    #   'Host': 'api.test.smeckn.com',
    #   'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '
    #   'AppleWebKit/537.36 (KHTML, like Gecko) '
    #   'Chrome/61.0.3163.100 Safari/537.36',
    #   'Via': '2.0 d7d969e3c6b32bf100efb5f21e71b14a.cloudfront.net '
    #   '(CloudFront)',
    #   'X-Amz-Cf-Id': 'vpGikfTos1XBudj0_pyUVArXv0c-XeKECLGJA8lnYzOjCRlup1kLGg==',
    #   'X-Amzn-Trace-Id': 'Root=1-5ad503b7-ea2a013c4b796554460e2440',
    #   'X-Forwarded-For': '209.17.40.44, 54.239.145.57',
    #   'X-Forwarded-Port': '443',
    #   'X-Forwarded-Proto': 'https',
    #   'upgrade-insecure-requests': '1'},
    #   'httpMethod': 'GET',
    #   'isBase64Encoded': False,
    #   'path': '/api/v1/',
    #   'pathParameters': {'proxy': 'api/v1'},
    #   'queryStringParameters': None,
    #   'requestContext': {'accountId': '225928776711',
    #   'apiId': 'ialdch9l1c',
    #   'extendedRequestId': 'Fc2EtHe8IAMFcuQ=',
    #   'httpMethod': 'GET',
    #   'identity': {'accessKey': None,
    #   'accountId': None,
    #   'caller': None,
    #   'cognitoAuthenticationProvider': None,
    #   'cognitoAuthenticationType': None,
    #   'cognitoIdentityId': None,
    #   'cognitoIdentityPoolId': None,
    #   'sourceIp': '209.17.40.44',
    #   'user': None,
    #   'userAgent': 'Mozilla/5.0 (X11; Linux '
    #   'x86_64) AppleWebKit/537.36 '
    #   '(KHTML, like Gecko) '
    #   'Chrome/61.0.3163.100 '
    #   'Safari/537.36',
    #   'userArn': None},
    #   'path': '/api/v1/',
    #   'protocol': 'HTTP/1.1',
    #   'requestId': '834b8a37-41b2-11e8-be31-ff62898f7c19',
    #   'requestTime': '16/Apr/2018:20:12:39 +0000',
    #   'requestTimeEpoch': 1523909559756,
    #   'resourceId': 'a97ppd',
    #   'resourcePath': '/{proxy+}',
    #   'stage': 'Default'},
    #   'resource': '/{proxy+}',
    #   'stageVariables': None
    # }
    @logFunc()
    def isFlaskEvent(self, event):  # pylint: disable=R0201
        """
        Return True if event is for flask
        """
        httpMethod = event.get("httpMethod", None)
        return (httpMethod != None)

    @logFunc()
    def createFlaskRequest(self, event, context):  # pylint: disable=R0201
        """
        Create a Flask request from an API Gateway event
        Args
          event:
            API Gateway Event
          context:
            Lambda context
        """
        httpMethod        = event.get("httpMethod")
        pathParams        = event.get("pathParameters")  # pylint: disable=W0612
        queryStringParams = event.get("queryStringParameters", None)
        headers           = event.get("headers", {})
        body              = event.get("body", None)
        bodyStr           = ""
        if body:
            bodyStr       = body.encode("utf-8")
            body          = six.BytesIO(bodyStr)
        path              = urls.url_unquote(event.get("path"))
        queryString       = ""
        if queryStringParams:
            queryString   = urlencode(queryStringParams)
        serverName        = "LambdaFlask"
        xForwardedPort    = headers.get("X-Forwarded-Port", "80")
        xForwardedFor     = headers.get("X-Forwarded-For", "")
        remoteAddr        = "127.0.0.1"
        if "," in xForwardedFor:
            # Last IP is cloudfront
            # 2nd to last IP is client
            remoteAddr    = xForwardedFor.split(", ")[-2]

        # Content-Type can come in either case
        contentType       = headers.get("Content-Type", None)
        if not contentType:
            contentType   = headers.get("content-type", "")
        contentLength     = "0"
        if body:
            contentLength = str(len(bodyStr))

        environ = {
            "HTTPS": "on",
            "wsgi.url_scheme": "https",
            "wsgi.version": (1, 0),
            "wsgi.input": body,
            "wsgi.errors": sys.stderr,
            "wsgi.multiprocess": False,
            "wsgi.multithread": False,
            "wsgi.run_once": False,
            "PATH_INFO": path,
            "QUERY_STRING": queryString,
            "REMOTE_ADDR": remoteAddr,
            "REQUEST_METHOD": httpMethod,
            "SERVER_NAME": serverName,
            "SERVER_PORT": xForwardedPort,
            "SERVER_PROTOCOL": str("HTTP/1.1"),
            "lambda.context": context,
            "CONTENT_TYPE": contentType,
            "CONTENT_LENGTH": contentLength
        }

        for header in headers:
            environHeader = "HTTP_{}".format(header.upper().replace("-", "_"))
            environ[environHeader] = str(headers[header])
        # logger.info("environ={} pathParams={} xForwardedFor={}".format(
        #     pprint.pformat(environ), pathParams, xForwardedFor))
        return environ

    @logFunc()
    def executeFlaskRequest(self, environ):
        """
        Execute Flask Request
        Args
          environ:
            WSGI environ dictionary
        Returns
          API Gateway Response dictionary
        """
        resp = {}
        try:
            flaskResp = Response.from_app(self.flaskAppObject, environ)
            resp["statusCode"] = flaskResp.status_code
            resp["body"]       = flaskResp.get_data(as_text=True)
            resp["headers"]    = {}
            for key, value in flaskResp.headers:
                resp["headers"][key] = value
        except Exception as e:  # pylint: disable=W0703
            exc_info = sys.exc_info()
            logger.exception(pprint.pformat(e.__dict__))
            resp["statusCode"] = 500
            body               = {
                "message": "Uncaught exception in flask app",
                "traceback": traceback.format_exception(*exc_info)
            }
            resp["body"]       = json.dumps(str(body), indent=4)
        return resp


# ------------------------ HANDLER ------------------------------------------ #
def handler(event, context):
    """
    Handle AWS Event
    """
    lh = LambdaHandler()
    return lh.handler(event, context)
