#!/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
 Beblsoft Flask Handler Callbacks Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger()


# ------------------------ CALLBACK FUNCTIONS ------------------------------- #
@logFunc()
def keepWarm(*args, **kwargs):  # pylint: disable=W0613
    """
    Keep the Server Warm
    """
    pass
