#!/usr/bin/env python3
"""
NAME:
 bMessage.py

DESCRIPTION
 Beblsoft SQS Message Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SQS MESSAGE ------------------------------ #
class BeblsoftSQSMessage():
    """
    Beblsoft SQS Message
    """

    def __init__(self, bSQSQueue, id, receiptHandle=None, body=None, bodyMD5=None,  # pylint: disable=W0622
                 attributeDict=None, msgAttributeDict=None, msgAttributeDictMD5=None,
                 sequenceNumber=None):
        """
        Initialize Object
        Args
          bSQSQueue:
            BeblsoftSQSQueue object
          id:
            Message ID
            Ex. "c80e8021-a70a-42c7-a470-796e1186f753"
          receiptHandle:
            An identifier associated with the act of receiving the message
            Ex. "AQEBJQ+/u6NsnT5t8Q/VbVxgdUl4TMKZ5FqhksRdIQvLBhwNvADoBxYSOVeCBXdnS9P+erlTtwEALHsnBXynkfPLH3BOUqmgzP25U8kl8eHzq6RAlzrSOfTO8ox9dcp6GLmW33YjO3zkq5VRYyQlJgLCiAZUpY2D4UQcE5D1Vm8RoKfbE+xtVaOctYeINjaQJ1u3mWx9T7tork3uAlOe1uyFjCWU5aPX/1OHhWCGi2EPPZj6vchNqDOJC/Y2k1gkivqCjz1CZl6FlZ7UVPOx3AMoszPuOYZ+Nuqpx2uCE2MHTtMHD8PVjlsWirt56oUr6JPp9aRGo6bitPIOmi4dX0FmuMKD6u/JnuZCp+AXtJVTmSHS8IXt/twsKU7A+fiMK01NtD5msNgVPoe9JbFtlGwvTQ=="
          body:
            The message's contents (not URL-encoded)
            Ex. "{\"foo\":\"bar\"}"
          bodyMD5:
            An MD5 digest of the non-URL-encoded message body string
            Ex. "9bb58f26192e4ba00f01e2e7b136bbd8"
          attributeDict:
            A map of the attributes requested in ReceiveMessage
            Ex. {
              "ApproximateReceiveCount": "3",
              "SentTimestamp": "1529104986221",
              "SenderId": "594035263019",
              "ApproximateFirstReceiveTimestamp": "1529104986230"
            }
          msgAttributeDict:
            Attribut Dict. Each key has sub dict with of Name, Type, and Value
            Ex. {}
          msgAttributeDictMD5:
            An MD5 digest of the non-URL-encoded message attribute string
          sequenceNumber:
            FIFO only
            Message sequence number
        """
        self.bSQSQueue           = bSQSQueue
        self.id                  = id
        self.receiptHandle       = receiptHandle
        self.body                = body
        self.bodyMD5             = bodyMD5
        self.attributeDict       = attributeDict
        self.msgAttributeDict    = msgAttributeDict
        self.msgAttributeDictMD5 = msgAttributeDictMD5
        self.sequenceNumber      = sequenceNumber
        self.sqsClient           = self.bSQSQueue.sqsClient

    def __str__(self):
        return "[{} ID={} ReceiptHandle={}]".format(
            self.__class__.__name__, self.id, self.receiptHandle)

    # -------------------------- VERBS -------------------------------------- #
    @staticmethod
    @logFunc()
    def send(bSQSQueue, body, delayS=0, msgAttributeDict=None,
             deduplicationID=None, groupID=None):
        """
        Send Object
        Args
          See https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sqs.html#SQS.Client.send_message

        Returns
          BeblsoftSQSMessage without receiptHandle
        """
        kwargs                               = {}
        kwargs["QueueUrl"]                   = bSQSQueue.url
        kwargs["MessageBody"]                = body
        kwargs["DelaySeconds"]               = delayS
        if msgAttributeDict:
            kwargs["MessageAttributes"]      = msgAttributeDict
        if deduplicationID:
            kwargs["MessageDeduplicationId"] = deduplicationID
        if groupID:
            kwargs["MessageGroupId"]         = groupID
        resp = bSQSQueue.sqsClient.send_message(**kwargs)
        bSQSMsg = BeblsoftSQSMessage(
            bSQSQueue             = bSQSQueue,
            id                    = resp.get("MessageId"),
            body                  = body,
            bodyMD5               = resp.get("MD5OfMessageBody"),
            msgAttributeDict      = msgAttributeDict,
            msgAttributeDictMD5   = resp.get("MD5OfMessageAttributes"),
            sequenceNumber        = resp.get("SequenceNumber", None))
        logger.debug("{} Sent".format(bSQSMsg))
        return bSQSMsg

    @staticmethod
    @logFunc()
    def receive(bSQSQueue, attributeNames=["All"], messageAttributeNames=["All"],  # pylint: disable=W0102
                maxNumberOfMessages=1, visibilityTimeoutS=None, waitTimeS=30,
                receiveRequestAttemptId=None):
        """
        Receive Object
        Args
          bSQSQueue:
            BeblsoftSQSQueue object
          attributeNames:
            A list of s that need to be returned along with each message
            Ex. ["All"]
          messageAttributeNames:
            The name of the message attribute, where N is the index
            Ex. ["All"]
          maxNumberOfMessages:
            The maximum number of messages to return.
            Min = 1, Max = 10
          visibilityTimeoutS:
            The duration (in seconds) that the received messages are hidden
            from subsequent retrieve requests after being retrieved
            Ex1. None
            Ex2. 30
          waitTimeS:
            The duration (in seconds) for which the call waits for a message
            to arrive in the queue before returning.
            Ex. 30
          receiveRequestAttemptId:
            FIFO only
            The token used for deduplication of ReceiveMessage calls
            Ex. None
          See https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sqs.html#SQS.Client.receive_message

        Returns
          List of BeblsoftSQSMessage objects received
        """
        kwargs                                  = {}
        kwargs["QueueUrl"]                      = bSQSQueue.url
        kwargs["AttributeNames"]                = attributeNames
        kwargs["MessageAttributeNames"]         = messageAttributeNames
        kwargs["MaxNumberOfMessages"]           = maxNumberOfMessages
        if visibilityTimeoutS:
            kwargs["VisibilityTimeout"]         = visibilityTimeoutS
        kwargs["WaitTimeSeconds"]               = waitTimeS
        if receiveRequestAttemptId:
            kwargs["ReceiveRequestAttemptId"]   = receiveRequestAttemptId

        # Send Request
        resp        = bSQSQueue.sqsClient.receive_message(**kwargs)
        msgRcvDicts = resp.get("Messages")
        bSQSMsgList = []

        for msgRcvDict in msgRcvDicts:
            bSQSMsg = BeblsoftSQSMessage(
                bSQSQueue            = bSQSQueue,
                id                   = msgRcvDict.get("MessageId"),
                receiptHandle        = msgRcvDict.get("ReceiptHandle"),
                body                 = msgRcvDict.get("Body"),
                bodyMD5              = msgRcvDict.get("MD5OfBody"),
                attributeDict        = msgRcvDict.get("Attributes", None),
                msgAttributeDict     = msgRcvDict.get("MessageAttributes", None),
                msgAttributeDictMD5  = msgRcvDict.get("MD5OfMessageAttributes", None)
            )
            bSQSMsgList.append(bSQSMsg)
        return bSQSMsgList

    @logFunc()
    def delete(self):  # pylint: disable=W0613
        """
        Delete Object
        """
        self.sqsClient.delete_message(
            QueueUrl      = self.bSQSQueue.url,
            ReceiptHandle = self.receiptHandle)

    @logFunc()
    def changeVisibility(self, visibilityTimeoutS=30):  # pylint: disable=W0613
        """
        Change Object Visibility
        """
        self.sqsClient.change_message_visibility(
            QueueUrl          = self.bSQSQueue.url,
            ReceiptHandle     = self.receiptHandle,
            VisibilityTimeout = visibilityTimeoutS)
