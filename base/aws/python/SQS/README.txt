OVERVIEW
===============================================================================
- AWS Simple Queue Service (SQS) Technology Documentation

- Description
  * SQS is a fully managed message queuing service that enables developers to decouple
    and scale microservices, distributed systems, and serverless applications
  * Eliminates the complexity and overhead associated with managing and operating
    message middleware
  * Store, send, and receive messages between software components at any volume,
    without losing messages or requiring other services to be available

- URLS
  * Home                            : https://aws.amazon.com/sqs/
  * Architecture                    : https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-basic-architecture.html



FEATURES
===============================================================================
- Standard Queues -------------------
  * Unlimited throughput            : Nearly unlimited number of transactions per second
  * At-Least-Once Delivery          : A message is delivered at least once
                                      Occasionally more than one copy is delivered
                                      Applications must be idempotent
  * Best-Effort Ordering            : Occassionally, messages deliverd out of order
                                      Applications can put sequencing information inside message

- FIFO Queues -----------------------
  * High throughput                 : 300 transactions per second (1 transaction = max 10 messages)
  * Exactly-once processing         : Message delivered once and available until consumer
                                      processes and deletes it
  * First-in-First-out delivery     : Order of which messages are sent is strictly preserved

- Functionality ---------------------
  * Unlimited queues                : Create unlimited ques in any region
  * Payload size                    : Max payload size is 256 KB
                                      Messages charged in increments of 64KB
  * Batches                         : Send, receive, or delete messages in batches of up to 10 messages or 256KB
                                      Batches cost the same as single messages
  * Long polling                    : Reduce etraneous polling to minimize cost while receiving new
                                      messages as quickly as possible
  * Retention period                : Up to 14 days
  * Simultaneous                    : Send and read messages simultaneously
  * Message locking                 : When a message is received, it becomes "locked" while being processed
                                      This keeps others from processing it simultaneously
                                      If message processing fails, lock expires and message available again
  * Queue sharing                   : Share SQS queues anonymously or with specific AWS accounts
  * Server-side encryption (SSE)    : Messages are encrypted using AWS KMS when at rest
  * Dead Letter Queues (DLQ)        : Handle messages that have not been successfully processed by a consumer
                                      with Dead Letter Queues
                                      Setup separate consumers to process DLQs

- Common Use Cases ------------------
  * Work Queues                     : Decouple components of a distributed application
  * Buffer and Batch Operations     : Add scalability and reliability to architecture
  * Request offloading              : Move slow operations off of interactive request paths
  * Fanout                          : Combine with SNS to send identical copies of a message to multiple
                                      queues in parallel
  * Priority                        : Use separate queues to provide prioritization of work
  * Scalability                     : Easy to scale message processors
  * Resiliency                      : Decouple parts of system so if one part fails, others don't need
                                      to fail as well



BASIC ARCHITECTURE
===============================================================================
- Three main parts                  1 Components of developer system
                                    2 Queues
                                    3 Messages
- Simple timeline
  1 Producer component sends
    Message A to queue              : SQS servers store the message redundantly
  2 Consumer retrieves message A
    from queue                      : While message A is being processed it remains
                                      in the queue and isn't returned to subsequent
                                      requests for the duration of the visibility timeout
  3 Consumer deletes message A      : This prevents the message from being received again

- Polling Types
  Short                             : AWS samples a subset of servers and returns messages from only
                                      those servers
  Long                              : Helps reduce the cost of using SQS by eliminated the number of empty responses
                                      Eliminates false empty responses by querying all AWS SQS servers
                                      Returns messages as soon as they are available


IDENTIFIERS, MESSAGE ATTRIBUTES
===============================================================================
- Identifiers -----------------------
  * Queues                          : Names must be unique to account and region
                                      Fifo queue must end with .fifo suffix
                                      Ex URL: https://sqs.us-east-2.amazonaws.com/123456789012/MyQueue
  * Message ID                      : Each message receives a system-assigned message ID
                                      Max length is 100 characters
  * Receipt Handle                  : Each time message is received it comes with a receipt handle
                                      To delete message or change visibility, must provide receipt handle
                                      Different receits of same message will have different receit handles
                                      Ex. MbZj6wDWli+JvwwJaBV+3dcjk2YW2vA3+STFFljTM8tJJg6HRG6PYSasuWXPJB+CwLj1FjgXUv1uSj1gUPAWV66FU/WeR4mq2OKpEGYWbnLmpRCJVAyeMjeU5ZBdtcQ+QEauMZc8ZRv37sIW2iJKq3M9MFx1YvV11A2x/KSbkJ0=
  * FIFO Message Deduplication ID   : Token used for deduplication of sent messages
  * FIFO Message Group ID           : Message group identifier. Messages in the same group are processed
                                      one by one
  * FIFO Message Sequence Number    : Large, non-consecutive number assigned by SQS

- Message Attributes ----------------
  * Description                     : SQS allows from structured metadata (timestamps, geospatial,...)
                                      with message attributes
                                      Each message can have up to 10 attributes
                                      Optional and separate from message body
  * Attribute Components
    Name                            : Message attribute name. Up to 256 characters
    Type                            : String, Number, Binary, or Custom
    Value                           : Message attribute value



DEAD LETTER QUEUES
===============================================================================
- Description                       : Dead-letter queues are targeted by other queues (source queues)
                                      for messages that can't be processed successfully
                                    : Useful for debugging application or messaging system
- Redrive policy                    : Specifies how the source queue handles messages
                                      Ex. If maxReceiveCount is 5, and consumer receives message
                                      5 times without deleting it, SQS moves message to DLQ
- Restrictions                      : DLQ must be same type (standard, FIFO) as source queue
                                      DLQ must be in same account as source queue
                                      DLQ must be in same region as source queue
- Benefits                          : Isolate failures
                                      Configure alarm for unhandled messages
                                      Analyze DLQ messages to see what went wrong
                                      Prevent failing messages from being overly retried



VISIBILITY TIMEOUT
===============================================================================
- Description                       : When a consumer receives and processes a message froma queue
                                      the message remains in the queue.
                                    : SQS doesn't delete the message until the consumer deletes it
                                    : Immediately after the message is received, it remains in the queue
                                    : To prevent other consumers from processing the message again,
                                      SQS sets a visibility timeout, a period of time during which
                                      SQS prevents other consumers from receiving and processing the
                                      message
                                    : Default = 30 seconds, Minumum = 0 seconds, Maximum = 12 hours
- Picture

        ReceiveMessage     ReceiveMessage                 ReceiveMessage
        Request            Request                        Request
          |                   |                             |
          |--------------------------------------------     |
          v        Visibility Timeout (in seconds)    |     v
    -----------------------------------------------------------------> Time
          |                   |                             |
        Message            Message Not                    Message
        Returned           Returned                       Returned

- Note                              : For standard queues, the visibility timeout isn't a guarantee
                                      against receiving a message twice
- Message States                    1 Sent to a queue by producer
                                      Message is stored
                                    2 Received from queue by consumer
                                      Visibility timeout starts
                                      Message is inflight (max=120,000 inflight)
                                    3 Deleted from queue
- Terminating Visibilty Timeout     : After a message is received by a consumer and the visibility
                                      timeout begins, the ChangeMessageVisibility API allows
                                      one to terminate the message invisibility and make it avaiable
                                      to other consumers



DELAY QUEUES, MESSAGE TIMERS
===============================================================================
- Delay Queues                      : Delay queues let one postpone the delivery of new messages to a queue
                                      for a number of seconds
                                    : Default delay = 0 seconds, Maximum delay = 15 minutes
- Note
  * Standard Queues                 : Delay is not retroactive - changing settings doesn't affect
                                      messages already in the queue
  * FIFO Queues                     : Delay is retroactive - changing settings affects delay of
                                      messages already in the queue
- Picture

        SendMessage     ReceiveMessage      ReceiveMessage     ReceiveMessage     ReceiveMessage
        Request            Request          Request            Request            Request
          |                   |               |                   |                 |
          |-----------------------------      |----------------------------------   |
          v  Delay (in seconds)        |      v  Visibility Timeout (in seconds)|   v
    ------------------------------------------|-------------------------------------|-----> Time
          |                   |               |                   |                 |
        Message            Message Not      Message            Message Not        Message
        Added to Queue     Returned         Returned           Returned           Returned

- Message Timers                    : Message timers let you specify an initial invisibility period
                                      for a message added to a queue
                                    : Message sent with 45 second timer isn't visible for first 45 seconds in queue
                                    : Default seconds = 0, Maximum = 15 minutes



BEST PRACTICES
===============================================================================
- Processing messages in timely
  manner                            : Extend visibility timeout to the maximum time it
                                      takes to process and delete the message
- Handling request errors           : Allow pause (200 ms) before requerying for messages
- Long Polling                      : Eliminates the cost of continues polling
                                    : Use a single thread to poll each queue
- Problematic messages              : Configure dead-letter queue
- Dead letter queue retention       : Set to be much longer than source queue retention
- Dead letter queue max receives    : On standard queues, avoid setting maximum receives to 1 when
                                      a dead-letter queue is configured
                                      If a ReceiveMessage call fails, message might be moved straight to
                                      dead-letter queue
- Request-response systems          : Don't create reply queues per message
                                      Create reply queues on startup or per producer
                                      Don't let producers share reply queues
- Batching Message Actions          : Send, Receive, and Delete messages in batches for less cost