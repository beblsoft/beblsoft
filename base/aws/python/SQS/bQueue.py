#!/usr/bin/env python3
"""
NAME:
 bQueue.py

DESCRIPTION
 Beblsoft SQS Queue Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SQS QUEUE -------------------------------- #
class BeblsoftSQSQueue(BeblsoftAWSObject):
    """
    Beblsoft SQS Queue
    """

    def __init__(self,
                 name,
                 delayS = 0,
                 maxMessageSizeB = 262144,  # 256KB
                 messageRetentionS = 345600,  # 4 days,
                 receiveMessageWaitS = 0,
                 visibilityTimeoutS = 30,
                 fifoQueue = False,
                 contentBasedDeduplication = False,
                 bSQSDeadLetterQueue = None,
                 maxReceiveCount = 3,
                 bKMSCMKey = None,
                 kmsDataKeyReuseS = 300,  # 5 minutes
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            Queue name
            ex. "smecknTestQueue"
          delayS:
            Length of time (in seconds), that queue delays message delivery
            An integer from 0 to 900 seconds (15 minutes)
          maxMessageSizeB:
            Maximum message size in bytes
            An integer from 1024 (1KiB) to 262144 (256KiB)
          messageRetentionS:
            Length of time (in seconds) SQS retains message
            Integer from 60 seconds to 1209600 seconds (14 days)
          receiveMessageWaitS:
            Length of time (in seconds) ReceiveMessage action waits for message to arrive
            Integer from 0 to 20
          visibilityTimeoutS:
            Queue visibility timeout (in seconds)
            Integer from 0 to 43200 (12 hours)
          fifoQueue:
            If True, queue is a fifo queue
          contentBasedDeduplication:
            If True, enables content-based deduplication
          bSQSDeadLetterQueue:
            BeblsoftSQSQueue object to use as dead letter queue
          maxReceiveCount:
            Number of times a message is delivered to source queue before being moved to dead letter queue
            Ex. 3
          bKMSCMKey:
            BeblsoftKMSCMKey object
          kmsDataKeyReuseS:
            Length of time (in seconds) for which SQS can reuse data key to encrypt and decrypt messages
            beforing calling KMS again
            Integer between 60 and 86400 (24 hours)
        """
        super().__init__()
        self.name                      = name
        self.delayS                    = delayS
        self.maxMessageSizeB           = maxMessageSizeB
        self.messageRetentionS         = messageRetentionS
        self.receiveMessageWaitS       = receiveMessageWaitS
        self.visibilityTimeoutS        = visibilityTimeoutS
        self.fifoQueue                 = fifoQueue
        self.contentBasedDeduplication = contentBasedDeduplication
        self.bSQSDeadLetterQueue       = bSQSDeadLetterQueue
        self.maxReceiveCount           = maxReceiveCount
        self.bKMSCMKey                 = bKMSCMKey
        self.kmsDataKeyReuseS          = kmsDataKeyReuseS
        self.bRegion                   = bRegion
        self.sqsClient                 = boto3.client("sqs",
                                                      region_name=bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def url(self):
        """
        Return object url
        """
        url = None
        try:
            resp = self.sqsClient.get_queue_url(QueueName = self.name)
            url  = resp.get("QueueUrl")
        except ClientError as e:
            if  e.response["Error"]["Code"] == "AWS.SimpleQueueService.NonExistentQueue":
                pass
            else:
                raise e
        return url

    @property
    def arn(self):
        """
        Return object arn
        """
        return self.getValueFromMetadata("QueueArn")

    # -------------------------- HELPERS ------------------------------------ #
    @logFunc()
    def getAttributeDict(self, includeFifo=True):
        """
        Return attributes
        """
        a                                       = {}
        a["DelaySeconds"]                       = self.delayS
        a["MaximumMessageSize"]                 = self.maxMessageSizeB
        a["MessageRetentionPeriod"]             = self.messageRetentionS
        a["ReceiveMessageWaitTimeSeconds"]      = self.receiveMessageWaitS
        a["VisibilityTimeout"]                  = self.visibilityTimeoutS
        if self.bKMSCMKey:
            a["KmsMasterKeyId"]                 = self.bKMSCMKey.id
            a["KmsDataKeyReusePeriodSeconds"]   = self.kmsDataKeyReuseS
        if includeFifo and self.fifoQueue:  # Note: AWS rejected "FifoQueue Key"
            a["FifoQueue"]                      = "true" if self.fifoQueue else "false"
            a["ContentBasedDeduplication"]      = self.contentBasedDeduplication
        if self.bSQSDeadLetterQueue:
            arn                  = self.bSQSDeadLetterQueue.arn
            rcvCnt               = self.maxReceiveCount
            a["RedrivePolicy"]   = "{"
            a["RedrivePolicy"]  += """ "deadLetterTargetArn":"{}", """.format(arn)
            a["RedrivePolicy"]  += """ "maxReceiveCount":"{}" """.format(rcvCnt)
            a["RedrivePolicy"]  += "}"

        for k,v in a.items():  # All values must be strings
            a[k] = str(v)
        return a

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):  # pylint: disable=W0613
        """
        Create object
        """
        self.sqsClient.create_queue(
            QueueName  = self.name,
            Attributes = self.getAttributeDict())

    @logFunc()
    def _delete(self, **kwargs):  # pylint: disable=W0613
        """
        Delete object
        """
        self.sqsClient.delete_queue(
            QueueUrl = self.url)

    @logFunc()
    def _update(self, **kwargs):  # pylint: disable=W0613
        """
        Update object
        """
        self.sqsClient.set_queue_attributes(
            QueueUrl   = self.url,
            Attributes = self.getAttributeDict(includeFifo=False))

    @logFunc()
    def _purge(self, **kwargs):
        """
        Purge object
        """
        self.sqsClient.purge_queue(
            QueueUrl = self.url)

    @staticmethod
    @logFunc()
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all
        """
        sqsClient = boto3.client("sqs", region_name=bRegion.name)
        resp = sqsClient.list_queues()
        urls = resp.get("QueueUrls")
        logger.info("Queue URLs:\n{}".format(pprint.pformat(urls)))

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sqs.html#SQS.Client.get_queue_attributes        """
        meta = None
        url  = self.url

        if url:
            try:
                resp = self.sqsClient.get_queue_attributes(
                    QueueUrl=url, AttributeNames=["All"])
            except ClientError as e:
                raise e
            else:
                meta = resp.get("Attributes")
        return meta
