#!/usr/bin/env python3
"""
NAME:
 bRegion.py

DESCRIPTION
 Beblsoft Region Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import pprint
import boto3
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT REGION ----------------------------------- #
class BeblsoftRegion():
    """
    Beblsoft Region
    """

    def __init__(self, name):
        """
        Initialize Object
        Args
          name: region name
        """
        self.name       = name
        self.ec2Client  = boto3.client("ec2", region_name=self.name)
        self.ec2Res     = boto3.resource("ec2", region_name=self.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def describeAZs(self):
        """
        Describe availability zones
        """
        filt = {
            "Name": "region-name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_availability_zones(Filters=[filt])
        azs  = resp.get("AvailabilityZones", None)
        logger.info("Availability Zones:\n{}".format(pprint.pformat(azs)))

    @staticmethod
    def getDefault():
        """
        Return default region
        """
        session = boto3.session.Session()
        return BeblsoftRegion(name=session.region_name)

    @staticmethod
    def getCurrent():
        """
        Return the current region
        Useful for lambda functions
        """
        return BeblsoftRegion(name=os.environ["AWS_DEFAULT_REGION"])

    @staticmethod
    def describeAll():
        """
        Describe all regions
        """
        ec2Client = boto3.client("ec2")
        resp = ec2Client.describe_regions()
        regions = resp.get("Regions", None)
        logger.info("Regions:\n{}".format(pprint.pformat(regions)))
