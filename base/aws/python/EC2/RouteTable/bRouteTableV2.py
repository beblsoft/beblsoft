#!/usr/bin/env python3
"""
NAME:
 bRouteTableV2.py

DESCRIPTION
 Beblsoft Route Table Version 2 Functionality

UPGRADES OVER V1
 The BeblsoftRouteTableV2 Object (Version 2) doesn't create routes inside the
 route table object. Routes are delegated to their own objects (See the Route
 directory)
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT ROUTE TABLE V2 --------------------------- #
class BeblsoftRouteTableV2(BeblsoftAWSObject):
    """
    Beblsoft Route Table V2
    """

    def __init__(self, name, bSubnet):
        """
        Initialize Object
        Args
          name:
            Route Table name
          bSubnet:
            Beblsoft Subnet object
            Route table will only be associated with one subnet
        """
        super().__init__()
        self.name        = name
        self.bSubnet     = bSubnet
        self.bVPC        = bSubnet.bVPC
        self.bRegion     = self.bVPC.bRegion
        self.ec2Client   = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res      = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("RouteTableId")

    @property
    def associationId(self):  # pylint: disable=C0111
        meta = self.getMetadata()
        return meta.get("Associations")[0].get("RouteTableAssociationId")

    @property
    def routes(self):  # pylint: disable=C0111
        """
        Return [] or list of routes
        """
        routes = []
        meta   = self.getMetadata()
        if meta:
            routes = meta.get("Routes", [])
        return routes


    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, sleepS=2): #pylint: disable=W0221
        """
        Create object, store name as a tag attribute
        """
        nameTag = {
            "Key": "Name",
            "Value": self.name
        }
        resp  = self.ec2Client.create_route_table(VpcId=self.bVPC.id)
        rt    = resp.get("RouteTable")
        rtID  = rt.get("RouteTableId", None)
        time.sleep(sleepS) #Time for AWS changes to propagate
        rtRes = self.ec2Res.RouteTable(rtID) #pylint: disable=E1101
        rtRes.create_tags(Tags=[nameTag])
        self.ec2Client.associate_route_table(
            RouteTableId=self.id, SubnetId=self.bSubnet.id)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.ec2Client.disassociate_route_table(AssociationId=self.associationId)
        self.ec2Client.delete_route_table(RouteTableId=self.id)

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all route tables
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_route_tables()
        rts  = resp.get("RouteTables", None)
        logger.info("RouteTables:\n{}".format(pprint.pformat(rts)))

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_route_tables
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_route_tables(Filters=[filt])
        rts  = resp.get("RouteTables", None)
        if rts:
            meta = rts[0]
        return meta
