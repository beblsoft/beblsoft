#!/usr/bin/env python3
"""
NAME:
 bRouteTable.py

DESCRIPTION
 Beblsoft Route Table Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT ROUTE TABLE ------------------------------ #
class BeblsoftRouteTable(BeblsoftAWSObject):
    """
    Beblsoft Route Table
    """

    def __init__(self, name, bVPC, bSubnet,
                 igDestCidr="0.0.0.0/0", bInternetGateway=None):
        """
        Initialize Object
        Args
          name:
            Route Table name
          bVPC:
            Beblsoft VPC object
          bSubnet:
            Beblsoft Subnet object
            Route table will only be associated with one subnet
          igDestCidr:
            CIDR to forward to internet gateway
          bInternetGateway:
            Beblsoft Internet Gateway object
        """
        super().__init__()
        self.name             = name
        self.bVPC             = bVPC
        self.bSubnet          = bSubnet
        self.igDestCidr       = igDestCidr
        self.bInternetGateway = bInternetGateway
        self.bRegion          = bVPC.bRegion
        self.ec2Client        = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res           = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("RouteTableId")

    @property
    def associationId(self):  # pylint: disable=C0111
        meta = self.getMetadata()
        return meta.get("Associations")[0].get("RouteTableAssociationId")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, associate=True, createRoutes=True):  # pylint: disable=W0221
        """
        Create object, store name as a tag attribute.
        Args
          associate:
            if True, associate route table with subnet
          createRoutes:
            If True, create route table routes
        """
        nameTag = {
            "Key": "Name",
            "Value": self.name
        }
        resp   = self.ec2Client.create_route_table(VpcId=self.bVPC.id)
        rt     = resp.get("RouteTable")
        rtID   = rt.get("RouteTableId", None)
        rtRes  = self.ec2Res.RouteTable(rtID) #pylint: disable=E1101
        rtRes.create_tags(Tags=[nameTag])
        if associate:
            self.ec2Client.associate_route_table(RouteTableId=self.id,
                                                 SubnetId=self.bSubnet.id)
        if createRoutes:
            self.createRoutes()

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.ec2Client.disassociate_route_table(AssociationId=self.associationId)
        self.ec2Client.delete_route_table(RouteTableId=self.id)

    @logFunc()
    def createRoutes(self):
        """
        Create routes associated with route table
        """
        if self.bInternetGateway:
            self.ec2Client.create_route(
                RouteTableId=self.id, DestinationCidrBlock=self.igDestCidr,
                GatewayId=self.bInternetGateway.id)
            logger.info("{} Created route from {} to {}".format(
                self, self.igDestCidr, self.bInternetGateway.id))

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all route tables
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_route_tables()
        rts  = resp.get("RouteTables", None)
        logger.info("RouteTables:\n{}".format(pprint.pformat(rts)))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_route_tables
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_route_tables(Filters=[filt])
        rts  = resp.get("RouteTables", None)
        if rts:
            meta = rts[0]
        return meta
