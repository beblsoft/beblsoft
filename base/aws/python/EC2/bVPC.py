#!/usr/bin/env python3
"""
NAME:
 bVPC.py

DESCRIPTION
 Beblsoft Virtual Private Cloud Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import time
from datetime import datetime, timedelta
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT VIRTUAL PRIVATE CLOUD -------------------- #
class BeblsoftVPC(BeblsoftAWSObject):
    """
    Beblsoft Virtual Private Cloud
    """

    def __init__(self, name, cidrBlock="10.0.0.0/16",
                 enableDnsHostnames=False, enableDnsSupport=False,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            VPC name
            ex. "MojourneyVPC"
          cidrBlock:
            Classless Inter Domain Routing Block
            ex. "10.0.0.0/16"
          enableDnsHostnames:
            Indicates whether the instances launched in the VPC get public DNS hostnames
          enableDnsSupport
            Indicates whether the DNS resolution is supported for the VPC
        """
        super().__init__()
        self.name               = name
        self.nameTag            = {
            "Key": "Name",
            "Value": self.name
        }
        self.cidrBlock          = cidrBlock
        self.enableDnsHostnames = enableDnsHostnames
        self.enableDnsSupport   = enableDnsSupport
        self.bRegion            = bRegion
        self.ec2Client          = boto3.client("ec2", region_name=bRegion.name)
        self.ec2Res             = boto3.resource("ec2", region_name=bRegion.name)

    def __str__(self):
        return "[{} Name={} Cidr={}]".format(
            self.__class__.__name__, self.name, self.cidrBlock)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self): #pylint: disable=C0111
        return self.getValueFromMetadata("VpcId")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create Object
        """
        resp   = self.ec2Client.create_vpc(CidrBlock=self.cidrBlock)
        vpc    = resp.get("Vpc", None)
        vpcId  = vpc.get("VpcId", None)
        vpcRes = self.ec2Res.Vpc(vpcId) #pylint: disable=E1101
        vpcRes.create_tags(Tags=[self.nameTag])
        vpcRes.modify_attribute(EnableDnsSupport={"Value": self.enableDnsSupport})
        vpcRes.modify_attribute(EnableDnsHostnames={"Value": self.enableDnsHostnames})
        waiter = self.ec2Client.get_waiter("vpc_available")
        waiter.wait(VpcIds=[vpcId])

    @logFunc()
    def _delete(self, sleepS=1, totalS=20): #pylint: disable=W0221
        """
        Delete Object
        """
        vpcID     = self.id
        startTime = datetime.now()
        deleted   = False
        while not deleted:
            try:
                self.ec2Client.delete_vpc(VpcId=vpcID)
                deleted = True
            except ClientError as e:
                message = e.response["Error"]["Message"]
                hasDeps = "The vpc '{}' has dependencies".format(vpcID) in message
                curTime = datetime.now()
                if curTime - startTime > timedelta(seconds=totalS):
                    logger.exception(pprint.pformat(e.__dict__))
                    raise(e)
                elif hasDeps:
                    # Retry
                    time.sleep(sleepS)
                else:
                    raise(e)
        self.ec2Client.delete_tags(Resources=[vpcID], Tags=[self.nameTag])

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all VPCs
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_vpcs()
        vpcs = resp.get("Vpcs", None)
        logger.info("VPCs:\n{}".format(pprint.pformat(vpcs,)))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get Object Metadata
        Returns
          None or metadata here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_vpcs
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_vpcs(Filters=[filt])
        vpcs = resp.get("Vpcs", None)
        if vpcs:
            meta = vpcs[0]
        return meta
