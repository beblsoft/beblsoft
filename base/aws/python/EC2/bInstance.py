#!/usr/bin/env python3
"""
NAME:
 bInstance.py

DESCRIPTION
 Beblsoft Instance Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT INSTANCE --------------------------------- #
class BeblsoftInstance(BeblsoftAWSObject):
    """
    Beblsoft Instance

    Logging Into Instance
      Gen   : ssh -i <keypairfile> <user>@<publicDNS>
      Amazon: ssh -i /tmp/jbenssonKeyPair.pem ec2-user@ec2-34-204-191-94.compute-1.amazonaws.com
      Ubuntu: ssh -i /tmp/jbenssonKeyPair.pem ubuntu@ec2-34-204-191-94.compute-1.amazonaws.com

    Instance Metadata
      meta data: curl http://169.254.169.254/latest/meta-data
      user data: curl http://169.254.169.254/latest/user-data
    """

    def __init__(self, name, imageId, bSubnet, bKeyPair, bSecurityGroup,
                 instanceType="t2.nano", monitoring=False, userDataFunc = lambda _: None):
        """
        Initialize Object
        Args
          name:
            instance name, stored as tag attribute
          imageId:
            Amazon Machine Image (AMI)
            To search for AMIs:
              https://console.aws.amazon.com/ec2
              > AMIs > Public/Private > Owner: Amazon images
              > Platform: Amazon Linux > Architecture: 64-bit
              > Root Device Type: EBS > Virtualization Type: HVM
              https://aws.amazon.com/marketplace
              > Operating Systemts > Select OS via filters
              > Continue > Manual Launch > Examine AMI
            ex. ami-fd8617eb - amzn-ami-hvm-2017.03.0.20170417-x86_64-ebs
          bSubnet:
            Beblsoft subnet object
          bKeyPair:
            Beblsoft key pair object
          instanceType:
            hardware on which to run instance
            ex. "t2.nano"
          userDataFunc:
            Function to return MIME user data string.
            See: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
            Ex. lambda _:None
        """
        super().__init__()
        self.name           = name
        self.nameTag        = {
            "Key": "Name",
            "Value": self.name
        }
        self.imageId        = imageId
        self.bSubnet        = bSubnet
        self.bKeyPair       = bKeyPair
        self.bSecurityGroup = bSecurityGroup
        self.instanceType   = instanceType
        self.monitoring     = monitoring
        self.userDataFunc   = userDataFunc
        self.bRegion        = bSubnet.bRegion
        self.ec2Client      = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res         = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={} ImageId={}]".format(self.__class__.__name__, self.name, self.imageId)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata(key="InstanceId")

    @property
    def domainName(self):  # pylint: disable=C0111
        return self.getValueFromMetadata(key="PublicDnsName")

    @property
    def state(self):  # pylint: disable=C0111
        return self.getValueFromMetadata(key="State.Name")

    @property
    def online(self):
        """
        Return True is object is running.
        """
        state    = None
        metadata = self.getMetadata()
        if metadata:
            state = metadata.get("State").get("Name")
        return state == "running"

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Launch instance
        """
        kwargs                     = {}
        kwargs["ImageId"]          = self.imageId
        kwargs["InstanceType"]     = self.instanceType
        kwargs["KeyName"]          = self.bKeyPair.name
        kwargs["MaxCount"]         = 1
        kwargs["MinCount"]         = 1
        kwargs["Monitoring"]       = {"Enabled": self.monitoring}
        kwargs["SecurityGroupIds"] = [self.bSecurityGroup.id]
        kwargs["SubnetId"]         = self.bSubnet.id
        userData                   = self.userDataFunc()
        if userData:
            kwargs["UserData"]     = userData

        resp       = self.ec2Client.run_instances(**kwargs)
        instances  = resp.get("Instances", None)
        instance   = instances[0]
        instanceId = instance.get("InstanceId", None)
        instRes    = self.ec2Res.Instance(instanceId) #pylint: disable=E1101
        instRes.create_tags(Tags=[self.nameTag])

        waiter = self.ec2Client.get_waiter("instance_running")
        waiter.wait(InstanceIds=[instanceId])

    @logFunc()
    def _delete(self, **kwargs):
        """
        Terminate instance
        """
        instID = self.id
        self.ec2Client.terminate_instances(InstanceIds=[self.id])
        waiter = self.ec2Client.get_waiter("instance_terminated")
        waiter.wait(InstanceIds=[self.id])
        self.ec2Client.delete_tags(Resources=[instID], Tags=[self.nameTag])

    @logFunc()
    def _update(self, **kwargs):
        """
        Update object
        """
        kwargs                     = {}
        userData                   = self.userDataFunc()
        if userData:
            kwargs["UserData"]     = {"Value": userData}
        self.ec2Client.modify_attribute(**kwargs)
        waiter = self.ec2Client.get_waiter("instance_status_ok")
        waiter.wait(InstanceIds=[self.id])

    @logFunc()
    def _start(self, **kwargs):
        """
        Start object
        """
        self.ec2Client.start_instances(InstanceIds=[self.id])
        waiter = self.ec2Client.get_waiter("instance_running")
        waiter.wait(InstanceIds=[self.id])

    @logFunc()
    def _stop(self, **kwargs):
        """
        Stop object
        """
        self.ec2Client.stop_instances(InstanceIds=[self.id])
        waiter = self.ec2Client.get_waiter("instance_stopped")
        waiter.wait(InstanceIds=[self.id])

    @logFunc()
    def _reboot(self, **kwargs):
        """
        Reboot object
        """
        self.ec2Client.reboot_instances(InstanceIds=[self.id])
        waiter = self.ec2Client.get_waiter("instance_status_ok")
        waiter.wait(InstanceIds=[self.id])

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault(), maxResultsPerCall=500, vpcID=None):
        """
        Describe all EC2 instances

        Args
          bRegion
            Beblsoft Region of where to search for instances
          maxResultsPerCall
            How many results to return in each call to describe_instances
            Between 5 and 1000
            Ex. 500
          vpcID:
            Only return instances in this VPC
            Ex. "vpc-0134661a86asdfasdf"

        Returns
          [] or List of instance metadata described here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_instances
        """
        ec2Client        = boto3.client("ec2", region_name=bRegion.name)
        filterList       = []
        instanceMetaList = []
        keepRequesting   = True
        nextToken        = None
        if vpcID is not None:
            filterList.append({
                "Name": "vpc-id",
                "Values": [vpcID]
            })

        # Keep requesting until all results
        while keepRequesting:
            resp           = ec2Client.describe_instances(Filters=filterList, MaxResults=maxResultsPerCall)
            nextToken      = resp.get("NextToken", "")
            keepRequesting = nextToken != ""
            reservations   = resp.get("Reservations", [])
            for reservation in reservations:
                instanceMetaList += reservation.get("Instances", [])
        return instanceMetaList

    @staticmethod
    def deleteIDList(idList, bRegion=BeblsoftRegion.getDefault()):
        """
        Terminate list of instances by ID

        Args:
          bRegion
            Beblsoft Region of where to search for instances
          idList
            List of instance IDs to terminate
            Ex. ["i-02eee1094e9090312"]
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        if idList:
            ec2Client.terminate_instances(InstanceIds=idList)
            logger.info("Deleted instance ids: {}".format(idList))

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_instances
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        insts    = None
        resp     = self.ec2Client.describe_instances(Filters=[filt])
        reservs  = resp.get("Reservations", None)
        if reservs:
            insts = reservs[0].get("Instances", None)
        if insts:
            meta = insts[0]
        return meta
