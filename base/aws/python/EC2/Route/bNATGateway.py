#!/usr/bin/env python3
"""
NAME:
 bNATGateway.py

DESCRIPTION
 Beblsoft NAT Gateway Route Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.EC2.Route.bRoute import BeblsoftRoute


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT NAT GATEWAY ROUTE ------------------------ #
class BeblsoftNATGatewayRoute(BeblsoftRoute):
    """
    Beblsoft NAT Gatway Route
    """

    def __init__(self, bRouteTableV2, destCIDR, bNATGateway):
        """
        Initialize object

        Args
          bNATGateway
            Beblsoft NAT Gateway Object
          Others:
            See BeblsoftRoute
        """
        super().__init__(bRouteTableV2, destCIDR)
        self.bNATGateway = bNATGateway

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def extraCreateKwargs(self):
        return {
            "NatGatewayId" : self.bNATGateway.id
        }
