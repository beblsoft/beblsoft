#!/usr/bin/env python3
"""
NAME:
 bInternetGateway.py

DESCRIPTION
 Beblsoft Internet Gateway Route Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.EC2.Route.bRoute import BeblsoftRoute


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT NAT GATEWAY ROUTE ------------------------ #
class BeblsoftInternetGatewayRoute(BeblsoftRoute):
    """
    Beblsoft Internet Gateway Route
    """

    def __init__(self, bRouteTableV2, destCIDR, bInternetGateway):
        """
        Initialize object

        Args
          bInternetGateway
            Beblsoft NAT Gateway object
          Others:
            See BeblsoftRoute
        """
        super().__init__(bRouteTableV2, destCIDR)
        self.bInternetGateway = bInternetGateway

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def extraCreateKwargs(self):
        return {
            "GatewayId" : self.bInternetGateway.id
        }
