#!/usr/bin/env python3
"""
NAME:
 bRoute.py

DESCRIPTION
 Beblsoft Route Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import abc
import boto3
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT ROUTE ------------------------------------ #
class BeblsoftRoute(BeblsoftAWSObject, abc.ABC):
    """
    Beblsoft Route

    Abstract base class that should be subclassed by each route type
    """

    def __init__(self, bRouteTableV2, destCIDR):
        """
        Initialize object

        Args
          bRouteTableV2:
            Beblsoft Route Table V2 Object
          destCIDR:
            IPv4 CIDR address block used for destination match
            Ex. "0.0.0.0/0"
        """
        super().__init__()
        self.bRouteTableV2 = bRouteTableV2
        self.destCIDR      = destCIDR
        self.bRegion       = self.bRouteTableV2.bRegion
        self.ec2Client     = boto3.client("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} routeTable={} destCIDR={}]".format(
            self.__class__.__name__, self.bRouteTableV2.name, self.destCIDR)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    @abc.abstractmethod
    def extraCreateKwargs(self):
        """
        Return componenet specific ec2Client.create_route kwargs
        """
        pass

    @property
    def state(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("State")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        kwargs  = {
            "DestinationCidrBlock": self.destCIDR,
            "RouteTableId": self.bRouteTableV2.id
        }
        kwargs.update(self.extraCreateKwargs)
        self.ec2Client.create_route(**kwargs)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.ec2Client.delete_route(
            DestinationCidrBlock=self.destCIDR,
            RouteTableId=self.bRouteTableV2.id)

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_route_tables
        """
        meta   = None
        for curRoute in self.bRouteTableV2.routes:
            if curRoute.get("DestinationCidrBlock") == self.destCIDR:
                meta = curRoute
                break
        return meta
