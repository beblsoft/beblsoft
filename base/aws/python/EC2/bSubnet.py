#!/usr/bin/env python3
"""
NAME:
 bSubnet.py

DESCRIPTION
 Beblsoft Subnet Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import pprint
import logging
from datetime import datetime, timedelta
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SUBNET ----------------------------------- #
class BeblsoftSubnet(BeblsoftAWSObject):
    """
    Beblsoft Subnet
    """

    def __init__(self, name, cidrBlock, bVPC, availabiltyZone,
                 publicIpOnLaunch=True):
        """
        Initialize Object
        Args
          name:
            Subnet name
          cidrBlock:
            cidr block
            Ex. "10.0.0.0/24"
          bVPC:
            Beblsoft VPC object
          availabilityZone:
            availability zone to launch subnet
          publicIpOnLaunch:
            If True, instances in subnet should be assigned public IPv4
        """
        super().__init__()
        self.name             = name
        self.nameTag          = {
            "Key": "Name",
            "Value": self.name
        }
        self.cidrBlock        = cidrBlock
        self.bVPC             = bVPC
        self.availabiltyZone  = availabiltyZone
        self.publicIpOnLaunch = publicIpOnLaunch
        self.bRegion          = bVPC.bRegion
        self.ec2Client        = boto3.client("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("SubnetId")

    @property
    def state(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("State")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, sleepS=2):  # pylint: disable=W0221
        """
        Create object
        """
        resp   = self.ec2Client.create_subnet(AvailabilityZone=self.availabiltyZone,
                                              CidrBlock=self.cidrBlock, VpcId=self.bVPC.id)
        sub    = resp.get("Subnet", None)
        subID  = sub.get("SubnetId", None)
        time.sleep(sleepS)  # Small sleep for AWS changes to propagate
        self.ec2Client.modify_subnet_attribute(
            SubnetId=subID, MapPublicIpOnLaunch={"Value": self.publicIpOnLaunch})
        waiter = self.ec2Client.get_waiter("subnet_available")
        waiter.wait(SubnetIds=[subID])
        self.ec2Client.create_tags(Resources=[subID], Tags=[self.nameTag])

    @logFunc()
    def _delete(self, sleepS=1, totalS=30):  # pylint: disable=W0221
        """
        Delete object
        """
        subID     = self.id
        startTime = datetime.now()
        deleted   = False
        while not deleted:
            try:
                self.ec2Client.delete_subnet(SubnetId=subID)
                deleted = True
            except ClientError as e:
                message = e.response["Error"]["Message"]
                hasDeps = "The subnet '{}' has dependencies".format(subID) in message
                curTime = datetime.now()
                if curTime - startTime > timedelta(seconds=totalS):
                    logger.exception(pprint.pformat(e.__dict__))
                    raise(e)
                elif hasDeps:
                    # Retry
                    time.sleep(sleepS)
                else:
                    raise(e)
        self.ec2Client.delete_tags(Resources=[subID], Tags=[self.nameTag])

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all subnets
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_subnets()
        subs = resp.get("Subnets", None)
        logger.info("Subnets:\n{}".format(pprint.pformat(subs)))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_subnets
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_subnets(Filters=[filt])
        subs = resp.get("Subnets", None)
        if subs:
            meta = subs[0]
        return meta
