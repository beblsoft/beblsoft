# EC2 Documentation

Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute
capacity in the cloud. It is designed to make web-scale cloud computing easier for developers.

Relevant URLS:
[Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html),
[Components](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Networking.html),
[Boto3](http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#id647)

## Definitions

- __Virtual Private Cloud__: Logially isolated virtual network inside an AWS datacenter
- __Instance__: Machine running in the cloud
- __Subnet__: Range of IP addresses in VPC
- __Public Subnet__: Subnet with access to internet
- __Private Subnet__: Subnet without access to internet
- __Route__: Rule to specify network traffic direction and destination
- __Route Table__: Table of routes. Each subnet has a default route table
- __Internet Gateway__: Component that allows communication between instances in your VPC and the Internet.
- __Elastic IP__: An Elastic IP address is a static, public IPv4 address designed for dynamic cloud computing.
- __NAT Gateway__: Componenet that enables instances in a private subnet to connect to the internet, but
  prevents the internet from initiating a connection.

## VPC: Private Subnet Configuration

```text
  Subnet 0 Traffic
  Inbound                           : None Allowed
  Outbound                          : None Allowed
   ------------------------------------------------------------------
   | VPC                                                            |
   | CIDR: 10.0.0.0/16                                              |
   |                                                                |
   | ----------------------------------------------                 |
   | | Subnet 0 [Private]                         |                 |
   | | CIDR: 10.0.0.0/24                          |                 |
   | |                                            |                 |
   | | Example Use Case:                          |                 |
   | |  Databases that shouldn't be accessed      |                 |
   | |  from public internet                      |                 |
   | |                                            |                 |
   | | RouteTable                                 |                 |
   | |   Route: 10.0.0.0/16 -> Local VPC          |                 |
   | ----------------------------------------------                 |
   |                                                                |
   ------------------------------------------------------------------
```

## VPC: Internet Gateway Configuration

```text
  Subnet 0 Traffic
  Inbound                           : Allowed
  Outbound                          : Allowed
   ------------------------------------------------------------------
   | VPC                                                            |
   | CIDR: 10.0.0.0/16                                              |
   |                                                                |
   | ----------------------------------------------                 |
   | | Subnet 0 [Public]                          |                 |
   | | CIDR: 10.0.0.0/24                          |                 |
   | |                                            |                 |
   | | Example Use Case:                          |                 |
   | |  Bastian Hosts that need to be logged      |                 |
   | |  Into and need access to internet          |                 |
   | |                                            |                 |
   | | RouteTable                                 |           ------------
   | |   Route: 10.0.0.0/16 -> Local VPC          |           | Internet |
   | |   Route: 10.0.0.0/0  -> Internet Gateway ---<---->---- | Gateway  |
   | ----------------------------------------------           ------------
   |                                                                |
   ------------------------------------------------------------------
```

## VPC: NAT Gateway Configuration

```text
  Subnet 1 Traffic
  Inbound                           : Allowed
  Outbound                          : Allowed

  Subnet 0 Traffic
  Inbound                           : None Allowed
  Outbound                          : Allowed
   ------------------------------------------------------------------
   | VPC                                                            |
   | CIDR: 10.0.0.0/16                                              |
   |                                                                |
   | ----------------------------------------------                 |
   | | Subnet 1 [Public]                          |                 |
   | | CIDR: 10.0.0.0/24                          |                 |
   | |                                            |                 |
   | | RouteTable                                 |           ------------
   | |   Route: 10.0.0.0/16 -> Local VPC          |           | Internet |
   | |   Route: 10.0.0.0/0  -> Internet Gateway ---<---->---- | Gateway  |
   | |                                            |           ------------
   | |                                            |                 |
   | |                          ----------------  |                 |
   | |                          | Elastic IP   |  |                 |
   | |                          | NAT Gateway  |  |                 |
   | |                          ----------------  |                 |
   | |                                       |    |                 |
   | ----------------------------------------|-----                 |
   |                                         |                      |
   |                                         |                      |
   | ----------------------------------------|-----                 |
   | | Subnet 0 [Private]                    |    |                 |
   | | CIDR: 10.0.1.0/24                     |    |                 |
   | |                                       |    |                 |
   | | Example Use Case:                     ^    |                 |
   | |  Instances that need public internet  |    |                 |
   | |  but shouldn't be connected to        |    |                 |
   | |                                       |    |                 |
   | | RouteTable                            |    |                 |
   | |   Route: 10.0.0.0/16 -> Local VPC     |    |                 |
   | |   Route: 10.0.0.0/0  -> NAT Gateway----    |                 |
   | ----------------------------------------------                 |
   |                                                                |
   ------------------------------------------------------------------
```
