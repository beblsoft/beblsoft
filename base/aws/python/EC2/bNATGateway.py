#!/usr/bin/env python3
"""
NAME:
 bNATGateway.py

DESCRIPTION
 Beblsoft NAT Gateway Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import logging
from datetime import datetime
import boto3
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT NAT GATEWAY ------------------------------ #
class BeblsoftNATGateway(BeblsoftAWSObject):
    """
    Beblsoft NAT Gatway
    """

    def __init__(self, name, bSubnet, bAddress):
        """
        Initialize object
        Args
          name:
            NAT Gateway name
            Ex. "SmcecknTestNatGateway"
          bSubnet:
            Beblsoft Subnet Object
          bAddress:
            Beblsoft Address Object
        """
        super().__init__()
        self.name              = name
        self.nameTag           = {
            "Key": "Name",
            "Value": self.name
        }
        self.bSubnet   = bSubnet
        self.bAddress  = bAddress
        self.bVPC      = self.bSubnet.bVPC
        self.bRegion   = self.bVPC.bRegion
        self.ec2Client = boto3.client("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("NatGatewayId")

    @property
    def state(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("State")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, delayS=10, maxAttempts=60):  # pylint: disable=W0221
        """
        Create object
        Args
          maxAttempts:
            Number of times waiter waits
          delayS:
            Delay before waiter requeries state
        """
        resp = self.ec2Client.create_nat_gateway(
            AllocationId=self.bAddress.allocationID,
            ClientToken=str(datetime.now()), SubnetId=self.bSubnet.id)
        natID = resp.get("NatGateway").get("NatGatewayId")
        waiter = self.ec2Client.get_waiter("nat_gateway_available")
        waiter.wait(
            Filters = [{"Name": "nat-gateway-id", "Values": [natID]}],
            WaiterConfig= {
                "Delay": delayS,
                "MaxAttempts": maxAttempts})
        self.ec2Client.create_tags(Resources=[natID], Tags=[self.nameTag])

    @logFunc()
    def _delete(self, delayS=10, maxAttempts=60): #pylint: disable=W0221
        """
        Delete object
        Args
          maxAttempts:
            Number of times waiter waits
          delayS:
            Delay before waiter requeries state
        """
        attempts = 0
        natID    = self.id
        self.ec2Client.delete_nat_gateway(NatGatewayId=natID)
        while self.state != "deleted":
            time.sleep(delayS)
            attempts += 1
            if attempts > maxAttempts:
                raise BeblsoftError(msg="{} Not Deleted".format(self))
        self.ec2Client.delete_tags(Resources=[natID], Tags=[self.nameTag])

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_nat_gateways
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_nat_gateways(Filters=[filt])
        ngs  = resp.get("NatGateways", None)
        if ngs:
            meta = ngs[0]
        return meta
