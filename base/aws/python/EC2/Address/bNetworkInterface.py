#!/usr/bin/env python3
"""
NAME:
 bNetworkInterface.py

DESCRIPTION
 Beblsoft Network Interface Address Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.Address.bAddress import BeblsoftAddress


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT NETWORK INTERFACE ADDRESS ---------------- #
class BeblsoftNetworkInterfaceAddress(BeblsoftAddress):
    """
    Beblsoft Network Interface Address
    """

    def __init__(self, bNetworkInterface, domain="vpc"):
        """
        Initialize object
        Args
          bNetworkInterface:
            Beblsoft NetworkInterface Object
        """
        self.bNetworkInterface = bNetworkInterface
        super().__init__(
            name               = "{}Address".format(bNetworkInterface.name),
            domain             = domain,
            bRegion             = bNetworkInterface.bRegion)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        super()._create(**kwargs)
        self.ec2Client.associate_address(
            AllocationId=self.allocationID,
            NetworkInterfaceId=self.bNetworkInterface.id)


    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.ec2Client.disassociate_address(
            AssociationId=self.associationID)
        super()._delete(**kwargs)
