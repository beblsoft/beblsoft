#!/usr/bin/env python3
"""
NAME:
 bAddress.py

DESCRIPTION
 Beblsoft Address Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT ADDRESS ---------------------------------- #
class BeblsoftAddress(BeblsoftAWSObject):
    """
    Beblsoft Address
    Also known as an AWS Elastic IP
    """

    def __init__(self, name, domain="vpc", bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            Address name
            Ex. "SmcecknTestNATAddress"
          domain:
            One of "vpc" | "standard"
        """
        super().__init__()
        self.name              = name
        self.nameTag           = {
            "Key": "Name",
            "Value": self.name
        }
        self.domain    = domain
        self.bRegion   = bRegion
        self.ec2Client = boto3.client("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def allocationID(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("AllocationId")

    @property
    def associationID(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("AssociationId")

    @property
    def publicIP(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("PublicIp")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):  # pylint: disable=W0221
        """
        Create object
        """
        resp = self.ec2Client.allocate_address(Domain=self.domain)
        aid  = resp.get("AllocationId")
        self.ec2Client.create_tags(Resources=[aid], Tags=[self.nameTag])

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        aid = self.allocationID
        self.ec2Client.delete_tags(Resources=[aid], Tags=[self.nameTag])
        self.ec2Client.release_address(AllocationId=aid)

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_addresses
        """
        meta  = None
        filt  = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp  = self.ec2Client.describe_addresses(Filters=[filt])
        aList = resp.get("Addresses", None)
        if aList:
            meta = aList[0]
        return meta
