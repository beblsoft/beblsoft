#!/usr/bin/env python3
"""
NAME:
 bKeyPair.py

DESCRIPTION
 Beblsoft Key Pair Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import stat
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT KEY PAIR --------------------------------- #
class BeblsoftKeyPair(BeblsoftAWSObject):
    """
    Beblsoft Key Pair

    Logging into EC2 instance:
      ssh -i <keyfile>.pem             ec2-user@<EC2PublicDNS>
      ssh -i /tmp/MojourneyKeyPair.pem ec2-user@ec2-34-229-135-200.compute-1.amazonaws.com
    """

    def __init__(self, name, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name: Key pair name
        """
        super().__init__()
        self.name      = name
        self.pemPath   = "/tmp/{}.pem".format(name)
        self.bRegion   = bRegion
        self.ec2Client = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res    = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={} Path={}]".format(
            self.__class__.__name__, self.name, self.pemPath)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def fingerprint(self):  # pylint: disable=C0111
        return self.getValueFromMetadata(key="KeyFingerprint")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create Object
        """
        resp        = self.ec2Client.create_key_pair(KeyName=self.name)
        fingerprint = resp.get("KeyFingerprint", None)
        material    = resp.get("KeyMaterial", None)
        waiter      = self.ec2Client.get_waiter("key_pair_exists")
        waiter.wait(KeyNames=[self.name])

        # Write material pem to file, Give file read-only permission.
        with open(self.pemPath, "w") as f:
            f.write(material)
        os.chmod(self.pemPath, stat.S_IRUSR)

        logger.info("{} Created:\nFingerprint:{}\nMaterial:\n{}".format(
            self, fingerprint, material))

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete Object
        """
        self.ec2Client.delete_key_pair(KeyName=self.name)
        try:
            os.remove(self.pemPath)
        except FileNotFoundError as e:  # pylint: disable=W0612
            pass

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all key pairs
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp      = ec2Client.describe_key_pairs()
        keyPairs  = resp.get("KeyPairs", None)
        logger.info("Key Pairs:\n{}".format(pprint.pformat(keyPairs)))

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_key_pairs
        """
        meta = None
        try:
            resp = self.ec2Client.describe_key_pairs(KeyNames=[self.name])
            kps  = resp.get("KeyPairs", None)
            if kps:
                meta = kps[0]
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "InvalidKeyPair.NotFound":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
