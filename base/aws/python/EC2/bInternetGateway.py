#!/usr/bin/env python3
"""
NAME:
 bInternetGateway.py

DESCRIPTION
 Beblsoft Internet Gateway Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT INTERNET GATEWAY ------------------------- #
class BeblsoftInternetGateway(BeblsoftAWSObject):
    """
    Beblsoft Internet Gateway
    """

    def __init__(self, name, bVPC):
        """
        Initialize Object
        Args
          name: Internet Gateway name
          bVPC: Beblsoft VPC object
        """
        super().__init__()
        self.name      = name
        self.nameTag   = {
            "Key": "Name",
            "Value": self.name
        }
        self.bVPC      = bVPC
        self.bRegion   = bVPC.bRegion
        self.ec2Client = boto3.client("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata(key="InternetGatewayId")

    @property
    def state(self):  # pylint: disable=C0111
        meta = self.getMetadata()
        return meta.get("Attachments")[0].get("State")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self):  # pylint: disable=W0221
        """
        Create object
        Store name as a tag attribute.
        """
        resp   = self.ec2Client.create_internet_gateway()
        ig     = resp.get("InternetGateway")
        igID   = ig.get("InternetGatewayId", None)
        self.ec2Client.create_tags(Resources=[igID], Tags=[self.nameTag])
        self.ec2Client.attach_internet_gateway(InternetGatewayId=igID,
                                               VpcId=self.bVPC.id)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        igID = self.id
        self.ec2Client.detach_internet_gateway(InternetGatewayId=igID,
                                               VpcId=self.bVPC.id)
        self.ec2Client.delete_internet_gateway(InternetGatewayId=igID)
        self.ec2Client.delete_tags(Resources=[igID], Tags=[self.nameTag])

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all internet gateways
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_internet_gateways()
        igs  = resp.get("InternetGateways", None)
        logger.info("InternetGateways:\n{}".format(pprint.pformat(igs)))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_internet_gateways
        """
        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_internet_gateways(Filters=[filt])
        igs  = resp.get("InternetGateways", None)
        if igs:
            meta = igs[0]
        return meta
