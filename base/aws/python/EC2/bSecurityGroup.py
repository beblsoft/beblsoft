#!/usr/bin/env python3
"""
NAME:
 bSecurityGroup.py

DESCRIPTION
 Beblsoft Security Group Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import time
import pprint
from datetime import datetime, timedelta
import boto3
import botocore
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SECURITY GROUP --------------------------- #
class BeblsoftSecurityGroup(BeblsoftAWSObject):
    """
    Beblsoft Security Group
    """

    def __init__(self, name, bVPC, description=None,  # pylint: disable=W0102
                 ipIngressPermissions=[{
                     # Ports Commented Out = All
                     # "FromPort": 22,    # Port 22
                     # "ToPort": 22,      # Port 22
                     "IpProtocol": "-1",  # tcp,udp,icmp
                     "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
                 }],
                 ipEgressPermissions=[{
                     # Ports Commented Out = All
                     # "FromPort": 22,    # Port 22
                     # "ToPort": 22,      # Port 22
                     "IpProtocol": "-1",  # tcp,udp,icmp
                     "IpRanges": [{"CidrIp": "0.0.0.0/0"}]
                 }]):
        """
        Initialize Object
        Args
          name:
            Security Group name
          bVPC:
            Beblsoft VPC object
          ipEgressPermissions:
            ip outbound rules
          ipIngressPermissions:
            ip inbound rules
        """
        super().__init__()
        self.name                 = name
        self.nameTag              = {"Key": "Name", "Value": self.name}
        self.bVPC                 = bVPC
        self.description          = "{} Security Group".format(self.name)
        if description:
            self.description      = description
        self.ipIngressPermissions = ipIngressPermissions
        self.ipEgressPermissions  = ipEgressPermissions
        self.bRegion              = bVPC.bRegion
        self.ec2Client            = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res               = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("GroupId")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, sleepS=3, waitTotalS=30):  # pylint: disable=W0221
        """
        Create object
        """
        resp    = self.ec2Client.create_security_group(GroupName=self.name, Description=self.description,
                                                       VpcId=self.bVPC.id)
        groupID = resp.get("GroupId")

        # Ran into issue where security group is created, but create_tags operation fails with:
        # An error occurred (InvalidGroup.NotFound) when calling the CreateTags operation: The security group 'sg-0575545e7c4617eae' does not exist
        # Therefore, we loop waiting for security group to show up before proceeding
        startTime   = datetime.now()
        filt        = {"Name": "group-id", "Values": [groupID]}
        keepLooping = True
        while keepLooping:
            resp        = self.ec2Client.describe_security_groups(Filters=[filt])
            sgs         = resp.get("SecurityGroups", None)
            found       = len(sgs) >= 1
            if not found:
                time.sleep(sleepS)
            timedOut    = datetime.now() - startTime > timedelta(seconds=waitTotalS)
            keepLooping = not (found or timedOut)

        # Complete creation process
        sgRes = self.ec2Res.SecurityGroup(groupID) #pylint: disable=E1101
        sgRes.create_tags(Tags=[self.nameTag])
        self.authorizeIngressRules()
        self.authorizeEgressRules()

    @logFunc()
    def authorizeIngressRules(self):
        """
        Authorize ingress rules
        """
        try:
            self.ec2Client.authorize_security_group_ingress(
                GroupId=self.id, IpPermissions=self.ipIngressPermissions)
        except botocore.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "InvalidPermission.Duplicate":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e

    @logFunc()
    def authorizeEgressRules(self):
        """
        Authorize Egress Rules
        """
        try:
            self.ec2Client.authorize_security_group_egress(
                GroupId=self.id, IpPermissions=self.ipEgressPermissions)
        except botocore.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "InvalidPermission.Duplicate":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e

    @logFunc()
    def _delete(self, deleteENIs=True, sleepS=5, waitTotalS=120):  # pylint: disable=W0221
        """
        Delete security group
        Args
          deleteENIs:
            If True, delete any Elastic Network Interfaces associated with security group

        Note:
          Database Security Groups: deleteENIs should be False
          If True get error
          "(OperationNotPermitted) when calling the DetachNetworkInterface operation:
           You are not allowed to manage 'ela-attach' attachments."
        """
        groupID   = self.id
        deleted   = False
        startDate = datetime.now()

        if deleteENIs:
            self.deleteENIs()
        while not deleted:
            try:
                self.ec2Client.delete_security_group(GroupId=groupID)
                deleted = True
            except ClientError as e:
                message = e.response["Error"]["Message"]
                hasDeps = "resource {} has a dependent object".format(groupID) in message
                if datetime.now() - startDate > timedelta(seconds=waitTotalS):
                    raise(e)
                if hasDeps:
                    # Retry, waiting for dependency to be deleted
                    time.sleep(sleepS)
                else:
                    logger.exception(pprint.pformat(e.__dict__))
                    raise(e)
        self.ec2Client.delete_tags(Resources=[groupID], Tags=[self.nameTag])

    @logFunc()
    def deleteENIs(self, sleepS=3, waitTotalS=60):
        """
        Delete Elastic Network Interfaces associated with security group
        Args
          sleepS: time to sleep before retrying delete
        """
        startDate = datetime.now()
        while True:
            resp = self.ec2Client.describe_network_interfaces(Filters=[{"Name": "group-id", "Values": [self.id]}])
            enis = resp.get("NetworkInterfaces", None)
            if not enis:
                break
            for eni in resp.get("NetworkInterfaces", None):
                eniAttach   = eni.get("Attachment", None)
                attached    = True
                existing    = True
                eniAttachID = None
                eniID       = None
                while eniAttach and (attached or existing):
                    try:
                        if attached:
                            eniAttachID = eniAttach.get("AttachmentId")
                            self.ec2Client.detach_network_interface(AttachmentId=eniAttachID)
                            attached = False
                        if existing:
                            eniID = eni.get("NetworkInterfaceId")
                            self.ec2Client.delete_network_interface(NetworkInterfaceId=eniID)
                            existing = False
                    except ClientError as e:
                        message    = e.response["Error"]["Message"]
                        inUse      = "Network interface '{}' is currently in use.".format(eniID) in message
                        if datetime.now() - startDate > timedelta(seconds=waitTotalS):
                            raise(e)
                        if inUse:
                            # Retry, wait for use to stop
                            time.sleep(sleepS)
                        else:
                            logger.exception(pprint.pformat(e.__dict__))
                            raise(e)

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all security groups
        """
        ec2Client = boto3.client("ec2", region_name=bRegion.name)
        resp = ec2Client.describe_security_groups()
        sgs  = resp.get("SecurityGroups", None)
        logger.info("Security Groups:\n{}".format(pprint.pformat(sgs)))

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metdata
        Returns
          None or metadata here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_security_groups
        """
        meta = None
        filt = {"Name": "tag:Name", "Values": [self.name]}
        resp = self.ec2Client.describe_security_groups(Filters=[filt])
        sgs  = resp.get("SecurityGroups", None)
        if sgs:
            meta = sgs[0]
        return meta


# --------------------------- TEST HARNESS ---------------------------------- #
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    from base.aws.python.EC2.bVPC import BeblsoftVPC

    _bVPC          = BeblsoftVPC(name="BeblsoftCICDVPC")
    bSecurityGroup = BeblsoftSecurityGroup(name="BeblsoftCICDMiscSecurityGroup", bVPC=_bVPC)
    for _ in range(10):
        bSecurityGroup.create()
        bSecurityGroup.delete()
