#!/usr/bin/env python3
"""
NAME:
 bNetworkInterface.py

DESCRIPTION
 Beblsoft Network Interface Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT NETWORK INTERFACE ------------------------ #
class BeblsoftNetworkInterface(BeblsoftAWSObject):
    """
    Beblsoft Network Interface
    """

    def __init__(self, name, bSubnet, privateIPAddress=None, #pylint: disable=W0102
                 bSecurityGroupList=[], description=None):
        """
        Initialize object
        Args
          name:
            Network Interface Name
            Ex. "SmecknTestNATNetworkInterface"
          bSubnet:
            Beblsoft Subnet Object
          privateIPAddress:
            The primary IPv4 address of the network interface
            If not specified, AWS selects one from subnet range
          bSecurityGroupList
            List of Beblsoft Security Group Obects
          description:
            Description of network interface
        """
        super().__init__()
        self.name               = name
        self.nameTag            = {
            "Key": "Name",
            "Value": self.name
        }
        self.bSubnet            = bSubnet
        self.privateIPAddress   = privateIPAddress
        self.bSecurityGroupList = bSecurityGroupList
        self.description        = "{}".format(name)
        if description:
            self.description    = description
        self.bVPC               = self.bSubnet.bVPC
        self.bRegion            = self.bVPC.bRegion
        self.ec2Client          = boto3.client("ec2", region_name=self.bRegion.name)
        self.ec2Res             = boto3.resource("ec2", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("NetworkInterfaceId")

    @property
    def privateDnsName(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("PrivateDnsName")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, delayS=10, maxAttempts=60):  #pylint: disable=W0221
        """
        Create object
        Args
          maxAttempts:
            Number of times waiter waits
          delayS:
            Delay before waiter requeries state
        """
        kwargs                         = {}
        kwargs["SubnetId"]             = self.bSubnet.id
        kwargs["Description"]          = self.description
        if self.bSecurityGroupList:
            kwargs["Groups"]           = [bSG.id for bSG in self.bSecurityGroupList]
        if self.privateIPAddress:
            kwargs["PrivateIpAddress"] = self.privateIPAddress
        resp  = self.ec2Client.create_network_interface(**kwargs)
        ni    = resp.get("NetworkInterface")
        niID  = ni.get("NetworkInterfaceId")
        niRes = self.ec2Res.NetworkInterface(niID) #pylint: disable=E1101
        niRes.create_tags(Tags=[self.nameTag])
        waiter = self.ec2Client.get_waiter("network_interface_available")
        waiter.wait(
            Filters = [{"Name": "network-interface-id", "Values": [niID]}],
            WaiterConfig= {
            "Delay": delayS,
            "MaxAttempts": maxAttempts})

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        niID = self.id
        self.ec2Client.delete_network_interface(NetworkInterfaceId=niID)
        self.ec2Client.delete_tags(Resources=[niID], Tags=[self.nameTag])

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or object metadata here
          http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_network_interfaces
        """

        meta = None
        filt = {
            "Name": "tag:Name",
            "Values": [self.name]
        }
        resp = self.ec2Client.describe_network_interfaces(Filters=[filt])
        nis  = resp.get("NetworkInterfaces", None)
        if nis:
            meta = nis[0]
        return meta
