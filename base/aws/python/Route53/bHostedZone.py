#!/usr/bin/env python3
"""
NAME:
 bHostedZone.py

DESCRIPTION
 Beblsoft Hosted Functionality

AWS DOMAIN REGISTRATION:
 Example Fields that work
  - Domain: beblsoft.com
  - Domain Name Status Code: clientTransferProhibited
  - Transfer Lock: Enabled
  - Auto Renew: Enabled
  - Name Servers:
    ns-1170.awsdns-18.org, ns-160.awsdns-20.com,
    ns-1668.awsdns-16.co.uk, ns-925.awsdns-51.net
  - NOTE: the above name servers must come from the hosted zone NS record,
    or domain record will NOT be found

FORWARDING TO GMAIL:
 - Create manager@beblsoft.com, gmail account
 - Go to your domains: beblosoft.com
 - Manage DNS
 - Delete existing Mail Exchange (MX) record
 - Create record set
   Name: [Blank]
   Type: MX - Mail exchange
   TTL (Seconds): 3600
   Value:
     1  ASPMX.L.GOOGLE.COM
     5  ALT1.ASPMX.L.GOOGLE.COM
     5  ALT2.ASPMX.L.GOOGLE.COM
     10 ALT3.ASPMX.L.GOOGLE.COM
     10 ALT4.ASPMX.L.GOOGLE.COM

COMMANDS
 Show DNS records (1)   : host -a www.quoteablejoy.com
 Show DNS records (2)   : dig www.quotablejoy.com any
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT HOSTED ZONE ------------------------------ #
class BeblsoftHostedZone(BeblsoftAWSObject):
    """
    Beblsoft Hosted Zone
    """

    def __init__(self, domainName,
                 comment="HostedZone created by Route53 Registrar",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          domainName:
            Domain name of zone
            Ex. "example.com"
            Note: no trailing dot
          bRegion:
            Beblsoft Region object
        """
        super().__init__()
        self.domainName  = domainName
        self.bRegion     = bRegion
        self.comment     = comment
        if not self.comment:
            self.comment = "Default Hosted Zone Comment"
        self.r53Client   = boto3.client("route53", region_name=bRegion.name)

    def __str__(self):
        return "[{} DomainName={}]".format(self.__class__.__name__, self.domainName)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def id(self):  # pylint: disable=C0111
        hzID = self.getValueFromMetadata("HostedZone.Id")
        if hzID:
            hzID = hzID.lstrip("/hostedzone/")
        return hzID

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, callerReference=str(datetime.now())): #pylint: disable=W0221
        """
        Create object
        Args
          callerReference:
            A unique ID that identifies the request and allows failed requests
            to be retried
        """
        self.r53Client.create_hosted_zone(
            Name             = self.domainName,
            CallerReference  = callerReference,
            HostedZoneConfig = {"Comment": self.comment})

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.r53Client.delete_hosted_zone(Id=self.id)

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return object metadata
        Returns
          None or Meta object here
          http://boto3.readthedocs.io/en/latest/reference/services/route53.html#Route53.Client.get_hosted_zone
        """
        meta    = None
        allMeta = BeblsoftHostedZone.getAllMetadata(bRegion=self.bRegion)
        if allMeta:
            for curMeta in allMeta:
                domainMatch  = curMeta.get("Name").rstrip(
                    ".")      == self.domainName
                commentMatch = curMeta.get("Config").get("Comment") == self.comment
                if domainMatch and commentMatch:
                    meta = curMeta
                    break
        if meta:
            meta = self.r53Client.get_hosted_zone(Id=meta.get("Id"))
        return meta

    @staticmethod
    def getAllMetadata(bRegion=BeblsoftRegion.getDefault()):
        """
        Return all object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/route53.html#Route53.Client.list_hosted_zones
        """
        r53Client = boto3.client("route53", region_name=bRegion.name)
        resp = r53Client.list_hosted_zones()
        return resp.get("HostedZones", None)
