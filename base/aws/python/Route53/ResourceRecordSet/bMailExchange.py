#!/usr/bin/env python3
"""
NAME:
 bMailExchange.py

DESCRIPTION
 Beblsoft Mail Exchange Resource Record Set Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.Route53.ResourceRecordSet.bResourceRecordSet import BeblsoftResourceRecordSet
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT MAIL EXCHANGE RESOURCE RECORD SET -------- #
class BeblsoftMailExchangeResourceRecordSet(BeblsoftResourceRecordSet):
    """
    Beblsoft Mail Exchange Resource Record Set
    """

    def __init__(self, valueListFunc, **kwargs):
        """
        Initialize object
        Args
          For most:
            See BeblsoftResourceRecordSet
          valueListFunc:
            Function to return list of resource values
            A value is composed of a priority and a domain name that specifies a mail server.
            [priority] [mail server host name]
            Ex2. lambda: ["10 inbound-smtp.us-east-1.amazonaws.com"]
            Ex1. lambda: ["10 mailserver.example.com.",
                          "20 mailserver2.example.com."]
        """
        self.valueListFunc = valueListFunc
        super().__init__(**kwargs, recordType="MX")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        hzID = self.bHostedZone.id
        resp = self.r53Client.change_resource_record_sets(
            HostedZoneId = hzID,
            ChangeBatch   = {
                "Comment": self.changeComment,
                "Changes": [self.getChangeSet(action="CREATE")]
            })
        changeInfo = resp.get("ChangeInfo")
        changeId   = changeInfo.get("Id")
        self.waitChangeComplete(changeId=changeId)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        hzID = self.bHostedZone.id
        resp = self.r53Client.change_resource_record_sets(
            HostedZoneId = hzID,
            ChangeBatch  = {
                "Comment": self.changeComment,
                "Changes": [self.getChangeSet(action="DELETE")]
            })
        changeInfo = resp.get("ChangeInfo")
        changeId   = changeInfo.get("Id")
        self.waitChangeComplete(changeId=changeId)

    def getChangeSet(self, action):
        """
        Return change set for desired action
        Args
          action:
            Desired change action
            Ex. "CREATE", "DELETE"
        """
        return {
            "Action": action,
            "ResourceRecordSet": {
                "Name": self.domainName,
                "Type": self.type,
                "TTL": self.ttl,
                "ResourceRecords": [{"Value": val} for val in self.valueListFunc()],
            },
        }
