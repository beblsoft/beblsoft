#!/usr/bin/env python3
"""
NAME:
 bAlias.py

DESCRIPTION
 Beblsoft Alias Resource Record Set Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.Route53.ResourceRecordSet.bResourceRecordSet import BeblsoftResourceRecordSet
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT ALIAS RESOURCE RECORD SET ---------------- #
class BeblsoftAliasResourceRecordSet(BeblsoftResourceRecordSet):
    """
    Beblsoft Alias Resource Record Set
    """

    def __init__(self, dnsNameFunc, cloudFrontAlias=False,
                 evalTargetHealth=False, **kwargs):
        """
        Initialize object
        Args
          For most:
            See BeblsoftResourceRecordSet
          dnsNameFunc:
            Function to return the DNS Name of the Alias Target
          cloudFrontAlias:
            If True, alias points to a cloud front distribution
          evalTargetHealth:
            If True, preform health checks
        """
        self.dnsNameFunc      = dnsNameFunc
        self.cloudFrontAlias  = cloudFrontAlias
        self.evalTargetHealth = evalTargetHealth
        super().__init__(**kwargs)


    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        hzID = self.bHostedZone.id
        resp = self.r53Client.change_resource_record_sets(
            HostedZoneId = hzID,
            ChangeBatch   = {
                "Comment": self.changeComment,
                "Changes": [self.getChangeSet(hzID=hzID, action="CREATE")]
            })
        changeInfo = resp.get("ChangeInfo")
        changeId   = changeInfo.get("Id")
        self.waitChangeComplete(changeId=changeId)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        hzID = self.bHostedZone.id
        resp = self.r53Client.change_resource_record_sets(
            HostedZoneId = hzID,
            ChangeBatch  = {
                "Comment": self.changeComment,
                "Changes": [self.getChangeSet(hzID=hzID, action="DELETE")]
            })
        changeInfo = resp.get("ChangeInfo")
        changeId   = changeInfo.get("Id")
        self.waitChangeComplete(changeId=changeId)

    def getChangeSet(self, hzID, action):
        """
        Return change set for desired action
        Args
          hzID:
            Hosted Zone ID
          action:
            Desired change action
            Ex. "CREATE", "DELETE"
        """
        aliasHZID = hzID
        if self.cloudFrontAlias:
            aliasHZID = "Z2FDTNDATAQYW2"
        return {
            "Action": action,
            "ResourceRecordSet": {
                "Name": self.domainName,
                "Type": self.type,
                "AliasTarget": {
                    "HostedZoneId": aliasHZID,
                    "DNSName": self.dnsNameFunc(),
                    "EvaluateTargetHealth": self.evalTargetHealth
                }
            }
        }
