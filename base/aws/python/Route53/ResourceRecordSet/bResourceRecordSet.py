#!/usr/bin/env python3
"""
NAME:
 bResourceRecordSet.py

DESCRIPTION
 Beblsoft Resource Record Set Functionality
 Resource Record Sets tell DNS how to resolve requests agains particular
 queries.

TYPES
  A     : address record
  AAAA  : IPv6 address record
  CNAME : canonical name record
  MX    : mail exchange record
  NS    : name server record
  PTR   : pointer record
  SOA   : start of authority record
  SPF   : sender policy framework
  SRV   : service locator
  TXT   : text record
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from datetime import datetime
import boto3
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT RESOURCE RECORD SET ---------------------- #
class BeblsoftResourceRecordSet(BeblsoftAWSObject):
    """
    Beblsoft Resource Record Set
    """

    def __init__(self, bHostedZone, domainName, recordType, ttl=1800):
        """
        Initialize object
        Args
          bHostedZone:
            Beblsoft Hosted Zone
          domainName:
            Domain name
            Ex. "www.example.com"
          recordType:
            DNS Record Type
            One of:  "A", "AAAA", "CAA", "CNAME", "MX", "NAPTR", "NS", "PTR",
            "SOA", "SPF", "SRV", "TXT"
          ttl:
            The resource record cache time to live (TTL), in seconds
        """
        super().__init__()
        self.bHostedZone = bHostedZone
        self.bRegion     = bHostedZone.bRegion
        self.domainName  = domainName
        self.type        = recordType
        self.ttl         = ttl
        self.r53Client   = boto3.client("route53", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} DomainName={} Type={}]".format(
            self.__class__.__name__, self.domainName, self.type)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def changeComment(self):
        """
        Return change comment
        """
        return "{} change at {}".format(self, datetime.now())

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def waitChangeComplete(self, changeId, delayS=5, maxAttempts=120):
        """
        Wait for a change to complete
        Args
          changeId:
            Change identification
          delayS:
            Seconds to delay before requering r53Client.get_change for successful change state
          maxAttempts:
            # of times to query record state
        """
        waiter = self.r53Client.get_waiter("resource_record_sets_changed")
        waiter.wait(Id=changeId,
                    WaiterConfig= {"Delay": delayS, "MaxAttempts": maxAttempts})

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/route53.html#Route53.Client.list_resource_record_sets
        """
        meta    = None
        allMeta = BeblsoftResourceRecordSet.getAllMetadata(bHostedZone=self.bHostedZone)
        for curMeta in allMeta:
            domainMatch = curMeta.get("Name").rstrip(".") == self.domainName
            typeMatch   = curMeta.get("Type")             == self.type
            if domainMatch and typeMatch:
                meta = curMeta
                break
        return meta

    @staticmethod
    def getAllMetadata(bHostedZone):
        """
        Return all resource record set metadata in hosted zone
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/route53.html#Route53.Client.list_resource_record_sets
        """
        r53Client = boto3.client("route53", region_name=bHostedZone.bRegion.name)
        resp = r53Client.list_resource_record_sets(
            HostedZoneId=bHostedZone.id)
        return resp.get("ResourceRecordSets", None)
