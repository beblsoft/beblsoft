#!/usr/bin/env python3
"""
NAME:
 bEventSourceMapping.py

DESCRIPTION
 Beblsoft Lambda Event Source Mapping Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LAMBDA EVENT SOURCE MAPPING -------------- #
class BeblsoftLambdaEventSourceMapping(BeblsoftAWSObject):
    """
    Beblsoft Lambda Event Source Mapping
    """

    def __init__(self,
                 bFunction,
                 eventSourceARNFunc,
                 enabled = True,
                 batchSize = 1,
                 startingPosition = None,
                 startingPositionTimestamp = None):
        """
        Initialize object
        Args
          bFunction:
            BeblsoftLambdaFunction Object
          eventSourceARNFunc:
            Function to return ARN of poll-based resource that will feed events
            to lambda
            Ex. lambda : bSQSQueue.arn
          enabled:
            Whether the event source mapping is enabled
            Ex. True
          batchSize:
            The largest number of records that AWS Lambda will retrieve from
            your event source at the time of invoking your function.
            SQS: Min = 1, Max = 10
          startingPosition:
            The position in the DynamoDB or Kinesis stream where AWS Lambda should start reading
            One of 'TRIM_HORIZON'|'LATEST'|'AT_TIMESTAMP'
          startingPositionTimestamp:
            The timestamp of the data record from which to start reading.
        """
        super().__init__()
        self.bFunction                 = bFunction
        self.eventSourceARNFunc        = eventSourceARNFunc
        self.enabled                   = enabled
        self.batchSize                 = batchSize
        self.startingPosition          = startingPosition
        self.startingPositionTimestamp = startingPositionTimestamp
        self.lambdaClient              = self.bFunction.lambdaClient

    def __str__(self):
        return "[{} bFunction={}]".format(self.__class__.__name__, self.bFunction.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def uuid(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("UUID")

    @property
    def state(self):  # pylint: disable=C0111
        """
        Returns State
        One of "Creating", "Enabled", "Disabled", "Enabling", "Disabling", "Updating", or "Deleting"
        """
        return self.getValueFromMetadata("State")

    @property
    def stateTransitionReason(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("StateTransitionReason")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        kwargs                                   = {}
        kwargs["EventSourceArn"]                 = self.eventSourceARNFunc()
        kwargs["FunctionName"]                   = self.bFunction.arn
        kwargs["Enabled"]                        = self.enabled
        kwargs["BatchSize"]                      = self.batchSize
        if self.startingPosition:
            kwargs["StartingPosition"]           = self.startingPosition
        if self.startingPositionTimestamp:
            kwargs["StartingPositionTimestamp"]  = self.startingPositionTimestamp

        self.lambdaClient.create_event_source_mapping(**kwargs)
        self.waitChange(changeFunc = lambda: self.state == "Enabled")

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.lambdaClient.delete_event_source_mapping(UUID = self.uuid)
        self.waitChange(changeFunc = lambda: self.state == None)

    @logFunc()
    def _update(self, **kwargs):
        """
        Update object
        """
        kwargs                 = {}
        kwargs["UUID"]         = self.uuid
        kwargs["FunctionName"] = self.bFunction.arn
        kwargs["Enabled"]      = self.enabled
        kwargs["BatchSize"]    = self.batchSize
        self.lambdaClient.update_event_source_mapping(**kwargs)
        self.waitChange(changeFunc = lambda: self.state == "Enabled")

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html#Lambda.Client.list_event_source_mappings        """
        meta     = None
        arn      = self.eventSourceARNFunc()
        if arn:
            resp = self.lambdaClient.list_event_source_mappings(
                EventSourceArn   = self.eventSourceARNFunc(),
                FunctionName     = self.bFunction.name)
            esmList = resp.get("EventSourceMappings", None)
            if esmList:
                meta = esmList[0]
        return meta
