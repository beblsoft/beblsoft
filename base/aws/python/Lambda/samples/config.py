#!/usr/bin/env python3
"""
NAME:
 config.py

DESCRIPTION
 Lambda Function Sample Configuration
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
from base.aws.python.EC2.bVPC import BeblsoftVPC
from base.aws.python.EC2.bSubnet import BeblsoftSubnet
from base.aws.python.EC2.bSecurityGroup import BeblsoftSecurityGroup
from base.aws.python.EC2.bInternetGateway import BeblsoftInternetGateway
from base.aws.python.IAM.bRole import BeblsoftIAMRole
from base.aws.python.IAM.bRolePolicy import BeblsoftIAMRolePolicy
from base.aws.python.Lambda.bPackage import BeblsoftLambdaPackage
from base.aws.python.Lambda.bFunction import BeblsoftLambdaFunction


# ------------------------ CONFIG OBJECT ------------------------------------ #
class Config():
    """Configuration Object"""

    def __init__(self):
        """
        Initialize Object
        """

        lfSampleDir               = os.path.dirname(os.path.realpath(__file__))
        hwLFunctionDir            = "{}/helloWorld".format(lfSampleDir)

        # ------------- Network --------------------------------------------- #
        self.bVPC                 = BeblsoftVPC(
            name                          = "lambdaVPC",
            cidrBlock                     = "10.0.0.0/16",
            enableDnsHostnames            = True,
            enableDnsSupport              = True)
        self.bInternetGateway     = BeblsoftInternetGateway(
            name                          = "lambdaInternetGateway",
            bVPC                          = self.bVPC)
        self.bLambdaSubnet1       = BeblsoftSubnet(
            name                          = "lambdaSubnet1",
            cidrBlock                     = "10.0.0.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "us-east-1a")
        self.bLambdaSubnet2       = BeblsoftSubnet(
            name                          = "lambdaSubnet2",
            cidrBlock                     = "10.0.1.0/24",
            bVPC                          = self.bVPC,
            availabiltyZone               = "us-east-1b")
        self.bSecurityGroup       = BeblsoftSecurityGroup(
            name                          = "lambdaSecurityGroup",
            bVPC                          = self.bVPC,
            description                   = "Lambda Security Group")

        # ------------- IAM Roles ------------------------------------------- #
        self.bIAMRole             = BeblsoftIAMRole(
            name                          = "lambdaInstanceIAMRole",
            trustPolicyPath               = "{}/hwsIAMRoleTrustPolicy.json".format(lfSampleDir))
        self.bIAMRolePolicy       = BeblsoftIAMRolePolicy(
            name                          = "lambdaInstanceIAMRolePolicy",
            bIAMRole                      = self.bIAMRole,
            permissionPolicyPath          = "{}/hwsIAMRolePermissionPolicy.json".format(lfSampleDir))

        # ------------- Hello World Lambda Function ------------------------- #
        self.bHWLFunctionPackage  = BeblsoftLambdaPackage(
            bucketName                    = "jbensson-lambda-sample",
            bucketKey                     = "hwlFunction.zip",
            fromToPaths                   = [{"fromPath": "{}/*".format(hwLFunctionDir),
                                              "toPath": "."}],
            buildDir                      = "/tmp/hwlFunctionBuild")
        self.bHWLFunction         = BeblsoftLambdaFunction(
            name                          = "HelloWorldLambdaFunction",
            handler                       = "handler.handler",
            bLambdaPackage                = self.bHWLFunctionPackage,
            runTime                       = "python3.6",
            bIAMRole                      = self.bIAMRole,
            bSubnets                      = [self.bLambdaSubnet1,
                                             self.bLambdaSubnet2],
            bSecurityGroups               = [self.bSecurityGroup],
            envDict                       = {"HWLF_KEY": "HWLF_VAL"},
            description                   = "Hello World Lambda Function")
