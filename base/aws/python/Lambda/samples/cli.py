#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 Lambda Function Sample Command Line Interface
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import click
from base.aws.python.Lambda.samples.config import Config


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__file__)
cfg    = Config()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
def cli():
    """Lambda Function Command Line Interface"""
    logging.basicConfig(format="%(asctime)-15s: %(levelname)s %(message)s",
                        level=logging.INFO)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('boto3').setLevel(logging.CRITICAL)


# ----------------------- CREATE -------------------------------------------- #
@cli.command()
@click.option('--allobjs', is_flag=True, default=False, help="Create all objects")
@click.option('--network', is_flag=True, default=False, help="Create network objects")
@click.option('--iam', is_flag=True, default=False, help="Create IAM objects")
@click.option('--lambdas', is_flag=True, default=False, help="Create Lambda objects")
def create(allobjs, network, iam, lambdas):
    """Create Infrastucture"""
    if network or allobjs:
        cfg.bVPC.create()
        cfg.bInternetGateway.create()
        cfg.bLambdaSubnet1.create()
        cfg.bLambdaSubnet2.create()
        cfg.bSecurityGroup.create()
    if iam or allobjs:
        cfg.bIAMRole.create()
        cfg.bIAMRolePolicy.create()
    if lambdas or allobjs:
        cfg.bHWLFunction.create()


# ----------------------- DELETE -------------------------------------------- #
@cli.command()
@click.option('--allobjs', is_flag=True, default=False, help="Delete all objects")
@click.option('--network', is_flag=True, default=False, help="Delete network objects")
@click.option('--iam', is_flag=True, default=False, help="Delete IAM objects")
@click.option('--lambdas', is_flag=True, default=False, help="Delete Lambda objects")
def delete(allobjs, network, iam, lambdas):
    """Delete Infrastucture"""
    if lambdas or allobjs:
        cfg.bHWLFunction.delete()
    if iam or allobjs:
        cfg.bIAMRolePolicy.delete()
        cfg.bIAMRole.delete()
    if network or allobjs:
        cfg.bSecurityGroup.delete()
        cfg.bLambdaSubnet2.delete()
        cfg.bLambdaSubnet1.delete()
        cfg.bInternetGateway.delete()
        cfg.bVPC.delete()


# ----------------------- DESCRIBE ------------------------------------------ #
@cli.command()
@click.option('--allobjs', is_flag=True, default=False, help="Describe all objects")
@click.option('--network', is_flag=True, default=False, help="Describe network objects")
@click.option('--iam', is_flag=True, default=False, help="Describe IAM objects")
@click.option('--lambdas', is_flag=True, default=False, help="Describe Lambda objects")
def describe(allobjs, network, iam, lambdas):
    """Describe Infrastucture"""
    if network or allobjs:
        cfg.bVPC.describe()
        cfg.bInternetGateway.describe()
        cfg.bLambdaSubnet1.describe()
        cfg.bLambdaSubnet2.describe()
        cfg.bSecurityGroup.describe()
    if iam or allobjs:
        cfg.bIAMRole.describe()
        cfg.bIAMRolePolicy.describe()
    if lambdas or allobjs:
        cfg.bHWLFunction.describe()


# ----------------------- INVOKE -------------------------------------------- #
@cli.command()
@click.option('--hwf', is_flag=True, default=False, help="Invoke helloworld function")
def invoke(hwf):
    """Invoke AWS lambda function"""
    if hwf:
        cfg.bHWLFunction.invoke(payloadJSON={"HWL": "FUNCTION"})


# ----------------------- TAIL ---------------------------------------------- #
@cli.command()
@click.option('--hwf', is_flag=True, default=False, help="Tail helloworld function")
def tail(hwf):
    """Tail AWS lambda function"""
    if hwf:
        cfg.bHWLFunction.tail()


# ----------------------- UPDATE -------------------------------------------- #
@cli.command()
@click.option('--hwf', is_flag=True, default=False, help="Update helloworld function")
def update(hwf):
    """Update AWS lambda function"""
    if hwf:
        cfg.bHWLFunction.update()


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    cli()  # pylint: disable=E1120
