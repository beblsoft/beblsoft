#!/usr/bin/env python3
"""
NAME:
 handler.py

DESCRIPTION
 Lamda Hello World Handler
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import socket
import pprint
from datetime import datetime


# ------------------------ GLOBALS ------------------------------------------ #
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


# ------------------------ HANDLER ------------------------------------------ #
def handler(event, context):
    """
    Lambda handler
    """
    logger.warning("Hello World!")
    logger.warning("Host:{}".format(socket.gethostname()))
    logger.warning("Time:{}".format(datetime.now()))
    logger.warning("Event:{}".format(event))
    logger.warning("Context:{}".format(pprint.pformat(context.__dict__)))
    logger.warning("Goodbye World!")
    return 0
