#!/usr/bin/env python3
"""
NAME:
 bFunction.py

DESCRIPTION
 Beblsoft Lambda Function Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import time
import pprint
import base64
import json
import boto3
from botocore.exceptions import ClientError
from base.aws.python.CloudWatchLogs.bLogGroup import BeblsoftCloudWatchLogGroup
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LAMBDA FUNCTION -------------------------- #
class BeblsoftLambdaFunction(BeblsoftAWSObject):
    """
    Beblsoft Lambda Function
    """

    def __init__(self,
                 name,
                 handler,
                 bLambdaPackage,
                 runTime="python3.6",
                 bIAMRole=None,
                 bSubnets=None,
                 bSecurityGroups=None,
                 envDict=None,
                 envDictFunc=None,
                 bKMSKey=None,
                 timeoutS=3,
                 memorySizeMB=512,
                 description="Beblsoft Lambda Function",
                 logRetentionInDays=30):
        """
        Initialize Object
        Args
          name:
            Function name
            ex. "MyFunction"
          handler:
            Function handler [defined inside bLambdaCode] to be executed by lambda
            ex. "app.foo.bar"
          bLambdaPackage:
            Beblsoft Lambda Package
          runTime:
            Lambda runtime environment
            One of: 'nodejs'|'nodejs4.3'|'nodejs6.10'|'java8'|'python2.7'|'python3.6'|
            'dotnetcore1.0'|'nodejs4.3-edge'
          bIAMRole:
            Beblsoft IAM Role assumed by lambda function
          bSubnets:
            List of beblsoft subnet objects
          bSecurityGroups:
            List of beblsoft security group objects
          envDict:
            Dictionary of environment variables
            ex. {"key1":"val1", "key2":"val2"}
          envDictFunc:
            Function to return environment dictionary that is not known at
            object instantiation time. Function will be invoked when create
            action is called.
            Environment returned will override any setting in envDict.
            ex. lambda :{"key1": valForKey1(), "key2": valForKey2()}
          bKMSKey:
            Beblsoft Key Management Service Key
          timoutS:
            Execution time at which lambda will terminate the function, in seconds
          memorySizeMB:
            Lambda memory size in MB
          description:
            Textual description
          logRetentionInDays:
            Number of days to retain the log events in the specified group
        """
        super().__init__()
        self.name            = name
        self.handler         = handler
        self.bLambdaPackage  = bLambdaPackage
        self.runTime         = runTime
        self.bIAMRole        = bIAMRole
        self.bSubnets        = bSubnets
        self.bSecurityGroups = bSecurityGroups
        self.envDict         = envDict
        self.envDictFunc     = envDictFunc
        self.bKMSKey         = bKMSKey
        self.timeoutS        = timeoutS
        self.memorySizeMB    = memorySizeMB
        self.description     = description
        self.bRegion         = bLambdaPackage.bRegion
        self.bCWLogGroup     = BeblsoftCloudWatchLogGroup(
            name                 = "/aws/lambda/{}".format(self.name),
            retentionInDays      = logRetentionInDays,
            bRegion              = self.bRegion)
        self.lambdaClient    = boto3.client("lambda", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata("FunctionArn")

    @property
    def environment(self):
        """
        Return environment
        """
        env = {}
        if self.envDict:
            env.update(self.envDict)
        if self.envDictFunc:
            env.update(self.envDictFunc())
        return env

    # ---------------------- CREATE ----------------------------------------- #
    @logFunc()
    def _create(self, sleepS=1): #pylint: disable=W0221
        """
        Create lambda function
        Args
          sleepS:
            number of seconds to sleep while retying create operation.
            This is needed because IAM roles are not immediately available after creation

        Note:
          Log retention policy can't be set on log group until after it exists
          Use update operation
        """
        lambdaCreated = False
        self.bLambdaPackage.build()
        self.bLambdaPackage.upload()
        while not lambdaCreated:
            try:
                self.lambdaClient.create_function(
                    FunctionName = self.name,
                    Runtime      = self.runTime,
                    Role         = self.bIAMRole.arn,
                    Handler      = self.handler,
                    Code         = {"S3Bucket": self.bLambdaPackage.bBucket.name,
                                    "S3Key": self.bLambdaPackage.bBucketFile.key},
                    Description  = self.description,
                    Timeout      = self.timeoutS,
                    MemorySize   = self.memorySizeMB,
                    VpcConfig    = {"SubnetIds": [bSubnet.id for bSubnet in self.bSubnets],
                                    "SecurityGroupIds": [bSG.id for bSG in self.bSecurityGroups]},
                    Environment  = {"Variables": self.environment})
            except ClientError as e:
                message = e.response["Error"]["Message"]
                badRole = (
                    message == "The role defined for the function cannot be assumed by Lambda.")
                badPerm = (
                    message == "The provided execution role does not have permissions to call CreateNetworkInterface on EC2")
                if badRole or badPerm:
                    # Retry
                    time.sleep(sleepS)
                else:
                    logger.exception(pprint.pformat(e.__dict__))
                    raise(e)
            else:
                lambdaCreated = True


    # ---------------------- DELETE ----------------------------------------- #
    @logFunc()
    def _delete(self): #pylint: disable=W0221
        """
        Delete lambda function
        """
        self.lambdaClient.delete_function(FunctionName=self.name)

    # ---------------------- UPDATE ----------------------------------------- #
    @logFunc()
    def _update(self): #pylint: disable=W0221
        """
        Update lambda function
        """
        self.bLambdaPackage.build()
        self.bLambdaPackage.upload()
        self.lambdaClient.update_function_code(
            FunctionName=self.name,
            S3Bucket=self.bLambdaPackage.bBucket.name,
            S3Key=self.bLambdaPackage.bBucketFile.key)
        self.lambdaClient.update_function_configuration(
            FunctionName=self.name,
            Role=self.bIAMRole.arn,
            Handler=self.handler,
            Description=self.description,
            Timeout=self.timeoutS,
            MemorySize=self.memorySizeMB,
            VpcConfig={"SubnetIds": [bSubnet.id for bSubnet in self.bSubnets],
                       "SecurityGroupIds": [bSG.id for bSG in self.bSecurityGroups]},
            Environment={"Variables": self.environment})
        if self.bCWLogGroup.exists:
            self.bCWLogGroup.setRetentionPolicy()

    # ---------------------- PERMISSIONS ------------------------------------ #
    @logFunc()
    def addPermission(self, statementId, principal, sourceArn,
                      action="lambda:InvokeFunction"):
        """
        Add permission to lambda function allowing another principal to
        modify lambda

        Args
          statementId:
            Unique statement qualifier
          principal:
            Principal who is getting permission
            Ex1: "s3.amazonaws.com"
            Ex2: "events.amazonaws.com"
          sourceArn:
            Source arn allowed to invoke function
            Ex. "arn:aws:lambda:us-east-1:123456789012:function:ProcessKinesisRecords"
        """
        try:
            self.lambdaClient.add_permission(
                FunctionName=self.name, StatementId=statementId, Action=action,
                Principal=principal, SourceArn=sourceArn)
        except ClientError as e:
            if e.response["Error"]["Code"] == "ResourceConflictException":
                logger.info("{} Permission {} already exists".format(
                    self, statementId))
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        else:
            logger.info("{} Permission {} added".format(self, statementId))

    @logFunc()
    def removePermission(self, statementId):
        """
        Remove permission from lambda function

        Args
          statementId:
            Unique statement qualifier
        """
        try:
            self.lambdaClient.remove_permission(
                FunctionName=self.name, StatementId=statementId)
        except ClientError as e:
            if e.response["Error"]["Code"] == "ResourceNotFoundException":
                logger.info("{} Permission {} never existed".format(
                    self, statementId))
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        else:
            logger.info("{} Permission {} removed".format(self, statementId))

    # ---------------------- INVOKE ----------------------------------------- #
    @logFunc()
    def _invoke(self, #pylint: disable=W0221
                invocationType="RequestResponse",
                clientContext=None,
                payloadJSON=None,
                logType="Tail",
                raiseOnStatus=True,
                logFunction=logger.debug,
                evalRval=True):
        """
        Invoke lambda function
        Args
          invocationType:
            how to invoke lambda function
            one of: 'Event'|'RequestResponse'|'DryRun'
          ClientContext:
            Pass client-specific information to lambda function
            Ex. {"Hello":"World"}
            Internal: Must pass Base64 encoded JSON
          LogType:
            one of: 'None'|'Tail'
          payloadJSON:
            Python dictionary to be encoded as JSON Payload providing lambda function as input
            Internal: Bytes or seekable filelike object
          raiseOnStatus:
            If True, raise error if unsuccessful status code
            Successful   statuses:   200 - 299
            Unsuccessful statuses: ![200 - 299]
          logFunction:
            If not None, called to log function invocation metadata
            Ex. logger.debug
          evalRval:
            If True, evaluate the python return value
        """
        # Client context --------------
        if clientContext:
            clientContext = json.dumps(clientContext)
        else:
            clientContext = "{}"
        clientContext     = base64.b64encode(bytes(clientContext, "utf-8"))
        clientContext     = clientContext.decode("utf-8")

        # Payload ---------------------
        payload           = b""
        if payloadJSON:
            payload       = json.dumps(payloadJSON)

        # Invoke ----------------------
        resp              = self.lambdaClient.invoke(
            FunctionName     = self.name,
            InvocationType   = invocationType,
            LogType          = logType,
            ClientContext    = clientContext,
            Payload          = payload)

        # Process output --------------
        status            = resp.get("StatusCode")
        successStatus     = 200 <= status and status <= 299  # pylint: disable=C0122,R1716
        error             = resp.get("FunctionError", None)
        success           = not error and successStatus

        # Log -------------------------
        log               = resp.get("LogResult", None)
        if log:
            log           = base64.b64decode(log).decode("utf-8")

        # Rval ------------------------
        rval              = resp.get("Payload")
        if rval:
            rval          = rval.read().decode("utf-8")
        if rval == "null":
            rval          = None
        if rval and evalRval:
            try:
                rval      = eval(rval)
            except Exception as _: #pylint: disable=W0703
                pass

        # Message ---------------------
        msg               = ""
        msg              += "{} Invoked:\n".format(self)
        msg              += "status: {}\n".format(status)
        msg              += "error:  {}\n".format(error)
        msg              += "rval:   {}\n".format(rval)
        msg              += "log:    {}\n".format(log)

        if logFunction:
            logFunction(msg)
        if raiseOnStatus and not success:
            raise BeblsoftError(code=BeblsoftErrorCode.AWS_LAMBDA_INVOCATION_FAILED,
                                msg=msg)
        return rval

    # ---------------------- TAIL ------------------------------------------- #
    @logFunc()
    def _tail(self): #pylint: disable=W0221
        """
        Tail lambda function logs
        """
        self.bCWLogGroup.tail()

    # ---------------------- DUMP STREAM TO FILE ---------------------------- #
    @logFunc()
    def dumpStreamToFile(self, **cwlgDumpStreamToFileKwargs):
        """
        Dump log stream to file
        """
        self.bCWLogGroup.dumpStreamToFile(**cwlgDumpStreamToFileKwargs)

    # ---------------------- GET METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return object metadata
        Returns
          None or metadata here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html#Lambda.Client.get_function        """
        meta = None
        try:
            resp = self.lambdaClient.get_function(FunctionName=self.name)
            meta = resp.get("Configuration", None)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "ResourceNotFoundException":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
