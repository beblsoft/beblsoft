#!/usr/bin/env python3
"""
NAME:
 bPackage.py

DESCRIPTION
 Beblsoft Lambda Package Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import glob
import shutil
from distutils import dir_util
import logging
from base.aws.python.S3.bBucket import BeblsoftBucket
from base.aws.python.S3.bBucketFile import BeblsoftBucketFile
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT LAMBDA PACKAGE --------------------------- #
class BeblsoftLambdaPackage():
    """
    Beblsoft Lambda Package

    Class to handle defining, packaging, uploading, and cleaning lambda package.
    """

    def __init__(self, bucketName, bucketKey, fromToPaths,
                 existingBucket=True, buildDir="/tmp/lambda",
                 archivePath="/tmp/lambdaArchive",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          bucketName:
            Name of S3 bucket
          bucketKey:
            Upload Key inside bucket
            ex. "lambda.zip"
          fromToPaths:
            Relevant paths to copy into build directory
            ex. [{
                   "fromPath": "/path/to/project/*",
                   "toPath": ".",
                   "excludeStrList": ["venv", "__pycache__"]
                }],

            fromPath: absolute path
            toPath: relative directory inside build directory to copy contents
            excludeStrList: strings in fromPath glob to exclude
          existingBucket:
            If True, Bucket already exists and doesn't need to be created
          zipPath:
            Path to zip build directory into
          buildDir:
            Build directory
        """
        self.bucketKey       = bucketKey
        self.fromToPaths     = fromToPaths
        self.archivePath     = archivePath
        self.archiveType     = "zip"
        self.archivePathFull = "{}.{}".format(self.archivePath, self.archiveType)
        self.buildDir        = buildDir
        self.bRegion         = bRegion
        self.bBucket         = BeblsoftBucket(
            name                 = bucketName,
            acl                  = "public-read-write",
            bRegion              = bRegion)
        self.existingBucket  = existingBucket
        self.bBucketFile     = BeblsoftBucketFile(
            key                  = bucketKey,
            bBucket              = self.bBucket,
            localPath            = self.archivePathFull,
            acl                  = "private")

    def __str__(self):
        return "[{} Bucket={} Key={}]".format(self.__class__.__name__,
                                              self.bBucket.name, self.bucketKey)


    def shouldExcludePath(self, path, excludeStrList): #pylint: disable=R0201
        """
        Return True if path should be excluded
        """
        for excludeStr in excludeStrList:
            if excludeStr in path:
                return True
        return False

    @logFunc()
    def build(self):
        """
        Build lambda package
        """
        # Delete local, create builddir
        self.deleteLocal()
        os.makedirs(self.buildDir)

        # Copy fromToPaths into build directory
        # fromPath could contain "*" so we glob
        for fromToPath in self.fromToPaths:
            fromPath       = fromToPath.get("fromPath")
            toPath         = "{}/{}".format(self.buildDir, fromToPath.get("toPath"))
            excludeStrList = fromToPath.get("excludeStrList", [])
            os.makedirs(toPath, exist_ok=True)

            for path in glob.glob(fromPath):
                # Should be exclude path?
                if self.shouldExcludePath(path, excludeStrList):
                    continue
                # Copy in relevant files, directories
                if os.path.isfile(path):         # File Copy
                    shutil.copy(path, toPath)
                elif os.path.isdir(path):        # Directory Copy
                    commonPrefix = os.path.commonprefix([path, fromPath])
                    nonCommon    = path.replace(commonPrefix, "")
                    src          = path
                    dest         = "{}/{}".format(toPath, nonCommon)
                    dir_util.copy_tree(src, dest) # Use dir_util as shutil
                                                  # doesn't overwrite

        # Zip Build Directory
        # root_dir = root directory to zip
        # base_dir = directory of extracted contents
        # I.e. to zip /dir1/dir2/dir3/dir4 and have it unzip to dir3/dir4
        #  root_dir=/dir1/dir2/dir3/dir4
        #  base_dir=dir3/dir4
        shutil.make_archive(base_name=self.archivePath, format=self.archiveType,
                            root_dir=self.buildDir, base_dir=".")
        logger.info("{} Packaged".format(self))

    @logFunc()
    def upload(self):
        """
        Upload lambda package to bucket
        """
        if not self.existingBucket:
            self.bBucket.create()
        self.bBucketFile.upload()
        logger.info("{} Uploaded".format(self))

    @logFunc()
    def delete(self):
        """
        Clean local and remote artifacts
        """
        self.deleteLocal()
        self.deleteRemote()
        logger.info("{} Deleted".format(self))

    @logFunc()
    def deleteLocal(self):
        """
        Remove local build artifacts
        """
        if os.path.exists(self.buildDir):
            shutil.rmtree(self.buildDir)
        if os.path.exists(self.archivePathFull):
            os.remove(self.archivePathFull)

    @logFunc()
    def deleteRemote(self):
        """
        Remove remote artifacts
        """
        self.bBucketFile.delete()
        if not self.existingBucket:
            self.bBucket.delete()
