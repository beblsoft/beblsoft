#!/usr/bin/env python3
"""
NAME:
 bRule.py

DESCRIPTION
 Beblsoft CloudWatchEvents Rule Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CLOUD WATCH EVENTS RULE ----------------- #
class BeblsoftCloudWatchEventsRule(BeblsoftAWSObject):
    """
    Beblsoft Cloud Watch Events Rule

    Rules match incoming events and route them to particular targets
    """

    def __init__(self, name, bIAMRole,
                 scheduleExpression=None, eventPatternFunc=None,
                 description=None, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            Rule Name
            Ex. BeblsoftLambdaFlask-keepWarm
          bIAMRole:
            Beblsoft IAM Role
          scheduleExpression:
            Scheduling Expression
            Ex1. "cron(0 20 * * ? *)"
            Ex2. "rate(5 minutes)"
          eventPatternFunc:
            Function to return event pattern to match
          description:
            Description of the rule
        """
        super().__init__()
        self.name               = name
        self.bIAMRole           = bIAMRole
        self.scheduleExpression = scheduleExpression
        self.eventPatternFunc   = eventPatternFunc
        if not eventPatternFunc and not scheduleExpression:
            raise BeblsoftError(
                msg="Must specify scheduleExpression or eventPatternFunc")
        if description:
            self.description    = description
        else:
            self.description    = "{} Cloud Watch Events Rule".format(name)
        self.bRegion            = bRegion
        self.cweClient          = boto3.client('events',
                                               region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata(key="Arn")


    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargsIn):  # pylint: disable=W0613
        """
        Create object
        """
        kwargs                           = {}
        kwargs["Name"]                   = self.name
        if self.scheduleExpression:
            kwargs["ScheduleExpression"] = self.scheduleExpression
        if self.eventPatternFunc:
            kwargs["EventPattern"]       = self.eventPatternFunc
        kwargs["State"]                  = "ENABLED"
        kwargs["Description"]            = self.description
        kwargs["RoleArn"]                = self.bIAMRole.arn
        self.cweClient.put_rule(**kwargs)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.cweClient.delete_rule(Name=self.name)

    @logFunc()
    def _enable(self, **kwargs):
        """
        Enable object
        """
        self.cweClient.enable_rule(Name=self.name)

    @logFunc()
    def _disable(self, **kwargs):
        """
        Disable object
        """
        self.cweClient.disable_rule(Name=self.name)

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Return rule metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/events.html#CloudWatchEvents.Client.describe_rule
        """
        meta = None
        try:
            meta = self.cweClient.describe_rule(Name=self.name)
        except ClientError as e:
            if e.response["Error"]["Code"] == "ResourceNotFoundException":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        return meta
