#!/usr/bin/env python3
"""
NAME:
 bRuleTarget.py

DESCRIPTION
 Beblsoft CloudWatchEvents Rule Target Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import json
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CLOUD WATCH EVENTS RULE TARGET ---------- #
class BeblsoftCloudWatchEventsRuleTarget(BeblsoftAWSObject):
    """
    Beblsoft Cloud Watch Events Rule Target
    """

    def __init__(self, bRule, tid, arnFunc, bIAMRole, kwargs={}):  # pylint: disable=W0102
        """
        Initialize object
        Args
          bRule:
            BeblsoftCloudWatchEventsRule object
          tid:
            Target ID
            Ex. "asdflkaglkjh"
          arnFunc:
            Target ARN
            Ex. "arn:aws:lambda:us-east-1:123456789012:function:ProcessKinesisRecords"
          bIAMRole:
            BeblsoftIAMRole
          kwargs:
            Extra args to pass to target
        """
        super().__init__()
        self.bRule     = bRule
        self.id        = tid
        self.arnFunc   = arnFunc
        self.bIAMRole  = bIAMRole
        self.bRegion   = bRule.bRegion
        self.kwargs    = kwargs
        self.cweClient = boto3.client('events', region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Rule={} id={}]".format(
            self.__class__.__name__, self.bRule.name, self.id)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata(key="Arn")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        #Code taken from zappa.core:schedule_events
        inputTemplate = '{"time": <time>, '  \
            '"detail-type": <detail-type>, ' \
            '"source": <source>,'            \
            '"account": <account>, '         \
            '"region": <region>,'            \
            '"detail": <detail>, '           \
            '"version": <version>,'          \
            '"resources": <resources>,'      \
            '"id": <id>,'                    \
            '"kwargs": %s'                   \
            '}' % json.dumps(self.kwargs)
        inputPathsMap = {
            'time': '$.time',
            'detail-type': '$.detail-type',
            'source': '$.source',
            'account': '$.account',
            'region': '$.region',
            'detail': '$.detail',
            'version': '$.version',
            'resources': '$.resources',
            'id': '$.id'
        }
        targetArg     = {
            "Id": self.id,
            "Arn": self.arnFunc(),
            'InputTransformer': {
                'InputPathsMap': inputPathsMap,
                'InputTemplate': inputTemplate
            }
        }
        self.cweClient.put_targets(Rule=self.bRule.name, Targets=[targetArg])

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.cweClient.remove_targets(Rule=self.bRule.name, Ids=[self.id])


    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/events.html#CloudWatchEvents.Client.list_targets_by_rule
        """
        meta = None
        try:
            resp    = self.cweClient.list_targets_by_rule(Rule=self.bRule.name)
            targets = resp.get("Targets", [])
            for target in targets:
                if target.get("Id", None) == self.id:
                    meta = target
                    break
        except ClientError as e:
            if e.response["Error"]["Code"] == "ResourceNotFoundException":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        return meta
