#!/usr/bin/env python3
"""
NAME:
 bRuleLambaTarget.py

DESCRIPTION
 Beblsoft CloudWatchEvents Rule Lambda Target Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.CloudWatchEvents.bRuleTarget import BeblsoftCloudWatchEventsRuleTarget
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT CLOUD WATCH EVENTS RULE LAMBDA TARGET --- #
class BeblsoftCloudWatchEventsRuleLambdaTarget(BeblsoftCloudWatchEventsRuleTarget):
    """
    Beblsoft Cloud Watch Events Rule Target
    """

    def __init__(self, bLambdaFunction, **kwargs):  # pylint: disable=W0102
        """
        Initialize object
        Args
          bLambdaFunction:
            BeblsoftLambdaFunction
        """
        self.bLambdaFunction = bLambdaFunction
        super().__init__(arnFunc=lambda : bLambdaFunction.arn, **kwargs)

    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        super()._create()
        self.bLambdaFunction.addPermission(
            statementId=self.id, principal="events.amazonaws.com",
            sourceArn=self.bRule.arn)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.bLambdaFunction.removePermission(
            statementId=self.id)
        super()._delete()
