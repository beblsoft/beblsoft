#!/usr/bin/env python3
"""
NAME:
 bStack.py

DESCRIPTION
 AWS Cloud Formation Stack Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT CLOUD FORMATION STACK -------------------- #
class BeblsoftCloudFormationStack():
    """Beblsoft Cloud Formation Stack"""

    def __init__(self, name, templatePath, stackTags=None,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            stack name
          templatePath:
            template file
          stackTags:
            list of dictionary tags
            [{"Key":"Name", "Value":"Mojourney"},...]
        """
        self.name         = name
        self.templatePath = templatePath
        self.stackTags    = stackTags
        self.templateStr  = ""
        self.bRegion      = bRegion
        self.cfClient     = boto3.client("cloudformation",
                                         region_name=bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create stack
        Read in template file and then issue cloud formation command
        """
        with open(self.templatePath, "r") as f:
            self.templateStr = f.read()
        resp    = self.cfClient.create_stack(StackName=self.name,
                                        TemplateBody=self.templateStr)
        stackId = resp.get("StackId", None)
        waiter  = self.cfClient.get_waiter("stack_create_complete")
        waiter.wait(StackName=stackId)
        logger.info("{} created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete stack
        """
        self.cfClient.delete_stack(StackName=self.name)
        waiter = self.cfClient.get_waiter("stack_delete_complete")
        waiter.wait(StackName=self.name)
        logger.info("{} deleted".format(self))

    @logFunc()
    def describe(self):
        """
        Describe stack
        """
        resp = self.cfClient.describe_stacks(StackName=self.name)
        stacks = resp.get("Stacks", None)
        logger.info("{} info:\n{}".format(
            self, pprint.pformat(stacks)))

    @logFunc()
    def describe_events(self):
        """
        Describe stack events in reverse chronological order
        """
        resp = self.cfClient.describe_stack_events(StackName=self.name)
        stackEvents = resp.get("StackEvents", None)
        logger.info("{} events:\n{}".format(
            self, pprint.pformat(stackEvents)))

    @logFunc()
    def validate_template(self):
        """
        Validate stack template
        """
        resp = self.cfClient.validate_template(TemplateBody=self.templateStr)
        logger.info("{} Validation Response:\n{}".format(
            self, pprint.pformat(resp)))
