#!/usr/bin/env python3
"""
NAME:
 cli.py

DESCRIPTION
 AWS Cloud Formation Command Line Interface
"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import click
from base.aws.python.CloudFormation.bStack import BeblsoftCloudFormationStack


# ------------------------ GLOBALS ------------------------------------------ #
class GlobalContext():
    """Global Context object for CLI"""

    def __init__(self):
        self.bcfs = None

gc             = GlobalContext()
logger         = logging.getLogger(__file__)
CLICK_CONTEXT  = dict(help_option_names=["-h", "--help"])
homeDir        = os.path.expanduser("~")
stackDict = {
    "vpc": BeblsoftCloudFormationStack(
        name         = "vpc",
        templatePath = "{}/git/beblsoft/base/aws/python/CloudFormation/samples/templates/vpc.yaml".format(homeDir),
        stackTags    = [{"Key": "Name", "Value": "Mojourney"}]),
    "vpcsub": BeblsoftCloudFormationStack(
        name    = "vpcsub",
        templatePath = "{}/git/beblsoft/base/aws/python/CloudFormation/samples/vpcsub.yaml".format(homeDir),
        stackTags    = [{"Key": "Name","Value": "Mojourney"}])
}


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=CLICK_CONTEXT, options_metavar="[options]")
@click.option("--stackname", type=click.Choice(stackDict.keys()), help="Stack to modify")
def cli(stackname):
    """AWS Cloud Formation CLI"""
    gc.bcfs = stackDict[stackname]
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)


@cli.command()
def create_stack():
    """Create stack"""
    gc.bcfs.create_stack()


@cli.command()
def delete_stack():
    """Delete stack"""
    gc.bcfs.delete_stack()


@cli.command()
def describe_stack():
    """Describe stack"""
    gc.bcfs.describe_stack()

@cli.command()
def describe_stack_events():
    """Describe stack events"""
    gc.bcfs.describe_stack_events()


@cli.command()
def validate_template():
    """Validate template"""
    gc.bcfs.validate_template()


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    cli() #pylint: disable=E1120
