OVERVIEW
===============================================================================
Cloud Formation is a service that helps you model and set up your AWS resources.
Developers create templates that describe AWS resources and Cloud Formation
provisions and configures these template resources.


FEATURES
===============================================================================
- Simplify infrastructure management    : Easily manage a set of resources as a single unit
- Quickly replicate infrastructure      : Reuse templates consistently and repeatibly
- Control and track changes to
  infrastructure                        : Templates allow for version control of resources


TEMPLATES
===============================================================================
- General
  * JSON or YAML formatted text file
  * Extensions: .json, .yaml, .template, or .txt
  * Blueprints for building AWS resources
- Anatomy
  Format Version - optional             : CloudFormation template version
  Description    - optional             : Text string that describes the template
  Metadata       - optional             : Objects that provide addition template information
  Parameters     - optional             : Values to pass template at runtime. Can refer
                                          to these values in Resources and Outputs sections
  Mappings       - optional             : Mappings of keys to associated values. That can be looked
                                          up in other places in the template. Map with Fn:FindInMap
  Conditions     - optional             : Conditions that control whether certain resources are created
                                          or properties are assigned a value. (ex. Test vs. Production)
  Transform      - optional             : Version of AWS Serverless Application Model (SAM)
  Resources      - required             : Stack resources and their properties
  Outputs        - optional             : Values that are returned when viewing stack properties
- YAML Template
                                          AWSTemplateFormatVersion: "version date"
                                          Description:
                                            String
                                          Metadata:
                                            template metadata
                                          Parameters:
                                            set of parameters
                                          Mappings:
                                            set of mappings
                                          Conditions:
                                            set of conditions
                                          Transform:
                                            set of transforms
                                          Resources:
                                            set of resources
                                          Outputs:
                                            set of outputs
- Intrinsic Functions
  Base64                          : Return base64 representation of a string
                                    !Base64 <value to encode>
  And                             : Return true if all specified values hold
  Equals                          : Compare if two values are equal
  If                              : Return one value if condition true, another if false
  Not                             : True for false condition, false for true condition
  Or                              : True if anyone of specified conditions is true
  FindInMap                       : Return value corresponding to keys in two-level mapping section
  GetAtt                          : Return attribute of resource in template
  GetAZs                          : Return array of availability zones
  ImportValue                     : Returns the value of an output exported by another stack
  Join                            : Appends a set of values into a single value
  Select                          : Return a single object from list of objects by index
                                    !Select ["1", ["apples", "grapes", "oranges"]]
  Split                           : Split a string into a list of values by delimiter
                                    !Split ["|", "a|b|c"]
  Sub                             : Substitute variables in input string with specified value
                                    Name: !Sub
                                      - www.${Domain}
                                      - {Domain:!Ref RootDomainName}
  Ref                             : Return value of specified parameter or resource


API ACTIONS
===============================================================================
- create-stack                    : Create stack
- list-stacks                     : List stacks created in the past
- describe-stack-events           : Describe events used creating stack
- list-stack-resources            : List resources in stack
- get-template                    : Retrieve stack template
- validate-template               : Validate template
- package                         : Upload artifacts to S3 and update template
- deploy                          : Create a change set, execute change set in same step
- delete                          : Delete a stack



STACKS and CHANGE SETS
===============================================================================
- General
  * Template resources are managed as a single unit called a stack
  * Can create, update, and delete stacks
  * Change sets allow developers to summarize the changes to a stack, before
    the changes are actually applied to the stack
- Create
  * AWS provisions and configures resouces by making calls to AWS services described in template
  * After resouces have been created, CloudFormation reports that stack has been created
  * If stack creation fails, CloudFormation rolls back the changes by deleting the resources
    it created
- Update
  * When modifying a stack, modify the stack's template.
  * To update a stack, create a change set by submitting a modified version of the original
    stack template.
  * CloudFormation compares the modified template with the original and generates a change set
  * After reviewing the changes, update the stack
- Delete
  * Specify stack to delete. CloudFormation deletes the stack and all resources in that stack