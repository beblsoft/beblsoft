OVERVIEW
===============================================================================
Cloud Formation Sample Projects Documentation


PROJECT: BUCKET WEB
===============================================================================
- Description
  * Create static website from an S3 bucket
- To run
  * Go to directory              : cd samples/bucketWeb
  * Create stack                 : aws cloudformation create-stack --stack-name jbensson1 \
                                   --template-body file://./templates/bucketWeb.yaml
  * Describe stack
    Note the BucketName, Website : aws cloudformation describe-stacks --stack-name jbensson1
  * Upload index.html            : aws s3 cp bucketWeb/index.html s3://jbensson1-s3bucket-8n5pxfvobnqn
  * Upload error.html            : aws s3 cp bucketWeb/error.html s3://jbensson1-s3bucket-8n5pxfvobnqn
  * View the website             : Chrome: http://jbensson1-s3bucket-8n5pxfvobnqn.s3-website-us-east-1.amazonaws.com/
  * Delete the stack             : aws cloudformation delete-stack --stack-name jbensson1


PROJECT: CF.PY
===============================================================================
- Description
  * Cloud formation sample stacks
- Sample stacks
  vpc                            : Virtual Private Cloud
  vpcsub                         : Virtual Private Cloud with Subnets