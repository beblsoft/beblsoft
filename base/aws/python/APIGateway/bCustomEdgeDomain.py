#!/usr/bin/env python3
"""
NAME:
 bEdgeCustomDomain.py

DESCRIPTION
 Beblsoft API Gateway Custom Edge Optimized Domain
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bRegion import BeblsoftRegion


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY CUSTOM EDGE DOMAIN ----------- #
class BeblsoftAPIGatewayCustomEdgeDomain():
    """
    Beblsoft API Gateway Custom Edge Domain
    """

    def __init__(self, domainName, bCertificate,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          domainName:
            Custom Edge domain name
            Ex. "api.quotablejoy.com"
          bCertificate:
            Beblsoft ACM Certificate object
        """
        self.domainName   = domainName
        self.bCertificate = bCertificate
        self.bRegion      = bRegion
        self.agClient     = boto3.client("apigateway",
                                         region_name=self.bRegion.name)
        self.cfClient     = boto3.client("cloudfront")

    def __str__(self):
        return "[{} domainName={}]".format(self.__class__.__name__, self.domainName)

    @property
    def cfDomain(self):
        """
        Return cloudfront distribution domain
        """
        cfd  = None
        meta = self.__getMetadata()
        if meta:
            cfd = meta.get("distributionDomainName", None)
        return cfd

    @property
    def cfId(self):
        """
        Return cloudfront distribution id
        """
        cfId     = None
        match    = None
        cfDomain = self.cfDomain
        if cfDomain:
            match = re.match(r"(.*)\.cloudfront\.net", cfDomain)
        if match:
            cfId = match.group(1)
        return cfId

    @property
    def exists(self):
        """
        Return True if Edge Domain Exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @logFunc()
    def create(self):
        """
        Create Edge Domain
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.create_domain_name(
                domainName=self.domainName, certificateName=self.bCertificate.domainName,
                certificateArn=self.bCertificate.arn,
                endpointConfiguration={"types": ["EDGE"]})
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete Edge Domain
        """
        if self.exists:
            self.agClient.delete_domain_name(
                domainName=self.domainName)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe Edge Domain
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Get Edge Domain metadata
        """
        meta = None
        try:
            meta = self.agClient.get_domain_name(domainName=self.domainName)
        except ClientError as e:
            if e.response["Error"]["Code"] == "NotFoundException":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        return meta
