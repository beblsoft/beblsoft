#!/usr/bin/env python3
"""
NAME:
 bResource.py

DESCRIPTION
 Beblsoft API Gateway Resource Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY RESOURCE --------------------- #
class BeblsoftAPIGatewayResource():
    """
    Beblsoft API Gateway Resource
    """

    def __init__(self, bAGRestAPI, bAGParentRes, pathPart):
        """
        Initialize Object
        Args
          bAGRestAPI:
            BeblsoftAPIGatewayRestAPI
          bAGParentRes:
            Parent BeblsoftAPIGatewayResource
          pathPart:
            Last Segment for resource
        """
        self.bAGRestAPI   = bAGRestAPI
        self.bAGParentRes = bAGParentRes
        self.pathPart     = pathPart
        self.bRegion      = bAGRestAPI.bRegion
        self.agClient     = boto3.client("apigateway",
                                         region_name=self.bRegion.name)

    def __str__(self):
        return "[{} pathPart={}]".format(self.__class__.__name__, self.pathPart)

    @property
    def id(self):
        """
        Return Resource ID
        """
        resID = None
        meta = self.getMetadata()
        if meta:
            resID = meta.get("id", None)
        return resID

    @property
    def apiID(self):
        """
        Return API ID
        """
        return self.bAGRestAPI.id

    @logFunc()
    def create(self):
        """
        Create Resource
        """
        resID = self.id
        if resID:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.create_resource(
                restApiId=self.apiID,
                parentId=self.bAGParentRes.id,
                pathPart=self.pathPart)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete Resource
        """
        resID = self.id
        if resID:
            self.agClient.delete_resource(
                restApiId=self.apiID,
                resourceId=self.id)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe Resource
        """
        meta = self.getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def invoke(self, httpMethod, pathWithQueryString="", body="",
               headers=None, stageVariables=None):
        """
        Invoke resource
        Args
          httpMethod:
            Invoke method
          pathWithQueryString:
            URI path, including query string
          body:
            Simulated request body
          headers:
            Key value dictionary of simulated headers
          stageVariables:
            Key value dictionary of stage variables
        """
        if not headers:
            headers        = {}
        if not stageVariables:
            stageVariables = {}

        resp = self.agClient.test_invoke_method(
            restApiId=self.apiID,
            resourceId=self.id,
            httpMethod=httpMethod,
            pathWithQueryString=pathWithQueryString,
            body=body,
            headers=headers,
            stageVariables=stageVariables)
        logger.info("{} {} Invoked:\n{}".format(
            self, httpMethod, pprint.pformat(resp)))
        return resp

    @logFunc()
    def getMetadata(self):
        """
        Return Resource Metadata
        """
        meta = None
        for resMeta in BeblsoftAPIGatewayResource.getAllResMetadata(self.bAGRestAPI):
            matchingParent   = resMeta.get("parentId", None) == self.bAGParentRes.id
            matchingPathPart = resMeta.get("pathPart", None) == self.pathPart
            if matchingParent and matchingPathPart:
                meta = resMeta
                break
        return meta

    @staticmethod
    def getAllResMetadata(bAGRestAPI, limit=500):
        """
        Return All Resources Metadata
        """
        apiID      = bAGRestAPI.id
        agClient   = boto3.client("apigateway", region_name=bAGRestAPI.bRegion.name)
        allResMeta = []
        if apiID:
            resp = agClient.get_resources(restApiId=apiID, limit=limit)
            allResMeta = resp.get("items", [])
        return allResMeta
