#!/usr/bin/env python3
"""
NAME:
 bLambdaIntegration.py

DESCRIPTION
 Beblsoft API Gateway Lambda Integration Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.APIGateway.bIntegration import BeblsoftAPIGatewayIntegration
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY LAMBDA INTEGRATION ----------- #
class BeblsoftAPIGatewayLambdaIntegration(BeblsoftAPIGatewayIntegration):
    """
    Beblsoft API Gateway Lambda Integration
    """

    def __init__(self, bAGMethod, bLambdaFunction, bIAMRole, *args, **kwargs):
        """
        Initialize Object
        Args
          bAGMethod:
            Beblsoft API Gateway Method
          bLambdaFunction:
            Beblsoft Lambda Function
          bIAMRole:
            Beblsoft IAM Role
        """
        self.bLambdaFunction = bLambdaFunction
        super().__init__(bAGMethod=bAGMethod, iType="AWS_PROXY", bIAMRole=bIAMRole,
                         integrationHttpMethod="POST", *args, **kwargs)

    @property
    def uri(self):
        """
        Return endpoint URI
        """
        return "arn:aws:apigateway:{}:lambda:path/2015-03-31/functions/{}/invocations".format(
            self.bRegion.name, self.bLambdaFunction.arn)

    @logFunc()
    def create(self):
        """
        Create integration
        """
        if self.exists:
            logger.info("{} Already exists".format(self))
        else:
            self.agClient.put_integration(
                restApiId=self.apiID,
                resourceId=self.resID,
                httpMethod=self.httpMethod,
                type=self.type,
                integrationHttpMethod=self.integrationHttpMethod,
                uri=self.uri,
                credentials=self.bIAMRole.arn,
                passthroughBehavior=self.passThroughBehavior)
            logger.info("{} Created".format(self))
