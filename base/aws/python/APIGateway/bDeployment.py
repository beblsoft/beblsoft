#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft API Gateway Deployment Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY DEPLOYMENT ------------------- #
class BeblsoftAPIGatewayDeployment():
    """
    Beblsoft API Gateway Deployment
    """

    def __init__(self, bAGRestAPI, description=None):
        """
        Initialize Object
        Args
          bAGRestAPI:
            Beblsoft API Gateway Rest API
          description:
            Description of deployment resource
        """
        self.bAGRestAPI       = bAGRestAPI
        if description:
            self.description  = description
        else:
            self.description  = "{} Deployment".format(self.bAGRestAPI.name)
        self.bRegion          = bAGRestAPI.bRegion
        self.agClient         = boto3.client("apigateway",
                                             region_name=self.bRegion.name)

    def __str__(self):
        return "[{} restAPI={}]".format(
            self.__class__.__name__, self.bAGRestAPI.name)

    @property
    def exists(self):
        """
        Return True if deployment exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def id(self):
        """
        Return deployment ID
        """
        dID  = None
        meta = self.__getMetadata()
        if meta:
            dID = meta.get("id", None)
        return dID

    @property
    def apiID(self):
        """
        Return rest api ID
        """
        return self.bAGRestAPI.id

    @logFunc()
    def create(self):
        """
        Create deployment
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.create_deployment(
                restApiId=self.apiID,
                description=self.description)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete deployment
        """
        if self.exists:
            self.agClient.delete_deployment(
                restApiId=self.apiID,
                deploymentId=self.id)
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe deployment
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Get deployment metadata
        """
        dMeta     = None
        dMetaList = BeblsoftAPIGatewayDeployment.__getAllMetadata(self.bAGRestAPI)
        for curDMeta in dMetaList:
            if curDMeta.get("description", None) == self.description:
                dMeta = curDMeta
                break
        return dMeta

    @staticmethod
    def __getAllMetadata(bAGRestAPI, limit=500):
        """
        Return metadata on all deployments
        """
        dMetaList = []
        apiID     = bAGRestAPI.id
        agClient  = boto3.client("apigateway",
                                 region_name=bAGRestAPI.bRegion.name)
        if apiID:
            resp = agClient.get_deployments(restApiId=apiID, limit=limit)
            dMetaList = resp.get("items", [])
        return dMetaList
