#!/usr/bin/env python3
"""
NAME:
 bRestAPI.py

DESCRIPTION
 Beblsoft API Gateway Rest API Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY REST API --------------------- #
class BeblsoftAPIGatewayRestAPI():
    """
    Beblsoft API Gateway Rest API
    """

    def __init__(self, name, description, endpointConfig="EDGE",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            API name
          description:
            API description
          endpointConfig:
            Endpoint configuration of this RestAPI
            One of: "REGIONAL", "EDGE"
          bRegion:
            Beblsoft Region Object
        """
        self.name           = name
        self.description    = description
        self.endpointConfig = endpointConfig
        self.bRegion        = bRegion
        self.agClient       = boto3.client("apigateway", region_name=bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @property
    def id(self):
        """
        Return Rest API ID
        """
        apiID   = None
        meta = self.__getMetadata()
        if meta:
            apiID = meta.get("id", None)
        return apiID

    @logFunc()
    def create(self):
        """
        Create Rest API
        """
        apiID = self.id
        if apiID:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.create_rest_api(name=self.name, description=self.description)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete Rest API
        """
        try:
            agID = self.id
            if not agID:
                logger.info("{} Never existed".format(self))
            else:
                self.agClient.delete_rest_api(restApiId=self.id)
                logger.info("{} Deleted".format(self))
        except ClientError as e:
            logger.exception(e.__dict__)
            raise e

    @logFunc()
    def describe(self):
        """
        Describe Rest API
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self, limit=500):
        """
        Return Rest API Metadata
        """
        meta    = None
        resp    = self.agClient.get_rest_apis(limit=limit)
        apiList = resp.get("items", None)
        for api in apiList:
            if api.get("name") == self.name:
                meta = api
                break
        return meta
