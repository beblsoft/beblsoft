#!/usr/bin/env python3
"""
NAME:
 bMethod.py

DESCRIPTION
 Beblsoft API Gateway Method Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY METHOD ----------------------- #
class BeblsoftAPIGatewayMethod():
    """
    Beblsoft API Gateway Method
    """

    def __init__(self, bAGResource, httpMethod, apiKeyRequired=False,
                 operationName="RandomOperation"):
        """
        Initialize Object
        Args
          bAGResource:
            Beblsoft API Gateway Resource
          httpMethod:
            Request HTTP method
            One of: GET, HEAD, PUT, POST, DELETE, ANY
          apiKeyRequired:
            If True, method requires API key
          operationName:
            Human-friendly operation id for the method
            Ex. "ListPets" for GET on /pets
        """
        self.bAGResource       = bAGResource
        self.httpMethod        = httpMethod
        self.authorizationType = "NONE"  # One of: NONE, AWS_IAM, or CUSTOM
        self.apiKeyRequired    = apiKeyRequired
        self.operationName     = operationName
        self.bRegion           = bAGResource.bRegion
        self.agClient          = boto3.client("apigateway",
                                              region_name=self.bRegion.name)

    def __str__(self):
        return "[{} res={} httoMethod={}]".format(
            self.__class__.__name__, self.bAGResource.pathPart, self.httpMethod)

    @property
    def exists(self):
        """
        Return True if method exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def apiID(self):
        """
        Return API ID
        """
        return self.bAGResource.bAGRestAPI.id

    @property
    def resID(self):
        """
        Return resource ID
        """
        return self.bAGResource.id

    @logFunc()
    def create(self):
        """
        Create method
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.put_method(
                restApiId=self.apiID,
                resourceId=self.resID,
                httpMethod=self.httpMethod,
                authorizationType=self.authorizationType,
                apiKeyRequired=self.apiKeyRequired,
                operationName=self.operationName)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete method
        """
        if self.exists:
            self.agClient.delete_method(
                restApiId=self.apiID,
                resourceId=self.resID,
                httpMethod=self.httpMethod)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe method
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Return method metdata
        """
        meta  = None
        try:
            if (self.apiID and self.resID):
                meta = self.agClient.get_method(
                    restApiId=self.apiID,
                    resourceId=self.resID,
                    httpMethod=self.httpMethod)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "NotFoundException":
                logger.exception(e.__dict__)
                raise(e)
        return meta
