#!/usr/bin/env python3
"""
NAME:
 bRootResource.py

DESCRIPTION
 Beblsoft API Gateway Root Resource Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.APIGateway.bResource import BeblsoftAPIGatewayResource
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY ROOT RESOURCE ---------------- #
class BeblsoftAPIGatewayRootResource(BeblsoftAPIGatewayResource):
    """
    Beblsoft API Gateway Root Resource
    """

    def __init__(self, bAGRestAPI):
        """
        Initialize Object
        Args
          bAGRestAPI:
            BeblsoftAPIGatewayRestAPI
        """
        super().__init__(bAGRestAPI=bAGRestAPI, bAGParentRes=None, pathPart="/")

    @logFunc()
    def create(self):
        """
        Override create, cannot create root resource
        """
        pass

    @logFunc()
    def delete(self):
        """
        Override delete, cannot delete root resource
        """
        pass

    @logFunc()
    def getMetadata(self):
        """
        Return Root Resource Metadata
        """
        meta = None
        for resMeta in BeblsoftAPIGatewayRootResource.getAllResMetadata(self.bAGRestAPI):
            matchingPath = resMeta.get("path", None) == self.pathPart
            if matchingPath:
                meta = resMeta
                break
        return meta
