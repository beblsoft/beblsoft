#!/usr/bin/env python3
"""
NAME:
 bStage.py

DESCRIPTION
 Beblsoft API Gateway Stage Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY STAGE ------------------------ #
class BeblsoftAPIGatewayStage():
    """
    Beblsoft API Gateway Stage
    """

    def __init__(self, bAGDeployment, name, description=None,
                 cacheClusterEnabled=False, cacheClusterSize="0.5",
                 variables=None):
        """
        Initialize Object
        Args
          bAGDeployment:
            Beblsoft API Gateway Deployment Object
          name:
            stage name
          description:
            stage description
          cacheClusterEnabled:
            If True, enable cache for Stage
          cacheClusterSize:
            Cache size
            One of:"0.5"|"1.6"|"6.1"|"13.5"|"28.4"|"58.2"|"118"|"237"
          variables:
            Dictionary of variables for stage resource
        """
        self.bAGDeployment        = bAGDeployment
        self.name                 = name
        if description:
            self.description      = description
        else:
            self.description      = "{} Stage".format(name)
        self.cacheClusterEnabled  = cacheClusterEnabled
        self.cacheClusterSize     = cacheClusterSize
        if variables:
            self.variables        = variables
        else:
            self.variables        = {}
        self.bRegion              = bAGDeployment.bRegion
        self.agClient             = boto3.client("apigateway",
                                                 region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @property
    def exists(self):
        """
        Return True if stage exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def apiID(self):
        """
        Return Rest API ID
        """
        return self.bAGDeployment.bAGRestAPI.id

    @property
    def deploymentID(self):
        """
        Return Deployment ID
        """
        return self.bAGDeployment.id

    @property
    def url(self):
        """
        Return stage url
        """
        url = None
        if self.exists:
            url = "https://{}.execute-api.{}.amazonaws.com/{}".format(
                self.apiID, self.bAGDeployment.bAGRestAPI.bRegion.name, self.name)
        return url

    @logFunc()
    def create(self):
        """
        Create stage
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            self.agClient.create_stage(
                restApiId=self.apiID,
                stageName=self.name,
                deploymentId=self.deploymentID,
                description=self.description,
                cacheClusterEnabled=self.cacheClusterEnabled,
                cacheClusterSize=self.cacheClusterSize,
                variables=self.variables)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete stage
        """
        if self.exists:
            self.agClient.delete_stage(
                restApiId=self.apiID,
                stageName=self.name)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe stage
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))
        logger.info("{} URL={}".format(self, self.url))

    @logFunc()
    def __getMetadata(self):
        """
        Get stage metadata stage
        """
        meta = None
        try:
            apiID = self.apiID
            if apiID:
                meta = self.agClient.get_stage(
                    restApiId=self.apiID,
                    stageName=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "NotFoundException":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
