#!/usr/bin/env python3
"""
NAME:
 bBasePathMapping.py

DESCRIPTION
 Beblsoft API Gateway Base Path Mapping
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY BASE PATH MAPPING ------------ #
class BeblsoftAPIGatewayBasePathMapping():
    """
    Beblsoft API Gateway Base Path Mapping
    """

    def __init__(self, domainName, bAGStage, basePath=""):
        """
        Initialize object
        Args
          domainName:
            Domain Name of base path mapping
            Ex. "api.quotablejoy.com"
          bAGStage:
            Beblsoft API Gateway Stage Object
          basePath:
            The base path name that callers of the API must provide as part
            of the URL after the domain name.
            Ex. "foo"
            Then "api.quotablejoy.com/foo" would map to stage
        """
        self.domainName     = domainName
        if basePath:
            self.basePath   = basePath
        else:
            self.basePath   = "(none)"
        self.bAGStage       = bAGStage
        self.bAGRestAPI     = bAGStage.bAGDeployment.bAGRestAPI
        self.bRegion        = bAGStage.bRegion
        self.agClient       = boto3.client("apigateway",
                                       region_name=self.bRegion.name)

    def __str__(self):
        return "[{} domainName={} basePath={}]".format(
            self.__class__.__name__, self.domainName, self.basePath)

    @property
    def exists(self):
        """
        Return True if Base Path Mapping Exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @logFunc()
    def create(self):
        """
        Create base path mapping
        """
        if self.exists:
            logger.info("{} Already exists".format(self))
        else:
            self.agClient.create_base_path_mapping(
                domainName=self.domainName, basePath=self.basePath,
                restApiId=self.bAGRestAPI.id, stage=self.bAGStage.name)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete base path mapping
        """
        if self.exists:
            self.agClient.delete_base_path_mapping(
                domainName=self.domainName, basePath=self.basePath)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe base path mapping
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Get base path mapping metadata
        """
        meta = None
        try:
            meta = self.agClient.get_base_path_mapping(
                domainName=self.domainName, basePath=self.basePath)
        except ClientError as e:
            if e.response["Error"]["Code"] == "NotFoundException":
                pass
            else:
                logger.info(pprint.pformat(e.__dict__))
                raise e
        return meta
