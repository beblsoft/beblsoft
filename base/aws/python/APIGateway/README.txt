OVERVIEW
==============================================================================
This document overviews API Gateway


OBJECT ARCHITECTURE
==============================================================================
  Route53                   - Human readable name
    |                         Ex. app.quotablejoy.com
    |
  Custom Domain             - Support regional or global domains
    |                         Ex. d2sxs4i9nlamdk.cloudfront.net                     [Edge Optimized]
    |                         Ex. d-numh1z56v6.execute-api.us-west-2.amazonaws.com  [Regional]
    |
  Base Path Mapping         - Map domains to one or more stage deployments
    |
  Stage                     - Allow for multiple api versions. Or prod/dev
    |                         Ex. https://6z25k1haj9.execute-api.us-east-1.amazonaws.com/Default
    |
  Deployment                - Deploy and API
    |
  Rest API                  - Encapsulates all api resources and methods
    |
  Root Resource             - The base API resource, all other resources
    |                         connect back to root resource
    |
  Resource                  - Endpoint in the API
    |
  Method                    - Method on resource
    |                         Ex. GET, POST, DELETE, HEAD
    |
  [Lambda] Integration      - Map API Gateway resource/method to other AWS
    |                         Functionality
    |
  Lambda Function           - Lambda handler
                              Ex. QuoteProdFlask