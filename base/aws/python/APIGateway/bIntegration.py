#!/usr/bin/env python3
"""
NAME:
 bIntegration.py

DESCRIPTION
 Beblsoft API Gateway Integration Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import abc
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT API GATEWAY INTEGRATION ------------------ #
class BeblsoftAPIGatewayIntegration(abc.ABC):
    """
    Beblsoft API Gateway Integration
    """

    def __init__(self, bAGMethod, iType, bIAMRole,
                 integrationHttpMethod="ANY",
                 passThroughBehavior="NEVER", timeoutMS=29000,
                 cacheNamespace="none", cacheKeyParemeters=None):
        """
        Initialize Object
        Args
          bAGMethod:
            Beblsoft API Gateway Method
          iType:
            Integration Type
            One of: HTTP, AWS, MOCK, HTTP_PROXY, AWS_PROXY
          bIAMRole:
            Beblsoft IAM Role
          bRegion:
            Beblsoft Region Object
          integrationHttpMethod:
            Integration HTTP Method
            Required when integration is HTTP or AWS
          passthroughBehavior:
            Specifies the passthrough behavior for incoming request based on
            Content-Type header in the request
            One of: NEVER, WHEN_NO_MATCH, WHEN_NO_TEMPLATES
          timeoutMS:
            Custom timeout between 50 and 29,000 milliseconds
          cacheNamespace:
            Put integration cache namespace
          cacheKeyParameters:
            Put integration input's cache key parameters
        """
        self.bAGMethod               = bAGMethod
        self.type                    = iType
        self.bIAMRole                = bIAMRole
        self.integrationHttpMethod   = integrationHttpMethod
        self.passThroughBehavior     = passThroughBehavior
        self.timeoutMS               = timeoutMS
        self.cacheNamespace          = cacheNamespace
        if cacheKeyParemeters:
            self.cacheKeyParemeters  = cacheKeyParemeters
        else:
            self.cacheKeyParameters  = []
        self.bRegion                 = bAGMethod.bRegion
        self.agClient                = boto3.client("apigateway",
                                                    region_name=self.bRegion.name)

    def __str__(self):
        return "[{} res={} httpMethod={}]".format(
            self.__class__.__name__, self.bAGMethod.bAGResource.pathPart,
            self.httpMethod)

    @property
    def exists(self):
        """
        Return True if integration exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def apiID(self):
        """
        Return API ID
        """
        return self.bAGMethod.bAGResource.bAGRestAPI.id

    @property
    def resID(self):
        """
        Return resource ID
        """
        return self.bAGMethod.bAGResource.id

    @property
    def httpMethod(self):
        """
        Return http method
        """
        return self.bAGMethod.httpMethod

    @abc.abstractmethod
    def create(self):
        """
        Create integration
        """
        pass

    @logFunc()
    def delete(self):
        """
        Delete integration
        """
        if self.exists:
            self.agClient.delete_integration(
                restApiId=self.apiID,
                resourceId=self.resID,
                httpMethod=self.httpMethod)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe integration
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Get integration metadata
        """
        meta = None
        try:
            if (self.apiID and self.resID):
                meta = self.agClient.get_integration(
                    restApiId=self.apiID,
                    resourceId=self.resID,
                    httpMethod=self.httpMethod)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "NotFoundException":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
