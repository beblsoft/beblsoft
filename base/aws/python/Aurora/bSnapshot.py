#!/usr/bin/env python3
"""
NAME:
 bSnapshot.py

DESCRIPTION
 Beblsoft Aurora Database Cluster Snapshot Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT DATABASE CLUSTER SNAPSHOT ---------------- #
class BeblsoftDatabaseClusterSnapshot(BeblsoftAWSObject):
    """
    Beblsoft Database Cluster Snapshot
    """

    def __init__(self,
                 snapshotID,
                 bDBCluster):
        """
        Initialize object
        Args
          snapshotID:
            Identifier of the db snapshot
            Ex. "SmecknTestSnap"
          bDBCluster:
            Beblsoft Database Cluster
        """
        super().__init__()
        self.id          = snapshotID
        self.bDBCluster  = bDBCluster
        self.bRegion     = bDBCluster.bRegion
        self.rdsClient   = boto3.client("rds",
                                        region_name=self.bRegion.name)

    def __str__(self):
        return "[{} dbClusterID={} id={}]".format(
            self.__class__.__name__, self.bDBCluster.id, self.id)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def arn(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("DBClusterSnapshotArn")

    @property
    def status(self): #pylint: disable=C0111
        return self.getValueFromMetadata("Status")

    @property
    def percentProgress(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("PercentProgress")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self):  # pylint: disable=W0221
        """
        Create object
        """
        kwargs                                 = {}
        kwargs["DBClusterSnapshotIdentifier"]  = self.id
        kwargs["DBClusterIdentifier"]          = self.bDBCluster.id
        # kwargs["Tags"]                       = []
        self.rdsClient.create_db_cluster_snapshot(**kwargs)
        self.waitChange(changeFunc=lambda: self.status == "available")

    @logFunc()
    def _delete(self):  # pylint: disable=W0221
        """
        Delete object
        """
        self.rdsClient.delete_db_cluster_snapshot(
            DBClusterSnapshotIdentifier = self.id)
        self.waitChange(changeFunc=lambda: self.arn is None)

    @logFunc()
    def _copy(self, targetSnapID):  # pylint: disable=W0221
        """
        Copy object
        Args
          targetSnapID:
            ID of newly created snapshot
        """
        self.rdsClient.delete_db_cluster_snapshot(
            SourceDBClusterSnapshotIdentifier = self.id,
            TargetDBClusterSnapshotIdentifier = targetSnapID)

    @staticmethod
    def describeAll(bDBCluster=None,
                    bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all objects
        """
        rdsClient        = boto3.client("rds", region_name=bRegion.name)
        marker           = None
        snapList         = []
        while True:
            kwargs                            = {}
            kwargs["Marker"]                  = marker
            if bDBCluster:
                kwargs["DBClusterIdentifier"] = bDBCluster.id
            resp         = rdsClient.describe_db_cluster_snapshots(**kwargs)
            snapList    += resp.get("DBClusterSnapshots", [])
            # Continue looping?
            marker       = resp.get("Marker", None)
            if not marker:
                break

        logger.info("Database Cluster Snapshots:\n{}")
        logger.info(pprint.pformat(snapList))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.describe_db_cluster_snapshots
        """
        meta = None
        resp = self.rdsClient.describe_db_cluster_snapshots(
            DBClusterIdentifier=self.bDBCluster.id,
            DBClusterSnapshotIdentifier=self.id)
        snapList = resp.get("DBClusterSnapshots", None)
        if snapList:
            meta = snapList[0]
        return meta
