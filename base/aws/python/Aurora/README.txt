OVERVIEW
===============================================================================
Amazon Aurora Technology Documentation
- Description
  * Amazon Aurora is a MySQL and PostgreSQL compatible relational database built
    for the cloud, that combines the performance and availability of high-end
    commercial databases with the simplicity and cost-effectiveness of open source
    databases.
  * Aurora features a distributed, fault-tolerant, self-healing storage system
    that auto-scales up to 64TB per database instance. Aurora delivers high
    performance and availability with up to 15 low-latency read replicas,
    point-in-time recovery, continuous backup to Amazon S3, and replication across
    three Availability Zones.
- Features
  * High Performance and Scalability: 5x throughput of MySQL, 3x of PostgreSQL
  * High Availablility, Durability  : 99.99% availability
  * Highly Secure                   : VPC Isolation, encrpytion
  * MySQL and PostgeSQL Compatible  : Code, applications, drivers, and tools
                                      need little or no change
  * Fully Managed                   : Software patching, setup, config, and backups
  * Migration Support               : Support for migrating existing DBs


RELEVANT URLS
===============================================================================
- Home                              : https://aws.amazon.com/rds/aurora/
- Aurora Documentation              : https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Aurora.html
- RDS Documentation                 : https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html
- Aurora MySQL Parameters           : https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraMySQL.Reference.html#AuroraMySQL.Reference.ParameterGroups
- Wikipedia                         : https://en.wikipedia.org/wiki/Amazon_Aurora


RELATIONAL DATABASE SERVICE (RDS) TERMS
===============================================================================
- DB Instance                       : Basic building block of RDS
                                      Isolated database environment in the cloud
                                      Can contain multiple user-created databases
                                      Each runs a DB engine
- DB Engine                         : Ex. MySQL, MariaDB, PostgreSQL, Oracle, and Microsoft SQL Server
                                      Parameters set by parameter group
- DB Parameter Group                : Parameters to control the behavior of databases
- DB Instance Class                 : Sets the DB instance computation and memory capacity
- DB Instance Storage               : Comes in 3 types
                                      Magnetic, General Purpose(SSD), and Provisioned IOPs
- Virtual Private Cloud (VPC)       : DB instnace can be run inside a VPC
                                      Can control network environment: subnets, routing, and acls
- Security Group                    : Controls access to DB instance
- Cloud Watch Monitoring            : Use CloudWatch to monitor DB instance


HIGH LEVEL
===============================================================================
- DB Cluster                        : One or more DB instances, and a cluster volume
                                      that manages the data for those instances
- Cluster Volume                    : Virtual database storage volume that spans
                                      multiple availability zones
                                      Copied across multiple AZs in a single region
                                      Can grow to a maxiumum size of 64 TiB
- Instances Types
  Primary instance                  : Supports read and write ops
                                      Performs all data modifications to cluster volume
                                      Each cluster has 1 primary instance
  Replica instance                  : Supports read operations only
                                      Usually less than 100 ms lag
- DB Cluster
  --------------------------------------------------------------------------
  | Amazon Aurora DB Cluster                                               |
  |                                                                        |
  | ---------------------   ---------------------    --------------------- |
  | |                   |   |                   |    |                   | |
  | |     --------      |   |     ---------     |    |     ---------     | |
  | |     |Master|      |   |     |Replica|     |    |     |Replica|     | |
  | |     --------      |   |     ---------     |    |     ---------     | |
  | |   Reads  | |      |   |   Reads  | |      |    |   Reads  | |      | |
  | |   Writes v ^      |   |          v ^      |    |          v ^      | |
  | -----------|-|-------   -----------|-|-------    -----------|-|------- |
  |            | |                     | |                      | |        |
  |  --------------------------------------------------------------------  |
  |  | Cluster Volume                                                   |  |
  |  |  ----------  ----------  ----------  ----------       ---------- |  |
  |  |  | Disk 1 |  | Disk 2 |  | Disk 3 |  | Disk 4 |  ...  | Disk n | |  |
  |  |  ----------  ----------  ----------  ----------       ---------- |  |
  |  --------------------------------------------------------------------  |
  |                                                                        |
  --------------------------------------------------------------------------
- Endpoints                         : Connect to DB instances using an endpoint
                                      An endpoint is a URL that contains host
                                      addresses and a port
  Cluster Endpoint                  : Endpoint that connects to primary instance
                                      Read, write, DDL changes
                                      Aurora will automatically fail over the endpoint if primary fails
                                      Ex. mydbcluster.cluster-123456789012.us-east-1.rds.amazonaws.com:3306
  Read Endpoint                     : Endpoint for cluster that connects to one of replicas
                                      Load balancing support for read operations
                                      Ex. mydbcluster.cluster-ro-123456789012.us-east-1.rds.amazonaws.com:3306
  Instance Endpoint                 : Endpoint that connects to specific DB instance
                                      Ex. mydbinstance.123456789012.us-east-1.rds.amazonaws.com:3306


MANAGING A DB CLUSTER
===============================================================================
- Storage Scaling                   : Volume automatically scales in 10 gigibyte
                                      increments to 64 TiB
- Instance Scaling                  :                            vCPU  ECU  Memory (GiB)
                                      Smallest = db.t2.small        1    1      2
                                      Largest  = db.r4.16xlarge    64  195    488
- Read Scaling                      : See replication below
                                      Smallest = db.t2.small                  45
                                      Largest  = db.r4.16xlarge             6000
                                      Set max_connections parameter, up to 16,000
- Fault Tolerance                   : Cluster Volume spans multiple AZs. Each AZ has a single copy of the data
                                      If primary instance fails:
                                      - replica is promoted (less than 120 seconds)
                                        replicas are promoted based on priority
                                      - new primary is created (less than 10 minutes)
- Backing Up and Restoring          : Aurora auto backs up cluster volume and retains restore data for
                                      backup retention period (1-35 days)
                                      Backups are continuous and incremental
                                      Can snapshot a backup
                                      Recover data by creating a new Aurora DB cluster from the backup data
                                      Latest Restorable Time/ Earliest Restorable Time in RDS console (typically within 5 minutes)
- Backtracking                      : Backtrack a DB server to rewind the DB cluster to a previous point in time
- Cloning                           : See cloning below
- Instance Parameters               : Set parameters in DB parameter group
                                      Parameters apply at cluster level or instance level


CLONING
===============================================================================
- High Level                        : Using cloning, one can quickly and cost-effectively
                                      create clones of ones databases
- Use Cases                         : Experiment and assess impact of schema changes
                                      Perform workload-intensive operations
                                      Create a copy of a production DB cluster for non-production
- Limits                            : Clone DBs must be in same AWS region
                                      Only 15 clones per copy
                                      Different VPCs allowed, but subnets must be the same
- Copy-On-Write                     : Data is copied at the time the data chanes, either on the source
                                      database or the clone database
- Source Deletion                   : When deleting a source database that has one or more
                                      associated clones, the clones are not affected
- Commands                          : restore-db-cluster-to-point-in-time


REPLICATION
===============================================================================
- Type 1
  Read Replicas                     : Used for read scaling
                                      Max 15 replicas per cluster
                                      Lag less than 100 milliseconds
                                      Replicas are failover targets, if primary fails, replica is promoted to primary
                                      Use REPEATABLE READ default transaction isolation level
                                      DDL statements on primary cause replica connection interruption
                                      CloudWatch ReplicaLag metric
                                      Query mysql.ro_replica_status check the value in the Replica_lag_in_msec
- Type 2
  DB Clusters in same region
  with binlog replication           : Steps Below
                                      1 Enable Binary logs on replication master
                                        binlog_format to MIXED
                                      2 Retain binary logs on replication master until no longer needed
                                        On master: CALL mysql.rds_set_configuration('binlog retention hours', 144);
                                      3 Create a snapshot of the replication master
                                        aws rds describe-events: Save the snapshot binlog position, mysql-bin-changelog.000003 4278
                                      4 Copy the snapshot
                                      5 Load the snapshot into replica target
                                        Restore target from master snapshot
                                      6 Enable replication on replica target
                                        Before doing so, create a snapshot of replica target in case anything goes wrong
                                        On target, create user id for replication:
                                          mysql> CREATE USER 'repl_user'@'<domain_name>' IDENTIFIED BY '<password>';
                                        On master, update with target privileges:
                                          mysql>GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'repl_user'@'<domain_name>';
                                        On target, start the replication:
                                          CALL mysql.rds_set_external_master ('mydbinstance.123456789012.us-east-1.rds.amazonaws.com', 3306,
                                          'repl_user', '<password>', 'mysql-bin-changelog.000031', 107, 0);
                                          CALL mysql.rds_start_replication;
                                      7 Monitor replica
                                        Check for Master Failover
                                        SHOW SLAVE STATUS
- Type 3
  DB Clusters in different regions,
  using read replica in different
  region                            : See Documentation


SERVERLESS
===============================================================================
- Description
  * Amazon Aurora Serverless is an on-demand, autoscaling configuration for Amazon Aurora.
  * Serverless DB cluster is a DB cluster that automatically starts up, shuts down,
    and scales up or down capacity based on your application's needs.
  * Serverless provides a relatively simple, cost-effective option for infrequent,
    intermittent, or unpredictable workloads. It can provide this because it automatically
    starts up, scales capacity to match your application's usage, and shuts down
    when it's not in use.
  * Non-serverless DB cluster is called a provisioned cluster
- Advantages
  Simpler                           : Removes the complexity of managing DB instances
  Scalable                          : Scales compute and memory as needed
  Cost-effective                    : Pay for the data consume on a per-second basis
  Highly-Available Storage          : Fault-tolerant, distributed storage with six way replication
- Use Cases
  Infrequently Used Applications    : Will turn off when not in use
  New Applications                  : Will scale to desired load
  Variable Workloads
  Unpredictable Workloads
  Development and test databases
  Multi-tenant applications
- Architecture
  * Proxy Fleet                     : DB endpoint connects to proxy fleet that routes
                                      workload to fleet of resources that is automatically scaled
  * Engine mode                     : New "serverless" DB engine mode
                                      non-serverless use "provisioned" DB engine mode
  * Picture                         : ---------------
                                      | Application |
                                      ---------------
                                         |
                                      ---------------
                                      | Proxy Fleet |
                                      ---------------
                                         |
                                      ---------------
                                      | DB Pool     |
                                      ---------------
                                         |
                                      ---------------
                                      | DB Storage  |
                                      ---------------
  * ACUs (Aurora Capacity Units)   : Combo of processing and memory
                                     DB storage scales from 10GiB to 64 TiB
                                     Min ACU is lowest capacity to which DB can scale down
                                     Max ACU is highest capacity to which DB can scale up
  * Warm pool of resources         : Managed by serverless in an AWS region to minimize
                                     scaling time. When serverless adds new resources to
                                     DB cluster, it uses the proxy fleet to switch
                                     active client connections to the new resources.
- Autoscaling
  * Load-Based                     : Seamless scaling based on load (CPU utilization
                                     and number of connections)
                                     Goes to zero capacity if there are no
                                     connections for 5-minute period
  * Scale up                       : If any of conditions are met
                                     CPU > 70 %
                                     > 90 % connections used
                                     Cooldown is 3 minutes after last op
  * Scale down                     : If all of conditions are met
                                     CPU < 30 %
                                     < 40 % connections used
                                     Cooldown is 15 minuets since last scaling op
  * Scaling Point                  : Time at which the database can safely initiate
                                     scaling operation. Might not be able to scale if:
                                     Long-running queries, temporary tables/locks
- Auto Pause and Resume
  * Pause                          : Can choose to pause cluster after a given
                                     amount of time with no activity (Default=5min)
                                     Only charged for DB storage
  * Resume                         : If new DB connections are requested, the DB
                                     cluster automatically resumes