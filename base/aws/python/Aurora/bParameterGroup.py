#!/usr/bin/env python3
"""
NAME:
 bParameterGroup.py

DESCRIPTION
 Beblsoft Aurora Database Cluster Parameter Group Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT DATABASE CLUSTER PARAMETER GROUP --------- #
class BeblsoftDatabaseClusterParameterGroup(BeblsoftAWSObject):
    """
    Beblsoft Database Cluster Parameter Group
    """

    def __init__(self,
                 name,
                 family,
                 description=None,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            DB Cluster Parameter Group Name
            Ex. "SmecknDBClusterParameterGroup"
          family:
            DB cluster parameter group family name
            Ex. "aurora5.6", "aurora-mysql5.7"
          description:
            Description for group
        """
        super().__init__()
        self.name        = name
        self.family      = family
        self.description = description
        self.bRegion     = bRegion
        self.rdsClient   = boto3.client("rds",
                                        region_name=self.bRegion.name)

    def __str__(self):
        return "[{} name={} family={}]".format(
            self.__class__.__name__, self.name, self.family)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def arn(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("DBClusterParameterGroupArn")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self):  # pylint: disable=W0221
        """
        Create object
        """
        kwargs                                     = {}
        kwargs["DBClusterParameterGroupName"]      = self.name
        kwargs["DBParameterGroupFamily"]           = self.family
        if self.description:
            kwargs["Description"]                  = self.description
        else:
            kwargs["Description"]                  = "{} Cluster Parameter Group".format(
                self.name)
        # kwargs["Tags"]                           = []
        self.rdsClient.create_db_cluster_parameter_group(**kwargs)

    @logFunc()
    def _delete(self):  # pylint: disable=W0221
        """
        Delete object
        """
        self.rdsClient.delete_db_cluster_parameter_group(
            DBClusterParameterGroupName = self.name)

    @logFunc()
    def _update(self, paramList):  # pylint: disable=W0221
        """
        Update object
        Args
          paramList:
            List of parameter dictionary each with following keys
              ParameterName:
                Specifies the name of the parameter.
              ParameterValue:
                Specifies the value of the parameter.
              Description:
                Provides a description of the parameter.
              Source:
                Indicates the source of the parameter value.
              ApplyType:
                Specifies the engine specific parameters type.
              DataType:
                Specifies the valid data type for the parameter.
              AllowedValues:
                Specifies the valid range of values for the parameter.
              IsModifiable:
                Indicates whether (true ) or not (false ) the parameter can be modified. Some parameters have security or operational implications that prevent them from being changed.
              MinimumEngineVersion:
                The earliest engine version to which the parameter can apply.
              ApplyMethod:
                Indicates when to apply parameter updates.
              SupportedEngineModes:
                The valid DB engine modes.
        Reference:
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.modify_db_cluster_parameter_group
        """
        self.rdsClient.modify_cluster_parameter_group(
            DBClusterParameterGroupName = self.name,
            Parameters = paramList)

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all objects
        """
        rdsClient        = boto3.client("rds", region_name=bRegion.name)
        marker           = None
        pgList           = []
        while True:
            resp         = rdsClient.describe_db_cluster_parameter_groups(
                Marker=marker)
            pgList      += resp.get("DBClusterParameterGroups", [])
            # Continue looping?
            marker       = resp.get("Marker", None)
            if not marker:
                break

        logger.info("Database Cluster Parameter Groups:\n{}")
        logger.info(pprint.pformat(pgList))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.describe_db_cluster_parameter_groups
        """
        meta = None
        try:
            resp = self.rdsClient.describe_db_cluster_parameter_groups(
                DBClusterParameterGroupName=self.name)
            pgList = resp.get("DBClusterParameterGroups", None)
            if pgList:
                meta = pgList[0]
        except ClientError as e:
            message = e.response["Error"]["Message"]
            if "not found" in message:
                pass
            else:
                raise(e)
        return meta
