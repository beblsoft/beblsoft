#!/usr/bin/env python3
"""
NAME:
 bCluster.py

DESCRIPTION
 Beblsoft Aurora Database Cluster Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import time
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT DATABASE CLUSTER ------------------------- #
class BeblsoftDatabaseCluster(BeblsoftAWSObject):
    """
    Beblsoft Database Cluster
    """

    def __init__(self,
                 clusterID,
                 dbName,
                 bSecurityGroup,
                 bDBClusterParameterGroup,
                 bDBSubnetGroup,
                 masterUsername,
                 masterUserPassword,
                 engine="aurora-mysql",
                 engineVersion="5.7.12",
                 port=3306,
                 engineMode="provisioned",
                 scalingConfiguration=None,
                 backupRetentionPeriod=1,
                 characterSetName=None):
        """
        Initialize object
        Args
          clusterID:
            DB Cluster Identifier
            Stored as a lowercase string
            Ex. "smecknaccountdb1"
          dbName:
            Name for database. Up to 64 alpha numeric characters
            If None, no DB created in cluster
          bSecurityGroup,
            Beblsoft Security Group
          bDBClusterParameterGroup:
            Beblsoft Database Cluster Parameter Group
          bDBSubnetGroup:
            Beblosft Database Subnet Group
          engine:
            Database engine for cluster
            One of: aurora (for MySQL 5.6-compatible Aurora)
                    aurora-mysql (for MySQL 5.7-compatible Aurora)
                    aurora-postgresql
          engineVersion:
            Ex. Aurora Mysql: 5.6.10a
          port:
            Port number on which DB instances accept connections
            Default = 3306
          masterUsername:
            Name of master user
          masterUserPassword:
            Password for master user
          backupRetentionPeriod:
            Number of days for which backups are retained
            Minumum,Default=1
          engineMode:
            One of "provisioned" or "serverless"
          scalingConfiguration:
            Only for serverless engineMode
            Dictionary With Keys:
              MinCapacity:
                Minium Capacity for an Aurora DB cluster
                Valid capacity values are 2, 4, 8, 16, 32, 64, 128, and 256
              MaxCapacity:
                Maximum Capacity for an Aurora DB cluster
                Valid capacity values are 2, 4, 8, 16, 32, 64, 128, and 256
              AutoPause:
                Allow DB cluster to be paused if idle
                Note: DB's can take on order of 20 seconds to start back up!
              SecondsUntilAutoPause:
                Time, in seconds, before an Aurora DB cluster is paused
          characterSetName:
            Specified character set for DB cluster
        """
        super().__init__()
        self.id                       = clusterID
        self.dbName                   = dbName
        if not dbName:
            self.dbName               = ""
        self.bSecurityGroup           = bSecurityGroup
        self.bDBClusterParameterGroup = bDBClusterParameterGroup
        self.bDBSubnetGroup           = bDBSubnetGroup
        self.masterUsername           = masterUsername
        self.masterUserPassword       = masterUserPassword
        self.engine                   = engine
        self.engineVersion            = engineVersion
        self.port                     = port
        self.engineMode               = engineMode
        self.scalingConfiguration     = scalingConfiguration
        self.backupRetentionPeriod    = backupRetentionPeriod
        self.characterSetName         = characterSetName
        self.bRegion                  = bDBSubnetGroup.bRegion
        self.rdsClient                = boto3.client("rds",
                                                     region_name=self.bRegion.name)

    def __str__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def arn(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("DBClusterArn")

    @property
    def capacity(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("Capacity")

    @property
    def endpoint(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("Endpoint")

    @property
    def readerEndpoint(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("ReaderEndpoint")

    @property
    def status(self):  # pylint: disable=C0111
        return self.getValueFromMetadata("Status")


    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, sleepS=10):  # pylint: disable=W0221
        """
        Create object
        Args
          sleepS:
            Time to sleep after DB becomes available
            Bug in AWS where lambda connection failed directly after DB became available
        """
        kwargs                                      = {}
        kwargs["BackupRetentionPeriod"]             = self.backupRetentionPeriod
        if self.characterSetName:
            kwargs["CharacterSetName"]              = self.characterSetName
        kwargs["DatabaseName"]                      = self.dbName
        kwargs["DBClusterIdentifier"]               = self.id
        kwargs["DBClusterParameterGroupName"]       = self.bDBClusterParameterGroup.name
        kwargs["DBSubnetGroupName"]                 = self.bDBSubnetGroup.name
        kwargs["Engine"]                            = self.engine
        kwargs["EngineVersion"]                     = self.engineVersion
        kwargs["Port"]                              = self.port
        kwargs["MasterUsername"]                    = self.masterUsername
        kwargs["MasterUserPassword"]                = self.masterUserPassword
        kwargs["EngineMode"]                        = self.engineMode
        if self.scalingConfiguration:
            kwargs["ScalingConfiguration"]          = self.scalingConfiguration
        kwargs["SourceRegion"]                      = self.bRegion.name
        kwargs["VpcSecurityGroupIds"]               = [self.bSecurityGroup.id]
        # kwargs["AvailabilityZones"]               = None
        # kwargs["OptionGroupName"]                 = None
        # kwargs["PreferredBackupWindow"]           = None
        # kwargs["PreferredMaintenanceWindow"]      = None
        # kwargs["ReplicationSourceIdentifier"]     = None
        # kwargs["Tags"]                            = None
        # kwargs["StorageEncrypted"]                = None
        # kwargs["KmsKeyId"]                        = None
        # kwargs["EnableIAMDatabaseAuthentication"] = None
        # kwargs["BacktrackWindow"]                 = None
        # kwargs["EnableCloudwatchLogsExports"]     = None
        self.rdsClient.create_db_cluster(**kwargs)
        self.waitChange(changeFunc=lambda: self.status == "available")
        time.sleep(sleepS)

    @logFunc()
    def _delete(self, skipFinalSnapshot=True, finalDBSnapshotID=None):  # pylint: disable=W0221
        """
        Delete object
        """
        kwargs                                  = {}
        kwargs["DBClusterIdentifier"]           = self.id
        kwargs["SkipFinalSnapshot"]             = skipFinalSnapshot
        if finalDBSnapshotID:
            kwargs["FinalDBSnapshotIdentifier"] = finalDBSnapshotID
        self.rdsClient.delete_db_cluster(**kwargs)
        self.waitChange(changeFunc=lambda: self.arn is None)

    @logFunc()
    def _update(self, applyImmediately=True):  # pylint: disable=W0221
        """
        Update object
        """
        kwargs                                        = {}
        kwargs["DBClusterIdentifier"]                 = self.id
        kwargs["ApplyImmediately"]                    = applyImmediately
        kwargs["BackupRetentionPeriod"]               = self.backupRetentionPeriod
        kwargs["DBClusterParameterGroupName"]         = self.bDBClusterParameterGroup.name
        kwargs["MasterUserPassword"]                  = self.masterUserPassword
        if self.scalingConfiguration:
            kwargs["ScalingConfiguration"]            = self.scalingConfiguration
        kwargs["VpcSecurityGroupIds"]                 = [self.bSecurityGroup.id]
        # kwargs["Port"]                                = self.port           # Can't with Aurora
        # kwargs["EngineVersion"]                       = self.engineVersion  # Can't with Aurora
        # kwargs["OptionGroupName"]                   = None
        # kwargs["NewDBClusterIdentifier"]            = None
        # kwargs["PreferredBackupWindow"]             = None
        # kwargs["PreferredMaintenanceWindow"]        = None
        # kwargs["EnableIAMDatabaseAuthentication"]   = False
        # kwargs["BacktrackWindow"]                   = None
        # kwargs["CloudwatchLogsExportConfiguration"] = []
        self.rdsClient.modify_db_cluster(**kwargs)

    @logFunc()
    def _restore(self, bDBClusterSnapshot):  # pylint: disable=W0221
        """
        Restore object from snapshot
        Args
          bDBClusterSnapshot:
            Beblsoft Database Cluster Snapshot
        """
        kwargs                                        = {}
        kwargs["DBClusterIdentifier"]                 = self.id
        kwargs["SnapshotIdentifier"]                  = bDBClusterSnapshot.id
        kwargs["Engine"]                              = self.engine
        kwargs["EngineVersion"]                       = self.engineVersion
        kwargs["Port"]                                = self.port
        kwargs["DBSubnetGroupName"]                   = self.bDBSubnetGroup.name
        kwargs["DatabaseName"]                        = self.dbName
        kwargs["EngineMode"]                          = self.engineMode
        if self.scalingConfiguration:
            kwargs["ScalingConfiguration"]            = self.scalingConfiguration
        kwargs["VpcSecurityGroupIds"]                 = [self.bSecurityGroup.id]
        # kwargs["AvailabilityZones"]                 = []
        # kwargs["OptionGroupName"]                   = None
        # kwargs["Tags"]                              = []
        # kwargs["KmsKeyId"]                          = None
        # kwargs["EnableIAMDatabaseAuthentication"]   = False
        # kwargs["BacktrackWindow"]                   = None
        # kwargs["EnableCloudwatchLogsExports"]       = []
        self.rdsClient.restore_db_cluster_from_snapshot(**kwargs)
        self.waitChange(changeFunc=lambda: self.status == "available")

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all objects
        """
        rdsClient        = boto3.client("rds", region_name=bRegion.name)
        marker           = None
        clusterList      = []
        while True:
            resp         = rdsClient.describe_db_clusters(Marker=marker)
            clusterList += resp.get("DBClusters", [])
            # Continue looping?
            marker       = resp.get("Marker", None)
            if not marker:
                break

        logger.info("Database Clusters:\n{}")
        logger.info(pprint.pformat(clusterList))

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.describe_db_clusters
        """
        try:
            meta = None
            resp = self.rdsClient.describe_db_clusters(
                DBClusterIdentifier=self.id)
            clusterList = resp.get("DBClusters", None)
            if clusterList:
                meta = clusterList[0]
        except ClientError as e:
            message = e.response["Error"]["Message"]
            if "not found" in message:
                pass
            else:
                raise(e)
        return meta
