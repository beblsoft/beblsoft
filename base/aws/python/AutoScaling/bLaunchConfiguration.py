#!/usr/bin/env python3
"""
NAME:
 bLaunchConfiguration.py

DESCRIPTION
 Beblsoft Auto Scaling Group Launch Configuration
"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
import time
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from jinja2 import Environment, FileSystemLoader
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger   = logging.getLogger(__file__)


# ------------------------ BEBLSOFT LAUNCH CONFIGURATION -------------------- #
class BeblsoftLaunchConfiguration():
    """
    Beblsoft Launch Configuration
    """

    def __init__(self, name, imageId, instanceType, bKeyPair, bIAMInstanceProfile,
                 bSecurityGroups, bContainerCluster=None, maxWaitSeconds=20,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            launch configuration name
            ex. "MojourneyLaunchConfiguration"
          imageId:
            id of the AMI us use to launch EC2 instances
            ex. "ami-9eb4b1e5", the 2017 ecs optimized AMI
          instanceType:
            type of ec2 instance
            ex: "t2.nano"
          bKeyPair:
            Beblsoft key pair object
          bIAMInstanceProfile:
            Beblsoft IAM Instance Profile object
          bSecurityGroups:
            List of beblsoft security group objects to associate with instances
          bContainerCluster:
            Beblsoft container cluster object

        """
        self.name                = name
        self.imageId             = imageId
        self.instanceType        = instanceType
        self.bKeyPair            = bKeyPair
        self.bIAMInstanceProfile = bIAMInstanceProfile
        self.bSecurityGroups     = bSecurityGroups
        self.bContainerCluster   = bContainerCluster
        self.fileDir             = os.path.dirname(os.path.realpath(__file__))
        self.jinjaEnv            = Environment(
            loader                 = FileSystemLoader(self.fileDir),
            trim_blocks            = True)
        self.userDataTemplate    = self.jinjaEnv.get_template(
            "launchConfigUserData.jinja")
        self.userData            = self.userDataTemplate.render(
            bContainerCluster       = self.bContainerCluster)
        self.maxWaitSeconds      = maxWaitSeconds
        self.bRegion             = bRegion
        self.asClient            = boto3.client(
            "autoscaling",
            region_name            = self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create launch configuration
        Note:
          Many times the create_launch_configuration request fails to find a
          valid IAM Instance Profile even when one exists.
          Therefore, we loop retrying the request until it succeeds.
        """
        created = False
        seconds = 0
        while not created:
            try:
                self.asClient.create_launch_configuration(
                    LaunchConfigurationName=self.name, ImageId=self.imageId, KeyName=self.bKeyPair.name,
                    SecurityGroups=[sg.id for sg in self.bSecurityGroups],
                    InstanceType=self.instanceType, UserData=self.userData,
                    IamInstanceProfile=self.bIAMInstanceProfile.name)
            except ClientError as e:
                if (seconds > self.maxWaitSeconds):
                    raise e
                else:
                    time.sleep(1)
                    seconds += 1
            else:
                created = True
        logger.info("{} created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete launch configuration
        """
        try:
            self.asClient.delete_launch_configuration(
                LaunchConfigurationName=self.name)
        except ClientError as e:
            if "Launch configuration name not found" in e.response["Error"]["Message"]:
                logger.info("{} didn't exist".format(self))
            else:
                raise e
        else:
            logger.info("{} deleted".format(self))

    @logFunc()
    def describe(self):
        """
        Describe launch configuration
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all launch configurations
        """
        asClient = boto3.client("autoscaling", region_name = bRegion.name)
        resp = asClient.describe_launch_configurations()
        lcs = resp.get("LaunchConfigurations", None)
        logger.info("Launch Configurations:\n{}".format(lcs))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        meta = None
        resp = self.asClient.describe_launch_configurations(
            LaunchConfigurationNames=[self.name])
        lcs = resp.get("LaunchConfigurations", None)
        if lcs:
            meta = lcs[0]
        return meta
