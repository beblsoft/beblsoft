#!/usr/bin/env python3
"""
NAME:
 bAutoScalingGroup.py

DESCRIPTION
 Beblsoft Auto Scaling Group
"""


# ------------------------ IMPORTS ------------------------------------------ #
import time
import pprint
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT AUTO SCALING GROUP ---------------------- #
class BeblsoftAutoScalingGroup():
    """
    Beblsoft Auto Scaling Group
    """

    def __init__(self, name, bLaunchConfiguration, bSubnets, minSize=1, maxSize=2,
                 desiredCapacity=1, defaultCooldown=300, sleepAfterCreate=10):
        """
        Initialize object
        Args
          name:
            auto scaling group name
            ex. "MojourneyAutoScalingGroup"
          bLaunchConfiguration:
            Beblsoft launch configuration object
          bSubnets:
            list of Beblsoft Subnet objects
          minSize:
            the minimum size of the group
          maxSize:
            the maximum size of the group
          desiredCapacity:
            the number of EC2 instances that should be running in the group
          defaultCooldown:
            the amount of time, in seconds, after a scaling activity completes
            before another scaling activity can start
          sleepAfterCreate:
            the amount of time, in seconds, to sleep after the the autoscaling
            group is created. This allows time for the auto scaling group
            to launch its instances
        """
        self.name                 = name
        self.bLaunchConfiguration = bLaunchConfiguration
        self.bSubnets             = bSubnets
        self.minSize              = minSize
        self.maxSize              = maxSize
        self.desiredCapacity      = desiredCapacity
        self.defaultCooldown      = defaultCooldown
        self.sleepAfterCreate     = sleepAfterCreate
        self.bRegion              = self.bLaunchConfiguration.bRegion
        self.asClient             = boto3.client("autoscaling",
                                                 region_name=self.bRegion.name)
        self.ec2Client            = boto3.client("ec2",
                                                 region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @logFunc()
    def create(self):
        """
        Create auto scaling group
        Notes
          - Create auto scaling group and then wait for all instances to startup
          - Added a time.sleep after the auto scaling group is created so that
            the call to self.__getInstances will succeed
        """
        self.asClient.create_auto_scaling_group(
            AutoScalingGroupName=self.name, LaunchConfigurationName=self.bLaunchConfiguration.name,
            MinSize=self.minSize, MaxSize=self.maxSize, DesiredCapacity=self.desiredCapacity,
            DefaultCooldown=self.defaultCooldown,
            VPCZoneIdentifier="{}".format(",".join([subnet.id for subnet in self.bSubnets])))
        logger.info("{} created".format(self))
        time.sleep(self.sleepAfterCreate)

        insts = self.__getInstances()
        if insts:
            waiter = self.ec2Client.get_waiter("instance_running")
            waiter.wait(InstanceIds=[inst.get("InstanceId") for inst in insts])
        logger.info("{} instances running".format(self))

    @logFunc()
    def delete(self, force=True):
        """
        Delete auto scaling group
        Args
          force: group will be deleted along with all instances associated with
                 the group, without waiting for all instances to be terminated
        Note
          Delete auto scaling group, and then wait for all of its associated
          instances to be terminated
        """
        insts = self.__getInstances()
        self.asClient.delete_auto_scaling_group(AutoScalingGroupName=self.name,
                                                ForceDelete=force)
        logger.info("{} deleted".format(self))
        if insts:
            waiter = self.ec2Client.get_waiter("instance_terminated")
            waiter.wait(InstanceIds=[inst.get("InstanceId") for inst in insts])
        logger.info("{} instances terminated".format(self))

    @logFunc()
    def describe(self):
        """
        Describe Auto Scaling Group
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def describeInstances(self, partial=True):
        """
        Describe Auto Scaling Group Instances
        Args
          partial: If True report subset of instance attributes
        """
        describeInstanceKeys = ["ImageId", "InstanceId", "InstanceType",
                                "KeyName", "Placement", "PublicDnsName", "PublicIpAddress",
                                "State", "SubnetId"]
        pInsts = []
        insts  = self.__getInstances()
        if partial:
            for inst in insts:
                pInst = {k: inst[k] for k in describeInstanceKeys}
                pInsts.append(pInst)
        else:
            pInsts = insts
        logger.info("{} instances:\n{}".format(self, pprint.pformat(pInsts)))

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all Auto Scaling Groups
        """
        asClient = boto3.client("autoscaling",region_name = bRegion.name)
        resp = asClient.describe_auto_scaling_groups()
        asgs = resp.get("AutoScalingGroups, None")
        logger.info("Auto Scaling Groups:\n{}".format(pprint.pformat(asgs)))

    @logFunc()
    def __getMetadata(self):
        """
        Return metadata
        """
        meta = None
        resp = self.asClient.describe_auto_scaling_groups(
            AutoScalingGroupNames=[self.name])
        asgs = resp.get("AutoScalingGroups", None)
        if asgs:
            meta = asgs[0]
        return meta

    @logFunc()
    def __getInstances(self):
        """
        Return instance metadata
        Note:
          Retrieve EC2 instance metadata as it is more verbose than auto scaling
          instance metada.
        """
        reservs = None
        insts   = None
        meta    = self.__getMetadata()
        if meta:
            insts = meta.get("Instances", None)
        if insts:
            resp = self.ec2Client.describe_instances(
                InstanceIds=[inst.get("InstanceId") for inst in insts])
            reservs = resp.get("Reservations", None)
        if reservs:
            insts = reservs[0].get("Instances", None)
        return insts
