# AWS WorkMail Documentation

Amazon WorkMail is a managed email and calendaring service with strong security controls and support
for existing desktop and mobile email client applications. Amazon WorkMail gives users the ability
to seamlessly access their email, contacts, and calendars using the client application of their
choice.

Relevant URLs:
[Home](https://aws.amazon.com/workmail/),
[Administrator Guide](https://docs.aws.amazon.com/workmail/latest/adminguide/what_is.html),
[User Guide](https://docs.aws.amazon.com/workmail/latest/userguide/what_is.html)

## Features

- Managed Service: makes it easy to manage your corporate email infrastructure and eliminates the need
  for up-front investments to license and provision on-premise email servers

- Free migration: No Cost to migrate from other services. AWS uses audriga and Transend to make it
  easier for you to move your email to Amazon WorkMail.

- Microsoft Outlook Compatible

- Enterprise-Grade Security

- Active Directory Integration

- Journaling: allows you to record all email communication sent or received by your Amazon Workmail
  organization.

- Interoperability with Microsoft Exchange Server

- Administrative SDK

- IMAP Protocol Support

- Feature-Rich Web Client: Users can access their email and calendars, view shared calendars, quickly
  schedule meetings with co-workers, or search the company address book using their browser.

- Mobile Device Management

- Email Flow Rules: allows you to use email flow ruls to filter inbound email traffic for your Amazon
  WorkMail organizations

- Spam and Virus Protection

- Large Mailboxes: 50 GB

- AWS CloudTrail Integration

## Concepts

- __Organization__: A tenant setup for Amazon WorkMail.

- __Alias__: A globally unique name to identify your organization. The alias is used to access the
  Amazon WorkMail web application (https://alias.awsapps.com/mail).

- __Domain__: The web address that comes after the @ symbol in an email address. You can add a domain
  that receives mail and delivers it to mailboxes in your organization.

- __Test mail domain__: A domain is automatically configured during setup that can be used for testing
  Amazon WorkMail. The test mail domain is alias.awsapps.com and is used as the default domain if
  you do not configure your own domain. The test mail domain is subject to different limits.
  For more information, see Amazon WorkMail Limits.

- __Directory__: An AWS Simple AD, AWS Managed AD, or AD Connector created in AWS Directory Service.
  If you create an organization using the Amazon WorkMail Quick setup, we create a WorkMail directory
  for you. You cannot view a WorkMail directory in AWS Directory Service.

- __User__: A user created in the AWS Directory Service. When a user is enabled for Amazon WorkMail,
  they receive their own mailbox to access. When a user is disabled, they cannot access Amazon WorkMail.

- __Group__: A group used in AWS Directory Service. A group can be used as a distribution list or a
  security group in Amazon WorkMail. Groups do not have their own mailboxes.

- __Resource__: A resource represents a meeting room or equipment resource that can be booked by
  Amazon WorkMail users.

- __Mobile device policy__ Various IT policy rules that control the security features and behavior of
  a mobile device.

