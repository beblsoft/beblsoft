#!/usr/bin/env python3
"""
NAME:
 bDatabaseInstance.py

DESCRIPTION
 Beblsoft Database Instance Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT DATABASE INSTANCE ------------------------ #
class BeblsoftDatabaseInstance(BeblsoftAWSObject):
    """
    Beblsoft Database Instance

    Description
      Wrapper over AWS Relational Database Service (RDS)
      Several instance types optimized for memory, performance, or I/O

    Engines
      Amazon Aurora
      PostgreSQL
      MySQL
      MariaDB
      Oracle
      Microsoft SQL Server

    MySQL
      mysql -u <user>   -h <host>                                                       -p
      mysql -u testUser -h mojourneydbinstance.c6qdegjyuiln.us-east-1.rds.amazonaws.com -p
    """

    def __init__(self, name, instanceId, instanceClass, engine, engineVersion,
                 port, masterUsername, masterUserPassword, bSecurityGroup,
                 bDatabaseSubnetGroup, allocatedStorageGB, storageType, backupRetentionPeriod,
                 multiAZ, publiclyAccessible, skipFinalSnapshot):
        """
        Initialize object
        Args
          name:
            name of database when DB instance created
            ex. mojourney
          instanceId:
            database instance identifier
            ex. mojourneyDBInstance
          instanceClass:
            Compute and memory capacity of DB instance
            ex. db.t1.micro, db.m1.small, db.m1.medium, ...
          engine
            name of the engine used for instance
            ex. aurora, mariadb, mysql, oracle-ee, postgres, sqlserver-ee
          engineVersion
            version number of the database engine to uuse
            ex mysql. "5.6.35"
          port
            port number on which the database accepts connections
            mysql default: 3306
          masterUsername
            name of master user
            ex. testUser
          masterUserPassword
            password for master user
            ex. "12345678"
          bSecurityGroup
            Beblsoft security group object
          bDatabaseSubnetGroup
            Beblsoft database subnet group object
          allocatedStorageGB:
            amount of storage (in GB) initially allocated to database instance
            for standard storage. 5 - 3072
          storageType
            storage type to be associated with DB instance
            ex. standard, gp2, io1
          backupRetentionPeriod
            Number of days for which automated backups are retained
            ex. 0
          multiAZ
            Specifies if the DB instance is a Multi-AZ deployment
            ex. False
          publiclyAccessible
            Specifies the accessibility to the DB instance
            ex. True
          skipFinalSnapshot
            Determines whether the final DB cluster snapshot is created before the DB is deleted
            ex. True

        MySQL Notes
          Had issue creating mysql version at 5.7.17
          In order for public internet to access instance, need to modify db instance
          security group
          AWS > RDS Console > Select Instance > Security Group default (sg-cd08d6b4) >
          Actions > Edit inbound rules > Source: Custom 0.0.0.0/0

        """
        super().__init__()

        self.name                  = name
        self.instanceId            = instanceId
        self.instanceClass         = instanceClass
        self.engine                = engine
        self.engineVersion         = engineVersion
        self.port                  = port
        self.masterUsername        = masterUsername
        self.masterUserPassword    = masterUserPassword
        self.bSecurityGroup        = bSecurityGroup
        self.bDatabaseSubnetGroup  = bDatabaseSubnetGroup
        self.allocatedStorageGB    = allocatedStorageGB
        self.storageType           = storageType
        self.backupRetentionPeriod = backupRetentionPeriod
        self.multiAZ               = multiAZ
        self.publiclyAccessible    = publiclyAccessible
        self.skipFinalSnapshot     = skipFinalSnapshot
        self.bRegion               = bDatabaseSubnetGroup.bRegion
        self.rdsClient             = boto3.client("rds",
                                                  region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={} class={} engine={}.{}]".format(
            self.__class__.__name__, self.name, self.instanceClass,
            self.engine, self.engineVersion,)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def domainName(self): #pylint: disable=C0111
        return self.getValueFromMetadata("Endpoint.Address")

    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata("DBInstanceArn")


    @property
    def status(self): #pylint: disable=C0111
        return self.getValueFromMetadata("DBInstanceStatus")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create Database Instance
        """
        self.rdsClient.create_db_instance(
            DBName                = self.name,
            DBInstanceIdentifier  = self.instanceId,
            DBInstanceClass       = self.instanceClass,
            Engine                = self.engine,
            EngineVersion         = self.engineVersion,
            Port                  = self.port,
            MasterUsername        = self.masterUsername,
            MasterUserPassword    = self.masterUserPassword,
            VpcSecurityGroupIds   = [self.bSecurityGroup.id],
            DBSubnetGroupName     = self.bDatabaseSubnetGroup.name,
            AllocatedStorage      = self.allocatedStorageGB,
            StorageType           = self.storageType,
            BackupRetentionPeriod = self.backupRetentionPeriod,
            MultiAZ               = self.multiAZ,
            PubliclyAccessible    = self.publiclyAccessible)
        waiter = self.rdsClient.get_waiter("db_instance_available")
        waiter.wait(DBInstanceIdentifier = self.instanceId)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete Database Instance
        """
        if self.exists:
            self.rdsClient.delete_db_instance(
                DBInstanceIdentifier=self.instanceId,
                SkipFinalSnapshot=self.skipFinalSnapshot)
            waiter = self.rdsClient.get_waiter("db_instance_deleted")
            waiter.wait(DBInstanceIdentifier = self.instanceId)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never Existed".format(self))

    @logFunc()
    def _modify(self, cls=False, mup=False, scg=False, #pylint: disable=W0221
               sbg=False, als=False, st=False, brp=False,
               maz=False, pa=False):
        """
        Modify Database Instance
        """
        kwargs = {}
        kwargs["DBInstanceIdentifier"]      = self.instanceId
        if cls:
            kwargs["DBInstanceClass"]       = self.instanceClass
        if mup:
            kwargs["MasterUserPassword"]    = self.masterUserPassword
        if scg:
            kwargs["VpcSecurityGroupIds"]   = [self.bSecurityGroup.id]
        if sbg:
            kwargs["DBSubnetGroupName"]     = self.bDatabaseSubnetGroup.name
        if als:
            kwargs["AllocatedStorage"]      = self.allocatedStorageGB
        if st:
            kwargs["StorageType"]           = self.storageType
        if brp:
            kwargs["BackupRetentionPeriod"] = self.backupRetentionPeriod
        if maz:
            kwargs["MultiAZ"]               = self.multiAZ
        if pa:
            kwargs["PubliclyAccessible"]    = self.publiclyAccessible
        self.rdsClient.modify_db_instance(**kwargs)

    @logFunc()
    def _start(self, **kwargs):
        """
        Start Database Instance
        """
        self.rdsClient.start_db_instance(DBInstanceIdentifier=self.instanceId)
        waiter = self.rdsClient.get_waiter("db_instance_available")
        waiter.wait(dbInstanceIdentifier = self.instanceId)

    @logFunc()
    def _stop(self, **kwargs):
        """
        Stop Database Instance
        """
        self.rdsClient.stop_db_instance(DBInstanceIdentifier=self.instanceId)

    @logFunc()
    def _reboot(self, **kwargs):
        """
        Reboot Database Instance
        """
        self.rdsClient.reboot_db_instance(DBInstanceIdentifier=self.instanceId)
        waiter = self.rdsClient.get_waiter("db_instance_available")
        waiter.wait(dbInstanceIdentifier = self.instanceId)

    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all Database Instances
        """
        rdsClient = boto3.client("rds", region_name=bRegion.name)
        resp = rdsClient.describe_db_instances()
        dbInsts = resp.get("DBInstances", None)
        logger.info("DBInstances:\n{}".format(pprint.pformat(dbInsts)))

    @logFunc()
    def _getMetadata(self):
        """
        Get Object Metadata
        Returns
          None or metadata here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.describe_db_instances        """
        meta = None
        try:
            resp = self.rdsClient.describe_db_instances(
                DBInstanceIdentifier=self.instanceId)
            dbInsts = resp.get("DBInstances", None)
            if dbInsts:
                meta = dbInsts[0]
        except ClientError as e:
            if e.response["Error"]["Code"] != "DBInstanceNotFound":
                raise e
        return meta
