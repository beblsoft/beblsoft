#!/usr/bin/env python3
"""
NAME:
 bSubnetGroup.py

DESCRIPTION
 Beblsoft Database Subnet Group Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT DATABASE SUBNET GROUP -------------------- #
class BeblsoftDatabaseSubnetGroup(BeblsoftAWSObject):
    """
    Beblsoft Database Subnet Group

    A database subnet group is a combination of one or more VPC subnets.
    """

    def __init__(self, name, bSubnets, description=None):
        """
        Initialize object
        Args
          name:
            name of DB subnet group
          bSubnets
            List of beblsoft subnet objects
          description:
            description of DB subnet group

        """
        super().__init__()

        self.name            = name.lower()
        self.description     = self.name
        if description:
            self.description = description
        self.bSubnets        = bSubnets
        self.bRegion         = bSubnets[0].bRegion
        self.rdsClient       = boto3.client("rds", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}".format(self.__class__.__name__, self.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata("DBSubnetGroupArn")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create database subnet group
        """
        self.rdsClient.create_db_subnet_group(
            DBSubnetGroupName=self.name,
            DBSubnetGroupDescription=self.description,
            SubnetIds=[bSubnet.id for bSubnet in self.bSubnets])

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete database subnet group
        """
        self.rdsClient.delete_db_subnet_group(DBSubnetGroupName=self.name)


    @staticmethod
    def describeAll(bRegion=BeblsoftRegion.getDefault()):
        """
        Describe all database subnet groups
        """
        rdsClient = boto3.client("rds", region_name=bRegion.name)
        resp = rdsClient.describe_db_subnet_groups()
        dbSubGrps = resp.get("DBSubnetGroups", None)
        logger.info("DBInstances:\n{}".format(pprint.pformat(dbSubGrps)))

    @logFunc()
    def _getMetadata(self):
        """
        Get Object Metadata
        Returns
          None or metadata here
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.describe_db_subnet_groups        """
        meta      = None
        dbSubGrps = None
        try:
            resp = self.rdsClient.describe_db_subnet_groups(DBSubnetGroupName=self.name)
            dbSubGrps = resp.get("DBSubnetGroups", None)
            if dbSubGrps:
                meta = dbSubGrps[0]
        except ClientError as e:
            if e.response["Error"]["Code"] != "DBSubnetGroupNotFoundFault":
                raise e
        return meta
