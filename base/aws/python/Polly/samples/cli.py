#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Polly Samples
"""


# ----------------------------- IMPORTS ------------------------------------- #
import os
import sys
import traceback
import logging
import subprocess
import pprint
import json
from contextlib import closing
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.aws.python.Polly.bClient import BeblsoftPollyClient


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Polly -----------------------------------
        self.bPolly            = BeblsoftPollyClient()

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/pollySample.log", type=click.Path(),
              help="Specify log file Default=/tmp/pollySample.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Polly Samples
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- SYNTHESIZE SPEECH --------------------------------- #
@cli.command()
def synthesize_speech():
    """
    Synthesize Speech

    Bibliography: https://docs.aws.amazon.com/polly/latest/dg/get-started-what-next.html
    """
    text           = "Hello World!"
    outputDir      = os.path.dirname(os.path.realpath(__file__))
    outputFileName = "helloWorld.mp3"
    outputFile     = os.path.join(outputDir, outputFileName)
    resp           = gc.bPolly.synthesizeSpeech(text=text, textType="text",
                                                outputFormat="mp3", sampleRate="22050",
                                                voiceID="Joey")

    # Note: Closing the stream is important as the service throttles on the
    # number of parallel connections. Here we are using contextlib.closing to
    # ensure the close method of the stream object will be called automatically
    # at the end of the with statement's scope.
    with closing(resp["AudioStream"]) as stream:
        with open(outputFile, "wb") as of:
            of.write(stream.read())  # pylint: disable=E1101
    subprocess.call(["xdg-open", outputFile])


# ----------------------- SYNTHESIZE SPEECH SSML ---------------------------- #
@cli.command()
def synthesize_speech_ssml():
    """
    Synthesize Speech with SSML
    """
    text           = ("""<speak>"""
                      """  Mary had a little lamb <break time="1s"/>Whose fleece was white as snow."""
                      """  I already told you I <emphasis level="strong">really like</emphasis> that person."""
                      """  <lang xml:lang="fr-FR">Je ne parle pas français.</lang> """
                      """  Mi piace <lang xml:lang="en-US">Bruce Springsteen.</lang> """
                      """  <p>This is the second paragraph.</p> """
                      """  I say, <phoneme alphabet="ipa" ph="ˈpi.kæn">pecan</phoneme> """
                      """  Sometimes it can sometimes be useful to <prosody volume="loud">increase the volume for a specific speech.</prosody>  """
                      """  For dramatic purposes, you might wish to <prosody rate="slow">speed up the speaking rate of your text.</prosody>"""
                      """  Do you like sythesized speech <prosody pitch="high">with a pitch that is higher than normal?</prosody>  """
                      """  Richard's number is <say-as interpret-as="telephone">2122241555</say-as>"""
                      """  The word <say-as interpret-as="characters">read</say-as> may be interpreted as either the present simple form <w role="amazon:VB">read</w>, or the past participle form <w role="amazon:VBD">read</w>"""
                      """  Sometimes you want to insert only <amazon:breath duration="medium" volume="x-loud"/>a single breath."""
                      """  Some audio is difficult to hear in a moving vehicle, but <amazon:effect name="drc"> this audio is less difficult to hear in a moving vehicle.</amazon:effect>"""
                      """  This is Matthew speaking in my normal voice. <amazon:effect phonation="soft">This is Matthew speaking in my softer voice.</amazon:effect>"""
                      """  <amazon:effect name="whispered">If you make any noise, </amazon:effect> she said, <amazon:effect name="whispered">they will hear us.</amazon:effect>"""
                      """</speak>""")
    outputDir      = os.path.dirname(os.path.realpath(__file__))
    outputFileName = "ssml.mp3"
    outputFile     = os.path.join(outputDir, outputFileName)
    resp           = gc.bPolly.synthesizeSpeech(text=text, textType="ssml",
                                                outputFormat="mp3", sampleRate="22050",
                                                voiceID="Joey")
    with closing(resp["AudioStream"]) as stream:
        with open(outputFile, "wb") as of:
            of.write(stream.read())  # pylint: disable=E1101
    subprocess.call(["xdg-open", outputFile])


# ----------------------- GET SPEECH MARKS ---------------------------------- #
@cli.command()
def get_speech_marks():
    """
    Get Speeh Marks
    """
    text  = "Mary had a little lamb."
    marks = ["sentence", "viseme", "word"]
    resp  = gc.bPolly.synthesizeSpeech(text=text, textType="text",
                                       outputFormat="json", speechMarkTypes=marks)
    with closing(resp["AudioStream"]) as stream:
        output     = stream.read()  # pylint: disable=E1101
        outputStr  = output.decode("utf-8")
        for line in outputStr.splitlines():
            outputJSON = json.loads(line)
            logger.info(pprint.pformat(outputJSON))
    # {'end': 23, 'start': 0, 'time': 0, 'type': 'sentence', 'value': 'Mary had a little lamb.'}
    # {'end': 4, 'start': 0, 'time': 6, 'type': 'word', 'value': 'Mary'}
    # {'time': 6, 'type': 'viseme', 'value': 'p'}
    # {'time': 94, 'type': 'viseme', 'value': 'E'}
    # {'time': 145, 'type': 'viseme', 'value': 'r'}
    # {'time': 260, 'type': 'viseme', 'value': 'i'}
    # {'end': 8, 'start': 5, 'time': 351, 'type': 'word', 'value': 'had'}
    # {'time': 351, 'type': 'viseme', 'value': 'k'}
    # {'time': 412, 'type': 'viseme', 'value': 'a'}
    # {'time': 471, 'type': 'viseme', 'value': 't'}
    # {'end': 10, 'start': 9, 'time': 502, 'type': 'word', 'value': 'a'}
    # {'time': 502, 'type': 'viseme', 'value': '@'}
    # {'end': 17, 'start': 11, 'time': 536, 'type': 'word', 'value': 'little'}
    # {'time': 536, 'type': 'viseme', 'value': 't'}
    # {'time': 666, 'type': 'viseme', 'value': 'i'}
    # {'time': 709, 'type': 'viseme', 'value': 't'}
    # {'time': 747, 'type': 'viseme', 'value': 't'}
    # {'end': 22, 'start': 18, 'time': 784, 'type': 'word', 'value': 'lamb'}
    # {'time': 784, 'type': 'viseme', 'value': 't'}
    # {'time': 907, 'type': 'viseme', 'value': 'a'}
    # {'time': 1046, 'type': 'viseme', 'value': 'p'}
    # {'time': 1145, 'type': 'viseme', 'value': 'sil'}


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
