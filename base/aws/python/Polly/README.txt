OVERVIEW
===============================================================================
AWS Polly Technology Documentation
- Description                       : Amazon Polly is a cloud service that converts text into lifelike speech
                                    : Multiple languages supported
                                    : Multiple voices supported
- Use Cases ------------------------: Newsreaders
                                    : Games
                                    : eLearning Platforms
                                    : Accessibility Applications (E.g. for visually impaired)
                                    : Iternet of Things
- Certifications --------------------
  * HIPAA                           : Health Insurance Portability and Accountability Act of 1996
  * Not for use with PCI DSS        : Payment Card Industry Data Security Standard
- Benefits --------------------------
  * High Quality                    : Text-to-Speech (TTS) technology to synthesize natural speech with high pronunciation accuracy
  * Low Latency                     : Fast response times
  * Large portfolio of languages
    and voices                      : Dozens of voices and multiple languages
  * Cost-effective                  : Pay-per-use model
  * Cloud-based solution
- URLS ------------------------------
 * Home                             : https://aws.amazon.com/polly/
 * Documentation                    : https://docs.aws.amazon.com/polly/latest/dg/what-is.html
 * Console                          : https://console.aws.amazon.com/polly/home/SynthesizeSpeech
 * Supported Languages              : https://docs.aws.amazon.com/polly/latest/dg/SupportedLanguage.html
 * Viseme Tables for Languages      : https://docs.aws.amazon.com/polly/latest/dg/ref-phoneme-tables-shell.html
 * SSML Spec                        : https://www.w3.org/TR/2010/REC-speech-synthesis11-20100907/
 * AWS SSML Tags                    : https://docs.aws.amazon.com/polly/latest/dg/supported-ssml.html


VOICES
===============================================================================
- Available Voices -------------------
  * Deprecation                      : AWS doesn't plan on retiring any voices
  * Chart                            : Language                       |  Male Names/ID       | Female Names/ID
                                       -------------------------------|----------------------|-----------------
                                       Chinese, Mandarin (cmn-CN)     |                      | Zhiyu
                                       Danish (da-DK)                 |  Mads                | Naja
                                       Dutch (nl-NL)                  |  Ruben               | Lotte
                                       English (Australian) (en-AU)   |  Russell             | Nicole
                                       English (British) (en-GB)      |  Brian               | Amy, Emma
                                       English (Indian) (en-IN)       |                      | Aditi (bilingual with Hindi), Raveena
                                       English (US) (en-US)           |  Joey,Justin,Matthew | Ivy,Joanna,Kendra,Kimberly,Salli
                                       English (Welsh) (en-GB-WLS)    |  Geraint             |
                                       French (fr-FR)                 |  Mathieu             | Céline/Celine
                                       French (Canadian) (fr-CA)      |  Chantal             |
                                       German (de-DE)                 |  Hans                | Marlene,Vicki
                                       Hindi (hi-IN)                  |                      | Aditi (bilingual with Indian English)
                                       Icelandic (is-IS)              |  Karl                | Dóra/Dora
                                       Italian (it-IT)                |  Giorgio             | Carla,Bianca
                                       Japanese (ja-JP)               |  Takumi              | Mizuki
                                       Korean (ko-KR)                 |                      | Seoyeon
                                       Norwegian (nb-NO)              |                      | Liv
                                       Polish (pl-PL)                 |  Jacek,Jan           | Ewa,Maja
                                       Portuguese (Brazilian) (pt-BR) |  Ricardo             | Vitória/Vitoria
                                       Portuguese (European) (pt-PT)  |  Cristiano           | Inês/Ines
                                       Romanian (ro-RO)               |                      | Carmen
                                       Russian (ru-RU)                |  Maxim               | Tatyana
                                       Spanish (European) (es-ES)     |  Enrique             | Conchita,Lucia
                                       Spanish (Mexican) (es-MX)      |                      | Mia
                                       Spanish (US) (es-US)           |  Miguel              | Penélope/Penelope
                                       Swedish (sv-SE)                |                      | Astrid
                                       Turkish (tr-TR)                |                      | Filiz
                                       Welsh (cy-GB)                  |                      | Gwyneth
- Voice Speed -----------------------
  * Speech Marks                    : Use to specify how long text takes to be spoken
                                      Some voices are considerably faster than others
  * Prosody tags                    : Used to speed/slow voice
                                      One of: x-slow, slow, medium, fast, x-fast
                                      Ex1. <prosody rate="slow">Mary had a little lamb.</prosody>
                                      Ex2. <prosody rate="85%">
- Bilingual Voices ------------------
  * Description                     : A bilingual voice (ex. Aditi speaks English and Hindi)
                                      can speak two languages fluently
  * Mixing Languages                : Possible to mix languages within a single sentence with bilingual voice
                                      Ex. Devanagari + English: "This is the song कभी कभी अदिति"
- Languages Supported ---------------
  * Chart                           : Language              | Language Code
                                      ----------------------|----------------
                                      Chinese, Mandarin     | cmn-CN
                                      Danish                | da-DK
                                      Dutch                 | nl-NL
                                      English, Australian   | en-AU
                                      English, British      | en-GB
                                      English, Indian       | en-IN
                                      English, US           | en-US
                                      English, Welsh        | en-GB-WLS
                                      French                | fr-FR
                                      French, Canadian      | fr-CA
                                      Hindi                 | hi-IN
                                      German                | de-DE
                                      Icelandic             | is-IS
                                      Italian               | it-IT
                                      Japanese              | ja-JP
                                      Korean                | ko-KR
                                      Norwegian             | nb-NO
                                      Polish                | pl-PL
                                      Portuguese, Brazilian | pt-BR
                                      Portuguese, European  | pt-PT
                                      Romanian              | ro-RO
                                      Russian               | ru-RU
                                      Spanish, European     | es-ES
                                      Spanish, Mexican      | es-MX
                                      Spanish, US           | es-US
                                      Swedish               | sv-SE
                                      Turkish               | tr-TR
                                      Welsh                 | cy-GB



SPEECH MARKS
===============================================================================
- Description                       : Speech marks are metadata that describe the speech you want to synthesize
                                      When you request speech marks for text, polly returns this metadata
                                      instead of synthesized speech
- Types -----------------------------
  * Sentence
  * Word
  * Viseme                          : Describes face and mount movements to each phoneme being spoken
  * ssml                            : Describes a <mark> element in ssml input text
- Visemes ---------------------------
  * Viseme                          : A viseme represents the position of the face and mouth when saying a word
                                      They are the basic building blocks of speech
  * Phoneme                         : The distinct units of sound in specified language
                                      A single viseme can map to multiple phonemes
  * Example                         : In English, "pet" and "bet" sound different but observed visually without
                                      sound the same
  * Codes                           : Each viseme can be identified by a particular code



SPEECH SYNTHESIS MARKUP LANGUAGE (SSML)
===============================================================================
- Description                       : Using SSML-enhanced input gives programmer additional control
                                      over how Amazon Polly generates speech from text provided
- Supported Tags
  * Adding a Pause                  : <break>
  * Emphasizing Words               : <emphasis>
  * Specifying Another Language for
    Specific Words                  : <lang>
  * Placing a Custom Tag in Your Txt: <mark>
  * Adding a Pause Between Paras    : <p>
  * Using Phonetic Pronunciation    : <phoneme>
  * Controlling Volume, Speaking
    Rate, and Pitch                 : <prosody>
  * Setting a Maximum Duration for
    Synthesized Speech              : <prosody amazon:max-duration>
  * Adding a Pause Between Sentences: <s>
  * Controlling How Special Types
    of Words Are Spoken             : <say-as>
  * Identifying SSML-Enhanced Text  : <speak>
  * Pronouncing Acronyms and
    Abbreviations                   : <sub>
  * Improving Pronunciation by
    Specifying Parts of Speech      : <w>
  * Adding the Sound of Breathing   : <amazon:auto-breaths>
  * Adding Dynamic Range Compression: <amazon:effect name="drc">
  * Speaking Softly                 : <amazon:effect phonation="soft">
  * Controlling Timbre              : <amazon:effect vocal-tract-length>
  * Whispering                      : <amazon: effect name="whispered">



MANAGING LEXICONS
===============================================================================
- Description                       : Pronunciation lexicons enable developers to customize the pronunciation of words
- Examples                          : "g3t sm4rt" -> pronounce as "get smart"
                                      "W3C" -> "World Wide Web Consortium"

