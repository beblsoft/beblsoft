OVERVIEW
===============================================================================
Polly Samples Documentation



PROJECT: SAMPLES
===============================================================================
- Description                       : Polly Samples
- Directory                         : ./samples
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Commands --------------------------
  * Synthesize Speech               : ./cli.py synthesize-speech
  * Synthesize Speech with SSML     : ./cli.py synthesize-speech-ssml
  * Get Speech Marks                : ./cli.py get-speech-marks
