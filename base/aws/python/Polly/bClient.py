#!/usr/bin/env python3.6
"""
NAME:
 bClient.py

DESCRIPTION
 Beblsoft Polly Client
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT POLLY CLIENT ----------------------------- #
class BeblsoftPollyClient():
    """
    Beblsoft Polly Client
    """

    def __init__(self, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        """
        self.pollyClient = boto3.client("polly", region_name=bRegion.name)

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ----------------------- SYNTHESIZE SPEECH ----------------------------- #
    @logFunc()
    def synthesizeSpeech(self, text, textType,
                         outputFormat="mp3", sampleRate="22050",
                         voiceID="Ivy", languageCode=None,
                         speechMarkTypes=None):
        """
        Synthesize Speech
        Args
          text:
            Input text to synthesize
            Text Ex. "Hello World!"
            SSML Ex. "<speak>Hello World!</speak>"
          textType:
            Type of text
            One of: "ssml" | "text"
          outputFormat:
            Format in whcih the returned output will be encoded
            One of "json" | "mp3" | "ogg_vorbis" | "pcm"
          sampleRate:
            Audio Frequency in Hz
            MP3 and ogg_vorbis values: "8000" | "16000" | "22050"
            PCM values: "8000"| "16000"
          voiceID:
            Voice to use for synthesis
            Ex. "Ivy"
          languageCode:
            Optional language code for request
            Only necessary if using a bilingual voice (ex. Aditi)
            Ex. "en-US"
          speechMarkTypes:
            List of speech marks returned for input text
            List [ "sentence" | "ssml" | "viseme" | "word" ]

        Returns
          {
            'AudioStream': StreamingBody(),
            'ContentType': 'string',
            'RequestCharacters': 123
          }

        If speechMarkTypes specified:
        Returns JSON encoded in AudioStream:
          [{
            'end': 23,                         # Offset in bytes of the end of the object
            'start': 0,                        # Offset in bytes of the start of the object
            'time': 0,                         # Timestamp in milliseconds from the beginning of the corresponding audio stream
            'type': 'sentence',                # Type of speech mark (sentence, word, viseme, or ssml)
            'value': 'Mary had a little lamb.' # Type value
                                               #  ssml     - ssml tag
                                               #  viseme   - viseme name
                                               #  word     - substring
                                               #  sentence - substring
          }, ...]
        """
        kwargs                        = {}
        kwargs["Text"]                = text
        kwargs["TextType"]            = textType
        kwargs["OutputFormat"]        = outputFormat
        kwargs["SampleRate"]          = sampleRate
        kwargs["VoiceId"]             = voiceID
        if languageCode:
            kwargs["LanguageCode"]    = languageCode
        if speechMarkTypes:
            kwargs["SpeechMarkTypes"] = speechMarkTypes

        resp = self.pollyClient.synthesize_speech(**kwargs)
        resp.pop("ResponseMetadata")
        return resp
