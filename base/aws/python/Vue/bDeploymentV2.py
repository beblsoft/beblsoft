#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

CHANGES FROM V1:
  Client specifies install, build, and delete code functions
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.aws.python.S3.bBucketWebsite import BeblsoftBucketWebsite
from base.aws.python.S3.bBucketDirectory import BeblsoftBucketDirectory
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.CloudFront.bErrorResponse import BeblsoftCloudFrontErrorResponse
from base.aws.python.CloudFront.bOrigin import BeblsoftCloudFrontOrigin
from base.aws.python.CloudFront.bCacheBehavior import BeblsoftCloudFrontCacheBehavior
from base.aws.python.CloudFront.bDistribution import BeblsoftCloudFrontDistribution
from base.aws.python.Route53.ResourceRecordSet.bAlias import BeblsoftAliasResourceRecordSet
from base.bebl.python.run.bParallel import runPythonStrOnObjList
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT VUE DEPLOYMENT V2 ------------------------ #
class BeblsoftVueDeploymentV2():
    """
    Beblsoft Vue Deployment
    """

    def __init__(self, name,
                 distDir, installCodeFunc, buildCodeFunc, deleteCodeFunc,
                 bucketName, bHostedZone, domainList, bCertificate,
                 indexDocument="index.html",
                 enableCFDistribution=True, pathInvalidationList=None,
                 enableHostedZones=True,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            Deployment Name
            Ex. "QuoteProdVueDeployment"
          distDir:
            Vue code distribution directory. Where files are built
            Ex. "/home/jbensson/beblsoft/smeckn/webClient/dist"
          installCodeFunc:
            Function to install code
          buildCodeFunc:
            Function to build code
          deleteCodeFunc:
            Function to delete code
          bucketName:
            Name of the S3 bucket to store Vue application
            ex. "www.example.com"
          bHostedZone:
            Beblsoft Hosted Zone Object
          domainList:
            List of client domains to create
          bCertificate:
            Beblsoft ACM Certificate Object
            Ex. certificate for "*.quotablejoy.com"
          enableCFDistribution:
            If True, enable cloud front distribution
          pathInvalidationList:
            List of CF Distribution paths to invalidate when updating code
            Ex1. ["/*"]
            Ex2. ["/index.html"]
          enableHostedZones:
            If True, enable hosted zones
        """
        self.name                         = name
        self.distDir                      = distDir
        self.installCodeFunc              = installCodeFunc
        self.buildCodeFunc                = buildCodeFunc
        self.deleteCodeFunc               = deleteCodeFunc
        self.bHostedZone                  = bHostedZone
        self.domainList                   = domainList
        self.bCertificate                 = bCertificate
        self.enableCFDistribution         = enableCFDistribution
        self.pathInvalidationList         = pathInvalidationList
        self.enableHostedZones            = enableHostedZones
        self.bRegion                      = bRegion
        self.indexDocument                = indexDocument

        # S3 Bucket
        self.bBucketWebsite               = BeblsoftBucketWebsite(
            name                              = bucketName,
            indexDocument                     = self.indexDocument,
            acl                               = "public-read",
            bRegion                           = self.bRegion)
        self.bBucketDirectory             = BeblsoftBucketDirectory(
            key                               = "",
            bBucket                           = self.bBucketWebsite,
            localPath                         = "{}".format(self.distDir),
            acl                               = "public-read")

        # Cloud Front Distribution
        self.bCFOrigin                    = BeblsoftCloudFrontOrigin(
            idFunc                            = lambda: "S3-{}".format(self.bBucketWebsite.name),
            domainNameFunc                    = lambda: self.bBucketWebsite.domainName,
            s3OriginConfig                    = {"OriginAccessIdentity": ""},)
        self.bCFCacheBehavior             = BeblsoftCloudFrontCacheBehavior(
            bOrigin                           = self.bCFOrigin,
            viewerProtocolPolicy              = "redirect-to-https")
        self.bCFErrorResp403              = BeblsoftCloudFrontErrorResponse(
            errorCode                         = 403,
            responsePagePath                  = "/{}".format(self.indexDocument),
            responseCode                      = 200)
        self.bCFErrorResp404              = BeblsoftCloudFrontErrorResponse(
            errorCode                         = 404,
            responsePagePath                  = "/{}".format(self.indexDocument),
            responseCode                      = 200)
        self.bCFDistribution              = BeblsoftCloudFrontDistribution(
            name                              = "CFD-{}".format(name),
            bOriginList                       = [self.bCFOrigin],
            bDefaultCacheBehavior             = self.bCFCacheBehavior,
            defaultRootObject                 = self.indexDocument,
            bErrorResponseList                = [self.bCFErrorResp403,
                                                 self.bCFErrorResp404],
            cNameList                         = self.domainList,
            bCertificate                      = self.bCertificate)

        # Route53 Resource Record Sets
        self.bResourceRecordSetList       = [
            BeblsoftAliasResourceRecordSet(
                bHostedZone                   = bHostedZone,
                domainName                    = dn,
                recordType                    = "A",
                dnsNameFunc                   = lambda: self.bCFDistribution.domainName,
                cloudFrontAlias               = True)
            for dn in self.domainList
        ]
        self.bResourceRecordSetList      += [
            BeblsoftAliasResourceRecordSet(
                bHostedZone                   = bHostedZone,
                domainName                    = dn,
                recordType                    = "AAAA",
                dnsNameFunc                   = lambda: self.bCFDistribution.domainName,
                cloudFrontAlias               = True)
            for dn in self.domainList
        ]

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Vue Deployment
        """
        self.bBucketWebsite.create()
        self.updateCode()
        if self.enableCFDistribution:
            self.bCFDistribution.create()
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "create()")

    @logFunc()
    def delete(self):
        """
        Delete Vue Deployment
        """
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "delete()")
        if self.enableCFDistribution:
            self.bCFDistribution.delete()
        self.deleteRemoteCode()
        self.bBucketWebsite.delete()

    @logFunc()
    def describe(self):
        """
        Describe Vue Deployment
        """
        self.bBucketWebsite.describe()
        if self.enableCFDistribution:
            self.bCFDistribution.describe()
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "describe()")

    @logFunc()
    def update(self):
        """
        Update Vue Deployment
        """
        self.updateCode()
        if self.enableCFDistribution and self.pathInvalidationList:
            self.bCFDistribution.invalidate(pathList=self.pathInvalidationList)

    # ----------------------- CODE FUNCTIONS -------------------------------- #
    @logFunc()
    def deleteRemoteCode(self):
        """
        Delete Remote Code
        """
        self.bBucketDirectory.delete()

    @logFunc()
    def updateCode(self):
        """
        Update Code
        """
        self.deleteCodeFunc()
        self.installCodeFunc()
        self.buildCodeFunc()
        self.uploadCode()

    @logFunc()
    def uploadCode(self):
        """
        Upload code
        """
        self.bBucketDirectory.upload()
