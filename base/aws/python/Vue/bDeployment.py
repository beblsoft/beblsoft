#!/usr/bin/env python3
"""
NAME:
 bDeployment.py

DESCRIPTION
 Beblsoft Vue Deployment Functionality
 Vue Applications are deployed on AWS as shown in the picture below.

STACK PICTURE
    Client HTTP Request
          |
    AWS Route 53 Hosted Zone      - DNS Routing
          |
    AWS Cloudfront Distribution   - Global Caching Endpoints
          |
    AWS S3 Bucket Website         - Data source
          |
    Vue Application
"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from jinja2 import Environment, FileSystemLoader
from base.aws.python.S3.bBucketWebsite import BeblsoftBucketWebsite
from base.aws.python.S3.bBucketDirectory import BeblsoftBucketDirectory
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.CloudFront.bErrorResponse import BeblsoftCloudFrontErrorResponse
from base.aws.python.CloudFront.bOrigin import BeblsoftCloudFrontOrigin
from base.aws.python.CloudFront.bCacheBehavior import BeblsoftCloudFrontCacheBehavior
from base.aws.python.CloudFront.bDistribution import BeblsoftCloudFrontDistribution
from base.aws.python.Route53.ResourceRecordSet.bAlias import BeblsoftAliasResourceRecordSet
from base.bebl.python.run.bRun import run
from base.bebl.python.run.bParallel import runPythonStrOnObjList
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT VUE DEPLOYMENT --------------------------- #
class BeblsoftVueDeployment():
    """
    Beblsoft Vue Deployment
    """

    def __init__(self, name, codeDir, bucketName, bHostedZone,
                 domainList, autoGenDictFunc, bCertificate,
                 enableCFDistribution=True, enableHostedZones=True,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize object
        Args
          name:
            Deployment Name
            Ex. "QuoteProdVueDeployment"
          codeDir:
            Vue code directory
            Ex. "/home/jbensson/beblsoft/quote/client"
          bucketName:
            Name of the S3 bucket to store Vue application
            ex. "www.example.com"
          bHostedZone,
            Beblsoft Hosted Zone Object
          domainList:
            List of client domains to create
          autoGenDictFunc:
            Function to return key value pairs that will be placed in
            autogen.js
            Ex. lambda : {"domainName":"api.quotablejoy.com"}
          bCertificate:
            Beblsoft ACM Certificate Object
            Ex. certificate for "*.quotablejoy.com"
          enableCFDistribution:
            If True, enable cloud front distribution
          enableHostedZones:
            If True, enable hosted zones
        """
        self.name                         = name
        self.codeDir                      = codeDir
        self.bHostedZone                  = bHostedZone
        self.domainList                   = domainList
        self.autoGenDictFunc              = autoGenDictFunc
        self.bCertificate                 = bCertificate
        self.enableCFDistribution         = enableCFDistribution
        self.enableHostedZones            = enableHostedZones
        self.bRegion                      = bRegion
        self.indexDocument                = "index.html"

        # S3 Bucket
        self.bBucketWebsite               = BeblsoftBucketWebsite(
            name                              = bucketName,
            indexDocument                     = self.indexDocument,
            acl                               = "public-read",
            bRegion                           = self.bRegion)
        self.bBucketDirectory             = BeblsoftBucketDirectory(
            key                               = "",
            bBucket                           = self.bBucketWebsite,
            localPath                         = "{}/dist/".format(self.codeDir),
            acl                               = "public-read")

        # Cloud Front Distribution
        self.bCFOrigin                    = BeblsoftCloudFrontOrigin(
            idFunc                            = lambda: "S3-{}".format(self.bBucketWebsite.name),
            domainNameFunc                    = lambda: self.bBucketWebsite.domainName,
            s3OriginConfig                    = {"OriginAccessIdentity": ""},)
        self.bCFCacheBehavior             = BeblsoftCloudFrontCacheBehavior(
            bOrigin                           = self.bCFOrigin,
            viewerProtocolPolicy              = "redirect-to-https")
        self.bCFErrorResp403              = BeblsoftCloudFrontErrorResponse(
            errorCode                         = 403,
            responsePagePath                  = "/{}".format(self.indexDocument),
            responseCode                      = 200)
        self.bCFErrorResp404              = BeblsoftCloudFrontErrorResponse(
            errorCode                         = 404,
            responsePagePath                  = "/{}".format(self.indexDocument),
            responseCode                      = 200)
        self.bCFDistribution              = BeblsoftCloudFrontDistribution(
            name                              = "CFD-{}".format(name),
            bOriginList                       = [self.bCFOrigin],
            bDefaultCacheBehavior             = self.bCFCacheBehavior,
            defaultRootObject                 = self.indexDocument,
            bErrorResponseList                = [self.bCFErrorResp403,
                                                 self.bCFErrorResp404],
            cNameList                         = self.domainList,
            bCertificate                      = self.bCertificate)

        # Route53 Resource Record Sets
        self.bResourceRecordSetList       = [
            BeblsoftAliasResourceRecordSet(
                bHostedZone                   = bHostedZone,
                domainName                    = dn,
                recordType                    = "A",
                dnsNameFunc                   = lambda: self.bCFDistribution.domainName,
                cloudFrontAlias               = True)
            for dn in self.domainList
        ]
        self.bResourceRecordSetList      += [
            BeblsoftAliasResourceRecordSet(
                bHostedZone                   = bHostedZone,
                domainName                    = dn,
                recordType                    = "AAAA",
                dnsNameFunc                   = lambda: self.bCFDistribution.domainName,
                cloudFrontAlias               = True)
            for dn in self.domainList
        ]

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ----------------------- FULL STACK CRUD ------------------------------- #
    @logFunc()
    def create(self):
        """
        Create Vue Deployment
        """
        self.bBucketWebsite.create()
        self.updateCode()
        if self.enableCFDistribution:
            self.bCFDistribution.create()
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "create()")

    @logFunc()
    def delete(self):
        """
        Delete Vue Deployment
        """
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "delete()")
        if self.enableCFDistribution:
            self.bCFDistribution.delete()
        self.deleteRemoteCode()
        self.bBucketWebsite.delete()

    @logFunc()
    def describe(self):
        """
        Describe Vue Deployment
        """
        self.bBucketWebsite.describe()
        if self.enableCFDistribution:
            self.bCFDistribution.describe()
        if self.enableHostedZones:
            runPythonStrOnObjList(self.bResourceRecordSetList, "describe()")

    @logFunc()
    def update(self):
        """
        Update Vue Deployment
        """
        self.updateCode()
        if self.enableCFDistribution:
            self.bCFDistribution.invalidate(pathList=["/*"])

    # ----------------------- CODE FUNCTIONS ---------------------- #
    @logFunc()
    def installCodeDeps(self):
        """
        Install code dependencies
        """
        run(cmd="npm install", cwd=self.codeDir, raiseOnStatus=True)
        logger.info("{} Installed code dependencies".format(self))

    @logFunc()
    def buildCode(self):
        """
        Build Code
        """
        self.autoGenerateCodeSettings()
        run(cmd="npm run build", cwd=self.codeDir, raiseOnStatus=True)
        logger.info("{} Built code".format(self))

    @logFunc()
    def autoGenerateCodeSettings(self):
        """
        Auto generate Vue Settings File
        """
        templateFile        = "autogen.js"
        settingsPath        = "{}/{}".format(self.codeDir, templateFile)
        curFileDir          = os.path.dirname(os.path.realpath(__file__))
        jinjaEnv            = Environment(
            loader             = FileSystemLoader(curFileDir),
            trim_blocks        = True)
        template            = jinjaEnv.get_template(templateFile)
        templateStr         = template.render(
            generatorFile      = __file__,
            argDict            = self.autoGenDictFunc())
        if os.path.exists(settingsPath):
            os.remove(settingsPath)
        with open(settingsPath, "w") as f:
            f.write(templateStr)

    @logFunc()
    def deleteLocalCode(self):
        """
        Delete Local Code
        """
        run(cmd="npm run clean", cwd=self.codeDir, raiseOnStatus=True)
        logger.info("{} Cleaned code".format(self))

    @logFunc()
    def deleteRemoteCode(self):
        """
        Delete Remote Code
        """
        self.bBucketDirectory.delete()

    @logFunc()
    def updateCode(self):
        """
        Update Code
        """
        self.deleteLocalCode()
        self.installCodeDeps()
        self.buildCode()
        self.uploadCode()

    @logFunc()
    def uploadCode(self):
        """
        Upload code
        """
        self.bBucketDirectory.upload()
