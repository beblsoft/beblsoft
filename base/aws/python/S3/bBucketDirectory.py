#!/usr/bin/env python3
"""
NAME:
 bBucketDirectory.py

DESCRIPTION
 Beblsoft Bucket Directory Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
import boto3
from base.aws.python.S3.bBucketFile import BeblsoftBucketFile
from base.bebl.python.run.bParallel import runPythonStrOnObjList
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT BUCKET DIRECTORY ------------------------- #
class BeblsoftBucketDirectory():
    """
    Beblsoft Bucket Directory
    """

    def __init__(self, key, bBucket, localPath, downloadPath=None,
                 acl="public-read"):
        """
        Initialize Object
        Args
          key:
            Directory key, must be unique in bucket
          bBucket:
            Beblsoft Bucket object
          localPath:
            local directory to upload
          downloadPath:
            location to download directory
          acl:
            access control list
            one of: 'private'|'public-read'|'public-read-write'|'authenticated-read'|
            'aws-exec-read'|'bucket-owner-read'|'bucket-owner-full-control',
        """
        self.key              = key
        self.bBucket          = bBucket
        self.localPath        = localPath
        self.downloadPath     = downloadPath
        if not self.downloadPath:
            self.downloadPath = localPath
        self.acl              = acl
        self.bRegion          = bBucket.bRegion
        self.s3Client         = boto3.client("s3", region_name=self.bRegion.name)
        self.s3Res            = boto3.resource("s3", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Bucket={} Key={}]".format(
            self.__class__.__name__, self.bBucket.name, self.key)

    @logFunc()
    def upload(self):
        """
        Upload local directory to bucket
        """
        fileList    = []
        bBucketList = []
        for (root, _, files) in os.walk(self.localPath):
            fileList += [os.path.join(root, f) for f in files]

        for file in fileList:
            bBucketList.append(BeblsoftBucketFile(
                key       = os.path.join(self.key, os.path.relpath(file,
                                                                   self.localPath)),
                bBucket   = self.bBucket,
                localPath = file,
                acl       = self.acl))
        runPythonStrOnObjList(bBucketList, "upload()", parallel=True)

    @logFunc()
    def download(self):
        """
        Download directory from bucket
        """
        runPythonStrOnObjList(self.__getBBucketFiles(), "download()", parallel=True)

    @logFunc()
    def delete(self):
        """
        Delete directory from bucket
        """
        runPythonStrOnObjList(self.__getBBucketFiles(), "delete()", parallel=True)

    @logFunc()
    def describe(self):
        """
        Describe bucket directory
        """
        runPythonStrOnObjList(self.__getBBucketFiles(), "describe()", parallel=False)

    @logFunc()
    def __getBBucketFiles(self):
        """
        Return list of the bucket's BeblsoftBucketFile objects that belong
        to the directory
        """
        keyMetaList     = self.bBucket.getKeyMetaList()
        bBucketFileList = []
        for keyMeta in keyMetaList:
            key = keyMeta.get("Key")
            if key.startswith(self.key):
                bBucketFileList.append(
                    BeblsoftBucketFile(
                        key          = key,
                        bBucket      = self.bBucket,
                        localPath    = os.path.join(self.localPath,
                                                    os.path.relpath(key, self.key)),
                        downloadPath = os.path.join(self.downloadPath,
                                                    os.path.relpath(key, self.key)),
                        acl          = self.acl))
        return bBucketFileList
