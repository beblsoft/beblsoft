#!/usr/bin/env python3
"""
NAME:
 bBucketFile.py

DESCRIPTION
 Beblsoft Bucket File Functionality

NOTE
 File is synonomous with an AWS Object
"""

# ------------------------ IMPORTS ------------------------------------------ #
import mimetypes
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT BUCKET FILE ------------------------------ #
class BeblsoftBucketFile():
    """
    Beblsoft Bucket File
    """

    def __init__(self, key, bBucket, localPath, downloadPath=None,
                 acl="public-read"):
        """
        Initialize Object
        Args
          key:
            file key, must be unique in bucket
          bBucket:
            Beblsoft Bucket object
          localPath:
            local file to upload
          downloadPath:
            location to download file
          acl:
            access control list
            one of: 'private'|'public-read'|'public-read-write'|'authenticated-read'|
            'aws-exec-read'|'bucket-owner-read'|'bucket-owner-full-control',
        """
        self.key                = key
        self.bBucket            = bBucket
        self.localPath          = localPath
        self.downloadPath       = downloadPath
        if not self.downloadPath:
            self.downloadPath   = self.localPath
        self.acl                = acl
        self.contentType, _     = mimetypes.guess_type(self.localPath)
        if not self.contentType:
            self.contentType    = "binary/octet-stream"
        self.bRegion            = bBucket.bRegion
        self.s3Client           = boto3.client("s3", region_name=self.bRegion.name)
        self.s3Res              = boto3.resource("s3", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Bucket={} Key={}]".format(
            self.__class__.__name__, self.bBucket.name, self.key)

    @property
    def exists(self):
        """
        Return True if the bucket file exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @logFunc()
    def upload(self):
        """
        Upload file to bucket
        """
        objRes    = self.__getResource()
        waiter    = self.s3Client.get_waiter("object_exists")
        with open(self.localPath, "rb") as f:
            objRes.put(ACL=self.acl,
                       Body=f.read(),
                       ContentType=self.contentType)
        waiter.wait(Bucket=self.bBucket.name, Key=self.key)
        logger.info("{} Uploaded Key {} <- {}".format(
            self.bBucket, self.key, self.localPath))

    @logFunc()
    def download(self):
        """
        Download file from bucket
        """
        objRes = self.__getResource()
        objRes.download_file(self.downloadPath)
        logger.info("{} Downloaded Key {} -> {}".format(
            self.bBucket, self.key, self.downloadPath))

    @logFunc()
    def delete(self):
        """
        Delete file from bucket
        """
        if self.exists:
            objRes = self.__getResource()
            objRes.delete()
            waiter = self.s3Client.get_waiter("object_not_exists")
            waiter.wait(Bucket=self.bBucket.name, Key=self.key)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe bucket file
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    @logFunc()
    def __getMetadata(self):
        """
        Return bucket metadata
        """
        meta = None
        try:
            objRes = self.__getResource()
            meta   = objRes.get()
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code == "NoSuchBucket":
                pass
            if code =="NoSuchKey":
                pass
            else:
                logger.exception(e.__dict__)
                raise(e)
        return meta

    @logFunc()
    def __getResource(self):
        """
        Get bucket file resource
        """
        return self.s3Res.Object(self.bBucket.name, self.key) #pylint: disable=E1101
