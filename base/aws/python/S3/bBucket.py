#!/usr/bin/env python3
"""
NAME:
 bBucket.py

DESCRIPTION
 Beblsoft Bucket Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger   = logging.getLogger(__file__)


# ----------------------- BEBLSOFT BUCKET ----------------------------------- #
class BeblsoftBucket():
    """
    Beblsoft Bucket
    """

    def __init__(self, name, acl="public-read",
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            bucket name
            ex. jbensson-bucket1
          acl:
            access control list
            one of: private, public-read, public-read-write, authenticated-read
        """
        self.name     = name
        self.acl      = acl
        self.bRegion  = bRegion
        self.s3Client = boto3.client("s3", region_name=bRegion.name)
        self.s3Res    = boto3.resource("s3", region_name=bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @property
    def exists(self):
        """
        Return True if Bucket exists
        """
        res = self.__getResource()
        return (res.creation_date != None)

    @logFunc()
    def create(self):
        """
        Create bucket
        """
        if self.exists:
            logger.info("{} Already exists".format(self))
        else:
            self.s3Client.create_bucket(Bucket=self.name, ACL=self.acl)
            waiter = self.s3Client.get_waiter("bucket_exists")
            waiter.wait(Bucket=self.name)
            logger.info("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete bucket
        """
        if self.exists:
            self.s3Client.delete_bucket(Bucket=self.name)
            waiter = self.s3Client.get_waiter("bucket_not_exists")
            waiter.wait(Bucket=self.name)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @logFunc()
    def describe(self):
        """
        Describe bucket
        """
        keyList = self.getKeyMetaList()
        logger.info("{} Keys:\n{}".format(self, pprint.pformat(keyList)))

    @logFunc()
    def getKeyMetaList(self):
        """
        Return list of bucket key metadata
        """
        keyMetaList = []
        if self.exists:
            paginator = self.s3Client.get_paginator("list_objects")
            pageIt = paginator.paginate(Bucket=self.name)
            for page in pageIt:
                keyMetaList += page.get("Contents", None)
        return keyMetaList

    @logFunc()
    def __getResource(self):
        """
        Return bucket resource
        """
        return self.s3Res.Bucket(self.name) #pylint: disable=E1101

    @staticmethod
    def list(bRegion=BeblsoftRegion.getDefault()):
        """
        List all buckets
        """
        s3Client = boto3.client("s3", region_name=bRegion.name)
        resp     = s3Client.list_buckets()
        buckets  = resp.get("Buckets", None)
        if buckets:
            logger.info("Buckets:")
            for bucket in buckets:
                logger.info(pprint.pformat(bucket))
        else:
            logger.info("No buckets exist")
