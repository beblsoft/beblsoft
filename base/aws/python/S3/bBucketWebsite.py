#!/usr/bin/env python3
"""
NAME:
 bBucketWebsite.py

DESCRIPTION
 Beblsoft Bucket Website Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
from botocore.exceptions import ClientError
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.aws.python.S3.bBucket import BeblsoftBucket
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT BUCKET WEBSITE --------------------------- #
class BeblsoftBucketWebsite(BeblsoftBucket):
    """
    Beblsoft Bucket Website
    """

    def __init__(self, name,
                 indexDocument=None, errorDocument=None,
                 acl="public-read", bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          For most:
            See BeblsoftBucket
          indexDocument:
            Document to forward requests
            ex. "index.html"
          errorDocument:
            Object key to use for 4XX error occurs
            ex "error.html"
        """
        self.indexDocument = indexDocument
        self.errorDocument = errorDocument
        super().__init__(name=name, acl=acl, bRegion=bRegion)

    @property
    def isWebConfigured(self):
        """
        Return True if bucket is configured as a website
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def domainName(self):
        """
        Return bucket domain name
        Note:
          Used for connecting Cloud Front with S3 Website
        """
        return "{}.s3.amazonaws.com".format(self.name)

    @property
    def s3DomainName(self):
        """
        Return S3 Domain Name
        Note:
          Used for visting S3 website in Chrome
        """
        return "{}.s3-website-{}.amazonaws.com".format(self.name, self.bRegion.name)

    @logFunc()
    def create(self):
        """
        Create bucket website
        """
        super().create()
        if self.isWebConfigured:
            logger.info("{} Already configured as website".format(self))
        else:
            websiteConfig = {}
            if self.indexDocument:
                websiteConfig["IndexDocument"] = {"Suffix": self.indexDocument}
            if self.errorDocument:
                websiteConfig["ErrorDocument"] = {"Key": self.errorDocument}
            self.s3Client.put_bucket_website(
                Bucket=self.name,
                WebsiteConfiguration=websiteConfig)
            logger.info("{} Configured as a website".format(self))

    @logFunc()
    def delete(self):
        """
        Delete bucket website
        """
        if self.isWebConfigured:
            self.s3Client.delete_bucket_website(
                Bucket=self.name)
            logger.info("{} Deleted website configuration".format(self))
        else:
            logger.info("{} Never configured as a website".format(self))
        super().delete()

    @logFunc()
    def describe(self):
        """
        Describe bucket website
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))
        super().describe()

    @logFunc()
    def __getMetadata(self):
        """
        Return bucket website metadata
        """
        meta = None
        try:
            meta = self.s3Client.get_bucket_website(Bucket=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code == "NoSuchWebsiteConfiguration":
                pass
            elif code == "NoSuchBucket":
                pass
            else:
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
