#!/usr/bin/env python3
"""
NAME:
 bCMKey.py

DESCRIPTION
 Beblsoft KMS Customer Master Key Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT KMS CM KEY ------------------------------- #
class BeblsoftKMSCMKey(BeblsoftAWSObject):
    """
    Beblsoft KMS CM Key
    """

    def __init__(self, name, policy=None, description=None,  # pylint: disable=W0102
                 usage="ENCRYPT_DECRYPT", origin="AWS_KMS",
                 bpplSafetyCheck=False, tagList=[],
                 pendingWindowDays=30,
                 bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            Key Name
            Ex. SmecknKey
          policyStr:
            Key policy to attach to the CMK
            If None, AWS uses Default Key Policy
          description:
            Description of CMK
          usage:
            Intended use of the CMK
          origin:
            Source of CMK's key material
            One of "AWS_KMS" | "EXTERNAL"
          bpplSafetyCheck:
            Bypass Policy Lockout Safety Check
            If True, could prevent creater from deleting the key
          tagList:
            List of key tags
            [{
              'TagKey': 'string',
              'TagValue': 'string'
            }]
          pendingWindowDays:
            Number of days after the waiting period before AWS deletes the CMK
            Between 7 and 30
        """
        super().__init__()
        self.name              = name
        self.policy            = policy
        self.description       = description
        self.usage             = usage
        self.origin            = origin
        self.bpplSafetyCheck   = bpplSafetyCheck
        self.nameTag           = {
            "TagKey": "name",
            "TagValue": self.name
        }
        self.tagList           = tagList
        self.tagList.append(self.nameTag)
        self.pendingWindowDays = pendingWindowDays
        self.bRegion           = bRegion
        self.kmsClient         = boto3.client("kms", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def id(self): #pylint: disable=C0111
        return self.getValueFromMetadata(key="KeyId")

    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata(key="Arn")

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        kwargs                                   = {}
        if self.policy:
            kwargs["Policy"]                     = self.policy
        if self.description:
            kwargs["Description"]                = self.description
        kwargs["KeyUsage"]                       = self.usage
        kwargs["Origin"]                         = self.origin
        kwargs["BypassPolicyLockoutSafetyCheck"] = self.bpplSafetyCheck
        kwargs["Tags"]                           = self.tagList
        self.kmsClient.create_key(**kwargs)
        self.enable()

    @logFunc()
    def _delete(self):  # pylint: disable=W0221
        """
        Delete object
        """
        self.disable()
        self.kmsClient.schedule_key_deletion(
            KeyId=self.id, PendingWindowInDays=self.pendingWindowDays)

    @logFunc()
    def _enable(self, **kwargs):
        """
        Enable object
        """
        self.kmsClient.enable_key(KeyId=self.id)

    @logFunc()
    def _disable(self, **kwargs):
        """
        Disable object
        """
        self.kmsClient.disable_key(KeyId=self.id)

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/kms.html#KMS.Client.describe_key
        """
        keyId = None
        meta  = None
        # Double for loop
        # - Get all key id/arns
        # - Get each key's tags
        #   If matching name tag found, got the right key
        keyIdArnList = BeblsoftKMSCMKey.getKeyIDArnList(
            bRegion=self.bRegion)
        for curKey in keyIdArnList:
            curKeyId      = curKey.get("KeyId")
            curKeyTagList = BeblsoftKMSCMKey.getKeyTagList(
                keyID=curKeyId, bRegion=self.bRegion)
            for curTag in curKeyTagList:
                keyMatch = curTag.get("TagKey")    == "name"
                nameMatch = curTag.get("TagValue") == self.name
                if keyMatch and nameMatch:
                    keyId = curKeyId
                    break
            if keyId:
                break
        if keyId:
            resp = self.kmsClient.describe_key(KeyId=keyId)
            meta = resp.get("KeyMetadata")
        return meta

    # -------------------------- STATIC FUNCTIONS --------------------------- #
    @staticmethod
    def getKeyTagList(keyID, limit=100, bRegion=BeblsoftRegion.getDefault()):
        """
        Return list of keyID tags
        Args
          limit:
            pagination limit
        Returns
           [{'TagKey': 'string',
             'TagValue': 'string' },... ],
        """
        nextMarker = None
        tagList    = []
        kmsClient  = boto3.client("kms", region_name=bRegion.name)
        while True:
            kwargs               = {}
            kwargs["KeyId"]      = keyID
            kwargs["Limit"]      = limit
            if nextMarker:
                kwargs["Marker"] = nextMarker
            try:
                resp = kmsClient.list_resource_tags(**kwargs)
                tagList.extend(resp.get("Tags"))
                nextMarker = resp.get("NextMarker")
            except ClientError as e:
                code = e.response["Error"]["Code"]
                if code != "AccessDeniedException":
                    logger.exception(pprint.pformat(e.__dict__))
                    raise(e)
                nextMarker = None
            if not nextMarker:
                break
        return tagList

    @staticmethod
    def getKeyIDArnList(limit=100, bRegion=BeblsoftRegion.getDefault()):
        """
        Return list of all account key ids and arns
        Args
          limit:
            pagination limit
        Returns
          [{'KeyId': 'string',
            'KeyArn': 'string'},... ],
        """
        nextMarker   = None
        keyIdArnList = []
        kmsClient    = boto3.client("kms", region_name=bRegion.name)
        while True:
            kwargs                   = {}
            kwargs["Limit"]          = limit
            if nextMarker:
                kwargs["NextMarker"] = nextMarker
            resp = kmsClient.list_keys(**kwargs)
            keyIdArnList.extend(resp.get("Keys"))
            nextMarker = resp.get("NextMarker")
            if not nextMarker:
                break
        return keyIdArnList
