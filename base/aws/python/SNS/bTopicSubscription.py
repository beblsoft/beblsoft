#!/usr/bin/env python3
"""
NAME:
 bTopicSubscription.py

DESCRIPTION
 Beblsoft SNS Topic Subscription Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SNS TOPIC SUBSCRIPTION ------------------- #
class BeblsoftSNSTopicSubscription(BeblsoftAWSObject):
    """
    Beblsoft SNS Topic Subscription
    """

    def __init__(self, bSNSTopic, protocol, endpointFunc):
        """
        Initialize Object
        Args
          bSNSTopic
            Beblsoft SNS Topic
          protocol:
            One of
            http                     : delivery of JSON-encoded message via HTTP POST
            https                    : delivery of JSON-encoded message via HTTPS POST
            email                    : delivery of message via SMTP
            email-json               : delivery of JSON-encoded message via SMTP
            sms                      : delivery of message via SMS
            sqs                      : delivery of JSON-encoded message to an Amazon SQS queue
            application              : delivery of JSON-encoded message to an EndpointArn for a mobile app and device.
            lambda                   : delivery of JSON-encoded message to an AWS Lambda function.
          endpointFunc:
            Function to return protocol endpoint
            Ex. lambda: arn:aws:lambda:us-east-1:225928776711:function:SmecknTestLambdaSwitchDeployment
            Endpoint one of
            For http protocol        : the endpoint is an URL beginning with "http://"
            For https protocol       : the endpoint is a URL beginning with "https://"
            For email protocol       : the endpoint is an email address
            For email-json protocol  : the endpoint is an email address
            For sms protocol         : the endpoint is a phone number of an SMS-enabled device
            For sqs protocol         : the endpoint is the ARN of an Amazon SQS queue
            For application protocol : the endpoint is the EndpointArn of a mobile app and device.
            For lambda protocol      : the endpoint is the ARN of an AWS Lambda function.
        """
        super().__init__()
        self.bSNSTopic    = bSNSTopic
        self.bRegion      = bSNSTopic.bRegion
        self.protocol     = protocol
        self.endpointFunc = endpointFunc
        self.snsClient    = boto3.client("sns", region_name=self.bRegion.name)

    def __str__(self):
        return "[{} Topic={}]".format(self.__class__.__name__, self.bSNSTopic.name)

    # ----------------------- PROPERTIES ------------------------------------ #
    @property
    def arn(self): #pylint: disable=C0111
        return self.getValueFromMetadata(key="SubscriptionArn")

    # ----------------------- VERBS ----------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create object
        """
        self.snsClient.subscribe(TopicArn=self.bSNSTopic.arn,
                                 Protocol=self.protocol, Endpoint=self.endpointFunc())

    @logFunc()
    def _delete(self, **kwargs):
        """
        Delete object
        """
        self.snsClient.unsubscribe(SubscriptionArn=self.arn)

    @logFunc()
    def _confirm(self, token, authOnUnsubscribe=True):  # pylint: disable=W0221
        """
        Confirm subscription
        Args
          token:
            short lived token sent to endpoint during subscribe action
          autnOnUnsubscribe:
            If True, disallows unathenticated unsubscribes
        """
        authStr = 'true'
        if not authOnUnsubscribe:
            authStr = "false"
        self.snsClient.confirm_subscription(
            TopicArn=self.bSNSTopic.arn,
            Token=token, AuthenticateOnUnsubscribe=authStr)

    # ----------------------- METADATA -------------------------------------- #
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/sns.html#SNS.Client.list_subscriptions_by_topic
        """
        meta          = None
        subscriptions = self.bSNSTopic.getAllSubscriptions()
        for s in subscriptions:
            protocolMatch = s.get("Protocol") == self.protocol
            endpointMatch = s.get("Endpoint") == self.endpointFunc()
            if protocolMatch and endpointMatch:
                meta = s
                break
        return meta
