#!/usr/bin/env python3
"""
NAME:
 bTopic.py

DESCRIPTION
 Beblsoft SNS Topic Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SNS TOPIC -------------------------------- #
class BeblsoftSNSTopic(BeblsoftAWSObject):
    """
    Beblsoft SNS Topic
    """

    def __init__(self, name, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        Args
          name:
            Topic name
            ex. smecknTestEmailSendTopic
          acl:
            access control list
            one of: private, public-read, public-read-write, authenticated-read
        """
        super().__init__()
        self.name      = name
        self.bRegion   = bRegion
        self.stsClient = boto3.client("sts")
        self.snsClient = boto3.client("sns", region_name=bRegion.name)

    def __str__(self):
        return "[{} Name={}]".format(self.__class__.__name__, self.name)

    @classmethod
    def fromARN(cls, arn):
        """
        Return Beblsoft SNS Topic object given an ARN
        """
        (_, _, _, regionName, _, name) = arn.split(":")
        bRegion = BeblsoftRegion(name = regionName)
        return BeblsoftSNSTopic(name=name, bRegion=bRegion)

    # -------------------------- PROPERTIES --------------------------------- #
    @property
    def arn(self):
        """
        Return object arn
        """
        resp = self.stsClient.get_caller_identity()
        arn  = "arn:aws:sns:{}:{}:{}".format(
            self.bRegion.name, resp.get("Account"), self.name)
        return arn

    # -------------------------- VERBS -------------------------------------- #
    @logFunc()
    def _create(self, **kwargs): #pylint: disable=W0613
        """
        Create object
        """
        self.snsClient.create_topic(Name=self.name)

    @logFunc()
    def _delete(self, **kwargs): #pylint: disable=W0613
        """
        Delete object
        """
        self.snsClient.delete_topic(TopicArn=self.arn)

    @logFunc()
    def publish(self, message, subject=None):
        """
        Publish message to a topic
        Args
          message:
            Message to send to topic [Required]
            UTF-8 encoded string, at most 256 KB in size
          subject:
            Subject of message for email endpoints

        Returns
          Message ID
        """
        kwargs                = {}
        kwargs["TopicArn"]    = self.arn
        kwargs["Message"]     = message
        if subject:
            kwargs["Subject"] = subject

        resp = self.snsClient.publish(**kwargs)
        return resp.get("MessageId")

    # -------------------------- METADATA ----------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
          None or metadata object here
          http://boto3.readthedocs.io/en/latest/reference/services/sns.html#SNS.Client.get_topic_attributes
        """
        meta = None
        try:
            meta = self.snsClient.get_topic_attributes(TopicArn=self.arn)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "NotFound":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta

    # -------------------------- HELPERS ------------------------------------ #
    @logFunc()
    def getAllSubscriptions(self):
        """
        Get list of all topic subscriptions
        Returns:
          [] -OR- Subscription List
        """
        subscriptions = []
        nextToken     = None
        if self.exists:
            # Loop pulling out all subscriptions
            while True:
                kwargs                  = {}
                kwargs["TopicArn"]      = self.arn
                if nextToken:
                    kwargs["NextToken"] = nextToken
                resp = self.snsClient.list_subscriptions_by_topic(**kwargs)
                subscriptions.extend(resp.get("Subscriptions"))
                nextToken = resp.get("NextToken", None)
                if not nextToken:
                    break
        return subscriptions
