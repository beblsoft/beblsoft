#!/usr/bin/env python3.6
"""
NAME:
 bClient.py

DESCRIPTION
 Beblsoft Translate Client
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import boto3
from base.aws.python.EC2.bRegion import BeblsoftRegion
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT TRANSLATE CLIENT ------------------------- #
class BeblsoftTranslateClient():
    """
    Beblsoft Translate Client
    """

    def __init__(self, bRegion=BeblsoftRegion.getDefault()):
        """
        Initialize Object
        """
        self.transClient = boto3.client("translate", region_name=bRegion.name)

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ----------------------- TRANSLATE TEXT -------------------------------- #
    @logFunc()
    def translateText(self, sourceText, targetLanguageCode, sourceLanguageCode="auto",
                      terminologyList=None):
        """
        Translate Text
        Args
          sourceText:
            Source text to be translated
            Max bytes = 5000
          targetLanguageCode:
            Language code of desired target text
            Ex. "es"
          sourceLanguageCode:
            Language code of source text
            Specify auto to have AWS use Comprehend to determine source language
            Ex1. "en"
            Ex2. "auto"
          terminologyList:
            TerminologyNames list that is used by TranslateText
            Min length = 0
            Max length = 1

        Returns
          {
            'TranslatedText': 'string',
            'SourceLanguageCode': 'string',
            'TargetLanguageCode': 'string',
            'AppliedTerminologies': [
                {
                  'Name': 'string',
                  'Terms': [
                      {
                          'SourceText': 'string',
                          'TargetText': 'string'
                      },
                  ]
                },
            ]
          }
        """
        kwargs                           = {}
        kwargs["Text"]                   = sourceText
        kwargs["TargetLanguageCode"]     = targetLanguageCode
        if sourceLanguageCode:
            kwargs["SourceLanguageCode"] = sourceLanguageCode
        if terminologyList:
            kwargs["TerminologyNames"]   = terminologyList

        resp = self.transClient.translate_text(**kwargs)
        resp.pop("ResponseMetadata")
        return resp
