OVERVIEW
===============================================================================
AWS Translate Technology Documentation
- Description                       : Amazon translate is a neural machine translation service that delivers
                                      fast, high quality, and affordable language translation
                                    : Neural machine translation is a form of language translation automation that
                                      uses deep learning models to deliver more accurate and more natural sounding
                                      translation than traditional statistical and rule-based translation algorigthms
- Features --------------------------
  * Broad language coverage         : Supports 21 languages
  * Neural Network Based            : Uses neural networks leveraging entire sentence context instead of minimal phrase based
  * Named Entity Translation Custom : Setup custom rules for named entities
  * Language Identification         : Automatically identifies the source language when it is not specified
  * Real-time Translations          : Instant translation
  * Secure                          : Comm between webpage or apps and service is protected by SSL encryption
- URLS ------------------------------
 * Home                             : https://aws.amazon.com/translate/
 * Documentation                    : https://docs.aws.amazon.com/translate/latest/dg/what-is.html
 * Supported Language Pairs         : https://docs.aws.amazon.com/translate/latest/dg/pairs.html



HOW IT WORKS
===============================================================================
- Neural Netwokrs                   : Translate service is based on neural networks trained for language translation
- Text Conversion -------------------
  * Source Text                     : Text that will be translated, in UTF-8 format
  * Output Text                     : Text that has been translated, in UTF-8 format
- Translation Model -----------------
  * Encoder                         : Reads a source sentence one word at a time and constructs a semantic
                                      representation that captures its meaning
  * Decoder                         : Uses the semantic representation to generate a translation one word at
                                      a time in the target language
  * Attention Mechanisms            : Used to help understand context. Helps decoder decide which words in source
                                      text are most relevant for generating target word
- Language Codes --------------------
  * Arabic                          : ar
  * Chinese (Simplified)            : zh
  * Chinese (Traditional)           : zh-TW
  * Czech                           : cs
  * Danish                          : da
  * Dutch                           : nl
  * English                         : en
  * Finnish                         : fi
  * French                          : fr
  * German                          : de
  * Hebrew                          : he
  * Indonesian                      : id
  * Italian                         : it
  * Japanese                        : ja
  * Korean                          : ko
  * Polish                          : pl
  * Portuguese                      : pt
  * Russian                         : ru
  * Spanish                         : es
  * Swedish                         : sv
  * Turkish                         : tr
- Automatic Language Detection -----: Automatically detects source text language
- Exception Handling ----------------
  * UnsupportedLanguagePairException: Translate doesn't support translation between pair
  * DetectedLanguageLowConfidence
    Exception                       : Translate has low confidence that it detected the correct source language



CUSTOM TERMINOLOGY
===============================================================================
- Description                       : Using custom terminology with translation requests enables developer to make sure
                                      that brand names, character names, model names, and other unique content
                                      is translated exactly as desired, regardless of its context and the Amazon
                                      Translate algorithm's decision