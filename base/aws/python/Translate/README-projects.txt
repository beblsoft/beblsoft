OVERVIEW
===============================================================================
Translate Samples Documentation



PROJECT: SAMPLES
===============================================================================
- Description                       : Translate Samples
- Directory                         : ./samples
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Commands --------------------------
  * Translate text                  : ./cli.py translate-text