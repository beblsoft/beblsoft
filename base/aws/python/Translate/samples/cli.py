#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Translate Samples
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.aws.python.Translate.bClient import BeblsoftTranslateClient


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Translate -------------------------------
        self.bTranslate        = BeblsoftTranslateClient()

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/translateSample.log", type=click.Path(),
              help="Specify log file Default=/tmp/translateSample.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Translate Samples
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- TRANSLATE TEXT ------------------------------------ #
@cli.command()
def translate_text():
    """
    Translate Text
    """
    textList = [
        "Hello World!",
        "Hey Andrew, my name is James. What is your name?"
    ]
    for text in textList:
        resp = gc.bTranslate.translateText(
            sourceText = text,
            targetLanguageCode = "es",
            sourceLanguageCode = "en")
        logger.info("English={}".format(text))
        logger.info("Spanish={}".format(resp.get("TranslatedText")))


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
