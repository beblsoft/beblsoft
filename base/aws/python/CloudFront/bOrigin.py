#!/usr/bin/env python3
"""
NAME:
 bOrigin.py

DESCRIPTION
 Beblsoft Cloud Front Origin Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT CLOUD FRONT ORIGIN ----------------------- #
class BeblsoftCloudFrontOrigin():
    """
    Beblsoft Cloud Front Origin
    """

    def __init__(self,  # pylint: disable=W0102
                 idFunc,
                 domainNameFunc,
                 originPath="",
                 customHeaders= {
                     "Quantity": 0,
                     "Items":[]
                 },
                 s3OriginConfig = None,
                 customOriginConfig = None):
        """
        Initialize Object
        Args
          For detailed description see:
            CloudFront.Client.create_distribution
            http://boto3.readthedocs.io/en/latest/reference/services/cloudfront.html#CloudFront.Client.create_distribution_with_tags

          idFunc:
            Function to return origin ID
            For Amazon S3 returns: S3-Website-myawsbucket.s3.amazonaws.co
          domainNameFunc:
            Function to return domain name
            Ex. lambda:some.url
            For Amazon S3 returns: myawsbucket.s3.amazonaws.com
          originPath:
            Optional element that causes cloudfront to request content
            from a directory in your S3 bucket
            Ex. "/production"
          customHeaders:
            A complex type that contains names and values for the custom
            headers that you want.
          s3OriginConfig:
            A complex type that contains information about the Amazon S3 origin.
          customOriginConifg:
            A complex type that contains information about a custom origin.
            If the origin is an Amazon S3 bucket, use the S3OriginConfig element
            instead.
        """
        self.idFunc             = idFunc
        self.domainNameFunc     = domainNameFunc
        self.originPath         = originPath
        self.customHeaders      = customHeaders
        self.s3OriginConfig     = s3OriginConfig
        self.customOriginConfig = customOriginConfig

    @property
    def id(self):
        """
        Return origin id
        """
        return self.idFunc()

    @property
    def domainName(self):
        """
        Return origin domain name
        """
        return self.domainNameFunc()

    @property
    def distributionConfig(self):
        """
        Return distribution configuration
        """
        dc = {
            "Id": self.id,
            "DomainName": self.domainNameFunc(),
            "OriginPath": self.originPath,
            "CustomHeaders": self.customHeaders,
        }
        if self.s3OriginConfig:
            dc["S3OriginConfig"]     = self.s3OriginConfig
        if self.customOriginConfig:
            dc["CustomOriginConfig"] = self.customOriginConfig
        return dc
