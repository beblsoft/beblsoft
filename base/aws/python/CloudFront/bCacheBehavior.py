#!/usr/bin/env python3
"""
NAME:
 bCacheBehavior.py

DESCRIPTION
 Beblsoft Cloud Front Cache Behavior Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT CLOUD FRONT CACHE BEHAVIOR --------------- #
class BeblsoftCloudFrontCacheBehavior():
    """
    Beblsoft Cloud Front Cache Behavior
    """

    def __init__(self,  # pylint: disable=W0102
                 bOrigin,
                 pathPattern="*",
                 forwardedValues = {
                     "QueryString": True,
                     "Cookies": {
                         "Forward": "all"  # "none" | "whitelist" | "all",
                     },
                     "Headers": {
                        "Quantity": 0,
                        "Items": []
                     },
                     "QueryStringCacheKeys": {
                        "Quantity": 0,
                        "Items": []
                     }
                 },
                 trustedSigners = {
                     "Enabled": False,
                     "Quantity": 0,
                 },
                 viewerProtocolPolicy = "allow-all",  # "allow-all"|"https-only"|"redirect-to-https",
                 allowedMethods = {
                     "Quantity": 2,
                     "Items": ["GET", "HEAD"],
                     "CachedMethods": {
                         "Quantity": 2,
                         "Items": ["GET", "HEAD"],
                     },
                 },
                 smoothStreaming = False,
                 defaultTTL = 86400,
                 minTTL = 300,
                 maxTTL = 31536000,
                 compress = False,
                 lambdaFunctionAssociations = {
                     "Quantity": 0
                 }):
        """
        Initialize Object
        Args
          For detailed description see:
            CloudFront.Client.create_distribution
            http://boto3.readthedocs.io/en/latest/reference/services/cloudfront.html#CloudFront.Client.create_distribution_with_tags

          bOrigin:
            Beblsoft Cloud Front Origin Object
          pathPattern:
            Pattern that specifies which requests to apply cache behavior
          forwardedValues:
            A complex type that specifies how CloudFront handles query strings
            and cookies.
          trustedSigners:
            A complex type that specifies the AWS accounts, if any, that you
            want to allow to create signed URLs for private content.
          allowedMethods:
            A complex type that controls which HTTP methods CloudFront
            processes and forwards to your Amazon S3 bucket or your custom
            origin.
          smoothStreaming:
            Indicates whether you want to distribute media files in the
            Microsoft Smooth Streaming format using the origin that is
            associated with this cache behavior
          defaultTTL:
            The default amount of time that you want objects to stay in
            CloudFront caches before CloudFront forwards another request to
            your origin to determine whether the object has been updated.
          minTTL:
            The minimum amount of time that you want objects to stay in
            CloudFront caches before CloudFront forwards another request to
            your origin to determine whether the object has been updated.
          maxTTL:
            The maximum amount of time that you want objects to stay in
            CloudFront caches before CloudFront forwards another request to your
            origin to determine whether the object has been updated
          compress:
            Whether you want CloudFront to automatically compress certain files
            for this cache behavio
          lambdaFunctionAssociations:
            A complex type that contains zero or more Lambda function
            associations for a cache behavior.
        """
        self.bOrigin                    = bOrigin
        self.pathPattern                = pathPattern
        self.forwardedValues            = forwardedValues
        self.trustedSigners             = trustedSigners
        self.viewerProtocolPolicy       = viewerProtocolPolicy
        self.allowedMethods             = allowedMethods
        self.smoothStreaming            = smoothStreaming
        self.defaultTTL                 = defaultTTL
        self.minTTL                     = minTTL
        self.maxTTL                     = maxTTL
        self.compress                   = compress
        self.lambdaFunctionAssociations = lambdaFunctionAssociations
