#!/usr/bin/env python3
"""
NAME:
 bErrorResponse.py

DESCRIPTION
 Beblsoft Cloud Front Error Response Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT CLOUD FRONT ERROR RESPONSE --------------- #
class BeblsoftCloudFrontErrorResponse():
    """
    Beblsoft Cloud Front Error Response
    """

    def __init__(self, errorCode, responsePagePath, responseCode,
                 errorCachingMinTTL=300):
        """
        Initialize Object
        Args
          errorCode:
            HTTP status code for which custom error page should be served
            Ex. 404
          responsePagePath:
            Path to custom error page that cloud font returns to viewer
            Ex. "403-forbidden.html"
          responseCode:
            HTTP status code that cloud front returns to viewer
            Ex. 200
          errorCachingMinTTL:
            Miniumum amount of time, in seconds, that cloud front should cache
            the HTTP status code
            Ex. 300
        """
        self.errorCode          = errorCode
        self.responsePagePath   = responsePagePath
        self.responseCode       = str(responseCode)
        self.errorCachingMinTTL = errorCachingMinTTL

    @property
    def distributionConfig(self):
        """
        Return distribution configuration
        """
        return {
            "ErrorCode": self.errorCode,
            "ResponsePagePath": self.responsePagePath,
            "ResponseCode": self.responseCode,
            "ErrorCachingMinTTL": self.errorCachingMinTTL
        }
