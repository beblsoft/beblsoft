#!/usr/bin/env python3
"""
NAME:
 bDistribution.py

DESCRIPTION
 Beblsoft Cloud Front Distribution Functionality

RELEVANT URLS
  https://www.davidbaumgold.com/tutorials/host-static-site-aws-s3-cloudfront/#make-a-cloudfront-distribution
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import logging
import pprint
from datetime import datetime
import boto3
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger   = logging.getLogger(__file__)
cfClient = boto3.client("cloudfront")


# ----------------------- BEBLSOFT CLOUD FRONT DISTRIBUTION ----------------- #
class BeblsoftCloudFrontDistribution():
    """
    Beblsoft Cloud Front Distribution
    """

    def __init__(self,  # pylint: disable=W0102
                 name,
                 bOriginList,
                 bDefaultCacheBehavior,
                 defaultRootObject,
                 bCacheBehaviorList=None,
                 bErrorResponseList=None,
                 cNameList=None,
                 comment=None,
                 dLogging = {
                     "Enabled": False,
                     "IncludeCookies": False,
                     "Bucket": "",
                     "Prefix": ""
                 },
                 priceClass="PriceClass_All",
                 restrictions = {
                     "GeoRestriction": {
                         "RestrictionType": "none",
                         "Quantity": 0,
                         "Items": []
                     }
                 },
                 webACLID="",
                 httpVersion="http2",
                 enableIPV6=True,
                 bCertificate=None,
                 sslSupportedMethod="sni-only",
                 minCertProtocolVersion="TLSv1.1_2016"):
        """
        Initialize Object
        Args
          For detailed description see:
            CloudFront.Client.create_distribution
            http://boto3.readthedocs.io/en/latest/reference/services/cloudfront.html#CloudFront.Client.create_distribution_with_tags

          name:
            Distribution name
          bOriginList:
            List of Beblsoft Cloud Front Origin Objects
            Note: bOriginList[0].domainName must be unique to the distribution!!
          bDefaultCacheBehavior:
            Default Beblsoft Cloud Front Cache Behavior Object
          defaultRootObject:
            Object that CloudFront requests from origin
            Ex. "index.html"
          bCacheBehaviorList:
            List of Beblsoft Cloud Front Cache Behavior Objects
          bErrorResponseList:
            List of Beblsoft Cloud Front Error Response Objects
          cNameList:
            List of CNAMEs to associate with distribution
            Ex. ["example.com", "www.example.com"]
          comment:
            Any comments to include in distribution
          dLogging:
            A complex type that controls whether access logs are written for
            the distribution.
          priceClass:
            Price class corresponding to the max price you want to pay for service
            One of: "PriceClass_100" | "PriceClass_200" | "PriceClass_All"
          viewerCertificate:
            Specify security configuration
          restrictions:
            A complex type that identifies ways in which you want to restrict
            distribution of your content.
          httpVersion:
            Specify the max http version that viewers can communicate
            with cloud front
            One of: "http1.1" | "http2"
          enableIPV6:
            If True, enable IP V6
          bCertificate:
            Beblsoft ACM Certificate Object
            Ex. cert that covers "*.quotablejoy.com"
          sslSupportedMethod:
            One of: 'sni-only'|'vip'
          minCertProtocolVersion:
            One of: 'SSLv3'|'TLSv1'|'TLSv1_2016'|'TLSv1.1_2016'|'TLSv1.2_2018',
        """
        # Name
        self.name                   = name
        self.nameTag                = {"Key": "name",
                                       "Value": self.name}

        # Origin
        self.bOriginList            = bOriginList

        # Cache Behaviors
        self.bDefaultCacheBehavior  = bDefaultCacheBehavior
        self.defaultRootObject      = defaultRootObject
        self.bCacheBehaviorList     = bCacheBehaviorList
        if not self.bCacheBehaviorList:
            self.bCacheBehaviorList = []
        self.bDefaultOrigin         = self.bDefaultCacheBehavior.bOrigin

        # Error Response
        self.bErrorResponseList     = bErrorResponseList
        if not self.bErrorResponseList:
            self.bErrorResponseList = []

        # CNames
        self.cNameList              = cNameList
        if not self.cNameList:
            self.cNameList          = []

        # Misc Fields
        self.comment                = comment
        if not self.comment:
            self.comment            = ""
        self.logging                = dLogging
        self.priceClass             = priceClass
        self.restrictions           = restrictions
        self.webACLID               = webACLID
        self.httpVersion            = httpVersion
        self.enableIPV6             = enableIPV6

        # Viewer Certificate
        self.bCertificate           = bCertificate
        self.sslSupportedMethod     = sslSupportedMethod
        self.minCertProtocolVersion = minCertProtocolVersion

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ------------------------ PROPERTIES ----------------------------------- #
    @property
    def exists(self):
        """
        Return True if the distribution exists
        """
        meta = self.__getMetadata()
        return (meta != None)

    @property
    def id(self):
        """
        Return distribution id
        """
        distID   = None
        meta = self.__getMetadata()
        if meta:
            distID = meta["Distribution"]["Id"]
        return distID

    @property
    def arn(self):
        """
        Return distribution arn
        """
        arn  = None
        meta = self.__getMetadata()
        if meta:
            arn = meta["Distribution"]["ARN"]
        return arn

    @property
    def domainName(self):
        """
        Return distribution domain name
        """
        dn   = None
        meta = self.__getMetadata()
        if meta:
            dn = meta["Distribution"]["DomainName"]
        return dn

    @property
    def status(self):
        """
        Return distribution status
        One of: "InProgress" | "Deployed"
        """
        status = None
        meta = self.__getMetadata()
        if meta:
            status = meta["Distribution"]["Status"]
        return status

    @property
    def enabled(self):
        """
        Return enabled status
        """
        enabled = None
        meta = self.__getMetadata()
        if meta:
            enabled = meta["Distribution"]["DistributionConfig"]["Enabled"]
        return enabled

    @property
    def viewerCertificate(self):
        """
        Return viewer certificate
        """
        vc = None
        if self.bCertificate:
            vc = {
                "CloudFrontDefaultCertificate": False,
                "ACMCertificateArn": self.bCertificate.arn,
                "SSLSupportMethod": self.sslSupportedMethod,
                "MinimumProtocolVersion": self.minCertProtocolVersion,
                "CertificateSource": "acm"
            }
        else:
            vc = {
                "CloudFrontDefaultCertificate": True,
                "MinimumProtocolVersion": "TLSv1"
            }
        return vc

    # ------------------------ CREATE --------------------------------------- #
    @logFunc()
    def create(self, callerReference="Created at {}".format(datetime.now()),
               delayS=5, maxAttempts=800):
        """
        Create distribution
        Args
          callerReference:
            A unique value that ensures that the request can't be replayed
          delayS:
            Waiter config: Amount of seconds to wait inbetween attempts
          maxAttempts:
            Waiter config: Number of attempts to be made
        """
        if self.exists:
            logger.info("{} Already exists".format(self))
        else:
            distConfig = {
                "CallerReference": callerReference,
                "Aliases": {
                    "Quantity": len(self.cNameList),
                    "Items": self.cNameList
                },
                "DefaultRootObject": self.defaultRootObject,
                "Origins": {
                    "Quantity": len(self.bOriginList),
                    "Items": [bO.distributionConfig for bO in self.bOriginList]
                },
                "DefaultCacheBehavior": {
                    "TargetOriginId": self.bDefaultOrigin.id,
                    "ForwardedValues": self.bDefaultCacheBehavior.forwardedValues,
                    "TrustedSigners": self.bDefaultCacheBehavior.trustedSigners,
                    "ViewerProtocolPolicy": self.bDefaultCacheBehavior.viewerProtocolPolicy,
                    "AllowedMethods": self.bDefaultCacheBehavior.allowedMethods,
                    "SmoothStreaming": self.bDefaultCacheBehavior.smoothStreaming,
                    "MinTTL": self.bDefaultCacheBehavior.minTTL,
                    "MaxTTL": self.bDefaultCacheBehavior.maxTTL,
                    "DefaultTTL": self.bDefaultCacheBehavior.defaultTTL,
                    "Compress": self.bDefaultCacheBehavior.compress,
                    "LambdaFunctionAssociations": self.bDefaultCacheBehavior.lambdaFunctionAssociations
                },
                "CacheBehaviors": {
                    "Quantity": len(self.bCacheBehaviorList),
                    "Items": [{
                        "TargetOriginId": bCB.bOrigin.id,
                        "PathPattern": bCB.pathPattern,
                        "ForwardedValues": bCB.forwardedValues,
                        "TrustedSigners": bCB.trustedSigners,
                        "ViewerProtocolPolicy": bCB.viewerProtocolPolicy,
                        "AllowedMethods": bCB.allowedMethods,
                        "SmoothStreaming": bCB.smoothStreaming,
                        "MinTTL": bCB.minTTL,
                        "MaxTTL": bCB.maxTTL,
                        "DefaultTTL": bCB.defaultTTL,
                        "Compress": bCB.compress,
                        "LambdaFunctionAssociations": bCB.lambdaFunctionAssociations
                    } for bCB in self.bCacheBehaviorList]
                },
                "CustomErrorResponses": {
                    "Quantity": len(self.bErrorResponseList),
                    "Items": [bER.distributionConfig for bER in self.bErrorResponseList]
                },
                "Comment": self.comment,
                "Logging": self.logging,
                "PriceClass": self.priceClass,
                "Enabled": True,
                "ViewerCertificate": self.viewerCertificate,
                "Restrictions": self.restrictions,
                "WebACLId": self.webACLID,
                "HttpVersion": self.httpVersion,
                "IsIPV6Enabled": self.enableIPV6,
            }

            distConfigWithTags = {
                "DistributionConfig": distConfig,
                "Tags": {
                    "Items": [self.nameTag]
                }
            }

            resp = cfClient.create_distribution_with_tags(
                DistributionConfigWithTags=distConfigWithTags)
            logger.info("{} Creation request issued".format(self))
            distID = resp.get("Distribution").get("Id")
            waiter = cfClient.get_waiter('distribution_deployed')
            waiter.wait(Id=distID,
                        WaiterConfig={"Delay": delayS, "MaxAttempts": maxAttempts})
            logger.info("{} Created".format(self))

    # ------------------------ DISABLE/DELETE ------------------------------- #
    @logFunc()
    def disable(self):
        """
        Disable distribution
        Returns
          eTag returned after disable or None
        """
        eTag = None
        if self.exists:
            distMeta   = self.__getMetadata()
            distConfig = distMeta["Distribution"]["DistributionConfig"]
            distConfig["Enabled"] = False
            resp = cfClient.update_distribution(DistributionConfig=distConfig,
                                                Id=distMeta["Distribution"]["Id"],
                                                IfMatch=distMeta["ETag"])
            eTag = resp["ETag"]
            self.waitUpdateInProgress(requestType="disable")
        else:
            logger.info("{} Doesn't exist".format(self))
        return eTag

    @logFunc()
    def delete(self):
        """
        Delete distribution
        """
        if self.exists:
            eTag = self.disable()
            cfClient.delete_distribution(Id=self.id, IfMatch=eTag)
            logger.info("{} Deleted".format(self))
        else:
            logger.info("{} Never existed".format(self))

    # ------------------------ UPDATE --------------------------------------- #
    @logFunc()
    def update(self, allobjs=False, vc=False):
        """
        Update distribution
        """
        if self.exists:
            if vc or allobjs:
                self.updateViewerCertificate()
        else:
            logger.info("{} Doesn't exist".format(self))

    @logFunc()
    def updateViewerCertificate(self):
        """
        Update viewer certificate
        """
        distMeta   = self.__getMetadata()
        distConfig = distMeta["Distribution"]["DistributionConfig"]
        distConfig["ViewerCertificate"] = self.viewerCertificate
        cfClient.update_distribution(DistributionConfig=distConfig,
                                     Id=distMeta["Distribution"]["Id"],
                                     IfMatch=distMeta["ETag"])
        self.waitUpdateInProgress(requestType="ViewerCertificate")

    @logFunc()
    def waitUpdateInProgress(self, requestType="N/A", delayS=60, maxAttempts=100):
        """
        Wait for update to complete
        Args
          requestType:
            Request Type
            Ex. "disable"
          delayS:
            Waiter config: Amount of seconds to wait inbetween attempts
          maxAttempts:
            Waiter config: Number of attempts to be made
        """
        attempts = 0
        logger.info("{} {} request issued".format(self, requestType))
        while self.status == "InProgress":
            attempts += 1
            if attempts >= maxAttempts:
                raise BeblsoftError(msg="{} still InProgress after {} attempts".format(self, attempts))
            time.sleep(delayS)
        logger.info("{} {} request complete".format(self, requestType))

    # ------------------------ DESCRIBE ------------------------------------- #
    @logFunc()
    def describe(self):
        """
        Describe Cloud Front Distribution
        """
        meta = self.__getMetadata()
        logger.info("{} Info:\n{}".format(self, pprint.pformat(meta)))

    # ------------------------ INVALIDATE ----------------------------------- #
    @logFunc()
    def invalidate(self, pathList,
                   callerReference="Invalidated at {}".format(datetime.now()),
                   delayS=5, maxAttempts=400):
        """
        Invalidate all of the paths in the path list
        Args
          pathList:
            List of paths to invalidate
          callerReference:
            A unique value that ensures that the request can't be replayed
          delayS:
            Waiter config: Amount of seconds to wait inbetween attempts
          maxAttempts:
            Waiter config: Number of attempts to be made
        """
        if self.exists:
            distId = self.id
            resp   = cfClient.create_invalidation(
                DistributionId=distId, InvalidationBatch= {
                    "Paths": {
                        "Quantity": len(pathList),
                        "Items": pathList
                    },
                    "CallerReference": callerReference
                })
            logger.info("{} Invalidation request on {} issued".format(self, pathList))
            invalID = resp.get("Invalidation").get("Id")
            waiter  = cfClient.get_waiter("invalidation_completed")
            waiter.wait(DistributionId=distId, Id=invalID, WaiterConfig={"Delay": delayS, "MaxAttempts": maxAttempts})
            logger.info("{} Invalidation request on {} complete".format(self, pathList))
        else:
            logger.info("{} Doesn't exist".format(self))

    # ------------------------ INTERNAL ------------------------------------- #
    @logFunc()
    def __getMetadata(self):
        """
        Return distribution metadata

        Reference:
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudfront.html#CloudFront.Client.list_tags_for_resource
        """
        distMeta = None
        # Iterate over all distributions, looking for a matching distribution
        for curDistMeta in BeblsoftCloudFrontDistribution.__getAllDistributionList():
            # New Method:
            # Match based on self.bOriginList[0].domainName
            curDistID           = curDistMeta["Id"]
            curMatch            = False
            curDistID           = curDistMeta["Id"]
            curOriginDomainList = [origin["DomainName"] for origin in curDistMeta["Origins"]["Items"]]
            curMatch            = self.bOriginList[0].domainName in curOriginDomainList
            if curMatch:
                distMeta        = cfClient.get_distribution(Id=curDistID)
                break

            # Old Method:
            # Match based on the distribution's name tag
            # This results in lots of calls to list_tags_for_resource and generates:
            #  botocore.exceptions.ClientError: An error occurred (Throttling) when calling the
            #  ListTagsForResource operation (reached max retries: 4): Rate exceeded
            #
            # Old implementation below:
            # --------
            # curMatch         = False
            # curDistID        = curDistMeta["Id"]
            # curDistARN       = curDistMeta["ARN"]
            # resp             = cfClient.list_tags_for_resource(Resource=curDistARN)
            # curDistTags      = resp["Tags"]["Items"]
            # for curDistTag in curDistTags:
            #     kMatch       = curDistTag["Key"] == self.nameTag["Key"]
            #     nMatch       = curDistTag["Value"] == self.nameTag["Value"]
            #     curMatch     = kMatch and nMatch
            #     if curMatch:
            #         distMeta = cfClient.get_distribution(Id=curDistID)
            # if curMatch:
            #     break
        return distMeta

    @staticmethod
    def __getAllDistributionList():
        """
        Get all distribution metadata in region

        Reference:
          https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudfront.html#CloudFront.Client.list_distributions
        """
        distList = []
        resp     = cfClient.list_distributions()
        distListMeta = resp.get("DistributionList", None)
        if distListMeta and distListMeta.get("Quantity") > 0:
            distList = distListMeta.get("Items")
        return distList
