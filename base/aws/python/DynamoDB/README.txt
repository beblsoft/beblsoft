OVERVIEW
===============================================================================
AWS DynamoDB Documentation
- Description
  * Amazon DynamoDB is a fast and flexible NoSQL database service for all
    applications that need consistent, single-digit millisecond latency at any
    scale. It is a fully managed cloud database and supports both document and
    key-value store models.
  * Its flexible data model, reliable performance, and automatic scaling of
    throughput capacity make it a great fit for mobile, web, gaming, ad tech,
    IoT, and many other applications.
  * Amazon DynamoDB Accelerator (DAX) is a fully managed, highly available,
    in-memory cache that can reduce DynamoDB response times from milliseconds to
    microseconds, even at millions of requests per second.
- Relevant URLS
  * Product Details                 : https://aws.amazon.com/dynamodb/details/
  * Install Local DynamoDB          : https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html
  * Many-Many Relationships         : https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/bp-adjacency-graphs.html
  * Relational Modeling             : https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/bp-modeling-nosql-B.html


PRODUCT DEFAILS
===============================================================================
- DynamoDB Accelerator (DAX)        : Fully managed, highly available, in-memory cache
                                      for DynamoDB. Latencies from milliseconds
                                      to microseconds
- Global Tables                     : Fully managed, multi-region, and multi-master
                                      database that provides fast, local, read, and write
                                      performance for global applications
- Backup and Restore                : On-Demand backups for DynamoDB tables for
                                      data archiving.
- Encryption at Rest                : Secure data by using AWS Key Management Service
                                      (KMS). Seamless to users.
- Seamless Scaling                  : Seamless, automatic scaling of throughput
                                      and storage scaling via APIs
- Key-value Data Model Support      : Key-value data structures. Each item(row)
                                      is a key-value pair. Can also query non-primary
                                      key attributes using Global and Local
                                      Secondary Indexes
- Document Data Model Support       : Supports storing, querying, and updating
                                      documents. Store JSON directly in DynamoDB tables.
- High Availability                 : Highly available with automatic and synchronous
                                      data replication across three facilities in an AWS
                                      region.
- Develop Locally, Scale Globally   : Download local Amazon DynamoDB to test applications
                                      then seamlessly scal to the cloud
- Secondary Indexes                 : Efficiently query on any attribute (column)
                                      using secondary indexes. Can create and delete
                                      secondary indexes from your table at any time
- Streams                           : Time ordered sequence of item-level changes
                                      in any DynamoDB table
- Cross-region Replication          : Automatically replicates DynamoDB tables across
                                      AWS regions
- Triggers                          : Integration with AWS Lambda to execute custom
                                      functions whenever and item changes in a DynamoDB table
- Strong Consistency, Atomic
  Counters                          : Unlike many non-relational databases, DynamoDB
                                      makes development easier by allowing strong
                                      consistency on reads and atomic ops on numerical
                                      attributes


CORE COMPONENTS
===============================================================================
- Description
  * In DynamoDB, tables, items, and attributes are the core components that you
    work with. A table is a collection of items, and each item is a collection of attributes.
- Table                             : Collection of data
- Items                             : Each table contains zero or more items.
                                      An item is a group of attributes is uniquely identifiable
                                      amoung all the other itmes
                                      There is no limit to the number of items that can be stored
                                      in a table
- Attributes                        : Each item is composed of one or more attributes.
                                      An attribute is a fundamental data element, something
                                      that does not need to be broken down any further
- Example People Table              : Each item has a unique ID, the primary key, PersonID.
                                      Other than primary key, the table is schemaless.
                                      Address is a nested attribute. Dynamo supports 32 levels of nesting.
                                      -----------------------------------------
                                      | ------------------------------------- |
                                      | | {                                 | |
                                      | |   "PersonID": 101,                | |
                                      | |   "LastName": "Smith",            | |
                                      | |   "FirstName": "Fred",            | |
                                      | |   "Phone": "666-4321"             | |
                                      | | }                                 | |
                                      | ------------------------------------- |
                                      |                                       |
                                      | ------------------------------------- |
                                      | | {                                 | |
                                      | |   "PersonID": 102,                | |
                                      | |   "LastName": "Jones",            | |
                                      | |   "FirstName": "Mary",            | |
                                      | |   "Address": {                    | |
                                      | |     "Street": "123 Main"          | |
                                      | |     "City": "Anytown"             | |
                                      | |     "State": "OH"                 | |
                                      | |     "ZIPCode": "12345"            | |
                                      | |   }                               | |
                                      | | }                                 | |
                                      | ------------------------------------- |
                                      -----------------------------------------
- Primary Keys -------------------- : When you create a table, you must specify the
                                      primary key of the table. The primary key uniquely
                                      identifies each item in the table so no two
                                      items can have the same key
                                    : Must be scalar: string, number, or binary
  * Parition Key                    : A simple primary key, composed of one attribute
                                      DynamoDB uses the partition key value as input into
                                      an internal hash functon that determines the key's
                                      physical storage
  * Partition Key and Sort Key      : Referred to as a composite primary key.
                                      Composed of two attributes (partition key, sort key)
                                      All items with the same partition key are
                                      stored together, in sorted order by sort key value.
- Secondary Indexes --------------- : Can create one or more secondary indexes on a table
                                      A secondary index lets you query the data using an alternate key
                                      in addition to queryies agains the primary key
                                    : Can define up to 5 global secondary indexes
                                      and 5 local secondary indexes per table.
  * Global Secondary Index          : An index with a partition key and sort key that
                                      can be different from those on the table.
                                      GSI don't have to be unique.
  * Local Secondary Index           : An index that has the same parition key as the table,
                                      but a different sort key. Must be less than 10GB in size.
  * Limitations                     : Each table is limited to a maxiumum of 5 global
                                      secondary indexes and 5 local secondary indexes
- DynamoDB Streams ---------------- : Optional feature that captures change modification
                                      events in tables.
                                    : Events: new item added, item updated, item deleted
                                    : Stream records can be used to trigger AWS lambda functions


API
===============================================================================
- Control Plane ------------------- : Create and manage DynamoDB tables
  * CreateTable                     : Create a new table.
                                      Create one or more secondary indexes
  * DescribeTable                   : Return information about the table, such as
                                      primary key schema, throughput settings, etc.
  * ListTables                      : Return names of all tables
  * UpdateTable                     : Modify the settings of a table or indexes
  * DeleteTable                     : Remove table and all of its dependent objects
- Data Plane ---------------------- : CRUD actions on table
  * PutItem                         : Writes a single item to a table
  * BatchWriteItem                  : Write upto 25 items
  * GetItem                         : Retrieve a single item from a table
  * BatchGetItem                    : Retrieve up to 100 items
  * Query                           : Retrieves all items that have a specific parition key
                                      Optionally apply condition to the sort key values
                                      Can use this operation on an index, provided index
                                      has a partition key and a sort key
  * Scan                            : Retreive all items in the specified table or index
                                      Optionally apply filtering condition
  * UpdateItem                      : Modify one or more item attributes
                                      Optionaly implement an atomic counter
  * DeleteItem                      : Delete single item from table
  * BatchWriteItem                  : Delte upto 25 items from one or more tables
- Streams ------------------------- : Enable or disable stream on a table
  * ListStreams                     : Return list of all streams
  * DescribeStreams                 : Return information about a stream
  * GetShardIterator                : Return a shard iterator
  * GetRecords                      : Retreive one or more stream records


NAMING RULES AND DATA TYPES
===============================================================================
- Naming Rules
  * All names must be encoded in UTF-8, case-sensitive
  * Tables and indexes must be between 3 and 255 characacters
  * Allowed characters: a-z, A-Z, 0-9, _, -, .
  * Attribute names must be between 1 and 255 characters
- Data Types
  * Scalar                          : Represents exactly one value: number string, binary, Boolean, null
  * Document                        : Complex structure with nested attributes. list, map
  * Set                             : Multiple scalar values. String set, number set, binary set
- Scalar Types
  * String                          : Unicode UTF-8. 0 < length < 400 KB
                                      Max partition key = 2048
                                      Max sort key = 1024
  * Number                          : Positive, negative, or 0
                                      38 digits of precision
                                      Positive range 1E-130     to 9.99E+125
                                      Negative range -9.99E+125 to -1E+130
  * Binary                          : Raw unsigned bytes
                                      0 < length < 400 KB
                                      Must encode binary values in base64-encoded format
                                      before sending to Dyamo
  * Boolean                         : true or false
  * Null                            : Undefined state
- Document Types                    : Nested structures
                                      Max 32 levels deep
  * Lists                           : [...]
                                      Similar to JSON arrray
                                      No restrictions on data types store in list element
                                      Favorite Things: ["Cookies", 3.14159]
  * Map                             : Unordered collection of name-value pairs
                                      {
                                        Day: "Monday",
                                        UnreadEmails: 42,
                                        ItemsOnMyDesk: [
                                            "Coffee Cup",
                                            "Telephone",
                                            {
                                                Pens: { Quantity : 3},
                                                Pencils: { Quantity : 2},
                                                Erasers: { Quantity : 1}
                                            }
                                        ]
                                      }
- Set Types                         : Sets of number, string, or binary values
                                      All values must be the same type
                                      ["Black", "Green", "Red"]


READ CONSISTENCY, THROUGHPUT CAPACITY, PARTITIONS
===============================================================================
- Consistency Models ----------------
- Eventual Consistency              : When you read from a DynamoDB table,
                                      the response might not reflect the results of a recently
                                      completed write operation
- Strong Consistent Reads           : When you request a strongly consistent read, DynamoDB
                                      returns a response with the most up-to-date data,
                                      reflecting the updates from all prior write operations
                                      that were successful. A strongly consistent read might not
                                      be available if there is a network delay or outage.
- Throughput Capacity ---------------
- Read Capacity Unit                : Represents one strongly consistnet read per second or two
                                      eventually consistent reads per second
- Write Capacity Unit               : One write capcapity unit is one write per second upto
                                      1 KB in size
- Exceeding capacity                : Dynamo will throttle requests if capacity exceeded
                                      Request will fail with HTTP 400 code and
                                      ProvisionedThroughputExceededException
- Auto Scaling                      : Define a range (upper and lower limits) for read and
                                      write capacity units
                                      DynamoDB auto scales throughput within range
- Reserved Capacity                 : Pay one-time upfront fee and commit to a minimum usage
                                      level for a period of time. Realize significant cost savings
- Data Partitioning -----------------
- Parition                          : Allocation of storage for a table
                                      Backed by Solid-State Drives (SSDs)
                                      Auto replicated across AZs in region
                                      More paritions are allocatd in throughput settings change
                                      or an existing partition fills to capacity
- Data Distribution:
  Partition Key                     : DynamoDB hashes the parition key to determine
                                      which partition the data should reside
- Data Distribution:
  Partition Key and Sort Key        : DynamoDB stores all values with the same parition key
                                      on the same partition, ordered by sort key value


BEST PRACTICES
===============================================================================
- NoSQL Design for DynamoDB ---------
  RDBMS                             : Data can be queried flexibly, but queries don't scale
                                      Design for normalization
  NoSQL                             : Design schema to make the most common and important
                                      queries as fast as possible
  NoSQL Design                      : Before desiging know the questions that schema will answer
                                      Maintain as few tables as possible. Most only require one table
  Approaching NoSQL Design          : Understand access patterns
  * Data Size                       : How much data will be stored
  * Data Shape                      : Data is shaped as it will be queried
  * Data Velocity                   : Understand peak loads
  * Keep related data together      : Keep related data together
  * Use sort order                  : Related items can be grouped and queried efficiently if their design
                                      causes them to sort together
  * Distribute queries              : Avoid hot spots
  * Use global secondary indexes    : Enables different queries than your main table can support
- Partition Key Design -------------: Design for uniform access to all partition keys
  Burst Capacity                    : Whenever you aren't using partition's full throughput DynamoDB
                                      reserves a portion of unused capacity for later bursts
  Adaptive Capacity                 : Adaptive capacity enables your application to continue reading
                                      and writing to hot partitions without being throttled,
                                      provided that traffic does not exceed your table’s total
                                      provisioned capacity or the partition maximum capacity.
  Distributing Workloads            : More distinct partition key values that your workload accesses,
                                      the more those requests will be spread across the partitioned
                                      space. In general, you will use your provisioned throughput
                                      more efficiently as the ratio of partition key values accessed
                                      to the total number of partition key values increases.
  * Ex good parition key            : User ID
  * Ex bad parition key             : Status Code
  Distribute Workloads Evenly       : Add random number to partition keys
  Distribute writes on upload       : Upload to multiple partitions simultaneously
- Sort Key Design -------------------
  Well designed sort keys           : Gather information together in one place where
                                      it can be queried efficiently
                                      Composite let you define hierarchical (one-to-many) relationships
                                      in the data
                                      [country]#[region]#[state]#[county]#[city]#[neighborhood]
  Sort keys for Version Control     : For each new item, create two copies of the item.
                                      One stored in (v0_) and one in (vN_).
                                      Latest version is always stored in v0_ and history is
                                      shown in vi_
- Secondary Indexes --------------- : Secondary indexes are often essential to support the query
                                      patterns that your application requires. At the same time,
                                      overusing secondary indexes or using them inefficiently can add
                                      cost and reduce performance unnecessarily.
  Efficient Usage                   : Don't crate secondary indexes on attributes you don't query often
                                      Avoid indexing tables that experience heavy write activity
  Choose Projections Carefully      : Balance the need to keep indexes as small as possible against the
                                      the need to keep fetches to a minimum
- Relational Modeling ---------------
  Modeling DynamoDB                 : NoSQL design requires a different mindset than RDBMS design.
                                      For an RDBMS, you can create a normalized data model without thinking
                                      about access patterns. You can then extend it later when new
                                      questions and query requirements arise.
                                      For DynamoDB, by contrast, you shouldn't start designing your
                                      schema until you know the questions it needs to answer.
                                      Understanding the business problems and the application use cases
                                      up front is absolutely essential.
  Common Approach                   : Identify application layer entities and use denormalization
                                      and composite key aggregation to reduce query complexity


EXAMPLE: SPARSE INDEXES
===============================================================================
- Description
  * For any item in a table, DynamoDB writes a corresponding index entry only
    if the index sort key value is present
  * If the sort key doesn't exist, the item is sparse

- Use Case 1: Order tracking
  * Table (Partition Key: CustomerID, Sort Key:OrderID)
  * To track open orders, you can insert a Boolean attribute named isOpen in
    order items that have not already shipped. Then when the order ships, you
    can delete the attribute. If you then create an index on CustomerId (partition key)
    and isOpen (sort key), only those orders with isOpen defined appear in it.
    When you have thousands of orders of which only a small number are open,
    it's faster and less expensive to query that index for open orders than
    to scan the entire table.

- Use Case 2: Game Champ tracking using secondary indexes
  * Table (Partition Key: Player_ID, Sort Key: Game_ID)
    --------------------------------------------------------------------
    | Primary Key         | Data Attributes                            |
    -------------------------------------------------------------------|
    | Parition  | Sort    |                                            |
    | Player_ID | Game_ID |  Attribute 1 | Attribute 2 | Attribute 3   |
    --------------------------------------------------------------------
    |  Rick     | Game_1  |  game score  | date of game|               |
    |           |         |  Score=36.75 | 2017-11-14  |               |
    |           | Game_2  |  game score  | date of game|               |
    |           |         |  Score=72    | 2017-11-14  |               |
    |           | Game_3  |  game score  | date of game|type of award  |
    |           |         |  Score=135   | 2017-11-14  |award=Champ    |
    --------------------------------------------------------------------
    |  Padma    | Game_4  |  game score  | date of game|               |
    |           |         |  Score=25.75 | 2017-11-14  |               |
    |           | Game_5  |  game score  | date of game|type of award  |
    |           |         |  Score=147   | 2017-11-14  |award=champ    |
    |           | Game_6  |  game score  | date of game|type of award  |
    |           |         |  Score=169   | 2017-11-14  |award=Champ    |
    --------------------------------------------------------------------

  * GSI (Partition Key: Award)
    --------------------------------------------------------------------
    | Primary Key| Projected Attributes                                |
    -------------------------------------------------------------------|
    | Parition   |                                                     |
    | Award      | Player_ID | Game_ID      | Score       | Date       |
    --------------------------------------------------------------------
    |  Champ     | Rick      | Game_3       | 135         | 2017-11-14 |
    |  Champ     | Padma     | Game_5       | 147         | 2017-11-14 |
    |  Champ     | Padma     | Game_6       | 169         | 2017-11-14 |
    --------------------------------------------------------------------


EXAMPLE: AGGREGATION
===============================================================================
- Description
  * Mainaining near real-time aggregations and key metrics on top of rapidly
    changing data is becoming more and more valuable

- Use Case: Music Library Tracking Total Song Downloads over time
  * Table (Partition Key: Song_ID, Sort Key: Overloaded)
    --------------------------------------------------------------------------
    | Primary Key               | Data Attributes                            |
    -------------------------------------------------------------------------|
    | Parition  | Sort          |                                            |
    | Song_ID   | Overload      |  Attribute 1 | Attribute 2 | Attribute 3   |
    --------------------------------------------------------------------------
    |  Song-129 | Details       |Title=WildLove|Argist=ArgyBo|Downloads=15555|
    |           |               |              |             |               |
    |  Song-129 | Month-2018-01 |Month=2018-01 |MonthTotal=12|               |
    |           |               |              |             |               |
    |  Song-129 | Did-23948723  |Time=<tsamp1> |             |               |
    |           |               |              |             |               |
    |  Song-129 | Did-23423423  |Time=<tsamp1> |             |               |
    --------------------------------------------------------------------------

  * GSI (Partition Key:Month, Sort Key: MonthTotal)
    --------------------------------------------------------------------
    | Primary Key            | Projected Attributes                    |
    -------------------------------------------------------------------|
    | Parition   | Sort      |                                         |
    | Month      | MonthTotal| Song_ID                                 |
    --------------------------------------------------------------------
    |  2018-01   | 12        | Song-129                                |
    --------------------------------------------------------------------


EXAMPLE: GSI OVERLOADING
===============================================================================
- Description
  * Although Amazon DynamoDB has a limit of five global secondary indexes per table,
    in practice, you can index across far more than five data fields. As opposed
    to a table in a relational database management system (RDBMS), in which the
    schema is uniform, a table in DynamoDB can hold many different types of
    data items at one time. In addition, the same attribute in different items
    can contain entirely different types of information

- Use Case: HR System
  * The data attribute is common to all items, but is overloaded allowing for
    various queries

  * Table (Partition Key: Player_ID, Sort Key: Game_ID)
    ----------------------------------------------------------------------
    | Primary Key                 | Data Attributes                      |
    ---------------------------------------------------------------------|
    | Parition  | Sort            |                                      |
    | Object    | Overloaded      |  Attribute 1   | ...                 |
    ----------------------------------------------------------------------
    |  HR-974   | Employee_Name   | Data=JohnSmith | ...                 |
    |           | YYYY-Q1         | Data=$5,477    | Name=JohnSmith      |
    |           | HR_confidential | Data=2008-11-08| Name=JohnSmith      |
    |           | Warehouse_01    | Data=JohnSmith |                     |
    |           | v0_Job_title    | Data=Operator-1|                     |
    |           | v1_Job_title    | Data=Operator-2|                     |
    |           | v2_Job_title    | Data=Supervisor|                     |
    |           | Curr_Job_title  | Data=Supervisor|                     |
    ----------------------------------------------------------------------
  * GSI (PartitionKey:TableSortKey, SortKey: Table Data Attribute)
  * Allowable queries
    Look up employee by name
    Find all employees working in a particular warehouse
    Find list of all recent hires by querying off HR_confidential



