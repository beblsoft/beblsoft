#!/usr/bin/python3.6
"""
NAME:
 movies.py

DESCRIPTION
 Demonstrate DynamoDB functionality with Movie Example

URLS
 Samples Taken From : https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.html
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import random
import json
import decimal
import pprint
import boto3
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError
import click


# ----------------------- GLOBALS ------------------------------------------- #
logger    = logging.getLogger(__file__)
dDBRes    = boto3.resource('dynamodb', region_name='us-east-1',
                           endpoint_url="http://localhost:8000")
dDBClient = boto3.client("dynamodb", region_name='us-east-1',
                         endpoint_url="http://localhost:8000")
tableName = "Movies"
dataFile  = "movieDataLarge.json"


# ------------------------ DECIMAL ENCODER ---------------------------------- #
class DecimalEncoder(json.JSONEncoder):  # pylint: disable=C0111

    def default(self, o):  # pylint: disable=E0202
        if isinstance(o, decimal.Decimal):
            if abs(o) % 1 > 0:  # pylint: disable=R1705
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
def cli():
    """Lambda Function Command Line Interface"""
    logging.basicConfig(format="%(asctime)-15s: %(levelname)s %(message)s",
                        level=logging.INFO)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('boto3').setLevel(logging.CRITICAL)


# ----------------------- TABLE COMMANDS ------------------------------------ #
@cli.command()
def table_create():
    """
    Create table
    """
    table = dDBRes.create_table(  #pylint: disable=E1101
        TableName=tableName,
        KeySchema=[{
            'AttributeName': 'year',
            'KeyType': 'HASH'  # Partition key
        }, {
            'AttributeName': 'title',
            'KeyType': 'RANGE'  # Sort key
        }],
        AttributeDefinitions=[{
            'AttributeName': 'year',
            'AttributeType': 'N'
        }, {
            'AttributeName': 'title',
            'AttributeType': 'S'
        }],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10
        }
    )
    print("Table status:", table.table_status)


@cli.command()
def table_delete():
    """
    Delete table
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    table.delete()


@cli.command()
def table_populate():
    """
    Populate table
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101

    with open(dataFile) as json_file:
        movies    = json.load(json_file, parse_float = decimal.Decimal)
        for movie in movies:
            year  = int(movie['year'])
            title = movie['title']
            info  = movie['info']

            print("Adding movie:", year, title)
            table.put_item(
                Item = {
                    'year': year,
                    'title': title,
                    'info': info,
                }
            )


@cli.command()
def table_describe():
    """
    Describe table
    """
    resp = dDBClient.describe_table(TableName=tableName)
    print(pprint.pformat(resp))


# ----------------------- ITEM COMMANDS ------------------------------------- #
@cli.command()
def item_create():
    """
    Create item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    response = table.put_item(
        Item={
            'year': year,
            'title': title,
            'info': {
                'plot': "Nothing happens at all.",
                'rating': random.randint(0, 9)
            }
        }
    )
    print("PutItem succeeded:")
    print(json.dumps(response, indent=4))


@cli.command()
def item_delete():
    """
    Delete item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    print("Attempting a conditional delete...")
    response = table.delete_item(
        Key={
            'year': year,
            'title': title
        }
    )
    print("DeleteItem succeeded:")
    print(json.dumps(response, indent=4, cls=DecimalEncoder))


@cli.command()
def item_cond_delete():
    """
    Conditionally Delete item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    print("Attempting a conditional delete...")
    try:
        response = table.delete_item(
            Key={
                'year': year,
                'title': title
            },
            ConditionExpression="info.rating <= :val",
            ExpressionAttributeValues= {
                ":val": decimal.Decimal(5)
            }
        )
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        print("DeleteItem succeeded:")
        print(json.dumps(response, indent=4, cls=DecimalEncoder))


@cli.command()
def item_read():
    """
    Read item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    try:
        response = table.get_item(
            Key={
                'year': year,
                'title': title
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        item = response['Item']
        print(item)
        print("GetItem succeeded:")
        print(json.dumps(item, indent=4, cls=DecimalEncoder))


@cli.command()
def item_update():
    """
    Update item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    response = table.update_item(
        Key={
            'year': year,
            'title': title
        },
        UpdateExpression="set info.rating = :r, info.plot=:p, info.actors=:a",
        ExpressionAttributeValues={
            ':r': decimal.Decimal(5.5),
            ':p': "Everything happens all at once.",
            ':a': ["Larry", "Moe", "Curly"]
        },
        ReturnValues="UPDATED_NEW"
    )
    print("UpdateItem succeeded:")
    print(json.dumps(response, indent=4, cls=DecimalEncoder))


@cli.command()
def item_cond_update():
    """
    Conditionally Update item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    # Conditional update (will fail)
    print("Attempting conditional update...")
    try:
        response = table.update_item(
            Key={
                'year': year,
                'title': title
            },
            UpdateExpression="remove info.actors[0]",
            ConditionExpression="size(info.actors) > :num",
            ExpressionAttributeValues={
                ':num': 3
            },
            ReturnValues="UPDATED_NEW"
        )
    except ClientError as e:
        if e.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(e.response['Error']['Message'])
        else:
            raise
    else:
        print("UpdateItem succeeded:")
        print(json.dumps(response, indent=4, cls=DecimalEncoder))


@cli.command()
def item_inc_atomic():
    """
    Increment item atomic
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    title = "The Big New Movie"
    year  = 2015

    response = table.update_item(
        Key={
            'year': year,
            'title': title
        },
        UpdateExpression="set info.rating = info.rating + :val",
        ExpressionAttributeValues={
            ':val': decimal.Decimal(1)
        },
        ReturnValues="UPDATED_NEW"
    )

    print("UpdateItem succeeded:")
    print(json.dumps(response, indent=4, cls=DecimalEncoder))


@cli.command()
def item_query():
    """
    Query for item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    print("Movies from 1992 - titles A-L, with genres and lead actor")
    response = table.query(
        ProjectionExpression="#yr, title, info.genres, info.actors[0]",
        # Expression Attribute Names for Projection Expression only.
        ExpressionAttributeNames={"#yr": "year"},
        KeyConditionExpression=Key('year').eq(1992) & Key('title').between('A', 'L')
    )

    for i in response[u'Items']:
        print(json.dumps(i, cls=DecimalEncoder))


@cli.command()
def item_scan():
    """
    Scan for item
    """
    table = dDBRes.Table(tableName) #pylint: disable=E1101
    fe    = Key('year').between(1950, 1959)
    pe    = "#yr, title, info.rating"
    ean   = {"#yr": "year", }

    response = table.scan(
        FilterExpression=fe,
        ProjectionExpression=pe,
        ExpressionAttributeNames=ean
    )

    for i in response['Items']:
        print(json.dumps(i, cls=DecimalEncoder))

    while 'LastEvaluatedKey' in response:
        response = table.scan(
            ProjectionExpression=pe,
            FilterExpression=fe,
            ExpressionAttributeNames= ean,
            ExclusiveStartKey=response['LastEvaluatedKey']
        )

    for i in response['Items']:
        print(json.dumps(i, cls=DecimalEncoder))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    cli()  # pylint: disable=E1120
