#!/usr/bin/env python3
"""
NAME:
 bCachedSecret.py

DESCRIPTION
 Beblsoft Cached Secret Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import json
import logging
from base.aws.python.SecretsManager.bSecret import BeblsoftSecret


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT CACHED SECRET ---------------------------- #
class BeblsoftCachedSecret(BeblsoftSecret):
    """
    Beblsoft Cached Secret

    Same functionality as Beblsoft Secret except secret properties are
    only read once and then cached in memory.
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize Object
        Args
          See BeblsoftSecret
        """
        super().__init__(*args, **kwargs)
        self.__propertyDict = None

    # --------------------- PROPERTIES -------------------------------------- #
    def __getattr__(self, prop):
        """
        Return property value
        Args
          prop:
            property value
            Ex. "username"
        Returns
          Property value store in secret string
        """
        if not self.__propertyDict:
            secretMeta = self.smClient.get_secret_value(SecretId=self.name)
            secretString = secretMeta.get("SecretString")
            self.__propertyDict = json.loads(secretString)
        return self.__propertyDict[prop]


# ----------------------- TEST HARNESS -------------------------------------- #
if __name__ == "__main__":
    bSecret1 = BeblsoftCachedSecret("SmecknTestSecret",
                                    properties=["RECAPTCHA_SITE_KEY", "RECAPTCHA_SECRET_KEY"])
    bSecret1.update()
    print("bSecret1 username:{}".format(bSecret1.RECAPTCHA_SITE_KEY))
    print("bSecret1 password:{}".format(bSecret1.RECAPTCHA_SECRET_KEY))
