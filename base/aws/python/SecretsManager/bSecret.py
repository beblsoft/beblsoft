#!/usr/bin/env python3
"""
NAME:
 bSecret.py

DESCRIPTION
 Beblsoft Secret Functionality
"""

# ------------------------ IMPORTS ------------------------------------------ #
import json
import logging
import pprint
import boto3
from botocore.exceptions import ClientError
from botocore.client import Config
from base.bebl.python.log.bLogFunc import logFunc
from base.aws.python.Common.Object.bAWS import BeblsoftAWSObject
from base.aws.python.EC2.bRegion import BeblsoftRegion


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT SECRET ----------------------------------- #
class BeblsoftSecret(BeblsoftAWSObject):
    """
    Beblsoft Secret

    Deployment Usage - Allow application setup code to easily manage secrets
      bSecret    = BeblsoftSecret(name="SmecknTestSecret")
      bSecret.create()
      # Fill in values in AWS Management console

    Client Usage - Allow application clients to quickly access secrets
      bSecret   = BeblsoftSecret(name="SmecknTestSecret")
      siteKey   = bSecret.SITE_KEY
      secretKey = bSecret.SECRET_KEY
    """

    def __init__(self, name, # pylint: disable=W0102
                 description=None, bKMSCustomerMasterKey=None,
                 recovWindowDays=30,
                 bRegion=BeblsoftRegion.getDefault(),
                 connectTimeoutS = 1,
                 readTimeoutS = 1):
        """
        Initialize Object
        Args
          name:
            Secret Name
            Ex. SmecknTestDatabase
          description:
            User provided description
          bKMSCustomerMasterKey:
            Beblsoft KMS Customer Master Key Object
          recovWindowDays:
            Number of days the Secrets manager waits before it deletes the secret
            Range is 7-30 inclusive
        """
        super().__init__()
        self.name                  = name
        if description:
            self.description       = description
        else:
            self.description       = "{} Secret".format(name)
        self.bKMSCustomerMasterKey = bKMSCustomerMasterKey
        self.recovWindowDays       = recovWindowDays
        self.bRegion               = bRegion
        self.smClientConfig        = Config(connect_timeout=connectTimeoutS,
                                            read_timeout=readTimeoutS)
        self.smClient              = boto3.client("secretsmanager",
                                                  region_name=self.bRegion.name,
                                                  config=self.smClientConfig)
        if self.bKMSCustomerMasterKey:
            assert(self.bKMSCustomerMasterKey.bRegion.name == self.bRegion.name)

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # --------------------- PROPERTIES -------------------------------------- #
    def __getattr__(self, prop):
        """
        Return property value
        Args
          prop:
            property value
            Ex. "username"
        Returns
          Property value store in secret string
        """
        secretMeta   = self.smClient.get_secret_value(SecretId=self.name)
        secretString = secretMeta.get("SecretString")
        propertyDict = json.loads(secretString)
        return propertyDict[prop]

    @property
    def arn(self):
        """
        Return secret arn
        """
        arn  = None
        meta = self._getMetadata()
        if meta:
            arn = meta.get("ARN", None)
        return arn

    # --------------------- VERBS ------------------------------------------- #
    @logFunc()
    def _create(self, **kwargs):
        """
        Create empty secret object
        """
        secretString = json.dumps({})
        kwargs = {}
        if self.bKMSCustomerMasterKey:
            kwargs["KmsKeyId"] = self.bKMSCustomerMasterKey.id
        self.smClient.create_secret(
            Name=self.name, Description=self.description, SecretString=secretString,
            **kwargs)

    @logFunc()
    def _delete(self, **kwargs):
        """
        Schedule object deletion
        """
        self.smClient.delete_secret(
            SecretId=self.name, RecoveryWindowInDays=self.recovWindowDays)

    @logFunc()
    def _restore(self, **kwargs):
        """
        Save object from pending deletion
        """
        self.smClient.restore_secret(SecretId=self.name)

    # ----------------------- METADATA -------------------------------------- #
    @logFunc()
    def _getMetadata(self):
        """
        Get object metadata
        Returns
        """
        meta = None
        try:
            meta = self.smClient.describe_secret(SecretId=self.name)
        except ClientError as e:
            code = e.response["Error"]["Code"]
            if code != "ResourceNotFoundException":
                logger.exception(pprint.pformat(e.__dict__))
                raise(e)
        return meta
