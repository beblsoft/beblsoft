OVERVIEW
===============================================================================
Amazon Web Services (AWS) Technology Documentation
- Relevant URLS
  * Home                            : https://aws.amazon.com/?nc2=h_lg
  * Products                        : https://aws.amazon.com/products/



SERVICE CATALOG
===============================================================================
- Compute ---------------------------
  * Elastic Cloud Compute (EC2)     : Virtual servers in the cloud
  * EC2 Auto Scaling                : Scale compute to meet demand
  * Elastic Container Service (ECS) : Run and manage Docker containers
  * Elastic Container Registry (ECR): Store and retrieve Docker images
  * Lightsail                       : Launch and manage virtual private servers
  * Batch                           : Run batch jobs at any scale
  * Elastic Beanstalk               : Run and manage web apps
  * Fargate                         : Run containers without managing servers or clusters
  * Lambda                          : Run code in response to events
  * Severless App Repository        : Discover, deploy, and publish serverless applications
  * VMWare Cloud on AWS             : Build a hybrid cloud without custom hardware
  * Outposts                        : AWS services on-premises

- Storage ---------------------------
  * S3                              : Scalable storage in the cloud
  * EBS                             : Block storage for EC2
  * Elastic File System             : Managed file storage for EC2
  * Glacier                         : Low-cost archive storage
  * Storage Gateway                 : Hybrid storage integration
  * Snowball                        : Petabyte-scale data transport
  * Snowball Edge                   : Petabyte-scale data transport with onboard compute
  * Snowmobile                      : Exabyte-scale data transport

- Database --------------------------
  * Amazon Aurora                   : High performance managed relational database
  * RDS                             : Managed relational database service for MySQL, PostgreSQL, Oracle, SQL Server, and Maria
  * DynamoDB                        : Managed NoSQL database
  * ElastiCache                     : In-memory cachiny system
  * Redshift                        : Fast, simple, cost-effective data warehousing
  * Neptune                         : Fully-managed graph database service
  * Database Migration Service      : Migrate databases with minimal downtime
  * Quantum Ledger Database         : Fully managed ledger database
  * Timestream                      : Fully managed time series database

- Network and Content Delivery ------
  * Virtual Private Cloud (VPC)     : Isolated cloud resources
  * VPC PrivateLink                 : Securely access services hosted on AWS
  * CloudFront                      : Global content delivery network
  * Route 53                        : Scalable Domain Name System
  * API Gateway                     : Build, deploy, and manage APIs
  * Direct Connect                  : Dedicated network connection to AWS
  * Elastic Load Balancing          : High scale load balancing
  * Cloud Map                       : App resource registry for microservices
  * App Mesh                        : Monitor and control microservices
  * Transit Gateway                 : Scale VPC and account connections
  * Global Accelerator              : Improve application availability and performance

- Developer Tools -------------------
  * CodeStar                        : Develop and deploy AWS Apps
  * CodeCommit                      : Store code in private Git Repositories
  * CodeBuild                       : Build and test code
  * CodeDeploy                      : Automate code deployment
  * CodePipeline                    : Release software using continuous delivery
  * Cloud9                          : Write, run, and debug code on Cloud IDE
  * X-Ray                           : Analyze and debug applications
  * Command Line Interface          : Unified tool to manage AWS services

- Management and Governance ---------
  * CloudWatch                      : Monitor resources and apps
  * Auto Scaling                    : Scale multiple resources to meet demand
  * CloudFormation                  : Create and manage resources with templates
  * CloudTrail                      : Track user activity and API usage
  * Config                          : Track resouce inventory and changes
  * OpsWorks                        : Automate operations with Chef and Puppet
  * Systems Manager                 : Gain operational insights and take action
  * Trusted Advisor                 : Optimize performance and security
  * Personal Health Dashboard       : Personalized view of AWS Service Health
  * Control Tower                   : Set up and govern a secure, compliant, multi-account environment
  * License Manager                 : Track, manage, and control licenses
  * Well-Architected Tool           : Review and improve workloads

- Media Services --------------------
  * Elastic Transcoder              : Scalable media transcoding
  * Kinesis Video Streams           : Process and analyze video streams
  * Elemental MediaConvert          : Convert file-based video content
  * Elemental MediaLive             : Convert live video content
  * Elemental MediaPackage          : Video origination and packaging
  * Elemental MediaStore            : Media storage and simple HTTP origin
  * Elemental MediaTailor           : Video personalization and monetization
  * Elemental MediaConnect          : Reliable and secure live video transport

- Security, Identity, and Compliance-
  * Identity and Access Management  : Manage user acces and encryption keys
  * Cloud Directory                 : Cloud-native directories
  * Cognito                         : Identity management for apps
  * Single Sign-On                  : Cloud SSO Service
  * GuardDuty                       : Managed threat detection service
  * Inspector                       : Analyze app security
  * Macie                           : Discover, classify, and protect data
  * Certificate Manager             : Provision, manage, and deploy SSL/TLS certificates
  * CloudHSM                        : Hardware-based key storage and regulatory compliance
  * Directory Service               : Host and manage active directory
  * Firewall Manager                : Central management of firewall rules
  * Key Management Service          : Managed creation and control of encryption keys
  * Organizations                   : Policy-based managedment for multiple AWS Accounts
  * Secrets Manager                 : Rotate, manage, and retrieve secrets
  * Shield                          : Distributed Denial of Service (DDoS) protection
  * WAF                             : Filter malicious web traffic
  * Artifact                        : On-demand access to AWS compliance reports
  * Security Hub                    : Unified security and compliance center

- Analytics -------------------------
  * Athena                          : Query data in SL using SQL
  * CloudSearch                     : Managed search service
  * Elasicsearch Service            : Run and scale eslasticsearch clusters
  * EMR                             : Hosted hadoop framework
  * Kinesis                         : Work with real-time streaming data
  * Redshift                        : Fast, simple, cost-effective data warehousing
  * Quicksight                      : Fast business analytics service
  * Data Pipeline                   : Orchestration service for periodic, data-driven workflows
  * Glue                            : Prepare and load data
  * Managed streaming for Kafka     : Fully managed Apache Kafka service
  * Lake Formation                  : Build a secure data lake in days

- Machine Learning ------------------
  * SageMaker                       : Build, train, and deploy machine learning models at scale
  * Comprehend                      : Discover insights and relationships in text
  * Lex                             : Build voice and text chatbots
  * Polly                           : Turn text into lifelike speech
  * Rekognition                     : Analyze image and video
  * Translate                       : Natural and fluent language translation
  * Transcribe                      : Automatic speech recognition
  * Deeplens                        : Deep learning enabled video camera
  * Deep Learning AMIs              : Quickly start deep learning on EC2
  * Personalize                     : Build real-time recommendations into applications
  * Forecast                        : Increase forecast accuracy using machine learning
  * Inferentia                      : Machine learning inference chip
  * Textract                        : Extract text and data from documents
  * Elastic Inference               : Deep learning inference acceleration

- Mobile ----------------------------
  * Amplify                         : Build and deploy mobile and web apps
  * API Gateway                     : Build, deploy, and manage APIs
  * Pinpoint                        : Push notifications for mobile apps
  * App Sync                        : Real-time and offline mobile data apps
  * Device Farm                     : Test Android, FireOS, and iOS on real devices in cloud

- AR and VR -------------------------
  * Sumerian                        : Build and run Virtual Reality and Augmented Reality Applications

- Application Integration -----------
  * Step Functions                  : Coordinate distributed apps
  * Simple Queue Service (SQS)      : Managed message queues
  * Simple Notification Service(SNS): Pub/sub, mobile push and SMS
  * MQ                              : Managed message broker for ActiveMQ

- Customer Engagement ---------------
  * Connect                         : Cloud-based contact center
  * Pinpoint                        : Push notifications for mobile apps
  * Simple Email Service (SES)      : Email sending and receiving

- Business Applications -------------
  * Alexa for Business              : Empower organization with Alexa
  * Chime                           : Frustration-free meetings, video calls, and chat
  * WorkDocs                        : Enterprise storage and sharing service
  * WorkMail                        : Secure and managed business email and calendaring

- Internet of Things ----------------
  * IoT Core                        : Connect devices to the cloud
  * FreeRTOS                        : IoT operating system for microcontrollers
  * Greengrass                      : Local compute, messaging, and sync for devices
  * IoT 1-Click                     : One click creation of an AWS Lambda Trigger
  * IoT Analytics                   : Analytics for IoT Devices
  * IoT Button                      : Cloud programmable dash button
  * IoT Device Defender             : Security management for IoT devices
  * IoT Device Management           : Onboard, organize, and remotely manage IoT devices
  * IoT Events                      : IoT event detection and response
  * IoT SiteWise                    : IoT data collector and interpreter
  * Partner Device Catalog          : Curated catalog of AWS-compatible IoT hardware
  * IoT Things Graph                : Easily connect devices and web services

- Game Development ------------------
  * GameLift                        : Simple, fast, cost-effective dedicated game server hosting
  * Lumberyard                      : Free cross-platform 3D game engine with full source

- Cost Management -------------------
  * Cost Explorer                   : Analyze AWS cost and usage
  * Budgets                         : Custome cost and usage budgets
  * Reserved instance reporting     : Dive deeper into reserved instances
  * Cost and Usage Report           : Access comprehensive cost and usage information

- Blockchain ------------------------
  * Managed Blockchain              : Create and manage scalable blockchain networks

- Robotics --------------------------
  * RoboMaker                       : Develop, test, and deploy robotics apps

- Satellite -------------------------
  * Ground Station                  : Fully managed ground station as a service