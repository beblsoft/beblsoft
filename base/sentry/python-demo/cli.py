#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Sentry API Sample Command Line Interface
"""


# ----------------------------- IMPORTS ------------------------------------- #
import os
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.sentry.python.bSentry import BeblsoftSentry
from base.sentry.python.project.dao import BSentryProjectDAO
from base.sentry.python.release.dao import BSentryReleaseDAO


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Sentry ----------------------------------
        self.organizationSlug   = "beblsoft"
        self.project            = "smeckn"
        self.repository         = "gitlab.com/beblsoft/beblsoft"
        self.commitSHA          = "885a1fada9380969642be10911c7c076c6074c97"
        self.authToken          = os.environ["SENTRY_AUTH_TOKEN"]
        self.bSentry            = BeblsoftSentry(authToken=self.authToken)

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None


logger         = logging.getLogger(__name__)
defaultLogFile = "/tmp/sentryAPIDemo.log"
gc             = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default=defaultLogFile, type=click.Path(),
              help="Specify log file Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Sentry API Sample Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- PROJECT ------------------------------------------- #
@cli.command()
def project_list():
    """
    Project CRUD
    """
    bSentryProjectModelList = BSentryProjectDAO.list(bSentry=gc.bSentry)
    for bSentryProjectModel in bSentryProjectModelList:
        logger.info(bSentryProjectModel.slug)


# ----------------------- RELEASE ------------------------------------------- #
@cli.command()
def release_crud():
    """
    Release CRUD
    """
    # Create
    bSentryReleaseModel = BSentryReleaseDAO.create(bSentry=gc.bSentry, organizationSlug=gc.organizationSlug,
                                                   version=gc.commitSHA, commitRef=gc.commitSHA,
                                                   projectList=[gc.project])
    # Get
    bSentryReleaseModel = BSentryReleaseDAO.get(bSentry=gc.bSentry, organizationSlug=gc.organizationSlug,
                                                version=gc.commitSHA)

    # Describe
    bSentryReleaseModel.describe()

    # Delete
    BSentryReleaseDAO.delete(bSentry=gc.bSentry, organizationSlug=gc.organizationSlug, version=gc.commitSHA)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
