#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Sentry Release Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.sentry.python.common.models import BSentryCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT SENTRY RELEASE MODEL --------------------- #
class BSentryReleaseModel(BSentryCommonModel):
    """
    Beblsoft Sentry Release Model

    Reference:
      https://docs.sentry.io/api/releases/get-organization-releases/

    {
      "authors": [],
      "commitCount": 0,
      "data": {},
      "dateCreated": "2018-11-06T21:19:58.559Z",
      "dateReleased": null,
      "deployCount": 0,
      "firstEvent": "2018-11-06T21:19:58.639Z",
      "lastCommit": null,
      "lastDeploy": null,
      "lastEvent": "2018-11-06T21:19:58.639Z",
      "newGroups": 0,
      "owner": null,
      "projects": [
        {
          "name": "Prime Mover",
          "slug": "prime-mover"
        }
      ],
      "ref": null,
      "shortVersion": "2b6af31",
      "url": null,
      "version": "2b6af31b2edccc73a629108b17344dfe20858780"
    },

    """

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def version(self):  # pylint: disable=C0111
        return self.getValueFromObj("version")

    @property
    def shortVersion(self):  # pylint: disable=C0111
        return self.getValueFromObj("shortVersion")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("dateCreated"), "%Y-%m-%dT%H:%M:%S%Z")
        return d.astimezone(tz=timezone.utc)

    @property
    def releaseDate(self):
        """
        Release date in utc
        """
        d = datetime.strptime(self.getValueFromObj("dateReleased"), "%Y-%m-%dT%H:%M:%S%Z")
        return d.astimezone(tz=timezone.utc)
