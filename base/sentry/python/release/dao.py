#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Sentry Release DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.sentry.python.release.models import BSentryReleaseModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT SENTRY RELEASE DAO ----------------------- #
class BSentryReleaseDAO():
    """
    Beblsoft Sentry Release DAO
    """

    # ------------------- CREATE -------------------------------------------- #
    @staticmethod
    @logFunc()
    def create(bSentry, organizationSlug, version, commitRef=None, projectList=None):
        """
        Create Object

        Args
          bSentry:
            Beblsoft Sentry Object
          organizationSlug:
            Slug of Organization the release belongs to
            Ex. "beblsoft"
          version:
            A version identifier for this release. Ex. Git Hash
            Ex. "3dc00672f16c747e128ded4c43a1aa9026679eab"
          commitRef:
            Optional commit reference
            Ex. "3dc00672f16c747e128ded4c43a1aa9026679eab"
          projectList:
            List of project slugs involved in release
            Ex. ["smeckn"]

        Returns
          BSentryReleaseModel
        """
        data                 = {}
        data["version"]      = version
        if commitRef:
            data["ref"]      = commitRef
        if projectList:
            data["projects"] = projectList
        else:
            data["projects"] = []

        obj  = bSentry.requestJSON(
            method       = "POST",
            relativePath = "organizations/{}/releases/".format(organizationSlug),
            data         = data)
        return BSentryReleaseModel(obj=obj)

    # ------------------- DELETE -------------------------------------------- #
    @staticmethod
    @logFunc()
    def delete(bSentry, organizationSlug, version):
        """
        Delete Object
        """
        bSentry.request(
            method        = "DELETE",
            relativePath  = "organizations/{}/releases/{}/".format(organizationSlug, version))

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bSentry, organizationSlug, version):
        """
        Get Object

        Returns
          BSentryReleaseModel
        """
        obj = bSentry.requestJSON(
            method        = "GET",
            relativePath  = "organizations/{}/releases/{}/".format(organizationSlug, version))
        return BSentryReleaseModel(obj=obj)
