#!/usr/bin/env python3.6
"""
 NAME
  bSentry.py

 DESCRIPTION
  Beblsoft Sentry API Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import json
import logging
import pprint
import requests
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- BEBLSOFT SENTRY ----------------------------- #
class BeblsoftSentry():
    """
    Beblsoft Sentry

    Root Object for Accessing the Sentry API
    Note: this is separate from the Python Sentry SDK
    """

    def __init__(self, authToken, server="https://sentry.io", version="0",
                 timeoutS=10.0, session=None):  # pylint: disable=W0622
        """
        Initialize object
        Args
          authToken:
            Auth Token from Sentry
            See: https://sentry.io/settings/account/api/auth-tokens/
          server:
            Sentry API Server
            Ex. "https://sentry.io/"
          version:
            Version of API to use
            Ex. "0"
          timeoutS:
            Float describing time (in seconds) that client will wait for responses
          session:
            Reqests session object
        """
        self.server    = server
        self.authToken = authToken
        self.version   = version
        self.timeoutS  = timeoutS
        self.session   = session or requests.Session()

    # ------------------------- REQUEST ------------------------------------- #
    @logFunc()
    def request(self, method, relativePath, headers=None, params=None, data=None,
                raiseOnError=True):
        """
        Executes a graph API request

        Args
          method:
            HTTP Method
            Ex. "GET"
          relativePath:
            Path relative to server/api/<version>
            Ex. "projects"
          headers:
            Dictionary headers for the request
            Ex. {}
          params:
            Dictionary Query strying parameters
            Ex. { }
          data:
            Dictionary of Post/put data
            Ex. {}

        Returns
          Requests response object
          http://docs.python-requests.org/en/master/api/#requests.Response
        """
        path = "{}/api/{}/{}".format(self.server, self.version, relativePath)
        if not params:
            params = {}
        if not headers:
            headers = {}
        headers["Authorization"] = "Bearer {}".format(self.authToken)
        headers["Content-Type"]  = "application/json"

        # Send Request
        try:
            resp = self.session.request(
                method,
                path,
                timeout = self.timeoutS,
                params  = params,
                data    = json.dumps(data),
                headers = headers)
        except requests.HTTPError as e:
            raise BeblsoftError(code=BeblsoftErrorCode.SENTRY_REQUEST_FAILED,
                                originalError=e)
        # Does Not Exist
        if raiseOnError and resp.status_code == 404:
            raise BeblsoftError(code=BeblsoftErrorCode.SENTRY_REQUEST_DOES_NOT_EXIST,
                                msg="Resp={}".format(pprint.pformat(resp.__dict__)))

        # Process Error
        success = 200 <= resp.status_code and resp.status_code <= 299 #pylint: disable=C0122
        error   = not success
        if raiseOnError and error: #pylint: disable=E1101
            raise BeblsoftError(code=BeblsoftErrorCode.SENTRY_REQUEST_FAILED,
                                msg="Resp={}".format(pprint.pformat(resp.__dict__)))

        return resp

    # ------------------------- REQUEST JSON -------------------------------- #
    @logFunc()
    def requestJSON(self, **requestKwargs):
        """
        Execute request and return JSON response
        """
        return self.request(**requestKwargs).json()
