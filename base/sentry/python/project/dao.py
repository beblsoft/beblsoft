#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Sentry Project DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.sentry.python.project.models import BSentryProjectModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT SENTRY PROJECT DAO ----------------------- #
class BSentryProjectDAO():
    """
    Beblsoft Sentry Project DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def list(bSentry):
        """
        Returns
          BSentryProjectModelList
        """
        bSentryProjectModelList = []
        objList                 = bSentry.requestJSON(
            method                = "GET",
            relativePath          = "projects/")
        for obj in objList:
            bSentryProjectModelList.append(BSentryProjectModel(obj=obj))
        return bSentryProjectModelList
