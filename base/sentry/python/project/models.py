#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Sentry Project Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.sentry.python.common.models import BSentryCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT SENTRY PROJECT MODEL --------------------- #
class BSentryProjectModel(BSentryCommonModel):
    """
    Beblsoft Sentry Project Model

    Reference:
      https://docs.sentry.io/api/projects/get-project-index/

    JSON:
      {
        "avatar": {
          "avatarType": "letter_avatar",
          "avatarUuid": null
        },
        "color": "#bf6e3f",
        "dateCreated": "2018-11-06T21:20:08.064Z",
        "features": [
          "servicehooks",
          "sample-events",
          "data-forwarding",
          "rate-limits",
          "minidump"
        ],
        "firstEvent": null,
        "hasAccess": true,
        "id": "4",
        "isBookmarked": false,
        "isInternal": false,
        "isMember": true,
        "isPublic": false,
        "name": "The Spoiled Yoghurt",
        "organization": {
          "avatar": {
            "avatarType": "letter_avatar",
            "avatarUuid": null
          },
          "dateCreated": "2018-11-06T21:19:55.101Z",
          "id": "2",
          "isEarlyAdopter": false,
          "name": "The Interstellar Jurisdiction",
          "require2FA": false,
          "slug": "the-interstellar-jurisdiction",
          "status": {
            "id": "active",
            "name": "active"
          }
        },
        "platform": null,
        "slug": "the-spoiled-yoghurt",
        "status": "active"
      }

    """

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @property
    def slug(self):  # pylint: disable=C0111
        return self.getValueFromObj("slug")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("dateCreated"), "%Y-%m-%dT%H:%M:%S%Z")
        return d.astimezone(tz=timezone.utc)
