#!/usr/bin/env python3
"""
NAME
 breadcrumbs.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
    dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
    max_breadcrumbs       = 100,
    release               = "f21533f293edf982b33cc8103880e9e062672304",
    environment           = "Production",
    server_name           = "https://sentry.io")

    sentry_sdk.add_breadcrumb(message="Message crumb!")
    sentry_sdk.add_breadcrumb(message="Data crumb!", data={"foo": "bar"})
    sentry_sdk.add_breadcrumb(message="Category crumb!", category="auth")
    sentry_sdk.add_breadcrumb(message="Debug crumb!", level="debug")
    sentry_sdk.add_breadcrumb(message="Info crumb!", level="info")
    sentry_sdk.add_breadcrumb(message="Warning crumb!", level="warning")
    sentry_sdk.add_breadcrumb(message="Error crumb!", level="error")
    sentry_sdk.add_breadcrumb(message="Fatal crumb!", level="fatal")
    raise Exception("Exception!")
