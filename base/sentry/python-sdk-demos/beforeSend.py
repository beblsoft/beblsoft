#!/usr/bin/env python3
"""
NAME
 beforeSend.py

SEE ALSO
 Filtering Python Events :https://docs.sentry.io/error-reporting/configuration/filtering/?platform=python
"""

# ------------------------------ IMPORTS ------------------------------------ #
import pprint
import sentry_sdk


# ------------------------------ BEFORE SEND -------------------------------- #
def beforeSend(event, hint):
    """
    Function to be executed before event sent to Sentry server
    """
    for obj in [event, hint]:
        print("-" * 80)
        print(pprint.pformat(obj))
    return event


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
    dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
    max_breadcrumbs       = 100,
    release               = "f21533f293edf982b33cc8103880e9e062672304",
    environment           = "Production",
    server_name           = "https://sentry.io",
    before_send           = beforeSend)

    raise Exception("Exception!")
