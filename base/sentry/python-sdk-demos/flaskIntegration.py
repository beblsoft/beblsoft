#!/usr/bin/env python3
"""
NAME
 flaskIntegration.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
from flask import Flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration


# ------------------------------ GLOBALS ------------------------------------ #
app = Flask(__name__)


# ------------------------------ ROUTES ------------------------------------- #
@app.route("/foo")
def fooFunc():  # pylint: disable=C0111
    raise Exception("Foo Exception")


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io",
        integrations          = [FlaskIntegration()],
        request_bodies        = "always")

    app.run(host="0.0.0.0", port=5000, debug=True)
