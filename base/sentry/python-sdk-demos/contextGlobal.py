#!/usr/bin/env python3
"""
NAME
 contextGlobal.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io")

    with sentry_sdk.configure_scope() as scope:
        scope.user = {"email": "bensson.james@gmail.com"}

    # Scope.user IS set
    raise Exception("Exception!")
