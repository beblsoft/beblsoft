#!/usr/bin/env python3
"""
NAME
 exceptionCaptured.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk


# ------------------------------ HELPERS ------------------------------------ #
def a(arg1, arg2, arg3=3):  # pylint: disable=C0111,W0613
    b(arg1=arg1, arg2=arg2)


def b(arg1, arg2):  # pylint: disable=C0111
    c(arg1=arg1, arg2=arg2)


def c(arg1, arg2):  # pylint: disable=C0111
    d(arg1=arg1, arg2=arg2)


def d(arg1, arg2):  # pylint: disable=C0111
    raise Exception("Basic Exception {} {}!".format(arg1, arg2))


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        with_locals           = True,
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io")

    try:
        a(arg1=1, arg2=2)
    except Exception as e:  # pylint: disable=W0703
        sentry_sdk.capture_exception(e)
