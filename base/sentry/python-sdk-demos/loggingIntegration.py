#!/usr/bin/env python3
"""
NAME
 loggingIntegration.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import logging
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration, ignore_logger


# ------------------------------ GLOBALS ------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    logging.basicConfig(level=logging.NOTSET)

    sentryLogging = LoggingIntegration(
        level       = logging.NOTSET,
        event_level = logging.ERROR)

    ignore_logger("requests")
    ignore_logger("boto3")

    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io",
        integrations          = [sentryLogging],
        request_bodies        = "always")

    logger.debug("Debug Breadcrumb!")
    logger.debug("Debug Breadcrumb with extra data!", extra={"bar": 43})
    logger.info("Info Breadcrumb!")
    logger.warning("Warning Breadcrumb!")
    logger.error("Event!")
