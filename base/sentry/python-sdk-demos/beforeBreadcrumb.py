#!/usr/bin/env python3
"""
NAME
 beforeBreadcrumb.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import pprint
import sentry_sdk


# ------------------------------ BEFORE SEND -------------------------------- #
def beforeBreadcrumb(crumb, hint):
    """
    Function to be executed before breadcrumb  sent to Sentry server
    """
    for obj in [crumb, hint]:
        print("-" * 80)
        print(pprint.pformat(obj))
    return crumb


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
    dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
    max_breadcrumbs       = 100,
    release               = "f21533f293edf982b33cc8103880e9e062672304",
    environment           = "Production",
    server_name           = "https://sentry.io",
    before_breadcrumb     = beforeBreadcrumb)

    sentry_sdk.add_breadcrumb(message="I am a breadcrumb!")
    raise Exception("Exception!")
