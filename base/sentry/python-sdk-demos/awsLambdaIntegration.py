#!/usr/bin/env python3
"""
NAME
 awsLambdaIntegration.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io",
        integrations          = [AwsLambdaIntegration()],
        request_bodies        = "always")

    raise Exception("AWS Lambda Exception")
