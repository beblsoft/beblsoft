#!/usr/bin/env python3
"""
NAME
 context.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "https://sentry.io")

    with sentry_sdk.configure_scope() as scope:
        scope.user = {"id": 324857,
                      "email": "bensson.james@gmail.com",
                      "username": "jbensson",
                      "ip_address": "5.4.3.2"
                      }
        scope.set_tag("Language", "Python3.6")
        scope.set_tag("Platform", "LinuxMint")
        scope.set_tag("Hostname", "foo.bar@goo.com")
        scope.set_extra("logStream", "thelongestLogStream@cloudwatch.amazonaws.com")
        scope.set_extra("debugToken", "2349087sdfkjhasdf092384023lkj")
        scope.level = "warning" # fatal, error, warning, info, or debug

    raise Exception("My Other Error")
