#!/usr/bin/env python3
"""
NAME
 configOptions.py

DESCRIPTION
 Display Configuration Options

BIBLIOGRAPHY
 Python SDK          : https://github.com/getsentry/sentry-python/tree/master/sentry_sdk
 Python SDK Constants: https://github.com/getsentry/sentry-python/blob/master/sentry_sdk/consts.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
import sentry_sdk


# ------------------------------ MAIN --------------------------------------- #
if __name__ == "__main__":
    sentry_sdk.init(
        dsn                   = "https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
        with_locals           = True,
        max_breadcrumbs       = 100,
        release               = "f21533f293edf982b33cc8103880e9e062672304",
        environment           = "Production",
        server_name           = "My-Awesome-Server",
        shutdown_timeout      = 2.0,
        integrations          = [],
        in_app_include        = [],
        in_app_exclude        = [],
        default_integrations  = True,
        dist                  = None,
        transport             = None,
        sample_rate           = 1.0,
        send_default_pii      = False,
        http_proxy            = None,
        https_proxy           = None,
        ignore_errors         = [],
        request_bodies        = "medium",
        before_send           = None,
        before_breadcrumb     = None,
        debug                 = True,
        attach_stacktrace     = False,
        ca_certs              = None)

    raise Exception("Hello World Sentry!")
