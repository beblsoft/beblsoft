OVERVIEW
===============================================================================
Sentry Projects Documentation


PROJECT: PYTHON-DEMO
===============================================================================
- Description:                      : Wrapper program over Sentry Python API
- Directory                         : ./python-demo
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 /tmp/sentryAPIVenv
  * Enter virtualenv                : source /tmp/sentryAPIVenv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- To Run
  * Help                            : ./cli.py --help
  * List projects                   : ./cli.py project-list
  * Release CRUD                    : ./cli.py release-crud



PROJECT: PYTHON-SDK-DEMOS
===============================================================================
- Description:                      : Sentry Python Demos
                                    : All programs use Sentry HelloWorld project DSN
- Directory                         : ./python-sdk-demos
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 /tmp/sentryVenv
  * Enter virtualenv                : source /tmp/sentryVenv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

 - Hello World ---------------------: Capture Hello World Exception
  * Run program                     : ./helloWorld.py
                                      Program will exception events after running

- Configuration Options ------------: Display all Sentry configuration options
  * Run program                     : ./configOptions.py

- Exception Unhandled --------------: Configure a basic exception stack to fire
  * Run program                     : ./exceptionUnhandled.py

- Exception Captured ---------------: Example capturing a thrown exception
  * Run program                     : ./exceptionCaptured.py

- Message --------------------------: Capture an example message
  * Run program                     : ./message.py

- Before Send ----------------------: Register a beforeSend hook to print the the event
                                      before sending to Sentry servers
  * Run program                     : ./beforeSend.py

- Before Breadcrumb ----------------: Register a beforeBreadcrumb hook to print the breadcrumbs
                                      before sending to Sentry servers
  * Run program                     : ./beforeBreadcrumb.py

- Breadcrumbs ----------------------: Add multiple breadcrumbs
  * Run program                     : ./breadcrumbs.py

- Context Global -------------------: Example modifying the global sentry context
  * Run program                     : ./contextGlobal.py

- Context Local --------------------: Example modifying the local context. Scope changes are removed
                                      after context manager exits.
  * Run program                     : ./contextLocal.py

- Context --------------------------: Example showing all the things that can be set inside the context
                                    : ./context.py

- Flask Integration ----------------: Example using the flask integration
  * Run program                     : ./flaskIntegration.py
  * Trigger Exception               : Open in Chrome: http://localhost:5000/foo

- AWS Lambda Integration -----------: Example using AWS lambda integration
  * Run program                     : ./awsLambdaIntegration.py

- Logging Integration --------------: Example creating breadcrumbs from log data
  * Run program                     : ./loggingIntegration.py



PROJECT: VUE-DEMOS
===============================================================================
- Description:                      : Sentry Vue Demos
                                    : All programs use Sentry HelloWorld project DSN
- Directory                         : ./vue-demos
 - Basics --------------------------: Basic examples of Vue inside Sentry
  * View in Chrome                  : Open File
                                      Open Console
                                      Click "Throw Error"
  * Buttons
    errorStack1                     : Trigger series of functions that will Throw an error
    captureException                : Capture a caught exception
    captureExceptionWithReportDialog: Capture an exception an open a reporting dialog
                                      to collect user feedback
    captureMessage                  : Capture a message
    captureMessageWithScope         : Capture a message adding global scope
    captureMessageWithLocalScope    : Capture a message with local scope
    captureMessageWithMultipleScopes: Capture a message configuring the scope multiple times
    captureMessageWithBreadcrumbs   : Capture a message leveraging breadcrumbs
