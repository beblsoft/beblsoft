OVERVIEW
===============================================================================
Sentry Technology Documentation
- Description                       : Sentry is an open-source error tracking tool that helps
                                      you monitor and fix crashes in real time
- Problem                           : Post-deployment process of discovery, investigation, and
                                      remediation is painful, confusing, annoying, and lengthy
                                      Sentry solves this
- Relevant URLS ---------------------
  * Home                            : https://sentry.io/welcome/
  * Docs                            : https://docs.sentry.io/
  * Python Client GitHub            : https://github.com/getsentry/sentry-python
  * Python Client SDK Docs          : https://getsentry.github.io/sentry-python/
  * Sentry-CLI Github               : https://github.com/getsentry/sentry-cli



WORKFLOW
===============================================================================
- Developer Workflow ---------------: Design
                                      Code and Review
                                      Testing / QA
                                      CI / CD
                                      Issue Discovery
                                      Investication
                                      Remediation
- Production Bug with Sentry --------
  * Deploy Code                     : Via CICD
  * Bug Happens                     : NullPointerException
  * Alert                           : Sentry picks up the error immediately and alerts via Slack, ...
  * Dashboard                       : Notification takes you to dashboard that gives necessary context
                                      to triage the problem - frequency, user imact, code affected,
                                      and which team member is likely owner for the issue
                                    : Detailed information to help debug: stack trace, stack locals,
                                      preceding events, commits, that likely caused the issue,
                                      and custom data captured
                                    : Automatically start tracking the issue in project management tool
  * User feedback                   : Sentry can offer users affected a friendly way to
                                      send debugging information



HOW TO GET THERE
===============================================================================
- 1 SDK Integration ----------------: Sentry is a client/server architecture
                                    : Integrate Senty's SDK into application and it starts sending
                                      errors to Sentry's servers as they happen
                                    : Once Sentry starts receiving errors, they appear in the dashboard
                                    : Breadcurmbds allow application to lead a trail of events
                                      prior to the error
- 2 Releases -----------------------: A release is a version of your code that is deployed to an environment
                                    : When sentry is givent release information, Sentry can predict the
                                      commits that caused an error, as well as the developer who is
                                      likely best suited to fix it
                                    : More frequently releases are shipped, the more accurate the
                                      predictions become because there are fewer commits with each
                                      new release
                                    : Configure SDK with a release tag (unique id for release, like
                                      a git sha or custom package version)
                                      SentryClient.setRelease({
                                        release: "6d5a6a446805a06154e25e2fa203d67b9e762f5d"
                                      });
                                    : When code is deployed, make an API call to tell Sentry which
                                      commits are associated with the release
- 3 User Feedback ------------------: With Sentry you can collect additional feedback from the user
                                      when they hit an error. This is primarily useful in situations
                                      wheere you might generally render a plain error page
                                    : When backend detects an error, instead of returning a generic
                                      500.html, return a page with Sentry's embeddable JS widget
                                      containing the feddback form



ERROR REPORTING
===============================================================================
- Getting Started -------------------
  * Install an SDK                  : Sentry captures data by using an SDK within your application's
                                      runtime. These are platform specific and allow Sentry to
                                      have a deep understanding of how application works
  * Configure the SDK               : After you completed setting up a project in Sentry,
                                      you'll be given a DSN, or Data Source Name
                                    : DSN is found in Project Name -> Project Settings -> Client Keys
    Python                          : import sentry_sdk
                                      sentry_sdk.init("https://<key>@sentry.io/<project>")

- Capturing ------------------------: Out of the box SDKs will attempt to hook themselves into runtime
                                      environment or framework to automatically report fatal errors
                                      automatically
                                    : Often useful to manually report errors or messages to Sentry
                                    : Can asl record breadcrumbs that lead up to an event
                                      Breadcrumbs are buffered until the next event is sent
  * Errors                          : Allows capturing exceptions to sent to Sentry
    Python                          : from sentry_sdk import capture_exception
                                      try:
                                          a_potentially_failing_function()
                                      except Exception as e:
                                          # Alternatively the argument can be omitted
                                          capture_exception(e)
  * Messages                        : Can also capture a bare message
                                      Message is textual information that should be sent to Sentry
    Python                          : from sentry_sdk import capture_message
                                      capture_message("Something went wrong")
  * Events                          : SDKs provide way to capture entire custom event objects

- Configuration --------------------: Options are set when SDKs are first initialized
  * Python                          : import sentry_sdk
                                      sentry_sdk.init(
                                          'https://<key>@sentry.io/<project>',
                                          max_breadcrumbs=50,
                                          debug=True)
  * Common Options                  : Common options across SDKs
    dsn                             : DSN telss the SDK where to send events to. If this
                                      value is not provided, the SDK will try to read it from
                                      the SENTRY_DSN environment variable
    debug                           : Toggles debug mode. Default is false. Recommended not to set
                                      true in production
    release                         : Sets the release. Release names are strings. By default SDK
                                      will try to read from SENTRY_RELEASE environment variable
    environment                     : Sets the environment. Ex. staging, production. By default SDK
                                      reads value from SENTRY_ENVIRONMENT variable
    sample_rate                     : Configures sample rate as a percentage of events to be sent
                                      in the range of 0.0 to 1.0. Default is 1.0 which means that 100%
                                      of events are sent
    max_breadcrumbs                 : Controls total amount of breadcrumbds that should be captured
                                      Defaults to 100
    attach_stacktrace               : When True, stack traces are attached to all logged messages
                                      Defaults to false
    send_default_pii                : If True, certain PII information is added by active integrations
                                      Recommended to turn on feature and use server side PII stripping
                                      to remove the values
    server_name                     : Name of server, usually autodetected by SDKs
    blacklist_urls                  : List of strings or regex patterns that match error URLs
                                      that should not be sent to Sentry
    whitelist_urls                  : List of strings or regex patterns that match error URLs
                                      that should be sent to Sentry
    in_app_include                  : List of string prefixes of module names that belong to the app.
                                      Takes precedence over in_app_exclude
    in_app_exclude                  : List of string prefixes of module names that do not belong to
                                      the app, but rather third-party packages. Modules not part of
                                      the app will be hidden from stack traces
    request_bodies                  : Controls if integrations should capture HTTP request bodies.
                                      never  - request bodies are never sent
                                      small  - only small request bodies (max ~4KB)
                                      medium - max ~10KB
                                      always - always capture body
    with_locals                     : When enabled, local variables are sent along with stackframes.
                                      Can have performance and PII imact. Enabled by default
    ca_certs                        : Path to an alternative CA bundle file in PEM-format
  * Integration configuration
    integrations                    : Integrations are configured through this parameter on library
                                      initialization
    default_integrations            : Set to false to disable default integrations
  * Hooks
    before_send                     : Function called with SDK event object before event object is sent
                                      Useful for PII stripping
    before_breadcrumb               : Function called with an SDK breadcrumb object before the breadcrumb
                                      is added to the scope. When nothging is returned the breadcrumb
                                      is dropped
  * Transport options
    transport                       : Useful to capture events for unit testing or send through complex
                                      setup for proxy authentication
    http_proxy                      : When set a proxy is configured that should be used for outbound requests
    https_proxy                     : Configures a separate proxy for outgoing HTTPS requests
    shutdown_timeout                : Controls how many seconds to wait before shutting down.
                                      Default is around 2 seconds.

- Filtering events -----------------: Run custom logic before events are sent
  * Before Send Python              : import sentry_sdk
                                      def strip_sensitive_data(event, hint):
                                          # modify event here
                                          return event
                                      sentry_sdk.init(before_send=strip_sensitive_data)
  * Before Breadcrumb Python        : import sentry_sdk
                                      def before_breadcrumb(crumb, hint):
                                          if 'log_record' in hint:
                                              crumb['data']['thread'] = hint['log_record'].threadName
                                          return crumb
                                      sentry_sdk.init(before_breadcrumb=before_breadcrumb)

- Security Policy Reporting --------: Sentry provides the ability to collect information on
                                      Content-Security-Policy (CSP) violations, as well as Expect-CT and
                                      HTTP Public Key Pinning (HPKP) failures by setting proper HTTP
                                      header
                                    : Integration processes consists of configuring the appropriate
                                      header with project key's security header endpoint found
                                      at Project Settings > Security Headers
  * Content Security Policy (CSP)   : Security standard which helps prevent cross-site scripting (XSS),
                                      clickjacking, and other code injection attacks.
                                    : Content-Security-Policy: ...;
                                      report-uri https://sentry.io/api/1426526/security/?sentry_key=<sentry key>
                                    : Content-Security-Policy-Report-Only: ...;
                                      report-uri https://sentry.io/api/1426526/security/?sentry_key=<sentry key>
  * Expect-CT                       : Certificate transparency (CT) is a security standard which helps
                                      track and identify valid certificates, allowing identification of
                                      maliciously issued certificates
                                    : Expect-CT: ...,
                                      report-uri="https://sentry.io/api/1426526/security/?sentry_key=<sentry key>
  * HTTP Public Key Pinning (HPKP)  : Security feature that tells a web client to associate a specific
                                      cryptographic public key with a certain web server to decrease
                                      the risk of MITM attacks
                                    : Public-Key-Pins: ...;
                                      report-uri="https://sentry.io/api/1426526/security/?sentry_key=<sentry key>
  * Additional config               : Also pass
                                      sentry_environment: The environment name (e.g. production).
                                      sentry_release:     The version of the application.



ENRICHING ERROR DATA
===============================================================================
- Context --------------------------: Sentry supports additional context with events. Often
                                      this context is shared amongst any issue captured in its lifecycle
  * Structured contexts             : Specific structured contexts (OS info, runtime info, etc). This is normally
                                      set automatically
  * User                            : Current actor
    Python                          : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.user = {"email": "john.doe@example.com"}
    User Scope
    id                              : Internal id for user
    username
    email
    ip_address
  * Tags                            : Tags are various key/value pairs that get assigned to an event,
                                      and can later be used as a breakdown or quick access to
                                      finding related events
    Python                          : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.set_tag("page_locale", "de-at")
    Use Cases                       : Server hostname, platform version (e.g. iOS 5.0), user's language
  * Level                           : Set the severity of an event to one of five values
                                      "fatal", "error", "warning", "info", and "debug"
    Python                          : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.level = 'warning'
  * Fingerprint                     : Sentry uses fingerprints to decide how to group errors into
                                      issues
                                    : Override the default grouping by passing an array of strings
    Python                          : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.fingerprint = ['my-view-function']
  * Extra Context                   : In addition to the structured context that Sentry understands,
                                      can send arbitrary key/value pairs of data which will be stored
                                      alongside the event
                                    : These are not indexed and are simply used to add additional
                                      information about what might be happening
    Python                          : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.set_extra("character_name", "Mighty Fighter")
    Payload size                    : Be aware of maximum payload size. There are times, when
                                      you may not want to send the whole application state as
                                      extra data. Don't send over 200kB or will get HTTP Error 413
                                      Payload Too Large
  * Unsetting Context               : Context is held in the current scope and this is cleared out
                                      at the end of each operation (request etc)
- Environments ---------------------: As of Sentry 9, can easily filter issues, releases, and user
                                      feedback by environment
  * Set Default                     : Can see a certain environment every time you open Sentry
                                      project settings > Environments, click "Set as default"
  * Python                          : import sentry_sdk
                                      sentry_sdk.init(environment="staging")
- Breadcrumbs ----------------------: Sentry supports a concept called Breadcrumps, which is a
                                      trail of events which happened prior to an issue.
                                      Often times these events are very similar to traditional
                                      logs, but also have the ability to record more rich
                                      structured data
  * Attributes                      : Each crumb has the following attributes
    Message                         : A string describing the event. The most common vector, often used
                                      as a drop-in replacement for a traditional log message
    Data                            : A mapping (str=>str) of metadata around the event
                                      Often used instead of a message, but may also be used in addition
    Category                        : Category to label the event under. Similar to logger name (ex. auth)
    Level                           : May be any of fatal, error, warning, info, or debug
    Type                            : Semi internal attribute type exists which can control the type
                                      of breadcrumb. Influences how crumb is rendered
                                      default: default rendering
                                      http: renders as HTTP request
                                      error: renders as hard error
  * Recording Python                : Manual recording is available and easy to use
                                      from sentry_sdk import add_breadcrumb
                                      add_breadcrumb(
                                          category='auth',
                                          message='Authenticated user %s' % user.email,
                                          level='info')
  * Automatic breadcrumbs           : SDKs will automatically start recording breadcrumbs by
                                      enabling integrations
  * Customization Python            : before_breadcrumb hook allows breadcrumb to be discarded or modified
                                      import sentry_sdk
                                      def before_breadcrumb(crumb, hint):
                                          if crumb['category'] == 'a.spammy.Logger':
                                              return None
                                          return crumb
                                      sentry_sdk.init(before_breadcrumb=before_breadcrumb)
- User Feedback --------------------: Sentry provides the ability to collect additional feedback from the
                                      user upon hitting an error. This is primarily useful in situations
                                      where you might generally render a plain error page
                                      (the classic 500.html)
                                    : To collect the feedback, an embeddable JS widget is available
  * Required information            : User name, user email address, description of what happened
  * Collecting feedback Javascript  : <script src="https://browser.sentry-cdn.com/5.0.5/bundle.min.js" crossorigin="anonymous"></script>
                                      <script>
                                        Sentry.init({
                                          dsn: 'https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526',
                                          beforeSend(event) {
                                            // Check if it is an exception, if so, show the report dialog
                                            if (event.exception) {
                                              Sentry.showReportDialog();
                                            }
                                            return event;
                                          }
                                        });
                                      </script>
  * Customizing the Widget          : Several parameters are available for customization
    eventId                         : Default: manually set the id of the event
    dsn                             : Default: Manually set dsn to report to
    user                            : Default: Manually set user data
    user.email                      : Default: user's email address
    user.name                       : Default: Name
    lang                            : Default: override for Sentry's language code
    title                           : Default: It lloks like we're having issues
    subtitle                        : Default: Our team has been notified
    subtitle2                       : Default: If you'd like to help, tell us what happened below
    labelName                       : Default: Name
    labelEmail                      : Default: Email
    labelComments                   : Default: What happened?
    labelClose                      : Default: Close
    labelSubmit                     : Default: Submit
    errorGeneric                    : Default: An unknown error occured while submitting your report
                                               please try again.
    errorFormEntry                  : Default: Some fields were invalid. Please correct the errors and
                                               try again.
    successMessage                  : Default: Your feedback has been sent. Thank you!
- Scopes and Hubs ------------------: When an event is captured and sent to Sentry, SDKs will merge that
                                      event data with extra information from the current scope. SDKs will
                                      typically automatically manage the scopes for you in the framework
                                      integrations
  * Hub                             : Hub is the central point that SDKs use to route an event to Sentry
                                      When you call init() a hub is created and a client and a blank
                                      scope are created on it. That hub is then associated with the current
                                      thread and will internally hold a stack of scopes
  * Scope                           : The scope holds useful information that should be sent along with
                                      the event. For instance contexts or breadcrumbs are stored in
                                      the scope. When a scope is pushed, it inherits all data from
                                      the parent scope and when it pops all modifications are reverted
                                    : Default SDK integrations will push and pop scopes intelligently
  * Scope and Hub workings          : When you start using an SDK scope and hub are automatically created
                                    : Unlikely to modify hub unless writing an integration
                                    : Modify scope anytime configure-scope is called
                                    : When a global function like capture_event is called, Sentry
                                      discovers the current hub and asks it to apture an event.
                                      Internally the hub will then merge the event with the topmost
                                      scope's data
  * Configuring Scope Python        : from sentry_sdk import configure_scope
                                      with configure_scope() as scope:
                                          scope.set_tag("my-tag", "my value")
                                          scope.user = {'id': 42, 'email': 'john.doe@example.com'}
  * Local scopes                    : Support for pushing and configuring a scope in one go.
                                      Typically called with-scope or push-scope
                                    : Helpful to send data with one specific event
    Python                          : from sentry_sdk import push_scope, capture_exception
                                      with push_scope() as scope:
                                          scope.set_tag("my-tag", "my value")
                                          scope.level = 'warning'
                                          # will be tagged with my-tag="my value"
                                          capture_exception(Exception("my error"))
                                      # will not be tagged with my-tag
                                      capture_exception(Exception("my other error"))



WORKFLOW AND INTEGRATIONS
===============================================================================
- Releases -------------------------: A release is a version of your code that is deployed to an
                                      environment. When you give Sentry information about your
                                      releases, you unlock a number of new features:
                                      - Determine the issues and regressions introduced in a new release
                                      - Predict which commit caused an issue and who is likely
                                        responsible
                                      - Resolve issues by including the issue number in commit message
                                      - Receive email notifications when your code gets deployed
  1 Configure SDK                   : Include a release ID when you configure your client SDK
                                    : Commonly a git SHA or a custom version number
                                    : Releases are global per organization
                                    : import sentry_sdk
                                      sentry_sdk.init(release="55348d9db2e69887eba5d408a66fae204dbc4e96")
                                    : This annotates each error with the release
  2 Create Release and Associate
    Commits                         : Tell Sentry about a new release and which commits are associated
                                      with it. This allows Sentry to pinpoint which commits likely
                                      caused an issue, and allow team to resolve issues by referencing
                                      the issue number in a commit message
                                    : There are two ways to do this
    Link Repository                 : Use repository integrations (GitHub, GitLab, Bitbucket, ...)
                                      Organization Settings > Integrations
                                      Add a repository
                                    : Must have Admin or Owner permissions
    Associated Commits w/ Release   : Add step in release process to create a Sentry release object and
                                      associate it with commits in liked repository
                                    : import subprocess
                                      import requests
                                      SENTRY_API_TOKEN = <my_api_token>
                                      sha_of_previous_release = <previous_sha>
                                      log = subprocess.Popen([
                                          'git',
                                          '--no-pager',
                                          'log',
                                          '--no-merges',
                                          '--no-color',
                                          '--pretty=%H',
                                          '%s..HEAD' % (sha_of_previous_release,),
                                      ], stdout=subprocess.PIPE)

                                      commits = log.stdout.read().strip().split('\n')
                                      data = {
                                          'commits': [{'id': c, 'repository': 'my-repo-name'} for c in commits],
                                          'version': commits[0],
                                          'projects': ['my-project', 'my-other-project']}

                                      res = requests.post(
                                          'https://sentry.io/api/0/organizations/my-org/releases/',
                                          json=data,
                                          headers={'Authorization': 'Bearer {}'.format(SENTRY_API_TOKEN)})
    After Associating Commits       : Suspect commits and suggested assignees will start appearing on
                                      the issue page. We determine these by tying together the commits
                                      in the release, files touched by those commits, files observed
                                      in the stack trace, authors of those files, and ownership rules
                                    : Will be able to resolve issues by including the issue ID in
                                      your commit message
                                    : Ex commit.
                                      Prevent empty queries on users
                                      Fixes SENTRY-317
  3 Tell Sentry when deployed       : Tell Sentry when you deploy a release. Automatically send
                                      and email to Sentry users who have committed to the release
                                      that is being deployed
- Issue Owners ---------------------: Issue owners feature allows you to reduce noise by directing
                                      notifications to specific teams or users based on a path or URL
                                    : In project settings define which users or teams own paths or URLs for
                                      application. When an exception is caught that triggers aler, Sentry
                                      evaluates whither the exceptions URL tag matches the URL specified
                                      or the path matches in the exception's stack trace
  * Configuration
    Adding a new rule               : Project Settings > Issue Owners
                                      Add rule to specify path or URL to forward to owner
                                    : Can add new rule from individual issue
                                      "Create Ownership Rule"
- Notifications --------------------: Notifications can be routed to many supported integrations
  * Alerts                          : Generated based upon a project's alert rules
                                      Set in Project Settings > Alerts > Rules
    Conditions                      : Rules provide several conditions that you're able to configure
                                      Ex. An issue is seen more than 100 times in one minute.
    Actions                         : Send email, post on Slack, ...
  * Workflow                        : Most activity within Sentry will generate a workflow notification.
                                      Core workflow notifications: assignment, comments, regressions
                                    : By default sent to anyone who is a member of the project the
                                      issue occurs in, but each member may choose their participation state
  * Configure                       : Each category has its own set of configuration and subscription
                                      options. By default subscribed to everything, but this can
                                      be changed in Account Settings > Notifications
  * Issue States                    : Sentry provides a few states for each issue, which greatly impact
                                      how notifications work
    unresolved                      : The default state when an issue is added to the system
    resolved                        : Issue marked resolved when an individual resolves it or when the
                                      project's auto resolve feature is configured
    ignored                         : Regardless of alert rules, new events in ignored issues will not
                                      send alerts.
- Search ---------------------------: Search is available on sentry issues, events, and releases
  * Syntax                          : Queries are constructed using a key:value pattern, with an
                                      optional raw search at the end
                                    : Ex. is:resolved user.username:"Jane Doe" server:web-8 example error
  * Exclusion                       : Use the negation operator "!" to exclude a search parameter
                                      Ex. is:unresolved !user.email:example@customer.com
  * Wildcards                       : Search supports the wildcard operator "*" as a placeholder for
                                      specific characters and strings
                                    : Ex. browser:"Safari 11*"
  * Issue properties                : Issues are aggregate of one or more events
    is                              : Status of issue: resolved, unresolved, ignored, assigned, unassigned
    assigned                        : Filter on assigned to
    bookmarks
    has                             : Value for a specific tag
    first-release
    age                             : Created since age
                                    : Ex new in last 24 hours. age:-24h
                                    : Ex older than 24 hours. age:+24h
                                    : Supported suffixes: m, h, d, w
    timesSeen                       : Restrict to events seen certain amount of times
                                    : Ex. timesSeen:>10
                                    : Ex. timesSeen:>=10
    lastSeen                        : Restrict to when last seen. Usage similar to age
  * Event properties                : Events are the underlying event data captured using Sentry SDKs
    geo.country_code                : geographic area
    geo.region
    geo.city
    release                         : Specific release
    user.id                         : Specific user
    user.email
    user.ip
    event.timestamp                 : Events in certain range
                                    : Ex. event.timestamp:>=2016-01-02T01:00:00 event.timestamp:<2016-01-02T02:00:00
    device.arch                     : Specific device
    device.brand
    device.locale
    device.model_id
    device.uuid
    os.build                        : Specific OS
    os.kernel_version
    stack.abs_path                  : Specific stack
    stack.filename
    stack.function
    stack.module
    stack.stack_level
  * Custom tags                     : Can use any tag that you've specified as a token
                                    : Tags are various key/value pairs that get assigned to an event,
                                      and you can use them later as a breakdown or quick access to finding
                                      related events
  * Saving searches                 : Can save a search by performing search, clicking dropdown and
                                      "Save Current Search"
- Visibility ------------------------
  * Discover                        : Query event data in Sentry, accross any number of projects within
                                      organization
                                    : Uncover patterns and trens in events' data
    Summaries                       : Select event attributes that you want to summarize over
    Aggregations                    : Ex. count, uniq, avg
    Conditions                      : Conditions to filter events from results by field
                                      Ex. IS NULL, =, != LIKE, NOT LIKE
    Order by                        : Order by summary
    Limit
    Project selector
    Date Range
  * Dashboards                      : Various visualizations of errors accross organization
                                      Ex. error graphs, geographic mapping, and list of browsers
  * Events                          : Events view uncovers raw error stream for any group of
                                      projects, including envoronment or time range
- Integrations ---------------------: Sentry integrates seamlessly with favorite apps and services
  * Global Integrations             : Azure Devops, Bitbucket, GitHub, GitHub Enterprise, GitLab, JIRA,
                                      JIRA Server, Slack
  * Per-Project Integrations        : Amazon SQS, Asana, Campfire, Flowdock, GitLab, Heroku, HipChat,
                                      Lighthouse, OpsGenie, PagerDuty, Phabricator, Pivotal Traker,
                                      Pushover, Redmine, Splunk, Taiga, Teamwork, Trello, Twillio
  * GitLab                          : Sentry's GitLab integration helps find and fix bugs faster by using
                                      data from GitLab commits
    Configure                       : In Sentry:
                                        Sentry Settings > Integrations
                                        Find GitLab icon and click install
                                      In Gitlab:
                                        User Settings > Applications
                                        Name: Sentry
                                        Redirect URI: https://sentry.io/extensions/gitlab/setup/
                                        Scopes: api
                                        Save Application
                                        Should see Application ID, Secret, Callback URL, scopes
                                      In Sentry:
                                        Enter Gitlab URL (https://gitlab.com/), GitLab group path,
                                        application ID and application secret
                                        Click enter, authorize in Gitlab
                                        Next to GitLab instance, click Configure
                                        Add Repository, select correct repository
    Issue Management                : Issue tracking allows you to create GitLab issues from
                                      within Sentry and link Sentry issues to existing GitLab
                                      issues
                                    : On issue tracking page, right hand side
                                      Link GitLab issue +
    Commit Tracking                 : Commit tracking allows you to hone in on problematic commits
                                      With commit tracking, can better isolate what might be problematic
                                      by leveraging information from releases like tags and metadata
    Suspect commit                  : Once tracking commits, the 'suspect commit', is the commit that
                                      likely introduced the error. Sentry can pinpoint the commit and
                                      identify the developer
    Resolve via Commit or PR        : Resolve issues by including fixes -ID in your commit messages
                                      Keyword to include is fixes



DATA MANAGEMENT
===============================================================================
- Rollups and Grouping -------------: An important part of Sentry is how it aggregates similar events
                                      together and creates rollups
  * Grouping Priorities             : Grouping switches behavior base on interfaces within the event
                                      - If interfaces uses in an event differn, those events won't be grouped
                                      - If a stack trace or exception is involved in a report, then
                                        grouping will only consider this information
                                      - If a template is involved, then grouping will consider the template
                                      - As a fallback, the message will be used for grouping
    By stacktrace                   : When Sentry detects a stack trace in the event data, the grouping
                                      effectively is based entirely on the stack trace
                                    : Ensure that Sentry can access your Source Maps
    By exception                    : If no stack trace, group by type, and value of excpetion
    By Template
    Fallback grouping               : If nothing else, Grouping falls back to message
    Customized with fingerprints    : For advanced use cases, can override the Sentry default grouping
                                      using the fingerprint attribute
                                    : Can be used to split apart large default groupings, or merge multiple
                                      small groupings together
- Sensitive Data -------------------: Important to know what data is being sent to Sentry, and that sensitive
                                      is handled appropriately
  * Custom Event Processing         : Use before-send function to modify event data and remove sensitive
                                      data
  * Server-Side Scrubbing           : Project Settings > Data Privacy > Data Scrubber
                                    : When scrubbing enabled, sentry scrubs values that look like credit
                                      cards, password, secret, passwd, api_key, apikey, access_token,
                                      auth_token, credentials, mysql_pwd, stripetoken, card
                                    : Can add additional values to "Additional Sensitve Fields"
  * Restricting Emails              : Organization Settings > Enhanced Privacy
                                      Will begin restricting email data to only basic attributes
  * Removing data                   : Can be difficult to full remove data from sentry
                                      In worst case, may need to remove and readd project
- Data Forwarding ------------------: Sentry provides the ability to forward processed events to certain
                                      third-party providers such as segment or Amazon SQS
                                    : Project > Settings > Data Forwarding



USER ACCOUNTS AND ORGANIZATIONS
===============================================================================
- Membership -----------------------: Membership in Sentry is handled at the organizational level.
                                      The system is designed so each user has a singular account
                                      which can be reused across multiple organizations (even those
                                      using SSO)
                                    : Each user has their own account, and will then be able to set
                                      their own personal preferences in addition to receiving
                                      notifications for events
  * Roles                           : Access to organizations is dictated by roles. Roles are scoped
                                      to the organization: Owner, Manager, Admin, Member, Billing
                                    : Access to organizations is dictated by Roles, which is scoped
                                      to entire Organization
- Quotas & Filtering ---------------: Each subscription tier in Sentry provides different monthly
                                      quotas for your event capacity. If capacity is consuped
                                      server responds with HTTP 429
  * Increasing                      : Can add additional quota at any time during billing period
                                      either by ipgrading to high tiero or increasing on-demand
                                      capacity. sales@sentry.io
  * Rate Limiting Projects          : Per-key rate limits allow you to set the maximum volume of
                                      events a key will accept during a period of time
                                      Project Settings > Client Keys (DSN) > Configure > Rate Limit
  * Inbound filters                 : Can filter server side before rate limits are checked
                                    : Project Settings > Inbound Filters
    Built in
    IP Blocklist                    : Block rogue clients
    By Releases
    By Error Message
    By Issue
    Spike Protection                : Prevent huge overages, flooding to consuming event capacity
                                      Organization Settings > Usage and Billing > Subscript > Spike Protection
  * Controlling volume              : Configure SDK to reduce volume being sent
                                      Turn on inbound filters for legacy browsers, extensions,
                                      localhost, and web crawlers
                                      Set per-key rate limts for each DSN key in a project
- 2 Factor Authentication ----------: Can require organization personel to do 2 factor authentication
- Single Sign-On (SSO) -------------: Allow you to manage organization entire membership via a third-
                                      party provider



PYTHON PLATFORM
===============================================================================
- Description  ---------------------: Sentry Python SDK provides support for Python 2.7 and 3.4 or later
  * Installation                    : pip3 install sentry-sdk
- Integrations ---------------------: Integrations extend the functionality of the SDK for some common
                                      frameworks and libraries
  * Web Frameworks                  : Django, Flask, Sanic, Pyramid, AIOHTTP, Tornado, Bottle, Generic WSGI
  * Task Queues                     : Celery, RQ (Redis Queue)
  * Serverless                      : AWS Lambda, Generic Serverless
  * Other Integrations              : Logging, GNU Backtrace, Default Integrations
  * Hints                           : Python SDK provides some common hints for breadcrumbs and events.
                                      These hints are passed as the hint parameter to before_send and
                                      before_breadcrumb
                                      exc_info        - (exc_type, exc_value, tb)
                                      log_record      - log_record that created hint
                                      httplib_request - httplib request object
- Flask Integration -----------------
  * Install                         : pip3 install --upgrade 'sentry-sdk[flask]==0.7.10'
  * Import                          : sentry_sdk.integrations.flask.FlaskIntegration
  * Init                            : import sentry_sdk
                                      from sentry_sdk.integrations.flask import FlaskIntegration
                                      sentry_sdk.init(
                                          dsn="https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
                                          integrations=[FlaskIntegration()])
                                      app = Flask(__name__)
  * Behavior                        : Sentry Python SDK will install the flask integration for all apps,
                                      hooks into Flask's signals
                                    : All exceptions leading to an Internal Server Error reported
                                    : Request data is attached to all events: HTTP method, URL, headers,
                                      form data, JSON payloads
- GNU Backtrace Integration ---------
  * Import                          : sentry_sdk.integrations.gnu_backtrace.GnuBacktraceIntegration
  * Behavior                        : Integration parses native stack trace produced by backtrace_symbols
                                      and concatenates them with the Python traceback
                                    : Integration is experimental
- Logging Integration ---------------
  * Import                          : sentry_sdk.integrations.logging.LoggingIntegration
  * Init                            : import logging
                                      import sentry_sdk
                                      from sentry_sdk.integrations.logging import LoggingIntegration

                                      # All of this is already happening by default!
                                      sentry_logging = LoggingIntegration(
                                          level=logging.INFO,        # Capture info and above as breadcrumbs
                                          event_level=logging.ERROR  # Send errors as events
                                      )
                                      sentry_sdk.init(
                                          dsn="https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
                                          integrations=[sentry_logging]
                                      )
  * Usage                           : import logging
                                      logging.info("I am a breadcrumb")
                                      logging.error("I am an event", extra=dict(bar=43))
                                      logging.error("An exception happened", exc_info=True)
                                    : Info captured as a breadcrumb
                                    : Error sent as an event, bar will be an event attribute
                                    : exc_info true causes stack trace to be sent
  * Ignoring a logger               : from sentry_sdk.integrations.logging import ignore_logger
                                      ignore_logger("a.spammy.logger")
                                      logger = logging.getLogger("a.spammy.logger")
                                      logger.error("hi")  # no error sent to sentry
  * Options                         : level (default INFO): Sentry SDK will record log records with a
                                        level >= level, and ignore the rest
                                      event_level (default ERROR): Python SDK will report log records
                                        with a level higher than or equal to event_level as events
- Serverless Integration ------------
  * Import                          : sentry_sdk.integrations.serverless.serverless_function
  * Init                            : import sentry_sdk
                                      from sentry_sdk.integrations.serverless import serverless_function
                                      sentry_sdk.init(dsn="https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526")
                                      @serverless_function
                                      def my_function(...):
                                          ...
  * Behavior                        : Exceptions raising from those functions will be reported to Sentry
                                    : Each call of a decorated function will block and wait for current
                                      events to be sent before returning
                                    : When there are no events to be sent, this will not add a delay.
                                      However, if there are errors, this delay will delay the return
                                      of your serverless functions until the events are sent
                                      Control shutdown time with shutdown_timeout client option
- AWS Lambda Integration ------------
  * Import                          : sentry_sdk.integrations.aws_lambda.AwsLambdaIntegration
  * Init                            : import sentry_sdk
                                      from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration
                                      sentry_sdk.init(
                                          dsn="https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526",
                                          integrations=[AwsLambdaIntegration()])
  * Behavior                        : Automatically report all uncaught exceptions from lambda functions
                                    : Request data is attached to all events: HTTP method, URL, headers,
                                      form data, JSON payloads
                                    : Similar to generic serverless integration
- Default Integrations -------------: System integrations are enabled by default and tie into standard
                                      library or the interpreter itself
  * To disable                      : init(..., default_integrations=False)
  * Atexit                          : sentry_sdk.integrations.atexit.AtexitIntegration
                                    : Integrates with interpreter's atexit system to automatically flush
                                      events from the background queue on interpreter shutdown
                                      Disable by setting shutdown_timeout to 0
  * Excepthook                      : sentry_sdk.integrations.excepthook.ExcepthookIntegration
                                    : Registers with the interpreter's except hook system
                                      Any exception that is unhandled will be reported to Sentry
                                      automatically
  * Deduplication                   : sentry_sdk.integrations.dedupe.DedupeIntegration
                                    : Deduplicates certain events
  * Stdlib                          : sentry_sdk.integrations.stdlib.StdlibIntegration
                                    : Instruments certain modules in the standard libary to emit breadcrumbs.
                                      The Sentry Python SDK enables this by default
  * Modules                         : sentry_sdk.integrations.modules.ModulesIntegration
                                      Sends a list of installed Python packages along with each event
  * Argv                            : sentry_sdk.integrations.argv.ArgvIntegration
                                    : Adds sys.argv as extra attribute to each event
  * Logging                         : sentry_sdk.integrations.logging.LoggingIntegration
  * Threading                       : sentry_sdk.integrations.threading.ThreadingIntegration
                                      Reports crashing threads.
                                      Accepts an option propagate_hub that changes the way clients are
                                      transferred between threads, and transfoers scope data (such as tags)
                                      from the parent thread to the child thread



JAVASCRIPT PLATFORM
===============================================================================
- Description -----------------------
  * Installation                    : yarn add @sentry/node@5.0.6
                                    : nmp install @sentry/node@5.0.6
  * Connect to Sentry               : import * as Sentry from '@sentry/node';
                                      Sentry.init({
                                        dsn: 'https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526',
                                        integrations: [new MyAwesomeIntegration()]
                                      });
  * Verify Setup                    : Sentry.captureException(new Error("This is my fake error message"));
  * Capture Errors                  : try {
                                        aFunctionThatMightFail();
                                      } catch (err) {
                                        Sentry.captureException(err);
                                      }
  * Auto Error Capture              : By including and configuring the Sentry Browser SDK, Sentry
                                      will automatically attach global handlers to capture uncaught
                                      exceptions and unhandled rejections
  * Souce Maps                      : Most modern JS transpilers support source maps
    UglifyJS                        : Tool for minifying source code for production
                                    : Generate map: uglifyjs app.js -o app.min.js.map --source-map url=app.min.js.map,includeSources
    WebPack                         : Sentry webpack plugin that configures source maps and uploads them to
                                      Sentry during the build
- Vue Integration -------------------
  * Installation                    : npm install @sentry/browser
                                      npm install @sentry/integrations
  * Usage                           : import Vue from 'vue'
                                      import * as Sentry from '@sentry/browser';
                                      import * as Integrations from '@sentry/integrations';

                                      Sentry.init({
                                        dsn: 'https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526',
                                        integrations: [
                                          new Integrations.Vue({
                                            Vue,
                                            attachProps: true,
                                          }),
                                        ],
                                      });
  * Usage Via CDN                   : <script src="https://browser.sentry-cdn.com/5.0.6/bundle.min.js" crossorigin="anonymous"></script>
                                      <script src="https://browser.sentry-cdn.com/5.0.6/vue.min.js" crossorigin="anonymous"></script>
                                      <script>
                                        Sentry.init({
                                          dsn: 'https://3cbcc98690934b1681e721dea4caf2cc@sentry.io/1426526',
                                          integrations: [
                                            new Sentry.Integrations.Vue({
                                              Vue,
                                              attachProps: true,
                                            }),
                                          ],
                                        });
                                      </script>
  * Behavior                        : Report any uncaught exceptions triggered from application
                                    : Vue integration will capture name and props state of the active
                                      component where the error was thrown
- Default Integrations -------------: System integrations are enabled by default
  * To disable                      : init(..., default_integrations:false)
  * Override settings               : provide a new instance
                                      init(..., integrations: [new Sentry.Integrations.Breadcrumbs({ console: false })].)
  * InboundFilters                  : Ingnore specific errors based on the type of the message,
                                      as well as blacklist/whitelist urls which exception originates
                                      from.
  * FunctionToString                : SDK can provide original functions and method names, even when they
                                      are wrapped by error breadcrumb handlers
  * TryCatch                        : Integration wraps native time and events APIs in try catch blocks
                                      to handle async exceptions
  * Breadcrumbs                     : Wrap native APIs to capture breadcrumbs
                                    : {
                                        beacon: boolean;  // Log HTTP requests done with the Beacon API
                                        console: boolean; // Log calls to `console.log`, `console.debug`, etc
                                        dom: boolean;     // Log all click and keypress events
                                        fetch: boolean;   // Log HTTP requests done with the Fetch API
                                        history: boolean; // Log calls to `history.pushState` and friends
                                        sentry: boolean;  // Log whenever we send an event to the server
                                        xhr: boolean;     // Log HTTP requests done with the XHR API
                                      }
  * GlobalHandlers                  : Attach global handlers to capture uncaught exceptions and
                                      unhandled rejections
                                    : {
                                        onerror: boolean;
                                        onunhandledrejection: boolean;
                                      }
  * LinkedErrors                    : Configure linked errors. They'll be recursively read up to a
                                      specified limit and lookup will be performed by a specific key
                                    : {
                                        key: string;
                                        limit: number;
                                      }
  * UserAgent                       : Attaches user-agent information to the event, which allows correct
                                      catalogue and tagging with specific OS, Browser, and version
                                      informations
- Pluggable Integrations -----------: Integrations that can be additionally enabled, to provide some
                                      very specific features.
  * Dedupe                          : Deduplicates certain events, enabled by default
  * Debug                           : Easliy inspect the content of the prcessed event
                                    : {
                                        debugger: boolean; // trigger DevTools debugger instead of using console.log
                                        stringify: boolean; // stringify event before passing it to console.log
                                      }
  * ExtraErrorData                  : Extracts all non-native attributes from the Error object and attaches
                                      them to the event as extra data
                                    : {
                                        depth: number; // limit of how deep the object serializer should go. Anything deeper then limit will be replaced with standard Node.js REPL notation of [Object], [Array], [Function] or primitive value. Defaults to 3.
                                      }
  * RewriteFrames                   : Apply transformation to each frame on the stack trace
                                    : {
                                        root: string; // root path that will be appended to the basename of the current frame's url
                                        iteratee: (frame) => frame); // function that take the frame, apply any transformation on it and returns it back
                                      }
  * ReportingObserver               : Integration hooks into ReportingObserver API
                                    : {
                                        types: <'crash'|'deprecation'|'intervention'>[];
                                      }
- Loader ---------------------------: Loader loads
- Sentry Testkit -------------------: When building tests for application, want to assert that the right
                                      flow-tracking or error is being sent to Sentry, but without really sending
                                      it to the Sentry servers. Don't want to swamp Sentry with false reports
                                    : Sentry Testkit is a Sentry plugin that allows intercepting Sentry's reports
                                      and further inspection of the data being sent
  * Install                         : npm install sentry-testkit --save-dev
  * Using                           : const sentryTestkit = require("sentry-testkit")
                                      const { testkit, sentryTransport } = sentryTestkit()

                                      // initialize your Sentry instance with sentryTransport
                                      Sentry.init({
                                        dsn: 'some_dummy_dsn',
                                        transport: sentryTransport
                                        //... other configurations
                                      })

                                      // then run any scenario that should call Sentry.catchException(...)
                                      expect(testKit.reports()).toHaveLength(1)
                                      const report = testKit.reports()[0]
                                      expect(report).toHaveProperty(/*...*/)



COMMAND LINE INTERFACE
===============================================================================
- Description ----------------------: For certain actions, you can use the sentry-cli command line executable.
                                      If connects to the Sentry API and manages data for projects
- Installation ----------------------
  * Install (via sentry.sh)         : sentry_install
  * Update (via sentry.sh)          : sentry_update
  * Uninstall (via sentry.sh)       : sentry_uninstall
  * Help                            : sentry-cli --help
- Authentication Options ------------
  * Login                           : sentry-login
  * ~/.sentryclirc                  : [auth]
                                      token=your-auth-token
  * Environment                     : export SENTRY_AUTH_TOKEN=your-auth-token
  * Parameter                       : sentry-cli --auth-token your-auth-token
- Configuration --------------------: Need token with scopes: project:read, project:releases, org:read
  * File                            : ~/.sentryclirc, .env files
                                    : INI Syntax
  * Values
    SENTRY_AUTH_TOKEN
    SENTRY_URL                      : Default https://sentry.io/
    SENTRY_ORG                      : Ex. beblsoft
    SENTRY_PROJECT                  : Ex. smeckn
    http_proxy
    SENTRY_HTTP_MAX_RETRIES
    SENTRY_LOG_LEVEL                : Default warning
    SENTRY_NO_PROGRESS_BAR
    SENTRY_DISABLE_UPDATE_CHECK
    DEVICE_FAMILY
    DEVICE_MODEL
  * Validate                        : sentry-cli info
- Releases --------------------------
  * Creating                        : sentry-cli releases new
  * Finalizing                      : Release is created unreleased
                                      Can be changed by passing the --finalized flag to the new command
                                      sentry-cli releases finalize VERSION
  * Propose version                 : VERSION=`sentry-cli releases propose-version`
  * Commit integration              : Associate commits with release
                                    : Ex. Sentry has access to sources
                                      sentry-cli releases set-commits "$VERSION" --auto
  * Artifacts                       : When working with JS or other platforms, can upload release
                                      artifacts to Sentry which are then considered during processing
                                    : Most common are source maps
                                    : sentry-cli releases files VERSION upload /path/to/file '~/file.js'
                                    : sentry-cli releases files VERSION upload-sourcemaps /path/to/sourcemaps
  * List files                      : sentry-cli releases files VERSION list
  * Delete files                    : sentry-cli releases files VERSION delete --all
  * Create deploy                   : sentry-cli releases deploys VERSION new -e ENVIRONMENT
- Debug Information Files ----------: Validate and upload debug information files (dSYM, Proguard files, etc)
- Sending events -------------------: Send events directly from cli
                                    : export SENTRY_DSN=https://3cbcc98690934b1681e721dea4caf2cc:97da6aef85ea4accb1092ef172c134f9@sentry.io/1426526
                                    : sentry-cli send-event -m "Hello from Sentry"
  * Bash hook                       : For bash scripts, can also enable automatic error sending using the
                                      sentry-cli bash hook. This enables set -e and will send a sentry
                                      event for unhandled errors
                                    : #!/bin/bash
                                      export SENTRY_DSN=https://3cbcc98690934b1681e721dea4caf2cc:97da6aef85ea4accb1092ef172c134f9@sentry.io/1426526
                                      eval "$(sentry-cli bash-hook)"
                                      # rest of the script goes here



API REFERENCE
===============================================================================
- Description ----------------------: The Sentry API is used for submitting events to the Sentry
                                      collector as well as exporting and managing data.
                                    : Reporting an web APIs are individually versioned
- Versioning -----------------------: Current version of the webAPI is known as v0
                                      Considered to be in draft phase
- Requests -------------------------: All API requests should be made to the /api/0/ prefix
                                    : All return JSON as a response
                                    : Ex.
                                      curl -i https://sentry.io/api/0/
                                      HTTP/1.0 200 OK
                                      Date: Sat, 14 Feb 2015 18:47:20 GMT
                                      Content-Type: application/json
                                      Content-Language: en
                                      Allow: GET, HEAD, OPTIONS
                                      {"version": "0"}
  * HTTP Verbs
    DELETE                          : Used for deleting
    GET                             : Used for retrieving
    OPTIONS                         : Used for describing
    POST                            : Used for creating
    PUT                             : Used for updating
  * Parameters and Data             : All parameters not in URL should be encoded as JSON
                                      with Content-Type "application/json"
                                    : Ex.
                                      $ curl -i https://sentry.io/api/0/projects/1/groups/ \
                                             -d '{"status": "resolved"}' \
                                             -H 'Content-Type: application/json'
- Authentication -------------------: Passed using an auth header, and are used to authenticate as a user account
                                    : Ex.
                                      $ curl -H 'Authorization: Bearer {TOKEN}' \
                                             https://sentry.io/api/0/projects/
  * Create new key                  : Account Settings > Auth Tokens
- Pagination -----------------------: Handled in the API via the Link header standard
                                    : When supported, cursors will always be returned for both a previous
                                      and next page, even if there are no results on these pages
                                    : To help understand if you actually need to paginate results="[true|false]"
                                      is also returned
- Issues ----------------------------
  * List                            : GET /api/0/projects/{organization_slug}/{project_slug}/issues/
  * Retrieve                        : GET /api/0/issues/{issue_id}/
  * Update                          : PUT /api/0/issues/{issue_id}/
  * Remove                          : DELETE /api/0/issues/{issue_id}/
  * List Events
  * List Hashes
  * Retrieve Tag Details
  * List Tag values
  * Retrieve newest event
  * Retrieve oldest event
  * Bulk mutate
  * Bulk remove
- Events ----------------------------
  * List                            : GET /api/0/projects/{organization_slug}/{project_slug}/events/
  * Resolve                         : GET /api/0/organizations/{organization_slug}/eventids/{event_id}/
  * Retrieve                        : GET /api/0/projects/{organization_slug}/{project_slug}/events/{event_id}/
  * Retrieve Counts                 : GET /api/0/projects/{organization_slug}/{project_slug}/stats/
- User Feedback ---------------------
  * List for project
  * Submit
- Releases --------------------------
  * List                            : GET /api/0/organizations/{organization_slug}/releases/
  * Retrieve                        : GET /api/0/organizations/{organization_slug}/releases/{version}/
  * Create                          : POST /api/0/organizations/{organization_slug}/releases/
  * Update                          : PUT /api/0/organizations/{organization_slug}/releases/{version}/
  * Delete                          : DELETE /api/0/organizations/{organization_slug}/releases/{version}/
  * List issues
  * List deploys
  * Create deploy
  * List files
  * Retrieve file
  * Upload file
  * Update file
  * Delete file
  * List commits
  * Retrieve files changed
- Organizations ---------------------
  * Create                          : POST /api/0/organizations/
  * Delete                          : DELETE /api/0/organizations/{organization_slug}/
  * List repo commits
  * List projects
  * List repos
  * List
  * Retrieve
  * Retrieve event counts
  * Update
- Projects --------------------------
  * List                            : GET /api/0/projects/
  * Create                          : POST /api/0/teams/{organization_slug}/{team_slug}/projects/
  * Retrieve                        : GET /api/0/projects/{organization_slug}/{project_slug}/
  * Update                          : PUT /api/0/projects/{organization_slug}/{project_slug}/
  * Delete                          : DELETE /api/0/projects/{organization_slug}/{project_slug}/
  * List users
  * List tags's values
  * List by teams
  * List client keys
  * Create client key
  * Update client key
  * Delete client key
  * List service hooks
  * Retrieve a service hook
  * Create a service hook
  * Update a service hook
  * Remove a service hook
- Teams -----------------------------
  * List                            : GET /api/0/organizations/{organization_slug}/teams/
  * Create                          : POST /api/0/organizations/{organization_slug}/teams/
  * Retrieve                        : GET /api/0/teams/{organization_slug}/{team_slug}/
  * Update                          : PUT /api/0/teams/{organization_slug}/{team_slug}/
  * Delete                          : DELETE /api/0/teams/{organization_slug}/{team_slug}/



ON PREMISE
===============================================================================
- Description ----------------------: The Sentry Server is the core Python application that powers
                                      all of the Sentry installations.
                                    : Can run a Sentry server on your own infrastructure
- Installation ----------------------
  * Minimum Services                : PostgresSQL, Docker postgres:9.5
                                    : Redis > 3.0
                                    : Subdomain to host Sentry (sentry.yourcompany.com)
  * Docker Install
  * Python Install
