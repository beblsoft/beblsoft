# MySQL Projects Documentation

## sql-demos

All examples are located in `./sql-demos` directory.

### limbs.sql

Show basic syntax to create, populate, and destroy a database.

To Run: `mysql < limbs.sql`

### my.cnf

Example MySQL configuration file

To set as default: `ln -s <this program> ~/.my.cnf`
