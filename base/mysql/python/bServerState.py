#!/usr/bin/env python3
"""
NAME:
 bServerState.py

DESCRIPTION
 Beblsoft MySQL Server State Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import enum
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL SERVER STATE ---------------------- #
class BeblsoftMySQLServerState(enum.Enum):
    """
    Beblsoft MySQL Server State
    """

    ONLINE  = 0
    OFFLINE = 1
