#!/usr/bin/env python3
"""
NAME:
 bDatabaseMigrations.py

DESCRIPTION
 Beblsoft MySQL Database Migrations Functionality

SEE ALSO:
 Alembic Configuration  : https://alembic.zzzcomputing.com/en/latest/api/config.html
 Alembic Commands       : https://alembic.zzzcomputing.com/en/latest/api/commands.html#alembic-command-toplevel

"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
import glob
import re
import logging
import alembic.config  # pylint: disable=E0401
import alembic.command
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.stream.bStream import BeblsoftStream


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL DATABASE MIGRATIONS --------------- #
class BeblsoftMySQLDatabaseMigrations():
    """
    Beblsoft MySQL Database
    """

    def __init__(self, bDatabase, alembicConfigPath, alembicVersionDir):
        """
        Initialize object
        Args
          bDatabase:
            BeblsoftMySQLDatabase Object
          alembicConfigPath:
            Path to alembic configuration (alembic.ini)
            Ex. "/home/jbensson/beblsoft/projects/smeckn/server/gDBMigrations/alembic.ini"
          alembicVersionDir:
            Directory containing alembic version scripts
            Ex. /home/jbensson/beblsoft/projects/smeckn/server/gDBMigrations/versions"

        Notes
          All alembic commands will be invoked with -x engineStr=<engineStr>
          to tell the alembic engine the correct directory
          See runAlembicCommand below.
        """
        self.bDatabase         = bDatabase
        self.alembicConfigPath = alembicConfigPath
        self.alembicVersionDir = alembicVersionDir

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def version(self):
        """
        Version
        Returns
          '' or valid version
        """
        version = ''
        with self.bDatabase.sessionScope() as s:
            try:
                rs      = s.execute("SELECT * from alembic_version;")
                version = rs.fetchone()[0]
            except TypeError as _:
                version = ''
        return version

    @property
    def history(self):
        """
        Return list of versions from oldest to newest

        Ex. [ 'first', 'second', .. 'newer2', ... 'newest']
        """
        bufList       = []
        versionList   = []
        alembicConfig = self.getAlembicConfig(writeFunc=lambda b: bufList.append(b)) #pylint: disable=W0108
        alembic.command.history(alembicConfig)
        # bufList:
        # ['2dc0cb93d4c3 -> 5c2c05639532 (head), Add_dbVersion_to_account_table', '\n',
        #  '<base> -> 2dc0cb93d4c3, Add account table and accountDBServer table', '\n']
        # Build versionList newest to oldest and then return flipped
        for elem in bufList:
            match = re.match(r"<?(\w*)>? -> (\w*).*", elem)
            if match:
                versionList.append(match.group(2)) # 2nd version. I.e. On First line: 5c2c05639532
        versionList.reverse()
        return versionList


    # ---------------------- CONFIG ----------------------------------------- #
    @logFunc()
    def getAlembicConfig(self, writeFunc=logger.debug):
        """
        Return alembic config object

        """
        bLogStream = BeblsoftStream(writeFunc=writeFunc)
        cmdOpts    = BeblsoftAttrDict()
        cmdOpts.x  = ["engineStr={}".format(self.bDatabase.engineStr)]
        config     = alembic.config.Config(
            file_    = self.alembicConfigPath,
            stdout   = bLogStream,
            cmd_opts = cmdOpts)
        return config

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def stamp(self, version):
        """
        Stamp migration version on database
        """
        alembic.command.stamp(self.getAlembicConfig(), revision=version)

    @logFunc()
    def revise(self, autogenerate=True, msg=None):
        """
        Revise
        Args
          autogenerate:
            If True, autogenerate migration scripts
          msg:
            Revision message
        """
        alembic.command.revision(self.getAlembicConfig(), message=msg,
                                 autogenerate=autogenerate)
        # Give all version files full permissions
        for file in glob.glob("{}/*.py".format(self.alembicVersionDir)):
            os.chmod(file, 0o777)

    @logFunc()
    def upgrade(self, toVersion="head"):
        """
        Upgrade
        """
        alembic.command.upgrade(self.getAlembicConfig(), revision=toVersion)

    @logFunc()
    def downgrade(self, toVersion):
        """
        Downgrade
        """
        alembic.command.downgrade(self.getAlembicConfig(), revision=toVersion)

    @logFunc()
    def describeVersionData(self, version="head"):
        """
        Describe version data
        Logs:
          Rev: 9760145e52bd
          Parent: <base>
          Path: /home/jbensson/git/beblsoft/playground/alembic/singleDB/migrations/versions/2018-11-01_14.38.02_9760145e52bd.py
              Initial Schema
              Revision ID: 9760145e52bd
              Revises:
              Create Date: 2018-11-01 14:38:02.494679-05:00
        """
        bufList       = []
        alembicConfig = self.getAlembicConfig(writeFunc=lambda b: bufList.append(b)) #pylint: disable=W0108
        alembic.command.show(alembicConfig, rev=version)
        for buf in bufList:
            logger.info(buf)

    @logFunc()
    def describe(self):
        """
        Describe migrations
        """
        msg  = "Version : {}\n".format(self.version)
        msg += "History : {}\n".format(self.history)
        logger.info(msg)
        return msg
