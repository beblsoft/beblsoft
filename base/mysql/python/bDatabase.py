#!/usr/bin/env python3
"""
NAME:
 bDatabase.py

DESCRIPTION
 Beblsoft MySQL Database Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import traceback
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.pool import NullPool
from sqlalchemy.exc import OperationalError
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL DATABASE -------------------------- #
class BeblsoftMySQLDatabase():
    """
    Beblsoft MySQL Database
    """

    def __init__(self, bServer, name, charset="utf8mb4", collation="utf8mb4_unicode_ci",
                 echo=False, poolClass=NullPool,
                 alembicConfigPath=None, alembicVersionDir=None):
        """
        Initialize object
        Args
          bServer:
            BeblsoftMySQLServer Object
          name:
            User name
            Ex. "jbensson"
          charset:
            Database character set
            Ex. "utf8"
          collation:
            Collation refers to a set of rules that determine how data is sorted and compared.
            Character data is sorted using rules that define the correct character sequence, with options for specifying case-sensitivity, accent marks, kana character types and character width.
            Ex. "utf8mb4_unicode_ci"
            To check mysql database charset and collation
              SELECT DEFAULT_CHARACTER_SET_NAME, DEFAULT_COLLATION_NAME FROM
              INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'db_name';
          echo:
            If True, echo database commands
          poolClass:
            SqlAlchemy connection pool class
            Ex. NullPool (one connection per session)
          alembicConfigPath:
            See bDatabaseMigrations
          alembicVersionDir:
            See bDatabaseMigrations
        """
        self.bServer         = bServer
        self.name            = name
        self.charset         = charset
        self.collation       = collation
        self.echo            = echo
        self._engine         = None
        self._session        = None
        self.poolClass       = poolClass

        # Migrations, only import if necessary
        if alembicConfigPath:
            from base.mysql.python.bDatabaseMigrations import BeblsoftMySQLDatabaseMigrations
            self.bDBMigrations      = BeblsoftMySQLDatabaseMigrations(
                bDatabase              = self,
                alembicConfigPath      = alembicConfigPath,
                alembicVersionDir      = alembicVersionDir)
        else:
            self.bDBMigrations      = None

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    # ---------------------- ALTERNATE CONSTRUCTORS ------------------------- #
    @staticmethod
    @logFunc()
    def fromSession(session):
        """
        Return database from session
        """
        return session.bDatabase

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def engine(self):
        """
        Return engine
        """
        if not self._engine:
            self._engine    = create_engine(
                name_or_url    = self.engineStr,
                echo           = self.echo,
                poolclass      = self.poolClass)
        return self._engine

    @property
    def engineStr(self):
        """
        Return engine string
        """
        eStr = "mysql://{}:{}@{}:{}/{}?charset={}".format(
            self.bServer.user, self.bServer.password, self.bServer.domainName,
            self.bServer.port, self.name, self.charset)
        return eStr

    @property
    def session(self):
        """
        Return session
        """
        if not self._session:
            sessionMaker            = sessionmaker(
                autocommit            = False,
                autoflush             = True,
                bind                  = self.engine)
            self._session           = scoped_session(sessionMaker)
            self._session.bDatabase = self  # Backpointer
        return self._session

    @property
    def exists(self):
        """
        Return True if database exists
        """
        exists = False
        s      = self.bServer.session
        rs     = s.execute("SHOW DATABASES;")
        for row in rs:
            if self.name == row[0]:
                exists = True
                break
        return exists

    @property
    def tables(self):
        """
        Return list of tables
        """
        tables = []
        with self.sessionScope() as s:
            rs = s.execute("SHOW TABLES;")
            tables = [list(row)[0] for row in rs.fetchall()]
        return tables

    # ---------------------- CRUD ------------------------------------------- #
    @logFunc()
    def create(self, stamp=True):
        """
        Create database
        Args
          stamp:
            If True, stamp the migration base version
        """
        if self.exists:
            logger.debug("{} Already exists".format(self))
        else:
            with self.bServer.sessionScope() as s:
                s.execute("CREATE DATABASE {} CHARACTER SET {} COLLATE {}".format(
                    self.name, self.charset, self.collation))
            if self.bDBMigrations and stamp:
                self.bDBMigrations.stamp(version="base")
            logger.debug("{} Created".format(self))

    @logFunc()
    def delete(self):
        """
        Delete database
        """
        if self.exists:
            try:
                with self.bServer.sessionScope() as s:
                    s.execute("DROP DATABASE {}".format(self.name))
            except OperationalError as e:
                if "database doesn't exist" in str(e):  # Database already deleted
                    pass
                else:
                    raise e
            logger.debug("{} Dropped".format(self))
        else:
            logger.debug("{} Never existed".format(self))

    @logFunc()
    def rename(self, newName):
        """
        Rename database
        """
        # Create new tmp db object before we rename ourselves
        oldName = self.name
        tmpDB   = BeblsoftMySQLDatabase(self.bServer, newName)
        tmpDB.create()

        # Rename all old tables
        with self.bServer.sessionScope() as s:
            s.execute("USE {}".format(self.name))
            rs = s.execute("SHOW TABLES")
            for row in rs:
                s.execute("RENAME TABLE {}.{} to {}.{};".format(
                    self.name, row[0], newName, row[0]))

        # Delete old db
        self.delete()
        self.name = newName
        logger.debug("{} Renamed to {}->{}".format(self, oldName, newName))

    @logFunc()
    def describe(self):
        """
        Describe database
        """
        msg = "{} Info:\n".format(self)
        msg += "Tables  : {}\n".format(self.tables)
        logger.info(msg)
        if self.bDBMigrations:
            msg += self.bDBMigrations.describe()
        return msg

    # --------------------- SESSION ----------------------------------------- #
    @contextmanager
    def sessionScope(self, commit=True):
        """
        Provide a session scope around a series of operations

        Args
          commit:
            If True, commit session after context completes
            Note, if a commit occurs the session will be cleared
        """
        try:
            # Note:
            #  - yield statement must be put inside try block so session is
            #    closed when an exception occurs
            #  - When yield was above try statement, hang was caused in
            #    LoginTestCase.test_valid. Teardown code was trying to remove
            #    a database that contained an open connection
            yield self.session
            if commit:
                self.session.commit()
        except Exception as e:
            logger.debug(traceback.format_exc())
            self.session.rollback()
            raise e
        finally:
            self.session.close()

    @staticmethod
    @contextmanager
    def transactionScope(session, nested=False):
        """
        Provide a transactional scope around a series of operations within
        an existing session

        Args
          session:
            sqlalchemy session in which to begin transaction
          nested:
            If True, preform work in a nested transaction
            Changes from nested transaction DO NOT persist until parent transaction has been committed

        Bibliography
          https://docs.sqlalchemy.org/en/latest/orm/session_transaction.html#session-begin-nested
        """
        try:
            if nested:
                session.begin_nested()
            yield
            session.commit()
        except Exception as e:
            logger.debug(traceback.format_exc())
            session.rollback()
            raise e

    # --------------------- LOCKING ----------------------------------------- #
    @staticmethod
    @contextmanager
    def lockTables(session, lockStr):
        """
        Provide a table locked scope around a series of operations
        Locking tables begins its own transaction

        Args
          session:
            sqlalchemy session in which to lock tables
          lockStr:
            String to lock tables
            Ex. "ProfileGroup WRITE, Profile READ"
            Note: Lock must be in CAPS

        Note:
          Session is unconditionally committed after context completes

        Bibliography
          https://dev.mysql.com/doc/refman/5.6/en/lock-tables.html
        """
        try:
            session.execute("LOCK TABLES {}".format(lockStr))
            setattr(session, "LOCKSTR", lockStr)
            yield session
            session.commit()
        except Exception as e:
            logger.debug(traceback.format_exc())
            session.rollback()
            raise e
        finally:
            if getattr(session, "LOCKSTR", None):
                delattr(session, "LOCKSTR")
            session.execute("UNLOCK TABLES")

    @staticmethod
    @contextmanager
    def lockAssert(session, lockStr):
        """
        Assert that all locks in lockStr have been taken
        """
        actualLockStr  = getattr(session, "LOCKSTR", "")
        actualLockList = [x.strip() for x in actualLockStr.split(",")]
        lockList       = [x.strip() for x in lockStr.split(",")]
        for lock in lockList:
            if not any(lock in s for s in actualLockList):
                raise BeblsoftError(BeblsoftErrorCode.MYSQL_LOCK_NOT_HELD,
                                    intMsgFormatDict={"lock": lock})

    # ---------------------- RUN SQL ---------------------------------------- #
    def runSQL(self, sqlStatement, rowsAsLists=True):
        """
        Run SQL on database
        Args:
          sqlStatement
            SQL Statement to execute on database
            Ex. "SHOW TABLES;"
          rowsAsLists:
            Return rows as lists, not tuples

        Returns:
          List of result set rows
          Ex. "SHOW TABLES;" could return
          [
            [ "Alembic Version" ]
            [ "Account" ]
          ]
        """
        rval = []
        with self.sessionScope() as s:
            rs     = s.execute(sqlStatement)
            rval = rs.fetchall()
            if rowsAsLists:
                rval = [list(row) for row in rval]
        return rval
