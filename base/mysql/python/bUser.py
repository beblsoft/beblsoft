#!/usr/bin/env python3
"""
NAME:
 bUser.py

DESCRIPTION
 Beblsoft MySQL User Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL USER ------------------------------ #
class BeblsoftMySQLUser():
    """
    Beblsoft MySQL User
    """

    def __init__(self, bServer, name, password):
        """
        Initialize object
        Args
          bServer:
            BeblsoftMySQLServer Object
          name:
            User name
            Ex. "jbensson"
          password:
            User password
            Ex. "foo1"
        """
        self.bServer  = bServer
        self.name     = name
        self.password = password

    def __str__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)

    @property
    def exists(self):
        """
        Return True if user exists
        """
        with self.bServer.sessionScope() as s:
            rs = s.execute("SELECT USER,HOST from mysql.user WHERE user='{}';".format(
                self.name))
            return len(rs) > 0

    @logFunc()
    def create(self):
        """
        Create user
        """
        if self.exists:
            logger.info("{} Already Exists".format(self))
        else:
            with self.bServer.sessionScope() as s:
                s.execute("CREATE USER '{}'@'%' IDENTIFIED BY '{}';".format(
                    self.name, self.password))
                s.execute("GRANT ALL    ON *.*        TO '{}'@'%';".format(self.name))
                s.execute("GRANT SELECT ON mysql.*    TO '{}'@'%';".format(self.name))
                s.execute("GRANT SELECT ON mysql.user TO '{}'@'%';".format(self.name))
                logger.info("{} Created".format(self))

    @logFunc()
    def drop(self):
        """
        Drop user
        """
        if self.exists:
            with self.bServer.sessionScope() as s:
                s.execute("DROP USER '{}'@'%'".format(self.name))
                logger.info("{} Dropped".format(self))
        else:
            logger.info("{} Never existed".format(self))

    @staticmethod
    def describeAll(bServer):
        """
        Show all users
        """
        with bServer.sessionScope() as s:
            rs = s.execute("SELECT USER,HOST from mysql.user;")
            logger.info("{} Users:\n {}".format(
                bServer, [[row[0], row[1]] for row in rs]))
