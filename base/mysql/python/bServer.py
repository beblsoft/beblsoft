#!/usr/bin/env python3
"""
NAME:
 bServer.py

DESCRIPTION
 Beblsoft MySQL Server Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import time
import logging
from datetime import datetime, timedelta
from contextlib import contextmanager
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from sqlalchemy.orm import sessionmaker, scoped_session
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.mysql.python.bServerState import BeblsoftMySQLServerState


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL SERVER ---------------------------- #
class BeblsoftMySQLServer():
    """
    Beblsoft MySQL Server
    """

    def __init__(self, domainNameFunc, user, password,
                 port=3306, echo=False, charset="utf8mb4"):
        """
        Initialize object
        domainNameFimc:
          Function to return Database Domain name
          Ex1. lambda: "localhost"
          Ex2. lambda: "mojourneydbinstance.c6qdegjyuiln.us-east-1.rds.amazonaws.com"
        user:
          Database user
          Ex. "jbensson"
        password:
          Database user password
          Ex. "welcome1"
        port:
          Port to connect to database
        echo:
          If True, print database commands
        charset:
            Database character set
            Ex. "utf8"
        """
        self.domainNameFunc = domainNameFunc
        self._domainName    = None
        self.user           = user
        self.password       = password
        self.port           = port
        self.echo           = echo
        self.charset        = charset
        self._engine        = None
        self._session       = None

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ---------------------- PROPERTIES ------------------------------------- #
    @property
    def engine(self):
        """
        Return sqlalchemy engine
        """
        if not self._engine:
            self._engine    = create_engine(
                name_or_url     = self.engineStr,
                echo            = self.echo,
                poolclass       = NullPool)
        return self._engine

    @property
    def engineStr(self):
        """
        Return engine string
        """
        eStr = "mysql://{}:{}@{}:{}/?charset={}".format(
            self.user, self.password, self.domainName, self.port, self.charset)
        return eStr

    @property
    def session(self):
        """
        Return sqlalchemy session
        """
        if not self._session:
            sessionMaker   = sessionmaker(
                autocommit   = False,
                autoflush    = True,
                bind         = self.engine)
            self._session  = scoped_session(sessionMaker)
        return self._session

    @property
    def domainName(self):
        """
        Return domain name
        """
        if not self._domainName:
            self._domainName = self.domainNameFunc()
        return self._domainName

    @property
    def databases(self):
        """
        Return list of databases strings on server
        """
        databases = []
        with self.sessionScope() as s:
            rs = s.execute("SHOW DATABASES;")
            for row in rs:
                databases.append(row[0])
        return databases

    @property
    def versions(self):
        """
        Return database server version

        Ex Return (Aurora):
          {
            'aurora_version': '1.17.98',
            'innodb_version': '1.2.10',
            'protocol_version': '10',
            'slave_type_conversions': '',
            'version': '5.6.10',
            'version_comment': 'MySQL Community Server (GPL)',
            'version_compile_machine': 'x86_64',
            'version_compile_os': 'Linux'
          }
        """
        versionDict = {}
        with self.sessionScope() as s:
            rs = s.execute("""SHOW VARIABLES LIKE "%version%";""")
            for row in rs:
                versionDict[row[0]] = row[1]
        return versionDict

    @property
    def state(self):
        """
        Return BeblsoftMySQLServerState
        """
        state = None
        try:
            with self.sessionScope(commit=False) as s:
                s.execute("SELECT 1")
            state = BeblsoftMySQLServerState.ONLINE
        except BeblsoftError as e:
            if isinstance(e.originalError, sqlalchemy.exc.OperationalError):
                eStr = str(e.originalError)
                if "Can't connect to MySQL server" in eStr:
                    state = BeblsoftMySQLServerState.OFFLINE
                elif "Unknown MySQL server host" in eStr:
                    state = BeblsoftMySQLServerState.OFFLINE
                else:
                    raise e
            else:
                raise e
        return state

    # ---------------------- WAIT ONLINE ------------------------------------ #
    @logFunc()
    def waitOnline(self, reCheckS=1, maxWaitS=60):
        """
        Wait for server to come online
        Args
          reCheckS:
            How frequently (in seconds) to check database online
            Ex. 1
          maxWaitS:
            Maximum time (in seconds) to wait for database to come online
            Ex. 60
        """
        startDate = datetime.now()

        while self.state != BeblsoftMySQLServerState.ONLINE:
            time.sleep(reCheckS)
            continueLooping = datetime.now() - startDate < timedelta(seconds=maxWaitS)
            if not continueLooping:
                raise BeblsoftError(code=BeblsoftErrorCode.MYSQL_SERVER_NOT_ONLINE,
                                    intMsgFormatDict={"domainName": self.domainName})

    # --------------------- SESSION ----------------------------------------- #
    @contextmanager
    def sessionScope(self, commit=True):
        """
        Provide a transactional scope around a series of operations
        """
        try:
            yield self.session
            if commit:
                self.session.commit()
        except Exception as e:
            self.session.rollback()
            raise BeblsoftError(code=BeblsoftErrorCode.DB_COMMIT_FAILED,
                                originalError=e)
        finally:
            self.session.close()
