#!/usr/bin/env python3
"""
NAME:
 bDump.py

DESCRIPTION
 Beblsoft MySQL Dump Functionality
"""


# ------------------------ IMPORTS ------------------------------------------ #
import os
from os.path import expanduser
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.run.bRun import run


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------ BEBLSOFT MYSQL DUMP ------------------------------ #
class BeblsoftMySQLDump():
    """
    Beblsoft MySQL Dump
    """

    def __init__(self, bServer, path):
        """
        Initialize object
        Args
          bServer:
            BeblsoftMySQLServer Object
          path:
            Dump Path
            ex. "/tmp/dump-20180320-1149"
        """
        self.bServer  = bServer
        self.cfgPath  = "{}/.my.cnf".format(expanduser("~"))
        self.path     = path

    def __str__(self):
        return "[{} path={}]".format(self.__class__.__name__, self.path)

    @logFunc()
    def create(self, dbName=None):
        """
        Dump database
        Args
          dbName:
            Database name to dump
            Ex. "quote"
            If None, all databases on server will be dumped
        """
        try:
            dbStr = ""
            self.createConfigFile()
            if dbName:
                dbStr = " --databases {} ".format(dbName)
            cmd = "mysqldump {} > {}".format(dbStr, self.path)
            run(cmd=cmd, raiseOnStatus=True)
        except Exception as e:
            raise(e)
        else:
            logger.info("{} Created".format(self))
        finally:
            self.deleteConfigFile()

    @logFunc()
    def load(self):
        """
        Import database dump
        """
        try:
            self.createConfigFile()
            cmd = "mysql -u {} -h {} < {}".format(
                self.bServer.masterUser, self.bServer.domainName, self.path)
            run(cmd=cmd, raiseOnStatus=True)
        except Exception as e:
            raise(e)
        else:
            logger.info("{} Loaded".format(self))
        finally:
            self.deleteConfigFile()

    @logFunc()
    def createConfigFile(self):
        """
        Create mysqldump config file
        """
        with open(self.cfgPath, "w") as f:
            f.write("[mysqldump]\n")
            f.write("user={}    \n".format(self.bServer.masterUser))
            f.write("password={}\n".format(self.bServer.masterPassword))
            f.write("           \n")
            f.write("[client]   \n")
            f.write("user={}    \n".format(self.bServer.masterUser))
            f.write("password={}\n".format(self.bServer.masterPassword))

    @logFunc()
    def deleteConfigFile(self):
        """
        Delete mysqldump config file
        """
        os.remove(self.cfgPath)
