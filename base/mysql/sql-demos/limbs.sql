# Name:
#   limbs.sql
#
# Description:
#   Show basic syntax to create, populate, and destroy a database
#
# Usage
#   mysql < limbs.sql

#Setup
DROP DATABASE IF EXISTS cookbook;
CREATE DATABASE cookbook;
USE cookbook;
CREATE TABLE limbs
(
	thing 	VARCHAR(20),
	legs  	INT,
	arms	INT
);

#Populate table
INSERT INTO limbs (thing,legs,arms) VALUES ('human',2,2);
INSERT INTO limbs (thing,legs,arms) VALUES ('insect',6,0);
INSERT INTO limbs (thing,legs,arms) VALUES ('squid',0,10);
INSERT INTO limbs (thing,legs,arms) VALUES ('fish',0,0);
INSERT INTO limbs (thing,legs,arms) VALUES ('centipede',100,0);
INSERT INTO limbs (thing,legs,arms) VALUES ('table',4,0);
INSERT INTO limbs (thing,legs,arms) VALUES ('armchair',4,2);
INSERT INTO limbs (thing,legs,arms) VALUES ('phonograph',0,1);
INSERT INTO limbs (thing,legs,arms) VALUES ('tripod',3,0);
INSERT INTO limbs (thing,legs,arms) VALUES ('space alien',NULL,NULL);

#Print Table
SELECT * FROM limbs;

#Clean up
DROP TABLE IF EXISTS limbs;
DROP DATABASE IF EXISTS cookbook;