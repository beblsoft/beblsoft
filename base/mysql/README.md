# MySQL Technology Documentation

MySQL is an open-source relational database mamangement system (RDBMS). It is used by many
commercial sites including: Google, Facebook, Twitter, Flickr, and YouTube.

Relevant URLs:
[Home](https://www.mysql.com/),
[Wikipedia](https://en.wikipedia.org/wiki/MySQL)

## Sharding

A database shard is a horizontal partition of data in a database or search engine. Each individual
partition is referred to as a __shard__ or __database shard__.

Explanation URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Shard_(database_architecture)),
[Sharding Explained](https://medium.com/@jeeyoungk/how-sharding-works-b4dec46b3f6)

Implementation URLs:
[Pinterest Sharding](https://medium.com/@Pinterest_Engineering/sharding-pinterest-how-we-scaled-our-mysql-fleet-3f341e96ca6f),
[Facebook Sharding](https://www.facebook.com/notes/facebook-engineering/under-the-hood-mysql-pool-scanner-mps/10151750529723920/),
[Facebook Windex](https://www.facebook.com/notes/facebook-engineering/windex-automation-for-database-provisioning/10151551942243920),
[MySQL Fabric Demo](http://www.clusterdb.com/mysql-fabric/mysql-fabric-adding-high-availability-and-scaling-to-mysql)

Python URLs:
[UUID Generation](https://docs.python.org/3.5/library/uuid.html),
[SQLAlchemy ShardedSession](http://docs.sqlalchemy.org/en/latest/orm/extensions/horizontal_shard.html),
[SQLAlchemy Shard Example](http://docs.sqlalchemy.org/en/latest/_modules/examples/sharding/attribute_shard.html)

## Facebook MySQL Pool Scanner (MPS)

Facebook splits data into thousands of shards. Each database instance holds a group of such shards.
A Facebook user's profile is assigned to a shard when the profile is created and every shard holds
data relating to thousands of users.

Facebook Database Server
```text
    ------------------------------
    | Database Server            |
    |                            |
    | ------------ ------------  |
    | | Instance | | Instance |  |
    | |----------| |----------|  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | |  Shard   | |   Shard  |  |
    | ------------ ------------  |
    ------------------------------
```
Each instances has a few copies on other instances, that are hosted on different servers. This is for
1. High Availability - If a server goes down, have data available elsewhere
2. Performance       - Different geographical regions have their own replicas

Use master/slave replication
```text
                        ------------
       ------------   / | Slave    |
       | Master   |  /  | Instance |
       | Instance |--|  ------------
       ------------  \
                      \ ------------
                        | Slave    |
                        | Instance |
                        ------------
```

A server is a container of instances. Some instances on the server can be master and others are slaves.