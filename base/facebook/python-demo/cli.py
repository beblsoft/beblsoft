#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Facebook Server Sample Command Line Interface
"""


# ----------------------------- IMPORTS ------------------------------------- #
import os
import sys
import traceback
import logging
from datetime import datetime
import requests
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.facebook.python.bGraph import BeblsoftFacebookGraph
from base.facebook.python.app.dao import BFacebookAppDAO
from base.facebook.python.testUser.dao import BFacebookTestUserDAO
from base.facebook.python.user.dao import BFacebookUserDAO
from base.facebook.python.debugAccessToken.dao import BFacebookDebugAccessTokenDAO
from base.facebook.python.ltAccessToken.dao import BFacebookLTAccessTokenDAO
from base.facebook.python.post.dao import BFacebookPostDAO
from base.facebook.python.photo.dao import BFacebookPhotoDAO
from base.facebook.python.video.dao import BFacebookVideoDAO
from base.facebook.python.comment.dao import BFacebookCommentDAO


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # FB Graph --------------------------------
        self.bFBGraph          = BeblsoftFacebookGraph(
            appID                  = 1925974167437170,  # Beblsoft Demo
            appSecret              = "ee8668381ba010ab70645f65a583f802",
            timeoutS               = 1000.0,
            version                = "4.0")

        # FB Tokens -------------------------------
        self.testUserID        = 102691597514550  # BelsoftDemo TestUser
        self.postID            = "102691597514550_102690744181302"
        self.photoID           = 102691187514591
        self.videoID           = 102700404180336
        self.commentID         = "159730640506_4731592"  # Comments not supported in test mode?

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

    @property
    def testUserAccessToken(self):  # pylint: disable=C0111
        return BFacebookTestUserDAO.getAccessToken(
            bFBGraph    = self.bFBGraph, appID=self.bFBGraph.appID,
            testUserID  = self.testUserID,
            accessToken = self.bFBGraph.appAccessToken)


logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/fbPythonServerDemo.log", type=click.Path(),
              help="Specify log file Default=/tmp/fbPythonServerDemo.log")
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Facebook Python Server Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- TEST USER ----------------------------------------- #
@cli.command()
def testuser_crud():
    """
    Create, read, update, and delete
    """
    name                   = "John Smith"
    newName                = "Peter Johnson"
    permStrList            = ["user_birthday", "user_hometown"]

    # Create
    bFBTestUserCreateModel = BFacebookTestUserDAO.create(
        bFBGraph              = gc.bFBGraph,
        appID                 = gc.bFBGraph.appID,
        name                  = name,
        permStrList           = permStrList)
    bFBTestUserCreateModel.describe()

    # Read
    bFBUserModel           = BFacebookTestUserDAO.getByID(
        bFBGraph              = gc.bFBGraph,
        userID                = bFBTestUserCreateModel.id,
        accessToken           = gc.bFBGraph.appAccessToken)
    bFBUserModel.describe()

    # Update and Delete
    BFacebookTestUserDAO.updateCredentials(
        bFBGraph              = gc.bFBGraph,
        testUserID            = bFBTestUserCreateModel.id,
        name                  = newName)
    BFacebookTestUserDAO.deleteByID(
        bFBGraph              = gc.bFBGraph,
        testUserID            = bFBTestUserCreateModel.id)


@cli.command()
def testuser_get_all():
    """
    Get all
    """
    bFFBTestUserGetAllModelList = BFacebookTestUserDAO.getAll(
        bFBGraph                  = gc.bFBGraph,
        appID                     = gc.bFBGraph.appID)

    for bFFBTestUserGetAllModel in bFFBTestUserGetAllModelList:
        bFBUserModel = BFacebookTestUserDAO.getByID(
            bFBGraph    = gc.bFBGraph,
            userID      = bFFBTestUserGetAllModel.id,
            accessToken = gc.bFBGraph.appAccessToken)
        bFBUserModel.describe()


@cli.command()
@click.argument("appid")
@click.argument("appsecret")
def testuser_associate(appid, appsecret):
    """
    Associate test user with app
    """
    newAppAccessToken = "{}|{}".format(appid, appsecret)
    bFBTestUserCreateModel = BFacebookTestUserDAO.associate(
        bFBGraph              = gc.bFBGraph,
        testUserID            = gc.testUserID,
        appID                 = appid,
        ownerAccessToken      = gc.bFBGraph.appAccessToken,
        accessToken           = newAppAccessToken)
    bFBTestUserCreateModel.describe()


@cli.command()
@click.argument("appid")
@click.argument("appsecret")
def testuser_disassociate(appid, appsecret):
    """
    Dissassociate test user with app
    """
    appAccessToken = "{}|{}".format(appid, appsecret)
    BFacebookTestUserDAO.disassociate(
        bFBGraph              = gc.bFBGraph,
        testUserID            = gc.testUserID,
        appID                 = appid,
        accessToken           = appAccessToken)


# ----------------------- APPLICATION --------------------------------------- #
@cli.command()
def app_get():
    """
    Get info
    """
    bFBAppModel = BFacebookAppDAO.getByID(bFBGraph=gc.bFBGraph, appID=gc.bFBGraph.appID)
    bFBAppModel.describe()


# ----------------------- DEBUG TOKEN --------------------------------------- #
@cli.command()
def debug_access_token_get():
    """
    Debug access token
    """
    bFBDebugAccessTokenModel = BFacebookDebugAccessTokenDAO.get(bFBGraph=gc.bFBGraph,
                                                                inputToken=gc.testUserAccessToken)
    bFBDebugAccessTokenModel.describe()


# ----------------------- LONG TERM TOKEN ----------------------------------- #
@cli.command()
def long_term_token_get():
    """
    Get long term token
    """
    bFBLTAccessTokenModel = BFacebookLTAccessTokenDAO.get(
        bFBGraph=gc.bFBGraph, shortTermAccessToken=gc.testUserAccessToken)
    bFBLTAccessTokenModel.describe()


# ----------------------- USER ---------------------------------------------- #
@cli.command()
def user_get():  # pylint: disable=E0102
    """
    Get user info
    """
    bFBUserModel    = BFacebookTestUserDAO.getByID(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken)
    bFBUserModel.describe()


@cli.command()
def user_permissions_get():
    """
    Get user permissions
    """
    bFBUserPermissionsModel = BFacebookUserDAO.getPermissions(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken)
    bFBUserPermissionsModel.describe()


@cli.command()
def user_posts_get():
    """
    Get user posts
    """
    bFBPostModelList = BFacebookUserDAO.getAllPosts(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken,
        type           = "posts")
    for bFBPostModel in bFBPostModelList:
        bFBPostModel.describe()


@cli.command()
def user_posts_get_list():
    """
    Get user post list
    """
    (bFBPostModelList, _) = BFacebookUserDAO.getPostList(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken,
        type           = "posts",
        pageLimit      = 5)
    for bFBPostModel in bFBPostModelList:
        bFBPostModel.describe()


@cli.command()
def user_photos_get():
    """
    Get user photos
    """
    bFBPhotoModelList = BFacebookUserDAO.getAllPhotos(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken,
        type           = "uploaded")
    for bFBPhotoModel in bFBPhotoModelList:
        bFBPhotoModel.describe()


@cli.command()
def user_photos_download():
    """
    Download photos
    """
    # Create download directory
    photoDir = "/home/jbensson/Downloads/pics"
    if not os.path.isdir(photoDir):
        os.makedirs(photoDir)

    bFBPhotoModelList = BFacebookUserDAO.getAllPhotos(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken,
        type           = "uploaded")

    for bFBPhotoModel in bFBPhotoModelList:
        url     = bFBPhotoModel.largestImage.source
        photoID = bFBPhotoModel.id
        r       = requests.get(url, allow_redirects=True)
        with open("{}/{}.jpg".format(photoDir, photoID), 'wb') as f:
            f.write(r.content)


@cli.command()
def user_videos_get():
    """
    Get user videos
    """
    bFBVideoModelList = BFacebookUserDAO.getAllVideos(
        bFBGraph       = gc.bFBGraph,
        userID         = gc.testUserID,
        accessToken    = gc.testUserAccessToken,
        type           = "uploaded")
    for bFBVideoModel in bFBVideoModelList:
        bFBVideoModel.describe()


# ----------------------- POSTS --------------------------------------------- #
@cli.command()
def post_get():  # pylint: disable=E0102
    """
    Get post
    """
    bFBPostModel = BFacebookPostDAO.getByID(
        bFBGraph       = gc.bFBGraph,
        postID         = gc.postID,
        accessToken    = gc.testUserAccessToken)
    bFBPostModel.describe()


@cli.command()
def post_comments_get():
    """
    Get post comments
    """
    bFBCommentModelList = BFacebookPostDAO.getAllComments(
        bFBGraph       = gc.bFBGraph,
        objID          = gc.postID,
        accessToken    = gc.testUserAccessToken)
    for bFBCommentModel in bFBCommentModelList:
        bFBCommentModel.describe()


# ----------------------- PHOTO --------------------------------------------- #
@cli.command()
def photo_get():  # pylint: disable=E0102
    """
    Get info
    """
    bFBPhotoModel = BFacebookPhotoDAO.getByID(
        bFBGraph       = gc.bFBGraph,
        photoID        = gc.photoID,
        accessToken    = gc.testUserAccessToken)
    bFBPhotoModel.describe()


@cli.command()
def photo_comments_get():  # pylint: disable=E0102
    """
    Get photo comments
    """
    bFBCommentModelList = BFacebookPhotoDAO.getAllComments(
        bFBGraph       = gc.bFBGraph,
        objID          = gc.photoID,
        accessToken    = gc.testUserAccessToken)
    for bFBCommentModel in bFBCommentModelList:
        bFBCommentModel.describe()


# ----------------------- VIDEO --------------------------------------------- #
@cli.command()
def video_get():  # pylint: disable=E0102
    """
    Get info
    """
    bFBVideoModel = BFacebookVideoDAO.getByID(
        bFBGraph       = gc.bFBGraph,
        videoID        = gc.videoID,
        accessToken    = gc.testUserAccessToken)
    bFBVideoModel.describe()


@cli.command()
def video_comments_get():  # pylint: disable=E0102
    """
    Get video comments
    """
    bFBCommentModelList = BFacebookVideoDAO.getAllComments(
        bFBGraph       = gc.bFBGraph,
        objID          = gc.videoID,
        accessToken    = gc.testUserAccessToken)
    for bFBCommentModel in bFBCommentModelList:
        bFBCommentModel.describe()


# ----------------------- COMMENTS ------------------------------------------ #
@cli.command()
def comment_get():  # pylint: disable=E0102
    """
    Get comment
    """
    bFBCommentModel = BFacebookCommentDAO.getByID(
        bFBGraph       = gc.bFBGraph,
        commentID      = gc.commentID,
        accessToken    = gc.testUserAccessToken)
    bFBCommentModel.describe()


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
