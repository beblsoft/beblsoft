OVERVIEW
===============================================================================
Facebook Samples Documentation



PROJECT: JS LOGIN DEMO
===============================================================================
- Description                       : JS application that logs in with Facebook
                                    : BeblsoftDemo Facebook Application ID Used
- Directory                         : ./js-login-demo
- To Run
  * Install dependencies            : npm install
  * Allow chrome to run with
    invalid certs from localhost    : In chrome, go to URL: chrome://flags/#allow-insecure-localhost
                                      Click Allow on "Allow invalid certificates from resources loaded from localhost"
                                      Relaunch chrome
  * Run express server              : node server.js
                                      Open chrome to https://localhost:8080



PROJECT: VUE LOGIN DEMO
===============================================================================
- Description                       : Vue application that logs in with Facebook
                                    : BeblsoftDemo Facebook Application ID Used
- Directory                         : ./vue-login-demo
- To Run
  * Install dependencies            : npm install
  * Allow chrome to run with
    invalid certs from localhost    : In chrome, go to URL: chrome://flags/#allow-insecure-localhost
                                      Click Allow on "Allow invalid certificates from resources loaded from localhost"
                                      Relaunch chrome
  * Run express server              : node server.js
                                      Open chrome to https://localhost:8080



PROJECT: PYTHON DEMO
===============================================================================
- Description                       : Demo python facebook interface
                                    : BeblsoftDemo Facebook Application ID Used
- !! IMPORTANT NOTE !!              : For User Data Calls to work, must first use one of the
                                      first two projects above to grant BeblsoftDemo Application
                                      permission to access BeblsoftTest User's data
- Directory                         : ./python-demo
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Test User Commands ----------------
  * Do test user crud               : ./cli.py testuser-crud
  * Print all test users            : ./cli.py testuser-get-all
  * Associate test user with app    : ./cli.py testuser-associate <appid> <appsecret>
  * Disassociate test user with app : ./cli.py testuser-disassociate <appid> <appsecret>
- Application Commands --------------
  * Get information                 : ./cli.py app-get
- Debug token commands --------------
  * Debug token                     : ./cli.py debug-access-token-get
- Long Term Token Commands ----------
  * Extend short token              : ./cli.py long-term-token-get
- User Commands ---------------------
  * Get information                 : ./cli.py user-get
  * Get permissions for token       : ./cli.py user-permissions-get
  * Get posts                       : ./cli.py user-posts-get
  * Get photos                      : ./cli.py user-photos-get
  * Download photos                 : ./cli.py user-photos-download
  * Get videos                      : ./cli.py user-videos-get
- Post Commands ---------------------
  * Get information                 : ./cli.py post-get
  * Get comments                    : ./cli.py post-comments-get
- Video Commands --------------------
  * Get information                 : ./cli.py video-get
  * Get comments                    : ./cli.py video-comments-get
- Comment Commands ------------------
  * Get information (doesn't work
    on test data)                    : ./cli.py comment-get
