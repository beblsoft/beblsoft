#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Video Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import math
import logging
from datetime import datetime, timezone
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK VIDEO MODEL --------------------- #
class BFacebookVideoModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Video Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/user/videos/
      https://developers.facebook.com/docs/graph-api/reference/video/

    obj Format:
    {
      'id': '313296206193107',
      'created_time': '2018-12-30T15:08:14+0000',
      'updated_time': '2018-12-30T15:08:51+0000'
      'embeddable': True,
      'icon': 'https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/DggDhA4z4tO.gif',
      'length': 7.808,
      'permalink_url': '/100025381898139/videos/313296206193107/',
      'published': True,
      'picture': 'https://scontent.xx.fbcdn.net/v/t15.5256-10/p168x128/48549207_313296326193095_3923521292833128448_n.jpg?_nc_cat=103&_nc_ht=scontent.xx&oh=50e1453411368214aea4bce71c1d4af9&oe=5CCD4CD7',
      'source': 'https://scontent.xx.fbcdn.net/v/t42.1790-29/49725944_435383650331946_1071125295563538432_n.mp4?_nc_cat=100&efg=eyJybHIiOjYzMCwicmxhIjo1MTIsInZlbmNvZGVfdGFnIjoic2QifQ%3D%3D&rl=630&vabr=350&_nc_ht=scontent.xx&oh=c15e45bb7d3dac034ffa1944f6ad942f&oe=5C439917',
      'status': {'video_status': 'ready'},
      'comments': {
        'data': [],
        'summary': {
          'can_comment': False,
          'order': 'ranked',
          'total_count': 0
        }
      },
      'reactions': {
        'data': [],
        'summary': {
          'total_count': 0,
          'viewer_reaction': 'NONE'
        }
      },
      'likes': {
        'data': [],
        'summary': {
          'can_like': False,
          'has_liked': False,
          'total_count': 0
        }
      },
      'format': [{
        'embed_html':
           '<iframe '
           'src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F100025381898139%2Fvideos%2F313296206193107%2F&width=130" '
           'width="130" height="73" '
           'style="border:none;overflow:hidden" scrolling="no" '
           'frameborder="0" allowTransparency="true" '
           'allowFullScreen="true"></iframe>',
        'filter': '130x130',
        'height': 73,
        'picture': 'https://scontent.xx.fbcdn.net/v/t15.5256-10/s130x130/48549207_313296326193095_3923521292833128448_n.jpg?_nc_cat=103&_nc_ht=scontent.xx&oh=d1a8f2c6ba794bf452b1ddbc80328d74&oe=5CD00323',
        'width': 130
      },{
        'embed_html':
           '<iframe '
           'src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F100025381898139%2Fvideos%2F313296206193107%2F&width=480" '
           'width="480" height="270" '
           'style="border:none;overflow:hidden" scrolling="no" '
           'frameborder="0" allowTransparency="true" '
           'allowFullScreen="true"></iframe>',
        'filter': '480x480',
        'height': 270,
        'picture': 'https://scontent.xx.fbcdn.net/v/t15.5256-10/s480x480/48549207_313296326193095_3923521292833128448_n.jpg?_nc_cat=103&_nc_ht=scontent.xx&oh=182c4b0a4eade02f9ba3f802e2982bf5&oe=5CCC1B16',
        'width': 480
      }, {
        'embed_html':
           '<iframe '
           'src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F100025381898139%2Fvideos%2F313296206193107%2F&width=654" '
           'width="654" height="368" '
           'style="border:none;overflow:hidden" scrolling="no" '
           'frameborder="0" allowTransparency="true" '
           'allowFullScreen="true"></iframe>',
        'filter': 'native',
        'height': 368,
        'picture': 'https://scontent.xx.fbcdn.net/v/t15.5256-10/48549207_313296326193095_3923521292833128448_n.jpg?_nc_cat=103&_nc_ht=scontent.xx&oh=6bfa2a4198151cd514bb67f19f9c8100&oe=5CFCCCD1',
        'width': 654
      }],
    }
    """

    DEFAULT_FIELD_LIST  = ["id",
                           "created_time",
                           "description",
                           "embeddable",
                           "format",
                           "icon",
                           "length",
                           "permalink_url",
                           "picture",
                           "published",
                           "source",
                           "status",
                           "title",
                           "universal_video_id",
                           "updated_time",
                           "comments.summary(true)",
                           "likes.summary(true)",
                           "reactions.summary(true)"]
    DESCRIBE_PROPERTIES = ["id", "createDate", "updateDate", "source"]

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("created_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def updateDate(self):
        """
        Update date in utc
        """
        d = datetime.strptime(self.getValueFromObj("updated_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def source(self):  # pylint: disable=C0111
        return self.getValueFromObj("source")

    @source.setter
    def source(self, source):
        self.obj["source"] = source

    @property
    def lengthS(self):  # pylint: disable=C0111
        """
        Return length as integer
        Ex. 7.88 -> 8
        """
        lengthSec = self.getValueFromObj("length")
        lengthSec = math.ceil(lengthSec) if lengthSec else None
        return lengthSec

    @property
    def picture(self):  # pylint: disable=C0111
        return self.getValueFromObj("picture")

    @property
    def nComments(self):  # pylint: disable=C0111
        return self.getValueFromObj("comments.summary.total_count")

    @property
    def nLikes(self):  # pylint: disable=C0111
        return self.getValueFromObj("likes.summary.total_count")

    @property
    def nReactions(self):  # pylint: disable=C0111
        return self.getValueFromObj("reactions.summary.total_count")
