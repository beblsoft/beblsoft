#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Video DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.video.models import BFacebookVideoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK VIDEO DAO ----------------------- #
class BFacebookVideoDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook Video DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, videoID, accessToken, fieldList=None):
        """
        Returns
          BFacebookVideoModel
        """
        fieldList = fieldList if fieldList else BFacebookVideoModel.DEFAULT_FIELD_LIST
        obj       = bFBGraph.requestJSON(
            method          = "GET",
            relativePath    = "{}".format(videoID),
            accessToken     = accessToken,
            params          = {"fields": ",".join(fieldList)})
        return BFacebookVideoModel(obj=obj)
