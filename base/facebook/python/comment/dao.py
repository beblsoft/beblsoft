#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Comment DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.comment.models import BFacebookCommentModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK COMMENT DAO --------------------- #
class BFacebookCommentDAO():
    """
    Beblsoft Facebook Comment DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, commentID, accessToken, fieldList=None):
        """
        Returns
          BFacebookCommentModel
        """
        fieldList = fieldList if fieldList else BFacebookCommentModel.DEFAULT_FIELD_LIST
        obj       = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}".format(commentID),
            accessToken   = accessToken,
            params        = {"fields": ",".join(fieldList)})
        return BFacebookCommentModel(obj=obj)
