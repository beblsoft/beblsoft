#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Comment Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK COMMENT MODEL ------------------- #
class BFacebookCommentModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Comment Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/object/comments
      https://developers.facebook.com/docs/graph-api/reference/v3.2/comment

    obj Format:
    {
      'comment_count': 0,
      'created_time': '2009-04-28T16:12:40+0000',
      'from': {
        'id': '10160135909285507',
        'name': 'James Bensson'
      },
      'id': '159730640506_4128409',
      'like_count': 0,
      'message': 'lol, yea my form definitely needs some improvement',
      'object': {
        'created_time': '2009-04-20T16:04:31+0000',
        'id': '159730640506'
      }
    }
    """

    DEFAULT_FIELD_LIST  = ["id",
                           "created_time",
                           "comment_count",
                           "from",
                           "like_count",
                           "message",
                           "object",
                           "parent"]

    DESCRIBE_PROPERTIES = ["id", "createDate", "message"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def message(self):  # pylint: disable=C0111
        return self.getValueFromObj("message")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("created_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)
