#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Test User Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK APP TEST USER CREATE MODEL ------ #
class BFacebookTestUserCreateModel(BFacebookCommonModel):
    """
    Beblsoft Facebook App Test User Create Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/app/accounts/test-users

    obj Format:
    {
      'id': '101864874258796',
      'access_token': 'EAAbXqc4Yv3IBAPGROCndmKvShMp5z2wX2pdX2UNiqoKv2Ywwakx7aUdZBClXbLZAUmImKQGXubrcN2q318KfZBT2rHSc4r8hH91aQCzlTEM7IfhxsuRSWo4jZB7eNmZA6p7tAO7MVZCh2VSLDahTOwLEEzie4xgBiaHIDSNurTi8hyAzvrUmh0TbWlKPvEq7uZBvsaopqfZC2cOcJVfYqSpl',
      'login_url': 'https://developers.facebook.com/checkpoint/test-user-login/101864874258796/',
      'email': 'ldjsueaiaw_1547740193@tfbnw.net',
      'password': '1411634559'
    }
    """

    DEFAULT_PERMISSION_STRLIST = ["user_birthday", "user_hometown", "user_location",
                                  "user_likes", "user_photos", "user_videos",
                                  "user_friends", "user_tagged_places", "user_posts",
                                  "email", "public_profile"]

    DESCRIBE_PROPERTIES        = ["id", "accessToken", "email", "password"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def accessToken(self):  # pylint: disable=C0111
        return self.getValueFromObj("access_token")

    @property
    def email(self):  # pylint: disable=C0111
        return self.getValueFromObj("email")

    @property
    def password(self):  # pylint: disable=C0111
        return self.getValueFromObj("password")


# ----------------------- BEBLSOFT FACEBOOK APP TEST USER UPDATE MODEL ------ #
class BFacebookTestUserGetAllModel(BFacebookCommonModel):
    """
    Beblsoft Facebook App Test User Update Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/app/accounts/test-users

    obj Format:
    {
      'id': '202707600625096',
      'login_url': 'https://developers.facebook.com/checkpoint/test-user-login/202707600625096/',
      'access_token': 'EAAbXqc4Yv3IBABranJwOvWhwhd4lUzCWFNUT71GzSOqsXFyOkcpa0G33qh0dyyZCjhY4MFnTcsul6qKVylePHYQr05OjzQKAjRzaXnI2hLIXMefdryt9dstaUbBR5eZB8ZATicZAzqXxr6iOSZA45mpjoZCoZCjPZAIEv2662h7MShOeaIeMgZA4EXhfqnzLwXH8bTsBnKvwexgZDZD'
    }
    """

    DESCRIBE_PROPERTIES = ["id", "accessToken"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def accessToken(self):  # pylint: disable=C0111
        return self.getValueFromObj("access_token")
