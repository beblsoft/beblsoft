#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Test User DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.user.dao import BFacebookUserDAO
from base.facebook.python.testUser.models import BFacebookTestUserCreateModel, BFacebookTestUserGetAllModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK TEST USER DAO ------------------- #
class BFacebookTestUserDAO(BFacebookUserDAO):
    """
    Beblsoft Facebook Test User DAO
    """

    # ------------------- CREATE -------------------------------------------- #
    @staticmethod
    @logFunc()
    def create(bFBGraph, appID, name, accessToken=None, installed=True, permStrList=None):
        """
        Create object

        Returns
          BFacebookTestUserCreateModel
        """
        permStrList        = permStrList if permStrList else BFacebookTestUserCreateModel.DEFAULT_PERMISSION_STRLIST
        obj                = bFBGraph.requestJSON(
            method             = "POST",
            relativePath       = "{}/accounts/test-users".format(appID),
            accessToken        = accessToken if accessToken else bFBGraph.appAccessToken,
            data               = {"installed": installed,
                                  "permissions": ",".join(permStrList),
                                  "name": name})
        return BFacebookTestUserCreateModel(obj=obj)

    # ------------------- DELETE -------------------------------------------- #
    @staticmethod
    @logFunc()
    def deleteByID(bFBGraph, testUserID, accessToken=None):
        """
        Delete object
        """
        bFBGraph.requestJSON(
            method             = "DELETE",
            relativePath       = "{}".format(testUserID),
            accessToken        = accessToken if accessToken else bFBGraph.appAccessToken)

    # ------------------- ASSOCIATE ----------------------------------------- #
    @staticmethod
    @logFunc()
    def associate(bFBGraph, testUserID, appID, ownerAccessToken, accessToken=None, installed=True, permStrList=None):
        """
        Associate Test User with Application
        Args
          testUserID:
            ID of existing test user
          appID:
            AppID to associate user with
          ownerAccessToken:
            Access token for app that already owns user
          accessToken:
            Access token of app that will receive new user

        Returns
          BFacebookTestUserCreateModel
        """
        permStrList        = permStrList if permStrList else BFacebookTestUserCreateModel.DEFAULT_PERMISSION_STRLIST
        obj                = bFBGraph.requestJSON(
            method             = "POST",
            relativePath       = "{}/accounts/test-users".format(appID),
            accessToken        = accessToken if accessToken else bFBGraph.appAccessToken,
            data               = {"installed": installed,
                                  "permissions": ",".join(permStrList),
                                  "uid": testUserID,
                                  "owner_access_token": ownerAccessToken})
        return BFacebookTestUserCreateModel(obj=obj)

    # ------------------- DISASSOCIATE -------------------------------------- #
    @staticmethod
    @logFunc()
    def disassociate(bFBGraph, testUserID, appID, accessToken=None):
        """
        Disassociate Test User with Application
        """
        bFBGraph.requestJSON(
            method             = "DELETE",
            relativePath       = "{}/accounts/test-users".format(appID),
            accessToken        = accessToken if accessToken else bFBGraph.appAccessToken,
            data               = {"uid": testUserID})

    # ------------------- UPDATE CREDENTIALS -------------------------------- #
    @staticmethod
    @logFunc()
    def updateCredentials(bFBGraph, testUserID, accessToken=None, name=None, password=None):
        """
        Update credentials

        Note:
          For some reason updating the password doesn't work here.
          Keep getting:
          {'message': '(#100) Invalid password', 'type': 'OAuthException',
                        'code': 100, 'fbtrace_id': 'G5EwMnOAhYB'}
        """
        data                 = {}
        if name:
            data["name"]     = name
        if password:
            data["password"] = password

        bFBGraph.requestJSON(
            method        = "POST",
            relativePath  = "{}".format(testUserID),
            accessToken   = accessToken if accessToken else bFBGraph.appAccessToken,
            data          = data)

    # ------------------- GET ALL ------------------------------------------- #
    @staticmethod
    @logFunc()
    def getAll(bFBGraph, appID, accessToken=None):
        """
        Get all objects associated with appID

        Returns
          BFacebookTestUserGetAllModel List
        """
        resp = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}/accounts/test-users".format(appID),
            accessToken   = accessToken if accessToken else bFBGraph.appAccessToken)

        bFFBTestUserGetAllModelList = [BFacebookTestUserGetAllModel(obj) for obj in resp.get("data")]
        return bFFBTestUserGetAllModelList

    # ------------------- GET ACCESS TOKEN ---------------------------------- #
    @staticmethod
    @logFunc()
    def getAccessToken(bFBGraph, appID, testUserID, accessToken=None):
        """
        Get an access token for testUserID

        Returns
          accessToken
          Ex. 'EAAbXqc4Yv3IBABranJwOvWhwhd4lUzCWFNUT71GzSOqsXFyOkcpa0G33qh0dyyZCjhY4MFnTcsul6qKVylePHYQr05OjzQKAjRzaXnI2hLIXMefdryt9dstaUbBR5eZB8ZATicZAzqXxr6iOSZA45mpjoZCoZCjPZAIEv2662h7MShOeaIeMgZA4EXhfqnzLwXH8bTsBnKvwexgZDZD'
        """
        testUserAccessToken         = None
        bFFBTestUserGetAllModelList = BFacebookTestUserDAO.getAll(
            bFBGraph=bFBGraph, appID=appID,
            accessToken=accessToken if accessToken else bFBGraph.appAccessToken)

        for bFFBTestUserGetAllModel in bFFBTestUserGetAllModelList:
            if int(bFFBTestUserGetAllModel.id) == int(testUserID):
                testUserAccessToken = bFFBTestUserGetAllModel.accessToken

        if not testUserAccessToken:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_NO_ACCESS_TOKEN_FOR_USER,
                                extMsgFormatDict={"userID": testUserID})

        return testUserAccessToken
