#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Post Media DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.postMedia.models import BFacebookPostMediaModel
from base.facebook.python.photo.dao import BFacebookPhotoDAO
from base.facebook.python.video.dao import BFacebookVideoDAO


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK POST MEDIA DAO ------------------ #
class BFacebookPostMediaDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook Post Media DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getFromBFBPostModel(bFBGraph, bFBPostModel, accessToken):
        """
        Returns
          BFacebookPostMediaModel
        """
        bFBPhotoModel = None
        bFBVideoModel = None

        if bFBPostModel.type == "photo":
            bFBPhotoModel = BFacebookPhotoDAO.getByID(bFBGraph=bFBGraph, photoID=bFBPostModel.objectID,
                                                      accessToken=accessToken)
        if bFBPostModel.type == "video":
            bFBVideoModel = BFacebookVideoDAO.getByID(bFBGraph=bFBGraph, videoID=bFBPostModel.objectID,
                                                      accessToken=accessToken)

        return BFacebookPostMediaModel(bFBPostModel=bFBPostModel, bFBPhotoModel=bFBPhotoModel,
                                       bFBVideoModel=bFBVideoModel)
