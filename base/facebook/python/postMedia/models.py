#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Post Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
import pprint
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK POST MEDIA MODEL ---------------- #
class BFacebookPostMediaModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Post Media Model
    """

    def __init__(self, bFBPostModel, bFBPhotoModel=None, bFBVideoModel=None):
        """
        Initialize object
        """
        super().__init__(obj=bFBPostModel.obj)
        self.bFBPostModel  = bFBPostModel
        self.bFBPhotoModel = bFBPhotoModel
        self.bFBVideoModel = bFBVideoModel


    # ------------------- DESCRIBE ------------------------------------------ #
    def describe(self): #pylint: disable=W0221
        """
        Describe object
        """
        postDPDict  = self.bFBPostModel.getDescribePropertyDict()
        photoDPDict = self.bFBPhotoModel.getDescribePropertyDict() if self.bFBPhotoModel else {}
        videoDPDict = self.bFBVideoModel.getDescribePropertyDict() if self.bFBVideoModel else {}

        logger.info("[{} {}{}{}]".format(
            self.__class__.__name__,
            pprint.pformat(postDPDict), pprint.pformat(photoDPDict), pprint.pformat(videoDPDict)))
