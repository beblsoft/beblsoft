#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Long Term Access Token DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.ltAccessToken.models import BFacebookLTAccessTokenModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK LONG TERM ACCESS TOKEN DAO ------ #
class BFacebookLTAccessTokenDAO():
    """
    Beblsoft Facebook Long Term Access Token DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bFBGraph, shortTermAccessToken):
        """
        Args
          shortTermAccessToken:
            Access token given to client

        Return
          BFacebookLTAccessTokenModel
        """
        obj = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "oauth/access_token",
            accessToken   = bFBGraph.appAccessToken,
            params        = {
                "grant_type": "fb_exchange_token",
                "client_id": bFBGraph.appID,
                "client_secret": bFBGraph.appSecret,
                "fb_exchange_token": shortTermAccessToken
            })
        return BFacebookLTAccessTokenModel(obj=obj)
