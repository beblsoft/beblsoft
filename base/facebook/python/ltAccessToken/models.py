#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Long Term Access Token Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
from datetime import timedelta
import logging
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK LONG TERM ACCESS TOKEN MODEL ---- #
class BFacebookLTAccessTokenModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Long Term Access Token Model

    Reference:
      https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing/

    obj Format:
    {
      'access_token': 'EAAbXqc4Yv3IBAFnn0WX0bkNL6AXSjpxRiyl6JwZBcb2Foj81RDwhWJnWE2ecWIKhZAPJuleC0SdSFhDfldUgvpx843FTCF92dbjDKwEwmZCDxSmRJrml8ZCsRHbFazKQK6rNm1c9vaH4l0hOlwBWvLHzhLq3JB5ntHRkfy39pFB7vE00BR4v',
      'expires_in': 5183898,
      'token_type': 'bearer'
    }
    """

    DESCRIBE_PROPERTIES = ["token", "expireTimeDelta"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def token(self):  # pylint: disable=C0111
        return self.getValueFromObj("access_token")

    @property
    def expireTimeDelta(self):
        """
        Returns timedelta until token expires
        """
        return timedelta(seconds=self.getValueFromObj("expires_in"))
