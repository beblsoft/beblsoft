#!/usr/bin/env python3.6
"""
 NAME
  bGraph.py

 DESCRIPTION
  Beblsoft Facebook Graph Functionality

 BIBLIOGRAPHY
   Based off Facebook Python SDK    : https://github.com/mobolic/facebook-sdk
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
import requests
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- BEBLSOFT FACEBOOK GRAPH --------------------- #
class BeblsoftFacebookGraph():
    """
    Beblsoft Facebook Graph
    """

    FACEBOOK_GRAPH_URL         = "https://graph.facebook.com"
    FACEBOOK_WWW_URL           = "https://www.facebook.com"
    FACEBOOK_OAUTH_DIALOG_PATH = "dialog/oauth?"

    def __init__(self, appID, appSecret, timeoutS=10.0, version="3.2", proxyDict=None, session=None):
        """
        Initialize object
        Args
          appID:
            Application ID
          appSecret:
            Application Secret
          timeoutS:
            Float describing time (in seconds) that client will wait for responses
          version:
            String describing FB graph API use
          proxyDict:
            Dictionary with proxy settings for requests
          session:
            Reqests session object
        """
        self.appID          = appID
        self.appSecret      = appSecret
        self.appAccessToken = "{}|{}".format(self.appID, self.appSecret)
        self.timeoutS       = timeoutS
        self.version        = version
        self.versionStr     = "v{}".format(version)
        self.proxyDict      = proxyDict
        self.session        = session or requests.Session()

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    @logFunc()
    def getVersion(self):
        """
        Return current version of Graph API
        """
        resp = self.request(
            method        = "GET",
            relativePath  = "me")
        headers = resp.headers
        version = headers["facebook-api-version"].replace("v", )
        return version

    # ------------------------- REQUEST ------------------------------------- #
    @logFunc()
    def request(self, method, relativePath, accessToken=None,
                params=None, data=None, files=None):
        """
        Executes a graph API request

        Args
          method:
            HTTP Method
            Ex. "GET"
          relativePath:
            Path relative to https://graph.facebook.com/v3.2/
            Ex. "me"
          accessToken:
            Access token to use for request
          params:
            Dictionary Query strying parameters
            Ex. { 'fields' : 'name,creationDate'}
          data:
            Dictionary of Post/put data
            Ex. {}
          files:
            Files to add to request

        Returns
          Requests response object
          http://docs.python-requests.org/en/master/api/#requests.Response
        """
        path = "{}/{}/{}".format(BeblsoftFacebookGraph.FACEBOOK_GRAPH_URL,
                                 self.versionStr, relativePath)
        if not params:
            params = {}

        # Add accessToken to request
        if data and "access_token" not in data:
            data["access_token"] = accessToken
        elif "access_token" not in params:
            params["access_token"] = accessToken

        # Send Request
        try:
            resp = self.session.request(
                method,
                path,
                timeout = self.timeoutS,
                params  = params,
                data    = data,
                proxies = self.proxyDict,
                files   = files)
        except requests.HTTPError as e:
            raise BeblsoftError(code=BeblsoftErrorCode.FACEBOOK_GRAPH_REQUEST_FAILED,
                                originalError=e)

        # Process results
        headers     = resp.headers
        contentType = headers["content-type"]
        respJSON    = resp.json() if "json" in contentType else None
        errorResp   = respJSON.get("error", None)

        # Process Errors
        if respJSON and isinstance(respJSON, dict) and errorResp:
            errorMsg = errorResp.get("message", None)
            code     = BeblsoftErrorCode.FACEBOOK_GRAPH_REQUEST_FAILED
            if "Invalid OAuth access token" in errorMsg:
                code = BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID
            raise BeblsoftError(code=code, msg="Resp={}".format(respJSON))

        return resp

    # ------------------------- REQUEST JSON -------------------------------- #
    @logFunc()
    def requestJSON(self, **requestKwargs):
        """
        Execute request and return JSON response
        """
        return self.request(**requestKwargs).json()
