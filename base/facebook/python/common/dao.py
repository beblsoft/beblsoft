#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Common DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from urllib.parse import parse_qs, urlparse
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.comment.models import BFacebookCommentModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK COMMON DAO ---------------------- #
class BFacebookCommonDAO():
    """
    Beblsoft Facebook Common DAO
    """

    # ------------------- COMMENTS ------------------------------------------ #
    @staticmethod
    @logFunc()
    def getCommentList(bFBGraph, objID, accessToken, filter="toplevel",  # pylint: disable=W0622
                       fieldList=None, nextLink=None, pageLimit=100):
        """
        Return object comment list

        Args
          objID:
            Facebook object ID
          filter:
            One of "toplevel" or "stream"
            toplevel (default) - returns all top level comments in chronological order
            stream - returns all-level comments in chronological order

        Returns
          (BFacebookCommentModel List, nextLink)
          nextLink is None if no more
        """
        params                    = {"filter": filter,
                                     "fields": ",".join(fieldList if fieldList else BFacebookCommentModel.DEFAULT_FIELD_LIST)}
        (objList, nextLink)       = BFacebookCommonDAO.getEdgeList(bFBGraph=bFBGraph, objID=objID, edgeName="comments",
                                                                   params=params, accessToken=accessToken,
                                                                   nextLink=nextLink, pageLimit=pageLimit)
        bFacebookCommentModelList = [BFacebookCommentModel(obj = obj) for obj in objList]
        return (bFacebookCommentModelList, nextLink)

    @staticmethod
    @logFunc()
    def getAllComments(**getCommentListKwargs):
        """
        Return all object comments
        """
        return BFacebookCommonDAO.getAllEdges(getEdgeListFunc=BFacebookCommonDAO.getCommentList, **getCommentListKwargs)

    # ------------------- GENERIC EDGE -------------------------------------- #
    @staticmethod
    @logFunc()
    def getEdgeList(bFBGraph, objID, edgeName, params, accessToken, nextLink=None, pageLimit=100):
        """
        Return object edge object list

        Args
          objID:
            Facebook object ID
          edgeName:
            Edge name
            Ex. "comments"
          params:
            Parameters to pass to get request
          accessToken:
            Facebook access token
          nextLink:
            Next link from previous request to getEdgeList
          pageLimit:
            Page size to query facebook

        Return
          (List of all object edges, nextLink)
          nextLink is None if no more
        """
        nextParams      = {}
        if nextLink:
            nextParams  = parse_qs(urlparse(nextLink).query)
        params.update(nextParams)
        params["limit"] = pageLimit

        # Send request
        resp            = bFBGraph.requestJSON(
            method          = "GET",
            relativePath    = "{}/{}".format(objID, edgeName),
            accessToken     = accessToken,
            params          = params)

        edgeList        = resp.get("data", [])
        nextLink        = resp.get("paging", {}).get("next", None)

        return (edgeList, nextLink)

    # ------------------- GET ALL EDGES ------------------------------------- #
    @staticmethod
    @logFunc()
    def getAllEdges(getEdgeListFunc, **getEdgeListFuncKwargs):
        """
        Return all object edges

        Args
          getEdgeListFunc:
            Iterator function to return one section of list
            Returns (curEdgeList, nextLink)
          getEdgeListFuncKwargs:
            kwargs to getEdgeListFunc

        Return
          List of all object edges
        """
        edgeList = []
        nextLink = None
        done     = False

        while not done:
            (curEdgeList, nextLink) = getEdgeListFunc(nextLink=nextLink, **getEdgeListFuncKwargs)
            edgeList               += curEdgeList
            done                    = nextLink is None
        return edgeList
