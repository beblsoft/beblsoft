#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Photo Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK PHOTO IMAGE MODEL --------------- #
class BFacebookPhotoImageModel():
    """
    Beblsoft Facebook Photo Image
    """

    def __init__(self, source, width, height):
        """
        Initialize object
        Args
          source:
            Source URL
            Ex. 'https://scontent.xx.fbcdn.net/v/t1.0-9/166655_10150335356370507_4065466_n.jpg?_nc_cat=110&_nc_ht=scontent.xx&oh=09fd480ebadc164be836e7326c26a8b0&oe=5C6B64D5',
          width:
            Width in pixels
            Ex. 720
          height:
            Height in pixels
            Ex. 478
        """
        self.source = source
        self.width  = width
        self.height = height

    def __str__(self):
        return "[{} source={} width={} height={}]".format(
            self.__class__.__name__, self.source, self.width, self.height)

    @staticmethod
    def fromDict(d):
        """
        Return model from dictionary
        """
        return BFacebookPhotoImageModel(
            source = d.get("source"),
            width  = d.get("width"),
            height = d.get("height"))


# ----------------------- BEBLSOFT FACEBOOK PHOTO MODEL --------------------- #
class BFacebookPhotoModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Photo Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/photo/

    obj Format:
    {
      'id': '10150335356370507',
      'created_time': '2010-12-27T03:14:35+0000',
      'updated_time': '2010-12-27T03:14:36+0000',
      'width': 720
      'height': 478,
      'images': [{
          'height': 478,
          'source': 'https://scontent.xx.fbcdn.net/v/t1.0-9/166655_10150335356370507_4065466_n.jpg?_nc_cat=110&_nc_ht=scontent.xx&oh=09fd480ebadc164be836e7326c26a8b0&oe=5C6B64D5',
          'width': 720},
         {'height': 320,
          'source': 'https://scontent.xx.fbcdn.net/v/t1.0-0/p320x320/166655_10150335356370507_4065466_n.jpg?_nc_cat=110&_nc_ht=scontent.xx&oh=be5be5c9a01390ae19478dfb27fb8deb&oe=5CB22BAE',
          'width': 482},
         {'height': 130,
          'source': 'https://scontent.xx.fbcdn.net/v/t1.0-0/p130x130/166655_10150335356370507_4065466_n.jpg?_nc_cat=110&_nc_ht=scontent.xx&oh=82c322f05dee4341e176ee934f5252f2&oe=5C71D2F8',
          'width': 195},
         {'height': 225,
          'source': 'https://scontent.xx.fbcdn.net/v/t1.0-0/p75x225/166655_10150335356370507_4065466_n.jpg?_nc_cat=110&_nc_ht=scontent.xx&oh=2dd37d9fce0b6662269e673e0a712642&oe=5C66B413',
          'width': 338}
      ],
      'comments': {'data': [],
        'paging': {
          'cursors': {
             'after': 'QVFIUkx6eDE4cW1ZAY0d3RU5aMnVqenIwM2pZAUzNxSzM3bGlFWFVmR1RSX3RsRm56OXFVMHVoQ1MwSXo3a0FmUHpsTE93cElUYWtleEluc2QxU3gtNUcwRjJR',
             'before': 'QVFIUkxCVWEyTmN2ZA01JYzJPRGFKdXZAPS3VQU2JSaksyUFlGT21xQmpRN1hXTmR3Nkl5WlVjS3VWZAVU3d3VCWTRHQVExOUY5aUNKdmtvYkNjZAFFtbEZAmaGV3'
          }
         },
         'summary': {
           'can_comment': True,
           'order': 'chronological',
           'total_count': 2
         }
      },
      'likes': {
        'data': [],
        'paging': {
          'cursors': {
             'after': 'QVFIUkVudTM1NHJhakhwYzU3dl9MQmc4SHpXMFd3ZAGJQSTZA5bllJNzNLWlY0VC1wNWNJQzNFZA2JfUkJKUTduN1JaTmoZD',
             'before': 'QVFIUkVudTM1NHJhakhwYzU3dl9MQmc4SHpXMFd3ZAGJQSTZA5bllJNzNLWlY0VC1wNWNJQzNFZA2JfUkJKUTduN1JaTmoZD'
           }
        },
        'summary': {
          'can_like': True,
          'has_liked': False,
          'total_count': 2
         }
      },
      'reactions': {
        'data': [],
        'paging': {
          'cursors': {
            'after': 'TVRJek1qWXhNREV5TVRveE1qazJNVFF6TlRjek9qSTFOREE1TmpFMk1UTT0ZD',
            'before': 'TVRJek1qWXhNREV5TVRveE1qazJNVFF6TlRjek9qSTFOREE1TmpFMk1UTT0ZD'
          }
        },
        'summary': {
          'total_count': 2,
          'viewer_reaction': 'NONE'
        }
      },
    }
    """
    DEFAULT_FIELD_LIST  = ["id",
                           "created_time",
                           "updated_time",
                           "images",
                           "height",
                           "width",
                           "comments.summary(true)",
                           "likes.summary(true)",
                           "reactions.summary(true)"]

    DESCRIBE_PROPERTIES = ["id", "createDate", "updateDate", "largestImage"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("created_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def updateDate(self):
        """
        Update date in utc
        """
        d = datetime.strptime(self.getValueFromObj("updated_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def nComments(self):  # pylint: disable=C0111
        return self.getValueFromObj("comments.summary.total_count")

    @property
    def nLikes(self):  # pylint: disable=C0111
        return self.getValueFromObj("likes.summary.total_count")

    @property
    def nReactions(self):  # pylint: disable=C0111
        return self.getValueFromObj("reactions.summary.total_count")

    @property
    def largestImage(self):  # pylint: disable=C0111
        d = self.getValueFromObj("images")[0]
        return BFacebookPhotoImageModel.fromDict(d)

    @largestImage.setter
    def largestImage(self, bFBPhotoImageModel):
        """
        Set largest image
        """
        d = self.getValueFromObj("images")[0]
        d["source"] = bFBPhotoImageModel.source
        d["height"] = bFBPhotoImageModel.height
        d["width"]  = bFBPhotoImageModel.width

    @property
    def smallestImage(self):  # pylint: disable=C0111
        d = self.getValueFromObj("images")[-1]
        return BFacebookPhotoImageModel.fromDict(d)
