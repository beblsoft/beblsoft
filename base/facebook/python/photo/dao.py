#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Photo DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.photo.models import BFacebookPhotoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK PHOTO DAO ----------------------- #
class BFacebookPhotoDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook Photo DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, photoID, accessToken, fieldList=None):
        """
        Returns
          BFacebookPhotoModel
        """
        fieldList = fieldList if fieldList else BFacebookPhotoModel.DEFAULT_FIELD_LIST
        obj       = bFBGraph.requestJSON(
            method          = "GET",
            relativePath    = "{}".format(photoID),
            accessToken     = accessToken,
            params          = {"fields": ",".join(fieldList)})
        return BFacebookPhotoModel(obj=obj)
