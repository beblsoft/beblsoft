#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Debug Access Token Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timedelta
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK DEBUG ACCESS TOKEN MODEL -------- #
class BFacebookDebugAccessTokenModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Debug Access Token Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/debug_token

    obj Format:
    {
     'app_id': '1925974167437170',
     'application': 'BeblsoftDemo',
     'data_access_expires_at': 1550421648,
     'expires_at': 1542650400,
     'is_valid': True,
     'scopes': ['user_birthday', 'user_videos', ...  'user_posts', 'email', 'public_profile'],
     'type': 'USER',
     'user_id': '10160135909285507'
    }
    """

    DESCRIBE_PROPERTIES = ["appID", "appName", "expireDate", "scopes", "isValid", "userID"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def appID(self):  # pylint: disable=C0111
        return self.getValueFromObj("app_id")

    @property
    def appName(self):  # pylint: disable=C0111
        return self.getValueFromObj("application")

    @property
    def expireDate(self):
        """
        Returns datetime when token expires
        """
        return datetime.fromtimestamp(self.getValueFromObj("expires_at"))

    @property
    def dataAccessExpireDate(self):
        """
        Return datetime when app loses access to data
        """
        return datetime.fromtimestamp(self.getValueFromObj("data_access_expires_at"))

    @property
    def scopeList(self):  # pylint: disable=C0111
        return self.getValueFromObj("scopes")

    @property
    def isValid(self):  # pylint: disable=C0111
        return self.getValueFromObj("is_valid")

    @property
    def userID(self):  # pylint: disable=C0111
        return self.getValueFromObj("user_id")

    # ------------------- VERIFY VALID -------------------------------------- #
    @logFunc()
    def verifyValid(self):
        """
        Verify token is valid
        """
        if not self.isValid:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID)

    # ------------------- VERIFY USER ID ------------------------------------ #
    @logFunc()
    def verifyUserID(self, userID):
        """
        Verify that correct userID
        """
        if str(userID) != str(self.userID):
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID_USERID)

    # ------------------- VERIFY TIME REMAINING ----------------------------- #
    @logFunc()
    def verifyTimeRemaining(self, remainingDelta=timedelta(days=2)):
        """
        Verify not expired
        Args
          remainingDelta:
            Time delta that needs to be remaining to not consider the
            the token expired
        """
        utcnow = datetime.utcnow()
        if self.expireDate - remainingDelta < utcnow:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_HAS_EXPIRED,
                                msg="expireDate={}".format(self.expireDate))

        if self.dataAccessExpireDate - remainingDelta < utcnow:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_HAS_EXPIRED,
                                msg="dataAccessExpireDate={}".format(self.dataAccessExpireDate))

    # ------------------- VERIFY PERM GRANTED ------------------------------- #
    @logFunc()
    def verifyPermGranted(self, permStr):
        """
        Verify that permission has been granted
        Args
          permStr:
            Facebook permission string
            Ex. "user_birthday"
        """
        granted         = False
        for curScope in self.scopeList:
            if curScope == permStr:
                granted = True
                break

        if not granted:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_PERMISSION_REQUIRED,
                                extMsgFormatDict={"permStr": permStr})

    @logFunc()
    def verifyPermListGranted(self, permStrList):
        """
        Verify that permission list have all been granted
        Args
          permStrList:
            Facebook permission string list
            Ex. ["user_birthday", "user_location"]
        """
        for permStr in permStrList:
            self.verifyPermGranted(permStr)
