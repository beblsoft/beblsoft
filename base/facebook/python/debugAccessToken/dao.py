#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Debug Access Token DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.debugAccessToken.models import BFacebookDebugAccessTokenModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK DEBUG ACCESS TOKEN DAO ---------- #
class BFacebookDebugAccessTokenDAO():
    """
    Beblsoft Facebook Debug Access Token DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bFBGraph, inputToken):
        """
        Returns
          BFacebookDebugAccessTokenModel
        """
        obj = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "debug_token",
            accessToken   = bFBGraph.appAccessToken,
            params        = {"input_token": inputToken}
        )["data"]

        return BFacebookDebugAccessTokenModel(obj=obj)
