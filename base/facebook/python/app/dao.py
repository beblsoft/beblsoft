#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook App DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.app.models import BFacebookAppModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK APP DAO ------------------------- #
class BFacebookAppDAO():
    """
    Beblsoft Facebook App DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, appID, fieldList=None):
        """
        Returns
          BFacebookAppModel
        """
        fieldList        = fieldList if fieldList else BFacebookAppModel.DEFAULT_FIELD_LIST
        obj              = bFBGraph.requestJSON(
            method          = "GET",
            relativePath    = "{}".format(appID),
            accessToken     = bFBGraph.appAccessToken,
            params          = {"fields": ",".join(fieldList)})
        return BFacebookAppModel(obj=obj)
