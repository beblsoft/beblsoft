#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook App Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK APP MODEL ----------------------- #
class BFacebookAppModel(BFacebookCommonModel):
    """
    Beblsoft Facebook App Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/application/

    obj Format:
    {
       'app_domains': ['localhost'],
       'app_name': 'BeblsoftDemo',
       'app_type': 0,
       'contact_email': 'benssonjames@gmail.com',
       'creator_uid': '10160135909285507',
       'id': '1925974167437170',
       'name': 'BeblsoftDemo'
    }
    """
    DEFAULT_FIELD_LIST  = ["id",
                           "app_domains",
                           "app_name",
                           "app_type",
                           "description",
                           "company",
                           "contact_email",
                           "creator_uid",
                           "name"]
    DESCRIBE_PROPERTIES = ["id", "name", "creatorID"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @property
    def creatorID(self):  # pylint: disable=C0111
        return self.getValueFromObj("creator_uid")
