#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook User DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.user.models import BFacebookUserModel, BFacebookUserPermissionsModel
from base.facebook.python.post.models import BFacebookPostModel
from base.facebook.python.photo.models import BFacebookPhotoModel
from base.facebook.python.video.models import BFacebookVideoModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK USER DAO ------------------------ #
class BFacebookUserDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook User DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, userID, accessToken, fieldList=None):
        """
        Returns
          BFacebookUserModel
        """
        fieldList  = fieldList if fieldList else BFacebookUserModel.DEFAULT_FIELD_LIST
        obj        = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}".format(userID),
            accessToken   = accessToken,
            params        = {"fields": ",".join(fieldList)})
        return BFacebookUserModel(obj=obj)

    # ------------------- POSTS --------------------------------------------- #
    @staticmethod
    @logFunc()
    def getPostList(bFBGraph, userID, accessToken, type="posts",  # pylint: disable=W0622
                    fieldList=None, order="chronological", nextLink=None, pageLimit=100):
        """
        Return list of posts

        Args
          type:
            Type of posts
            One of "feed", "posts", "tagged"
          fieldList:
            List of post attributes
          order:
            One of "chronological" or "reverse_chronological"
          pageLimit:
            Page size to query facebook

        Note 1:
          Only userID comments are returned, other user's comments are not returned

        Returns
          (BFacebookPostModel List, nextLink)
          nextLink is None if no more
        """
        params                 = {"order": order,
                                  "fields": ",".join(fieldList if fieldList else BFacebookPostModel.DEFAULT_FIELD_LIST)}
        (objList, nextLink)    = BFacebookUserDAO.getEdgeList(bFBGraph=bFBGraph, objID=userID,
                                                              edgeName=type, params=params, accessToken=accessToken,
                                                              nextLink=nextLink, pageLimit=pageLimit)
        bFacebookPostModelList = [BFacebookPostModel(obj=obj) for obj in objList]
        return (bFacebookPostModelList, nextLink)

    @staticmethod
    @logFunc()
    def getAllPosts(**getPostListKwargs):
        """
        Return all posts
        """
        return BFacebookUserDAO.getAllEdges(getEdgeListFunc=BFacebookUserDAO.getPostList, **getPostListKwargs)

    # ------------------- PHOTOS -------------------------------------------- #
    @staticmethod
    @logFunc()
    def getPhotoList(bFBGraph, userID, accessToken, type="uploaded", fieldList=None,  # pylint: disable=W0622
                     order="chronological", nextLink=None, pageLimit=100):
        """
        Return user photos

        Args
          type:
            Type of photo
            One of "tagged" or "uploaded"
          fieldList:
            List of photo attributes
          order:
            One of "chronological" or "reverse_chronological"
          pageLimit:
            Page size to query facebook

        Note:
          To retrieve a tagged in photo that was NOT uploaded by the user you are querying,
          there are two conditions:
            1. The user you are querying has to have accepted the
               user_friends permission
            2. Their friend who owns the photo in which the user is tagged has to
               also have accepted the user_photos & user_friends permission
          See: https://stackoverflow.com/questions/35059150/graph-api-not-returning-all-tagged-in-photos

        Returns
          (BFacebookPhotoModel List, nextLink)
          nextLink is None if no more
        """
        params                  = {"order": order,
                                   "type": type,
                                   "fields": ",".join(fieldList if fieldList else BFacebookPhotoModel.DEFAULT_FIELD_LIST)}
        (objList, nextLink)     = BFacebookUserDAO.getEdgeList(bFBGraph=bFBGraph, objID=userID,
                                                               edgeName="photos", params=params,
                                                               accessToken=accessToken,
                                                               nextLink=nextLink, pageLimit=pageLimit)
        bFacebookPhotoModelList = [BFacebookPhotoModel(obj=obj) for obj in objList]
        return (bFacebookPhotoModelList, nextLink)

    @staticmethod
    @logFunc()
    def getAllPhotos(**getPhotoListKwargs):
        """
        Return all photos
        """
        return BFacebookUserDAO.getAllEdges(getEdgeListFunc=BFacebookUserDAO.getPhotoList, **getPhotoListKwargs)

    # ------------------- VIDEOS -------------------------------------------- #
    @staticmethod
    @logFunc()
    def getVideoList(bFBGraph, userID, accessToken, type="uploaded", fieldList=None,  # pylint: disable=W0622
                     order="chronological", nextLink=None, pageLimit=100):
        """
        Return user videos

        Args
          type:
            Type of photo
            One of "tagged" or "uploaded"
          fieldList:
            List of photo attributes
          order:
            One of "chronological" or "reverse_chronological"
          pageLimit:
            Page size to query facebook

        Returns
          (BFacebookVideoModel List, nextLink)
          nextLink is None if no more
        """
        params                  = {"order": order,
                                   "type": type,
                                   "fields": ",".join(fieldList if fieldList else BFacebookVideoModel.DEFAULT_FIELD_LIST)}
        (objList, nextLink)     = BFacebookUserDAO.getEdgeList(bFBGraph=bFBGraph, objID=userID,
                                                               edgeName="videos", params=params,
                                                               accessToken=accessToken,
                                                               nextLink=nextLink, pageLimit=pageLimit)
        bFacebookVideoModelList = [BFacebookVideoModel(obj=obj) for obj in objList]
        return (bFacebookVideoModelList, nextLink)

    @staticmethod
    @logFunc()
    def getAllVideos(**getVideoListKwargs):
        """
        Return all videos
        """
        return BFacebookUserDAO.getAllEdges(getEdgeListFunc=BFacebookUserDAO.getVideoList, **getVideoListKwargs)

    # ------------------------- GET PERMISSIONS ----------------------------- #
    @staticmethod
    @logFunc()
    def getPermissions(bFBGraph, userID, accessToken):
        """
        Returns
          BFacebookUserPermissionsModel
        """
        obj = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}/permissions".format(userID),
            accessToken   = accessToken)
        return BFacebookUserPermissionsModel(obj=obj)
