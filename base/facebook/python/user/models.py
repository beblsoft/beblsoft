#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook User Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK USER MODEL ---------------------- #
class BFacebookUserModel(BFacebookCommonModel):
    """
    Beblsoft Facebook User Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/user/

    obj Format:
    {
     "birthday": "08/07/1990",
     "email": "benssonjames@gmail.com",
     "first_name": "James",
     "hometown": {"id": "104055622965100", "name": "Attleboro, Massachusetts"},
     "id": "10160135909285507",
     "last_name": "Bensson",
     "location": {"id": "108284282528586", "name": "Providence, Rhode Island"},
     "name": "James Bensson",
     "short_name": "James"
    }
    """

    DEFAULT_FIELD_LIST  = ["id", "name", "first_name", "last_name", "middle_name", "short_name",
                           "gender", "address", "age_range", "birthday", "email", "hometown", "link",
                           "location"]

    DESCRIBE_PROPERTIES = ["id", "name", "email", "gender", "birthday", "hometown"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return int(self.getValueFromObj("id"))

    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @name.setter
    def name(self, name):
        self.obj["name"] = name

    @property
    def email(self):  # pylint: disable=C0111
        return self.getValueFromObj("email")

    @property
    def gender(self):  # pylint: disable=C0111
        return self.getValueFromObj("gender")

    @property
    def birthday(self):  # pylint: disable=C0111
        """
        Return birthday as a datetime
        Note on parsing:
          The person's birthday. This is a fixed format string, like MM/DD/YYYY.
          However, people can control who can see the year they were born separately
          from the month and day so this string can be only the year (YYYY) or
          the month + day (MM/DD)
        """
        dt   = None
        val  = self.getValueFromObj("birthday")
        for fmt in ["%m/%d/%Y", "%m/%d", "%Y"]:
            try:
                dt = datetime.strptime(val, fmt)
                break
            except (ValueError, TypeError):
                pass
        return dt

    @property
    def hometown(self):  # pylint: disable=C0111
        return self.getValueFromObj("hometown.name")


# ----------------------- BEBLSOFT FACEBOOK USER PERMISSIONS MODEL ---------- #
class BFacebookUserPermissionsModel(BFacebookCommonModel):
    """
    Beblsoft Facebook User Permissions Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/user/permissions/

    obj Format:
    [
      {"permission": "user_birthday", "status": "granted"},
      {"permission": "user_hometown", "status": "granted"},
      {"permission": "user_location", "status": "granted"},
      {"permission": "user_likes", "status": "granted"},
      {"permission": "user_photos", "status": "granted"},
      {"permission": "user_videos", "status": "granted"},
      {"permission": "user_friends", "status": "granted"},
      {"permission": "user_tagged_places", "status": "granted"},
      {"permission": "user_posts", "status": "granted"},
      {"permission": "email", "status": "granted"},
      {"permission": "public_profile", "status": "granted"}]
    ]
    """

    DESCRIBE_PROPERTIES = ["obj"]

    # ------------------- VERIFY GRANTED ------------------------------------ #
    def verifyGranted(self, permStr):
        """
        Verify that permission has been granted
        Args
          permStr:
            Facebook permission string
            Ex. "user_birthday"
        """
        granted         = False
        for curPerm in self.obj:
            curSamePerm = curPerm.get("permission") == permStr
            curGranted  = curPerm.get("status") == "granted"
            granted     = curSamePerm and curGranted
            if granted:
                break

        if not granted:
            raise BeblsoftError(BeblsoftErrorCode.FACEBOOK_PERMISSION_REQUIRED,
                                extMsgFormatDict={"permStr": permStr})

    def verifyListGranted(self, permStrList):
        """
        Verify that permission list have all been granted
        Args
          permStrList:
            Facebook permission string list
            Ex. ["user_birthday", "user_location"]
        """
        for permStr in permStrList:
            self.verifyGranted(permStr)
