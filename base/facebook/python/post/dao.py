#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Post DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.post.models import BFacebookPostModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK POST DAO ------------------------ #
class BFacebookPostDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook Post DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, postID, accessToken, fieldList=None):
        """
        Returns
          BFacebookPostModel
        """
        fieldList = fieldList if fieldList else BFacebookPostModel.DEFAULT_FIELD_LIST
        obj       = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}".format(postID),
            accessToken   = accessToken,
            params        = {"fields": ",".join(fieldList)})
        return BFacebookPostModel(obj=obj)
