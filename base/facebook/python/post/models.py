#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Post Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timedelta, timezone
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK POST MODEL ---------------------- #
class BFacebookPostModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Post Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/post

    obj Format (Status):
    {
      'id': '102691597514550_102690744181302',
      'type': 'status',
      'updated_time': '2019-01-21T15:33:07+0000'
      'created_time': '2019-01-21T15:33:07+0000',
      'message': 'Hello World 1!',
      'permalink_url': 'https://www.facebook.com/102691597514550/posts/102690844181292',
      'likes': {'data': [],
                'summary': {'can_like': False,
                            'has_liked': False,
                            'total_count': 0}},
      'reactions': {'data': [],
                    'summary': {'total_count': 0, 'viewer_reaction': 'NONE'}},
      'comments': {'data': [],
                   'summary': {'can_comment': False,
                               'order': 'chronological',
                               'total_count': 0}},
    }

    obj Format (Photo):
    {
      'id': '102691597514550_102691314181245',
      'type': 'photo',
      'updated_time': '2019-01-21T15:33:57+0000'
      'created_time': '2019-01-21T15:33:57+0000',
      'full_picture': 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/50259443_102691257514584_2236422907347599360_o.jpg?_nc_cat=108&_nc_ht=scontent.xx&oh=cdd52880491e40873d552500c48b19ca&oe=5CFA5208',
      'icon': 'https://www.facebook.com/images/icons/photo.gif',
      'link': 'https://www.facebook.com/photo.php?fbid=102691250847918&set=a.102691310847912&type=3',
      'name': "BeblsoftDemo's cover photo",
      'permalink_url': 'https://www.facebook.com/102691597514550/posts/102690844181292',
      'object_id': '102691250847918',
      'reactions': {'data': [],
                    'summary': {'total_count': 0, 'viewer_reaction': 'NONE'}},
      'likes': {'data': [],
                'summary': {'can_like': False,
                            'has_liked': False,
                            'total_count': 0}},
      'comments': {'data': [],
                   'summary': {'can_comment': False,
                               'order': 'chronological',
                               'total_count': 0}},
    }

    obj Format (Video):
    {
      'id': '102691597514550_102700544180322',
      'type': 'video',
      'created_time': '2019-01-21T15:47:39+0000',
      'updated_time': '2019-01-21T15:47:39+0000'
      'permalink_url': 'https://www.facebook.com/102691597514550/posts/102690844181292',
      'full_picture': 'https://scontent.xx.fbcdn.net/v/t15.5256-10/50055905_102701040846939_7867706927457763328_n.jpg?_nc_cat=107&_nc_ht=scontent.xx&oh=4dfcf345f22f67180bfbaaa1b0c43816&oe=5CF7CD37',
      'icon': 'https://www.facebook.com/images/icons/video.gif',
      'link': 'https://www.facebook.com/102691597514550/videos/102700404180336',
      'object_id': '102700404180336',
      'properties': [{'name': 'Length', 'text': '00:14'}],
      'reactions': {'data': [],
                    'summary': {'total_count': 0, 'viewer_reaction': 'NONE'}},
      'likes': {'data': [],
                'summary': {'can_like': False,
                            'has_liked': False,
                            'total_count': 0}},
      'comments': {'data': [],
                   'summary': {'can_comment': False,
                               'order': 'chronological',
                               'total_count': 0}},
    }

    obj Format (Link):
    {
      'id': '102691597514550_102740737509636',
      'type': 'link',
      'created_time': '2019-01-21T16:38:13+0000',
      'updated_time': '2019-01-21T16:38:13+0000'
      'permalink_url': 'https://www.facebook.com/102691597514550/posts/102690844181292',
      'description': 'The official website for all things Disney: theme parks, '
                     'resorts, movies, tv programs, characters, games, videos, '
                     'music, shopping, and more!',
      'full_picture': 'https://external.xx.fbcdn.net/safe_image.php?d=AQAE1GHRKJjiVntu&url=https%3A%2F%2Flumiere-a.akamaihd.net%2Fv1%2Fimages%2Fimage_308e48ed.png&_nc_hash=AQBkCNiJ2mTdOF_w',
      'icon': 'https://www.facebook.com/images/icons/post.gif',
      'link': 'https://www.disney.com/',
      'message': 'Greatest place ever!\n\n',
      'caption': 'disney.com',
      'name': 'Disney.com | The official home for all things Disney',
      'reactions': {'data': [],
                    'summary': {'total_count': 0, 'viewer_reaction': 'NONE'}},
      'likes': {'data': [],
                'summary': {'can_like': False,
                            'has_liked': False,
                            'total_count': 0}},
      'comments': {'data': [],
                   'summary': {'can_comment': False,
                               'order': 'chronological',
                               'total_count': 0}},
    }
    """

    DEFAULT_FIELD_LIST  = ["id",
                           "type",
                           "created_time",
                           "updated_time",
                           "message",
                           "shares",
                           "description",
                           "full_picture",
                           "icon",
                           "object_id",
                           "link",
                           "source",
                           "properties",
                           "caption",
                           "permalink_url",
                           "comments.summary(true)",
                           "name",
                           "likes.summary(true)",
                           "reactions.summary(true)"]
    DESCRIBE_PROPERTIES = ["id", "type", "createDate", "updateDate",
                           "message", "objectID", "fullPicture",
                           "link", "caption", "name", "description",
                           "source", "videoLengthS", "permalinkURL"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def type(self):  # pylint: disable=C0111
        return self.getValueFromObj("type")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("created_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def updateDate(self):
        """
        Update date in utc
        """
        d = datetime.strptime(self.getValueFromObj("updated_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def message(self):  # pylint: disable=C0111
        return self.getValueFromObj("message")

    @message.setter
    def message(self, message):  # pylint: disable=C0111
        """
        Set message
        """
        self.obj["message"] = message

    @property
    def description(self):
        """
        Description of a link in the post
        """
        return self.getValueFromObj("description")

    @property
    def objectID(self):  # pylint: disable=C0111
        return self.getValueFromObj("object_id")

    @property
    def source(self):  # pylint: disable=C0111
        """
        A URL to any Flash movie or video file attached to the post.
        """
        return self.getValueFromObj("source")

    @source.setter
    def source(self, val):
        """
        Used for testing
        """
        self.obj["source"] = val

    @property
    def videoLengthS(self):  # pylint: disable=C0111
        """
        Looking For property with format:
        'properties': [{'name': 'Length', 'text': '00:14'}],
        """
        properties = self.getValueFromObj("properties", default={})
        lengthS    = None
        for prop in properties:
            if prop.get("name", None) == "Length":
                length  = datetime.strptime(prop.get("text"), "%M:%S")
                delta   = timedelta(minutes=length.minute, seconds=length.second)
                lengthS = int(delta.total_seconds())
        return lengthS

    @property
    def fullPicture(self):  # pylint: disable=C0111
        """
        URL to a full-sized version of the Photo published in the Post or scraped
        from a link in the Post. If the photo's largest dimension exceeds 720 pixels,
        it will be resized, with the largest dimension set to 720.
        """
        return self.getValueFromObj("full_picture")

    @fullPicture.setter
    def fullPicture(self, val):
        """
        Used for testing
        """
        self.obj["full_picture"] = val

    @property
    def link(self):  # pylint: disable=C0111
        return self.getValueFromObj("link")

    @link.setter
    def link(self, val):
        """
        Used for testing
        """
        self.obj["link"] = val

    @property
    def caption(self):  # pylint: disable=C0111
        """
        Link caption
        """
        return self.getValueFromObj("caption")

    @property
    def name(self):  # pylint: disable=C0111
        """
        Link name
        """
        return self.getValueFromObj("name")

    @property
    def linkDescription(self):  # pylint: disable=C0111
        """
        Link description
        """
        return self.getValueFromObj("description")

    @property
    def properties(self):  # pylint: disable=C0111
        """
        Object properties
        Ex. Video Length
        """
        return self.getValueFromObj("properties")

    @property
    def permalinkURL(self):
        """
        Permanent Link to underlying post
        Ex. https://www.facebook.com/102691597514550/posts/102690744181302
        """
        return self.getValueFromObj("permalink_url")

    @property
    def nComments(self):  # pylint: disable=C0111
        return self.getValueFromObj("comments.summary.total_count", default=0)

    @property
    def nLikes(self):  # pylint: disable=C0111
        return self.getValueFromObj("likes.summary.total_count", default=0)

    @property
    def nReactions(self):  # pylint: disable=C0111
        return self.getValueFromObj("reactions.summary.total_count", default=0)

    @property
    def isEmpty(self):
        """
        Return True if this is an empty post

        Empirically, Facebook sometimes posts that do not contain any data:
        One example is Post ID 10161898193350507_103035805506 (bensson.james@gmail.com)
        It has the following JSON Packet.
        {
           'id': '10161898193350507_103035805506',
           'created_time': '2008-12-12T03:13:33+0000',
           'comments': {'data': [], 'summary': {'can_comment': True, 'order': 'chronological', 'total_count': 0 }},
           'likes': {'data': [],'summary': {'can_like': True, 'has_liked': False, 'total_count': 0}},
           'reactions': {'data': [],'summary': {'total_count': 0, 'viewer_reaction': 'NONE'}},
           'type': 'status',
           'updated_time': '2008-12-12T03:13:33+0000'
        }
        """
        rval = False
        if self.type == 'status':
            rval = self.message == None
        elif self.type == 'photo':
            rval = self.fullPicture == None
        elif self.type == 'video':
            rval = self.fullPicture == None
        elif self.type == 'link':
            rval = self.name == None
        else:
            rval = True

        return rval
