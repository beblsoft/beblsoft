#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Facebook Link Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.facebook.python.common.models import BFacebookCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK LINK MODEL ---------------------- #
class BFacebookLinkModel(BFacebookCommonModel):
    """
    Beblsoft Facebook Link Model

    Reference:
      https://developers.facebook.com/docs/graph-api/reference/v3.2/link
    """

    DEFAULT_FIELD_LIST  = ["id",
                           "type",
                           "caption",
                           "created_time",
                           "name",
                           "message",
                           "description",
                           "picture",
                           "comments.summary(true)",
                           "likes.summary(true)",
                           "reactions.summary(true)"]
    DESCRIBE_PROPERTIES = ["id", "createDate", "updateDate",
                           "message", "objectID"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def type(self):  # pylint: disable=C0111
        return self.getValueFromObj("type")

    @property
    def createDate(self):
        """
        Create date in utc
        """
        d = datetime.strptime(self.getValueFromObj("created_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def updateDate(self):
        """
        Update date in utc
        """
        d = datetime.strptime(self.getValueFromObj("updated_time"), "%Y-%m-%dT%H:%M:%S%z")
        return d.astimezone(tz=timezone.utc)

    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @property
    def message(self):  # pylint: disable=C0111
        return self.getValueFromObj("message")

    @property
    def description(self):  # pylint: disable=C0111
        return self.getValueFromObj("description")

    @property
    def picture(self):  # pylint: disable=C0111
        return self.getValueFromObj("picture")

    @property
    def nComments(self):  # pylint: disable=C0111
        return self.getValueFromObj("comments.summary.total_count")

    @property
    def nLikes(self):  # pylint: disable=C0111
        return self.getValueFromObj("likes.summary.total_count")

    @property
    def nReactions(self):  # pylint: disable=C0111
        return self.getValueFromObj("reactions.summary.total_count")
