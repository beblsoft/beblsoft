#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Facebook Link DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.facebook.python.common.dao import BFacebookCommonDAO
from base.facebook.python.link.models import BFacebookLinkModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT FACEBOOK LINK DAO ------------------------ #
class BFacebookLinkDAO(BFacebookCommonDAO):
    """
    Beblsoft Facebook Link DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bFBGraph, linkID, accessToken, fieldList=None):
        """
        Returns
          BFacebookLinkModel
        """
        fieldList = fieldList if fieldList else BFacebookLinkModel.DEFAULT_FIELD_LIST
        obj       = bFBGraph.requestJSON(
            method        = "GET",
            relativePath  = "{}".format(linkID),
            accessToken   = accessToken,
            params        = {"fields": ",".join(fieldList)})
        return BFacebookLinkModel(obj=obj)
