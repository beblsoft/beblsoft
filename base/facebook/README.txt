OVERVIEW
===============================================================================
- Facebook Technology Documentation
- Relevant URLS ---------------------
  * Facebook Developer Docs         : https://developers.facebook.com/docs/
  * Facebook App Development        : https://developers.facebook.com/docs/apps
  * Login Web Documentation         : https://developers.facebook.com/docs/facebook-login/web
  * Login Permissions               : https://developers.facebook.com/docs/facebook-login/permissions
  * Platform Policy                 : https://developers.facebook.com/policy
  * App Review Submission           : https://developers.facebook.com/docs/facebook-login/review/how-to-submit
- Relevant Tool URLs ----------------
  * Tools                           : https://developers.facebook.com/tools
  * Javascript Test Console         : https://developers.facebook.com/tools/console/
  * Graph API Explorer              : https://developers.facebook.com/tools/explorer/
  * Access token tool               : https://developers.facebook.com/tools/accesstoken
  * Upgrade Tool                    : https://developers.facebook.com/docs/graph-api/advanced/api-upgrade-tool
- APIs, Client Libraries ------------
  * Python SDK                      : https://facebook-sdk.readthedocs.io/en/latest/api.html
  * Python SDK Github               : https://github.com/mobolic/facebook-sdk
  * Javascript SDK                  : https://developers.facebook.com/docs/javascript
- Status ----------------------------
  * Current Platform Status         : https://developers.facebook.com/status/dashboard/
  * Platform issues                 : https://developers.facebook.com/status/issues/


APP BASICS
===============================================================================
- Creating an app                   1 Login to Facebook
                                    2 Create a developer account
                                    3 Create a new facebook app [see settings below]
                                    4 Create test users
                                    5 Submit for review
- Settings --------------------------
  * App ID                         I: Unique ID given to App, passed to APIs, SDKs
  * App Secret                     I: Unique secret assocociated with app that authenticates
                                      requests made by application servers to Facebook
  * App Name                       I: Application Name, Ex. Smeckn
  * App Domains                    I: Provide Google Play and/or Apple App Store URL
  * Privacy Policy URL
  * Terms of Service URL
  * App Icon                       I: Artwork or image used to identify app
  * Category
- Advanced Settings -----------------
  * Upgrade the API Version        I: Change the API version your app uses
  * App Restrictions                : Set Age, Country, Alcohol restrictions and
  *                                   Social Discovery on your app.
  * Security                        : View and reset your Client Token, set your Server IP
  *                                   Whitelist and other security requirements.
  * Domain Manager                  : Add domains or domain prefixes to be prefetched by Facebook.
  * App Page                        : Create a Facebook Page for you App.
  * Business Manager                : Associate your app with a business to manage
  * Advertising Accounts            : Provide one or more IDs for authorized Ad Accounts
  *                                   to be used to run and pay for app ads.
  * Migrations                      : Turn on migrations to roll out API changes.
  * Share Redirect Whitelist        : Allow cross domain share redirects to your urls.
  * Delete App                     I: Permanently delete an app and all its data.
- Developer Roles -------------------
  * Administrators                  : Administrators hav`e complete access to your app.
  * Developers                      : Developers can test your app, view insights, and change most app settings.
                                      The can not add or remove roles, reset the app secret, or delete the app.
  * Testers                         : Testers can only test the app in Development Mode, i.e. not public.
  * Analytic Users                  : Analytic users can only access app analytics. They can not access app
                                      settings or test your app while it is in Development Mode.
- Security --------------------------
  * Development Mode                : When testing apps, put them in development mode
                                      This hides the app entirely from all users who haven't
                                      authorized on app dashboard
  * IP Address Whitelist            : Ensure that developers must use company IP address
                                      to update app settings
  * Update Notification             : Register email address to be notified of app changes
  * Server Whitelist                : Restrict API calls to come from white-listed servers
  * SSL                             : Client must use TLS
                                      Be able to verify a certificate signed using sha256WithRSAEncryption
  * Social Plugin Confiration       : System may ask users to confirm likes or posts
- App Lifecycle ---------------------
  1. Develop                        : App starts in development mode
                                      Gates access to app
                                   I: Toggled in App Review Tab
                                    : Add roles: admin, developer, testers, insight users, test users
  2. Test                           : Use test users to go through end to end flows and social features
  3. Soft Launch and Geo-Restriction: Consider soft-launching in a few specific countries before
                                      releasing it ot the world
  4. Full Launch                    : Remove country restictions
                                    : Change app off of development mode
                                    : Submit app to App Review



ACCESS TOKENS
===============================================================================
- Description -----------------------
  * Where they come from            : After a user connects with app via Facebook login and approves app requested
                                      permissions, app obtains access token
  * Definition                      : Access token is an opaque string that identifies a user, app, or page
                                      and can be used to make graph API calls
  * Portability                     : The same token can be used by different platforms: web,
                                      android, iOS, or server side calls
  * Authentication                  : Enables people to log in to your mobile or web app and create an account using FB credentials
    Expires                         : 60 days after last use
  * Data Access                     : Allows application to access user data
    Expires                         : 90 days since authorization
                                      After this time, must reauthorize app's permissions
    Check expiration                : Use debug_token endpoint
- Types -----------------------------
  * User Access Token               : Most common type of token
                                      Needed any time the app calls an API to read, modify, or write
                                      a specific person's facebook data on their behalf
  * App Access Token                : Needed to modify and read app settings
                                      Obtain via server-to-server call
  * Page Access Token               : Similar to user access tokens, except they provide permission
                                      to APIs that read, write, or modify data beloning to facebook page
  * Client Token                    : Identifier to embed into native mobile binaries or desktop
                                      apps to identify app. Isn't meant to be secret
- User Access Tokens ----------------
  * Flow                            1 Client requests access and permissions via SDK and Login Dialog
                                    2 User authenticates and approves permissions
                                    3 Access token is returned to client
  * Short Term                      : Lifetime of an hour or two
  * Long Term                       : Lifetime of about 60 days
                                      Can convert short term to long term using server side call
  * Getting server long lived token
    Picture                         : -----------------------   -----------------------   -----------------------
                                      | Client              |   | Server              |   | Facebook            |
                                      |                     |   |                     |   |                     |
                                      | 1. Short lived      |   |                     |   |                     |
                                      |    through Login ----->---                    |   |                     |
                                      |                     |   | 2. Receives short   |   |                     |
                                      |                     |   |    lived-------------->----                   |
                                      |                     |   |                     |   | 3. Generates long   |
                                      |                     |   |                     |   |    lived            |
                                      |                     |   |                -------<------                 |
                                      |                     |   | 4. Receives long    |   |                     |
                                      |                     |   |    lived            |   |                     |
                                      |                     |   |                     |   |                     |
                                      -----------------------   -----------------------   -----------------------
    Server call                     : GET /oauth/access_token?grant_type=fb_exchange_token&client_id={app-id}&client_secret={app-secret}&fb_exchange_token={short-lived-token}
    Note                            : Long lived token should only be used on the server and
                                      at most 1 client. If more than one client token needed see below
  * Getting client long lived tokens
    Picture                         : -----------------------   -----------------------   -----------------------
                                      | Client              |   | Server              |   | Facebook            |
                                      |                     |   |                     |   |                     |
                                      |                     |   | 1. Long lived       |   |                     |
                                      |                     |   |             ---------->----                   |
                                      |                     |   |                     |   | 2. Generate code    |
                                      |                     |   |             --------<------                   |
                                      |                     |   | 3. Send code back   |   |                     |
                                      |                     |   |    to client        |   |                     |
                                      | 4. Use code to      |   |                     |   |                     |
                                      |    get long lived ----->------------------------->----                  |
                                      |                     |   |                     |   | 5. Generate long    |
                                      |                     |   |                     |   |    lived            |
                                      |                 ------<--------------------------<----                  |
                                      | 6. Receive long     |   |                     |   |                     |
                                      |    lived            |   |                     |   |                     |
                                      |                     |   |                     |   |                     |
                                      |                     |   |                     |   |                     |
                                      -----------------------   -----------------------   -----------------------
    Server call to generate code    : GET https://graph.facebook.com/oauth/client_code?access_token=...&client_secret=...&redirect_uri=...&client_id=...
    Client call to get long lived   : https://graph.facebook.com/oauth/access_token?code=...&client_id=...&redirect_uri=...&machine_id= ...
- App Access Tokens -----------------
  * Generating                      : curl -X GET "https://graph.facebook.com/oauth/access_token?client_id=your-app-id&client_secret=your-app-secret&redirect_uri=your-redirect-url&grant_type=client_credentials"
  * Security                        : NEVER hardcode into client-side code
  * Send app requests using app
    secret not app token            : curl -X GET "https://graph.facebook.com/your-endpoint?key=value&access_token="your-app_id|your-app_secret"
- Page Access Tokens ----------------
  * Generating                      : Admin of page must grant extended permission called manage_pages
- Debugging -------------------------
  * Retreive token information      : GET /debug_token?input_token={input-token}&access_token={access-token}
                                      Where input_token: access token you want information about
                                      access_token: app access token or valid user access token
                                      Ex return.
                                      {
                                         "data": {
                                             "app_id": 000000000000000,
                                             "application": "Social Cafe",
                                             "expires_at": 1352419328,
                                             "is_valid": true,
                                             "issued_at": 1347235328,
                                             "scopes": [
                                                 "email",
                                                 "user_location"
                                             ],
                                             "user_id": 1207059
                                         }
                                     }
  * Expired token errors            : Facebook will error api requests with bad access tokens
                                      Errors will be explained in response payload. For example.
                                      {
                                        "error": {
                                          "message": "Error validating access token: Session has expired on Wednesday, 14-Feb-18 18:00:00 PST. The current time is Thursday, 15-Feb-18 13:46:35 PST.",
                                          "type": "OAuthException",
                                          "code": 190,
                                          "error_subcode": 463,
                                          "fbtrace_id": "H2il2t5bn4e"
                                        }
                                      }
- Request Configurations ------------
  * Login on client
    API Requests from client        : + Simple to implement
                                      - No offline posting, long term access
  * Login on client with
    long term token
    API Requests from client        : + Auth not required often
                                      - No offline posting
  * Login on client
    API Requests from server        : + Offline posting, extra security with server calls (appsecret_proof)
                                      - Client must call server to proxy



PERMISSIONS
===============================================================================
- Description                       : Permissions are how an application asks someone to access their data
                                      Person's privacy settings combined with what app asks for determines
                                      what app can access
- When                              : Ask at any time for permissions
- Portable                          : Permissions granted on one platform are granted on other platforms
- Revoking                          : People can revoke permissions granted at any time
                                      Apps must regularly check their granted permissions
- Pages and Business Assets ---------
  * ads_management
  * ads_read
  * business_management
  * leads_retrieval
  * read_audience_network_insights
  * read_insights
  * manage_pages                    : App can retrieve Page Access Tokens for Pages and Apps person administers
  * pages_show_list
  * pages_manage_cta
  * pages_manage_instant_art
  * publish_pages                   : Publish posts, comments, and like Pages managed by person using app
                                      Must also have manage_pages permission
  * read_page_mailboxes
- User Data Permissions -------------
  * default                         : Does not require app review
                                      id, first_name, last_name, middle_name, name, name_format, picture, short_name
  * email                           : Does not require app review
                                      email
  * groups_access_member_info       : Publicly available group member information
  * publish_to_groups               : Post content into a group on behalf of a User who has granted the app this permission
  * user_age_range                  : age range
  * user_birthday                   : birthday
  * user_events                     : Read only access to events a person is a host or RSVPed to
  * user_friends                    : List of friends that also use the app
  * user_gender                     : Gender
  * user_hometown                   : Hometown
  * user_likes                      : Pages that the person has liked
  * user_link                       : User Facebook profile URL
  * user_location                   : Current location
  * user_photos                     : Photos person has uploaded or been tagged in
  * user_posts                      : Posts on person's timeline, posts they are tagged in
  * user_tagged_places              : Places person has been tagged at
  * user_videos                     : Access to videos person has uploaded
- Optimizing permissions requests ---
  * Guidelines                      : Only ask for what you need. More permissions required, more likely app will not be installed
                                    : Ask in context where permissions are required
                                    : Separate read and publish permissions
                                    : Never ask for permissions needed in the future
                                    : Tell people ahead of time why you are requesting a permission
- Permissions API -------------------
  * Check user permissions          : GET /{user-id}/permissions
  * Returns                         :{
                                       "data": [
                                         {
                                           "permission": "public_profile",
                                           "status": "granted"
                                         },
                                         {
                                           "permission": "publish_actions",
                                           "status": "granted"
                                         },
                                       ]
                                     }
  * Revoke permission               : DELETE /{user-id}/permissions/{permission-name}
  * Revoke Login                    : DELETE /{user-id}/permissions
- Handling Declined Permissions ----1 Continue without information
                                    2 Explain why and reprompt
                                    3 Collect the information yourself



WEB APPS
===============================================================================
- Creating a web app with login -----
  1 Do steps in APP BASICS
  2 Setup App Domains               : Settings > Basic > App Domains > localhost
  3 Facebook Login Settings        I: Client OAuth Login > Yes
                                      Web OAuth Login > Yes
                                      Valid OAuth Redirects > https://localhost:8080/
- Initialize Facebook SDK ----------: Allows application to call into facebook
  * Coode                           : window.fbAsyncInit = function() {
                                        FB.init({
                                          appId      : '{your-app-id}',
                                          cookie     : true,  // enable cookies to allow the server to access
                                                              // the session
                                          xfbml      : true,  // parse social plugins on this page
                                          version    : 'v2.8' // use graph api version 2.8
                                        });
                                        // Check login status here
                                      };
                                      // Load the SDK asynchronously
                                      (function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "https://connect.facebook.net/en_US/sdk.js";
                                        fjs.parentNode.insertBefore(js, fjs);
                                      }(document, 'script', 'facebook-jssdk'));
- Check login status ---------------: Determine if user is logged in
  * Code                            : FB.getLoginStatus(function(response) {
                                        // Do something with response
                                      });
  * Response                        : {  status: 'connected',
                                         authResponse: {
                                           accessToken: '...',
                                           expiresIn:'...',
                                           reauthorize_required_in:'...'
                                           signedRequest:'...',
                                           userID:'...' } }
- Log user in ----------------------: Prompt facebook to log user in
  * Code                            : FB.login(function(response) {
                                        if (response.status === 'connected') {
                                          // Logged into your app and Facebook.
                                        } else {
                                          // The person is not logged into this app or we are unable to tell.
                                        }
                                      }, { scope: 'public_profile,email', return_scopes: true })
  * Args
    cb                              : callback function
    auth_type                       : Ex. 'rerequest', 'reauthenticate'
    scope                           : comma separated list of extended permissions
    return_scopes                   : when true, granted scopes are passed back
    enable_profile_selector         : when true, prompt user to grant permission of pages
    profile_selector_ids            : comma separated list of ids of profile_selector
- Log user out ---------------------: Have facebook log user out
  * Code                            : FB.logout(function(response) {
                                        // Person is now logged out
                                      })
- Access the graph API -------------: Send facebook requests on behalf of user
  * Code                            : FB.api('/me', function(response) {
                                        console.log(JSON.stringify(response))



PLATFORM VERSIONING, UPGRADING
===============================================================================
- APIs versions have 2 types
  1. core                           : Anything considered a core API node, field, edge, dialog, SDK or SDK method
                                      will remail available and unchanged for at least two years.
                                      Any changes must do so by releasing a new version
  2. extended                       : Everything beyond core. Subject to more rapid change.
                                      See platform migrations
- Version Schedules                 : Each version is guaranteed to operate for two years
                                      Each version will no longer be usable two years after the date
                                      that the subsequent version is released
- Unversioned Calls                 : Defaults to the oldest available version of the API
                                      Best to specify a version
- Migrations                        : Migrations are changes to extended elements of Facebook Platform
                                      https://developers.facebook.com/docs/apps/migrations
                                      They give a 90-day window to migrate app.
                                      Activate migrations in App Dashboard under Settings > Advanced > Migrations
                                      Client-side activation:
                                      http://graph.facebook.com/path?migrations_override={"migration1":true, "migration2":false}
- Upgrading                         : Best to upgrade to the latest version to maximize the time between upgrades
- API Upgrade Tool                  : https://developers.facebook.com/tools/api_versioning
                                      Displays a customized list of changes that impact app
- Test Users                        : Can edit test users and override the minimum Graph API version for calls with the user's acces
- Early Upgrade                     : A month before Version Upgrade one can
                                      Upgrade Developers and Admins
                                      Upgrade All Calls, still allows one to go back to previous version



TESTING
===============================================================================
- Test apps -------------------------
  * Description                     : Test apps let you quickly create Facebook App IDs for use during development,
                                      testing, staging, or QA phases or app development
  * Advantages                      : Share the same app-scoped User ID namespace
                                    : One place to manage roles accoss all test apps
                                    : Admins on production have access on test apps
                                    : When creating test apps - the settings are copied
                                      from production app
                                    : Always in development mode, making it less likely someone may chance upon a pre-release
                                    : Mave the same Platform Migration options as production app
                                    : Have the same version availability as production app
  * Manage via app dashboard        : Create/edit/delete
- Test users ------------------------
  * Description                     : A test user is a special facebook account, invisible to real accounts,
                                      which can be created within an app for the purpose of manual or automated testing
  * Graph API Control - All Users   : /app/accounts/test-users
                                      Create, read, get access tokens, associate, dissasociate, login
  * Graph API Control - Single User : /test-user
                                      See details about an individual test account
                                      Update a test
                                      Delete test accounts
                                      Create friend connections
  * Manage via app dashboard        : Create, list, tokens, log in, update, delete, create friends
  * Rules                           : Maximum of 2000 test users/app
                                    : Test users will only have 'Tester' privileges on the
                                      associated app. This means that they can use the app in
                                      Public Mode or Development Mode, but cannot edit any
                                      technical settings or access insights for that app.
                                    : Test accounts cannot be converted to normal user accounts.
- Test cases ------------------------
  * New FB login                    : Someone new logs into your app
  * FB Login with existing email    : Someone logs in with Facebook after previously logging in with same email address
  * Existing FB login               : Existing FB user logs back in
  * Canceled Login
  * User revokes FB permissions     : App should reprompt FB login
  * FB user changes their password  : Causes FB access tokens to become invalid
  * Disabled FB platform
  * Access Token Expired



APP REVIEW
===============================================================================
- Description                       : App review is a review process that facebook uses as a way to ensure
                                      the best posisible facebook experience for your app's audience
- Criteria
  * Utility                         : Requested permissions must clearly improve the user experience
  * Visibility                      : Data gained from the permission needs to be tied to direct use
  * No overrequesting               : App only requests permissions it really needs
  * Write permissions               : Write permissions are not abused
  * Devices                         : App works on several devices



GRAPH API
===============================================================================
- Overview --------------------------
  * Description                     : The Graph API is the primary way to get data into and out of the Facebook Platform.
                                      It's a low level HTTP-based API that apps can use to programmatically manipulate data.
  * Concepts
    node                            : Individual objects. Ex. User, Photo
    edges                           : Connections between a collection of objects and a single object.
                                      Ex. photos on a page
    fields                          : Data about an object
                                      Ex. User's birthday
  * Host URL                        : graph.facebook.com, Video uploads to graph-video.facebook.com
  * Object IDs                      : Nodes are individual objects, each with a unique ID
  * Versions                        : Graph API has multiple versions. No version will default to oldest available version
    Specify a specific version      : GET graph.facebook.com/v2.9/20531316728/photos
- Pagination Routes -----------------
  * before                          : Cursor points to start of current page
  * after                           : Cursor points to end of page of data
  * offset                          : Offset of start of each page
  * since                           : Unix timestap or strtotime data value that points to start of time-based data
  * until                           : Unix timestap or strtotime data value that points to end of time-based data
  * previous                        : Graph API endpoint that will return previous page
  * next                            : Graph API endpoint that will return next page
- Access Token ----------------------
  * Convert to long term            : GET oauth/access_token/?grant_type=fb_exchange_token&client_id=app-id&client_secret=app-secret&fb_exchange_token=short-lived-token
  * Add access token to all requests: POST v2.11/id/feed?message=Hello World!&access_token=access-token
- Error Responses -------------------
  * Fields
    message                         : Human readable description of error
    code                            : Error code
    error_subcode                   : Additional information about error
    error_user_msg                  : Message to display to user
    fbtrace_id                      : Internal support identifier
  * Code or Type
    OAuthException                  : Access token expired
    102                             : API Session expired
    1                               : API Unknown, downtime issue, wait and retry
    2                               : API Service, downtime issue, wait and retry
    4                               : API Too Many Calls
    17                              : API User Too Many Calls
    10                              : API Permission Denied
    190                             : Access token has expired
    200-299                         : API Permission
    341                             : Application limit reached
    368                             : Temporarily blocked for policies violations
    506                             : Duplicate Post
    1609005                         : Error Posting Link
- Debug Mode ------------------------
  * Enabling                        : Use debug in query string
  * Example                         : GET graph.facebook.com/v3.2/me/posts?debug=all
  * Returns debug in payload        :   "__debug__": { }
- Webhooks --------------------------
  * Description                     : Allow apps to receive real-time HTTP notifications of
                                      changes to objects in social graph
- Nested requests -------------------
  * Structure                       : GET {node-id}?fields=<first-level>{<second-level>}
  * Example 1 Level                 : GET me?fields=albums.limit(5){name}
  * Example 2 Levels                : GET me?fields=albums.limit(5){name, photos.limit(2)}
- Large Requests --------------------
  * Use POST instead of GET         : Add method=get as a parameter
- Batch Requests --------------------
  * Get platform and me             : GET /?ids=platform,me
  * Get photos from different uids  : photos?ids={user-id-a},{user-id-b}
- Introspection ---------------------See edge nodes withouth knowing ahead of time
  * Show metadata on me             : me?metadata=1
- Searching -------------------------
  * Search for a place              : GET search?q=coffee&type=place&center=37.76,-122.427&distance=1000
- Batch Requests --------------------Make multiple requests to api at once
  * Example                         : curl \
                                      -F 'access_token=...' \
                                      -F 'batch=[{"method":"GET", "relative_url":"me"},{"method":"GET", "relative_url":"me/friends?limit=50"}]' \
                                      https://graph.facebook.com
- Aliasing Fields ------------------Provide aliases for fields using the as parameter
  * Example                         : GET me?fields=id,name,picture.width(100).height(100).as(picture_small),picture.width(720).height(720).as(picture_large)
- Localalization --------------------
  * Language codes                  : Uses formate ll_CC where
                                      ll = Language code
                                      CC = country code
  * Example Codes                   : en_US, ar_AR, es_LA
  * Users Locale                    : See User locale field
  * Load correct FB SDK             : js.src = "https://connect.facebook.net/es_LA/all.js";



GRAPH API ENDPOINT REFERENCE
===============================================================================
- Generic Samples -------------------
  * Get page                        : GET 20531316728
  * Get page cover                  : GET 20531316728?fields=cover
  * Get page feed edge              : GET 820882001277849/feed
  * Get page feed with limit        : GET 820882001277849?fields=feed.limit(3)
  * Get page feed in chron order    : GET 1809938745705498?fields=comments.order(chronological).limit(3)
  * Get page feed in rev chron      : GET 1809938745705498?fields=comments.order(reverse_chronological).limit(3)
  * Get page node photos fields     : GET 20531316728/photos?fields=height,width,link
  * Create a comment on a photo     : POST 1809938745705498/comments?message=Awesome!
  * Update comment message          : POST 1809938745705498_1810399758992730?message=Happy%20Holidays!
  * Delete comment                  : DELETE 1809938745705498_1810399758992730
- Application ----------------------- A facebook app
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/application
  * Fields
    id                              : app id
    app_domains                     : domains and subdomains this app can use
    app_name                        : app name
    app_type                        : app type
    category                        : app category
    contact_email                   : Email address listed for people using the app
    created_time                    : Timestamp when app was created
    creator_uid                     : User id of app creator
    icon_url                        : URL of app's icon
    link                            : Link to app on facebook
    logo_url                        : URL of app's logo
    name                            : app name
    website_url                     : App url website
  * Edges
    accounts                        : Test accounts associated with app
    appassets                       : Application assets
    banned                          : List of people banned from app
    picture                         : App's profile picture
    roles                           : Developer roles
  * Examples
    Get app                         : GET {application-id}
    Get accounts                    : GET {application-id}/accounts
    Create account                  : POST {application-id}/accounts
    Delete account                  : DELETE {account-id}
- User ------------------------------
  * URLs
    User reference                  : https://developers.facebook.com/docs/graph-api/reference/user/
    User feed reference             : https://developers.facebook.com/docs/graph-api/reference/v3.2/user/feed
  * Fields
    id                              : id of person's user account
    address                         : user's address
    age_range
    birthday                        : MM/DD/YYYY
    email                           : Primary email
    first_name                      : Person's first name
    gender                          : male or female
    hometown
    last_name
    link                            : Link to timeline
    location
    middle_name
    name                            : Full name
    short_name                      : Shortened, locale-aware name
  * Edges
    family                          : Person's family relationships
    friends                         : Person's friends
    likes                           : All the pages the person has liked
    permissions                     : Permissions person has granted app
    feed                            : Feed of posts and links published by this person
    home                            : Homepage feed
    videos                          : Videos person is tagged in or has updated
  * Examples
    Get me                          : GET me?fields=id,name,education,address,birthday,gender,currency,hometown,significant_other,middle_name,cover,email,first_name,last_name,name_format,political,relationship_status,religion,family,photos.limit(10),friends.limit(10),likes.limit(10),posts.limit(10)
    Get my posts [content I added]  : GET me?fields=posts.limit(3){id,type,message,full_picture,from,created_time,updated_time},id,name
    Get my feed                     : GET me?fields=feed
    Get my photos                   : GET me?fields=photos.limit(5){id}
    Get friends installed app
    and total friends               : GET me/friends
    Get number of pages I've liked  : GET me?fields=likes.summary(true)
- Test User -------------------------Test user associated with a facebook app
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/test-user
  * Examples
    Create test user                : POST {app-id}/accounts/test-users
    Get all                         : GET 1925974167437170/accounts/test-users?fields=id,login_url,access_token
    Create                          : Use web console
    Delete                          : DELETE 108932333457381
- Comment ---------------------------Comment made on various types of content
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/comment
  * Fields
    id
    attachment                      : StoryAttachment
    comment_count                   : Number of replies to this comment
    created_time                    : Time comment was made
    from                            : Person who made comment
    like_count                      : Number of times comment liked
    message                         : Comment Text
    object                          : Parent photo or video
    parent                          : Comment parent for comment replies
  * Edges
    comments
    likes
    reactions
    private_replies
  * Examples
    Get post comments               : GET 10160135909285507_10154209800235507?fields=comments{id,from,message,object,parent,created_time}
    Get comment                     : 10154209800235507_10154212622780507?fields=id,object,parent,from,created_time,message,likes.summary(true)
- Photo -----------------------------
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/photo/
  * Fields
    id
    album                           : Album containing photo
    backdated_time
    created_time
    from                            : Profile that uploaded photo
    height
    icon
    images                          : Different stored representations of picture
    link                            : Link to photo on facebook
    name                            : User provided caption
    picture                         : Link to 100px wide photo
    place                           : Place associated with photo
    updated_time                    : Last time photo was updated
    target                          : Target photo is published to
    width                           : Width of photo in pixels
  * Edges
    likes
    reactions
    comments                        : Commonts on photo
    tags                            : Users tagged in photo
  * Examples
    Get my photos                   : me?fields=photos.limit(3){id,album,created_time,updated_time,height,width,picture,images}
    Get photo metadata              : 10150244201455507?fields=comments.summary(true){id,from,message},reactions.summary(true)
- Post ------------------------------
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/post
  * Fields
    id
    cpation                         : link caption
    created_time                    : time post was initially published
    description                     : description of a link in the post
    from                            : name and id about profile that created post
    full_picture                    : URL to full-sized version of photo published
    icon                            : Link to icon
    link                            : Link attached to this post
    message                         : Status message in the post
    message_tags                    : Array of profiles tagged in message
    name                            : name of the link
    object_id                       : ID of uploaded photo or video attached
    picture                         : Picture resized to 130 pixels
    privacy                         : Post privacy settings
    properties                      : Video properties
    shares                          : Shares count of this post
    type                            : Type of post {link, status, photo, video, offer}
    updated_time                    : When object was updated
  * Edges
    comments
    likes
    reactions
  * Examples
    Get my posts with fields        : GET me?fields=posts.order(chronological){id,type,full_picture,created_time,updated_time}
    Get post metadatas              : GET 10160135909285507_10154209800235507?fields=reactions.summary(true),likes.summary(true),comments{id,from}
- Profile ---------------------------
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/profile
  * Description                     : One of: User, Page, Group, Event, Application
- Status ----------------------------
- Thread ----------------------------
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/thread
  * Fields
    id                              : Unique ID for this message thread
    comments                        : The messages in this thread. Message []
    to                              : Profiles that are subscribed to the thread
    unread                          : The amount of messages that are unread by the session profile
    unseen                          : The amount of messages that are unseen by the session profile
    updated_time                    : When the tread was last updated
- URL -------------------------------
- Video -----------------------------
- Conversation ----------------------
- Doc -------------------------------
- Event -----------------------------
- Friend List -----------------------
- Group -----------------------------
- Link ------------------------------
- Message ---------------------------
  * URL                             : https://developers.facebook.com/docs/graph-api/reference/v3.2/message
  * Fields
    created_time                    : A timestamp of when message was created
    from                            : The sender of this message. In some cases the name of the user will
                                      resolve to "Facebook User" in order to protect their privacy
    id                              : The unique ID for message
    message                         : The text of the message
    subject                         : The subject of the message
    tags                            : A set of tags indicating the message folder and source
    to                              : A list of recipients of the message
- Notification ----------------------
- Object Comments -------------------
- Object Likes ----------------------
- Object Sharedposts ----------------
- Page ------------------------------
- Payment ---------------------------
- Place -----------------------------




