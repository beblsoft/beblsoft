#!/usr/bin/env python3
"""
 NAME
  bLastpass.py

 DESCRIPTION
   Lastpass integration

 LASPASS CLI WORKFLOWS
  Logon to LastPass via their CLI and get the password for test1
  To Login w/out prompt:      export LPASS_DISABLE_PINENTRY=1
                              lpass login --trust --force beblsoftmanager@gmail.com
                               <<< <password>
  To see password for test1:  lpass show test1 --password
  To logout:                  lpass logout

 TEST
   ./bLastpass.py
"""


# ----------------------------- IMPORTS ------------------------------------- #
import getpass
import subprocess
import os
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- LASTPASS CLASS ------------------------------ #
class BeblsoftLastpass():
    """
    Lastpass class
    """

    def __init__(self, masterAccount, masterPassword,  # pylint: disable=W0621
                 accounts, login=False):
        """
        Initialize LastPass Instance
        Args
          masterAccount:
            LastPass master account name
            Ex. "beblsoftmanager@gmail.com"
          masterPassword:
            LastPass master password
            Ex. "FooPassword"
          accounts:
            List of LastPass accounts
            Ex. ["QuoteProdDB", "QuoteProdFB"]
          login:
            If True, login to lastpass on object instantiation
        """
        self.masterAccount  = masterAccount
        self.masterPassword = masterPassword
        self.accounts       = accounts
        self.passwordDict   = {}
        self.env            = os.environ.copy()
        self.env.update({"LPASS_DISABLE_PINENTRY": "1"})

        if login:
            self.login()

    def __str__(self):
        s = ""
        s += "LastPassInstance: \n"
        s += "\tmasterAccount {}\n".format(self.masterAccount)
        s += "\tAccount Passwords\n"
        for account in self.passwordDict:
            s += "\t\tAccount: {}, Password: {}\n".format(
                account, self.passwordDict[account])
        return s

    @logFunc()
    def login(self):
        """
        Login to LastPass Account
        """
        try:
            p = subprocess.Popen(["lpass login --trust --force {}".format(self.masterAccount)],
                                 stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                 stderr=subprocess.PIPE, shell=True, env=self.env)

            # Encode and send the master password
            password = self.masterPassword.encode("utf-8")
            stdout, stderr = p.communicate(password)  # pylint: disable=W0612
            out = stdout.decode("utf-8")

            # Process ouput
            if(out.find("Success") == -1):
                raise BeblsoftError(code=BeblsoftErrorCode.LP_CANNOT_LOGIN)

        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.LP_CANNOT_LOGIN,
                                originalError=e)

    @logFunc()
    def logout(self):  # pylint: disable=R0201
        """
        Logout of  master account
        """
        try:
            subprocess.check_output(
                ["lpass", "logout", "--force"], universal_newlines=True, stderr=subprocess.PIPE)
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.LP_CANNOT_LOGOUT,
                                originalError=e)

    @logFunc()
    def loadAllPasswords(self):
        """
        Load all account passwords
        Note:
          Since logging into LastPass is an expensive operation, we try to use
          an existing login to retrieve the desired accounts.
          If the first attempt fails, we then login and then try again.
        """
        try:
            for account in self.accounts:
                self.passwordDict[account] = self._getPassword(account)
        except BeblsoftError as e:  # pylint: disable=W0612
            self.login()
            for account in self.accounts:
                self.passwordDict[account] = self._getPassword(account)

    @logFunc()
    def getPassword(self, account):
        """
        Get password for named account
        Args
          account: name of account for which password is desired
        Note
          Password lazy-loading
        """
        if account not in self.accounts:
            raise BeblsoftError(code=BeblsoftErrorCode.LP_UNKNOWN_ACCOUNT)
        if account not in self.passwordDict.keys():
            self.loadAllPasswords()
        if account not in self.passwordDict.keys():
            raise BeblsoftError(code=BeblsoftErrorCode.LP_UNABLE_TO_GET_PASSWORD)
        return self.passwordDict[account]

    @logFunc()
    def _getPassword(self, account):
        """
        Get password for named account
        (requires login() to have been called prior to invocation)
        Args
          account: name of account for which password is desired
        """
        password = ""
        strippedOutput = ""
        try:
            password = subprocess.check_output(["lpass", "show", account, "--password"], #pylint: disable=E1123
                                               universal_newlines=True, stderr=subprocess.PIPE,
                                               env=self.env)
            strippedOutput = password.replace("\n", "")
            return(strippedOutput)
        except Exception as e:
            errorMsg = "Unable get password for account {} from LastPass".format(
                account)
            raise BeblsoftError(code=BeblsoftErrorCode.LP_UNABLE_TO_GET_PASSWORD,
                                msg=errorMsg, originalError=e)


# ----------------------------- MAIN: TEST HARNESS -------------------------- #
if __name__ == "__main__":
    masterAccount  = "beblsoftmanager@gmail.com"
    masterPassword = getpass.getpass("Enter {} Password:".format(masterAccount))
    accountList    = ["test"]
    bLastpass      = BeblsoftLastpass(
        masterAccount  = masterAccount,
        masterPassword = masterPassword,
        accounts       = accountList)
    bLastpass.login()
    bLastpass.loadAllPasswords()
    print(bLastpass)
