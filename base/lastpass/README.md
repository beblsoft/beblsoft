# Lastpass Technology Documentation

Lastpass is a digital password manager. It allows a user to store passwords and other
secret information that is accessible through a single password.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/LastPass),
[Home](https://www.lastpass.com/)


