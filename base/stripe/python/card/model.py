#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Stripe Card Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.common.model import StripeCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STRIPE CARD MODEL --------------------------------- #
class StripeCardModel(StripeCommonModel):
    """
    Stripe Card Model

    Reference:
      https://stripe.com/docs/api/cards/object
      https://stripe.com/docs/api/cards/retrieve

    obj Format:
      <Card card id=card_1DbDR9ECEJ911IPjLYhle4UK at 0x00000a> JSON: {
        "id": "card_1DbDR9ECEJ911IPjLYhle4UK",
        "object": "card",
        "address_city": null,
        "address_country": null,
        "address_line1": null,
        "address_line1_check": null,
        "address_line2": null,
        "address_state": null,
        "address_zip": null,
        "address_zip_check": null,
        "brand": "Visa",
        "country": "US",
        "customer": "cus_E3K5MmQETtyY9P",
        "cvc_check": null,
        "dynamic_last4": null,
        "exp_month": 8,
        "exp_year": 2019,
        "fingerprint": "izQce5vxiBzSu7rS",
        "funding": "credit",
        "last4": "4242",
        "metadata": {
        },
        "name": null,
        "tokenization_method": null
      }
    """

    def __str__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def customerID(self):  # pylint: disable=C0111
        return self.getValueFromObj("customer")

    @property
    def brand(self):  # pylint: disable=C0111
        return self.getValueFromObj("brand")

    @property
    def last4(self):  # pylint: disable=C0111
        return self.getValueFromObj("last4")

    # ------------------- VERIFICATION -------------------------------------- #
    def verifyCustomer(self, stripeCustomerID):
        """
        Verify customer ID
        """
        if stripeCustomerID != self.customerID:
            raise BeblsoftError(BeblsoftErrorCode.STRIPE_CUSTOMER_DOES_NOT_OWN_CARD)
