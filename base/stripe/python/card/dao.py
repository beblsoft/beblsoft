#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Stripe Card DAO Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.customer.dao import StripeCustomerDAO
from base.stripe.python.card.model import StripeCardModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- STRIPE CARD DAO ----------------------------- #
class StripeCardDAO():
    """
    Stripe Card Data Access Object
    """

    # ----------------------------- CREATE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def create(bStripe, stripeCustomerID=None, source=None, metadata=None):  # pylint: disable=W0221
        """
        Create object

        Args
          stripeCustomerID:
            Stripe Customer ID
          source:
            Source token
            Ex. "tok_visa"
          metadata:
            metadata associated with object
            Ex. { "aID": 1 }

        Reference:
          https://stripe.com/docs/api/customers/create?lang=python

        Returns:
          StripeCardModel
        """
        stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe,
                                                        stripeCustomerID=stripeCustomerID)
        obj                 = stripeCustomerModel.obj.sources.create(source=source,
                                                                     metadata=metadata)
        return StripeCardModel(obj=obj)

    # ----------------------------- DELETE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def delete(bStripe, stripeCustomerID, stripeCardID):
        """
        Delete object

        Reference:
          https://stripe.com/docs/api/cards/delete
        """
        stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe,
                                                        stripeCustomerID=stripeCustomerID)
        obj                 = stripeCustomerModel.obj.sources.retrieve(stripeCardID)
        obj.delete()

    # ----------------------------- UPDATE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def update(bStripe, stripeCustomerID, stripeCardID, **kwargs):
        """
        Update object

        Reference:
          https://stripe.com/docs/api/cards/update

        Returns:
          StripeCardModel
        """
        stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe,
                                                        stripeCustomerID=stripeCustomerID)
        obj                 = stripeCustomerModel.obj.sources.retrieve(stripeCardID)
        for key, value in kwargs.items():
            obj[key]        = value
        obj.save()
        return StripeCardModel(obj=obj)

    # ----------------------------- GET BY ID ------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getByID(bStripe, stripeCustomerID, stripeCardID):
        """
        Get object

        Reference:
          https://stripe.com/docs/api/cards/object
          https://stripe.com/docs/api/cards/retrieve

        Returns:
          StripeCardModel
        """
        stripeCardModel = None
        try:
            stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe,
                                                            stripeCustomerID=stripeCustomerID)
            obj                 = stripeCustomerModel.obj.sources.retrieve(stripeCardID)
            stripeCardModel     = StripeCardModel(obj=obj)
        except TypeError as e:
            if "quote_from_bytes() expected bytes" in str(e):
                pass
            else:
                raise e
        except bStripe.stripe.error.InvalidRequestError as e:
            if "No such customer" in str(e):
                pass
            else:
                raise e
        return stripeCardModel

    # ----------------------------- GET ALL --------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getAll(bStripe, stripeCustomerID):
        """
        Get all objects

        Reference:
          https://stripe.com/docs/api/cards/list

        Returns:
          StripeCardModel List
        """
        stripeCardModelList = []
        stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe,
                                                        stripeCustomerID=stripeCustomerID)
        sources = getattr(stripeCustomerModel.obj, "sources", None)  # For VCR
        if sources:
            for obj in sources.auto_paging_iter():
                stripeCardModel = StripeCardModel(obj=obj)
                stripeCardModelList.append(stripeCardModel)

        return stripeCardModelList
