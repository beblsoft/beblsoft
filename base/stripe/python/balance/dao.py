#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Stripe Balance DAO Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.balance.model import StripeBalanceModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- STRIPE BALANCE DAO -------------------------- #
class StripeBalanceDAO():
    """
    Stripe Balance Data Access Object
    """

    # ----------------------------- GET ------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def get(bStripe):
        """
        Get object

        Returns:
          StripeBalanceModel
        """
        obj = bStripe.stripe.Balance.retrieve()
        return StripeBalanceModel(obj=obj)
