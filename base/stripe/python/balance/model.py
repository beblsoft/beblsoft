#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Stripe Balance Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.stripe.python.common.model import StripeCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STRIPE BALANCE MODEL ------------------------------ #
class StripeBalanceModel(StripeCommonModel):
    """
    Stripe Balance Model

    Reference:
      https://stripe.com/docs/api/balance/balance_object?lang=python
      https://stripe.com/docs/api/balance/balance_retrieve?lang=python

    obj Format:
      <Balance balance at 0x00000a> JSON: {
            "object": "balance",
            "available": [
              {
                "currency": "usd",
                "amount": 0,
                "source_types": {
                  "card": 0
                }
              }
            ],
            "livemode": false,
            "pending": [
              {
                "currency": "usd",
                "amount": 0,
                "source_types": {
                  "card": 0
                }
              }
            ]
          }
    """

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def available(self):  # pylint: disable=C0111
        return self.getValueFromObj("available")

    @property
    def pending(self):  # pylint: disable=C0111
        return self.getValueFromObj("pending")
