#!/usr/bin/env python3
"""
NAME:
 bModel.py

DESCRIPTION
 Stripe Common Model
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import logging
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__file__)


# ----------------------- STRIPE COMMON MODEL ------------------------------- #
class StripeCommonModel():
    """
    Stripe Common Model
    """

    def __init__(self, obj):  # pylint: disable=W0622
        """
        Initialize object
        Args
          obj:
            Stripe object metadata
        """
        self.obj = obj

    # ----------------------- METADATA PROPERTIES --------------------------- #
    def getValueFromObj(self, key, required=True, default=None):
        """
        Return value from metadata dictionary
        Nested values are allowed
        Args
          key:
            Key into object
            Ex1. 56
            Ex2. "Foo"
            Ex3. "Foo.bar.baz"
          required:
            If True, raise exception if not found
          default:
            Default value
        """
        value = None
        if isinstance(key, int):
            value = self.__getValueForKeyList(key, self.obj, required, default)
        else:
            value = self.__getValueForKeyList(key.split('.'), self.obj,
                                              required, default)
        return value

    def __getValueForKeyList(self, keyList, d, required, default):
        """
        Return value given a list of keys
        """
        value = None
        if len(keyList) == 1:
            value = self.__getValueForKey(keyList[0], d, required, default)
        else:
            value = self.__getValueForKeyList(keyList[1:],
                                              self.__getValueForKey(
                                                  keyList[0], d, required, default),
                                              required,
                                              default)
        return value

    def __getValueForKey(self, key, d, required, default):  # pylint: disable=R0201
        """
        Return object value for key
        """
        value = None
        try:
            value = d[key]
        except Exception as e:  # pylint: disable=W0703
            if required:
                raise BeblsoftError(
                    code=BeblsoftErrorCode.STRIPE_NO_ATTRIBUTE, originalError=e)
            else:
                value = default
        return value

    # ----------------------- DESCRIBE -------------------------------------- #
    def describe(self):
        """
        Describe object
        """
        logger.info("{} Info:\n{}".format(self, pprint.pformat(self.obj)))
