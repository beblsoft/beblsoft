#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Stripe Charge Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from base.stripe.python.common.model import StripeCommonModel
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STRIPE CHARGE MODEL ------------------------------- #
class StripeChargeModel(StripeCommonModel):
    """
    Stripe Charge Model

    Reference:
      https://stripe.com/docs/api/charges/object
      https://stripe.com/docs/api/charges/retrieve?lang=python

    obj Format:
      <Charge charge id=ch_1Db9dpECEJ911IPj8vKWXCMv at 0x00000a> JSON: {
        "id": "ch_1Db9dpECEJ911IPj8vKWXCMv",
        "object": "charge",
        "amount": 100,
        "amount_refunded": 0,
        "application": null,
        "application_fee": null,
        "balance_transaction": "txn_1Db9dp2eZvKYlo2C60ZoPz3C",
        "captured": false,
        "created": 1543336949,
        "currency": "usd",
        "customer": null,
        "description": "My First Test Charge (created for API docs)",
        "destination": null,
        "dispute": null,
        "failure_code": null,
        "failure_message": null,
        "fraud_details": {
        },
        "invoice": null,
        "livemode": false,
        "metadata": {
        },
        "on_behalf_of": null,
        "order": null,
        "outcome": null,
        "paid": true,
        "payment_intent": null,
        "receipt_email": null,
        "receipt_number": null,
        "refunded": false,
        "refunds": {
          "object": "list",
          "data": [
          ],
          "has_more": false,
          "total_count": 0,
          "url": "/v1/charges/ch_1Db9dpECEJ911IPj8vKWXCMv/refunds"
        },
        "review": null,
        "shipping": null,
        "source": {
          "id": "card_1Db4y8ECEJ911IPjylw9888l",
          "object": "card",
          "address_city": null,
          "address_country": null,
          "address_line1": "",
          "address_line1_check": null,
          "address_line2": null,
          "address_state": null,
          "address_zip": "42424",
          "address_zip_check": "unchecked",
          "brand": "Visa",
          "country": "US",
          "customer": null,
          "cvc_check": "unchecked",
          "dynamic_last4": null,
          "exp_month": 4,
          "exp_year": 2024,
          "fingerprint": "izQce5vxiBzSu7rS",
          "funding": "credit",
          "last4": "4242",
          "metadata": {
          },
          "name": null,
          "tokenization_method": null
        },
        "source_transfer": null,
        "statement_descriptor": null,
        "status": "succeeded",
        "transfer_group": null
      }
    """

    def __str__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def customerID(self):  # pylint: disable=C0111
        return self.getValueFromObj("customer")

    @property
    def captured(self):  # pylint: disable=C0111
        return self.getValueFromObj("captured")

    @property
    def amount(self):  # pylint: disable=C0111
        return self.getValueFromObj("amount")

    @property
    def bCurrencyCode(self):  # pylint: disable=C0111
        return BeblsoftCurrencyCode.fromStripeCode(self.getValueFromObj("currency"))

    @property
    def description(self):  # pylint: disable=C0111
        return self.getValueFromObj("description")

    @property
    def createdDate(self):  # pylint: disable=C0111
        return datetime.fromtimestamp(self.getValueFromObj("created"))

    @property
    def sourceID(self):  # pylint: disable=C0111
        return self.getValueFromObj("source.id")
