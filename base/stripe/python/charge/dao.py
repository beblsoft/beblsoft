#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Stripe Charge DAO Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.charge.model import StripeChargeModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- STRIPE CHARGE DAO --------------------------- #
class StripeChargeDAO():
    """
    Stripe Charge Data Access Object
    """

    # ------------------------- CREATE -------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def create(bStripe,
               amount,
               currency,
               stripeCustomerID    = None,
               sourceID            = None,
               capture             = True,
               statementDescriptor = None,
               description         = None,
               metadata            = None):
        """
        Create object

        Args
          amount:
            A positive integer representing how much to charge, in the smallest
            currency unit
            Ex. 100 cents to charge $1.00, or 100 to charge ¥100, a zero-decimal currency).
            Minimum amount is $0.50 USD
          currency:
            Three-letter ISO currency code, in lowercase. Must be a supported currency.
            Ex. "usd"
          stripeCustomerID:
            Stripe Customer ID
          sourceID:
            Stripe Source ID
          statementDescriptor:
            An arbitrary string to be displayed on your customer’s credit card statement.
            This can be up to 22 characters.
            Ex. "Facebook Analysis"
          description:
            An arbitrary string which you can attach to a Charge object.
            Ex. "First Charge!"
          metadata:
            Dictionary of custom metadata to associate with object
            Ex. { "foo": "bar" }
          capture:
            If True, capture the charge
            If False, authorize the charge to capture later

        Reference:
          https://stripe.com/docs/api/charges/create?lang=python

        Returns:
          StripeChargeModel
        """
        kwargs                             = {}
        kwargs["currency"]                 = currency
        kwargs["amount"]                   = amount
        if stripeCustomerID:
            kwargs["customer"]             = stripeCustomerID
        if sourceID:
            kwargs["source"]               = sourceID
        kwargs["capture"]                  = capture
        if statementDescriptor:
            kwargs["statement_descriptor"] = statementDescriptor
        if metadata:
            kwargs["metadata"]             = metadata
        if description:
            kwargs["description"]          = description
        obj = bStripe.stripe.Charge.create(**kwargs)
        return StripeChargeModel(obj = obj)

    # ------------------------- CAPTURE ------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def capture(bStripe, stripeChargeID):  # pylint: disable=W0221
        """
        Capture object

        Reference:
          https://stripe.com/docs/api/charges/capture?lang=python

        Returns:
          StripeChargeModel
        """
        obj = bStripe.stripe.Charge.retrieve(stripeChargeID)
        obj.capture()
        return StripeChargeModel(obj = obj)

    # ------------------------- UPDATE -------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def update(bStripe, stripeChargeID, **kwargs):
        """
        Update object

        Reference:
          https://stripe.com/docs/api/charges/update?lang=python

        Returns:
          StripeChargeModel
        """
        obj = bStripe.stripe.Charge.retrieve(stripeChargeID)
        for key, value in kwargs.items():
            obj[key] = value
        obj.save()
        return StripeChargeModel(obj = obj)

    # ------------------------- GET ----------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getByID(bStripe, stripeChargeID):
        """
        Get by id

        Reference:
          https://stripe.com/docs/api/charges/object
          https://stripe.com/docs/api/charges/retrieve?lang=python

        Returns:
          StripeChargeModel
        """
        obj = bStripe.stripe.Charge.retrieve(stripeChargeID)
        return StripeChargeModel(obj = obj)

    # ------------------------- GET ALL ------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getAll(bStripe, stripeCustomerID = None):
        """
        Get all objects

        Reference:
          https://stripe.com/docs/api/charges/list?lang=python

        Returns:
          StripeChargeModel List
        """
        stripeChargeModelList = []

        kwargs = {}
        if stripeCustomerID:
            kwargs["customer"] = stripeCustomerID
        for obj in bStripe.stripe.Charge.auto_paging_iter(**kwargs):
            stripeChargeModel = StripeChargeModel(obj = obj)
            stripeChargeModelList.append(stripeChargeModel)
        return stripeChargeModelList

    # ------------------------- GET LIST ------------------------------------ #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getList(bStripe, stripeCustomerID=None, startingAfterID=None, limit=None):
        """
        Get all objects

        Reference:
          https://stripe.com/docs/api/charges/list?lang=python

        Returns:
          (StripeChargeModel List, hasMore)
        """
        stripeChargeModelList = []
        hasMore               = False

        kwargs = {}
        if stripeCustomerID:
            kwargs["customer"]       = stripeCustomerID
        if limit:
            kwargs["limit"]          = limit
        if startingAfterID:
            kwargs["starting_after"] = startingAfterID

        iterObj = bStripe.stripe.Charge.list(**kwargs)
        hasMore = iterObj.get("has_more")
        for obj in iterObj.get("data"):
            stripeChargeModel = StripeChargeModel(obj = obj)
            stripeChargeModelList.append(stripeChargeModel)
        return (stripeChargeModelList, hasMore)
