#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Stripe Card Token Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.stripe.python.common.model import StripeCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STRIPE CARD TOKEN MODEL --------------------------- #
class StripeCardTokenModel(StripeCommonModel):
    """
    Stripe Card Token Model

    obj Format:
      <Token token id=tok_1Daq2jECEJ911IPjs97nrvet at 0x00000a> JSON: {
        "id": "tok_1Daq2jECEJ911IPjs97nrvet",
        "object": "token",
        "card": {
          "id": "card_1Daq2jECEJ911IPjZ6Qyrqsz",
          "object": "card",
          "address_city": null,
          "address_country": null,
          "address_line1": null,
          "address_line1_check": null,
          "address_line2": null,
          "address_state": null,
          "address_zip": null,
          "address_zip_check": null,
          "brand": "Visa",
          "country": "US",
          "cvc_check": null,
          "dynamic_last4": null,
          "exp_month": 8,
          "exp_year": 2019,
          "fingerprint": "izQce5vxiBzSu7rS",
          "funding": "credit",
          "last4": "4242",
          "metadata": {
          },
          "name": "Jenny Rosen",
          "tokenization_method": null
        },
        "client_ip": null,
        "created": 1543261613,
        "livemode": false,
        "type": "card",
        "used": false
      }
    """

    def __str__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")
