#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Stripe Card Token DAO Functionality
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.cardToken.model import StripeCardTokenModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- STRIPE CARD TOKEN --------------------------- #
class StripeCardTokenDAO():
    """
    Stripe Card Token Data Access Object

    Note:
      - This class should ONLY be used for demonstration purposes ONLY!
      - Stripe Token should be generated with Checkout, Elements, or Mobile Libraries
    """

    # ----------------------------- CREATE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def create(bStripe, number=None, expMonth=None, expYear=None, cvc=None):
        """
        Create object

        Args
          bStripe:
            Beblsoft Stripe Object
          number:
            Card number
            Ex. '4242424242424242'
          expMonth:
            Expiration Month
            Ex. 12
          expYear:
            Expiration Year
            Ex. 2019
          cvc:
            CVC Code
            Ex. 123

        Reference:
          https://stripe.com/docs/api/tokens/create_card?lang=python

        Returns:
          StripeCardTokenModel
        """
        kwargs = {
            "card": {
                "number": number,
                "exp_month": expMonth,
                "exp_year": expYear,
                "cvc": cvc
            }
        }

        obj = bStripe.stripe.Token.create(**kwargs)
        return StripeCardTokenModel(obj=obj)

    # ----------------------------- GET ------------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getByID(bStripe, stripeCardTokenID):
        """
        Get object metadata

        Reference:
          https://stripe.com/docs/api/tokens/object?lang=python
          https://stripe.com/docs/api/tokens/retrieve?lang=python

        Returns:
          StripeCardTokenModel
        """
        stripeCardTokenModel = None
        try:
            obj = bStripe.stripe.Token.retrieve(stripeCardTokenID)
            stripeCardTokenModel = StripeCardTokenModel(obj=obj)
        except bStripe.stripe.error.InvalidRequestError as e:
            if "No such token" in str(e):
                pass
            else:
                raise e
        return stripeCardTokenModel
