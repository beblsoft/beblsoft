#!/usr/bin/env python3
"""
 NAME
  model.py

 DESCRIPTION
  Stripe Customer Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.stripe.python.common.model import StripeCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- STRIPE CUSTOMER MODEL ----------------------------- #
class StripeCustomerModel(StripeCommonModel):
    """
    Stripe Customer Model

    Reference:
      https://stripe.com/docs/api/customers/object?lang=python

    obj Format:
      <Customer customer id=cus_E3BMHbxkiSGdM0 at 0x00000a> JSON: {
        "id": "cus_E3BMHbxkiSGdM0",
        "object": "customer",
        "account_balance": 0,
        "created": 1543319111,
        "currency": "usd",
        "default_source": null,
        "delinquent": false,
        "description": null,
        "discount": null,
        "email": null,
        "invoice_prefix": "7183B26",
        "livemode": false,
        "metadata": {
        },
        "shipping": null,
        "sources": {
          "object": "list",
          "data": [
          ],
          "has_more": false,
          "total_count": 0,
          "url": "/v1/customers/cus_E3BMHbxkiSGdM0/sources"
        },
        "subscriptions": {
          "object": "list",
          "data": [
          ],
          "has_more": false,
          "total_count": 0,
          "url": "/v1/customers/cus_E3BMHbxkiSGdM0/subscriptions"
        },
        "tax_info": null,
        "tax_info_verification": null
      }
    """

    def __str__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def defaultSourceID(self):  # pylint: disable=C0111
        return self.getValueFromObj("default_source")
