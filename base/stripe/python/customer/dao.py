#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Stripe Customer Data Access Object
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.customer.model import StripeCustomerModel


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- STRIPE CUSTOMER DAO ------------------------- #
class StripeCustomerDAO():
    """
    Stripe Customer Data Access Object
    """

    # ----------------------------- CREATE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def create(bStripe, email=None, metadata=None,
               description=None,):  # pylint: disable=W0221
        """
        Args
          email:
            Customer Email
            Ex. "jenny.rosen@example.com"
          metadata:
            Dictionary of metadata to associate with customer
            Ex. { "foo": "bar" }
          descrption:
            Arbitrary description for customer
            Ex. "Best customer in the world!"

        Reference:
          https://stripe.com/docs/api/customers/create?lang=python

        Returns
          StripeCustomerModel
        """
        kwargs                    = {}
        if email:
            kwargs["email"]       = email
        if metadata:
            kwargs["metadata"]    = metadata
        if description:
            kwargs["description"] = description

        obj = bStripe.stripe.Customer.create(**kwargs)
        return StripeCustomerModel(obj=obj)

    # ----------------------------- DELETE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def delete(bStripe, stripeCustomerID):
        """
        Delete object

        Reference:
          https://stripe.com/docs/api/customers/delete?lang=python
        """
        obj = bStripe.stripe.Customer.retrieve(stripeCustomerID)
        obj.delete()

    # ----------------------------- UPDATE ---------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def update(bStripe, stripeCustomerID, **kwargs):
        """
        Update object

        Reference:
          https://stripe.com/docs/api/customers/update?lang=python

        Returns:
          Object Metadata
        """
        obj = bStripe.stripe.Customer.retrieve(stripeCustomerID)
        for key, value in kwargs.items():
            obj[key] = value
        obj.save()
        return StripeCustomerModel(obj=obj)

    # ----------------------------- SET DEFAULT ----------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler(maxRetries=0)
    def setDefaultCard(bStripe, stripeCustomerID, stripeCardID):
        """
        Set As Customer's Default Source

        Reference:
          https://stripe.com/docs/api/customers/update

        Returns:
          StripeCardModel
        """
        if not isinstance(stripeCardID, str):
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_CARDID_NOT_STRING)
        if not stripeCardID:
            raise BeblsoftError(code=BeblsoftErrorCode.STRIPE_CARDID_TOO_SHORT)

        obj = bStripe.stripe.Customer.retrieve(stripeCustomerID)
        obj.default_source = stripeCardID
        obj.save()
        return StripeCustomerModel(obj=obj)

    # ----------------------------- GET BY ID ------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getByID(bStripe, stripeCustomerID):
        """
        Get by ID

        Reference:
          https://stripe.com/docs/api/customers/object?lang=python
          https://stripe.com/docs/api/customers/retrieve?lang=python

        Returns:
          StripeCustomerModel
        """
        stripeCustomerModel = None
        try:
            obj                 = bStripe.stripe.Customer.retrieve(stripeCustomerID)
            stripeCustomerModel = StripeCustomerModel(obj=obj)
        except bStripe.stripe.error.InvalidRequestError as e:
            if "No such customer" in str(e):
                pass
            else:
                raise e
        return stripeCustomerModel

    # ----------------------------- GET ALL --------------------------------- #
    @staticmethod
    @logFunc()
    @BeblsoftStripe.errorHandler()
    def getAll(bStripe):
        """
        Get all

        Reference:
          https://stripe.com/docs/api/customers/list?lang=python

        Returns:
          StripeCustomerModel
        """
        stripeCustomerModelList = []
        stripe                  = bStripe.stripe

        for obj in stripe.Customer.auto_paging_iter():
            stripeCustomerModel = StripeCustomerModel(obj=obj)
            stripeCustomerModelList.append(stripeCustomerModel)
        return stripeCustomerModelList
