OVERVIEW
===============================================================================
Stripe Technology Documentation
- Description                       : Stripe's allows individuals and businesses to receive payments
                                      over the Internet. Stripe provides the technical, fraud prevention, and banking
                                      infrastructure required to operate on-line payment systems.
- Features --------------------------
  * Payments                        : Commerce toolkit, built for developers
  * Billing                         : Build and scale recurring business model
  * Connect                         : Everything platforms need to get sellers paid
  * Sigma                           : Businesss data at your fingertips
  * Atlas                           : Best way to start an internet business
  * Radar                           : Fight fraud with machine learning
  * Issuing                         : Issue and manage payment cards for your businesses
  * Terminal                        : Stripe's hardware/software solution for accepting in-person payments
- Info URLs -------------------------
  * Home                            : https://stripe.com/
  * Documentation                   : https://stripe.com/docs
  * Wikipedia                       : https://en.wikipedia.org/wiki/Stripe_(company)
  * API Keys                        : https://stripe.com/docs/keys
- API URLs --------------------------
  * Python API Docs                 : https://stripe.com/docs/api/authentication?lang=python
  * Stripe (JS) Elements Examples   : https://github.com/stripe/elements-examples
  * API Updates                     : https://groups.google.com/a/lists.stripe.com/forum/#!forum/api-announce
  * API Change Log                  : https://stripe.com/docs/upgrades#api-changelog
- Dashboard URLs --------------------
  * Home                            : https://dashboard.stripe.com/test/dashboard
  * API Keys                        : https://dashboard.stripe.com/account/apikeys
  * Activate Account                : https://dashboard.stripe.com/account/details
- Misc URLs -------------------------
  * Test Site SSL                   : https://www.ssllabs.com/ssltest
  * Recipes                         : https://stripe.com/docs/recipes



TERMINOLOGY, OBJECTS
===============================================================================
- Profile                           : Each Stripe user (Admin, Developer, Tester, ...) has a Stripe
                                      profile and can login to their own Stripe Dashboard
- Account                           : An entity, associated with a businesses, that can charge customers and
                                      facilitate payments
                                      Each Profile can be connected to zero, one, or more Stripe Accounts
- Balance                           : Amount of money in an Account
- Token                             : Tokenization is the process Stripe uses to collect sensitive card,
                                      bank, or PII directly from customer in a secure manner.
                                      A token representing this information is returned to developer's server
                                      for future use.
                                      Generated with Checkout, Elements, or mobile libraries.
                                      A token can only be charged once, for multiple uses it must be associated
                                      with a customer
- Customer                          : Person or organization that buys goods or services from a store or business.
                                      The Stripe customer object allows developers to implement recurring charges
                                      and tack charges associated with the same buyer.
- Card                              : Stripe object that represents a credit card
                                      Can associate multiple Cards with a Customer
- Source                            : Stripe object that represents a variety of payment instruments (Ex. card)
                                      Once chargeable, they can be charged or attached to customers
- Charge                            : Stripe object used to charge credit or debit cards
- Refund                            : Refund object allows developers to refund a charge that was previously
                                      created but not yet charged
- Dispute                           : A dispute occurs when a customer questions charge with card issuer.
                                      When this happens, you're given the opportunity to respond to the dispute
                                      with evidence that shows that a charge is legitimate



PAYMENTS: QUICKSTART
===============================================================================
- Steps                             : 2 step process with client and server side actions
  1 Client token                    : On client, Stripe securely collects customer's
                                      payment information and returns representative token
                                      This is submitted to server
  2 Server Charge                   : Using token, server-side code makes an API request
                                      to create a charge and complete the payment
- Step 1 Use checkout to get card   : <form action="your-server-side-code" method="POST">
                                        <script
                                          src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                          data-key="pk_test_hkoj9EowEd1aFejN4oy2vzvD"
                                          data-amount="999"
                                          data-name="Smeckn"
                                          data-description="Example charge"
                                          data-zip-code="true"
                                          data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                          data-locale="auto">
                                        </script>
                                      </form>
- Step 2. Server side code charges  : stripe.api_key = "sk_test_SNXLp43e0uGeczaQrMh5vEn2"
                                      token = request.form['stripeToken'] # Using Flask
                                      charge = stripe.Charge.create(
                                          amount=999,
                                          currency='usd',
                                          description='Example charge',
                                          source=token)



PAYMENTS: COLLECTING PAYMENT DETAILS
===============================================================================
- Checkout --------------------------
  * Description                     : Stripes Securely accepts customer's payment details and directly
                                      passes them to Stripe's servers
                                      Stripe returns a token representation of payments, which
                                      can be submitted to server for use
  * Flow                            1 Customer arrives at payment page, includes checkout code
                                    2 Customer fills out form and clicks pay $9.99
                                    3 Checkout sends payment details directly to Stripe from customer's
                                      browser
                                    4 Stripe returns token to Checkout, or error message if
                                      card-network validation fails
                                    5 Checkout takes token and stores it in page's primary form
                                    6 Checkout submits form to server
                                    7 Server uses posted token to charge the card
- Checkout Integrations -------------
  1 Simple                          : Simple integration provides blue Pay with Card Button
  2 Custom                          : Custom integration create custom button and passes
                                      a Stripe token to JavaScript callback
- Stripe.js & Elements --------------
  * Description                     : Foundational JavaScript library for building payment flows
                                      Makes working with sensitive data (cards, bank accounts, personally identifiable, ...) easy
  * Element features                : Automatically format card info as its entered
                                      Translate placeholders into customer's preferred language
                                      Responsive design on mobile
                                      Customizable styling to match checkout flow
  * Creating custom payments        1 Set up Stripe Elements
                                    2 Create your payment form
                                    3 Create token to securely transmit card information
                                    4 Submit token and rest of form to server
- Saving Cards ----------------------
  * Customer object                 : After customer's payment information is collected,
                                      a card token is generated. Card token can be used to create
                                      a Stripe customer object that can be used for subscriptions or later charges
  * Create customer (Python)        : stripe.api_key = "sk_test_AVVBGX9JqS3hFrM8JGDzBAX5"
                                      # Create a Customer:
                                      customer = stripe.Customer.create(
                                        source='tok_mastercard',
                                        email='paying.user@example.com',
                                      )
                                      # Note, save customer ID in database
  * Charge customer (Python)        : # Charge the Customer instead of the card:
                                      charge = stripe.Charge.create(
                                        amount=1000,
                                        currency='usd',
                                        customer=customer.id,
                                      )
  * Automatic Card Updates          : Saved payment method can continue to work even after physical card has been replaced
                                      Stripe works with card networks and automatically attempts to updated saved
                                      card details whenever a customer receives a new card
                                      Use webhooks to be notified before or after a card is automatically updated
  * Changing payment method (Python): stripe.api_key = "sk_test_AVVBGX9JqS3hFrM8JGDzBAX5"
                                      stripe.Customer.modify(
                                       'cus_V9T7vofUbZMqpv',
                                       source='tok_visa',
                                      )
  * Multiple payment methods        : Customers can store multiple payment methods.
                                      The first one saved to a customer is a set as the default_source
                                      Can update the customer's default source at any time



PAYMENTS: CHARGES
===============================================================================
- Creating Charges ------------------
  * Creating charges                : Once customer's credit card has been securely collected and tokenized
                                      can charge the card immediately or save it for later
  * Statement Descriptor            : Dynamic statement descriptors are supported
  * Two step payments
    1 Auth                          : Funds are guaranteed by the card issuer and the amount held
                                      on the customer's card for up to seven days
    2 Capture                       : To settle authorized charge issue capture chare request.
                                      If the charge is not captured within
                                      this time, the authorization is canceled and funds released
                                      The capture charge must be less than the initial amount.
    Refund                          : Authorized funds can be refunded
  * Storing information in metadata : Associate other information - meaningful to you - with Stripe activity
                                      Do not store sensitive information in metadata
  * Emailed receipts                : Stripe can automatically send email receipts to customers
                                      whenver they make a successful payment, or when payment is refunded
- Declined and failed payments ------
  * Payments declined by card issuer: Most declines categorized as "generic"
                                      If all card information correct, best to have customer contact card issuer
                                      outcome of payment explains reason
                                      Ex outcome:
                                      {
                                        network_status: "declined_by_network"
                                        reason: "expired_card"
                                        risk_level: "normal"
                                        seller_message: "The bank returned the decline code `expired_card`."
                                        type: "issuer_declined"
                                      },
                                      More fields collected [i.e. zip, CVC], the lower the risk of
                                      fraudulent activity
                                      Use charge tokens within a few minutes so CVC doesn't expire
  * Blocked payments                : Radar, stripe's automated fraud prevention toolset,
                                      blocks any payments that are identified as being high-risk
                                      Ex outcome:
                                      {
                                        network_status: "not_sent_to_network"
                                        reason: "highest_risk_level"
                                        risk_level: "highest"
                                        seller_message: "Stripe blocked this charge as too risky."
                                        type: "blocked"
                                      },
                                      Use the dashboard to unblock payments known to be valid
  * Invalid API Calls               : Weed these out in development
                                      Ex outcome:
                                      {
                                        network_status: "not_sent_to_network"
                                        type: "invalid"
                                      },
- Disputes and Fraud ----------------
  * Disputes                        : (Aka Chargebacks) Occur when a cardholder questions payment with card issuer
                                      The payment amount, along with a separate $15.00 dispute fee
                                      is deducted from account balance
                                      Dispute resolution process through Stripe dashboard
                                      Card holder can dispute a charge up to 90 days after a payment
  * Receiving a dispute             : Automated email
                                      Optional webhook
                                      Actions: respond and submit evidence, or accept
  * Dispute fees                    : $15.00 dispute fee charged by card network
                                      Stripe reimburses dispute fee if found in your favor
                                      Stripe is not refunded by card network
  * Responding to disputes          1 Email customer within stripe, possible they didn't recognize
                                      charge on their statement
                                    2 Submit evidence (7 - 21 day window)
                                      Can only be submitted once. Make sure to provide all relevant
                                      information and review it carefully before submit
  * Disputs: what to submit         : Appropriate to reason for disput
                                      Web logs, email communications, shipment tracking numbers,
                                      delivery confirmation, proof of prior refunds or replacements, etc.
                                    : Keep evidence relevant and to the point
                                    : Provide clear and accurate evidence
                                    : Include proof of customer authorization
                                    : Include proof of service or delivery
                                    : Include a copy of your terms of service and refund policy
  * Responding to disputes          : (python)
                                      dispute = stripe.Dispute.retrieve('dp_H7KnXYqtSRuB7FXA3W9u')
                                      stripe.Dispute.modify('dp_H7KnXYqtSRuB7FXA3W9u', ...)
  * Withdrawing dispute             : Can take 60-75 days for dispute to be closed and refunds returned
                                      Email stripe with evidence of re-billing
  * Dispute metrics
    Dispute activity                : Percentage of disputes on successful payments by dispute date
                                      Monitored by dispute and card fraud monitoring programs
                                      Can levy fines if this value is too hight
                                      Dispute activity > .75% is considered excessive
    Dispute rate                    : Percentage of disputes on successful payments by charge date
    Benchmarking                    : Optimize fraud prevention strategy to maximize revenue while minimizing
                                      disputes
  * Inquiries and retrievals        : Some card issuers (American Express, Discover) may begin to
                                      investigate before creating a formal dispute
                                      Shown in dashboard
                                      Resolve here before a formal dispute is launched and funds are reclaimed
  * Prevention
    Collect as much info as possible: Customer name, email, CVC, full billing address, shippin address
    Verification checks             : Verification API response
                                      Card Verification Code Chec (CVC)
                                      Address verification (AVS)
    Effective Customer Communication: Clear description of refund and cancellation policies
                                      Make terms of service and policies easy to find on website
                                      Require customers to agree to them
                                      Provide online tracking information
                                      Use a recognizable name for statement descriptor
                                      Each stripe account should represent a separate business
- Common types fraud ----------------
  1 Stolen cards
  2 Overpayments                    : Beaware of payments to thirdparty
  3 Alternative refunds             : Only refund via the card
  4 Marketplace                     : When connecting buyers and sellers, beaware of sellers taking
                                      payment without providing service
  5 Card testing                    : Don't allow cards to be tested on site. Use CAPTCHA or rate limiting
  6 Friendly                        : Customer honestly wants a refund. Combat with clearly displayed
                                      return policies at checkout that with which the customer is required
                                      to agree
- Identifying fraud -----------------
  * General                         : Use of likely false information (asdkf12495@freemail.example.com)
                                      Inconsistent details across multiple purchases
                                      Scripted communication
                                      Unusually large orders
                                      Many Payments (same card but different shipping, many cards same shipping, ...)
                                      Split large order into multiple payments
                                      Process payment manually
                                      Overcharge request
                                      Refund outside card network
  * Physical Goods                  : Shipping and billing addresses match
                                      Changing shipping address after order palce
                                      Rush orders or requests
                                      Credit card country of origin
                                      High shipping costs
                                      Shipping to a freight forwarder
  * Digital goods or services       : Multiple accounts using similar email addresses
                                      Multiple charges to same email address
                                      Unexptected or significant changes in account activity
                                      View payment, IP address, email logs, usage logs
  * Donations or crowdfunding       : Make sure donation makes sense for campaign
                                      Be wary of large donations and refund requests
                                      Monitor declined payments

- Fraud prevention best practices ---
  * Use Radar for Fraud Teams
  * Refund fraud ASAP               : Refund as fraud action. This tells stripe it was a fraudulent charge
                                      and refunds before dispute is filed
  * Contact customer to confirm
    order
  * Radar rules                     : Use radar rules to intercept and process fraudulent payments
  * Manually review payments        : Radar can put payment into review
  * Delay shipping by 24-48 hours   : Allows real cardholder to review statement
  * Ship to verified addresses
  * Use auth and capture
  * Set custom statement descriptor : Then ask customer to verify statement id over email/phone
  * Verify customer identity
- Refunds ---------------------------
  * Description                     : Stripe supports the ability to refund charges made to account
                                      Refund requests made to customer's bank made immediately
                                      Refund appears as credit 5-10 business days later
                                      Once issued, refund cannot be canceled
                                      Email optionally sent to customer
  * Issuing Full Refund             : API Ex.
                                      stripe.api_key = "sk_test_SNXLp43e0uGeczaQrMh5vEn2"
                                      refund = stripe.Refund.create(
                                          charge='ch_1JXRTTkBaEEgH5vQW562',
                                      )
  * Issuing Partial Refund          : API Ex.
                                      stripe.api_key = "sk_test_SNXLp43e0uGeczaQrMh5vEn2"
                                      refund = stripe.Refund.create(
                                          charge='ch_gzMzj7NR6p1ugZy3qDWl',
                                          amount=1000,
                                      )
  * Destination                     : Always refund through Stripe, or else fraudulent actions can occur
  * Failed refunds                  : Can occur if customer's bank or card issuer has been unable
                                      to process it correctly (e.g. a closed bank account)
- Receiving Payouts -----------------
  * Description                     : To receive funds for payments you've processed, Stripe makes
                                      deposits (payouts) from your available account balance into
                                      your bank account
                                      Payout schedule depends on industry, country, and risk profile
  * First payment                   : Takes 7-10 days to process
  * Add bank account information    : Add Routing Number, Account Number
                                      Bank accounts must be located in country where settlement currency
                                      is based
  * Multiple banks to avoid
    currency conversion             : Can use different bank accounts for different settlement currencies
                                      Stripe will not do currency conversion
  * Payout schedule
    Initially received              : Reflected as pending balance(less any Stripe fees)
                                      Becomes available for payout
    2-day rolling                   : Australia, USA
                                      Payouts contain payments processed two business days prior
    4-day rolling                   : New Zealand
    7-day rolling                   : All countries
    30-day rolling                  : Brazil
  * Manual payouts                  : API Ex.
                                      stripe.api_key = "sk_test_SNXLp43e0uGeczaQrMh5vEn2"
                                      payout = stripe.Payout.create(
                                        amount=5000,
                                        currency='usd',
                                      )
  * Negative Payouts                : In some cases, you may have a negative account balance due to disputes
                                      Bank Account added must support credit and debit transactions
  * Failed payouts                  : If payout fails for any reason, reenter bank credentials
                                      and or contact bank
- Supported Currencies --------------
  * Currencies                      : Stripe supports processing payments in 135+ currencies
                                      Allows customers to be charged in native currency while receiving
                                      funds in yours
  * Three places                    1 Customer's credit card currency
                                    2 Currency of charge, presentment currency
                                    3 Currency accepted by destination bank, settlement currency
  * Zero-decimal currencies         : All API requests expect amounts to be provided in currency's smallest unit
                                      Ex $10 USD, provide 1000 (1000 cents)
  * Currency conversion             : Stripe uses rates slightly above the mid market rate




PAYMENTS: SOURCES
===============================================================================
- Payment Methods -------------------
  * Source Objects                  : Allow developer to accept a variety of payment methods with a single API
                                      Source represents a customer's payment instrument
                                      Sources can be charged directly or attached to customers for later reuse
  * 4 source characteristics
    1 Pull or push                  : How funds for method are transferred from customer
                                      Pull: Customer account is debited without customer interaction (Ex. Cards)
                                      Push: Customer pushes funds to your account (Ex. ACH Credit Transfers)
    2 Flow                          : Type of action customer must take to authenticate
    3 Usage                         : Whether source is reusable
    4 Sync or async                 : Whether resulting charge can be confirmed immediately or after delay
                                      Stripe will generate a webhook event when async charges have been completed
  * Reuse                           : Sources that are reusable have usage: reusable
                                      Sources that are not reusable have usage: single_use before use and usage: consumed after use
  * Methods                         : Cards, 3D Secure, SEPA Direct Debit, ACH Debits with Auth,
                                      ACH Debits with microdeposits, Alipay, Bancontact, EPS, Giropay, IDEAL,
                                      Przelewy24, Multibanco, Sofort, ACH Credit Transfer, ACH, Amex Express Checkout,
                                      Masterpass, Visa Checkout, Apple Pay, Google Pay, Microsoft Pay
- Customers -------------------------
  * Attach source to customer
    (python)                        : stripe.api_key = "sk_test_AVVBGX9JqS3hFrM8JGDzBAX5"
                                      customer = stripe.Customer.retrieve("cus_AFGbOSiITuJVDs")
                                      customer.sources.create(source="src_18eYalAHEMiOZZp1l9ZTjSU0")
  * Update customer default source
    (python)                        : stripe.api_key = "sk_test_AVVBGX9JqS3hFrM8JGDzBAX5"
                                      customer = stripe.Customer.retrieve("cus_AFGbOSiITuJVDs")
                                      customer.default_source = "src_18eYalAHEMiOZZp1l9ZTjSU0"
                                      customer.save()
  * Charging a customer default
    source (python)                 : charge = stripe.Charge.create(
                                        amount=1099,
                                        currency='eur',
                                        customer='cus_AFGbOSiITuJVDs')
  * Detaching a source              : stripe.api_key = "sk_test_AVVBGX9JqS3hFrM8JGDzBAX5"
                                      customer = stripe.Customer.retrieve("cus_E3BPwWXGy2IPHL")
                                      customer.sources.retrieve("src_1Db52kECEJ911IPj6hVK8MO9").detach()



PAYMENTS: TESTING
===============================================================================
- Test Card Numbers -----------------
  * 4242424242424242                : Visa
  * 4000056655665556                : Visa (debit)
  * 5555555555554444                : Mastercard
  * 2223003122003222                : Mastercard (2-series)
  * 5200828282828210                : Mastercard (debit)
  * 5105105105105100                : Mastercard (prepaid)
  * 378282246310005                 : American Express
  * 371449635398431                 : American Express
  * 6011111111111117                : Discover
  * 6011000990139424                : Discover
  * 30569309025904                  : Diners Club
  * 38520000023237                  : Diners Club
  * 3566002020360505                : JCB
  * 6200000000000005                : UnionPay
- Test Tokens -----------------------
  * tok_visa                        : Visa
  * tok_visa_debit                  : Visa (debit)
  * tok_mastercard                  : Mastercard
  * tok_mastercard_debit            : Mastercard (debit)
  * tok_mastercard_prepaid          : Mastercard (prepaid)
  * tok_amex                        : American Express
  * tok_discover                    : Discover
  * tok_diners                      : Diners Club
  * tok_jcb                         : JCB
  * tok_unionpay                    : UnionPay
- Card numbers for specific responses
  * 4000000000000077                : Charge succeeds and funds will be added directly to your available balance (bypassing your pending balance).
  * 4000000000000093                : Charge succeeds and domestic pricing is used (other test cards use international pricing). This card is only significant in countries with split pricing.
  * 4000000000000010                : The address_line1_check and address_zip_check verifications fail. If your account is blocking payments that fail ZIP code validation, the charge is declined.
  * 4000000000000028                : Charge succeeds but the address_line1_check verification fails.
  * 4000000000000036                : The address_zip_check verification fails. If your account is blocking payments that fail ZIP code validation, the charge is declined.
  * 4000000000000044                : Charge succeeds but the address_zip_check and address_line1_check verifications are both unavailable.
  * 4000000000005126                : Charge succeeds but refunding a captured charge fails with a failure_reason of expired_or_canceled_card.
  * 4000000000000101                : If a CVC number is provided, the cvc_check fails. If your account is blocking payments that fail CVC code validation, the charge is declined.
  * 4000000000000341                : Attaching this card to a Customer object succeeds, but attempts to charge the customer fail.
  * 4000000000009235                : Results in a charge with a risk_level of elevated.
  * 4000000000004954                : Results in a charge with a risk_level of highest.
  * 4100000000000019                : Results in a charge with a risk_level of highest. The charge is blocked as it's considered fraudulent.
  * 4000000000000002                : Charge is declined with a card_declined code.
  * 4000000000009995                : Charge is declined with a card_declined code. The decline_code attribute is insufficient_funds.
  * 4000000000009987                : Charge is declined with a card_declined code. The decline_code attribute is lost_card.
  * 4000000000009979                : Charge is declined with a card_declined code. The decline_code attribute is stolen_card.
  * 4000000000000069                : Charge is declined with an expired_card code.
  * 4000000000000127                : Charge is declined with an incorrect_cvc code.
  * 4000000000000119                : Charge is declined with a processing_error code.
  * 4242424242424241                : Charge is declined with an incorrect_number code as the card number fails the Luhn check.
- Tokens for specific responses -----
  * tok_bypassPending               : Charge succeeds and funds will be added directly to your available balance (bypassing your pending balance).
  * tok_domesticPricing             : Charge succeeds and domestic pricing is used (other test cards use international pricing). This card is only significant in countries with split pricing.
  * tok_avsFail                     : The address_line1_check and address_zip_check verifications fail. If your account is blocking payments that fail ZIP code validation, the charge is declined.
  * tok_avsLine1Fail                : Charge succeeds but the address_line1_check verification fails.
  * tok_avsZipFail                  : The address_zip_check verification fails. If your account is blocking payments that fail ZIP code validation, the charge is declined.
  * tok_avsUnchecked                : Charge succeeds but the address_zip_check and address_line1_check verifications are both unavailable.
  * tok_refundFail                  : Charge succeeds but refunding a captured charge fails with a failure_reason of expired_or_canceled_card.
  * tok_cvcCheckFail                : If a CVC number is provided, the cvc_check fails. If your account is blocking payments that fail CVC code validation, the charge is declined.
  * tok_chargeCustomerFail          : Attaching this card to a Customer object succeeds, but attempts to charge the customer fail.
  * tok_riskLevelElevated           : Results in a charge with a risk_level of elevated.
  * tok_riskLevelHighest            : Results in a charge with a risk_level of highest.
  * tok_chargeDeclined              : Charge is declined with a card_declined code.
  * tok_chargeDeclined
    InsufficientFunds               : Charge is declined with a card_declined code. The decline_code attribute is insufficient_funds.
  * tok_chargeDeclinedFraudulent    : Results in a charge with a risk level of highest. The charge is blocked as it's considered fraudulent.
  * tok_chargeDeclinedIncorrectCvc  : Charge is declined with an incorrect_cvc code.
  * tok_chargeDeclinedExpiredCard   : Charge is declined with an expired_card code.
  * tok_chargeDeclined
    ProcessingError                 : Charge is declined with a processing_error code.



SIGMA
===============================================================================
- Overview --------------------------
  * Description                     : Stripe Sigma makes all your transactional data available as an
                                      interactive SQL environment in the Dashboard.
                                      Query results are displayed directly in the browser and can be downloaded
                                      in CSV format for use in reporting tools or spreadsheet applications
  * Payment                         : Stripe Sigma is a feature available at an additional cost
  * Writing Queries                 : ANSI SQL used
                                      Any team members on your account with permission to view reports can write queries
                                      Ex.
                                      select
                                        date_format(created, '%Y-%m-%d') as day,
                                        id,
                                        amount,
                                        currency,
                                        source_id
                                      from balance_transactions
                                      where type = 'refund'
                                      order by day desc
                                      limit 5
  * Query History                   : View all performed queries within the History tab
  * Schema                          : Data available is shown in schema tab
  * Data freshness                  : Stripe sigma does not immediately reflect account's most recent
                                      transactions as additional processing time - up to three days -
                                      is required to make the data available to query



RADAR
===============================================================================
- Overview --------------------------
  * Description                     : Stripe Radar is a suite of modern tools to help you fight fraud
  * Machine Learning                : At the center of Stripe Radar is a machine learning system that
                                      provides real-time transaction scoring based upon thousands of signals
                                      about each incoming card payment
  * Tiers
    Stripe Radar                    : Free
    Stripe Radar for Fraud Teams    : Costs extra money
  * Features
    ML based risk evaluations       : On all card payments
    Default Rules                   : For CVC and ZIP code verification and dynamic authentication
    Insights                      FT: On latest state of fraud prevention
    Rules                         FT: To automate custome business logic
    Review                        FT: Workflows for suspicious payments
    Lists                         FT: To automatically allow or block trusted or blacklisted customers
    Risk Score                    FT: (0-100) that gives you granular insight into risk level of card
  * Requirements                    : Must use Checkout, Stripe.js and Elements, or Mobile SDK
- Risk Evaluation -------------------
  * Card Stats                      : When business using Stripe sees a card for the first time,
                                      there's an 80% chance that card has been seen on network in the past
  * Risk outcomes                   : Shows how likely a payment might be fraudulent
                                      Each payment includes information on the outcome of the risk evaluation
                                      If payment is declined, extra information is included
    High Risk                       : Payments likely to be fraudulent, blocked by default
                                      Can mark card as safe in the dashboard
    Elevated Risk                   : Payments with increased chance of being fraudulent. Allowed be default
    Normal Risk                     : Payments with fewer characteristics of fraud
  * Search                          : Can search all payments based on risk
  * Feedback on risk                : Refund or report payments that you believe are fraudulent
- Reviews ---------------------------
  * Description                     : Create rules to review payments that meet a specified criteria
  * Review Queue                    : Prioritized list of payments that need further investigation
    List View                       : Perform reviews at-a-glance
    Detailed View                   : Gives more context about payment
  * Actions                         : Remove payment from review queue by one of the following actions
    Approve                         : Closes review with no changes made to payment
    Refund                          : Refund payment without reporting it to Stripe as fraudulent
    Refund and report fraud         : Refunds payment and reports it to Stripe as fraud
  * Best Practices                  1 Focus time on payments where human judgement can add insight
                                    2 Leverage insights from reviewers to develop hypotheses for fraud prevention
                                    3 Customize process by presenting data unique to business in payment metadata
                                    4 Don't slow down your customer journey
- Lists -----------------------------
  * Description                     : Allow developers to block, allow, or place in review matching payments
  * Default lists                   : Default lists by Stripe to get you started
    Card BIN                        : Bank Identification Number (BIN), first six digits of the card number
    Card country                    : Two-letter code coressponding to where card was issued (ex. US)
    Card fingerprint                : Stripe's fingerprint for the card Ex. XypJT2D1ve8ds1
    Card description                : Description supplied with the payment
    Client IP country               : Two-letter code corresponding to country-level geolcation of IP Address
    Client IP address               : IP address from which payment originates (Ex. 13.112.224.240)
    Email                           : First email derived from the charge, card, or customer objects (Ex. jenny.rosen@example.com)
    Email Domain                    : First email domain derived from the Chare, Card, or Customer objects (Ex. example.com)
  * Custom Lists                    : Create custom lists with specific type of information
- Rules -----------------------------
  * Description                     : Allow for developers to optimize fraud prevention by taking actions:
                                      Request 3D Secure, Allow, Block, Review payments
  * Built in rules                  : Block if Stripe evaluates the payment as high risk
                                      Review if Stripe evaluates the payment as elevated risk
  * Request 3D Secure Rules         : Dynamically prompt customer to perform 3D Secure authentication
                                      Don't send too many payents through 3D Secure
  * Review Rules                    : Don't set too broad a rule, or review queue will bload
                                      Test rules over past 6 months of data to ensure
                                      1 Overall volume of matching payments is low
                                      2 Human reviewers add value
                                      3 Rule results in mix of successful and refunded or disputed payments
  * Block Rules                     : Block payment from occuring. Ensure
                                      1 False positives are as low as possible
                                      2 Minimize unnecessary rules
  * Allow Rules                     : Override all other rules, including Stripe's machine learning models
                                      Should be used with the most caution
  * Monitor and Maintain Rules      : Regularly monitor rules to ensure they are still effective
                                      Regularly monitor your manual review queue
                                      Analyze your disputed and refunded payments
                                      Anticipate large changes to your business that might impact fraud rate



DASHBOARD
===============================================================================
- Overview --------------------------
  * Description                     : Stripe Dashboard is a feature-rich user interface for you to
                                      operate and configure your Stripe account
  * Settings                        : Found within each section. (e.g. Subscription settings,
                                      Stripe Radar rules). Additional settings in Business Settings
  * Reports                         : All transactional data can be filtered and exported as reports in CSV
                                      format. Reports can be exported to QuickBooks
- Search ----------------------------
  * Search the following            : Connected accounts, coupons, customers, invoices and invoice items, orders, payments,
                                      payouts, plans, products, sources, transfers
  * Search Filters
    amount                          : The amount of an object. For decimal currencies, use a decimal point for both currency units (e.g., dollars and cents).
                                      amount:149.99
    brand                           : The brand of card associated with an object.
                                      brand:visa
    country                         : The two-letter ISO code representing the country associated with an object.
                                      country:GB
    created                         : The date an object was created (identical to date).
                                      created:2018/10/29
    currency                        : The three-letter ISO code representing the currency of an object.
                                      currency:EUR
    date                            : The date an object was created (identical to created).
                                      date:yesterday
    email                           : The email (either full address or part of one) of an object.
                                      email:jenny.rosen@example.com
    exp                             : The expiration date of the card associated with an object.
                                      exp:11/20
    flow                            : The type of flow for customer action that applies to a Sources payment.
                                      flow:redirect
    last4                           : The last four digits of the card associated with an object.
                                      last4:4080
    metadata                        : Metadata value on a supported object. Additional search options for metadata are also available.
                                      metadata:555-5555
    name                            : The cardholder or customer name associated with an object.
                                      name:jenny
    number                          : The unique number identifying an invoice.
                                      number:06b2b1a642-0023
    postal                          : The ZIP code associated with an object.
                                      postal:12345
    receipt                         : The receipt number used in a payment or refund email receipt.
                                      receipt:3330-2392
    risk_level                      : The risk level of a payment determined by Radar.
                                      risk_level:elevated
    status                          : The status of an object.
                                      status:canceled
    type                            : The type of payment method used for a Sources payment.
                                      type:ideal
    usage                           : The usage availability of a Sources payment method.
                                      usage:single_use
    zip                             : The ZIP code associated with an object.
                                      zip:12345
  * Search Operators
    >                               : Greater than an amount, or after the specified date.
                                      amount:>149.99
    <                               : Less than an amount, or before the specified date.
                                      date:<2018-10-29
    .                               : Within a range of dates or amounts.
                                      amount:50.00..99.99
  * IS Operator
    charge                          : A payment (identical to payment).
                                      is:charge
    captured                        : A payment that has been captured.
                                      is:captured
    coupon                          : A coupon.
                                      is:coupon
    customer                        : A customer.
                                      is:customer
    disputed                        : A payment that has been disputed.
                                      is:disputed
    invoice                         : An invoice.
                                      is:invoice
    invoiceitem                     : An invoice item.
                                      is:invoiceitem
    paid                            : A payment or invoice that has been paid.
                                      is:paid
    payment                         : A payment (identical to charge).
                                      is:payment
    payout                          : A payout.
                                      is:payout
    plan                            : A subscription plan.
                                      is:plan
    refunded                        : A payment or invoice that has been refunded.
                                      is:refunded
    transfer                        : A movement of funds between Stripe accounts.
                                      is:transfer
  * Combinding and Negating
    last4:4242 exp:11/20            : The last four digits of the card are 4242 and expiration date is 11/20.
    last4:4242 -exp:11/20           : The last four digits of the card are 4242 and expiration date is not 11/20.
    type:ideal status:canceled      : iDEAL payments where the source has been canceled and not used to complete a payment.
  * Metadata searches               : To search for a specific metadata key name, use metadata key name as a filter
                                      ex order_id:xyn712
- Teams and User Roles --------------
  * Description                     : Invite team members to access Dashboard and help manage business.
                                      Each team member has different privileges that restrict information
  * Management                      : Team members and user roles are managed in account's business settings
  * Roles
    Administrator                   : The user has the same level of access as the account owner and can view API keys,
                                      change account settings, invite new users, etc. Administrators cannot delete or
                                      make changes to the account owner. Only owners and administrators can connect
                                      the Stripe account to Connect platforms.
    Developer                       : The user cannot manage team members on your Stripe account, update bank account
                                      information, or edit payout settings.
    Analyst                         : The user cannot access any account settings or view API keys. They also cannot
                                      make changes to Stripe Radar settings.
    Support                         : Specialist  The user cannot access any account settings or view API keys.
                                      They also cannot view summarized financial reports, aggregate payment information,
                                      inspect payouts from Stripe to your bank account, or view and make changes
                                      to Stripe Radar settings.
    View Only                       : The user has read-only access to all payment information in your account but
                                      cannot access any account settings or view API keys. They also cannot
                                      view summarized financial reports, aggregate payment information, or
                                      inspect payouts from Stripe to your bank account, or view and make
                                      changes to Stripe Radar settings.
  * Invitations                     : Sent via Email. User with existing account is prompted to log in and then
                                      can switch between accounts
  * Notes                           : Can mention team members when adding a note to a payment. Any team member
                                      that is mentioned receives an email notification with the note and link
                                      to the payment it was added to
  * Email Notifications Reasons     : A successful payment was received
    (changed in user profile)         An application fee is collected from a connected account
                                      A payment is disputed by a customer
                                      A payment is marked as elevated risk by Stripe or Radar rule
                                      Mentioned in a not
                                      Customer sens and incorrect amount to pay their invoice
                                      Webhook delivery fails



ACCOUNT
===============================================================================
- Single Account --------------------
  * Activating                      : Fill out account application, submit
                                      Once account is activated, can immediately start accepting payments
  * Updating Business Information   : Following information made visible to customers on card statement or email receipts
                                      Business name and website URL
                                      Busines email address, phone number, and address
                                      Support sit URL
                                      Statement descriptr text
  * Keep Account Safe               : Keep private info private
                                      Don't reuse Stripe Password
                                      Use team members to share access
                                      Update computer, browser regularly
                                      Beware of phishing
                                      Enable two-step verification
- Multiple Accounts -----------------
  * Description                     : Can create additional Stripe accounts and associate them with your email address
                                      Switch between accounts in dashboard
  * Benefits of multiple accounts   : Separate tax and legal entity information
                                      Unique statement descriptor and public business information
                                      Easier reporting and reconciliation
                                      Payouts to separate bank accounts
- Checklist ------------------------: Enable 2-Step Authentication
                                      Confirm statement descriptor
                                      Set up email notifications
                                      Prevent and manage fraud, have evidence ready
                                      Review bank account info
                                      Establish stripe access for team



DEVELOPMENT
===============================================================================
- Quickstart ------------------------
  1 Obtain API keys                 : https://stripe.com/docs/keys
  2 Install API library             : (python) sudo pip3 install --upgrade stripe
  3 Make a test API request         : stripe.api_key = "sk_test_4eC39HqLyjWDarjtT1zdp7dc"
                                      charge = stripe.Charge.create(
                                        amount=999,
                                        currency='usd',
                                        source='tok_visa',
                                        receipt_email='jenny.rosen@example.com')
- Security --------------------------
  * Payment Card Industry Data
    Security Standards (PCI DSS)    : Anyone involved with processing, transmission, or storage of card data
                                      must comply with PCI DSS
                                      Stripe has been audited by an independent PCI Qualified Security Assessor (QSA)
                                      and is certified as a PCI Level 1 Service Provider
  * PCI Compliance                  : Simplest way is to never see (or have access to) card data at all
                                      Use Checkout, Stripe.js and Elements, or mobile SDK libs to collect payments
                                      Serve pages using TLS so they make use of HTTPS
                                      Review and validate account PCI compliance
  * Validating PCI Compliance       : Fill out Self-Assesment Questionaire (SAQ)
    Checkout/Elements               : Requirement: Pre-filled SAQ A
                                      No action required as Stripe proves that card data never touches your servers
  * TLS and HTTPS                   : TLS   = Transport Layer Security
                                      HTTPS = Hyper Text Transport Protocol Secure
                                      Must use modern version (TLS 1.2)
    Encryption                      : TLS encrypts and verifies integrity of traffic between client and server
    Verify                          : TLS verfifies that client is talking to correct server, not man-in-the-middle
  * Setting TLS
    Digital certificate             : A file issued by a certification authority (CA), needed to use TLS
                                      Assures client that it is talking to correct server
    Certificate providers           : Let's Encrypt, DigiCert, NameCheap
- Webhooks --------------------------
  * Description                     : Stripe can send webhook events that notify your application any time
                                      an event happens on your account. Especially useful for async events
                                      Stripe sends Event object via HTTP POST to endpoint URL defined
                                      in webhook settings
  * Configuring                     : Configure in Dashboard's Webhooks settings section
  * Responding                      : Success if endpoint returns 2xx
                                      In live mode, Stripe attempts to deliver for 3 days with exponential back off
                                      In test mode, retry three times over a few hours
  * Best Practices                  : Test webhooks before going live
                                      Event processing needs to be idempotent as events may be sent twice
                                      Verify webhook signatures
  * API Versions                    : Event structure based on Account API Version at time of event's occurrence
  * Signatures                      : Webhook signatures can be checked to verify the payload is from Stripe
- Error Codes -----------------------
  * 3 types of status codes
    2xx                             : Success, request worked as expected
    4xx                             : Error due to information provided
    5xx                             : Error due to Stripe's servers
- API Keys --------------------------
  * Description                     : Stripe authenticates API requests with account API keys
  * Modes
    Test                            : Keys for testing and development
    Live                            : Production keys
  * Types
    Publishable                     : Can be put in client code
                                      Ex. pk_test_hkoj9EowEd1aFejN4oy2vzvD
    Secret                          : Need to be kept confidential and only stored on servers
                                      Ex. sk_test_SNXLp43e0uGeczaQrMh5vEn2
  * Rolling                         : When rolling an API key you can choose to block the
                                      old key immediately or allow it to work for 12 hours
  * Restricted API Keys             : Can create keys with restricted access that can
                                      only do a subset of actions
                                      Good for reducing microservice risks
- Libraries -------------------------
  * API Libraries                   : Ruby, Python, Java, Node, Go, .NET, iOS and Android
  * Community Libraries             : Angular, C#, ColdFusion, Elixir, JavaScript, Perl, PHP, React Native, Ruby
- Upgrades --------------------------
  * Description                     : API version controls API and webhook behavior
                                      Version set first time API request is made
                                      If backwards-incompatible change is made, new dated version released but
                                      don't change current version until ready to upgrade
  * Backwards-compatible changes    : Adding new API resources
                                      Adding new optional request params to existing API methods
                                      Adding new properties to existing API responses
                                      Changing order of properties in existing API responses
                                      Changing the length or format of object IDs
                                      IDs will never exceed 255 characters. MySQL VARCHAR(255) COLLATE utf8_bin
                                      Adding new events (Webhooks should gracefully handle unknown events)
  * Testing new versions            : Set Stripe-Version header in test or live mode
  * Rolling Back                    : For 72 hours after you've upgraded API version, can safely roll back to version
                                      prior. Failed webhooks will resend with old structure



REPORTING
===============================================================================
- Dashboard Reporting ---------------
  * Monthly report                  : Download from data settings
    Monthly activity                : Summary: sales, refunds, disputes, dispute reversals, other adjusments, net activity
    Reserve Summary                 : Funds held in reserver to mitigate risk
    Transfer Summary
    Balance Summary
    Transfer Reconciliation
- Quickbooks ------------------------
  * Generate                        : Dashboard > Business Settings > Data
  * Quickbooks Accounts
    Stripe Account                  : Type:Bank                : All charges, refunds, and payouts
    Stripe Checking Account         : Type:Bank                : Represents your actual bank account to which Stripe sends payouts
    Stripe Payment Processing Fees  : Type:Expense             : Processing fees for all charges
    Stripe Returns                  : Type:Income              : All refunds
    Stripe Sales                    : Type:Income              : All charges minus processing fees
    Stripe Third-Party Account      : Type:Tax-Related Expense : Every transfer to a third-party
    Stripe Other Fees               : Type:Expense             : Adjustments
    Stripe Processing Fees Adj      : Type:Expense             : Adjustments
    Stripe Other Income             : Type:Income              : Adjustments
- Balance Transactions --------------
  * Description                     : Balance transactions are created for every type of transaction that comes
                                      into, or flows out of, your Stripe account's balance
                                      When a payment is first received, it is initially reflected as pending balance
                                      (less any Stripe fees). This balance becomes available according to payout
                                      schedule
  * Balance Transaction Types
    Related to Charges and Payments
    charge                          : Created when credit card is charted
    payment                         : Created when alternative payment method is charged
    payment_failure_refund          :
    payment_refund                  : Alternative payment method is refunded
    refund                          : Credit card chage refund initiated
    refund_failure                  : Credit card charge refund fails
  * Balance Transaction Types
    Related to Stripe balance chnges
    adjustment                      : Ex. refund failures, disputes, dispute reversals (win dispute)
    payout                          : Payout from Stripe balance to your bank account
    payout_cancel
    reserved_funds                  : Stripe reserving funds to mitigate risk
    stripe_fee                      : Fees for Stripe software and services (e.g Radar, Connect, and Billing)
    stripe_fx_fee                   : Stripe currency conversion fee
    tax_fee                         : Tax on Stripe fees
    topup                           : Funds transferred into Stripe balance from bank account
    topup_reversal