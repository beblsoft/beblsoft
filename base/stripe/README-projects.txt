OVERVIEW
===============================================================================
Stripe Samples Documentation



PROJECT: JS DEMOS
===============================================================================
- Description                       : JS stripe applications
                                    : BeblsoftDemo Stripe Business Used
- Relevant URL                      : https://stripe.com/docs/checkout#integration-simple
- Directory                         : ./js-demos
- simpleCheckout.html ---------------
  * Description                     : Checkout example using Stripe button
  * Relevant URL                    : https://stripe.com/docs/checkout#integration-simple
  * To run                          : Open file in chrome
                                      Click "Pay with Card" button
- customCheckout.html ---------------
  * Description                     : Checkout example using custom button
  * Relevant URL                    : https://stripe.com/docs/checkout#integration-custom
  * To run                          : Open file in chrome
                                      Click "Purchase" Button
- elements.html ---------------------
  * Description                     : Very basic stripe elements example
  * Relevant URL                    : https://stripe.com/docs/stripe-js/elements/quickstart#setup
  * To run                          : Open file in chrome
                                      Fill out credit card form, Click "Submit Payment"
                                      Token will be printed to console



PROJECT: JS ELEMENTS DEMOS
===============================================================================
- Description                       : JS stripe elements demons taken from Github
                                    : BeblsoftDemo Stripe Business Used
- Bibliography URL                  : https://github.com/stripe/elements-examples
- Directory                         : ./js-elements-demos
- To Run                            : Open index.html in chrome



PROJECT: VUE DEMOS
===============================================================================
- Description                       : Vue stripe applications
                                    : BeblsoftDemo Stripe Business Used
- Directory                         : ./vue-demos
- elements.html ---------------------
  * Description                     : Very basic stripe elements example
  * Relevant URL                    : https://stripe.com/docs/stripe-js/elements/quickstart#setup
  * To run                          : Open file in chrome
                                      Fill out credit card form, Click "Purchase"



PROJECT: PYTHON DEMO
===============================================================================
- Description                       : Demo python stripe interface
                                    : BeblsoftDemo Stripe Business Used
- Directory                         : ./python-demo
- Install dendencies                : virtualenv -p /usr/bin/python3.6 venv
                                      source venv/bin/activate
                                      pip3 install -r requirements.txt
- Help ------------------------------
  * Generic                         : ./cli.py -h
  * Command Specific                : ./cli.py <command> -h
- Commands --------------------------
  * Balance actions                 : ./cli.py balance-actions
  * Token actions                   : ./cli.py token-actions
  * Customer crud                   : ./cli.py customer-crud
  * Customer change default card    : ./cli.py customer-swap-sources
  * Card crud                       : ./cli.py card-crud
  * Charge actions                  : ./cli.py charge-actions
  * Charge auth and capture         : ./cli.py charge-auth-capture