#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Stripe Python Sample Command Line Interface
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.balance.dao import StripeBalanceDAO
from base.stripe.python.cardToken.dao import StripeCardTokenDAO
from base.stripe.python.customer.dao import StripeCustomerDAO
from base.stripe.python.charge.dao import StripeChargeDAO
from base.stripe.python.card.dao import StripeCardDAO


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Stripe ----------------------------------
        self.bStripe           = BeblsoftStripe(secretKey="sk_test_AVVBGX9JqS3hFrM8JGDzBAX5")

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None

logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/stripePythonServerDemo.log", type=click.Path(),
              help="Specify log file Default=/tmp/stripePythonServerDemo.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Stripe Python Sample Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- BALANCE ACTIONS ----------------------------------- #
@cli.command()
def balance_actions():
    """
    Balance Actions
    """
    stripeBalanceModel = StripeBalanceDAO.get(bStripe=gc.bStripe)
    stripeBalanceModel.describe()


# ----------------------- TOKEN ACTIONS ------------------------------------- #
@cli.command()
def token_actions():
    """
    Token Actions
    """
    stripeCardTokenModel  = StripeCardTokenDAO.create(
        bStripe     = gc.bStripe,
        number      = "4242424242424242",
        expMonth    = 12,
        expYear     = 2019,
        cvc         = "123")
    stripeCardTokenModel.describe()


# ----------------------- CUSTOMER CRUD ------------------------------------- #
@cli.command()
def customer_crud():
    """
    Customer CRUD
    """
    # Create
    stripeCustomerModel = StripeCustomerDAO.create(
        bStripe        = gc.bStripe,
        email          = "bensson.james@gmail.com",
        metadata       = {"aID": 10})
    stripeCustomerModel.describe()

    # Update
    stripeCustomerModel = StripeCustomerDAO.update(bStripe=gc.bStripe,
                                                   stripeCustomerID=stripeCustomerModel.id,
                                                   email="foo@gmail.com")
    stripeCustomerModel.describe()

    # Delete
    StripeCustomerDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)

    # Verify Delete
    stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=gc.bStripe,
                                                    stripeCustomerID=stripeCustomerModel.id)
    logger.info(stripeCustomerModel)


# ----------------------- CUSTOMER SWAP SOURCES ----------------------------- #
@cli.command()
def customer_swap_sources():
    """
    Customer Swap Sources
    """
    # Create
    stripeCustomerModel  = StripeCustomerDAO.create(
        bStripe              = gc.bStripe,
        email                = "bensson.james@gmail.com",
        metadata             = {"aID": 10})
    stripeCardModel1     = StripeCardDAO.create(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        source               = "tok_visa",
        metadata             = {"id": 1})
    stripeCardModel2     = StripeCardDAO.create(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        source               = "tok_visa",
        metadata             = {"id": 2})

    # Set 1 Default
    stripeCustomerModel   = StripeCustomerDAO.setDefaultCard(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                                                             stripeCardID=stripeCardModel1.id)
    logger.info(stripeCustomerModel.defaultSourceID)

    # Set 2 Default
    stripeCustomerModel   = StripeCustomerDAO.setDefaultCard(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                                                             stripeCardID=stripeCardModel2.id)
    logger.info(stripeCustomerModel.defaultSourceID)

    # Delete
    StripeCardDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                         stripeCardID=stripeCardModel1.id)
    StripeCardDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                         stripeCardID=stripeCardModel2.id)
    StripeCustomerDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)


# ----------------------- CARD CRUD ----------------------------------------- #
@cli.command()
def card_crud():
    """
    Card CRUD
    """
    # Create
    stripeCustomerModel  = StripeCustomerDAO.create(
        bStripe              = gc.bStripe,
        email                = "bensson.james@gmail.com",
        metadata             = {"aID": 10})
    stripeCardModel      = StripeCardDAO.create(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        source               = "tok_visa",
        metadata             = {"id": 1})

    # Update
    stripeCardModel      = StripeCardDAO.update(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        stripeCardID         = stripeCardModel.id,
        metadata             = {"aID": 10})

    # Describe
    stripeCardModel.describe()

    # Describe All
    stripeCardModelList = StripeCardDAO.getAll(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)
    for curStripeCardModel in stripeCardModelList:
        curStripeCardModel.describe()

    # Delete
    StripeCardDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                         stripeCardID=stripeCardModel.id)
    StripeCustomerDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)


# ----------------------- CHARGE ACTIONS ------------------------------------ #
@cli.command()
def charge_actions():
    """
    Charge Actions
    """
    # Create
    stripeCustomerModel  = StripeCustomerDAO.create(
        bStripe              = gc.bStripe,
        email                = "bensson.james@gmail.com",
        metadata             = {"aID": 10})
    stripeCardModel      = StripeCardDAO.create(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        source               = "tok_visa",
        metadata             = {"id": 1})
    stripeChargeModel    = StripeChargeDAO.create(
        bStripe                = gc.bStripe,
        stripeCustomerID       = stripeCustomerModel.id,
        amount                 = 100,
        currency               = "USD",
        description            = "Facebook Profile Analysis",
        metadata               = {"aID": 10, "smFBPID": 12},
        statementDescriptor    = "Facebook Analysis")

    # Describe
    stripeChargeModel.describe()

    # Describe All
    stripeChargeModelList = StripeChargeDAO.getAll(gc.bStripe, stripeCustomerID=stripeCustomerModel.id)
    for curStripeChargeModel in stripeChargeModelList:
        curStripeChargeModel.describe()

    # Delete
    StripeCardDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                         stripeCardID=stripeCardModel.id)
    StripeCustomerDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)


# ----------------------- CHARGE AUTH CAPTURE ------------------------------- #
@cli.command()
def charge_auth_capture():
    """
    Charge Auth and Capture
    """
    # Create
    stripeCustomerModel  = StripeCustomerDAO.create(
        bStripe              = gc.bStripe,
        email                = "bensson.james@gmail.com",
        metadata             = {"aID": 10})
    stripeCardModel      = StripeCardDAO.create(
        bStripe              = gc.bStripe,
        stripeCustomerID     = stripeCustomerModel.id,
        source               = "tok_visa",
        metadata             = {"id": 1})

    # Authorize Charge
    stripeChargeModel    = StripeChargeDAO.create(
        bStripe                = gc.bStripe,
        stripeCustomerID       = stripeCustomerModel.id,
        amount                 = 100,
        currency               = "USD",
        description            = "Facebook Profile Analysis",
        metadata               = {"aID": 10, "smFBPID": 12},
        statementDescriptor    = "Facebook Analysis",
        capture                = False)
    stripeChargeModel.describe()

    # Capture Charge
    stripeChargeModel    = StripeChargeDAO.capture(
        bStripe                = gc.bStripe,
        stripeChargeID         = stripeChargeModel.id)
    stripeChargeModel.describe()

    # Delete
    StripeCardDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id,
                         stripeCardID=stripeCardModel.id)
    StripeCustomerDAO.delete(bStripe=gc.bStripe, stripeCustomerID=stripeCustomerModel.id)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
