# Instagram Documentation

Instagram is a photo a photo and video-sharing social networking service owned by Facebook.

Relevant URLs:
[Home](https://www.instagram.com/),
[Wikipedia](https://en.wikipedia.org/wiki/Instagram),
[Developer Home](https://www.instagram.com/developer/)

## API Endpoints

### /users/self

Get information abut the user of the access_token

```bash
GET https://api.instagram.com/v1/users/self/?access_token=ACCESS-TOKEN
{
  "data": {
    "id": "1574083",
    "username": "snoopdogg",
    "full_name": "Snoop Dogg",
    "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1574083_75sq_1295469061.jpg",
    "bio": "This is my bio",
    "website": "http://snoopdogg.com",
    "is_business": false,
    "counts": {
      "media": 1320,
      "follows": 420,
      "followed_by": 3410
    }
  }
}
```

### /users/self/media/recent

Get the most recent media published by the owner of the access_token.

```bash
GET https://api.instagram.com/v1/users/self/media/recent/?access_token=ACCESS-TOKEN
{
    "data": [{
        "id": "22721881",
        "type": "image",
        "created_time": "1296710327",
        "link": "http://instagr.am/p/BWrVZ/",
        "users_in_photo": [],
        "filter": "Earlybird",
        "tags": ["foodtruck"],
        "location": {
            "latitude": 37.778720183610183,
            "longitude": -122.3962783813477,
            "id": "520640",
            "street_address": "",
            "name": "Le Truc"
        }
        "likes": {
            "count": 15
        },
        "comments": {
            "count": 0
        },
        "user": {
            "username": "kevin",
            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_3_75sq_1295574122.jpg",
            "id": "3"
        },
        "caption": {
            "created_time": "1296710352",
            "text": "Inside le truc #foodtruck",
            "from": {
                "username": "kevin",
                "full_name": "Kevin Systrom",
                "type": "user",
                "id": "3"
            },
            "id": "26621408"
        },
        "images": {
            "low_resolution": {
                "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/6ea7baea55774c5e81e7e3e1f6e791a7_6.jpg",
                "width": 306,
                "height": 306
            },
            "thumbnail": {
                "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/6ea7baea55774c5e81e7e3e1f6e791a7_5.jpg",
                "width": 150,
                "height": 150
            },
            "standard_resolution": {
                "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/6ea7baea55774c5e81e7e3e1f6e791a7_7.jpg",
                "width": 612,
                "height": 612
            }
        },
    },
    {
        "id": "363839373298",
        "type": "video",
        "users_in_photo": null,
        "filter": "Vesper",
        "tags": [],
        "location": null
        "link": "http://instagr.am/p/D/",
        "created_time": "1279340983",
        "comments": {
            "count": 2
        },
        "likes": {
            "count": 1
        },
        "caption": null,
        "videos": {
            "low_resolution": {
                "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
                "width": 480,
                "height": 480
            },
            "standard_resolution": {
                "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
                "width": 640,
                "height": 640
           },
        },
        "images": {
            "low_resolution": {
                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
                "width": 306,
                "height": 306
            },
            "thumbnail": {
                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
                "width": 150,
                "height": 150
            },
            "standard_resolution": {
                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
                "width": 612,
                "height": 612
            }
        },
        "user": {
            "username": "kevin",
            "full_name": "Kevin S",
            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_3_75sq_1295574122.jpg",
            "id": "3"
        },
    },
   ]
}
```

### /media/media-id/comments

Get a list of recent comments on your media object.

```bash
GET https://api.instagram.com/v1/media/{media-id}/comments?access_token=ACCESS-TOKEN

{
    "data": [
        {
            "id": "420"
            "created_time": "1280780324",
            "text": "Really amazing photo!",
            "from": {
                "username": "snoopdogg",
                "profile_picture": "http://images.instagram.com/profiles/profile_16_75sq_1305612434.jpg",
                "id": "1574083",
                "full_name": "Snoop Dogg"
            },
        },
        ...
    ]
}
```
