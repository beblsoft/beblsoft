# Twitter Documentation

Twitter is an American online news and social networking service on which users post and interact
with messages known as "tweets". Tweets were originally restricted to 140 characters, but on
November 7, 2017, this limit was doubled to 280 for most languages. Registered users can post,
like, and retweet tweets, but unregistered users can only read them.

Generic URLs:
[Home](https://twitter.com/?lang=en),
[Wikipedia](https://en.wikipedia.org/wiki/Twitter)

Developer Doc URLs:
[Developer Home](https://developer.twitter.com/content/developer-twitter/en.html),
[Docs](https://developer.twitter.com/en/docs),
[API Reference](https://developer.twitter.com/en/docs/api-reference-index),
[Twitter Test Accounts](http://www.ragorder.com/twitter-test-account-testing-the-twitter-api-with-a-temporary-account/)

Data Dictionary URLs:
[Tweet Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object),
[User Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object),
[Entity Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object),
[Extended Entity Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object),
[Places Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects)

Developer Code Utility URLs:
[TwitterDev Github Repos](https://github.com/twitterdev),
[API Libraries](https://developer.twitter.com/en/docs/developer-utilities/twitter-libraries.html),
[twitter-text Parser](https://developer.twitter.com/en/docs/developer-utilities/twitter-text)

Developer Application URLs:
[Developer Portal](https://developer.twitter.com/en/docs/basics/developer-portal/overview),
[App Dashboard](https://developer.twitter.com/en/apps)

## Getting Started

### Use Cases

- Integrate Tweets into your app or website?
- Add the ability to post Tweets or send Direct Messages from your app or website?
- Target an audience and advertise on the Twitter network?
- Listen in real-time for Tweets of interest?
- Conduct historical research and search from Twitter's massive archive of publicly-available Tweets posted since March 2006?
- Measure how your Tweets or account are engaged with?

### Creating a Twitter App

Integrating with the Twitter APIs requires the creation of a Twitter App and the generation of consumer
keys and access tokens.

- Visit the developer portal with an approved developer account
- Create a twitter app to generate your authentication tokens

### Twurl

There are many libraries and utilities in different programming languages to help you get started.
Once you've established access, making API requests is simple.

Quickly make your first API request with the HTTP wrapper called
[twurl](https://developer.twitter.com/en/docs/basics/developer-portal/overview).

### Post Tweets

One of the most comomon uses of the standard API family are the endpoints used to post Tweets. There
are also methods for retweeting, favoriting tweets, deleting tweets, and other common actions.

### Advertise with Twitter

Twitter Ads API endpoints provide developers a programmatic way to create and manage Twitter Ads
campaigns. In addition, these endpoints provide a means to access features like Audiences, Creatives,
Targeting, etc.

### Listen in real-time for Tweets of interest

Twitter is a real-time communication network, with very little latency. Many twitter integrations
depend on the ability to stream Tweets in real-time using HTTP streaming.

### Search the Tweet archive

Public conversations have been happening on the Twitter platform since early 2006. With the historical
APIs, Twitter data has informed an amazing variety of research. When it comes to filtering and collecting
Tweets, the search APIs are by far the most commonly used tools. With these search APIs, a single query
is submitted, and the API responds with a first 'page' of results, providing a 'next' token that is
used to paginate through results as needed.

### Measure and listen for Twitter Engagement

The engagement API provides endpoints for query-based access to organic impressions and engagement
data for tweets from authorized/owned accounts, as well as engagement metrics for Favorites, Retweets,
and Replicas for any Tweet.

### Data Objects

The data provided by Twitter APIs are made up of data objects and their attributes rendered in JavaScript
Object Notation (JSON). The basic object types are:

- Tweet object
- User object: public twitter account metadata
- Entities and extended entities: metda (photo, video, gif), hashtags, URLs, mentions, and polls
- Geospatial objects

## Things every developer should know

- There are different API families

  The standard (free) Twitter APIs consist of REST APIs and Streaming APIs.

  The enterprise (paid subscription) APIs include filtered firehose, historical search, and engagement
  APIs for deeper data analytics, listening, and other enterprise business applications.

  The premium (pay as you go) APIs consist of reliable and affordable versions of enterprise APIs,
  allowing your business to grow with your usage.

  Additionally, there are some families of APIs (such as Ads API) which require applications to be
  whitelisted in order to make use of them.

- The API aims to be a RESTful resource

- The API is HTTP-based (over SSL)

- Tweet IDs can break JavaScript

  Use the `id_str` field instead of `id` whenever present to stay safe. Web browsers/ Javascript
  interpreters/ JSON consumers may munch larger integer-based ids, which is why it is recommended to use
  the string representation.

- There are limits to how many calls and changes can be made in a day

  API usage is rate limited, with additional account-based fair use limits on write/create/delete
  endpoints, to protect twitter from abuse.

- Parameters have certain expectations

    * Parameter values should be converted to UTF-8 and URL encoded.
    * The page parameter begins at 1, not 0.

  Where noted, some API methods will return different results based on HTTP headers sent by the client.
  Where the same behavior can be controlled by both a parameter and an HTTP header, the parameter will
  take precedence.

- There are pagination limits

  Clients may access a theoretical maxiumum of 3,200 statuses via the page and count parameters for the
  user_timeline REST API methods. Other timeline methods have a theoretical maximum of 800 statuses.
  Requests for more than the limit will result in a reply with a status code of 200 and an empty result
  in the format requested. Twitter still maintains a database of all the Tweets sent by a user.
  However, to ensure performance, this limit is in place on the API calls.

- There are Twitter API libraries for almost any language

  The community has created numerous
  [Twitter API libraries](https://developer.twitter.com/en/docs/developer-utilities/twitter-libraries.html).

## Twitter Developer Apps

### Callback URLs

Sign in with Twitter allows developers to access Twitter content in order to make it easy for their users
to sign in with just a few clicks. Developers use __Callback URLs__ as part of this integration in order
to provide directions on where a user should go after signing in with their Twitter credentials.
Any developer using Sign in with Twitter must explicitly declare their callback URLs in a whitelist
in the Twitter apps settings which can be accessed in the apps dashboard.

Best practices:

- Need more than 10 callback URLs? There is a hard limit of 10 callback URLs in the Twitter apps dashboard.
  Combine callback URLs into a single address and use query strings for more combinations.

- Do not add query strings to your callback URLs in your Twitter app's configuration

- Don't use localhost as a callback URL. Instead of using localhost, use custom host locally or
  https://127.0.0.1

- Mobile apps with app-specific protocols must use just the protocol

## Developer Apps

If you create a new Twitter app, you _must_ have an approved developer account. Also, if you are a
member of a team account, you must be assigned an admin role.

Accessing the Twitter APIs requires a set of credentials that you must pass with each request. These
credentials can come in different forms depending on the type of authentication that is required by the
specific endpoint.

Each Twitter app will be able to generate its own API key and API secret key that you will use to make
requests on behalf of the app, as well as an access tokens and access token secret that you will use
to make requests on behalf of the owning user.

The Twitter app dashboard allows developers to quickly and easily perform the following tasks:
- View exsting apps and their associated app ID.
- Create a new app (if you have an approved developer account)
- Delete an unused app
- Open up a specific app's settings. Within the settings you can see the app details, keys, tokens,
  and permissions.

## Developer Portal

The twitter developer portal is a set of self-serve tools that developers can use to manage their
access to the premium APIs, as well as to create and manage their Twitter apps.

The portal is made up of the following pages:

- A developer dashboard that displays Premium API usage and subscription level
- A subscriptions page
- An apps page where you can create and manage your Twitter apps
- An environments page where you can set up your developer environments
- A bulling page where you can view your payment details and previous invoices
- A teams page where you can add and manage the different handles that have access to your team's
  premium APIs

To get a twitter developer account, [apply](https://developer.twitter.com/en/apply).

Developers can both create and manage Twitter apps from the developer portal under the "Apps" section.
This includes editing an app's callback URL, setting an app's access levels, and importantly, finding
an app's ID, consumer tokens, and access tokens. You can also regenerate an app's keys if you believe
they've been compromised or lost.

## Authentication

Using the Twitter API requires an authorized Twitter App and authenticated requests.  The Twitter API
supports a few authentication methods depending on the endpoint.  Each endpoint's API reference document
will describe the required authentication method.

When considering an API endpoint, it's important to note which authentication is needed on the request
and create a plan for authorization when developing your application.  In general, endpoints related
to specific user information will require OAuth (Application-user) authentication, and endpoints related
to retrieving publicly available information will require OAuth2 (bearer token) or Basic Auth (for
Enterprise data APIs).

| API type                      | Authentication method                                          |
|:------------------------------|:---------------------------------------------------------------|
| Standard REST & Streaming API | OAuth2 (bearer token), OAuth 1a (application-user)             |
| Premium API OAuth2            | (bearer token), OAuth 1a (application-user)                    |
| Enterprise API                | Basic Auth, OAuth2 (bearer token), OAuth 1a (application-user) |
| Ads API OAuth 1a              | (application-user)                                             |

### Authorizing a Request

Twitter needs authorization information to answer the following questions:
- Which application is making the request
- Which user the request is posting on behalf of
- Whether the user has granted the application authorization to post on the user’s behalf
- Whether the request has been tampered by a third party while in transit

To allow applications to provide this information, Twitter’s API relies on the OAuth 1.0a protocol.
At a very simplified level, Twitter’s implementation requires that requests needing authorization contain
an additional HTTP `Authorization` header with enough information to answer the questions listed above.

A valid request is shown below:
```text
POST /1.1/statuses/update.json?include_entities=true HTTP/1.1
Accept: */*
Connection: close
User-Agent: OAuth gem v0.4.4
Content-Type: application/x-www-form-urlencoded
Authorization:
OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog",
oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",
oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D",
oauth_signature_method="HMAC-SHA1",
oauth_timestamp="1318622958",
oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
oauth_version="1.0"
Content-Length: 76
Host: api.twitter.com

status=Hello%20Ladies%20%2b%20Gentlemen%2c%20a%20signed%20OAuth%20request%21
```

#### Collecting Parameters

You should be able to see that the header contains 7 key/value pairs, where the keys all begin with
the string “oauth_”. For any given Twitter API request, collecting these 7 values and creating a
similar header will allow you to specify authorization for the request.

- __Consumer Key__: The `oauth_consumer_key` identifies which application is making the request. Obtain
  this value from the settings page of your Twitter App. Example: `oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog"`

- __Nonce__: The `oauth_nonce` parameter is a unique token your application should generate for each
  unique request. Twitter will use this value to determine whether a request has been submitted multiple times.
  The value for this request was generated by base64 encoding 32 bytes of random data, and stripping out
  all non-word characters, but any approach which produces a relatively random alphanumeric string is OK.
  `oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg"`

- __Signature__: The `oauth_signature` parameter contains a value which is generated by running all of the
  other request parameters and two secret values through a signing algorithm. The purpose of the signature
  is so that Twitter can verify that the request has not been modified in transit, verify the application
  sending the request, and verify that the application has authorization to interact with the user's
  account. Ex. `oauth_signature="tnnArxj06cWHq44gCs1OSKk/jLY="`

- __Signature method__: The `oauth_signature_method` used by Twitter is HMAC-SHA1. This value should be used for
  any authorized request sent to Twitter's API. See below for explanation.

- __Timestamp__: The `oauth_timestamp` parameter indicates when the request was created. This value should be the
  number of seconds since the Unix epoch at the point the request is generated, and should be easily generated in most
  programming languages. Twitter will reject requests which were created too far in the past, so it is important
  to keep the clock of the computer generating requests in sync with NTP. Ex. `oauth_timestamp=1318622958`

- __Token__: The `oauth_token` parameter typically represents a user's permission to share access to their account
 with your application. There are a few authentication requests where this value is not passed or is a different
 form of token. For most general-purpose requests, this is referred to as an __access token__.

 You can generate a valid access token for your account on the settings page for your Twitter app on the
 developer portal. Ex. `oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb"`

- __Version__: The `oauth_version` parameter should always be 1.0 for any request sent to the Twitter API.
  Ex. `oauth_version=1.0`

#### Building the Header String

To build the header string, imagine writing a string named DST.

1. Append the string "OAuth" (including the space at the end) to DST.
2. For each key/value pair of the 7 parameters listed above:
    * Percent encode the key and append it to DST
    * Append the equals character `=` to DST
    * Append a double quote `"` to DST
    * Percent encode the value and append it to DST
    * Append a double quote `"` to DST
    * If there are key/value pairs remaining, append a comma `,` and a space ` ` to DST

Pay particular attention to the percent encoding of the values when building the string. For example
the `oauth_signature` value of `tnnArxj06cWHq44gCs1OSKk/jLY=` must be encoded as `tnnArxj06cWHq44gCs1OSKk%2FjLY%3D`.

Performing these steps on the parameters collected above results in the following string:
```text
OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg", oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1318622958", oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb", oauth_version="1.0"
```

### Creating a signature

Twitter requires a OAuth 1.0a HMAC-SHA1 signature for a HTTP request. This signature will be suitable
for passing to the Twitter API as part of an authorized request.

The request used to demonstrate signing is:

```text
POST /1.1/statuses/update.json?include_entities=true HTTP/1.1
Accept: */*
Connection: close
User-Agent: OAuth gem v0.4.4
Content-Type: application/x-www-form-urlencoded
Content-Length: 76
Host: api.twitter.com

status=Hello%20Ladies%20%2b%20Gentlemen%2c%20a%20signed%20OAuth%20request%21
```

#### Collecting the request method and URL

To produce a signature, start by determining the HTTP method and URL of the request. These two are
known when creating the request, so they are easy to obtain.

`HTTP Method="POST"`

`Base URL="https://api.twitter.com/1.1/statuses/update.json"`

#### Collecting parameters

Next, gather all of the parameters included in the request. There are two such locations for these additional
parameters - the URL (as part of the querystring) and the request body. In addition to the request parameters, every
`oauth-*` parameter needs to be included in the signature. Here are the parameters for the above
request:

| Name                   | Value                                              |
|:-----------------------|:---------------------------------------------------|
| status                 | Hello Ladies + Gentlemen, a signed OAuth request!  |
| include_entities       | true                                               |
| oauth_consumer_key     | xvz1evFS4wEEPTGEFPHBog                             |
| oauth_nonce            | kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg         |
| oauth_signature_method | HMAC-SHA1                                          |
| oauth_timestamp        | 1318622958                                         |
| oauth_token            | 370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb |
| oauth_version          | 1.0                                                |

These values need to be encoded into a single string which will be used later on. The process to build
the string is very specific.

1. Percent encode every key and value that will be signed
2. Sort the list parameters alphatetically by encoded key
3. For each key/value pair:
    * Append the encoded key to the output string
    * Append the `=` character to the output string
    * Append the encoded value to the output string
    * If there are more key/value pairs remaining, append a `&` character to the output string

The following parameter string will be produced by repeating these steps with the parameters collected above:
```text
include_entities=true&oauth_consumer_key=xvz1evFS4wEEPTGEFPHBog&oauth_nonce=kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1318622958&oauth_token=370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb&oauth_version=1.0&status=Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21
```

#### Creating the signature base string

The three values collected so far must be joined to make a single string, from which the signature will be
generated. This is called the __signature base string__ by the OAuth specification.

To encode the HTTP method, base URL, and parameter string into a single string:

1. Convert the HTTP method to uppercase and set the output string to equal this value.
2. Append the `&` character to the output string
3. Percent encode the URL and append it to the output string
4. Append the `&` character to the output string
5. Percent encode the parameter string and append it to the output string

This produces the following signature base string:

```text
POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521
```

Make sure to percent encode the parameter string! The signature base string should contain exactly 2
ampsersand `&` characters. The percent `%` characters in the parameter string should be encoded as `%25`
in the base string.

#### Getting a signing key

The last pieces of data to collect are secrets which identify the Twitter app making the request, and
the user the request is on behalf of. It is very important to note that these values are incredibly sensitive
and should never be shared with anyone.

The value value which identifies your app to Twitter is called the __consumer secret__ and can be found in the
developer portal by viewing the app details page. This will be the same for every request your Twitter app
sends. For example: `Consumer secret="kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw"`

The value which identifies the account your application is acting behalf of is called the __oauth token secret__.
This value can be obtained in several ways. For example: `OAuth token secret="LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"`

Once again, it is very important to keep these values private to your application. If you feel that your values have
been compromised, regenerate your tokens.

Both of these values need to be combined to form a __signing key__ which will be used to generate the signature.
The signing key is simply the percent encoded consumer secret, followed by an ampersand character `&`, followed
by the percent encoded token secret. Note that there are some flows, such as when obtaining a request token,
where the token secret is not yet known. In this case, the signing key should consist of the percent encoded
consumer secret followed by an ampersand and no oauth token secret.

Signing key example: `kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw&LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE`

#### Calculating the signature

The signature is calculated by passing the signature base string and signing key to the HMAC-SHA1 hashing
algorithm. The output of the HMAC signing function is a binary string. This needs to be base64 encoded
to produce the signature string.

For example, the output given the base string and signing key given on this page is
`84 2B 52 99 88 7E 88 7602 12 A0 56 AC 4E C2 EE 16 26 B5 49`. That value, when converted to base64,
is the OAuth signature for this request: `OAuth signature="hCtSmYh+iHYCEqBWrE7C7hYmtUk="`

### Application-only authentication: OAuth2 (bearer token)

Application-only authentication is a form of authentication where an application makes API requests on
its own behalf, without the user context. This method is for developers that just need read-only access
public information.

Example: If a developer is trying to view Tweets or lists that are publically available, then they just
have to use application-only authentication.

To use this method, you need to use a __bearer token__. You can generate a bearer token by passing
your consumer key and secret key through the __POST oauth2/token__ endpoint.

API calls using app-only authentication are rate limited per API method at the app level.  Not all API
methods support application-only authentication because some methods require a user context (for example,
a Tweet can only be created by a logged-in user, so user context is required for that operation).

#### Flow

The application-only auth flow follows these steps:

- An application encodes its consumer key and secret into a specially encoded set of credentials.
- An application makes a request to the POST oauth2 / token endpoint to exchange these credentials for a bearer token.
- When accessing the REST API, the application uses the bearer token to authenticate.

```text

  Application Server

  1. User consumer key and secret key to request                POST /oauth2/token
     bearer token. Twitt                            -------->  Authorization: Basic --->  Twitter Returns Bearer Token

                                                                GET /search/tweets.js
  2. Request API Resource                           -------->  Authorization: Bearner --->  Twitter handles API request
```

#### About

- __Tokens are passwords__: Keep in mind that the consumer key and secrent, bearer token credentials, and the bearer
  token itself grant access to make requests on behalf of an application. These values should be considered as sensitive
  as passwords, and must not be shared or distributed to untrusted parties.

- __SSL Required__: All requests _must_ use HTTPS endpoints

- __No user context__: When issuing requests using application-only auth, there is no concept of a "current user".
  Therefore, endpoints such as POST statuses/update will not function with application-only auth.

- __Rate Limiting__: Applications have two kinds of rate limiting pols
  Requests made on behalf of users with access tokens depletes from a different rate limiting context than
  that used in application-only authentication. In other words, requests made on behalf of users will not
  deplete from the rate limits available through app-only auth and requests made through app-only will not deplete
  the rate limits used in user-based auth.

  `GET application/rate_limit_status` supports application-only authentication.

#### 1. Encode consumer key and secret

The steps to encode an application's consumer key and secret into a set of credentials to obtain a bearer token are:
1. URL encode the consumer key and the consumer secret accorindg to RFC 1738.
2. Concatenate the encoded consumer key, a colon character ":", and the encoded consumer secret into a single
   string.
3. Base64 encode the string from the previous step

| Name                                               | Value                                                                                       |
|:---------------------------------------------------|:--------------------------------------------------------------------------------------------|
| Consumer key                                       | xvz1evFS4wEEPTGEFPHBog                                                                      |
| Consumer secret                                    | L8qq9PZyRg6ieKGEKhZolGC0vJWLw8iEJ88DRdyOg                                                   |
| RFC 1738 encoded consumer key (does not change)    | xvz1evFS4wEEPTGEFPHBog                                                                      |
| RFC 1738 encoded consumer secret (does not change) | L8qq9PZyRg6ieKGEKhZolGC0vJWLw8iEJ88DRdyOg                                                   |
| Bearer token credentials                           | xvz1evFS4wEEPTGEFPHBog:L8qq9PZyRg6ieKGEKhZolGC0vJWLw8iEJ88DRdyOg                            |
| Base64 encoded bearer token credentials            | :: eHZ6MWV2RlM0d0VFUFRHRUZQSEJvZzpMOHFxOVBaeVJnNmllS0dFS2hab2xHQzB2SldMdzhpRUo4OERSZHlPZw== |

#### 2. Obtain a bearer token

The value calculated in step 1 must be exchanged for a bearer token by issuing a request to `POST oauth2/token`.

- The request must be a HTTP Post
- The request must include an `Authorization` header with the value of `Basic <base64 encoded value from step 1>`
- The request must include a Content-Type header with the value of application/x-www-form-urlencoded;charset=UTF-8.
- The body of the request must be `grant_type=client_credentials`
- Example response
  ```json
  {"token_type":"bearer","access_token":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%2FAAAAAAAAAAAAAAAAAAAA%3DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"}
  ```

#### 3. Invalidating a bearer token

Should a bearer token become compromised or need to be invalidated for any reason, issue a call to `POST oauth2/invalidate_token`

#### 4. Authenticate API requests with the bearer token

The bearer token may be used to issue requests to API endpoints which support application-only auth.
To use the bearer token, construct a normal HTTPs request and include an `Authorization` header with the
value of `Bearer <base64 bearer token value from step 2>. Signing is not required.`

### Application-user authentication: OAuth 1a (access token for user context)

The user authentication method of authentication allows an authorized app to act on behalf of the
user, as the user.

Example: if a developer wanted to build a feature that would allow a user to post Tweets through their
platform using the statuses/update endpoint, the developer would have to use user authentication to
get permission from the user to post a Tweet on their behalf.

User authentication requires the consumer key and secret from your Twitter app and the access token
and access token secret retrieved through the
[3-legged-OAuth](https://developer.twitter.com/en/docs/basics/authentication/overview/3-legged-oauth)
process with the user that the developer is action on the behalf of. Not all API methods support
application-user authentication.

#### Flow

The possible states for the 3-legged sign in interaction are illustrated in the following flowchart:

```text

  User clicks "Sign in       User signed into Twitter         Twitter asks users
    with Twitter"     ------------------------------------->  to grant access    ---------
                      |                                                                   \
                      |                                                                    ------> Site Collects OAuth Token
                      |      User not signed into Twitter                                 /
                      ------------------------------------->  Twitter asks the user ------
                                                              to sign in and grant access
```

#### Terminology Clarification

- App Key = API Key = Consumer API Key = Consumer Key = Customer Key = oauth_consumer_key
- App Key Secret = API Secret Key = Consumer Secret = Consumer Key = Customer Key
- Callback URL = oauth_callback
- Request token = oauth_token
- Request token secret = oauth_token_secret
- Access Token = Token = resulting oauth_token
- Access token secret = Token Secret = resulting oauth_token_secret

#### 1. POST oauth/request_token

The only unique parameter in this request is oauth_callback, which must be a URL-encoded version of the URL
you wish your user to be redirected to when they complete step 2. The remaining parameters are added by the OAuth
signing process.

Any callback URL that you use with the POST oauth/request_token will have to be whitelisted within your Twitter
app settings in the app details page of developer portal.

Request includes:

`oauth_callback="https%3A%2F%2FyourWhitelistedCallbackUrl.com"`

`oauth_consumer_key="cChZNFj6T5R0TigYB9yd1w"`

Response includes:

`oauth_token=NPcudxy0yU5T3tBzho7iCotZ3cnetKwcTIRlX0iwRl0`

`oauth_token_secret=veNRnAWe6inFuo8o2u8SLLZLjolYDmDP7SzL0YfYI`

`oauth_callback_confirmed=true`

#### 2. GET oauth/authorize

Example URL to redirect user to:

`https://api.twitter.com/oauth/authorize?oauth_token=NPcudxy0yU5T3tBzho7iCotZ3cnetKwcTIRlX0iwRl0`

Upon successful authentication, your `callback_url` would receive a request containing the `oauth_token`
and `oauth_verifier` parameters. You application should verify that the token matches the request token
received in step 1.

Request from client's redirect:

`https://yourWhitelistedCallbackUrl.com?oauth_token=NPcudxy0yU5T3tBzho7iCotZ3cnetKwcTIRlX0iwRl0&oauth_verifier=uw7NjWHT6OJ1MpJOXsHfNxoAhPKpgI8BlYDhxEjIBY`

#### 3. POST oauth/access_token

Converting the request token to an access token.

Request includes:

`POST /oauth/access_token`

`oauth_consumer_key=cChZNFj6T5R0TigYB9yd1w`

`oauth_token=NPcudxy0yU5T3tBzho7iCotZ3cnetKwcTIRlX0iwRl0`

`oauth_verifier=uw7NjWHT6OJ1MpJOXsHfNxoAhPKpgI8BlYDhxEjIBY`

Response includes:

`oauth_token=7588892-kagSNqWge8gB1WwE3plnFsJHAZVfxWD7Vb57p0b4`

`oauth_token_secret=PbKfYqSryyeKDWz4ebtY3o5ogNLG11WJuZBc9fQrQo`

#### 4. Using these credentials for app-user required requests

Request includes:

`POST statuses/update.json`

`oauth_consumer_key=cChZNFj6T5R0TigYB9yd1w`

`oauth_token=7588892-kagSNqWge8gB1WwE3plnFsJHAZVfxWD7Vb57p0b`

## Rate Limiting

Rate limiting of the standard API is primarily on a per-user basis (per user access token). If a method
allows for 15 requests per rate limit window, then it allows 15 requests per window per access token.

WHen using application-only authentication, rate limits are determined globally for the entire application.
If a method allows for 15 requests per rate limit window, then it allows you to make 15 requests per window
on behalf of your application. This limit is considered completely separate from per-user limits.

### 15 minute windows

Rate limits are divided into 15 minute intervals. All endpoints require authentication, so there is no concept
of unauthenticated calls and rate limits.

There are two initial buckts avaulable for GET requests: 15 calls every 15 minuts, and 180 calls every 15
minutes.

### HTTP Headers and Response Codes

Use the HTTP headers in order to understand where the application is at for a given rate limit, on the method
that was just utilized.

- `x-rate-limit-limit`: the rate limit ceiling for that given endpoint
- `x-rate-limit-remaining`: the number of requests left for the 15 minute window
- `x-rate-limit-reset`: the remaining window before the rate limit resets, in UTC epoch seconds

When an application exceeds the rate limit for a given standard API endpoint, the API will return
a __HTTP 429: "Too Many Requests"__ response code, and the following error will be returned in
the response body:

```json
 { "errors": [ { "code": 88, "message": "Rate limit exceeded" } ] }
```

To better predict the rate limits available, consider periodically using __GET application / rate_limit_status__.
The the rate limiting HTTP headers, this resource's response will indicate the rate limit status for the
calling context.

### GET and POST Request Limits

Rate limits on reads from the system (GET) are defined on a per user and per application basis, while
rate limits on writes into the system (POST) are defined solely at the user account level.

There may be times when the rate limit values that are returned are inconsistent, or cases where no
headers are returned at all. Perhaps memcache has been reset, or one memcache was busy so the system
spoke to a different instance. There is a best effort to maintain consistency, with a tendency towards
giving an application extra calls if there is an inconsistency.

### Tips to avoid being Rate Limited

- __Caching__: Store API responses in your application or on your site if you expect a lot of use

- __Prioritize active users__: Consider only requesting data for users who have recently signed into your site

- __Adapt to the search results__: If your application monitors a high volume of search terms, query less
  often for searches that have no results than for those that do.

- __Use application-only auth as a "reserve"__: Requests using Application-only authentication are evaluated
  in a separate context to an application's per-user rate limits. For many scenarios, you may want to use
  this additional rate limit pool as a "reserve" for your typical user-based operations

- __Blacklisting__: If an application abuses the rate limits, it will be blacklisted. Blacklisted apps are unable
  to get a response from the Twitter API. If you or your application has been blacklisted and you think
  there has been a mistake, you can use our [Platform Support forms](https://support.twitter.com/forms/platform)
  to request assistance.

### Streaming API

The Streaming API has rate limiting and access levels that are appropriate for long-lived connections.

Leveraging the Streaming API is a great way to free-up your rate limits for more inventive uses of
the Twitter API.

If the initial reconnect attempt is unsuccessful, your client should continue attempting to reconnect
using an exponential back-off pattern until it successfully reconnects. (e.g. wait 1 second, wait
2 seconds, then, 4, 8, 16, etc. with some reasonable upper limit)

## Response Codes

The standard Twitter API returns HTTP status codes in addition to JSON-based error codes and messages.

### HTTP Status Codes

The Twitter API attempts to return appropriate [HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
for every request.

| Code | Text                  | Description                                                                                                                                                                                                                                                                                                |
|:-----|:----------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 200  | OK                    | Success!                                                                                                                                                                                                                                                                                                   |
| 304  | Not Modified          | There was no new data to return.                                                                                                                                                                                                                                                                           |
| 400  | Bad Request           | The request was invalid or cannot be otherwise served. An accompanying error message will explain further. Requests without authentication are considered invalid and will yield this response.                                                                                                            |
| 401  | Unauthorized          | Missing or incorrect authentication credentials. This may also returned in other undefined circumstances.                                                                                                                                                                                                  |
| 403  | Forbidden             | The request is understood, but it has been refused or access is not allowed. An accompanying error message will explain why. This code is used when requests are being denied due to update limits . Other reasons for this status being returned are listed alongside the error codes in the table below. |
| 404  | Not Found             | The URI requested is invalid or the resource requested, such as a user, does not exist.                                                                                                                                                                                                                    |
| 406  | Not Acceptable        | Returned when an invalid format is specified in the request.                                                                                                                                                                                                                                               |
| 410  | Gone                  | This resource is gone. Used to indicate that an API endpoint has been turned off.                                                                                                                                                                                                                          |
| 420  | Enhance Your Calm     | Returned when an app is being rate limited for making too many requests.                                                                                                                                                                                                                                   |
| 422  | Unprocessable Entity  | Returned when the data is unable to be processed (for example, if an image uploaded to POST account / update_profile_banner is not valid, or the JSON body of a request is badly-formed).                                                                                                                  |
| 429  | Too Many Requests     | Returned when a request cannot be served due to the app's rate limit having been exhausted for the resource. See Rate Limiting.                                                                                                                                                                            |
| 500  | Internal Server Error | Something is broken. This is usually a temporary error, for example in a high load situation or if an endpoint is temporarily having issues. Check in the developer forums in case others are having similar issues,  or try again later.                                                                  |
| 502  | Bad Gateway           | Twitter is down, or being upgraded.                                                                                                                                                                                                                                                                        |
| 503  | Service Unavailable   | The Twitter servers are up, but overloaded with requests. Try again later.                                                                                                                                                                                                                                 |
| 504  | Gateway timeout       | The Twitter servers are up, but the request couldn’t be serviced due to some failure within the internal stack. Try again later.                                                                                                                                                                           |

### Error Messages

Twitter API error messages are returned in JSON format. For example, an error might look like this:

```json
{"errors":[{"message":"Sorry, that page does not exist","code":34}]}
```

### Error Codes

In addition to descriptive error text, error messages contain machine-parseable codes. While the
text for an error message may change, the codes will stay the same.

The following table describes the codes which may appear when working with the standard API. If
an error response is not listed in the table, fall back to examining the HTTP status codes above
in order to determine the best way to address the issue.

| Code | Text                                                                                             | Description                                                                                                                                                                                                                                                                                                                                                                              |
|:-----|:-------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 3    | Invalid coordinates.                                                                             | Corresponds with HTTP 400. The coordinates provided as parameters were not valid for the request.                                                                                                                                                                                                                                                                                        |
| 13   | No location associated with the specified IP address.                                            | Corresponds with HTTP 404. It was not possible to derive a location for the IP address provided as a parameter on the geo search request.                                                                                                                                                                                                                                                |
| 17   | No user matches for specified terms.                                                             | Corresponds with HTTP 404. It was not possible to find a user profile matching the parameters specified.                                                                                                                                                                                                                                                                                 |
| 32   | Could not authenticate you.                                                                      | Corresponds with HTTP 401. There was an issue with the authentication data for the request.                                                                                                                                                                                                                                                                                              |
| 34   | Sorry, that page does not exist                                                                  | Corresponds with HTTP 404. The specified resource was not found.                                                                                                                                                                                                                                                                                                                         |
| 36   | You cannot report yourself for spam.                                                             | Corresponds with HTTP 403. You cannot use your own user ID in a report spam call.                                                                                                                                                                                                                                                                                                        |
| 38   | `<named>` parameter is missing.                                                                  | Corresponds with HTTP 403. The request is missing the `<named>` parameter (such as media, text, etc.) in the request.                                                                                                                                                                                                                                                                    |
| 44   | attachment_url parameter is invalid                                                              | Corresponds with HTTP 400. The URL value provided is not a URL that can be attached to this Tweet.                                                                                                                                                                                                                                                                                       |
| 50   | User not found.                                                                                  | Corresponds with HTTP 404. The user is not found.                                                                                                                                                                                                                                                                                                                                        |
| 63   | User has been suspended.                                                                         | Corresponds with HTTP 403 The user account has been suspended and information cannot be retrieved.                                                                                                                                                                                                                                                                                       |
| 64   | Your account is suspended and is not permitted to access this feature                            | Corresponds with HTTP 403. The access token being used belongs to a suspended user.                                                                                                                                                                                                                                                                                                      |
| 68   | The Twitter REST API v1 is no longer active. Please migrate to API v1.1.                         | Corresponds to a HTTP request to a retired v1-era URL.                                                                                                                                                                                                                                                                                                                                   |
| 87   | Client is not permitted to perform this action.                                                  | Corresponds with HTTP 403. The endpoint called is not a permitted URL.                                                                                                                                                                                                                                                                                                                   |
| 88   | Rate limit exceeded                                                                              | The request limit for this resource has been reached for the current rate limit window.                                                                                                                                                                                                                                                                                                  |
| 89   | Invalid or expired token                                                                         | The access token used in the request is incorrect or has expired.                                                                                                                                                                                                                                                                                                                        |
| 92   | SSL is required Only SSL connections are allowed in the API.                                     | Update the request to a secure connection. See how to connect using TLS                                                                                                                                                                                                                                                                                                                  |
| 93   | This application is not allowed to access or delete your direct messages                         | Corresponds with HTTP 403. The OAuth token does not provide access to Direct Messages.                                                                                                                                                                                                                                                                                                   |
| 99   | Unable to verify your credentials.                                                               | Corresponds with HTTP 403. The OAuth credentials cannot be validated. Check that the token is still valid.                                                                                                                                                                                                                                                                               |
| 120  | Account update failed: value is too long (maximum is nn characters).                             | Corresponds with HTTP 403. Thrown when one of the values passed to the update_profile.json endpoint exceeds the maximum value currently permitted for that field. The error message will specify the allowable maximum number of nn characters.                                                                                                                                          |
| 130  | Over capacity                                                                                    | Corresponds with HTTP 503. Twitter is temporarily over capacity.                                                                                                                                                                                                                                                                                                                         |
| 131  | Internal error                                                                                   | Corresponds with HTTP 500. An unknown internal error occurred.                                                                                                                                                                                                                                                                                                                           |
| 135  | Could not authenticate you                                                                       | Corresponds with HTTP 401. Timestamp out of bounds (often caused by a clock drift when authenticating - check your system clock)                                                                                                                                                                                                                                                         |
| 139  | You have already favorited this status.                                                          | Corresponds with HTTP 403. A Tweet cannot be favorited (liked) more than once.                                                                                                                                                                                                                                                                                                           |
| 144  | No status found with that ID.                                                                    | Corresponds with HTTP 404. The requested Tweet ID is not found (if it existed, it was probably deleted)                                                                                                                                                                                                                                                                                  |
| 150  | You cannot send messages to users who are not following you.                                     | Corresponds with HTTP 403. Sending a Direct Message failed.                                                                                                                                                                                                                                                                                                                              |
| 151  | There was an error sending your message: reason                                                  | Corresponds with HTTP 403. Sending a Direct Message failed. The reason value will provide more information.                                                                                                                                                                                                                                                                              |
| 160  | You've already requested to follow user.                                                         | Corresponds with HTTP 403. This was a duplicated follow request and a previous request was not yet acknowleged.                                                                                                                                                                                                                                                                          |
| 161  | You are unable to follow more people at this time                                                | Corresponds with HTTP 403. Thrown when a user cannot follow another user due to reaching the limit. This limit is applied to each user individually, independent of the applications they use to access the Twitter platform.                                                                                                                                                            |
| 179  | Sorry, you are not authorized to see this status                                                 | Corresponds with HTTP 403. Thrown when a Tweet cannot be viewed by the authenticating user, usually due to the Tweet’s author having protected their Tweets.                                                                                                                                                                                                                             |
| 185  | User is over daily status update limit                                                           | Corresponds with HTTP 403. Thrown when a Tweet cannot be posted due to the user having no allowance remaining to post. Despite the text in the error message indicating that this error is only thrown when a daily limit is reached, this error will be thrown whenever a posting limitation has been reached. Posting allowances have roaming windows of time of unspecified duration. |
| 186  | Tweet needs to be a bit shorter.                                                                 | Corresponds with HTTP 403. The status text is too long.                                                                                                                                                                                                                                                                                                                                  |
| 187  | Status is a duplicate                                                                            | The status text has already been Tweeted by the authenticated account.                                                                                                                                                                                                                                                                                                                   |
| 195  | Missing or invalid url parameter                                                                 | Corresponds with HTTP 403.  The request needs to have a valid url parameter.                                                                                                                                                                                                                                                                                                             |
| 205  | You are over the limit for spam reports.                                                         | Corresponds with HTTP 403. The account limit for reporting spam has been reached. Try again later.                                                                                                                                                                                                                                                                                       |
| 214  | Owner must allow dms from anyone.                                                                | Corresponsds with HTTP 403.  The user is not set up to have open Direct Messages when trying to set up a welcome message.                                                                                                                                                                                                                                                                |
| 215  | Bad authentication data                                                                          | Corresponds with HTTP 400. The method requires authentication but it was not presented or was wholly invalid.                                                                                                                                                                                                                                                                            |
| 220  | Your credentials do not allow access to this resource.                                           | Corresponds with HTTP 403. The authentication token in use is restricted and cannot access the requested resource.                                                                                                                                                                                                                                                                       |
| 226  | This request looks like it might be automated.                                                   | To protect our users from spam and other malicious activity, we can’t complete this action right now.    We constantly monitor and adjust our filters to block spam and malicious activity on the Twitter platform. These systems are tuned in real-time. If you get this response our systems have flagged the Tweet or Direct Message as possibly fitting this profile.                |
| 231  | User must verify login                                                                           | Returned as a challenge in xAuth when the user has login verification enabled on their account and needs to be directed to twitter.com to generate a temporary password. Note that xAuth is no longer an available option for authentication on the API.                                                                                                                                 |
| 251  | This endpoint has been retired and should not be used.                                           | Corresponds to a HTTP request to a retired URL.                                                                                                                                                                                                                                                                                                                                          |
| 261  | Application cannot perform write actions.                                                        | Corresponds with HTTP 403. Thrown when the app is restricted from POST, PUT, or DELETE actions. Check the information on your app dashboard. You may also file a ticket at https://help.twitter.com/forms/platform.                                                                                                                                                                      |
| 271  | You can’t mute yourself.                                                                         | Corresponds with HTTP 403. The authenticated user account cannot mute itself.                                                                                                                                                                                                                                                                                                            |
| 272  | You are not muting the specified user.                                                           | Corresponds with HTTP 403. The authenticated user account is not muting the account a call is attempting to unmute.                                                                                                                                                                                                                                                                      |
| 323  | Animated GIFs are not allowed when uploading multiple images.                                    | Corresponds with HTTP 400. Only one animated GIF may be attached to a single Tweet.                                                                                                                                                                                                                                                                                                      |
| 324  | The validation of media ids failed.                                                              | Corresponds with HTTP 400. There was a problem with the media ID submitted with the Tweet.                                                                                                                                                                                                                                                                                               |
| 325  | A media id was not found.                                                                        | Corresponds with HTTP 400. The media ID attached to the Tweet was not found.                                                                                                                                                                                                                                                                                                             |
| 326  | To protect our users from spam and other malicious activity, this account is temporarily locked. | Corresponds with HTTP 403. The user should log in to https://twitter.com to unlock their account before the user token can be used.                                                                                                                                                                                                                                                      |
| 327  | You have already retweeted this Tweet.                                                           | Corresponds with HTTP 403. The user cannot retweet the same Tweet more than once.                                                                                                                                                                                                                                                                                                        |
| 349  | You cannot send messages to this user.                                                           | Corresponds with HTTP 403. The sender does not have privileges to Direct Message the recipient.                                                                                                                                                                                                                                                                                          |
| 354  | The text of your direct message is over the max character limit.                                 | Corresponds with HTTP 403. The message size exceeds the number of characters permitted in a Direct Message.                                                                                                                                                                                                                                                                              |
| 355  | Subscription already exists.                                                                     | Corresponds with HTTP 409 Conflict.  Related to Account Activity API request to add a new subscription for an authenticated user.                                                                                                                                                                                                                                                        |
| 385  | You attempted to reply to a Tweet that is deleted or not visible to you.                         | Corresponds with HTTP 403. A reply can only be sent with reference to an existing public Tweet.                                                                                                                                                                                                                                                                                          |
| 386  | The Tweet exceeds the number of allowed attachment types.                                        | Corresponds with HTTP 403. A Tweet is limited to a single attachment resource (media, Quote Tweet, etc.)                                                                                                                                                                                                                                                                                 |
| 407  | The given URL is invalid.                                                                        | Corresponds with HTTP 400. A URL included in the Tweet could not be handled. This may be because a non-ASCII URL could not be converted, or for other reasons.                                                                                                                                                                                                                           |
| 415  | Callback URL not approved for this client application.                                           | Corresponds with HTTP 403. The app callback URLs must be whitelisted via the app details page in the developer portal. Only approved callback URLs may be used by the Twitter app.                                                                                                                                                                                                       |
| 416  | Invalid / suspended application                                                                  | Corresponds with HTTP 401. The app has been suspended and cannot be used with Sign-in with Twitter.                                                                                                                                                                                                                                                                                      |
| 417  | Desktop applications only support the oauth_callback value 'oob'                                 | Corresponds with HTTP 401. The application is attempting to use out-of-band PIN-based OAuth, but a callback URL has been specified in the app settings.                                                                                                                                                                                                                                  |

## Cursoring

The Twitter REST API utilizes a technique called 'cursoring' to paginate large result sets. Cursoring
separates results into pages (the size of which are defined by the `count` request parameter) and
provides a means to move backwards and forwards through these pages.

To retrieve cursored results, you initially pass a `cursor` with a value of `-1` to the endpoint.
By default, an API endpoint that supports cursoring will assume `-1` as `cursor` if you do not provide one.
The response to a cursored request will contain `previous_cursor`, `next_cursor`, `previous_cursor_str`,
and `next_cursor_str`. The `_str` values are provided for languages that cannot support large integers
(e.g. JavaScript).

The `next_cursor` is the cursor you should send to the endpoint to receive the next batch or responses,
and the `previous_cursor` is the cursor that you should send to receive the previous batch. You will
know that you have requested the last available page of results when the API responds with a `next_cursor = 0`.

### PseudoCode

The pseudocode to iterate over all responses from a cursored endpoint is:

```
cursor = -1
api_path = "https://api.twitter.com/1.1/endpoint.json?screen_name=targetUser"

do {
    url_with_cursor = api_path + "&cursor=" + cursor
    response_dictionary = perform_http_get_request_for_url( url_with_cursor )
    cursor = response_dictionary[ 'next_cursor' ]
}
while ( cursor != 0 )
```

## Security

There is a real and present danger of hackers, who aim to spread spam, install malware, and generally
promote mayhem across the web. To that end, this page is to inform you of many threads that exists,
and good practices you can use to help keep your application as safe as possible.

If you've discovered a security issue that directly affects Twitter, please contact
[Twitter Security](https://support.twitter.com/forms/security). If your security issue is legitimate,
there could be compensation through the bug bounty program.

### Threats

The following threats are applicable no matter what your platform.

- __Password Retention__: Do not retain passwords. Instead, opt to use OAuth tokens where possible.
  Encrypt other sensitive data as needed.

- __Input Validation__: Don't assume that your users will provide you with valid, trustworthy data.
  Sanitize all data, checking for sane string lengths, valid file types, etc.

- __Unencrypted Communication (no SSL)__: Twitter provides all REST API methods over SSL. Whenever your
  code might be operating on an untrusted network, please make sure use of SSL for all authenticated
  or sensitive requests. For example, posting a status, requesting recent direct messages, and updating
  profile attributes should all be performed over SSL in a Twitter client. Modern hardware eliminates
  most performace impact for encrypted traffic.

- __Tokens and Secrets__: Twitter APIs rely on OAuth for authorizing each call. When working with an
  access token and token secret, be sure to encrypt them (and other sensitive data) as safely as
  possible. At Twitter, they use bcrypt-ruby as needed.

- __Exposed Debugging Information__: Be sure that you're not exposing sensitive information through debugging
  screens/logs. Some web frameworks make it easy to access debugging information if your application is
  not properly configured.

- __Inadequate Testing__: Ensure that your tests check not just that you can do what should be able to do,
  but that the bad guys can't do what they shouldn't be able to do. Put yourself in an attacker's mindset
  and whip up some evil tests.

- __Not Letting People Help__: Set up `security@yourapplication.com` to go directly to your phone.
  Make it easy for people to contact you about potential security issues with your application. If
  someone does report a security flaw to you, be nice to them; they've just done you a huge favor.
  Thank them for their time and fix the issue promptly. Security is hard, and nobody is perfect. As
  long as you’re fixing the issues that are reported to you, you’re doing right.

  Consider hiring security professionals to do an audit and/or penetration test. You can’t depend
  solely on the kindness of strangers; for every vulnerability that someone was nice enough to report
  to you, there’s ten more that malicious hackers have found. A good security firm will dig deep to
  uncover issues. Look for firms and individual consultants that do more than run a few automated tools.

- __The Law__: If your application is (going to be) handling money, you may be required by law to adhere
  to certain security practices and regulations. Find out what's applicable to you and make sure you're
  up to code.

### Web Application Security

- __Unfiltered Input, Unescaped Output__: FIEO: Filter Input, Escape Output

  Filter anything coming into application: Twitter API data, cookie data, user-supplied form input, URL params, etc.

  Escape all output being sent by your application: SQL sent to DB server, HTML sent to browsers, JSON/XML
  sent to other systems, and commands sent to shell programs

- __Cross-Site Scripting (XSS)__: XSS attacks are, by most measures, the most common form of security
  problem on the web. In short, if an attacker can get their own JavaScript code into your application,
  they can do bad stuff. Anywhere you store and display untrusted, user-supplied data needs to be checked,
  sanitized, and HTML escapted.

- __SQL Injection__: If your application makes use of a database, you need to be aware of SQL injection.
  Again, anywhere you accept input is a potential target for an attacker to break out of their input
  field and into your database. Use database libraries that protect against SQL injection in a systematic
  way.

  The two main approaches to defending against SQL injection are escaping before constructing your
  SQL statement and using parameterized input to create statements. The latter is recommended, as it's
  less prone to programmer error.

- __Lake of Rate Limiting__: CAPTCHAs where appropriate to slow down potential spammers and attackers.

- __Lack of Information about Threats__: If you think there’s an issue with your web application, how do
  you find out for sure? Have critical exceptions and errors emailed to you and keep good logs. You may
  want to put together a dashboard of critical statistics so that you can see at a glance if something is
  going wrong (or staying right).

- __Unencrypted Storage of Credentials__: As aforementioned, for optimal security you should be using OAuth.
  But once you have a token with which to make requests on behalf of a user, where do you put it? Ideally,
  in an encrypted store managed by your operating system. On Mac OS X, this would be the Keychain. In
  the GNOME desktop environment, there’s the Keyring. In the KDE desktop environment, there’s KWallet

## Twitter IDs (snowflake)

Snowflake is a service used to generate unique IDs for objects within Twitter (Tweets, Direct Messages,
Users, Collections, Lists etc.). These IDs are unique 64-bit unsigned integers, which are based on time, instead
of being sequential. The full ID is composed of a timestamp, a worker number, and a sequence number. When
consuming the API using JSON, it is important to always use the field `id_str` instead of `id`. This is
due to the way JavaScript and other languages that consume JSON evaluate large integers. If you come
across a scenario where it doesn't appear that `id` and `id_str` match, it's due to your environment
having already parsed the `id` integer and munging the number in the process.

### The problem

Some programming languages such as JavaScript cannot support numbers with > 53 bits. This can be easily
examined by running a command similar to: `(90071992547409921).toString()` in a browser console or by
running the following JSON snipped through your JSON parser.

```json
{"id": 10765432100123456789, "id_str": "10765432100123456789"}
```

In affected JSON parsers the ID will _not_ be converted successfully and will lose accuracy. In some
parsers there may even be an exception.

### The solution

To allow Javascript and JSON parsers to read the IDs, Twitter objects include a string version of any ID
when responding with JSON. Status, User, Direct Message, Saved Search and other IDs in the Twitter API
are therefore returned as both an integer and a string in JSON responses.

For example, a status object contains an `id` and an `id_str`.

```json
[
  {
    "id": 12738165059,
    "id_str": "12738165059",
    "created_at": "Thu Oct 14 22:20:15 +0000 2010",
    "text": "@themattharris hey how are things?",
    "contributors": [
      {
        "id": 819797,
        "id_str": "819797",
        "screen_name": "episod"
      }
    ],
    ...
  }
]
```

### What developers need to do

Use the string version of id, `id_str`, instead of the integer `id`.

## Counting Characters

Twitter limits Tweet length to a specific number of characters for display (originally and historically,
the well-known 140 characters, although this has been made more flexible over time). This makes the
definition of a “character” and how they are counted central to any Twitter application. This page aims
to provide information on how the Twitter server-side code counts characters for the Tweet length restriction.
The examples on this page will be provided mostly in the Ruby programming language but the concepts should
be applicable to all languages.

For programmers with experience in Unicode processing the short answer to the question is that Tweet
length is measured by the number of codepoints in the NFC normalized version of the text.

### Twitter Character Encoding

All Twitter attributes accept UTF-8 encoded text via the API. All other encodings must be converted
to UTF-8 before sending them to Twitter in order to guarantee that the data is not corrupted.

### Definition of a Character

While Wikipedia has an article for a Character (computing) it’s a very technical and purposely vague definition.
The definition we’re interested in here is not the general definition of a character in computing but
rather the definition of what “character” means when we say “a specific number of characters”.

For many Tweets, all characters are a single byte, and this page is of no use. The number of characters
in a Tweet will effectively be equal to the byte length of the text. If you use anything beyond the
most basic letters, numbers, and punctuation the situation gets more confusing. While many people use
multi-byte Kanji characters to exemplify these issues, Twitter has found that accented vowels cause
the most confusion because English speakers simply expect them to work. Take the following example:
the word “café”. It turns out there are two byte sequences that look exactly the same, but use a
different number of bytes:

| Text | Byte Sequence                 | Description                                               |
|:-----|:------------------------------|:----------------------------------------------------------|
| café | 0x63 0x61 0x66 0xC3 0xA9      | Using the “é” character, called the “composed character”. |
| café | 0x63 0x61 0x66 0x65 0xCC 0x81 | Using the combining diacritical, which overlaps the “e”   |

### Counting Characters

The “café” issue mentioned above raises the question of how you count the characters in the Tweet string
“café”. To the human eye the length is clearly four characters. Depending on how the data is represented
this could be either five or six UTF-8 bytes. Twitter does not want to penalize a user for the fact we
use UTF-8 or for the fact that the API client in question used the longer representation. Therefore, Twitter
does count “café” as four characters, no matter which representation is sent.

Nearly all user input methods automatically convert the longer combining mark version into the composed
version but the Twitter API cannot count on that. Even if we did ignore that the byte length of the “é”
character is two bytes rather than the one you would expect. Below there is some more specific information
on how to get that information out of Ruby/Rails but for now I’ll cover the general concepts that
should be available in any language.

The Unicode Standard covers much more that a listing of characters with numbers associated. Unicode does
provide such a list of “codepoints” (`more info <http://www.unicode.org/charts/>`__), which is the
U+XXXX notation you sometimes see. The Unicode Standard also provides several different ways to encode
those codepoints (UTF-8 and UTF-16 are examples, but there are others). The Unicode standard also
provides some detailed information on how to deal with character issues such as Sorting, Regular
Expressions and of importance to this issue, Normalization.

#### Unicode Normalization

The Unicode Standard provides information on several different kinds of normalization, Canonical and
Compatibility. There is a full description of the different options in the Unicode Standard Annex #15,
the report on normalization. The normalization report is 32 pages and covers the issue in great detail.
Reproducing the entire report here would be of very little use so instead we’ll focus on what normalization Twitter is using.

Twitter counts the length of a Tweet using the Normalization Form C (NFC) version of the text. This
type of normalization favors the use of a fully combined character (0xC3 0xA9 from the café example)
over the long-form version (0x65 0xCC 0x81). Twitter also counts the number of codepoints in the text
rather than UTF-8 bytes. The 0xC3 0xA9 from the café example is one codepoint (U+00E9) that is encoded
as two bytes in UTF-8, whereas 0x65 0xCC 0x81 is two codepoints encoded as three bytes.

## t.co Links

Tens of millions of links are posted and Tweeted on Twitter each day. Wrapping these shared links
helps Twitter protect users from malicious content while offering useful insights on engagement. All
links submitted within Tweets and Direct Messages, regardless of length, will eventually be wrapped
with the t.co shortener.

### How Twitter Wraps URLs

Twitter uses the [twitter-text](https://github.com/twitter/twitter-text) library to parse status bodies
and extract the entities, including URLs, that are returned with Tweet responses.

The general rule of thumb as to which URLs will be wrapped in t.co:
- Twitter wraps links without protocols in addition to fully qualified HTTP or HTTPs URLs
- Trailing periods and commas, unless URL-encoded, are ignored and not considered part of a URL.
  Two URLs connected by a period or comma will not be parsed.
- The URL should not include login information. ( https://user:password@twitter.com/ will not be wrapped)

### Best Practices

#### Linking to Content within Tweets

We recommend the following when rendering links within Tweets:

- Use the “url” value of a URL entity for the href attribute of an anchor tag.

- Use the “expanded_url” value of a URL entity for the title attribute of an anchor tag, so that
  when users hover over the link they see the fully expanded URL.

- Use the “display_url” value of a URL entity for the displayed text between opening & closing anchor
  tags. If you prefer not to display the truncated format, use the full “expanded_url” instead.

- Non-HTML based environments obviously require more creative approaches, but the spirit of t.co
  should be kept in mind. Display as appropriate for your environment.

- When a disruption in the ability to resolve t.co links occurs, it’s recommended to display t.co
  links unless you already have a resolution for the URL. t.co’s redirection and malware protection
  capabilities are fully intact when these disruptions occur.

#### Working with Tweet input text

- Request GET help / configuration once daily in your application and cache the “short_url_length_https”
  (the maximum length for HTTPS-based t.co links) for 24 hours.

- If you offer a “remaining character count” feature in a Tweet entry input field, consider URLs
  detected in user input with twitter-text as being equal in length to the current “short_url_length_https” value.

### When are tweets wrapped?

Links will be wrapped when Twitter receives a Tweet using POST statuses/update or a direct message using
POST direct_messages/new. The wrapped link will be contained in the response to a successful request.
There is no need to make any extra API calls.

### Will t.co wrapped links always be the same length?

The maximum length of t.co URLs changes over time. Issue a request daily to GET help/configuration and
examine the fields short_url_length and short_url_length_https to determine the current maximum length
of wrapped URLs. These values do not change often.

## Accounts and Users

### Subscribe to Account Activity

#### Account Activity API Overview

The account activity API provides you the ability to subscribe to realtime activities related to a user
account via webhooks. This means that you can receive realtime Tweets, Direct Messages, and other account
events from one or more of your owned or subscribed accounts through a single connection.

You will receive all related activities below for each user subscription on your webhook registration:
- Tweets (by user)
- Tweet deletes (by user)
- @mentions (of user)
- Replies (to or from user)
- Retweets (by user or of user)
- Quote Tweets (by user or of user)
- Retweets of Quoted Tweets (by user or of user)
- Likes (by user or of user)
- Follows (by user or of user)
- Unfollows (by user)
- Blocks (by user)
- Unblocks (by user)
- Mutes (by user)
- Unmutes (by user)
- Direct Messages sent (by user)
- Direct Messages received (by user)
- Typing indicators (to user)
- Read receipts (to user)
- Subscription revokes (by user)

### Manage account settings and profile

With proper authorization your application can read and update a user's account and profile settings.
Note: not all settings are exposed via the API.

### Mute, block, and report users

Your app can mute, block, and report users for the authenticated user.

### Follow, search, and get users

The following API endpoints can be used to programmatically follow users, search for users, and
get user information.

Friends and followers:

- GET followers/ids
- GET followers/list
- GET friends/ids
- GET friends/list
- GET friendships/incoming
- GET friendships/lookup
- GET friendships/no_retweets/ids
- GET friendships/outgoing
- GET friendships/show

POST friendships:

- POST friendships/create
- POST friendships/destroy
- POST friendships/update

Get user info:

- GET users/lookup
- GET users/search
- GET users/show
- GET users/suggestions
- GET users/suggestions/:slug
- GET users/suggestions/:slug/members

To avoid confusion around the term "friends" and "followers" with respect to API endpoints, below
is a definition of each:

__Friends__: we refer to "friends" as the Twitter users that a specific user follows (e.g., following).
Therefore, the `GET friends/ids` endpoint returns a collection of user IDs that the specified user follows.

__Followers__: refers to the Twitter users that follow a specific user. Therefore, making a request to
the `GET followers/ids` endpoint returns a collection of user IDs for every user following the specified
user.

### Create and manage lists

A list is a curated group of Twitter accounts. You can create your own lists or subscribe to lists
created by others for the authenticated user. Viewing a list timeline will show you a stream of Tweets
from only the accounts on that list.

### User profile images and banners

#### Profile Images

Profile images (also known as avatars) are an important component of a Twitter account's expression
of identity.

##### Alternate image sizes

Obtain a user’s most recent profile image, along with the other components comprising their identity
on Twitter, from `GET users/show`. The user object contains the `profile_image_url` and `profile_image_url_https`
fields. These fields will contain the resized “normal” variant of the user’s uploaded image. This “normal”
variant is typically 48px by 48px.

By modifying the URL, it is possible to retrieve other variant sizings such as "bigger", "mini", and "original".
Consult the table below for more examples:

| Variant  | Dimensions | Example                                                                                                                                                        | URL |
|:---------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------|:----|
| normal   | 48x48      | http://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_normal.png https://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_normal.png |     |
| bigger   | 73x73      | http://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_bigger.png https://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_bigger.png |     |
| mini     | 24x24      | http://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_mini.png https://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_mini.png     |     |
| original | original   | http://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3.png https://pbs.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3.png               |     |

##### Default profile images

Some users may not have uploaded a profile image. Users who have not uploaded a profile image can be
identified by the `default_profile_image` field of their user object having the `true` value.

The `profile_image_url` and `profile_image_url_https` URLs provided for users in this case will indicate
Twitter’s default profile photo, which is Thttps://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png.

##### Outdate profile images

If a 403 or 404 error is returned when trying to access a profile image, refresh the user object using
`GET users/show` to retrieve the most recent `profile_image_url` or `profile_image_url_https`. The
URL may have changed, whuch happens when the user updates their profile image

#### Profile Banners

Profile banners allow users to further customize the expressiveness of their profiles. Use `POST
account/update_profile_banner` to upload a profile banner on behalf of a user.

Profile banners come in a variety of display-enhanced sizes. The variant sizes are available through
a request to `GET users/profile_banner` or by modifying the final path component of the `profile_banner_url`
found in a user object according to the table below.

The following sizes are available:

| Dimensions | Example URL                                                       |
|:-----------|:------------------------------------------------------------------|
| 1500x500   | https://pbs.twimg.com/profile_banners/6253282/1431474710/1500x500 |
| 600x200    | https://pbs.twimg.com/profile_banners/6253282/1431474710/600x200  |
| 300x100    | https://pbs.twimg.com/profile_banners/6253282/1431474710/300x100  |

The following sizes are available for certain screen types:

| Screen Type   | Dimensions | Example URL                                                            |
|:--------------|:-----------|:-----------------------------------------------------------------------|
| web           | 520x260    | https://pbs.twimg.com/profile_banners/6253282/1431474710/web           |
| web_retina    | 1040x520   | https://pbs.twimg.com/profile_banners/6253282/1431474710/web_retina    |
| ipad          | 626x313    | https://pbs.twimg.com/profile_banners/6253282/1431474710/ipad          |
| ipad_retina   | 1252x626   | https://pbs.twimg.com/profile_banners/6253282/1431474710/ipad_retina   |
| mobile        | 320x160    | https://pbs.twimg.com/profile_banners/6253282/1431474710/mobile        |
| mobile_retina | 640x320    | https://pbs.twimg.com/profile_banners/6253282/1431474710/mobile_retina |

## Tweets

### Post, retrieve, and engage with Tweets

The following API endpoints can be used to programmatically create, retrieve, and delete Tweets, Retweets,
and Likes.

Tweets:
- POST statuses/update
- POST statuses/destroy/:id
- GET statuses/show/:id
- GET statuses/oembed
- GET statuses/lookup

Retweets:
- POST statuses/retweet/:id
- POST statuses/unretweet/:id
- GET statuses/retweets/:id
- GET statuses/retweets_of_me
- GET statuses/retweeters/ids

Likes (formerly favorites)
- POST favorites/create/:id
- POST favorites/destroy/:id
- GET favorites/list

Some terminology:

__Tweet/Status__: when a status message is shared on Twitter.

__Retweet__: when a Tweet is re-shared by another specific user.

__Like__: when a Tweet recieves a 'heart' from a specific user, formerly known as a facorite or 'star'

### Get Tweet Timelines

A timeline is simply a list, or an aggregated stream of Tweets. The Twitter API has several endpoints
that return a timeline of Tweet data.

| API endpoint                   | Description                                                                                                      |
|:-------------------------------|:-----------------------------------------------------------------------------------------------------------------|
| GET statuses/home_timeline     | Returns a collection of the most recent Tweets posted by the authenticating user and the users they follow.      |
| GET statuses/user_timeline     | Returns a collection of the most recent Tweets posted by the indicated by the screen_name or user_id parameters. |
| GET statuses/mentions_timeline | Returns the 20 most recent mentions (Tweets containing a users’s @handle) for the authenticating user.           |

### Curate a collection of Tweets

A collection is an editable group of tweets hand-selected by a Twitter user or programmatically managed
via collection APIs. Each collection is public and has its own page on twitter.com, making it easy to
share and embed in your website and apps.

Each collection has a public URL on Twitter.com. Share a collection with others by including it in a Tweet,
email, or other share method. Example: https://twitter.com/NYTNow/timelines/576828964162965504

Collections can also be embeded onto your website.

### Optimize Tweets with Cards

With Twitter cards, you can attach rich photos, videos, and media experiences to Tweets, helping to drive
traffic to your website.

### Search Tweets

The Twitter API platform offers three tiers of search APIs:

- __Standard__: This search API searches against a sampling of recent Tweets published in the past 7 days.
  Part of the 'public' set of APIs.

- __Premium__: Free and paid access to either the last 30 days of Tweets or access to Tweets from as early
  as 2006. Built on the reliability and full-fidelity of enterprise data APIs, provides the opportunity to
  upgrade your access as your app and business grow.

- __Enterprise__: Paid (and managed) access to either the last 30 days of Tweets or access to Tweets
  from as early as 2006.  Provides full-fidelity data, direct account management support, and dedicated
  technical support to help with integration strategy.

### Filter Realtime Tweets

The Twitter API platform offers two options for streaming realtime Tweets. Each option offers a varying
number of filters and filtering capabilities.

| API Category    | Number of filters | Filtering operators                                           | Rule management    |                                                                                                        |
|:----------------|:------------------|:--------------------------------------------------------------|:-------------------|:-------------------------------------------------------------------------------------------------------|
| statuses/filter | Standard          | 400 keywords, 5,000 userids and 25 location boxes             | Standard operators | One filter rule on one allowed connection, disconnection required to adjust rule                       |
| PowerTrack      | Enterprise        | Up to 250,000 filters per stream, up to 2,048 characters each | Premium operators  | Thousands of rules on a single connection, no disconnection needed to add/remove rules using Rules API |

### Sample Realtime Tweets

`GET statuses/sample`

Returns a small random sample of all public statuses. The Tweets returned by the default access level
are the same, so if two different clients connect to this endpoint, they will see the same Tweets.

### Get batch historical Tweets

#### Historical PowerTrack overview

Historical PowerTrack provides access to the entire historical archive of public Twitter data – back to
the first Tweet – using the same rule-based-filtering system as the realtime PowerTrack stream to deliver
complete coverage of historical Twitter data. As with Twitter Search APIs, user profile metadata are
updated to match values at the time of query, rather than matching values in place at the time of
the (historical) Tweet.

Accessing data through Historical PowerTrack is accomplished through creating historical ‘jobs’ – a
set of PowerTrack filtering rules and a historical time frame for which you would like to retrieve
matching data from the Twitter archive. These jobs can be created and managed through the Historical
PowerTrack API.

### Rules and Filtering

### Data enrichments

Premium enrichments are additive metadata included in the response payload of some of the data APIs.
They are available in paid subscription plans only.

| Enrichment                 | Description                                                                                                                                                             |
|:---------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Expanded and Enhanced URLs | Automatically expands shortened URLs (e.g., bitly) that are included in the body of a Tweet and provides HTML Title and Description metadata from the destination page. |
| Matching rules object      | Indicates which rule or rules matched the Tweets received. The object returns rule tag and rule ID in the response object.                                              |
| Poll metadata              | Notes the presence of the poll in a Tweet, includes the list of poll choices, and includes both the poll duration and expiration time.                                  |
| Profile geo                | Derived user profile location data including the [longitude, latitude] coordinates (where possible) and related place metadata.                                         |

### Tweet Objects

All Twitter APIs that return Tweets provide that data encoded using JavaScript Object Notation (JSON).
JSON is based on key-value pairs, with named attributes and associated values. These attributes, and
their state are used to describe objects.

At Twitter we serve many objects as JSON, including _Tweets and Users_, These obejcts all encapsulate
core attributes that describe the object. Each Tweet has an auther, a message, a unique ID, a timestamp of
when it was posted, and sometimes geo metadata shared by the user. Each User has a Twitter name, an ID,
a number of followers, and most often an account bio.

With each Tweet we also generate "entity" objects, which are arrays of common Tweet contents such as
hashtags, mentions, media, and links. If there are links, the JSON payload can also provide metadata
such as the fully unwound URL and the webpage’s title and description.

So, in addition to the text content itself, a Tweet can have over 150 attributes associated with it.

The following JSON illustrates the structure for these objects and some of their attributes:

```json
{
  "created_at": "Thu Apr 06 15:24:15 +0000 2017",
  "id_str": "850006245121695744",
  "text": "1\/ Today we\u2019re sharing our vision for the future of the Twitter API platform!\nhttps:\/\/t.co\/XweGngmxlP",
  "user": {
    "id": 2244994945,
    "name": "Twitter Dev",
    "screen_name": "TwitterDev",
    "location": "Internet",
    "url": "https:\/\/dev.twitter.com\/",
    "description": "Your official source for Twitter Platform news, updates & events. Need technical help? Visit https:\/\/twittercommunity.com\/ \u2328\ufe0f #TapIntoTwitter"
  },
  "place": {
  },
  "entities": {
    "hashtags": [
    ],
    "urls": [
      {
        "url": "https:\/\/t.co\/XweGngmxlP",
        "unwound": {
          "url": "https:\/\/cards.twitter.com\/cards\/18ce53wgo4h\/3xo1c",
          "title": "Building the Future of the Twitter API Platform"
        }
      }
    ],
    "user_mentions": [
    ]
  }
}
```

#### Tweet Objects

When ingesting Tweet data the main object is the Tweet Object, which is a parent object to several
child objects.

```json
{
"created_at": "Thu May 10 15:24:15 +0000 2018",
 "id_str": "850006245121695744",
 "text": "Here is the Tweet message.",
 "user": {              // User object that describes who authored the Tweet
 },
 "place": {             // If the tweet is geo-tagged, there will be a place object included
 },
 "entities": {          // Encapsulates arrays of hashtags, user mentions, URLs, cashtags, and native media
 },
 "extended_entities": { // If the Tweet has any 'attached' or 'native' media (photos, video, animated GIF), they are here
 }
}
```

#### Extended Tweets

JSON that describes Extended Tweets was introduced when 280-character Tweets were launched in November 2017.
Tweet JSON was extended to encapsulate these longer messages, while not breaking the thousands of apps parsing
these fundamental Twitter objects. To provide full backward compatibility, the original 140-character 'text'
field, and the entity objects parsed from that, were retained. In the case of Tweets longer than 140
characters, this root-level 'text' field would become truncated and thus incomplete. Since the root-level
'entities' objects contain arrays of key metadata parsed from the 'text' message, such as included hashtags
and links, these collections would be incomplete. For example, if a Tweet message was 200 characters long,
with a hashtag included at the end, the legacy root-level 'entities.hashtags' array would not include it.

A new 'extended_tweet' field was introduced to hold the longer Tweet messages and complete entity metadata.
The "extended_tweet" object provides the "full_text" field that contains the complete, untruncated Tweet
message when longer than 140 characters. The "extended_tweet" object also contains an "entities" object
with complete arrays of hashtags, links, mentions, etc.

Extended Tweets are identified with a root-level "truncated" boolean. When true ("truncated": true),
the "extended_tweet" fields should be parsed instead of the root-level fields.

Note in the JSON example below that the root-level "text" field is truncated and the root-level "entities.hashtags"
array is empty even though the Tweet message includes three hashtags. Since this is an Extended Tweet,
the "truncated" field is set to true, and the "extended_tweet" object provides complete "full_text" and "entities" Tweet metadata.

```json
{
    "created_at": "Thu May 10 17:41:57 +0000 2018",
    "id_str": "994633657141813248",
    "text": "Just another Extended Tweet with more than 140 characters, generated as a documentation example, showing that [\"tru… https://t.co/U7Se4NM7Eu",
    "display_text_range": [0, 140],
    "truncated": true,
    "user": {
        "id_str": "944480690",
        "screen_name": "FloodSocial"
    },
    "extended_tweet": {
        "full_text": "Just another Extended Tweet with more than 140 characters, generated as a documentation example, showing that [\"truncated\": true] and the presence of an \"extended_tweet\" object with complete text and \"entities\" #documentation #parsingJSON #GeoTagged https://t.co/e9yhQTJSIA",
        "display_text_range": [0, 249],
        "entities": {
            "hashtags": [{
                "text": "documentation",
                "indices": [211, 225]
            }, {
                "text": "parsingJSON",
                "indices": [226, 238]
            }, {
                "text": "GeoTagged",
                "indices": [239, 249]
            }]
        }

    },
    "entities": {
        "hashtags": []
    }
}
```

#### Retweets

If you are working with Retweet or Quote Tweet objects, then that JSON payload will contain multiple
Tweet objects, and each Tweet object will contain its own User object. The root-level object will
contain information on the type of action taken, i.e. whether it is a Retweet or a Quote Tweet, and
will contain an object that describes the 'original' Tweet being shared.

Retweets always contain two Tweet objects. The 'original' Tweet being Retweeted is provided in a
"retweeted_status" object. The root-level object encapsulates the Retweet itself, including a User
object for the account taking the Retweet action and the time of the Retweet. Retweeting is an
action to share a Tweet with your followers, and no other new content can be added. Also, a (new)
location cannot be provided with a Retweet. While the 'original' Tweet may have geo-tagged, the
Retweet "geo" and "place" objects will always be null.

Even before the introduction of Extended Tweets, the root-level "entities" object was in some cases
truncated and incomplete due to the "RT @username " string being appended to Tweet message being
Retweeted.  Note that if a Retweet gets Retweeted, the "retweet_status" will still point to the original
Tweet, meaning the intermediate Retweet is not included. Similar behavior is seen when using twitter.com
to 'display' a Retweet. If you copy the unique Tweet ID assigned to the Retweet 'action', the original
Tweet is displayed.

Below is an example structure for a Retweet. Again, when parsing Retweets, it is key to parse the
"retweeted_status" object for complete (original) Tweet message and entity metadata.

```json
{
  "tweet": {
    "text": "RT @author original message"
    "user": {
          "screen_name": "Retweeter"
    },
    "retweeted_status": {
      "text": "original message".
        "user": {
            "screen_name": "OriginalTweeter"
        },
        "place": {
        },
        "entities": {
        },
        "extended_entities": {
        }
    },
    "entities": {
    },
    "extended_entities": {
    }
  }
}
```

#### Quote Tweets

Quote Tweets are much like Retweets except that they include a new Tweet message. These new messages
can contain their own set of hashtags, links, and other "entities" metadata. Quote Tweets can also
include location information shared by the user posting the Quote Tweet, along with media such as
GIFs, videos, and photos.

Quote Tweets will contain at least two Tweet objects, and in some cases, three. The Tweet being Quoted,
which itself can be a Quoted Tweet, is provided in a "quoted_status" object. The root-level object
encapsulates the Quote Tweet itself, including a User object for the account taking the sharing action
and the time of the Quote Tweet.

Note that Quote Tweets can now have photos, GIFs, or videos, added to them using the 'post Tweet'
user-interface. When links to externally hosted media are included in the Quote Tweet message, the
root-level "entities.urls" will describe those. Media attached to Quote Tweets will appear in the
root-level "extended_entities" metadata.

When Quote Tweets were first launched, a shortened link (t.co URL) was appended to the 'original'
Tweet message and provided in the root-level "text" field. In addition, metadata for that t.co URL
was included in the root-level 'entities.urls' array.

Below is an example structure for a Quote Tweet using this original formatting. Note that the root-level
"text" attribute is based on the quoted Tweet's message, plus a Twitter shortened URL to the quoted Tweet.

```json
{
  "text": "My added comments to this Tweet ---> https:\/\/t.co\/LinkToTweet",
  "user": {
    "screen_name": "TweetQuoter"
  },
  "quoted_status": {
    "text": "original message",
    "user": {
      "screen_name": "OriginalTweeter"
    },
    "place": {
    },
    "entities": {
    },
    "extended_entities": {
    }
  },
  "quoted_status_permalink": {
     "url": "https:\/\/t.co\/LinkToTweet",
     "expanded": "https:\/\/twitter.com\/OriginalTweeter\/status\/994281226797137920",
     "display": "twitter.com\/OriginalTweeter\/status\/994281226797137920"
  },
  "place": {
  },
  "entities": {
  },
  "extended_entities": {
  }
}
```

#### Data dictionaries

Whatever your Twitter use case, understanding what these JSON-encoded Tweet objects and attributes represent
is critical to successfully finding your data signals of interest. To help in that effort, there are a
set of Data Dictionaries for these fundamental Twitter objects.

Data Dictionary URLs:
[Tweet Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object),
[User Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object),
[Entity Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object),
[Extended Entity Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object),
[Places Object](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects)

#### Parsing Best Practices

- Twitter JSON is encoded using UTF-8 characters.

- Parsers should tolerate variance in the ordering of fields with ease. It should be assumed that Tweet
  JSON is served as an unordered hash of data.

- Parsers should tolerate the addition of 'new' fields. The Twitter platform has continually evolved
  since 2006, so there is a long history of new metadata being added to Tweets.

- JSON parsers must be tolerant of ‘missing’ fields, since not all fields appear in all contexts.

- It is generally safe to consider a nulled field, an empty set, and the absence of a field as the same thing.

### Tweet Compliance

One of Twitter's core values is to defend and respect the user's voice. This includes respecting their
expectations and intent when they delete or modify the content they choose to share on Twitter.
Twitter believes that this is critically important to the long term health of its platform.

Twitter puts controls in the hands of its users, giving individuals the ability to control their own
Twitter experience. Twitter believes that business consumers that receive Twitter data have a responsibility
to honor the expectations and intent of end users.

For more information on the types of compliance events that are possible on the Twitter platform,
reference our article,
[Honoring User Intent on Twitter](https://developer.twitter.com/en/docs/tweets/compliance/guides/honoring-user-intent).

Any developer or company consuming Twitter data via an API holds an obligation to use all reasonable
efforts to honor changes to user content. This obligation extends to user events such as deletions,
modifications, and changes to sharing options (e.g., content becoming protected or withheld). Please
reference the specific language in the [Developer Policy](https://developer.twitter.com/en/developer-terms/policy.html)
and/or your Twitter Data Agreement to understand how this obligation affects your use of Twitter data.

### Tweet Updates

#### Rendering Modes

There are _two modes_ for rendering Tweet JSON objects to API clients: __compatibility__ mode and __extended__
mode. Compatibility mode is the default mode for the standard REST and Streaming APIs and enterprise products,
and is designed to not break existing clients.

REST API clients may opt into the extended mode via a request parameter.

In the future, an additional announcement will be made when the time is right to make a change for the
rendering mode to default to extended mode.

#### Extended mode JSON Rendering

In extended mode, the following will be true both for Classic Tweets and Extended Tweets:

1. The text field is no longer included; instead, the payload will contain a field named full_text,
   which contains the entire untruncated Tweet text.

2. The payload shall contain a field named display_text_range, which is an array of two unicode code
   point indices, identifying the inclusive start and exclusive end of the displayable content of the tweet.

3. The truncated field will be set to false.

4. The entity fields will contain all entities, both hidden and displayable.

#### Consumption

Any REST API endpoints that return Tweets will accept a new `tweet_mode` request parameter.

Valid request values are `compat` and `extended`, which give compatibility mode and extended mode, respectively.

The default mode (if no parameter is provided) is compatibility mode, to support older clients and display methods.

Tweets rendered in compatibility mode via the standard REST API will not contain the extended_tweet field.
REST API clients that wish to get the full text can instead opt into extended mode.

#### Sample Tweet with 280 Characters

```json
{
  "created_at": "Tue Sep 26 21:00:22 +0000 2017",
  "id": 912783930431905797,
  "id_str": "912783930431905797",
  "text": "Can’t fit your Tweet into 140 characters? 🤔\n\nWe’re trying something new with a small group, and increasing the char… https://t.co/y1rJlHsVB5",
  "source": "Twitter Web Client",
  "truncated": true,
  "in_reply_to_status_id": null,
  "in_reply_to_status_id_str": null,
  "in_reply_to_user_id": null,
  "in_reply_to_user_id_str": null,
  "in_reply_to_screen_name": null,
  "user": {
    "id": 783214,
    "id_str": "783214",
    "name": "Twitter",
    "screen_name": "Twitter",
    "location": "Everywhere",
    "url": "https://about.twitter.com/",
    "description": "What’s happening?!",
    "translator_type": "null",
    "protected": false,
    "verified": true,
    "followers_count": 56119543,
    "friends_count": 140,
    "listed_count": 91049,
    "favourites_count": 5911,
    "statuses_count": 10132,
    "created_at": "Tue Feb 20 14:35:54 +0000 2007",
    "utc_offset": null,
    "time_zone": null,
    "geo_enabled": true,
    "lang": "en",
    "contributors_enabled": false,
    "is_translator": false,
    "profile_background_color": "null",
    "profile_background_image_url": "null",
    "profile_background_image_url_https": "null",
    "profile_background_tile": null,
    "profile_link_color": "null",
    "profile_sidebar_border_color": "null",
    "profile_sidebar_fill_color": "null",
    "profile_text_color": "null",
    "profile_use_background_image": null,
    "profile_image_url": "null",
    "profile_image_url_https": "https://pbs.twimg.com/profile_images/1111729635610382336/_65QFl7B_normal.png",
    "profile_banner_url": "https://pbs.twimg.com/profile_banners/783214/1556918042",
    "default_profile": false,
    "default_profile_image": false,
    "following": null,
    "follow_request_sent": null,
    "notifications": null
  },
  "geo": null,
  "coordinates": null,
  "place": null,
  "contributors": null,
  "is_quote_status": false,
  "extended_tweet": {
    "full_text": "Can’t fit your Tweet into 140 characters? 🤔\n\nWe’re trying something new with a small group, and increasing the character limit to 280! Excited about the possibilities? Read our blog to find out how it all adds up. 👇\nhttps://t.co/C6hjsB9nbL",
    "display_text_range": [
      0,
      239
    ],
    "entities": {
      "hashtags": [

      ],
      "urls": [
        {
          "url": "https://t.co/C6hjsB9nbL",
          "expanded_url": "https://cards.twitter.com/cards/gsby/4ubsj",
          "display_url": "cards.twitter.com/cards/gsby/4ub…",
          "unwound": {
            "url": "https://cards.twitter.com/cards/gsby/4ubsj",
            "status": 200,
            "title": "Giving you more characters to express yourself",
            "description": null
          },
          "indices": [
            216,
            239
          ]
        }
      ],
      "user_mentions": [

      ],
      "symbols": [

      ]
    }
  },
  "quote_count": 46955,
  "reply_count": 17263,
  "retweet_count": 50214,
  "favorite_count": 89601,
  "entities": {
    "hashtags": [

    ],
    "urls": [
      {
        "url": "https://t.co/y1rJlHsVB5",
        "expanded_url": "https://twitter.com/i/web/status/912783930431905797",
        "display_url": "twitter.com/i/web/status/9…",
        "indices": [
          117,
          140
        ]
      }
    ],
    "user_mentions": [

    ],
    "symbols": [

    ]
  },
  "favorited": false,
  "retweeted": false,
  "possibly_sensitive": false,
  "filter_level": "low",
  "lang": "en",
  "matching_rules": [
    {
      "tag": null
    }
  ]
}
```

## Direct Messages

### Sending and Receiving Events

To send a new Direct Message use `POST direct_messages/events/new`. You may also attach Quick Replies
or media.

You can retrieve Direct Messages from up to the past 30 days with `GET direct_messages/events/list`

### Welcome Messages

Welcome Messages provide the ability to display a message to people who are entering a Direct Message
conversation. Welcome messages can be customized for different referral paths.  For example, users
who click on Direct Message links in a Tweet or if a user enters a Direct Message view for the first
time with no prior context. Welcome Messages can contain any content that a Direct Message would,
including media, Quick Replies, and more.

### Message Attachments

Media may be attached and retrieved for Direct Messages through authenticated calls with app-user
authorization. Direct Messages with an image will contain a media object with relevant details.

### Quick Replies

Messages published with `POST direct_messages/events/new` (message_create) can have an attached
Quick Reply to request stuctured input from a user.

### Buttons

Buttons enable developers to add up to three call-to-action (CTA) buttons to any Direct Message or
Welcome Message. These buttons can be used to open any URL from the Direct Message compose view.
The text labels displayed on the buttons can be fully customized.

### Typing Indicator and Read Receipts

Send typing indicators or read receipts that are displayed in the Direct Message conversation view to let users
know that their messages are being processed.

### Conversation Management

When creating an app to manage Direct Messages, having context of where the user came from or what
application was responsible for sending the message can be important information for managing the conversation.
Having this context can help improve analytics or determine the next action your app should take.

### Custom Profiles

Custom profiles allow a Direct Message author to present a different identity than that of the Twitter account
being used. For example, brands may want customer service agents positing under a single Twitter account
to use their own name and photo. A custom profile may alos be used to attach a unique identity to a message
authored by an automated application or bot so that users clearly understand they are talking to a bot.

### Customer Feedback Cards

Collecting structured feedback about customer interactions is a useful part of the customer service
experience, providing quantitative measures of service quality and effectiveness that benefits both
people and businesses. By asking for feedback in context and shortly after the interaction is complete,
people are more likely to provide a response and more likely to provide feedback that reflects their
interaction. This feature supports the programmatic creation and delivery of feedback prompts that
allow someone to submit responses to feedback surveys after a conversation in Direct Messages.

## Twitter for Websites

### Overview

Twitter for websites is a suite of tools bringing Twitter content and functionality to your webpages
and apps, enabling the Twitter audience to share your content, and follow your Twitter accounts.

### Embdedded Tweets

Embedded Tweets bring your pick of content from Twitter into your website articles. An embedded Tweet
includes photos, video, and cards created for display on Twitter, and can even stream live video
from Periscope. All aspects of Twitter's display requirements are handled for you by using the tools:
author attribution, Tweet actions, hashtags, mentions, and other key components of the Twitter experience.

An embedded Tweet consists of two parts: An HTML snippet hosted in your web page, and the Twitter
for Websites JavaScript to transform that code into a fully-rendered Tweet. You can copy embedded Tweet
markup generated from the Tweet menu on Twitter.com or TweetDeck, paste a URL into a supporting CMS,
or add a Tweet to the page programmatically using a JavaScript factory function.

Special tags may be used to modify the embedded tweet display in different ways.

### Embedded Timelines

Embedded timelines are an easy way to embed Tweets on your website in a compact, linear view. Display
the latest Tweets from a Twitter account, lists, or curated collections.

### Moments

An embedded Moment displays Tweets with imags, video and Cards, as curated by the Moment's creator.
Moments are optimized to fill large displays and responsively, automatically scale down for sidebars
and mobile devices.

### Tweet Button

The Tweet button is a small button displayed on your website to help viewers easily share your content
on Twitter. A Tweet button consists of two parts: a link to the Tweet composer on Twitter.com and
Twitter for Websites JavaScript to enhance the link with the easily recognizable Tweet button.

### Follow Button

The Follow button is a small button displayed on your websites to help users easily follow a Twitter account.
A Follow button consists of two parts: a link to follow web intent page on Twitter.com and the Tiwtter for
Websites JavaScript to transform the link into Twitter's Follow button.

### Direct Messages

The Message button is a small button to help customers easily send a Direct Message to you on Twitter.
Allow your customers to contact you to ask questions and get support from right on your website.

A message button consists of two parts: a link to the direct message composer on Twitter.com, and the
Twitter for Websites JavaScript to enhance the link with the recognizable Message button.

### Sign in with Twitter

Place a button on your site or application which allows Twitter users to enjoy the benefits of a registered
account in as little as one click. Works on websites, iOS, mobile, and desktop applications.

Features:

- __Ease of use__ - A new visitor to your site only has to click two buttons in order to sign in for the first time.
- __Twitter integration__ - The Sign in with Twitter flow can grant authorization to use Twitter APIs on your users behalf.
- __OAuth based__ - A wealth of client libraries and example code are compatible with the Sign in with Twitter API.

Available for

- __Browsers__: If your users can access a browser, you can integrate with Sign in with Twitter. Learn about the browser sign in flow.
- __Mobile devices__: Any web-connected mobile device can take advantage of Sign in with Twitter. Learn about the mobile sign in flow.

### Periscope

The Periscope card brings live video from Periscope into article or website. Broadcasts can be shared on
Twitter and embedded as Tweets or in timelines. Broadcasters can use the On Air Button to show the world
when you're live, right inside your website.

### Web Intents

Web Intents provide popup-optimized flows for working with Tweets & Twitter Users: Tweet, Reply, Retweet,
Like, and Follow. They make it possible for users to interact with Twitter content in the context of your site,
without leaving the page or having to authorize a new app just for the interaction. Web intets are mobile
web friendly, include native app handlers on iOS and Android when the Twitter app is installed, and are easy
to implement.

### Webpage Properties

### JavaScript API

### Supported Languages and Browsers

### Privacy

## Labs

### Overview

Twitter Developer Labs is a program that invites our developer community to partner with us in shaping
the next generation of our API. Labs releases allow developers to test and share feedback on previews
of new API products and features. The endpoints, documentation, and resources for Labs will be updated
as we incorporate feedback from the developer community.

### Tweets and Users

### Tweet Metrics

### Filtered Stream

## Developer Utilities

### Get App Rate Limit Status

Every application is allocated a set of rate limits. This endpoint enables a subset of the the current
rate limits for read-only (GET) operations to be programmatically queried.

### Get Twitter Configuration Details

A number of Twitter API parameters and defaults are fixed, including twitter.com slugs which are not
usernames, maximum photo resolutions, the length of t.co shortened URLs, and more. Applications should
request this endpoint when they are loaded, but no more than once a day to check for current defaults.

### Get Twitter Supported Languages

The standard Twitter API supports a number of different languages. This endpoint the list of languages
supported by Twitter, along with the language code(s) supported by Twitter.

### Get Twitter's Privacy Policy

To conform with Twitter's privacy policy, some applications may need to display the policy to end users.
This endpoint provides access to the current version of the policy.

### Get Twitter's Terms of Service

To conform with Twitter's Terms of Service policy, some applications may need to display the ToS to
end users. This endpoint provides access to the current version of the ToS. Note, this is not the
same as the developer policy.

### Get API Usage Metrics

The Usage API is a free REST API that provides programmatic access and visibility into activity consumption
across products for your enterprise account. It is the most important and best tool for helping to
monitor and manage usage across the different APIs under your account.

### Twitter Libraries

Various third party libraries wrap over the twitter api.

### twitter-text Parser


