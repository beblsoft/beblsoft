#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter User Mention Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER USER MENTIONS MODEL -------------- #
class BTwitterUserMentionModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter User Mention Model
    Represents other Twitter users mentioned in the text of the Tweet.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#mentions

    obj Format:
    {
      "name": "Twitter API",
      "indices": [
        4,
        15
      ],
      "screen_name": "twitterapi",
      "id": 6253282,
      "id_str": "6253282"
    }
    """
    DESCRIBE_PROPERTIES = ["name", "screenName"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @property
    def screenName(self):  # pylint: disable=C0111
        return self.getValueFromObj("screen_name")

    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def idStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("id_str")

    @property
    def indices(self):  # pylint: disable=C0111
        return self.getValueFromObj("indices")
