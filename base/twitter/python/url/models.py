#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter URL Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER URL MODEL ------------------------ #
class BTwitterURLModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter URL Model
    Represents URLs included in the text of a Tweet.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#urls

    obj Format:
    {
      "indices": [
        32,
        52
      ],
      "url": "http://t.co/IOwBrTZR",
      "display_url": "youtube.com/watch?v=oHg5SJ…",
      "expanded_url": "http://www.youtube.com/watch?v=oHg5SJYRHA0"
    }
    """
    DESCRIBE_PROPERTIES = ["url", "displayURL"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def url(self):  # pylint: disable=C0111
        return self.getValueFromObj("url")

    @property
    def displayURL(self):  # pylint: disable=C0111
        return self.getValueFromObj("display_url")

    @property
    def expandedURL(self):  # pylint: disable=C0111
        return self.getValueFromObj("expanded_url")

    @property
    def indices(self):  # pylint: disable=C0111
        return self.getValueFromObj("indices")
