#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter Rate Limit Status DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.twitter.python.rateLimitStatus.models import BTwitterRateLimitStatusModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER RATE LIMIT STATUS DAO ------------ #
class BTwitterRateLimitStatusDAO():
    """
    Beblsoft Twitter Rate Limit Status DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bTwitter, resourceList=None, **requestJSONKwargs):
        """
        Args:
          List of resources for which to return rate limit status
          Ex. ["statuses", "friends"]
          If None, use BTwitterRateLimitStatusModel.DEFAULT_RESOURCE_LIST

        Reference:
          https://developer.twitter.com/en/docs/developer-utilities/rate-limit-status/api-reference/get-application-rate_limit_status

        Returns
          BTwitterRateLimitStatusModel
        """
        resourceList     = resourceList if resourceList else BTwitterRateLimitStatusModel.DEFAULT_RESOURCE_LIST
        obj              = bTwitter.requestJSON(
            method          = "GET",
            relativePath    = "application/rate_limit_status.json",
            params          = {"resources": ",".join(resourceList)},
            **requestJSONKwargs)
        return BTwitterRateLimitStatusModel(obj=obj)
