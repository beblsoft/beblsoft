#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Rate Limit Status Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER RATE LIMIT STATUS MODEL ---------- #
class BTwitterRateLimitStatusModel(BTwitterCommonModel):
    """
    Beblsoft Twitter Rate Limit Status Model

    Reference:
      https://developer.twitter.com/en/docs/developer-utilities/rate-limit-status/api-reference/get-application-rate_limit_status
      https://developer.twitter.com/en/docs/basics/rate-limits

    obj Format:
    {
      "rate_limit_context": {
        "access_token": "786491-24zE39NUezJ8UTmOGOtLhgyLgCkPyY4dAcx6NA6sDKw"
      },
      "resources": {
        "users": {
          "/users/profile_banner": {
            "limit": 180,
            "remaining": 180,
            "reset": 1403602426
          },
          "/users/suggestions/:slug/members": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/users/show/:id": {
            "limit": 180,
            "remaining": 180,
            "reset": 1403602426
          },
          "/users/suggestions": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/users/lookup": {
            "limit": 900,
            "remaining": 900,
            "reset": 1403602426
          },
          "/users/search": {
            "limit": 900,
            "remaining": 900,
            "reset": 1403602426
          },
          "/users/contributors": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/users/contributees": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/users/suggestions/:slug": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          }
        },
        "statuses": {
          "/statuses/mentions_timeline": {
            "limit": 75,
            "remaining": 75,
            "reset": 1403602426
          },
          "/statuses/lookup": {
            "limit": 900,
            "remaining": 900,
            "reset": 1403602426
          },
          "/statuses/show/:id": {
            "limit": 900,
            "remaining": 900,
            "reset": 1403602426
          },
          "/statuses/oembed": {
            "limit": 180,
            "remaining": 180,
            "reset": 1403602426
          },
          "/statuses/retweeters/ids": {
            "limit": 75,
            "remaining": 75,
            "reset": 1403602426
          },
          "/statuses/home_timeline": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/statuses/user_timeline": {
            "limit": 900,
            "remaining": 900,
            "reset": 1403602426
          },
          "/statuses/retweets/:id": {
            "limit": 75,
            "remaining": 75,
            "reset": 1403602426
          },
          "/statuses/retweets_of_me": {
            "limit": 75,
            "remaining": 75,
            "reset": 1403602426
          }
        },
        "help": {
          "/help/privacy": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/help/tos": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/help/configuration": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          },
          "/help/languages": {
            "limit": 15,
            "remaining": 15,
            "reset": 1403602426
          }
        },
        "search": {
          "/search/tweets": {
            "limit": 180,
            "remaining": 180,
            "reset": 1403602426
          }
        }
      }
    }
    """
    DESCRIBE_PROPERTIES   = ["rateLimitContextAccessToken"]
    DEFAULT_RESOURCE_LIST = ["application", "users", "statuses", "help", "search"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def rateLimitContextAccessToken(self):  # pylint: disable=C0111
        return self.getValueFromObj("rate_limit_context.access_token")

    @property
    def applicationResource(self):  # pylint: disable=C0111
        return self.getValueFromObj("resources.application")

    @property
    def usersResource(self):  # pylint: disable=C0111
        return self.getValueFromObj("resources.users")

    @property
    def statusesResource(self):  # pylint: disable=C0111
        return self.getValueFromObj("resources.statuses")

    @property
    def helpResource(self):  # pylint: disable=C0111
        return self.getValueFromObj("resources.help")

    @property
    def searchResource(self):  # pylint: disable=C0111
        return self.getValueFromObj("resources.search")
