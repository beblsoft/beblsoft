#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Tweet Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER TWEET MODEL ---------------------- #
class BTwitterTweetModel(BTwitterCommonModel):  # pylint: disable=R0904
    """
    Beblsoft Twitter Tweet Model

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
      https://developer.twitter.com/en/docs/tweets/tweet-updates

    Tweet Cases:
      1.  Regular Tweet
          Tweet

      2.  Reply Tweet
          Tweet
          - in_reply_to_*

      3.  Quote Tweet
          Tweet
          - quoted_status

      4a. Retweet
          Tweet
          - retweeted_status

      4b. Retweet Quote Tweet
          Tweet
          - retweeted_status
            - quoted_status

    obj Format:
    {
        "created_at": "Thu Apr 06 15:28:43 +0000 2017",
        "id": 850007368138018817,
        "id_str": "850007368138018817",
        "text": "RT @TwitterDev: 1/ Today we’re sharing our vision for the future of the Twitter API platform!nhttps://t.co/XweGngmxlP",
        "truncated": false,
        "source": "<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>",
        "in_reply_to_status_id": null,
        "in_reply_to_status_id_str": null,
        "in_reply_to_user_id": null,
        "in_reply_to_user_id_str": null,
        "in_reply_to_screen_name": null,
        "is_quote_status": false,
        "retweet_count": 284,
        "favorite_count": 0,
        "favorited": false,
        "retweeted": false,
        "possibly_sensitive": false,
        "lang": "en"
        "geo": null,
        "coordinates": null,
        "place": null,
        "contributors": null,
        "entities": {
          "hashtags": [],
          "symbols": [],
          "media": [],
          "polls": [],
          "user_mentions": [
            {
              "screen_name": "TwitterDev",
              "name": "TwitterDev",
              "id": 2244994945,
              "id_str": "2244994945",
              "indices": [
                3,
                14
              ]
            }
          ],
          "urls": [
            {
              "url": "https://t.co/XweGngmxlP",
              "expanded_url": "https://cards.twitter.com/cards/18ce53wgo4h/3xo1c",
              "display_url": "cards.twitter.com/cards/18ce53wg…",
              "indices": [
                94,
                117
              ]
            }
          ]
        },
        "extended_entities": {
          "media": []
        },
        "user": {... },
        "retweeted_status": {...},
    },
    """
    DESCRIBE_PROPERTIES = ["idStr", "createDate"]

    # ------------------- BASIC PROPERTIES ---------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def idStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("id_str")

    @property
    def createDate(self):
        """
        Return createDate as datetime
        """
        d = datetime.strptime(self.getValueFromObj("created_at"), "%a %b %d %H:%M:%S %z %Y")
        return d.astimezone(tz=timezone.utc)

    @property
    def source(self):  # pylint: disable=C0111
        """
        Utility used to post the tweet
        Ex. "Twitter Web Client"
        """
        return self.getValueFromObj("source")

    @property
    def truncated(self):  # pylint: disable=C0111
        """
        Indicates wheter the value of the text parameter was truncated
        Always False for extended Tweets
        """
        return self.getValueFromObj("truncated")

    @property
    def retweetCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("retweet_count")

    @property
    def favoriteCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("favorite_count")

    @property
    def possiblySensitive(self):  # pylint: disable=C0111
        return self.getValueFromObj("possibly_sensitive")

    # ------------------- TEXT EXTRACTING METHODS --------------------------- #
    def getText(self, fromRetweet=True):  # pylint: disable=C0111
        """
        Args
          fromRetweet:
            If True, and Tweet is a Retweet, take text from retweet.
            This is useful as text in native tweet can be truncated
        """
        text = self.getValueFromObj("text")
        if self.isRetweetedStatus and fromRetweet:
            text = self.bTwitterRetweetedTweetModel.getText(fromRetweet=fromRetweet)
        return text

    def getFullText(self, fromRetweet=True):  # pylint: disable=C0111
        """
        For extended_tweets only
        Args
          fromRetweet:
            If True, and Tweet is a Retweet, take text from retweet.
            This is useful as text in native tweet can be truncated
        """
        fullText = self.getValueFromObj("full_text")
        if self.isRetweetedStatus and fromRetweet:
            fullText = self.bTwitterRetweetedTweetModel.getFullText(fromRetweet=fromRetweet)
        return fullText

    def getParsedText(self, fromRetweet=True, stripMediaURLs=True, convertURLsToExpaned=True,
                      stripQuotedStatusURL=True, stripExtraWhiteSpace=True):
        """
        Return Parsed Text
        Args
          fromRetweet:
            If True, and Tweet is a Retweet, do parsing off of retweet
            This is useful as text in native tweet can be truncated
            Ex. True
          stripMediaURLs:
            If True, strip media urls from text
            Ex. True
          convertURLsToExpanded:
            If True, convert urls to expaneded_url
            For example: "http://t.co/IOwBrTZR" -> "http://www.youtube.com/watch?v=oHg5SJYRHA0"
            Ex. True
          stripQuotedStatusURL:
            If True, strip quoted status URL
            For example: "https://twitter.com/LoisBar13/status/1187004894290550784" -> ""
            Ex. True
          stripExtraWhiteSpace:
            If True, strip extra white space from tweet
            Ex. True

        Warning
          Be careful changing this method.
          Changing it will force Smeckn to reanalyze all updated Twitter tweet text.
          Consider making getParsedTextV2 instead.

        Returns
          Parsed Tweet Text, None if no text
        """
        isRetweet               = self.isRetweetedStatus
        bTwitterTweetModel      = self.bTwitterRetweetedTweetModel if isRetweet and fromRetweet else self
        bTwitterQuoteTweetModel = bTwitterTweetModel.bTwitterQuoteTweetModel

        # Get Text
        text = bTwitterTweetModel.getFullText()
        if not text:
            text = bTwitterTweetModel.getText()

        # Strip Media URLs
        if stripMediaURLs:
            for bTwitterMediaModel in bTwitterTweetModel.bTwitterMediaModelList:
                text = text.replace(bTwitterMediaModel.url, "")

        # Convert URLs to Expanded
        if convertURLsToExpaned:
            for bTwitterURLModel in bTwitterTweetModel.bTwitterURLModelList:
                text = text.replace(bTwitterURLModel.url, bTwitterURLModel.expandedURL)

        # Strip Quoted Status URL
        if stripQuotedStatusURL and bTwitterQuoteTweetModel:
            quotedStatusURL = "https://twitter.com/{}/status/{}".format(bTwitterQuoteTweetModel.bTwitterUserModel.screenName,
                                                                        bTwitterQuoteTweetModel.idStr)
            text            = text.replace(quotedStatusURL, "")

        # Strip Extra Whitespace
        if stripExtraWhiteSpace:
            text = text.strip()

        # Convert Empty Strings to None
        if not text or len(text) == 0:
            text = None

        return text

    # ------------------- REPLY PROPERTIES ---------------------------------- #
    @property
    def inReplyToStatusID(self):  # pylint: disable=C0111
        return self.getValueFromObj("in_reply_to_status_id")

    @property
    def inReplyToStatusIDStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("in_reply_to_status_id_str")

    @property
    def inReplyToUserID(self):  # pylint: disable=C0111
        return self.getValueFromObj("in_reply_to_user_id")

    @property
    def inReplyToUserIDStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("in_reply_to_user_id_str")

    @property
    def inReplyToUserScreenName(self):  # pylint: disable=C0111
        return self.getValueFromObj("in_reply_to_screen_name")

    # ------------------- USER PROPERTIES ----------------------------------- #
    @property
    def bTwitterUserModel(self):  # pylint: disable=C0111
        """
        Return BTwitterUserModel for user who posted this tweet
        """
        from base.twitter.python.user.models import BTwitterUserModel

        bTwitterUserModel     = None
        userObj               = self.getValueFromObj("user")
        if userObj:
            bTwitterUserModel = BTwitterUserModel(obj=userObj)
        return bTwitterUserModel

    # ------------------- QUOTED TWEET -------------------------------------- #
    @property
    def isQuoteStatus(self):  # pylint: disable=C0111
        return self.getValueFromObj("is_quote_status")

    @property
    def quoteStatusID(self):  # pylint: disable=C0111
        return self.getValueFromObj("quoted_status_id")

    @property
    def quoteStatusIDStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("quoted_status_id_str")

    @property
    def bTwitterQuoteTweetModel(self):  # pylint: disable=C0111
        """
        Return Quoted Tweet BTwitterTweetModel
        """
        bTwitterTweetModel     = None
        quoteTweetObj          = self.getValueFromObj("quoted_status")
        if quoteTweetObj:
            bTwitterTweetModel = BTwitterTweetModel(obj=quoteTweetObj)
        return bTwitterTweetModel

    # ------------------- RETWEETED TWEET ----------------------------------- #
    @property
    def isRetweetedStatus(self):  # pylint: disable=C0111
        return self.getValueFromObj("retweeted_status") != None

    @property
    def bTwitterRetweetedTweetModel(self):  # pylint: disable=C0111
        """
        Return Retweeted Tweet BTwitterTweetModel
        """
        bTwitterTweetModel     = None
        retweetedTweetObj      = self.getValueFromObj("retweeted_status")
        if retweetedTweetObj:
            bTwitterTweetModel = BTwitterTweetModel(obj=retweetedTweetObj)
        return bTwitterTweetModel

    # ------------------- [EXTENDED] ENTITIES ------------------------------- #
    def getBTwitterEntityModelList(self, path, modelClass):
        """
        Return BTwitter Model List
        Args
          path:
            Path in Tweet object where entity list is found
            Ex. "entities.hashtags"
          modelClass:
            BTwitter Model Class to cast the underlying object
            Ex. BTwitterHashtagModel
        """
        bTwitterModelList = []
        objList           = self.getValueFromObj(path, default=[])
        for obj in objList:
            bTwitterModelList.append(modelClass(obj=obj))
        return bTwitterModelList

    @property
    def bTwitterHashtagModelList(self):  # pylint: disable=C0116
        from base.twitter.python.hashtag.models import BTwitterHashtagModel
        return self.getBTwitterEntityModelList(path="entities.hashtags", modelClass=BTwitterHashtagModel)

    @property
    def bTwitterSymbolModelList(self):  # pylint: disable=C0116
        from base.twitter.python.symbol.models import BTwitterSymbolModel
        return self.getBTwitterEntityModelList(path="entities.symbols", modelClass=BTwitterSymbolModel)

    @property
    def bTwitterMediaModelList(self):  # pylint: disable=C0116
        from base.twitter.python.media.models import BTwitterMediaModel
        return self.getBTwitterEntityModelList(path="extended_entities.media", modelClass=BTwitterMediaModel)

    @property
    def bTwitterPollModelList(self):  # pylint: disable=C0116
        from base.twitter.python.poll.models import BTwitterPollModel
        return self.getBTwitterEntityModelList(path="entities.polls", modelClass=BTwitterPollModel)

    @property
    def bTwitterUserMentionModelList(self):  # pylint: disable=C0116
        from base.twitter.python.userMention.models import BTwitterUserMentionModel
        return self.getBTwitterEntityModelList(path="entities.user_mentions", modelClass=BTwitterUserMentionModel)

    @property
    def bTwitterURLModelList(self):  # pylint: disable=C0116
        from base.twitter.python.url.models import BTwitterURLModel
        return self.getBTwitterEntityModelList(path="entities.urls", modelClass=BTwitterURLModel)
