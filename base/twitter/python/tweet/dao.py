#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter Tweet DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.twitter.python.tweet.models import BTwitterTweetModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER TWEET DAO ------------------------ #
class BTwitterTweetDAO():
    """
    Beblsoft Twitter Tweet DAO
    """

    # ------------------- GET BY ID ----------------------------------------- #
    @staticmethod
    @logFunc()
    def getByID(bTwitter, tweetID, includeEntities=True,
                trimUser=False, tweetMode="extended", **requestJSONKwargs):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          tweetID:
            The ID of the tweet for which to return results.
            Ex. "1187669117307965440"
          tweetMode:
            Controls tweet payload format
            Allowed Values: "compat" or "extended"
            "extended" is the latest format that does not truncate tweets
            Reference: https://developer.twitter.com/en/docs/tweets/tweet-updates
          includeEntities:
            If False, the entities node will not be included
            Ex. True
          trimUser:
            If True, each Tweet returned will include a user object with only the user ID
            If False, receive the complete user object
          requestJSONKwargs:
            Extra args to passthrough to requestJSON

        Reference:
          https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-show-id

        Returns
          BTwitterTweetModel
        """
        # Fill out parameters
        params                    = {"id": tweetID,
                                     "include_entities": str(includeEntities).lower(),
                                     "trim_user": str(trimUser).lower(),
                                     "tweet_mode": tweetMode}
        # Send request
        obj                       = bTwitter.requestJSON(
            method                   = "GET",
            relativePath             = "statuses/show.json",
            params                   = params,
            **requestJSONKwargs)

        return BTwitterTweetModel(obj=obj)

    # ------------------- LOOKUP ID LIST ------------------------------------ #
    @staticmethod
    @logFunc()
    def lookupIDList(bTwitter, tweetIDList, includeEntities=True,
                     trimUser=False, tweetMode="extended", **requestJSONKwargs):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          tweetIDList:
            List of tweet IDs for which to return results
            Up to 100 allowed in a single request
            Ex. ["1187669117307965440"]
          tweetMode:
            Controls tweet payload format
            Allowed Values: "compat" or "extended"
            "extended" is the latest format that does not truncate tweets
            Reference: https://developer.twitter.com/en/docs/tweets/tweet-updates
          includeEntities:
            If False, the entities node will not be included
            Ex. True
          trimUser:
            If True, each Tweet returned will include a user object with only the user ID
            If False, receive the complete user object
          requestJSONKwargs:
            Extra args to passthrough to requestJSON

        Reference:
          https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-lookup

        Returns
          [BTwitterTweetModel]
        """
        # Fill out parameters
        params                    = {"id": ",".join(tweetIDList),
                                     "include_entities": str(includeEntities).lower(),
                                     "trim_user": str(trimUser).lower(),
                                     "tweet_mode": tweetMode}

        # Send request
        objList                   = bTwitter.requestJSON(
            method                   = "GET",
            relativePath             = "statuses/lookup.json",
            params                   = params,
            **requestJSONKwargs)

        # Parse Response
        bTwitterTweetModelList    = []
        for obj in objList:
            bTwitterTweetModelList.append(BTwitterTweetModel(obj=obj))

        return bTwitterTweetModelList
