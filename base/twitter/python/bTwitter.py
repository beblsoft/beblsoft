#!/usr/bin/env python3.6
"""
 NAME
  bTwitter.py

 DESCRIPTION
  Beblsoft Twitter Functionality

 REFERNCE
   Twiter API                    : https://developer.twitter.com/en/docs/api-reference-index
   Python-Twitter Implementation : https://github.com/bear/python-twitter/blob/master/twitter/api.py
   Python Twitter Signature      : https://github.com/anein/twitter-signature-python/blob/master/TwitterSignature.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging
import requests
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.twitter.python.bTwitterRequestOAuth import BeblsoftTwitterRequestOAuth


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT TWITTER --------------------------------- #
class BeblsoftTwitter():
    """
    Beblsoft Twitter
    """

    TWITTER_API_URL = "https://api.twitter.com"

    def __init__(self,
                 appID,
                 consumerAPIKey,
                 consumerAPISecretKey,
                 appAccessToken,
                 appAccessTokenSecret,
                 version=1.1,
                 timeoutS=10.0,
                 proxyDict=None,
                 session=None):
        """
        Initialize object
        Args
          appID:
            Twitter Application ID
            Ex. 16867076
          consumerAPIKey:
            Consumer API Key
            Ex. "q8sasdflkjhasdjkfhasdj"
          consumerAPISecretKey:
            Consumer API Secret Key
            Ex. "asdpf987q2u43lkadjshfop9aifnasdfjfhladksfjhasdfkld"
          appAccessToken:
            Application access token
            Ex. "1234987132498713499-ASKDJHdDFlkadjf0adsfjlk2j309sd"
          appAccessTokenSecret:
            Application access token secret
            Ex. "LKJDYHFKLJMDF098asdflkjasdf0asdflkjLKJASDSKKK"
          version:
            Twitter API Version
            Ex. 6.0
          timeoutS:
            Float describing time (in seconds) that client will wait for responses
          proxyDict:
            Dictionary with proxy settings for requests
          session:
            Reqests session object
        """
        self.appID                = appID
        self.consumerAPIKey       = consumerAPIKey
        self.consumerAPISecretKey = consumerAPISecretKey
        self.appAccessToken       = appAccessToken
        self.appAccessTokenSecret = appAccessTokenSecret
        self.version              = version
        self.timeoutS             = timeoutS
        self.proxyDict            = proxyDict
        self.session              = session or requests.Session()

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------------- REQUEST ------------------------------------- #
    @logFunc()
    def request(self,
                method,
                path=None,
                relativePath=None,
                bTwitterRequestOAuth=None,
                oauthToken=None,
                oauthTokenSecret=None,
                useAppAccessTokens=False,
                params=None,
                data=None,
                headers=None,
                files=None,
                raiseOnStatus=True):
        """
        Executes a graph API request

        Args
          method:
            HTTP Method
            Ex. "GET"
          path:
            Full http path to make request on
            Ex. "POST https://api.twitter.com/oauth/request_token"
          relativePath:
            Path relative to https://api.twitter.com/<version>/
            Ex. "statuses/update.json"
          bTwitterRequestOAuth:
            Custom BeblsoftTwitterRequestOAuth object
            If None, BeblsoftTwitterRequestOAuth is created
          oauthToken:
            OAuth Token for the Request
            Specifies which context the request is coming from: user or application
            If None, application access token is used
          oauthTokenSecret:
            OAuth Token Secret for the Request
            Specifies which context the request is coming from: user or application
            If None, application access token secret is used
          useAppAccessTokens:
            If True, use application access tokens to make the request
          params:
            Dictionary Query strying parameters
            Ex. { "foo": "bar" }
          data:
            Dictionary of Post/put data
            Ex. { "foo": "bar" }
          headers:
            Dictionary of headers
            Ex. { "foo": "bar" }
          files:
            Files to add to request
          raiseOnStatus:
            If True, raise error if response status code != 200
            Ex. True

        Returns
          Requests response object
          http://docs.python-requests.org/en/master/api/#requests.Response
        """
        # Format Request
        method = method.upper()
        if relativePath and not path:
            path = "{}/{}/{}".format(BeblsoftTwitter.TWITTER_API_URL, self.version, relativePath)
        if not path:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_PATH_NOT_SPECIFIED)
        if not params:
            params = {}
        if not data:
            data = {}
        if not headers:
            headers = {}

        # Setup Authorization
        if not bTwitterRequestOAuth:
            if useAppAccessTokens:
                oauthToken           = self.appAccessToken
                oauthTokenSecret     = self.appAccessTokenSecret
            bTwitterRequestOAuth     = BeblsoftTwitterRequestOAuth(
                method                   = method,
                path                     = path,
                consumerAPIKey           = self.consumerAPIKey,
                consumerAPISecretKey     = self.consumerAPISecretKey,
                oauthToken               = oauthToken,
                oauthTokenSecret         = oauthTokenSecret,
                requestParams            = params,
                requestData              = data)
        headers["Authorization"]     = bTwitterRequestOAuth.getAuthorizationHeaderValue()

        # Send Request
        try:
            resp = self.session.request(
                method,
                path,
                timeout = self.timeoutS,
                params  = params,
                data    = data,
                headers = headers,
                proxies = self.proxyDict,
                files   = files)
        except requests.HTTPError as e:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_REQUEST_FAILED, originalError=e)

        # Process results
        headers       = resp.headers
        contentType   = headers["content-type"]
        respJSON      = resp.json() if "json" in contentType else None

        # Handle Errors
        # On Error : Twitter Returns Dict List Of Error Codes
        # Reference: https://developer.twitter.com/en/docs/basics/response-codes
        # {
        #   'errors': [{'code': 32, 'message': 'Could not authenticate you.'}]
        # }
        errorList      = respJSON.get("errors", []) if isinstance(respJSON, dict) else []
        bErrorCodeDict = {32: BeblsoftErrorCode.TWITTER_API_REQUEST_BAD_AUTHENTICATION,
                          220: BeblsoftErrorCode.TWITTER_API_REQUEST_BAD_AUTHENTICATION}
        errorMsg       = "statusCode={} respJSON={}".format(resp.status_code, respJSON)
        if errorList:
            errorCode  = errorList[0].get("code")
            bErrorCode = bErrorCodeDict.get(errorCode, BeblsoftErrorCode.TWITTER_API_REQUEST_FAILED)
            raise BeblsoftError(code=bErrorCode, msg=errorMsg)
        if raiseOnStatus and resp.status_code != 200:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_REQUEST_FAILED, msg=errorMsg)

        # Return Response
        return resp

    # ------------------------- REQUEST JSON -------------------------------- #
    @logFunc()
    def requestJSON(self, **requestKwargs):
        """
        Execute request and return JSON response
        """
        return self.request(**requestKwargs).json()
