#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter Verify Account Credentials DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.twitter.python.verifyAccountCredentials.models import BTwitterVerifyAccountCredentialsModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER VERIFY ACCOUNT CREDENTIALS DAO --- #
class BTwitterVerifyAccountCredentialsDAO():
    """
    Beblsoft Twitter Verify Account Credentials DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bTwitter, oauthToken, oauthTokenSecret,
            includeEntities=True, skipStatus=True, includeEmail=False):
        """
        Verify user credentials are valid

        Args:
          bTwitter:
            Beblsoft Twitter Object
          oauthToken:
            User OAuth Token
            Ex. D6c-_wAAAAABAV8EAAABbe-ygow
          oauthTokenSecret:
            User OAuth Token Secret
            Ex. qrLBK51NoIfxkec0e6NcEGioMvpgdY8l
          includeEntities:
            If False, exclude the entities node in the returned object
          skipStatus:
            If True, returning status in the returned object
          includeEmail:
            If True, email will be returned in the user object as a string

        Reference:
          https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials
          https://developer.twitter.com/en/docs/twitter-for-websites/log-in-with-twitter/guides/implementing-sign-in-with-twitter

        Returns
          BTwitterVerifyAccountCredentialsModel
        """
        obj                  = bTwitter.requestJSON(
            method                = "GET",
            relativePath          = "account/verify_credentials.json",
            oauthToken            = oauthToken,
            oauthTokenSecret      = oauthTokenSecret,
            params                = {"include_entities": str(includeEntities).lower(),
                                     "skip_status": str(skipStatus).lower(),
                                     "include_email": str(includeEmail).lower()
                                     })
        return BTwitterVerifyAccountCredentialsModel(obj=obj)

    # ------------------- CREDENTIALS ARE VALID ----------------------------- #
    @staticmethod
    @logFunc()
    def credentialsAreValid(bTwitter, oauthToken, oauthTokenSecret):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          oauthToken:
            User OAuth Token
            Ex. D6c-_wAAAAABAV8EAAABbe-ygow
          oauthTokenSecret:
            User OAuth Token Secret
            Ex. qrLBK51NoIfxkec0e6NcEGioMvpgdY8l

        Returns
          True, if user credentials are valid
          False, otherwise
        """
        rval     = False
        try:
            _    = BTwitterVerifyAccountCredentialsDAO.get(bTwitter=bTwitter,
                                                           oauthToken=oauthToken,
                                                           oauthTokenSecret=oauthTokenSecret)
            rval = True
        except BeblsoftError as bError:
            if bError.code == BeblsoftErrorCode.TWITTER_API_REQUEST_BAD_AUTHENTICATION:
                pass
            else:
                raise bError

        return rval
