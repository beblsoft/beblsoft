#!/usr/bin/env python3
"""
NAME:
 models.py

DESCRIPTION
 Beblsoft Twitter Common Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import pprint
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__file__)


# ----------------------- BEBLSOFT TWITTER COMMON MODEL --------------------- #
class BTwitterCommonModel():
    """
    Beblsoft Twitter Common Model
    """

    def __init__(self, obj):  # pylint: disable=W0622
        """
        Initialize object
        Args
          obj:
            Twitter object metadata
        """
        self.obj = obj

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------- METADATA PROPERTIES ------------------------------- #
    @logFunc()
    def getValueFromObj(self, key, required=False, default=None):
        """
        Return value from metadata dictionary
        Nested values are allowed
        Args
          key:
            Key into object
            Ex1. 56
            Ex2. "Foo"
            Ex3. "Foo.bar.baz"
          required:
            If True, raise exception if not found
          default:
            Default value
        """
        value = None
        if isinstance(key, int):
            value = self.__getValueForKeyList(key, self.obj, required, default)
        else:
            value = self.__getValueForKeyList(key.split('.'), self.obj,
                                              required, default)
        return value

    def __getValueForKeyList(self, keyList, d, required, default):
        """
        Return value given a list of keys
        """
        value = None
        if len(keyList) == 1:
            value = self.__getValueForKey(keyList[0], d, required, default)
        else:
            value = self.__getValueForKeyList(keyList[1:],
                                              self.__getValueForKey(
                                                  keyList[0], d, required, default),
                                              required,
                                              default)
        return value

    def __getValueForKey(self, key, d, required, default):  # pylint: disable=R0201
        """
        Return object value for key
        """
        value = None
        try:
            value = d[key]
        except Exception as e:  # pylint: disable=W0703
            if required:
                raise BeblsoftError(
                    code=BeblsoftErrorCode.TWITTER_NO_ATTRIBUTE, originalError=e)
            else:
                value = default
        return value

    # ------------------- DESCRIBE ------------------------------------------ #
    DESCRIBE_PROPERTIES = []

    @logFunc()
    def describe(self, properties=None):
        """
        Describe object
        """
        dpDict = self.getDescribePropertyDict(properties=properties)
        logger.info("[{} {}]".format(self.__class__.__name__, pprint.pformat(dpDict)))

    @logFunc()
    def getDescribePropertyDict(self, properties=None):
        """
        Return describe string
        """
        d          = {}
        properties = properties if properties else self.DESCRIBE_PROPERTIES
        for prop in properties:
            d[prop] = str(getattr(self, prop))
        return d
