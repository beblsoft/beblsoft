#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Hashtag Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER HASHTAG MODEL -------------------- #
class BTwitterHashtagModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Hashtag Model
    Represents hashtags which have been parsed out of the Tweet text.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#hashtags

    obj Format:
    {
      "text": "hashtag",
      "indices": [
        23,
        31
      ]
    }
    """
    DESCRIBE_PROPERTIES = ["text"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def text(self):  # pylint: disable=C0111
        return self.getValueFromObj("text")

    @property
    def indices(self):  # pylint: disable=C0111
        return self.getValueFromObj("indices")
