#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Poll Models
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER POLL OPTION MODEL ---------------- #
class BTwitterPollOptionModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Poll Model
    Represents hashtags which have been parsed out of the Tweet text.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#polls

    obj Format:
    {
      "position": 1,
      "text": "I read documentation once."
    },
    """
    DESCRIBE_PROPERTIES = ["text", "position"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def position(self):  # pylint: disable=C0111
        return self.getValueFromObj("position")

    @property
    def text(self):  # pylint: disable=C0111
        return self.getValueFromObj("text")


# ----------------------- BEBLSOFT TWITTER POLL MODEL ----------------------- #
class BTwitterPollModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Poll Model
    Represents hashtags which have been parsed out of the Tweet text.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#polls

    obj Format:
    {
        "options": [
          {
            "position": 1,
            "text": "I read documentation once."
          },
          {
            "position": 2,
            "text": "I read documentation twice."
          },
          {
            "position": 3,
            "text": "I read documentation over and over again."
          }
        ],
        "end_datetime": "Thu May 25 22:20:27 +0000 2017",
        "duration_minutes": 60
      }
    """
    DESCRIBE_PROPERTIES = ["durationMinutes"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def bOptionModelList(self):  # pylint: disable=C0111
        """
        Return [BTwitterPollOptionModel]
        """
        optionObjList    = self.getValueFromObj("options", default=[])
        bOptionModelList = []
        for optionObj in optionObjList:
            bOptionModelList.append(BTwitterPollModel(obj=optionObj))
        return bOptionModelList

    @property
    def durationMinutes(self):  # pylint: disable=C0111
        return self.getValueFromObj("duration_minutes")

    @property
    def endDate(self):
        """
        Return endDate as datetime
        """
        d = datetime.strptime(self.getValueFromObj("end_datetime"), "%a %b %d %H:%M:%S %z %Y")
        return d.astimezone(tz=timezone.utc)
