#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter User Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime, timezone
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER USER MODEL ----------------------- #
class BTwitterUserModel(BTwitterCommonModel):
    """
    Beblsoft Twitter User Model

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object

    obj Format:
    {
        "id": 6253282,
        "id_str": "6253282",
        "name": "Twitter API",
        "screen_name": "TwitterAPI",
        "location": "San Francisco, CA",
        "profile_location": null,
        "description": "The Real Twitter API. Tweets about API changes, service issues and our Developer Platform. Don't get an answer? It's on my website.",
        "url": "https://t.co/8IkCzCDr19",
        "entities": {
            "url": {
                "urls": [{
                    "url": "https://t.co/8IkCzCDr19",
                    "expanded_url": "https://developer.twitter.com",
                    "display_url": "developer.twitter.com",
                    "indices": [
                        0,
                        23
                    ]
                }]
            },
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 6133636,
        "friends_count": 12,
        "listed_count": 12936,
        "created_at": "Wed May 23 06:01:13 +0000 2007",
        "favourites_count": 31,
        "utc_offset": null,
        "time_zone": null,
        "geo_enabled": null,
        "verified": true,
        "statuses_count": 3656,
        "lang": null,
        "contributors_enabled": null,
        "is_translator": null,
        "is_translation_enabled": null,
        "profile_background_color": null,
        "profile_background_image_url": null,
        "profile_background_image_url_https": null,
        "profile_background_tile": null,
        "profile_image_url": null,
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/942858479592554497/BbazLO9L_normal.jpg",
        "profile_banner_url": null,
        "profile_link_color": null,
        "profile_sidebar_border_color": null,
        "profile_sidebar_fill_color": null,
        "profile_text_color": null,
        "profile_use_background_image": null,
        "has_extended_profile": null,
        "default_profile": false,
        "default_profile_image": false,
        "following": null,
        "follow_request_sent": null,
        "notifications": null,
        "translator_type": null
    }
    """
    DESCRIBE_PROPERTIES   = ["idStr", "name", "screenName", "createDate"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def idStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("id_str")

    @property
    def name(self):  # pylint: disable=C0111
        return self.getValueFromObj("name")

    @property
    def screenName(self):  # pylint: disable=C0111
        return self.getValueFromObj("screen_name")

    @property
    def createDate(self):
        """
        Return createDate as datetime
        """
        d = datetime.strptime(self.getValueFromObj("created_at"), "%a %b %d %H:%M:%S %z %Y")
        return d.astimezone(tz=timezone.utc)

    @property
    def description(self):  # pylint: disable=C0111
        return self.getValueFromObj("description")

    @property
    def followersCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("followers_count")

    @property
    def friendsCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("friends_count")

    @property
    def statusesCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("statuses_count")

    @property
    def favoritesCount(self):  # pylint: disable=C0111
        return self.getValueFromObj("favourites_count")
