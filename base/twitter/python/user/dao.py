#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter User DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.twitter.python.user.models import BTwitterUserModel
from base.twitter.python.tweet.models import BTwitterTweetModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER USER DAO ------------------------- #
class BTwitterUserDAO():
    """
    Beblsoft Twitter User DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bTwitter, userID=None, screenName=None, tweetMode="extended", **requestJSONKwargs):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          userID:
            The ID of the user for whom to return results.
            Either userID or screenName is required
            Ex. 234098234
          screenName:
            The screen name of the user for whom to return results
            Either userID or screenName is required
            Ex. "FooBar"
          tweetMode:
            Controls tweet payload format
            Allowed Values: "compat" or "extended"
            "extended" is the latest format that does not truncate tweets
            Reference: https://developer.twitter.com/en/docs/tweets/tweet-updates
          requestJSONKwargs:
            Extra args to passthrough to requestJSON

        Reference:
          https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-users-show

        Returns
          BTwitterUserModel
        """
        # Fill out parameters
        params                    = {"tweet_mode": tweetMode }
        if userID:
            params["user_id"]     = userID
        if screenName:
            params["screen_name"] = screenName

        # Send request
        obj                       = bTwitter.requestJSON(
            method                   = "GET",
            relativePath             = "users/show.json",
            params                   = params,
            **requestJSONKwargs)

        return BTwitterUserModel(obj=obj)

    # ------------------- TIMELINE TWEETS ----------------------------------- #
    @staticmethod
    @logFunc()
    def getTimelineTweets(bTwitter, userID=None, screenName=None, count=100, sinceID=None,
                          maxID=None, includeRetweets=True, excludeReplies=False, trimUser=False,
                          sortOldToYoung=True, tweetMode="extended", **requestJSONKwargs):
        """
        Get Timeline Tweets

        Args
          bTwitter:
            Beblsoft Twitter Object
          userID:
            The ID of the user for whom to return results.
            Either userID or screenName is required
            Ex. 234098234
          screenName:
            The screen name of the user for whom to return results
            Either userID or screenName is required
          count:
            Specifies the number of Tweets to retrieve
            Max Value 200
            Ex. 200
            Note: This number may be different from the actual number of Tweets
            returned due to includeRetweets and excludeReplies
          sinceID:
            Return results with ID greater than (more recent than) the specified ID
            Note: Lower IDs = Older Tweets
            Ex. 12345
            There are limits to the number of Tweets that can be accessed through the API.
            If the limit of Tweets has occured since the since_id, the since_id will be forced to the oldest ID available.
          maxID:
            Return results with ID less than (older than) the specified ID
            Note: Lower IDs = Older Tweets
            Ex. 54321
          includeRetweets:
            If True, return retweets
            If False, don't return retweets
          excludeReplies:
            If True, filter out Tweet replies
          trimUser:
            If True, each Tweet returned in a timeline will include a user object
            only including the status authors numerical ID.
            If False, the complete user object will be preset
          sortOldToYoung:
            If True, sort Tweets  oldest   (lowest ID)  to youngest (highest ID)
            If False, sort Tweets youngest (highest ID) to oldest   (lowestID)
          tweetMode:
            Allowed Values: "compat" or "extended"
            "extended" is the latest format that does not truncate tweets
            Reference: https://developer.twitter.com/en/docs/tweets/tweet-updates

        Note
          If both sinceID and maxID are None, Twitter returns the Youngest (most recent)
          tweets available.
          To iterate through all tweets, start with yougest, then iterate backward
          by decreasing the maxID parameter.

        Reference
          https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
          https://developer.twitter.com/en/docs/tweets/timelines/guides/working-with-timelines

        Returns
          [BTwitterTweetModel]
          Sorted Order: Oldest (lowest ID) -> Youngest (highest ID)
        """
        # Fill out parameters
        params                    = {"include_rts": str(includeRetweets).lower(),
                                     "exclude_replies": str(excludeReplies).lower(),
                                     "trim_user": str(trimUser).lower(),
                                     "tweet_mode": tweetMode }
        if userID:
            params["user_id"]     = userID
        if screenName:
            params["screen_name"] = screenName
        if count is not None:
            params["count"]       = count
        if sinceID is not None:
            params["since_id"]    = sinceID
        if maxID is not None:
            params["max_id"]      = maxID
        if maxID is not None:
            params["max_id"]      = maxID

        # Send request
        objList                   = bTwitter.requestJSON(
            method                   = "GET",
            relativePath             = "statuses/user_timeline.json",
            params                   = params,
            **requestJSONKwargs)

        # Convert to Tweet List and Sort
        bTwitterTweetModelList    = [BTwitterTweetModel(obj=obj) for obj in objList]
        bTwitterTweetModelList.sort(reverse=not sortOldToYoung, key=lambda bTwitterTweet: bTwitterTweet.id)

        return bTwitterTweetModelList

    @staticmethod
    @logFunc()
    def getAllTimelineTweets(sortOldToYoung=True, **getTimelineTweetsKwargs):
        """
        Get All Timline Tweets

        Returns
          [BTwitterTweetModel]
        """
        bTwitterTweetModelList        = []
        keepRequesting                = True
        nextMaxID                     = None

        # Loop until all tweets have been retrieved
        # First iteration, sinceID and maxID are None, so twitter returns the youngest tweets available
        # Keep iterating, updating nextMaxID to represent older and older tweets
        while keepRequesting:
            # Get latest batch of tweets, add to list
            curBTwitterTweetModelList = BTwitterUserDAO.getTimelineTweets(
                maxID                    = nextMaxID,
                sortOldToYoung           = sortOldToYoung, **getTimelineTweetsKwargs)
            bTwitterTweetModelList   += curBTwitterTweetModelList
            bTwitterTweetModelList.sort(reverse=not sortOldToYoung, key=lambda bTwitterTweet: bTwitterTweet.id)

            # Update keepRequesting, nextMaxID
            keepRequesting            = len(curBTwitterTweetModelList) > 0
            if keepRequesting:
                curMaxID              = bTwitterTweetModelList[0].id if sortOldToYoung else bTwitterTweetModelList[-1].id
                nextMaxID             = curMaxID - 1

        return bTwitterTweetModelList
