#!/usr/bin/env python3.6
"""
 NAME
  bTwitterRequestOAuth.py

 DESCRIPTION
  Beblsoft Twitter Request OAuth Functionality

 REFERENCE
  Python Twitter Signature Example    : https://github.com/anein/twitter-signature-python/blob/master/TwitterSignature.py
  Authorizing a Twitter Request       : https://developer.twitter.com/en/docs/basics/authentication/guides/authorizing-a-request
  Creating a Twitter Signature        : https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature
  Percent Encoding Twitter Parameters : https://developer.twitter.com/en/docs/basics/authentication/guides/percent-encoding-parameters
"""


# ------------------------ IMPORTS ------------------------------------------ #
import hmac
import time
import logging
import random
import hashlib
import base64
from urllib import parse
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ BEBLSOFT TWITTER REQUEST OAUTH ------------------- #
class BeblsoftTwitterRequestOAuth():
    """
    Beblsoft Twitter Request OAuth
    """

    def __init__(self,
                 method,
                 path,
                 consumerAPIKey,
                 consumerAPISecretKey,
                 oauthToken           = None,
                 oauthTokenSecret     = None,
                 oauthCallback        = None,
                 oauthVersion         = 1.0,
                 oauthSignatureMethod = "HMAC-SHA1",
                 nNonceRandomBits     = 64,
                 requestParams        = None,
                 requestData          = None):
        """
        Initialize object
        Args
          method:
            HTTP Request Method
            Ex. "GET"
          path:
            HTTP Request Path
            Ex. "https://api.twitter.com/6.0/statuses/update.json"
          consumerAPIKey:
            Consumer API Key
            Ex. "q8sasdflkjhasdjkfhasdj"
          consumerAPISecretKey:
            Consumer API Secret Key
            Ex. "asdpf987q2u43lkadjshfop9aifnasdfjfhladksfjhasdfkld"
          oauthToken:
            OAuth Token
          oauthTokenSecret:
            OAuth Token Secrent
          oauthCallback:
            Value specified here will be used as the URL a user is redirected to should they
            approve your application's access to their account.
            Ex. "http://themattharris.local/auth.php twitterclient://callback"
            Note: Callback must be whitelisted within the app settings
          oauthVersion:
            OAuth Version
            Ex. 1.0
          nNonceRandomBits:
            Number of random bits in nonce value
            Ex. 64
          requestParams:
            Request Query String Parameter Dictionary
            Ex. {"foo": "bar"}
          requestData:
            Request Data Dictionary
            Ex. {""}
        """
        self.method               = method.upper()
        self.path                 = path
        self.requestParams        = requestParams if requestParams else {}
        self.requestData          = requestData if requestData else {}
        self.consumerAPIKey       = consumerAPIKey
        self.consumerAPISecretKey = consumerAPISecretKey
        self.oauthSignatureMethod = oauthSignatureMethod
        self.oauthToken           = oauthToken
        self.oauthTokenSecret     = oauthTokenSecret
        self.oauthCallback        = oauthCallback
        self.oauthVersion         = oauthVersion
        self.timestamp            = int(time.time())
        self.nonce                = str(random.getrandbits(nNonceRandomBits))

    def __str__(self):
        return "[{}]".format(self.__class__.__name__)

    # ------------------------- BASE OAUTH DICT ----------------------------- #
    @property
    def baseOAuthDict(self):
        """
        Return base oauth dictionary
        Used for authorization header and signature
        """
        d = {
            "oauth_consumer_key": self.consumerAPIKey,
            "oauth_nonce": self.nonce,
            "oauth_signature_method": self.oauthSignatureMethod,
            "oauth_timestamp": self.timestamp,
            "oauth_version": self.oauthVersion,
        }
        if self.oauthToken:
            d["oauth_token"] = self.oauthToken
        if self.oauthCallback:
            d["oauth_callback"] = self.oauthCallback
        return d

    # ------------------------- SIGNATURE ----------------------------------- #
    @logFunc()
    def getSignature(self):
        """
        Returns the Authorization Signature for the Request

        Reference
          https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature

        Returns
          Oauth Signature
        """
        sha1HMAC         = hmac.new(
            key            = self.getSigningKey().encode("utf-8"),
            msg            = self.getSignatureBaseString().encode("utf-8"),
            digestmod      = hashlib.sha1)
        signature        = sha1HMAC.digest()
        signatureBase64  = base64.b64encode(signature).decode("utf-8")
        return signatureBase64

    # ------------------------- SIGNATURE BASE STRING ----------------------- #
    @logFunc()
    def getSignatureBaseString(self):
        """
        Return oauth signature base string

        Parameter String Refererence
          https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature
          See "Collecting Parameters"
        Base String Refererence
          https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature
          See "Creating the signature base string"
        """
        # Steps
        # -------
        # 1. Collect dictionary of parameters
        # 2. Percent Encode Every Key and Value to be Signed
        # 3. Create string from sorted key value pairs
        # 4. Combine method, path, and parameter string to create base string
        startDict       = {**self.requestParams, **self.requestData, **self.baseOAuthDict}
        encodedDict     = {self.percentEncode(key): self.percentEncode(startDict[key]) for key in startDict}
        parameterString = "&".join("{}={}".format(key, encodedDict[key]) for key in sorted(encodedDict))
        baseString      = "{}&{}&{}".format(self.method, self.percentEncode(self.path),
                                            self.percentEncode(parameterString))
        return baseString

    # ------------------------- SIGNING KEY --------------------------------- #
    @logFunc()
    def getSigningKey(self):
        """
        Return oauth signing key

        Refererence
          https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature
          See "Getting a signing key"
        """
        return "{}&{}".format(
            self.percentEncode(self.consumerAPISecretKey),
            self.percentEncode(self.oauthTokenSecret) if self.oauthTokenSecret else "")

    # ------------------------- AUTHORIZATION HEADER VALUE ------------------ #
    @logFunc()
    def getAuthorizationHeaderValue(self):
        """
        Returns the Authorization Header for the Request

        Reference
          https://developer.twitter.com/en/docs/basics/authentication/guides/authorizing-a-request

        Returns
          Authorization Header String
          Ex. OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog",
              oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",
              oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D",
              oauth_signature_method="HMAC-SHA1",
              oauth_timestamp="1318622958",
              oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
              oauth_version="1.0"
        """
        valueDict = {**self.baseOAuthDict, "oauth_signature": self.getSignature()}
        keyString = ", ".join(
            "{}=\"{}\"".format(self.percentEncode(key), self.percentEncode(valueDict[key]))
            for key in valueDict)
        return "OAuth {}".format(keyString)

    # ------------------------- PERCENT ENCODE ------------------------------ #
    @staticmethod
    def percentEncode(val):
        """
        Return percent encoded value

        Reference:
          https://developer.twitter.com/en/docs/basics/authentication/guides/percent-encoding-parameters
        """
        return parse.quote(str(val), safe="")
