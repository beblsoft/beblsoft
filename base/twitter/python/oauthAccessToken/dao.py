#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter OAuth Access Token DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from urllib import parse
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.twitter.python.oauthAccessToken.models import BTwitterOAuthAccessTokenModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER OAUTH ACCESS TOKEN DAO ----------- #
class BTwitterOAuthAccessTokenDAO():
    """
    Beblsoft Twitter OAuth Access Token DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bTwitter, oauthRequestToken, oauthRequestTokenSecret, oauthVerifier):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          oauthRequestToken:
            OAuth Token Returned from BTwitterOAuthRequestTokenDAO.get
            Ex. D6c-_wAAAAABAV8EAAABbe-ygow
          oauthRequestToken:
            OAuth Token Secret Returned from BTwitterOAuthRequestTokenDAO.get
            Ex. qrLBK51NoIfxkec0e6NcEGioMvpgdY8l
          oauthVerifier:
            OAuth Verifier Token sent to OAuth Callback
            Ex. KZ3cVBUpQwPMqWUSHvWbJD9ess2qeZ3O

        Reference:
          https://developer.twitter.com/en/docs/basics/authentication/api-reference/access_token
          https://developer.twitter.com/en/docs/twitter-for-websites/log-in-with-twitter/guides/implementing-sign-in-with-twitter

        Returns
          BTwitterOAuthAccessTokenModel
        """
        resp                     = bTwitter.request(
            method                   = "POST",
            path                     = "https://api.twitter.com/oauth/access_token",
            oauthToken               = oauthRequestToken,
            oauthTokenSecret         = oauthRequestTokenSecret,
            params                   = {"oauth_verifier": oauthVerifier})


        # Parse Response into BTwitterOAuthAccessTokenModel
        # Resp.text="oauth_token=1172206846410727424-QPfXnpMHk1wPa9uwJVJg2q1a38lDgR&oauth_token_secret=TeWuBsUnCDDPuXw3q1Q1boVpaYyJGzfsF7mkM89OEUoHl&user_id=1172206846410727424&screen_name=SmecknInc"
        respDict                       = parse.parse_qs(qs=resp.text)
        oauthToken                     = respDict.get("oauth_token", [None])[0]
        oauthTokenSecret               = respDict.get("oauth_token_secret", [None])[0]
        userID                         = respDict.get("user_id", [None])[0]
        screenName                     = respDict.get("screen_name", [None])[0]
        bTwitterOAuthAcccessTokenModel = BTwitterOAuthAccessTokenModel(
            oauthToken                   = oauthToken,
            oauthTokenSecret             = oauthTokenSecret,
            userID                       = userID,
            screenName                   = screenName)

        # Handle Error Cases
        if not oauthToken or not oauthTokenSecret:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_OAUTH_BAD_TOKENS)
        if not userID or not screenName:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_REQUEST_NO_USER_ID)

        return bTwitterOAuthAcccessTokenModel
