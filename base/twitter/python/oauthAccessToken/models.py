#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter OAuth Access Token Models
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER OAUTH ACCESS TOKEN MODEL --------- #
class BTwitterOAuthAccessTokenModel(BTwitterCommonModel):
    """
    Beblsoft Twitter OAuth Access Token Model

    Reference:
      https://developer.twitter.com/en/docs/basics/authentication/api-reference/access_token
    """

    def __init__(self, oauthToken, oauthTokenSecret, userID, screenName):  #pylint: disable=W0231
        """
        Initialize object
        Args
          oauthToken:
            OAuth Token
            Ex. Z6eEdO8MOmk394WozF5oKyuAv855l4Mlqo7hhlSLik
          oauthTokenSecret:
            OAuth Token Secret
            Ex. Kd75W4OQfb2oJTV0vzGzeXftVAwgMnEK9MumzYcM
          userID:
            Twitter User ID
            Ex. "1172206846410727424"
          screenName:
            Twitter Screen Name
            Ex. "SmecknInc"
        """
        self.oauthToken       = oauthToken
        self.oauthTokenSecret = oauthTokenSecret
        self.userID           = userID
        self.screenName       = screenName

    DESCRIBE_PROPERTIES = ["oauthToken", "userID", "screenName"]
