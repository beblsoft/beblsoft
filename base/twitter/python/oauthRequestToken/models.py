#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter OAuth Request Token Models
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER OAUTH REQUEST TOKEN MODEL -------- #
class BTwitterOAuthRequestTokenModel(BTwitterCommonModel):
    """
    Beblsoft Twitter OAuth Request Token Model

    Reference:
      https://developer.twitter.com/en/docs/basics/authentication/api-reference/request_token
    """

    def __init__(self, oauthToken, oauthTokenSecret, oauthCallbackConfirmed):  #pylint: disable=W0231
        """
        Initialize object
        Args
          oauthToken:
            OAuth Token
            Ex. Z6eEdO8MOmk394WozF5oKyuAv855l4Mlqo7hhlSLik
          oauthTokenSecret:
            OAuth Token Secret
            Ex. Kd75W4OQfb2oJTV0vzGzeXftVAwgMnEK9MumzYcM
          oauthCallbackConfirmed
            OAuth Callback Confirmed
            Ex. True
        """
        self.oauthToken             = oauthToken
        self.oauthTokenSecret       = oauthTokenSecret
        self.oauthCallbackConfirmed = oauthCallbackConfirmed

    DESCRIBE_PROPERTIES = ["oauthToken", "oauthCallbackConfirmed"]
