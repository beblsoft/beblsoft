#!/usr/bin/env python3.6
"""
 NAME
  dao.py

 DESCRIPTION
  Beblsoft Twitter OAuth Request Token DAO Functionality
"""


# ----------------------- IMPORTS ------------------------------------------- #
import logging
from urllib import parse
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.twitter.python.bTwitterRequestOAuth import BeblsoftTwitterRequestOAuth
from base.twitter.python.oauthRequestToken.models import BTwitterOAuthRequestTokenModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER OAUTH REQUEST TOKEN DAO ---------- #
class BTwitterOAuthRequestTokenDAO():
    """
    Beblsoft Twitter OAuth Request Token DAO
    """

    # ------------------- GET ----------------------------------------------- #
    @staticmethod
    @logFunc()
    def get(bTwitter, oauthCallback, accessType="read", raiseOnUnconfirmedCallback=True):
        """
        Args:
          bTwitter:
            Beblsoft Twitter Object
          oauthCallback:
            Value specified here will be used as the URL a user is redirected to should they
            approve your application's access to their account.
            Ex. "http://themattharris.local/auth.php twitterclient://callback"
            Note: Callback must be whitelisted within the app settings
          accessType:
            Overrides the access level an application requests to a users account
            Supported values are "read" and "write"
          raiseOnUnconfirmedCallback:
            If True, raise error if callback is not confirmed
            Ex. True

        Reference:
          https://developer.twitter.com/en/docs/basics/authentication/api-reference/request_token

        Note:
          We need to create special BeblsoftTwitterRequestOAuth as this request doesn't
          have an oauthToken or oauthTokenSecret.

        Returns
          BTwitterOAuthRequestTokenModel
        """
        method                   = "POST"
        path                     = "https://api.twitter.com/oauth/request_token"
        params                   = {"x_auth_access_type": accessType}
        bTwitterRequestOAuth     = BeblsoftTwitterRequestOAuth(
            method                   = method,
            path                     = path,
            consumerAPIKey           = bTwitter.consumerAPIKey,
            consumerAPISecretKey     = bTwitter.consumerAPISecretKey,
            oauthCallback            = oauthCallback,
            requestParams            = params)
        resp                     = bTwitter.request(
            method                   = method,
            path                     = path,
            bTwitterRequestOAuth     = bTwitterRequestOAuth,
            params                   = params)

        # Parse Response into BTwitterOAuthRequestTokenModel
        # Resp.text="oauth_token=Z6eEdO8MOmk394WozF5oKyuAv855l4Mlqo7hhlSLik&oauth_token_secret=Kd75W4OQfb2oJTV0vzGzeXftVAwgMnEK9MumzYcM&oauth_callback_confirmed=true"
        respDict                       = parse.parse_qs(qs=resp.text)
        oauthToken                     = respDict.get("oauth_token", [None])[0]
        oauthTokenSecret               = respDict.get("oauth_token_secret", [None])[0]
        oauthCallbackConfirmed         = respDict.get("oauth_callback_confirmed", [None])[0]
        oauthCallbackConfirmed         = oauthCallbackConfirmed == "true"
        bTwitterOAuthRequestTokenModel = BTwitterOAuthRequestTokenModel(
            oauthToken                   = oauthToken,
            oauthTokenSecret             = oauthTokenSecret,
            oauthCallbackConfirmed       = oauthCallbackConfirmed)

        # Handle Error Cases
        if not oauthCallbackConfirmed and raiseOnUnconfirmedCallback:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_OAUTH_UNCONFIRMED_CALLBACK)
        if not oauthToken or not oauthTokenSecret:
            raise BeblsoftError(code=BeblsoftErrorCode.TWITTER_API_OAUTH_BAD_TOKENS)

        # Return Result
        return bTwitterOAuthRequestTokenModel
