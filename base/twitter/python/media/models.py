#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Media Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER MEDIA MODEL ---------------------- #
class BTwitterMediaModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Media Model
    Represents media elements uploaded with the Tweet.

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#media
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object

    photo obj Format ----------------------------------------------------------
    {
      "id": 861627472244162561,
      "id_str": "861627472244162561",
      "type": "photo",
      "media_url": "http://pbs.twimg.com/media/C_UdnvPUwAE3Dnn.jpg",
      "media_url_https": "https://pbs.twimg.com/media/C_UdnvPUwAE3Dnn.jpg",
      "url": "https://t.co/9r69akA484",
      "display_url": "pic.twitter.com/9r69akA484",
      "expanded_url": "https://twitter.com/FloodSocial/status/861627479294746624/photo/1",
      "indices": [
        68,
        91
      ],
      "sizes": {
        "medium": {
          "w": 1200,
          "h": 900,
          "resize": "fit"
        },
        "small": {
          "w": 680,
          "h": 510,
          "resize": "fit"
        },
        "thumb": {
          "w": 150,
          "h": 150,
          "resize": "crop"
        },
        "large": {
          "w": 2048,
          "h": 1536,
          "resize": "fit"
        }
      }
    }

    video obj Format ----------------------------------------------------------
    {
      "id": 869317980307415040,
      "id_str": "869317980307415040",
      "type": "video",
      "media_url": "http://pbs.twimg.com/ext_tw_video_thumb/869317980307415040/pu/img/t_E6wyADk_PvxuzF.jpg",
      "media_url_https": "https://pbs.twimg.com/ext_tw_video_thumb/869317980307415040/pu/img/t_E6wyADk_PvxuzF.jpg",
      "url": "https://t.co/TLSTTOvvmP",
      "display_url": "pic.twitter.com/TLSTTOvvmP",
      "expanded_url": "https://twitter.com/FloodSocial/status/869318041078820864/video/1",
      "indices": [
        31,
        54
      ],
      "sizes": {
        "small": {
          "w": 340,
          "h": 604,
          "resize": "fit"
        },
        "large": {
          "w": 720,
          "h": 1280,
          "resize": "fit"
        },
        "thumb": {
          "w": 150,
          "h": 150,
          "resize": "crop"
        },
        "medium": {
          "w": 600,
          "h": 1067,
          "resize": "fit"
        }
      },
      "video_info": {  // video_info or additional_media_info
        "aspect_ratio": [
          9,
          16
        ],
        "duration_millis": 10704,
        "variants": [
          {
            "bitrate": 320000,
            "content_type": "video/mp4",
            "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/180x320/FMei8yCw7yc_Z7e-.mp4"
          },
          {
            "bitrate": 2176000,
            "content_type": "video/mp4",
            "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/720x1280/octt5pFbISkef8RB.mp4"
          },
          {
            "bitrate": 832000,
            "content_type": "video/mp4",
            "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/360x640/2OmqK74SQ9jNX8mZ.mp4"
          },
          {
            "content_type": "application/x-mpegURL",
            "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/pl/wcJQJ2nxiFU4ZZng.m3u8"
          }
        ]
      }
      "additional_media_info": { // video_info or additional_media_info
        "title": "#ATLvsNYJ: Tomlinson TD from McCown",
        "description": "NFL",
        "embeddable": false,
        "monetizable": true
      },
    }

    animated_gif obj Format ---------------------------------------------------
    {
      "id": 870042654213459968,
      "id_str": "870042654213459968",
      "type": "animated_gif",
      "media_url": "http://pbs.twimg.com/tweet_video_thumb/DBMDLy_U0AAqUWP.jpg",
      "media_url_https": "https://pbs.twimg.com/tweet_video_thumb/DBMDLy_U0AAqUWP.jpg",
      "url": "https://t.co/nD6G4bWSKb",
      "display_url": "pic.twitter.com/nD6G4bWSKb",
      "expanded_url": "https://twitter.com/FloodSocial/status/870042717589340160/photo/1",
      "indices": [
        29,
        52
      ],
      "sizes": {
        "medium": {
          "w": 350,
          "h": 262,
          "resize": "fit"
        },
        "small": {
          "w": 340,
          "h": 255,
          "resize": "fit"
        },
        "thumb": {
          "w": 150,
          "h": 150,
          "resize": "crop"
        },
        "large": {
          "w": 350,
          "h": 262,
          "resize": "fit"
        }
      },
      "video_info": {
        "aspect_ratio": [
          175,
          131
        ],
        "variants": [
          {
            "bitrate": 0,
            "content_type": "video/mp4",
            "url": "https://video.twimg.com/tweet_video/DBMDLy_U0AAqUWP.mp4"
          }
        ]
      }
    }
    """
    DESCRIBE_PROPERTIES = ["id", "idStr", "type", "mediaURLHttps"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def id(self):  # pylint: disable=C0111
        return self.getValueFromObj("id")

    @property
    def idStr(self):  # pylint: disable=C0111
        return self.getValueFromObj("id_str")

    @property
    def type(self):  # pylint: disable=C0111
        """
        Return "photo", "video", or "animated_gif"
        """
        return self.getValueFromObj("type")

    @property
    def mediaURLHttps(self):  # pylint: disable=C0111
        return self.getValueFromObj("media_url_https")

    @property
    def url(self):  # pylint: disable=C0111
        return self.getValueFromObj("url")

    @property
    def displayURL(self):  # pylint: disable=C0111
        return self.getValueFromObj("display_url")

    @property
    def expandedURL(self):  # pylint: disable=C0111
        return self.getValueFromObj("expanded_url")

    @property
    def indices(self):  # pylint: disable=C0111
        return self.getValueFromObj("indices")

    @property
    def bTwitterVideoInfoModel(self):  # pylint: disable=C0111
        """
        Return corresponding BTwitterVideoInfoModel
        """
        bTwitterVideoInfoModel = None
        videoObj               = self.getValueFromObj("video_info")
        if videoObj:
            bTwitterVideoInfoModel = BTwitterVideoInfoModel(obj=videoObj)
        return bTwitterVideoInfoModel


# ----------------------- BEBLSOFT TWITTER VIDEO INFO MODEL ----------------- #
class BTwitterVideoInfoModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Video Info Model

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object

    obj Format:
    {
      "aspect_ratio": [
        9,
        16
      ],
      "duration_millis": 10704,
      "variants": [
        {
          "bitrate": 320000,
          "content_type": "video/mp4",
          "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/180x320/FMei8yCw7yc_Z7e-.mp4"
        },
        {
          "bitrate": 2176000,
          "content_type": "video/mp4",
          "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/720x1280/octt5pFbISkef8RB.mp4"
        },
        {
          "bitrate": 832000,
          "content_type": "video/mp4",
          "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/360x640/2OmqK74SQ9jNX8mZ.mp4"
        },
        {
          "content_type": "application/x-mpegURL",
          "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/pl/wcJQJ2nxiFU4ZZng.m3u8"
        }
      ]
    }
    """
    DESCRIBE_PROPERTIES = ["aspectRatio", "durationMS"]


    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def aspectRatio(self):  # pylint: disable=C0111
        return self.getValueFromObj("aspect_ratio")

    @property
    def durationMS(self): #pylint: disable=C0116
        return self.getValueFromObj("duration_millis")

    @property
    def bTwitterVideoVariantModelList(self):
        """
        Return [BTwitterVideoVariantModel]
        """
        bTwitterVideoVariantModelList = []
        variantObjList                = self.getValueFromObj("variants", default=[])
        for variantObj in variantObjList:
            bTwitterVideoVariantModelList.append(BTwitterVideoVariantModel(obj=variantObj))
        return bTwitterVideoVariantModelList

    @property
    def bTwitterVideoVariantModelHighestBitRate(self):
        """
        Return BTwitterVideoVariantModel with Highest Bitrate
        """
        highestBTwitterVideoVariantModel = None
        for curBTwitterVideoVariantModel in self.bTwitterVideoVariantModelList:
            curBitRate     = curBTwitterVideoVariantModel.bitRate
            highestBitRate = highestBTwitterVideoVariantModel.bitRate if highestBTwitterVideoVariantModel else None
            if not highestBitRate:
                highestBTwitterVideoVariantModel = curBTwitterVideoVariantModel
            elif curBitRate and curBitRate > highestBitRate:
                highestBTwitterVideoVariantModel = curBTwitterVideoVariantModel
        return highestBTwitterVideoVariantModel

    @property
    def bTwitterVideoVariantModelLowestBitRate(self):
        """
        Return BTwitterVideoVariantModel with Lowest Bitrate
        """
        lowestBTwitterVideoVariantModel = None
        for curBTwitterVideoVariantModel in self.bTwitterVideoVariantModelList:
            curBitRate    = curBTwitterVideoVariantModel.bitRate
            lowestBitRate = lowestBTwitterVideoVariantModel.bitRate if lowestBTwitterVideoVariantModel else None
            if not lowestBitRate:
                lowestBTwitterVideoVariantModel = curBTwitterVideoVariantModel
            elif curBitRate and curBitRate < lowestBitRate:
                lowestBTwitterVideoVariantModel = curBTwitterVideoVariantModel
        return lowestBTwitterVideoVariantModel


# ----------------------- BEBLSOFT TWITTER VIDEO VARIANT MODEL -------------- #
class BTwitterVideoVariantModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Video Model

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object

    obj Format:
    {
      "bitrate": 320000,
      "content_type": "video/mp4",
      "url": "https://video.twimg.com/ext_tw_video/869317980307415040/pu/vid/180x320/FMei8yCw7yc_Z7e-.mp4"
    },
    """
    DESCRIBE_PROPERTIES = ["bitRate", "contentType", "url"]


    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def bitRate(self):  # pylint: disable=C0111
        return self.getValueFromObj("bitrate")

    @property
    def contentType(self): #pylint: disable=C0116
        return self.getValueFromObj("content_type")

    @property
    def url(self): #pylint: disable=C0116
        return self.getValueFromObj("url")
