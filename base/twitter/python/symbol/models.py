#!/usr/bin/env python3
"""
 NAME
  models.py

 DESCRIPTION
  Beblsoft Twitter Symbol Model
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from base.twitter.python.common.models import BTwitterCommonModel


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT TWITTER SYMBOL MODEL --------------------- #
class BTwitterSymbolModel(BTwitterCommonModel): #pylint: disable=R0904
    """
    Beblsoft Twitter Symbol Model
    Represents symbols, i.e. $cashtags, included in the text of the Tweet

    Reference:
      https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object#symbols

    obj Format:
    {
      "indices": [
        12,
        17
      ],
      "text": "twtr"
    }
    """
    DESCRIBE_PROPERTIES = ["text"]

    # ------------------- PROPERTIES ---------------------------------------- #
    @property
    def text(self):  # pylint: disable=C0111
        return self.getValueFromObj("text")

    @property
    def indices(self):  # pylint: disable=C0111
        return self.getValueFromObj("indices")
