# Twitter Samples Documentation

Note all sample programs use the `BeblsoftDemo` Twitter Application.

## js-demo

Sample JavaScript application to demonstrate logging into twitter, printing twitter user profile,
and printing twitter user tweets.

Note: this app does the following things that should never be done in production:
- Stateful server that stores data globally
- Does not handle error cases gracefully

Relevant URLs:
[Twitter Login Flow](https://developer.twitter.com/en/docs/twitter-for-websites/log-in-with-twitter/guides/implementing-sign-in-with-twitter)

To run:

```bash
# Enter directory
cd ./js-demo

# Setup Virtual Environment
virtualenv -p /usr/bin/python3.6 /tmp/twitterJSDemo
source /tmp/twitterJSDemo/bin/activate
pip3 install -r requirements.txt

# Start Application
./app.py

# Open Chrome
# Go to http://127.0.0.1:3000/index
# Open the Chrome terminal

# Step 1:
# Click "Delete Server State"
# Client -> DELETE Server/serverState: This resets all of the server global data

# Step 2:
# Click "Login with Twitter"
# Client -> GET Server/oauthToken
# Server -> POST Twitter/oauth/request_token. Server then returns oauth request token to client.
# Client opens popup window that redirects to Twitter/oauth/authenticate?oauth_token=${oauthToken}
# Login with Twitter Credentials.
# After Successful login. Twitter -> Server/oauthCallback&oauth_verifier=asdflkjh.
# Server -> POST Twitter/oauth/access_token to generate client oauth access token
# Server -> GET Twitter/account/verify_credentials.json to verify access token
# Server returns callback.html which closes the popup
# Client detects that the popup has been closed

# Step 3:
# Click "Get Twitter Profile"
# Client -> GET Server/twitterProfile.
# Server -> GET Twitter/users/show. Server returns user data to client.

# Step 4:
# Click "Get Twitter Tweets"
# Client -> GET Server/twitterTweet.
# Server -> GET Twitter/users/show. Server returns tweet data to client.

# Stop Application

# Deactivate Venv
deactivate
```

## vue-demo

Sample Vue client application that talks to a python server. Demonstrate logging into twitter, printing
twitter user profile, and printing twitter user tweets.

### Server

To run the server:

```bash
# Enter directory
cd ./vue-demo/server

# Setup Virtual Environment
virtualenv -p /usr/bin/python3.6 /tmp/twitterVueDemo
source /tmp/twitterVueDemo/bin/activate
pip3 install -r requirements.txt

# Start Application
./app.py

# Run client in another terminal (see below)..

# Stop Server
# Ctrl + C

# Deactivate Venv
deactivate
```

### Client

To run the client:

```bash
# Enter directory
cd ./vue-demo/client

# Install Application
npm install

# Start Application
npm run serve

# Open Chrome
# Go to http://127.0.0.1:8080
# Open the Chrome terminal

# Step 1:
# Click "Delete Server State"
# Client -> DELETE Server/serverState: This resets all of the server global data

# Step 2:
# Click "Login with Twitter"
# Client -> GET Server/oauthRequestToken
# Server -> POST Twitter/oauth/request_token. Server then returns oauth request token to client.
# Client opens popup window that redirects to Twitter/oauth/authenticate?oauth_token=${oauthRequestToken}
# Login with Twitter Credentials.
# After Successful login. Twitter -> Client/OAuthCallback&oauth_verifier=asdflkjh.
# Client -> POST Server/oauthAccessToken?oauth_verifier=${this.oauth_verifier}
# Server -> POST Twitter/oauth/access_token to generate client oauth access token
# Server -> GET Twitter/account/verify_credentials.json to verify access token
# Server returns to Client/OauthCallback which then closes the popup
# Client detects that the popup has been closed

# Step 3:
# Click "Get Twitter Profile"
# Client -> GET Server/twitterProfile.
# Server -> GET Twitter/users/show. Server returns user data to client.
# Client renders user data

# Step 4:
# Click "Get Twitter Tweets"
# Client -> GET Server/twitterTweet.
# Server -> GET Twitter/users/show. Server returns tweet data to client.
# Client renders the tweet data

# Stop Application
# Ctrl + C
```

Relevant Files:

```bash
$ ./vue-demo/client/src
.
├── index.scss
├── main.js
├── App.vue
├── Home.vue                         - Home component with buttons
├── OAuthCallback.vue                - Handles OAuth Redirect after twitter authenticates user
├── router.js
└── TwitterProfile.vue               - Render Twitter Profile
├── TwitterCustomTweet.vue           - Custom rendering of tweet
├── TwitterCustomTweetBody.vue       - Custom rendering of tweet body
├── TwitterCustomTweetMixin.js       - Custom rendering mixin
├── TwitterCustomTweetSeparator.vue
├── TwitterEmbeddedTweet.vue         - Leverage 'vue-embed-tweet' for Twitter oembed tweet rendering
```

## python-demo

Sample python command line interface to send requests directly to twitter.

To run:

```bash
# Enter directory
cd ./python-demo

# Setup Virtual Environment
virtualenv -p /usr/bin/python3.6 /tmp/twitterPythonDemo
source /tmp/twitterPythonDemo/bin/activage
pip3 install -r requirements.txt

# Get Help
./cli.py -h

# Request Oauth Token
./cli.py  oauth-requesttoken

# Get Rate Limit Status
./cli.py  oauth-requestoken

# Deactivate Venv
deactivate
```