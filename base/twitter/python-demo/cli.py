#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Twitter Server Sample Command Line Interface
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.twitter.python.bTwitter import BeblsoftTwitter
from base.twitter.python.rateLimitStatus.dao import BTwitterRateLimitStatusDAO
from base.twitter.python.oauthRequestToken.dao import BTwitterOAuthRequestTokenDAO
from base.twitter.python.user.dao import BTwitterUserDAO
from base.twitter.python.tweet.dao import BTwitterTweetDAO


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Twitter API -----------------------------
        self.bTwitter          = BeblsoftTwitter(
            appID                  = 16867076,  # BeblsoftDemo
            consumerAPIKey         = "q8sc8TbIVRWRqSI1QibEeJWTN",
            consumerAPISecretKey   = "Ubzg6yGHWTM2q4QgpLglv804zSn2bWb9Fc5bZYrrmaBF10tUi2",
            appAccessToken         = "1172206846410727424-QPfXnpMHk1wPa9uwJVJg2q1a38lDgR",
            appAccessTokenSecret   = "TeWuBsUnCDDPuXw3q1Q1boVpaYyJGzfsF7mkM89OEUoHl",
            version                = 1.1)
        self.oauthCallbackURL  = "http://127.0.0.1:3000"
        self.userScreenName    = "FrankBar13"
        self.tweetID           = "1186970145626959873"
        self.tweetIDList       = ["1186970145626959873", "1187370648261267456", "1187674940486672384"]

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None


logger         = logging.getLogger(__name__)
gc             = GlobalContext()
defaultLogFile = "/tmp/twitterPythonServerDemo.log"


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]), options_metavar="[options]")
@click.option("--logfile", default=defaultLogFile, type=click.Path(), help="Specify log file Default={}".format(defaultLogFile))
@click.option("-a", '--appendlog', is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2", type=click.Choice(gc.cLogFilterMap.keys()), help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Twitter Python Server Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- RATE LIMIT STATUS --------------------------------- #
@cli.command()
def ratelimitstatus_get():
    """
    Get rate limit status
    """
    bTwitterRateLimitStatusModel = BTwitterRateLimitStatusDAO.get(bTwitter=gc.bTwitter, useAppAccessTokens=True)
    bTwitterRateLimitStatusModel.describe()


# ----------------------- OAUTH REQUEST TOKEN ------------------------------- #
@cli.command()
def oauth_requesttoken():
    """
    Request oauth token
    """
    bTwitterOAuthRequestTokenModel = BTwitterOAuthRequestTokenDAO.get(
        bTwitter=gc.bTwitter, oauthCallback=gc.oauthCallbackURL)
    bTwitterOAuthRequestTokenModel.describe()


# ----------------------- USER ---------------------------------------------- #
@cli.command()
def user_get():
    """
    Get user object
    """
    bTwitterUserModel = BTwitterUserDAO.get(bTwitter=gc.bTwitter, screenName=gc.userScreenName, useAppAccessTokens=True)
    bTwitterUserModel.describe()


@cli.command()
def user_timeline_get():
    """
    Get user timeline tweets
    """
    bTwitterTweetModelList = BTwitterUserDAO.getAllTimelineTweets(bTwitter=gc.bTwitter, screenName=gc.userScreenName,
                                                                  useAppAccessTokens=True)
    for bTwitterTweetModel in bTwitterTweetModelList:
        # bTwitterTweetModel.describe()
        print(bTwitterTweetModel.idStr)
        print(bTwitterTweetModel.getParsedText())
        print()
        # if "Quoting a retweet" in bTwitterTweetModel.getFullText():
        #     import pprint
        #     print(pprint.pformat(bTwitterTweetModel.obj))

# ----------------------- TWEET --------------------------------------------- #
@cli.command()
def tweet_getbyid():
    """
    Get tweet object by id
    """
    bTwitterTweetModel = BTwitterTweetDAO.getByID(bTwitter=gc.bTwitter, tweetID=gc.tweetID, useAppAccessTokens=True)
    bTwitterTweetModel.describe()
    # Hashtag
    for bTwitterHashtagModel in bTwitterTweetModel.bTwitterHashtagModelList:
        bTwitterHashtagModel.describe()
    # Symbol
    for bTwitterSymbolModel in bTwitterTweetModel.bTwitterSymbolModelList:
        bTwitterSymbolModel.describe()
    # Media
    for bTwitterMediaModel in bTwitterTweetModel.bTwitterMediaModelList:
        bTwitterMediaModel.describe()
    # Poll
    for bTwitterPollModel in bTwitterTweetModel.bTwitterPollModelList:
        bTwitterPollModel.describe()
    # User Mention
    for bTwitterUserMentionModel in bTwitterTweetModel.bTwitterUserMentionModelList:
        bTwitterUserMentionModel.describe()
    # URL
    for bTwitterURLModel in bTwitterTweetModel.bTwitterURLModelList:
        bTwitterURLModel.describe()


@cli.command()
def tweet_lookupidlist():
    """
    Lookup tweets by id list
    """
    bTwitterTweetModelList = BTwitterTweetDAO.lookupIDList(bTwitter=gc.bTwitter, tweetIDList=gc.tweetIDList,
                                                           useAppAccessTokens=True)
    for bTwitterTweetModel in bTwitterTweetModelList:
        bTwitterTweetModel.describe()


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
