/*
  NAME
    TwitterCustomTweetMixin.js

  DESCRIPTION
    Base Class for Twitter Custom Tweet Components that deal with a single
    TwitterTweet Objec
*/


/* ------------------------ IMPORTS ---------------------------------------- */
import _ from 'lodash';
import moment from 'moment';

/* ------------------------ EXPORTS ---------------------------------------- */
export default {
  /* ---------------------- PROPS ------------------------------------------ */
  props: {
    // Twitter Tweet Object
    // Reference: https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
    twitterTweet: {
      type: Object,
      required: true
    },
  },

  /* ---------------------- COMPUTED --------------------------------------- */
  computed: {

    /* -------------------- TWEET ------------------------------------------ */
    // Reference: https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
    /**
     * @return {String} Type of Tweet: 'REGULAR', 'QUOTE', 'RETWEET', or 'REPLY'
     */
    tweetType() {
      let rval = 'REGULAR';
      if (this.twitterTweet.quoted_status) {
        rval = 'QUOTE'
      } else if (this.twitterTweet.retweeted_status) {
        rval = 'RETWEET'
      } else if (this.twitterTweet.in_reply_to_status_id_str) {
        rval = 'REPLY'
      }
      return rval;
    },
    tweetID() {
      return this.twitterTweet.id_str;
    },
    tweetCreateDateString() {
      return moment(this.twitterTweet.created_at, 'ddd MMM DD HH:mm:ss ZZ YYYY').format('LLL')
    },
    tweetLink() {
      return `https://twitter.com/i/web/status/${this.twitterTweet.id_str}`;
    },
    tweetText() {
      return this.twitterTweet.full_text;
    },
    tweetRetweetCount() {
      return this.twitterTweet.retweet_count;
    },
    tweetFavoriteCount() {
      return this.twitterTweet.favorite_count;
    },
    quotedTweet() {
      return this.twitterTweet.quoted_status;
    },
    reTweetedTweet() {
      return this.twitterTweet.retweeted_status;
    },
    tweetRepliedToTweetID() {
      return this.twitterTweet.in_reply_to_status_id_str
    },
    tweetRepliedToUserID() {
      return this.twitterTweet.in_reply_to_user_id_str
    },
    tweetRepliedToUserScreenName() {
      return this.twitterTweet.in_reply_to_screen_name
    },

    /* -------------------- TWEET ENTITIES --------------------------------- */
    // Reference: https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object
    tweetEntities() {
      return this.twitterTweet.entities;
    },

    /* -------------------- TWEET MEDIA ------------------------------------ */
    // Reference: https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object
    tweetPhotoMediaList() {
      let rval = []
      try {
        rval = _.filter(this.twitterTweet.extended_entities.media, { type: 'photo' });
      } catch (err) { /* No extended entities */ }
      return rval;
    },
    tweetVideoMediaList() {
      let rval = []
      try {
        rval = _.filter(this.twitterTweet.extended_entities.media, { type: 'video' });
      } catch (err) { /* No extended entities */ }
      return rval;
    },
    tweetAnimatedGifMediaList() {
      let rval = []
      try {
        rval = _.filter(this.twitterTweet.extended_entities.media, { type: 'animated_gif' });
      } catch (err) { /* No extended entities */ }
      return rval;
    },

    /* -------------------- TWEET AUTHOR ----------------------------------- */
    tweetAuthorName() {
      return this.twitterTweet.user.name;
    },
    tweetAuthorScreenName() {
      return this.twitterTweet.user.screen_name;
    },
    tweetAuthorProfilePictureLink() {
      return this.twitterTweet.user.profile_image_url_https
    },
    tweetAuthorLink() {
      return `https://twitter.com/${this.twitterTweet.user.screen_name}`;
    },
  },

  /* ---------------------- METHODS ---------------------------------------- */
  methods: {
    onTweetLinkClicked() {
      window.open(this.tweetLink);
    },
    onTweetAuthorLinkClicked() {
      window.open(this.tweetAuthorLink);
    },
  }
};
