# Image Documentation

## Flaticon Attribution

All images must be purchased before going live to production.

Icon made by [author link] from www.flaticon.com

| Image       | Link                                                                                              | Description |
|:------------|:--------------------------------------------------------------------------------------------------|:------------|
| heart.png   | https://www.flaticon.com/free-icon/like_148836#term=heart&page=1&position=1                       |             |
| retweet.png | https://www.flaticon.com/free-icon/retweet_1388997                                                |             |
| twitter.png | https://www.flaticon.com/free-icon/twitter_174876#term=twitter&page=1&position=2                  |             |
| comment.png | https://www.flaticon.com/free-icon/comment-white-oval-bubble_25663#term=comment&page=1&position=6 |             |
| more.png    | https://www.flaticon.com/free-icon/show-more-button-with-three-dots_61140                         |             |
