import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './Home.vue'
import OAuthCallback from './OAuthCallback.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/oauthCallback',
      name: 'oauthCallback',
      component: OAuthCallback,
      props: (route) => ({
        denied: route.query.denied,
        oauth_verifier: route.query.oauth_verifier
      })
    }
  ]
})

export default router;
