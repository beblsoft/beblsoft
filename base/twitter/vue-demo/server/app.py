#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Flask-RestPlus ReCaptcha Server
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import logging
from flask import Flask, Response
from flask_cors import CORS
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401
from base.twitter.python.bTwitter import BeblsoftTwitter
from base.twitter.python.oauthRequestToken.dao import BTwitterOAuthRequestTokenDAO
from base.twitter.python.oauthAccessToken.dao import BTwitterOAuthAccessTokenDAO
from base.twitter.python.verifyAccountCredentials.dao import BTwitterVerifyAccountCredentialsDAO
from base.twitter.python.user.dao import BTwitterUserDAO


# ------------------------ GLOBALS ------------------------------------------ #
app                            = Flask(__name__)
api                            = Api(app)
logger                         = logging.getLogger()
serverPort                     = 3000
clientPort                     = 8080
bTwitter                       = BeblsoftTwitter(
    appID                          = 16867076,  # BeblsoftDemo
    consumerAPIKey                 = "q8sc8TbIVRWRqSI1QibEeJWTN",
    consumerAPISecretKey           = "Ubzg6yGHWTM2q4QgpLglv804zSn2bWb9Fc5bZYrrmaBF10tUi2",
    appAccessToken                 = "1172206846410727424-QPfXnpMHk1wPa9uwJVJg2q1a38lDgR",
    appAccessTokenSecret           = "TeWuBsUnCDDPuXw3q1Q1boVpaYyJGzfsF7mkM89OEUoHl",
    version                        = 1.1)
oauthRequestToken              = None
oauthRequestTokenSecret        = None
bTwitterOAuthAcccessTokenModel = None
oauthCallback                  = "http://127.0.0.1:{}/oauthCallback?foo=bar&baz=laz".format(clientPort)
fileDirectory                  = os.path.dirname(os.path.realpath(__file__))


# ------------------------ INDEX ROUTE -------------------------------------- #
@api.route("/index")
class IndexResource(Resource):
    """
    Index Routes
    """

    @api.representation("text/html")
    def get(self):  # pylint: disable=R0201
        """
        Handle get
        """
        data      = ""
        indexFile = "{}/{}".format(fileDirectory, "index.html")
        with open(indexFile, "r") as f:
            data = f.read()
        resp = Response(data, mimetype="text/html", headers=None)
        return resp


# ------------------------ SERVER STATE ROUTES ------------------------------ #
@api.route("/serverState")
class ServerStateResource(Resource):
    """
    Server State Resource
    """

    def delete(self):  # pylint: disable=R0201
        """
        Handle delete
        """
        global oauthRequestToken
        global oauthRequestTokenSecret
        global bTwitterOAuthAcccessTokenModel
        oauthRequestToken              = None
        oauthRequestTokenSecret        = None
        bTwitterOAuthAcccessTokenModel = None


# ------------------------ OAUTH REQUEST TOKEN ROUTES ----------------------- #
oauthRequestTokenGetOutputModel = api.model("OAuthRequestTokenGetOutputModel", {
    "oauthRequestToken": fields.String(
        title       = "OAuth Request Token",
        description = "OAuth Request Token",
        required    = True,
        example     = "Z6eEdO8MOmk394WozF5oKyuAv855l4Mlqo7hhlSLik")
})


@api.route("/oauthRequestToken")
class OAuthRequestTokenResource(Resource):
    """
    OAuth Request Token Routes
    """

    @api.marshal_with(oauthRequestTokenGetOutputModel)
    def get(self):  # pylint: disable=R0201
        """
        Handle get
        """
        global oauthRequestToken
        global oauthRequestTokenSecret
        bTwitterOAuthRequestTokenModel = BTwitterOAuthRequestTokenDAO.get(bTwitter=bTwitter,
                                                                          oauthCallback=oauthCallback)
        oauthRequestToken              = bTwitterOAuthRequestTokenModel.oauthToken
        oauthRequestTokenSecret        = bTwitterOAuthRequestTokenModel.oauthTokenSecret
        return {"oauthRequestToken": oauthRequestToken}


# ------------------------ OAUTH ACCESS TOKEN ROUTES ------------------------ #
oauthAccessTokenPostParser = api.parser()
oauthAccessTokenPostParser.add_argument(
    "oauth_verifier",
    type         = str,
    required     = False,
    default      = None,
    location     = "args")


@api.route("/oauthAccessToken")
class OAuthAccessTokenResource(Resource):
    """
    OAuth Access Token Routes
    """

    @api.expect(oauthAccessTokenPostParser)
    @api.representation("text/html")
    def post(self):  # pylint: disable=R0201
        """
        Handle post
        """
        global bTwitterOAuthAcccessTokenModel
        args                           = oauthAccessTokenPostParser.parse_args()
        oauthVerifier                  = args.get("oauth_verifier")

        # Get Access Tokens, update globals
        bTwitterOAuthAcccessTokenModel = BTwitterOAuthAccessTokenDAO.get(
            bTwitter                      = bTwitter,
            oauthRequestToken             = oauthRequestToken,
            oauthRequestTokenSecret       = oauthRequestTokenSecret,
            oauthVerifier                 = oauthVerifier)

        # Verify Credentials
        validCredentials               = BTwitterVerifyAccountCredentialsDAO.credentialsAreValid(
            bTwitter                      = bTwitter,
            oauthToken                    = bTwitterOAuthAcccessTokenModel.oauthToken,
            oauthTokenSecret              = bTwitterOAuthAcccessTokenModel.oauthTokenSecret)
        assert(validCredentials)


# ------------------------ TWITTER PROFILE ---------------------------------- #
@api.route("/twitterProfile")
class TwitterProfileResource(Resource):
    """
    Twitter Profile Routes
    """

    def get(self):  # pylint: disable=R0201
        """
        Handle get
        """
        bTwitterUser = BTwitterUserDAO.get(
            bTwitter         = bTwitter,
            userID           = bTwitterOAuthAcccessTokenModel.userID,
            oauthToken       = bTwitterOAuthAcccessTokenModel.oauthToken,
            oauthTokenSecret = bTwitterOAuthAcccessTokenModel.oauthTokenSecret)
        return bTwitterUser.obj


# ------------------------ TWITTER TWEETS ----------------------------------- #
@api.route("/twitterTweet")
class TwitterTweetResource(Resource):
    """
    Twitter Tweet Routes
    """

    def get(self):  # pylint: disable=R0201
        """
        Handle get
        """
        bTwitterTweetList = BTwitterUserDAO.getAllTimelineTweets(
            bTwitter         = bTwitter,
            userID           = bTwitterOAuthAcccessTokenModel.userID,
            oauthToken       = bTwitterOAuthAcccessTokenModel.oauthToken,
            oauthTokenSecret = bTwitterOAuthAcccessTokenModel.oauthTokenSecret)
        return [bTwitterTweet.obj for bTwitterTweet in bTwitterTweetList]


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    CORS(app)
    logger.setLevel(level=logging.NOTSET)
    app.run(debug=True, port=serverPort)
