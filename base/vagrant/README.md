# Vagrant Documentation

Vagrant is a tool for building an managing virtual machine environments in a single workflow.
With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup
time, increases production parity, and makes the "works on my machine" excuse a relic of the past.

Relevant URLs:
[Home](https://www.vagrantup.com/),
[Getting Started](https://www.vagrantup.com/intro/getting-started/index.html),
[Docs](https://www.vagrantup.com/docs/index.html),
[Downloads](https://www.vagrantup.com/downloads.html),
[Box Cloud Catalog](https://app.vagrantup.com/boxes/search),

## Why Vagrant?

Vagrant provices easy to configure, reproducible, and portable work environments built on top of
industry standard technology and controlled by a single consistent workflow to help maximize the
productivity and flexibility of you and your team.

## Getting Started

### Installation

To use vagrant locally with virtualbox, virtual box must be installed. To install it see `virtualBox.sh`.

To install vagrant, see `vagrant.sh`.

```bash
# Install
vagrant_install

# Uninstall
vagrant_uninstall
```

### Project Setup

The first step in configuring any Vagrant project is to create a Vagrantfile. The purpose of the Vagrantfile
is twofold:

1. Mark the root directory of your project. Many of the configuration options in Vagrant are relative
   to this root directory.

2. Describe the kind of machine and resources you need to run your project, as well as what software
   to install and how you want to access it.

`vagrant init` is a built-in command for initializing a directory for usage with Vagrant.
The Vagranfile is meant to be committed to version control with your project. This way, every person
working with that project can benefit from Vagrant without any upfront work.

### Boxes

Instead of building a virtual machine from scratch, which would be a slow and tedious process, Vagrant
uses a base image to quickly clone a virtual machine. These base images are known as "boxes" in Vagrant,
and specifying the box to use for your vagrant environment is always the first step after creating
a new Vagrantfile.

`vagrant box add hashicorp/precise64` stores a box under a specific name so that multiple Vagrant
environments can re-use it. This command will download the box named "hashicorp/precise64" from
HashiCorp's Vagrant Cloud box catalog.

Boxes are globally stored for the current user. Box names are namespaced `<username>/<boxname>`.

#### Using a Box

Can specify an explicit version of the box as follows:
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise64"
  config.vm.box_version = "1.1.0"
end
```

Can specify a URL to a box directly:
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise64"
  config.vm.box_url = "https://vagrantcloud.com/hashicorp/precise64"
end
```

### Up, SSH, and Destroy

In less than a minute, `vagrant up` will have a virtual machine up and running.

Since Vagrant runs the machine headlessly, you wont be able to see anything. Use `vagrant ssh`
to log into the machine.

To teardown the machine, run `vagrant destroy`.

### Synced Folders

While its cool to have a virtual machine so easily, not many people want to edit files using just
plain terminal-based editors over SSH. Luckily with Vagrant you do not have to.

By using _synced folders_, Vagrant will automatially sync your files to and from the guest machine.
By default, Vagrant shares your project directory to the `/vagrant` directory in your guest machine.

With synced folders, you can continue to use your own editor on your host machine and have the files
sync into the guest machine!

### Provisioning

Vagrant has built-in support for _automated provisioning_. Using this feature, Vagrant will automatically
install software when you `vagrant up` so that the guest machine can be repeatebly created and ready-to-use.

For example, the following Vagrantfile will run `boostrap.sh`:
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise64"
  config.vm.provision :shell, path: "bootstrap.sh"
end
```

For complex provisioning scripts, it may be more efficient to package a custom Vagrant box with those
packages pre-installed instead of building them each time.

### Networking

Vagrant has various networking options for accessing the underlying remote machine.

One option is _port forwarding_. Port forwarding allows you to specify ports on the guest machine
to share via a port on the host machine. This allows you to access a port on your own machine, but
actually have all the network traffic forwarded to a specific port on the guest machine.

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise64"
  config.vm.provision :shell, path: "bootstrap.sh"
  config.vm.network :forwarded_port, guest: 80, host: 4567
end
```

### Share

Vagrant Share makes it easy to share and collaborate environments. Vagrant Share lets you share your
Vagrant environemnt to anyone around the world with an internet connection. It will give you a URL
that will route directly to your Vagrant environment from any device in the world that is connected
to the internet.

To share the vagrant environment:
```bash
vagrant share
```

### Teardown

Several ways to stop the virtual machine:

- __Suspending__: `vagrant suspend` will save the current running state of the machine and stop it.
  `vagrant up` will then resume where you left of. Upside is that it is quick (5-10s) to start up.
  Downside is that VM eats up disk space.

- __Halting__: `vagrant halt` gracefully shuts down the guest operating system and powers down the guest
  machine. The benefit of this method is that it will cleanly shut down the machine, preserving the
  contents of disk, and allowing it to be cleanly started again. The downside is that it'll take some
  extra time to start from a cold boot, and the guest machine still consumes disk space.

- __Destroying__: `vagrant destroy` will remove all traces of the gues machine from your system. It'll
  stop the guest machine, power it down, and remove all of the guest hard disks. Again, when you are ready
  to work again, just issue a `vagrant up`. The benefit of this is that _no cruft_ is left on your machine.
  The disk space and RAM consumed by the guest machine is reclaimed and your host machine is left clean.
  The downside is that `vagrant up` takes time to reimport and re-provision.

### Providers

In the getting started guide, your project was always backed with VirtualBox. But Vagrant can work
with a wide variety of backend providers, such as VMware, Hyper-V, and more.

Once you have a provider installed, you do not need to make any modifications to your Vagrantfile,
just `vagrant up` with the propert provider and Vagrant will do the rest.

```bash
$ vagrant up --provider=vmware_fusion
```

Once you run `vagrant up` with another provider, every other Vagrant command does not need to be told
what provider to use. Vagrant can automatically figure it out.

## Command-Line Interface

Almost all interaction with Vagrant is done through the command-line interface. The interface is
available using the `vagrant` command, and comes installed with Vagrant automatically.

### `vagrant box add ADDRESS`

Adds a box with the given address to Vagrant.

### `vagrant box list`

Lists all the boxes that are installed into Vagrant.

### `vagrant box outdated`

Tells you whether or not the  box you are using is outdated.

### `vagrant box prune`

Removes old versions of installed boxes

### `vagrant box remove NAME`

Removes a box from Vagrant that matches the given name

### `vagrant box repackage NAME PROVIDER VERSION`

Repackages the given box and puts in in the current directory so you can redistribute it.

### `vagrant box update`

Updates the box for the current Vagrant environment if there are updates available

### `vagrant cloud auth login`

Login to vagrant cloud

### `vagrant cloud auth logout`

Logout from vagrant cloud

### `vagrant cloud auth whoami`

Print current user.

### `vagrant cloud box create ORGANIZATION/BOX-NAME`

Create a new box in Vagrant cloud

### `vagrant cloud box delete ORGANIZATION/BOX-NAME`

Permanently delete box in cloud

### `vagrant cloud box show ORGANIZATION/BOX-NAME`

Display box details.

### `vagrant cloud box update ORGANIZATION/BOX-NAME`

Update box

### `vagrant cloud provider create ORGANIZATION/BOX-NAME PROVIDER-NAME VERSION [URL]`

Create new provider entry on Vagrant cloude.

### `vagrant cloud provider delete ORGANIZATION/BOX-NAME PROVIDER-NAME VERSION`

Delete provider entry.

### `vagrant cloud provider update ORGANIZATION/BOX-NAME PROVIDER-NAME VERSION [URL]`

Update provider entry

### `vagrant cloud provider upload ORGANIZATION/BOX-NAME PROVIDER-NAME VERSION BOX-FILE`

Upload box file for specified version and provider

### `vagrant cloud publish ORGANIZATION/BOX-NAME VERSION PROVIDER-NAME [PROVIDER-FILE]`

Create and update a Vagrant box on Vagrant cloud.

### `vagrant cloud search QUERY`

Search vargrant cloud for matching vagrant boxes.

### `vagrant cloud version`

Used to manage life cycle operations for all version entities for a box on Vagrant cloud.

### `vagrant connect`

The connect command complements the share command by enabling access to shared environments.

### `vagrant destroy [name|id]`

Stops the running machine Vagrant is managing and destroys all resources that were created during the machine
creation process.

### `vagrant global-status`

Tells you the state of all active Vagrant environments on the system.

### `vagrant halt [name|id]`

Shots down the running machine Vagrant is managing

### `vagrant init [name [url]]`

Initializes the current directory to be a Vagrant environment by creating an initial Vagrantfile
if one does not already exist.

### `vagrant login`

Used to authenticate with HashiCorp's Vagrant Cloud server.

### `vagrant package [name|id]`

This packages a currently running VirtualBox or Hyper-V environment into a re-usable box.

### `vagrant plugin expunge`

Removes all user installed plugin information. All plugin gems, their dependencies, and the `plugins.json`
file are removed.

### `vagrant plugin install <name> ...`

Installs a plugin with the given name or file path.

### `vagrant plugin license <name> <license-file>`

Installs a license for a proprietary Vagrant plugin.

### `vagrant plugin list`

Kusts all installed plugins and their respective versions.

### `vagrant plugin repair`

Vagrant may fail to properly initialize user installed custom plugins. This can be caused by plugin
installation/removal, or by manual manipulation of plugin related files. Vagrant can attempt to automatically
repair the problem.

### `vagrant plugin uninstall <name> [<name2> <name3> ...]`

Uninstalls the plugin with the given name.

### `vagrant plugin update [<name>]`

Updates the plugins that are installed within vagrant.

### `vagrant port [name|id]`

Displays the full list of guest ports mapped to host machine ports.

### `vagrant powershell`

Opens a PowerShell prompt on the host into a running Vagrant guest machine.

### `vagrant provision [vm-name]`

Runs any configured provisioners agains the running Vagrant managed machine.

### `vagrant rdb`

This will start an RDP client for a remote desktop session with the guest. This only works for Vagrant
environments that support remote desktop, which is typically only Windows.

### `vagrant reload [name|id]`

The equivalent of running a `halt` followed by an `up`.

### `vagrant resume [name|id]`

This resumes a Vagrant managed machine that was previously suspended.

### `vagrant share`

The share command initializes a Vagrant Share session, allowing you to share you environment with anyone
in the world, enabling collaboration directly in your Vagrant environment in almost any network environment.

### `vagrant snapshot push`

Takes a snapshot and pushes it onto the snapshot stack.

### `vagrant snapshot pop`

This command is the inverse of `vagrant snapshot push`. It will restore the pushed state.

### `vagrant snapshot save [vm-name] NAME`

This command saves a new named snapshot.

### `vagrant snapshot restore [vm-name] NAME`

This command restores the named snapshot.

### `vagrant snapshot list`

This command will list all the snapshots taken.

### `vagrant snapshot delete [vm-name] NAME`

This command will delete the named snapshot.

### `vagrant ssh [name|id] [-- extra_ssh_args]`

SSH into a running Vagrant machine and give you access to a shell. On a simple vagrant project, the
instance will be named default.

### `vagrant ssh-config [name|id]`

This will output valid configuration for an SSH config file to SSH into the running Vagrant machine
from ssh directly (instead of using `vagrant ssh`)

### `vagrant stats [name|id]`

This will tell you the state of the machines Vagrant is managing.

### `vagrant suspend [name|id]`

This suspends the guest machine Vagrant is managing, rather than fully shutting it down or destryoying it.

A suspend effectively saves the exact point-in-time state of the machine, so that when you resume it later,
it begines running immediately from that point, rather than doing a full boot.

### `vagrant up [name|id]`

This command creates and configures guest machines according to your Vagrantfile.

### `vagrant upload source [destination] [name|id]`

Command uploads files and directories from the host to the guest machine.

### `vagrant validate`

Command validates your Vagrantfile.

### `vagrant version`

This command tells you the version of Vagrant you have installed as well as the latest version of
Vagrant that is currently available.

## Vagrant Share

Vagrant Share allows you to share your Vagrant environment with anyone in the world, enabling collaboration
directly on your Vagrant environment in always any network environment with a single command:
`vagrant share`.

Vagrant share has  three primary modes:

- __HTTP sharing__ will create a URL that you can give to anyone. This URL will route directly to
  your vagrant environment

- __SSH sharing__ will allow instant SSH access to your vagrant environment by anyone by running
  `vagrant connect --ssh` on the remote side.

- __General sharing__ allows anyone to access any exposed port of your Vagrant environment by running
  `vagrant connect` on the remote side. This is useful if the remote side wants to access your Vagrant
  environment as if it were a computer on the LAN.

### Installation

Vagrant share is a Vagrant plugin that must be installed. It is not included with Vagrant system
packages. To install run the following command:

```bash
vagrant plugin install vagrant-share
```

## Vagrantfile

The primary function of the Vagrantfile is to describe the type of machine required for a project,
and how to configure and provision these machines. Vagrantfiles are called Vagrantfiles because the
actual literal filename for the file is `Vagrantfile` (casing does not matter unless your file system
is running in a strict case sensitive mode).

Vagrant is meant to run with one Vagrantfile per project, and the Vagrantfile is supposed to be committed
to version control. This allows other developers involved in the project to check out the code, run `vagrant up`,
and be on their way. Vagrantfiles are portable across every platform Vagrant supports.

The syntax of Vagrantfiles is Ruby.

### Configuration Version

If you run `vagrant init` today, the Vagrantfile will be roughly the following format:

```ruby
Vagrant.configure("2") do |config|
  # ...
end
```

The "2" in the first line represents the version of the configuration object.

### Minimum Vagrant Version

Vagrant version requirements should be at the top of a Vagrantfile:
```ruby
Vagrant.require_version ">= 1.3.5"
```

Multiple versions can also be specified:
```ruby
Vagrant.require_version ">= 1.3.5", "< 1.4.0"
```

### `config.vm`

The settings within `config.vm` modify the configuration of the machine that Vagrant manages.

- `config.vm.base_mac`
- `config.vm.base_address`
- `config.vm.boot_timeout`
- `config.vm.box`
- `config.vm.box_check_update`
- `config.vm.box_download_checksum`
- `config.vm.box_download_checksum_type`
- `config.vm.box_download_client_cert`
- `config.vm.box_download_ca_cert`
- `config.vm.box_download_Ca_path`
- `config.vm.bpx_download_insecure`
- `config.vm.box_download_location_trusted`
- `config.vm.box_url`: URL that the configured box can be found at
- `config.vm.box_version`
- `config.vm.communicator`
- `config.vm.graceful_halt_timeout`
- `config.vm.guest`
- `config.vm.hostname`: Hostname that the machine should have. Vagrant updates guests' `/etc/hosts`
- `config.vm.ignore_box_vagrantfile`
- `config.vm.network`
- `config.vm.post_up_message`
- `config.vm.provider`
- `config.vm.provision`: Configures provisioners on the machine, so that software can be automatically
  installed and configured when the machine is created.
- `config.vm.synced_folder`: Configures synced folders on the machine, so that folders on your host
  machine can be synced to and from the guest machine.
- `config.vm.usable_port_range`: A range of ports to use for handling collisions

### `config.ssh`

The settings within `config.ssh` relate to configuring how Vagrant will access your machine over SSH.

- `config.ssh.compression`
- `config.ssh.config`
- `config.ssh.dsa_authentication`
- `config.ssh.export_command_template`
- `config.ssh.extra_args`
- `config.ssh.forward_agent`
- `config.ssh.forward_env`
- `config.ssh.forward_x11`
- `config.ssh.guest_port`
- `config.ssh.host`
- `config.ssh.insert_key`
- `config.ssh.keep_alive`
- `config.ssh.keys_only`
- `config.ssh.paranoid`
- `config.ssh.password`
- `config.ssh.port`
- `config.ssh.private_key_path`
- `config.ssh.proxy_command`
- `config.ssh.pty`
- `config.ssh.remote_user`
- `config.ssh.shell`
- `config.ssh.sudo_command`
- `config.ssh.username`
- `config.ssh.verify_host_key`

### `config.vagrant`

The settings within `config.vagrant` modify the behavior of Vagrant itself.

- `config.vagrant.host`
- `config.vagrant.plugins`: Define plugin, list of plugins, or definition of plugins to install for the
  local project. If the plugins are not available. It will attempt to automatically install them into the
  local project.

  ```ruby
  config.vagrant.plugins = ["vagrant-plugin", "vagrant-other-plugin"]
  ```

- `config.vagrant.sensitive`: Value or list of values that should not be displayed in Vagrant's output.

  ```ruby
  config.vagrant.sensitive = ["MySecretPassword", ENV["MY_TOKEN"]]
  ```

## Boxes

Boxes are the package formate for Vagrant environments. A box can be used by anyone on any platform
that Vagrant supports to bring up an identical working environment.

The `vagrant box` utility provides all the functionality for managing boxes.

Discover public boxes in the [public box catalog](https://app.vagrantup.com/boxes/search)

## Provisiong

Provisioners in Vagrant allow you to automatically install software, alter configurations, and more
on the machine as part of the `vagrant up` process.

This is useful since boxes typically are not built perfectly for your use case.

Vagrant gives you multiple options for provisioning the machine, from simple shell scripts to more
complex, industry-standard configuration management systems.

### When it happens

Provisioning happens at certain points during the lifetime of your Vagrant environment:

- On first `vagrant up` that creates the environment, provisioning is run. If the environment
  was already created and the up is just resuming a machine or booting up up, they will not run
  unless the `--provision` flag is explicitly provided

- When `vagrant provision` is used on a running environment

- When `vagrant reload --provision` is called

You can also bring up your environment and explicitly _not_ run provisioners by specifying `--no-provision`.

### Basic Usage

Shell provisioning:

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo hello"
end
```

With ruby block:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell" do |s|
    s.inline = "echo hello"
  end
end
```

Run on every up and reload:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo hello",
    run: "always"
end
```

Never run except if specified on command line with `--provision-with bootstrap`
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "bootstrap", type: "shell", run: "never" do |s|
    s.inline = "echo hello"
  end
end
```

Multiple provisioners:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo foo"

  config.vm.define "web" do |web|
    web.vm.provision "shell", inline: "echo bar"
  end

  config.vm.provision "shell", inline: "echo baz"
end
```
Note: the ordering is "foo", "baz", "bar" as ordering is outside in.

### File

The Vagrant file provisioner allows you to upload a file or directory from the host machine to the guest
machine.

Moving local `.gitconfig` over
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
end
```

Can also move directories:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "file", source: "~/path/to/host/folder", destination: "$HOME/remote/newfolder"
end
```

### Shell

The Vagrant Shell provisioner allows you to upload and execut a script within the guest machine.

For POSIX-like machines, the shell provisioner executes scripts with SSH. For Windows guest machines
that are are configured to use WinRM, the shell provisioner executes PowerShell and Batch scripts
over WinRM.

Options

- __inline__: Specifies a shell command inline to execute on the remote machine
- __path__: path to a shell script ot upload and execute
- __args__: args to pass to shell script
- __binary__
- __env__
- __keep_color__
- __md5__
- __name__
- __powershell_args__
- __powershell_elevated_interactive__
- __privileged__: Specifies whether to execute the shell script as a privileged user or not
- __reboot__
- __reset__
- __sha1__
- __sensitive__
- __upload_path__: Is the remote path where the shell script will be uploaded to. By default this is
  `/tmp/vagrant-shell` or `C:\tmp\vagrant-shell` on windows

Inline scripts:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell",
    inline: "echo Hello, World"
end
```

Multiline inline:
```ruby
$script = <<-SCRIPT
echo I am provisioning...
date > /etc/vagrant_provisioned_at
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: $script
end
```

External script:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", path: "script.sh"
end
```

Arguments:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell" do |s|
    s.inline = "echo $1"
    s.args   = "'hello, world!'"
  end
end
```

## Networking

In order to access the Vagrant environment created, Vagrant exposes some high-level networking options
for things such as forwarded ports, connecting to a public network, or creating a private network.

The high-level networking options are meant to define an abstraction that works across multiple providers.

### Forwarded Ports

Vagrant forwarded ports allow you to acces a port on your host machine and have all data forwarded to
a port on the guest machine, over either TCP or UDP.

Basic port fowarding configuration:
```ruby
Vagrant.configure("2") do |config|
  # ...
  config.vm.network "forwarded_port", guest: 80, host: 8080
end
```

Specify host and protocol:
```ruby
Vagrant.configure("2") do |config|
  config.vm.network "forwarded_port", guest: 2003, host: 12003, protocol: "tcp"
  config.vm.network "forwarded_port", guest: 2003, host: 12003, protocol: "udp"
end
```

## Synced Folders

Synced folders enable Vagrant to sync a folder on the host machine to the guest machine, allowing you
to continue working on your project's files on your host machine, but use the resources in the guest
machine to compile or run your project.

By default, Vagrant will share your project directory (the directory with the Vagrantfile) to `/vagrant`.

### Basic Usage

Syncing a folder:
```ruby
Vagrant.configure("2") do |config|
  config.vm.synced_folder "src/", "/srv/website"
end
```

### NFS

In some cases the default shared folder implementations (such as VirtualBox shared folders) have high
performance penalties. If you are seeing less than ideal performance with synced foders, NFS can offer
a solution. Vagrant has built-in support to orchestrate the configuration of the NFS server on the
host and guest for you.

Basic usage:
```ruby
Vagrant.configure("2") do |config|
  config.vm.synced_folder ".", "/vagrant", type: "nfs"
end
```

## Multi-Machine

Vagrant is able to define and control multiple guest machines per Vagrantfile. This is known as a
"multi-machine" environment.

Here are some relevant use-cases:
- Accurately modeling a multi-server production topology, such as separating a web and database server
- Modeling a distributed system and how they interact with each other
- Testing an interface, such as an API to a service component
- Disaster-case testing: machines dying, network partitions, slow networks, inconsistent world views, etc.

Create a configuration within a configuration:
```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo Hello"

  config.vm.define "web" do |web|
    web.vm.box = "apache"
  end

  config.vm.define "db" do |db|
    db.vm.box = "mysql"
  end
end
```

The moment more than one machine is defined within a Vagrantfile, the usage of the various `vagrant`
commands changes slightly. Commands that only make sense to target a single machine, such as `vagrant ssh`,
now require the name of the machine to control. For example `vagrant ssh web`. Other commands, such as
`vagrant up`, operate on every machine by default.

You can specify a _primary machine_. The primary machine will be the default machine used when a specific
machine is not specified.
```ruby
config.vm.define "web", primary: true do |web|
  # ...
end
```

## Providers

While Vagrant ships out of the box with support for VirtualBox, Hpyer-V, and Docker, Vagrant has the
ability to manage other types of machines as well. This is done by using other proivders with Vagrant.

### Installation

Providers are distrubuted as Vagrant plugins, and are therefore installed using standard plugin installation
steps.

## Plugins

Vagrant comes with many great features out of the box to get your environments up and running. Sometimes,
however, you want to change the way Vagrant does something or add additional functionality to Vagrant.
This can be done via Vagrant _plugins_.

Plugins are powerful, first-class citizens that extend Vagrant using a well-documented, stable API that
can withstand major version upgrades.

### Installation

```bash
# Installing a plugin from a known gem source
$ vagrant plugin install my-plugin

# Installing a plugin from a local file source
$ vagrant plugin install /path/to/my-plugin.gem
```

### Usage

Once a plugin is installed, you should refer to the plugin's documentation to see exactly how to use it.

## Triggers

Vagrant is capable of executing machine triggers _before_ or _after_ Vagrant commands.

Each trigger is expected to be given a command key for when it should be fired during the Vagrant
command lifecycle. These could be defined as a single key or an array which acts like a whitelist
for the defined trigger.

```ruby
# single command trigger
config.trigger.after :up do |trigger|
...
end

# multiple commands for this trigger
config.trigger.before [:up, :destroy, :halt, :package] do |trigger|
...
end

# or defined as a splat list
config.trigger.before :up, :destroy, :halt, :package do |trigger|
...
end
```