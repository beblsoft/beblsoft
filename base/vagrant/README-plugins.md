# Vagrant Plugins Documentation

## Vagrant AWS Provider

Vagrant AWS providers is a Vagrant 1.2+ plugin that adds an AWS provider to Vagrant, allowing Vagrant
to control and provision machines in EC2 and VPC.

Relevant URLs:
[Github](https://github.com/mitchellh/vagrant-aws)

### Features

- Boot EC2 or VPC instances
- SSH into the instances
- Provision the instances with any built-in Vagrant provisioner
- Minimal synced folder support via `rsync`
- Define region-specific configurations so Vagrant can manage machines in multiple regions
- Package running instances into new vagrant-aws friendly boxes

### Usage

```bash
# Install Plugin
vagrant plugin install vagrant-aws

# Start up vagrant file
# Need an AWS-compatible box file for Vagrant
vagrant up --provider=aws
```

### Quickstart

After installing the plugin, the quickest way to get started is to actually use a dummy AWS box and
specify all the details manually within a `config.vm.provider` block..

First add the dummy box you want:
```bash
$ vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
```

Make a Vagrantfile that looks like the following:
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "dummy"

  config.vm.provider :aws do |aws, override|
    aws.access_key_id = "YOUR KEY"
    aws.secret_access_key = "YOUR SECRET KEY"
    aws.session_token = "SESSION TOKEN"
    aws.keypair_name = "KEYPAIR NAME"

    aws.ami = "ami-7747d01e"

    override.ssh.username = "ubuntu"
    override.ssh.private_key_path = "PATH TO YOUR PRIVATE KEY"
  end
end
```

### Box Format

Every provider in Vagrant must introduce a custom box format. THis provider intruduces `aws` boxes.

### Configuration

This provider exposes quite a few provider-specific configuration options:

- `access_key_id`
- `ami`
- `availability_zone`
- `aws_profile`
- `aws_dir`: AWS config and credentials location. Defaults o `$HOME/.aws`
- `instance_ready_timeout`
- `instance_check_interval`
- `instance_package_timeout`
- `instance_type`
- `keypair_name`
- `monitoring`
- `session_token`
- `private_ip_address`
- `elastic_ip`
- `region`
- `secret_access_key`
- `security_options`
- `iam_instance_profile_arn`
- `subnet_id`
- `associate_public_ip`
- `ssh_host_attribute`
- `tenancy`
- `tags`
- `package_tags`
- `use_iam_profile`
- `block_device_mapping`
- `elb`
- `unregister_elb_from_az`
- `terminate_on_shutdown`
- `endpoint`

