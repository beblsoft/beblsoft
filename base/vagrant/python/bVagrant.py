#!/usr/bin/env python3.6
"""
 NAME
  bVagrant.py

 DESCRIPTION
  Beblsoft Vagrant Functionality

 REQUIREMENTS
  Vagrant must be installed on the host machine
  See vagrant.sh

 REFERENCE
  Heavily influenced by python-vagrant
  https://github.com/todddeluca/python-vagrant/blob/master/vagrant/__init__.py
"""


# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.run.bRun import run


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- BEBLSOFT VAGRANT ---------------------------- #
class BeblsoftVagrant():
    """
    Beblsoft Vagrant

    Object wraps the vagrant executable
    """

    def __init__(self, rootDirectory):
        """
        Initialize object
        Args
          rootDirectory
            Root Directory where Vagrantfile resides
            Ex. "/home/jbensson/git/beblsoft/base/python-demo"
        """
        self.rootDirectory = rootDirectory

    def up(self, vmName=None, provider=None, provision=None, **runKwargs):
        """
        Invoke `vagrant up` to start a box or boxes.
        Args
          vmName:
            Name of the VM
            Ex. "Foo"
          provider:
            Provider to use for up
            Ex. "aws"
          provision:
            If True, provision
            If False, no-provision
            If None, use underlying vagrant default
          **runKwargs:
            Extra Kwargs to pass to run
        """
        provisionStr = ""
        if provision is True:
            provisionStr = "--provision"
        if provision is False:
            provisionStr = "--no-provision"

        cmd = "vagrant up {} {} {}".format(
            "--provider={}".format(provider) if provider else "",
            provisionStr, vmName if vmName else "")
        return run(cmd=cmd, cwd=self.rootDirectory, **runKwargs)

    def destroy(self, vmName=None, **runKwargs):
        """
        Invoke `vagrant destroy` to destroy box or boxes
        Args
          vmName:
            Name of the VM
            Ex. "Foo"
          **runKwargs:
            Extra Kwargs to pass to run
        """
        cmd = "vagrant destroy --force {}".format(vmName if vmName else "")
        return run(cmd=cmd, cwd=self.rootDirectory, **runKwargs)

    def ssh(self, sshCommand, vmName=None, **runKwargs):
        """
        Invoke `vagrant ssh`
        Args
          vmName:
            Name of the VM
            Ex. "Foo"
          sshCommand:
            SSH Command to execute
            Ex. "echo Hello World"
          **runKwargs:
            Extra Kwargs to pass to run
        """
        cmdList = ["vagrant", "ssh", "--command={}".format(sshCommand)]
        if vmName:
            cmdList.append(vmName)
        return run(cmd=cmdList, cwd=self.rootDirectory, **runKwargs)

    def provision(self, vmName=None, **runKwargs):
        """
        Invoke `vagrant provision`
        Args
          vmName:
            Name of the VM
            Ex. "Foo"
        """
        cmd = "vagrant provision {}".format(vmName if vmName else "")
        return run(cmd=cmd, cwd=self.rootDirectory, **runKwargs)
