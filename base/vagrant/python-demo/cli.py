#!/usr/bin/env python3.6
"""
 NAME
  cli.py

 DESCRIPTION
  Vagrant Python Sample Command Line Interface
"""


# ----------------------------- IMPORTS ------------------------------------- #
import os
import sys
import traceback
import logging
from datetime import datetime
import click
from base.bebl.python.log.bLog import BeblsoftLog
from base.vagrant.python.bVagrant import BeblsoftVagrant


# ----------------------- GLOBAL CONTEXT ------------------------------------ #
class GlobalContext():
    """
    Global Context object for CLIs
    """

    def __init__(self):
        """
        Initialize Object
        """
        # Vagrant ---------------------------------
        self.filePath          = os.path.realpath(__file__)
        self.directoryPath     = os.path.dirname(self.filePath)
        self.bVagrant          = BeblsoftVagrant(rootDirectory=self.directoryPath)

        # Log -------------------------------------
        self.bLog              = None
        self.cLogFilterMap     = {
            "0": logging.CRITICAL,
            "1": logging.WARNING,  # Tests log here
            "2": logging.INFO,
            "3": logging.DEBUG
        }
        self.startTime         = None
        self.endTime           = None


logger = logging.getLogger(__name__)
gc     = GlobalContext()


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
@click.option("--logfile", default="/tmp/vagrantPythonDemo.log", type=click.Path(),
              help="Specify log file Default=/tmp/vagrantPythonServerDemo.log")
@click.option("-a", "--appendlog", is_flag=True, default=False, help="Append to existing log file")
@click.option("-v", "--verbose", default="2",
              type=click.Choice(gc.cLogFilterMap.keys()),
              help="Console verbosity level. Default=2")
def cli(logfile, appendlog, verbose):
    """
    Vagrant Python Sample Command Line Interface
    """
    gc.bLog = BeblsoftLog(logFile=logfile, logFileAppend=appendlog, cFilter=gc.cLogFilterMap[verbose])
    gc.bLog.logHeader()


# ----------------------- COMMANDS ------------------------------------------ #
@cli.command()
def up():
    """
    Up
    """
    gc.bVagrant.up(raiseOnStatus=True, bufferedLogFunc=logger.info)


@cli.command()
def destroy():
    """
    Destroy
    """
    gc.bVagrant.destroy(raiseOnStatus=True)


@cli.command()
def ssh():
    """
    SSH
    """
    gc.bVagrant.ssh(sshCommand="echo Hello World!", raiseOnStatus=True, bufferedLogFunc=logger.info)


@cli.command()
def provision():
    """
    Provision
    """
    gc.bVagrant.provision(raiseOnStatus=True, bufferedLogFunc=logger.info)


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    try:
        gc.startTime = datetime.now()
        cli(obj = {})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
    finally:
        if gc.bLog:
            gc.endTime = datetime.now()
            gc.bLog.logFooter(gc.startTime, gc.endTime)
