# Vagrant Projects Documentation

Note: All examples assume Virtual Box has been installed.

## vagrant-demos/hello-world

Vagrant hello world program.

```bash
# Enter directory
cd vagrant-demos/hello-world;

# Start VM
vagrant up

# SSH into VM. Ctrl-D to exit
vagrant ssh

# Destroy VM
vagrant destroy
```

## vagrant-demos/web-server

Vagrant program that demonstrates how to run a provisioning script.

```bash
# Enter directory
cd vagrant-demos/web-server;

# Start VM
vagrant up

# SSH into VM. Ctrl-D to exit
vagrant ssh

# Verify apache running
wget -qO- 127.0.0.1
# Quit

# From local host
# Open browser to http://127.0.0.1:4567

# Destroy VM
vagrant destroy
```

## vagrant-demos/aws

Demonstrates how Vagrant can spawn a VM on AWS.

```bash
# Enter directory
cd vagrant-demos/aws;

# First examine Vagrantfile to fix any fields

# Start VM
vagrant up --provider=aws

# SSH into VM
vagrant ssh

# Destroy VM
vagrant destroy
```

## python-demo

Demonstrate usage of BeblsoftVagrant python package.

```bash
# Enter directory
cd python-demo

# Setup Venv
virtualenv -p /usr/bin/python3.6 /tmp/vagrantVenv
source /tmp/vagrantVenv/bin/activate
pip3 install -r requirements.txt

# Start VM
./cli.py up

# Re Provision VM
./cli.py provision

# SSH command on VM
./cli.py ssh

# Destroy VM
./cli.py destroy
```