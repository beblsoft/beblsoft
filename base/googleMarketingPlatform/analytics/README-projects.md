# Google Analytics Projects Documentation

All projects use the following Google Analytics credentials:

- Account: `BeblsoftDemo`
- Property: `BeblsoftDemo`
- [Dashboard URL](https://analytics.google.com/analytics/web/?authuser=1#/realtime/rt-overview/a149611596w212122340p203521161/)

## js-analytics-demo

Project demonstrates basic usage of analytics.js.

Steps to run the project:
- Go to directory: `cd js-analytics-demo;`
- Install: `npm install`
- Run Server: `npm start`
- View in chrome: http://localhost:3000/index.html
- Open Console to view debug messages

## vue-analytics-demo

Project demonstrates basic usage of vue-analytics plugin.

Project was built with the vue cli: `vue init webpack vue-analytics-demo`

Steps to run the project:
- Go to directory: `cd vue-analytics-demo;`
- Install: `npm install`
- Run Server: `npm run dev`
- View in chrome: http://localhost:8080
- Open Console to view debug messages
- Build for production: `npm run build`

Relevant Files:
- `src/main.js`: Instantiates Vue Analytics object