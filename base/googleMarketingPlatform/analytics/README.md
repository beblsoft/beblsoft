# Google Analytics Technology Documentation

Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.
It is leveraged by businesses to track user website activity. For example, Google analytics will
track _user session duration_, _pages seen_, and _clicks rates_, among many other metrics.

Relevant URLs:

- [Home](https://marketingplatform.google.com/about/analytics/)
- [Help](https://support.google.com/analytics#topic=3544906)
- [Console](https://analytics.google.com/analytics/web)
- [Wikipedia](https://en.wikipedia.org/wiki/Google_Analytics)
- Courses:
  [Academy](https://analytics.google.com/analytics/academy/),
  [Beginner Course](https://analytics.google.com/analytics/academy/course/6)
- Developer:
  [Docs Home](https://developers.google.com/analytics/),
  [Architecture](https://developers.google.com/analytics/devguides/platform/)
- Data Collection APIs:
  [analytics.js](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
  [gtag.js](https://developers.google.com/analytics/devguides/collection/gtagjs/),
  [Measurement Protocol](https://developers.google.com/analytics/devguides/collection/protocol/v1/),
  [Android](https://developers.google.com/analytics/devguides/collection/firebase/android/),
  [iOS](https://developers.google.com/analytics/devguides/collection/firebase/ios/)
- Vue Analytics:
  [Github](https://github.com/MatteoGabriele/vue-analytics),
  [NPM](https://www.npmjs.com/package/vue-analytics)

## Digital Analytics

__Digital analytics__ is the analysis of business data which drives the continual improvement of customer
online experiences in order to achieve desired business outcomes.

The __purchase funnel__ is a consumer focused marketing model which illustrates the theoretical customer
journey towards a purchase of a product or service. Here are the steps in a purchase funnel:

- __Acquisition__: Building awareness and acquiring user interest
- __Behavior__: Users engage with business
- __Conversion__: User becomes a customer and transacts with business

In the __offline world__ the purchase funnel is hard to measure. In the __online world__ one can measure many different
aspects using digital analytics. More specifically, digital analytics allows a business to track all customer
actions which led to a purchase.

Steps track a website:
- [Create a Google Analytics Account](https://marketingplatform.google.com/about/analytics/)
- Add a small piece of JS tracking code to each page of your site
- Every time a user visits a webpage, the tracking code will collect anonymous information about how that
  user interacted with he page

Information collected includes user session duration, pages seen, clicks, browsers used, operating oystem, etc.
When Analytics processes the data, it aggregates and organizes the output based on particular criteria like
whether the user's device is mobile or desktop, or which browser they're using.

## Components

Google Analytics is composed of the following components:
- Collection: Collects user-interaction data
- Configuration: Allows you to manage how data is processed
- Processing: Processes the user-interaction data, with the configuration data
- Reporting: Provides access to all the processed data

Architecture Picture:

  ```text
   Collection                 |                Configuration                    | Reporting
                              |                                                 |
                              |          Web        Management  Provisioning    |
                              |       Interface       API           API         |    Metadata API
                              |            \          |           /             |     |
                              |             \         |          /              |    Embed API
                              |               Configuration Data                |     |
                              |-----------------------|-------------------------| |- Core Reporting API
   analytics.js    Measurement|                       |                         | |
   Android SDK --> Protocol -------> Logs -----> Processing ---> Query Engine-----|
   iOS SDK                    |                                                 | |
                              |                                                 | |- Multi-Channel Funnels API
                              |                                                 | |- Real Time Reporting API
                              |                Processing                       | |- Web Interface
  ```

## Account Structure

Google Analytics has the following  __Hierarchy__: `Organization` > `Account` > `Property` > `View`

  ```text
  -------------------------------------------------
  | Organization                                  |
  |                                               |
  |  -------------------------------------------- |
  |  | Account                                  | |
  |  |                                          | |
  |  |  --------------------------------------- | |
  |  |  | Property                            | | |
  |  |  | ---------  ---------  ---------     | | |
  |  |  | | View  |  | View  |  | View  |     | | |
  |  |  | ---------  ---------  ---------     | | |
  |  |  --------------------------------------- | |
  |  |                                          | |
  |  |  --------------------------------------- | |
  |  |  | Property                            | | |
  |  |  | ---------  ---------  ---------     | | |
  |  |  | | View  |  | View  |  | View  |     | | |
  |  |  | ---------  ---------  ---------     | | |
  |  |  --------------------------------------- | |
  |  |                                          | |
  |  -------------------------------------------- |
  -------------------------------------------------
  ```

Hierarchy Definitions:
- __Organization__: Manage multiple accounts under one grouping.
- __Account__: Determines how data is collected from your website and manages who can access that data.
  Typically have one analytics account for each distinct business or business unit.
- __Property__: Each property can collect data independently of each other using a unique tracking ID
  that appears in tracking code. May assign multiple properties to each account, so you can collect
  data from different websites, mobile applications, or other digital assets associated with the business.
- __View__: Each property can have multiple views. Use views to see different reports off of the same dataset.
   New views only include data from the data the view was created and onwards
    * __Filters__: Allow you to filter traffic coming into the view. I.e. avoid internal developer traffic
    * __Goals__:   View level lets you set Analytics "Goals". Goals are a valuable way to track conversions, or business
      objectives, from your website.
- __Users__ : Users can have permissions at the account, property, or view level. Each level inherits permissions
  from the level above it.

## Events

Events are used to collect data about interactions with your content. They are measured independently
from a web page or a screen load. Downloads, link clicks, form submissions, and video plays are all examples
of potential Event actions.

An event has the following components. An event hit includes a value for each component, and these
values are displayed in event reports.
- __Category__:
  `Ex. "Videos"`
  A category is a name that you supply as a way to group objects that you want to analyze.
- __Action__:
  `Ex. "Play"`
  Use the action parameter to name the type of event or interaction you want to measure for a particular
  web object. All actions are listed independently from their parent categories. A unique event is determined
  by a unique action name.
- __Label__:
  `Ex. "/video/BirthdayParty.mp4"`
  With labels, you can provide additional information for events that you want to analyze, such as movie
  titles or names of downloaded files.
- __Value__ (optional)
  `Ex. 1298`
  An integer that is used to assign numerical value to a page object.

The term "non-interaction" applies to an optional boolean parameter that can be passed to the method
that sends the Event hit. This parameter allows you to determine how you want bounce rate defined
for your pages on your site that also include event measurement. When set to `true` the event will not
be used to calculate the bounce rate (i.e. a bounce can still occur even if the event is triggered).

In general, a _bounce_ is described as a single-page session to your site. In Analytics, a bounce
is calcuated specifically as a sessions that do not generate any events.

The Event measurement model is highly flexible and can be extended well beyond the common model.
Below are the best practices:
- Determine in advance all elements for which you want to analyze data.
- Work with report user to plan Event measurement reports
- Adopt a consisten and clear naming convention. Every name for categories, actions, and labels
  should be consistent
