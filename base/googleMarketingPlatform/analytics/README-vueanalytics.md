# Vue Analytics Technology Documentation

vue-analytics is a Vue plugin for Google Analytics.

Features:

- Automatic Google Analytics script loading
- Automatic page tracking
- Event batching
- Opt-out from Google Analytics with promise support
- Multiple domain ID tracking system
- Vuex support
- E-commerce API
- Vue error exception tracking system
- Debugging API

Relevant URLs:
[NPM](https://www.npmjs.com/package/vue-analytics),
[Github](https://github.com/MatteoGabriele/vue-analytics),
[Tips and Tricks](https://medium.com/dailyjs/tips-tricks-for-vue-analytics-87a9d2838915)

## Installation

NPM install:
```bash
npm install vue-analytics
```

Register with Vue Application:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X'
})
```

Use in `this.$ga` component:
```javascript
export default {
  name: 'MyComponent',

  methods: {
    track () {
      this.$ga.page('/')
    }
  }
}
```

## Track multiple accounts

Simply pass an array of strings for multiple tracking system. Every hit will be fired twice, each time
with a different tracker name:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: ['UA-XXX-A', 'UA-XXX-B']
})
```

## Page Tracking

Enable auto page tracking by configuring the VueRouter with vue-analytics:
```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAnalytics from 'vue-analytics'

const router = new VueRouter({
  router: // your routes
})

Vue.use(VueAnalytics, {
  id: 'UA-138916035-1',
  router,
  ignoreRoutes: [],                  // Routes to not track
  autoTracking: {
    page: true,                      // Enable page autotracking
    pageviewOnLoad: true,            // Send pageview event on load
    screenview: false,               // Don't track screens
    pageviewTemplate(route) {        // Template to fill out Google Analytics pageview
      return {
        page: route.path,
        title: document.title,
        location: window.location.href
      };
    },
    transformQueryString: true,      // Allow route query to be sent as query string
    prependBase: true,               // Merge base path with router path
    skipSamePath: false              // Track the same paths
  }
})
```

Can also manually track pages by calling `this.$ga.page('/')` from inside a component.

## Event Tracking

Example event tracking within component:
```javascript
this.$ga.event({
  eventCategory: 'category',
  eventAction: 'action',
  eventLabel: 'label',
  eventValue: 123
})
```

## Screen Tracking

Manually:
```javascript
export default {
  name: 'MyComponent',

  methods: {
    track () {
      this.$ga.screenview({
        screenName: 'home',
        ... // other properties
      })
    }
  }
}
```

Autotracking:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  autoTracking: {
    screenview: true
  }
})
```

## Event Batches

It is possible to fire event using a queue that will be processed on a certain rate:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  batch: {
    enabled: true, // enable/disable
    amount: 2,     // amount of events fired
    delay: 500     // delay in milliseconds
  }
})
```

## v-ga

This directive allows us to centralize all track events in one object and share it across the entire
application without needs to add extra logic to our component methods.

To start tracking the value of `name`, start by adding a method in the commands object:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
   id: 'UA-XXX-X',
   commands: {
      trackName (name = 'unknown') {
         this.$ga.event('randomClicks', 'click', 'name', name)
      }
   }
})
```

Add the `v-ga` directive to element and access the method from the `commands` list that now is shared
in the `$ga` object:
```html
<template>
   <button v-ga="$ga.commands.trackName.bind(this, name)" @click="logName"> Log me </button>
</template>

<script>
   export default {
      name: 'myComponent',
      data () {
         return {
            name: 'John'
         }
      },
      methods: {
         logName () {
            console.log(this.name)
         }
      }
   }
</script>
```

## Cross-domain tracking

Enable cross-domain tracking as follows:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  linkers: ['example1.com', 'example2.com']
})
```

## User Timings

User timing syntax:
```javascript
this.$ga.time({
  timingCategory: 'category',
  timingVar: 'variable',
  timingValue: 123,
  timingLabel: 'label'
})
```

## Exception Tracking

Exception tracking allows you to measure the number and type of crashes or errors that occur in your
application.

Enable auto tracking:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  autoTracking: {
    exception: true,    // Enable exception tracking
    exceptionLogs: true // Log to console
  }
})
```

Manual tracking:
```javascript
try {
  // some code that might crash
} catch (error) {
  // handle your error here

  // track the error with analytics
  // depending on the error you might want to check
  // if a `message` property exists or not
  const exception = error.message || error
  this.$ga.exception(exception)
}
```

## Require

Use `require` method to load a plugin:
```javascript
const options = {};
this.$ga.require('pluginName', options);
```

## Set

Set a single field and value paire or a group of field/value pairs on a tracker object:
```javascript
this.$ga.set({ fieldName: fieldValue })
```

Set fields before first hit:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  set: [
    { field: 'fieldname', value: 'fieldvalue' }
  ]
})
```

## Social Interactions

Track social interactions as follows:
```javascript
this.$ga.social({
  socialNetwork: 'Facebook',
  socialAction: 'like',
  socialTarget: 'http://myownpersonaldomain.com'
})
```

## Tracker fields

Setup initial track fields using the `fields` prop:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  fields: {
    userId: 'xxx'
  }
})
```

Setup fields per tracker ID:
```javascript
Vue.use(VueAnalytics, {
  id: ['UA-12345-1', 'UA-54321-2'],
  //fields for both IDS
  fields: {
    userId: '1',
  },
  customIdFields: {
    'UA-12345-1': {
      clientId: '2'
    },
    'UA-54321-2': {
      clientId: '3'
    }
  }
})
```

## On Analytics Ready

Callbacks can be added to fire when analytics.js is loaded:
```javascript
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
  beforeFirstHit () {
    // this is right after the tracker and before every other hit to Google Analytics
  },
  ready () {
    // here Google Analytics is ready to track!
  }
})
```

Can also import the `onScriptLoaded` method which returns a promise:
```javascript
import VueAnalytics, { onAnalyticsReady } from 'vue-analytics'

Vue.use(VueAnalytics, { ... })

const App = new Vue({ ... })

onAnalyticsReady().then(() => {
  App.$mount('#app')
})
```

## Custom Methods

Access the Google Analytics API directly:
```javascript
this.$ga.query(...)
```

## E-commerce

All e-commerce features are built in. Enable the ecommerce plugins as follows:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  ecommerce: {
    enabled: true,    // Enable ecommerce
    options: { ... }, // ecommerce options
    enhanced: true    // Enable enhanced ecommerce
  }
})
```

All e-commerce features are accessible via the ecommerce object:
```html
<template>
  <div>
    <button @click="addItem">Add item!</button>
  </div>
</template>

<script>
  export default {
    name: 'myComponent',

    methods: {
      addItem () {
        this.$ga.ecommerce.addItem({
          id: '1234',                     // Transaction ID. Required.
          name: 'Fluffy Pink Bunnies',    // Product name. Required.
          sku: 'DD23444',                 // SKU/code.
          category: 'Party Toys',         // Category or variation.
          price: '11.99',                 // Unit price.
          quantity: '1'                   // Quantity.
        })
      }
    }
  }
</script>
```

## Untracked Hits

Due to different types of connections, loading Google Analytics script and having the application
up and running at the same time can be difficult and lead to untracked hits.

VueAnalytics takes care of this eventualy by storing all untracked events and tracks them later on.
To turn this feature off:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
   id: 'UA-XXX-X',
   autotracking: {
     untracked: false
   }
})
```

## Vuex

Can use VueAnalytics from Vuex by using the following steps:
1. Make sure to have vue-analytics installed before start using it in store:
   ```javascript
   // main.js
   import Vue from 'vue'
   import store from './store'
   import App from './App'
   import VueAnalytics from 'vue-analytics'

   Vue.use(VueAnalytics, {
     id: 'UA-xxxx-1'
   })

   new Vue({
     store,
     render: h => h(App)
   }).$mount('#app')
   ```
2. Start using vue-analytics API in store
   ```javascript
   // store.js
   import Vue from 'vue'
   import Vuex from 'vuex'
   import { event } from 'vue-analytics'

   Vue.use(Vuex)

   export default new Vuex.Store({
     state: {
       counter: 0
     },
     actions: {
       increase ({ commit, state }) {
         commit('increase', state.counter + 1)
       }
     },
     mutations: {
       increase (state, payload) {
         state.counter = payload
         event('user-click', 'increase', 'counter', state.counter)
       }
     }
   })
   ```

## Turn off during development

Stop sending hits during development:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  debug: {
    sendHitTask: process.env.NODE_ENV === 'production'
  }
})
```

## Debugging

Implements the [Google Analytics Debug Lib](https://developers.google.com/analytics/devguides/collection/analyticsjs/debugging)
Use `analytics_debug.js` instead of `analytics.js`:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  debug: {
    enabled: true, // Load analytics_debug.js
    trace: true,
    sendHitTask: true
  }
})
```

## Opt-out from Google Analytics

Opt-out from Google Analytics by setting `disabled` property to `true`.:
```javascript
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

// boolean
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  disabled: true
})

// function
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  disabled: () => {
    return true
  }
})

// promise
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  disabled: Promise.resolve(true)
})

// function that returns a promise
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  disabled: () => {
    return Promise.resolve(true)
  }
})
```

Can also disable tracking from everywhere at anytime as follows:
```javascript
export default {
  methods: {
    disableTracking () {
      this.$ga.disable()
      // from now on analytics is disabled
    },
    enableTracking () {
      this.$ga.enable()
      // from now on analytics is enabled
    }
  }
}
```

or:
```javascript
Vue.$ga.disable()
Vue.$ga.enable()
```

## Custom analytics.js URL

Due to country restrictions, in specific cases is necessary to add a custom URL to load the analytics.js file:
```javascript
Vue.use(VueAnalytics, {
  id: 'UA-XXX-X',
  customResourceURL: 'http://your-custom-url/analytics.js'
})
```

