# analytics.js Technology Documentation

The analytics.js library is a JavaScript library for measuring how users interact with your website.

Relevant URLs:

- [Documentation](https://developers.google.com/analytics/devguides/collection/analyticsjs/)

## Installation

Add the following code to website near the top of the `<head>` tag and before any other script or
CSS tags. Replace `GA_MEASUREMENT_ID` with the `property ID` of the Google Analytics
property you wish to track.

```html
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'GA_MEASUREMENT_ID', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
```

The above code above does the following:
- Asynchronously downloads analytics.js library from `https://www.google-analytics.com/analytics.js`
- Initializes a global `ga` function (called the `ga()` command queue) that allows you to schedule
  commands to be run once the analytics library is ready to go
- Adds a command to the `ga()` command queue to create a new tracker obeject for `GA_MEASUREMENT_ID`
- Adds another command to the `ga()` command queue to send a pageview to Google Analytics for the current page.

Data captured includes:
- Total time user spends on site
- Time a user spends on each page and in what order pages were visited
- What internal links were clicked
- Geographic location of user
- What browser or OS is being used
- Screen size
- Referring site

## The ga command queue

Almost everything you need to track with analytics.js can be done using the `ga()` command queue.
`ga()` adds commands to `ga.q`. After `analytics.js` is loaded all the commands on `ga.q` are executed.

To add commands to the queue: `ga('command', 'arg1', ..., 'argN')`. For example:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');
```

Arguments may also be passed as an object. For example:
```javascript
ga('create', {
  trackingId: 'UA-XXXXX-Y',
  cookieDomain: 'auto'
});
```

## Trackers

Tracker objects are objects that can collect and store data and then send that data to Google Analytics.
When creating a new tracker, specify a __tracking ID__ (which is the same as the property ID that
corresponds to one of your Google Analytics properties) as well as a __cookie domain__, which specifies
how cookies are stored.

Tracker usage is shown below:
- Create basic: `ga('create', 'UA-XXXX-Y', 'auto');`
- Create a named tracker: `ga('create', 'UA-XXXXX-Y', 'auto', 'myTracker');`
- Specify fields at creation time: `ga('create', 'UA-XXXXX-Y', 'auto', 'myTracker', { userId: '12345'});`
- Create multiple trackers on the same site:
  ```javascript
  ga('create', 'UA-XXXXX-Y', 'auto');
  ga('create', 'UA-XXXXX-Z', 'auto', 'clientTracker');
  ```
- Run commands for a specific tracker:
  ```javascript
  ga('send', 'pageview');
  ga('clientTracker.send', 'pageview');
  ```
- Access a tracker after it has been created with the `ready` callback:
  ```javascript
  ga('create', 'UA-XXXXX-Y', 'auto');
  ga(function(tracker) {
    // Logs the tracker created above to the console.
    console.log(tracker);
  });
  ```

Methods to access trackers:
- Get tracker by name: `ga.getByName('myTracker');`
- Get all trackers: `ga.getAll();`
- Get tracker name: `tracker.get('name');`
- Get tracker client ID: `tracker.get('clientId');`
- Get tracker referrer: `tracker.get('referrer');`

Methods to update tracker data:

- Update default tracker page field: `ga('set', 'page', '/about');`
- Set multiple default fields at once: `ga('set', { page: '/about',  title: 'About Us'});`
- Update tracker object itself: `ga(function(tracker) { tracker.set('page', '/about')' });`

## Hits

When a tracker sends data to Google Analytics its called sending a __hit__. Every hit must have
a __hit type__. Some hit types include: `pageview`, `screenview`, `event`, `transaction`, `item`,
`social`, `exception`, and `timing`. Hits can be viewed in the Chrome Developer network tab as requests
going to `google-analytics.com/collect`.

A tracker's `send` method can be called to send data to Google Analytics. Signature for send is as follows:
```javascript
ga('[trackerName.]send', [hitType], [...fields], [fieldsObject]);
ga('myTracker.send', {
  hitType: 'event',
  eventCategory: 'Video',
  eventAction: 'play',
  eventLabel: 'cats.mp4'
});
```

In many cases you need to know when a hit is done being sent to Google Analytics, so page can take
action immediately afterward. For example, if user hits submit button, ga code must run before new
page loads. Can leverage the __hitCallback__ to determine when the hit is done sending. Example below:
```javascript
// Function to execute fn after a timeout if no other client calls it
// Handles case where Google Analytics fails to load
function createFunctionWithTimeout(callback, opt_timeout) {
  var called = false;
  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, opt_timeout || 1000);
  return fn;
}

// Gets a reference to the form element, assuming it contains the id attribute "signup-form".
// Adds a listener for the "submit" event.
var form = document.getElementById('signup-form');
form.addEventListener('submit', function(event) {
  // Prevents the browser from submitting the form
  // and thus unloading the current page.
  event.preventDefault();

  // Sends the event to Google Analytics and resubmits the form once the hit is done.
  ga('send', 'event', 'Signup Form', 'submit', {
    hitCallback: createFunctionWithTimeout(function() {
      form.submit();
    })
  });
});
```

analytics.js supports various transport mechanisms to send hits:
- `image`: Using the `Image` object
- `xhr`: Using the `XMLHttpRequest` object
- `beacon`: Using the `navigator.sendBeacon` method. This mechanism is able to send hits if page
  is being unloaded. To use if current browser supports beacon: `ga('set', 'transport', 'beacon');`

## Plugins

Plugins are scripts that enhance the functionality of analytics.js to aid in measuring user interaction.

The `require` command takes the name of a plugin and registers it for use with the ga command queue.
analytics.js will halt execution of the command queue until the `require` command has fully loaded the
desired plugin.
- Require a plugin: `ga('[trackerName.]require', pluginName, [pluginOptions]);`
- Require Enhanced Ecommerce plugin: `ga('require', 'ec');`
- Require Display Features plugin:
  ```javascript
    ga('myTracker.require', 'displayfeatures', {
    cookieName: 'display_features_cookie'
  });
  ```

Official Google Analytics plugins get loaded automatically when they are required. However, third
party plugin code must be manually added. For example:
```html
<script>
ga('create', 'UA-XXXXX-Y', 'auto');
ga('require', 'linkTracker');
ga('send', 'pageview');
</script>

<!--Note: plugin scripts must be included after the tracking snippet. -->
<script async src="/path/to/link-tracker-plugin.js"></script>
```

After requiring a plugin, its methods become available for use with the `ga()` command queue.
- Signature for calling a plugin: `ga('[trackerName.][pluginName:]methodName', ...args);`
- Calling Enhanced Ecommerce plugin: `ga('ec:addProduct', {'id': 'P12345', 'quantity': 1});`

## Debugging

Google analytics provides a debug version of the analytics.js library that logs detailed messages
to the Javascript console as it is running. The debug url is `https://www.google-analytics.com/analytics_debug.js`

On some environments, e.g. development or testing, you might not want to send hits to Google Analytics.
To disable, set the `sendHitTask` to `null` as follows:
```javascript
if (location.hostname == 'localhost') {
  ga('set', 'sendHitTask', null);
}
```

Enable trace debugging to output more verbose information to the console. Enable as follows:
```javascript
window.ga_debug = {trace: true };
```

Google analytics provides a
[Chrome Extension](https://chrome.google.com/webstore/detail/google-analytics-debugger/jnkmfdileelhofjcijamephohjechhna)
that can enable the debug version of analytics.js without requiring you to change tracking code.
This allows you to debug your own sites and see how other sites have implemented Google Analytics tracking
with analytics.js.

## Page Tracking

Page tracking allows you to measure the number of views you had for a particular page on your website.
Pages often correspond to an entire HTML document, but they can also represent dynamically loaded content,
also known as _virtual pageviews_.

Pageview hits can be sent using the `send` command as follows:
```javascript
ga('send', 'pageview', [page], [fieldsObject]);
ga('send', {hitType: 'pageview', page: location.pathname });
```

In some cases the URL to send to Google Analytics is different from the URL that appears on the
address bar. For example. `/user/USER_ID/profile` will be different for each user. To replace
the `USER_ID` field:
```javascript
// Checks to see if the current user's userID is found in the URL, if it is, remove it.
// (Note, this assume the user ID is stored in a variable called `userID`)
// Converts: /user/2348234098/profile -> /user/profile
if (document.location.pathname.indexOf('user/' + userID) > -1) {
  var page = document.location.pathname.replace('user/' + userID, 'user');
  ga('send', {hitType: 'pageview', page: page});
}
```

## Event Tracking

Events are user interactions with content that can be measured independently from a web page or
a screen load. Downloads, mobile ad clicks, gadgets, Flash elements, AJAX embedded elements, and
video plays are all examples of actions you might want to measure as Events.

Events can be sent using the `send` command as follows:
```javascript
ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);
ga('send', { hitType: 'event', eventCategory: 'Videos', eventAction: 'play', eventLabel: 'Fall Campaign' });
```

If a user clicks a link or submits a form to an external domain, that action is not captured unless
you specifically tell Google Analytics what happened. Use the following event handler to send outbound
link click events:
```javascript
function handleOutboundLinkClicks(event) {
  ga('send', 'event', {
    eventCategory: 'Outbound Link',
    eventAction: 'click',
    eventLabel: event.target.href,
    transport: 'beacon'
  });
}
```

In some cases you might want to send an event as a non-interaction event. To do this, specify the
`nonInteraction` field as `true` as follows:
```javascript
ga('send', 'event', 'Videos', 'play', 'Fall Campaign', {
  nonInteraction: true
});
```

## Social Interactions

Use social interaction analytics to measure the number ot times users click on social buttons embedded
in webpages. For example, you might measure a Facebook "Like" or Twitter "Tweet".

Social interactions can be sent using the `send` command as follows:
```javascript
ga('send', 'social', [socialNetwork], [socialAction], [socialTarget], [fieldsObject]);
ga('send', {
  hitType: 'social',
  socialNetwork: 'Facebook',
  socialAction: 'like',
  socialTarget: 'http://myownpersonaldomain.com'
});
```

If you use the official Facebook Like buttons and subscribe to `edge.create` event you can notify
when a like happens as follows:
```javascript
FB.Event.subscribe('edge.create', function(url) {
  ga('send', 'social', 'facebook', 'like', url);
});
```

## Screen Tracking

Screens in Google Analytics represent content users are viewing within an app. Measuring screen
views allows you to see which content is being viewed most by your users, and how they are navigating
between different pieces of content.

Screen hits can be sent using the `send` command as follows:
```javascript
// Create and set appName first
ga('create', 'GA_MEASUREMENT_ID', 'auto');
ga('set', 'appName', 'myAppName');

ga('send', 'screenview', [fieldsObject]);
ga('send', 'screenview', {screenName: 'Home'});
```

## User Timings

Reducing page load time improves the overal user experience of a site. Google Analytics has a number
of reports that automatically measure and report on page load times

Timings can be sent using the `send` command as follows:
```javascript
ga('send', 'timing', [timingCategory], [timingVar], [timingValue], [timingLabel], [fieldsObject]);
ga('send', {
  hitType: 'timing',
  timingCategory: 'JS Dependencies',
  timingVar: 'load',
  timingValue: 3549
});
```
Use `window.performance` object for measuring performance of web page load time:
```javascript
// Feature detects Navigation Timing API support.
if (window.performance) {
  // Gets the number of milliseconds since page load
  // (and rounds the result since the value must be an integer).
  var timeSincePageLoad = Math.round(performance.now());

  // Sends the timing hit to Google Analytics.
  ga('send', 'timing', 'JS Dependencies', 'load', timeSincePageLoad);
}
```

## Exception tracking

Exception tracking allows you to measure the number and type of crashes or errors that occur on your
property.

Exception hits can be sent using the `send` command as follows:
```javascript
ga('send', 'exception', [fieldsObject]);

try {
  // Runs code that may or may not work.
  window.possiblyUndefinedFunction();
} catch(err) {
  ga('send', 'exception', {
    'exDescription': err.message,
    'exFatal': false
  });
}
```

## Cookies and User Identification

In order for Google Analytics to determine that two distinct hits belong to the same user,
a unique identifier assoicated with that particular user must be sent with each hit.

The analytics.js library accomplishes this via the `Client ID` field, a unique, randomly generated
string that gets stored in the browser's cookies.

Cookies uses:

- `_ga`: Store Client ID. Used to distinguish users
- `_gid`: Used to distinguish users
- `AMP_TOKEN`: Contains a token that can be used to retrieve a Client ID from API Client ID service
- `_gac_<property-id>`: Contains campaign releated information for the user. If you have linked Google
   Analytics and Google Ads accounts, Google Ads website conversion tags will read this cookie unless
   you opt-out

Can use `User ID feature` to track users across different devices.

Setup cookies as follows:
```javascript
// Manual setup
ga('create', 'UA-XXXXX-Y', {
  'cookieName': 'gaCookie',
  'cookieDomain': 'blog.example.co.uk',
  'cookieExpires': 60*60*24*28  // Time in seconds.
});

// Automatic setup
ga('create', 'UA-XXXXX-Y', 'auto');
```

To retrieve the client ID from the cookie:
```javascript
ga(function(tracker) {
  var clientId = tracker.get('clientId');
});
```

To disable cookies:
```javascript
ga('create', 'UA-XXXXX-Y', {
  'storage': 'none'
});
```

Setup user ID after authentication. Through a process of Session Unification, Google Analytics is able
to associate these hits with the correct user at processing time.
```javascript
// Creates the tracker and sends a pageview as normal
// since the `userId` value is not yet known.
ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');

// At a later time, once the `userId` value is known,
// sets the value on the tracker.
ga('set', 'userId', USER_ID);

// Setting the userId doesn't send data to Google Analytics.
// You must also use a pageview or event to send the data.
ga('send', 'event', 'authentication', 'user-id available');
```

## Custom Dimensions and Metrics

Custom dimensions and metrics are a powerful way to send custom data to Google Analytics.
You send custom dimension and metric data using either one or both of the following values:
- `dimension[0-9]+`: Text, not required, Dimension index
- `metric[0-9]+`: integer, not required, Metric index

Sending data:
- With pageview: `ga('send', 'pageview', { 'dimension15':  'My Custom Dimension'});`
- With event: `ga('send', 'event', 'category', 'action', {'metric18': 8000});`
- Set for duration: `ga('set', { 'dimension5': 'custom dimension data',  'metric5': 'custom metric data'});`

## Tasks

Each time the `send` command is caleed, analytics.js executes a sequence of tasks to validate, construct,
and send a measurement protocol request from the user's browser to Google Analytics.

The following tasks are executed:
- __customTask__: By default this task does nothing. Override it to provide custom behavior.

- __previewTask__: Aborts the request if the page is only being rendered to generate a 'Top Sites'
  thumbnail for Safari.

- __checkProtocolTask__: Aborts the request if the page protocol is not http or https.

- __validationTask__:  Aborts the request if required fields are missing or invalid.

- __checkStorageTask__: Aborts the request if the tracker is configured to use cookies but the
  user's browser has cookies disabled.

- __historyImportTask__:   Imports information from ga.js and urchin.js cookies to preserve visitor
  history when a site migrates to Universal Analytics.

- __samplerTask__: Samples out visitors based on the sampleRate setting for this tracker.

- __buildHitTask__: Builds a measurement protocol request string and stores it in the hitPayload field.

- __sendHitTask__: Transmits the measurement protocol request stored in the hitPayload field to Google
  Analytics servers.

- __timingTask__:  Automatically generates a site speed timing hit based on the siteSpeedSampleRate
  setting for this tracker.

- __displayFeaturesTask__: Sends an additional hit if display features is enabled and a previous hit
  has not been sent within the timeout period set by the display features cookie (_gat).

Taskes can be accessed or replaced using the standard tracker `get` and `set` methods. Using these
methos, you may replace tasks with your own custom functions, or augment the existing functionality by
chaining your custom functions to execute before or after an existing task.

Overriding a task:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');
ga('set', 'sendHitTask', function(model) {
  console.log(model.get('hitPayload'));
});
```

Adding to a task:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');

ga(function(tracker) {
  var originalBuildHitTask = tracker.get('buildHitTask');
  tracker.set('buildHitTask', function(model) {
    if (document.cookie.match(/testing=true/)) {
      throw 'Aborted tracking for test user.';
    }
    originalBuildHitTask(model);
  });
});

ga('send', 'pageview');
```

Disabling a task:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');
ga('set', 'checkProtocolTask', null); // Disables file protocol checking.
ga('send', 'pageview');
```

## IP Anonymization

In some cases, you might need to anonymize the IP address of the hit sent to Google Analytics.

For all hits:
```javascript
ga('set', 'anonymizeIp', true);
```

For a single hit:
```javascript
ga('send', 'pageview', {
  'anonymizeIp': true
});
```

## User Opt-Out

In some cases, it may be necessary to disable the Google Analytics tracking code on a page
without having to remove the JavaScript tracking snippet.

To disable tracking:
```javascript
window['ga-disable-UA-XXXXX-Y'] = true;
```

To opt-out of Google Analytics for all sites, even those whose source you do not control, install
the [Google Analytics opt-out browser add-on](http://support.google.com/analytics/bin/answer.py?hl=en&answer=181881)

## Display Features

[Google Analytics Adverstising Features](https://support.google.com/analytics/answer/3450482) can
be enabled in Google Analytics from `Property Settings` > `Data Collection`. Advertising features plugin
can be used to programmatically enable advertising features, as well as to override and disable all
advertising reporting and remarketing features.

To enable:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');

// Important to enable after tracker creation
ga('require', 'displayfeatures');
```

To disable:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');
ga('set', 'allowAdFeatures', false);
```

## Ecommerce

Ecommerce tracking allows you to measure the number of transactions and revenue that your website
generates.

A __transaction__ represents the entire transaction that occurs on your site. It contains the
following fields:
- `id`: The transaction ID. (e.g. 1234)
- `affiliation`:  The store or affiliation from which this transaction occurred (e.g. Acme Clothing).
- `revenue`:  Specifies the total revenue or grand total associated with the transaction (e.g. 11.99).
  This value may include shipping, tax costs, or other adjustments to total revenue that you want
  to include as part of your revenue calculations.
- `shipping`:  Specifies the total shipping cost of the transaction. (e.g. 5)
- `tax`:  Specifies the total tax of the transaction. (e.g. 1.29)

An __item__ represents the individual products that were in the shopping cart. It contains the following
fields:
- `id`: The transaction ID. This ID is what links items to the transactions to which they belong. (e.g. 1234)
- `name`: The item name. (e.g. Fluffy Pink Bunnies)
- `sku`:  Specifies the SKU or item code. (e.g. SKU47)
- `category`:  The category to which the item belongs (e.g. Party Toys)
- `price`:  The individual, unit, price for each item. (e.g. 11.99)
- `quantity`:  The number of units purchased in the transaction. If a non-integer value is passed into
  this field (e.g. 1.5), it will be rounded to the closest integer value.

Typically implement ecommerce tracking once the user has completed the checkout process. This
is generally occurs on the "Thank You" page.

Load the ecommerce plugin:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');

// Important to enable after tracker creation
ga('require', 'ecommerce');
// Now can use ecommerc commands
```

Adding a transaction:
```javascript
ga('ecommerce:addTransaction', {
  'id': '1234',                     // Transaction ID. Required.
  'affiliation': 'Acme Clothing',   // Affiliation or store name.
  'revenue': '11.99',               // Grand Total.
  'shipping': '5',                  // Shipping.
  'tax': '1.29'                     // Tax.
});
```

Adding items:
```javascript
ga('ecommerce:addItem', {
  'id': '1234',                     // Transaction ID. Required.
  'name': 'Fluffy Pink Bunnies',    // Product name. Required.
  'sku': 'DD23444',                 // SKU/code.
  'category': 'Party Toys',         // Category or variation.
  'price': '11.99',                 // Unit price.
  'quantity': '1'                   // Quantity.
});
```

Sending data:
```javascript
ga('ecommerce:send');
```

Clearing data:
```javascript
ga('ecommerce:clear');
```

Specify local currency:
```javascript
ga('ecommerce:addTransaction', {
  'id': '1234',
  'affiliation': 'Acme Clothing',
  'revenue': '11.99',
  'shipping': '5',
  'tax': '1.29',
  'currency': 'EUR'  // local currency code.
});
```
## Enhanced Ecommerce

The enhanced ecommerce plug-in enables the measurement of user interactions with products on ecommerce
websites across the user's shopping experience, including: product impressions, product clicks,
viewing product details, adding a product to a shopping cart, initiating the checkout process, transactions,
and refunds.

## Enhanced Link Attribute

Enhanced link attribution improves the accuracy of your in-page analytics report by automatically
differentiating between multiple links to the same URL on a single page by using link element IDs.

Usage:
```javascript
ga('create', 'UA-XXXXX-Y', 'auto');
ga('require', 'linkid');
```

## Linker

The linker plugin simplifies the process of implementing cross-domain tracking.

## Single Page Application Tracking

A single page application is a web application or website that loads all of the resources required
to navigate throughout the site on the first page load. As the user clicks links and interacts with
the page, subsequent content is loaded dynamically. The application will often update the URL in the
address bar to emulate traditional page navigation, but another full page request is never made.
When the site loads a new page dynmically, the virtual pageview must be tracked manually.

Tracking virtual pageviews:
```javascript
ga('set', 'page', '/new-page.html');
ga('send', 'pageview');
```

## Content Experiments

Google Analytics Content Experiments Framework enables you to test almost any change or variation to
a property to see how it performs in optimizing a specific goal. For example, increasing goal completions
or decreasing bounce rates. This allows you to identify changes worth implementing based on the direct impact
they have on the performance of your website.

## Limits and Quotas

Google Analytics is used by millions of sites and apps. To protect the system from receiving more data
than it can handle, and to ensure an equitable distribution of system resources, the following limits
are in place:
- 10 million hits per month per property
- 200,000 hits per user per day
- 500 hits per session