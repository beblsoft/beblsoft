# Google Optimizer Overview

Google Optimier allows you to run experiments independent of changing your code to see if you
can drive better conversions by making changes. Changes are often subtle, e.g. change the color of
a button, change the background image of a splash screen, etc.

Experiements are run independent of changing your code so that you can try things out on a subset of
your customers.  This means you are not uploading code all the time and are able to do tests on a
randomly chosen subset of your audience.

Only change the code one you have validated the experiement yields better results than the original code.

## Where to Focus

Focus on optimizing application areas of highest value.  These include:

- Top Landing Pages
- Pages you are paying for advertising
- Pages that drive conversions

## Definitions

- **A/B Tests** - change a single page and see if the new page can outperform the original, e.g. banner
- **Multivariant Tests** - change many things att once, often used to make further improvements
on a successful A/B Test, a drill down test.
- **Redirect Tests** - redirect a subset of your customers to a different page to new test page

## Setup

- Create your Optimizer Account (https://optimize.google.com/optimize/home/#/accounts)
- Link Google Optimizer to Google Analytics
- Install the Google Optimizer Tag
- Add the browser extension to Chrome
- Create the variant and launch the test

## Useful links:

- Good intro to Google Optimizer: https://www.youtube.com/watch?v=IzTLxFOETgc
- How to add the page-hidding snippet: https://support.google.com/optimize/answer/7100284
- How to modify the Google Optimize snippet so that it's compatible with Google Tag Manager (and prevents additional pageviews):
https://support.google.com/optimize/answer/7359264
- How to implement Google Optimize using Google Tag Manager (not recommended by Google):
https://support.google.com/optimize/answer/6314801
