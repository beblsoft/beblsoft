/**
 * @file Server EntryPoint
 */

/* ------------------------ IMPORTS ---------------------------------------- */
const express = require('express');


/* ------------------------ GLOBALS ---------------------------------------- */
const app = express();
const port = 2100;


/* ------------------------ ROUTES ----------------------------------------- */
// Server static files from the current directory
let options = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['htm', 'html'],
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) { res.set('x-timestamp', Date.now()); }
};
app.use(express.static('.', options));


/* ------------------------ MAIN ------------------------------------------- */
app.listen(port, () => { console.log(`Example app listening on port ${port}!`); });
