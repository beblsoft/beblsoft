# Google Optimize Projects Documentation

All projects use the following Google Analytics credentials:

- Account: `BeblsoftDemo`
- Property: `BeblsoftDemo`
- [Dashboard URL](https://analytics.google.com/analytics/web/?authuser=1#/realtime/rt-overview/a149611596w212122340p203521161/)

## js-optimize-demo

Project demonstrates basic usage of optimize.js. (Note the majority
of the code comes from ../analytics/js-analytics-demo.)

Steps to run the project:
- Go to directory: `cd js-optimize-demo;`
- Install: `npm install`
- Run Server: `npm start`
- View in chrome: http://localhost:2100/index.html
- Open Console to view debug messages

## vue-optimize-demo

Project demonstrates basic usage of optimize with a small (one line)
extension to vue-analytics plugin. (Note the majority
of the code comes from ../analytics/vue-analytics-demo.)

Steps to run the project:
- Go to directory: `cd vue-optimize-demo;`
- Install: `npm install`
- Run Server: `npm run dev`
- View in chrome: http://localhost:8080
- Open Console to view debug messages
- Build for production: `npm run build`

Relevant Files:
- `src/main.js`: Instantiates Vue Analytics object

## Tips Within Google Analytics/Google Optimize

- Start out with an A/B Test, the demo test done was simply to change the font color to green
- To extend your Google Analytic setup to include Optimize, add in the anti-flicker snippet
customized with your Optimize ID, e.g., GTM-NT2BSZF.  Also add in a ga('require', 'GTM-NT2BSZF');
call after the analytics setup.  Put everything in the head section as recommended by Google.
- Bring up Optimize by clicking on the Optimize plug-in within Chrome.  Ensure that you are using
your smeckn account and are hooked to the appropriate Google
Analytics Property, e.g. BebsoftDemo UA-149611596-1, All Web Data.
- Use blue button in Optimize to Create and experience (experiment) or copy existing. Wizard is
very good.  Start w/ A/B testing as this is the easiest.
- There is a button (View Report in Analytics) to get you directly into Analytics and see
your experiment.
- Use the Preview button to check your experiment.  This is very useful for localhost
test experiments to see if the experiment works on a single workstation.
- Do not use localhost in your URL to bring up the client,
use http://127.0.0.1:2100/index.html, Google Optimize expects periods in the domain name.