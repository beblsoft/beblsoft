// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import VueAnalytics from 'vue-analytics';


class GoogleAnalytics {

  constructor() {

    Vue.use(VueAnalytics, {
      id: 'UA-149611596-1',
      commands: {
        trackEvent({ eventCategory = 'clicks', eventAction = 'downClick', eventLabel = '', eventValue = null }) {
          this.$ga.event({ eventCategory, eventAction, eventLabel, eventValue })
        }
      },
      router,
      ignoreRoutes: [], // Routes to not track
      debug: {
        enabled: true, // Load analytics_debug.js
        trace: true,
        sendHitTask: true
      },
      autoTracking: {
        page: true, // Enable page autotracking
        pageviewOnLoad: true, // Send pageview event on load
        screenview: false, // Don't track screens
        pageviewTemplate(route) { // Template to fill out Google Analytics pageview
          return {
            page: route.path,
            title: document.title,
            location: window.location.href
          };
        },
        transformQueryString: true, // Allow route query to be sent as query string
        prependBase: true, // Merge base path with router path
        skipSamePath: false, // Track the same paths
        exception: true, // Enable exception tracking
        exceptionLogs: true // Log to console
      },
      beforeFirstHit() {
        console.log('Before first hit');
      },
      ready() {
        console.log('Analytics ready');
      }
    });

    // setup for optimize
    Vue.$ga.require('GTM-NT2BSZF');
  }
}

let analytics = new GoogleAnalytics();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
