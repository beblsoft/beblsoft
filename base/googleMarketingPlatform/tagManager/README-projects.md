# Google Tag Manager Projects Documentation

All projects use the following Google Tag Manager credentials:

- Account: `BeblsoftDemo`
- Containers: `BeblsoftDemo` (GTM-KBR5PXF)
- [Dashboard URL](https://tagmanager.google.com/#/container/accounts/4703004868/containers/13160574/workspaces/1)

## js-tagmanager-demo

Project demonstrates basic usage of tag manager. (Note the majority
of the code comes from ../analytics/js-analytics-demo.)

Steps to run the project:
- Go to directory: `cd js-tagmanager-demo;`
- Install: `npm install`
- Run Server: `npm start`
- View in chrome: http://localhost:2100/index.html
- Open Console to view debug messages
- Go to extension, google tag assistant to confirm tag is installed, should see GTM-KBR5PXF*.
- Now add Google Analytics tag for BeblsoftDemo, UA-149611596-1, Google Analytics:
Universal Analytics, Tracj Type: Page View, Default Name, Trigger All Pages

*NOTE: If your container ID (i.e. what’s referenced here as ‘account ID’) is of format GTM-XXXXXXX,
where the portion after ‘GTM-’ is longer than 6 characters, you may see this error unnecessarily.
This false error will be resolved in a future update. In the meantime, as long as your container ID
follows the correct format as described below, you may ignore this error. Our ID is indeed longer than
6 characters so we are ignoring this warning.

*********************************************
## vue-optimize-demo

Project demonstrates basic usage of tag manager with addition of vue-gtm component.

Steps to run the project:
- Go to directory: `cd vue-tagmanager-demo;`
- Install: `npm install`
- Run Server: `npm run dev`
- View in chrome: http://localhost:8080
- Open Console to view debug messages, use Google Analytics Debugger and Tag Assistant plug-ins to
see tags that have been loaded.
- Build for production: `npm run build`

Relevant Files:
- `src/main.js`: Instantiates Vue Tag Manager object