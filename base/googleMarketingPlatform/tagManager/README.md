# Google Tag Manager Overview

Google Tag Manager is nothing but a container.  It is essentially a layer of indirection to that you
can install new tags, e.g. Google Analytics, Google Optimize independent of changing the code.

## Why Google Tags

- flexible triggers
- tag templeting
- modularity, reusablity
- greater involvement w/ marketing

## Google Tag Manager Key Concepts:

- GTM makes it easy to manage tags.
- GTM uses a container, not a GA property to correpond to a website.
- Once a GTM container is established, new tags can be CRUD'd and triggered
- A tag is a piece of JS managed as a self-contained unit in GTM.
- A campaign tag has nothing to do w/ a GTM tag, nor does an HTML tag.
- Triggers enable tags to fire.  There is a built-in All Pages trigger.
- You can have variables to populate and trigger tags.
-You can als have data layer object stores which you pass to GTM.

## Setup

- Setup your tag manager account:  https://tagmanager.google.com/#/home, name container like your website
- After You get Tag Manager to work, deploy Google Analytics with Tag Manager:
https://support.google.com/tagmanager/answer/6107124?hl=en
- After You get Analytics to fire, add Optimize:
https://support.google.com/optimize/answer/7513085?hl=en (follow step 2)
- Load up Google Tag Assistant from Chrom web store extenstions to see all tags in your application.
https://chrome.google.com/webstore/category/ext/11-web-development
- Load Google Analytics Debugger (GA Debug) from Chrome web store too.
Click on the extension icon to load it.  Then within your development console you will see GA commands.

## Common Mistakes with GA:

1.  Inadvertently collecting personal info
2.  Assuming data quality takes care of itself
3.  Not separating customers from prospects
4.  Comparing Ecommerce data with back-end data
5.  Not cleaning data or creating backups
6.  Not using GTM to manage setup
7.  Not defining goals
8.  Not bothering w/ campaign tracking
9.  Using campaign tracking for internal goals
10.  Not keeping notes of change