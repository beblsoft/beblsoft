import Vue from 'vue'
import Router from 'vue-router'
import Comp1 from '@/comp1'
import Comp2 from '@/comp2'
import Comp3 from '@/comp3'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/comp1',
      name: 'Comp1',
      component: Comp1
    },
    {
      path: '/comp2',
      name: 'Comp2',
      component: Comp2
    },
    {
      path: '*',
      name: 'Comp3',
      component: Comp3
    }
  ]
})
