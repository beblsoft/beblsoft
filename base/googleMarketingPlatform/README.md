# Google Marketing Overview

## Definitions

- **Goggle Marketing Platform** - Overall umbrella for tool set.  Free tools and tools for pay--150K
for pay tool per year. Toolset designed to understand a products customer based, test out changes to
the site without code changes and manage ad integration, again without code change once code has
been instrumented. There are three tools of interest:

    - **Optimize** - Tool used for A/B testing, e.g., how do two different banner .pngs do across a
    two random samples of my users?

    - **Tag Manager** - Tool that provides a container for Google and 3rd party tracking code. Once a
    container is in place, new tracking code can be downloaded independent of changing the app.  Keep
    in mind that tracking code is JavaScript so you are in essence extending an app dynamically without
    a lot of integration test.

    - **Analytics** - Tool to gain a deep understanding of customers, their behaviors and monitoring conversions

## Initial Use of Tools for Smeckn

- Analytics:

    - Time on Site Audience->Overview->Avg. Session Duration
    - Conversion from splash to account [Admin (low left gear)->Goals (View column) create a rule
    with /app/CreateAccountEmail
    - Conversion from account to payment [Conversons->Goals->Goal URLs - same navigation as above but
    create a rule with /appComponents/payments/PaymentThankYou

- Tag Manager:

    - No plans to use; think it is better to integrate tracking code the manual way with more test.

- Optimize:

    - A/B Testing

## Useful links:

- https://marketingplatform.google.com/about/analytics/ - log into Google Analytics
- https://ga-dev-tools.appspot.com/campaign-url-builder/  - add context to URLs to get more granular analytics
- https://www.youtube.com/watch?v=gBeMELnxdIg - good, current intro to Google Analytics (free dashboard)