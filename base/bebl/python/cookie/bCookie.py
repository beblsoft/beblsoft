#!/usr/bin/env python3
"""
NAME:
 bCookie.py

DESCRIPTION
 Beblsoft Cookie Functionality

 We patch the Python stdlib Morsel to include SameSite as shown here
 https://stackoverflow.com/questions/50813091/how-do-i-set-the-samesite-attribute-of-http-cookies-in-python

 Consumers can import the fixed cookies and Morsel from this module
"""

# ----------------------- IMPORTS ------------------------------------------- #
from http import cookies #pylint: disable=W0611
from http.cookies import Morsel


# ----------------------- PATCH MORSEL -------------------------------------- #
Morsel._reserved[str('samesite')] = str('SameSite')  # pylint: disable=W0212
