#!/usr/bin/env python3
"""
NAME:
 bTitle.py

DESCRIPTION
 Title Printing Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #


# ----------------------- REGEX VALIDATORS ---------------------------------- #
def printTitle(string, startDashes=15, totalLength=80):
    """
    Print title
    """
    title      = "\n{} {} ".format("-" * startDashes, string)
    nEndDashes = totalLength - len(title)
    title      = "{} {}".format(title, "-" * nEndDashes)
    print(title)
