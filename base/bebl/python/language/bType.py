#!/usr/bin/env python3
"""
 NAME
  bType.py

 DESCRIPTION
  Beblsoft Language Type
"""


# ----------------------------- GLOBALS ------------------------------------- #
import enum


# ----------------------------- LANGUAGE ------------------------------------ #
class BeblsoftLanguageType(enum.Enum):
    """
    Beblsoft Language Type

    Note 1:
      These numbers must never change! Database enumerations are based off
      of them (i.e. Smeckn AccountDatabase: CompLang table) and will be corrupted
      if the numbers change

    Note 2:
      Add new languages to the end of the list
    """
    Afar          = 1
    Abkhazian     = 2
    Afrikaans     = 3
    Akan          = 4
    Albanian      = 5
    Amharic       = 6
    Arabic        = 7
    Aragonese     = 8
    Armenian      = 9
    Assamese      = 10
    Avaric        = 11
    Avestan       = 12
    Aymara        = 13
    Azerbaijani   = 14
    Bashkir       = 15
    Bambara       = 16
    Basque        = 17
    Belarusian    = 18
    Bengali       = 19
    Bihari        = 20
    Bislama       = 21
    Bosnian       = 22
    Breton        = 23
    Bulgarian     = 24
    Burmese       = 25
    Catalan       = 26
    Chamorro      = 27
    Chechen       = 28
    Chinese       = 29
    Church        = 30
    Chuvash       = 31
    Cornish       = 32
    Corsican      = 33
    Cree          = 34
    Czech         = 35
    Danish        = 36
    Divehi        = 37
    Dutch         = 38
    Dzongkha      = 39
    English       = 40
    Esperanto     = 41
    Estonian      = 42
    Ewe           = 43
    Faroese       = 44
    Persian       = 45
    Fijian        = 46
    Finnish       = 47
    French        = 48
    Western       = 49
    Fulah         = 50
    Georgian      = 51
    German        = 52
    Gaelic        = 53
    Irish         = 54
    Galician      = 55
    Manx          = 56
    Greek         = 57
    Guarani       = 58
    Gujarati      = 59
    Haitian       = 60
    Hausa         = 61
    Hebrew        = 62
    Herero        = 63
    Hindi         = 64
    Croatian      = 65
    Hungarian     = 66
    Igbo          = 67
    Icelandic     = 68
    Ido           = 69
    Inuktitut     = 70
    Interlingue   = 71
    Interlingua   = 72
    Indonesian    = 73
    Inupiaq       = 74
    Italian       = 75
    Javanese      = 76
    Japanese      = 77
    Kalaallisut   = 78
    Kannada       = 79
    Kashmiri      = 80
    Kanuri        = 81
    Kazakh        = 82
    Kikuyu        = 83
    Kinyarwanda   = 84
    Kirghiz       = 85
    Komi          = 86
    Kongo         = 87
    Korean        = 88
    Kuanyama      = 89
    Kurdish       = 90
    Lao           = 91
    Latin         = 92
    Latvian       = 93
    Limburgan     = 94
    Lingala       = 95
    Lithuanian    = 96
    Luxembourgish = 97
    Luba          = 98
    Ganda         = 99
    Macedonian    = 100
    Marshallese   = 101
    Malayalam     = 102
    Maori         = 103
    Marathi       = 104
    Malay         = 105
    Malagasy      = 106
    Maltese       = 107
    Mongolian     = 108
    Nauru         = 109
    Navajo        = 110
    Ndonga        = 111
    Nepali        = 112
    Norwegian     = 113
    Bokmål        = 114
    Chichewa      = 115
    Occitan       = 116
    Ojibwa        = 117
    Oriya         = 118
    Oromo         = 119
    Ossetian      = 120
    Panjabi       = 121
    Pali          = 122
    Polish        = 123
    Portuguese    = 124
    Pushto        = 125
    Quechua       = 126
    Romansh       = 127
    Romanian      = 128
    Rundi         = 129
    Russian       = 130
    Sango         = 131
    Sanskrit      = 132
    Sinhala       = 133
    Slovak        = 134
    Slovenian     = 135
    Northern      = 136
    Samoan        = 137
    Shona         = 138
    Sindhi        = 139
    Somali        = 140
    Sotho         = 141
    Spanish       = 142
    Sardinian     = 143
    Serbian       = 144
    Swati         = 145
    Sundanese     = 146
    Swahili       = 147
    Swedish       = 148
    Tahitian      = 149
    Tamil         = 150
    Tatar         = 151
    Telugu        = 152
    Tajik         = 153
    Tagalog       = 154
    Thai          = 155
    Tibetan       = 156
    Tigrinya      = 157
    Tonga         = 158
    Tswana        = 159
    Tsonga        = 160
    Turkmen       = 161
    Turkish       = 162
    Twi           = 163
    Uighur        = 164
    Ukrainian     = 165
    Urdu          = 166
    Uzbek         = 167
    Venda         = 168
    Vietnamese    = 169
    Volapük       = 170
    Welsh         = 171
    Walloon       = 172
    Wolof         = 173
    Xhosa         = 174
    Yiddish       = 175
    Yoruba        = 176
    Zhuang        = 177
    Zulu          = 178
    Cebuano       = 179
