#!/usr/bin/env python3
"""
 NAME
  bLanguage.py

 DESCRIPTION
  Beblsoft Language Functionality

 REFERENCE
  Wikipedia          : https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
  ISO639-2 Languages : http://www.loc.gov/standards/iso639-2/php/code_list.php
  Comprehend         : https://docs.aws.amazon.com/comprehend/latest/dg/how-languages.html
"""


# ----------------------------- GLOBALS ------------------------------------- #
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.language.bType import BeblsoftLanguageType


# ----------------------------- LANGUAGE TYPE ------------------------------- #
class BeblsoftLanguage():
    """
    Beblsoft Language
    """

    codeMap = {}
    typeMap = {}

    def __init__(self, bLanguageType, codeList=None):
        """
        Initialize Object
          Args
            bLanguageType:
              Beblsoft Langauge Type
              Ex. BeblsoftLanguageType.English
            codeList:
              List of codes that can identify language
              Ex. ["eng", "en"]
        """
        self.bLanguageType                      = bLanguageType
        self.codeList                           = codeList
        for code in codeList:
            BeblsoftLanguage.codeMap[code]      = self
        BeblsoftLanguage.typeMap[bLanguageType] = self

    def __repr__(self):
        return "[{} type={}]".format(
            self.__class__.__name__, self.bLanguageType.name)

    @property
    def twoLetterCode(self):
        """
        Return language's two letter code
        """
        twoLetterCode = None
        for code in self.codeList:
            if len(code) == 2:
                twoLetterCode = code
                break
        if not twoLetterCode:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_NO_LANGUAGE_CODE,
                                msg="No two letter code bLanguage={}".format(self))
        return twoLetterCode

    @staticmethod
    def fromCode(code, raiseOnNone=True):
        """
        Return bLanguage from code
        Args
          code:
            Language Code
            Ex1. "af"    (Afrikaans)
            Ex2. "zh"    (Chinese)
            Ex3. "zh-TW" (Chinese)

        """
        bLanguage = BeblsoftLanguage.codeMap.get(code, None)
        if not bLanguage and raiseOnNone:
            raise BeblsoftError(code = BeblsoftErrorCode.GEN_UNKNOWN_LANGUAGE,
                                msg  = "No language specified for code '{}'".format(code))
        return bLanguage

    @staticmethod
    def fromType(bLanguageType, raiseOnNone=True):
        """
        Return bLanguage from name
        """
        bLanguage = BeblsoftLanguage.typeMap.get(bLanguageType, None)
        if not bLanguage and raiseOnNone:
            raise BeblsoftError(code = BeblsoftErrorCode.GEN_UNKNOWN_LANGUAGE,
                                msg  = "No language specified for type '{}'".format(bLanguageType))
        return bLanguage


# ----------------------------- BEBLSOFT LANGUAGES -------------------------- #
bLanguages = [
    BeblsoftLanguage(BeblsoftLanguageType.Afar, codeList=["aar", "aa"]),
    BeblsoftLanguage(BeblsoftLanguageType.Abkhazian, codeList=["abk", "ab"]),
    BeblsoftLanguage(BeblsoftLanguageType.Afrikaans, codeList=["afr", "af"]),
    BeblsoftLanguage(BeblsoftLanguageType.Akan, codeList=["aka", "ak"]),
    BeblsoftLanguage(BeblsoftLanguageType.Albanian, codeList=["sqi", "sq"]),
    BeblsoftLanguage(BeblsoftLanguageType.Amharic, codeList=["amh", "am"]),
    BeblsoftLanguage(BeblsoftLanguageType.Arabic, codeList=["ara", "ar"]),
    BeblsoftLanguage(BeblsoftLanguageType.Aragonese, codeList=["arg", "an"]),
    BeblsoftLanguage(BeblsoftLanguageType.Armenian, codeList=["hye", "hy"]),
    BeblsoftLanguage(BeblsoftLanguageType.Assamese, codeList=["asm", "as"]),
    BeblsoftLanguage(BeblsoftLanguageType.Avaric, codeList=["ava", "av"]),
    BeblsoftLanguage(BeblsoftLanguageType.Avestan, codeList=["ave", "ae"]),
    BeblsoftLanguage(BeblsoftLanguageType.Aymara, codeList=["aym", "ay"]),
    BeblsoftLanguage(BeblsoftLanguageType.Azerbaijani, codeList=["aze", "az"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bashkir, codeList=["bak", "ba"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bambara, codeList=["bam", "bm"]),
    BeblsoftLanguage(BeblsoftLanguageType.Basque, codeList=["eus", "eu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Belarusian, codeList=["bel", "be"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bengali, codeList=["ben", "bn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bihari, codeList=["bih", "bh"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bislama, codeList=["bis", "bi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bosnian, codeList=["bos", "bs"]),
    BeblsoftLanguage(BeblsoftLanguageType.Breton, codeList=["bre", "br"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bulgarian, codeList=["bul", "bg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Burmese, codeList=["mya", "my"]),
    BeblsoftLanguage(BeblsoftLanguageType.Catalan, codeList=["cat", "ca"]),
    BeblsoftLanguage(BeblsoftLanguageType.Chamorro, codeList=["cha", "ch"]),
    BeblsoftLanguage(BeblsoftLanguageType.Chechen, codeList=["che", "ce"]),
    BeblsoftLanguage(BeblsoftLanguageType.Chinese, codeList=["zho", "zh", "zh-TW"]),
    BeblsoftLanguage(BeblsoftLanguageType.Church, codeList=["chu", "cu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Chuvash, codeList=["chv", "cv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Cornish, codeList=["cor", "kw"]),
    BeblsoftLanguage(BeblsoftLanguageType.Corsican, codeList=["cos", "co"]),
    BeblsoftLanguage(BeblsoftLanguageType.Cree, codeList=["cre", "cr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Czech, codeList=["ces", "cs"]),
    BeblsoftLanguage(BeblsoftLanguageType.Danish, codeList=["dan", "da"]),
    BeblsoftLanguage(BeblsoftLanguageType.Divehi, codeList=["div", "dv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Dzongkha, codeList=["dzo", "dz"]),
    BeblsoftLanguage(BeblsoftLanguageType.English, codeList=["eng", "en"]),
    BeblsoftLanguage(BeblsoftLanguageType.Esperanto, codeList=["epo", "eo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Estonian, codeList=["est", "et"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ewe, codeList=["ewe", "ee"]),
    BeblsoftLanguage(BeblsoftLanguageType.Faroese, codeList=["fao", "fo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Fijian, codeList=["fij", "fj"]),
    BeblsoftLanguage(BeblsoftLanguageType.Finnish, codeList=["fin", "fi"]),
    BeblsoftLanguage(BeblsoftLanguageType.French, codeList=["fra", "fr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Western, codeList=["fry", "fy"]),
    BeblsoftLanguage(BeblsoftLanguageType.Fulah, codeList=["ful", "ff"]),
    BeblsoftLanguage(BeblsoftLanguageType.Georgian, codeList=["kat", "ka"]),
    BeblsoftLanguage(BeblsoftLanguageType.German, codeList=["deu", "de"]),
    BeblsoftLanguage(BeblsoftLanguageType.Gaelic, codeList=["gla", "gd"]),
    BeblsoftLanguage(BeblsoftLanguageType.Irish, codeList=["gle", "ga"]),
    BeblsoftLanguage(BeblsoftLanguageType.Galician, codeList=["glg", "gl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Manx, codeList=["glv", "gv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Greek, codeList=["ell", "el"]),
    BeblsoftLanguage(BeblsoftLanguageType.Guarani, codeList=["grn", "gn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Gujarati, codeList=["guj", "gu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Haitian, codeList=["hat", "ht"]),
    BeblsoftLanguage(BeblsoftLanguageType.Hausa, codeList=["hau", "ha"]),
    BeblsoftLanguage(BeblsoftLanguageType.Hebrew, codeList=["heb", "he"]),
    BeblsoftLanguage(BeblsoftLanguageType.Herero, codeList=["her", "hz"]),
    BeblsoftLanguage(BeblsoftLanguageType.Hindi, codeList=["hin", "hi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Croatian, codeList=["hrv", "hr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Hungarian, codeList=["hun", "hu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Igbo, codeList=["ibo", "ig"]),
    BeblsoftLanguage(BeblsoftLanguageType.Icelandic, codeList=["isl", "is"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ido, codeList=["ido", "io"]),
    BeblsoftLanguage(BeblsoftLanguageType.Inuktitut, codeList=["iku", "iu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Interlingue, codeList=["ile", "ie"]),
    BeblsoftLanguage(BeblsoftLanguageType.Interlingua, codeList=["ina", "ia"]),
    BeblsoftLanguage(BeblsoftLanguageType.Indonesian, codeList=["ind", "id"]),
    BeblsoftLanguage(BeblsoftLanguageType.Inupiaq, codeList=["ipk", "ik"]),
    BeblsoftLanguage(BeblsoftLanguageType.Italian, codeList=["ita", "it"]),
    BeblsoftLanguage(BeblsoftLanguageType.Javanese, codeList=["jav", "jv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Japanese, codeList=["jpn", "ja"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kalaallisut, codeList=["kal", "kl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kannada, codeList=["kan", "kn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kashmiri, codeList=["kas", "ks"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kanuri, codeList=["kau", "kr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kazakh, codeList=["kaz", "kk"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kikuyu, codeList=["kik", "ki"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kinyarwanda, codeList=["kin", "rw"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kirghiz, codeList=["kir", "ky"]),
    BeblsoftLanguage(BeblsoftLanguageType.Komi, codeList=["kom", "kv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kongo, codeList=["kon", "kg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Korean, codeList=["kor", "ko"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kuanyama, codeList=["kua", "kj"]),
    BeblsoftLanguage(BeblsoftLanguageType.Kurdish, codeList=["kur", "ku"]),
    BeblsoftLanguage(BeblsoftLanguageType.Lao, codeList=["lao", "lo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Latin, codeList=["lat", "la"]),
    BeblsoftLanguage(BeblsoftLanguageType.Latvian, codeList=["lav", "lv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Limburgan, codeList=["lim", "li"]),
    BeblsoftLanguage(BeblsoftLanguageType.Lingala, codeList=["lin", "ln"]),
    BeblsoftLanguage(BeblsoftLanguageType.Lithuanian, codeList=["lit", "lt"]),
    BeblsoftLanguage(BeblsoftLanguageType.Luxembourgish, codeList=["ltz", "lb"]),
    BeblsoftLanguage(BeblsoftLanguageType.Luba, codeList=["lub", "lu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ganda, codeList=["lug", "lg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Macedonian, codeList=["mkd", "mk"]),
    BeblsoftLanguage(BeblsoftLanguageType.Marshallese, codeList=["mah", "mh"]),
    BeblsoftLanguage(BeblsoftLanguageType.Malayalam, codeList=["mal", "ml"]),
    BeblsoftLanguage(BeblsoftLanguageType.Maori, codeList=["mri", "mi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Marathi, codeList=["mar", "mr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Malay, codeList=["msa", "ms"]),
    BeblsoftLanguage(BeblsoftLanguageType.Malagasy, codeList=["mlg", "mg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Maltese, codeList=["mlt", "mt"]),
    BeblsoftLanguage(BeblsoftLanguageType.Mongolian, codeList=["mon", "mn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Nauru, codeList=["nau", "na"]),
    BeblsoftLanguage(BeblsoftLanguageType.Navajo, codeList=["nav", "nv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ndonga, codeList=["ndo", "ng"]),
    BeblsoftLanguage(BeblsoftLanguageType.Nepali, codeList=["nep", "ne"]),
    BeblsoftLanguage(BeblsoftLanguageType.Dutch, codeList=["nld", "nl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Norwegian, codeList=["nno", "no", "nn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Bokmål, codeList=["nob", "nb"]),
    BeblsoftLanguage(BeblsoftLanguageType.Chichewa, codeList=["nya", "ny"]),
    BeblsoftLanguage(BeblsoftLanguageType.Occitan, codeList=["oci", "oc"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ojibwa, codeList=["oji", "oj"]),
    BeblsoftLanguage(BeblsoftLanguageType.Oriya, codeList=["ori", "or"]),
    BeblsoftLanguage(BeblsoftLanguageType.Oromo, codeList=["orm", "om"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ossetian, codeList=["oss", "os"]),
    BeblsoftLanguage(BeblsoftLanguageType.Panjabi, codeList=["pan", "pa"]),
    BeblsoftLanguage(BeblsoftLanguageType.Persian, codeList=["fas", "fa"]),
    BeblsoftLanguage(BeblsoftLanguageType.Pali, codeList=["pli", "pi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Polish, codeList=["pol", "pl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Portuguese, codeList=["por", "pt"]),
    BeblsoftLanguage(BeblsoftLanguageType.Pushto, codeList=["pus", "ps"]),
    BeblsoftLanguage(BeblsoftLanguageType.Quechua, codeList=["que", "qu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Romansh, codeList=["roh", "rm"]),
    BeblsoftLanguage(BeblsoftLanguageType.Romanian, codeList=["ron", "ro"]),
    BeblsoftLanguage(BeblsoftLanguageType.Rundi, codeList=["run", "rn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Russian, codeList=["rus", "ru"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sango, codeList=["sag", "sg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sanskrit, codeList=["san", "sa"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sinhala, codeList=["sin", "si"]),
    BeblsoftLanguage(BeblsoftLanguageType.Slovak, codeList=["slk", "sk"]),
    BeblsoftLanguage(BeblsoftLanguageType.Slovenian, codeList=["slv", "sl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Northern, codeList=["sme", "se"]),
    BeblsoftLanguage(BeblsoftLanguageType.Samoan, codeList=["smo", "sm"]),
    BeblsoftLanguage(BeblsoftLanguageType.Shona, codeList=["sna", "sn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sindhi, codeList=["snd", "sd"]),
    BeblsoftLanguage(BeblsoftLanguageType.Somali, codeList=["som", "so"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sotho, codeList=["sot", "st"]),
    BeblsoftLanguage(BeblsoftLanguageType.Spanish, codeList=["spa", "es"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sardinian, codeList=["srd", "sc"]),
    BeblsoftLanguage(BeblsoftLanguageType.Serbian, codeList=["srp", "sr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Swati, codeList=["ssw", "ss"]),
    BeblsoftLanguage(BeblsoftLanguageType.Sundanese, codeList=["sun", "su"]),
    BeblsoftLanguage(BeblsoftLanguageType.Swahili, codeList=["swa", "sw"]),
    BeblsoftLanguage(BeblsoftLanguageType.Swedish, codeList=["swe", "sv"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tahitian, codeList=["tah", "ty"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tamil, codeList=["tam", "ta"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tatar, codeList=["tat", "tt"]),
    BeblsoftLanguage(BeblsoftLanguageType.Telugu, codeList=["tel", "te"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tajik, codeList=["tgk", "tg"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tagalog, codeList=["tgl", "tl"]),
    BeblsoftLanguage(BeblsoftLanguageType.Thai, codeList=["tha", "th"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tibetan, codeList=["bod", "bo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tigrinya, codeList=["tir", "ti"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tonga, codeList=["ton", "to"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tswana, codeList=["tsn", "tn"]),
    BeblsoftLanguage(BeblsoftLanguageType.Tsonga, codeList=["tso", "ts"]),
    BeblsoftLanguage(BeblsoftLanguageType.Turkmen, codeList=["tuk", "tk"]),
    BeblsoftLanguage(BeblsoftLanguageType.Turkish, codeList=["tur", "tr"]),
    BeblsoftLanguage(BeblsoftLanguageType.Twi, codeList=["twi", "tw"]),
    BeblsoftLanguage(BeblsoftLanguageType.Uighur, codeList=["uig", "ug"]),
    BeblsoftLanguage(BeblsoftLanguageType.Ukrainian, codeList=["ukr", "uk"]),
    BeblsoftLanguage(BeblsoftLanguageType.Urdu, codeList=["urd", "ur"]),
    BeblsoftLanguage(BeblsoftLanguageType.Uzbek, codeList=["uzb", "uz"]),
    BeblsoftLanguage(BeblsoftLanguageType.Venda, codeList=["ven", "ve"]),
    BeblsoftLanguage(BeblsoftLanguageType.Vietnamese, codeList=["vie", "vi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Volapük, codeList=["vol", "vo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Welsh, codeList=["cym", "cy"]),
    BeblsoftLanguage(BeblsoftLanguageType.Walloon, codeList=["wln", "wa"]),
    BeblsoftLanguage(BeblsoftLanguageType.Wolof, codeList=["wol", "wo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Xhosa, codeList=["xho", "xh"]),
    BeblsoftLanguage(BeblsoftLanguageType.Yiddish, codeList=["yid", "yi"]),
    BeblsoftLanguage(BeblsoftLanguageType.Yoruba, codeList=["yor", "yo"]),
    BeblsoftLanguage(BeblsoftLanguageType.Zhuang, codeList=["zha", "za"]),
    BeblsoftLanguage(BeblsoftLanguageType.Zulu, codeList=["zul", "zu"]),
    BeblsoftLanguage(BeblsoftLanguageType.Cebuano, codeList=["ceb"]),
]
