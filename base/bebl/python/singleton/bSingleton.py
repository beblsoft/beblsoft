#!/usr/bin/env python
"""
 NAME
  bSingleton.py

 DESCRIPTION
  Singleton Functionality

 NOTE
  Mileage may vary
"""

# ------------------------------- IMPORTS ----------------------------------- #
import logging


# ------------------------------ GLOBALS ------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------------ BEBLSOFT SINGLETON METACLASS --------------- #
class BeblsoftSingleton(type):
    """
    Metaclass to create singleton objects
    https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

    Usage:
      class MyClass(BaseClass, metaclass=BeblsoftSingleton):
          pass
    """
    __instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Call Object
        """
        logger.info("args={}, kwargs={}".format(args, kwargs))
        if cls not in cls.__instances:
            cls.__instances[cls] = super(BeblsoftSingleton,
                                         cls).__call__(*args, **kwargs)
        return cls.__instances[cls]
