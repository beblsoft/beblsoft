#!/usr/bin/env python3
"""
NAME:
 bLogger.py

DESCRIPTION
 Beblsoft Logger Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import json
import logging
from base.bebl.python.json.bEncoder import BeblsoftJSONEncoder


# ------------------------ GLOBALS ------------------------------------------ #
bLogger = logging.getLogger(__name__)


# ----------------------- BEBLSOFT LOGGER CLASS ----------------------------- #
class BeblsoftLogger():
    """
    Beblsoft Logger
    """

    # ------------------- LOG ----------------------------------------------- #
    @staticmethod
    def log(msg               = "",  # pylint: disable=W0102
            level             = logging.INFO,
            logger            = bLogger,
            isException       = False,
            bLogCode          = None,
            bLogValue         = None,
            bError            = None,
            filterWordList    = ["password"],
            **kwargs):
        """
        Log a record
        Args
          level:
            Log level
            logging.CRITICAL - Program cannot continue, Rarely used. I.e. Network down
            logging.ERROR    - Errors, exceptions. I.e. DB IO failed
            logging.WARNING  - Non-regular events. I.e. taking a long time to connect to database
            logging.INFO     - Regular events. Production logs here. I.e. Customer Logs In.
            logging.DEBUG    - Verbose. I.e. @logFunc() decorator that logs function calls
            logging.NOTSET   - Rarely used
            Reference: https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
          isException:
            If True, Log as an exception
            Ex. False
          msg:
            Log message
            Ex. "Hello World"
            Note: must not contain its own JSON object as CloudWatch Logs Insights
            only parses for the first JSON object in the log output message
          bLogCode:
            Beblsoft Log Code
            Ex. BeblsoftLogCode.AUTH_LOGIN_SUCCESS
          bLogValue:
            Value assoicated with bLogCode
            Ex. 120938
          logger:
            Python logger to use to log the message
            Default: bLogger in this file
          bError:
            Beblsoft Error
          filterWordList:
            Lists of words to filter
            NOTE: all kwargs will have filter words removed!
            Ex. ["password"]
          kwargs:
            Dictionary of args to attach to log record
            Ex. { 'aID': 12987, 'isValid': True }

        Note:
          All objects must be JSON encodable via BeblsoftJSONEncoder

        Format
          The following keys are available in many (but not all) logs
          <message> {
            bLogCode: <string>
            logValue: <number>|<string>
            bError: {}
          }
        """

        # Machine Parseable Message
        msgDict = {}
        msgDict.update(kwargs)

        # Log Code, Value
        if bLogCode:
            msgDict["bLogCode"]  = bLogCode.name
        if bLogValue:
            msgDict["bLogValue"] = bLogValue

        # Error information
        if bError:
            msgDict["bError"] = bError

        # Message: iterate through and filter each key
        # Note: don't indent JSON as this adds lots of extra unnecessary space to log files
        for k, v in msgDict.items():
            vJSON = json.dumps(v, cls=BeblsoftJSONEncoder, sort_keys=True)
            for filterWord in filterWordList:
                if filterWord.lower() in vJSON.lower():
                    msgDict[k] = "Content Filtered"
        msgJSON = json.dumps(msgDict, cls=BeblsoftJSONEncoder, sort_keys=True)
        msg     = "{} {}".format(msg, msgJSON)

        # Make a log entry
        if isException:
            logger.exception(msg=msg)
        else:
            logger.log(level=level, msg=msg)

    # ------------------- LOG WRAPPERS -------------------------------------- #
    @staticmethod
    def debug(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(level=logging.DEBUG, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)

    @staticmethod
    def info(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(level=logging.INFO, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)

    @staticmethod
    def warning(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(level=logging.WARNING, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)

    @staticmethod
    def error(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(level=logging.ERROR, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)

    @staticmethod
    def critical(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(level=logging.CRITICAL, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)

    @staticmethod
    def exception(**kwargs):  # pylint: disable=C0111
        BeblsoftLogger.log(isException=True, callerStackIndex=kwargs.pop("callerStackIndex", 2), **kwargs)
