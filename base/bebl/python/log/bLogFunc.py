#!/usr/bin/env python3
"""
NAME:
 bLogFunc.py

DESCRIPTION
 Beblsoft Log Function Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import logging
from datetime import datetime
from functools import wraps
import threading
import traceback
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict


# ----------------------- GLOBALS ------------------------------------------- #
logger     = logging.getLogger(__file__)
threadData = threading.local()  # Each thread gets its own copy


# ----------------------- LOG FUNC DECORATOR -------------------------------- #
def logFunc(logArgs=True, logRval=True, level=logging.DEBUG, ignoreArgsList=['password']):  # pylint: disable=W0102,R0915
    """
    Decorator to log functions with arguments
    Args
      logArgs:
        If True, log arguments
      logRval:
        If True, log return value
      level:
        Level at which to log function
        Ex. logging.DEBUG
      ignoreArgsList:
        List of arguments to NOT log, case insensitive
        Ex. ['password', 'bankID']
    """
    def logFuncInner(func):  # pylint: disable=R0915
        """
        Real decorator needed to pass arguments
        Args
          func:
            function being decorated
        Returns:
          wrapper:
            wrapped function
        """
        funcName      = func.__name__
        modName       = func.__module__
        headerLen     = 120
        nEqualsStart  = 10
        nSpacesIndent = 4

        def setupThreadData():
            """
            Setup thread data

            Sets up
              threadData.logFunc.indentSpaces
              threadData.logFunc.lastException
            """
            if not getattr(threadData, 'logFunc', None):
                threadData.logFunc = BeblsoftAttrDict()
            if not getattr(threadData.logFunc, 'indentSpaces', None):
                threadData.logFunc.indentSpaces = 0
            if not getattr(threadData.logFunc, 'lastException', None):
                threadData.logFunc.lastException = None

        def ignoreArgsInString(string):
            """
            Return True if an ignored arg is in string
            """
            for ignoreArg in ignoreArgsList:
                if ignoreArg.lower() in string.lower():
                    return True
            return False

        def logStartLine(indentSpaces, *args, **kwargs):
            """
            Log function Start line
            """
            argStr = ""
            if logArgs:
                if args:
                    argStr += "args={}".format(args)
                if kwargs:
                    argStr  += "kwargs={}".format(kwargs)
                argStr = "IgnoreArgs" if ignoreArgsInString(
                    string=argStr) else argStr
            startLine = "{}{}ENTER:{}[{}]".format(
                " " * indentSpaces, "=" * nEqualsStart, modName, funcName)
            nEqualsEnd = headerLen - len(startLine)
            startLine  = "{}{} {}".format(startLine, "=" * nEqualsEnd, argStr)
            logger.log(level=level, msg=startLine)

        def logEndLine(rval, startTime, endTime, indentSpaces):
            """
            Log Function End line
            """
            if logRval:
                rvalStr = "rval={}".format(rval)
            endLine = "{}{}EXIT:{}[{}],{} ".format(
                " " * indentSpaces, "=" * nEqualsStart, modName, funcName, endTime - startTime)
            nEqualsEnd = headerLen - len(endLine)
            endLine = "{}{} {}".format(endLine, "=" * nEqualsEnd, rvalStr)
            logger.log(level=level, msg=endLine)

        def logException(e):
            """
            Log Exception
            """
            if threadData.logFunc.lastException != e:
                logger.log(level=level, msg=traceback.format_exc().strip())
            threadData.logFunc.lastException = e

        def getUpdateIndentSpaces():
            """
            Get and update indent spaces
            """
            spaces = threadData.logFunc.indentSpaces
            threadData.logFunc.indentSpaces = spaces + nSpacesIndent
            return spaces

        def resetIndentSpaces():
            """
            Reset indent spaces value
            """
            threadData.logFunc.indentSpaces -= nSpacesIndent

        @wraps(func)
        def wrapper(*args, **kwargs):  # pylint: disable=C0111
            """
            Wrapper to execute function

            Note:
              Set startTime before logStartLine() as logStartLine could throw
              exceptions will trying to access arges (Ex. sqlalchemy args where
              DB is gone)
            """
            rval      = None
            startTime = datetime.now()
            endTime   = None

            setupThreadData()
            indentSpaces = getUpdateIndentSpaces()
            try:
                # Time and Execute Function
                logStartLine(indentSpaces, *args, **kwargs)
                rval = func(*args, **kwargs)
            except Exception as e:
                logException(e)
                raise e
            finally:
                endTime = datetime.now()
                logEndLine(rval, startTime, endTime, indentSpaces)
                resetIndentSpaces()
            return rval

        return wrapper
    return logFuncInner
