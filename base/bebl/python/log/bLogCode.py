#!/usr/bin/env python3
"""
NAME:
 bLogCode.py

DESCRIPTION
 Beblsoft Log Code Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum


# ----------------------- BEBLSOFT LOG CODE ENUM ---------------------------- #
class BeblsoftLogCode(enum.Enum):
    """
    Beblsoft Log Code Enumeration

    Note:
      Numbers shouldn't matter here as all enum members should always be accessed
      by their string names. Hence the enum.auto()
    """

    # ------------------- API ----------------------------------------------- #
    API_REQUEST_START                    = enum.auto()  # @ Debug
    API_REQUEST_COMPLETE                 = enum.auto()
    API_ERROR                            = enum.auto()

    # ------------------- AUTH ---------------------------------------------- #
    AUTH_LOGIN_SUCCESS                   = enum.auto()
    AUTH_LOGOUT_SUCCESS                  = enum.auto()

    # ------------------- GLOBAL JOB ---------------------------------------- #
    GJOB_SETUP                           = enum.auto()
    GJOB_SETUP_DUPLICATE                 = enum.auto()
    GJOB_TEARDOWN                        = enum.auto()
    GJOB_TEARDOWN_FAILED                 = enum.auto()
    GJOB_EXCEPTION                       = enum.auto()
    GJOB_HEARTBEAT_FAILED                = enum.auto()

    # ------------------- ACCOUNT JOB --------------------------------------- #
    AJOB_SETUP                           = enum.auto()
    AJOB_SETUP_DUPLICATE                 = enum.auto()
    AJOB_TEARDOWN                        = enum.auto()
    AJOB_TEARDOWN_FAILED                 = enum.auto()
    AJOB_EXCEPTION                       = enum.auto()
    AJOB_HEARTBEAT_FAILED                = enum.auto()

    # ------------------- LAMBDA SWITCH ------------------------------------- #
    LAMBDA_SWITCH_REQUEST_START          = enum.auto()  # @ Debug
    LAMBDA_SWITCH_REQUEST_DONE           = enum.auto()  # @ Debug
    LAMBDA_SWITCH_EXCEPTION              = enum.auto()
    LAMBDA_SWITCH_EVENT_NOT_HANDLED      = enum.auto()
    LAMBDA_SWITCH_ERROR_HANDLER          = enum.auto()

    # ------------------- SQS ----------------------------------------------- #
    SQS_DEAD_LETTER_RECEIVED             = enum.auto()

    # ------------------- PAYMENTS ------------------------------------------ #
    PAYMENT_CHARGED_USD                  = enum.auto()

    # ------------------- COMPREHEND ---------------------------------------- #
    COMPREHEND_SENTIMENT_ANALYZED        = enum.auto()
    COMPREHEND_SENTIMENT_FAILED          = enum.auto()
    COMPREHEND_LANGUAGE_ANALYZED         = enum.auto()
    COMPREHEND_LANGUAGE_FAILED           = enum.auto()

    # ------------------- FACEBOOK ------------------------------------------ #
    FACEBOOK_POST_SYNC_FAILED            = enum.auto()
    FACEBOOK_USER_RECORD_SYNC_FAILED     = enum.auto()
