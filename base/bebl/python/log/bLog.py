#!/usr/bin/env python3
"""
NAME:
 bLog.py

DESCRIPTION
 Beblsoft Log Functionality

USEFUL LINKS
 Log Formatter Variables  - https://docs.python.org/2/library/logging.html#logrecord-attributes
"""

# ----------------------- IMPORTS ------------------------------------------- #
import os
import sys
import logging
import threading


# ----------------------- GLOBALS ------------------------------------------- #
logger      = logging.getLogger(__file__)
threadData  = threading.local()  # Each thread gets its own copy


# ----------------------- BEBLSOFT LOG CLASS -------------------------------- #
class BeblsoftLog():
    """
    Beblsoft Log
    Wrapper class over logging interfaces
    """

    def __init__(self, #pylint: disable=W0102
                 logFile=None, logFileAppend=False,
                 cFilter=20,
                 nHeaderStars=90, encoding='utf-8',
                 warningMods=["urllib3", "boto3", "botocore", "alembic", "stripe"],
                 errorMods=[],
                 criticalMods=["flask"]):
        """
        Initialize object
        Args
          logFile:
            path of log file
            ex. "/tmp/foo.log"
          logFileAppend:
            If True, append to existing log file
            Otherwise truncate it
          logFileMaxStartLenMB:
            Length (in MB) to Truncate logfile
          cFilter:
            Filter to apply to log console messages
            most filtered: logging.CRITICAL
                CRITICAL  50
                ERROR     40
                WARNING   30
                INFO      20
                DEBUG     10
                NOTSET    0
                least filtered: logging.NOTSET
         nHeaderStars:
           number of stars for header and footer
         warningModes:
           List of Modules to set to warning
           "urllib3" - connectionpool, retry
        """
        self.logFile              = logFile
        self.logFileAppend        = logFileAppend
        self.nHeaderStars         = nHeaderStars
        self.cFilter              = cFilter

        # Silence Noisy Modules
        for mod in warningMods:
            logging.getLogger(mod).setLevel(logging.WARNING)
        for mod in errorMods:
            logging.getLogger(mod).setLevel(logging.ERROR)
        for mod in criticalMods:
            logging.getLogger(mod).setLevel(logging.CRITICAL)

        # Console Log
        self.cHandler = logging.StreamHandler()
        self.cHandler.setLevel(self.cFilter)
        self.cFormatter = logging.Formatter(fmt="%(message)s")
        self.cHandler.setFormatter(self.cFormatter)

        # File Log
        self.initLogFile()
        self.fHandler = logging.FileHandler(logFile, encoding=encoding)
        self.fHandler.setLevel(logging.DEBUG)
        self.fFormatter = logging.Formatter(
            fmt="%(asctime)s %(levelname)8s %(threadName)10s:%(message)s",
            datefmt="%m/%d/%Y %H:%M:%S")
        self.fHandler.setFormatter(self.fFormatter)

        # Root Logger
        logging.basicConfig(level=logging.DEBUG, handlers=[self.cHandler, self.fHandler])

    def initLogFile(self):
        """
        Initialize log file
        """
        # Create directory if it doesn't exist
        directory = os.path.dirname(self.logFile)
        if not os.path.exists(directory):
            os.makedirs(directory)

        # Touch the log file
        # Optionally truncate
        with open(self.logFile, "a") as f:
            os.utime(self.logFile, None)
            if not self.logFileAppend:
                f.truncate(0)

        # Full read/write permissions, useful when containers create log
        # files as root
        os.chmod(self.logFile, 0o666)

    def logHeader(self):
        """
        Log Header
        """
        logger.debug("*" * self.nHeaderStars)
        logger.debug("Command Start: {}".format(" ".join(sys.argv)))
        logger.info("Logfile: {}".format(self.logFile))
        logger.debug("*" * self.nHeaderStars)

    def logFooter(self, startTime=None, endTime=None):
        """
        Log Footer
        """
        logger.debug("*" * self.nHeaderStars)
        logger.debug("Command End: {}".format(" ".join(sys.argv)))
        if startTime:
            logger.debug("Start Time: {}".format(startTime))
        if endTime:
            logger.debug("End Time: {}".format(endTime))
        if startTime and endTime:
            logger.debug("Elapsed Time: {}".format(endTime - startTime))
        logger.debug("*" * self.nHeaderStars)


    # ------------------- CONSOLE HANDLER ----------------------------------- #
    def addConsoleFilter(self, filt):
        """
        Add console filter
        """
        self.cHandler.addFilter(filt)

    def removeConsoleFilter(self, filt):
        """
        Remove console filter
        """
        self.cHandler.removeFilter(filt)

    # ------------------- FILE HANDLER -------------------------------------- #
    def addFileFilter(self, filt):
        """
        Add file filter
        """
        self.fHandler.addFilter(filt)

    def removeFileFilter(self, filt):
        """
        Remove file filter
        """
        self.fHandler.removeFilter(filt)
