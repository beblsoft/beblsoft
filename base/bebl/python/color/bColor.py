#!/usr/bin/env python3
"""
NAME:
 bColor.py

DESCRIPTION
 Beblsoft Color Functionality

REFERENCE
 https://gist.github.com/tzuryby/1474991
"""


# ----------------------- COLOR CLASS --------------------------------------- #
class BeblsoftColor:
    """
    ANSI colors
    """
    # Colors
    NORMAL = "\033[0m"
    BLACK  = "\033[30m"
    RED    = "\033[31m"
    GREEN  = "\033[32m"
    YELLOW = "\033[33m"
    BLUE   = "\033[34m"
    PURPLE = "\033[35m"
    CYAN   = "\033[36m"
    GREY   = "\033[37m"

    # Fonts
    BOLD   = "\033[1m"
    ULINE  = "\033[4m"
    BLINK  = "\033[5m"
    INVERT = "\033[7m"

    @staticmethod
    def str2Color(s, toColor):
        """
        Return recolored string
        """
        return "{}{}{}".format(toColor, s, BeblsoftColor.NORMAL)
