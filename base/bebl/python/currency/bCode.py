#!/usr/bin/env python3
"""
 NAME
  bCode.py

 DESCRIPTION
  Beblsoft Currency Code Functionality
"""


# ----------------------------- GLOBALS ------------------------------------- #
import enum


# ----------------------------- CURRENCY CODE ------------------------------- #
class BeblsoftCurrencyCode(enum.Enum):
    """
    Beblsoft Currency Code

    Note:
      Codes taken from ISO_4217
      https://en.wikipedia.org/wiki/ISO_4217
      Add new codes to the end of the list
    """
    ALL = 1
    AMD = 2
    ANG = 3
    AOA = 4
    ARS = 5
    AUD = 6
    AWG = 7
    AZN = 8
    BAM = 9
    BBD = 10
    BDT = 11
    BGN = 12
    BHD = 13
    BIF = 14
    BMD = 15
    BND = 16
    BOB = 17
    BOV = 18
    BRL = 19
    BSD = 20
    BTN = 21
    BWP = 22
    BYN = 23
    BZD = 24
    CAD = 25
    CDF = 26
    CHE = 27
    CHF = 28
    CHW = 29
    CLF = 30
    CLP = 31
    CNY = 32
    COP = 33
    COU = 34
    CRC = 35
    CUC = 36
    CUP = 37
    CVE = 38
    CZK = 39
    DJF = 40
    DKK = 41
    DOP = 42
    DZD = 43
    EGP = 44
    ERN = 45
    ETB = 46
    EUR = 47
    FJD = 48
    FKP = 49
    GBP = 50
    GEL = 51
    GHS = 52
    GIP = 53
    GMD = 54
    GNF = 55
    GTQ = 56
    GYD = 57
    HKD = 58
    HNL = 59
    HRK = 60
    HTG = 61
    HUF = 62
    IDR = 63
    ILS = 64
    INR = 65
    IQD = 66
    IRR = 67
    ISK = 68
    JMD = 69
    JOD = 70
    JPY = 71
    KES = 72
    KGS = 73
    KHR = 74
    KMF = 75
    KPW = 76
    KRW = 77
    KWD = 78
    KYD = 79
    KZT = 80
    LAK = 81
    LBP = 82
    LKR = 83
    LRD = 84
    LSL = 85
    LYD = 86
    MAD = 87
    MDL = 88
    MGA = 89
    MKD = 90
    MMK = 91
    MNT = 92
    MOP = 93
    MRU = 94
    MUR = 95
    MVR = 96
    MWK = 97
    MXN = 98
    MXV = 99
    MYR = 100
    MZN = 101
    NAD = 102
    NGN = 103
    NIO = 104
    NOK = 105
    NPR = 106
    NZD = 107
    OMR = 108
    PAB = 109
    PEN = 110
    PGK = 111
    PHP = 112
    PKR = 113
    PLN = 114
    PYG = 115
    QAR = 116
    RON = 117
    RSD = 118
    RUB = 119
    RWF = 120
    SAR = 121
    SBD = 122
    SCR = 123
    SDG = 124
    SEK = 125
    SGD = 126
    SHP = 127
    SLL = 128
    SOS = 129
    SRD = 130
    SSP = 131
    STN = 132
    SVC = 133
    SYP = 134
    SZL = 135
    THB = 136
    TJS = 137
    TMT = 138
    TND = 139
    TOP = 140
    TRY = 141
    TTD = 142
    TWD = 143
    TZS = 144
    UAH = 145
    UGX = 146
    USD = 147
    USN = 148
    UYI = 149
    UYU = 150
    UYW = 151
    UZS = 152
    VES = 153
    VND = 154
    VUV = 155
    WST = 156
    XAF = 157
    XAG = 158
    XAU = 159
    XBA = 160
    XBB = 161
    XBC = 162
    XBD = 163
    XCD = 164
    XDR = 165
    XOF = 166
    XPD = 167
    XPF = 168
    XPT = 169
    XSU = 170
    XTS = 171
    XUA = 172
    XXX = 173
    YER = 174
    ZAR = 175
    ZMW = 176
    AED = 177
    AFN = 178

    def toStripeCode(self):
        """
        Convert BeblsoftCurrencyCode to stripe code
        """
        return str(self.name).lower()

    @staticmethod
    def fromStripeCode(stripeCode):
        """
        Return BeblsoftCurrencyCode from stripe code
        """
        return BeblsoftCurrencyCode[str(stripeCode).upper()]
