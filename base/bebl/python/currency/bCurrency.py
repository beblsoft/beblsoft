#!/usr/bin/env python3
"""
 NAME
  bCurrency.py

 DESCRIPTION
  Beblsoft Currency Functionality

 REFERENCE
  ISO 4217           : https://www.iso.org/iso-4217-currency-codes.html
  Wikipedia ISO 4217 : https://en.wikipedia.org/wiki/ISO_4217
  Stripe             : https://stripe.com/docs/currencies#presentment-currencies
"""


# ----------------------------- GLOBALS ------------------------------------- #
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.currency.bCode import BeblsoftCurrencyCode


# ----------------------------- CURRENCY ------------------------------------ #
class BeblsoftCurrency():
    """
    Beblsoft Currency
    """

    bCodeMap  = {}
    numStrMap = {}
    nameMap   = {}

    def __init__(self, bCode, numStr, name):
        """
        Initialize Object
          Args
            bCode:
              Beblsoft Currency Code
              Ex. BeblsoftCurrencyCode.AED
            numStr:
              Currency number string (ISO 4217)
              Ex. "784"
            name:
              Currency name
              Ex. "United Arab Emirates dirham"
        """
        self.bCode                         = bCode
        self.numStr                        = numStr
        self.name                          = name
        BeblsoftCurrency.bCodeMap[bCode]   = self
        BeblsoftCurrency.numStrMap[numStr] = self
        BeblsoftCurrency.nameMap[name]     = self

    def __str__(self):
        return "[{} bCode={} numStr={} name={}]".format(
            self.__class__.__name__, self.bCode, self.numStr, self.name)

    @staticmethod
    def bCurrencyFromCode(bCode, raiseOnNone=True):
        """
        Return bCurrency from bCode
        Args
          bCode:
            Beblsoft Currency Code
            Ex. BeblsoftCurrencyCode.USD
        """
        bCurrency = BeblsoftCurrency.bCodeMap.get(bCode, None)
        if not bCurrency and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_UNKNOWN_CURRENCY,
                                msg="No currency specified for bCode={}".format(bCode))

    @staticmethod
    def bCurrencyFromNumStr(numStr, raiseOnNone=True):
        """
        Return bCurrency from num string
        Args
          numStr:
            Currency Number
            Ex. "784"
        """
        bCurrency = BeblsoftCurrency.numStrMap.get(numStr, None)
        if not bCurrency and raiseOnNone:
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_UNKNOWN_CURRENCY,
                                msg="No currency specified for numStr={}".format(numStr))


# ----------------------------- BEBLSOFT CURRENCIES ------------------------- #
bCurrencyList = [
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AED, numStr="784", name="United Arab Emirates dirham"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AFN, numStr="971", name="Afghan afghani"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ALL, numStr="008", name="Albanian lek"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AMD, numStr="051", name="Armenian dram"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ANG, numStr="532", name="Netherlands Antillean guilder"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AOA, numStr="973", name="Angolan kwanza"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ARS, numStr="032", name="Argentine peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AUD, numStr="036", name="Australian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AWG, numStr="533", name="Aruban florin"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.AZN, numStr="944", name="Azerbaijani manat"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BAM, numStr="977",
                     name="Bosnia and Herzegovina convertible mark"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BBD, numStr="052", name="Barbados dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BDT, numStr="050", name="Bangladeshi taka"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BGN, numStr="975", name="Bulgarian lev"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BHD, numStr="048", name="Bahraini dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BIF, numStr="108", name="Burundian franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BMD, numStr="060", name="Bermudian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BND, numStr="096", name="Brunei dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BOB, numStr="068", name="Boliviano"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BOV, numStr="984", name="Bolivian Mvdol"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BRL, numStr="986", name="Brazilian real"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BSD, numStr="044", name="Bahamian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BTN, numStr="064", name="Bhutanese ngultrum"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BWP, numStr="072", name="Botswana pula"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BYN, numStr="933", name="Belarusian ruble"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.BZD, numStr="084", name="Belize dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CAD, numStr="124", name="Canadian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CDF, numStr="976", name="Congolese franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CHE, numStr="947", name="WIR Euro"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CHF, numStr="756", name="Swiss franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CHW, numStr="948", name="WIR Franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CLF, numStr="990", name="Unidad de Fomento"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CLP, numStr="152", name="Chilean peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CNY, numStr="156", name="Chinese yuan"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.COP, numStr="170", name="Colombian peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.COU, numStr="970", name="Unidad de Valor Real"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CRC, numStr="188", name="Costa Rican colon"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CUC, numStr="931", name="Cuban convertible peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CUP, numStr="192", name="Cuban peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CVE, numStr="132", name="Cape Verde escudo"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.CZK, numStr="203", name="Czech koruna"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.DJF, numStr="262", name="Djiboutian franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.DKK, numStr="208", name="Danish krone"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.DOP, numStr="214", name="Dominican peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.DZD, numStr="012", name="Algerian dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.EGP, numStr="818", name="Egyptian pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ERN, numStr="232", name="Eritrean nakfa"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ETB, numStr="230", name="Ethiopian birr"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.EUR, numStr="978", name="Euro"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.FJD, numStr="242", name="Fiji dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.FKP, numStr="238", name="Falkland Islands pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GBP, numStr="826", name="Pound sterling"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GEL, numStr="981", name="Georgian lari"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GHS, numStr="936", name="Ghanaian cedi"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GIP, numStr="292", name="Gibraltar pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GMD, numStr="270", name="Gambian dalasi"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GNF, numStr="324", name="Guinean franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GTQ, numStr="320", name="Guatemalan quetzal"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.GYD, numStr="328", name="Guyanese dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.HKD, numStr="344", name="Hong Kong dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.HNL, numStr="340", name="Honduran lempira"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.HRK, numStr="191", name="Croatian kuna"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.HTG, numStr="332", name="Haitian gourde"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.HUF, numStr="348", name="Hungarian forint"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.IDR, numStr="360", name="Indonesian rupiah"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ILS, numStr="376", name="Israeli new shekel"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.INR, numStr="356", name="Indian rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.IQD, numStr="368", name="Iraqi dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.IRR, numStr="364", name="Iranian rial"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ISK, numStr="352", name="Icelandic króna"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.JMD, numStr="388", name="Jamaican dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.JOD, numStr="400", name="Jordanian dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.JPY, numStr="392", name="Japanese yen"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KES, numStr="404", name="Kenyan shilling"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KGS, numStr="417", name="Kyrgyzstani som"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KHR, numStr="116", name="Cambodian riel"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KMF, numStr="174", name="Comoro franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KPW, numStr="408", name="North Korean won"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KRW, numStr="410", name="South Korean won"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KWD, numStr="414", name="Kuwaiti dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KYD, numStr="136", name="Cayman Islands dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.KZT, numStr="398", name="Kazakhstani tenge"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LAK, numStr="418", name="Lao kip"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LBP, numStr="422", name="Lebanese pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LKR, numStr="144", name="Sri Lankan rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LRD, numStr="430", name="Liberian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LSL, numStr="426", name="Lesotho loti"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.LYD, numStr="434", name="Libyan dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MAD, numStr="504", name="Moroccan dirham"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MDL, numStr="498", name="Moldovan leu"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MGA, numStr="969", name="Malagasy ariary"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MKD, numStr="807", name="Macedonian denar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MMK, numStr="104", name="Myanmar kyat"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MNT, numStr="496", name="Mongolian tögrög"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MOP, numStr="446", name="Macanese pataca"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MRU, numStr="929", name="Mauritanian ouguiya"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MUR, numStr="480", name="Mauritian rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MVR, numStr="462", name="Maldivian rufiyaa"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MWK, numStr="454", name="Malawian kwacha"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MXN, numStr="484", name="Mexican peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MXV, numStr="979", name="Mexican Unidad de Inversion"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MYR, numStr="458", name="Malaysian ringgit"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.MZN, numStr="943", name="Mozambican metical"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NAD, numStr="516", name="Namibian dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NGN, numStr="566", name="Nigerian naira"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NIO, numStr="558", name="Nicaraguan córdoba"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NOK, numStr="578", name="Norwegian krone"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NPR, numStr="524", name="Nepalese rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.NZD, numStr="554", name="New Zealand dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.OMR, numStr="512", name="Omani rial"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PAB, numStr="590", name="Panamanian balboa"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PEN, numStr="604", name="Peruvian sol"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PGK, numStr="598", name="Papua New Guinean kina"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PHP, numStr="608", name="Philippine peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PKR, numStr="586", name="Pakistani rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PLN, numStr="985", name="Polish złoty"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.PYG, numStr="600", name="Paraguayan guaraní"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.QAR, numStr="634", name="Qatari riyal"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.RON, numStr="946", name="Romanian leu"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.RSD, numStr="941", name="Serbian dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.RUB, numStr="643", name="Russian ruble"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.RWF, numStr="646", name="Rwandan franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SAR, numStr="682", name="Saudi riyal"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SBD, numStr="090", name="Solomon Islands dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SCR, numStr="690", name="Seychelles rupee"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SDG, numStr="938", name="Sudanese pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SEK, numStr="752", name="Swedish krona"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SGD, numStr="702", name="Singapore dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SHP, numStr="654", name="Saint Helena pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SLL, numStr="694", name="Sierra Leonean leone"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SOS, numStr="706", name="Somali shilling"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SRD, numStr="968", name="Surinamese dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SSP, numStr="728", name="South Sudanese pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.STN, numStr="930", name="São Tomé and Príncipe dobra"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SVC, numStr="222", name="Salvadoran colón"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SYP, numStr="760", name="Syrian pound"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.SZL, numStr="748", name="Swazi lilangeni"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.THB, numStr="764", name="Thai baht"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TJS, numStr="972", name="Tajikistani somoni"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TMT, numStr="934", name="Turkmenistan manat"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TND, numStr="788", name="Tunisian dinar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TOP, numStr="776", name="Tongan paʻanga"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TRY, numStr="949", name="Turkish lira"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TTD, numStr="780", name="Trinidad and Tobago dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TWD, numStr="901", name="New Taiwan dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.TZS, numStr="834", name="Tanzanian shilling"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UAH, numStr="980", name="Ukrainian hryvnia"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UGX, numStr="800", name="Ugandan shilling"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.USD, numStr="840", name="United States dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.USN, numStr="997", name="United States dollar next day"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UYI, numStr="940", name="Uruguay Peso en Unidades Indexadas"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UYU, numStr="858", name="Uruguayan peso"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UYW, numStr="927", name="Unidad previsional"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.UZS, numStr="860", name="Uzbekistan som"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.VES, numStr="928", name="Venezuelan bolívar soberano"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.VND, numStr="704", name="Vietnamese đồng"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.VUV, numStr="548", name="Vanuatu vatu"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.WST, numStr="882", name="Samoan tala"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XAF, numStr="950", name="CFA franc BEAC"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XAG, numStr="961", name="Silver"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XAU, numStr="959", name="Gold"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XBA, numStr="955", name="European Composite Unit"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XBB, numStr="956", name="European Monetary Unit"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XBC, numStr="957", name="European Unit of Account"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XBD, numStr="958", name="European Unit of Account"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XCD, numStr="951", name="East Caribbean dollar"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XDR, numStr="960", name="Special drawing rights"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XOF, numStr="952", name="CFA franc BCEAO"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XPD, numStr="964", name="Palladium"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XPF, numStr="953", name="CFP franc"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XPT, numStr="962", name="Platinum"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XSU, numStr="994", name="SUCRE"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XTS, numStr="963", name="Code reserved for testing purposes"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XUA, numStr="965", name="ADB Unit of Account"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.XXX, numStr="999", name="No currency"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.YER, numStr="886", name="Yemeni rial"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ZAR, numStr="710", name="South African rand"),
    BeblsoftCurrency(bCode=BeblsoftCurrencyCode.ZMW, numStr="967", name="Zambian kwacha")
]
