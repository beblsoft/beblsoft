#!/usr/bin/env python3
"""
 NAME
  decorators.py

 DESCRIPTION
  Common Beblosoft decorators
"""


# ------------------------------- IMPORTS ----------------------------------- #
import logging
from functools import wraps
import traceback
from flask import request
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------------- DECORATORS -------------------------------- #
def exceptFunc(func):
    """
    Decorator to log a function's exception
    Args
      func: the function being called
    Returns
      wrapper: wrapped function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):  # pylint: disable=C0111
        try:
            rval = func(*args, **kwargs)
        except Exception as e:  # pylint: disable=W0703
            logger.error("Exception:{}\n{}".format(
                e, traceback.format_exc()))
            rval = None
        else:
            pass
        finally:
            return rval  # pylint: disable=W0150

    return wrapper


def jsonRequired(func):
    """
    Decorator to force request JSON required
    Args
      func: the function being wrapped
    Returns
      wrapper: wrapped function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):  # pylint: disable=C0111
        if not request.is_json:
            raise BeblsoftError(BeblsoftErrorCode.GEN_JSON_REQUIRED)
        return func(*args, **kwargs)
    return wrapper
