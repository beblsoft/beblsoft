#!/usr/bin/env python3
"""
NAME:
 bStream.py

DESCRIPTION
 Beblsoft Stream Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
from io import IOBase


# ----------------------- BEBLSOFT STREAM CLASS ----------------------------- #
class BeblsoftStream(IOBase):
    """
    Beblsoft Stream
    """

    def __init__(self, writeFunc):
        """
        Initialize object
        Args
          writeFunc:
            Function to write stream
            Ex1. lambda b: buf.append(b)
            Ex2. logger.info
        """
        super().__init__()
        self.writeFunc = writeFunc

    def write(self, b):
        """
        Write bytes
        """
        if self.writeFunc:
            self.writeFunc(b)
