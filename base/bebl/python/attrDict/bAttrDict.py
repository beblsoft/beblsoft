#!/usr/bin/env python3
"""
 NAME
  bAttrDict.py

 DESCRIPTION
  Beblsoft Attribute Dictionary Functionality

 BIBLIOGRAPHY
  https://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute
"""


# ----------------------------- ATTR DICT CLASS ----------------------------- #
class BeblsoftAttrDict(dict):
    """
    Dictionary whose keys can be accessed as attributes
    Example:
      bAD = BeblsoftAttrDict(foo=1, bar=2)
      foo = bAD.foo
      #  -or-
      foo = bAD["foo"]
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        super().__init__(*args, **kwargs)
        self.__dict__ = self

    @staticmethod
    def fromExistingDict(d):
        """
        Alternate constructor

        Create new attr dict from existing dictionary
        """
        aD = BeblsoftAttrDict()
        for k,v in d.items():
            setattr(aD, k, v)
        return aD

    def __getattr__(self, attr):
        """
        Return attribute
        """
        return self.__dict__.get(attr)
