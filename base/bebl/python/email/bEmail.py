#!/usr/bin/env python3.6
"""
 NAME
  bEmail.py

 DESCRIPTION
   Email Functionality.
"""


# ----------------------------- IMPORTS ------------------------------------- #
import socket
from datetime import datetime
import getpass
import smtplib
import logging
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.lastpass.python.bLastpass import BeblsoftLastpass


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ------------------------------ MAIL CLASS --------------------------------- #
class BeblsoftEmail():
    """
    Email class - class to login to email server, send email and logout
    """

    def __init__(self, account, password, server=smtplib.SMTP('smtp.gmail.com:587')):
        self.account  = account
        self.password = password
        self.server   = server
        self.loggedIn = False

    def __str__(self):
        s = "[{} Account:{} Server:{}]".format(
            self.__class__.__name__, self.account, self.server)
        return s

    @logFunc()
    def login(self):
        """
        Login to the email server
        """
        try:
            self.server.ehlo()
            self.server.starttls()
            self.server.login(self.account, self.password)

            # Set loggedIn to True as successful so other functions work
            self.loggedIn = True
        except Exception as e:
            raise BeblsoftError(code=BeblsoftErrorCode.MS_CANNOT_LOGIN,
                                originalError=e)

    @logFunc()
    def send(self, to, subject, body):
        """
        Send an email after logging in
        """
        if not self.loggedIn:
            raise BeblsoftError(code=BeblsoftErrorCode.MS_NOT_LOGGED_IN)
        else:
            # Create the message
            msg = MIMEMultipart('alternative')
            msg['Subject'] = subject
            msg['From']    = self.account
            msg['To']      = to
            bodyText       = MIMEText(body, "plain")
            msg.attach(bodyText)

            # Send the email!
            try:
                self.server.sendmail(self.account, to, msg.as_string())
            except Exception as e:
                raise BeblsoftError(code=BeblsoftErrorCode.MS_UNABLE_TO_SEND_EMAIL,
                                    originalError=e)

    @logFunc()
    def logout(self):
        """
        Close down the email server
        """
        if not self.loggedIn:
            raise BeblsoftError(BeblsoftErrorCode.MS_NOT_LOGGED_IN)
        else:
            try:
                self.server.quit()
            except Exception as e:
                raise BeblsoftError(code=BeblsoftErrorCode.MS_CANNOT_LOGOUT,
                                    originalError=e)


# ----------------------------- MAIN: TEST HARNESS -------------------------- #
if __name__ == "__main__":
    masterAccount  = "beblsoftmanager@gmail.com"
    masterPassword = getpass.getpass("Enter {} Password:".format(masterAccount))
    emailAccount   = "google.com"
    emailTo        = "bensson.james@gmail.com"
    emailSubject   = "Hello World"
    emailBody      = "Hello from {} at {}".format(socket.gethostname(), datetime.now())

    bLastpass      = BeblsoftLastpass(
        masterAccount  = masterAccount,
        masterPassword = masterPassword,
        accounts       = [emailAccount])
    bLastpass.login()
    bLastpass.loadAllPasswords()

    bEmail         = BeblsoftEmail(
        account        = masterAccount,
        password       = bLastpass.getPassword(emailAccount))
    bEmail.login()
    bEmail.send(to=emailTo, subject=emailSubject, body=emailBody)
    bEmail.logout()
