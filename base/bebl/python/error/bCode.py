#!/usr/bin/env python3
"""
 NAME
  bCode.py

 DESCRIPTION
  Beblsoft Error Code Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import enum


# ----------------------------- ERROR CODES --------------------------------- #
class BeblsoftErrorCode(enum.Enum):
    """
    Error Codes
    """
    SUCCESS                                = 0  # 0 -  999 : Basic Reserve
    DEFAULT_ERROR                          = 1
    UNKNOWN_ERROR                          = 2
    TEST_ERROR                             = 3
    HEADER_ERROR                           = 4

    GEN_JSON_REQUIRED                      = 1000  # 1000 - 1099 : Generic Reserve
    GEN_JSON_VALIDATION_ERROR              = 1001
    GEN_UNKNOWN_CONFIG                     = 1002
    GEN_UNKNOWN_LANGUAGE                   = 1003
    GEN_BAD_CURSOR                         = 1004
    GEN_OBJECT_DOES_NOT_EXIST              = 1005
    GEN_UNKNOWN_CURRENCY                   = 1006
    GEN_NOT_YET_IMPLEMENTED                = 1007
    GEN_NO_LANGUAGE_CODE                   = 1008
    GEN_REQUEST_METHOD_NOT_ALLOWED         = 1009

    LP_UNABLE_TO_GET_PASSWORD              = 1120  # 1120 - 1139 : Lastpass Reserve
    LP_CANNOT_LOGIN                        = 1121
    LP_CANNOT_LOGOUT                       = 1122
    LP_NOT_LOGGED_IN                       = 1123
    LP_UNKNOWN_ACCOUNT                     = 1124

    JWT_NO_TOKEN                           = 1140  # 1140 - 1159 : JWT Reserve
    JWT_TOKEN_SPACES                       = 1141
    JWT_INVALID_TOKEN                      = 1142
    JWT_INVALID_PERMISSIONS                = 1143
    JWT_NO_PAYLOAD                         = 1144
    JWT_NO_ASSOCIATED_USER                 = 1145

    MS_CANNOT_LOGIN                        = 1160  # 1160 - 1179 : Mail Server Reserve
    MS_CANNOT_LOGOUT                       = 1161
    MS_NOT_LOGGED_IN                       = 1162
    MS_UNABLE_TO_SEND_EMAIL                = 1163

    AUTH_NO_EMAIL                          = 1180  # 1180 - 1199 : Auth Reserve
    AUTH_NO_PASSWORD                       = 1181
    AUTH_INVALID_EMAIL                     = 1182
    AUTH_INVALID_PASSWORD                  = 1183
    AUTH_EMAIL_ALREADY_EXISTS              = 1184
    AUTH_NO_ACCOUNT                        = 1185
    AUTH_ADMIN_REQUIRED                    = 1186
    AUTH_INVALID_TOKEN_FOR_ID              = 1187
    AUTH_INVALID_ID                        = 1188
    AUTH_TOKEN_NOT_PRESENT                 = 1189
    AUTH_ACCOUNT_TOKEN_NOT_PRESENT         = 1190
    AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED  = 1191

    AWS_NO_ATTRIBUTE                       = 1200  # 1200 - 1299 : AWS Reserve
    AWS_NOT_IMPLEMENTED                    = 1201
    AWS_OBJECT_DOESNT_EXIST                = 1202
    AWS_WAIT_TIMED_OUT                     = 1203
    AWS_ACTION_NOT_PERMITTED               = 1204
    AWS_LAMBDA_INVOCATION_FAILED           = 1205
    AWS_LAMBDA_INVOCATION_UNHANDLED        = 1206
    AWS_TOO_MANY_RECORDS                   = 1207

    DB_COMMIT_FAILED                       = 1300  # 1300 - 1399 : DB Reserve
    DB_DOESNT_EXIST                        = 1301
    DB_ONLINE_RO                           = 1302
    DB_OFFLINE                             = 1303
    DB_OBJECT_DOES_NOT_EXIST               = 1304

    RECAPTCHA_TOKEN_INVALID                = 1400  # 1400 - 1499 : ReCAPTCHA Reserve

    PROFILE_GROUP_NAME_BAD_LEN             = 1500  # 1500 - 1599 : Profile Group Reserve
    PROFILE_GROUP_NAME_INVALID             = 1501
    PROFILE_GROUP_NAME_EXISTS              = 1502
    PROFILE_GROUP_EXCEED_MAX               = 1503
    PROFILE_GROUP_DOES_NOT_EXIST           = 1504

    PROFILE_ID_EXISTS                      = 1600  # 1600 - 1699 : Profile Reserve
    PROFILE_DOES_NOT_EXIST                 = 1601
    PROFILE_NO_MANAGER                     = 1602
    PROFILE_NO_CONTENT_MANAGER             = 1603
    PROFILE_NO_CONTENT_SYNC_MANAGER        = 1604
    PROFILE_NO_CONTENT_ANALYSIS_MANAGER    = 1605
    PROFILE_NO_CONTENT_COUNT_MANAGER       = 1606
    PROFILE_NO_CONTENT_STATISTIC_DAO       = 1607
    PROFILE_NO_CONTENT_HISTOGRAM_DAO       = 1608
    PROFILE_INVALID_CONTENTATTR            = 1609
    PROFILE_INVALID_GROUP_BY               = 1610
    PROFILE_DBENUM_NO_CONTENTATTRTYPE      = 1611
    PROFILE_CONTENTATTRTYPE_NO_DBENUM      = 1612
    PROFILE_STATISTIC_CONTENT_NOTSUPPORTED = 1613
    PROFILE_HISTOGRAM_NOTSUPPORTED         = 1614
    PROFILE_INVALID_SORTTYPE               = 1615

    FACEBOOK_PROFILE_ID_EXISTS             = 1700  # 1700 - 1799 : Facebook Reserve
    FACEBOOK_PROFILE_DOES_NOT_EXIST        = 1701
    FACEBOOK_GRAPH_REQUEST_FAILED          = 1702
    FACEBOOK_GRAPH_MAINTYPE_DNE            = 1703
    FACEBOOK_PROFILE_ALREADY_IN_PG         = 1704
    FACEBOOK_PROFILE_ID_EXISTS_DIFPG       = 1705
    FACEBOOK_PROFILE_CREATED_TOO_FAST      = 1706
    FACEBOOK_NO_ATTRIBUTE                  = 1707
    FACEBOOK_PERMISSION_REQUIRED           = 1708
    FACEBOOK_NO_ACCESS_TOKEN_FOR_USER      = 1709
    FACEBOOK_ACCESS_TOKEN_INVALID_USERID   = 1710
    FACEBOOK_ACCESS_TOKEN_HAS_EXPIRED      = 1711
    FACEBOOK_ACCESS_TOKEN_INVALID          = 1712

    STRIPE_CARD_ERROR                      = 1800  # 1800 - 1899 : Stripe Reserve
    STRIPE_RATE_LIMIT_ERROR                = 1801
    STRIPE_INVALID_REQUEST_ERROR           = 1802
    STRIPE_AUTHENTICATION_ERROR            = 1803
    STRIPE_API_CONNECTION_ERROR            = 1804
    STRIPE_SERVER_ERROR                    = 1805
    STRIPE_NOT_IMPLEMENTED                 = 1806
    STRIPE_NO_ATTRIBUTE                    = 1807
    STRIPE_OBJECT_DOESNT_EXIST             = 1808
    STRIPE_NO_UNIT_MANAGER_EXISTS          = 1809
    STRIPE_UNSUPPORTED_CURRENCY            = 1810
    STRIPE_AMOUNT_UNDER_MIN_PURCHASE_COUNT = 1811
    STRIPE_CUSTOMER_DOES_NOT_EXIST         = 1812
    STRIPE_CREDIT_CARD_COUNT_REACHED       = 1813
    # EMPTY                                = 1814
    STRIPE_INVALID_CARD_SOURCE_TOKEN       = 1815
    STRIPE_CARDID_NOT_STRING               = 1816
    STRIPE_CARDID_TOO_SHORT                = 1817
    STRIPE_MUST_SPECIFY_CART_UNITS         = 1818
    STRIPE_CUSTOMER_DOES_NOT_OWN_CARD      = 1819

    COMPREHEND_TEXT_TOO_SMALL              = 1900  # 1900 - 1999 : Comprehend Reserve
    COMPREHEND_TEXT_TOO_BIG                = 1901
    COMPREHEND_NO_LANGUAGE_DETECTED        = 1902

    SYNC_ALREADY_IN_PROGRESS               = 2000  # 2000 - 2099 : Sync Reserve
    SYNC_NOT_RESTARTABLE                   = 2001
    SYNC_DAILY_THRESHOLD_REACHED           = 2002
    SYNC_NOT_ALLOWED_WITH_ANALYSIS         = 2003

    ANALYSIS_ALREADY_IN_PROGRESS           = 2100  # 2100 - 2099 : Analysis Reserve
    ANALYSIS_NOT_ALLOWED_WITH_SYNC         = 2101
    ANALYSIS_NOTHING_TO_ANALYZE            = 2102

    LEDGER_NOT_ENOUGH_UNITS_FOR_HOLD       = 2200  # 2200 - 2299 : Ledger Reserve
    LEDGER_HOLD_MUST_BE_POSITIVE           = 2201
    LEDGER_DEDUCTION_MUST_BE_NEGATIVE      = 2202
    LEDGER_DEDUCTION_INVALID_UNITTYPE      = 2203
    LEDGER_DEDUCTION_TOO_MANY_UNITS        = 2204
    LEDGER_ADDITION_MUST_BE_POSITIVE       = 2205

    MYSQL_LOCK_NOT_HELD                    = 2300  # 2300 - 2399 : MySQL Reserve
    MYSQL_SERVER_NOT_ONLINE                = 2301

    TEXT_FROM_USER_LIMIT_EXCEEDED          = 2400  # 2400 - 2499 : Text Reserve
    TEXT_CANT_DELETE_NOT_FROM_USER         = 2401

    SENTRY_REQUEST_FAILED                  = 2500  # 2500 - 2599 : Sentry Reserve
    SENTRY_NO_ATTRIBUTE                    = 2501
    SENTRY_REQUEST_DOES_NOT_EXIST          = 2502

    TWITTER_API_REQUEST_FAILED             = 2600  # 2600 - 2699 : Twitter Reserve
    TWITTER_NO_ATTRIBUTE                   = 2601
    TWITTER_API_REQUEST_BAD_AUTHENTICATION = 2602
    TWITTER_API_PATH_NOT_SPECIFIED         = 2603
    TWITTER_API_OAUTH_BAD_TOKENS           = 2604
    TWITTER_API_OAUTH_UNCONFIRMED_CALLBACK = 2605
    TWITTER_API_REQUEST_NO_USER_ID         = 2606
