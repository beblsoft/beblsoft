#!/usr/bin/env python3
"""
NAME
 bError.py

DESCRIPTION
 Beblsoft Error Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import inspect
from base.bebl.python.error.bData import BeblsoftErrorData
from base.bebl.python.error.bCode import BeblsoftErrorCode
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.json.bObjectEncoder import BeblsoftJSONObjectEncoder


# ----------------------------- ERROR CLASS --------------------------------- #
class BeblsoftError(Exception, BeblsoftJSONObjectEncoder):
    """
    Beblsoft Error Class
    """

    def __init__(self, code=BeblsoftErrorCode.UNKNOWN_ERROR,
                 msg=None, cmd=None, status=None, output=None,
                 intMsg=None, intMsgFormatDict=None,
                 extMsg=None, extMsgFormatDict=None,
                 httpStatus=None, originalError=None):
        """
        Initialize object
        Args
          code:
            Beblsoft Error Code
          msg:
            message from code throwing exception
          cmd:
            command that was executed
            Ex. "echo Hello World"
          status:
            Output status of command
          output:
            Command output
          intMsg:
            Message to only be shown internally
            Ex. "Invalid password"
          intMsgFormatDict:
            Dictionary of format arguments to pass to intMsg
            Ex. {"foo": "bar"}
          extMsg:
            Message to be shown to external customer
            Ex. "Invalid credentials"
          extMsgFormatDict:
            Dictionary of format arguments to pass to extMsg
            Ex. {"foo": "bar"}
          httpStatus:
            Http Status to be shown
            Ex. 400
          originalError:
            Original error that generated this error

        Note:
          Can't name "extData" "data" as Flask restplus tries to parse error.data as a dictionary!
          extData = extended data
        """
        super().__init__()
        self.code              = code
        self.msg               = msg
        self.cmd               = cmd
        self.status            = status
        self.output            = output
        self._extData          = BeblsoftErrorData.getByCode(code)
        self._intMsg           = intMsg
        self._intMsgFormatDict = intMsgFormatDict
        self._extMsg           = extMsg
        self._extMsgFormatDict = extMsgFormatDict
        self.httpStatus        = httpStatus if httpStatus else self._extData.httpStatus
        self.originalError     = originalError

        # Caller information
        (_, filename, lineNumber, function, _, _) = inspect.stack()[1]
        self.caller            = BeblsoftAttrDict()
        self.caller.filename   = filename
        self.caller.lineNumber = lineNumber
        self.caller.function   = function

    @property
    def intMsg(self):
        """
        Internal message
        Allow BeblsoftError instantiator to overried extData
        """
        intMsg = self._intMsg if self._intMsg else self._extData.intMsg
        intMsg = intMsg if not self._intMsgFormatDict else intMsg.format(**self._intMsgFormatDict)
        return intMsg

    @property
    def extMsg(self):
        """
        External message
        Allow BeblsoftError instantiator to overried extData
        """
        extMsg = self._extMsg if self._extMsg else self._extData.extMsg
        extMsg = extMsg if not self._extMsgFormatDict else extMsg.format(**self._extMsgFormatDict)
        return extMsg

    @property
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        keyList = ["code", "msg", "httpStatus", "extMsg", "intMsg", "cmd", "status",
                   "output", "originalError", "caller"]
        return self.attributeKeysToDict(keyList=keyList)

    def __str__(self):
        """
        Return String Representation
        """
        s = ""
        if self.msg:
            s += "msg:'{}' ".format(self.msg)
        if self.cmd:
            s += "cmd:'{}' ".format(self.cmd)
        if self.status:
            s += "status:'{}' ".format(self.status)
        if self.output:
            s += "output:'{}' ".format(self.output)
        if self.extMsg:
            s += "extMsg:'{}' ".format(self.extMsg)
        if self.intMsg:
            s += "intMsg:'{}' ".format(self.intMsg)
        if self.httpStatus:
            s += "httpStatus:'{}' ".format(self.httpStatus)
        if self.originalError:
            s += "originalError:'{}' ".format(self.originalError)
        return s
