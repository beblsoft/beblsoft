#!/usr/bin/env python3
"""
 NAME
  bData.py

 DESCRIPTION
  Beblsoft Error Data Functionality

 BEBLSOFT HTTP STATUS CODE USAGE
  Code|Meaning               |Use Cases
  ----|----------------------|----------------------------
  200 |OK                    |Successful GET request
      |                      |Successful POST request
  ----|----------------------|----------------------------
  400 |Bad Request           |Parameters not specified
      |                      |Parameters are formatted poorly
      |                      |Resource already exists, can't create
  401 |Unauthorized          |No Authentication Credentials
  402 |Failed Request        |All parameters were valid but request failed
      |                      |Bad Authentication Credentials
  404 |Not Found             |Resource was not found
  429 |Too Many Requests     |Too Many Requests
  ----|----------------------|----------------------------
  500 |Internal Server Error |Code error on the server
  501 |Not Implemented       |Method not implemented on the server
  503 |Service Unavailable   |Database is down temporarily
  ----|----------------------|----------------------------


 HTTP STATUS CODES
  Code|Meaning
  ----|-------------------------------------------------
  1xx |Informational responses
  100 |Continue
  101 |Switching Protocols
  102 |Processing (WebDAV; RFC 2518)
  103 |Early Hints (RFC 8297)
  ----|-------------------------------------------------
  2xx |Success
  200 |OK
  201 |Created
  202 |Accepted
  203 |Non-Authoritative Information (since HTTP/1.1)
  204 |No Content
  205 |Reset Content
  206 |Partial Content (RFC 7233)
  207 |Multi-Status (WebDAV; RFC 4918)
  208 |Already Reported (WebDAV; RFC 5842)
  226 |IM Used (RFC 3229)
  ----|-------------------------------------------------
  3xx |Redirection
  300 |Multiple Choices
  301 |Moved Permanently
  302 |Found
  303 |See Other (since HTTP/1.1)
  304 |Not Modified (RFC 7232)
  305 |Use Proxy (since HTTP/1.1)
  306 |Switch Proxy
  307 |Temporary Redirect (since HTTP/1.1)
  308 |Permanent Redirect (RFC 7538)
  ----|-------------------------------------------------
  4xx |Client errors
  400 |Bad Request
  401 |Unauthorized (RFC 7235)
  402 |Payment Required
  403 |Forbidden
  404 |Not Found
  405 |Method Not Allowed
  406 |Not Acceptable
  407 |Proxy Authentication Required (RFC 7235)
  408 |Request Timeout
  409 |Conflict
  410 |Gone
  411 |Length Required
  412 |Precondition Failed (RFC 7232)
  413 |Payload Too Large (RFC 7231)
  414 |URI Too Long (RFC 7231)
  415 |Unsupported Media Type
  416 |Range Not Satisfiable (RFC 7233)
  417 |Expectation Failed
  418 |I'm a teapot (RFC 2324, RFC 7168)
  421 |Misdirected Request (RFC 7540)
  422 |Unprocessable Entity (WebDAV; RFC 4918)
  423 |Locked (WebDAV; RFC 4918)
  424 |Failed Dependency (WebDAV; RFC 4918)
  426 |Upgrade Required
  428 |Precondition Required (RFC 6585)
  429 |Too Many Requests (RFC 6585)
  431 |Request Header Fields Too Large (RFC 6585)
  451 |Unavailable For Legal Reasons (RFC 7725)
  ----|-------------------------------------------------
  5xx |Server errors
  500 |Internal Server Error
  501 |Not Implemented
  502 |Bad Gateway
  503 |Service Unavailable
  504 |Gateway Timeout
  505 |HTTP Version Not Supported
  506 |Variant Also Negotiates (RFC 2295)
  507 |Insufficient Storage (WebDAV; RFC 4918)
  508 |Loop Detected (WebDAV; RFC 5842)
  510 |Not Extended (RFC 2774)
  511 |Network Authentication Required (RFC 6585)
"""


# ----------------------------- IMPORTS ------------------------------------- #
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- ERROR DATA CLASS ---------------------------- #
class BeblsoftErrorData():
    """
    Object Encasulating the data corresponding to a specific error code
    """

    def __init__(self, code, extMsg, intMsg=None, httpStatus=400):
        """
        Initialize Error Data object
        Register object with errorDataMap
        Args
          code:
            BeblsoftErrorCode Enumeration code
          intMsg:
            internal error message
          httpStatus:
            http status return to request
          extMsg:
            external error message
        """
        self.code        = code
        self.intMsg      = intMsg
        self.extMsg      = extMsg
        self.httpStatus  = httpStatus

    def __str__(self):
        return "[{} code={} intMsg={} extMsg={} httpStatus={}]".format(
            self.__class__.__name__, self.code, self.intMsg, self.extMsg,
            self.httpStatus)

    @staticmethod
    def getByCode(code):
        """
        Retrieve error data corresponding to a specific BeblsoftErrorCode code
        Args
          code:
            BeblsoftErrorCode code
        Returns
          BeblsoftErrorData structure
        """

        data     = None
        dataDict = errorDataMap.get(code, None)
        if dataDict:
            data = BeblsoftErrorData(code=code, **dataDict)
        else:
            data = BeblsoftErrorData(code=code, extMsg="Unknown error")
        return data


# ----------------------------- GLOBALS ------------------------------------- #
errorDataMap = {
    # ------------- BASIC ----------------------------- #
    BeblsoftErrorCode.SUCCESS: {
        "extMsg": "Success",
        "httpStatus": 200
    },
    BeblsoftErrorCode.DEFAULT_ERROR: {
        "extMsg": "Default Error",
        "httpStatus": 400
    },
    BeblsoftErrorCode.UNKNOWN_ERROR: {
        "intMsg": "Unknown Error",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.TEST_ERROR: {
        "intMsg": "Test Error",
        "extMsg": "Test error",
        "httpStatus": 400
    },
    BeblsoftErrorCode.HEADER_ERROR: {
        "intMsg": "Header Error",
        "extMsg": "Header Error",
        "httpStatus": 200  # Potentiall overridden
    },

    # ------------- GENERIC --------------------------- #
    BeblsoftErrorCode.GEN_JSON_REQUIRED: {
        "extMsg": "JSON required",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.GEN_JSON_VALIDATION_ERROR: {
        "extMsg": "JSON Validation Error",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.GEN_UNKNOWN_CONFIG: {
        "extMsg": "Unknown Configuration",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.GEN_UNKNOWN_LANGUAGE: {
        "extMsg": "Unknown Language",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.GEN_BAD_CURSOR: {
        "extMsg": "Invalid Cursor Provided",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.GEN_OBJECT_DOES_NOT_EXIST: {
        "extMsg": "Object does not exist",
        "httpStatus": 404,
    },
    BeblsoftErrorCode.GEN_NOT_YET_IMPLEMENTED: {
        "intMsg": "Functionality has not yet been implemented",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.GEN_NO_LANGUAGE_CODE: {
        "extMsg": "No language code exists for language",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.GEN_REQUEST_METHOD_NOT_ALLOWED: {
        "extMsg": "Request method not allowed",
        "httpStatus": 400,
    },

    # ------------- LASTPASS -------------------------- #
    BeblsoftErrorCode.LP_UNABLE_TO_GET_PASSWORD: {
        "intMsg": "Unable to get account password",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.LP_CANNOT_LOGIN: {
        "intMsg": "Unable to login to LastPass",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.LP_CANNOT_LOGOUT: {
        "intMsg": "Unable to log out of LastPass",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.LP_NOT_LOGGED_IN: {
        "intMsg": "Must login prior to accessing accounts",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.LP_UNKNOWN_ACCOUNT: {
        "intMsg": "Account is unknown to LastPass object",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    # ------------- JWT ------------------------------- #
    BeblsoftErrorCode.JWT_NO_TOKEN: {
        "intMsg": "No token attached to request object",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.JWT_TOKEN_SPACES: {
        "intMsg": "JWT token contains spaces",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.JWT_INVALID_TOKEN: {
        "intMsg": "Invalid JWT token",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.JWT_INVALID_PERMISSIONS: {
        "intMsg": "Invalid JWT permissions",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.JWT_NO_PAYLOAD: {
        "intMsg": "JWT contains no payload",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    BeblsoftErrorCode.JWT_NO_ASSOCIATED_USER: {
        "intMsg": "JWT contains no associated user",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },

    # ------------- MAIL SERVICE ---------------------- #
    BeblsoftErrorCode.MS_CANNOT_LOGIN: {
        "intMsg": "Unable to login to Email Server",
        "extMsg": "Internal Server error",
        "httpStatus": 500,
    },

    BeblsoftErrorCode.MS_CANNOT_LOGOUT: {
        "intMsg": "Unable to logout of Email Server",
        "extMsg": "Internal Server error",
        "httpStatus": 500,
    },

    BeblsoftErrorCode.MS_NOT_LOGGED_IN: {
        "intMsg": "Not logged into Email Server",
        "extMsg": "Internal Server error",
        "httpStatus": 500,
    },

    BeblsoftErrorCode.MS_UNABLE_TO_SEND_EMAIL: {
        "intMsg": "Unable to send email",
        "extMsg": "Internal Server error",
        "httpStatus": 500,
    },

    # ------------- AUTHENTICATION -------------------- #
    BeblsoftErrorCode.AUTH_NO_EMAIL: {
        "intMsg": "JSON contains no email field",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_NO_PASSWORD: {
        "intMsg": "JSON contains no password field",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_INVALID_EMAIL: {
        "intMsg": "Invalid email",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_INVALID_PASSWORD: {
        "intMsg": "Invalid password",
        "extMsg": "Invalid credentials",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_EMAIL_ALREADY_EXISTS: {
        "intMsg": "Email already exists",
        "extMsg": "Account with email already exists.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.AUTH_NO_ACCOUNT: {
        "intMsg": "Account doesn't exist",
        "extMsg": "Account does not exist.",
        "httpStatus": 404,
    },
    BeblsoftErrorCode.AUTH_ADMIN_REQUIRED: {
        "intMsg": "Admin Required",
        "extMsg": "Action requires admin credentials.",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_INVALID_TOKEN_FOR_ID: {
        "intMsg": "Invalid Token For ID",
        "extMsg": "Invalid token.",
        "httpStatus": 401,
    },
    BeblsoftErrorCode.AUTH_INVALID_ID: {
        "intMsg": "Invalid ID",
        "extMsg": "Invalid id.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.AUTH_TOKEN_NOT_PRESENT: {
        "extMsg": "Authentication token must be specified.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.AUTH_ACCOUNT_TOKEN_NOT_PRESENT: {
        "extMsg": "Account token must be specified.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.AUTH_ONLY_TEST_ACCOUNT_ACCESS_ALLOWED: {
        "extMsg": "Only test account access is allowed.",
        "httpStatus": 400,
    },

    # ------------- AWS ------------------------------- #
    BeblsoftErrorCode.AWS_NO_ATTRIBUTE: {
        "intMsg": "AWS Object doesn't contain attribute",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.AWS_NOT_IMPLEMENTED: {
        "intMsg": "AWS Object hasn't implmented method attribute",
        "extMsg": "Internal server error",
        "httpStatus": 501
    },
    BeblsoftErrorCode.AWS_OBJECT_DOESNT_EXIST: {
        "intMsg": "AWS Object does not exist",
        "extMsg": "Internal server error",
        "httpStatus": 501
    },
    BeblsoftErrorCode.AWS_WAIT_TIMED_OUT: {
        "intMsg": "AWS Wait Timed Out",
        "extMsg": "Internal server error",
        "httpStatus": 503
    },
    BeblsoftErrorCode.AWS_ACTION_NOT_PERMITTED: {
        "intMsg": "AWS Action Not Permitted",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.AWS_LAMBDA_INVOCATION_FAILED: {
        "intMsg": "AWS Lambda Invocation Failed",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.AWS_LAMBDA_INVOCATION_UNHANDLED: {
        "intMsg": "AWS Lambda Invocation Unhandled",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.AWS_TOO_MANY_RECORDS: {
        "intMsg": "AWS Too Many Records Present in Payload",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },

    # ------------- DATABASE -------------------------- #
    BeblsoftErrorCode.DB_COMMIT_FAILED: {
        "intMsg": "Database commit failed",
        "extMsg": "Internal server error",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.DB_DOESNT_EXIST: {
        "intMsg": "Database doesn't exist",
        "extMsg": "Internal server error",
        "httpStatus": 503,
    },
    BeblsoftErrorCode.DB_ONLINE_RO: {
        "intMsg": "Database is online for reads only",
        "extMsg": "Internal server error",
        "httpStatus": 503,
    },
    BeblsoftErrorCode.DB_OFFLINE: {
        "intMsg": "Database is offline",
        "extMsg": "Internal server error",
        "httpStatus": 503,
    },
    BeblsoftErrorCode.DB_OBJECT_DOES_NOT_EXIST: {
        "extMsg": "Object does not exist",
        "httpStatus": 404,
    },

    # ------------- RECAPTCHA ------------------------- #
    BeblsoftErrorCode.RECAPTCHA_TOKEN_INVALID: {
        "extMsg": "Invalid ReCAPTCHA token",
        "httpStatus": 401
    },

    # ------------- PROFILE GROUP --------------------- #
    BeblsoftErrorCode.PROFILE_GROUP_NAME_BAD_LEN: {
        "extMsg": "Profile Group Name Is Not Properly Sized",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_GROUP_NAME_INVALID: {
        "extMsg": "Profile Group Name Contains Invalid Characters",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_GROUP_NAME_EXISTS: {
        "extMsg": "Profile Group Name Already Exists",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_GROUP_EXCEED_MAX: {
        "extMsg": "Too Many Profile Groups Exist",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_GROUP_DOES_NOT_EXIST: {
        "extMsg": "Profile Group Does Not Exist",
        "httpStatus": 404
    },

    # ------------- PROFILE --------------------------- #
    BeblsoftErrorCode.PROFILE_ID_EXISTS: {
        "extMsg": "Profile ID Already Exists",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_DOES_NOT_EXIST: {
        "extMsg": "Profile Does Not Exist",
        "httpStatus": 404
    },
    BeblsoftErrorCode.PROFILE_NO_MANAGER: {
        "extMsg": "Profile type {profileType} is unmanaged.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_MANAGER: {
        "extMsg": "Profile type {profileType}, content type {contentType} is unmanaged.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_SYNC_MANAGER: {
        "extMsg": "Profile type {profileType}, content type {contentType} is unsyncable.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_ANALYSIS_MANAGER: {
        "extMsg": "Profile type {profileType}, content type {contentType}, does not support analysis type {analysisType}.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_COUNT_MANAGER: {
        "extMsg": "Profile type {profileType}, content type {contentType}, is uncountable.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_STATISTIC_DAO: {
        "extMsg": "Profile type {profileType}, content type {contentType}, has no statistics.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_NO_CONTENT_HISTOGRAM_DAO: {
        "extMsg": "Profile type {profileType}, content type {contentType}, has no histograms.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_INVALID_CONTENTATTR: {
        "extMsg": "Profile has no content attribute {contentAttrType}.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_INVALID_GROUP_BY: {
        "extMsg": "Profile has no group by {groupByType}.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_DBENUM_NO_CONTENTATTRTYPE: {
        "extMsg": "Internal Server Error",
        "intMsg": "Database enumeration {dbEnum} has no content attribute type.",
        "httpStatus": 500
    },
    BeblsoftErrorCode.PROFILE_CONTENTATTRTYPE_NO_DBENUM: {
        "extMsg": "Internal Server Error",
        "intMsg": "ContentAttributeType {contentAttrType} has no database enumeration.",
        "httpStatus": 500
    },
    BeblsoftErrorCode.PROFILE_STATISTIC_CONTENT_NOTSUPPORTED: {
        "extMsg": "Statistic not supported for content type {contentType} attribute type {contentAttrType}.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_HISTOGRAM_NOTSUPPORTED: {
        "extMsg": "Histogram not supported for content type {contentType} attribute type {contentAttrType} group by {groupByType}.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.PROFILE_INVALID_SORTTYPE: {
        "extMsg": "Operation not supported for sort type {sortType}.",
        "httpStatus": 400
    },


    # ------------- FACEBOOK -------------------------- #
    BeblsoftErrorCode.FACEBOOK_PROFILE_ID_EXISTS: {
        "extMsg": "Facebook Profile ID Already Exists",
        "httpStatus": 400
    },
    BeblsoftErrorCode.FACEBOOK_PROFILE_DOES_NOT_EXIST: {
        "extMsg": "Facebook Profile Does Not Exist",
        "httpStatus": 404
    },
    BeblsoftErrorCode.FACEBOOK_GRAPH_REQUEST_FAILED: {
        "intMsg": "Facebook Graph Request Failed",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.FACEBOOK_GRAPH_MAINTYPE_DNE: {
        "intMsg": "Facebook Graph Request Main Type Does Not Exist",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.FACEBOOK_PROFILE_ALREADY_IN_PG: {
        "extMsg": "Only one Facebook Profile is allowed per Profile Group.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_PROFILE_ID_EXISTS_DIFPG: {
        "extMsg": "Facebook profile already exists in a different profile group.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_PROFILE_CREATED_TOO_FAST: {
        "extMsg": "The number of facebook profiles created today have exceeded the limit.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_NO_ATTRIBUTE: {
        "intMsg": "Facebook object does not contain the specified attribute.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.FACEBOOK_PERMISSION_REQUIRED: {
        "intMsg": "Facebook permission is required to perform operation.",
        "extMsg": "Facebook permission '{permStr}' is required to perform operation.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_NO_ACCESS_TOKEN_FOR_USER: {
        "extMsg": "No facebook access token found for user id {userID}",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID_USERID: {
        "intMsg": "Invalid access token for user ID",
        "extMsg": "Facebook credentials don't match the current facebook profile. Please try again.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_HAS_EXPIRED: {
        "extMsg": "Facebook credentials have expired. Please refresh them.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.FACEBOOK_ACCESS_TOKEN_INVALID: {
        "extMsg": "Facebook credentials are invalid. Please refresh them.",
        "httpStatus": 400,
    },

    # ------------- STRIPE ---------------------------- #
    BeblsoftErrorCode.STRIPE_CARD_ERROR: {
        "extMsg": "Stripe failed to process card",
        "httpStatus": 400
    },
    BeblsoftErrorCode.STRIPE_RATE_LIMIT_ERROR: {
        "extMsg": "Server overloading Stripe",
        "httpStatus": 500
    },
    BeblsoftErrorCode.STRIPE_INVALID_REQUEST_ERROR: {
        "intMsg": "Invalid parameters supplied to Stripe's API",
        "extMsg": "Invalid parameters supplied to Stripe's API",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.STRIPE_AUTHENTICATION_ERROR: {
        "intMsg": "Authentication tokes have changed",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_API_CONNECTION_ERROR: {
        "intMsg": "Network communication with stripe failed",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_SERVER_ERROR: {
        "intMsg": "Stripe server's failed",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_NOT_IMPLEMENTED: {
        "intMsg": "Stripe function not implemented",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_NO_ATTRIBUTE: {
        "intMsg": "Stripe object does not have attribute",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_OBJECT_DOESNT_EXIST: {
        "intMsg": "Stripe object does not exist",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_NO_UNIT_MANAGER_EXISTS: {
        "intMsg": "No unit manager exists.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_UNSUPPORTED_CURRENCY: {
        "extMsg": "Currency is not supported.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.STRIPE_AMOUNT_UNDER_MIN_PURCHASE_COUNT: {
        "extMsg": "Must specify above the minimum purchase count.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.STRIPE_CUSTOMER_DOES_NOT_EXIST: {
        "intMsg": "Stripe customer does not exist.",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.STRIPE_CREDIT_CARD_COUNT_REACHED: {
        "extMsg": "Added the maximum number of cards. Please remove an existing card first.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.STRIPE_NO_ATTRIBUTE: {
        "intMsg": "Stripe Object doesn't contain attribute",
        "extMsg": "Internal server error",
        "httpStatus": 500
    },
    BeblsoftErrorCode.STRIPE_INVALID_CARD_SOURCE_TOKEN: {
        "intMsg": "Invalid stripe card source token",
        "extMsg": "Please specify a valid stripe card source token",
        "httpStatus": 400
    },
    BeblsoftErrorCode.STRIPE_CARDID_NOT_STRING: {
        "extMsg": "Invalid stripe card id. Please specify a valid string.",
        "httpStatus": 400
    },
    BeblsoftErrorCode.STRIPE_CARDID_TOO_SHORT: {
        "extMsg": "Invalid stripe card id. Please specify a nonempty string",
        "httpStatus": 400
    },
    BeblsoftErrorCode.STRIPE_MUST_SPECIFY_CART_UNITS: {
        "extMsg": "Must specify units to add to cart",
        "httpStatus": 400
    },
    BeblsoftErrorCode.STRIPE_CUSTOMER_DOES_NOT_OWN_CARD: {
        "intMsg": "Invalid stripe customer does not own card",
        "extMsg": "Invalid card id.",
        "httpStatus": 400
    },

    # ------------- COMPREHEND ------------------------ #
    BeblsoftErrorCode.COMPREHEND_TEXT_TOO_SMALL: {
        "intMsg": "Text too small to be analyzed by comprehend",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.COMPREHEND_TEXT_TOO_BIG: {
        "intMsg": "Text too big to be analyzed by comprehend",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.COMPREHEND_NO_LANGUAGE_DETECTED: {
        "extMsg": "No language detected by comprehend analysis.",
        "httpStatus": 400,
    },


    # ------------- SYNC ------------------------------ #
    BeblsoftErrorCode.SYNC_ALREADY_IN_PROGRESS: {
        "extMsg": "A sync is already in progress",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.SYNC_NOT_RESTARTABLE: {
        "extMsg": "Due to third party api restrictions, a sync cannot be started. Please try again later.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.SYNC_DAILY_THRESHOLD_REACHED: {
        "extMsg": "Due to third party api restrictions, another sync cannot be performed today. Please try again later",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.SYNC_NOT_ALLOWED_WITH_ANALYSIS: {
        "extMsg": "Cannot start sync while an analysis is occurring",
        "httpStatus": 400,
    },

    # ------------- ANALYSIS -------------------------- #
    BeblsoftErrorCode.ANALYSIS_ALREADY_IN_PROGRESS: {
        "extMsg": "An analysis is already in progress",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.ANALYSIS_NOT_ALLOWED_WITH_SYNC: {
        "extMsg": "Cannot start analysis while sync is occurring",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.ANALYSIS_NOTHING_TO_ANALYZE: {
        "extMsg": "All content has already been analyzed.",
        "httpStatus": 400,
    },

    # ------------- LEDGER ---------------------------- #
    BeblsoftErrorCode.LEDGER_NOT_ENOUGH_UNITS_FOR_HOLD: {
        "intMsg": "Hold requires more available units",
        "extMsg": "Not enough units are available to perform action",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.LEDGER_HOLD_MUST_BE_POSITIVE: {
        "intMsg": "Hold must contain a positive ledger count",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.LEDGER_DEDUCTION_MUST_BE_NEGATIVE: {
        "intMsg": "Deduction must contain a negative ledger count",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.LEDGER_DEDUCTION_INVALID_UNITTYPE: {
        "intMsg": "Deduction unit type must match hold unit type",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.LEDGER_DEDUCTION_TOO_MANY_UNITS: {
        "intMsg": "Deduction exceeds the hold amount",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.LEDGER_ADDITION_MUST_BE_POSITIVE: {
        "intMsg": "Addition must contain a negative ledger count",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },

    # ------------- MYSQL ----------------------------- #
    BeblsoftErrorCode.MYSQL_LOCK_NOT_HELD: {
        "intMsg": "MySQL Lock '{lock}' not held",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.MYSQL_SERVER_NOT_ONLINE: {
        "intMsg": "MySQL server {domainName} not online",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },

    # ------------- TEXT ------------------------------ #
    BeblsoftErrorCode.TEXT_FROM_USER_LIMIT_EXCEEDED: {
        "extMsg": "Text limit has been exceeded.",
        "httpStatus": 400,
    },
    BeblsoftErrorCode.TEXT_CANT_DELETE_NOT_FROM_USER: {
        "extMsg": "Cannot delete text.",
        "httpStatus": 400,
    },

    # ------------- SENTRY ---------------------------- #
    BeblsoftErrorCode.SENTRY_REQUEST_FAILED: {
        "intMsg": "Sentry Request Failed.",
        "extMsg": "Internal server error.",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.SENTRY_NO_ATTRIBUTE: {
        "intMsg": "Sentry object does not contain the specified attribute.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.SENTRY_REQUEST_DOES_NOT_EXIST: {
        "intMsg": "Sentry object does not exist.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },

    # ------------- TWITTER --------------------------- #
    BeblsoftErrorCode.TWITTER_API_REQUEST_FAILED: {
        "intMsg": "Twitter API Request Failed",
        "extMsg": "Internal server error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_NO_ATTRIBUTE: {
        "intMsg": "Twitter object does not contain the specified attribute.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_API_REQUEST_BAD_AUTHENTICATION: {
        "intMsg": "Twitter API request has bad authentication credentials.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_API_PATH_NOT_SPECIFIED: {
        "intMsg": "Twitter API path was not specified for request.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_API_OAUTH_BAD_TOKENS: {
        "intMsg": "Twitter API OAuth request returned bad tokens.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_API_OAUTH_UNCONFIRMED_CALLBACK: {
        "intMsg": "Twitter API OAuth request did not confirm callback specified.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
    BeblsoftErrorCode.TWITTER_API_REQUEST_NO_USER_ID: {
        "intMsg": "Twitter API request did not return user ID.",
        "extMsg": "Internal Server Error",
        "httpStatus": 500,
    },
}
