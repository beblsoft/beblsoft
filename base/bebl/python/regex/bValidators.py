#!/usr/bin/env python3
"""
NAME:
 bValidators.py

DESCRIPTION
 Beblsoft Regex Validator Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import re
import logging


# ----------------------- GLOBALS ------------------------------------------- #
logger = logging.getLogger(__file__)


# ----------------------- REGEX VALIDATORS ---------------------------------- #
def validateAlphaNumericDash(string):
    """
    Validate String Only Contains "a-z", "A-Z", "0-9", "-_"

    Returns
      True if string valid, False otherwise
    """
    regex     = r"[^a-zA-Z0-9\-\_]"
    compRegex = re.compile(regex)
    valid     = not bool(compRegex.search(string))
    return valid
