#!/usr/bin/env python3
"""
NAME:
 bThread.py

DESCRIPTION
 Beblsoft Thread Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import threading
from base.bebl.python.log.bLogFunc import logFunc


# ----------------------- JOIN ALL THREADS ---------------------------------- #
@logFunc()
def joinAllThreads():
    """
    Join all threads except for the current thread
    """
    currentThread = threading.current_thread()
    threadList    = threading.enumerate()

    for thread in threadList:
        if thread != currentThread:
            thread.join()


# ----------------------- JOIN THREADS BY NAME ------------------------------ #
@logFunc()
def joinThreadsByName(name=None, namePrefix=None):
    """
    Join threads that match name condition
    Args
      name:
        Thread name must match exactly
        Ex. "Thread1"
      namePrefix:
        Thread name must start with prefix
        Ex. "jqThread-"
    """
    currentThread = threading.current_thread()
    threadList    = threading.enumerate()

    for thread in threadList:
        nameMatch        = name and thread.name is name
        namePrefixMatch  = namePrefix and thread.name.startswith(namePrefix)
        notCurrentThread = thread != currentThread
        joinThread       = notCurrentThread and (nameMatch or namePrefixMatch)
        if joinThread:
            thread.join()
