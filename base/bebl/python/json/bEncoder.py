#!/usr/bin/env python3
"""
NAME:
 bEncoder.py

DESCRIPTION
 Beblsoft JSON Encoder Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import enum
import json


# ----------------------- BEBLSOFT JSON ENCODER CLASS ----------------------- #
class BeblsoftJSONEncoder(json.JSONEncoder):
    """
    Beblsoft JSON Encoder
    """

    # ------------------- DEFAULT ------------------------------------------- #
    def default(self, obj): #pylint: disable=E0202,W0221
        """
        Override default encoder

        Encoder attempts
         - Default json.JSONEncoder.default
         - Invoking object "jsonDict" property [See bObjectEncoder.py]
         - Handling special python types: e.g. Enums
         - Encoding object as a string
        """
        rval    = None

        # Default
        if rval is None:
            try:
                rval = json.JSONEncoder.default(self, obj) #pylint: disable=E1111
            except TypeError as _:
                pass

        # toJSON Method
        if rval is None:
            try:
                rval = obj.jsonDict
            except AttributeError as _:
                rval = None

        # Enum
        if rval is None:
            try:
                if issubclass(obj.__class__, enum.Enum):
                    rval          = str(obj.name)
            except AttributeError as _:
                rval = None

        # Use object string
        if rval is None:
            rval = "{}".format(obj)

        return rval
