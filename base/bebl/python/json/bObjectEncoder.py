#!/usr/bin/env python3
"""
NAME:
 bObjectEncoder.py

DESCRIPTION
 Beblsoft JSON Object Encoder Functionality
"""

# ----------------------- IMPORTS ------------------------------------------- #
import abc


# ----------------------- BEBLSOFT JSON OBJECT ENCODER CLASS ---------------- #
class BeblsoftJSONObjectEncoder(abc.ABC):
    """
    Beblsoft JSON Object Encoder
    """

    # ------------------- ABSTRACT ------------------------------------------ #
    @property
    @abc.abstractmethod
    def jsonDict(self):
        """
        Return json-encodable dictionary representation
        """
        pass

    # ------------------- HELPER FUNCTIONS ---------------------------------- #
    def attributeKeysToDict(self, keyList): #pylint: disable=E0202,W0221
        """
        Return Dictionary consisting of the attribute keys
        Args
          keyList:
            List of attribute keys to add to json-encodable dictionary
            Ex. ["id", "name", "length"]
        """
        d = {}
        for k in keyList:
            v = getattr(self, k, None)
            if v is not None:
                d[k] = v
        return d
