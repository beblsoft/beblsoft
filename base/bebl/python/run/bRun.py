#!/usr/bin/env python
"""
 NAME
  bRun.py

 DESCRIPTION
  Run Functionality
"""


# ------------------------------- IMPORTS ----------------------------------- #
import subprocess
import logging
import sys
import traceback
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------------ GLOBALS ------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------------- RUN --------------------------------------- #
@logFunc()
def run(cmd, env=None, cwd=None, shell=False, raiseOnStatus=False,
        bufferedLogFunc=None, logResults=True):
    """
    Run bash command in subprocess.
    Args
      cmd:
        string cmd to execute -OR list of string commands
        ex1. "echo 'HelloWorld'"
        ex2. ["npm run build"]
      env:
        environment dictionary
        ex. {"FOO":"BAR"}
      cwd:
        current working directory of child process
        ex. "/tmp/foo"
      shell:
        Run program inside a shell context
        I.e. "/bin/sh -c <cmd>"
      raiseOnStatus:
        If True, raise exception on non-zero status
      bufferedLogFunc:
        Function to buffer stdout
        ex. logger.info
    Returns:
      tuple: (status, output)
    """
    cmdList       = cmd
    if not isinstance(cmdList, list):
        cmd       = cmd.strip()
        cmdList   = cmd.split(" ")
    status        = None
    output        = ""
    originalError = None

    try:
        ps = subprocess.Popen(cmdList, env=env, cwd=cwd, shell=shell,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)
        if bufferedLogFunc:
            for line in iter(ps.stdout.readline, b''):
                line = line.decode()
                output += line
                bufferedLogFunc(line.rstrip())
            ps.wait()
        else:
            ps.wait()
            output = ps.stdout.read().decode()
        status = ps.returncode
        ps.stdout.close()
    except Exception as e: #pylint: disable=W0703
        exc_type, exc_value, exc_traceback = sys.exc_info()
        status = -1
        output = traceback.format_exception(exc_type, exc_value, exc_traceback)
        originalError = e
    finally:
        if raiseOnStatus and status != 0:
            raise BeblsoftError(cmd=cmd, status=status, output=output, originalError=originalError)
        if logResults:
            logger.debug("\ncmd='{}'\ncwd={}\nstatus={}\noutput={}".format(cmd, cwd, status, output))
    return (status, output)
