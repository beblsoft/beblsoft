#!/usr/bin/env python
"""
 NAME
  bParallel.py

 DESCRIPTION
  Run Functionality
"""


# ------------------------------- IMPORTS ----------------------------------- #
import logging
import queue
from threading import Thread
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------------ GLOBALS ------------------------------------ #
logger = logging.getLogger(__file__)


# ------------------------------- RUN FUNCTION ON LIST ---------------------- #
@logFunc()
def runFuncOnList(func, lst, parallel=False):
    """
    Run function on each lement in list
    Args
      func:
        Func to run on each element of list
        Ex lambda (num): num * 2s
      lst:
        List to run function on
        Ex. [1, 2, 3, 4]
      parallel:
        if True, create new thread to run on each object so execution happens
        in parallel
    Note:
      If any exception happens in child thread, it will be reraised in parent
    """
    threadList = []
    excQueue   = queue.Queue()

    def runFuncOnElem(func, elem, mainThread=False):  # pylint: disable=W0613
        """
        Nested function to do work
        Args
          mainThread:
            True if main thread of execution
        """
        try:
            func(elem)
        except Exception as e:  # pylint: disable=W0703
            if mainThread:
                raise(e)
            else:
                excQueue.put(e)

    if lst:
        # Main thread: execute all jobs, optionally in parallel
        for idx in range(0, len(lst)):
            elem = lst[idx]
            if parallel:
                thread = Thread(target=runFuncOnElem,
                                name="{}-{}".format(func.__name__[0:12], idx),
                                args=(func, elem, False))
                threadList.append(thread)
                thread.start()
            else:
                runFuncOnElem(func, elem, mainThread=True)

        # Wait for all threads to complete
        for thread in threadList:
            thread.join()

        # See if any child threads raised an exception. If so, reraise the
        # exception in the parent thread
        try:
            e = excQueue.get(block=False)
            raise(e)
        except queue.Empty as e:
            pass


# ------------------------------- RUN PYTHON STR ON LIST -------------------- #
@logFunc()
def runPythonStrOnObjList(objList=None, pythonStr=None, parallel=False):
    """
    Run python string on list of objects
    Args
      objList:
        list of python objects
      pythonStr:
        python command string to execute on objects
      parallel:
        if True, create new thread to run on each object so execution happens
        in parallel
    Example
      runPythonStrOnObjList(bBucketList, "upload()", parallel=True)
    Note:
      If any exception happens in child thread, it will be reraised in parent
    """
    threadList = []
    excQueue   = queue.Queue()

    def runPythonStrOnObj(obj, pythonStr, mainThread=False):  # pylint: disable=W0613
        """
        Nested function to do work of executing python string on object.
        Args
          mainThread:
            True if main thread of execution
        """
        evalStr = "obj.{}".format(pythonStr)
        try:
            eval(evalStr)
        except Exception as e:  # pylint: disable=W0703
            if mainThread:
                raise(e)
            else:
                excQueue.put(e)

    if objList:
        # Main thread: execute all jobs, optionally in parallel
        for idx in range(0, len(objList)):
            obj = objList[idx]
            if parallel:
                thread = Thread(target=runPythonStrOnObj,
                                name="{}-{}".format(pythonStr[0:12], idx),
                                args=(obj, pythonStr, False))
                threadList.append(thread)
                thread.start()
            else:
                runPythonStrOnObj(obj, pythonStr, mainThread=True)

        # Wait for all threads to complete
        for thread in threadList:
            thread.join()

        # See if any child threads raised an exception. If so, reraise the
        # exception in the parent thread
        try:
            e = excQueue.get(block=False)
            raise(e)
        except queue.Empty as e:
            pass
