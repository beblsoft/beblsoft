# ---------------------------- APPLICATIONS --------------------------------- #
This directory contains the following applications:
 - flaskHelloWorld.py/  - The simplest of all flask programs, used by
                          helloWorldFromCherryPy.py
 - flaskInCherryPy.py/  - Program demonstrating server starting at
                          localhost:8080 that serves up falskHelloWorld.py
 - helloFromCherryPy.py/- Program that demonstrates Cherry Py features from
                          their tutorial


# ----------------------------- INSTALLATION -------------------------------- #
Steps to install for cherrypy, ssl setup:

$ pip3 install --upgrade pip    optional upgrade of pip3
$ pip3 install cherrypy         install python3 cherrypy
$ pip3 install flask            install python3 flask (if not installed)
$ pip3 install paste            install python3 paste (cherrypy logging)


# ------------------------------ GENERATE KEYS ------------------------------ #
Note:  Browsers will consider this certificate insecure as it was not issued by a
CA!!!

$ openssl genrsa -out privkey.pem 2048        Generate a private key
$ openssl req -new -x509 -days 365 -key
privkey.pem -out cert.pem                     Generate a certificate with minimally
                                              common name set to localhost
Final Note:  Put keys is certificates directory (one level down) where
             flaskInCherryPy.py expects them.


# ----------------------------------- RUN ----------------------------------- #
$ ./helloFromCherryPy.py        starts up demo program, listens on localhost:8080
$ ./flaskInCherryPy.py          starts up separate flask app over https using
                                certificates in certificates folder, listens on
                                localhost:8080.  Flask app is in
                                flaskHelloWorld.py.