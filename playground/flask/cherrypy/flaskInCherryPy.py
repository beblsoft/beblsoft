#!/usr/bin/env python3
"""
NAME
 flaskInCherryPy.py

DESCRIPTION
 Simple CherryPy program that creates a SSL-based WSGI application server for
 a flask program

NOTE
 Any saved change to this file will restart the server.
"""

# ----------------------------- IMPORTS ------------------------------------- #
from flaskHelloWorld import app

import cherrypy
from paste.translogger import TransLogger


# ----------------------------- FUNCTIONS ----------------------------------- #
def run_server():
    """
    Run cherry py server
    """
    # Enable WSGI access logging via Paste
    app_logged = TransLogger(app)

    # Mount the WSGI callable object (app) on the root directory
    cherrypy.tree.graft(app_logged, '/')

    # Set the configuration of the web server with ssl using pregenerated
    # certificates
    cherrypy.config.update({
        'engine.autoreload_on': True,
        'log.screen': True,
        'server.socket_port': 8080,
        'server.socket_host': '0.0.0.0',
        'server.ssl_module': 'builtin',
        'server.ssl_certificate': './certificates/cert.pem',
        'server.ssl_private_key': './certificates/privkey.pem'

    })

    # Start the CherryPy WSGI web server
    cherrypy.engine.start()
    cherrypy.engine.block()


# ----------------------------- TEST DRIVER --------------------------------- #
if __name__ == "__main__":
    run_server()
