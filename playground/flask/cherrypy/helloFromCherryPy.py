#!/usr/bin/python3
"""
NAME
 helloFromCherryPy.py

DESCRIPTION
 Most basic web application using CherryPy demonstrating simple routing.
 Starts CherryPy WSGI server on localhost:8080 (default configuration)
"""


# ----------------------------- IMPORTS ------------------------------------- #
import random
import string
import cherrypy


# ----------------------------- CLASSES ------------------------------------- #
class HelloWorld(object):

    @cherrypy.expose
    def index(self):
        """
        From web browser: localhost:8080, returns Hellow World, Jamester!
        """
        return "Hello World, Jamester!"

    @cherrypy.expose
    def generate(self):
        """
        From web browser: localhost:8080/generate, e.g., returns:
        Jamester's random number: db0AFD3a
        """
        return "Jamester's random number: " + "".join(random.sample(string.hexdigits, 8))

    @cherrypy.expose
    def generate2(self, length=8):
        """
        From web browser: localhost:8080/generate2?length=16, e.g., returns
        Jamester's random number: Cb068A94a1DfBFc5
        Note: Only works upto a length of 22
        """
        someString = "Jamester's random number: " + \
            "".join(random.sample(string.hexdigits, int(length)))
        return someString

    @cherrypy.expose
    def htmlform(self):
        """
        From web browser: localhost:8080/htmlform,
        produces form to get length and then calls back to generate2
        (see e.g. above)
        """
        return """<html>
		 <head></head>
		  <body>
		   <form method="get" action="generate2">
		    <input type="text" value="8" name="length" />
		    <button type="submit">Give it now!</button>
		   </form>
		  </body>
		 </html>"""

# ----------------------------- TEST DRIVER --------------------------------- #
if __name__ == "__main__":
    cherrypy.quickstart(HelloWorld())
