#!/usr/bin/python3
"""
NAME
 flaskHelloWorld.py

DESCRIPTION
 Most basic flask program used by flaskInCherryPy to demonstrate running
 flask inside a SSL-based WSGI application server
"""

# --------------------------------- IMPORTS --------------------------------- #
from flask import Flask


# --------------------------------- GLOBALS --------------------------------- #
app = Flask(__name__)
app.debug = True


# ---------------------------------- ROUTES --------------------------------- #
@app.route("/")
def hello():
    return "Hello World from Flask running within CherryPi!"


# ----------------------------- TEST DRIVER --------------------------------- #
if __name__ == "__main__":
    app.run()
