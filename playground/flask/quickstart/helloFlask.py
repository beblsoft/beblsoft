#!/usr/bin/env python3
"""
NAME
 helloFlask.py

DESCRIPTION
 Program to serve as a quick intruction to Flask
"""

# ------------------------------ IMPORTS ------------------------------------ #
from flask import Flask, request, render_template, url_for, make_response, abort, redirect


# ------------------------------ GLOBALS ------------------------------------ #
app = Flask(__name__)


# ------------------------------ ROUTES ------------------------------------- #
@app.route('/')
def index():
    '''View function: Index page'''
    return 'Index Page'


@app.route('/HelloWorld')
def hello_world():
    '''View function: hello world!'''
    return 'Hello, World!'


@app.route('/hello')
@app.route('/hello/<name>')
def hello(name=None):
    '''View function: write name out to HTML template'''
    return render_template('hello.html', name=name)


@app.route('/user/<username>')
def show_user_profile(username):
    '''View function: parse a variable from URL'''
    return 'User %s' % username


@app.route('/post/<int:post_id>')
def show_post(post_id):
    '''View function: only accept integers from URL'''
    return 'Post %d' % post_id


@app.route('/verbs', methods=['GET', 'POST'])
def verbs():
    '''View function: parse http verbs from request'''
    if request.method == 'GET':
        return 'Recieved a GET!'
    if request.method == 'POST':
        return 'Recieved a POST!'


@app.route('/urls')
def urls():
    '''View function: use url_for function'''
    urls = []
    urls.append(url_for('index'))
    urls.append(url_for('hello_world'))
    return 'Application URLS: %s' % str(urls)


@app.route('/cookies')
def cookies():
    '''View function: set and retrieve cookies
     Must issue two get requests on the page to see cookies'''
    random_cookie = request.cookies.get('random_cookie')
    response = make_response(render_template('cookie.html', cookie=random_cookie))
    response.set_cookie('random_cookie', 'I am a random cookie')
    return response


@app.route('/redirector')
def redirector():
    '''View function: redirect client to another view function'''
    return redirect(url_for('index'))


@app.route('/error')
def error():
    '''View function: error out request'''
    abort(404)


@app.route('/logging')
def log():
    '''View function: display flask logging facilities'''
    app.logger.debug('Debug message')
    app.logger.warning('Warning message')
    app.logger.error('An error occurred')
    return "Logged messsages"



# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(debug=True)
