OVERVIEW
===============================================================================
Flask Projects Documentation


PROJECT: SINGLE FILE APPS
===============================================================================
- Description:
  * Various single file applications
- Directory                         : ./singleFileApps
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- helloFlask.py ------------------- : Flask Hello World
  * Start Flask app                 : ./helloFlask.py
  * See hello world                 : localhost:5000

- offload.py ---------------------- : Example offloading request to a worker thread
  * Start Flask app                 : ./helloFlask.py
  * Offload thread                  : GET localhost:5000/offload
                                      Offloaded thread will log


PROJECT: QUICK START
===============================================================================
- Description:
  * Flask Quickstart
  * Demonstrate route parsing for strings, redirection, error handling,
    and other basic features
- Directory                         : ./quickstart
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- To Run
  * Start Flask app                 : ./helloFlask.py
  * Index Page                      : localhost:5000
  * Hello Page                      : localhost:5000/hello/james
  * Post Page                       : localhost:5000/post/25
  * Redirect to index               : localhost:5000/redirector
  * Error                           : localhost:5000/error
  * Logging                         : localhost:5000/logging


PROJECT: FLASKR
===============================================================================
- Description:
  * Sample blog site backed by a sqlite database.
  * Contains unit tests to test the application.
- Directory                         : ./flaskr
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- To Run
  * Start Flask app                 : ./flaskr.py
  * Index Page                      : localhost:5000
  * Login                           : localhost:5000/login
                                      User/Pass = admin/default
                                      After logged in, add posts
  * Execute unit tests              : ./flaskr_tests.py


PROJECT: FLASKY
===============================================================================
- Description:
  * Fully fledged flask application complete with Web server, automated CLI,
    database migration scripts, and unit testing
- Directory                         : ./flasky
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements/dev.txt
- To Run                            : see ./flasky/README.txt


PROJECT: CHERRYPY
===============================================================================
- Description:
  * Cherry Py hello world app that wraps a Flask app with a Cherry Py server
  * Serves over HTTPS
- Directory                         : ./cherrypy
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements/dev.txt
- To Run
  * Start flask app                 : ./flaskInCherryPy.py
  * View serving over https         : https://localhost:8080i


PROJECT: SWAGGER
===============================================================================
- Description:
  * Demonstrates flask swagger which creates a swagger spec from a flask
    API
- Directory                         : ./swagger
- URLS
  * Flask-Swagger                   : https://github.com/gangverk/flask-swagger
  * Swagger-UI                      : https://github.com/swagger-api/swagger-ui
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements/dev.txt
- To Run
  * Start flask app                 : ./app.py
  * Point SwaggerUI at Flask App    : localhost:5000/spec
  * Generate Swagger Spec           : flaskswagger app:app
  * Get SwaggerUI Docker Container  : sudo docker pull swaggerapi/swagger-ui
  * Run SwaggerUI Container         : sudo docker run -p 80:8080 swaggerapi/swagger-ui