OVERVIEW
===============================================================================
Flask Technology Documentation
- Description
  * Flask is a micro web framework written in Python and based on the Werkzeug
    toolkit and Jinja2 template engine. It is BSD licensed.
  * Flask supports extensions that can add application features as if they were
    implemented in Flask itself.
- Relevant URLS
  * Home                            : http://flask.pocoo.org/
  * Github                          : https://github.com/pallets/flask


INSTALLATION
===============================================================================
- Install pip                       : get-pip.py
- Install python virtual env        : sudo apt-get install python-virtualenv
- Create python virtual environment
  diretory                          : virtualenv venv
- Activate virtual environment      : source venv/bin/activate
- Deactivate virtual environment    : deactivate
- Install module (virtual env must
  be activated)                     : pip install Flask; pip install <module name>
- Save modules descriptions to
  requirements.txt                  : pip freeze > requirements.txt
- Install modules in
  requirements.txt                  : pip install -r requirements/dev.txt


CONEXT GLOBALS
===============================================================================
- Description
  * Flask pushes the application and request contexts before
    dispatching a request.
  * Doing so in the python shell    : >> from hello import app
                                      >> from flask import current_app
                                      >> app_ctx = app.app_context()
                                      >> app_ctx.push()
                                      >> current_app.name   #Can now access current_app
                                      >> app_ctx.pop()
- Context Variables
  * current_app                     : Application context
                                      Application instance of active app
  * g                               : Application context
                                      Temporary storage
  * request                         : Request context
                                      HTTP Request object sent from client
  * session                         : Request context
                                      Dict of objects remembered between requests


VIEWS
===============================================================================
- Type conversion parameters
  * string                          : any string without a slash
  * int                             : accepts integers
  * float                           : like int but for floating point values
  * path                            : like the default but also accepts slashes
  * any                             : matches one of the items provided
  * uuid                            : accepts UUID strings


MODULES, EXTENSIONS
===============================================================================
- Description
  * Flask has multiple extensions that can be found http://bit.ly/fl-exreg
- script                          : CLI
- jinja                           : Templating
- bootstrap                       : HTML/CSS Formatter
  * blocks                        : doc, html_attribs, html, head, title, metas, styles, body_attribs,
                                    body, navbar, content, scripts
- moment                          : Localization of dates and times
- wtf                             : Web Form Integration
- sqlalchemy                      : Database ORM
- migrate                         : Database Migration
- mail                            : Email Support
- Werkzeug                        : HTTP utilities - cookies, tags, dates, file uploads...
- itsdangerous                    : Encryption, decryption
- forgerypy                       : Generating Demo Data
- markupsafe                      : HTML escaping
- pagedown                        : Markdown-to-HTML converter for wtf forms
- markdown                        : Markdown-to-HTML converter
- bleach                          : HTML checker
- httpie                          : HTTP testing
- coverage                        : Code coverage reports
- selenium                        : Browser testing
- gunicorn                        : Production web server
- uwsgi                           : Production web server
- babel                           : Internationalization and localization support
- restful                         : RESTful APIs
- celery                          : Queue background jobs
- frozen-flask                    : Conversion from flask app to static website
- debugtoolbar                    : In-browswer debugging tools
- assets                          : Merging, minifying, and compiling of CSS and JS assets
- oauth                           : Authentication against OAuth providers
- openid                          : Authentication against OpenID providers
- whhoshalchemy                   : Full-text search for sqlalchemny models based on Whhosh
- kvsession                       : Alternate implementation of user session that use server-side storage
- restplus                        : Support for quickly building REST APIs.
