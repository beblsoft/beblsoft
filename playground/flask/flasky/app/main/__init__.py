#!/usr/bin/python
#
# NAME
#  __init__.py
#
# DESCRIPTION
#   main blueprint package initialization file.


# ----------------------------- IMPORTS(1) ---------------------------------- #
from flask import Blueprint
from ..models import Permission


# ----------------------------- GLOBALS ------------------------------------- #
main = Blueprint('main', __name__)


# ----------------------------- IMPORTS(2) ---------------------------------- #
# views.py and errors.py both import main and add routes to it. If the below
# import statment was placed at the top of the file, the main object wouldn't
# exist when views and errors import it. 
#
from . import views, errors


# ----------------------------- FUNCTIONS ----------------------------------- #
@main.app_context_processor
def inject_permissions():
    return dict(Permission=Permission)


