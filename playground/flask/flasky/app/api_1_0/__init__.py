#!/usr/bin/python
#
# NAME
#  __init__.py
#
# DESCRIPTION
#  api_1_0 package initialization file.


from flask import Blueprint

api = Blueprint('api', __name__)

from . import authentication, posts, users, comments, errors
