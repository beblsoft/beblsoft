#!/usr/bin/python
#
# NAME
#  exceptions.py
#
# DESCRIPTION
#  Exception helper functions


# ----------------------------- FUNCTIONS ----------------------------------- #
class ValidationError(ValueError):
    pass
