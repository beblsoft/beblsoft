#!/usr/bin/python
#
# NAME
#  __init__.py
#
# DESCRIPTION
#   auth package initialization file.

from flask import Blueprint

auth = Blueprint('auth', __name__)

from . import views
