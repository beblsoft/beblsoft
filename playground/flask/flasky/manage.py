#!/usr/bin/python
#
# NAME
#  manage.py
#
# DESCRIPTION
#   Manager wrapper over flasky application. The manager facilitates command
#   line and python shell easy usage. See README.md for usage.
#
# ------------------------------- RUN APPLICATION --------------------------- #
# Steps to run basic flask hello.py application:
#  $ export FLASK_APP=hello.py
#  $ flask run                            :run internally visible server
#  $ python -m flask run --host=0.0.0.0   :run externally visible server
#
# ------------------------- ENVIRONMENT VARIABLES --------------------------- #
# Tell flask to run hello.py            :export FLASK_APP=hello.py
# Debugging mode                        :export FLASK_DEBUG=1
#
# ----------------------------- SERVER -------------------------------------- #
# $ python3 ./manage.py -h
# $ python3 ./manage.py runserver            :run server instance
#
# --------------------------- DATABASE INIT --------------------------------- #
# $ python3 ./manage.py db -h
# $ python3 ./manage.py db init
# $ python3 ./manage.py db upgrade
#
# ---------------------------- SHELL SESSION -------------------------------- #
# $ python3 ./manage.py shell -h
# $ python3 ./manage.py shell
# ----------- USER ----------------
# >> User.generate_fake()
# >> [print (u.name) for u in User.query.all()]
#
# >> j = User.query().filter_by(username="jbensson")
# >> print(j)                                                  #Prints the SQL query
# >> j = User.query().filter_by(username="jbensson").first()   #Executes the SQL query
# >> print(j.to_json())
#
# >> j.confirmed = True                                        #Set confimed property
# >> db.session.add(j)
# >> db.session.commit(j)
# >>
#
# ----------- APPLICATION ---------
# >> app.url_map

# ----------------------------- IMPORTS ------------------------------------- #
import os
COV = None
if os.environ.get('FLASK_COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db
from app.models import User, Follow, Role, Permission, Post, Comment
from flask.ext.script import Manager, Shell
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.migrate import upgrade
from app.models import Role, User
from werkzeug.contrib.profiler import ProfilerMiddleware
import unittest
import sys


# ----------------------------- GLOBALS ------------------------------------- #
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


# ----------------------------- MANAGER COMMANDS ---------------------------- #
def make_shell_context():
    """Create a shell dictionary context in which to play around"""
    return dict(app=app, db=db, User=User, Follow=Follow, Role=Role,
                Permission=Permission, Post=Post, Comment=Comment)
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)

@manager.command
def test(coverage=False):
    """Run the unit tests, optionally with code coverage"""
    if coverage and not os.environ.get('FLASK_COVERAGE'):
        os.environ['FLASK_COVERAGE'] = '1'
        os.execvp(sys.executable, [sys.executable] + sys.argv)

    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        COV.erase()


@manager.command
def profile(length=25, profile_dir=None):
    """Start the application under the code profiler."""
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[length],
                                      profile_dir=profile_dir)
    app.run()


@manager.command
def deploy():
    """Run deployment tasks."""
    # migrate database to latest revision
    upgrade()

    # create user roles
    Role.insert_roles()

    # create self-follows for all users
    User.add_self_follows()


# -------------------------------- MAIN ------------------------------------- #
if __name__ == '__main__':
    manager.run()
