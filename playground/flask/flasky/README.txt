OVERVIEW
===============================================================================
Flasky Application
Git URL: https://github.com/miguelgrinberg/flasky


CHECKOUTS
===============================================================================
- 1a     : Setup virtual environment
           virtualenv venv
           source venv/bin/activate
           pip install flask
           deactivate

- 2a     : Hello World route, no data passed down from client
           python hello.py
           Navigate to: http://localhost:5000

- 2b     : Hello World with data encoded in URL
           python hello.py
           Navigate to: http://localhost:5000/user/dblack

- 2c     : Introducing Flask script
           pip install flask-script
           python hello.py runserver
           Navigate to: http://localhost:5000/user/dblack

- 3a     : Same as 2c using templates

- 3b     : Integrating w/ flask-bootstrap templates
           pip install flask-bootstrap
           python hello.py runserver --port 5000
           Navigate to: http://localhost:5000/user/dblack

- 3c     : 404 and 500 page error handling
           python hello.py runserver --port 5000
           Navigate to: http://localhost:5000/junk
           Optionally add divide by zero to index() to simulate server error

- 3d     : Introduces static files
           Note - could'nt see static file

- 3e     : Introduces flask-moment
           pip install flask-moment
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000
           See the time displayed

- 4a     : Introduces flask_wtf forms
           pip install flask_wtf
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000
           Returns name upon entry or, without a name, client side validation

- 4b     : Introduces session storage
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000
           Name is remembered in session

- 4c     : Introduces message flashing
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000
           When name changes, message is flashed

- 5a     : Introduces Flask SQLAlchemy integration, basic example
           pip install flask-sqlalchemy
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000

- 5b     : Searches database for existing user
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000

- 5c     : Adds commandline argument to create python shell context
           python hello.py runserver --port 5000
           Navigate to http://localhost:5000

- 5d     : Demonstrates use of database migrate
           pip install flask-migrate
           python hello.py db init
           python hello.py db upgrade

- 6a     : Demonstrates integration with flask email; note need to add
           db.create_all() call in __main__ to get to work
           pip install flask-email
           python hello.py runserver
           Navigate to http://localhost:5000

- 6b     : Show use of threading of email sending task
           python hello.py runserver
           Navigate to http://localhost:5000

- 7a     : Refactor into large application structure
           pip install -r requirements.txt
           python manage.py runserver
           Navigate to http://localhost:5000
           python manage.py test





