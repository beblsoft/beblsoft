#!/usr/bin/env python3
"""
NAME:
 helloFlask.py

DESCRIPTION
 Flask Hello World program
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask import Flask


# ------------------------ GLOBALS ------------------------------------------ #
app = Flask(__name__)


# ------------------------ ROUTES ------------------------------------------- #
@app.route("/")
def hello_world():
    """
    Hello World Route
    """
    return "Hello World!"


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(debug=True)
