#!/usr/bin/env python3
"""
NAME:
 offload.py

DESCRIPTION
 Offload a thread during a request
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import logging
import threading
from datetime import datetime
from flask import Flask


# ------------------------ GLOBALS ------------------------------------------ #
app = Flask(__name__)


# ------------------------ ROUTES ------------------------------------------- #
@app.route("/offload")
def offload():
    """
    Offlad Route
    """
    date = datetime.utcnow()
    t    = threading.Thread(name="BackgroundThread",
                            target=backgroundFunction, kwargs={"date": date})
    t.start()
    # t.join()
    return "Time {}".format(date)


# ------------------------ BACKGROUND FUNCTION ------------------------------ #
def backgroundFunction(date):
    """
    Thread Function
    """
    logging.debug("Start Offload Date={}".format(date))
    time.sleep(2)
    logging.debug("Done Offload")


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG,
                        format="[%(levelname)8s] (%(threadName)-15s) %(message)s")
    app.run(debug=True)
