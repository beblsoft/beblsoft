#!/usr/bin/python
#
# NAME
#  flaskr_tests.py
#
# DESCRIPTION
#  Tests for Flaskr application
#
# DOCUMENTATION
#  Python unittests: https://docs.python.org/3.4/library/unittest.html
#
# USAGE
#  python flaskr_test.py       :Run all unittests
#  python flaskr_test.py -v    :Same as above, with verbosity


# ----------------------------- IMPORTS ------------------------------------- #
import os
import flaskr
import unittest
import tempfile


# ----------------------------- TESTS --------------------------------------- #
class FlaskrTestCase(unittest.TestCase):
	"""Test cases for flaskr web application"""

	# ------------- INIT/TEARDOWN ------------- #
	def setUp(self):
		"""Setup each test case"""
		self.db_fd, flaskr.app.config['DATABASE'] = tempfile.mkstemp()

		#Set TESTING config flag, disables error catching
		flaskr.app.config['TESTING'] = True
		self.app = flaskr.app.test_client()
		with flaskr.app.app_context():
			flaskr.init_db()

	def tearDown(self):
		"""Teardown each test case"""
		os.close(self.db_fd)
		os.unlink(flaskr.app.config['DATABASE'])

	# ------------- HELPERS ------------------- #
	def login(self, username, password):
		return self.app.post('/login', data=dict(
			username=username, password=password), follow_redirects=True)

	def logout(self):
		return self.app.get('/logout', follow_redirects=True)

	# ------------- TESTS --------------------- #
	#All functions prefixed with "test_" are treated as test cases.
	def test_empty_db(self):
		"""Test an empty database"""
		rv = self.app.get('/')
		assert(b'No entries here so far' in rv.data)

	def test_login_logout(self):
		"""Test login and logout functionality"""
		#Correct login credentials
		rv = self.login('admin', 'default')
		assert(b'You were logged in' in rv.data)
		rv = self.logout()
		assert(b'You were logged out' in rv.data)

		#Incorrect login credentials
		rv = self.login('adminx', 'default')
		assert(b'Invalid username' in rv.data)
		rv = self.login('admin', 'defaultx')
		assert(b'Invalid password' in rv.data)

	def test_messages(self):
		"""Test adding messages"""
		self.login('admin', 'default')
		rv = self.app.post('/add', data=dict(
			title='<Hello>',
			text='<strong>HTML</strong> allowed here'), follow_redirects=True)
		assert(b'No entries here so far' not in rv.data)
		assert(b'&lt;Hello&gt;' in rv.data)
		assert(b'<strong>HTML</strong> allowed here' in rv.data)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == '__main__':
	unittest.main()