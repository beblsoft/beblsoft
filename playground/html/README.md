# HTML Documentation

Hyper Text Markup Language (HTML) is the standard markup language for documents designed to be
displayed in the web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS)
and scripting languages such as JavaScript.

Relevant URLs:

- [Wikipedia](https://en.wikipedia.org/wiki/HTML)
- [Bootstrap](http://www.hongkiat.com/blog/bootstrap-alternatives/)
