# Android Technology Documentation

Android is a mobile operating system developed by Google. It is based on a modified version of the
Linux kernel and other open source software, and is designed primarily for touchscreen mobile devices
such as smartphones and tablets.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Android_(operating_system)),
[Home](https://www.android.com/),
[Developer Home](https://developer.android.com/),
[Installation](https://developer.android.com/studio/install)
