/*
 * NAME:
 *  PasswordFragment - Fragment to demonstrate prompting for a user/name within an application.
 *
 * COPYRIGHT:
 *   Copyright © 2016 Beblsoft, LLC. All Rights Reserved.
 *
 * DESCRIPTION:
 *   This demonstrates a way to implement a user authentication screen.
 */
package com.beblsoft.library;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class PasswordFragment extends Fragment {


    public PasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_password, container, false);

        Button submitButton = (Button) view.findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                EditText userNameDisplay = (EditText) view.findViewById(R.id.userNameText);
                EditText passwordDisplay = (EditText) view.findViewById(R.id.passwordText);

                CharSequence text = "user name: " + userNameDisplay.getText() + "\npassword: " + passwordDisplay.getText();

                Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override


    public void onDetach() {
        super.onDetach();
    }

}
