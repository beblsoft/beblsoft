/*
 * NAME:
 *  CustomMapFragment - Fragment to demonstrate integration of a map within our base application
 *
 * COPYRIGHT:
 *   Copyright © 2016 Beblsoft, LLC. All Rights Reserved.
 *
 * DESCRIPTION:
 *   This demonstrates a way to incorporate a map within a fragment.
 */
package com.beblsoft.library;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

public class CustomMapFragment extends Fragment {
    public CustomMapFragment() {
    }

    private MapView mapView;
    private GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_custommap, container, false);

        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        if(mapView != null)
        {
            googleMap = mapView.getMap();
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
        }

        // Set to Nashua's LatLng
        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(42.7575, -71.4644));
        CameraUpdate zoom= CameraUpdateFactory.zoomTo(15);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);

        return view;
    }

    @Override
    public void onResume()
    {
        mapView.onResume();
        super.onResume();
    }
}
