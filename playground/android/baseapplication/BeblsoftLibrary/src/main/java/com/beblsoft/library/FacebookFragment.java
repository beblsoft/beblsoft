/*
 * NAME:
 *   FacebookFragment - Fragment to demonstrate the facebook functionality.
 *
 * COPYRIGHT:
 *   Copyright © 2016 Beblsoft, LLC. All Rights Reserved.
 *
 * INSTALLATION:
 *   - Useful Link:
 *     https://developers.facebook.com/docs/android/getting-started
  *    https://developers.facebook.com/quickstarts/1559549101002451/?platform=android.
 *   - Go through steps on Facebook developer website to add a new Android application.
 *     At the end of process they generate an application ID number.
 *   - Project must be API 15: Android 4.0.3 or higher.
 *   - Add to build.gradle (Project:BaseApplication) - repositories {mavenCentral()}
 *   - Add to build.gradle (Module:BeblsoftLibrary)  - compile 'com.facebook...'
 *   - Generate and submit hash key so Facebook can recognize development environment:
 *       keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore |
 *       openssl sha1 -binary | openssl base64
 *   - Add android.permission.INTERNET to BaseApplication Manifest.
 *   - Add online-generated Facebook app ID to BaseApplication Manifest.
 *   - Add FacebookActivity to BaseApplication Manifest
 *   - If sending images or videos: Add FacebookContentProvider to BaseApplication Manifest
 *
 * FACEBOOK CLASSES:
 *   CallbackManager    - manages callbacks into Facebook SDL from Activity or Fragment
 *                        onActivityResult method. Keeps all facebook data structures consistent.
 *   AccessToken        - Ticket to facebook data, an entity must login for app to acquire
 *                        an access token.
 *   AccessTokenTracker - Tracks changes to the AccessToken.
 *   GraphRequest       - Request into Facebook Graph API to retrieve data.
 *   GraphResponse      - Response back from Facebook Graph API containing data.
 *   LoginButton        - Allows logging into Facbook.
 *   ProfilePictureView - Displays a user's profile picture.
 *
 * DESCRIPTION:
 *   The code in this fragment does the following:
 *     - Allows a user to hit the Facebook login button and login to Facebook.
 *     - Upon a successful login, the app is granted a Facebook Access Token. With this
 *       token it leverages the Facebook Graph API to query and display the user's data.
 *     - When the user logs out, the Access Token Tracker updates the UI to display void data.
 *
 * DESCRIPTION:
 *   //TODO: write description
 */

package com.beblsoft.library;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import org.json.JSONObject;

public class FacebookFragment extends Fragment {
    /*--------------------------- CONSTANTS ---------------------------------*/
    private static final String TAG = "FBFragment";  //Log Tag

    /*---------------------- INSTANCE VARIABLES -----------------------------*/
    private CallbackManager mFbCallbackManager;      //FB callback manager
    private LoginButton mLoginButton;                //FB login button
    private TextView mTextView;                      //Text view to display logged
    private ProfilePictureView mProfPicView;         //Profile Picture View
    private AccessTokenTracker mAccessTokenTracker;  //Track Access Token Changes

    private String mUserID    = "";                  //User ID
    private String mUserFName = "";                  //User first name
    private String mUserLName = "";                  //User last name
    private String mUserPURL  = "";                  //User photo URL

    /*---------------------- CALLBACKS/LISTENERS ----------------------------*/
    /**
     * Callback after the login button is pressed. Runs in the context of the
     * UI thread.
     */
    private FacebookCallback<LoginResult> mLoginButtonCallback =
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Toast.makeText(getContext(), "Login Successful!", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getContext(), "Login Cancelled.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(getContext(), "Login Error.", Toast.LENGTH_LONG).show();
                }
            };


    /*---------------------- FRAGMENT LIFECYCLE -----------------------------*/
    public FacebookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Initializes all pertinent instance variables.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_facebook, container, false);

        mFbCallbackManager = CallbackManager.Factory.create();

        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                    AccessToken currentAccessToken)
            {
                updateUserInfo();
            }
        };

        mLoginButton = (LoginButton) view.findViewById(R.id.fb_login_button);
        mLoginButton.setReadPermissions("user_friends");
        mLoginButton.setFragment(this);
        mLoginButton.registerCallback(mFbCallbackManager, mLoginButtonCallback);

        mTextView = (TextView) view.findViewById(R.id.fb_login_text);

        mProfPicView = (ProfilePictureView) view.findViewById(R.id.fb_image);

        updateUserInfo();

        return view;
    }

    /**
     * Pass the ActivityResult to the FB Callback manager so all FB Objects will be updated.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAccessTokenTracker.startTracking();
    }

    /*------------------------------ HELPER FUNCTIONS -----------------------*/
    /**
     * Based on the existence of a valid access token, query facebook graph
     * and pull out user information. When information available, display it
     * on the screen.
     */
    private void updateUserInfo()
    {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken == null) {
            mUserID    = "";
            mUserFName = "";
            mUserLName = "";
            mUserPURL  = "";
            updateUI();
        }
        else {
            Bundle params = new Bundle();
            params.putString("fields", "id,first_name,last_name");
            new GraphRequest(accessToken,
                    "/" + accessToken.getUserId(),
                    params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Log.d(TAG, "Graph request completed");
                            Log.d(TAG, response.toString());
                            JSONObject fields = response.getJSONObject();
                            if (fields != null) {
                                try {
                                    Log.d(TAG, fields.toString());
                                    mUserID = fields.getString("id");
                                    mUserFName = fields.getString("first_name");
                                    mUserLName = fields.getString("last_name");
                                } catch (org.json.JSONException unused) {
                                    Log.d(TAG, "Not all fields returned.");
                                }
                                updateUI();
                            }
                        }
                    }
            ).executeAsync();

        }
    }

    /**
     * Update the UI with user data.
     */
    private void updateUI() {
        String text = "";
        text += "User ID: "    + mUserID    + "\n";
        text += "First Name: " + mUserFName + "\n";
        text += "Last Name: "  + mUserLName;
        mTextView.setText(text);

        mProfPicView.setProfileId(mUserID);

    }
}
