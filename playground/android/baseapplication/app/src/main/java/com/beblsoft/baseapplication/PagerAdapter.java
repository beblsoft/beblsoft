/*
 * NAME:
 *  PagerAdapter - Provides tab-based navigation for any application.
 *
 * COPYRIGHT:
 *   Copyright © 2016 Beblsoft, LLC. All Rights Reserved.
 *
 * DESCRIPTION:
 *   This demonstrates use of tab-based navigation within an application.
 */
package com.beblsoft.baseapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.beblsoft.library.CameraFragment;
import com.beblsoft.library.FacebookFragment;
import com.beblsoft.library.CustomMapFragment;
import com.beblsoft.library.PasswordFragment;


public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    // Tabs are 0 indexed, so 0 refers to the first tab, 1 to the second, etc.
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new PasswordFragment();
            case 1:
                return new CameraFragment();
            case 2:
                return new FacebookFragment();
            case 3:
                return new CustomMapFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
