/*
 * NAME:
 *   MainActivity - template main activity for building a new application.
 *
 * COPYRIGHT:
 *   Copyright © 2016 Beblsoft, LLC. All Rights Reserved.
 *
 * DESCRIPTION:
 *   This is the initial entry point for a new application.
 */
package com.beblsoft.baseapplication;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.beblsoft.baseapplication.R;
import com.facebook.FacebookSdk;

public class MainActivity extends AppCompatActivity {

    /*---------------------- INSTANCE VARIABLES -----------------------------*/
    private ViewPager mViewPager;         //View Pager
    private PagerAdapter mPageAdapter;    //Page Adapter
    private TabLayout mTabLayout;         //Tab Layout container
    private Toolbar mToolbar;             //Toolbar

    /*---------------------- CALLBACKS/LISTENERS ----------------------------*/
    /**
     * Action to taken when a specific tab is selected
     */
    private TabLayout.OnTabSelectedListener mTabSelectedListener =
            new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            };

    /*---------------------- ACTIVITY LIFECYCLE -----------------------------*/

    /**
     * <p>Function does the following:
     * <ul>
     *     <li>Initializes FB SDK before it is used in any fragments.</li>
     *     <li>Initalizes Tab layout, Page Adapter.</li>
     * </ul>
     * </p>
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        FacebookSdk.sdkInitialize(getApplication());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Password"));
        tabLayout.addTab(tabLayout.newTab().setText("Camera"));
        tabLayout.addTab(tabLayout.newTab().setText("Facebook"));
        tabLayout.addTab(tabLayout.newTab().setText("Map"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPageAdapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        mViewPager.setAdapter(mPageAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(mTabSelectedListener);
    }
}