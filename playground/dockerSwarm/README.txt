OVERVIEW
===============================================================================
Docker Swarm Technology Documentation
- Description                       : Docker swarm is a native clustering system for Docker.
                                      It turns a pool of Docker hosts into a single, virtual host
                                      using an API proxy system
- Features --------------------------
  * Cluster management              : Use Docker Engine CLI to create a swarm of Docker Engines
                                      where you can deploy application services
  * Decentralized design
  * Declarative service model       : Define desired state of various services in application stack
  * Scaling                         : Declare the number of tasks for each service.
  * Desired state reconciliation    : Swarm manager automatically reconciles differences between actual
                                      state and desired state
  * Multi-host networking           : Specify overlay network for services
                                      Swarm manager automatically assigns addresses to containers
  * Service discovery               : Swarm manager nodes assign each service in swarm a unique DNS name
                                      and load balances running containers
  * Load balancing                  : Expose ports for services to an external load balancer
  * Secure by default               : Each node in swarm enforces TLS mutual authentication and encryption
  * Rolling updates                 : Apply service updates to nodes incrementally
- Relevant URLS ---------------------
  * Home                            : https://www.docker.com/
  * Documentation                   : https://docs.docker.com/engine/swarm/
  * Github                          : https://github.com/docker/swarm



CONCEPTS
===============================================================================
- What is swarm? -------------------: Swarm is the cluster management and orchestration features embedded in
                                      the Docker Engine and is built using swarmkit
                                    : A swarm consits of multiple Docker hosts which run in swarm mode
                                      and act as managers and workers
                                      A given Docker host can be a manager, a worker, or perform both roles
                                    : When you create a service you define its optimal state: number
                                      of replicas, network, and storage resources, ...
                                    : A task is a running container which is part of a swarm service and
                                      managed by a swarm manager
                                    : In the same way Docker Compose can define and run containers,
                                      define and run swarm service stacks
- Nodes ----------------------------: A node is an instance of the Docker engine participating in the
                                      swarm
  * Manager Node                    : To deploy application to a swarm, must submit a service definition
                                      to a manager node
                                    : Manager node dispatches units of work called tasks to worker nodes
                                    : Manager nodes also perform the orchestration and cluster management
                                      functions required to maintain the desired state of the swarm
                                      Manager nodes elect a single leader to conduct orchestration tasks
  * Worker Nodes                    : Receive and execute tasks dispatched from manager nodes
                                    : An agent runs on each worker node and reports on the tasks assigned
                                      to it
- Services and tasks ----------------
  * Service                         : A service is the definition of the tasks to execut on the manager
                                      or worker nodes. It is the central structure of the swarm
                                      system and the primary root of user interaction with the swarm
                                    : When you create a service, you specify which container image to
                                      use and which commands to execute inside running containers
  * Replicated Services             : Swarm manager distributes a specific number or replica tasks among
                                      the nodes based upon the scale of desired state
  * Global Services                 : Swarm runs one task for the service on every available node in cluster
  * Task                            : A docker container and the commands to run inside the container
                                      It is the atomic scheduling unit of swarm
- Load Balancing --------------------
  * Ingress                         : Swarm manager uses ingress load balancing to expose services
                                      you want to make available externally to the swarm



SECRETS
===============================================================================
- Description                       : A secret us a blob of data, such as a password, SSH private key,
                                      SSL certificate, or another piece of data that should not
                                      be transmitted over a network or stored unencrypted in a Dockerfile
                                    : Secrets are encrypted during transite and at rest in a Docker swarm
                                    : Given secret is only accessible to services which have been granted
                                      explicit access to it, and only while running
                                    : Note: only available to swarm services, not standalone containers
- How Docker manages secrets -------: When a secret is added to the swarm, Docker sends the secret to
                                      the swarm manager over a TLS connection
                                    : Secret stored in Raft log and is replicated across the other managers
                                    : When a newly-created container is given access to a secret, the
                                      decrypted secret is mounted into the counter in-memory filesystem
                                      /run/secrets/<secret_name>
                                    : When a container task stops running, the decrypted secrest shared are
                                      unmounted from the in-memory filesystem and flushed from node's memory



COMMANDS
===============================================================================
- Swarm -----------------------------
  * ca                              : Display and rotate the root CA
                                      docker swarm ca [OPTIONS]
  * init                            : Initialize a swarm
                                      docker swarm init [OPTIONS]
  * join                            : Join a swarm as a node and/or manager
                                      docker swarm join [OPTIONS] HOST:PORT
  * join-token                      : Manage join tokens
                                      docker swarm join-token [OPTIONS] (worker|manager)
  * leave                           : Leave the swarm
                                      docker swarm leave [OPTIONS]
  * unlock                          : Unlock swarm
                                      docker swarm unlock
  * unlock-key                      : Manage the unlock key
                                      docker swarm unlock-key [OPTIONS]
  * update                          : Update the swarm
                                      docker swarm update [OPTIONS]

- Service ---------------------------
  * create                          : Create a new service
                                      docker service create [OPTIONS] IMAGE [COMMAND] [ARG...]
  * inspect                         : Display detailed information on one or more services
                                      docker service inspect [OPTIONS] SERVICE [SERVICE...]
  * logs                            : Fetch the logs of a service or task
                                      docker service logs [OPTIONS] SERVICE|TASK
  * ls                              : List services
                                      docker service ls [OPTIONS]
  * ps                              : List the tasks of one or more services
                                      docker service ps [OPTIONS] SERVICE [SERVICE...]
  * rm                              : Remove one or more services
                                      docker service rm SERVICE [SERVICE...]
  * rollback                        : Revert changes to a service's configuration
                                      docker service rollback [OPTIONS] SERVICE
  * scale                           : Scale one or multiple replicated services
                                      docker service scale SERVICE=REPLICAS [SERVICE=REPLICAS...]
  * update                          : Update a service
                                      docker service update [OPTIONS] SERVICE

- Node ------------------------------
  * demote                          : Demote one or more nodes from manager in the swarm
                                      docker node demote NODE [NODE...]
  * inspect                         : Display detailed information on one or more nodes
                                      docker node inspect [OPTIONS] self|NODE [NODE...]
  * ls                              : List nodes in the swarm
                                      docker node ls [OPTIONS]
  * promote                         : Promote one or more nodes to manager in the swarm
                                      docker node promote NODE [NODE...]
  * ps                              : List tasks running on one or more nodes, defaults to current node
                                      docker node ps [OPTIONS] [NODE...]
  * rm                              : Remove one or more nodes from the swarm
                                      docker node rm [OPTIONS] NODE [NODE...]
  * update                          : Update a node
                                      docker node update [OPTIONS] NODE

- Stack -----------------------------
  * deploy                          : Deploy a new stack or update an existing stack
                                      docker stack deploy [OPTIONS] STACK
  * ls                              : List stacks
                                      docker stack ls [OPTIONS]
  * ps                              : List the tasks in the stack
                                      docker stack ps [OPTIONS] STACK
  * rm                              : Remove one or more stacks
                                      docker stack rm [OPTIONS] STACK [STACK...]
  * services                        : List the services in the stack
                                      docker stack services [OPTIONS] STACK

- Secrets ---------------------------
  * create                          : Create a secret from a file or STDIN as content
                                      docker secret create [OPTIONS] SECRET [file|-]
    from STDIN                      : printf "This is a secret" | docker secret create my_secret_data -
  * inspect                         : Display detailed information on one or more secrets
                                      docker secret inspect [OPTIONS] SECRET [SECRET...]
  * ls                              : List secrets
                                      docker secret ls [OPTIONS]
  * rm                              : Remove one or more secrets
                                      docker secret rm SECRET [SECRET...]

