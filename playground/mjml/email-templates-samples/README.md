# email-templates-samples

A collection of responsive email templates coded in MJML and available on
[mjml.io/templates](https://mjml.io/templates).

## Usage

```bash
# Enter Directory
cd email-templates-samples;

# Install with NPM
npm install

# Clean out old cruff
npm run clean

# Validate Files
npm run validate

# Watch mjml changes and build HTML files
npm run watch

# Generate new html and thumbnails
npm run generate
```
