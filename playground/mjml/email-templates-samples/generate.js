/**
 * @file Use MJML files to generate their HTML and Thumbnail equivalents
 */

/* ------------------------ IMPORTS ---------------------------------------- */
const promisify = require('es6-promisify')
const webshot = require('webshot')
const path = require('path')
const fs = require('fs')
const mjml2html = require('mjml')

/* ------------------------ GLOBALS ---------------------------------------- */
const access = promisify(fs.access)
const readDir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)
const mkdir = promisify(fs.mkdir)
const MJML_FOLDER = path.join(__dirname, 'mjml')
const HTML_FOLDER = path.join(__dirname, 'html')
const THUMB_FOLDER = path.join(__dirname, 'thumbnails')


/* ------------------------ HELPERS ---------------------------------------- */
/**
 * Check that folder is writable or create it
 */
async function isWritableOrCreate(folder) {
  try {
    await access(folder, fs.constants.R_OK | fs.constants.W_OK)
  } catch (err) {
    if (err.code === 'ENOENT') {
      await mkdir(folder)
    } else {
      throw err
    }
  }
}

/**
 * Read Content of MJML Path
 * @param  {String} mjmlPath
 * @return {Object}
 * {
 *   name:
 *     Type: String
 *     Description: Path basename
 *
 *   mjmlContent:
 *     Type: String
 *     Description: MJML string content
 * }
 */
async function readContent(mjmlPath) {
  const mjmlContent = await readFile(mjmlPath, { encoding: 'utf8' })
  return {
    name: path.basename(mjmlPath, '.mjml'),
    mjmlContent
  }
}

/**
 * Generate thumbnail and html derivatives
 * @param {Object} templateContent Output from readContent
 */
async function generateDerivatives(templateContent) {
  console.log(` > treating ${templateContent.name}`)
  const thumbnailPath = path.join(THUMB_FOLDER, `${templateContent.name}.jpg`)
  const html = await getHTML(templateContent.mjmlContent)
  const htmlPath = path.join(HTML_FOLDER, `${templateContent.name}.html`);
  await writeFile(htmlPath, html);
  await createThumbnail(thumbnailPath, html);
}

/**
 * Get HTML from mjml
 * @param  {String} mjml
 * @return {String} HTML content
 */
function getHTML(mjml) {
  return new Promise((resolve, reject) => {
    try {
      const res = mjml2html(mjml)
      resolve(res.html)
    } catch (err) {
      reject(err)
    }
  })
}

/**
 * Create thumbnail from HTML
 * @param  {String} thumnailPath
 * @param  {String} html
 * @return {Promise}
 */
function createThumbnail(thumbnailPath, html) {
  let webshotOptions = {
    siteType: 'html',
    screenSize: {
      width: 700,
    },
    shotSize: {
      width: 700,
      height: 'all',
    },
    defaultWhiteBackground: true,
  };
  return new Promise((resolve, reject) => {
    webshot(html, thumbnailPath, webshotOptions, (err) => {
      if (err) { return reject(err) }
      resolve()
    })
  })
}


/* ------------------------ MAIN ------------------------------------------- */
;
(async function () {
  try {
    await isWritableOrCreate(THUMB_FOLDER);
    await isWritableOrCreate(HTML_FOLDER);

    console.log('>> Reading templates');
    const templateNameList = await readDir(MJML_FOLDER)
    const templatePathList = templateNameList.map((relativePath) => {
      return path.join(MJML_FOLDER, relativePath)
    });
    const templateContentList = await Promise.all(templatePathList.map(readContent));

    console.log('>> Generating Derivatives');
    await templateContentList.forEach(async (templateContent) => {
      await generateDerivatives(templateContent);
    });

  } catch (err) {
    console.log('> Something went wrong')
    console.log(err.message || err)
    process.exit(1)
  }
})()
