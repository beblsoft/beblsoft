# MJML Documentation

MJML is a markup language designed to reduce the pain of coding a responsive email. Its semantic syntax
makes it easy and straightforward and its rich standard library components speeds up your development
time and lightens your email codebase. MJML's open-source engine generates high quality responsive
HTML compliant with best practices.

Relevant URLs:
[Home](https://mjml.io/),
[Github](https://github.com/mjmlio/mjml),
[Documentation](https://mjml.io/documentation/),
[Free Online Editor](https://mjml.io/try-it-live),
[Templates](https://mjml.io/templates)

Tools and Plugins:

- [MJML App](https://mjmlio.github.io/mjml-app/)
- [Atom Plugin](https://atom.io/users/mjmlio)
- [Sublime Text Plugin](https://packagecontrol.io/packages/MJML-syntax)

## Installation

```bash
# Via NPM
npm install mjml
```
## Usage

### Command Line Interface

Compile the MJML file and output the HTML content:
```bash
mjml input.mjml -o output.html
```

| Command                                                 | Description                                                                               | Default Value                                                 |
|:--------------------------------------------------------|:------------------------------------------------------------------------------------------|:--------------------------------------------------------------|
| `mjml -m [input]`                                       | Migrates a v3 MJML file to the v4 syntax                                                  | NA                                                            |
| `mjml [input] -o [output]`                              | Writes the output to [output]                                                             | NA                                                            |
| `mjml [input] -s`                                       | Writes the output to stdout                                                               | NA                                                            |
| `mjml -w [input]`                                       | Watches the changes made to input                                                         | NA                                                            |
| `mjml [input] --config.beautify`                        | Beautifies the output (true or false)                                                     | true                                                          |
| `mjml [input] --config.minify`                          | Minifies the output (true or false)                                                       | false                                                         |
| `mjml [input] --config.mjmlConfigPath [mjmlconfigPath]` | Uses the .mjmlconfig file in the specified path or directory to include custom components | The .mjmlconfig file in the current working directory, if any |
| `mjml [input] --config.validationLevel`                 | Validation level: 'strict', 'soft' or 'skip'                                              | 'soft'                                                        |

### NodeJS Module API

```js
import mjml2html from 'mjml'

/*
  Compile an mjml string
*/
const htmlOutput = mjml2html(`
  <mjml>
    <mj-body>
      <mj-section>
        <mj-column>
          <mj-text>
            Hello World!
          </mj-text>
        </mj-column>
      </mj-section>
    </mj-body>
  </mjml>
`, options)


/*
  Print the responsive HTML generated and MJML errors if any
*/
console.log(htmlOutput)
```

You can pass optional `options` as an object to the `mjml2html` function:

| option          | unit    | description                                                                            | default value   |
|:----------------|:--------|:---------------------------------------------------------------------------------------|:----------------|
| fonts           | object  | Default fonts imported in the HTML rendered by HTML                                    | See in index.js |
| keepComments    | boolean | Option to keep comments in the HTML output                                             | true            |
| beautify        | boolean | Option to beautify the HTML output                                                     | false           |
| minify          | boolean | Option to minify the HTML output                                                       | false           |
| validationLevel | string  | Available values for the validator: 'strict', 'soft', 'skip'                           | 'soft'          |
| filePath        | string  | Full path of the specified file to use when resolving paths from mj-include components | '.'             |
| mjmlConfigPath  | string  | The path or directory of the .mjmlconfig file                                          | process.cwd()   |

## Components

Components are the core of MJML. A component is an abstraction of a more complex email-responsive
HTML layout. It exposes attributes, enabling you to interact with the final component visual
aspect.

MJML comes out of the box with a set of standard components to help you easily build your first
templates without having to reinvent the wheel.

For instance, the `mj-button` component is, on the inside, a complex HTML layout:
```html
<!-- MJML -->
<mj-button href="#">
    Hello There!
</mj-button>

<!-- HTML -->
<table cellpadding="0" cellspacing="0" style="border:none;border-radius:3px;" align="center">
    <tbody>
        <tr>
            <td style="background-color:#414141;border-radius:3px;color:#ffffff;cursor:auto;" align="center" valign="middle" bgcolor="#414141">
                <a class="mj-content" href="#" style="display:inline-block;text-decoration:none;background-color:#414141;border:1px solid #414141;border-radius:3px;color:#ffffff;font-size:13px;font-weight:bold;padding:15px 30px;" target="_blank">
                    Hello There!
                </a>
            </td>
        </tr>
    </tbody>
</table>
```

### mjml

A MJML document starts with a `<mjml>` tag, it can contain only `my-head` and `mj-body` tags.
Both have the same purpose of `head` and `body` in a HTML document.

| attribute | unit   | description                                                                                                                              | default value |
|:----------|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------|:--------------|
| owa       | string | if set to "desktop", will force desktop display on outlook. Otherwise outlook will display mobile version since it ignores media queries | none          |
| lang      | string | used as `<html lang="">` attribute                                                                                                       | none          |

### mj-head

mj-head contains head components, related to the document such as style and meta elements.

### mj-body

This is the starting point of your email.

```html
<mjml>
  <mj-body>
    <!-- Your email goes here -->
  </mj-body>
</mjml>
```

| attribute        | unit          | description                                        | default value |
|:-----------------|:--------------|:---------------------------------------------------|:--------------|
| background-color | color formats | the general background color                       | n/a           |
| css-class        | string        | class name, added to the root HTML element created | n/a           |
| width            | px            | email's width                                      | 600px         |

### mj-include

The mjml-core package allows you to include external mjml files to build your email template.

```html
<!-- header.mjml -->
<mj-section>
  <mj-column>
    <mj-text>This is a header</mj-text>
  </mj-column>
</mj-section>

<!-- main.mjml -->
<mjml>
  <mj-body>
    <mj-include path="./header" /> <!-- or 'header.mjml' -->
  </mj-body>
</mjml>
```

## Standard Head Components

### mj-attributes

This tag allows you to modify default attributes on a `mj-tag` and add `mj-class` to them.

```html
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-text padding="0" />
      <mj-class name="blue" color="blue" />
      <mj-class name="big" font-size="20px" />
      <mj-all font-family="Arial" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text mj-class="blue big">
          Hello World!
        </mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-breakpoint

This tag allows you to control on which breakpoint the layout should go desktop/mobile.

```html
<mjml>
  <mj-head>
    <mj-breakpoint width="320px" />
  </mj-head>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text>
          Hello World!
        </mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-font

This tag allows you to import fonts if used in your MJML document.

```html
<mjml>
   <mj-head>
     <mj-font name="Raleway" href="https://fonts.googleapis.com/css?family=Raleway" />
   </mj-head>
   <mj-body>
     <mj-section>
       <mj-column>
         <mj-text font-family="Raleway, Arial">
           Hello World!
         </mj-text>
       </mj-column>
      </mj-section>
   </mj-body>
 </mjml>
```

### mj-preview

This tag allows you to set the preview that will be displayed in the inbox of the recipient.

```html
<mjml>
  <mj-head>
    <mj-preview>Hello MJML</mj-preview>
  </mj-head>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text>
          Hello World!
        </mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-style

This tag allows you to set CSS styles that will be applied to the HTML in your MJML document as well
as the HTML outputted. The CSS styles will be added to the head of the rendered HTML by default,
but can also be inlined by using the `inline="inline"` attribute.

```html
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-class name="mjclass" color="green" font-size="30px" />
    </mj-attributes>
    <mj-style inline="inline">
      .blue-text div {
        color: blue !important;
      }
    </mj-style>
    <mj-style>
      .red-text div {
        color: red !important;
        text-decoration: underline !important;
      }
    </mj-style>
  </mj-head>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text css-class="red-text">I'm red and underlined</mj-text>
        <mj-text css-class="blue-text">I'm blue because of inline</mj-text>
        <mj-text mj-class="mjclass">I'm green</mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-title

This tag allows you to set the title of an MJML document.

```html
<mjml>
  <mj-head>
    <mj-title>Hello MJML</mj-title>
  </mj-head>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text>
          Hello World!
        </mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

## Standard Body Components

MJML comes out of the box with a set of standard components to help you easily build your templates
without having to reinvent the wheel.

### mj-accordion

`mj-accordion` is an interactive MJML component to stack content in tabs, so the information is
collapsed and only the titles are visible. Readers can interact by clicking on the tabs to reveal
the content, providing a great experience on mobile devices where space is scarce.

Sub Elements:
- `mj-accordion-element`: create an accordion pane
- `mj-accordion-title`: Add style to an accordion pane title
- `mj-accordion-text`: Add text to accordion pane

```html
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-accordion border="none" padding="1px" />
      <mj-accordion-element icon-wrapped-url="http://i.imgur.com/Xvw0vjq.png" icon-unwrapped-url="http://i.imgur.com/KKHenWa.png" icon-height="24px" icon-width="24px" />
      <mj-accordion-title font-family="Roboto, Open Sans, Helvetica, Arial, sans-serif" background-color="#fff" color="#031017" padding="15px" font-size="18px" />
      <mj-accordion-text font-family="Open Sans, Helvetica, Arial, sans-serif" background-color="#fafafa" padding="15px" color="#505050" font-size="14px" />
    </mj-attributes>
  </mj-head>

  <mj-body>
    <mj-section padding="20px" background-color="#ffffff">
      <mj-column background-color="#dededd">
        <mj-accordion>
          <mj-accordion-element>
            <mj-accordion-title>Why use an accordion?</mj-accordion-title>
            <mj-accordion-text>
              <span style="line-height:20px">
                Because emails with a lot of content are most of the time a very bad experience on mobile, mj-accordion comes handy when you want to deliver a lot of information in a concise way.
              </span>
            </mj-accordion-text>
          </mj-accordion-element>
          <mj-accordion-element>
            <mj-accordion-title>How it works</mj-accordion-title>
            <mj-accordion-text>
              <span style="line-height:20px">
                Content is stacked into tabs and users can expand them at will. If responsive styles are not supported (mostly on desktop clients), tabs are then expanded and your content is readable at once.
              </span>
            </mj-accordion-text>
          </mj-accordion-element>
        </mj-accordion>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-button

Displays a customizable button.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-button font-family="Helvetica" background-color="#f45e43" color="white">
          Don't click me!
         </mj-button>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-carousel

`mj-carousel` displays a gallery of images or "carousel". Readers cna interact by hovering and
clicking on thumbnails depending on the email client they use.

This component enables you to set the styles of the carousel elements.

Sub Elements:
- `mj-carousel-image`: Component enables you to add and style the images in the carousel

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-carousel>
          <mj-carousel-image src="https://www.mailjet.com/wp-content/uploads/2016/11/ecommerce-guide.jpg" />
          <mj-carousel-image src="https://www.mailjet.com/wp-content/uploads/2016/09/3@1x.png" />
          <mj-carousel-image src="https://www.mailjet.com/wp-content/uploads/2016/09/1@1x.png" />
        </mj-carousel>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-column

Columns enable you to horizontally organize the content within your sections. They must be located
under `my-section` tags in order to be considered by the engine. To be responsive, columns are
expressed in terms of percentage.

Every single column has to contain something because they are responsive containers, and will be
vertically stacked on a mobile view. Any standard component, or component that you have defined
and registered, can be placed within a column - except `mj-column` or `mj-section` elements.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <!-- Your first column -->
      </mj-column>
      <mj-column>
        <!-- Your second column -->
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-divider

Displays a horizontal divider that can be customized like a HTML border.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-divider border-width="1px" border-style="dashed" border-color="lightgrey" />
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-group

mj-group allows you to prevent columns from stacking on mobile. To do so, wrap the columns inside
a `mj-group` tag, so they'll stay side by side on mobile.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-group>
        <mj-column>
          <mj-image width="137px" height="185px" padding="0"    src="https://mjml.io/assets/img/easy-and-quick.png" />
          <mj-text align="center">
            <h2>Easy and quick</h2>
            <p>Write less code, save time and code more efficiently with MJML’s semantic syntax.</p>
          </mj-text>
        </mj-column>
        <mj-column>
          <mj-image width="166px" height="185px" padding="0" src="https://mjml.io/assets/img/responsive.png" />
          <mj-text align="center">
            <h2>Responsive</h2>
            <p>MJML is responsive by design on most-popular email clients, even Outlook.</p>
          </mj-text>
        </mj-column>
      </mj-group>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-hero

Display a section with a background image and some content inside (`mj-text`, `mj-button`, ...)
wrapped in a `mj-hero` component.

```html
<mjml>
  <mj-body>
    <mj-hero
      mode="fixed-height"
      height="469px"
      background-width="600px"
      background-height="469px"
      background-url="https://cloud.githubusercontent.com/assets/1830348/15354890/1442159a-1cf0-11e6-92b1-b861dadf1750.jpg"
      background-color="#2a3448"
      padding="100px 0px">
      <mj-text
        padding="20px"
        color="#ffffff"
        font-family="Helvetica"
        align="center"
        font-size="45px"
        line-height="45px"
        font-weight="900">
        GO TO SPACE
      </mj-text>
      <mj-button href="https://mjml.io/" align="center">
        ORDER YOUR TICKET NOW
      </mj-button>
    </mj-hero>
  </mj-body>
</mjml>
```

### mj-image

Displays a responsive image in your email. It is similar to the HTML `<img/>` tag. Note that if
no width is provided, the image will use the parent column width.

### mj-navbar

Displays a menu for navigation with an optional hamburger mode for mobile devices.

Sub Elements:
- `mj-navbar-link`: Component displays an individual link in the navbar

```html
<mjml>
  <mj-body>
    <mj-section background-color="#ef6451">
      <mj-column>
        <mj-navbar base-url="https://mjml.io" hamburger="hamburger" ico-color="#ffffff">
            <mj-navbar-link href="/gettings-started-onboard" color="#ffffff">Getting started</mj-navbar-link>
            <mj-navbar-link href="/try-it-live" color="#ffffff">Try it live</mj-navbar-link>
            <mj-navbar-link href="/templates" color="#ffffff">Templates</mj-navbar-link>
            <mj-navbar-link href="/components" color="#ffffff">Components</mj-navbar-link>
        </mj-navbar>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-raw

Displays raw HTML that is not going to be parsed by the MJML engine. Anything left inside this tag
should be raw responsive HTML. If placed inside `<mj-head>`, its content will be added at the end
of the `<head>`.

```html
<mjml>
  <mj-body>
    <mj-raw>
      <!-- Your content goes here -->
    </mj-raw>
  </mj-body>
</mjml>
```

### mj-section

Sections are intended to be used as rows within your email. They will be used to structure the
layout.

The `full-width` property will be used to manage the background width. By default, it will be
600px. With the `full-width` property on, it will be changed to 100%.

```html
<mjml>
  <mj-body>
    <mj-section full-width="full-width" background-color="red">
      <!-- Your columns go here -->
    </mj-section>
  </mj-body>
</mjml>
```

### mj-social

Displays calls-to-action for various social networks with their associated logo.

Sub Elements:
- `mj-social-element`: This component enables you to display a given social network inside `mj-social`.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-social font-size="15px" icon-size="30px" mode="horizontal">
          <mj-social-element name="facebook" href="https://mjml.io/">
            Facebook
          </mj-social-element>
          <mj-social-element name="google" href="https://mjml.io/">
            Google
          </mj-social-element>
          <mj-social-element  name="twitter" href="https://mjml.io/">
            Twitter
          </mj-social-element>
        </mj-social>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

Supported networks with a share url:
- facebook
- twitter
- google
- pinterest
- linkedin
- tumblr
- xing

Without a share url:
- github
- instagram
- web
- snapchat
- youtube
- vimeo
- medium
- soundcloud
- dribbble

Custom social element:

```html
<mj-social-element href="url" background-color="#FF00FF" src="path-to-your-icon">
  Optional label
</mj-social-element>
```

### mj-spacer

Displays a blank space.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text>A first line of text</mj-text>
        <mj-spacer height="50px" />
        <mj-text>A second line of text</mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-table

This tag allows you to display a table and fill it with data.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-table>
          <tr style="border-bottom:1px solid #ecedee;text-align:left;padding:15px 0;">
            <th style="padding: 0 15px 0 0;">Year</th>
            <th style="padding: 0 15px;">Language</th>
            <th style="padding: 0 0 0 15px;">Inspired from</th>
          </tr>
          <tr>
            <td style="padding: 0 15px 0 0;">1995</td>
            <td style="padding: 0 15px;">PHP</td>
            <td style="padding: 0 0 0 15px;">C, Shell Unix</td>
          </tr>
          <tr>
            <td style="padding: 0 15px 0 0;">1995</td>
            <td style="padding: 0 15px;">JavaScript</td>
            <td style="padding: 0 0 0 15px;">Scheme, Self</td>
          </tr>
        </mj-table>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### mj-text

This tag allows you to display text in your email.

```html
<mjml>
  <mj-body>
    <mj-section>
      <mj-column>
        <mj-text>
          <h1>
            Hey Title!
          </h1>
        </mj-text>
      </mj-column>
    </mj-section>
  </mj-body>
</mjml>
```

### my-wrapper

Wrapper enables you to wrap multiple sections together. It's especially useful to achieve nested
layouts with shared border or background images across sections.

```html
<mjml>
  <mj-body>
    <mj-wrapper border="1px solid #000000" padding="50px 30px">
      <mj-section border-top="1px solid #aaaaaa" border-left="1px solid #aaaaaa" border-right="1px solid #aaaaaa" padding="20px">
        <mj-column>
          <mj-image padding="0" src="https://placeholdit.imgix.net/~text?&w=350&h=150" />
        </mj-column>
      </mj-section>
      <mj-section border-left="1px solid #aaaaaa" border-right="1px solid #aaaaaa" padding="20px" border-bottom="1px solid #aaaaaa">
        <mj-column border="1px solid #dddddd">
          <mj-text padding="20px"> First line of text </mj-text>
          <mj-divider border-width="1px" border-style="dashed" border-color="lightgrey" padding="0 20px" />
          <mj-text padding="20px"> Second line of text </mj-text>
        </mj-column>
      </mj-section>
    </mj-wrapper>
  </mj-body>
</mjml>
```

## Community Components

In addition to the standard components available in MJML, the community is contributing by creating
their own components.

To use a community component, proceed as follows:
- Install MJML locally with `npm install mjml`
- Install the community component `npm install {component-name}` in the same folder
- Create a `.mjmlconfig` file with:

  ```json
  {
    "packages": [
      "component-name/path-to-js-file"
    ]
  }
  ```

- Finally, you can now use the component in a MJML file.

  For example, `index.mjml`, and run MJML locally in your terminal
  `./node_modules/.bin/mjml index.mjml`

### mj-chart

Component displays charts

## Validating MJML

MJML provides a validation layer that helps you building your email. It can detect if you misplaced
or mispelled a MJML component, or if you used any unauthorised attribute on a specific component.

It supports 3 levels of validation:

- `skip`: your document is rendered without going through validation
- `soft`: your document is going through validation and is rendered, even if it has errors
- `strict`: your document is going through validation and is not rendered if it has any error

### Usage in CLI

```bash
mjml --config.validationLevel=skip template.mjml

mjml --validate template.mjml
```

### Usage in JavaScript

```js
mjml2html(inputMJML, { validationLevel: 'strict' })
```