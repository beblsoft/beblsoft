/*
  FILE
    array.cpp

  DESCRIPTION:
    Display templates class syntax
*/

/* ------------------------- INCLUDES -------------------------------------- */
#include <iostream>
#include "array.h"


/* ------------------------- GLOBALS --------------------------------------- */
using namespace std;



/* ------------------------- ARRAY CLASS IMPLEMENTATION -------------------- */
template <typename T>
Array<T>::Array(T arr[], int s) {
	ptr = new T[s];
	size = s;
	for (int i = 0; i < size; i++)
		ptr[i] = arr[i];
}

template <typename T>
void Array<T>::print() {
	for (int i = 0; i < size; i++) {
		cout << " " << *(ptr + i);
		cout << endl;
	}
}

/* ------------------------- MAIN ------------------------------------------ */
int main () {
   int arr[5] = {1, 2, 3, 4, 5};
   Array<int> a(arr, 5);

   a.print();

   return 0;
}
