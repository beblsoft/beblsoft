/*
  FILE
    array.h

  DESCRIPTION:
    Display templates class syntax
*/

#ifndef ARRAY_H
#define ARRAY_H


/* ------------------------- ARRAY CLASS DEFINITION ------------------------ */
template <typename T>
class Array {

public:
	Array(T arr[], int s);
	void print();

private:
	T   *ptr;
	int  size;
};


#endif /* ARRAY_H */