/*
  FILE
    max.cpp

  DESCRIPTION:
    Display template function syntax
*/

/* ------------------------- INCLUDES -------------------------------------- */
#include <iostream>


/* ------------------------- GLOBALS --------------------------------------- */
using namespace std;


/* ------------------------- TEMPLATE FUNCTION ----------------------------- */
template <typename T>
T templateMax(T x, T y) {
  return (x > y) ? x:y;
}


/* ------------------------- MAIN ------------------------------------------ */
int main () {
   cout << templateMax<int>(3, 7) << endl;
   cout << templateMax<char>('g', 'e') << endl;
   return 0;
}
