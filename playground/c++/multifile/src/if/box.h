/*
  FILE
    box.cpp

  DESCRIPTION:
	  Box Class Interface
*/


#ifndef BOX_H
#define BOX_H

using namespace std;


/* ------------------------- BOX CLASS ------------------------------------- */
class Box {

public:
	Box();
	Box(double length, double width, double height);
	~Box();

	double getVolume(void);
	double getLength(void);
	void   setLength(double length);
	double getWidth(void);
	void   setWidth(double width);
	double getHeight(void);
	void   setHeight(double height);

private:
	double length;
	double width;
	double height;
};


#endif /* BOX_H */
