/*
  FILE
    main.cpp

  DESCRIPTION:
    Multi File Main Program
*/

/* ------------------------- INCLUDES -------------------------------------- */
#include <iostream>
#include <string>
#include "box.h"


/* ------------------------- GLOBALS --------------------------------------- */
using namespace std;


/* ------------------------- MAIN ------------------------------------------ */
int main (){
   Box Box1;                // Declare Box1 of type Box
   Box Box2;                // Declare Box2 of type Box
   double volume = 0.0;     // Store the volume of a box here

   // box 1 specification
   Box1.setLength(6.0);
   Box1.setWidth(7.0);
   Box1.setHeight(5.0);

   // box 2 specification
   Box2.setLength(12.0);
   Box2.setWidth(13.0);
   Box2.setHeight(10.0);

   // volume of box 1
   volume = Box1.getVolume();
   cout << "Volume of Box1 : " << volume <<endl;

   // volume of box 2
   volume = Box2.getVolume();
   cout << "Volume of Box2 : " << volume <<endl;
}
