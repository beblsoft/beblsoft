/*
  FILE
    box.cpp

  DESCRIPTION:
	  Box Class Implementation
*/


/* ------------------------- INCLUDES -------------------------------------- */
#include "box.h"


/* ------------------------- BOX CLASS ------------------------------------- */
Box::Box() {
  length = width = height = 0;
}

Box::Box(double length, double width, double height) {
  this->length = length;
  this->width  = width;
  this->height = height;
}

Box::~Box() {
  /* Empty */
}

double Box::getVolume(void) {
  return length * width * height;
}

double Box::getLength(void) {
  return length;
}

void Box::setLength(double length) {
  this->length = length;
}

double Box::getWidth(void) {
  return width;
}

void Box::setWidth(double width) {
  this->width = width;
}

double Box::getHeight(void) {
  return height;
}

void Box::setHeight(double height) {
  this->height = height;
}