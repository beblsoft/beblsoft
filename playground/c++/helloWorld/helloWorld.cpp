/*
  FILE
    helloWorld.cpp

  DESCRIPTION:
    C++ Hello World Program
*/

#include <iostream>
#include <string>

using namespace std;

int main (){
  string s;
  cout << "Hello World!" << endl;
  return 0;
}
