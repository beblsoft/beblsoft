OVERVIEW
===============================================================================
C++ Sample Projects Documentation



PROJECT: HELLO WORLD
===============================================================================
- Description:
  * Basic hello world C++ program
- Directory                                 : ./helloWorld
- To Run
  * Clean code                              : make clean
  * Build code                              : make all
  * Run program                             : ./helloWorld


PROJECT: MULTI FILE
===============================================================================
- Description:
  * Basic multi file C++ program
- Directory                                 : ./multifile
- To Run
  * Clean code                              : make clean
  * Build code                              : make all
  * Run program                             : ./bin/multifile


PROJECT: TEMPLATES
===============================================================================
- Description:
  * Demonstrate C++ function templates (max.cpp)
  * Demonatrate C++ class templates (array.cpp)
- Directory                                 : ./templates
- To Run
  * Clean code                              : make clean
  * Build code                              : make all
  * Run max program                         : ./bin/max
  * Run array program                       : ./bin/array