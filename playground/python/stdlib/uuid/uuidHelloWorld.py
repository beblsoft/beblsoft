#!/usr/bin/env python3
"""
 NAME
  uuidDemo.py

 DESCRIPTION
  Python UUID Examples

 SAMPLE OUTPUT
   ======================================
   uuid1: Node and current time -> UUID
   Bytes        : b'\xd2\xa2C\x8atq\x11\xe8\xa2d`E\xcbd\xd3\x8b'
   Bytes LE     : b'\x8aC\xa2\xd2qt\xe8\x11\xa2d`E\xcbd\xd3\x8b'
   Fields       : (3533849482, 29809, 4584, 162, 100, 105852881392523)
   Hex          : d2a2438a747111e8a2646045cb64d38b
   Integer      : 279980401096881801344487313977205117835
   URN          : urn:uuid:d2a2438a-7471-11e8-a264-6045cb64d38b
   Variant      : specified in RFC 4122
   ======================================
   uuid3: Namespace and name -> MD5 Hash -> UUID
   Bytes        : b'<\xa8\xe6\xbcD\xab>\x06\x81\x8e\xee_\n\x9c\xb9\xc8'
   Bytes LE     : b'\xbc\xe6\xa8<\xabD\x06>\x81\x8e\xee_\n\x9c\xb9\xc8'
   Fields       : (1017702076, 17579, 15878, 129, 142, 262091967347144)
   Hex          : 3ca8e6bc44ab3e06818eee5f0a9cb9c8
   Integer      : 80630665489684195871458152246161488328
   URN          : urn:uuid:3ca8e6bc-44ab-3e06-818e-ee5f0a9cb9c8
   Variant      : specified in RFC 4122
   ======================================
   uuid4: Random -> UUID
   Bytes        : b'\xc4\xc7{\xfd\xfa\xb1D\xbd\xa3\xae\x13*\xa6G\xf3\xe4'
   Bytes LE     : b'\xfd{\xc7\xc4\xb1\xfa\xbdD\xa3\xae\x13*\xa6G\xf3\xe4'
   Fields       : (3301407741, 64177, 17597, 163, 174, 21073899287524)
   Hex          : c4c77bfdfab144bda3ae132aa647f3e4
   Integer      : 261564469107383863996121216423117190116
   URN          : urn:uuid:c4c77bfd-fab1-44bd-a3ae-132aa647f3e4
   Variant      : specified in RFC 4122
   ======================================
   uuid5: Namespace and name -> SHA-1 Hash -> UUID
   Bytes        : b"\xe0\xdcHaT\xd1Q\xdd\xb8'\xb4]\xa2FpS"
   Bytes LE     : b"aH\xdc\xe0\xd1T\xddQ\xb8'\xb4]\xa2FpS"
   Fields       : (3772532833, 21713, 20957, 184, 39, 198314247483475)
   Hex          : e0dc486154d151ddb827b45da2467053
   Integer      : 298890844409571837335618530613867737171
   URN          : urn:uuid:e0dc4861-54d1-51dd-b827-b45da2467053
   Variant      : specified in RFC 4122

"""

# ----------------------- IMPORTS ------------------------------------------- #
import uuid


# ----------------------- HELPERS ------------------------------------------- #
def printUUID(uid, title):
    """
    Print uuid
    Args
      uid:
        uuid to print
      title:
        Title of uuid
    """
    print("======================================")
    print("{}".format(title))
    print("Bytes        : {}".format(uid.bytes))
    print("Bytes LE     : {}".format(uid.bytes_le))
    print("Fields       : {}".format(uid.fields))
    print("Hex          : {}".format(uid.hex))
    print("Integer      : {}".format(uid.int))
    print("URN          : {}".format(uid.urn))
    print("Variant      : {}".format(uid.variant))


# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":
    # uuid1
    node      = uuid.getnode()
    uid1      = uuid.uuid1(node=node)
    printUUID(uid1, "uuid1: Node and current time -> UUID")

    # uuid3
    email     = "bensson.james@gmail.com"
    uid3      = uuid.uuid3(namespace=uuid.NAMESPACE_URL, name=email)
    printUUID(uid3, "uuid3: Namespace and name -> MD5 Hash -> UUID")

    # uuid4
    uid4      = uuid.uuid4()
    printUUID(uid4, "uuid4: Random -> UUID")

    # uuid5
    uid5      = uuid.uuid5(namespace=uuid.NAMESPACE_URL, name=email)
    printUUID(uid5, "uuid5: Namespace and name -> SHA-1 Hash -> UUID")
