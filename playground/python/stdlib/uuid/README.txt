OVERVIEW
===============================================================================
- Python uuid Documentation
- Description                       : uuid module provides UUID objects and functions for
                                      generating version 1, 3, 4, and 5 UUIDs as specified
                                      in RFC 4122
- Relevant URLs ---------------------
 * Home                             : https://docs.python.org/3/library/uuid.html
