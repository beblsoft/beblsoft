#!/usr/bin/python
"""
NAME
 jsonPerson.py

DESCRIPTION
  Sample person class implementing JSON methods
"""

# ----------------------------- IMPORTS ------------------------------------- #
import json
import os


# ----------------------------- PERSON CLASS -------------------------------- #
class Person():
    """
    Person Class
    """
    location = "Earth"

    def __init__(self):
        self.firstName = ""
        self.lastName  = ""
        self.email     = ""
        self.age       = None

    def toJSON(self):
        '''Return JSON object from Person object.'''
        return json.dumps(self, default=Person.toDict, indent=3)

    @staticmethod
    def fromJSON(jsonText):
        '''Return Person object from JSON text.'''
        return json.loads(jsonText, object_hook=Person.fromDict)

    def toJSONFile(self, filePath):
        '''Write Person object to JSON file.'''
        with open(filePath, 'w') as f:
            json.dump(self, f, default=Person.toDict, indent=3)

    @staticmethod
    def fromJSONFile(jsonFile):
        '''Return Person object from JSON file.'''
        with open(jsonFile, 'r') as f:
            p = json.load(f, object_hook=Person.fromDict)
        return p

    @staticmethod
    def toDict(obj):
        '''Return dictionary object from Person object.'''
        d = {'__classname__': Person.__name__}
        d.update(vars(obj))
        return d

    @staticmethod
    def fromDict(d):
        '''Return Person object from dictionary object.'''
        obj = None
        classname = d.pop('__classname__', None)
        if (not classname):
            return obj
        else:
            obj = Person()
            for key, value in d.items():
                setattr(obj, key, value)
        return obj


# ----------------------------- MAIN ---------------------------------------- #
def createJSONFromPerson(first, last, email, age, filePath):
    print("Creating JSON from person...")
    p = Person()
    p.firstName = first
    p.lastName  = last
    p.email     = email
    p.age       = age

    print(p.toJSON())
    p.toJSONFile(filePath)


def createPersonFromJSON(filePath):
    print("Creating Person from JSON...")
    p = Person.fromJSONFile(filePath)
    print(p.__dict__)

if __name__ == '__main__':
    filePath = os.getcwd() + "/" + "person.json"
    createJSONFromPerson("James", "Bensson", "bensson.james@gmail.com", 25, filePath)
    createPersonFromJSON(filePath)
