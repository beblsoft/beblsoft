OVERVIEW
===============================================================================
- JSON Documentation
- Description                       : JSON (JavaScript Object Notation), specified by RFC 7159 and ECMA-404
                                      is a lightweight data interchange format inspired by JavaScript
                                      literal syntax
                                    : Python json library exposes an API familiar to users with standard
                                      library marshal and pickle modules
- Relevant URLs ---------------------
 * Home                             : https://docs.python.org/3/library/json.html
