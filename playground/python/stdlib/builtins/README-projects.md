# Builtin Samples

## max.py

Program to demonstrate `max()` builtin.

To run:
```bash
./max.py
```
