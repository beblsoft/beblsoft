#!/usr/bin/env python3.6
"""
NAME
 max.py

DESCRIPTION
 max hello world
"""

# ----------------------------- HELPERS ------------------------------------- #
def getNum(obj):
    """
    Return number from object
    """
    num = obj.get("num", 0)
    num = num if num is not None else 0
    return num


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    complexList    = [{"num" : 1}, {"num" : 2}, {"num" : 3}, {"num" : 4}, {"num" : 5}, {"num" : None}]
    maxObj         = max(complexList, key=getNum)
    print(maxObj)
