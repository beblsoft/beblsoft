# Python Builtins Documentation

The Python interpreter has a number of functions and types built into it that are always available.

Relevant URLs:
[Documentation](https://docs.python.org/3.6/library/functions.html)
