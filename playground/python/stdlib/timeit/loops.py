#!/usr/bin/env python3
"""
 NAME
  loops.py

 DESCRIPTION
  Timeit Loop Comparison Program
"""

# ------------------------ IMPORTS ------------------------------------------ #
import timeit


# ------------------------ LOOPS -------------------------------------------- #
def loop1(nChars):  # pylint: disable=C0111
    _ = "-".join(str(n) for n in range(nChars))


def loop2(nChars):  # pylint: disable=C0111
    _ = "-".join([str(n) for n in range(nChars)])


def loop3(nChars):  # pylint: disable=C0111
    _ = "-".join(map(str, range(nChars)))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    nCharacters   = 10
    nIterations   = 100000
    funcList      = [loop1, loop2, loop3]

    # Time all functions
    for func in funcList:
        totalTime   = timeit.timeit(
            stmt        = "{}(nChars={})".format(func.__name__, nCharacters),
            number      = nIterations,
            globals     = globals())
        print("{} nIterations={} time={}".format(func.__name__, nIterations, totalTime))
