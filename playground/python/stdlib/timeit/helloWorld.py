#!/usr/bin/env python3
"""
 NAME
  helloWorld.py

 DESCRIPTION
  Timeit Hello World Program
"""

# ------------------------ IMPORTS ------------------------------------------ #
import timeit


# ------------------------ HELPERS ------------------------------------------ #
def helloWorld(): #pylint: disable=C0111
    _ = "Hello World!"


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    nIterations = 100000
    totalTime   = timeit.timeit(
        stmt        = "helloWorld()",
        setup       = "from __main__ import helloWorld",
        number      = nIterations)

    print("Total Time For {} Iterations = {}".format(nIterations, totalTime))
