OVERVIEW
===============================================================================
Python Timeit Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- helloWorld.py -------------------
  * Description                   : Timeit hello world program
  * Run                           : ./helloWorld.py

- loops.py ------------------------
  * Description                   : Time and various loops
  * Run                           : ./loops.py