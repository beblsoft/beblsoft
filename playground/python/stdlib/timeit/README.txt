OVERVIEW
===============================================================================
- Python Timeit Documentation
- Description                       : Module provides a simple way to time small bits of Python code
                                      Command Line Interface and Callable Python Interface
- URLS
  * Docs                            : https://docs.python.org/3/library/timeit.html


BASIC EXAMPLES
===============================================================================
- Command Line Interface            : $ python3 -m timeit '"-".join(str(n) for n in range(100))'
                                      $ python3 -m timeit '"-".join([str(n) for n in range(100)])'
                                      $ python3 -m timeit '"-".join(map(str, range(100)))'
- Python Interface                  : >>> import timeit
                                      >>> timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)
                                      >>> timeit.timeit('"-".join([str(n) for n in range(100)])', number=10000)
                                      >>> timeit.timeit('"-".join(map(str, range(100)))', number=10000)

PYTHON INTERFACE
===============================================================================
- Timer Class
  * Constructor                     : timeit.Timer(stmt='pass', setup='pass', timer=<timer function>, globals=None)
  * Execute a certain number of
    times                           : timeit(number=1000000)
  * Automatically determine # of
    times. Running more than .2s    : autorange(callback=None)
  * Call timeit a few times         : repeat(repeat=5, number=1000000)

- Timeit Function                   : Creates timer and calls timeit
  Signature                         : timeit.timeit(stmt='pass', setup='pass', timer=<default timer>, number=1000000, globals=None)

- Repeat Function                   : Create timer, run its repeat method
  Signature                         : timeit.repeat(stmt='pass', setup='pass', timer=<default timer>, repeat=5, number=1000000, globals=None)