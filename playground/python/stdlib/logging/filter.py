#!/usr/bin/env python3
"""
NAME
 filter.py

DESCRIPTION
 Filter log entries

BIBLIOGRAPHY
  https://docs.python.org/3.3/howto/logging-cookbook.html#using-filters-to-impart-contextual-information
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging

# ------------------------ LOG FILTER --------------------------------------- #
class LogFilter(logging.Filter):
    """
    Log Filter
    """

    def filter(self, record):
        """
        Args
          record:
            Log Record
            Ex. {
              'args': ('INFO', 2, 'parameters'),
              'created': 1569348285.091695,
              'exc_info': None,
              'exc_text': None,
              'filename': 'filter.py',
              'funcName': '<module>',
              'levelname': 'INFO',
              'levelno': 20,
              'lineno': 52,
              'module': 'filter',
              'msecs': 91.69507026672363,
              'msg': 'A message at %s level with %d %s',
              'name': 'd.e.f',
              'pathname': './filter.py',
              'process': 1322,
              'processName': 'MainProcess',
              'relativeCreated': 6.860971450805664,
              'stack_info': None,
              'thread': 140100008027968,
              'threadName': 'MainThread'
            }

        Returns
          True if record should be logged
          False otherwise
        """
        # import pprint
        # print(pprint.pformat(record.__dict__))
        return record.levelno >= logging.INFO


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)-15s %(name)-5s %(levelname)-8s %(message)s')

    # Create logger
    namedLogger = logging.getLogger('Named Logger')
    namedLogger.addFilter(LogFilter())

    # Log Some Messages
    namedLogger.debug('Debug message')
    namedLogger.info('Info message')
    namedLogger.warning('Warning message')
    namedLogger.error('Error message')
    namedLogger.critical('Critical message')
    namedLogger.debug('Debug message')
