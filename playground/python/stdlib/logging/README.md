# Python Logging Documentation

The python `logging` standard library module implements a flexible event logging system for applications
and libraries. The key benefit of having the logging API provided by a standard library module is that
all Python modules can participate in logging, so your application log can include your own messages
integrated with messages from third-party modules.

The basic classes defined by the module, together with their functions:

- __Loggers__ expose the interface that application code directly uses
- __Handlers__ send the log records (created by loggers) to the appropriate destination
- __Filters__ provide a finer grained facility for determining which log records to output
- __Formatters__ specify the layout of log records in the final output

Relevant URLs:
[Python Docs](https://docs.python.org/3.3/library/logging.html#module-logging),
[Cookbook](https://docs.python.org/3.3/howto/logging-cookbook.html#),
[Basic Logging Tutorial](https://docs.python.org/3.3/howto/logging.html#),
[Advanced Logging Tutorial](https://docs.python.org/3.3/howto/logging.html#advanced-logging-tutorial)
