# Python Logging Samples Documentation

## filter.py

Demonstrate filtering log records.

To run:
```bash
./filter.py
```