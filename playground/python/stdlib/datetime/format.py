#! /usr/bin/env python3
"""
NAME
 format.py

DESCRIPTION
 Program to demonstrate python strptime and strftime
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime


# ----------------------------- MAIN ---------------------------------------- #
timeStr = '1970-02-21 09:49:23.347000'
time    = datetime.datetime.strptime(timeStr, "%Y-%m-%d %H:%M:%S.%f")
print(time.strftime("%H:%M:%S.%f"))
