OVERVIEW
===============================================================================
- Python Datetime Documentation
- Description                       : datetime module supplies classes from manipulating
                                      dates and times in both simple and complex ways.
- Relevant URLs ---------------------
 * Home                             : https://docs.python.org/3/library/datetime.html