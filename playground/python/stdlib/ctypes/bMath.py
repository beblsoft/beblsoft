#!/usr/bin/env python3
"""
 NAME
  bMath.py

 DESCRIPTION
  Demonstrates several examples of how to write python wrappers over C code.

 ASSOCIATED C CODE:
  playground/c/src/bMath.c
"""

# ------------------------------- IMPORTS ----------------------------------- #
import ctypes


# ------------------------------- GLOBALS ----------------------------------- #
path = '../../../c/lib/libbMath.so'
mod = ctypes.cdll.LoadLibrary(path)


# ------------------------------- WRAPPER FUNCTIONS ------------------------- #
# ---------------- GCD -------------- #
gcd = mod.gcd
gcd.argtypes = (ctypes.c_int, ctypes.c_int)
gcd.restype = ctypes.c_int


# ---------------- DIVIDE ----------- #
_divide = mod.divide
_divide.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_int))
_divide.restype = ctypes.c_int


def divide(x, y):
    """Wrapper over _divide so client doesn't have to deal with ctypes"""
    rem = ctypes.c_int()
    quot = _divide(x, y, rem)
    return quot, rem.value


# ---------------- AVERAGE ---------- #
class DoubleArrayType():
    """Special type for double * argument"""

    def from_param(self, param): #pylint: disable=R1710
        """
        Function to tell ctypes how to handle parameter input
        """
        typename = type(param).__name__
        if hasattr(self, "from_" + typename):
            return getattr(self, "from_" + typename)(param)

    def from_list(self, param): #pylint: disable=R0201,C0111
        val = ((ctypes.c_double) * len(param))(*param)
        return val

DoubleArray = DoubleArrayType()
_avg = mod.avg
_avg.argtypes = (DoubleArray, ctypes.c_int)
_avg.restype = ctypes.c_double


def avg(values): #pylint: disable=C0111
    return _avg(values, len(values))


# ---------------- POINT ------------ #
class Point(ctypes.Structure): #pylint: disable=C0111
    _fields_  = [("x", ctypes.c_double), ("y", ctypes.c_double)]


# ---------------- DISTANCE --------- #
distance = mod.distance
distance.argtypes = (ctypes.POINTER(Point), ctypes.POINTER(Point))
distance.restype = ctypes.c_double


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    print("gcd(20,5)    = {}".format(gcd(20, 5)))
    print("divide(42,8) = {}".format(divide(42, 8)))
    print("avg[1,2,3]   = {}".format(avg([1, 2, 3])))

    p1 = Point(1, 2)
    p2 = Point(4, 5)
    print("Distance={}".format(distance(p1, p2)))
