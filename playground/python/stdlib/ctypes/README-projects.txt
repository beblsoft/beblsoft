OVERVIEW
===============================================================================
Python ctypes Samples Documentation



SINGLE FILE SAMPLES
===============================================================================
- Math ------------------------------
  * Description                     : Wrap over C math shared library in python
  * Associated C files              : playground/c/src/bMath.c
                                      playground/c/lib/libbMath.so
  * Run
    * Make C library                : cd playground/c
                                      make bMath
    * Run bMath.py                  : ./bMath.py
