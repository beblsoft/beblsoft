#!/usr/bin/env python3
"""
 NAME
  encodedValues.py

 DESCRIPTION
  Encode different types
  Cookie values are always encoded as strings

 OUTPUT
  integer
    Set-Cookie: integer=5
    value=5 <class 'str'>
    coded_value=5

  string_with_quotes
    Set-Cookie: string_with_quotes="He said\054 \"Hello\054 World!\""
    value=He said, "Hello, World!" <class 'str'>
    coded_value="He said\054 \"Hello\054 World!\""
"""

# -------------------------- IMPORTS ---------------------------------------- #
from http import cookies


# -------------------------- MAIN ------------------------------------------- #
if __name__ == "__main__":
    c = cookies.SimpleCookie()
    c['integer'] = 5
    c['string_with_quotes'] = 'He said, "Hello, World!"'

    for name in ['integer', 'string_with_quotes']:
        print(c[name].key)
        print('  {}'.format(c[name]))
        print('  value={} {}'.format(c[name].value, type(c[name].value)))
        print('  coded_value={}'.format(c[name].coded_value))
        print()
