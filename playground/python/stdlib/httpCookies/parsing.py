#!/usr/bin/env python3
"""
 NAME
  parsing.py

 DESCRIPTION
  Parse Cookie
"""

# -------------------------- IMPORTS ---------------------------------------- #
from http import cookies


# -------------------------- MAIN ------------------------------------------- #
if __name__ == "__main__":
    HTTP_COOKIE = r'Set-Cookie: integer=5; string_with_quotes="He said, \"Hello, World!\""'

    print('From constructor:')
    c = cookies.SimpleCookie(HTTP_COOKIE)
    print(c)

    print()
    print('From load():')
    c = cookies.SimpleCookie()
    c.load(HTTP_COOKIE)
    print(c)
