# http.cookies Samples Documentation

Run each file on the command line as follows: `./<file-name>.py`

## `helloWorld.py`

Cookie Hello World Program

## `morsels.py`

Construct a cookie with multiple morsels.

## `encodedValues.py`

Show that all cookie values are strings.

## `parsing.py`

Parse cookie string to create a cookie.

## `jsOutputFormat.py`

Output the cookies into different formats.