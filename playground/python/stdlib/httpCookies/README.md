# Python http.cookies Documentation

http.cookies module defines classes for abstracting the concept of cookies, an HTTP state management
mechanism. It supports both simple string-only cookies, and provides an abstraction for having
any serializable data-type as a cookie value.

Note:
The naming scheme is a bit confusing here. One _python Cookie_ represents a collection of many
individual http cookies from an HTTP cookie string. Each _python Morsel_ is a cookie.

Relevant URLs:
[Docs](https://docs.python.org/3/library/http.cookies.html),
[RFC 2109](https://tools.ietf.org/html/rfc2109.html),
[RFC 2068](https://tools.ietf.org/html/rfc2068.html)

## Cookie Classes

### http.cookies.CookieError

### http.cookies.BaseCoookie

This class is a dictionary-like object whose keys are strings and whose values are Morsel isntances.
Not that upon setting a key to a value, the value is first converted to a Morsel contain the key and
the value.

### http.cookies.SimpleCookie

This class derives from BaseCookie and overrides value_decode() and value_encode(). SimpleCookie
supports strings as cookie values. When setting the value, SimpleCookie calls the builtin `str()`
to convert the value to a string. Values received from HTTP are kept as strings.

## Morsel Objects

### http.cookies.Morsel

Abstract a key/value pair, which has some RFC 2109 attributes.

Morsels are dictionary-like objects, whose set of keys is constant - the value RFC 2109 sttributes,
which are:

- expires
- path
- comment
- domain
- max-age
- secure
- version
- httponly