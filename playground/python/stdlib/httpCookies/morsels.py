#!/usr/bin/env python3
"""
 NAME
  morsels.py

 DESCRIPTION
  Create Cookie Morsels

 OUTPUT
  Set-Cookie: encoded_value_cookie='cookie_value'; Comment=Notice that this cookie value has escaped quotes
  Set-Cookie: expires_at_time=cookie_value; expires=Sat, 14 Feb 2009 19:30:14
  Set-Cookie: restricted_cookie=cookie_value; Domain=PyMOTW; Path=/sub/path; secure
  Set-Cookie: with_max_age="expires in 5 minutes"; Max-Age=300

  key = expires_at_time
    value = cookie_value
    coded_value = cookie_value
    expires = Sat, 14 Feb 2009 19:30:14

  key = encoded_value_cookie
    value = 'cookie_value'
    coded_value = 'cookie_value'
    comment = Notice that this cookie value has escaped quotes

  key = with_max_age
    value = expires in 5 minutes
    coded_value = "expires in 5 minutes"
    max-age = 300

  key = restricted_cookie
    value = cookie_value
    coded_value = cookie_value
    secure = True
    domain = PyMOTW
    path = /sub/path
"""

# -------------------------- IMPORTS ---------------------------------------- #
from http import cookies
import datetime


# -------------------------- HELPERS ---------------------------------------- #
def showCookie(cookie):
    """
    Show Cookie
    """
    print(cookie)
    for _, morsel in cookie.items():
        print()
        print("key = {}".format(morsel.key))
        print("  value = {}".format(morsel.value))
        print("  coded_value = {}".format(morsel.coded_value))
        for name in morsel.keys():
            if morsel[name]:
                print("  {} = {}".format(name, morsel[name]))


# -------------------------- MAIN ------------------------------------------- #
if __name__ == "__main__":
    c = cookies.SimpleCookie()

    # A cookie with a value that has to be encoded to fit into the header
    c["encoded_value_cookie"] = "'cookie_value'"
    c["encoded_value_cookie"]["comment"] = "Notice that this cookie value has escaped quotes"

    # A cookie that only applies to part of a site
    c["restricted_cookie"] = "cookie_value"
    c["restricted_cookie"]["path"] = "/sub/path"
    c["restricted_cookie"]["domain"] = "PyMOTW"
    c["restricted_cookie"]["secure"] = True

    # A cookie that expires in 5 minutes
    c["with_max_age"] = "expires in 5 minutes"
    c["with_max_age"]["max-age"] = 300  # seconds

    # A cookie that expires at a specific time
    c["expires_at_time"] = "cookie_value"
    expires = datetime.datetime(2009, 2, 14, 18, 30, 14) + datetime.timedelta(hours=1)
    c["expires_at_time"]["expires"] = expires.strftime("%a, %d %b %Y %H:%M:%S")  # Wdy, DD-Mon-YY HH:MM:SS GMT

    showCookie(c)
