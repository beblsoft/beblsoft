#!/usr/bin/env python3
"""
 NAME
  jsOutputFormat.py

 DESCRIPTION
  Output Cookie in JavaScript
"""

# -------------------------- IMPORTS ---------------------------------------- #
from http import cookies


# -------------------------- MAIN ------------------------------------------- #
if __name__ == "__main__":
    c = cookies.SimpleCookie()
    c['mycookie'] = 'cookie_value'
    c['another_cookie'] = 'second value'
    print(c.js_output())
