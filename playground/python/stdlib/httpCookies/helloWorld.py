#!/usr/bin/env python3
"""
 NAME
  helloWorld.py

 DESCRIPTION
  Cookie Hello World
"""

# -------------------------- IMPORTS ---------------------------------------- #
from http import cookies


# -------------------------- HELPERS ---------------------------------------- #
if __name__ == "__main__":
    c = cookies.SimpleCookie()
    c['cookie_name'] = 'cookie_hello_world!'
    print(c)
