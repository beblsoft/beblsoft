#!/usr/bin/python
"""
NAME
 documentation.py

DESCRIPTION
  Highlights Python documentation facilities
  Put name text here... Ex. Module highlights Python documentation facilities.

  Put description text here...
  Ex. Python coding standard: use doc strings for high level documentation,
  comments for implementation details.

  Triple quotes attach a string to an object's __doc__ attrubute.


USAGE
 $ python3                   - start python3 interactive mode
 > import documentation
 > help(doc)                 - documentation on doc
 > dir(doc)                  - show attributes of doc object

MISC USAGE
 $ python3 -m pydoc -b       - start python3 documentation webserver, client
 $ pydoc3 -p 1234            - start python3 documentation server on port 1234
"""

# ----------------------- CONSTANTS ----------------------------------------- #
DOC_SAMPLE_CONSTANT = 10


# ----------------------- FUNCTIONS ----------------------------------------- #
def SampleFunc():
    """
    SampleFunc documentation
    """
    pass


# ----------------------- CLASSES ------------------------------------------- #
class SampleClass:
    """
    SampleClass documentation.
    """

    def __init__(self):
        """
        Sample initialization routine.
        """
        pass

    def SampleClassFunc(self):
        """
        SampleFunc documentation
        """
        pass
