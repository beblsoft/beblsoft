#!/usr/bin/env python3
"""
NAME
 base64HelloWorld.py

DESCRIPTION
 Base64 Hello World Program
"""

# ----------------------------- IMPORTS ------------------------------------- #
import base64


# ----------------------------- GLOBALS ------------------------------------- #
stringList = ["Hello World!", "Goodbye World!"]


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    for string in stringList:
        lJust   = 30
        encoded = base64.b64encode(bytes(string, "utf-8")).decode("utf-8")
        decoded = base64.b64decode(encoded).decode("utf-8")
        print("Regular {}, Encoded {}, Decoded {}".format(
            string.ljust(lJust), encoded.ljust(lJust), decoded.ljust(lJust)))
