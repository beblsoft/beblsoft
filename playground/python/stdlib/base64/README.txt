OVERVIEW
===============================================================================
- Python Base64 Encoding Documentation
- Description                       : Module provides encoding and decoding as specified
                                      in RFC 3548
                                    : Standard defines the Base16, Base32, and Base64 algorithms
                                      for encoding and decoding binary strings into text strings
- URLS
 * Documentation                    : https://docs.python.org/3.6/library/base64.html
