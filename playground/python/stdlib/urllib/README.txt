OVERVIEW
===============================================================================
- Python urllib Documentation
- Description                       : urllib pack collects several modules that work with URLs
  * urllib.request                  : Opening and reading URLs
  * urllib.error                    : Contains exceptions raised by urllib.request
  * urllib.parse                    : Parsing URLs
  * urllib.robotparser              : Parsing robots.txt files
- Relevant URLs ---------------------
 * Home                             : https://docs.python.org/3/library/urllib.html
