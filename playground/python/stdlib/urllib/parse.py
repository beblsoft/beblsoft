#!/usr/bin/env python3
"""
 NAME
  parse.py

 DESCRIPTION
  Python urllib parsing example

 SAMPLE OUTPUT
  ======================================
  Parameter Formatting...
  params=spam=1&eggs=12.12%2F2&bacon=1+223+4
  url=http://www.musi-cal.com/cgi-bin/query?spam=1&eggs=12.12%2F2&bacon=1+223+4
  ======================================
  Parse URL...
  url=http://www.cwi.nl:80/%7Eguido/Python.html?foo=bar
  scheme=http
  port=80
  netloc=www.cwi.nl:80
  path=/%7Eguido/Python.html
  query=foo=bar
"""

# ----------------------- IMPORTS ------------------------------------------- #
import urllib.parse


# ----------------------- HELPERS ------------------------------------------- #
def printLine(title, string): #pylint: disable=W0621
    """
    Print string
    """
    print("======================================")
    print("{}".format(title))
    print(string)

# ----------------------- MAIN ---------------------------------------------- #
if __name__ == "__main__":

    params        = {'spam': 1, 'eggs': "12.12/2", 'bacon': "1 223 4"}
    encodedParams = urllib.parse.urlencode({'spam': 1, 'eggs': "12.12/2", 'bacon': "1 223 4"})
    url           = "http://www.musi-cal.com/cgi-bin/query?{}".format(encodedParams)
    string        = "params        = {}\n".format(params)
    string       += "encodedParams = {}\n".format(encodedParams)
    string       += "url           = {}\n".format(url)
    printLine("Parameter Formatting...", string)


    url      = 'http://www.cwi.nl:80/%7Eguido/Python.html?foo=bar'
    uParsed  = urllib.parse.urlparse(url)
    string   = "url     = {}\n".format(url)
    string  += "scheme  = {}\n".format(uParsed.scheme)
    string  += "port    = {}\n".format(uParsed.port)
    string  += "netloc  = {}\n".format(uParsed.netloc)
    string  += "path    = {}\n".format(uParsed.path)
    string  += "query   = {}\n".format(uParsed.query)
    printLine("Parse URL...", string)
