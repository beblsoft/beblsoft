#!/usr/bin/env python3.6
"""
NAME
 pickleHelloWorld.py

DESCRIPTION
 Pickle hello world
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import pickle
import pprint


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    file         = "pickle.data"
    fileDir      = os.path.dirname(os.path.realpath(__file__))
    filePath     = os.path.join(fileDir, file)
    data         = {
        "a": [1, 2.0, 3, 4 + 6j],
        "b": (u"character string", b"byte_string"),
        "c": {None, True, False}
    }
    dataCopy     = None

    with open(filePath, "wb") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    with open(filePath, "rb") as f:
        dataCopy = pickle.load(f)

    print("DataCopy:\n{}\n".format(pprint.pformat(dataCopy)))
    print("Data == DataCopy: {}".format(data == dataCopy))
