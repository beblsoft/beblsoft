#!/usr/bin/env python3
"""
 NAME
  matchEmail.py

 DESCRIPTION
   Match Email Example
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re


# ------------------------ HELPERS ------------------------------------------ #
def matchSimulatorEmail(address):
    """
    Match email
    """
    match = re.match(r".*@simulator.amazonses.com$", address)
    out   = "Simulator Email" if match else "Unknown"
    print("{} -> {}".format(address, out))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":

    addressList = [
        "success+webAPITest@simulator.amazonses.com",
        "foobar@gmail.com"]

    for a in addressList:
        matchSimulatorEmail(a)
