#!/usr/bin/env python3
"""
 NAME
  matchError.py

 DESCRIPTION
   Match Error
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re


# ------------------------ HELPERS ------------------------------------------ #
def matchError(string):
    """
    Match error string
    """
    match = re.match(r".*Table '.*' doesn't exist.*", string)
    out   = "Match" if match else "No Match"
    print("{} -> {}".format(string, out))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":

    stringList = [
        "Table 'SmecknADB19.Analysis' doesn't exist",
        "asdTable 'SmecknADB19.Analysis' doesn't existasds",
        "Table 'SmecknADB19.Analysis' dosn't exist",
        "Tabe 'SmecknADB19.Analysis' doesn't exist"]

    for s in stringList:
        matchError(s)
