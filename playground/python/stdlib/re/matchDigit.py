#!/usr/bin/env python3
"""
 NAME
  matchDigit.py

 DESCRIPTION
   Match Digits Example
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re


# ------------------------ HELPERS ------------------------------------------ #
def matchDigitsAtEnd(string):
    """
    Match digits at end of string
    """
    num   = None
    match = re.match(r"(\D*)(\d*)$", string)
    if match:
        numStr = match.group(2)
        num    = int(numStr) if numStr else None
    print("{} -> {}".format(string, num))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":

    stringList = [
        "abcdefghijklmnopqrstuvwxyz1234",
        "asdfasdf2349283sdfs",
        "a!@#$%^&*()a"]

    for s in stringList:
        matchDigitsAtEnd(s)
