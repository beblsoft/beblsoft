#!/usr/bin/python3
"""
NAME
 renamer.py

DESCRIPTION
  Program removes a common prefix from files in a directory
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import re

# ----------------------------- GLOBALS ------------------------------------- #
prefixNChars  = 22
curDir        = os.getcwd()
desiredPrefix = "Chopin.*"


# ----------------------------- MAIN ---------------------------------------- #
for file in os.listdir(curDir):
    fileBase   = os.path.basename(file)
    prefix     = fileBase[0:prefixNChars]
    suffix     = fileBase[prefixNChars:]
    # print "Prefix = " + prefix
    # print "Suffix = " + suffix
    if (re.match(desiredPrefix, suffix)):
        os.rename(fileBase, suffix)
