OVERVIEW
===============================================================================
- Python Regex Documentation
- Description                       : Module provides regular expression matching operations
                                      similar to those found in Perl.
- URLS
 * Documentation                    : https://docs.python.org/3/library/re.html
