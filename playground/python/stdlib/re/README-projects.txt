OVERVIEW
===============================================================================
Python Regex Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- matchDigit.py -------------------
  * Description                   : Match a digit at end of string
  * Run                           : ./matchDigit.py

- matchEmail.py -------------------
  * Description                   : Match specific email addresses
  * Run                           : ./matchEmail.py

- stringValidation.py -------------
  * Description                   : Validate a series of strings
  * Run                           : ./stringValidation.py

- renamer.py ----------------------
  * Description                   : Rename files in a directory
  * Run                           : ./renamer.py
