#!/usr/bin/env python3
"""
 NAME
  stringValidation.py

 DESCRIPTION
   Demonstrate String Validation
"""

# ------------------------ IMPORTS ------------------------------------------ #
import re


# ------------------------ HELPERS ------------------------------------------ #
def validateAlphaNumericDash(stringList): #pylint: disable=W0621
    """
    Validate String Only Contains "a-z", "A-Z", "0-9", "-_"
    """
    regex     = r"[^a-zA-Z0-9\-\_]"
    compRegex = re.compile(regex)

    print("AlphaNumericDash Validation, Regex='{}'".format(regex))
    for string in stringList:
        match = not bool(compRegex.search(string))
        print("{:<10}: {}".format("Match" if match else "Not Match", string))


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":

    stringList = [
        "abcdefghijklmnopqrstuvwxyz",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "123456789",
        "-_",
        "abcdefghijklmnopqrstuvwxyz&^",
        "a!@#$%^&*()a"]

    validateAlphaNumericDash(stringList)
