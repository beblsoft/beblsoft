#!/usr/bin/env python3.6
"""
NAME
 hashHelloWorld.py

DESCRIPTION
 Hashlib hello world
"""

# ----------------------------- IMPORTS ------------------------------------- #
import hashlib



# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    string       = "Hello World Hashlib!"
    stringBytes  = bytearray(string, "utf-8")
    h            = hashlib.sha256()

    h.update(stringBytes)
    print("Algorithms Guaranteed : {}".format(hashlib.algorithms_guaranteed))
    print("Algorithms Available  : {}".format(hashlib.algorithms_available))
    print("String                : {}".format(string))
    print("Hash                  : {}".format(h))
    print("Hash Name             : {}".format(h.name))
    print("Hash Digest Size      : {}".format(h.digest_size))
    print("Hash Block Size       : {}".format(h.block_size))
    print("Hash Digest           : {}".format(h.digest()))
    print("Hash Hexdigest        : {}".format(h.hexdigest()))
