#!/usr/bin/env python3.6
"""
NAME
 hashBalke2B.py

DESCRIPTION
 Hashlib blake2b demonstration

OUTPUT
  $ ./hashBlake2B.py
  Algorithms Guaranteed : {'sha3_384', 'sha384', 'shake_256', 'sha256', 'sha1', 'sha224', 'sha3_224', 'blake2s', 'sha3_512', 'md5', 'blake2b', 'sha512', 'sha3_256', 'shake_128'}
  Algorithms Available  : {'SHA', 'RIPEMD160', 'ecdsa-with-SHA1', 'SHA256', 'DSA-SHA', 'sha3_384', 'sha384', 'sha256', 'sha224', 'dsaEncryption', 'SHA1', 'sha3_224', 'sha512', 'sha3_256', 'whirlpool', 'sha1', 'sha', 'ripemd160', 'blake2s', 'md5', 'SHA512', 'DSA', 'blake2b', 'SHA384', 'MD5', 'shake_128', 'MD4', 'shake_256', 'md4', 'dsaWithSHA', 'sha3_512', 'SHA224'}
  String                : Hello World Hashlib!
  Hash                  : <_blake2.blake2b object at 0x7f071905b750>
  Salt                  : b'\r3\xfe\xba\xb3sN\x0f\xf24N\xb2\xa2\x02\x08\xc8'
  Person                : b'Person1'
  Hash Name             : blake2b
  Hash Digest Size      : 32
  Hash Block Size       : 128
  Hash Digest           : b'\x8c4g\xe8\xa3\xf1\x96\xbd\xd1I<\xf6IY\x1f\xe1\x14\xff<\xccU\xee\x13Z\xd9\xae=\xe5Zb\x02J'
  Hash Hexdigest        : 8c3467e8a3f196bdd1493cf649591fe114ff3ccc55ee135ad9ae3de55a62024a
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import hashlib
from hashlib import blake2b #pylint: disable=E0611



# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    string      = "Hello World Hashlib!"
    stringBytes = bytearray(string, "utf-8")
    digestSize  = 32
    salt        = os.urandom(blake2b.SALT_SIZE)
    person      = b"Person1"
    h           = blake2b(digest_size=digestSize, salt=salt, person=person)

    h.update(stringBytes)
    print("Algorithms Guaranteed : {}".format(hashlib.algorithms_guaranteed))
    print("Algorithms Available  : {}".format(hashlib.algorithms_available))
    print("String                : {}".format(string))
    print("Hash                  : {}".format(h))
    print("Salt                  : {}".format(salt))
    print("Person                : {}".format(person))
    print("Hash Name             : {}".format(h.name))
    print("Hash Digest Size      : {}".format(h.digest_size))
    print("Hash Block Size       : {}".format(h.block_size))
    print("Hash Digest           : {}".format(h.digest()))
    print("Hash Hexdigest        : {}".format(h.hexdigest()))
