# Python Hmac Documentation

Module implements the HMAC algorithm as described in [RFC 2104](https://tools.ietf.org/html/rfc2104.html).

Relevant URLs:
[Documentation](https://docs.python.org/3.6/library/hmac.html)

## Methods

### Constructor: `hmac.new(key, msg=None, digestmod=None)`

Return a new hmac object. key is a bytes or bytearray object giving the secret key. If msg is
present, the method call update(msg) is made.

### `HMAC.update(msg)`

Update the hmac object with _msg_. Repeated calls are equivalent to a single call with the concatenation
of all the arguments: `m.update(a); m.update(b)` is equivalent to `m.update(a + b)`

### `HMAC.digest()`

Return the digest of the bytes bassed to the `update()` method so far. This bytes object will be the
same length as the _digest_size_ of the digest given to the constructor. It may contain non-ASCII
bytes, including NUL bytes.

### `HMAC.hexdigest()`

Like `digest()` except the digest is returned as a string twice the length containing only hexadecimal
digits. This may be used to exchange the value safely in email or other non-binary environments.

### `HMAC.copy()`

Return a copy ("clone") of the hmac object. This can be used to efficiently compute the digests of
strings that share a common initial substring.

### `HMAC.digest_size`

The size of the resulting HMAC digest in bytes

### `HMAC.block_size`

The internal block size of the hash algorithm in bytes.

### `HMAC.name`

The canonical name of this HMAC, always lowercase, e.g. `hmac-md5`
