#!/usr/bin/env python3.6
"""
NAME
 hmacHelloWorld.py

DESCRIPTION
 HMAC hello world
"""

# ----------------------------- IMPORTS ------------------------------------- #
import hmac
import hashlib


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    message          = "Hello World HMAC!"
    messageByteArray = bytearray(message, "utf-8")
    key              = "I am a random key!"
    keyByteArray     = bytearray(key, "utf-8")
    signature        = hmac.new(key=keyByteArray, msg=messageByteArray, digestmod=hashlib.sha1).digest()

    print("Message            : {}".format(message))
    print("Message Byte Array : {}".format(messageByteArray))
    print("Key                : {}".format(key))
    print("Key Byte Array     : {}".format(keyByteArray))
    print("Signature          : {}".format(signature))
