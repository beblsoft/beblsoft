#!/usr/bin/env python3
"""
 NAME
  processLock.py

 USAGE
  $ ./processLock.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process, Lock


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def func(l, i):
    """
    Process function
    """
    l.acquire()
    try:
        print("Hello world {}!".format(i))
    finally:
        l.release()

# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    lock  = Lock()
    pList = []
    for num in range(50):
        p = Process(target=func, args=(lock, num))
        p.start()
        pList.append(p)

    for p in pList:
        p.join()
