#!/usr/bin/env python3
"""
 NAME
  processPool.py

 DESCRIPTION
   Demonstrate Process Pool Usage

 USAGE
  $ ./processPool.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import time
import os
from multiprocessing import Pool, TimeoutError #pylint: disable=W0622


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def squared(x):
    """
    Return the number squared
    """
    return x * x


# ------------------------------- MAIN -------------------------------------- #
with Pool(processes=4) as pool:

    # print "[0, 1, 4,..., 81]"
    print(pool.map(squared, range(10)))

    # print same numbers in arbitrary order
    print(pool.imap_unordered(squared, range(10)))

    # evaluate "f(20)" asynchronously
    res = pool.apply_async(squared, (20,))  # runs in *only* one process
    print(res.get(timeout=1))               # prints "400"

    # evaluate "os.getpid()" asynchronously
    res = pool.apply_async(os.getpid, ())  # runs in *only* one process
    print(res.get(timeout=1))              # prints the PID of that process

    # launching multiple evaluations asynchronously *may* use more processes
    multiple_results = [pool.apply_async(os.getpid, ()) for i in range(4)]
    print([res.get(timeout=1) for res in multiple_results])

    # make a single worker sleep for 10 secs
    res = pool.apply_async(time.sleep, (10,))
    try:
        print(res.get(timeout=1))
    except TimeoutError:
        print("We lacked patience and got a multiprocessing. TimeoutError")

    print("For the moment, the pool remains available for more work")
