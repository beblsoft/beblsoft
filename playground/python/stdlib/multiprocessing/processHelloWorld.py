#!/usr/bin/env python3
"""
 NAME
  processHelloWorld.py

 USAGE
  $ ./processHelloWorld.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def helloWorld(name):
    """
    Hello World
    """
    print("Hello {}".format(name))


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    p = Process(target=helloWorld, args=("Bob",))
    p.start()
    p.join()
