#!/usr/bin/env python3
"""
 NAME
  processID.py

 USAGE
  $ ./processID.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from multiprocessing import Process


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def info():
    """
    Print process info
    """
    print("Module name: {}".format(__name__))
    print("Parent PID: {}".format(os.getppid()))
    print("PID: {}".format(os.getpid()))


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    p = Process(target=info, args=())
    p.start()
    p.join()
