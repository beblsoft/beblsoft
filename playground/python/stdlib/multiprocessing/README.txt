OVERVIEW
===============================================================================
- Python Multiprocessing Documentation

- Features --------------------------
  * Local and Remote Concurrency    : Side-step Global Interpreter lock for true parallelism on machine
  * Supported Environments          : Unix, Windows

- Key Classes -----------------------
  * Process                         : Process
  * Proces Pool                     : Pool
  * Shared communication            : Queues and Pipes
  * Synchronization                 : Lock
  * Shared Memory                   : Value, Array
  * Server Process                  : Manager

- URLs ------------------------------
  * Documentation                   : https://docs.python.org/3.6/library/multiprocessing.html




START METHODS
===============================================================================
- 3 ways to start process
  1 Spawn                           : Parent process starts fresh python interpreter process
                                      Default on windows
  2 Fork                            : Parent process uses os.fork() to fork the python interpreter
                                      Default on unix
  3 Fork server                     : Server process is started that forks child processes
- Set start method                  : multiprocessing.set_start_method('fork')