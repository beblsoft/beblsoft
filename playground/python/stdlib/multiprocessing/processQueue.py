#!/usr/bin/env python3
"""
 NAME
  processQueue.py

 USAGE
  $ ./processQueue.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process, Queue


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def func(queue):
    """
    Process function
    """
    queue.put([42, None, "Hello!"])


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    q = Queue()
    p = Process(target=func, args=(q,))
    p.start()
    print(q.get())
    p.join()
