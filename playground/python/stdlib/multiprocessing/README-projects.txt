OVERVIEW
===============================================================================
Python Multiprocessing Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- processHelloWorld.py ------------
  * Description                   : Spawn single process to print hello world
  * Run                           : ./processHelloWorld.py

- processID.py --------------------
  * Description                   : Have process print parent id, self id
  * Run                           : ./processID.py

- processLock.py ------------------
  * Description                   : Have multiple processes compete for lock
  * Run                           : ./processLock.py

- processPipe.py ------------------
  * Description                   : Have child process communicate with parent over pipe
  * Run                           : ./processPipe.py

- processPool.py ------------------
  * Description                   : Run multiple processes out of a pool
  * Run                           : ./processPool.py

- processQueue.py -----------------
  * Description                   : Have child processes put information on a queue
  * Run                           : ./processQueue.py

- processSharedMemory.py ----------
  * Description                   : Parent exports shared memory that child can use
  * Run                           : ./processSharedMemory.py

- processServerManager.py ---------
  * Description                   : Server manages shared data structures between children
  * Run                           : ./processServerManager.py