#!/usr/bin/env python3
"""
 NAME
  processServerManager.py

 USAGE
  $ ./processServerManager.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process, Manager


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def func(d, l):
    """
    Process function
    """
    d[1] = '1'
    d['2'] = 2
    d[0.25] = None
    l.reverse()

# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    with Manager() as manager:
        mDict = manager.dict()
        mList = manager.list(range(10))

        p = Process(target=func, args=(mDict, mList))
        p.start()
        p.join()

        print(mDict)
        print(mList)
