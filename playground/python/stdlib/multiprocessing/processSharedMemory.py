#!/usr/bin/env python3
"""
 NAME
  processSharedMemory.py

 USAGE
  $ ./processSharedMemory.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process, Value, Array


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def func(n, a):
    """
    Process function
    """
    n.value = 3.1415927
    for i in range(len(a)):
        a[i] = -a[i]


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    num = Value('d', 0.0)
    arr = Array('i', range(10))

    p = Process(target=func, args=(num, arr))
    p.start()
    p.join()

    print(num.value) # 3.1415927
    print(arr[:])    # [0, -1, -2, -3, -4, -5, -6, -7, -8, -9]
