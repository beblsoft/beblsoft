#!/usr/bin/env python3
"""
 NAME
  processPipe.py

 USAGE
  $ ./processPipe.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from multiprocessing import Process, Pipe


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def func(conn):
    """
    Process function
    """
    conn.send([42, None, "Hello!"])
    conn.close()


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    parentConn, childConn = Pipe()
    p = Process(target=func, args=(childConn,))
    p.start()
    print(parentConn.recv())
    p.join()
