#!/usr/bin/env python3
"""
NAME
 patchDict.py

DESCRIPTION
 Patch a dictionary
"""

# ----------------------------- IMPORTS ------------------------------------- #
from unittest.mock import patch, MagicMock
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- EXAMPLES ------------------------------------ #
def patchDict():  # pylint: disable=C0111
    """
    Patch a simple dictionary
    """
    printTitle("patchDict")
    d = {}
    with patch.dict(d, {'key': 'value'}):
        print(d)
    print(d)


def patchInsertModule():  # pylint: disable=C0111
    """
    Patch sys modules dictionary to add a new module
    Then use the mocked module
    """
    printTitle("patchInsertModule")
    myModule = MagicMock()
    myModule.function.return_value = "fish"
    with patch.dict("sys.modules", myModule=myModule):
        import myModule  # pylint: disable=E0401
        print(myModule.function("some", "args"))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    patchDict()
    patchInsertModule()
