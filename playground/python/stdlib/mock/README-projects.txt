OVERVIEW
===============================================================================
Python unittest.mock Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- mockFunction.py -----------------
  * Description                   : Demonstrate mocking a function
  * Run                           : ./mockFunction.py

- patchDict.py --------------------
  * Description                   : Demonstrate patching a dictionary. Allows instance properties to be changed.
  * Run                           : ./patchDict.py

- patchObject.py -----------------
  * Description                   : Demonstrate patching object methods, class attributes, and instance attributes
  * Run                           : ./patchObject.py

- patchTest.py -----------------
  * Description                   : Demonstrate using patch in a unittest
  * Run                           : ./patchTest.py

- patchUsage.py -----------------
  * Description                   : Demonstrate the different ways to use patch: start/stop,
                                    decorator, and context manager
  * Run                           : ./patchUsage.py
