#!/usr/bin/env python3
"""
NAME
 patchUsage.py

DESCRIPTION
 Demonstrate the different ways to patch
"""

# ----------------------------- IMPORTS ------------------------------------- #
from unittest.mock import patch
from base.bebl.python.print.bTitle import printTitle



# ----------------------------- FOO CLASS ----------------------------------- #
class Foo():  # pylint: disable=C0111

    def method(self):  # pylint: disable=R0201
        return 1


# ----------------------------- GLOBALS ------------------------------------- #
f = Foo()


# ----------------------------- EXAMPLES ------------------------------------ #
def patchStartStop():
    """
    Setup patch context with start and stop
    See patchTest.py for usage with unittests
    """
    printTitle("patchStartStop")
    patcher = patch.object(Foo, attribute='method', autospec=True, return_value=2)
    mockClass = patcher.start()  # pylint: disable=W0612
    print(f.method())
    patcher.stop()


@patch.object(Foo, attribute='method', autospec=True, return_value=2)
def patchDecorator(mockClass):  # pylint: disable=W0613
    """
    Setup patch context with function decorator
    """
    printTitle("patchDecorator")
    print(f.method())


def patchWith():
    """
    Setup patch context with context manager
    """
    printTitle("patchDecorator")
    with patch.object(Foo, attribute='method', autospec=True, return_value=2) as mockClass: #pylint: disable=W0612
        print(f.method())


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    patchStartStop()
    patchDecorator() #pylint: disable=E1120
    patchWith()
