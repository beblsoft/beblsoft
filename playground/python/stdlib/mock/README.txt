OVERVIEW
===============================================================================
- Python unittest.mock Documentation
- Description                       : unittest.mock is a library for testing in Python
                                    : It allows you to replace parts of your system under test with mock
                                      objects that make assertions about how they have been used
                                    : Mock provides a patch() decorator that handles patching module
                                      and class level attributes withint the scope of a test, along
                                      with sentinel for creating unique objects
- URLS
 * Documentation                    : https://docs.python.org/3.6/library/unittest.mock.html



CLASSES
===============================================================================
- Mock                              : Flexible object intended to replace the use of stubs and test doubles
                                      throughout code
                                    : Record how they are used, allowing assertions after code is done
- NonCallableMock                   : Non callable subclass of Mock
- MagicMock                         : Subclass of Mock with default implementations for most of magic methods
                                      Magic method examples: __dir__, __format__, ...
- MagicMock                         : Non callable subclass of MagicMock
- PropertyMock                      : Mock intended to be used as a property or other descriptor on a class



MOCK CLASS
===============================================================================
- Constructor                       : class unittest.mock.Mock(spec=None, side_effect=None, return_value=DEFAULT,
                                                               wraps=None, name=None, spec_set=None, unsafe=False,
                                                               **kwargs)
  * spec                            : List of strings or existing object that acts as specification for
                                      mock object
  * spec_set                        : Stricter variant of spec. If used, attempting to set or get an
                                      attribute on the mock will raise an AttributeError
  * side_effect                     : Function to call whenever mock is called, exception to throw,
                                      iterable to work through
                                      Cleared by setting to none
  * wraps                           : Item for the mock object to wrap
  * name                            : Mock name to be used for repr
  * **kwargs                        : Arbitrary keyword arguments for configuration, attached as attributes
                                     to the mock
- Methods
  * assert_called
  * assert_called_once
  * assert_called_with
  * assert_called_once_with
  * assert_any_call
  * assert_has_calls
  * assert_not_called
  * reset_mock                      : Reset call attributes on mock
  * mock_add_spec
  * attach_mock
  * configure_mock                  : Set attributes through keyword arguments
- Properties
  * called
  * call_count
  * return_value
  * side_effect
  * call_args
  * call_args_list
  * method_calls                    : Track calls to methods and attributes
  * mock_calls                      : Record all calls to mock object



PATCH FUNCTION
===============================================================================
- Invocation                        : mock.patch(target, new=DEFAULT, spec=None, create=False, spec_set=None,
                                                 autospec=None, new_callable=None, **kwargs)
- Descrption                        : Acts as a function decorator, class decorator, or context manager
                                    : Inside the body, target is patched with a new object
- Decorator example                 : @patch('__main__.SomeClass')
                                      def function(normal_argument, mock_class):
                                          print(mock_class is SomeClass)
- Context manager example           : with patch('__main__.Class') as MockClass:
                                          instance = MockClass.return_value
                                          instance.method.return_value = 'foo'
                                          assert Class() is instance
                                          assert Class().method() == 'foo'



PATCH OBJECT FUNCTION
===============================================================================
- Invocation                        : patch.object(target, attribute, new=DEFAULT, spec=None, create=False,
                                                   spec_set=None, autospec=None, new_callable=None, **kwargs)
- Description                       : Patch the named member (attribue) on an object (target) with a mock
                                      object
- Context manager example           : with patch.object(SomeClass, 'class_method') as mock_method:
                                          SomeClass.class_method(3)
                                          mock_method.assert_called_with(3)

