#!/usr/bin/env python3
"""
NAME
 mockFunction.py

DESCRIPTION
 Mock function

BIBLIOGRAPHY
 https://docs.python.org/3.6/library/unittest.mock.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
from unittest.mock import MagicMock
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- HELPERS ------------------------------------- #
class Foo():  # pylint: disable=C0111

    def method(self):  # pylint: disable=R0201
        return 1


# ----------------------------- EXAMPLES ------------------------------------ #
def mockReturnValue():
    """
    Mock a function return value
    """
    printTitle("mockFuncReturnValue")
    f        = Foo()
    f.method = MagicMock(return_value=2)
    rval     = f.method()
    f.method.assert_called()
    print("method rval={}".format(rval))


def mockThrowException():
    """
    Mock function to throw exception
    """
    printTitle("mockFuncThrowException")
    f        = Foo()
    f.method = MagicMock(side_effect=KeyError)
    try:
        f.method()
    except KeyError as _:
        print("Caught exception")


def mockSequentialReturnValues():
    """
    Mock a function sequential return values
    """
    printTitle("mockSequentialReturnValues")
    f            = Foo()
    rvalList     = [5, 4, 3, 2, 1]
    f.method     = MagicMock(side_effect=rvalList)
    for _ in rvalList:
        rval     = f.method()
        print("method rval={}".format(rval))


def mockReplaceWithNewFunction():
    """
    Mock a function by replacing it with a new function
    """
    printTitle("mockReplaceWithNewFunction")
    f        = Foo()
    f.method = MagicMock(side_effect=lambda a:5)
    rval     = f.method()
    f.method.assert_called()
    print("method rval={}".format(rval))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    mockReturnValue()
    mockThrowException()
    mockSequentialReturnValues()
    mockReplaceWithNewFunction()
