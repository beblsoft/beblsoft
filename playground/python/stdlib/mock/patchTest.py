#!/usr/bin/env python3
"""
NAME
 patchTest.py

DESCRIPTION
 Patch during a unit test
"""

# ----------------------------- IMPORTS ------------------------------------- #
import unittest
from unittest.mock import patch


# ----------------------------- FOO CLASS ----------------------------------- #
class Foo():  # pylint: disable=C0111
    def method(self):  # pylint: disable=R0201
        return 1


# ----------------------------- GLOBALS ------------------------------------- #
f = Foo()


# ----------------------------- TEST CASE ----------------------------------- #
class MyTest(unittest.TestCase): #pylint: disable=C0111
    def setUp(self):
        patcher        = patch.object(Foo, attribute='method', autospec=True, return_value=2)
        self.mockClass = patcher.start()
        self.addCleanup(patcher.stop) # Run after tearDown, stops mocking

    def test_mockClass(self):
        """
        Test mock class
        """
        self.assertEqual(f.method(), 2)



# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    unittest.main()
