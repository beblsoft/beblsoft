#!/usr/bin/env python3
"""
NAME
 patchObject.py

DESCRIPTION
 Patch an object
"""

# ----------------------------- IMPORTS ------------------------------------- #
from unittest.mock import patch, PropertyMock
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- FOO CLASS ----------------------------------- #
class Foo():  # pylint: disable=C0111
    cVal = 1

    def __init__(self):
        self.iVal = 1

    def method(self):  # pylint: disable=R0201
        return 1


# ----------------------------- GLOBALS ------------------------------------- #
f = Foo()


# ----------------------------- EXAMPLES ------------------------------------ #
def patchMethod():
    """
    Patch an object method
    """
    printTitle("patchMethod")

    def fakeMethod(self):  # pylint: disable=W0613,C0111
        return 2

    with patch.object(Foo, attribute='method', new=fakeMethod) as _:
        print(f.method())
    print(f.method())


def patchMethodAutoSpec():
    """
    Patch an object method using autospec
    """
    printTitle("patchMethodAutoSpec")
    with patch.object(Foo, attribute='method', autospec=True, return_value=2) as _:
        print(f.method())
    print(f.method())


@patch.object(Foo, attribute="method", autospec=True, return_value=2)
def patchMethodWithAutoSpecDecorator(_):
    """
    Patch object with autospec and decorator
    """
    printTitle("patchMethodWithAutoSpecDecorator")
    print(f.method())


def patchClassAttribute():
    """
    Patch a class attribute
    """
    printTitle("patchClassAttribute")
    with patch.object(Foo, attribute='cVal', new_callable=PropertyMock, return_value=2) as _:
        print(f.cVal)
    print(f.cVal)


def patchInstanceAttribute():
    """
    Patch an instance attribute
    """
    printTitle("patchInstanceAttribute")
    with patch.dict(f.__dict__, iVal=2):
        print(f.iVal)
    print(f.iVal)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    patchMethod()
    patchMethodAutoSpec()
    patchMethodWithAutoSpecDecorator() #pylint: disable=E1120
    patchClassAttribute()
    patchInstanceAttribute()
