#!/usr/bin/env python3
"""
NAME
 threadTimer.py

USAGE
 $ ./threadTimer.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import time
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func():
    """
    Thread Function
    """
    logging.debug("Timer complete!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    t1 = threading.Timer(2, func)
    t1.setName('t1')
    t2 = threading.Timer(3, func)
    t2.setName('t2')

    logging.debug("Starting timers")
    t1.start()
    t2.start()

    time.sleep(2)
    logging.debug("Canceling t2")
    t2.cancel()
    logging.debug("Done")
