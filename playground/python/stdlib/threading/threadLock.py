#!/usr/bin/env python3
"""
NAME
 threadLock.py

USAGE
 $ ./threadLock.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import time
import random
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- CLASSES ------------------------------------- #
class Counter():
    """
    Thread safe counter
    """

    def __init__(self, start=0):
        self.lock  = threading.Lock()
        self.value = start

    def increment(self):
        """
        Increment counter
        """
        self.lock.acquire()
        try:
            self.value = self.value + 1
            logging.debug("Incremented: {}".format(self.value))
        finally:
            self.lock.release()


# ----------------------------- FUNCTIONS ----------------------------------- #
def func(c, nIncs=5):
    """
    Thread function
    """
    for _ in range(nIncs):
        pauseS = random.random()
        time.sleep(pauseS)
        c.increment()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    counter  = Counter()
    nThreads = 5
    tList    = []

    for i in range(nThreads):
        t = threading.Thread(target=func, args=(counter,))
        t.start()
        tList.append(t)

    for t in tList:
        t.join()
