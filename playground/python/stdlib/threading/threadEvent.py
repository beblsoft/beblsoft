#!/usr/bin/env python3
"""
NAME
 threadEvent.py

USAGE
 $ ./threadEvent.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import time
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def block(e):
    """
    Block waiting for event
    """
    rval = e.wait()
    logging.debug("Event is set:{}".format(rval))


def nonBlock(e, t):
    """
    Wait t seconds for event to be set
    Args
      e:
        Event
      t:
        Time in seconds to wait for event
    """
    while not e.isSet():
        rval = e.wait(t)
        if rval:
            logging.debug("Event was set, exiting")
            break
        else:
            logging.debug("Event not set. Doing other work...")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    event = threading.Event()
    t1 = threading.Thread(name="block", target=block, args=(event,))
    t2 = threading.Thread(name="nonBlock", target=nonBlock, args=(event, 1))

    logging.debug("Starting threads")
    t1.start()
    t2.start()

    time.sleep(3)
    logging.debug("Setting event")
    event.set()

    t1.join()
    t2.join()
    logging.debug("Threads reaped")
