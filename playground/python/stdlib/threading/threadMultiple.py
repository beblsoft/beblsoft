#!/usr/bin/env python3
"""
NAME
 threadMultiple.py

USAGE
 $ ./threadMultiple.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func(num):
    """
    Thread Function
    """
    logging.debug("Thread num {}".format(num))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    tList = []

    for i in range(10):
        t = threading.Thread(name="Thread{}".format(i), target=func, args=(i,))
        tList.append(t)

    for t in tList:
        t.start()

    for t in tList:
        t.join()
