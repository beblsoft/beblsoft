#!/usr/bin/env python3
"""
NAME
 threadDaemon.py

USAGE
 $ ./threadDaemon.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import time
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func():
    """
    Thread Function
    """
    logging.debug("Starting")
    counter = 0
    while counter < 2:
        time.sleep(1)
        logging.debug("Counter={}".format(counter))
        counter += 1
    logging.debug("Exiting")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    logging.debug("Starting")
    d = threading.Thread(name="Daemon", target=func)
    d.setDaemon(True)
    d.start()
    d.join()
    logging.debug("Exiting")
