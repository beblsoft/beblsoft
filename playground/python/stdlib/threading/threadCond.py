#!/usr/bin/env python3
"""
NAME
 threadCond.py

USAGE
 $ ./threadCond.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def producer(cond): #pylint: disable=W0621
    """
    Producer
    """
    with cond:
        logging.debug("Notifying all.")
        cond.notifyAll()

def consumer(cond): #pylint: disable=W0621
    """
    Consumer
    """
    with cond:
        logging.debug("Waiting for resources.")
        cond.wait()
        logging.debug("Processing resources.")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    cond      = threading.Condition()
    nCThreads = 2
    cList     = []

    for i in range(nCThreads):
        c = threading.Thread(target=consumer, name="Consumer{}".format(i), args=(cond,))
        c.start()
        cList.append(c)

    p = threading.Thread(target=producer, name="Producer", args=(cond,))
    p.start()

    for c in cList:
        c.join()
    p.join()
