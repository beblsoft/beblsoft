#!/usr/bin/env python3
"""
NAME
 threadEnumerate.py

USAGE
 $ ./threadEnumerate.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import random
import time
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func():
    """
    Thread Function
    """
    sleepS = random.randint(1, 5)
    logging.debug("Sleeping for {} seconds".format(sleepS))
    time.sleep(sleepS)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    for i in range(3):
        t = threading.Thread(target=func)
        t.start()

    mainThread = threading.currentThread()
    for t in threading.enumerate():
        if t is mainThread:
            continue
        logging.debug("Joining {}".format(t.getName()))
        t.join()
