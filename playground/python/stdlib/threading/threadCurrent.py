#!/usr/bin/env python3
"""
NAME
 threadCurrent.py

USAGE
 $ ./threadMultiple.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func():
    """
    Thread Function
    """
    logging.debug("Thread ID        : {}".format(threading.currentThread().ident))
    logging.debug("Thread name      : {}".format(threading.currentThread().getName()))
    logging.debug("Thread is alive  : {}".format(threading.currentThread().is_alive()))
    logging.debug("Thread is daemon : {}".format(threading.currentThread().daemon))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    t = threading.Thread(name="ExampleThread", target=func)
    t.start()
    t.join()
