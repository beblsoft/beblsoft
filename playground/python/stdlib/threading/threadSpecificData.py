#!/usr/bin/env python3
"""
NAME
 threadSpecificData.py

USAGE
 $ ./threadSpecificData.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import random
import logging
import threading


# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def showValue(data):
    """
    Log data
    """
    try:
        value = data.value
    except AttributeError:
        logging.debug("No value yet")
    else:
        logging.debug("Value={}".format(value))


def func(data):
    """
    Thread function
    """
    showValue(data)
    data.value = random.randint(1, 100)
    showValue(data)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    localData = threading.local()
    nThreads  = 5
    tList     = []

    for i in range(nThreads):
        t = threading.Thread(target=func, args=(localData,))
        tList.append(t)

    showValue(localData)
    localData.value = 100
    showValue(localData)

    for t in tList:
        t.start()

    for t in tList:
        t.join()
