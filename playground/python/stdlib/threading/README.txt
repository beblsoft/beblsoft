OVERVIEW
===============================================================================
- Python Threading Documentation
- Description                       : Module constructs higher-level threading interfaces on top
                                      of the lower level _thread module
                                    : The Python Global Interpreter Lock prevents threads from
                                      truly running concurrently.
                                      To sidestep this, use multiprocessing
- URLS
 * Documentation                    : https://docs.python.org/3.6/library/threading.html


TERMS
===============================================================================
- Regular Thread                    : Main thread automatically waits for all regular threads
                                      to complete before exiting
- Daemon Thread                     : Runs without blocking the main program from exiting
                                      Must call thread.join() to wait for daemon to exit