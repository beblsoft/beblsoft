#!/usr/bin/env python3
"""
NAME
 threadSimple.py

DESCRIPTION
  Demonstrates use of simple threading, second example with logging

USAGE
 $ ./threadSimple.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading
import time

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )


# ----------------------------- FUNCTIONS ----------------------------------- #
def worker():
    """
    Worker Function
    """
    logging.debug('Starting')
    time.sleep(2)
    logging.debug('Exiting')


def service():
    """
    Service Function
    """
    logging.debug('Starting')
    time.sleep(3)
    logging.debug('Exiting')


# ----------------------------- MAIN ---------------------------------------- #
t = threading.Thread(name='service', target=service)
w = threading.Thread(name='worker', target=worker)
w2 = threading.Thread(target=worker)  # use default name

w.start()
w2.start()
t.start()
