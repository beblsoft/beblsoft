#!/usr/bin/env python3
"""
NAME
 threadSubclass.py

USAGE
 $ ./threadSubclass.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- CLASSES ------------------------------------- #
class MyThread(threading.Thread):
    """
    Thread subclass
    """

    def __init__(self, name=None, args=(), kwargs=None):
        threading.Thread.__init__(self, group=None, name=name)
        self.args   = args
        self.kwargs = kwargs

    def run(self):
        logging.debug("Running with args=[{}] and kwargs=[{}]".format(
            self.args, self.kwargs))

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    t = MyThread(args=(1, 0, 2), kwargs={'a': 'A', 'b': 'B'})
    t.start()
    t.join()
