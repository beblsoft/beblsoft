#!/usr/bin/env python3
"""
NAME
 threadStacks.py

USAGE
 $ ./threadStacks.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback
import time
import logging
import threading


# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def a(): b()  # pylint: disable=C0321,C0111


def b(): c()  # pylint: disable=C0321,C0111


def c(): d()  # pylint: disable=C0321,C0111


def d(): e()  # pylint: disable=C0321,C0111


def e(nIters=2, sleepS=1):  # pylint: disable=C0111
    j = 0
    while j < nIters:
        time.sleep(sleepS)
        j += 1


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    localData = threading.local()
    nThreads  = 1
    tList     = []

    for i in range(nThreads):
        t = threading.Thread(target=a)
        tList.append(t)

    for t in tList:
        t.start()

    logging.debug("Dumping thread stacks")
    for threadID, frame in sys._current_frames().items():  # pylint: disable=W0212
        logging.debug("\n")
        logging.debug("ThreadID: {}".format(threadID))
        for filename, lineno, name, line in traceback.extract_stack(frame):
            logging.debug("File: {}, line: {}, name: {}".format(filename, lineno, name))
            logging.debug("\t{}".format(line))

    for t in tList:
        t.join()
