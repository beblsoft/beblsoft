#!/usr/bin/env python3
"""
 NAME
  threadPool.py

 DESCRIPTION
   Demonstrate Thread Pool Usage

 USAGE
  $ ./threadPool.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from concurrent.futures import ThreadPoolExecutor


# ------------------------------- HELPER FUNCTIONS -------------------------- #
def squared(x):
    """
    Return the number squared
    """
    rval = x * x
    print(rval)
    return rval


# ------------------------------- MAIN -------------------------------------- #
with ThreadPoolExecutor(max_workers=10) as executor:
    for i in range(0, 10):
        executor.submit(squared, i)
