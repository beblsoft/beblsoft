#!/usr/bin/env python3
"""
NAME
 threadWithLock.py

USAGE
 $ ./threadWithLock.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func(l):
    """
    Thread function
    """
    with l:
        logging.debug("Lock acquired via with")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    lock     = threading.Lock()
    nThreads = 5
    tList    = []

    for i in range(nThreads):
        t = threading.Thread(target=func, args=(lock,))
        t.start()
        tList.append(t)

    for t in tList:
        t.join()
