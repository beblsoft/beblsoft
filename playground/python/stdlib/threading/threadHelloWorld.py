#!/usr/bin/env python3
"""
NAME
 threadHelloWorld.py

USAGE
 $ ./threadHelloWorld.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import threading

# ----------------------------- GLOBALS ------------------------------------- #
logging.basicConfig(level=logging.DEBUG,
                    format="[%(levelname)s] (%(threadName)-10s) %(message)s")


# ----------------------------- FUNCTIONS ----------------------------------- #
def func(name):
    """
    Thread Function
    """
    logging.debug("Hello World {}".format(name))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    t = threading.Thread(name="HelloWorldThread",
                         target=func, kwargs={"name": "Bob"})
    t.start()
    t.join()
