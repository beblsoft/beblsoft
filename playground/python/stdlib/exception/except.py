#!/usr/bin/python3
#
# NAME
#  except.py
#
# DESCRIPTION
#  Highlights Python exception facilities
#
# USAGE
#  ./except.py
#


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import traceback


# ----------------------------- FUNCTIONS ----------------------------------- #
def a():
    b()


def b():
    c()


def c():
    d()


def d():
    try:
        y = 1 / 0
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print("-------------------------------------------------")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout)
        print("-------------------------------------------------")
        traceback.print_stack()
    else:
        print("y={}".format(y))
    finally:
        print("Finally")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    a()
