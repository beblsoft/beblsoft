#!/usr/bin/env python3
#
# NAME
#  fullSlice.py
#
# DESCRIPTION
#  Demo of exception processing
#

# ----------------------------- IMPORTS ------------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.decorators.bDecorators import exceptFunc
from .bExceptions import BException, BErrorCodes


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)


# ----------------------------- FUNCTIONS------------------------------------ #
@logFunc()
@exceptFunc
def testDivideByZero():
    """
    Force error, divide by zero
    """
    try:
        x = 1 / 0
    except Exception as e:
        logger.warn("Caught Divide by Zero")
        raise BException(BErrorCodes.DIVIDE_BY_ZERO, originalException=e)


# ----------------------------- TEST DRIVER---------------------------------- #
if __name__ == "__main__":
    """
    Basic Test Driver for fullSlice.py
    """
    testDivideByZero()
