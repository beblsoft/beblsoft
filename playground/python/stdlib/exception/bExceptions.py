#!/usr/bin/env python3
#
# NAME
# 	bExceptions.py
#
# DESCRIPTION
#  Definition of Beblsoft Exceptions
#
# ----------------------------- IMPORTS ------------------------------------- #
from enum import Enum


# ----------------------------- CLASSES ------------------------------------- #
class BErrorCodes(Enum):
    DEFAULT_ERROR = 1000
    DIVIDE_BY_ZERO = 1001
    LP_UNABLE_TO_GET_PASSWORD = 1002
    LP_CANNOT_LOGIN = 1003
    LP_CANNOT_LOGOUT = 1004
    LP_NOT_LOGGED_IN = 1005
    LP_UNKNOWN_ACCOUNT = 1006
    MS_CANNOT_LOGIN = 1007
    MS_CANNOT_LOGOUT = 1008
    MS_NOT_LOGGED_IN = 1009
    MS_UNABLE_TO_SEND_EMAIL = 1010


def bErrorToText(code):
    errorToEnglishMap = {
        BErrorCodes.DEFAULT_ERROR: "Default  Error",
        BErrorCodes.DIVIDE_BY_ZERO: "Attempt to Divide By Zero",
        BErrorCodes.LP_UNABLE_TO_GET_PASSWORD: "Unable to get account password",
        BErrorCodes.LP_CANNOT_LOGIN: "Unable to login to LastPass",
        BErrorCodes.LP_CANNOT_LOGOUT: "Unable to log out of LastPass",
        BErrorCodes.LP_NOT_LOGGED_IN: "Must login prior to accessing accounts",
        BErrorCodes.LP_UNKNOWN_ACCOUNT: "Account is unknown to LastPass object",
        BErrorCodes.MS_CANNOT_LOGIN: "Unable to login to Email Server",
        BErrorCodes.MS_CANNOT_LOGOUT: "Unable to logout of Email Server",
        BErrorCodes.MS_NOT_LOGGED_IN: "Not logged into Email Server",
        BErrorCodes.MS_UNABLE_TO_SEND_EMAIL: "Unable to send email"
    }
    if code not in errorToEnglishMap.keys():
        return "Error code not found"
    else:
        return errorToEnglishMap[code]


# ----------------------------- EXCEPTIONS ---------------------------------- #
class BException(Exception):
    """ Base Beblsoft exception """

    def __init__(self, errorCode=BErrorCodes.DEFAULT_ERROR, originalException=None):
    	"""
    	Initialize exception
    	Args
    	  errorCode: BErrorCodes enumeration
    	  originalException: the original exception that caused this to be raised
    	"""
    	super().__init__(self)
    	self.code = errorCode
    	self.text = bErrorToText(errorCode)

    def __str__(self):
        return "Beblsoft Exception: Code: {}, Text: '{}'".format(self.code, self.text)


class BExceptionWithText(BException):
    """ Base Beblsoft exception with text """

    def __init__(self, msg="", errorCode=BErrorCodes.DEFAULT_ERROR, originalException=None):
    	"""
    	Initialize exception
    	Args
    	  msg: context specific message
    	  see BException for last two arguments
    	"""
    	BException.__init__(self, errorCode, originalException)
    	self.msg = msg

    def __str__(self):
        return "Beblsoft Exception: Code: {}, Text: '{}', Msg: '{}'".format(self.code, self.text, self.msg)
