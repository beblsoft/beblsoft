OVERVIEW
===============================================================================
- Click Documentation
- Description                       : Click is a Python package for creating beautiful command
                                      line interfaces in a compossable way with as little
                                      code as necessary
- 3 Points                          1 Arbitrary nesting of commands
                                    2 Automatic help page generation
                                    3 Lazy loading of subcommands at runtime
- Relevant URLS ---------------------
 * Home                             : https://click.palletsprojects.com/en/7.x/



INSTALLATION
===============================================================================
- Install                           : pip3 install click
- Uninstall                         : pip3 uninstall click
