#!/usr/bin/env python3
"""
 NAME
  argsOpts.py

 DESCRIPTION
  Demonstrate click arguments and options
"""


# ------------------------------- IMPORTS ----------------------------------- #
import click


# ------------------------------- FUNCTIONS --------------------------------- #
@click.command()
@click.option("--count", default=1, help="number of greetings")
@click.argument("name")
def hello(count, name):
    """
    Simple program that greets NAME for a total of COUNT times
    """
    for _ in range(count):
        click.echo("Hello {}!".format(name))


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    hello()  # pylint: disable=E1120
