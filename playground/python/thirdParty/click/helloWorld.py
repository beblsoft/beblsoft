#!/usr/bin/env python3
"""
 NAME
  helloWorld.py

 DESCRIPTION
  Hello World Program for Click
"""

# ------------------------------- IMPORTS ----------------------------------- #
import click


# ------------------------------- FUNCTIONS --------------------------------- #
@click.command()
def hello():
    """
    Hello Command
    """
    click.echo("Hello World")


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    hello()
