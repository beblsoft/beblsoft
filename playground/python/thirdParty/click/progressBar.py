#!/usr/bin/env python3
"""
 NAME
  progressBar.py

 DESCRIPTION
  Demonstrate progress bar
"""


# ------------------------------- IMPORTS ----------------------------------- #
import click


# ------------------------------- COMMANDS ---------------------------------- #
@click.command()
def progress():
    """
    Display a progress bar
    """
    length   = 1000000
    demoList = list(range(length))

    with click.progressbar(length=length, label="Progress") as pBar:
        for _ in demoList:
            pBar.update(1)

# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    progress()
