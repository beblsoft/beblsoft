OVERVIEW
===============================================================================
Click Samples Documentation



SAMPLES
===============================================================================
- Description --------------------: Different click samples
- Setup ---------------------------
  * Setup venv                    : virtualenv -p /usr/bin/python3.6 /tmp/clickVenv
  * Enter venv                    : source  /tmp/clickVenv/bin/activate
  * Install requirements          : pip3 install -r requirements.txt

- Hello World ---------------------
  * Description                   : Click hello world program
  * Run                           : ./helloWorld.py

- Arguments, Options --------------
  * Description                   : Demonstrate usage of arguments and options
  * Help                          : ./argsOpts.py --help
  * Run specifying name           : ./argsOpts.py steve
  * Run specifying count, name    : ./argsOpts.py --count=3 steve

- Nesting -------------------------
  * Description                   : Example nesting commands in a group
  * Help                          : ./nesting.py --help
  * Call initdb                   : ./nesting.py initdb
  * Call dropdb                   : ./nesting.py dropdb

- Progress Bar --------------------
  * Description                   : Example using a progress bar
  * Run                           : ./progressBar.py