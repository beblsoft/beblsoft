#!/usr/bin/env python3
"""
 NAME
  nesting.py

 DESCRIPTION
  Demonstrate click nesting commands
"""


# ------------------------------- IMPORTS ----------------------------------- #
import click


# ------------------------------- GROUPS ------------------------------------ #
@click.group()
def cli(): #pylint: disable=C0111
    pass


# ------------------------------- COMMANDS ---------------------------------- #
@cli.command()
def initdb():
    """
    Initialize database
    """
    click.echo("Initialized the database")


@cli.command()
def dropdb():
    """
    Drop database
    """
    click.echo("Dropped the database")


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    cli()
