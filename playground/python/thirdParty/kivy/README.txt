OVERVIEW
===============================================================================
- Kivy Documentation
- Description                       : Kivy is an open source ppython library for rapid development
                                      of applications that make use of innovative user interfaces, such
                                      as multi-touch applications
- Relevant URLs ---------------------
 * Home                             : https://kivy.org/#home
 * Documentation                    : https://kivy.org/doc/stable/gettingstarted/intro.html



INSTALLATION
===============================================================================
- Install                           : sudo add-apt-repository ppa:kivy-team/kivy     # Add kivy repo
                                      sudo apt-get update                            # Update packages
                                      sudo apt-get install -y python3-kivy           # Sources
                                      sudo apt-get install -y python-kivy-examples   # Examples
