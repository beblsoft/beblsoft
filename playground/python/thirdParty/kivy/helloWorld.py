#!/usr/bin/python3
"""
 NAME
  kivy_demo.py

 DESCRIPTION
  Program is a quick demo of kivy
"""


# ----------------------------- IMPORTS ------------------------------------- #
from kivy.app import App
from kivy.uix.button import Button


# ----------------------------- CLASSES ------------------------------------- #
class TestApp(App):
    """
    Test Application
    """

    def build(self):
        """
        Build
        """
        return Button(text='Hello World')


# ----------------------------- MAIN ---------------------------------------- #
TestApp().run()
