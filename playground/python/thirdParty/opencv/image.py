#!/usr/bin/env python3
"""
 NAME
  image.py

 DESCRIPTION
  Program to demonstrate basic openCV functionality.
  Eg. open, closing, manipulating picture matricies.

  Python OpenCV Tutorial:
  http://docs.opencv.org/trunk/doc/py_tutorials/py_tutorials.html
"""


# ----------------------------- IMPORTS ------------------------------------- #
import os
import cv2


# ----------------------------- FUNCTIONS ----------------------------------- #
def img_openDisplaySave(imgPath, display=True):
    """
    Open, display and save image
    Read Image
    flags:
      cv2.IMREAD_COLOR     : Loads a color image.
                             Any transparency of image will be neglected.
                             It is the default flag.
      cv2.IMREAD_GRAYSCALE : Loads image in grayscale mode
      cv2.IMREAD_UNCHANGED : Loads image as such including alpha channel
    """
    img = cv2.imread(imgPath, cv2.IMREAD_COLOR)
    #Display Image
    if(display):
        cv2.imshow('Boat Picture', img)
        cv2.waitKey(3000) # 3 seconds
        cv2.destroyAllWindows()
    #Write Image
    cv2.imwrite(imgPath, img)

def img_printStats(imgPath):
    """
    Print image statistics
    """
    img = cv2.imread(imgPath, cv2.IMREAD_COLOR)
    print("Image Shape: " + str(img.shape))
    print("Image Size: "  + str(img.size))
    print("Image Type: "  + str(img.dtype))



# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    inDir  = os.path.dirname(os.path.realpath(__file__))
    inFile = inDir + "/boat.jpg"
    outDir = inDir + "/partition"
    img_openDisplaySave(inFile, display=True)
    img_printStats(inFile)
