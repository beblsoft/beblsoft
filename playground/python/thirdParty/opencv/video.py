#!/usr/bin/env python3
"""
 NAME
  video.py

 DESCRIPTION
  Program to demonstrate basic openCV video functionality

  Code from Tutorial:
  http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html#display-video
"""


# ----------------------------- IMPORTS ------------------------------------- #
import numpy as np
import cv2
import time


# ----------------------------- FUNCTIONS ----------------------------------- #
def takeVideo():
    cap = cv2.VideoCapture(0)

    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


def saveVideo(outputVideo):
    cap = cv2.VideoCapture(0)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(outputVideo, fourcc, 20.0, (640,480))

    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret==True:
            # write the flipped frame
            out.write(frame)

            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()


def watchVideo(inputVideo):
    cap = cv2.VideoCapture(inputVideo)

    while(cap.isOpened()):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == '__main__':
    videoName = 'output.avi'
    saveVideo(videoName)
    time.sleep(2)
    watchVideo(videoName)
