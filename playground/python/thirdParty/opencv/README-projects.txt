OVERVIEW
===============================================================================
OpenCV Samples Documentation



SINGLE FILE SAMPLES
===============================================================================
- Description --------------------: Single File OpenCV Samples
- Setup ---------------------------
  * Setup venv                    : virtualenv -p /usr/bin/python3.6 /tmp/openCVVenv
  * Enter venv                    : source  /tmp/openCVVenv/bin/activate
  * Install requirements          : pip3 install -r requirements.txt

- Morph ---------------------------
  * Description                   : Demonstrate Morphological Transformations
  * Run                           : ./morph.py

- Trackbar ------------------------
  * Description                   : Have trackbars change r,g,b colors of picture
  * Run                           : ./trackbar.py

- Video ---------------------------
  * Description                   : Take and save a video using computer webcam
  * Run                           : ./video.py

- Image ---------------------------
  * Description                   : Open, display, save, and show image statistics
  * Run                           : ./image.py

- Draw ----------------------------
  * Description                   : Draw on console, Ctrl + C to exit
  * Run                           : ./draw.py

