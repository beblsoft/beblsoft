#!/usr/bin/env python3
"""
 NAME
  morph.py

 DESCRIPTION
  Program to demonstrate basic openCV Morphological Transformations

  Python OpenCV Tutorial:
  http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html#morphological-ops
"""

# ----------------------------- IMPORTS ------------------------------------- #
import numpy as np
from matplotlib import pyplot as plt
import cv2

# ----------------------------- MAIN ---------------------------------------- #
#Different Kernels
simpleKern  = np.ones((5,5),np.uint8)
rectKern    = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
ellipseKern = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
crossKern   = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,5))

img         = cv2.imread('j.png', 0);
erosion     = cv2.erode(img,simpleKern,iterations = 1)
dilation    = cv2.dilate(img,simpleKern,iterations = 1)
opening     = cv2.morphologyEx(img, cv2.MORPH_OPEN, simpleKern)
closing     = cv2.morphologyEx(img, cv2.MORPH_CLOSE, simpleKern)
gradient    = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, simpleKern)
tophat      = cv2.morphologyEx(img, cv2.MORPH_TOPHAT, simpleKern)
blackhat    = cv2.morphologyEx(img, cv2.MORPH_BLACKHAT, simpleKern)


plt.subplot(2,4,1),plt.imshow(img),plt.title('Input')
plt.subplot(2,4,2),plt.imshow(erosion),plt.title('Erosion')
plt.subplot(2,4,3),plt.imshow(dilation),plt.title('Dilation')
plt.subplot(2,4,4),plt.imshow(opening),plt.title('Opening')
plt.subplot(2,4,5),plt.imshow(closing),plt.title('Closing')
plt.subplot(2,4,6),plt.imshow(gradient),plt.title('Gradient')
plt.subplot(2,4,7),plt.imshow(tophat),plt.title('Tophat')
plt.subplot(2,4,8),plt.imshow(blackhat),plt.title('Blackhat')
plt.show()
