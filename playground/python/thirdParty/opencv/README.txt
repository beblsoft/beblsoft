OVERVIEW
===============================================================================
- OpenCV Documentation
- Description                       : OpenCV (Open Source Computer Vision Library)
                                    : Built to provide a common infrastructure for computer
                                      vision applications and to accelerate the use of machine
                                      perception in commercial products
- Installation ---------------------: pip3 install opencv-python
- Relevant URLs ---------------------
 * Home                             : https://opencv.org/
 * Python Tutorials                 : https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html

