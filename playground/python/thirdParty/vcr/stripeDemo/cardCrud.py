#!/usr/bin/env python3
"""
NAME
 cardCrud.py

DESCRIPTION
 VCR.py program to do card crud
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import logging
from datetime import datetime
from vcr import VCR
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.customer.dao import StripeCustomerDAO
from base.stripe.python.card.dao import StripeCardDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(message)s",
                    datefmt="%m/%d/%Y %H:%M:%S", level=logging.INFO)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    dirPath       = os.path.dirname(os.path.realpath(__file__))
    basename      = os.path.basename(__file__)
    cassettePath  = os.path.join(dirPath, "{}.cassette.yaml".format(basename))
    startTime     = datetime.now()
    bStripe       = BeblsoftStripe(secretKey="sk_test_AVVBGX9JqS3hFrM8JGDzBAX5")
    vcr           = VCR(record_mode="once")

    with vcr.use_cassette(cassettePath):
        # Create
        stripeCustomerModel  = StripeCustomerDAO.create(
            bStripe              = bStripe,
            email                = "bensson.james@gmail.com",
            metadata             = {"aID": 10})
        stripeCardModel      = StripeCardDAO.create(
            bStripe              = bStripe,
            stripeCustomerID     = stripeCustomerModel.id,
            source               = "tok_visa",
            metadata             = {"id": 1})

        # Update
        stripeCardModel      = StripeCardDAO.update(
            bStripe              = bStripe,
            stripeCustomerID     = stripeCustomerModel.id,
            stripeCardID         = stripeCardModel.id,
            metadata             = {"aID": 10})

        # Describe
        stripeCardModel.describe()

        # Describe All
        stripeCardModelList = StripeCardDAO.getAll(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id)
        for curStripeCardModel in stripeCardModelList:
            curStripeCardModel.describe()

        # Delete
        StripeCardDAO.delete(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id,
                             stripeCardID=stripeCardModel.id)
        StripeCustomerDAO.delete(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id)

    print("TimeDelta={}".format(datetime.now() - startTime))
