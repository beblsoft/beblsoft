#!/usr/bin/env python3
"""
NAME
 cardDefault.py

DESCRIPTION
 VCR.py program to set default card
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import logging
from datetime import datetime
from vcr import VCR
from base.stripe.python.bStripe import BeblsoftStripe
from base.stripe.python.customer.dao import StripeCustomerDAO
from base.stripe.python.card.dao import StripeCardDAO


# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(message)s",
                    datefmt="%m/%d/%Y %H:%M:%S", level=logging.INFO)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    dirPath       = os.path.dirname(os.path.realpath(__file__))
    basename      = os.path.basename(__file__)
    cassettePath  = os.path.join(dirPath, "{}.cassette.yaml".format(basename))
    startTime     = datetime.now()
    bStripe       = BeblsoftStripe(secretKey="sk_test_AVVBGX9JqS3hFrM8JGDzBAX5")
    vcr           = VCR(record_mode="once", match_on=["method", "uri"])

    with vcr.use_cassette(cassettePath):
        # Create
        stripeCustomerModel  = StripeCustomerDAO.create(
            bStripe              = bStripe,
            email                = "bensson.james@gmail.com",
            metadata             = {"aID": 10})
        stripeCardModel1     = StripeCardDAO.create(
            bStripe              = bStripe,
            stripeCustomerID     = stripeCustomerModel.id,
            source               = "tok_visa",
            metadata             = {"id": 1})
        stripeCardModel2     = StripeCardDAO.create(
            bStripe              = bStripe,
            stripeCustomerID     = stripeCustomerModel.id,
            source               = "tok_visa",
            metadata             = {"id": 2})

        stripeCardModelList  = [stripeCardModel1, stripeCardModel2]
        for stripeCardModel in stripeCardModelList:
            stripeCustomerModel = StripeCustomerDAO.setDefaultCard(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id,
                                                                   stripeCardID=stripeCardModel.id)
            stripeCustomerModel = StripeCustomerDAO.getByID(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id)
            assert stripeCustomerModel.defaultSourceID == stripeCardModel.id

        for stripeCardModel in stripeCardModelList:
            StripeCardDAO.delete(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id,
                                 stripeCardID=stripeCardModel.id)
        StripeCustomerDAO.delete(bStripe=bStripe, stripeCustomerID=stripeCustomerModel.id)

    print("TimeDelta={}".format(datetime.now() - startTime))
