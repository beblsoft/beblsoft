OVERVIEW
===============================================================================
Python VCR Samples Documentation


PROJECTS
===============================================================================
- Setup ---------------------------
  * Setup venv                    : virtualenv -p /usr/bin/python3.6 venv
  * Enter venv                    : source venv/bin/activate
  * Install requirements          : pip3 install -r requirements.txt

- Hello World ---------------------
  * Description                   : VCR.py hello world usage
  * Directory                     : ./helloWorld
  * Start                         : Delete cassette.yaml
  * First run generates cassete
    but slow                      : ./helloWorld.py
                                    RespJson={'userId': 1, 'id': 1, 'title': 'delectus aut autem', 'completed': False}
                                    delta=0:00:00.133759
  * Second run much faster        : ./helloWorld.py
                                    RespJson={'userId': 1, 'id': 1, 'title': 'delectus aut autem', 'completed': False}
                                    delta=0:00:00.005410

- Hello Test ----------------------
  * Description                   : VCR.py hello world in a unittest
  * Directory                     : ./helloTest
  * Run                           : ./helloTest.py

- Advanced Test -------------------
  * Description                   : Leverages vcrpy-unittest base test classes
                                    Stores cassettes in separate
  * Directory                     : ./advancedTest
  * File Tree
    .
    |-- cassettes                 : Cassette directory
    |   `-- foo
    |       `-- FooTestCase.
    |           test_url.yaml     : Cassette for foo_test
    |-- foo
    |   |-- foo_test.py           : FooTestCase
    |   |-- __init__.py
    |-- common.py                 : CommonTestCase
    |                               Puts cassettes in correct directory
    |                               Returns correct configuration for VCR object
    |-- main.py                   : Searches for and runs all tests
  * Run                           : ./main.py

- Stripe Demo ---------------------
  * Description                   : VCR.py wrapping over Stripe operations
  * Directory                     : ./stripeDemo
  * Run Card Crud                 : ./cardCrud.py
  * Run Card Default              : ./cardDefault.py

- Thread Demo ---------------------
  * Description                   : Demonstrates that VCR.py does NOT working with threading
  * Directory                     : ./threadDemo
  * Run                           : ./main.py

- Decode Response -----------------
  * Description                   : Example decoding responses
  * Directory                     : ./decodeResponse
  * Run                           : ./main.py
