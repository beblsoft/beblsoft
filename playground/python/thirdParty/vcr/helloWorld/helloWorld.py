#!/usr/bin/env python3
"""
NAME
 helloWorld.py

DESCRIPTION
 VCR.py hello world program
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from datetime import datetime
import vcr
import requests


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    dirPath       = os.path.dirname(os.path.realpath(__file__))
    cassettePath  = os.path.join(dirPath, "cassette.yaml")
    startTime     = datetime.now()
    resp          = None
    url           = "https://jsonplaceholder.typicode.com/todos/1"

    with vcr.use_cassette(cassettePath):
        resp     = requests.get(url)
    endTime      = datetime.now()
    print("RespJson={}\ndelta={}".format(resp.json(), endTime - startTime))
