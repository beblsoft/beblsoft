OVERVIEW
===============================================================================
- VCR.py Documentation
- Description                       : Simplifies and speeds up tests that make HTTP requests
                                    : The first time code is run inside VCR.py context manager or decorated
                                      function, VCR.py records all HTTP interactions through supported
                                      libraries and serializes them to a flat file, called a cassette
                                    : When the relevant piece of code is executed again, VCR.py will
                                      play back the responses from the cassette
                                      Preventing HTTP traffic
- Benefits                          : Ability to work offline
                                      Completely deterministic tests
                                      Increased execution speed
- API Changes                       : If server testing against chages its API, delete
                                      existing cassette files and then run tests again to create new cassette files

- URLS
 * Source                           : https://github.com/kevin1024/vcrpy
 * Documentation                    : https://vcrpy.readthedocs.io/en/stable/
 * VCR-UnitTest Source              : https://github.com/agriffis/vcrpy-unittest



INSTALLATION
===============================================================================
- Install                           : pip3 install vcrpy
- Compatibility                     : Python2.7 and Python3.4+
- HTTP Libraries Supported          : aiohttp, boto, boto3, http.client, httplib2,
                                      requests, tornado.httpclient, urllib2, urllib3
- Pyyaml                            : Causes VCR.py to run 10x faster
  * Test if installed               : python -c 'from yaml import CLoader'
  * Install                         : apt-get install libyaml-dev
                                      pip3 uninstall pyyaml
                                      pip3 --no-cache-dir install pyyaml
  * Install vcr-unittest            : pip3 install vcrpy-unittest


4 RECORD MODES
===============================================================================
1 once                              : Replay previously recorded interactions
                                      Record new interactions if there is no cassette file
                                      Cause an error to be raised for new request if there is a cassette file
                                      Default record mode
2 new_episodes                      : Record new interactions
                                      Replay previously recorded interactions, even if an existing cassette exists
                                      that is not identical
3 none                              : Replay previously recorded interactions
                                      Cause error to be raised for any new requests
4 all                               : Record new interactions
                                      Never replay previously recorded interactions
                                      Used to force VCR to re-record a cassette

