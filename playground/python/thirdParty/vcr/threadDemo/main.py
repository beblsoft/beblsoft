#!/usr/bin/env python3
"""
NAME
 main.py

DESCRIPTION
 VCR.py threading example, does NOT work!

RELATED
  https://github.com/kevin1024/vcrpy/issues/295
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import logging
from datetime import datetime
import threading
import requests
from vcr import VCR

# ----------------------------- GLOBALS ------------------------------------- #
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(message)s",
                    datefmt="%m/%d/%Y %H:%M:%S", level=logging.INFO)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    dirPath       = os.path.dirname(os.path.realpath(__file__))
    basename      = os.path.basename(__file__)
    cassettePath  = os.path.join(dirPath, "{}.cassette.yaml".format(basename))
    startTime     = datetime.now()
    vcr           = VCR(record_mode="once")
    url           = "https://jsonplaceholder.typicode.com/todos/1"
    nThreads      = 3
    tList         = []

    with vcr.use_cassette(cassettePath):
        def sendRequests(nRequests=10): #pylint: disable=C0111
            for _ in range(nRequests):
                _ = requests.get(url)

        for _ in range(nThreads):
            t = threading.Thread(target=sendRequests)
            t.start()
            tList.append(t)

        for t in tList:
            t.join()

    print("TimeDelta={}".format(datetime.now() - startTime))
