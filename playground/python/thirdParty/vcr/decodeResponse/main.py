#!/usr/bin/env python3
"""
NAME
 main.py

DESCRIPTION
 Decode a VCR.py response
"""

# ----------------------------- IMPORTS ------------------------------------- #
import json
import pprint
import vcr
import yaml


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    doc            = None
    cassettePath   = "/home/jbensson/git/beblsoft/projects/smeckn/server/test/cassettes/aDB/facebookPost/dao_test/FacebookPostModelDAOTestCase.test_createPostFromMedia.yaml"
    uri            = "https://graph.facebook.com/v3.2/924804401063069/accounts/test-users?access_token=924804401063069%7Cc7e56c465778e1edd36ef00911dd61b8"

    with open(cassettePath, "r") as f:
        doc = yaml.load(f)

    for interaction in doc["interactions"]:
        request         = interaction["request"]
        respEncoded     = interaction["response"]
        respDecoded     = vcr.filters.decode_response(respEncoded)
        respBodyString  = respDecoded["body"]["string"]
        respJSON        = json.loads(respBodyString)
        respData        = respJSON.get("data")

        if request["uri"] == uri:
            # print(pprint.pformat(request))
            print(pprint.pformat(respData))
