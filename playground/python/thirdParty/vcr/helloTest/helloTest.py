#!/usr/bin/env python3
"""
NAME
 helloTest.py

DESCRIPTION
 VCR.py hello world test program
"""

# ----------------------------- IMPORTS ------------------------------------- #
import unittest
from datetime import datetime
import vcr
import requests

# ----------------------------- GLOBALS ------------------------------------- #
testVCR = vcr.VCR(
    serializer="yaml",
    # cassette_library_dir="fixtures/cassettes",
    record_mode="once",
    match_on=["method", "scheme", "host", "port", "path", "query"]
)

# ----------------------------- TEST CASE ----------------------------------- #
class SimpleTestCase(unittest.TestCase):
    """
    Simple Test Case
    """

    @testVCR.use_cassette()
    def test_url(self):
        """
        Test fetching a url
        """
        startTime     = datetime.now()
        resp          = None
        url           = "https://jsonplaceholder.typicode.com/todos/1"
        resp          = requests.get(url)
        endTime       = datetime.now()
        self.assertIsNotNone(resp)
        print("RespJson={}\ndelta={}".format(resp.json(), endTime - startTime))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    unittest.main()
