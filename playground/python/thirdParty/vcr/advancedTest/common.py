#!/usr/bin/env python3
"""
NAME
 common.py

DESCRIPTION
 Common Test Case
"""

# ----------------------------- IMPORTS ------------------------------------- #
import sys
import os
import unittest
from vcr_unittest import VCRMixin  # pylint: disable=E0401


# ----------------------------- TEST CASE ----------------------------------- #
class CommonTestCase(VCRMixin, unittest.TestCase):
    """
    Common Test Case
    """

    def _get_vcr(self, **kwargs):
        vcr             = super()._get_vcr(**kwargs)
        vcr.record_mode = "once"
        vcr.match_on    = ["method", "scheme", "host", "port", "path", "query"]
        # print(pprint.pformat(vcr.__dict__))
        return vcr

    def _get_cassette_library_dir(self, **kwargs): #pylint: disable=W0613
        """
        Return cassette directory for test
        """
        curDir       = os.path.dirname(os.path.realpath(__file__))      # .../vcr/advancedTest
        subClassFile = sys.modules[self.__class__.__module__].__file__  # .../vcr/advancedTest/foo/foo_test.py
        subClassDir  = os.path.dirname(subClassFile)                    # .../vcr/advancedTest/foo
        commonPrefix = os.path.commonprefix([curDir, subClassDir])      # .../vcr/advancedTest
        relativePath = os.path.relpath(subClassDir, commonPrefix)       # foo
        libraryDir   = os.path.join(curDir, "cassettes", relativePath)  # .../vcr/advancedTest/cassettes/foo

        return libraryDir
