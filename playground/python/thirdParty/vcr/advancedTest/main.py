#!/usr/bin/env python3
"""
 NAME:
  main.py

 DESCRIPTION
  Run all tests
"""

# -------------------------- IMPORTS ---------------------------------------- #
import os
import unittest



# -------------------------- MAIN ------------------------------------------- #
if __name__ == "__main__":
    pattern = "*_test.py"
    dirPath = os.path.dirname(os.path.realpath(__file__))
    tests   = unittest.TestLoader().discover(start_dir=dirPath, pattern=pattern)
    result  = unittest.TextTestRunner(verbosity=2).run(tests)
