#!/usr/bin/env python3
"""
NAME
 foo_test.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from datetime import datetime
import requests
from playground.python.integrations.vcr.advancedTest.common import CommonTestCase


# ----------------------------- FOO TEST CASE ------------------------------- #
class FooTestCase(CommonTestCase):
    """
    Foo Test Case
    """

    def test_url(self):
        """
        Test fetching a url
        """
        startTime     = datetime.now()
        resp          = None
        url           = "https://jsonplaceholder.typicode.com/todos/1"
        resp          = requests.get(url)
        endTime       = datetime.now()
        self.assertIsNotNone(resp)
        print("RespJson={}\ndelta={}".format(resp.json(), endTime - startTime))
