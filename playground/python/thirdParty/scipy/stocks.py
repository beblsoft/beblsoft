#!/usr/bin/env python
"""
NAME
 stocks.py

DESCRIPTION
  Program to compare different stocks / mutual funds

INSTALL
 sudo apt-get install python-pip
 sudo pip install numpy
 sudo pip install matplotlib

RUN
 python ./stocks.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import sys
import numpy as np
import matplotlib.pyplot as plt


# ----------------------------- CLASSES ------------------------------------- #
class stock():
    """
    Stock Class
    """

    def __init__(self):
        self.name             = "test"
        self.frontload        = 0     # % taken upen invesment
        self.backload         = 0     # % taken upon removal
        self.annualInterest   = 6     # % interest acquired on each year
        self.expenseRatio     = .01    # % paid each year
        self.annualInvestment = 25000  # $ invested each year
        self.initialValue     = 103000  # Initial lump sum
        self.total            = []     # list of total money each year
        # total[0] = money at end of year 0

    def calculateTotal(self, years):
        """
        Calculate Total Appreciation
        """
        # Fills out self.total[0] -> self.total[years - 1] (total money at end of each year)
        annualValueAdd    = self.annualInvestment * (1 - self.frontload / 100)
        effectiveInterest = self.annualInterest - self.expenseRatio
        for year in range(years):
            if(year == 0):
                self.total.append((self.initialValue + annualValueAdd) * (1 + effectiveInterest / 100))
            else:
                self.total.append((self.total[year - 1] + annualValueAdd) *
                                  (1 + effectiveInterest / 100))

    def plot(self, years, display):
        """
        Plot Total Appreciation
        """
        self.calculateTotal(years)
        time = np.arange(0, years, 1)
        plt.plot(time, self.total, display)
        plt.xlabel("Time (years)")
        plt.ylabel("Money ($)")
        plt.title("Theoretical Earnings for stock:" + self.name)
        plt.grid(True)
        plt.show()

# ----------------------------- MAIN ---------------------------------------- #
s = stock()
s.plot(50, "bs")
