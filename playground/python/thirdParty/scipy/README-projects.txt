OVERVIEW
===============================================================================
SciPy Samples Documentation



SINGLE FILE SAMPLES
===============================================================================
- Setup -----------------------------
  * Setup venv                      : virtualenv -p /usr/bin/python3.6 /tmp/sciPyVenv
  * Enter venv                      : source  /tmp/sciPyVenv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- Sine Wave -------------------------
  * Description                     : Plots a sine wave
  * Run                             : ./sineWave.py

- Histogram -------------------------
  * Description                     : Plots a histogram
  * Run                             : ./histogram.py

- stocks.py -------------------------
  * Description                     : Model stock appreciation over time
  * Run                             : ./stocks.py
