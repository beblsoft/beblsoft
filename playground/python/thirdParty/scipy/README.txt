OVERVIEW
===============================================================================
- SciPy Documentation
- Description                       : Scientific computing tools for python
                                    : The SciPy ecosystem is a collection of open source software
                                      for scientfici computing in Python
- Ecosystem -------------------------
  * Core Packages
    Python                          : General purpose programming language
    Numpy                           : Fundamental package for numerical computation. Defines the
                                      numerical arrau and matric types and basic operations on them
    SciPy library                   : Collection of numerical algorithms and domain-specific toolboxes,
                                      including signal processing, optimization, statistics and much more
    Matplotlib                      : A mature and popular plotting package, which provides publication-
                                      quality 2D plotting and rudimentary 3D plotting
  * Data computation
    Pandas                          : High-performance, easy to use data structures
    SymPy                           : Symbolic mathematics and computer algebra
    scikit-image                    : Algorithms for image processing
    scikit-learn                    : Algorithms and tools for machine learning
    h5py and PyTables               : Access data stored in HDF5 format
  * Productivity and high-perf
    IPython                         : Rich interactive interface, letting you quickly process data
                                      and test ideas
    Jupyter                         : Notebooks provide IPythin functionality and more in web browser
                                      Allowing documentation in easily reproducible form
    Cython                          : Conveniently build C extensions, either to speed up critical code,
                                      or to integrate with C/C++ libraries
    Dask, Joblib, IPyParallel       : Distributed processing with a focus on numeric data
  * Quality Assurance
    nose                            : framework for testing Python code, being replace by py_test
    numpydoc                        : Standard and library for documenting Scientific Python libraries
- Relevant URLs ---------------------
 * Home                             : https://scipy.org/index.html
 * About                            : https://scipy.org/about.html
 * NumPy Home                       : https://docs.scipy.org/doc/numpy/
 * SciPy (lib) Home                 : https://docs.scipy.org/doc/scipy/reference/
 * Matplotlib Home                  : https://matplotlib.org/contents.html
 * IPython Home                     : http://ipython.org/ipython-doc/stable/index.html
 * SymPy Home                       : https://docs.sympy.org/latest/index.html
 * Pandas Home                      : https://docs.sympy.org/latest/index.html



INSTALLATION
===============================================================================
- Install                           : sudo apt-get install python3-tk
                                      pip3 install numpy scipy matplotlib ipython jupyter pandas sympy nose
- Uninstall                         : pip3 uninstall numpy scipy matplotlib ipython jupyter pandas sympy nose
