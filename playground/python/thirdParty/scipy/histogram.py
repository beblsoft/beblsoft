#!/usr/bin/env python3
"""
NAME
 histogram.py

DESCRIPTION
 Plot a histogram

REFERENCE
  https://matplotlib.org/gallery/statistics/histogram_features.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
import matplotlib.pyplot as plt
import numpy


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    # Example data
    mu        = 100  # mean of distribution
    sigma     = 15   # standard deviation of distribution
    x         = mu + sigma * numpy.random.randn(437)
    num_bins  = 50
    (fig, ax) = plt.subplots()

    # The data histogram
    (n, bins, patches) = ax.hist(x, num_bins, density=1)

    # Add a 'best fit' line
    y = ((1 / (numpy.sqrt(2 * numpy.pi) * sigma)) * numpy.exp(-0.5 * (1 / sigma * (bins - mu))**2))
    ax.plot(bins, y, '--')
    ax.set_xlabel('Smarts')
    ax.set_ylabel('Probability density')
    ax.set_title(r'Histogram of IQ: $\mu=100$, $\sigma=15$')

    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()
    plt.show()
