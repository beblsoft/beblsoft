OVERVIEW
===============================================================================
Python Pillow Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- pillowHelloWorld.py -------------
  * Description                   : Open a photo and display it
  * Run                           : ./pillowHelloWorld.py

- pillowConvert.py ----------------
  * Description                   : Convert an image into various formats
  * Run                           : ./pillowConvert.py

- pillowThumbnail.py --------------
  * Description                   : Convert an image into a JPEG thumbnail
  * Run                           : ./pillowThumbnail.py

- pillowCrop.py -------------------
  * Description                   : Crop image
  * Run                           : ./pillowCrop.py

- pillowStat.py -------------------
  * Description                   : Calculate image statistics
  * Run                           : ./pillowStat.py

