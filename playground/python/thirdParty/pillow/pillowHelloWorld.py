#!/usr/bin/python3
"""
 NAME
  pillowHelloWorld.py

 DESCRIPTION
  Pillow Hello World Program
"""

# ----------------------------- IMPORTS ------------------------------------- #
from PIL import Image


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    filename = "helloWorld.png"

    with Image.open(filename) as im:
        print("Image Format   : {}".format(im.format))
        print("Image Size     : {}".format(im.size))
        print("Image Mode     : {}".format(im.mode)) # L    = luminance
                                                     # RBG  = True Colors
                                                     # CMYK = pre-press
        print("Image Colors   : {}".format(im.getcolors()))
        print("Image Data     : {}".format(im.getdata()))
        print("Image Extrema  : {}".format(im.getextrema()))

        im.show()
