#!/usr/bin/python3
"""
 NAME
  pillowCrop.py

 DESCRIPTION
  Pillow program to crop image
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from PIL import Image


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    inFile      = "helloWorld.png"
    inFileNoPre = os.path.splitext(inFile)[0]  # No ".png"
    outFile     = "{}-cropped.png".format(inFileNoPre)

    with Image.open(inFile) as im:
        topLeftX     = 100
        topLeftY     = 100
        bottomRightX = 400
        bottomRightY = 400
        box          = (topLeftX, topLeftY, bottomRightX, bottomRightY)
        region       = im.crop(box)

        region.save(outFile)
