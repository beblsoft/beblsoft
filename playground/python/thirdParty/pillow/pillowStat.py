#!/usr/bin/python3
"""
 NAME
  pillowStat.py

 DESCRIPTION
  Pillow program list image stats
"""

# ----------------------------- IMPORTS ------------------------------------- #
from PIL import Image, ImageStat


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    inFile      = "helloWorld.png"

    with Image.open(inFile) as im:
        stat = ImageStat.Stat(im)
        print("Extrma : {}".format(stat.extrema))
        print("Count  : {}".format(stat.count))
        print("Sum    : {}".format(stat.sum))
        print("Sum2   : {}".format(stat.sum2))
        print("Mean   : {}".format(stat.mean))
        print("Median : {}".format(stat.median))
        print("RMS    : {}".format(stat.rms))
        print("VAR    : {}".format(stat.var))
        print("STDDEV : {}".format(stat.stddev))
