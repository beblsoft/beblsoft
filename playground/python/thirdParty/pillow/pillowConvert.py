#!/usr/bin/python3
"""
 NAME
  pillowConvert.py

 DESCRIPTION
  Pillow Conversion Program
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from PIL import Image


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    inFile         = "helloWorld.png"
    inFileNoPre    = os.path.splitext(inFile)[0]  # No ".jpg"
    outFileExtList = ["bmp", "im", "jpeg", "png", "ppm", "tiff", "WebP"]

    with Image.open(inFile) as im:
        for ext in outFileExtList:
            print(ext)
            outFile = "{}-converted.{}".format(inFileNoPre, ext)
            im.save(outFile)
