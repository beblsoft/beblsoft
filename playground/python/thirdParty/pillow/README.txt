OVERVIEW
===============================================================================
- Pillow Documentation
- Description                       : Pillow is the friendly PIL fork by Alex Clark and Contributors.
                                      PIL is the Python Imaging Library by Fredrik Lundh and Contributors.

- Use Cases -------------------------
  * Image Archiving                 : Create thumbnails, convert formats, print images, etc
  * Image Displaying                : Display images with .show() method
  * Image Processing                : Basic image processing functionality: point operations, filtering, etc.
                                      Also supports resizing, rotations, and affine transforms
- URLS ------------------------------
 * Home                             : https://python-pillow.org/
 * Documentation                    : https://pillow.readthedocs.io/en/5.1.x/handbook/tutorial.html
 * Github                           : https://github.com/python-pillow/Pillow


IMAGE COORDINATES
===============================================================================
- Coordinates                       : PIL sets (0,0) at upper left corner
- Axes                              :   0                              -> x
                                      0 -----------------------------------
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                        |                                 |
                                      | |                                 |
                                      v |                                 |
                                      y |                                 |
                                        -----------------------------------
