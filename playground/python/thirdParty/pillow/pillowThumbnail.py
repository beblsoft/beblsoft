#!/usr/bin/python3
"""
 NAME
  pillowThumbnail.py

 DESCRIPTION
  Pillow program to create thumbnail
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from PIL import Image


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    inFile         = "helloWorld.png"
    inFileNoPre    = os.path.splitext(inFile)[0]  # No ".png"
    outFile        = "{}.thumbnail".format(inFileNoPre)
    outFileFormat  = "jpeg"

    with Image.open(inFile) as im:
        im.save(outFile, format=outFileFormat)
