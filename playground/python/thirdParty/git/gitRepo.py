#!/usr/bin/env python3
"""
NAME
 gitRepo.py

DESCRIPTION
 Git Repository Functionality

PRINTS
 Git dir         : /home/jbensson/git/beblsoft/.git
 Woring tree dir : /home/jbensson/git/beblsoft
 Is repo dirty?  : True
 Branches        : [<git.Head "refs/heads/master">, <git.Head "refs/heads/mojourney-v0">, <git.Head "refs/heads/quote-v2">, <git.Head "refs/heads/smeckn-v1">]
 Refs            : [<git.Head "refs/heads/master">, <git.Head "refs/heads/mojourney-v0">, <git.Head "refs/heads/quote-v2">, <git.Head "refs/heads/smeckn-v1">, <git.RemoteReference "refs/remotes/origin/HEAD">, <git.RemoteReference "refs/remotes/origin/master">, <git.RemoteReference "refs/remotes/origin/mojourney-v0">, <git.RemoteReference "refs/remotes/origin/quote-v2">, <git.RemoteReference "refs/remotes/origin/smeckn-v1">]
 Remotes         : [<git.Remote "origin">]
 HEAD sha        : 1a2bd7f06f148b1c766a28a4124cb21996fecf86
 Description     : Unnamed repository; edit this file 'description' to name the repository.
"""

# ----------------------------- IMPORTS ------------------------------------- #
import git

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    repo = git.Repo(search_parent_directories=True)
    print("Git dir         : {}".format(repo.git_dir))
    print("Woring tree dir : {}".format(repo.working_tree_dir))
    print("Is repo dirty?  : {}".format(repo.is_dirty()))
    print("Branches        : {}".format(repo.branches))
    print("Refs            : {}".format(repo.refs))
    print("Remotes         : {}".format(repo.remotes))
    print("HEAD sha        : {}".format(repo.head.object.hexsha))
    print("Description     : {}".format(repo.description))
