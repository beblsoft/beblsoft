OVERVIEW
===============================================================================
- Git Python Documentation
- Description ----------------------: GitPython is a python library used to interact with git
                                      repositories
                                    : The object database implementation is optimized for handling
                                      large quantities of objects and large datasets, which is achieved
                                      by using low-level structures and data streaming
- Installation ----------------------
  * Command                         : pip3 install gitpython
  * Requires                        : > Python 2.7
                                      > Git 1.7.0
- Relevant URLS ---------------------
  * GitHub                          : https://github.com/gitpython-developers/GitPython
  * Documentation                   : https://gitpython.readthedocs.io/en/stable/intro.html
  * API Reference                   : https://gitpython.readthedocs.io/en/stable/reference.html#module-git.repo.base

