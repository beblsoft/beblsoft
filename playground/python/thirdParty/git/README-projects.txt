OVERVIEW
===============================================================================
Git Python Samples Documentation


SINGLE FILE PROJECTS
===============================================================================
- Setup ---------------------------
  * Virtual Environment           : virtualenv -p /usr/bin/python3.6 /tmp/gitVenv
                                  : source /tmp/gitVenv/bin/activate
                                  : ... commands
                                  : deactivate
- gitRepo.py ----------------------
  * Description                   : Example usage of git repo object
  * Run                           : ./gitRepo.py
