OVERVIEW
===============================================================================
- ForgeryPy Documentation
- Description                       : ForgeryPy3 is a fake data generator fully
                                      compatible with Python 2 and 3
- Install                           : pip3 install forgerypy3
- Home URL                          : https://pypi.org/project/ForgeryPy3/
- import                            : import forgery_py


API
===============================================================================
- Address module --------------------
  * address.city()                  : 'Larkspur'
  * address.continent()             : 'North America'
  * address.country()               : 'Belgium'
  * address.phone()                 : '5-(721)114-0241'
  * address.state()                 : 'Kansas'
  * address.state_abbrev()          : 'LA'
  * address.street_address()        : '4 Eastlawn Junctio'
  * address.street_name()           : 'Dexter'
  * address.street_number()         : '1'
  * address.street_suffix()         : 'Park'
  * address.zip_code()              : '65843-3832'

- Basic module ----------------------
  * basic.hex_color()	              : ‘6D1F5B’
  * basic.hex_color_short()	        : ‘05C’
  * basic.text()	                  : ‘ncvgIY0pGKGHv’
  * basic.boolean()	                : True
  * basic.color()	                  : ‘Red’
  * basic.encrypt()	                : ‘fc0d835dd4e4df144a33a6a346298b0f23dcd14a’
  * basic.frequency()	              : ‘Never’
  * basic.number()	                : 5
  * basic.password()	              : ‘KcLBHCv6’

- Credit Card module ----------------
  * credit_card.check_digit(num)	  : 5
  * credit_card.number()	          : 343682330855371
  * credit_card.type()	            : ‘American Express’
  * currency.code()	                : ‘CHF’
  * currency.description()	        : ‘Canada Dollars’

- Date Module -----------------------
  * date.date()	                    : datetime.date(2016, 11, 8)
  * date.datetime()	                : datetime.datetime(2016, 11, 27, 22, 29, 31, 45877)
  * date.day()	                    : 4
  * date.day_of_week()	            : ‘Thursday’
  * date.month()	                  : ‘September’
  * date.year()	                    : 2021

- Email Module ----------------------
  * email.address()	                : ‘gsmith@kamba.org’
  * email.body()	                  : ‘Lorem ipsum dolor sit amet, …’
  * email.subject()	                : ‘Lorem Ipsum Dolor Sit Amet…’

- Geo Module ------------------------
  * geo.latitude()	                : -8.095096815540515
  * geo.latitude_degrees()	        : -49
  * geo.latitude_direction()        : ‘N’
  * geo.latitude_minutes()	        : 14
  * geo.latitude_seconds()	        : 45
  * geo.longitude()	                : -22.56746406884514
  * geo.longitude_degrees()	        : 100
  * geo.longitude_direction()	      : ‘W’
  * geo.longitude_minutes()	        : 47
  * geo.longitude_seconds()	        : 41

- Internet Module -------------------
  * internet.cctld()	              : ‘om’
  * internet.domain_name()	        : ‘edgepulse.name’
  * internet.email_address()	      : ‘lillian@flashpoint.biz’
  * internet.email_subject()	      : ‘Lorem Ipsum Dolor Sit Amet…’
  * internet.ip_v4()	              : ‘96.36.71.94’
  * internet.top_level_domain()	    : ‘gov’
  * internet.user_name()	          : ‘earl’

- Lorem Ipsum Module ----------------
  * lorem_ipsum.paragraph()	        : ‘In hac habitasse platea dictumst…’
  * lorem_ipsum.paragraphs()	      : ‘Nam nulla. Phasellus sit amet erat.’
  * lorem_ipsum.sentence()	        : ‘Quisque porta volutpat erat.’
  * lorem_ipsum.sentences()	        : ‘Duis consequat… Integer non velit…’
  * lorem_ipsum.title()	            : ‘Vestibulum proin tristique lobortis!’
  * lorem_ipsum.word()	            : ‘maecenas’
  * lorem_ipsum.words()	            : ‘platea cubilia pede et ultrices congue’
  * lorem_ipsum.character()	        : ‘l’
  * lorem_ipsum.characters()	      : ‘lorem ipsu’
  * lorem_ipsum.
    lorem_ipsum_characters()  	    : ‘lorem ipsum dolor sit amet…’
  * lorem_ipsum.lorem_ipsum_words()	: [“lorem”, “ipsum”, “dolor”, …]
  * lorem_ipsum.text()	            : ‘Lorem ipsum dolor sit amet…’

- Monetary Module -------------------
  * monetary.formatted_money()	    : ‘$5.49’
  * monetary.money()	              : ‘9.20’

- Name Module -----------------------
  * name.company_name()	            : ‘Dabtype’
  * name.female_first_name()	      : ‘Katherine’
  * name.first_name()	              : ‘Jose’
  * name.full_name()	              : ‘James Williamson’
  * name.industry()	                : ‘Machine Tools & Accessories’
  * name.job_title()	              : ‘Operator’
  * name.job_title_suffix()	        : ‘I’
  * name.last_name()	              : ‘Henry’
  * name.location()	                : ‘Kwik-E-Mart’
  * name.male_first_name()	        : ‘Cheryl’
  * name.suffix()	                  : ‘IV’
  * name.title()	                  : ‘Ms’

- Personal Module -------------------
  * personal.abbreviated_gender()	  : ‘F’
  * personal.gender()	              : ‘Male’
  * personal.language()	            : ‘Tsonga’
  * personal.race()	                : ‘Sri Lankan’
  * personal.shirt_size()	          : ‘XS’

- Time Module -----------------------
  * time.zone()	                    : ‘Amsterdam’

