#!/usr/bin/env python3
"""
 NAME
  helloCallGraph.py

 DESCRIPTION
  Sample program to display call graph
"""

# ----------------------------- IMPORTS ------------------------------------- #
from pycallgraph import PyCallGraph, Config, GlobbingFilter #pylint: disable=E0401
from pycallgraph.output import GraphvizOutput #pylint: disable=E0401


# ----------------------------- GLOBALS ------------------------------------- #
graphviz             = GraphvizOutput()
graphviz.output_file = "/tmp/callgraph.png"

#Config options
# groups=True|False
# max_depth=N
config               = Config()
config.trace_filter  = GlobbingFilter(exclude=["pycallgraph.*", "zappa.*"])


# ----------------------------- FUNCTIONS ----------------------------------- #
def funcA(): #pylint: disable=C0111
    pass

def funcB(): #pylint: disable=C0111
    funcA()

def funcC(): #pylint: disable=C0111
    funcB()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    with PyCallGraph(output=graphviz, config=config):
        funcA()
        funcB()
        funcC()
    print("Callgraph here:{}".format(graphviz.output_file))
