OVERVIEW
===============================================================================
- Callgraph Documentation
- Description                       : Python Call Graph is a Python module that creates
                                      call graph visualizations for Python applications
- Installation                      : sudo apt-get install graphviz
                                      pip install pycallgraph
- Relevant URLs
  * Documentation                   : http://pycallgraph.slowchop.com/en/master/
