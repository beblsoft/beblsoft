OVERVIEW
===============================================================================
Callgraph Samples Documentation



HELLO CALL GRAPH
===============================================================================
- Description --------------------: Different plotting samples
- Setup ---------------------------
  * Install machine deps          : sudo apt-get install graphviz
  * Setup venv                    : virtualenv -p /usr/bin/python3.6 /tmp/callgraphVenv
  * Enter venv                    : source  /tmp/callgraphVenv/bin/activate
  * Install requirements          : pip3 install -r requirements.txt

- Run -----------------------------
  * Create callgraph              : ./helloCallGraph.py
  * Open callgraph                : /tmp/callgraph.png
