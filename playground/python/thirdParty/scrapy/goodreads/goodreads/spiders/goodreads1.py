#!/usr/bin/env python
"""
 NAME:
  Goodreads1Spider.py

 DESCRIPTION
  Intro Spider that scrapes famous quotes from website: https://www.goodreads.com/quotes

"""

# ----------------------------- IMPORTS ------------------------------------- #
# -*- coding: utf-8 -*-
import scrapy  # pylint: disable=E0401


class Goodreads1Spider(scrapy.Spider):
    """
    Goodreads1Spider Class
    """

    name = "goodreads1"
    allowed_domains = ['goodreads']
    start_urls = [
        'http://https://www.goodreads.com/quotes/inspirational',
    ]

    def parse(self, response):
        """
        parse
        """
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span.text::text').extract_first(),
                'author': quote.xpath('span/small/text()').extract_first(),
            }

        next_page = response.css('li.next a::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)
