#!/usr/bin/env python
"""
NAME:
 items.py

DESCRIPTION
 Item documenation
 Models for scraped items

DOCUMENATION
 https://docs.scrapy.org/en/latest/topics/items.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import scrapy


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


# ------------------------ PRODUCT CLASS ------------------------------------ #
class Product(scrapy.Item):
    """
    Product Class
    """
    name        = scrapy.Field()
    price       = scrapy.Field()
    stock       = scrapy.Field()
    lastUpdated = scrapy.Field(serializer=str)


# ------------------------ DISCOUNTED PRODUCT CLASS ------------------------- #
class DiscountedProduct(Product):
    """
    Discounted Product Class
    """
    discountPercent = scrapy.Field(serializer=str)
    discountExpDate = scrapy.Field()


# ------------------------ SPECIFIC PRODUCT CLASS --------------------------- #
class SpecificProduct(Product):
    """
    Specific Product Class

    Example of overwriting metadata
    """
    name = scrapy.Field(Product.fields["name"], serializer=2)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    p = Product(name="Desktop PC", price=1000)

    # Get field values
    logger.info("p name:  {}".format(p["name"]))
    logger.info("p price: {}".format(p.get("price")))

    # Provide default error for fields that haven't been set, or unknown
    logger.info("p stock: {}".format(p.get("stock", "DNE")))
    logger.info("p lala:  {}".format(p.get("lala", "DNE")))

    # Set field values
    p["stock"] = 1
    logger.info("p stock: {}".format(p.get("stock", "DNE")))

    # Accessing all populated values
    logger.info("p keys:  {}".format(p.keys()))
    logger.info("p items: {}".format(p.items()))

    # Copy
    p2 = p.copy()
    logger.info("p2 items:{}".format(p2.items()))
