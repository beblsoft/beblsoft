#!/usr/bin/env python
"""
NAME:
 quotes_spider.py

DESCRIPTION
 Quote Spiders
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import scrapy


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ QUOTES1 SPIDER CLASS ----------------------------- #
class Quotes1Spider(scrapy.Spider):
    """
    Quotes1 Spider Class

	DESCRIPTION
	  Log HTML pages

    ATTRS
      name:
        identifies the spider (must be unique within project)
      start_requests:
        Must return an iterable of requests (list or generator function)
        Subsequent requests will be generated successively
      parse:
        Called to handle response downloaded for each request made
        Parses the response, extracting the scraped data as dicts and
        also finds new URLs to follow and creats new requests
    """
    name = "quotes1"

    def start_requests(self):
        urls = [
            "http://quotes.toscrape.com/page/1/",
            "http://quotes.toscrape.com/page/2/",
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        self.log(response.body)


# ------------------------ QUOTES2 SPIDER CLASS ----------------------------- #
class Quotes2Spider(scrapy.Spider):
    """
    Quotes2 Spider Class

	DESCRIPTION
	  Log HTML pages with simplier start_urls
    """
    name       = "quotes2"
    start_urls = [
        "http://quotes.toscrape.com/page/1/",
        "http://quotes.toscrape.com/page/2/",
    ]

    def parse(self, response):
        self.log(response.body)


# ------------------------ QUOTES3 SPIDER CLASS ----------------------------- #
class Quotes3Spider(scrapy.Spider):
    """
    Quotes3 Spider Class

	DESCRIPTION
	  Parse text, author, and tags
    """
    name       = "quotes3"
    start_urls = [
        "http://quotes.toscrape.com/page/1/",
        "http://quotes.toscrape.com/page/2/",
    ]

    def parse(self, response):
        for quote in response.css("div.quote"):
            yield {
                "text": quote.css("span.text::text").extract_first(),
                "author": quote.css("small.author::text").extract_first(),
                "tags": quote.css("div.tags a.tag::text").extract(),
            }


# ------------------------ QUOTES4 SPIDER CLASS ----------------------------- #
class Quotes4Spider(scrapy.Spider):
    """
    Quotes4 Spider Class

    DESCRIPTION
	  Parse text, author, and tags from all pages
    """
    name       = "quotes4"
    start_urls = [
        "http://quotes.toscrape.com/page/1/",
    ]

    def parse(self, response):
        for quote in response.css("div.quote"):
            yield {
                "text": quote.css("span.text::text").extract_first(),
                "author": quote.css("small.author::text").extract_first(),
                "tags": quote.css("div.tags a.tag::text").extract(),
            }

        next_page = response.css("li.next a::attr(href)").extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)


# ------------------------ QUOTES5 SPIDER CLASS ----------------------------- #
class Quotes5Spider(scrapy.Spider):
    """
    Quotes5 Spider Class

	DESCRIPTION
	  Parse text and author for a specific tag or all tags
    """
    name = "quotes5"

    def start_requests(self):
        """
        Start requests
        """
        url = "http://quotes.toscrape.com/"
        tag = getattr(self, "tag", None)
        if tag is not None:
            url = url + "tag/" + tag
        yield scrapy.Request(url, self.parse)

    def parse(self, response):
        for quote in response.css("div.quote"):
            yield {
                "text": quote.css("span.text::text").extract_first(),
                "author": quote.css("small.author::text").extract_first(),
            }

        next_page = response.css("li.next a::attr(href)").extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
