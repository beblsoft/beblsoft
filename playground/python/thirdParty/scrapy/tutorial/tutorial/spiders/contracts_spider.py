#!/usr/bin/env python
"""
NAME:
 contracts_spider.py

DESCRIPTION
 Spider to document scrapy contracts

DOCUMENATION
 https://doc.scrapy.org/en/latest/topics/contracts.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
from scrapy import Spider, Item, Field


# ------------------------ EXAMPLE ITEM CLASS ------------------------------- #
class ExampleItem(Item):
    """
    Example Item
    """
    title     = Field()
    header    = Field()
    paragraph = Field()


# ------------------------ CONTRACTS SPIDER CLASS --------------------------- #
class ContractsSpider(Spider):
    """
    Contracts Spider Class
    """
    name            = "contracts"
    allowed_domains = ["example.com"]
    start_urls      = ["www.example.com"]

    def parse(self, response):
        """
        Parse Response

        @url http://www.example.com
        @returns items 1 1
        @returns requests 0 0
        @scrapes header paragraph
        """
        item              = ExampleItem()
        item['title']     = response.xpath('//title/text()').extract_first()
        item['header']    = response.xpath('//h1/text()').extract_first()
        item['paragraph'] = response.xpath('//p/text()').extract_first()
        return item
