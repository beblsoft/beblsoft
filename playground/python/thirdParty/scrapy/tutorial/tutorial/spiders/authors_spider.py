#!/usr/bin/env python
"""
NAME:
 authors_spider.py

DESCRIPTION
 Author Spiders
"""

# ------------------------ IMPORTS ------------------------------------------ #
import scrapy


# ------------------------ AUTHORS SPIDER CLASS ----------------------------- #
class AuthorsSpider(scrapy.Spider):
    """
    Authors Spider Class

    DESCRIPTION
      Parse all authors
    """
    name       = 'authors'
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        # Follow links with author pages
        for href in response.css('.author + a::attr(href)'):
            yield response.follow(href, self.parse_author)

        # Follow pagination links
        for href in response.css('li.next a::attr(href)'):
            yield response.follow(href, self.parse)

    def parse_author(self, response):  #pylint: disable=C0111,R0201
        def extract_with_css(query): #pylint: disable=C0111
            return response.css(query).extract_first().strip()

        yield {
            'name': extract_with_css('h3.author-title::text'),
            'birthdate': extract_with_css('.author-born-date::text'),
            'bio': extract_with_css('.author-description::text'),
        }
