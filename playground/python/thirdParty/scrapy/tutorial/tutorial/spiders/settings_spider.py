#!/usr/bin/env python
"""
NAME:
 settings_spider.py

DESCRIPTION
 Spider to print scrapy settings

DOCUMENATION
 https://docs.scrapy.org/en/latest/topics/settings.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import scrapy


# ------------------------ SETTINGS SPIDER CLASS ---------------------------- #
class SettingsSpider(scrapy.Spider):
    """
    Settings Spider Class
    """
    name       = "settings"
    start_urls = ["http://www.example.com"]

    def parse(self, response):
        """
        Parse web response
        """
        self.logger.info("Existing settings:\n{}".format(
            pprint.pformat(self.settings.attributes)))
