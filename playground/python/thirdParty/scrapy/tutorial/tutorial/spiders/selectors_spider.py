#!/usr/bin/env python
"""
NAME:
 selectors_spider.py

DESCRIPTION
 Spider to document scrapy selectors

DOCUMENATION
 https://docs.scrapy.org/en/latest/topics/selectors.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import scrapy


# ------------------------ SELECTORS SPIDER CLASS --------------------------- #
class SelectorsSpider(scrapy.Spider):
    """
    Selectors Spider Class
    """
    name            = "selectors"
    allowed_domains = ["scrapy.org"]
    start_urls      = ["https://doc.scrapy.org/en/latest/"
                       "_static/selectors-sample1.html"]

    def parse(self, response):
        """
        Parse Response

        HTML Code
          <html>
          <head>
           <base href='http://example.com/' />
           <title>Example website</title>
          </head>
          <body>
           <div id='images'>
            <a href='image1.html'>Name: My image 1 <br /><img src='image1_thumb.jpg' /></a>
            <a href='image2.html'>Name: My image 2 <br /><img src='image2_thumb.jpg' /></a>
            <a href='image3.html'>Name: My image 3 <br /><img src='image3_thumb.jpg' /></a>
            <a href='image4.html'>Name: My image 4 <br /><img src='image4_thumb.jpg' /></a>
            <a href='image5.html'>Name: My image 5 <br /><img src='image5_thumb.jpg' /></a>
           </div>
          </body>
         </html>
        """
        # Response
        self.logger.info("Resonse Data:")
        self.logger.info("Body:\n{}".format(response.body))

        # Title
        titleSel = response.xpath('//title/text()')
        self.logger.info("Title Selector: {}".format(titleSel))
        self.logger.info("Title: {}".format(titleSel.extract()))
        self.logger.info("Title: {}".format(titleSel.extract_first()))

        # Extract all link text
        linkTextSel = response.xpath('//div[@id="images"]/a/text()')
        self.logger.info("Link Text: {}".format(linkTextSel.extract()))

        # XPath returns None if it doesn't exist
        self.logger.info("None is None: {}".format(
            response.xpath('//div[@id="not-exists"]/text()').extract_first() is None))

        # Default
        self.logger.info("Not found: {}".format(
            response.xpath('//div[@id="not-exists"]/text()').extract_first(default='not-found')))

        # Print URLs
        urls = response.xpath('//a[contains(@href, "image")]/@href').extract()
        self.logger.info("URLs: {}".format(urls))

        # Print SRCs
        srcs = response.xpath('//a[contains(@href, "image")]/img/@src').extract()
        self.logger.info("SRCs: {}".format(srcs))

        # Regular Expressions
        names = response.xpath('//a[contains(@href, "image")]/text()').re(r"Name:\s*(.*)")
        firstName = response.xpath('//a[contains(@href, "image")]/text()').re_first(r"Name:\s*(.*)")
        self.logger.info("Names: {}".format(names))
        self.logger.info("First name: {}".format(firstName))

        # Relative XPaths
        # Must use './/' syntax
        divs = response.xpath('//div')
        for a in divs.xpath('.//a'):
            self.logger.info("Div a: {}".format(a.extract()))

        # Variables in xpath expressions
        firstLinkText = response.xpath('//div[@id=$id]/a/text()', id='images').extract_first()
        self.logger.info("FirstLinkText: {}".format(firstLinkText))

        # EXSLT extensions
        # ---------------------------------------------------------------------
        # Preregistered namespaces
        #    re   - re:test(), re:match(), re:replace()
        #           http://exslt.org/regexp/index.html
        #    set  - set:difference(), set:intersection(), set:distinct()
        #           set:has-same-node(), set:leading, set:trailing
        #           http://exslt.org/set/index.html
        #  re:test() Example
        #    HTML
        #      <div>
        #          <ul>
        #              <li class="item-0"><a href="link1.html">first item</a></li>
        #              <li class="item-1"><a href="link2.html">second item</a></li>
        #              <li class="item-inactive"><a href="link3.html">third item</a></li>
        #              <li class="item-1"><a href="link4.html">fourth item</a></li>
        #              <li class="item-0"><a href="link5.html">fifth item</a></li>
        #          </ul>
        #      </div>
        #
        #    >>> sel = Selector(text=doc, type="html")
        #    >>> sel.xpath('//li//@href').extract()
        #    [u'link1.html', u'link2.html', u'link3.html', u'link4.html', u'link5.html']
        #    >>> sel.xpath('//li[re:test(@class, "item-\d$")]//@href').extract()
        #    [u'link1.html', u'link2.html', u'link4.html', u'link5.html']
        #
        #
        # Difference between //node[1] and (//node)[1]
        # ---------------------------------------------------------------------
        #   //node[1]     - Selects all nodes occuring first under their respective parents
        #   (//node)[1]   - Selects all nodes in the document, and then gets only the first
        #
        #
        # Querying Class, use CSS
        # ---------------------------------------------------------------------
        # XPath selection of class that will miss elements with other classes:
        #   @class='someclass'
        # XPath selection of class that may contain different classname which same the same string
        #   contains(@class, 'someclass')
        # XPath correct way to select elements by class is too verbose:
        #    *[contains(concat(' ', normalize-space(@class), ' '), ' someclass ')]
        #
        # Scrapy allows for chaining selectors. Select by class using CSS, then switch to XPath
        #   sel.css('.shout').xpath('./time/@datetime').extract()
