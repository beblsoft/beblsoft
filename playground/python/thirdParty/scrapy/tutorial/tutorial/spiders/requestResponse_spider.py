#!/usr/bin/env python
"""
NAME:
 requestResponse_spider.py

DESCRIPTION
 Request Response Spider

BIBLIOGRAPHY
 Code from https://docs.scrapy.org/en/latest/topics/selectors.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import pprint
import scrapy


# ------------------------ REQUEST RESPONSE SPIDER CLASS -------------------- #
class RequestResponseSpider(scrapy.Spider):
    """
    Request Response Spider Class
    """
    name            = "requestresponse"
    allowed_domains = ["scrapy.org"]
    start_urls      = ["https://doc.scrapy.org/en/latest/"
                       "_static/selectors-sample1.html"]

    def parse(self, response):
        """
        Parse Response
        """
        request = response.request

        # Request Attributes
        self.logger.info("Request Attributes:")
        self.logger.info("URL: {}".format(request.url))
        self.logger.info("Callback: {}".format(request.callback))
        self.logger.info("Method: {}".format(request.method))
        self.logger.info("Meta: {}".format(request.meta))
        self.logger.info("Body:\n {}".format(request.body))
        self.logger.info("Headers:\n {}".format(pprint.pformat(request.headers)))
        self.logger.info("Cookies: {}".format(request.cookies))
        self.logger.info("Encoding: {}".format(request.encoding))
        self.logger.info("Priority: {}".format(request.priority))
        self.logger.info("Don't Filter: {}".format(request.dont_filter))
        self.logger.info("Errback: {}".format(request.errback))
        self.logger.info("Flags: {}".format(request.flags))

        # Response Attributes
        self.logger.info("Response Attributes:")
        self.logger.info("URL: {}".format(response.url))
        self.logger.info("Status: {}".format(response.status))
        self.logger.info("Headers:\n {}".format(pprint.pformat(response.headers)))
        self.logger.info("Body:\n{}".format(response.body))
