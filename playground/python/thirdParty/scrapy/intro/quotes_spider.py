#!/usr/bin/python
"""
 NAME:
  quotes_spider.py

 DESCRIPTION
  Intro Spider
  Scrapes quotes from website: http://quotes.toscrape.com

 BIBLIOGRAPHY
  https://docs.scrapy.org/en/latest/intro/overview.html

 USAGE
  Create virtualenv        : virtualenv -p /usr/bin/python2.7 venv
  Enter virtualenv         : source venv/bin/activate
  Install requirements     : pip2 install -r requirements.txt
  Run program              : scrapy runspider quotes_spider.py -o quotes.json
"""

# ----------------------------- IMPORTS ------------------------------------- #
import scrapy


# ----------------------------- QUOTE SPIDER CLASS -------------------------- #
class QuotesSpider(scrapy.Spider):
    """
    Quote Spider Class
    """

    name = "quotes"
    start_urls = [
        'http://quotes.toscrape.com/tag/humor/',
    ]

    def parse(self, response):
        """
        parse
        """
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span.text::text').extract_first(),
                'author': quote.xpath('span/small/text()').extract_first(),
            }

        next_page = response.css('li.next a::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)
