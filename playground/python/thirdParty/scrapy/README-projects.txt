OVERVIEW
===============================================================================
Scrapy Projects Documentation


PROJECT: INTRO
===============================================================================
- Description:
  * Hello world scraping quote site
  * https://doc.scrapy.org/en/latest/intro/overview.html
- To Run
  * Install scrapy           : see ./README.txt
  * Enter directory          : cd intro
  * Run program              : scrapy runspider quotes_spider.py -o quotes.json
  * Check output             : cat quotes.json


PROJECT: TUTORIAL
===============================================================================
- Description:
  * Scrapy tutorial program
  * https://doc.scrapy.org/en/latest/intro/tutorial.html
- To Run
  * Install scrapy               : see ./README.txt
  * Enter directory              : cd tutorial
  * Dump pages                   : scrapy crawl quotes1
  * Dump pages (simplified)      : scrapy crawl quotes2
  * Parse metadata from first 2
    pages                        : scrapy crawl quotes3
  * Parse metadata from all
    pages                        : scrapy crawl quotes4
  * Parse author metadata from
    all pages                    : scrapy crawl authors
  * Run parse a single author
    page, verifying spider       : scrapy parse --spider=authors -c parse_author \
                                   -d 1 http://quotes.toscrape.com/author/Albert-Einstein/
  * Parse quotes that are tagged
    with humor                   : scrapy crawl quotes5 -a tag=humor
  * Demonstrate selector usage   : scrapy crawl selectors
  * Print request and response
    metadata                     : scrapy crawl requestresponse
  * View scrapy settings         : scrapy crawl settings
  * Item object demonstration    : ./tutorial/items.py
  * Run sample contract check    : scrapy check
  * List all contract checks     : scrapy check -l


PROJECT: SCRAPYBOOK
==============================================================================
- Description:
  * Source code from "Learning Scrapy" by Dimitris Kouzis-Loukas
  * Git repo: https://github.com/scalingexcellence/scrapybook.git