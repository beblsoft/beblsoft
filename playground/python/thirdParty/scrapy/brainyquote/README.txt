https://www.brainyquote.com

To traverse authors, A..Z:

https://www.brainyquote.com/authors/a
  https://www.brainyquote.com/authors/abraham_lincoln
    https://www.brainyquote.com/quotes/abraham_lincoln_101343 -> a quote

    <div class="bq_fq bq_fq_lrg qt-fnt bqcpx bq-smpl-qt" data-sai-includehighlight="true">
    <p class="qt_101343">Nearly all men can stand adversity, but if you want to test a man&#39;s character, give him power.</p>
    <p class="bq_fq_a">
    <a href="/authors/abraham_lincoln" class="qa_101343 oncl_a">Abraham Lincoln</a>

    https://www.brainyquote.com/quotes/abraham_lincoln_145871

    <div class="bq_fq bq_fq_lrg qt-fnt bqcpx bq-smpl-qt" data-sai-includehighlight="true">
    <p class="qt_145871">All that I am, or hope to be, I owe to my angel mother.</p>
    <p class="bq_fq_a">
    <a href="/authors/abraham_lincoln" class="qa_145871 oncl_a">Abraham Lincoln</a>    


https://www.brainyquote.com/authors/b
...
https://www.brainyquote.com/authors/z


--------------------------------------------------------------------------------
->A to Z index of Authors
https://www.brainyquote.com/authors/a ... z


<div class="letter-navbar qs-blk" style="letter-spacing: 2px">
<div class="container letter-container">
<span class="body bq-tn-letters"> <span id="bq-auth-lbl" class="bq-tn-wrap">Authors:</span>
<span class="bq-tn-wrap">
<a href="/authors/a">A</a>
<a href="/authors/b">B</a>
<a href="/authors/c">C</a>
<a href="/authors/d">D</a>
<a href="/authors/e">E</a>
<a href="/authors/f">F</a>
<a href="/authors/g">G</a>
<a href="/authors/h">H</a>
<a href="/authors/i">I</a>
<a href="/authors/j">J</a>
<a href="/authors/k">K</a>
<a href="/authors/l">L</a>
<a href="/authors/m">M</a>
</span>
        <span class="bq-tn-wrap">
<a href="/authors/n">N</a>
<a href="/authors/o">O</a>
<a href="/authors/p">P</a>
<a href="/authors/q">Q</a>
<a href="/authors/r">R</a>
<a href="/authors/s">S</a>
<a href="/authors/t">T</a>
<a href="/authors/u">U</a>
<a href="/authors/v">V</a>
<a href="/authors/w">W</a>
<a href="/authors/x">X</a>
<a href="/authors/y">Y</a>
<a href="/authors/z">Z</a>
--------------------------------------------------------------------------------
->Author Selector
https://www.brainyquote.com/authors/abraham_lincoln

<div class="block-sm-holder-az">
<a href="/authors/abraham_lincoln" class="block-sm-az">
<span class="link-name">Abraham Lincoln</span>
</a>
</div>
--------------------------------------------------------------------------------
->Quote
https://www.brainyquote.com/quotes/abraham_lincoln_132309

<meta name="description" content="&quot;I do the very best I know how - the very best I can; and I mean to keep on doing so until the end.&quot; - Abraham Lincoln quotes from BrainyQuote.com">

<a href="/quotes/abraham_lincoln_132309" class="b-qt qt_132309 oncl_q" title="view quote">I do the very best I know how - the very best I can; and I mean to keep on doing so until the end.</a>



--------------------------------------------------------------------------------
view-source:https://www.brainyquote.com/authors/abraham_lincoln
<h1 class="quoteListH1 ">Abraham Lincoln Quotes</h1>

<a href="/quotes/abraham_lincoln_101343" class="b-qt qt_101343 oncl_q" title="view quote">Nearly all men can stand adversity, but if you want to test a man&#39;s character, give him power.</a>

--------------------------------------------------------------------------------
Each quote:
view-source:https://www.brainyquote.com/authors/abraham_lincoln

<div id="qpos_1_2" class="m-brick grid-item boxy bqQt">
    <div class="">
        <a title="view image" href="/quotes/abraham_lincoln_101343" class="oncl_q"><img class=" zoomc bqpht" src="/photos_tr/en/a/abrahamlincoln/101343/abrahamlincoln1.jpg" alt="Nearly all men can stand adversity, but if you want to test a man&#39;s character, give him power. - Abraham Lincoln"></a>
***        <a href="/quotes/abraham_lincoln_101343" class="b-qt qt_101343 oncl_q" title="view quote">Nearly all men can stand adversity, but if you want to test a man&#39;s character, give him power.</a>
        <a href="/quotes/abraham_lincoln_101343" class="bq-aut qa_101343 oncl_q" title="view quote">Abraham Lincoln</a>
    </div>
    <div class="kw-box">
        <a href="/topics/adversity" class="oncl_k" data-idx="0">Adversity</a>,
        <a href="/topics/character" class="oncl_k" data-idx="1">Character</a>,
        <a href="/topics/man" class="oncl_k" data-idx="2">Man</a>
    </div>
    <div class="qbn-box">
        <span class="bqAddQuote bqAddQuoteLg" style="float:right">
<span class="fa favHeart bqMIcon fa-heart-o"></span><span class="fa favAddToCol bqMIcon fa-plus-square-o"></span>
        </span>
        <div class="sh-box">
            <a href="/share/fb/101343?sr=ql" class="fa-stack fa-lg sh-fb" rel="nofollow" target="_blank">
<i class="fa fa-circle fa-stack-2x"></i>
<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
</a>
            <a href="/share/tw/101343?sr=ql&ti=Abraham+Lincoln+Quotes" class="fa-stack fa-lg sh-tw" rel="nofollow" target="_blank">
<i class="fa fa-circle fa-stack-2x"></i>
<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
</a>
            <a href="/share/pn/101343?sr=ql&ti=Abraham+Lincoln+Quotes+-+BrainyQuote" class="fa-stack fa-lg sh-pi" rel="nofollow" target="_blank">
<i class="fa fa-circle fa-stack-2x"></i>
<i class="fa fa-pinterest-p fa-stack-1x fa-inverse"></i>
</a>
        </div>
    </div>
</div>

<div id="qpos_1_3" class="m-brick grid-item boxy bqQt">

starts w/ qpos_1_2, then qpos_1_3, etc. but ads are inserted into array so not always a quote

if see one of these, skip the index:

<div id='div-gpt-ad-1418667263920-3_edge' class="mbl_qtbox">
<div id='div-gpt-ad-1418667263920-3' class="bq_ad_320x250_multi bqAdCollapse "></div>


Possible algorithm:

https://www.brainyquote.com/authors/abraham_lincoln
    get all bqQt selectors 

Loop over the titles like so:

xpath_jobs_sel = './/div[contains(@class,"listResults")]//a[@class="job-link"]'
for sel in response.xpath(xpath_jobs_sel):
    title = sel.xpath('./@title').extract()
The good thing is, you can nest XPaths. The first XPath yields a list with Selectors. Notice the dot at the beginning of the inner statement. It means that this statement is relative to the first!


