#!/usr/bin/env python
"""
 NAME:
  goodreads_spider.py

 DESCRIPTION
  Intro Spider that scrapes famous quotes from website: https://www.goodreads.com/quotes

"""

# ----------------------------- IMPORTS ------------------------------------- #
import scrapy  # pylint: disable=E0401


class GoodReadsSpider(scrapy.Spider):
    """
    GoodReadsSpider Class
    """

    name = "goodreads"
    start_urls = [
        'http://https://www.goodreads.com/quotes/inspirational',
    ]

    def parse(self, response):
        """
        parse
        """
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span.text::text').extract_first(),
                'author': quote.xpath('span/small/text()').extract_first(),
            }

        next_page = response.css('li.next a::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)
