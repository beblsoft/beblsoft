OVERVIEW
===============================================================================
Scrapy is an open source and collaborative framework for extracting the data
from websites in a fast, simple, yet extensible way. It includes the following
features:
  - Async processing and requests.
  - Interactive shell console.
  - CSS and XPath parsers.
  - Supports generation feed exports, json, csv, xml.
  - Telnet console to hook into console running scrapy, introspect and debug crawler.
Relevant URLs for scrapy development:
  - Home                   : https://scrapy.org/
  - Documentation          : https://docs.scrapy.org/en/latest/index.html
  - Source                 : https://github.com/scrapy/scrapy
  - Tutorial               : https://docs.scrapy.org/en/latest/intro/tutorial.html
  - Quote site to scrape   : http://quotes.toscrape.com/
  - Scrapy Book            : http://scrapybook.com/
  - Scrapy Book Source     : https://github.com/scalingexcellence/scrapybook
Installation steps:
  - Install dependencies   : sudo apt-get install python-dev python-pip libxml2-dev \
                             libxslt1-dev zlib1g-dev libffi-dev libssl-dev
  - Install with python2   : sudo pip2 install scrapy
  - Install with python3   : Failed


COMMAND LINE INTERFACE
===============================================================================
- Scrapy is controlled by the scrap command line tool.
- Configuration
  Global                        : /etc/scrapy.cfg
  User                          : ~/.config/scrapy.cfg
  Project [in project root]     : scrapy.cfg
- Environment Variables         : SCRAPY_SETTINGS_MODULE, SCRAPY_PROJECT, SCRAPY_PYTHON_SHELL
- Help                          : scrapy -h
- Command help                  : scrapy <command> -h
- Create tutorial project       : scrapy startproject tutorial
- Project file tree
  tutorial/                       - top level directory
    scrapy.cfg                    - deploy configuration file
    tutorial/                     - project's Python module, you'll import your code from here
        items.py                  - project items definition file
        middlewares.py            - project middlewares file
        pipelines.py              - project pipelines file
        settings.py               - project settings file
        spiders/                  - a directory where you'll later put your spiders
- Create a spider               : scrapy genspider quotes quotes.toscrape.com
- Run a quotes spider           : scrapy crawl quotes
- Output to file                : scrapy crawl quotes -o quotes-humor.json
- Set spider argument           : scrapy crawl quotes -a tag=humor
- Run all contract checks       : scrapy check
- List all contract checks      : scrapy check -l
- List all project spiders      : scrapy list
- Edit a spider using EDITOR
  environment variable          : scrapy edit quotes
- Download the given URL
  using the scrapy downloader   : scrapy fetch http://quotes.toscrape.com/page/1/
- Open page in browser as
  scrapy spider would see it    : scrapy view http://quotes.toscrape.com/page/1/
- Run interactive shell         : scrapy shell 'http://quotes.toscrape.com/page/1/'
- Fetch URL and parse with the
  spider that handles it        : scrapy parse http://www.example.com/ -c parse_item
- Get the value of a scrapy
  setting                       : scrapy settings --get BOT_NAME
- Run a spider self-contained
  in a Python file, without
  having to create a project    : scrapy runspider myspider.py
- Print scrapy version          : scrapy version
- Run benchmark test            : scrapy bench
- Create custom project cmds    : COMMANDS_MODULE = "mybot.commands"
                                  https://github.com/scrapy/scrapy/tree/master/scrapy/commands


SHELL
===============================================================================
- Scrapy shell is an interactive shell where you can try to debug scraping
  code very quickly, without having ro run the spider
- Leverages IPython (if installed), for autocompletion and colorized-output among
  other things
- Run scrapy shell on URL        : scrapy shell 'http://quotes.toscrape.com/page/1/'
                                   scrapy shell ./path/to/file.html
                                   scrapy shell file:///absolute/path/to/file.html
                                   [ ... Scrapy log here ... ]
                                   [s] Available Scrapy objects:
                                   [s]   scrapy     scrapy module (contains scrapy.Request, scrapy.Selector, etc)
                                   [s]   crawler    <scrapy.crawler.Crawler object at 0x7fa91d888c90>
                                   [s]   item       {}
                                   [s]   request    <GET http://quotes.toscrape.com/page/1/>
                                   [s]   response   <200 http://quotes.toscrape.com/page/1/>
                                   [s]   settings   <scrapy.settings.Settings object at 0x7fa91d888c10>
                                   [s]   spider     <DefaultSpider 'default' at 0x7fa91c8af990>
                                   [s] Useful shortcuts:
                                   [s]   shelp()           Shell help (print this help)
                                   [s]   fetch(req_or_url) Fetch request (or URL) and update local objects
                                   [s]   view(response)    View response in a browser
- Print help with available
  commands                       : shelp()
- Fetch a new response           : fetch(url[, redirect=True])
- Open the given response in
  local web browser              : view(response)
- Available objects
     Current crawler             : crawler
     Spider known to handle URL  : spider
     Request of last fetched page: request
     Last page response          : response
     Scrapy settings             : settings


ARCHITECTURE
===============================================================================
- Scrapy architecture diagram:
                         ------------------
                         |    SPIDERS     |
                         | xxxx xxxx xxxx |
                         ------------------              INTERNET
                             |   ^   |                      |
                             7   6   1                      v
                             v   |   v                      |
    ---------------          ----------             -----------------
    |    ITEM     |          | ENGINE |-->--4-->----|   DOWNLOADER  |
    |  PIPELINES  |----<--8--|        |--<--5--<----|               |
    ---------------       |  ----------             -----------------
                          v     ^    |
                          |     3    2
                          |     |    V
                          ---------------
                          |  SCHEDULER  |
                          |             |
                          ---------------
- Diagram steps explained:
   1. Engine gets initial request from spiider
   2. Engine schedules request
   3. Scheduler returns next request
   4. Engine sends requests to downloader
   5. After page downloaded, downloader sends response back to Engine
   6. Engine sends response to spider
   7. Spider processes the response and returns scraped items and new requests
   8. Engine sends processed items to Item Pipelines and sends requests to
      scheduler
   9. Process repeats at step 1 until no more requests.


SPIDERS
===============================================================================
- Spiders are classes which define how a certain site (or sites) will be scraped.
  This includes how to follow links and extract structured data from their
  pages.
- The crawling cycle is as follows:
  1. Generate initial requests to crawl, and specify callback function to parse
     See. start_requests(), start_urls, parse
  2. Callback function parses web response and returns dict, Item object,
     Request objects, or iterable of these objects
- Spider Classes
  scrapy.spiders.Spider              : Base spider class
      name                           : spider name, unique within project
      allowed_domains                : domains the spider is allowed to crawl
      start_urls                     : list of urls to start crawl
      logger                         : spider python logger
      start_requests()               : return iterable of request, can be a generator
      parse()                        : default callback to parse responses
                                       must return iterable of Request, dict, or Items
      log()                          : wrapper around self.logger.log
      closed()                       : called when the spider closes
  scrapy.spiders.CrawlSpider         : Follow links by defining a set of rules
      rules                          : List of one or more Rule objects
                                       Each Rule defines certain behavior for crawling the site
      parse_start_url()              : Called for start_urls responses
  scrapy.spiders.XMLFeedSpider       : Parse XML feeds by interating through by certain node name
      iterator                       : String to define iterator
                                       One of: "iternodes", "html", "xml"
      itertag                        : String with name of node (or element) to iterate inside
      namespaces                     : list of (prefix, uri) tuples which define namespaces available
  scrapy.spiders.CSVFeedSpider       : Similar to XMLFeedSpider, except iterate over rows
  scrapy.spiders.SitemapSpider       : Crawl a site by discovering URLs using Sitemaps
- Spider Arguments
  Pass arguments to spiders on the
  command line                       : scrapy crawl myspider -a category=electronics
  These arguments can be accessed as
  spider attributes.                 : self.category


SELECTORS
===============================================================================
- Selectors select certain parts of HTML document specified by either
  XPath or CSS expression
- XPath                              : language for selecting nodes in XML documents
- CSS                                : language for applying styles to HTML documents
- Selector Classes
  scrapy.selector.Selector           : Selector object
      xpath()                        : Find nodes matching xpath query and return SelectorList
      css()                          : Find nodes matching css and return SelectorList
      extract()                      : Serialize and return matched nodes
      re()                           : Apply regex and return list of unicode strings
      register_namespace()           : Register the given namespace to be used in selector
      remove_namespaces()            : Remove all namespaces
  scrapy.selector.SelectorList       : Subclass of list with selector methods


ITEMS
===============================================================================
- Terminology:
  Items                   : simple containers used to collect scraped data. They provide a
                            dictionary-like API with a convenient syntax for declaring fields
  Item fields             : can specify any kind of metadata. The goal of field is to define all
                            metadata in one place.
  Item loaders            : provide a convenient mechanism for populating scraped items
    input processor       : processes the extracted data as soon as it’s received
                            (through the add_xpath(), add_css() or add_value() methods)
                            and the result of the input processor is collected and kept inside
                            the ItemLoader
                            MapCompose(unicode.title, unicode.strip)
    output processor        Called after ItemLoader.load_item() is called. Result
                            of output processor is stored in item
    scrapy.loader.processors.
    Identity              : Returns the original values
    TakeFirst             : Returns the first non-null value
    Join(separator)       : Returns the values joined by separator
    Compose(*fns)         : Composition of the given functions
    MapCompose(*fns)      : Run functions all all inputs
    SelectJmes(json_path) : Quiers value using the json path
  Item pipelines          : post-processes item after it has been scraped
                            Example uses: cleansing, validating, checking for duplicates
                            storing persistently
                            To activate: must declare pipeline component in ITEM_PIPELINES
                            setting


FEED EXPORTS
===============================================================================
- Feed exports all scrapy spiders to store scraped data in an export file that can be
  consumed by other systems
- Formats supported out of the box
    JSON                  : Java Script Object Notation     - JsonItemExporter
    JSON Lines            : JSON Lines                      - JsonLinesItemExporter
    CSV                   : Comma Separated Value           - CsvItemExporter
    XML                   : eXtensible Markup Language      - XmlItemExporter
    Pickle                : Python object serializer        - PickleItemExporter
    Marshal               : Marshal format                  - MarshalItemExporter
- Storage Backends:
  Local filesystem        : file:///tmp/export.csv
  FTP                     : ftp://user:password@ftp.example.com/scraping/feeds/%(name)s/%(time)s.json
  S3                      : s3://mybucket/scraping/feeds/%(name)s/%(time)s.json
  Standard output         : stdout:
- Storage URI parameters
  %(time)s                : replaced by timestamp
  %(name)s                : replaced by spider name
  %(*)s                   : replaced by spider attribute spider.*
- Settings to control feed exports
  FEED_URI                : default URL for export feed
  FEED_FORMAT             : serialization format
  FEED_EXPORT_ENCODING    : feed encoding
  FEED_STORAGES           : dict containing additional feed storage backends
  FEED_EXPORTERS          : dict containing additional exporters supported
  FEED_STORE_EMPTY        : whether to store empty feeds
  FEED_EXPORT_FIELDS      : list of fields to export
  FEED_EXPORT_INDENT      : amount of spaces to indent each level


REQUESTS, RESPONSES
===============================================================================
- Scrapy uses requests and responses to crawl sites. Requests are generated in
  spiders and pass across the system until they reach the Downloader, which
  executes the request and returns a Response which travels back to the spider
- Request objects
  scrapy.http.Request      : Base request object
      url                  : string containing the URL of the request
      method               : string representing HTTP method
      headers              : dictionary-like object contain headers
      body                 : string that contains the request body
      meta                 : dict that contains metadata for the request
      copy()               : return request which is a copy of this request
      replace()            : return request with same members, except for thos replaced
  scrapy.http.FormRequest  : Contains formdata dict in constructor
- Response objects
  scrapy.http.Response     : Base response object
      url                  : string containing url of response
      status               : integer containing HTTP status
      headers              : dict with response headers
      body                 : response body in bytes
      request              : request object that generated the response
      meta                 : shortcut to request.meta
      flags                : list of flags for response
      copy()               : return a new response that is a copy
      replace()            : return a response with the same members, except for
                             those members overridden
      urljoin              : construct absolute url using response's url and
                             relative url
      follow               : return a request to follow a link url
  scrapy.http.TextResponse : supports different encodings
  scrapy.http.HtmlResponse : auto-discovering support for HTML
  scrapy.http.XmlResponse  : auto-discovering for XML


DEBUGGING
===============================================================================
- Most common techniaues for debugging spiders:
- Parse Command                 : Check behavior of spider at the method level
                                  Does not allow debugging code inside method
  Check a single parse method   : scrapy parse --spider=authors -c parse_author -d 1 http://quotes.toscrape.com/author/Albert-Einstein/
  Check items scraped from
  start_url                     : scrapy parse --spider=myspider -d 3 'http://example.com/page1'
- Scrapy Shell                  : Invoke shell from spiders to inspect responses
                                  scrapy.shell.inspect_response(response, self)
- Open in browser               : Open in browser from spiders to view response
                                  scrapy.utils.response.open_in_browser(response)
- Logging                       : Log the bad case in spider
                                  self.logger.warning("No item recieved")
- Memory Leaks                  : trackref(), guppy()


COMMON PRACTICES
===============================================================================
- Running spider from a python
  script                        : process = scrapy.crawler.CrawlerProcess(get_project_settings())
                                  process.crawl(MySpider)
                                  process.start()
- Running multiple spiders
  in the same process           : process = scrapy.crawler.CrawlerProcess()
                                  process.crawl(MySpider1)
                                  process.crawl(MySpider2)
                                  process.start()
- Distributed Crawls            : Paritition URL to crawl and send them to same
                                  spider, each on a different machine
- Tips to avoid getting banned  : Rotate user agent from well-known browsers
                                  Disable cookies
                                  Use download delays
                                  If possible, use Google cache to fetch pages
                                  Use a pool of rotating IPs Free = Tor project, scrapoxy. Paid = ProxyMesh
                                  Use a distributed downloader. Ex. Crawlera


CONTRACTS
===============================================================================
- Scrapy includes a built in mechanism to test spiders, call contracts
- Example contract              : def parse(self, response):
                                      """
                                      This function parses a sample response. Some contracts are mingled
                                      with this docstring.

                                      @url http://www.amazon.com/s?field-keywords=selfish+gene
                                      @returns items 1 16
                                      @returns requests 0 0
                                      @scrapes Title Author Year Price
                                      """
- Built-in contracts
  @url url                      : scrapy.contracts.default.UrlContract
                                  Set url used when checking contract conditions
  @returns items(s) | requests
    [min [max]]                 : scrapy.contracts.default.ReturnsContract
                                  Set lower and optionally upper bound for requests
                                  or items returned
  @scrapes field1 field2 ...    : scrapy.contracts.default.ScrapesContract
                                  Checks that all the items returned by the
                                  callback have specified fields
- Custom Contracts              : Extend scrapy.contracts.contract
                                  Add contract to SPIDER_CONTRACTS setting


SETTINGS
===============================================================================
- Scrapy settings allow scrapy behaviour to be customized.
  Settings are a global namespace of key-value mappings.
  Setting names usually prefix the component they configure
- Designate settings using environment
  variable                             : SCRAPY_SETTINGS_MODULE.
                                         myproject.settings
- Settings populated through precedence. The precedence is listed from highest
  to lowest below:
  Command line options (highest)       : scrapy crawl myspider -s LOG_FILE=scrapy.log
  Settings per-spider                  : settings in Spider class
                                         custom_settings = { 'SOME_SETTING' : 'some value'}
  Project settings                     : settings.py file in project
  Default settings per-command         : default_settings attribute of command class
  Default global settings (lowest)     : scrapy.settings.default_settings
- Access settings                      : spider instance: self.settings
- Settings:
  AWS_ACCESS_KEY_ID                    : AWS access key
  AWS_SECRET_ACCESS_KEY                : AWS secret access key
  BOT_NAME                             : name of boy implemented by scrapy project
  CONCURRENT_ITEMS                     : max number of concurrent items being processed
                                         in parallel. Default=100
  CONCURRENT_REQUESTS                  : max number of concurrent requests that
                                         will be performed by the scrapy downloader.
                                         Default=16
  CONCURRENT_REQUESTS_PER_DOMAIN       : max number of concurrent requests per domain.
                                         Default=8
  CONCURRENT_REQUESTS_PER_IP           : max number of concurrent requests per ip
                                         If, 0, PER_DOMAIN used instead. Default=0
  DEFAULT_ITEM_CLASS                   : Default class for instantiating items
                                         in scrapy shell. Default=scrapy.item.Item
  DEFAULT_REQUEST_HEADERS              : Request headers for http requests
  DEPTH_LIMIT                          : Max depth allowed to crawl for any site
  DEPTH_PRIORITY                       : Breath First Order (BFO) - set to > 0,
                                         diminishes priority at lower depth
                                         Depth First Order (DFO) - set to < 0,
                                         increases priority at lower depth
  DEPTH_STATS                          : If True, collect depth stats. Default=False
  DNSCACHE_ENABLED                     : If True, enable DNS in-memory cache. Default=True
  DNS_TIMEOUT                          : Timout for processing DNS queries. Default=60
  DOWNLOADER                           : Downloader for crawling
                                         Default='scrapy.core.downloader.Downloader'
  DOWNLOADER_HTTPCLIENTFACTORY         : Twisted protocol.ClientFactory
  DOWNLOADER_CLIENTCONTEXTFACTORY      : Classpath to ContextFactory
  DOWNLOADER_CLIENT_TLS_METHOD         : One of: TLS, TLSv1.0, TLSv1.1, TLSv1.2, SSLv3
                                         Default=TLS
  DOWNLOADER_MIDDLEWARES               : Dict containing middlewares and their orders
  DOWNLOADER_MIDDLEWARES_BASE          : Dict containing the downloader middlewares enabled
  DOWNLOADER_STATS                     : Whether to enable downloader stats. Default=True
  DOWNLOAD_DELAY                       : Amount of time (in secs) that downloader should
                                         wait before downloading consecutive pages on same
                                         website. Default=0. DOWNLOAD_DELAY=0.25
  DOWNLOAD_HANDLERS                    : Dict containing request downloader handlers
  DOWNLOAD_HANDLERS_BASE               : Dict containing request downloader handlers
  DOWNLOAD_TIMEOUT                     : Amount of time (in secs) downloader should
                                         wait before timing out. Default=180
  DOWNLOAD_MAXSIZE                     : Max response size. Default=1024MB
  DOWNLOAD_WARNSIZE                    : Response size at which warnings issued. Default=32MB
  DOWNLOAD_FAIL_ON_DATALOSS            : Whether to fail on broken responses. Default=True
  DUPEFILTER_CLASS                     : Class used to detect and filter duplicate requests
  DUPEFILTER_BUG                       : If True, log all duplicate requests. Default=False
  EDITOR                               : Editor for parsing spiders with edit. Default=vi
  EXTENSIONS                           : Dict containing extensions enabled for project
  EXTENSIONS_BASE                      : Dict containing extensions enabled for project
  FEED_TEMDIR                          : Custom folder to save crawler temp files before
                                         uploading to S3 or FTP
  FTP_PASSIVE_MODE                     : If True, use passive mode for FTP transfers. Default=True
  FTP_PASSWORD                         : FTP password. Default=guest
  FTP_USER                             : FTP user. Default=anonymous
  ITEM_PIPELINES                       : Dict containing item pipelines to use, and their orders
                                         Orders in 0-1000 range. Lower orders are higher priority
  ITEM_PIPELINES_BASE                  : Dict containing item pipelines to use
  LOG_ENABLED                          : If True, enable logging. Default=True
  LOG_ENCODING                         : Log encoding. Default=utf-8
  LOG_FILE                             : File to log to. Default=None, goes to stderr used
  LOG_FORMAT                           : Log format. Default='%(asctime)s [%(name)s] %(levelname)s: %(message)s'
  LOG_DATEFORMAT                       : Log date format. Default='%Y-%m-%d %H:%M:%S'
  LOG_LEVEL                            : Minimum level to log. Default='DEBUG'
  LOG_STDOUT                           : If True, all standard output redirected to log. Default=False
                                         I.e. if True, print('hello') will go to log file
  LOG_SHORT_NAMES                      : If True, logs contain the root path. Default=False
  MEMDEBUG_ENABLED                     : If True, enable memory debugging. Default=False
  MEMDEBUG_NOTIFY                      : List of email addresses to send memory report
  MEMUSAGE_ENABLED                     : If True, enable memory usage extension. Default=True
  MEMUSAGE_LIMIT_MB                    : Max memory to allow in MB before shutting down scrapy
                                         If zero, no check performed. Default=0
  MEMUSAGE_CHECK_INTERNAL_SECONDS      : Interval to check memory (in seconds). Default=60.0
  MEMUSAGE_NOTIFY_MAIL                 : List of emails to notify if memory limit reached
  MEMUSAGE_WARNING_MB                  : Max ammount of memory to allow in MB before sending
                                         warning email. Ignored if zero. Default=0
  NEWSPIDER_MODULE                     : Module to create new spiders with genspider. Default=''
  RANDOMIZE_DOWNLOAD_DELAY             : If True, wait random amount of time between next
                                         request to decrease chance that crawler will be detected
                                         Default=True
  REACTOR_THREADPOOL_MAXSIZE           : Max limit for Twisted Reactor thrad pool size. Default=10
  REDIRECT_MAX_ITEMS                   : Max times a request can be redirected. Default=20
  REDIRECT_PRIORITY_ADJUST             : If > 0 give greated priority to redirected requests
                                         Default=+2
  RETRY_PRIORITY_ADJUST                : If > 0 give greater priority to retried request
                                         Default=-1
  ROBOTSTXT_OBEY                       : If True, scrapy will respect robots.txt policies.
                                         Default=False
  SCHEDULER                            : Scheduler for crawling
                                         Default='scrapy.core.scheduler.Scheduler'
  SCHEDULER_DEBUG                      : If True, log debug information about scheduler
                                         Default=False
  SCHEDULER_DISK_QUEUE                 : Disk queue used for scheduler
  SCHEDULER_MEMORY_QUEUE               : In-memory queue used by scheduler
  SCHEDULER_PRIORITY_QUEUE             : Priority queue used by scheduler
  SPIDER_CONTRACTS                     : Dict containing spider contracts for project
  SPIDER_CONTRACTS_BASE                : Dict containing spider contracts for project
  SPIDER_LOADER_CLASS                  : Class for loading spiders
  SPIDER_LOADER_WARN_ONLY              : If True, don't fail if spider fails to load.
                                         Default=False
  SPIDER_MIDDLEWARES                   : Dict containing spider middlewares
  SPIDER_MIDDLEWARES_BASE              : Dict containing spider middlewares
  SPIDER_MODULES                       : List of modules where scrapy will look for spiders
  STATS_CLASS                          : Class for collecting stats
  STATS_DUMP                           : If True, dump scrapy stats when spider finishes.
                                         Default=True
  STATSMAILER_RCPTS                    : Email list to send stats
  TELNETCONSOLE_ENABLED                : If True, enable telnet console. Default=True
  TELNETCONSOLE_PORT                   : Port range for telnet console. Default=[6023,6073]
  TEMPLATES_DIR                        : Templates dir inside scrapy module
  URLLENGTH_LIMIT                      : Max URL length to allow for crawled URLs. Default=2083
  USER_AGENT                           : User-Agent to use when crawling, unless overriden
                                         Default="Scrapy/VERSION (+https://scrapy.org)"


EXCEPTIONS
===============================================================================
- Scrapy exception classes
  scrapy.exceptions.DropItem         : Item pipeline stops processing item
  scrapy.exceptions.CloseSpider      : Raised when spider calls CloseSpider()
  scrapy.exceptions.DontCloseSpider  : Raised in spider_idle signal handler
                                       to preven spider from being closed
  scrapy.exceptions.IgnoreRequest    : Request should be ignored
  scrapy.exceptions.NotConfigured    : Raised by components to indicate that they
                                       will remain disabled
  scrapy.exceptions.NotSupported     : Raised to indicate an unsupported feature


SELECTOR GADGET
===============================================================================
- SelectorGadget is an open source tool that makes CSS selector generation and
  discovery on complicated sites a breeze.
- Site: http://selectorgadget.com/
- Usage
  1. Click on a page element that you would like your selector to match
     (it will turn green). SelectorGadget will then generate a minimal
     CSS selector for that element, and will highlight (yellow) everything that
     is matched by the selector.
  2. Click on a highlighted element to remove it from the selector (red)
  3. Click on an unhighlighted element to add it to the selector.
  Through this process of selection and rejection, SelectorGadget helps you come
  up with the perfect CSS selector for your needs.





