#!/usr/bin/env python3
"""
Game of Life Simulation
Wikipedia Description: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

Execution:
   ./life.py -h
   ./life.py -b [0-11]

Modules Needed:
   pygame: sudo apt-get install python-pygame
   numpy:  sudo apt-get install python-numpy
"""

# ---------------------------- IMPORTS --------------------------------------- #
import pygame
from optparse import OptionParser
import numpy as np
import math
import random


# ---------------------------- CLASSES --------------------------------------- #
#
# Class: Board
# ------------
# Hold board state, evolve board over generations
#
class Board():
    CELL_DEAD  = 0
    CELL_ALIVE = 1

    def __init__(self, matrix):
        self.matrix = matrix
        (self.rows, self.cols) = matrix.shape

    #Advance board 1 generation
    def evolve1(self):
        nextGen = np.full(self.matrix.shape, Board.CELL_DEAD, dtype=int)
        for row in range (0, self.rows):
            for col in range (0, self.cols):
                nextGen[row, col] = Board.getCellNextGen(self.matrix, row, col)
        self.matrix = nextGen

    #Advance board n generations
    def evolveN(self, nEvolutions):
        for evolution in (0, nEvolutions):
            self.evolve

    @staticmethod
    #Return number of neighboring cells alive
    def getCellNumNeighborsAlive(matrix, row, col):
        nNeighborsAlive = 0
        delta = [-1, 0, 1]
        #Iterate over 8 positions surrounding cell
        #If neighbor outside matrix, neighbor is considered dead
        for rowDelta in delta:
            for colDelta in delta:
                if ((colDelta == 0) and (rowDelta == 0)): continue
                r = row + rowDelta
                c = col + colDelta
                if ((r < 0) or (r >= len(matrix[:,0]))): continue
                if ((c < 0) or (c >= len(matrix[0,:]))): continue
                if(matrix[r,c] == Board.CELL_ALIVE):
                    nNeighborsAlive += 1
        return nNeighborsAlive

    @staticmethod
    #Return cell state in next generation, based on the current generation
    def getCellNextGen(matrix, row, col):
        curGen  = matrix[row,col]
        nextGen = Board.CELL_DEAD
        nNeighborsAlive = Board.getCellNumNeighborsAlive(matrix, row, col)
        assert ((curGen == Board.CELL_DEAD) or (curGen == Board.CELL_ALIVE))
        if (curGen == Board.CELL_ALIVE):
          if   (nNeighborsAlive <= 1): nextGen = Board.CELL_DEAD  #Under Population
          elif (nNeighborsAlive >= 4): nextGen = Board.CELL_DEAD  #Over  Population
          else                       : nextGen = Board.CELL_ALIVE #Just  Right
        else:
          if   (nNeighborsAlive == 3): nextGen = Board.CELL_ALIVE #Reproduction
          else                       : nextGen = Board.CELL_DEAD
        return nextGen

    #Draw board on input PyGame Screen
    def draw(self, screen, screenPixHeight, screenPixWidth, deadColor, aliveColor):
        screen.fill(deadColor)
        rowPixHeight = math.floor(screenPixHeight / self.rows)
        colPixWidth  = math.floor(screenPixWidth / self.cols)

        for row in range (0, self.rows):
            for col in range (0, self.cols):
                if (self.matrix[row,col] == 1):
                    pygame.draw.rect(screen, aliveColor,
                                     [col*colPixWidth, row*rowPixHeight, colPixWidth, rowPixHeight])
        pygame.display.flip()


# ---------------------------- CONSTANTS ------------------------------------- #
BLACK             = (   0,   0,   0)
WHITE             = ( 255, 255, 255)
SCREEN_PIX_HEIGHT = 400
SCREEN_PIX_WIDTH  = 1000
GENERATION_DELAY  = 50
TIME_DELAY        = 20


# ---------------------------- MAIN ------------------------------------------ #
if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-b", "--board", dest="boardNum", default=11, action="store", help="Board Number to Display [0-11]")
    (options, args) = parser.parse_args()

    matrix = board = None
    boardNum = int(options.boardNum)
    #Different Board Types
    #Block
    if (boardNum == 0):
        BOARD_ROWS = 4
        BOARD_COLS = 4
        matrix     = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1:3,1:3] = Board.CELL_ALIVE
    #Beehive
    elif (boardNum == 1):
        BOARD_ROWS = 5
        BOARD_COLS = 6
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1,2:4]   = Board.CELL_ALIVE
        matrix[2,(2,5)] = Board.CELL_ALIVE
        matrix[3,2:4]   = Board.CELL_ALIVE
    #Loaf
    elif (boardNum == 2):
        BOARD_ROWS = 6
        BOARD_COLS = 6
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1,2:4]   = Board.CELL_ALIVE
        matrix[2,(1,4)] = Board.CELL_ALIVE
        matrix[3,(2,4)] = Board.CELL_ALIVE
        matrix[4,3]     = Board.CELL_ALIVE
    #Board
    elif (boardNum == 3):
        BOARD_ROWS = 5
        BOARD_COLS = 5
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1,1:3]   = Board.CELL_ALIVE
        matrix[2,(1,3)] = Board.CELL_ALIVE
        matrix[3,2]     = Board.CELL_ALIVE
        board = Board(matrix)
    #Blinker
    elif (boardNum == 4):
        BOARD_ROWS = 5
        BOARD_COLS = 5
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[2,1:4]    = Board.CELL_ALIVE
    #Toad
    elif (boardNum == 5):
        BOARD_ROWS = 6
        BOARD_COLS = 6
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[2,2:5] = Board.CELL_ALIVE
        matrix[3,1:4] = Board.CELL_ALIVE
    #Beacon
    elif (boardNum == 6):
        BOARD_ROWS = 6
        BOARD_COLS = 6
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1:3,1:3] = Board.CELL_ALIVE
        matrix[3:5,3:5] = Board.CELL_ALIVE
    #Pulsar
    elif (boardNum == 7):
        SUB_BOARD_ROWS = 6
        SUB_BOARD_COLS = 6
        subMatrix = np.full([SUB_BOARD_ROWS, SUB_BOARD_COLS], Board.CELL_DEAD, dtype=int)
        subMatrix[0,(2,3)]       = Board.CELL_ALIVE
        subMatrix[1,(3,4)]       = Board.CELL_ALIVE
        subMatrix[2,(0,3,5)]     = Board.CELL_ALIVE
        subMatrix[3,(0,1,2,4,5)] = Board.CELL_ALIVE
        subMatrix[4,(1,3,5)]     = Board.CELL_ALIVE
        subMatrix[5,(2,3,4)]     = Board.CELL_ALIVE

        BOARD_ROWS = 17
        BOARD_COLS = 17
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[2:8,2:8]   = subMatrix
        matrix[2:8,9:15]  = subMatrix[:,::-1]
        matrix[9:15,2:8]  = subMatrix[::-1,:]
        matrix[9:15,9:15] = subMatrix[::-1,::-1]
    #Glider
    elif (boardNum == 8):
        BOARD_ROWS = 20
        BOARD_COLS = 20
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1,(1,3)] = Board.CELL_ALIVE
        matrix[2,(2,3)] = Board.CELL_ALIVE
        matrix[3,2] = Board.CELL_ALIVE
    #Space Ship
    elif (boardNum == 9):
        BOARD_ROWS = 10
        BOARD_COLS = 20
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[3,(4,5)] = Board.CELL_ALIVE
        matrix[4,(2,3,5,6)] = Board.CELL_ALIVE
        matrix[5,(2,3,4,5)] = Board.CELL_ALIVE
        matrix[6,(3,4)] = Board.CELL_ALIVE
    #R-pentomino
    elif (boardNum == 10):
        BOARD_ROWS = 100
        BOARD_COLS = 100
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[50,(52,53, 54)] = Board.CELL_ALIVE
        matrix[51,(51)] = Board.CELL_ALIVE
        matrix[52,(51)] = Board.CELL_ALIVE
    #Gosper Glider Gun
    elif (boardNum == 11):
        BOARD_ROWS = 11
        BOARD_COLS = 38
        matrix = np.full([BOARD_ROWS, BOARD_COLS], Board.CELL_DEAD, dtype=int)
        matrix[1,25] = Board.CELL_ALIVE
        matrix[2,(23,25)] = Board.CELL_ALIVE
        matrix[3,(13,14,21,22,35,36)] = Board.CELL_ALIVE
        matrix[4,(12,16,21,22,35,36)] = Board.CELL_ALIVE
        matrix[5,(1,2,11,17,21,22)] = Board.CELL_ALIVE
        matrix[6,(1,2,11,15,17,18,23,25)] = Board.CELL_ALIVE
        matrix[7,(11,17,25)] = Board.CELL_ALIVE
        matrix[8,(12,16)] = Board.CELL_ALIVE
        matrix[9,(13,14)] = Board.CELL_ALIVE
    #Random
    elif (boardNum == 12):
        BOARD_ROWS = 100
        BOARD_COLS = 100
        matrix = np.zeros([BOARD_ROWS, BOARD_COLS])
        for row in range(0, BOARD_ROWS):
            for col in range(0, BOARD_COLS):
                if (random.random() > .5): matrix[row,col] = Board.CELL_ALIVE
                else                     : matrix[row,col] = Board.CELL_DEAD

    board = Board(matrix)

    pygame.init()
    screen = pygame.display.set_mode([SCREEN_PIX_WIDTH, SCREEN_PIX_HEIGHT])
    pygame.display.set_caption("Game of Life")

    done       = False
    clock      = pygame.time.Clock()
    genCounter = 0
    while done == False:
        clock.tick(TIME_DELAY);

        if(genCounter*TIME_DELAY > GENERATION_DELAY):
            board.draw(screen, SCREEN_PIX_HEIGHT, SCREEN_PIX_WIDTH, BLACK, WHITE)
            board.evolve1()
            genCounter = 0
        genCounter += 1

        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                pygame.quit()
