OVERVIEW
===============================================================================
- PyGame Documentation
- Description                       : pygame is a free and open source python library for making multimedia
                                      applications like games built on top of the excellent SDL library
- Install ---------------------------
  * Install                         : pip3 install pygame
  * Uninstall                       : pip3 uninstall pygame
- Relevant URLs ---------------------
 * Home                             : https://www.pygame.org/wiki/about
 * Getting Started                  : https://www.pygame.org/wiki/GettingStarted
