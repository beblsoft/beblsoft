OVERVIEW
===============================================================================
PyGame Samples Documentation



SINGLE FILE SAMPLES
===============================================================================
- Description ----------------------: Single file samples
- Setup -----------------------------
  * Setup venv                      : virtualenv -p /usr/bin/python3.6 /tmp/pygameVenv
  * Enter venv                      : source  /tmp/pygameVenv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- Bouncing Rectangle ----------------
  * Description                     : Create a game with a bouncing rectangle
  * Run                             : ./rect.py

- Game of life ----------------------
  * Description                     : Start the game of life with different boards
  * Help                            : ./life.py -h
  * Board N                         : ./life.py -b [Board number 0-11]
                                      ./life.py -b 0
