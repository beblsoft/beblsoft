#!/usr/bin/env python3
"""
NAME
 info.py

DESCRIPTION
 Display process information from psutil

EXAMPLE OUTPUT
 $ ./info.py
 name             : info.py
 pid              : 14062
 ppid             : 22904
 exe              : /usr/bin/python3.6
 cmdline          : ['/usr/bin/python3.6', './info.py']
 create_time      : 1541435090.59
 status           : running
 cwd              : /home/jbensson/git/beblsoft/playground/python/integrations/psutil
 username         : jbensson
 uids             : puids(real=1000, effective=1000, saved=1000)
 terminal         : /dev/pts/18
 nice             : 0
 ionice           : pionice(ioclass=<IOPriority.IOPRIO_CLASS_NONE: 0>, value=4)
 io_counters      : pio(read_count=186, write_count=13, read_bytes=0, write_bytes=0, read_chars=915827, write_chars=538)
 num_ctx_switches : pctxsw(voluntary=0, involuntary=5)
 num_fds          : 4
 num_threads      : 1
 threads          : [pthread(id=14062, user_time=0.04, system_time=0.0)]
 cpu_times        : pcputimes(user=0.04, system=0.0, children_user=0.0, children_system=0.0)
 cpu_percent      : 0.0
 cpu_affinity     : [0, 1, 2, 3, 4, 5, 6, 7]
 cpu_num          : 4
 memory_info      : pmem(rss=12754944, vms=53350400, shared=6381568, text=3878912, lib=0, data=6119424, dirty=0)
 memory_full_info : pfullmem(rss=12754944, vms=53350400, shared=6381568, text=3878912, lib=0, data=6119424, dirty=0, uss=9392128, pss=9461760, swap=0)
 memory_percent   : 0.07615667536248401
 children         : []
 open_files       : []
 connections      : []
 is_running       : True
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
import psutil


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    p = psutil.Process(os.getpid())
    print("name             : {}".format(p.name()))
    print("pid              : {}".format(p.pid))
    print("ppid             : {}".format(p.ppid()))
    print("exe              : {}".format(p.exe()))
    print("cmdline          : {}".format(p.cmdline()))
    # print("environ          : {}".format(p.environ()))
    print("create_time      : {}".format(p.create_time()))
    print("status           : {}".format(p.status()))
    print("cwd              : {}".format(p.cwd()))
    print("username         : {}".format(p.username()))
    print("uids             : {}".format(p.uids()))
    print("terminal         : {}".format(p.terminal()))
    print("nice             : {}".format(p.nice()))
    print("ionice           : {}".format(p.ionice()))
    print("io_counters      : {}".format(p.io_counters()))
    print("num_ctx_switches : {}".format(p.num_ctx_switches()))
    print("num_fds          : {}".format(p.num_fds()))
    print("num_threads      : {}".format(p.num_threads()))
    print("threads          : {}".format(p.threads()))
    print("cpu_times        : {}".format(p.cpu_times()))
    print("cpu_percent      : {}".format(p.cpu_percent()))
    print("cpu_affinity     : {}".format(p.cpu_affinity()))
    print("cpu_num          : {}".format(p.cpu_num()))
    print("memory_info      : {}".format(p.memory_info()))
    print("memory_full_info : {}".format(p.memory_full_info()))
    print("memory_percent   : {}".format(p.memory_percent()))
    # print("memory_maps      : {}".format(p.memory_maps()))
    print("children         : {}".format(p.children()))
    print("open_files       : {}".format(p.open_files()))
    print("connections      : {}".format(p.connections()))
    print("is_running       : {}".format(p.is_running()))
