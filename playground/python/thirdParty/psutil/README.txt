OVERVIEW
===============================================================================
- Psutil Documentation
- Description                       : Psutil (python system and progess utilities) is a cross-platform
                                      library for retrieving information on running processes and
                                      system utilization (CPU, memory, disks, network, sensos) in Python
- Installation ----------------------
  * Install                         : pip3 install psutil
  * Uninstall                       : pip3 uninstall psutil
- Relevant URLs ---------------------
 * Home                             : https://psutil.readthedocs.io/en/latest/
