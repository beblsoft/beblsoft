OVERVIEW
===============================================================================
Psutil Samples Documentation



SINGLE FILE SAMPLES
===============================================================================
- Description ----------------------: Single file samples
- Setup -----------------------------
  * Setup venv                      : virtualenv -p /usr/bin/python3.6 /tmp/psutilVenv
  * Enter venv                      : source /tmp/psutilVenv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- info ------------------------------
  * Description                     : Display system information via the psutil command
  * Run                             : ./info.py
