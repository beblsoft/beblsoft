#!/usr/bin/env python3
"""
 NAME:
  app.py

 DESCRIPTION
  Basic Flask Application
"""


# ----------------------------- IMPORTS ------------------------------------- #
import json
import logging
import pprint
from flask import Flask, Response, request
from flask_cors import CORS


# ----------------------------- GLOBALS ------------------------------------- #
app       = Flask(__name__)
app.debug = True
logger    = logging.getLogger(__name__)
CORS(app)


# ------------------------ JSON RESPONSE ------------------------------------ #
class JSONResponse(Response):
    """
    JSON Response Class
    """
    MAGIC = 7292018

    def __init__(self, data=None, **kwargs):
        """
        Initialize respone object
        Args
          status: HTTP status code
          code: Beblsoft error code
          data: String of data
        """
        super().__init__(self, **kwargs)
        self.mimetype = "application/json"
        if not data:
            data      = {}
        dataDict      = dict(magic=JSONResponse.MAGIC, data=data)
        self.data     = json.dumps(dataDict)


# ----------------------------- ROUTES -------------------------------------- #
@app.route("/hw", methods=["GET"])
def hello_world():
    """
    Return Hello World
    """
    logger.warning("request={}".format(pprint.pformat(request.__dict__)))
    return JSONResponse(data={"Hello": "World!"})


@app.route("/echo", methods=["POST"])
def echo():
    """
    Echo the posted data back to the client
    """
    logger.warning("request={}".format(pprint.pformat(request.__dict__)))
    logger.warning("requestJSON={}".format(pprint.pformat(request.get_json())))
    return JSONResponse(data=request.get_json())


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    app.run()
