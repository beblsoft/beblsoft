OVERVIEW
===============================================================================
Zappa Samples Documentation



BASIC APPLICATION
===============================================================================
- Description                       : Basic application to demonstrate zappa functionality
- Usage:
  * Enter directory                 : cd basic
  * Setup virtual environment       : See python.sh: python3_setupVenv
  * Enter virtual environment       : source venv/bin/activate
  * Create configuration file       : zappa init
                                      Take defaults
  * Deploy server                   : zappa deploy dev
  * Update server                   : zappa update dev
  * Undeploy server                 : zappa undeploy -y dev
  * Package server                  : zappa package dev
  * Tail logs                       : zappa tail dev
- Get /hw                           : curl --silent -X GET https://13vzwfagok.execute-api.us-east-1.amazonaws.com/dev/hw | jq
- Post /echo                        : curl --silent -H "Content-Type: application/json" -X POST -d '{"foo":"bar"}' https://13vzwfagok.execute-api.us-east-1.amazonaws.com/dev/echo | jq

