OVERVIEW
===============================================================================
- Zappa Technology Documentation
- Description                       : Zappa is a framework for creating server-less, event driven Python
                                      applications on the AWS cloud.
                                    : It creates the following stack:
                                          AWS API Gateway        Custom Events
                                                     |            |
                                                   AWS Lambda Handler
                                                     |            |
                                       WSGI App (Flask, Django)   Custom Handlers
- Relevant URLs
  * Guthub                          : https://github.com/Miserlou/Zappa
