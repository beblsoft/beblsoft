#!/usr/bin/env python3
"""
NAME
 contextManager.py

DESCRIPTION
 FreezeGun Context Manager Usage
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
def test():
    """
    Demonstrate time being frozen and unfrozen
    """
    assert datetime.datetime.now() != datetime.datetime(2012, 1, 14)

    with freeze_time("2012-01-14"):
        assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)

    assert datetime.datetime.now() != datetime.datetime(2012, 1, 14)
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test()
