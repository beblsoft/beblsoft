#!/usr/bin/env python3
"""
NAME
 decorator.py

DESCRIPTION
 FreezeGun Decorator Usage
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
import unittest
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
@freeze_time("2012-01-14")
def test():
    """
    Frozen test
    """
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)


# Or a unittest TestCase
@freeze_time("1955-11-12")
class MyTests(unittest.TestCase):
    """
    Unittest TestCase. Freezes for every test, from the start of setUpClass to the end of tearDownClass
    """

    def test_the_class(self): #pylint: disable=R0201,C0111
        assert datetime.datetime.now() == datetime.datetime(1955, 11, 12)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test()
    unittest.main(verbosity=2)
