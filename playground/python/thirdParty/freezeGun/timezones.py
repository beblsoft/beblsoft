#!/usr/bin/env python3
"""
NAME
 timezones.py

DESCRIPTION
 FreezeGun Usage with Timezones
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
@freeze_time("2012-01-14 03:21:34", tz_offset=-4)
def test():
    """
    Verify timezone usage
    """
    assert datetime.datetime.utcnow() == datetime.datetime(2012, 1, 14, 3, 21, 34)
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 13, 23, 21, 34)

    # datetime.date.today() uses local time
    assert datetime.date.today() == datetime.date(2012, 1, 13)
    print("No Asserts!")


@freeze_time("2012-01-14 03:21:34", tz_offset=-datetime.timedelta(hours=3, minutes=30))
def test_timedelta_offset():
    """
    Timezone with timedelta offset
    """
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 13, 23, 51, 34)
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test()
    test_timedelta_offset()
