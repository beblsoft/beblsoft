#!/usr/bin/env python3
"""
NAME
 moveTime.py

DESCRIPTION
 Manually move the frozen time
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
def test_move_to():
    """
    Test manually moving the time
    """
    initial_datetime = datetime.datetime(year=1, month=7, day=12,
                                        hour=15, minute=6, second=3)

    other_datetime = datetime.datetime(year=2, month=8, day=13,
                                        hour=14, minute=5, second=0)
    with freeze_time(initial_datetime) as frozen_datetime:
        assert frozen_datetime() == initial_datetime

        frozen_datetime.move_to(other_datetime)
        assert frozen_datetime() == other_datetime

        frozen_datetime.move_to(initial_datetime)
        assert frozen_datetime() == initial_datetime
        print("No Asserts!")


@freeze_time("2012-01-14", as_arg=True)
def test_move_again(frozen_time):
    """
    Test manually moving the time
    """
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)
    frozen_time.move_to("2014-02-12")
    assert datetime.datetime.now() == datetime.datetime(2014, 2, 12)
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test_move_to()
    test_move_again() #pylint: disable=E1120
