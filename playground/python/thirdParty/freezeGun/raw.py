#!/usr/bin/env python3
"""
NAME
 raw.py

DESCRIPTION
 FreezeGun Raw Usage
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
def test():
    """
    Demonstrate time being frozen and unfrozen
    """
    freezer = freeze_time("2012-01-14 12:00:01")
    freezer.start()
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 14, 12, 0, 1)
    freezer.stop()
    assert datetime.datetime.now() != datetime.datetime(2012, 1, 14, 12, 0, 1)
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test()
