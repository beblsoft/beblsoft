#!/usr/bin/env python3
"""
NAME
 functionGenerator.py

DESCRIPTION
 FreezeGun Usage with function and generator arguments
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
def test_lambda():
    """
    Used with lambda arg
    """
    with freeze_time(lambda: datetime.datetime(2012, 1, 14)):
        assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)
    print("No Asserts!")

def test_generator():
    """
    Used with generator arg
    """
    datetimes = (datetime.datetime(year, 1, 1) for year in range(2010, 2012))

    with freeze_time(datetimes):
        assert datetime.datetime.now() == datetime.datetime(2010, 1, 1)

    with freeze_time(datetimes):
        assert datetime.datetime.now() == datetime.datetime(2011, 1, 1)

    # The next call to freeze_time(datetimes) would raise a StopIteration exception.
    print("No Asserts!")

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test_lambda()
    test_generator()
