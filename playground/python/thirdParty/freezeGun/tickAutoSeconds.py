#!/usr/bin/env python3
"""
NAME
 tickAutoSeconds.py

DESCRIPTION
 Freeze go back in time but keep time moving in set increments
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
@freeze_time("Jan 14th, 2020", auto_tick_seconds=15)
def test_tick_auto():
    """
    Test tick auto
    """
    first_time = datetime.datetime.now()
    auto_incremented_time = datetime.datetime.now()
    assert first_time + datetime.timedelta(seconds=15) == auto_incremented_time
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test_tick_auto()
