# FreezeGun Projects Documentation

Each project requires the following setup:
```bash
virtualenv -p /usr/bin/python3.6 /tmp/fgvenv
source /tmp/fgvenv/bin/activate
pip3 install -r requirements.txt
```

## decorator.py

Use `freeze_time` decorator to freeze time.

To run:
```bash
./decorator.py
```

## contextManager.py

Use `freeze_time` context manager to freeze time.

To run:
```bash
./contextManager.py
```

## raw.py

Use without decorator or context manager.

To run:
```bash
./raw.py
```

## timezones.py

Use with timezone calculations.

To run:
```bash
./timezones.py
```

## functionGenerator.py

Use with function and generator arguments.

To run:
```bash
./timezones.py
```

## tick.py

Use with tick argument which keeps clock moving forward.

To run:
```bash
./tick.py
```

## tickAutoSeconds.py

Use with `auto_tick_seconds` argument which keeps clock moving forward at set increments.

To run:
```bash
./tickAutoSeconds.py
```

## tickManual.py

Call `frozen_datetime.tick()` to manually increment the time.

To run:
```bash
./tickManual.py
```

## moveTime.py

Call `frozen_datetime.move_to()` to manually move the time.

To run:
```bash
./moveTime.py
```

