#!/usr/bin/env python3
"""
NAME
 tick.py

DESCRIPTION
 Freeze go back in time but keep time moving
"""

# ----------------------------- IMPORTS ------------------------------------- #
import datetime
from freezegun import freeze_time


# ----------------------------- TESTS --------------------------------------- #
@freeze_time("Jan 14th, 2020", tick=True)
def test_tick():
    """
    Verify time moving forward
    """
    assert datetime.datetime.now() > datetime.datetime(2020, 1, 14)
    print("No Asserts!")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    test_tick()
