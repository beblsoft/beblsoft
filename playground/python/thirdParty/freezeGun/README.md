# FreezeGun Documentation

FreezeGun is a library that allows your Python tests to travel through time by mocking the datetime
module.

Relevant URLs:
[GitHub](https://github.com/spulec/freezegun),
[pip](https://pypi.org/project/freezegun/0.1.11/)

## Installation

```bash
pip3 install freezegun
```

## Usage

Once the decorator or context manager have been invoked, all calls to:

- `datetime.datetime.now()`
- `datetime.datetime.utcnow()`
- `datetime.date.today()`
- `time.time()`
- `time.localtime()`
- `time.gmtime()`
- `time.strftime()`

will return the time that has been frozen.

```python
from freezegun import freeze_time
import datetime
import unittest


@freeze_time("2012-01-14")
def test():
    assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)

# Or a unittest TestCase - freezes for every test, from the start of setUpClass to the end of tearDownClass
@freeze_time("1955-11-12")
class MyTests(unittest.TestCase):
    def test_the_class(self):
        assert datetime.datetime.now() == datetime.datetime(1955, 11, 12)

# Or any other class - freezes around each callable (may not work in every case)
@freeze_time("2012-01-14")
class Tester(object):
    def test_the_class(self):
        assert datetime.datetime.now() == datetime.datetime(2012, 1, 14)
```
