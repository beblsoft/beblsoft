#!/usr/bin/env python3
"""
NAME
 tp_click.py
"""


# ----------------------- IMPORTS ------------------------------------------- #
import click


# ----------------------- BASE ---------------------------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
def cli():
    """
    Sample Command Line Interface
    """
    pass

# ----------------------- COMMANDS ------------------------------------------ #
@cli.command()
@click.option("--secret", "-s", type=click.Choice(["1", "2", "3"]), default="3",
              help="Specify secret type. Default=Test")
def command1(secret): #pylint: disable=C0111
    print(secret)


@cli.command()
def command2(): #pylint: disable=C0111
    print("Comand 2")



# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    cli()
