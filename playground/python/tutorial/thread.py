#!/usr/bin/env python3
"""
NAME
 thread.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
import threading
from urllib.request import urlopen



# ------------------------ HELPER FUNCTIONS --------------------------------- #
def runNQueries(nQueries=50, multiThread=False):
    """
    Run N Queries
    """

    def runSingleQuery():
        """
        Do a single query
        """
        url    = "http://www.google.com"
        thread = threading.current_thread()
        with urlopen(url) as resp:
            print("{} {}".format(thread.name, resp))

    if not multiThread:
        for _ in range(nQueries):
            runSingleQuery()
    else:
        threadList = []
        for i in range(nQueries):
            t = threading.Thread(name="Query{}".format(i), target=runSingleQuery)
            t.start()
            threadList.append(t)
        for t in threadList:
            t.join()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    print("Single Threaded Queries")
    print("-" * 50)
    runNQueries(multiThread=False)


    print()
    print("Multi Threaded Queries")
    print("-" * 50)
    runNQueries(multiThread=True)
