#!/usr/bin/env python3
"""
NAME
 while.py
"""


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    # Basic while loop
    count    = 0
    countMax = 10

    while True:
        # count += 3
        count = count + 3
        print(count)
        if count >= countMax:
            break
