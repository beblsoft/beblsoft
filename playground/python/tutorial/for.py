#!/usr/bin/env python3
"""
NAME
 for.py
"""


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    # Iterate over list
    nameList = ["John", "Barry", "Paula", "James"]
    for name in nameList:
        print(name)

    print("-" * 20)

    # Iterate over list, printing index, range(start, end, step)
    numList = range(-5, 10, 1)
    for idx, num in enumerate(numList):
        print("idx={} num={}".format(idx, num))

    print("-" * 20)

    # Break, continue
    numList = range(-5, 10, 1)
    for num in numList:
        if num == 3:
            print("Skipping number")
            continue
        elif num >= 6:
            print("Breaking out of loop!")
            break
        print(num)
