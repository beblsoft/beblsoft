#!/usr/bin/env python3
"""
NAME
 tp_flask.py
"""

# ------------------------------ IMPORTS ------------------------------------ #
from flask import Flask


# ------------------------------ GLOBALS ------------------------------------ #
app = Flask(__name__)


# ------------------------------ ROUTES ------------------------------------- #
@app.route("/")
def index():
    """
    Index route
    """
    return "Index Page"


@app.route("/hello/<name>")
def hello(name=None):
    """
    Hello Route
    """
    return "Hello {}!".format(name)



# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(debug=True)
