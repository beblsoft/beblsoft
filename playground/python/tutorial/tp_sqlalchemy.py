#!/usr/bin/env python3
"""
NAME
 tp_sqlalchemy.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, select
from sqlalchemy import Table, Column, Integer, String


# ----------------------------- GLOBALS ------------------------------------- #
Base     = declarative_base()
engine   = create_engine("sqlite:///:memory:", echo=False)
metadata = MetaData()


# ----------------------------- TABLES -------------------------------------- #
users = Table(
    "users", metadata,
    Column("id", Integer, primary_key=True),
    Column("name", String)
)


# ----------------------------- FUNCTIONS ----------------------------------- #
def createTables():  # pylint: disable=C0111
    metadata.create_all(engine)


def deleteTables():  # pylint: disable=C0111
    metadata.drop_all(engine)


def populate():
    """
    Populate tables
    """
    print("Populating...")
    conn = engine.connect()
    conn.execute(users.insert(), [{"id": 1, "name": "Steve"}])  # pylint: disable=E1120


def updateData():
    """
    Update data
    """
    print("Updating...")
    conn = engine.connect()
    stmt = users.update().where(users.c.id == 1).values(name="John")  # pylint: disable=E1120
    conn.execute(stmt)


def printState(title):
    """
    Print database state
    """
    conn  = engine.connect()
    result = conn.execute(select([users]))
    print(title)
    for row in result:
        print(row)
    print("\n")
    result.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState(title="After Populate...")
    updateData()
    printState(title="After Update...")
    deleteTables()
