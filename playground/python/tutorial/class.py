#!/usr/bin/env python3
"""
NAME
 class.py
"""


# ------------------------ PERSON CLASS ------------------------------------- #
class Person():
    """
    Person Class
    """

    def __init__(self, firstName, lastName):
        """
        Initialize object
        """
        self.firstName = firstName
        self.lastName  = lastName

    def __str__(self):
        """
        Return object string
        """
        return "[{} fullName='{}']".format(self.__class__.__name__, self.fullName)

    @property
    def fullName(self):
        """
        Return full name
        """
        return "{} {}".format(self.firstName, self.lastName)

    def sayHi(self):
        """
        Say Hi
        """
        print("Hi {}!".format(self.fullName))

    def sayBye(self):
        """
        Say Bye
        """
        print("Bye {}!".format(self.fullName))


# ------------------------ WORKER CLASS ------------------------------------- #
class Worker(Person):
    """
    Worker
    """

    def __init__(self, occupation, **personKwargs):
        """
        Initialize object
        """
        super().__init__(**personKwargs)
        self.occupation = occupation

    def __str__(self):
        """
        Return object string
        """
        return "[{} fullName='{}' occupation='{}']".format(
            self.__class__.__name__, self.fullName, self.occupation)


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    john  = Person(firstName="John", lastName="Smith")
    john.sayHi()
    print(john)
    john.sayBye()

    print("-" * 20)

    peter = Worker(firstName="Peter", lastName="Anderson", occupation="Football Player")
    peter.sayHi()
    print(peter)
    peter.sayBye()

