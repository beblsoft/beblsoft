#!/usr/bin/env python3
"""
NAME
 dict.py
"""

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    d = {
        "person": 1,
        "item": 2,
        "foo": "bar",
        "subDict": {
            "1": 2,
            "3": 4
        },
        1: 2
    }

    print("d          = {}".format(d))
    print("Length     = {}".format(len(d)))
    print("d[person]  = {}".format(d["person"]))

    print("-" * 20)

    for key, value in d.items():
        print(key, value)
