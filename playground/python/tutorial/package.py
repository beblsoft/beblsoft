#!/usr/bin/env python3
"""
NAME
 package.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
from packageA import module1
from packageA import module2



# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    module1.hello()
    module2.goodBye()
