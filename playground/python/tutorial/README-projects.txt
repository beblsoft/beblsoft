OVERVIEW
===============================================================================
Python Tutorial Samples
- Relevant URLs
  * Python Tutorial               : https://docs.python.org/3/tutorial/


STANDARD LIBRARY SAMPLES
===============================================================================
- Description --------------------: Python Standard Library Samples
                                  : All of these samples only require the python interpreter
                                    and the included standard libraries. No other dependencies
                                    are required
- Python Interpreter --------------
  * Description                   : Python commands can be run with the interactive interpreter
  * To run                        : $ python3
                                    Ctrl + D to exit
  * help                          : >>> help()
                                    Ctrl + C to exit help
  * Simple Command                : a = 2 + 2
                                    print(a)
- if.py ---------------------------
  * Description                   : Control flow with if and else statements
  * Run                           : ./if.py
- for.py --------------------------
  * Description                   : Demonstrate for loop usage
  * Run                           : ./for.py
- while.py ------------------------
  * Description                   : Demonstrate while loops. Useful when number
                                    of iterations are unknown
  * Run                           : ./while.py
- function.py ---------------------
  * Description                   : Example using functions
  * Run                           : ./function.py
- list.py ------------------------
  * Description                   : Example using a list
  * Run                           : ./list.py
- dict.py ------------------------
  * Description                   : Example using a dictionary
  * Run                           : ./dict.py
- class.py -----------------------
  * Description                   : Introduction to classes
                                  : Demonstrate basic inheritance relationship
  * Run                           : ./class.py
- error.py ------------------------
  * Description                   : Demonstrate try, raise, excepty, else, and finally
  * Run                           : ./error.py
- package.py ----------------------
  * Description                   : Demonstrate creating and importing python packages
  * Relevant Files                : ./packageA/__init__.py
                                  : ./packageA/module1.py
                                  : ./packageA/module2.py
  * Run                           : ./package.py
- files.py ------------------------
  * Description                   : Demonstrate file creating, reading, writing, and deletion
  * Run                           : ./files.py
- dates.py ------------------------
  * Description                   : Demonstrate working with dates and times
  * Run                           : ./dates.py
- thread.py -----------------------
  * Description                   : Example using multiple threads to accomplish a task
  * Run                           : ./thread.py
- logs.py -------------------------
  * Description                   : Demonstrate using the python logger
  * Run                           : ./logs.py
- test.py -------------------------
  * Description                   : Demonstrate basic unittests
  * Run                           : ./test.py



THIRD PARTY SAMPLES
===============================================================================
- Description --------------------: Python Third Party Samples
                                  : All of these samples only require specific third parties
                                    to be installed
- Setup ---------------------------
  * Virtual Environment           : virtualenv -p /usr/bin/python3.6 venv
                                  : source venv/bin/activate
                                  : pip3 install -r requirements.txt
                                  : ... run commands ...
                                  : deactivate
- tp_sqlalchemy.py ----------------
  * Description                   : Demonstrate using a mysql database using sqlalchemy
  * Run                           : ./tp_sqlalchemy.py
- tp_click.py ---------------------
  * Description                   : Demonstrate using command line interface builder click
  * Help                          : ./tp_click.py -h
- tp_matplotlib.py ----------------
  * Description                   : Demonstrate a basic plot with matplotlib
  * Run                           : ./tp_matplotlib.py
- tp_flask.py ---------------------
  * Description                   : Demonstrate creating a simple web application with flask
  * Run                           : ./tp_flask.py
                                  : Visit Chrome http://localhost:5000/
                                  : Visit Chrome http://localhost:5000/hello/John
