#!/usr/bin/env python3
"""
NAME
 if.py
"""


# ----------------------------- MAIN ---------------------------------------- #
if __name__== "__main__":
    x = 65

    if x < 0:
        print("X < 0")
    elif 0 < x < 10:
        print("0 < x < 10")
    elif 10 < x < 100:
        print("10 < x < 100")
    else:
        print("X > 100")
