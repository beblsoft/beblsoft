#!/usr/bin/env python3
"""
NAME
 dates.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
from datetime import datetime, timedelta


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    now      = datetime.now()
    nMinutes = 5
    delta    = timedelta(minutes=nMinutes)

    print("Now            = {}".format(now))
    print("{} Minutes ago = {}".format(nMinutes, now - delta))
