#!/usr/bin/env python3
"""
NAME
 test.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
import unittest


# ------------------------ TEST CASE ---------------------------------------- #
class MainTestCase(unittest.TestCase):
    """
    Main Test Case
    """

    def test_1(self):
        """
        First test
        """
        self.assertEqual(1, 1)

    def test_2(self):
        """
        Second test
        """
        self.assertEqual(2, 2)

    def test_3(self):
        """
        Third test
        """
        self.assertEqual(3, 3)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    unittest.main(verbosity=2)
