#!/usr/bin/env python3
"""
NAME
 tp_matplotlib.py
"""

# ----------------------------- IMPORTS ------------------------------------- #
import matplotlib.pyplot as plt
import numpy


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    # Data for plotting
    t = numpy.arange(0.0, 2.0, 0.01)
    s = numpy.sin(2 * numpy.pi * t)

    (fig, ax) = plt.subplots()
    ax.plot(t, s)
    ax.set(xlabel="time (s)", ylabel="voltage (mV)", title="Sine Wave")
    ax.grid()
    plt.show()
