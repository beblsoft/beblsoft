#!/usr/bin/env python3
"""
NAME
 files.py
"""

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    filePath  = "/tmp/example.txt"

    # Create file
    with open(filePath, "w") as f:
        f.write("1\n")
        f.write("2\n")
        f.write("3\n")
        f.write("4\n")

    # Read file
    with open(filePath) as f:
        string = f.read()
        print(string)

    print("-" * 20)

    # Read file, one line at a time
    with open(filePath) as f:
        for line in f:
            print(line.strip())
