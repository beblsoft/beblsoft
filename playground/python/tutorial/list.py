#!/usr/bin/env python3
"""
NAME
 list.py
"""

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    fruits = ["orange", "pear", "orange", "kiwi", "apple", "mango", "strawberry"]
    print("Fruits          = {}".format(fruits))
    print("Length          = {}".format(len(fruits)))
    print("fruits[0]       = {}".format(fruits[0]))
    print("fruits[1]       = {}".format(fruits[1]))
    print("fruits[-1]      = {}".format(fruits[-1]))
    print("Sorted          = {}".format(sorted(fruits)))
    print("Sorted Reverse  = {}".format(sorted(fruits, reverse=True)))
    print("Count of orange = {}".format(fruits.count("orange")))

    print("-" * 20)

    for fruit in fruits:
        print(fruit)

    print("-" * 20)

    print("Split this string into a list separated by spaces".split(" "))

    print("-" * 20)

    print(" ".join(["Convert", "This", "List", "To", "A", "String"]))

    print("-" * 20)

    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
    ]
    print("Matrix = {}".format(matrix))

    print("-" * 20)

    mixedList = ["1", 2, [1, 2, 3], {'a': 1}]
    print("MixedList = {}".format(mixedList))
