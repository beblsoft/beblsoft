#!/usr/bin/env python3
"""
NAME
 error.py
"""


# ------------------------ HELPER FUNCTION ---------------------------------- #
def errorFunction(divideByZero=False, raiseException=False):
    """
    Error Function
    """
    try:
        print("Try")

        if divideByZero:
            _ = 1 / 0
        if raiseException:
            raise Exception("Error not caught")

    except ZeroDivisionError as e:
        print("Caught {}".format(e))

    else:
        print("No errors thrown")

    finally:
        print("Finally")


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    errorFunction()

    print("-" * 20)

    errorFunction(divideByZero=True)

    print("-" * 20)

    errorFunction(raiseException=True)
