#!/usr/bin/env python3
"""
NAME
 function.py
"""


# ----------------------------- HELPER FUNCTIONS ---------------------------- #
def functionBasic(arg1, arg2=2, arg3=3):
    """
    Basic
    """
    print("functionBasic. arg1={} arg2={} arg3={}".format(arg1, arg2, arg3))


def functionRecursive(levels=0, maxLevels=10):
    """
    Recursive function
    """
    print("functionRecursive: levels={}/{}".format(levels, maxLevels))
    if levels == maxLevels:
        return 0
    else:
        return functionRecursive(levels=levels + 1, maxLevels=maxLevels)


def functionArbitraryArguments(*args, **kwargs):
    """
    Arbitrary Arguments
    """
    print("functionArbitraryArguments args={} kwargs={}".format(args, kwargs))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":

    functionBasic(arg1=1)
    functionBasic(arg1=1, arg2=1)
    functionBasic(arg1=1, arg2=1, arg3=1)

    print("-" * 40)

    functionRecursive(maxLevels = 50)

    print("-" * 40)

    functionArbitraryArguments(1, 2, 3)
    functionArbitraryArguments(a=1, b=2, c=3)
    functionArbitraryArguments(1, 2, 3, a=1, b=2, c=3)
