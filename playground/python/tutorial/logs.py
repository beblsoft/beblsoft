#!/usr/bin/env python3
"""
NAME
 logs.py
"""


# ------------------------ IMPORTS ------------------------------------------ #
import logging


# ------------------------ GLOBALS ------------------------------------------ #
logger = logging.getLogger(__name__)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format="%(asctime)s %(levelname)8s %(threadName)10s:%(message)s",
                        datefmt="%m/%d/%Y %H:%M:%S", level=level)
    logger.debug("Debug!")
    logger.info("Info!")
    logger.warning("Warning!")
    logger.error("Error!")
    logger.critical("Critical!")
    # logger.exception("Exception!")
