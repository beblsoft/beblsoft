#!/usr/bin/env python3
"""
NAME
 magicBall.py

DESCRIPTION
 Program simpulates a magic 8 ball.
 http://www.pythonforbeginners.com/code-snippets-source-code/magic-8-ball-written-in-python/

USAGE
  ./magicBall.py
"""


# ----------------------------- IMPORTS ------------------------------------- #
import sys
import random

# ----------------------------- MAIN ---------------------------------------- #
ans = True

while ans:
    question = input("Ask the magic 8 ball a question: (press enter to quit) ")

    answers = random.randint(1,8)

    if question == "":
        sys.exit()

    elif answers == 1:
        print("It is certain")

    elif answers == 2:
        print("Outlook is good")

    elif answers == 3:
        print("You may rely on it")

    elif answers == 4:
        print("Ask again later")

    elif answers == 5:
        print("Concentrate and ask again")

    elif answers == 6:
        print("Reply hazy, try again")

    elif answers == 7:
        print("My reply is no")

    elif answers == 8:
        print("My sources say no")
