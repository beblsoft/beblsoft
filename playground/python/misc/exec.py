#!/usr/bin/env python3
# pylint: disable=W0122
"""
 NAME
  exec.py

 DESCRIPTION
   Demonstrate Exec Usage
"""

# ------------------------------- IMPORTS ----------------------------------- #
import logging
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------------- HELPER FUNCTIONS -------------------------- #
@logFunc()
def execBasic():
    """
    Basic Exec
    """
    code = """print("Hello World!"); """
    exec(code, globals(), locals())

@logFunc()
def execNoLocals():
    """
    No Locals. rval is NOT correctly returned
    """
    rval = None
    code = """rval = "I am an unhappy return value";"""
    exec(code)
    return rval

@logFunc()
def execLocalsOut():
    """
    Locals Out. rval IS correctly returned
    """
    localDict = {}
    code      = """rval = "I am a happy return value";"""
    exec(code, globals(), localDict)
    return localDict.get("rval")

@logFunc()
def execLocalsIn():
    """
    Locals In
    """
    localDict = {"a":1, "b":2}
    code      = """print(a,b);"""
    exec(code, globals(), localDict)


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    execBasic()
    execNoLocals()
    execLocalsOut()
    execLocalsIn()
