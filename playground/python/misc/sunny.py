#!/usr/bin/env python3
"""
NAME
 sunny.py

DESCRIPTION
  Cool Programming things to show Sunny!

SYLLABUS
  - Filesystem GUI
  - Terminal
    - ls, cd, cat
    - ps, psl,
    - ifconfig, slurm -i eth0, rrun tcpdump -i eth0
    - ping www.google.com, ping www.espn.com, curl ipinfo.io/<ip address>
  - Programming Below
  - Game of life
    - cd ../life; ./life -b [6, 7, 8, 11]
  - GUI
    - python/GUI/helloWorld.py
    - python/GUI/progressbar.py
  - Mojourney
  - Oracle Work
    - VPN
    - ssh
    - Emacs
    - aslexp.h
    - Google Drive
"""

# ----------------------------- IMPORTS ------------------------------------- #
import webbrowser


# ----------------------------- CONSTANTS ----------------------------------- #
ADULT_AGE = 18


# ----------------------------- FUNCTIONS ----------------------------------- #
# 1: Input and output
name = input("Hi, What's your name? ")
print("Hi " + name + "!")

# 2: If, Else
age = int(input("How old are you? "))
if age > ADULT_AGE:
    print("Wow, you're old!")
else:
    print("Still growing up it seems.")

# 3: Chrome
url = input("What's you're favorite website? ")
webbrowser.open("http://" + url)
