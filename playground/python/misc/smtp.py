#!/usr/bin/env python3
"""
NAME
 smtp.py

DESCRIPTION
  Leverages Python's smtplib to send an email via gmail.
"""


# ----------------------------- IMPORTS ------------------------------------- #
import smtplib
import socket
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime
import getpass


# ----------------------------- GLOBALS ------------------------------------- #
#Sender Receiver Details
#hard code to beblsoftmanager if know password or enter one
#sender     = input("Enter user account in quotes:")
sender     = 'beblsoftmanager@gmail.com'
senderPass = getpass.getpass("Enter password for " + sender + ":")
toAddrs    = ['darrylpblack@gmail.com', 'bensson.james@gmail.com']
server     = smtplib.SMTP('smtp.gmail.com:587')

#Message
msg            = MIMEMultipart('alternative')
msg['Subject'] = "Beblsoft's first programtic email!"
msg['From']    = sender
msg['To']      = str(toAddrs)
body           = "Email coming from: " + socket.gethostname() + "\n"
body          += "Time sent: " + str(datetime.datetime.now()) + "\n"
bodyText       = MIMEText(body, "plain")
msg.attach(bodyText)


# ----------------------------- MAIN ---------------------------------------- #
server.ehlo()
server.starttls()
server.login(sender, senderPass)
server.sendmail(sender, toAddrs, msg.as_string())
server.quit()
