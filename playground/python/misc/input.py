#!/usr/bin/env python3
"""
NAME
 input.py

DESCRIPTION
 Python script to read input

USAGE
  ./input.py
"""


# ----------------------------- MAIN ---------------------------------------- #
while True:
    reply = input('Enter text:')
    if reply == 'stop':
        break
    print(reply.upper())
