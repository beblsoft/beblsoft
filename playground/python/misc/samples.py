#!/usr/bin/python
#
# NAME
#  samples.py
#
# DESCRIPTION
#  Program to display basic python programming constructs
#  Most of code from:  https://developers.google.com/edu/python


# ----------------------------- IMPORTS ------------------------------------- #
import re  # used for regular expressions
import os  # used for os functions
import sys  # used for sys functions
import urllib  # used for URL manipulation


# ----------------------------- FUNCTIONS ----------------------------------- #
# defining a function block, begins
def wget(url):
    ufile = urllib.urlopen(url)  # get file-like object for url
    info = ufile.info()  # meta-info about the url content
    if info.gettype() == 'text/html':
        print 'base url:' + ufile.geturl()
        text = ufile.read()  # read all its text
        print text


# -------------------------------- MAIN ------------------------------------- #
print "hello world"

# Demo string array manipulation
colors = ['red', 'blue', 'green']
print colors[0]  # red
print colors[2]  # green
print len(colors)  # 3

# Demo for loop
squares = [1, 4, 9, 16, 25]
sum = 0
for num in squares:
    sum += num
print sum  # 30

# Conditional print using in
list = ['larry', 'curly', 'moe']
if 'curly' in list:
    print 'curly is in the list'

speed = 81
if speed >= 80:
    print 'You are so busted'
else:
    print 'Have a nice day'

dog = 21
if(dog == 21):
    print "dog is 21"
elif(dog == 22):
    print "dog is 22"
else:
    print "dog is not 21 or 22"


# print the numbers from 0 through 9  for(i=0; i < 10, i++)
for i in range(10):
    print i

# List manipulation
list = ['larry', 'curly', 'moe']
list.append('shemp')  # append elem at end
list.insert(0, 'xxx')  # insert elem at index 0
list.extend(['yyy', 'zzz'])  # add list of elems at end
print list  # ['xxx', 'larry', 'curly', 'moe', 'shemp', 'yyy', 'zzz']
print list.index('curly')  # 2
list.remove('curly')  # search and remove that element
list.pop(1)  # removes and returns 'larry'
print list  # ['xxx', 'moe', 'shemp', 'yyy', 'zzz']

# Getting help
S = 'cat'  # define a string S set to cat
print dir(S)  # print out all methods associated w/ String S
print S.isalpha()  # show that string is composed of chars
print S.upper()

# Can build up a dict by starting with the the empty dict {}
# and storing key/value pairs into the dict like this:
# dict[key] = value-for-that-key
dict = {}
dict['a'] = 'alpha'
dict['g'] = 'gamma'
dict['o'] = 'omega'

print dict  # {'a': 'alpha', 'o': 'omega', 'g': 'gamma'}

print dict['a']  # Simple lookup, returns 'alpha'
dict['a'] = 6  # Put new key/value into dict
print 'a' in dict  # True
print dict  # {'a': 6, 'o': 'omega', 'g': 'gamma'}

# print dict['z']                  ## Throws KeyError, given z not in dictionary
if 'z' in dict:
    print dict['z']  # Avoid KeyError
print dict.get('z')  # None (instead of KeyError)


# delete operator
var = 6
del var  # var no more!

list = ['a', 'b', 'c', 'd']
del list[0]  # Delete first element
del list[-2:]  # Delete last two elements
print list  # ['b']

dict = {'a': 1, 'b': 2, 'c': 3}
del dict['b']  # Delete 'b' entry
print dict  # {'a':1, 'c':3}


# Echo the contents of a file, open read Universal where all line-endings come back as '\n'
f = open('foo.txt', 'rU')
for line in f:  # iterates over the lines of the file
    print line,  # trailing , so print does not add an end-of-line char
    # since 'line' already includes the end-of line.
f.close()

# search for the pattern word followed by any three char word
str = 'an example word:cat!!'
match = re.search(r'word:\w\w\w', str)
# If-statement after search() tests if it succeeded
if match:
    print 'found', match.group()  # 'found word:cat'
else:
    print 'did not find'

# demo system functions
dir = os.path.dirname(os.path.abspath(__file__))  # get current directory, print contents
print dir

# now print out all the files in the directory
filenames = os.listdir(dir)
for filename in filenames:
    print filename

# Exception processing
try:
    # Either of these two lines could throw an IOError, say
    # if the file does not exist or the read() encounters a low level error.
    filename = "dog"
    f = open(filename, 'rU')
    text = f.read()
    f.close()
except IOError:
    # Control jumps directly to here if any of the above lines throws IOError.
    sys.stderr.write('problem reading: ' + filename + '\n')
    # In any case, the code then continues with the line after the try/except

## HTTP = urllib and urlparse
url = "http://www.google.com"
wget(url)  # function defined above

# demo sorting
a = [5, 1, 4, 3]
print sorted(a)  # [1, 3, 4, 5]
print a

# Getting help
S = 'cat'  # define a string S set to cat
print S.isalpha()  # show that string is composed of chars
print S.upper()
