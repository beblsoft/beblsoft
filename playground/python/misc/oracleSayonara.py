#!/usr/bin/env python3
"""
NAME
 oracleSayonara.py

DESCRIPTION
 Script to tell how long to be left at oracle

USAGE
  ./oracleSayonara.py
"""

# ---------------------- IMPORTS --------------------------------------------- #
from datetime import datetime


# ---------------------- MAIN ----------------------------------------------- #
nowDate      = datetime.now()
sayonaraDate = datetime.strptime('Aug 14 2020  5:00PM', '%b %d %Y %I:%M%p')
delta        = sayonaraDate - nowDate
print("Days to Oracle Sayonara for Jamester and Dblack = {}".format(delta))
