#!/usr/bin/env python3
"""
NAME
 calculator.py

DESCRIPTION
  Calculator Program
  See http://sthurlow.com/python/lesson05/    (example 7)

USAGE
  ./calculator.py
"""

# ----------------------------- MAIN ---------------------------------------- #
print("Welcome to calc.py")

# Initialize state variables
continueWithCalc = True
operation = 0

while continueWithCalc == True:

    # print what options you have
    print("\nYour options are:")
    print("1) Addition")
    print("2) Subtraction")
    print("3) Multiplication")
    print("4) Division")
    print("5) Quit calculator.py\n")

    try:
        operation = int(input("Choose your option: "))
        if operation == 1:
            add1 = int(input("Add this: "))
            add2 = int(input("  to this: "))
            print("   ", add1, "+", add2, "=", add1 + add2)
        elif operation == 2:
            sub2 = int(input("Subtract this: "))
            sub1 = int(input("  from this: "))
            print("   ", sub1, "-", sub2, "=", sub1 - sub2)
        elif operation == 3:
            mul1 = int(input("Multiply this: "))
            mul2 = int(input("  with this: "))
            print("   ", mul1, "*", mul2, "=", mul1 * mul2)
        elif operation == 4:
            div1 = int(input("Divide this: "))
            div2 = int(input("  by this: "))
            print("   ", div1, "/", div2, "=", div1 / div2)
        elif operation == 5:
            continueWithCalc = False
    except Exception as _: #pylint: disable=W0703
        print("Invalid option, try again!\n")

print("Thank you for using calc.py!")
