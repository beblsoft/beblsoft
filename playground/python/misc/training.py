#!/usr/bin/python
"""
 NAME
  training.py

 DESCRIPTION
  A simple program designed to demonstrate the basic programming constructs of python.
  Note that most of this code came from:  https://developers.google.com/edu/python

 USAGE
  ./training.py -h
"""


# ----------------------------- IMPORTS ------------------------------------- #
import re          # used for regular expressions
import os          # used for os functions
import sys         # used for sys functions
import urllib      # used for URL manipulation
import argparse    # used for argument parsing


# ----------------------------- FUNCTIONS ----------------------------------- #
# Definition of a function block to be used later in sample; function block must be
# defined before use.  Notice the careful indenting, a requirement of python.  Python
# uses indenting to signify association grouping, not braces like java.
#
# This function block is used to open a URL passed in as a parameter, and if text html, dump out
# the contents of the url
def wget(url):
    """Document the function here...."""
    ufile = urllib.urlopen(url)  # get file-like object for url
    info = ufile.info()          # meta-info about the url content
    if info.gettype() == 'text/html':
        print 'base url:' + ufile.geturl()
        text = ufile.read()         # read all its text
        print text


def helloWorld():
    print("**Demo simple print:")
    print "Hello World!"


def ifStatements():
    print "**Demo a series of simple if statements, notice the : at the end of the if statement"
    speed = 100
    if speed >= 80:
        print 'Dude, you are so busted as your speed is greater than 80 mph.'
    else:
        print 'Have a nice day.'

    # Add in the else if, or elif in python; notice how " and ' are interchangable
    speed = 80
    if(speed == 80):
        print "Dude, you are going 80; that is too fast."
    elif(speed <= 22):
        print "Dude, you drive like an old lady."
    else:
        print "Have a great day."

    # Change assignment, show how to convert int to string
    speed = 22
    if(speed == 80):
        print "Dude, you are going 80; that is too fast."
    elif(speed <= 22):
        print "Dude, you drive like an old lady. Your speed is: " + str(speed)
    else:
        print "Have a great day."


def stringArrays():
    print "**Demo string array manipulation, including the size of the array"
    colors = ['red', 'blue', 'green']
    print colors[0]    # red
    print colors[2]    # green
    print "There are " + str(len(colors)) + " colors in the list."  # 3
    print "\n"

    print "**Demo conditional output with list using in"
    list = ['larry', 'curly', 'moe']
    if 'curly' in list:
        print 'curly is in the list'


def forLoops():
    print "**Demo the a for loop with a predefined integer array"
    squares = [1, 4, 9, 16, 25]
    sum = 0
    for num in squares:
        sum += num
    print "The sum equals: " + str(sum)          # 30
    print "\n"

    print "**Demo a for loop similar to java for(i=0; i < 3; i++)"
    for i in range(0, 3):
        print "For loop interation: " + str(i)


def listManipulate():
    print "**Demo list listManipulate"
    list = ['larry', 'curly', 'moe']
    list.append('jamester')       # append elem at end
    list.insert(0, 'dblack')      # insert elem at index 0
    list.extend(['emp3', 'emp4'])  # add list of elems at end
    print list                    # ['dblack', 'larry', 'curly', 'moe', 'jamester', 'emp3', 'emp4']
    print list.index('curly')     # 2
    list.remove('curly')          # search and remove that element
    list.pop(1)                   # removes and returns 'larry'
    print list                    # ['dblack', 'moe', 'jamester', 'emp3', 'emp4']


def dictManipulate():
    print "**Demo dictionary manipulate"
    # Can build up a dict by starting with the the empty dict {}
    # and storing key/value pairs into the dict like this:
    # dict[key] = value-for-that-key
    dict = {}
    dict['a'] = 'alpha'
    dict['g'] = 'gamma'
    dict['o'] = 'omega'

    print dict  # {'a': 'alpha', 'o': 'omega', 'g': 'gamma'}

    print dict['a']     # Simple lookup, returns 'alpha'
    dict['a'] = 6       # Put new key/value into dict
    print 'a' in dict   # True
    print dict          # {'a': 6, 'o': 'omega', 'g': 'gamma'}

    # print dict['z']                  # Throws KeyError, given z not in dictionary
    if 'z' in dict:
        print dict['z']     # Avoid KeyError
    print dict.get('z')                 # None (instead of KeyError)


def deleteOperator():
    print "**Demo use of the delete operator"
    # delete operator
    var = 6
    del var  # var no more!

    list = ['a', 'b', 'c', 'd']
    del list[0]     # Delete first element
    del list[-2:]   # Delete last two elements
    print list      # ['b']

    dict = {'a': 1, 'b': 2, 'c': 3}
    del dict['b']   # Delete 'b' entry
    print dict      # {'a':1, 'c':3}


def fileManipulate():
    print "**Demo opening, read a file"
    # Echo the contents of a file, open read Universal where all line-endings come back as '\n'
    f = open('foo.txt', 'rU')
    for line in f:  # iterates over the lines of the file
        print line,  # trailing , so print does not add an end-of-line char
        # since 'line' already includes the end-of line.
    f.close()


def patternMatching():
    print "**Demo pattern matching"
    # search for the pattern word followed by any three char word
    str = 'an example word:cat!!'
    match = re.search(r'word:\w\w\w', str)
    # If-statement after search() tests if it succeeded
    if match:
        print 'found', match.group()  # 'found word:cat'
    else:
        print 'did not find'


def systemFunctions():
    print "**Demo use of system functions"
    dir = os.path.dirname(os.path.abspath(__file__))  # get current directory, print contents
    print dir

    # now print out all the files in the directory
    filenames = os.listdir(dir)
    for filename in filenames:
        print filename


def exceptions():
    print "**Demo exception processing"
    try:
        # Either of these two lines could throw an IOError, say
        # if the file does not exist or the read() encounters a low level error.
        filename = "dog"
        f = open(filename, 'rU')
        text = f.read()
        f.close()
    except IOError:
        # Control jumps directly to here if any of the above lines throws IOError.
        sys.stderr.write('problem reading: ' + filename + '\n')
        # In any case, the code then continues with the line after the try/except


def sorting():
    print "**Demo sorting"
    a = [5, 1, 4, 3]
    print sorted(a)  # [1, 3, 4, 5]
    print a


def readWebsite():
    print "**Demo reading a website"
    # HTTP = urllib and urlparse
    url = "http://www.jbensson.com"
    wget(url)  # function defined above


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Python training code")

    # Add a line for each option, set default to 0 so doesn't happen automatically
    parser.add_argument('--helloWorld', dest='helloWorld', action='count',
                        default=0, help="Execute helloWorld function.")
    parser.add_argument('--ifStatements', dest='ifStatements', action='count',
                        default=0, help="Execute ifStatements function.")
    parser.add_argument('--stringArrays', dest='stringArrays', action='count',
                        default=0, help="Execute stringArrays function.")
    parser.add_argument('--forLoops', dest='forLoops', action='count',
                        default=0, help="Execute forLoops function.")
    parser.add_argument('--listManipulate', dest='listManipulate', action='count',
                        default=0, help="Execute listManipulate function.")
    parser.add_argument('--dictManipulate', dest='dictManipulate', action='count',
                        default=0, help="Execute dictManipulate function.")
    parser.add_argument('--deleteOperator', dest='deleteOperator', action='count',
                        default=0, help="Execute deleteOperator function.")
    parser.add_argument('--fileManipulate', dest='fileManipulate', action='count',
                        default=0, help="Execute fileManipulate function.")
    parser.add_argument('--patternMatching', dest='patternMatching', action='count',
                        default=0, help="Execute patternMatching function.")
    parser.add_argument('--systemFunctions', dest='systemFunctions', action='count',
                        default=0, help="Execute systemFunctions function.")
    parser.add_argument('--exceptions', dest='exceptions', action='count',
                        default=0, help="Execute exceptions function.")
    parser.add_argument('--sorting', dest='sorting', action='count',
                        default=0, help="Execute sorting function.")
    parser.add_argument('--readWebsite', dest='readWebsite', action='count',
                        default=0, help="Execute readWebsite function.")

    args = parser.parse_args()

    # Now have a line for each supported menu item
    if (args.helloWorld):
        helloWorld()
    elif (args.ifStatements):
        ifStatements()
    elif (args.stringArrays):
        stringArrays()
    elif (args.forLoops):
        forLoops()
    elif (args.listManipulate):
        listManipulate()
    elif (args.dictManipulate):
        dictManipulate()
    elif (args.deleteOperator):
        deleteOperator()
    elif (args.fileManipulate):
        fileManipulate()
    elif (args.patternMatching):
        patternMatching()
    elif (args.systemFunctions):
        systemFunctions()
    elif (args.exceptions):
        exceptions()
    elif (args.sorting):
        sorting()
    elif (args.readWebsite):
        readWebsite()
    else:
        parser.print_help()
