OVERVIEW
===============================================================================
Python Misc Samples Documentation



SINGLE FILE EXAMPLES
===============================================================================
- calculator.py -------------------
  * Description                   : Simple calculator program
  * Run                           : ./calculator.py

- exec.py -------------------------
  * Description                   : Example using the python builtin exec() function
  * Run                           : ./exec.py

- input.py ------------------------
  * Description                   : Retrieve and print user input
  * Run                           : ./input.py

- magicBall.py --------------------
  * Description                   : Program to simulate a magic 8-ball
  * Run                           : ./magicBall.py

- oracleSayonara.py ---------------
  * Description                   : Saying goodbye to Oracle
  * Run                           : ./oracleSayonara.py

- samples.py ----------------------
  * Description                   : Display basic python programming constructs
  * Run                           : ./samples.py

- smtp.py -------------------------
  * Description                   : Send an SMTP email
  * Run                           : ./smtp.py

- sunny.py ------------------------
  * Description                   : Cool programming things to show Sunny
  * Run                           : ./sunny.py

- super.py ------------------------
  * Description                   : Demonstrate python usage of super and multiple inheritance
  * Run                           : ./super.py

- template.py ---------------------
  * Description                   : Template for new python programs
  * Run                           : ./template.py

- training.py ---------------------
  * Description                   : Python training program
  * Run                           : ./training.py
