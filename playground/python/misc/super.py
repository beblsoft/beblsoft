#!/usr/bin/python
# pylint: disable=W0122
"""
 NAME
  super.py

 DESCRIPTION
   Demonstrate Super Usage

 METHOD RESOLUTION ORDER (MRO)
   Determines which method or property is used when multiple are defined in hierarchy
   Python uses C3Linearization to determine MRO
   See Wikipedia: https://en.wikipedia.org/wiki/C3_linearization

 OUTPUT
   $ ./super.py
   first
   third
   second
   fourth
   ['Fourth', 'Second', 'Third', 'First', 'object']
"""


# ------------------------------- SAMPLE CLASS HIERARCHY -------------------- #
class First(object): #pylint: disable=R0205,C0111
    def __init__(self,):
        print("first")

class Second(First): #pylint: disable=C0111
    def __init__(self): #pylint: disable=W0231
        super(Second, self).__init__()
        print("second")

class Third(First): #pylint: disable=C0111
    def __init__(self): #pylint: disable=W0231
        super(Third, self).__init__()
        print("third")

class Fourth(Second, Third): #pylint: disable=C0111
    def __init__(self):
        super(Fourth, self).__init__()
        print("fourth")


# ------------------------------- MAIN -------------------------------------- #
if __name__ == "__main__":
    fourth = Fourth()
    print([x.__name__ for x in Fourth.__mro__])
