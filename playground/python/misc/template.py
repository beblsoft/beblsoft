#!/usr/bin/env python3
"""
NAME
 template.py

DESCRIPTION
  Python template starter file
"""


# ----------------------- IMPORTS ------------------------------------------- #


# ----------------------- CONSTANTS ----------------------------------------- #
DOC_SAMPLE_CONSTANT = 10


# ----------------------- FUNCTIONS ----------------------------------------- #
def SampleFunc():
    """
    SampleFunc documentation
    """
    pass


# ----------------------- CLASSES ------------------------------------------- #
class SampleClass:
    """
    SampleClass documentation.
    """

    def __init__(self):
        """
        Sample initialization routine.
        """
        pass

    def SampleClassFunc(self):
        """
        SampleFunc documentation
        """
        pass


# ----------------------- MAIN------ ---------------------------------------- #
if __name__ == "__main__":
    pass
