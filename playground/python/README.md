# Python Technology Documentation

Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum
and first released in 1991, Python's design philosophy emphasizes code readability with its notable use
of significant whitespace. Its langue constructs an object-oriented approach aims to help programmers
write clear, logical code for small and large-scale projects.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language)),
[Standard Library](https://docs.python.org/3/library/)

## Standard Library

| Chappter | Title            | Description                                                                                           |
|:---------|:-----------------|:------------------------------------------------------------------------------------------------------|
| 5.4      | __exceptions__   | Create, throw, catch exceptions, errors                                                               |
| 6.2      | __re__           | Regular Expression Processing                                                                         |
| 6.7      | __readline__     | GNU readline interface                                                                                |
| 7.1      | __struct__       | Interpret bytes as packed binary data                                                                 |
| 7.2      | __codecs__       | Codec registry and base classes                                                                       |
| 8.1      | __datetime__     | Time access and conversions                                                                           |
| 8.2      | __calendar__     | Calendar-related functions                                                                            |
| 9.2      | __math__         | Mathematical Functions                                                                                |
| 10.2     | __functools__    | Higer-order functions and operations on callables                                                     |
| 11.10    | __shutil__       | High-level file operations                                                                            |
| 12.1     | __pickle__       | Object serialization                                                                                  |
| 13.2     | __gzip__         | Support for gzip files                                                                                |
| 14.1     | __csv__          | CSV file reading and writing                                                                          |
| 16.1     | __os__           | Operating system interfaces                                                                           |
| 16.2     | __io__           | Core tools for working with streams                                                                   |
| 16.4     | __argparse__     | Parse command line options                                                                            |
| 16.6     | __logging__      | Tie into the linux system logging interface. Different logging levels                                 |
| 16.10    | __curses__       | Terminal handling for character displays                                                              |
| 16.15    | __errno__        | Standard errno system symbols                                                                         |
| 16.16    | __ctypes__       | A foreign function library for python                                                                 |
| 17.1     | __threading__    | Thread-based parallelism. Spawn, reap threads. Instantiate, grab, release locks. Implement work queue |
| 18.1     | __socket__       | Low-level networking interface                                                                        |
| 18.2     | __ssl__          | TLS/SSL wrapper for socket objects                                                                    |
| 18.5     | __asyncio__      | asynchronous I/O, event loop, coroutines and tasks                                                    |
| 19.1     | __email__        | Email and MIME handling package                                                                       |
| 19.2     | __json__         | JSON encoder and decoder                                                                              |
| 21.1     | __webbrowser__   | web-browser controller                                                                                |
| 21.2     | __cgi__          | Common Gateway Interface support                                                                      |
| 21.4     | __wsgiref__      | WSGI Utilities and Reference Implementation                                                           |
| 21.5     | __urllib__       | URL handling modules                                                                                  |
| 21.11    | __http__         | HTTP modules                                                                                          |
| 21.13    | __ftplib__       | FTP protocol client                                                                                   |
| 21.15    | __imaplib__      | IMAP4 protocol client                                                                                 |
| 21.17    | __smtplib__      | SMTP protocol client                                                                                  |
| 21.19    | __telnetlib__    | Telnet client                                                                                         |
| 21.21    | __socketserver__ | Framework for network servers                                                                         |
| 22.1     | __audioop__      | Manipulate raw audio data                                                                             |
| 22.4     | __wave__         | Read and write WAV files                                                                              |
| 25.1     | __tkinter__      | Python interface to Tcl/Tk                                                                            |
| 26.1     | __typing__       | Support for type hints                                                                                |
| 26.2     | __pydoc__        | Documentation generator and online help system                                                        |
| 26.4     | __unittest__     | Unit testing framework                                                                                |
| 27.3     | __pdb__          | Python debugger                                                                                       |
| 27.5     | __timeit__       | Measure execution time                                                                                |
| 27.6     | __trace__        | Trace statement execution                                                                             |
| 28.1     | __distutils__    | Building and installing Python modules                                                                |
| 28.2     | __ensurepip__    | Bootstrapping the pip installer                                                                       |
| 28.3     | __venv__         | Creation virtual environments                                                                         |
| 28.4     | __zipapp__       | Manage executable python zip archives                                                                 |
| 29.1     | __sys__          | System-specific parameters and functions                                                              |
| 29.6     | __contextlib__   | Utilities for with-statement contexts                                                                 |
| 31.2     | __pkgutil__      | Package extension utility                                                                             |
| 31.4     | __runpy__        | Locating and executing Python modules                                                                 |
| 32.1     | __parser__       | Access python parse trees                                                                             |
| 35.9     | __fcntl__        | Fcntl, ioctl system calls                                                                             |

## Web, Networking

| Title            | Description                                                                                                              |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------------|
| AWS              | Boto3 Python API. S3-Create/destroy bucket, upload/download data. EC2-create/destroy VM, SSH Keys, run program, get logs |
| Flask            | WSGI Container, Routes, Blueprints                                                                                       |
| Beautiful Soup   | HTML Parser                                                                                                              |
| Requests library | HTTP verbs - create, get, put, post, delete. Send/Recieve JSON. Add/Remove/Read Cookies                                  |
| Skulpt           | Python to javascript                                                                                                     |
| Brython          | Python running in browser                                                                                                |
| Celery           | Distribuited task queue                                                                                                  |
| Zappa            | Integrate Flask with AWS API Gateway                                                                                     |

## Database, Persistence

| Title       | Description                                           |
|:------------|:------------------------------------------------------|
| PyMongo     | MongoDB Driver                                        |
| SQL Alchemy | Relational database ORM                               |
|             | Wrap over sqlite, MySql databases                     |
|             | Create sample schema with teachers, students, courses |
|             | Database migration                                    |
| cx_Oracle   | Interface to Oracle DB                                |

## Math, Science

| Title      | Description                                   |
|:-----------|:----------------------------------------------|
| Numpy      | BaseN-dimensional array package               |
| SciPy      | Scientific computing                          |
| Matplotlib | 2D Plotting                                   |
| Sympy      | Symbolic math                                 |
| Pandas     | Data structures and analysis                  |
| Jupyter    | Interactive plotting                          |
| IPython    | Interactive shell, data visualization support |

## Game, GUI

| Title  | Description                                        |
|:-------|:---------------------------------------------------|
| Pygame | 2d game development                                |
| Pyglet | 3d game animation                                  |
| kivy   | GUI builder, slick website: https://kivy.org/#home |
|        | Support on Linux, Windows, OS X, Android, and iOS  |
| pyQT   | GUI builder                                        |
| PyGTK  | GUI builder                                        |

## Image, Video, Sound

| Title         | Description                                     |
|:--------------|:------------------------------------------------|
| PIL           | Python Image Library                            |
| OpenCV-Python | Python Open Computer Vision                     |
| music21       | Symbolic notes                                  |
| Python DSP    | http://greenteapress.com/thinkdsp/thinkdsp.pdf  |
| Python MIDI   | Musical Instrument Digital Interface processing |

## TEST, INSTALL

| Title       | Description                                           |
|:------------|:------------------------------------------------------|
| nose        | Python testing library                                |
| coverage.py | Code coverage for python programs                     |
| PyInstaller | Packages python programs into stand-alone executables |

## 3rd Party APIs

[Reference](http://www.computersciencezone.org/50-most-useful-apis-for-developers/)

| Title        | Description                                                      |
|:-------------|:-----------------------------------------------------------------|
| Google APIs: | see https://developers.google.com/apis-explorer/#p/              |
|              | Analytics                                                        |
|              | AD Exchange Buyer                                                |
|              | AD Exchange Seller                                               |
|              | Fonts                                                            |
|              | Translate                                                        |
|              | Geocoding                                                        |
|              | Youtube                                                          |
| Social Media | Twitter                                                          |
|              | Facebook:http://facebook-sdk.readthedocs.io/en/latest/index.html |
|              | Google+                                                          |
|              | Interest                                                         |
|              | Tumblr                                                           |
|              | Instagram                                                        |
|              | Flickr                                                           |
|              | Meetup                                                           |
| Marketplaces | Foursquare                                                       |
|              | eBay                                                             |
|              | 3Taps                                                            |
| Misc         | Weather underground                                              |
|              | Tastekid                                                         |
|              | Klout                                                            |

## Code Analysis

| Title     | Description                                                      |
|:----------|:-----------------------------------------------------------------|
| Pylint    | Check python sytax                                               |
| PyChecker | Check python correctness                                         |
| PyFlakes  | Check python correctness. Tries hard not to emit false positives |
| MyPy      | Experimental optional static type checker                        |

## Misc

Find python package file

```python
import os.path
import my_module
print(os.path.abspath(my_module.__file__))
```