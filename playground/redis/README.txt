OVERVIEW
===============================================================================
- Redis Technology Documentation
- Description                       : Redis is an open source (BSD licensed), NoSQL, in-memory
                                      data structure key-value store
                                    : It can be used as a database, cache, and message broker.
                                    : It supports data structures such as strings, hashes, lists, sets,
                                      sorted sets with range queries, bitmaps, hyperloglogs, geospatial
                                      indexes with radius queries and streams.
- Features --------------------------
  * Transactions                    : Execution of a group of commands in a single atomic step
  * Pub/Sub                         : Publish Subscribe mechanism where senders are decoupled from receivers
  * Lua Scripting                   : Evaluate scripts using the Lua interpreter built into Redis
  * Keys with a limited time-to-live: Set keys to expire after a certain amount of time
  * LRU Eviction of keys            : Automatically evict old data from cache
  * Automatic Failover              : Sentinel monitors system and automatically fails over if master instance fails
- Relevant URLS ---------------------
  * Home                            : https://redis.io
  * Python Redis                    : https://pypi.org/project/redis/
  * Command Cheat Sheet             : https://gist.github.com/LeCoupa/1596b8f359ad8812c7271b5322c30946
  * Tutorial                        : http://try.redis.io/



SERVER INSTALLATION
===============================================================================
- Install redis                     : redis_install   [redis.sh]
- Uninstall redis                   : redis_uninstall [redis.sh]
- Open Redis CLI                    : redis-cli



PYTHON CLIENT
===============================================================================
- Install                           : Install redis server (see above)
                                      pip3 install redis
- Uninstall redis                   : pip3 uninstall redis
- Getting Started                   : >>> import redis
                                      >>> r = redis.Redis(host='localhost', port=6379, db=0, decode_respo)
                                      >>> r.set('foo', 'bar')
                                      True
                                      >>> r.get('foo')
                                      'bar'


COMMAND REFERENCE
===============================================================================
- Server ----------------------------
  * Start redis with config file    : redis-server /path/redis.conf
  * Open redis prompt               : redis-cli

- Strings ---------------------------
  * append a value to a key         : APPEND key value
  * count set bits in a string      : BITCOUNT key [start end]
  * set value in key                : SET key value
  * set if not exist value in key   : SETNX key value
  * overwrite part of a string at
    key starting at the specified
    offset                          : SETRANGE key offset value
  * get the length of the value
    stored in a key                 : STRLEN key
  * set multiple keys to multiple
    values                          : MSET key value [key value ...]
  * set multiple keys to multiple
    values, only if none of the keys
    exist                           : MSETNX key value [key value ...]
  * get value in key                : GET key
  * get a substring value of a key
    and return its old value        : GETRANGE key value
  * get the values of all the given
    keys                            : MGET key [key ...]
  * increment value in key          : INCR key
  * increment the integer value of a
    key by the given amount         : INCRBY key increment
  * increment the float value of a
    key by the given amount         : INCRBYFLOAT key increment
  * decrement the integer value of
    key by one                      : DECR key
  * decrement the integer value of a
    key by the given number         : DECRBY key decrement
  * delete key                      : DEL key
  * key will be deleted in 120
    seconds                         : EXPIRE key 120
  * returns the number of seconds
    until a key is deleted          : TTL key

- Lists ----------------------------: A list is a series of ordered values
  * put the new value at the end of
    the list                        : RPUSH key value [value ...]
  * append a value to a list, only
    if the exists                   : RPUSHX key value
  * put the new value at the start
    of the list                     : LPUSH key value [value ...]
  * give a subset of the
    list                            : LRANGE key start stop
  * get an element from a list by
    its index                       : LINDEX key index
  * insert an element before or
    after another element in a list : LINSERT key BEFORE|AFTER pivot value
  * return the current length of the
    list                            : LLEN key
  * remove the first element from
    the list and returns it         : LPOP key
  * set the value of an element in a
    list by its index               : LSET key index value
  * trim a list to the specified
    range                           : LTRIM key start stop
  * remove the last element from the
    list and returns it             : RPOP key
  * remove the last element in a
    list, prepend it to another list
    and return it                   : RPOPLPUSH source destination
  * remove and get the first element
    in a list, or block until one is
    available                       : BLPOP key [key ...] timeout
  * remove and get the last element
    in a list, or block until one is
    available                       : BRPOP key [key ...] timeout

- Sets -----------------------------: A set is similar to a list, except it does not
                                      have a specific order and each element may only appear once.
  * add the given value to the set  : SADD key member [member ...]
  * get the number of members in a
    set                             : SCARD key
  * remove the given value from the
    set                             : SREM key member [member ...]
  * test if the given value is in
    the set.                        : SISMEMBER myset value
  * return a list of all the members
    of this set                     : SMEMBERS myset
  * combine two or more sets and
    returns the list of all elements: SUNION key [key ...]
  * intersect multiple sets         : SINTER key [key ...]
  * move a member from one set to
    another                         : SMOVE source destination member
  * remove and return one or
    multiple random members from a
    set                             : SPOP key [count]

- Sorted Sets ----------------------: A sorted set is similar to a regular set, but now
                                      each value has an associated score.
                                      This score is used to sort the elements in the set.
  * add one or more members to a
    sorted set, or update its score
    if it already exists            : ZADD key [NX|XX] [CH] [INCR] score member [score member ...]  #
  * get the number of members in a
    sorted set                      : ZCARD key
  * count the members in a sorted
    set with scores within the
    given values                    : ZCOUNT key min max
  * increment the score of a member
    in a sorted set                 : ZINCRBY key increment member
  * returns a subset of the sorted
    set                             : ZRANGE key start stop [WITHSCORES]
  * determine the index of a member
    in a sorted set                 : ZRANK key member
  * remove one or more members from
    a sorted set                    : ZREM key member [member ...]
  * remove all members in a sorted
    set within the given indexes    : ZREMRANGEBYRANK key start stop
  * remove all members in a sorted
    set, by index, with scores
    ordered from high to low        : ZREMRANGEBYSCORE key min max
  * get the score associated with
    the given mmeber in a sorted set: ZSCORE key member
  * return a range of members in a
    sorted set, by score            : ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT offset count]

- Hashes ---------------------------: Hashes are maps between string fields and string values,
                                      so they are the perfect data type to represent objects.
  * get the value of a hash field   : HGET key field
  * get all the fields and values
    in a hash                       : HGETALL key
  * set the string value of a hash
    field                           : HSET key field value
  * set the string value of a hash
    field, only if the field does
    not exists                      : HSETNX key field value
  * set multiple fields at once     : HMSET key field value [field value ...]
  * increment value in hash by X    : HINCRBY key field increment
  * delete one or more hash fields  : HDEL key field [field ...]
  * determine if a hash field exists: HEXISTS key field
  * get all the fields in a hash    : HKEYS key
  * get all the fields in a hash    : HLEN key
  * get the length of the value
    of a hash field                 : HSTRLEN key field
  * get all the values in a hash    : HVALS key

- HyperLogLog ----------------------: Uses randomization in order to provide an approximation of the
                                      number of unique elements in a set using just a constant, and small,
                                      amount of memory
  * add the specified elements
    to the specified HyperLogLog    : PFADD key element [element ...]
  * return the approximated
    cardinality of the set(s)
    observed by the HyperLogLog at
    key's)                          : PFCOUNT key [key ...]
  * merge N HyperLogLogs into
    a single one                    : PFMERGE destkey sourcekey [sourcekey ...]

- Publication and Subscription ------
  * listen for messages published to
    channels matching the given
    patterns                        : PSUBSCRIBE pattern [pattern ...]
  * inspect the state of the Pub/Sub
    subsystem                       : PUBSUB subcommand [argument [argument ...]]
  * post a message to a channel     : PUBLISH channel message
  * stop listening for messages
    posted to channels matching the
    given patterns                  : PUNSUBSCRIBE [pattern [pattern ...]]
  * listen for messages published to
    the given channels              : SUBSCRIBE channel [channel ...]
  * stop listening for messages
    posted to the given channels    : UNSUBSCRIBE [channel [channel ...]]

- Other Commands --------------------
  * find all keys matching the given
    pattern                         : KEYS pattern
