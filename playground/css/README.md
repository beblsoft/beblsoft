# CSS Documentation

Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a
document written in a markup language like HTML. CSS describes how elements should be rendered on screen,
on papager, in speech, or on other media.

CSS is one of the core languages of the open Web and is standardized across Web browsers according to
the [W3C Specification](https://www.w3.org/Style/CSS/#specs).

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
[Mozilla Docs](https://developer.mozilla.org/en-US/docs/Web/CSS),
[W3Schools](https://www.w3schools.com/css/default.asp)

## How CSS Works

A __document__ is usually a text file structured using _markup language_. HTML is the most common
markup language, but others exist such as SVG or XML.

__Presenting__ a document to a user means converting it into a usable form for your audience.
Browsers like Firefox, Chrome, or Internet Explorner are designed to present documents visually
on a computer screen, projector, or printer.

Web browsers apply __CSS rules__ to a document to affect how they are displayed. A CSS rule is formed
from:
- A _selector_, which selects the elements you want to update
- A set of _properties_, which have values set to update the selector

A set of CSS rules contained within a __stylesheet__ determines how a webpage should look.

### Example

```html
<!-- HTML Document -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My CSS experiment</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Hello World!</h1>
    <p>This is my first CSS example</p>
  </body>
</html>
```

```css
/* CSS Used to Style HTML */
h1 {
  color: blue;
  background-color: yellow;
  border: 1px solid black;
}

p {
  color: red;
}
```
![](https://mdn.mozillademos.org/files/12537/first-example.png)

### Displaying a Document

When a browser displays a document, it must combine the document's content with its style information.
It processes the document in two stages:

1. The browser converts HTML and CSS into the DOM (Document Object Model). The DOM represents the
   document in the computer's memory. The DOM combines the document's content with its style.

2. The browser displays the contents of the DOM

![](https://mdn.mozillademos.org/files/11781/rendering.svg)

### The DOM

A DOM has a tree-like structure. Each element, attribute and piece of text in the markup language
becomes a DOM node in the tree structure. The nodes are defined by their relationship to other
DOM nodes. Some elements are parents of child nodes, and child nodes have siblings.

Let's assume the following HTML code:
```html
<p>
  Let's use:
  <span>Cascading</span>
  <span>Style</span>
  <span>Sheets</span>
</p>
```

In the DOM the node corresponding to our `<p>` element is a parent. Its children are a text node
and the nodes corresponding to our `<span>` elements.
```text
P
├─ "Let's use:"
├─ SPAN
|  └─ "Cascading"
├─ SPAN
|  └─ "Style"
└─ SPAN
   └─ "Sheets"
```

This is how a browser interprets the previous HTML snippet.
```text
Let's use: Cascading Style Sheets
```

### Applying CSS to HTML

Therre are threee different ways to apply CSS to an HTML document.

1. External Stylesheet

   An external stylesheet is when you have your CSS written in a separate file with a `.css` extension,
   and your reference it from an HTML `<link>` element.

   This method is arguably the best, as you can use one stylesheet to style multiple documents
   and only need to update the CSS in one place if changes are needed.

   HTML file:
   ```html
   <!DOCTYPE html>
   <html>
     <head>
       <meta charset="utf-8">
       <title>My CSS experiment</title>
       <!-- Links in CSS stylesheet -->
       <link rel="stylesheet" href="style.css">
     </head>
     <body>
       <h1>Hello World!</h1>
       <p>This is my first CSS example</p>
     </body>
   </html>
   ```

   CSS file:
   ```css
   h1 {
     color: blue;
     background-color: yellow;
     border: 1px solid black;
   }

   p {
     color: red;
   }
   ```

2. Internal Stylesheet

   An internal stylesheet is where you don't have an external CSS file, but instead place your
   CSS inside a `<style>` element, contained inside the HTML head.

    ```html
   <!DOCTYPE html>
   <html>
     <head>
       <meta charset="utf-8">
       <title>My CSS experiment</title>
       <style>
         h1 {
           color: blue;
           background-color: yellow;
           border: 1px solid black;
         }

         p {
           color: red;
         }
       </style>
     </head>
     <body>
       <h1>Hello World!</h1>
       <p>This is my first CSS example</p>
     </body>
   </html>
   ```

3. Inline Styles

   Inline styles are CSS declarations that affect one element only, contained within a `style`
   attribute.

   This method is NOT recommended. You might have to update the same information multiple times
   per document and it also mixes your presentational CSS information with your HTML structural
   information, making the CSS harder to read and understand. Keeping your different types of code
   separated and pure makes for a much easier job for all who work on the code.

    ```html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>My CSS experiment</title>
      </head>
      <body>
        <h1 style="color: blue;background-color: yellow;border: 1px solid black;">Hello World!</h1>
        <p style="color:red;">This is my first CSS example</p>
      </body>
    </html>
    ```

## CSS Syntax

### Building Blocks

At its most basic level, CSS consists various building blocks:

- __Properties__: Human-readable identifiers that indicate which stylistic features (e.g. font, width,
    backgroud color) you want to change. There are more than 300 different properties in CSS.

- __Values__: Each specified property is given a value, which indicates how you want to change those
  stylistic features

- __CSS declaration__: the pairing of property and value. (e.g `background-color: red`)
  The property and value in each pair is separated by a colon. If the property is unknown or if
  a value is not valid, the declaration is deemed invalud and is wholly ignored by the browser's
  CSS engine.

- __CSS Declaration Blocks__: block of properties and values. Each block of declarations is wrapped
  by braces `{ ... }`. THe declarations inside a declaration block must be separated by semi-colons `;`,
  otherwise the code won't work. The last declaration of a block doesn't need to be terminated by a semi-colon.
  However, it is _good style_ to do so. It is perfectly valid for a declaration block to be empty.
  ```css
  {
    colour: blue;
    background-color: yellow;
    border: 1px solid black;
  }
  ```

- __Selector__: A selector is a pattern that matches some elements on the page.
  (e.g. `h1`, `p`, `.some-class`, `#some-id`).

- __CSS Rulesets__: Pairing of a CSS declaration block with a selector.
  ```
  h1 {
    colour: blue;
    background-color: yellow;
    border: 1px solid black;
  }
  ```

- __At-rules__: Used in CSS to convey metadata, conditional information, or other descriptive information.
  They start with an at sign (`@`), followed by an identifier to say what kind of rule it is. Each
  type of at-rule, defined by the identifier, has its own internal syntax and semantics. Examples
  include
    * `@charset` and `@import` - metadata
      Ex. `@import 'custom.css'`
    * `@media` or `@document` - conditional information, also called nested statements
    * `@font-face` - descriptive information

- __Nested statements__: Are a specific subset of at-rule. The syntax of which is a nested block of CSS
  that will only be applied to the document if a specific condition is matched.
    * `@media`- only applied if the device which runs in the browser matches the expressed condition
      ```css
      @media (min-width: 801px) {
        body {
          margin: 0 auto;
          width: 800px;
        }
      }
      ```
    * `@supports` - only applied if the browser actually supports the tested feature
    * `@docuemt` - only applied if the current page matches some conditions

### Readability

Below are some best practices for writing maintainable CSS:

- __White Space__: White space means actual spaces, tabs, and new lines. You can add white space to
  make your stylesheets more readable. In the same manner as HTML, the browser tends to ignore much
  of the whitespace inside your CSS. A lot of the whitespace is just there to aid readability for
  humans.

  Preferable:
  ```css
  body {
    font: 1em/150% Helvetica, Arial, sans-serif;
    padding: 1em;
    margin: 0 auto;
    max-width: 33em;
  }

  @media (min-width: 70em) {
    body {
      font-size: 130%;
    }
  }

  h1 {
    font-size: 1.5em;
  }

  div p, #id:first-line {
    background-color: red;
    background-style: none
  }

  div p {
    margin: 0;
    padding: 1em;
  }

  div p + p {
    padding-top: 0;
  }
  ```

  Not preferable:
  ```css
  body {font: 1em/150% Helvetica, Arial, sans-serif; padding: 1em; margin: 0 auto; max-width: 33em;}
  @media (min-width: 70em) { body {font-size: 130%;} }

  h1 {font-size: 1.5em;}

  div p, #id:first-line {background-color: red; background-style: none}
  div p {margin: 0; padding: 1em;}
  div p + p {padding-top: 0;}
  ```

- __Comments__: As with HTML, you are encouraged to make comments in your css, to help you understand
  how your code works when coming back to it after several months, and to help others understand it.
  Comments are also useful for temporarily commenting out certain parts of the code for testing
  purposes. Comments are made with `/* .... */`.
  ```css
  /* Handle basic element styling */
  /* -------------------------------------------------------------------------------------------- */
  body {font: 1em/150% Helvetica, Arial, sans-serif; padding: 1em; margin: 0 auto; max-width: 33em;}
  @media (min-width: 70em) {
    /* Let's special case the global font size. On large screen or window,
       we increase the font size for better readability */
    body {font-size: 130%;}
  }

  h1 {font-size: 1.5em;}

  /* Handle specific elements nested in the DOM  */
  /* -------------------------------------------------------------------------------------------- */
  div p, #id:first-line {background-color: red; background-style: none}
  div p                 {margin          :   0; padding         : 1em;}
  div p + p             {padding-top     :   0;                       }
  ```

- __Shorthand__: Some properties like `font`, `background`, `padding`, `border`, and `margin` are called
  shorthand properties. This is because they allow you to set several property values in a single line,
  saving time and making your code neater in the process.

  This line:
  ```css
  /* in shorthand like padding and margin, the values are applied
     in the order top, right, bottom, left (the same order as an analog clock). There are also other
     shorthand types, for example two values, which set for example
     the padding for top/bottom, then left/right */
  padding: 10px 15px 15px 5px;
  ```
  Does the same as these:
  ```css
  padding-top: 10px;
  padding-right: 15px;
  padding-bottom: 15px;
  padding-left: 5px;
  ```

## Selectors

### Types

Selectors can be divided into the following categories:

- __Type,element selectors__: Match one or more elements based on element type, `class`, or `id`

- __Attribute selectors__: Match one or more elements based on their attributes/attribute values

- __Pseudo-classes__: Match one or more elements that exist in a certain state, such as an element
  that is being hovered over by a mouse pointer

- __Pseudo-elements__: Match one or more parts of content that are in a certain position in relation
  to an element. For example, the first word of each paragraph.

- __Combinators__: Ways of combining two or more selectors in useful ways for very specific selections.
  For example, only select paragraphs that are direct descendants of divs.

- __Multiple selectors__: One can put multiple selectors together, each separated by a comma to apply
  a single set of declarations to all elements selected.

### Element Selector

This selector is just a case-insensitve match between the selector name and a given HTML element
name. This is the simplest way to target all elements of a given type.

For example

```html
<p>What color do you like?</p>
<div>I like blue.</div>
<p>I prefer BLACK!</p>
```

```css
/* All p elements are red */
p {
  color: red;
}

/* All div elements are blue */
div {
  color: blue;
}
```

### Class Selector

A class selector consists of a dot `.`, followed by a class name. A class name is any value, without
spaces, placed within an HTML `class` attribute. It is up to you to choose a name for the class. Multiple
elements in a document can have the same class value, and a single element can have multiple class names
separated by white space.

For example:
```html
<ul>
  <li class="first done">Create an HTML document</li>
  <li class="second done">Create a CSS style sheet</li>
  <li class="third">Link them all together</li>
</ul>
```

```css
/* The element with the class "first" is bolded */
.first {
  font-weight: bold;
}

/* All the elements with the class "done" are strikethrough */
.done {
  text-decoration: line-through;
}
```

### ID Selector

The ID selector consists of a hash/pound symbol `#`, followed by the ID name of a given element.
Any element can have a unique ID name set with the `id` attribute. It is up to you to choose
an ID name.

For example:
```html
<p id="polite"> — "Good morning."</p>
<p id="rude"> — "Go away!"</p>
```

```css
#polite {
  font-family: cursive;
}

#rude {
  font-family: monospace;
  text-transform: uppercase;
}
```

### Universal Selector

The universal selector (`*`) allows selecting all elements on a page. As it is rarely used to apply
a style to every element on a page, it is often used in combination with other selectors.

Note: using the universal selector in large web pages can have a perceptible performance impact.
Web pages will display slower than expected. There are not many instances where you should use
this selector.

For example:
```html
<div>
  <p>I think the containing box just needed
  a <strong>border</strong> or <em>something</em>,
  but this is getting <strong>out of hand</strong>!</p>
</div>
```

```css
* {
  padding: 5px;
  border: 1px solid black;
  background: rgba(255,0,0,0.25)
}
```

### Attribute Selectors

Attribute selectors are a special kind of selector that will match elements based on their attributes
and attribute values. Their generic syntax consists of square brackets (`[]`) containing an attribute
name followed by an optional condition to match against the value of the attribute.

__Presence and value attribute selectors__ try to match an exact attribute value:
- `[attr]`: This selector will select all elements with the attribute `attr`, whatever the value
- `[attr=val]`: This selector will select all slements with the attribute `attr`, but only if its
  value is `val`
- `[attr~=val]`: This selector will select all elements with the attribute `attr`, but only if `val`
  is one of the space-separated list of words contained in `attr's` value

For example:
```html
Ingredients for my recipe: <i lang="fr-FR">Poulet basquaise</i>
<ul>
  <li data-quantity="1kg" data-vegetable>Tomatoes</li>
  <li data-quantity="3" data-vegetable>Onions</li>
  <li data-quantity="3" data-vegetable>Garlic</li>
  <li data-quantity="700g" data-vegetable="not spicy like chili">Red pepper</li>
  <li data-quantity="2kg" data-meat>Chicken</li>
  <li data-quantity="optional 150g" data-meat>Bacon bits</li>
  <li data-quantity="optional 10ml" data-vegetable="liquid">Olive oil</li>
  <li data-quantity="25cl" data-vegetable="liquid">White wine</li>
</ul>
```

```css
/* All elements with the attribute "data-vegetable"
   are given green text */
[data-vegetable] {
  color: green;
}

/* All elements with the attribute "data-vegetable"
   with the exact value "liquid" are given a golden
   background color */
[data-vegetable="liquid"] {
  background-color: goldenrod;
}

/* All elements with the attribute "data-vegetable",
   containing the value "spicy", even among others,
   are given a red text color */
[data-vegetable~="spicy"] {
  color: red;
}
```

__Substring value attribute selectors__ are also know as "RegExp-like selectors". They offer flexible
matching in a similar fashion to regular expressions (but to be clear, these selectors are not true
regular expression):
- `[attr^=val]`: This selector will select all elements with the attribute `attr` for which the value
  starts with `val`

- `[attr|=val]`: This selector will select all elements with the attribute `attr` for which the value
  is exactly `val` or starts with `val-`

- `[attr$=val]`: This selector will select all elements with the attribute `attr` for which the value
  ends with `val`

- `[attr*=val]`: This selector will select all elements with the attribute `attr` for which the value
  contains the substring `val`

For example:
```css
/* Classic usage for language selection */
[lang|="fr"] {
  font-weight: bold;
}

/* All elements with the attribute "data-quantity", for which
   the value starts with "optional" */
[data-quantity^="optional"] {
  opacity: 0.5;
}

/* All elements with the attribute "data-quantity", for which
   the value ends with "kg" */
[data-quantity$="kg"] {
  font-weight: bold;
}

/* All elements with the attribute "data-vegetable" containing
   the substring "not spicy" are turned back to green */
[data-vegetable*="not spicy"] {
  color: green;
}
```

### Pseudo-Classes

A CSS __pseudo-class__ is a keyword added to the end of a selector, preceded by a colon (`:`),
which is used to specify that you want to style the selected element but only when it is in a
certain state.

For example, you might want to style a link element only when it is being hovered over by the mouse
pointer or a checkbox when it is disabled or checked.

List:

- `:active`
- `:checked`
- `:default`
- `:dir`
- `:disabled`
- `:empty`
- `:enabled`
- `:first`
- `:first-child`
- `:first-of-type`
- `:fullscreen`
- `:focus`
- `:focus-within`
- `:hover`
- `:interminate`
- `:in-range`
- `:invalid`
- `:lang`
- `:last-child`
- `:last-of-type`
- `:left`
- `:link`
- `:matches()`
- `:not`
- `:nth-child`
- `:not`
- `:nth-child`
- `:nth-last-child`
- `:nth-last-of-type`
- `:nth-of-type`
- `:only-child`
- `:only-of-type`
- `:optional`
- `:out-of-range`
- `:read-only`
- `:read-write`
- `:required`
- `:right`
- `:root`
- `:scope`
- `:target`
- `:valid`
- `:visited`

Example use:
```html
<a href="https://developer.mozilla.org/" target="_blank">Mozilla Developer Network</a>
```

```css
/* These styles will style our link
   in all states */
a {
  color: blue;
  font-weight: bold;
}

/* We want visited links to be the same color
   as non visited links */
a:visited {
  color: blue;
}

/* We highlight the link when it is
   hovered over (mouse over), activated (mouse down)
   or focused (keyboard) */
a:hover,
a:active,
a:focus {
  color: darkred;
  text-decoration: none;
}
```

### Pseuod-Elements

Pseudo-elements are very much like pseudo-classes. They are keywords, this time preceded by two
colons (`::`), that can be added to the end of selectors to select a certain part of an element.

List:

- `::after`
- `::before`
- `::first-letter`
- `::first-line`
- `::selection`
- `::backdrop`

For example:
```html
<ul>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/CSS">CSS</a> defined in the MDN glossary.</li>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML">HTML</a> defined in the MDN glossary.</li>
</ul>
```

```css
/* All elements with an attribute "href" with values
   starting with "http" will have an arrow added after their
   content (to indicate they are external links) */
[href^=http]::after {
  content: '⤴';
}
```

### Combinators

Using one selector at a time is useful, but can be inefficient in some situations. CSS selectors
become even more useful when you start combining them to perform fine-grained selections.
CSS has several ways to select elements based on how they are related to one another.

| Name                        | Syntax | Selects                                                                                                                                                                                |
|:----------------------------|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Selector list               | A, B   | Any element matching A and/or B (see Groups of selectors on one rule, below - Group of Selectors is not considered to be a combinator).                                                |
| Descendant combinator       | A B    | Any element matching B that is a descendant of an element matching A (that is, a child, or a child of a child, etc.). the combinator is one or more spaces or dual greater than signs. |
| Child combinator            | A > B  | Any element matching B that is a direct child of an element matching A.                                                                                                                |
| Adjacent sibling combinator | A + B  | Any element matching B that is the next sibling of an element matching A (that is, the next child of the same parent).                                                                 |
| General sibling combinator  | A ~ B  | Any element matching B that is one of the next siblings of an element matching A (that is, one of the next children of the same parent).                                               |

Note: if a single selector is not supported by the browser, then the whole selector list gets ignored.

For example:
```html
<table lang="en-US" class="with-currency">
  <thead>
    <tr>
      <th scope="col">Product</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="2" scope="row">Total:</th>
      <td>148.55</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td>Lawnchair</td>
      <td>1</td>
      <td>137.00</td>
    </tr>
    <tr>
      <td>Marshmallow rice bar</td>
      <td>2</td>
      <td>1.10</td>
    </tr>
    <tr>
      <td>Book</td>
      <td>1</td>
      <td>10.45</td>
    </tr>
  </tbody>
</table>
```

```css
/* Basic table setup */
table {
  font: 1em sans-serif;
  border-collapse: collapse;
  border-spacing: 0;
}

/* All <td>s within a <table> and all <th>s within a <table>
   Comma is not a combinator, it just allows you to target
   several selectors with the same CSS ruleset */
table td, table th {
  border: 1px solid black;
  padding: 0.5em 0.5em 0.4em;
}

/* All <th>s within <thead>s that are within <table>s */
table thead th {
  color: white;
  background: black;
}

/* All <td>s preceded by another <td>,
   within a <tbody>, within a <table> */
table tbody td + td {
  text-align: center;
}

/* All <td>s that are a last child,
   within a <tbody>, within a <table> */
table tbody td:last-child {
  text-align: right;
}

/* All <th>s, within a <tfoot>s, within a <table> */
table tfoot th {
  text-align: right;
  border-top-width: 5px;
  border-left: none;
  border-bottom: none;
}

/* All <td>s preceded by a <th>, within a <table> */
table th + td {
  text-align: right;
  border-top-width: 5px;
  color: white;
  background: black;
}

/* All pseudo-elements "before" <td>s that are a last child,
   appearing within elements with a class of "with-currency" that
   also have an attribute "lang" with the value "en-US" */
.with-currency[lang="en-US"] td:last-child::before {
  content: '$';
}

/* All pseudo-elements "after" <td>s that are a last child,
   appearing within elements with the class "with-currency" that
   also have an attribute "lang" with the value "fr" */
.with-currency[lang="fr"] td:last-child::after {
  content: ' €';
}
```

## Values and Units

There are many value types in CSS.
- __Numeric Values__: Length values for specifying e.g. element width, border thickness, or font
  size, and unitless integers
- __Percentages__: Specifying size or length
- __Colors__: Specifying background colors, text colors, etc...
- __Functions__: Specifying e.g. backgroud images or background image gradients

### Length and Size Values

You'll use length and size units all the time in your CSS for layous, typography, and more.

For example:
```html
<p>This is a paragraph.</p>
<p>This is a paragraph.</p>
<p>This is a paragraph.</p>
```
```css
p {
  margin: 5px;
  padding: 10px;
  border: 2px solid black;
  background-color: cyan;
}
p:nth-child(1) {
  width: 150px;
  font-size: 18px;
}
p:nth-child(2) {
  width: 250px;
  font-size: 24px;
}
p:nth-child(3) {
  width: 350px;
  font-size: 30px;
}
```

Pixels(px) are referred to as __absolute units__ because they will always be the same size regardless
of any other related settings. Other absolute units are as follows:

- `q`: Quarter millimeters
- `mm`: millimeters
- `cm`: centimeters
- `in`: inches
- `pt`: points (1/72 of an inch)
- `pc`: picas (12 points)

There are also relative units, which are relative to the current element's font-size or viewport
size:

- `em`: 1 em is the same as the font-size of the current element. The default base font-size given
  to web pages by web browsers before CSS styling is applied is 16 pixels. `em` are the most common
  relative unit you'll use in web development__
- `ex`: Height of a lower case x
- `ch`: Width of the number 0
- `rem`: The root em works exactly the same way as `em`, except that it will always equal the size
   of the default base font-size; inherited font sizes will have no effect
- `vw`: 1/100 of the width of the viewport
- `vh`: 1/100 of the height of the viewport

Relative unts are quite useful - you can size your HTML elements relative to your font or viewport
size, so that the layout will still look correct.

### Unitless Values

You'll sometimes come across unitless numeric values in CSS. This is not always an error. In fact,
it is perfectly allows in some circumstances.

```css
margin: 0;
```

### Percentages

You can use percentage values to specify most things that can be specified by specific numeric values.
This allows us to create, for example, boxes whose width will always shift to be a certain percentage
of their parent container's width.

__Liquid layouts__: Leverage percentages to shift the content as the browser viewport changes
__Fixed width layout__: Stays the same regardless of viewport changes

### Colors

There are many ways to specify color in CSS, some of which were implemented more recently than others.
The same color values can be used everywhere in CSS, whether you are specifying text color, background
color, or whatever else.

The standard color system available in modern computers is __24 bit__, which allows the display of about
16.7 million distinct colors via a combination of different __red__, __green__, and __blue__ channels,
with 256 values per channel. (`256 x 256 x 256 = 16,777,216`)

- __Keywords__: The simplest, oldest color types in CSSS are the color keywords. These are specific
  strings representing particular color values.
  ```css
  p {
    background-color: red;
  }
  ```
  There are 165 differnt keywords available for modern web browsers.
  [Full Color List](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)

- __Hexadecimal Values__: The next ubiquitous color system is hexadecimal colors, or hex codes.
  Each hex value consists of a hash/point symbol `#` followed by size hex numbers. Each can take
  a value between 0 and f (which represnets 15) - so 0123456789abcdef.
  ```css
  /* equivalent to the red keyword */
  p:nth-child(1) {
    background-color: #ff0000;
  }
  ```

- __RGB__: The third scheme is RGB. The RGB value is a function - `rgb()` - which is given three
  parameters that represent __red__, __green__, and __blue__ channel values of the colors
  ```css
  /* equivalent to the red keyword */
  p:nth-child(1) {
    background-color: rgb(255,0,0);
  }
  ```

- __HSL__: The `hsl()` function accepts __hue__, __saturation__, and __lightness__ values, which
  are used to distinguish between the 16.7 million collors as follows:
    * __hue__: the base shade of the color. This takes a value between 0 and 360, representing the
      angles in a round color wheel
    * __saturation__: how saturated is the color? This takes a value from 0-100%. Where 0 is no color
      (it will appear as a shade of grew) and 100% is full color saturation
    * __lightness__: how light or bright is the color? This takes a value of 0-100%, where 0 is no light
      (it will appear completely black) and 100% is full light (it will appear completely white)

  ```css
  /* equivalent to the red keyword */
  p:nth-child(1) {
    background-color: hsl(0,100%,50%);
  }
  ```

- __RGBA and HSLA__: RGB and HSL both have corresponding modes - RGBA and HSLA - that allow you to set
  not only what color to display, but also what __transparency__ you want the color to have. Their corresponding
  functions take the same parameters, plus the fourth value in the range 0-1 - which sets the transparency or
  __alpha channel__. 0 is completely transparent, and 1 is completely opaque.

  ```css
  /* Transparent red */
  p:nth-child(1) {
    background-color: rgba(255,0,0,0.5);
  }
  ```

- __Opacity__: There is another way to specify transparency via CSS - the `opacity` property.
  Instead of setting the transparency of a particular color, this sets the transparency of all
  selected elements and __all their children__ (be careful).
  ```css
  /* Red with opacity */
  p:nth-child(2) {
    background-color: rgb(255,0,0);
    opacity: 0.5;
  }
  ```

### Functions

In programming, a function is a reusable section of code that can be run multiple times to complete
a repetitive task with minimum effort on the part of both the developer and the computer. Anytime
you see a name with parenthesis after it, containing one or more values separated by commas, you are
dealing with a function.

For example:
```css
/* Set the background color */
background-color: rgba(255,0,0,0.5);
background-color: hsla(240,100%,50%,0.5);

/* calculate the new position of an element after it has been rotated by 45 degress */
transform: rotate(45deg);

/* calculate the new position of an element after it has been moved across 50px and down 60px */
transform: translate(50px, 60px);

/* calculate the computed value of 90% of the current width minus 15px */
width: calc(90% - 15px);

/* fetch an image from the network to be used as a background image */
background-image: url('myimage.png');

/* create a gradient and use it as a background image */
background-image: linear-gradient(to left, teal, aquamarine);
```

## Cascading

CSS is an abbreviation for _Cascading Style Sheets_, which indicates that the notion of the cascade
is important. At its most basic level, it indicates that the order of CSS rules matter, but it's more
complex than that. What selectors win out in the cascade depends on three factors (listed in order
of weight - earlier ones overrule later):

1. Importance
2. Specificity
3. Source Order

### Importance

In CSS, there is a special piece of syntax you can use to make sure that a certain declaration will
_always_ win over all others: `!important`.

For example:
```html
<p class="better">This is a paragraph.</p>
<p class="better" id="winning">One selector to rule them all!</p>
```

```css
#winning {
  background-color: red;
  border: 1px solid black;
}

.better {
  background-color: gray;
  border: none !important; /* Overries border from #winning */
}
```

The only way to override `!important` declaration would be to include another `!important` declaration
of the same specificty later in the source order, or one with higher specificity.

It is __strongly recommended__ that you __never__ use `!important` unless you absolutely have to.
It makes debugging large style sheets very difficult.

### Specificity

__Specificity__ is a measure of how specific a selector is - how many elements it could match.

The amount of specificity a selector has is measured using four different values.

1. Thousands: Score one in this column if the declaration is inside a `style` attribute,
   aka inline styles. Such declarations don't have selectors, so specificity is simply 1000.

2. Hundreds: Score one in this column for each ID selector contained inside the overall selector

3. Tens: Score one in this column for each class selector, attribute selector, or pseudo-class
   contained inside the overall selector

4. Ones: Score one in this column for each element selector or pseudo-element contained inside the
   overall selector

| Selector                                                     | Thousands | Hundreds | Tens | Ones | Total specificity |
|:-------------------------------------------------------------|:----------|:---------|:-----|:-----|:------------------|
| h1                                                           | 0         | 0        | 0    | 1    | 0001              |
| h1 + p::first-letter                                         | 0         | 0        | 0    | 3    | 0003              |
| li > a[href*="en-US"] > .inline-warning                      | 0         | 0        | 2    | 2    | 0022              |
| #identifier                                                  | 0         | 1        | 0    | 0    | 0100              |
| No selector, with a rule inside an element's style attribute | 1         | 0        | 0    | 0    | 1000              |

If multiple selectors have the same importance and specificity, which selector wins is decided by
which comes later in the Source order.

### Source Order

If multiple competing selectors have the same importance and specificity, the third factor that
comes into play to help decide which rule wins is source order - later rules will win over earlier
rules.

```css
p {
  color: blue;
}

/* This rule will win over the first one */
p {
  color: red;
}
```

### Mixing

One thing to keep in mind when cascading styles is that cascading is applied at the property level.
When several CSS rules match the same element, they are all applied to that element. Only after
that are any conflicting properties evaluated to see which individual styles win over others.

```html
<p>I'm <strong>important</strong></p>
```

```css
/* specificity: 0002 */
p strong {
  background-color: khaki; /* Is shown */
  color: green; /* Overrides color below */
}

/* specificity: 0001 */
strong {
  text-decoration: underline; /* Is shown */
  color: red;
}
```

## Inheritance

CSS inheritance is the idea that some property values applied to an element will be inherited by that
element's children, and some won't.

For example, `font-family` and `color` are inherited. It is easy for you to set a site-wide base font
by applying a font-family to the `html` element; you can then override the fonts on individual elements
where needed.

As another example, `margin`, `padding`, `border` and `background-image` are NOT inherited.

Which properties are inherited by default and which are not can be found in
[CSS Reference](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference).

CSS provides four special universal property values for specifying inheritance:

- __inherit__: Sets the property value applied to a selected element to be the same as that of its
  parent element
  ```css
    .my-class-1 a {
    color: inherit;
  }
  ```

- __initial__: Sete the property value applied to a selected element to be the same as the value
  set for that element in the browser's default style sheet. If no value is set by the browser's
  default style sheet and the property is naturally inherited, then the property value is set to
  `inherit` instead.

  Note: Not supported by Internet explorer.

- __unset__: Resets the property to its natural value. If the property is naturally inherited it acts
  like `inherit`, otherwise it acts like `initial`

  Note: Not supported by Internet explorer.

- __revert__: Reverts the property to the value it would have had if the current origin had not applied
  any styles to it.

The CSS shorthand property `all` can be used to apply one of these inheritance values to (almost)
all properties at once. Its value can be any of the inheritance values (`inherit`, `initial`, `unset`,
or `revert`)

## Box Model

The CSS box model is the foundation of layout on the Web - each element is represented as a rectangular
box, with the box's content, padding, border, and margin built up around one another like layers of
an onion.

### Box Properties

Every element within a document is structured as a rectangular box inside the document layout, the
size of which can be tweaked using specific CSS properties. The relevant properties are as follows:

- __width and height__: The width and height properties set the width and height of the content box,
  which is the area in which the content of the box is displayed.

  Other properties exist that allow more subtle ways of handling content box size - setting size
  contstrains rather than absolute size: `min-width`, `max-width`, `min-height`, and `max-height`

- __padding__: padding refers to the inner margin of a CSS box - between the outer edge of the content
  box and the inner edge of the border. The size of this layer can be set on all four sides at once
  with the `padding` shorthand property, or one side at a time with the `padding-top`, `padding-right`,
  `padding-bottom`, and `padding-left` properties

- __border__: Sits between the outer edge of padding and the inner edge of the margin. By default the border
  has size of 0 - making it invisible. The `border` shorthand property allows you to set the thickness,
  style, and color of the border. For example: `border: 1px solid black`.

    * `border-top`, `border-right`, `border-bottom`, `border-left`: Set the thickness, style and color
      of one side of the border.

    * `border-width`, `border-style`, `border-color`: Set only the thickness, style, or color
      individually, but for all four sides of the border.

    * You can also set one of the three properties of a single side of the border individually,
      using `border-top-width`, `border-top-style`, `border-top-color`, etc.

- __margin__: Surrounds the CSS box and pushes up against other CSS boxes in the layout. It behaves
  rather like `padding`. The shorthand property is `margin` and the individual properties are `margin-top`,
  `margin-right`, `margin-bottom`, and `margin-left`.

  Margins have a specific behavior called _margin collapsing_: When two boxes touch one another, the distance
  between them is the value of the larges of the two touching margines, not their sum.

![](https://mdn.mozillademos.org/files/13647/box-model-standard-small.png)

### Advanced Box Manipulation

- __Overflow__: When you set the size of a box with absolute values, the content may not fit within
  the allowed size, in which case the content overflows the box. To control what happens in such cases,
  you can use the `overflow` property. It takes several values:

    * `auto`: If there is too much content, the overflowing content is hidden and scroll bars are
      shown to let the user scroll to see all the content

    * `hidden`: If there is too much content, the overflowing content is hidden

    * `visible`: If there is too much content, the overflowing content is shown outside of the box

- __Background Clip__: Box backgrounds are made up of colors and images, stacked on top of each other
  (`background-color`, `background-image`). They are applied to a box and drawn under that box. By default,
  backgrounds extend to the outer edge of the border. However, this is configurable.
  ```css
  .default     { background-clip: border-box;  } /* Extend through border, then stop */
  .padding-box { background-clip: padding-box; } /* Extend through padding, stop before border */
  .content-box { background-clip: content-box; } /* Extend through content, stop before padding */
  ```

- __Outline__: The outline of a box is something that looks like a border but which is not part of the
  box model. It behaves like the border but is drawn on top of the box without changing the size of the box.

  Note: Beware when using the outline property. It is used in some cases for accessibility reasons to
  highlight active parts of a web page such as links when a user clicks them.

### Types of Boxes

The type of box applied to an element is specified by the `display` property. There are various
values available for `display`. Here are the common ones:

- __block__: A block box is defined as a box that's stacked upon other boxes and can have width
  and height set on it.

- __inline__: An inline box is the opposite of a block box. If flows with the document's text.
  I.e. it will appear on the same line as surrounding text and other inline elements, and its
  content will break with the flow of text, like lines of text in a paragraph.

  Width and height settings have no effect on inline boxes; any padding, margin and border set on
  inline boxes will update the position of surrounding text, but will not affect the position of
  surrounding block boxes.

- __inline-block__: An inline-block box is something in between the first two. If flows with
  surrounding text and other inline elements without creating line breaks before and after it unlike
  a block box, but it can be sized using width and height and maintains its block integrity like
  a block box.

## Debugging

Just like HTML, CSS is _permissive_, i.e. the page still displays even if there are syntax errors.
In CSS's case, if a declaration is invalid (syntax error or not supported by browser), the browser
just ignores it completely and moves on to the next one it finds.

### Inspecting the DOM and CSS

Open the chrome developer console with `Ctrl + Shift + I`.

Navigate to the `Elements` section. This lets you traverse the DOM examining the CSS applied
to each element.

### CSS Validation

Relevant URLs:
[URL Validator](https://jigsaw.w3.org/css-validator/),
[File Validator](https://jigsaw.w3.org/css-validator/#validate_by_upload),
[Direct Input Validator](https://jigsaw.w3.org/css-validator/#validate_by_input)

## Styling Text

Text inside an element is laid out inside the element's content box. It starts at the top left of
the content area and flows towards the end of the line. Once it reaches the end, it goes down to the
next line and continues.

CSS properties used to style text fall into two categories:
- Font styles: properties that affect the text font
- Text layout styles: properties that affect the spacing and other layout features

### Fonts

- __color__: Sets the color of the foreground content of the selected elements
  ```css
  p {
    color: red;
  }
  ```

- __font families__: Allows you to specify a font (or list of fonts) for the browser to apply
  to the selected elements.
  ```css
  p {
    font-family: arial;
  }
  ```

  __Web safe fonts__ are generally available across all systems and can therefore be used without
  much worry. [cssfontstack.com](https://www.cssfontstack.com/) maintains a list of web safe fonts.
    * Arial: sans-serif
    * Courier New: monospace
    * Georgia: serif
    * Times New Roman: serif
    * Trebuchet MS: sans-serif
    * Veranda: sans-serif

  __Font stacks__ allow you to gracefully degrade fonts if the first (or nth) desired font isn't found
  on the system.
  ```css
  p {
    font-family: "Trebuchet MS", Verdana, sans-serif;
  }
  ```

  CSS defines five generic names for fonts: `serif`, `sans-serif`, `monospace`, `cursive`, and `fantasy`.
    * `serif`: Fonts that have serifs (the flourishes and other small details you see at the ends of the strokes in some typefaces) |
    * `sans-serif`: Fonts that don't have serifs.                                                                                        |
    * `monospace`: Fonts where every character has the same width, typically used in code listings.                                     |
    * `cursive`: Fonts that are intended to emulate handwriting, with flowing, connected strokes.                                     |
    * `fantasy`: Fonts that are intended to be decorative.                                                                            |

- __font size__: Sizes the font. Can take various units.
    * `px` (pixels): The number of pixels high you want the text to be
    * `ems`: 1em is equal to the font size set on the parent element of the current element
      we are styling
    * `rems`: Work like `ems` except that 1 rem is equal to the font size set on the root element
      of the document, not the parent element. Not supported in IE 8 and below.

  The `font-size` of an element is inherited from that element's parent element. This all starts
  with the root element of the entire document `<html>`.

  ```css
  html {
    font-size: 10px;
  }

  h1 {
    font-size: 2.6rem;
  }

  p {
    font-size: 1.4rem;
    color: red;
    font-family: Helvetica, Arial, sans-serif;
  }
  ```

- __font-style__: Used to turn italic text on and off
    * `normal`: sets the text to the normal font
    * `italic`: Sets the text to use the _italic version of the font_ if available
    * `oblique`: Sets the text to use a simulated version of an italic font, created by
      slanting the normal version

- __font-weight__: Sets how bold the text is.
    * `normal`, `bold`: Normal and __bold__ font weight
    * `lighter`, `bolder`: Set lighter or heavier
    * 100 - 900: Numeric boldness values that provide finer grained control

- __text-transform__: Set font to be transformed
    * `none`: Prevents transformation
    * `uppercase`
    * `lowercase`
    * `capitalize`: Transforms all words to have the First Letter Capitalized
    * `full-width`: Transforms all glyphs to be written inside a fixed-width square

- __text-decoration__: Sets/unsets text decorations on fonts
    * `none`: Unsets an text decorations
    * `underline`
    * `overline`
    * `line-through`

- __text drop shadows__: Apply drop shadows to your text using the `text-shadow` property.
  ```css
  text-shadow: 4px 4px 5px red;
  ```
  The four properties are as follows:
    * Horizontal offset
    * Vertical offset
    * Blur radius
    * Base color of shadow (defaults to black)

- __other font styles__
    * `font-variant`: Switch between small caps and normal font alternatives.
    * `font-kerning`: Switch font kerning options on and off.
    * `font-feature-settings`: Switch various OpenType font features on and off.
    * `font-variant-alternates`: Control the use of alternate glyphs for a given font-face.
    * `font-variant-caps`: Control the use of alternate capital glyphs.
    * `font-variant-east-asian`: Control the usage of alternate glyphs for East Asian scripts, like Japanese and Chinese.
    * `font-variant-ligatures`: Control which ligatures and contextual forms are used in text.
    * `font-variant-numeric`: Control the usage of alternate glyphs for numbers, fractions, and ordinal markers.
    * `font-variant-position`: Control the usage of alternate glyphs of smaller sizes positioned as superscript or subscript.
    * `font-size-adjust`: Adjust the visual size of the font independently of its actual font size.
    * `font-stretch`: Switch between possible alternative stretched versions of a given font.
    * `text-underline-position`: Specify the position of underlines set using the text-decoration-line property underline value.
    * `text-rendering`: Try to perform some text rendering optimization.

### Web Fonts

As discussed previously, fonts are applied via the `font-family` property. Unfortunately, there are
only a few web-safe fonts that are supported by all browsers.

To solve the problem of font availability, CSS allows you to specify font files to be downloaded
along with your website as it is accessed, meanign that any browser that supports web fonts can
have exactly the fonts you speicfy.

First specify the fonts to download:
```css
@font-face {
  font-family: "myFont";
  src: url("myFont.woff");
}
```

After downloading the font, you can use it:
```css
html {
  font-family: "myFont", "Bitstream Vera Serif", serif;
}
```

Browsers support different font formats. Most modern browsers support __WOFF__ (Web Open Font Format)
and __WOFF2__ (Web Open Font Format V2), the most efficient format around.

Relevant Font URLs:
- [Font Squirrel](https://www.fontsquirrel.com/): Free font distributor
- [Google Fonts](https://www.google.com/fonts): Free font CDN

### Text Layout

- __text alignment__: `text-align` property is used to control how text is aligned within its containing
  content box. The following values are allowed:
    * `left`
    * `right`
    * `center`
    * `justify`: Makes the text spread out, varying the gaps in between the words so that all lines
      of text are the same width. Be careful!

- __line height__: The `line-height` property sets the height of each line of text. Recommended line
  height is 1.5-2 (double spaced)
  ```css
  line-height: 1.5;
  ```

- __letter and word spacing__: `letter-spacing` and `word-spacing` properties allow you to set the
  spacing between letters and words in your text.
  ```css
  p::first-line {
    letter-spacing: 2px;
    word-spacing: 4px;
  }
  ```

- __other text layout styles__
    * `text-indent`: Specify how much horizontal space should be left before the beginning of the first line of the text content.
    * `text-overflow`: Define how overflowed content that is not displayed is signaled to users.
    * `white-space`: Define how whitespace and associated line breaks inside the element are handled.
    * `word-break`: Specify whether to break lines within words.
    * `direction`: Define the text direction (This depends on the language and usually it's better to let HTML handle that part as it is tied to the text content.)
    * `hyphens`: Switch on and off hyphenation for supported languages.
    * `line-break`: Relax or strengthen line breaking for Asian languages.
    * `text-align-last`: Define how the last line of a block or a line, right before a forced line break, is aligned.
    * `text-orientation`: Define the orientation of the text in a line.
    * `word-wrap`: Specify whether or not the browser may break lines within words in order to prevent overflow.
    * `writing-mode`: Define whether lines of text are laid out horizontally or vertically and the direction in which subsequent lines flow.

## Styling Links

The first thing to understand is the concept of link states - different states that links can exist
in, whcih can be styled using different pseudo-classes:
- __Link (unvisited)__: `:link`. The default state that a link resides in, whcih it isn't in any other
  state.
- __Visited__: `:visited` A link when it has already been visited (exists in the browser's history)
- __Hover__: `:hover` A link when it is being hovered by the user's mouse pointer
- __Focus__: `:focus` A link when it has been focused (for example moved to by a keyboard using
  the `Tab` key)
- __Active__: `active` A link when it is being activated (e.g. clicked on)

### Default Style

The default link is as follows:
```html
<p><a href="#">A simple link</a></p>
```
The default style consists of:
- Underlined
- Unvisited links are ble
- Visited links are purple
- Active links are red
- Hovering a link makes the mouse pointer change to a little hand icon

This styling hasn't changed much since the mid-1990s.

### Styled Link

An example styled link is below:
```html
<p>There are several browsers available, such as <a href="#">Mozilla Firefox</a>, <a href="#">Google Chrome</a>, and
<a href="#">Microsoft Edge</a>.</p>
```

```css
a {
  outline: none;
  text-decoration: none;
  padding: 2px 1px 0;
}

a:link {
  color: #265301;
}

a:visited {
  color: #437A16;
}

a:focus {
  border-bottom: 1px solid;
  background: #BAE498;
}

a:hover {
  border-bottom: 1px solid;
  background: #CDFEAA;
}

a:active {
  background: #265301;
  color: #CDFEAA;
}
```

### Add icon to link

A common practice is to include icons on links to provide more of an indicator as to what kind of
content the link points to.

```html
<p>For more information on the weather, visit our <a href="http://#">weather page</a>,
look at <a href="http://#">weather on Wikipedia</a>, or check
out <a href="http://#">weather on Extreme Science</a>.</p>
```

```css
a[href*="http"] {
  background: url('https://mdn.mozillademos.org/files/12982/external-link-52.png') no-repeat 100% 0;
  background-size: 16px 16px;
  padding-right: 19px;
}
```

The code above adds a photo after each http link.

### Style link as button

Links are commonly styled to look and behave like buttons in certain circumstances.

```html
<ul>
  <li><a href="#">Home</a></li>
  <li><a href="#">Pizza</a></li>
  <li><a href="#">Music</a></li>
  <li><a href="#">Wombats</a></li>
  <li><a href="#">Finland</a></li>
</ul>
```

```css
body,html {
  margin: 0;
  font-family: sans-serif;
}

ul {
  padding: 0;
  width: 100%;
}

li {
  display: inline;
}

a {
  outline: none;
  text-decoration: none;
  display: inline-block;
  width: 19.5%;
  margin-right: 0.625%;
  text-align: center;
  line-height: 3;
  color: black;
}

li:last-child a {
  margin-right: 0;
}

a:link, a:visited, a:focus {
  background: yellow;
}

a:hover {
  background: orange;
}

a:active {
  background: red;
  color: white;
}
```

## Styling Boxes

The box model is as follows:

![](https://mdn.mozillademos.org/files/13647/box-model-standard-small.png)

A couple points worth remembering:
- __Box heights__ don't observe percentage lengths; box height always adopts the height of the box content,
  unless a specific absolute height is set (e.g. pixels or ems). Borders ignore percentage width settings too
- __Margin collapsing__ behavior: sets the distance between two boxes to be the largest margin length
  not the sum of the margins
- __Overflow__: When you set the size of a box with absolute values, the content may not fit within the
  allowed size, in whcih case the content overflows the box. To control what happens in such a case,
  us the `overflow` property. Values:
    * `auto`: Overflowing content is hidden and scroll bars are shown
    * `hidden`: Overflowing content is hidden
    * `visible`: Overflowing content is shown outside the box (default behavior)
- __Background Clip__: `background-clip` property allows one to specify how far the background extends.
  To the content, padding, or border.
- __Outline__: `outline` of a box is something that looks like a border but which is not part of the
  box model.
- __Width and Height Contraints__: `min-width`, `max-width`, `min-height`, and `max-height`. Can set
  the maxiumum and minimum height and width to prevent the box from looking bad
- __Centering__: `margin: 0 auto;` Centers the container inside its parent

Boxes can have multiple `display` types:
- __block__: A `block` box is defined as a box that's stacked upon other boxes
- __inline__: An `inline` box is the opposite of a `block` box. It flows with the document's text
  i.e. it will appear on the same line as surrounding text and other inline elements, and its content
  will break with the flow of the text. Width and height settings have no effect on inline boxes.
- __inline-block__: An `inline-block` box is something between the first two. If flows with surrounding
  text without creating line breaks and after it linke an `inline` box, but it can be sized using width
  and height and maintains its block integrity like a `block` box -- it won't be broken across paragraph lines.
- __table__: Allows you to emulate table layous using non-table elements, without abusing table HTML.
- __flex__: Allows you to solve many classic layout problems that have plagued CSS for a long time, such as
  laying out a series of containers in flexible equal width columns, or vertically centered content.
- __grid__: Gives CSS a native way of easily implementing grid systems

### Background

The background of an element is the area that sits underneath the element's content, badding, and
border. The background doesn't sit underneath the margin - the margin doesn't count as part of the
element's area, but rather the area outside the element.

There are many properties you can use to manipulate the element's background:
- `background-color`. Default is `transparent`. It is a good idea to set the background color.
  ```html
  <p>Exciting box!</p>
  ```

  ```css
  p {
    font-family: sans-serif;
    padding: 20px;
    /* background properties */
    background-color: yellow;
  }
  ```

- `background-image`: Specifies a background image to appear in the background
  ```css
  p {
    font-family: sans-serif;
    padding: 20px;
    /* background properties */
    background-color: yellow;
    background-image: url(https://mdn.mozillademos.org/files/13026/fire-ball-icon.png);
  }
  ```
  One important thing to bear in mind is that since background images are set using CSS and appear
  in the background of content, they will be invisible to assistive technologies like screen readers.

- `background-repeat`: Specifies whether the background should be repeated. Supported values are:
    * `no-repeat`: Only show image once
    * `repeat-x`: Repeat horizontally in background
    * `repeat-y`: Repeat vertically all the way down
    * `repeat`: Repeat both vertically and horizontally

  ```css
  p {
    font-family: sans-serif;
    padding: 20px;
    /* background properties */
    background-color: yellow;
    background-image: url(https://mdn.mozillademos.org/files/13026/fire-ball-icon.png);
    background-repeat: no-repeat;
  }
  ```

- `background-position`: Specifies the position. Allows you to position your background image wherever
  you want inside the background. Generally the property will take two values separated by a space, which
  specify the horizontal(x) and vertical(y) coordinates of the image. The top left corner of the image
  is the origin (0,0). Many different values are supported:
    * Absolute values like pixels - `background-position: 200px 25px`
    * Relative values like rems - `background-position: 20rem 2.5rem`
    * Percentages - `background-position: 90% 25%`
    * Keywords - `background-position: right center`. Two values can be `left`, `center`, `right`, and `top`,
      `center`, `bottom` repectively

  ```css
  p {
    font-family: sans-serif;
    padding: 20px;
    /* background properties */
    background-color: yellow;
    background-image: url(https://mdn.mozillademos.org/files/13026/fire-ball-icon.png);
    background-repeat: no-repeat;
    background-position: 99% center;
  }
  ```

- `backgorund-image` gradient: allow you to apply color gradients across a background.

  `linear-gradient`: Apply a linear-gradient across background
   ```css
   background-image: linear-gradient(to bottom, orange, yellow);
   background-image: repeating-linear-gradient(to right, yellow, orange 25px, yellow 50px);
   ```

- `background-attachment`: Specifies the behavior of an element's background when its content scrolls.
  Can take the following values:
    * `scroll`: Causes the element's background to scroll when the page is scrolled
    * `fixed`: Causes an element's background to be fixed to the viewport, so that it doesn't scroll
      when the page or element is scrolled
    * `local`: Fixes the background to the element it is set on, so when you scroll the element, the
      background scrolls with it

- `background`: Shorthand. Declare multiple values in a single line
  ```css
  background: yellow linear-gradient(to bottom, orange, yellow) no-repeat left center scroll;
  ```

- Multiple backgrounds: Can attach multiple backgrounds to the same element. Each background is
  separated by a comma.
  ```css
  background: url(image.png) no-repeat 99% center,
              url(background-tile.png),
              linear-gradient(to bottom, yellow, #dddd00 50%, orange);
  background-color: yellow;
  ```

- `background-size`: Allows a background image to be dynamically resized.
  ```css
  background-image: url(myimage.png);
  background-size: 16px 16px;
  ```

### Border

By default the border has a size of 0 - making it invisible - but you can set the thickness, style,
and color of the border to make it appear.

- __border shorthand__: The `border` property allows you to set all of the border values at once.
  ```html
  <p>I have a red border!</p>
  ```

  ```css
  p {
    padding: 10px;
    background: yellow;
    border: 2px solid red;
  }
  ```

- `border-top`, `border-right`, `border-bottom`, `border-left`: Set the thickness, style, and color
  of one side of the border

- `border-width`, `border-style`, `border-color`: Set the thickness, style, or color individually

- `border-top-width`, `border-top-style`, `border-top-color`, ...

- `border-radius`: Set rounded corners on boxes
  ```css
  border-radius: 20px;
  ```

  ```css
  /* 1st value is top left and bottom right corners,
     2nd value is top right and bottom left  */
  border-radius: 20px 10px;
  /* 1st value is top left corner, 2nd value is top right
     and bottom left, 3rd value is bottom right  */
  border-radius: 20px 10px 50px;
  /* top left, top right, bottom right, bottom left */
  border-radius: 20px 10px 50px 0;
  ```

- `border-image`: Allows you display patterned images along border

### Box Shadows

`box-shadow` allows you to apply one or more drop shadows to an actual element box.

```html
<article class="simple">
  <p><strong>Warning</strong>: The thermostat on the cosmic transcender has reached a critical level.</p>
</article>
```

```css
article {
  max-width: 500px;
  padding: 10px;
  background-color: red;
  background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.25));
}

.simple {
  box-shadow: 5px 5px 5px rgba(0,0,0,0.7);
}
```

### Filters

CSS Filters provide a way to apply filters to an element in the same way as you might apply a filter
to a layer in a graphics package like Photoshop. A filter can be applied to any element, block or
inline. One just needs to set the filter property and provide a filter function

```html
<p class="filter">Filter</p>
<p class="box-shadow">Box shadow</p>
```

```css
p {
  margin: 1rem auto;
  padding: 20px;
  width: 100px;
  border: 5px dashed red;
}

.filter {
  -webkit-filter: drop-shadow(5px 5px 1px rgba(0,0,0,0.7));
  filter: drop-shadow(5px 5px 1px rgba(0,0,0,0.7));
}

.box-shadow {
  box-shadow: 5px 5px 1px rgba(0,0,0,0.7);
}
```

Filters are very new and only supported in most modern browsers/
`-webkit-` prefix is called a _Vendor Prefix_, and is used sometimes by a browser before it finalizes
its implementation of a new feature, to support that feature and experiment with it while not clashing
with the non-prefixed version. Vendor prefixes were never intended to be used by web developers, but they
do get used sometimes on production pages if experimental features are really desired.

## CSS Layout

CSS page layout techniques allow us to take elements contained in a web page and control where they
are positioned relative to their default position in normal layout flow, the other elements around
them, their parent container, or the main viewport/window.

Each technique has its uses, advantages, and disadvantages. No technique is designed to be used in
isolation.

The methods that can change the layout are as follows:

- `display` property: Standard values are `block`, `inline`, `inline-block`, `flex`, `grid`
- __Floats__: Applying a float value such as `left` can cause block level elements to wrap alongside
  one side of an element, like the way images sometimes have text floating around them in magazine
  layouts
- `position` property: Allows you to precisely contorl the placement of boxes inside other boxes
- __Table layout__: features designed for styling the parts of an HTML table can be used on non-table
  elements using `display:table`
- __Multi-column layout__: multi-column layout properties cause the content of a block to layout
  in columns, as you might see in a newspaper

### Normal Flow

Normal flow is how the browser lays out HTML pages by default. HTML is displayed in the exact order in
which it appears in the source code, with elements stacked up on top of one another — the first paragraph,
followed by the unordered list, followed by the second paragraph.

By default, a block level element's content is 100% of the width of its parent element, and as tall
as its content. Inline elements are as tall as their content, and as wide as their content.

The direction in which block element contents are laid out is described as the _Block direction_.
The Block Direction runs veritcally in a language such as English, which has a horizontal writing
mode.

When you use CSS to create a layout, you are moving the elements away from the normal flow, but for
many of the elements on your page the normal flow will create exactly the layout you need. This is
why starting with a well-structured HTML document is so important, as you can then work with are laid
out by defautl rather than fighting against it.

### Flexbox Layout

Flexbox is the short name for the _Flexible Box Layout module_, designed to make it easy for us to
lay things out in __one dimension__ - either as a row or as a column. To use flexbox, you apply `display:flex`
to the parent element you want to lay out; all of its direct children become flex items.

For a long time, the only reliable cross browser-compatible tools avaiable for creating CSS layouts were
things like floats and positioning. These are fine and they work, but in some ways they are also rather
limiting and frustrating. The following simple layout requirements are either difficult or impossible
to achieve with such tools:
- Vertically centering a block of content inside its parent
- Making all the children of a container take up an equal amount of the available width/height regardless
  of how much is available
- Making all columns in a multiple column layout adopt the same height even if they contain a different
  amount of content

When elements are laid out as flexible boxes, they are laid out along two axes:
![](https://developer.mozilla.org/files/3739/flex_terms.png)

- __main axis__: is the axis running in the direction the flex items are being laid out in
  (e.g as rows accross the page). The start and end of this acis are called __main start__
  and __main end__
- __cross axis__: is the axis running perpendicular to the direction the flex items are being laid
  out in. The start and end of this axis are called the __cross start__ and __cross end__.
- the parent element has `display:flex` set on it and is called the __flex container__

Relevant properties:
- `flex-direction`: Specifies what direction the main axis runs in. By default this is set to `row`
  which causes blocks to be rendered left to right. Values: `row`, `row-reverse`, `column`, `column-reverse`

  ```css
  flex-direction: column;
  ```

- `flex-wrap`: Have flex blocks wrap
  ```css
  flex-wrap: wrap;
  ```

### Grid Layout

A grid is simply a collection of horizontal and vertical lines creating a pattern against which we
can line up our design elements. They can help us toq create designs where elements don't jump around
or change width as we move from page to page, providing greater consistency on our websites.

A grid will typically have __columns__, __rows__, and then gaps between each row and column - commonly
referred to as __gutters__.

![](https://mdn.mozillademos.org/files/13899/grid.png)

To define a grid we use the `display: grid;`. This causes all children of the container to be grid
items:
```css
.container {
    display: grid;
}
```

To have flexible colum width use `fr` unit to equally distribute space:
```css
.container {
    display: grid;
    grid-template-columns: 2fr 1fr 1fr;
}
```

To create gaps inbetween columns use `grid-column-gap`:
```css
.container {
    display: grid;
    grid-template-columns: 2fr 1fr 1fr;
    grid-gap: 20px;
}
```

### Floats

Floating an element changes the behavior of that element and the block level elements that follow
it in normal flow. The element is moved to the left or right and removed from normal flow, and the
surrounding content floats around the floated item.

The `float` property has four possible values:

- `left`
- `right`
- `none`: Default value
- `inherit`: `float` property is inherited from the element's parent

### Positioning Techniques

Positioning allows you to move an element from where it would be placed in normal flow to another location.
Positioning isn't a method for creating your main page layouts, it is more about managing and fine-tuning
the position of specific items on the page.

There are several types of positioning:
- __Static Positioning__: Default that each element gets. Put the element in the normal position
  of the document layout flow.

- __Relative Positioning__: Allows you to modify an element's position on the page, moving it relative
  to its position in normal flow

- __Absolute Positioning__: Moves an element completely out of the page's normal layout flow, like it is
  sitting on its own separate layer. From there, you can fix it in a position relative to the edges of the
  page's `<html>` element or its nearest positied ancestor

- __Fixed Positioning__: Very similar to absolute positioning, except it fixes and element to the browser
  viewport, not another element. This is useful for creating effects such as persistent navigation menu
  that always stays in the same place on the screen as the rest of the content scrolls.

- __Sticky Positioning__: Is a newer positioning method which makes an element act like `position:static`
  until it hits a defined offset from the viewport, at which point it acts like `position:fixed`

### Table Layout

HTML tables are fine for displaying tabular data, but many years ago - before even basic CSS was supported
reliably across browsers - web developers used to also use tables for entire web page louts - putting their
headers, footers, different columns etc. in various table rows and columns.

This worked at the time, but it has many problems - table layouts are inflexible, very heavy on markup,
difficult to debug, and semantically wrong.

### Mult-column layout

The multi-column layout module gives us a way to lay out content in columns, similar to how text flows
in a newspaper. While reading up and down columns is less useful in a web context as you don't want to force
users to scroll up and down, arranging content into columns can be a useful technique.

The `column-count` property, which tells the browser how many columns we would like to have.

Alternatively, the `column-width` property specifies the width of each column.

## Responsive Design

### Viewport

The viewport is a user's visible area of a web page. The viewport varies with the device, and will be
smaller on a mobile phone than on a computer screen. Before tablets and mobile phones, web pages were
designed only for computer screens, and it was common for web pages to have a static design and a fixed size.
Then, when we started surfing the internet using tablets and mobile phones, fixed size web pages were too
large to fit the viewport. To fix this, browsers on those devices scaled down the entire web page to fit
the screen... this didn't work so well.

HTML5 introduced a method to let web designers take control over the viewport, through the `<meta>` tag.
A `<meta>` viewport gives the browser instructions on how to control the page's dimensions and scaling.

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

Some rules for creating good user experiences:

1. Users are used to scroll websites vertically on both desktop and mobile devices, but not
   horizontally. Forcing the user to scroll horizontally will lead to a bad user experience.

2. Do NOT use large fixed width elements

3. Do NOT let the content rely on a particular viewport width to render well

4. Use CSS media queries to apply different styling for small and large screens

### Media Queries

Media queryies are a CSS technique introduced in CSS3. They leverage the `@media` tag to include
a block of CSS for a specific media type.

```css
/* For mobile phones: */
[class*="col-"] {
  width: 100%;
}

@media only screen and (min-width: 600px) {
  /* For tablets: */
  .col-s-1 {width: 8.33%;}
  .col-s-2 {width: 16.66%;}
  .col-s-3 {width: 25%;}
  .col-s-4 {width: 33.33%;}
  .col-s-5 {width: 41.66%;}
  .col-s-6 {width: 50%;}
  .col-s-7 {width: 58.33%;}
  .col-s-8 {width: 66.66%;}
  .col-s-9 {width: 75%;}
  .col-s-10 {width: 83.33%;}
  .col-s-11 {width: 91.66%;}
  .col-s-12 {width: 100%;}
}

@media only screen and (min-width: 768px) {
  /* For desktop: */
  .col-1 {width: 8.33%;}
  .col-2 {width: 16.66%;}
  .col-3 {width: 25%;}
  .col-4 {width: 33.33%;}
  .col-5 {width: 41.66%;}
  .col-6 {width: 50%;}
  .col-7 {width: 58.33%;}
  .col-8 {width: 66.66%;}
  .col-9 {width: 75%;}
  .col-10 {width: 83.33%;}
  .col-11 {width: 91.66%;}
  .col-12 {width: 100%;}
}
```

There are tones of screens and devices with different heights and widths, so it is hard to create
an exact breakpoint for each device. To keep things simple you could target five groups:
```css
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {...}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {...}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {...}

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {...}

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {...}
```

Media queries can also be used to change layout of a page depending on the orientation of the browser.
```css
@media only screen and (orientation: landscape) {
  body {
    background-color: lightblue;
  }
}
```

Media queries can be used to hide elements on specific screens:
```css
/* If the screen size is 600px wide or less, hide the element */
@media only screen and (max-width: 600px) {
  div.example {
    display: none;
  }
}
```