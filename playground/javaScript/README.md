# JavaScript Technology Documentation

JavaScript is a high-level, dynamic, untyped, and interpreted programming language. Alongside HTML
and CSS, JavaScript is one of the three core technolgies of World Wide Web content production.

Relevant URLs:
[List of useful libraries](https://www.javascripting.com/),
[Frameworks](https://www.sitepoint.com/top-javascript-frameworks-libraries-tools-use/),
[3rd Party APIs](http://www.computersciencezone.org/50-most-useful-apis-for-developers/),
[Airbnb Style Guide](https://github.com/airbnb/javascript),
[Drupal Comment syntax](https://www.drupal.org/docs/develop/standards/javascript/javascript-api-documentation-and-comment-standards),
[Testing Overview](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2018-f68950900bc3)

## Directory Layout

The JavaScript directory is layed out as follows:
```bash
$ tree -L 1
.
├── browserCore       - JavaScript(In Browser) Core Documentation and Demos
├── browserThirdParty - JavaScript(In Browser) ThridParty Documentation and Demos
├── courses           - JavaScript course material
├── nodeCore          - NodeJS Standard Libary Documentation and Demos
├── nodeThirdParty    - NodeJS ThirdParty Documentation and Demos
└── README.md
```

## Packages

### Frameworks

- __VueJS__: Model View View Model (MVVM) Framework
- __AngularJS__: MVC-type framework. Two way data binding for models and views.
  Reusable view components. Services framework.
- __Backbone.js__: Simplest JS framework
- __Ember.js__: Opinionated web app framework focused on programmer productivity
- <__Knockout__></__Knockout__>

### UI Libraries

- __jQuery__: Cross-Browswer support, DOM traversal, event handling, animation, AJAX
- __React__: Library for building user interfaces
- __D3.js__: Data visualization and charting
- __Babylon.js__: Video games
- __Video.js__: Universal video embed

### Package Management

- __NPM__ : npm is a package manager for the JavaScript programming language.
 It is the default package manager for the JavaScript runtime environment Node.js.
- __Yarn__: Alternative JavaScript package manager

### Build Systems, Task Runners

- __Webpack__ : Webpack is an open-source JavaScript module bundler. Its main purpose
 is to bundle JavaScript files for usage in a browser, yet it is also
 capable of transforming, bundling, or packaging just about any
 resource or asset
- __Grunt__: Grunt is a JavaScript task runner, a tool used to automatically perform
 frequent tasks such as minification, compilation, unit testing, and
 linting. It uses a command-line interface to run custom tasks defined in a
 file (known as a Gruntfile).
- __Gulp__: Task runner built on Node.js and npm, used for automation of
 time-consuming and repetitive tasks involved in web development
 like minification, concatenation, cache busting, unit testing,
 linting, optimization, etc
- __Brunch__: Alternative bundler

### Misc

- __Moment__                      : Parse, validate, manipulate and display dates
- __Underscore (Iodash)__         : Utility functions
- __UglifyJS__                    : parser, minifier, compressor or beautifier toolkit.

## 3rd Party APIs

### Ads

- __Google Ad Exchange Seller__
- __Google Ad Exchange Buyer__
- __Google AdSense__
- __Facebook Ads__

### Social media

- __Facebook__
- __Google+__
- __LinkedIn__
- __Twitter__
- __Pinterest__
- __Instagram__

### Maps

- __Google maps__

### Analytics, Artificial Inteligence

- __IBM Watson__                   : Text-to-speech, trade-off, personality, question and answer
- __Google Prediction__            : Sentiment analysis, spam detection, document classification, purchase prediction
- __AlchemyAPI__                   : (IBM)sentiment analysis, entity extraction, concept tagging, image tagging, and facial detection/recognition

### Infrastructure

- __AmazonS3__                     : Storage, developer infrastructure
- __Dropbox__

### Misc

- __Stripe.js__                    : Payment API
- __reCaptcha__                    : Ensures user is human
- __Google fonts__                 : Use a variety of fonts in browser

## Modules

JavaScript modules are small units of independent, reusable code. Various approaches have been
taken to create JavaScript Modules

### Server Side Modules

- __CommonJS__ (formerly ServerJS) is designed for server-side apps. It defines modules with exports
and require() syntax
  ```javascript
  // addModule.js
  exports.add = function(a, b) { return a + b; };

  // index.js
  var am = require('addModule'); // in the vein of node
  var c  = am.add(2, 3);
  ```

- __Node.js__ is an implementation of CommonJS Specification.

- __Browserify__ is a  CommonJS Module implementation that can run in the browser.

### Client Side Modules

- __AMD__ (Asynchronous Module Definition) Supports async loading of module deps. It is more suited
towards browsers.

  ```javascript
  define('module/id/string', ['module', 'dependency', 'array'],
  function(module, factory function) {
    return ModuleContents;
  });
  ```

- __RequireJS__ is an implementation of AMD Specification. It offers a CommonJS wrapper so CommonJS
modules can be directly imported to use with RequireJS.
  ```javascript
  define(function(require, exports, module) {
    var someModule = require('someModule'); // in the vein of node
    exports.doSomethingElse = function() { return someModule.doSomething() + "bar"; };
  });
  ```

### Module Standardization

- __ECMAScript 2015 (ES6)__
  ```javascript
  //------ lib.js ------
  export function square(x) {
      return x * x;
  }
  //------ main.js ------
  import { square, diag } from 'lib';
  console.log(square(11)); // 121
  ```

- Draft Spec for async loading
  ```javascript
  System.import('some_module')
        .then(some_module => {
            // Use some_module
        })
        .catch(error => {
            // ...
        });
  ```

- A __polyfill__ is a piece of code (usually JavaScript on the Web) used to provide modern
functionality on older browsers that do not natively support it.

- __Babel__ is a toolchain that is mainly used to convertES2015+ code into a backwards compatible version
of JavaScript in current and older browsers or environments.

### All In One Solutions

- __UMD__ (Universal Module Definition) is a mechanism allowing modules to be exported as AMD or CommonJS.
  ```javascript
  (function (root, factory) {
      if (typeof define === 'function' && define.amd) {
          // AMD
          define(['jquery', 'underscore'], factory);
      } else if (typeof exports === 'object') {
          // Node, CommonJS-like
          module.exports = factory(require('jquery'), require('underscore'));
      } else {
          // Browser globals (root is window)
          root.returnExports = factory(root.jQuery, root._);
      }
  }(this, function ($, _) {
      //    methods
      function a(){};    //    private because it's not returned (see below)
      function b(){};    //    public because it's returned
      function c(){};    //    public because it's returned
      //    exposed public methods
      return {
          b: b,
          c: c
      }
  }));
  ```

- __System.js__ is a universal module loader that supports CommonJS, AMD, and ES2015. It can work in
tandem with transpilers such as Babel or Traceur and can support Node and IE8+ environments.
  ```html
  <script src="system.js"></script>
  <script>
    // set our baseURL reference path
    System.config({
      baseURL: '/app',
      // or 'traceur' or 'typescript'
      transpiler: 'babel',
      // or traceurOptions or typescriptOptions
      babelOptions: {
      }
    });
    // loads /app/main.js
    System.import('main.js');
  </script>
  ```

## NodeJS

Node.js is an open-source, cross-platform JavaScript run-time environment that
executes JavaScript code outside of a browser. Node.js lets developers use JavaScript to write
Command Line tools and for server-side scripting—running scripts server-side to produce dynamic
web page content before the page is sent to the user's web browser. Consequently, Node.js represents
a "JavaScript everywhere" paradigm, unifying web application development around a single programming
language, rather than different languages for server side and client side scripts.

Node.js runs on various platforms (Windows, Linux, Unix, Mac OS X, etc.). Node.js runs single-threaded,
non-blocking, asynchronously programming, which is very memory efficient.

Relevant URLS:
[Home](https://nodejs.org/en/),
[Documentation](https://nodejs.org/en/docs/),
[Wikipedia](https://en.wikipedia.org/wiki/Node.js),
[W3 Schools](https://www.w3schools.com/nodejs/default.asp)

### Installation

Via nodejs.sh:

```bash
# Install nodejs
nodejs_install

# Install npm
npm_install
```

### Modules

Modules are the same as JavaScript libraries. They are a set of functions to include in application.

To include a module use `require()`:
```javascript
var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello World!');
}).listen(8080);
```

To create a module:
```javascript
// myModule.js
// Add to exports keyword to export functionality
exports.myDateTime = function () {
  return Date();
};
```

To include your module:
```javascript
var dt = require('./myModule.js')
console.log(dt.myDateTime());
```

### Built in Modules

- __assert__: Provides a set of assertion tests
- __buffer__: To handle binary data
- __child_process__: To run a child process
- __cluster__: To split a single Node process into multiple processes
- __crypto__: To handle OpenSSL cryptographic functions
- __dgram__: Provides implementation of UDP datagram sockets
- __dns__: To do DNS lookups and name resolution functions
- __domain__: Deprecated. To handle unhandled errors
- __events__: To handle events
- __fs__: To handle the file system
- __http__: To make Node.js act as an HTTP server
- __https__: To make Node.js act as an HTTPS server.
- __net__: To create servers and clients
- __os__: Provides information about the operation system
- __path__: To handle file paths
- __punycode__: Deprecated. A character encoding scheme
- __querystring__: To handle URL query strings
- __readline__: To handle readable streams one line at the time
- __stream__: To handle streaming data
- __string_decoder__: To decode buffer objects into strings
- __timers__: To execute a function after a given number of milliseconds
- __tls__: To implement TLS and SSL protocols
- __tty__: Provides classes used by a text terminal
- __url__: To parse URL strings
- __util__: To access utility functions
- __v8__: To access information about V8 (the JavaScript engine)
- __vm__: To compile JavaScript code in a virtual machine
- __zlib__: To compress or decompress files

## Security

__JavaScript Syntax eXtension__ (JSX) is a JavaScript extension syntax allowing quoting of HTML and using
HTML tag syntax to render subcomponent. Used by React

__Cross-site scripting (XSS)__ is a web vulneralbility whereby attacker injects client side scripts
into web pages viewed by other user. Don't allow any malicious JavaScript code to be loaded
into site.

__Cross-site request forgery (CSRF)__ is an attack that tricks the victim into submitting a malicious
request. It inherits the identity and privileges of the victim to perform an undesired function on
the victim's behalf. For most sites, browser requests automatically include any credentials associated
with the site, such as the user's session cookie. Therefore, if the user is currently authenticated to
the site, the site will have no way to distinguish between the forged request sent by the victim and a
legitimate request sent by the victim. For example a rouge email sent with an image
`<img href="http://bankofamerica.com?transferTo=12312039812@100"/>` When image is clicked, request
will be sent to bank of america with the user's logged-in credentials.
Solution: use local storage to store auth tokens. Send them in request headers

__Content Security Policy (CSP)__ is an added layer of security that helps to detect and mitigate certain
types of attacks, including Cross Site Scripting (XSS) and data injection attacks. These attacks are
used for everything from data theft to site defacement to distribution of malware. It prohibits new
Function() for evaluating expressions

## Testing

### Types

- Unit: Test individual functions or classes by supplying input and making sure output is as expected
- Integration: Testing processes or components to behave as expected
- User Interface (UI: Also known as Functional Tests. Test scenarios on the product itself

### Tools

- __jsdom__                       : Simulates WHATWG DOM and HTML standard
- __Istanbul__                    : Code Coverage reports
- __NYC__                         : Istanbul command line interface
- __Karma__                       : Lets tests run in browser and browser like environments
- __Chai__                        : Assertion Library
- __Unexpected__                  : Assertion Library
- __Sinon.JS__                    : Standalone test spies, stubs, and mocks
- __testdouble.js__               : Simiar to sinon
- __Wallaby__                     : Paid test runner
- __Cucumber__                    : Test runner for variety of languages

### Frameworks

- __Jest__                        : Fast test framework for large projects, based on Jasmine
- __Mocha__                       : Flexible and extendable test framework
- __Ava__                         : Simple test framework
- __tape__                        : Low level test framework
- __Jasmine__                     : Test framework around for the longest time

### UI Testing

- __Selenium__                    : UI testing tool
- __Appium__                      : Similar API to Selenium but for websites on mobile devices
- __Protractor__                  : Selenium wrapper that adds special hooks for angular
- __WebdriverIO__                 : Own implementation of Selenium WebDriver
- __Nightwatch__                  : Easiest and most readable syntax
- __TestCafe__                    : Alternative to Selenium-Based tools. Fast, cross browser, parallel testing, own ecosystem
- __Cypress__                     : Direct competitor to testcafe; more modern, flexible, and convenient
- __Puppeteer__                   : Node.js library developed by Google to control Headless Chrome
- __PhantomJS__                   : Implements the chromium engine to create a controllable browser
- __Nightmare__                   : Very simple test syntax
- __Casper__                      : Written on top of PhantomJS and SlimerJS
- __CodeceptJS__                  : Abstraction over different libraries' API's

### Structure

- __Behavior-Driven-Development (BDD)__ is a common way to structure tests.
  ```javascript
  describe('calculator', function() {
    // describes a module with nested "describe" functions
    describe('add', function() {
      // specify the expected behavior
      it('should add 2 numbers', function() {
         //Use assertion functions to test the expected behavior
         ...
      })
    })
  })
  ```
- __Assertion__ functions ensure tested variables contain expected values
  ```javascript
  // Chai expect (popular)
  expect(foo).to.be.a('string')
  expect(foo).to.equal('bar')
  ```
- __Spies__ provide information about functions how many times called?, by whom?
  ```javascript
  it('should call method once with the argument 3', () => {
    // create a sinon spy to spy on object.method
    const spy = sinon.spy(object, 'method')
    // call the method with the argument "3"
    object.method(3)
    // make sure the object.method was called once, with the right arguments
    assert(spy.withArgs(3).calledOnce)
  })
  ```
- __Stubbing__ or __dubbing__ replaces selected functions with other functions
  ```javascript
  // Sinon: make user.isValid() return true
  sinon.stub(user, 'isValid').returns(true)
  ```
- __Mocks__ or __Fakes__ fake certain module behaviors to test parts of process. For example, GET
  on `/users` can return fake data
- Various browser environments are used
    * jsdom                        = JavaScript environment that simpulates a real browser
    * Headless Browser Environment = Browser that runs without a UI
    * Real Browser Environment     = Actual browser that opens and runs your tests

## ES6 Additions

### Keywords

- `const`                         : Define constants with block scope. `const z = 3`
- `let`                           : Declare a block scoped variable (no hoisting) `let a = 4`

### Array Helper Functions

- `forEach`                       : Execute function for each element of array
  ```javascript
  var colors = ["red", "blue"]
  colors.forEach(function(color) {
    console.log(color);
  })
  ```
- `map`: Create a new array containing the same number of elements, but output elements are created
  by provided function
- `filter`: Creates new array containing a subset of the original
- `find`: Finds the first element that passes the test implemented by the provided function
- `every`: Checks if every element of array passes test implemented by provided function
- `some`: Checks if some elements of array pass test implemented by the provided function
- `reduce`: Applies function passed as the first parameter against an accumulater and each element
  in the array. Thus, reduces the array to a single value.

### Arrow Functions

```javascript
let array = [1, 2, 3, 4]
let sumOfArray = array.reduce((acc, value) => acc + value, 0)
```

### Classes

```javascript
class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y
  }
  toString() {
    return `x=${this.x} y=${this.y}`
  }
}
```

### Template Strings

```javascript
let bar = 1
let foo = `bar=${bar}`
```

### Default Function parameters

```javascript
function sort(arr = [], direction = 'ascending') {
  // Implement sort here
}
```

### Spread Operator

Enable extraction of array or object content as single elements

```javascript
let array = [1, 2, 3]
let arrayCopy = [...array]
```

### Rest Operator

Puts multiple single variables in an array

```javascript
function printColors(first, second, ...others) {
  // Access first, second, others
}
```

### Destructuring

Enables extraction of requested elements from an array or object
```javascript
function printColorsFromArray([, second, , fourth]) {
  // print second and fourth
}
function printInfoFromObject({firstName, lastName, profession}) {
  // print firstName, lastName
}
```

### Promises

Ensure that you get in the future results of deferred or long-running tasks

```javascript
func.then()  // handles success
    .catch(); // handles error
```

