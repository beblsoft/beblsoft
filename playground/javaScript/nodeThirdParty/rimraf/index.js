/**
 * @file Rimraf Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let rimraf = require('rimraf');
let fs = require('fs');
let path = require('path');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Rimraf example
 */
async function rimrafFunc() {
  let filePath = path.join(__dirname, 'a');

  await fs.mkdirSync(filePath);
  console.log(`${filePath} created`);
  rimraf.sync(filePath);
  console.log(`${filePath} deleted`);
}


/* ------------------------ MAIN ------------------------------------------- */
rimrafFunc();
