# Rimraf Samples Documentation

## index.js

Demonstrate creating a folder and removing it with rimraf.

To run:
- Install: `npm install`
- Run: `node index.js`