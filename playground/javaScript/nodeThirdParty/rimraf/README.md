# Rimraf Technology Documentation

Rimraf is the Unix command `rm -rf` for node.js.

Relevant URLs:
[NPM](https://www.npmjs.com/package/rimraf),
[GitHub](https://github.com/isaacs/rimraf)

## Installation

Install with npm: `npm install rimraf`

## API

### Functions

```javascript
// Async
rimraf('path/to/file', [opts], callback);

// Sync
rimraf.sync('path/to/file', [opts]);
```

