# UUID Samples Documentation

## index.js

Demonstrate creating v1, v3, v4, and v5 uuids.

To run:
- Install: `npm install`
- Run: `node index.js`