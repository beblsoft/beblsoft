# UUID Technology Documentation

UUID enables simple, fast generation of [RFC4122](https://www.ietf.org/rfc/rfc4122.txt) UUIDs.

Features:
- Support for Version 1, 3, 4, and 5 UUIDs
- Cross-platform
- Cryptographically-strong random number APIs
- Zero-dependency, small footprint

## Installation

Install with npm: `npm install uuid`

## Suported Versions

### Version 1: Timestamp

```javascript
const uuidv1 = require('uuid/v1');
uuidv1(); // ⇨ '45745c60-7b1a-11e8-9c9c-2d42b21b1a3e'
```

### Version 3: Namespace

```javascript
const MY_NAMESPACE = '1b671a64-40d5-491e-99b0-da01ff1f3341';
uuidv3('hello.example.com', uuidv3.DNS);        // ⇨ '9125a8dc-52ee-365b-a5aa-81b0b3681cf6'
uuidv3('http://example.com/hello', uuidv3.URL); // ⇨ 'c6235813-3ba4-3801-ae84-e0a6ebb7d138'
uuidv3('Hello, World!', MY_NAMESPACE);          // ⇨ 'e8b5a51d-11c8-3310-a6ab-367563f20686'
```

### Version 4: Random

```javascript
const uuidv4 = require('uuid/v4');
uuidv4(); // ⇨ '10ba038e-48da-487b-96e8-8d3b99b6d18a'
```

### Version 5: Namespace

```javascript
const MY_NAMESPACE = '1b671a64-40d5-491e-99b0-da01ff1f3341';
const uuidv5 = require('uuid/v5');
uuidv5('hello.example.com', uuidv5.DNS);        // ⇨ 'fdda765f-fc57-5604-a269-52a7df8164ec'
uuidv5('http://example.com/hello', uuidv5.URL); // ⇨ '3bbcee75-cecc-5b56-8031-b6641c1ed1f1'
uuidv5('Hello, World!', MY_NAMESPACE);          // ⇨ '630eb68f-e0fa-5ecc-887a-7c7a62614681'

```

## Command Line

UUIDs can be generated from the command line with the `uuid` command.

```bash
$ npx uuid --help
Usage:
  uuid
  uuid v1
  uuid v3 <name> <namespace uuid>
  uuid v4
  uuid v5 <name> <namespace uuid>
  uuid --help

$ npx uuid
ddeb27fb-d9a0-4624-be4d-4615062daed4

$ npx uuid v1
02d37060-d446-11e7-a9fa-7bdae751ebe1
```

