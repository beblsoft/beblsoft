/**
 * @file UUID Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let uuidv1 = require('uuid/v1');
let uuidv3 = require('uuid/v3');
let uuidv4 = require('uuid/v4');
let uuidv5 = require('uuid/v5');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * UUID examples
 */
function uuid({
  v1 = false,
  v3 = false,
  v4 = false,
  v5 = false,
  all = false
} = {}) {
  if (v1 || all) {
    logTitle({ title: 'v1' });
    let options = {
      node: [0x01, 0x23, 0x45, 0x67, 0x89, 0xab],
      clockseq: 0x1234,
      msecs: new Date().getTime(),
      nsecs: 5678
    };
    console.log(`${uuidv1(options)}`);
  }
  if (v3 || all) {
    logTitle({ title: 'v3' });
    let MY_NAMESPACE = '1b671a64-40d5-491e-99b0-da01ff1f3341';
    console.log(`${uuidv3('hello.example.com', uuidv3.DNS)}`);
    console.log(`${uuidv3('http://example.com/hello', uuidv3.URL)}`);
    console.log(`${uuidv3('HelloWorld', MY_NAMESPACE)}`);
  }
  if (v4 || all) {
    logTitle({ title: 'v4' });
    let options = {
      random: [
        0x10, 0x91, 0x56, 0xbe, 0xc4, 0xfb, 0xc1, 0xea,
        0x71, 0xb4, 0xef, 0xe1, 0x67, 0x1c, 0x58, 0x36
      ]
    };
    console.log(`${uuidv4()}`);
    console.log(`${uuidv4(options)}`);
  }
  if (v5 || all) {
    logTitle({ title: 'v5' });
    let MY_NAMESPACE = '1b671a64-40d5-491e-99b0-da01ff1f3341';
    console.log(`${uuidv5('hello.example.com', uuidv5.DNS)}`);
    console.log(`${uuidv5('http://example.com/hello', uuidv5.URL)}`);
    console.log(`${uuidv5('HelloWorld', MY_NAMESPACE)}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
uuid({ all: true });
