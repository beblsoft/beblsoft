/**
 * @file Axios Error Handling Example
 */

/* ------------------------ MAIN ------------------------------------------- */
let axios = require('axios');


/* ------------------------------------------------------------------------- */
/**
 * @class APIError
 * @extends {Error}
 */
class APIError extends Error {
  /**
   * @constructor
   * @param  {String} message - Error message
   * @param  {Number} code - Smeckn error code
   * @param  {Number} httpStatus - http error status Ex. 500
   */
  constructor({
    message = '',
    httpStatus = 400,
    code = -1,
  }) {
    super(message);
    this.code = code;
    this.httpStatus = httpStatus;
  }
}


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Function to wrap axios call and handle errors
 * @param {string} options.method
 * @param {string} options.url
 * @param {number} options.timeoutMS
 * @param {object} options.data
 * @return {Promise}
 */
function axiosWrapper({
  method = 'GET',
  url = 'https://jsonplaceholder.typicode.com/todos/1',
  timeoutMS = 10000,
  data = {},
}) {
  return new Promise((resolve, reject) => {
    let axiosConfig = {
      method: method,
      url: url,
      timeout: timeoutMS,
      data: data,
    };

    // Send Axios Request and process response
    axios.request(axiosConfig)
      .then((resp) => {
        console.log(resp.data);
        resolve(resp.data);
      })
      .catch((err) => {
        let apiError = null;
        let message = 'Server Error';
        let httpStatus = 400;
        let code = -1;

        // 3 Cases
        if (err.response) {
          // 1: Request made, but server responded with status code outside of 2xx range
          // console.log(err.response.data);
          // console.log(err.response.status);
          // console.log(err.response.headers);
        } else if (err.request) {
          // 2: The request was made but no response was received
          // console.log(err.request);
        } else {
          // 3: Something happened in setting up the request that went wrong
          // console.log(err.message);
        }
        console.log(err.code);
        console.log(err.message);
        apiError = new APIError({
          message: message,
          httpStatus: httpStatus,
          code: code
        });
        reject(apiError);
      });
  });
}


/* ------------------------ MAIN ------------------------------------------- */
axiosWrapper({ timeoutMS: 1 })
  .then((_) => { console.log('Success'); })
  .catch((_) => { console.log('Error'); });
