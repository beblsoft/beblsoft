/**
 * @file Axios Hello World Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let axios = require('axios');


/* ------------------------ MAIN ------------------------------------------- */
let axiosConfig = {
  method: 'GET',
  url: 'https://jsonplaceholder.typicode.com/todos/1',
  timeout: 10000,
  data: {}
};
axios.request(axiosConfig)
  .then((resp) => { console.log(resp.data); })
  .catch((err) => { console.log(err); });
