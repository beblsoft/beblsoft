# Axios Technology Documentation

__Axios__ is a promise based http client for the browser and node.js.

Relevant URLs:
  [NPM](https://www.npmjs.com/package/axios),
  [Github](https://github.com/axios/axios)
