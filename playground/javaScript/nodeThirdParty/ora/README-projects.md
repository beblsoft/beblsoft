# UUID Samples Documentation

## index.js

Demonstrate ora spinner features including among others:
- starting spinner
- changing spinner text
- changing spinner color
- succeding spinner
- failing spinner

To run:
- Install: `npm install`
- Run: `node index.js`