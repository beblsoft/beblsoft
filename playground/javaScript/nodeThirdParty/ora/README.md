# ORA Technology Documentation

ora is an elegant terminal spinner.

Relevant URLs:
[NPM](https://www.npmjs.com/package/ora),
[GitHub](https://github.com/sindresorhus/ora),
[Available Spinners](https://github.com/sindresorhus/cli-spinners/blob/master/spinners.json)

## Installation

Install with npm: `npm install ora`

## Usage

```javascript
const ora = require('ora');
const spinner = ora('Loading unicorns').start();

setTimeout(() => {
    spinner.color = 'yellow';
    spinner.text = 'Loading rainbows';
}, 1000);
```

