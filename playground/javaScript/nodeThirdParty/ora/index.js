/**
 * @file ORA Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let ora = require('ora');


/* ------------------------ GLOBALS ---------------------------------------- */
let delay = 2000;
let spinner = ora({
  text: 'Cyan!',
  prefixText: 'Prefix',
  // All spinners her
  // https://github.com/sindresorhus/cli-spinners/blob/master/spinners.json
  spinner: 'dots12',
  // Define own spinner
  // spinner: {
  //   interval: 80, // Optional
  //   frames: ['-', '+', '-']
  // }
  color: 'cyan',
  hideCursor: true,
  indent: 0,
  interval: 50
});


/* ------------------------ MAIN ------------------------------------------- */
spinner.start();
console.log(`\nIs spinning: ${spinner.isSpinning}\n`);

setTimeout(() => {
  spinner.color = 'red';
  spinner.text = 'Red!';
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.spinner = 'dots2';
  spinner.text = 'Dots2!';
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.succeed('Success!');
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.fail('Fail!');
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.warn('Warn!');
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.info('Info!');
}, delay);
delay += 1000;

setTimeout(() => {
  spinner.stop();
  spinner.clear();
}, delay);
delay += 1000;
