/**
 * @file Option Parsing Example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let program = require('commander');


/* ------------------------ PARSER ----------------------------------------- */
/*
 Option parsing:
   - .option() parsese options from process.argv. Unused put in program.args
   - Short flags may be passed as a single arg "-abc" = "-a -b -c"
   - Multi word options become camel cased "--template-engine" => program.templateEngine
   - Multi word options starting with "no" prefix negate the boolean value of the following word
     I.e "--no-sauce" sets false in program.sauce
*/
program
  .version('1.0.0')
  .option('-s, --size <size>', 'Pizza size', /^(large|medium|small)$/i, 'medium')
  .option('-n, --no-sauce', 'Remove sauce from pizza')
  .option('-p, --peppers', 'Add peppers')
  .option('-P, --pineapple', 'Add pineapple')
  .option('-d --drink [drink]', 'Drink', /^(coke|pepsi|izze)$/i)
  .option('-c, --cheese [type]', 'Add the specified type of cheese [marble]', 'marble')
  .parse(process.argv);


/* ------------------------ MAIN ------------------------------------------- */
console.log('Pizza Order');
console.log(`  - ${program.size} size`);
if (program.sauce) {
  console.log('  - sauce');
}
if (program.peppers) {
  console.log('  - peppers');
}
if (program.pineapple) {
  console.log('  - pineapple');
}
console.log('  - %s cheese', program.cheese);
if (program.drink) {
  console.log(`  - ${program.drink} drink`);
}
