/**
 * @file Hello World Example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let program = require('commander');


/* ------------------------ BASIC PARSER ----------------------------------- */
program
  .version('0.1.0')
  .parse(process.argv);
