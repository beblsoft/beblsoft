# Commander Projects Documentation

## helloWorld.js

Hello World Command Program.

To Run:
- Install Software : `npm install`
- Run program      : `node ./helloWorld.js -h`

## optionParsing.js

Demonstrate parsing options

To Run:
- Install Software : `npm install`
- Run program      : `node ./optionParsing.js -h`
- Get version      : `node ./optionParsing.js -V`
- Order pizza      : `node ./optionParsing.js`

## command.js

Invoke an action function based on the command passed in

To Run:
- Install Software : `npm install`
- Run program      : `node ./command.js rm foo`

## multiCommand.js

Program with multiple commands

To Run:
- Install Software                         : `npm install`
- Setup all environments with normal mode  : `node ./multiCommand.js setup -s normal`
- Execute command foo in exec mode normal  : `node ./multiCommand.js exec foo -e normal`
- Run program                              : `node ./multiCommand.js * normal`
