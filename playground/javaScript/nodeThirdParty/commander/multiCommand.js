/**
 * @file Multi Command Example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let program = require('commander');


/* ------------------------ PARSER ----------------------------------------- */
program
  .version('0.1.0')
  .option('-C, --chdir <path>', 'change the working directory')
  .option('-c, --config <path>', 'set config path. defaults to ./deploy.conf')
  .option('-T, --no-tests', 'ignore test hook');

/* ------------------------ SETUP ------------------------------------------ */
program
  .command('setup [env]')
  .description('run setup commands for all envs')
  .option('-s, --setup_mode [mode]', 'Which setup mode to use')
  .action(function(env = 'all', options) {
    let mode = options.setup_mode || 'normal';
    console.log('setup for %s env(s) with %s mode', env, mode);
  });

/* ------------------------ EXEC ------------------------------------------- */
program
  .command('exec <cmd>')
  .alias('ex')
  .description('execute the given remote cmd')
  .option('-e, --exec_mode <mode>', 'Which exec mode to use')
  .action(function(cmd, options) {
    console.log(`exec ${cmd} using ${options.exec_mode} mode`);
  }).on('--help', function() {
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ deploy exec sequential');
    console.log('  $ deploy exec async');
  });

/* ------------------------ MAIN ------------------------------------------- */
program
  .command('* [env]')
  .action(function(env = 'all') {
    console.log(`Deploying ${env}`);
  });

program.parse(process.argv);
