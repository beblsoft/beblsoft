# Babel Documentation

Babel is a JavaScript compiler allowing next generation JavaScript to be used in old browsers.

Relevant URLs:
[Home](https://babeljs.io/),
[Docs](https://babeljs.io/docs/en/),
[GitHub](https://github.com/babel/babel)

## What is Babel

### JavaScript Compiler

Babel is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible
version of JavaScript in current and older browsers or environments. Here are the main things Babel
can do for you:

- Transform syntax
- Polyfill features that are missing in your target environment (through `@babel/polyfill`)
- Source code transformations (codemods)

For example:
```js
// Babel Input: ES2015 arrow function
[1, 2, 3].map((n) => n + 1);

// Babel Output: ES5 equivalent
[1, 2, 3].map(function(n) {
  return n + 1;
});
```

### Pluggable

Babel is built out of plugins. Compose your own transformation pipeline using existing plugins or write
your own. Easily use a set of plugins by using or creating a preset.

```js
// A plugin is just a function
export default function ({types: t}) {
  return {
    visitor: {
      Identifier(path) {
        let name = path.node.name; // reverse the name: JavaScript -> tpircSavaJ
        path.node.name = name.split('').reverse().join('');
      }
    }
  };
}
```

### Debuggable

Source map so you can debug your compiled code with ease.

### Compact

Babel tries using the least amount of code possible with no dependence on a bulky runtime. This may
be difficult to do in cases, and there are "loose" options for specific transformations that may
tradeoff spec compliancy for readability, file size, and speed.


