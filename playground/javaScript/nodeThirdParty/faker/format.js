/**
 * @file Faker Format Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let faker = require('faker');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Format examples
 */
function format() {
  console.log(faker.fake('{{name.lastName}}, {{name.firstName}} {{name.suffix}}'));
}


/* ------------------------ MAIN ------------------------------------------- */
format();
