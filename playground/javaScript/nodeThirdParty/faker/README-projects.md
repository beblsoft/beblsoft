# Faker.js Projects Documentation

## api.js

Demo all of the Faker api methods.

Run:
- Install: `npm install`
- Run: `node api.js`

## format.js

Demonstrate creating a format string with fake data.

Run:
- Install: `npm install`
- Run: `node format.js`

## seed.js

Demonstrate using Faker with a seed.

Run:
- Install: `npm install`
- Run: `node seed.js`