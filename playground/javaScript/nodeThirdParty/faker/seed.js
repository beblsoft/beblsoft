/**
 * @file Faker Seed Example
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let faker = require('faker');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Seed example
 */
function seed() {
  faker.seed(123);
  let firstRandom = faker.random.number();
  console.log(firstRandom);

  // Setting the seed again resets the sequence.
  faker.seed(123);
  let secondRandom = faker.random.number();
  console.log(secondRandom);
}


/* ------------------------ MAIN ------------------------------------------- */
seed();
