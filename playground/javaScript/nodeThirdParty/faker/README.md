# Faker.js Technology Documentation

faker.js allows developers to generate massive amounts of fake data in the browser and node.js.

Relevant URLs:
[NPM](https://www.npmjs.com/package/faker),
[GitHub](https://github.com/marak/faker.js/),
[Demo](https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html)

## Installation

```bash
npm install faker # NPM
```

## Usage

Faker can be used in the following ways:
1. By installing the npm package
2. Sending http requests to the faker microservice `http://faker.hook.io`.

### curl

```bash
curl "http://faker.hook.io?property=name.findName&locale=de"
```

### Node.js

```javascript
var faker = require('faker');

var randomName = faker.name.findName(); // Rowan Nikolaus
var randomEmail = faker.internet.email(); // Kassandra.Haley@erich.biz
var randomCard = faker.helpers.createCard(); // random contact card containing many properties
```

## API

### Faker.address

- zipCode
- city
- cityPrefix
- citySuffix
- streetName
- streetAddress
- streetSuffix
- streetPrefix
- secondaryAddress
- county
- country
- countryCode
- state
- stateAbbr
- latitude
- longitude

### Faker.commerce

- color
- department
- productName
- price
- productAdjective
- productMaterial
- product

### Faker.company

- suffixes
- companyName
- companySuffix
- catchPhrase
- bs
- catchPhraseAdjective
- catchPhraseDescriptor
- catchPhraseNoun
- bsAdjective
- bsBuzz
- bsNoun

### Faker.database

- column
- type
- collation
- engine

### Faker.date

- past
- future
- between
- recent
- month
- weekday

### Faker.fake()

Format strings with fake data.
```javascript
faker.fake("{{name.lastName}}, {{name.firstName}} {{name.suffix}}");  // "Marks, Dean Sr."
```

### Faker.finance

- account
- accountName
- mask
- amount
- transactionType
- currencyCode
- currencyName
- currencySymbol
- bitcoinAddress
- iban
- bic

### Faker.hacker

- abbreviation
- adjective
- noun
- verb
- ingverb
- phrase

### Faker.helpers

- randomize
- slugify
- replaceSymbolWithNumber
- replaceSymbols
- shuffle
- mustache
- createCard
- contextualCard
- userCard
- createTransaction

### Faker.image

- image
- avatar
- imageUrl
- abstract
- animals
- business
- cats
- city
- food
- nightlife
- fashion
- people
- nature
- sports
- technics
- transport
- dataUri

### Faker.internet

- avatar
- email
- exampleEmail
- userName
- protocol
- url
- domainName
- domainSuffix
- domainWord
- ip
- ipv6
- userAgent
- color
- mac
- password

### Faker.lorem

- word
- words
- sentence
- slug
- sentences
- paragraph
- paragraphs
- text
- lines

### Faker.name

- firstName
- lastName
- findName
- jobTitle
- prefix
- suffix
- title
- jobDescriptor
- jobArea
- jobType

### Faker.phone

- phoneNumber
- phoneNumberFormat
- phoneFormats

### Faker.random

- number
- arrayElement
- objectElement
- uuid
- boolean
- word
- words
- image
- locale
- alphaNumeric

### Faker.system

- fileName
- commonFileName
- mimeType
- commonFileType
- commonFileExt
- fileType
- fileExt
- directoryPath
- filePath
- semver

## Localization

Faker supports multiple locales.

### Set the Locale

```javascript
// sets locale to de
faker.locale = "de";
```

### Available Locales

- az
- cz
- de
- de_AT
- de_CH
- en
- en_AU
- en_BORK
- en_CA
- en_GB
- en_IE
- en_IND
- en_US
- en_au_ocker
- es
- es_MX
- fa
- fr
- fr_CA
- ge
- id_ID
- it
- ja
- ko
- nb_NO
- nep
- nl
- pl
- pt_BR
- ru
- sk
- sv
- tr
- uk
- vi
- zh_CN
- zh_TW

## Randomness Seed

To have consistent results set a randomness seed.
```javascript
faker.seed(123);

var firstRandom = faker.random.number();

// Setting the seed again resets the sequence.
faker.seed(123);

var secondRandom = faker.random.number();

console.log(firstRandom === secondRandom); //true
```