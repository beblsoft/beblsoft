/**
 * @file Faker API Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let faker = require('faker');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * API examples
 */
function api({
  address = false,
  commerce = false,
  company = false,
  database = false,
  date = false,
  finance = false,
  hacker = false,
  helpers = false,
  image = false,
  internet = false,
  lorem = false,
  name = false,
  phone = false,
  random = false,
  system = false,
  all = false
} = {}) {
  if (address || all) {
    logTitle({ title: 'address' });
    console.log(`zipCode                 : ${faker.address.zipCode()}`);
    console.log(`city                    : ${faker.address.city()}`);
    console.log(`cityPrefix              : ${faker.address.cityPrefix()}`);
    console.log(`citySuffix              : ${faker.address.citySuffix()}`);
    console.log(`streetName              : ${faker.address.streetName()}`);
    console.log(`streetAddress           : ${faker.address.streetAddress()}`);
    console.log(`streetSuffix            : ${faker.address.streetSuffix()}`);
    console.log(`streetPrefix            : ${faker.address.streetPrefix()}`);
    console.log(`secondaryAddress        : ${faker.address.secondaryAddress()}`);
    console.log(`county                  : ${faker.address.county()}`);
    console.log(`country                 : ${faker.address.country()}`);
    console.log(`countryCode             : ${faker.address.countryCode()}`);
    console.log(`state                   : ${faker.address.state()}`);
    console.log(`stateAbbr               : ${faker.address.stateAbbr()}`);
    console.log(`latitude                : ${faker.address.latitude()}`);
    console.log(`longitude               : ${faker.address.longitude()}`);
  }
  if (commerce || all) {
    logTitle({ title: 'commerce' });
    console.log(`color                   : ${faker.commerce.color()}`);
    console.log(`department              : ${faker.commerce.department()}`);
    console.log(`productName             : ${faker.commerce.productName()}`);
    console.log(`price                   : ${faker.commerce.price()}`);
    console.log(`productAdjective        : ${faker.commerce.productAdjective()}`);
    console.log(`productMaterial         : ${faker.commerce.productMaterial()}`);
    console.log(`product                 : ${faker.commerce.product()}`);
  }
  if (company || all) {
    logTitle({ title: 'company' });
    console.log(`suffixes                : ${faker.company.suffixes()}`);
    console.log(`companyName             : ${faker.company.companyName()}`);
    console.log(`companySuffix           : ${faker.company.companySuffix()}`);
    console.log(`catchPhrase             : ${faker.company.catchPhrase()}`);
    console.log(`bs                      : ${faker.company.bs()}`);
    console.log(`catchPhraseAdjective    : ${faker.company.catchPhraseAdjective()}`);
    console.log(`catchPhraseDescriptor   : ${faker.company.catchPhraseDescriptor()}`);
    console.log(`catchPhraseNoun         : ${faker.company.catchPhraseNoun()}`);
    console.log(`bsAdjective             : ${faker.company.bsAdjective()}`);
    console.log(`bsBuzz                  : ${faker.company.bsBuzz()}`);
    console.log(`bsNoun                  : ${faker.company.bsNoun()}`);
  }
  if (database || all) {
    logTitle({ title: 'database' });
    console.log(`column                  : ${faker.database.column()}`);
    console.log(`type                    : ${faker.database.type()}`);
    console.log(`collation               : ${faker.database.collation()}`);
    console.log(`engine                  : ${faker.database.engine()}`);
  }
  if (date || all) {
    logTitle({ title: 'date' });
    console.log(`past                    : ${faker.date.past()}`);
    console.log(`future                  : ${faker.date.future()}`);
    console.log(`between                 : ${faker.date.between()}`);
    console.log(`recent                  : ${faker.date.recent()}`);
    console.log(`month                   : ${faker.date.month()}`);
    console.log(`weekday                 : ${faker.date.weekday()}`);
  }
  if (finance || all) {
    logTitle({ title: 'finance' });
    console.log(`account                 : ${faker.finance.account()}`);
    console.log(`accountName             : ${faker.finance.accountName()}`);
    console.log(`mask                    : ${faker.finance.mask()}`);
    console.log(`amount                  : ${faker.finance.amount()}`);
    console.log(`transactionType         : ${faker.finance.transactionType()}`);
    console.log(`currencyCode            : ${faker.finance.currencyCode()}`);
    console.log(`currencyName            : ${faker.finance.currencyName()}`);
    console.log(`currencySymbol          : ${faker.finance.currencySymbol()}`);
    console.log(`bitcoinAddress          : ${faker.finance.bitcoinAddress()}`);
    console.log(`iban                    : ${faker.finance.iban()}`);
    console.log(`bic                     : ${faker.finance.bic()}`);
  }
  if (hacker || all) {
    logTitle({ title: 'hacker' });
    console.log(`abbreviation            : ${faker.hacker.abbreviation()}`);
    console.log(`adjective               : ${faker.hacker.adjective()}`);
    console.log(`noun                    : ${faker.hacker.noun()}`);
    console.log(`verb                    : ${faker.hacker.verb()}`);
    console.log(`ingverb                 : ${faker.hacker.ingverb()}`);
    console.log(`phrase                  : ${faker.hacker.phrase()}`);
  }
  if (helpers || all) {
    logTitle({ title: 'helpers' });
    console.log(`randomize               : ${faker.helpers.randomize()}`);
    console.log(`slugify                 : ${faker.helpers.slugify()}`);
    console.log(`replaceSymbolWithNumber : ${faker.helpers.replaceSymbolWithNumber()}`);
    console.log(`replaceSymbols          : ${faker.helpers.replaceSymbols()}`);
    console.log(`shuffle                 : ${faker.helpers.shuffle()}`);
    console.log(`mustache                : ${faker.helpers.mustache()}`);
    console.log(`createCard              : ${faker.helpers.createCard()}`);
    console.log(`contextualCard          : ${faker.helpers.contextualCard()}`);
    console.log(`userCard                : ${faker.helpers.userCard()}`);
    console.log(`createTransaction       : ${faker.helpers.createTransaction()}`);
  }
  if (image || all) {
    logTitle({ title: 'image' });
    console.log(`image                   : ${faker.image.image()}`);
    console.log(`avatar                  : ${faker.image.avatar()}`);
    console.log(`imageUrl                : ${faker.image.imageUrl()}`);
    console.log(`abstract                : ${faker.image.abstract()}`);
    console.log(`animals                 : ${faker.image.animals()}`);
    console.log(`business                : ${faker.image.business()}`);
    console.log(`cats                    : ${faker.image.cats()}`);
    console.log(`city                    : ${faker.image.city()}`);
    console.log(`food                    : ${faker.image.food()}`);
    console.log(`nightlife               : ${faker.image.nightlife()}`);
    console.log(`fashion                 : ${faker.image.fashion()}`);
    console.log(`people                  : ${faker.image.people()}`);
    console.log(`nature                  : ${faker.image.nature()}`);
    console.log(`sports                  : ${faker.image.sports()}`);
    console.log(`technics                : ${faker.image.technics()}`);
    console.log(`transport               : ${faker.image.transport()}`);
    console.log(`dataUri                 : ${faker.image.dataUri()}`);
  }
  if (internet || all) {
    logTitle({ title: 'internet' });
    console.log(`avatar                  : ${faker.internet.avatar()}`);
    console.log(`email                   : ${faker.internet.email()}`);
    console.log(`exampleEmail            : ${faker.internet.exampleEmail()}`);
    console.log(`userName                : ${faker.internet.userName()}`);
    console.log(`protocol                : ${faker.internet.protocol()}`);
    console.log(`url                     : ${faker.internet.url()}`);
    console.log(`domainName              : ${faker.internet.domainName()}`);
    console.log(`domainSuffix            : ${faker.internet.domainSuffix()}`);
    console.log(`domainWord              : ${faker.internet.domainWord()}`);
    console.log(`ip                      : ${faker.internet.ip()}`);
    console.log(`ipv6                    : ${faker.internet.ipv6()}`);
    console.log(`userAgent               : ${faker.internet.userAgent()}`);
    console.log(`color                   : ${faker.internet.color()}`);
    console.log(`mac                     : ${faker.internet.mac()}`);
    console.log(`password                : ${faker.internet.password()}`);
  }
  if (lorem || all) {
    logTitle({ title: 'lorem' });
    console.log(`suffixes                : ${faker.company.suffixes()}`);
    console.log(`companyName             : ${faker.company.companyName()}`);
    console.log(`companySuffix           : ${faker.company.companySuffix()}`);
    console.log(`catchPhrase             : ${faker.company.catchPhrase()}`);
    console.log(`bs                      : ${faker.company.bs()}`);
    console.log(`catchPhraseAdjective    : ${faker.company.catchPhraseAdjective()}`);
    console.log(`catchPhraseDescriptor   : ${faker.company.catchPhraseDescriptor()}`);
    console.log(`catchPhraseNoun         : ${faker.company.catchPhraseNoun()}`);
    console.log(`bsAdjective             : ${faker.company.bsAdjective()}`);
    console.log(`bsBuzz                  : ${faker.company.bsBuzz()}`);
    console.log(`bsNoun                  : ${faker.company.bsNoun()}`);
  }
  if (name || all) {
    logTitle({ title: 'name' });
    console.log(`firstName               : ${faker.name.firstName()}`);
    console.log(`lastName                : ${faker.name.lastName()}`);
    console.log(`findName                : ${faker.name.findName()}`);
    console.log(`jobTitle                : ${faker.name.jobTitle()}`);
    console.log(`prefix                  : ${faker.name.prefix()}`);
    console.log(`suffix                  : ${faker.name.suffix()}`);
    console.log(`title                   : ${faker.name.title()}`);
    console.log(`jobDescriptor           : ${faker.name.jobDescriptor()}`);
    console.log(`jobArea                 : ${faker.name.jobArea()}`);
    console.log(`jobType                 : ${faker.name.jobType()}`);
  }
  if (phone || all) {
    logTitle({ title: 'phone' });
    console.log(`phoneNumber             : ${faker.phone.phoneNumber()}`);
    console.log(`phoneNumberFormat       : ${faker.phone.phoneNumberFormat()}`);
    console.log(`phoneFormats            : ${faker.phone.phoneFormats()}`);
  }
  if (random || all) {
    logTitle({ title: 'random' });
    console.log(`number                  : ${faker.random.number()}`);
    console.log(`arrayElement            : ${faker.random.arrayElement()}`);
    console.log(`objectElement           : ${faker.random.objectElement()}`);
    console.log(`uuid                    : ${faker.random.uuid()}`);
    console.log(`boolean                 : ${faker.random.boolean()}`);
    console.log(`word                    : ${faker.random.word()}`);
    console.log(`words                   : ${faker.random.words()}`);
    console.log(`image                   : ${faker.random.image()}`);
    console.log(`locale                  : ${faker.random.locale()}`);
    console.log(`alphaNumeric            : ${faker.random.alphaNumeric()}`);
  }
  if (system || all) {
    logTitle({ title: 'system' });
    console.log(`fileName                : ${faker.system.fileName()}`);
    console.log(`commonFileName          : ${faker.system.commonFileName()}`);
    console.log(`mimeType                : ${faker.system.mimeType()}`);
    console.log(`commonFileType          : ${faker.system.commonFileType()}`);
    console.log(`commonFileExt           : ${faker.system.commonFileExt()}`);
    console.log(`fileType                : ${faker.system.fileType()}`);
    console.log(`fileExt                 : ${faker.system.fileExt()}`);
    console.log(`directoryPath           : ${faker.system.directoryPath()}`);
    console.log(`filePath                : ${faker.system.filePath()}`);
    console.log(`semver                  : ${faker.system.semver()}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
api({ all: true });
