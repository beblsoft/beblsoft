# Validator Projects Docuemtation

## validators.js

Demonstrates many different string validators including: email, json, jwt, and url.

To run:
- Install: `npm install`
- Run: `node validators.js`

## sanitizers.js

Demonstrates many different string sanitizers including: escaping, unescaping, and email normalization.

To run:
- Install: `npm install`
- Run: `node sanitizers.js`

