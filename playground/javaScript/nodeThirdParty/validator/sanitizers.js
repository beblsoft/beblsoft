/**
 * @file Validator Sanitizer Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let validator = require('validator');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Sanitization examples
 */
function doSanitization({
  escape = false,
  unescape = false,
  normalizeEmail = false,
  all = false
} = {}) {
  if (escape || all) {
    logTitle({ title: 'escape' });
    console.log(`${validator.escape('John, Randy, & Steve')}`);
  }
  if (unescape || all) {
    logTitle({ title: 'unescape' });
    console.log(`${validator.unescape('John, Randy, &amp; Steve')}`);
  }
  if (normalizeEmail || all) {
    logTitle({ title: 'normalizeEmail' });
    console.log(`${validator.normalizeEmail('JAMES@gmail.com', { all_lowercase: true })}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
doSanitization({ all: true });
