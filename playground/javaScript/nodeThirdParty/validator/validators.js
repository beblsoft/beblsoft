/**
 * @file Validator Validators Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let validator = require('validator');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Validation examples
 */
function doValidation({
  contains = false,
  equals = false,
  isAlpha = false,
  isAlphanumeric = false,
  isCreditCard = false,
  isCurrency = false,
  isDataURI = false,
  isDecimal = false,
  isEmail = false,
  isFQDN = false,
  isHash = false,
  isIP = false,
  isISO8601 = false,
  isInt = false,
  isJSON = false,
  isJWT = false,
  isMimeType = false,
  isMobilePhone = false,
  isURL = false,
  isUUID = false,
  matches = false,
  all = false
} = {}) {
  if (contains || all) {
    logTitle({ title: 'contains' });
    console.log(`${validator.contains('1234', '123')}`);
    console.log(`${validator.contains('1234', '5')}`);
  }
  if (equals || all) {
    logTitle({ title: 'equals' });
    console.log(`${validator.equals('1234', '1234')}`);
    console.log(`${validator.equals('1234', '123')}`);
  }
  if (isAlpha || all) {
    logTitle({ title: 'isAlpha' });
    console.log(`${validator.isAlpha('abcd', 'en-US')}`);
    console.log(`${validator.isAlpha('1234', 'en-US')}`);
  }
  if (isAlphanumeric || all) {
    logTitle({ title: 'isAlphanumeric' });
    console.log(`${validator.isAlphanumeric('abcd', 'en-US')}`);
    console.log(`${validator.isAlphanumeric('1234', 'en-US')}`);
    console.log(`${validator.isAlphanumeric('$@#$', 'en-US')}`);
  }
  if (isCreditCard || all) {
    logTitle({ title: 'isCreditCard' });
    console.log(`${validator.isCreditCard('4485356177727734')}`);
    console.log(`${validator.isCreditCard('1234567812345678')}`);
  }
  if (isCurrency || all) {
    logTitle({ title: 'isCurrency' });
    console.log(`${validator.isCurrency('$1.00', { symbol: '$' })}`);
    console.log(`${validator.isCurrency('$1.ab', { symbol: '$' })}`);
  }
  if (isDataURI || all) {
    logTitle({ title: 'isDataURI' });
    console.log(`${validator.isDataURI('data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D')}`);
    console.log(`${validator.isDataURI('text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D')}`);
  }
  if (isDecimal || all) {
    logTitle({ title: 'isDecimal' });
    console.log(`${validator.isDecimal('0.1111', { locale:'en-US' })}`);
    console.log(`${validator.isDecimal('a.1111', { locale:'en-US' })}`);
  }
  if (isEmail || all) {
    logTitle({ title: 'isEmail' });
    console.log(`${validator.isEmail('steven-sdflkj@gmail.com')}`);
    console.log(`${validator.isEmail('2340985askabsgmail.com')}`);
  }
  if (isFQDN || all) {
    logTitle({ title: 'isFQDN' });
    console.log(`${validator.isFQDN('www.smeckn.com')}`);
    console.log(`${validator.isFQDN('asdfasdfasdf')}`);
  }
  if (isHash || all) {
    logTitle({ title: 'isHash' });
    console.log(`${validator.isHash('3a922387e2277d2ee7fa9969c2054aa4', 'md4')}`);
    console.log(`${validator.isHash('3a922387e2277d2ee7fa9969c2054aa4sdfsdf', 'md4')}`);
  }
  if (isIP || all) {
    logTitle({ title: 'isIP' });
    console.log(`${validator.isIP('1.2.3.4')}`);
    console.log(`${validator.isIP('3a9223sdf')}`);
  }
  if (isISO8601 || all) {
    logTitle({ title: 'isISO8601' });
    console.log(`${validator.isISO8601('2008-09-15T15:53:00')}`);
    console.log(`${validator.isISO8601('3a9223sdf')}`);
  }
  if (isInt || all) {
    logTitle({ title: 'isInt' });
    console.log(`${validator.isInt('234523452345')}`);
    console.log(`${validator.isInt('3a9223sdf')}`);
  }
  if (isJSON || all) {
    logTitle({ title: 'isJSON' });
    console.log(`${validator.isJSON('{"name":"John", "age":31, "city":"New York"}')}`);
    console.log(`${validator.isJSON('3a9223sdf')}`);
  }
  if (isJWT || all) {
    logTitle({ title: 'isJWT' });
    console.log(`${validator.isJWT('xxxxx.yyyyy.zzzzz')}`);
    console.log(`${validator.isJWT('3a9223sdf')}`);
  }
  if (isMimeType || all) {
    logTitle({ title: 'isMimeType' });
    console.log(`${validator.isMimeType('application/javascript')}`);
    console.log(`${validator.isMimeType('3a9223sdf')}`);
  }
  if (isMobilePhone || all) {
    logTitle({ title: 'isMobilePhone' });
    console.log(`${validator.isMobilePhone('508-233-3234', 'en-US')}`);
    console.log(`${validator.isMobilePhone('53308-233-3234', 'en-US')}`);
  }
  if (isURL || all) {
    logTitle({ title: 'isURL' });
    console.log(`${validator.isURL('https://www.smeckn.com')}`);
    console.log(`${validator.isURL('www.smeckn.com')}`);
    console.log(`${validator.isURL('smeckn.com')}`);
    console.log(`${validator.isURL('com')}`);
  }
  if (isUUID || all) {
    logTitle({ title: 'isUUID' });
    console.log(`${validator.isUUID('45745c60-7b1a-11e8-9c9c-2d42b21b1a3e')}`);
    console.log(`${validator.isUUID('asdfasdflkajsd')}`);
  }
  if (matches || all) {
    logTitle({ title: 'matches' });
    console.log(`${validator.matches('foo', /foo/i)}`);
    console.log(`${validator.matches('asdfasdf', /foo/i)}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
doValidation({ all: true });
