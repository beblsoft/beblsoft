# Validator Technology Docuemtation

Validator is a library of string validators and sanitizers.

Relevant URLs:
[NPM](https://www.npmjs.com/package/validator),
[Github](https://github.com/chriso/validator.js)

## Installation

Install with npm: `npm install validator`

## Validators

See `validators.js`

## Sanitizers

See `sanitizers.js`