# AutoPrefixer Documentation

Autoprefixer is a PostCSS plugin to parse CSS and add vendor prefixes to CSS rules using values from
[Can I Use](https://caniuse.com/). Being _PostCSS_ autoprefixer runs after css _preprocessors_ such as
sass and less create the css.

Relevant URLs:
[GitHub](https://github.com/postcss/autoprefixer),
[Interactive Demo](https://autoprefixer.github.io/),
[CSS-Tricks](https://css-tricks.com/autoprefixer/)

## Example

Write your CSS rules without vendor prefixes (in fact, forget about them entirely):

```css
::placeholder {
  color: gray;
}

.image {
  background-image: url(image@1x.png);
}
@media (min-resolution: 2dppx) {
  .image {
    background-image: url(image@2x.png);
  }
}
```

Autoprefixer will use the data base on current browser popularity and property support to apply prefixes
for you.

```css
::-webkit-input-placeholder {
  color: gray;
}
::-moz-placeholder {
  color: gray;
}
:-ms-input-placeholder {
  color: gray;
}
::-ms-input-placeholder {
  color: gray;
}
::placeholder {
  color: gray;
}

.image {
  background-image: url(image@1x.png);
}
@media (-webkit-min-device-pixel-ratio: 2),
       (-o-min-device-pixel-ratio: 2/1),
       (min-resolution: 2dppx) {
  .image {
    background-image: url(image@2x.png);
  }
}
```

## Browsers

Autoprefixer uses [Browserslist](https://github.com/ai/browserslist), so you can specify the browsers
you want to target in your project with queries liek `> 5%`.

The best way to provide browsers is a `.browserslistrc` file in your project root, or by adding a
`browserslist` key to your package.json.

## Usage

### Webpack

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      }
    ]
  }
}
```

And create a `postcss.config.js` with:

```js
module.exports = {
  plugins: [
    require('autoprefixer')
  ]
}
```

### CLI

You can use the `postcss-cli` to run Autoprefixer from CLI:

```bash
npm install postcss-cli autoprefixer
npx postcss *.css --use autoprefixer -d build/
```

### JavaScript

You can use Autoprefixer with PostCSS in your Node.js application or if you want to develop an
Autoprefixer plugin for a new environment.

```js
const autoprefixer = require('autoprefixer')
const postcss = require('postcss')

postcss([ autoprefixer ]).process(css).then(result => {
  result.warnings().forEach(warn => {
    console.warn(warn.toString())
  })
  console.log(result.css)
})
```