/**
 * @file Routing Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let express = require('express');


/* ------------------------ GLOBALS ---------------------------------------- */
let app = express();
let port = 3000;

/* ------------------------ ROUTES ----------------------------------------- */
app.get('/get', function (req, res) {
  res.send('Got a GET request !');
});

app.post('/post', function (req, res) {
  res.send('Got a POST request');
});

app.put('/put', function (req, res) {
  res.send('Got a PUT request');
});

app.delete('/delete', function (req, res) {
  res.send('Got a DELETE request');
});


/* ------------------------ MAIN ------------------------------------------- */
app.listen(port, () => { console.log(`Listening on port ${port}!`); });
