/**
 * @file HelloWorld Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let express = require('express');


/* ------------------------ GLOBALS ---------------------------------------- */
let app = express();
let port = 3000;


/* ------------------------ ROUTES ----------------------------------------- */
app.get('/', function (req, res) {
  res.send('Hello World');
});


/* ------------------------ MAIN ------------------------------------------- */
app.listen(port, () => { console.log(`Listening on port ${port}!`); });
