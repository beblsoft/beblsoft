# Express Samples Documentation

## helloWorld.js

Hello World Express Application

To run:
- Install: `npm install`
- Start: `node helloWorld.js`
- View: Open Chrome to http://localhost:3000

## routing.js

Demonstrate listening on different routes and methods.

To run:
- Install: `npm install`
- Start: `node routing.js`

In another terminal:

- `curl -X GET http://localhost:3000/get`
- `curl -X POST http://localhost:3000/post`
- `curl -X PUT http://localhost:3000/put`
- `curl -X DELETE http://localhost:3000/delete`

## staticFiles.js

Demonstrate serving static files.

To run:
- Install: `npm install`
- Start: `node staticFiles.js`
- View: Open Chrome to http://localhost:3000/staticFiles.js

## https.js

Demonstrate serving static files over HTTPs.
Note: Chrome must be configured to allowed self-signed certificates.

To run:
- Install: `npm install`
- Start: `node https.js`
- View: Open Chrome to http://localhost:8080