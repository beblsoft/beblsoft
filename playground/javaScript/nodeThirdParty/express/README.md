# Express Technology Documentation

Exrpress is a fast, unopinionated, minimalist web framework for __node__.
Express's philosophy is to provide small, robust tooling for HTTP servers. This makes it a great
solution from single page applications, web sites, hybrids, or public HTTP APIs.

Features:
- Robust routing
- Focus on high performance
- High test coverage
- HTTP helpers (redirection, caching, etc)
- View system supporting 14+ template engines
- Content negotiation
- Executable for generating applications quickly

Relevant URLs:
[Home](http://expressjs.com/),
[NPM](https://www.npmjs.com/package/express),
[GitHub](https://github.com/expressjs/expressjs.com)

## Installation

Express requires Node.js >= 0.10. Installation instructions:

```bash
npm install express
```

## Basic Usage

```javascript
let express = require('express');
let app = express();

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(3000)
```