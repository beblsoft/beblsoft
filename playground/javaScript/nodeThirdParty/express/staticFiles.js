/**
 * @file Example Serving Static Files
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let express = require('express');


/* ------------------------ GLOBALS ---------------------------------------- */
let app = express();
let port = 3000;
let rootDir = '.';
let options = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['htm', 'html', 'js'],
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now());
  }
};

/* ------------------------ ROUTES ----------------------------------------- */
app.use(express.static(rootDir, [options]));


/* ------------------------ MAIN ------------------------------------------- */
app.listen(port, () => { console.log(`Listening on port ${port}!`); });
