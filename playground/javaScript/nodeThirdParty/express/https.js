/*
  NAME:
    server.js

  DESCRIPTION:
    Serve https
*/

/* ------------------------ REQUIRE ---------------------------------------- */
let path = require('path');
let fs = require('fs');
let express = require('express');
let https = require('https');


/* ------------------------ CONSTANTS -------------------------------------- */
const PORT = 8080;
const HOST = 'localhost';


/* ------------------------ MAIN ------------------------------------------- */
var key = fs.readFileSync('./security/localhost.key');
var cert = fs.readFileSync('./security/localhost.pem')
var https_options = {
    key: key,
    cert: cert
};
app = express();

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/https.html'));
});

server = https.createServer(https_options, app).listen(PORT, HOST);
console.log('HTTPS Server listening on https://%s:%s', HOST, PORT);