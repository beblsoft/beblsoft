/**
 * @file Moment Manipulation Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Manipulate Examples
 */
function manipulate({
  add = false,
  subtract = false,
  startOfTime = false,
  endOfTime = false,
  localUTC = false,
  all = false
} = {}) {
  if (add || all) {
    logTitle({ title: 'Add' });
    console.log(`Add            : ${moment().add(7, 'days').format()}`);
  }
  if (subtract || all) {
    logTitle({ title: 'Subtract' });
    console.log(`Subtract       : ${moment().subtract(7, 'days').format()}`);
  }
  if (startOfTime || all) {
    logTitle({ title: 'Start of Time' });
    console.log(`year           : ${moment().startOf('year').format()}`);
    console.log(`month          : ${moment().startOf('month').format()}`);
    console.log(`quarter        : ${moment().startOf('quarter').format()}`);
    console.log(`week           : ${moment().startOf('week').format()}`);
    console.log(`isoWeek        : ${moment().startOf('isoWeek').format()}`);
    console.log(`day            : ${moment().startOf('day').format()}`);
    console.log(`date           : ${moment().startOf('date').format()}`);
    console.log(`hour           : ${moment().startOf('hour').format()}`);
    console.log(`minute         : ${moment().startOf('minute').format()}`);
    console.log(`second         : ${moment().startOf('second').format()}`);
  }
  if (endOfTime || all) {
    logTitle({ title: 'End of Time' });
    console.log(`year           : ${moment().endOf('year').format()}`);
    console.log(`month          : ${moment().endOf('month').format()}`);
    console.log(`quarter        : ${moment().endOf('quarter').format()}`);
    console.log(`week           : ${moment().endOf('week').format()}`);
    console.log(`isoWeek        : ${moment().endOf('isoWeek').format()}`);
    console.log(`day            : ${moment().endOf('day').format()}`);
    console.log(`date           : ${moment().endOf('date').format()}`);
    console.log(`hour           : ${moment().endOf('hour').format()}`);
    console.log(`minute         : ${moment().endOf('minute').format()}`);
    console.log(`second         : ${moment().endOf('second').format()}`);
  }
  if (localUTC || all) {
    logTitle({ title: 'Local vs. UTC' });
    let a = moment([2011, 0, 1, 8]);
    console.log(`local          : ${a.local().format()}`);
    console.log(`utc            : ${a.utc().format()}`);
    console.log(`utc offset     : ${a.local().utcOffset()}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
manipulate({ all: true });
