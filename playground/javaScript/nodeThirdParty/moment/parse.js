/**
 * @file Moment Parsing Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Parse examples
 */
function parse({
  now = false,
  ISOString = false,
  RFC2822 = false,
  formatString = false,
  invalid = false,
  locale = false,
  strict = false,
  object = false,
  timeStamp = false,
  utc = false,
  all = false
} = {}) {
  if (now || all) {
    logTitle({ title: 'Parse Now' });
    console.log(`Empty          : ${moment().format()}`);
    console.log(`Undefined      : ${moment(undefined).format()}`);
    console.log(`List           : ${moment([]).format()}`);
    console.log(`Braces         : ${moment({}).format()}`);
  }
  if (ISOString || all) {
    logTitle({ title: 'Parse ISO String' });
    console.log(`With T         : ${moment('2013-02-08T01:00:00.000').format()}`);
    console.log(`With Space     : ${moment('2013-02-08 01:00:00.000', moment.ISO_8601).format()}`);
    console.log(`UTC            : ${moment('2013-02-08T01:00:00.000Z').format()}`);
    console.log(`UTC Shift      : ${moment('2013-02-08T01:00:00.000+07:00').format()}`);
  }
  if (RFC2822 || all) {
    logTitle({ title: 'Parse RFC2822 String' });
    console.log(`Short          : ${moment('6 Mar 17 21:22 UT').format()}`);
    console.log(`Long           : ${moment('Mon, 06 Mar 2017 21:22:23 +0000').format()}`);
  }
  if (formatString || all) {
    logTitle({ title: 'Parse Format String' });
    console.log(`Basic          : ${moment('12-25-1995', 'MM-DD-YYYY').format()}`);
    console.log(`UTC            : ${moment('2010-10-20 4:30 +0000', 'YYYY-MM-DD HH:mm Z').format()}`);
  }
  if (invalid || all) {
    logTitle({ title: 'Invalid' });
    console.log(`Bad month      : ${moment('2010 13', 'YYYY MM').isValid()}`);
    console.log(`Bad day        : ${moment('2010 11 31', 'YYYY MM DD').isValid()}`);
    console.log(`Not leap year  : ${moment('2010 2 29', 'YYYY MM DD').isValid()}`);
    console.log(`Bad month      : ${moment('2010 notamonth 29', 'YYYY MM DD').isValid()}`);
  }
  if (locale || all) {
    logTitle({ title: 'Locale' });
    console.log(`English        : ${moment('Thursday, September 4 1986 8:30 PM', 'LLLL', 'en').format()}`);
    console.log(`French         : ${moment('2012 juillet', 'YYYY MMM', 'fr').format()}`);
  }
  if (strict || all) {
    logTitle({ title: 'Strict' });
    console.log(`Not Strict     : ${moment('It is 2012-05-25', 'YYYY-MM-DD').isValid()}`);
    console.log(`Strict         : ${moment('It is 2012-05-25', 'YYYY-MM-DD', true).isValid()}`);
  }
  if (object || all) {
    logTitle({ title: 'Object' });
    console.log(`From Literals  : ${moment({ years:2010, months:3, date:5, hours:15, minutes:10, seconds:3, milliseconds:123 }).format()}`);
    console.log(`From Strings   : ${moment({ years:'2010', months:'3', date:'5', hours:'15', minutes:'10', seconds:'3', milliseconds:'123' }).format()}`);
  }
  if (timeStamp || all) {
    logTitle({ title: 'Timestamp' });
    console.log(`Milliseconds   : ${moment(1318781876406).format()}`);
    console.log(`Seconds        : ${moment.unix(1318781876.721).format()}`);
  }
  if (utc || all) {
    logTitle({ title: 'UTC' });
    console.log(`Local          : ${moment().format()}`);
    console.log(`UTC            : ${moment().utc().format()}`);
  }

}


/* ------------------------ MAIN ------------------------------------------- */
parse({ all: true });
