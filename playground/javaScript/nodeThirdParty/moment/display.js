/**
 * @file Moment Display Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Display Examples
 */
function display({
  generic = false,
  formatTable = false,
  formatLocaleTable = false,
  escape = false,
  timeFromTo = false,
  difference = false,
  misc = false,
  all = false
} = {}) {
  if (generic || all) {
    logTitle({ title: 'Generic' });
    console.log(`Default        : ${moment().format()}`);
    console.log(`Ex 1           : ${moment().format('dddd, MMMM Do YYYY, h:mm:ss a')}`);
    console.log(`Ex 2           : ${moment().format('ddd, hA')}`);
    console.log(`Ex 3           : ${moment().format('YYYY MM DD')}`);
  }
  if (formatTable || all) {
    logTitle({ title: 'Format Table' });
    console.log(`Month          : ${moment().format('M')}`);
    console.log(`Month          : ${moment().format('Mo')}`);
    console.log(`Month          : ${moment().format('MM')}`);
    console.log(`Month          : ${moment().format('MMM')}`);
    console.log(`Month          : ${moment().format('MMMM')}`);
    console.log(`Quarter        : ${moment().format('Q')}`);
    console.log(`Quarter        : ${moment().format('Qo')}`);
    console.log(`Day of Month   : ${moment().format('D')}`);
    console.log(`Day of Month   : ${moment().format('Do')}`);
    console.log(`Day of Month   : ${moment().format('DD')}`);
    console.log(`Day of Year    : ${moment().format('DDD')}`);
    console.log(`Day of Year    : ${moment().format('DDDo')}`);
    console.log(`Day of Year    : ${moment().format('DDDD')}`);
    console.log(`Day of Week    : ${moment().format('d')}`);
    console.log(`Day of Week    : ${moment().format('do')}`);
    console.log(`Day of Week    : ${moment().format('dd')}`);
    console.log(`Day of Week    : ${moment().format('ddd')}`);
    console.log(`Day of Week    : ${moment().format('dddd')}`);
    console.log(`Day of Week    : ${moment().format('e')}`);
    console.log(`Day of Week    : ${moment().format('E')}`);
    console.log(`Week of Year   : ${moment().format('w')}`);
    console.log(`Week of Year   : ${moment().format('wo')}`);
    console.log(`Week of Year   : ${moment().format('ww')}`);
    console.log(`Week of Year   : ${moment().format('W')}`);
    console.log(`Week of Year   : ${moment().format('Wo')}`);
    console.log(`Week of Year   : ${moment().format('WW')}`);
    console.log(`Year           : ${moment().format('YY')}`);
    console.log(`Year           : ${moment().format('YYYY')}`);
    console.log(`Year           : ${moment().format('Y')}`);
    console.log(`Week Year      : ${moment().format('gg')}`);
    console.log(`Week Year      : ${moment().format('gggg')}`);
    console.log(`Week Year      : ${moment().format('GG')}`);
    console.log(`Week Year      : ${moment().format('GGGG')}`);
    console.log(`AM/PM          : ${moment().format('A')}`);
    console.log(`AM/PM          : ${moment().format('a')}`);
    console.log(`Hour           : ${moment().format('H')}`);
    console.log(`Hour           : ${moment().format('HH')}`);
    console.log(`Hour           : ${moment().format('h')}`);
    console.log(`Hour           : ${moment().format('hh')}`);
    console.log(`Hour           : ${moment().format('k')}`);
    console.log(`Hour           : ${moment().format('kk')}`);
    console.log(`Minute         : ${moment().format('m')}`);
    console.log(`Minute         : ${moment().format('mm')}`);
    console.log(`Second         : ${moment().format('s')}`);
    console.log(`Second         : ${moment().format('ss')}`);
    console.log(`Fractional Sec : ${moment().format('S')}`);
    console.log(`Fractional Sec : ${moment().format('SS')}`);
    console.log(`Fractional Sec : ${moment().format('SSS')}`);
    console.log(`Fractional Sec : ${moment().format('SSSSS')}`);
    console.log(`Time Zone      : ${moment().format('Z')}`);
    console.log(`Time Zone      : ${moment().format('ZZ')}`);
    console.log(`Unix TStamp    : ${moment().format('X')}`);
    console.log(`Unix MS TStamp : ${moment().format('x')}`);
  }
  if (formatLocaleTable || all) {
    logTitle({ title: 'Format Locale Table' });
    console.log(`LTS            : ${moment().format('LTS')}`);
    console.log(`L              : ${moment().format('L')}`);
    console.log(`l              : ${moment().format('l')}`);
    console.log(`LL             : ${moment().format('LL')}`);
    console.log(`ll             : ${moment().format('ll')}`);
    console.log(`LLL            : ${moment().format('LLL')}`);
    console.log(`lll            : ${moment().format('lll')}`);
    console.log(`LLLL           : ${moment().format('LLLL')}`);
    console.log(`llll           : ${moment().format('llll')}`);
  }
  if (escape || all) {
    logTitle({ title: 'Escape' });
    console.log(`Sample         : ${moment().format('[Today]: dddd')}`);
  }
  if (timeFromTo || all) {
    logTitle({ title: 'Time From and To' });
    let a = moment([2007, 0, 28]);
    let b = moment([2007, 0, 29]);
    console.log(`a              : ${a.format()}`);
    console.log(`b              : ${b.format()}`);
    console.log(`a from now     : ${a.fromNow()}`);
    console.log(`a from b       : ${a.from(b)}`);
    console.log(`a to now       : ${a.toNow()}`);
    console.log(`a to b         : ${a.to(b)}`);
  }
  if (difference || all) {
    logTitle({ title: 'Difference' });
    let a = moment([2007, 0, 29]);
    let b = moment([2007, 0, 28]);
    console.log(`a              : ${a.format()}`);
    console.log(`b              : ${b.format()}`);
    console.log(`a diff b (ms)  : ${a.diff(b)}`);
    console.log(`a diff b (days): ${a.diff(b, 'days')}`);
  }
  if (misc || all) {
    logTitle({ title: 'Misc' });
    console.log(`Unix MS        : ${moment(1318874398806).valueOf()}`);
    console.log(`Unix Sec       : ${moment(1318874398806).unix()}`);
    console.log(`ISO 8601       : ${moment().toISOString()}`);
    console.log(`Object         : ${moment().toObject()}`);
    console.log(`JS Date        : ${moment().toString()}`);
    console.log(`Inspect        : ${moment().inspect()}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
display({ all: true });
