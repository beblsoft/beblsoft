/**
 * @file Moment Duration Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Duration Examples
 */
function duration({
  create = false,
  clone = false,
  humanize = false,
  getters = false,
  addSubtract = false,
  all = false
} = {}) {
  if (create || all) {
    logTitle({ title: 'Creating' });
    console.log(`milliseconds        : ${moment.duration(100)}`);
    console.log(`seconds             : ${moment.duration(100, 'seconds')}`);
    console.log(`minutes             : ${moment.duration(100, 'minutes')}`);
    console.log(`hours               : ${moment.duration(100, 'hours')}`);
    console.log(`days                : ${moment.duration(100, 'days')}`);
    console.log(`weeks               : ${moment.duration(100, 'weeks')}`);
    console.log(`months              : ${moment.duration(100, 'months')}`);
    console.log(`years               : ${moment.duration(100, 'years')}`);
    console.log(`object              : ${moment.duration({ seconds: 2, minutes: 2, hours: 2, days: 2, weeks: 2, months: 2, years: 2 }) }`);
  }
  if (clone || all) {
    logTitle({ title: 'Cloning' });
    let a = moment.duration(100);
    let b = a.clone();
    console.log(`a                   : ${a}`);
    console.log(`b                   : ${b}`);
  }
  if (humanize || all) {
    logTitle({ title: 'Humanize' });
    console.log(`Ex 1                : ${moment.duration(1, 'minutes').humanize()}`);
    console.log(`Ex 2                : ${moment.duration(2, 'minutes').humanize()}`);
    console.log(`Ex 3                : ${moment.duration(24, 'hours').humanize()}`);
  }
  if (getters || all) {
    logTitle({ title: 'Getters' });
    let a = moment.duration(123948732450987);
    console.log(`a                   : ${a}`);
    console.log(`milliseconds        : ${a.milliseconds()}`);
    console.log(`asMilliseconds      : ${a.asMilliseconds()}`);
    console.log(`seconds             : ${a.seconds()}`);
    console.log(`asSeconds           : ${a.asSeconds()}`);
    console.log(`minutes             : ${a.minutes()}`);
    console.log(`asMinutes           : ${a.asMinutes()}`);
    console.log(`hours               : ${a.hours()}`);
    console.log(`asHours             : ${a.asHours()}`);
    console.log(`days                : ${a.days()}`);
    console.log(`asDays              : ${a.asDays()}`);
    console.log(`weeks               : ${a.weeks()}`);
    console.log(`asWeeks             : ${a.asWeeks()}`);
    console.log(`months              : ${a.months()}`);
    console.log(`asMonths            : ${a.asMonths()}`);
    console.log(`years               : ${a.years()}`);
    console.log(`asYears             : ${a.asYears()}`);
  }
  if (addSubtract || all) {
    logTitle({ title: 'Add, Subtract' });
    let a = moment.duration(2, 'seconds');
    let b = moment.duration(1, 'seconds');
    console.log(`a                   : ${a}`);
    console.log(`b                   : ${b}`);
    console.log(`a.add(b)            : ${a.add(b)}`);
    console.log(`a.subtract(b)       : ${a.subtract(b)}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
duration({ all: true });
