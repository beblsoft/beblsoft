/**
 * @file Moment Geting and Setting Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Get Set Examples
 */
function getSet({
  timescales = false,
  maxmin = false,
  all = false
} = {}) {
  if (timescales || all) {
    logTitle({ title: 'Timescales' });
    let m = moment();
    m.millisecond(1);
    console.log(`millisecond    : ${m.millisecond()}`);
    m.second(2);
    console.log(`second         : ${m.millisecond()}`);
    m.minute(3);
    console.log(`minute         : ${m.minute()}`);
    m.hour(4);
    console.log(`hour           : ${m.hour()}`);
    m.date(5);
    console.log(`date           : ${m.date()}`);
    m.day(6);
    console.log(`day            : ${m.day()}`);
    m.dayOfYear(7);
    console.log(`dayOfYear      : ${m.dayOfYear()}`);
    m.week(8);
    console.log(`week           : ${m.week()}`);
    m.month(9);
    console.log(`month          : ${m.month()}`);
    m.quarter(3);
    console.log(`quarter        : ${m.quarter()}`);
    m.year(2012);
    console.log(`year           : ${m.year()}`);
  }
  if (maxmin || all) {
    logTitle({ title: 'Maxiumum, Minimum' });
    let a = moment().subtract(1, 'day');
    let b = moment().add(1, 'day');
    console.log(`a              : ${a.format()}`);
    console.log(`b              : ${b.format()}`);
    console.log(`maxiumum       : ${moment.max(a, b).format()}`);
    console.log(`minimum        : ${moment.min(a, b).format()}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
getSet({ all: true });
