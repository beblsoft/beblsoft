# Moment.js Documentation

Moment.js allows developers to parse, validate, manipulate, and display dates and times in JavaScript.

Moment.js was designed to work for Node.js and in the browser.

Relevant URLs:
[Home](https://momentjs.com/),
[Docs](https://momentjs.com/docs/),
[GitHub](https://github.com/moment/moment/)

## Installation

```bash
npm install moment --save   # npm
yarn add moment             # Yarn
Install-Package Moment.js   # NuGet
spm install moment --save   # spm
meteor add momentjs:moment  # meteor
bower install moment --save # bower (deprecated)
```

## Parse

Instead of modifying the native Date.prototype, Moment.js creates a wrapper for the Date object.
To get this wrapper object, simply call moment() with one of the supported input types.
The Moment prototype is exposed through moment.fn. If you want to add your own functions,
that is where you would put them.

Timezones when parsing dates are determined as follows:
- `moment(...)` is local mode. Ambiguous input (without offset) is asumed to be local time.
  Unambiguous input (with offset) is adjusted to local time.
- `moment.utc(...)` is utc mode. Ambiguous input is assumed to be UTC. Unambiguous input is adjusted to UTC
- `moment.parseZone()` keep the input zone passed in. If the input is ambigious, it is the same as
   local
- `moment.tz(...)` with the moment-timezone plugin can parse input in a specific time zone.

### Now

Return current date and time:
```javascript
moment();
moment(undefined); // From 2.14.0 onward, also supported
moment([]);
moment({});
```

### String (no format)

When creating from a string without a format, moment parses in the following order:
[ISO 8601](https://en.wikipedia.org/wiki/ISO_8601),
[RFC 2822](https://tools.ietf.org/html/rfc2822#section-3.3),
`Date` string.
```javascript
moment('2013-02-08T24:00:00.000');
```

ISO strings:
```bash
2013-02-08                     # A calendar date part
2013-W06-5                     # A week date part
2013-039                       # An ordinal date part

20130208                       # Basic (short) full date
2013W065                       # Basic (short) week, weekday
2013W06                        # Basic (short) week only
2013050                        # Basic (short) ordinal date

2013-02-08T09                  # An hour time part separated by a T
2013-02-08 09                  # An hour time part separated by a space
2013-02-08 09:30               # An hour and minute time part
2013-02-08 09:30:26            # An hour, minute, and second time part
2013-02-08 09:30:26.123        # An hour, minute, second, and millisecond time part
2013-02-08 24:00:00.000        # hour 24, minute, second, millisecond equal 0 means next day at midnight

20130208T080910,123            # Short date and time up to ms, separated by comma
20130208T080910.123            # Short date and time up to ms
20130208T080910                # Short date and time up to seconds
20130208T0809                  # Short date and time up to minutes
20130208T08                    # Short date and time, hours only

2013-02-08 09+07:00            # +-HH:mm
2013-02-08 09-0100             # +-HHmm
2013-02-08 09Z                 # Z
2013-02-08 09:30:26.123+07:00  # +-HH:mm
2013-02-08 09:30:26.123+07     # +-HH
```

RFC 2822 strings:
```bash
6 Mar 17 21:22 UT
6 Mar 17 21:22:23 UT
6 Mar 2017 21:22:23 GMT
06 Mar 2017 21:22:23 Z
Mon 06 Mar 2017 21:22:23 z
Mon, 06 Mar 2017 21:22:23 +0000
```

### String + Format

If the string format is known, that can be used to parse the moment:
```javascript
moment('12-25-1995', 'MM-DD-YYYY');
```

Year, month, and day tokens:

| Input    | Example       | Description                                            |
|:---------|:--------------|:-------------------------------------------------------|
| YYYY     | 2014          | 4 or 2 digit year                                      |
| YY       | 14            | 2 digit year                                           |
| Y        | -25           | Year with any number of digits and sign                |
| Q        | 1..4          | Quarter of year. Sets month to first month in quarter. |
| M MM     | 1..12         | Month number                                           |
| MMM MMMM | Jan..December | Month name in locale set by moment.locale()            |
| D DD     | 1..31         | Day of month                                           |
| Do       | 1st..31st     | Day of month with ordinal                              |
| DDD DDDD | 1..365        | Day of year                                            |

Hour, minute, second, tokens:

| Input    | Example        | Description                                                                 |
|:---------|:---------------|:----------------------------------------------------------------------------|
| X        | 1410715640.579 | Unix timestamp                                                              |
| x        | 1410715640579  | Unix ms timestamp                                                           |
| H HH     | 0..23          | Hours (24 hour time)                                                        |
| h hh     | 1..12          | Hours (12 hour time used with a A.)                                         |
| k kk     | 1..24          | Hours (24 hour time from 1 to 24)                                           |
| a A      | am pm          | Postor ante meridiem (Note the one character a p are also considered valid) |
| m mm     | 0..59          | Minutes                                                                     |
| s ss     | 0..59          | Seconds                                                                     |
| S SS SSS | 0..999         | Fractional seconds                                                          |
| Z ZZ     | +12:00         | Offset from UTC as +-HH:mm, +-HHmm, or Z                                    |

Locale aware formats:

| Input | Example                            | Description                                       |
|:------|:-----------------------------------|:--------------------------------------------------|
| L     | 04/09/1986                         | Date (in local format)                            |
| LL    | September 4 1986                   | Month name, day of month, year                    |
| LLL   | September 4 1986 8:30 PM           | Month name, day of month, year, time              |
| LLLL  | Thursday, September 4 1986 8:30 PM | Day of week, month name, day of month, year, time |
| LT    | 08:30 PM                           | Time (without seconds)                            |
| LTS   | 08:30:00 PM                        | Time (with seconds)                               |

### Validation

Check the validity of a date with the `isValid()` method
```javascript
moment('It is 2012-05-25', 'YYYY-MM-DD').isValid();       // true
moment('It is 2012-05-25', 'YYYY-MM-DD', true).isValid(); // false
moment('2012-05-25',       'YYYY-MM-DD', true).isValid(); // true
moment('2012.05.25',       'YYYY-MM-DD', true).isValid(); // false
```

### Object

Can create a momeent by specifying units in an object;
```javascript
moment({ years:2010, months:3, date:5, hours:15, minutes:10, seconds:3, milliseconds:123});
moment({ years:'2010', months:'3', date:'5', hours:'15', minutes:'10', seconds:'3', milliseconds:'123'});
```

### Unix Timestamp (milliseconds)

Can create a moment by passing an integeger representing milliseconds since UnixEpoch.
```javascript
let day = moment(1318781876406);
```

### Unix Timestamp (seconds)

Can create a moment from seconds since Unix Epoch.
```javascript
let day = moment.unix(1318781876.721);
```

### Date

Can create moment from pre-existing Javascript `Date` object.
```javascript
let day = new Date(2011, 9, 16);
let dayWrapper = moment(day);
```

### Clone

Can create a moment from another moment.
```javascript
let day = moment.unix(1318781876.721);
let dupDay = moment(day);
let dupDupDay = dupDay.clone();
```

### UTC

By default, moment parses and displays in local time. If you want to parse or display a moment in UTC,
use `moment.utc()` instead of `moment()`. While in UTC mode, all display methods will display in UTC time
instead of local time.

```javascript
moment().format();     // 2013-02-04T10:35:24-08:00
moment.utc().format(); // 2013-02-04T18:35:24+00:00
```

### Creation Data

After a moment object is created, all of the inputs can be accessed with `creationData()` method:
```javascript
moment("2013-01-02", "YYYY-MM-DD", true).creationData() === {
    input: "2013-01-02",
    format: "YYYY-MM-DD",
    locale: Locale obj,
    isUTC: false,
    strict: true
}
```

### Defaults

You can create a moment object specifying only some of the units, and the rest will be defaulted
to the current day, month or year, or 0 for hours, minutes, seconds, and milliseoncs.
```javascript
moment();  // current date and time
moment(5, "HH");  // today, 5:00:00.000
moment({hour: 5});  // today, 5:00:00.000
moment({hour: 5, minute: 10});  // today, 5:10.00.000
moment({hour: 5, minute: 10, seconds: 20});  // today, 5:10.20.000
moment({hour: 5, minute: 10, seconds: 20, milliseconds: 300});  // today, 5:10.20.300
moment(5, "DD");  // this month, 5th day-of-month
moment("4 05:06:07", "DD hh:mm:ss");  // this month, 4th day-of-month, 05:06:07.000
moment(3, "MM");  // this year, 3rd month (March)
moment("Apr 4 05:06:07", "MMM DD hh:mm:ss");  // this year, 4th April, 05:06:07.000
```

## Get and Set

Moment.js uses overloaded getters and setters. Calling these methods without parameters acts as a
getter, and calling them with a parameter acts as a setter.
```javascript
moment().seconds(30) // set
moment().seconds()   // get
```

Getters and setters for different times
```javascript
// Millisecond
moment().millisecond(Number);
moment().millisecond(); // Number

// Second
moment().second(Number);
moment().second(); // Number

// Minute
moment().minute(Number);
moment().minute(); // Number

// Hour
moment().hour(Number);
moment().hour(); // Number

// Date of Month
moment().date(Number);
moment().date(); // Number

// Day of week
moment().day(Number|String);
moment().day(); // Number

// Day of year
moment().dayOfYear(Number);
moment().dayOfYear(); // Number

// Week of year
moment().week(Number);
moment().week(); // Number

// Month
moment().month(Number|String);
moment().month(); // Number

// Quarter
moment().quarter(); // Number
moment().quarter(Number);

// Year
moment().year(Number);
moment().year(); // Number
```

Get directly:
```javascript
moment().get('year');
moment().get('month');  // 0 to 11
moment().get('date');
moment().get('hour');
moment().get('minute');
moment().get('second');
moment().get('millisecond');
```

Set directly:
```javascript
moment().set('year', 2013);
moment().set('month', 3);  // April
moment().set('date', 1);
moment().set('hour', 13);
moment().set('minute', 20);
moment().set('second', 30);
moment().set('millisecond', 123);
```

### Maximum

Returns the maximum (most distant future) of the given moment instances.
```javascript
let a = moment().subtract(1, 'day');
let b = moment().add(1, 'day');
moment.max(a, b);  // b
```

### Minimum

Returns the minimum (most distant past) of the given moment instances.
```javascript
let a = moment().subtract(1, 'day');
let b = moment().add(1, 'day');
moment.min(a, b);  // a
moment.min([a, b]); // a
```

## Manipulate

Once you have a `Moment` you may want to manipulate it in some way. Moment.js uses method chaining.
All moments are mutable. Calling any of the manipulation methods will change the original moment.
If you want to create a copy and manipulate it, call `moment.clone()` first.

Long chaining expression:
```javascript
moment().add(7, 'days').subtract(1, 'months').year(2009).hours(0).minutes(0).seconds(0);
```

### Add

Mutates the original moment by adding time.
```javascript
moment().add(7, 'days');
moment().add(7, 'd');
```

Keys: years, quarters, months, weeks, days, hours, minutes, seconds, milliseconds, ms

Can also use durations to add moments:
```javascript
let duration = moment.duration({'days' : 1});
moment([2012, 0, 31]).add(duration); // February 1
```

### Subtract

Mutates the original moment by subtracting time.
```javascript
moment().subtract(7, 'days');
moment().subtract(1, 'seconds');
```

### Start of Time

Mutates the original moment by setting it to the start of a unit of time.
```javascript
moment().startOf('year');     // set to January 1st, 12:00 am this year
moment().startOf('month');    // set to the first of this month, 12:00 am
moment().startOf('quarter');  // set to the beginning of the current quarter, 1st day of months, 12:00 am
moment().startOf('week');     // set to the first day of this week, 12:00 am
moment().startOf('isoWeek');  // set to the first day of this week according to ISO 8601, 12:00 am
moment().startOf('day');      // set to 12:00 am today
moment().startOf('date');     // set to 12:00 am today
moment().startOf('hour');     // set to now, but with 0 mins, 0 secs, and 0 ms
moment().startOf('minute');   // set to now, but with 0 seconds and 0 milliseconds
moment().startOf('second');   // same as moment().milliseconds(0);
```

### End of Time

Mutates the original moment by setting it to the end of a unit of time.
```javascript
moment().endOf("year"); // set the moment to 12-31 23:59:59.999 this year
```

### Set to maximum

Limits the moment to a maximum of another moment value.
So `a.max(b)` is the same as `a = moment.min(a,b)`.

### Set to minimum

Limits the moment to a minimum of another moment value.
So `a.min(b)` is the same as `a = moment.max(a, b)`.

### Local

Sets a flag on the original moment to use local time to display a moment instead of the original
moment's time.
```javascript
let a = moment.utc([2011, 0, 1, 8]);
a.hours(); // 8 UTC
a.local();
a.hours(); // 0 PST
```

### UTC

Sets a flag on the original moment to use UTC to display a moment instead of the original moment's time.
```javascript
let a = moment([2011, 0, 1, 8]);
a.hours(); // 8 PST
a.utc();
a.hours(); // 16 UTC
```

### UTC Offset

Gets the UTC offset in minutes
```javascript
moment().utcOffset(); // -240
```

Set the UTC offset by supplying minutes. The offset is set on the moment object that `utcOffset()`
is called on. If you are wanting to set the offset globally use moment-timezone.

## Display

Once parsing and manipulation are done, you need some way to display the moment.

### Format

`format()` takes a string of tokens and replace tokens with corresponding moment values.
```javascript
moment().format();                                // "2014-09-08T08:02:17-05:00" (ISO 8601, no fractional seconds)
moment().format("dddd, MMMM Do YYYY, h:mm:ss a"); // "Sunday, February 14th 2010, 3:25:50 pm"
moment().format("ddd, hA");                       // "Sun, 3PM"
moment('gibberish').format('YYYY MM DD');         // "Invalid date"
```

Format table:

|                            | Token              | Output                                  |
|:---------------------------|:-------------------|:----------------------------------------|
| Month                      | M                  | 1 2 ... 11 12                           |
|                            | Mo                 | 1st 2nd ... 11th 12th                   |
|                            | MM                 | 01 02 ... 11 12                         |
|                            | MMM                | Jan Feb ... Nov Dec                     |
|                            | MMMM               | January February ... November December  |
| Quarter                    | Q                  | 1 2 3 4                                 |
|                            | Qo                 | 1st 2nd 3rd 4th                         |
| Day of Month               | D                  | 1 2 ... 30 31                           |
|                            | Do                 | 1st 2nd ... 30th 31st                   |
|                            | DD                 | 01 02 ... 30 31                         |
| Day of Year                | DDD                | 1 2 ... 364 365                         |
|                            | DDDo               | 1st 2nd ... 364th 365th                 |
|                            | DDDD               | 001 002 ... 364 365                     |
| Day of Week                | d                  | 0 1 ... 5 6                             |
|                            | do                 | 0th 1st ... 5th 6th                     |
|                            | dd                 | Su Mo ... Fr Sa                         |
|                            | ddd                | Sun Mon ... Fri Sat                     |
|                            | dddd               | Sunday Monday ... Friday Saturday       |
| Day of Week (Locale)       | e                  | 0 1 ... 5 6                             |
| Day of Week (ISO)          | E                  | 1 2 ... 6 7                             |
| Week of Year               | w                  | 1 2 ... 52 53                           |
|                            | wo                 | 1st 2nd ... 52nd 53rd                   |
|                            | ww                 | 01 02 ... 52 53                         |
| Week of Year (ISO)         | W                  | 1 2 ... 52 53                           |
|                            | Wo                 | 1st 2nd ... 52nd 53rd                   |
|                            | WW                 | 01 02 ... 52 53                         |
| Year                       | YY                 | 70 71 ... 29 30                         |
|                            | YYYY               | 1970 1971 ... 2029 2030                 |
|                            | Y                  | 1970 1971 ... 9999 +10000 +10001        |
|                            |                    |                                         |
| Week Year                  | gg                 | 70 71 ... 29 30                         |
|                            | gggg               | 1970 1971 ... 2029 2030                 |
| Week Year (ISO)            | GG                 | 70 71 ... 29 30                         |
|                            | GGGG               | 1970 1971 ... 2029 2030                 |
| AM/PM                      | A                  | AM PM                                   |
|                            | a                  | am pm                                   |
| Hour                       | H                  | 0 1 ... 22 23                           |
|                            | HH                 | 00 01 ... 22 23                         |
|                            | h                  | 1 2 ... 11 12                           |
|                            | hh                 | 01 02 ... 11 12                         |
|                            | k                  | 1 2 ... 23 24                           |
|                            | kk                 | 01 02 ... 23 24                         |
| Minute                     | m                  | 0 1 ... 58 59                           |
|                            | mm                 | 00 01 ... 58 59                         |
| Second                     | s                  | 0 1 ... 58 59                           |
|                            | ss                 | 00 01 ... 58 59                         |
| Fractional Second          | S                  | 0 1 ... 8 9                             |
|                            | SS                 | 00 01 ... 98 99                         |
|                            | SSS                | 000 001 ... 998 999                     |
|                            | SSSS ... SSSSSSSSS | 000[0..] 001[0..] ... 998[0..] 999[0..] |
| Time Zone                  | z or zz            | EST CST ... MST PST                     |
|                            |                    |                                         |
|                            | Z                  | -07:00 -06:00 ... +06:00 +07:00         |
|                            | ZZ                 | -0700 -0600 ... +0600 +0700             |
| Unix Timestamp             | X                  | 1360013296                              |
| Unix Millisecond Timestamp | x                  | 1360013296123                           |

Localized formats:

|                                       Description | Code | Example                             |
|--------------------------------------------------:|:-----|:------------------------------------|
|                                 Time with seconds | LTS  | 8:30:25 PM                          |
|                 Month numeral, day of month, year | L    | 09/04/1986                          |
|                                                   | l    | 9/4/1986                            |
|                    Month name, day of month, year | LL   | September 4, 1986                   |
|                                                   | ll   | Sep 4, 1986                         |
|              Month name, day of month, year, time | LLL  | September 4, 1986 8:30 PM           |
|                                                   | lll  | Sep 4, 1986 8:30 PM                 |
| Month name, day of month, day of week, year, time | LLLL | Thursday, September 4, 1986 8:30 PM |
|                                                   | llll | Thu, Sep 4, 1986 8:30 PM            |

To escape characters in format strings, wrap the characters in square brackets:
```javascript
moment().format('[today] dddd'); // 'today Sunday'
```

Default format is ISO8601: `YYYY-MM-DDTHH:mm:ssZ`
This is controled by `moment.defaultFormat` or `moment.defaultFormatUtc`.

### Time from now

Display difference in time from now. This is useful for past times.
```javascript
moment([2007, 0, 29]).fromNow(); // 4 years ago
```

### Time from X

Display a moment in relation to an arbitrary time.
```javascript
let a = moment([2007, 0, 28]);
let b = moment([2007, 0, 29]);
a.from(b) // "a day ago"
```

### Time to now

Display difference in time from now. This is useful for future times.
```javascript
moment([2007, 0, 29]).toNow(); // in 4 years
```

### Time to X

Display a moment in relation to an arbitrary time.
```javascript
let a = moment([2007, 0, 28]);
let b = moment([2007, 0, 29]);
a.to(b);                     // "in a day"
```

### Calendar Time

Calendar time displays time relative to a given `referenceTime` (defaults to now).
```javascript
moment().calendar(null, {
    sameDay: '[Today]',
    nextDay: '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay: '[Yesterday]',
    lastWeek: '[Last] dddd',
    sameElse: 'DD/MM/YYYY'
});
```

### Difference

To get the difference in milliseconds:
```javascript
let a = moment([2007, 0, 29]);
let b = moment([2007, 0, 28]);
a.diff(b) // 86400000
```

To get difference in another unit of measurement:
```javascript
var a = moment([2007, 0, 29]);
var b = moment([2007, 0, 28]);
a.diff(b, 'days') // 1
```

### Unix Timestamp (milliseconds)

Output the moment's milliseconds since Unix Epoch:
```javascript
moment(1318874398806).valueOf(); // 1318874398806
+moment(1318874398806); // 1318874398806
```

### Unix Timestamp (seconds)

Output the moment's seconds since Unix Epoch:
```javascript
moment(1318874398806).unix(); // 1318874398
```

### As ISO 8601 String

Format to ISO8601 standard:
```javascript
moment().toISOString() // 2013-02-04T22:44:30.652Z
```

### As object

Return object containing date information.
```javascript
moment().toObject()  // {
                     //     years: 2015
                     //     months: 6
                     //     date: 26,
                     //     hours: 1,
                     //     minutes: 53,
                     //     seconds: 14,
                     //     milliseconds: 600
                     // }
```

### As String

Returns an english string similar to JS Date's `.toString()`.
```javascript
moment().toString() // "Sat Apr 30 2016 16:59:46 GMT-0500"
```

### Inspect

Returns a machine readblae string, that can be evaluated to produce the same moment.
```javascript
moment().inspect()                                          // 'moment("2016-11-09T22:23:27.861")'
moment.utc().inspect()                                      // 'moment.utc("2016-11-10T06:24:10.638+00:00")'
moment.parseZone('2016-11-10T06:24:12.958+05:00').inspect() // 'moment.parseZone("2016-11-10T06:24:12.958+05:00")'
moment(new Date('nope')).inspect()                          // 'moment.invalid(/* Invalid Date */)'
moment('blah', 'YYYY').inspect()                            // 'moment.invalid(/* blah */)
```

## Query

### Is Before

Check if a moment is before another moment.
```javascript
moment('2010-10-20').isBefore('2010-10-21'); // true
```

### Is Same

Check if a moment is the same as another moment. Can specify the granularity in the check.
```javascript
moment('2010-10-20').isSame('2010-10-20'); // true
moment('2010-10-20').isSame('2009-12-31', 'year');  // false
moment('2010-10-20').isSame('2010-01-01', 'year');  // true
moment('2010-10-20').isSame('2010-12-31', 'year');  // true
moment('2010-10-20').isSame('2011-01-01', 'year');  // false
```

Note: `moment().isSame()` has undefined behavior depending on how fast the code runs.

### Is After

Check if a moment is after another moment.
```javascript
moment('2010-10-20').isAfter('2010-10-19'); // true
moment('2010-10-20').isAfter('2010-01-01', 'year'); // false
moment('2010-10-20').isAfter('2009-12-31', 'year'); // true
```

### Is Same or After

```javascript
moment('2010-10-20').isSameOrBefore('2010-10-21');  // true
moment('2010-10-20').isSameOrBefore('2010-10-20');  // true
moment('2010-10-20').isSameOrBefore('2010-10-19');  // false
moment('2010-10-20').isSameOrBefore('2009-12-31', 'year'); // false
moment('2010-10-20').isSameOrBefore('2010-12-31', 'year'); // true
moment('2010-10-20').isSameOrBefore('2011-01-01', 'year'); // true
```

### Is Same or After

```javascript
moment('2010-10-20').isSameOrAfter('2010-10-19'); // true
moment('2010-10-20').isSameOrAfter('2010-10-20'); // true
moment('2010-10-20').isSameOrAfter('2010-10-21'); // false
moment('2010-10-20').isSameOrAfter('2011-12-31', 'year'); // false
moment('2010-10-20').isSameOrAfter('2010-01-01', 'year'); // true
moment('2010-10-20').isSameOrAfter('2009-12-31', 'year'); // true
```

### Is Between

Check if a moment is between two other moments.
```javascript
moment('2010-10-20').isBetween('2010-10-19', '2010-10-25'); // true
moment('2010-10-20').isBetween('2010-10-19', undefined); moment('2010-10-20').isBetween('2010-01-01', '2012-01-01', 'year'); // false
moment('2010-10-20').isBetween('2009-12-31', '2012-01-01', 'year'); // true
```

### Is Daylight Saving Time

Check if the current moment is in daylight saving time.
```javascript
moment([2011, 2, 12]).isDST(); // false, March 12 2011 is not DST
moment([2011, 2, 14]).isDST(); // true, March 14 2011 is DST
```

### Is DSF Shifted

Another important piece of validation is to know if the date has been moved by a DST.
```javascript
moment('2013-03-10 2:30', 'YYYY-MM-DD HH:mm').format(); //=> '2013-03-10T01:30:00-05:00'

```

### Is Leap Year

Returns `true` if that year is a leap year, `false` otherwise:
```javascript
moment([2000]).isLeapYear() // true
moment([2001]).isLeapYear() // false
moment([2100]).isLeapYear() // false
```

### Is a Momement

Check if a variable is a moment object

```javascript
moment.isMoment()           // false
moment.isMoment(new Date()) // false
moment.isMoment(moment())   // true
```

### Is a Date

Check if a variable is a native js Date object.
```javascript
moment.isDate();           // false
moment.isDate(new Date()); // true
moment.isDate(moment());   // false
```

## Internationalization (il8n)

Moment.js has robust support for internationalization. You can load multiple locales and easily
switch between them. Can assign locale at the global level or to a specific moment.

### Changing Locale Globally

Load a locale:
```javascript
moment.locale('fr', {
    months : 'janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre'.split('_'),
    monthsShort : 'janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.'.split('_'),
    monthsParseExact : true,
    weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
    weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
    weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
    weekdaysParseExact : true,
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay : '[Aujourd’hui à] LT',
        nextDay : '[Demain à] LT',
        nextWeek : 'dddd [à] LT',
        lastDay : '[Hier à] LT',
        lastWeek : 'dddd [dernier à] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : 'dans %s',
        past : 'il y a %s',
        s : 'quelques secondes',
        m : 'une minute',
        mm : '%d minutes',
        h : 'une heure',
        hh : '%d heures',
        d : 'un jour',
        dd : '%d jours',
        M : 'un mois',
        MM : '%d mois',
        y : 'un an',
        yy : '%d ans'
    },
    dayOfMonthOrdinalParse : /\d{1,2}(er|e)/,
    ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'e');
    },
    meridiemParse : /PD|MD/,
    isPM : function (input) {
        return input.charAt(0) === 'M';
    },
    // In case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example).
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
    // },
    meridiem : function (hours, minutes, isLower) {
        return hours < 12 ? 'PD' : 'MD';
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // Used to determine first week of the year.
    }
});
```

Once you load a locale, it becomes the active locale. To change active locales, simply call
`moment.locale` with the key of a loaded locale.
```javascript
moment.locale('fr');
moment(1316116057189).fromNow(); // il y a une heure
moment.locale('en');
moment(1316116057189).fromNow(); // an hour ago
```

Changing the global locale doesn't affect existing instances:
```javascript
moment.locale('fr');
var m = moment(1316116057189);
m.fromNow(); // il y a une heure

moment.locale('en');
m.fromNow(); // il y a une heure
moment(1316116057189).fromNow(); // an hour ago
```

### Changing Locale Locally

A global locale configuration can be problematic when passing around moments that may need to be
formatted into different locale.
```javascript
moment.locale('en');          // default the locale to English
var localLocale = moment();

localLocale.locale('fr');     // set this instance to use French
localLocale.format('LLLL');   // dimanche 15 juillet 2012 11:01
moment().format('LLLL');      // Sunday, July 15 2012 11:01 AM

moment.locale('es');          // change the global locale to Spanish
localLocale.format('LLLL');   // dimanche 15 juillet 2012 11:01
moment().format('LLLL');      // Domingo 15 Julio 2012 11:01

localLocale.locale(false);    // reset the instance locale
localLocale.format('LLLL');   // Domingo 15 Julio 2012 11:01
moment().format('LLLL');      // Domingo 15 Julio 2012 11:01
```

### Loading Locale in NodeJS

Load a locale as follows:
```javascript
var moment = require('moment');
moment.locale('fr');
moment(1316116057189).fromNow(); // il y a une heure
```

### Loading Locale in the browser

Load a locale as follows:
```html
<script src="moment.js"></script>
<script src="locale/fr.js" charset="UTF-8"></script>
<script src="locale/pt.js" charset="UTF-8"></script>
<script>
  moment.locale('fr');  // Set the default/global locale
  // ...
</script>
```

### Get and set locale

```javascript
moment.locale('en'); // set to english
moment.locale(); // returns 'en'
```

### Locale Specific Functionality

Access properties of the currently loaded locale through `moment.localeData(key)`.

```javascript
// get current locale
var currentLocaleData = moment.localeData();
var frLocaleData = moment.localeData('fr');
```

The following methods are available:
```javascript
localeData.months(aMoment);                    // full month name of aMoment
localeData.monthsShort(aMoment);               // short month name of aMoment
localeData.monthsParse(longOrShortMonthString);// returns month id (0 to 11) of input
localeData.weekdays(aMoment);                  // full weekday name of aMoment
localeData.weekdaysShort(aMoment);             // short weekday name of aMoment
localeData.weekdaysMin(aMoment);               // min weekday name of aMoment
localeData.weekdaysParse(minShortOrLongWeekdayString);  // returns weekday id (0 to 6) of input
localeData.longDateFormat(dateFormat);         // returns the full format of abbreviated date-time
                                               // formats LT, L, LL and so on
localeData.isPM(amPmString);                   // returns true iff amPmString represents PM
localeData.meridiem(hours, minutes, isLower);  // returns am/pm string for particular time-of-day in upper/lower case
localeData.calendar(key, aMoment);             // returns a format that would be used for calendar
                                               // representation. Key is one of 'sameDay', 'nextDay',
                                               // 'lastDay', 'nextWeek', 'prevWeek', 'sameElse'
localeData.relativeTime(number, withoutSuffix, key, isFuture);  // returns relative time string, key is on of 's', 'm', 'mm', 'h', 'hh', 'd', 'dd', 'M', 'MM', 'y', 'yy'. Single letter when number is 1.
localeData.pastFuture(diff, relTime);          // convert relTime string to past or future string depending on diff
localeData.ordinal(number);                    // convert number to ordinal string 1 -> 1st
localeData.preparse(str);                      // called before parsing on every input string
localeData.postformat(str);                    // called after formatting on every string
localeData.week(aMoment);                      // returns week-of-year of aMoment
localeData.invalidDate();                      // returns a translation of 'Invalid date'
localeData.firstDayOfWeek();                   // 0-6 (Sunday to Saturday)
localeData.firstDayOfYear();                   // 0-15 Used to determine first week of the year.
```

### Pseudo Locale

Pseudo locales can be useful when testing, as they make obvious what data has and has not been
localized. Just include the pseudo-locale, and set moment's locale to x-pseudo. Text from Moment
will be very easy to spot.

```javascript
moment.locale('x-pseudo');
moment().format('LLL'); //14 F~ébrú~árý 2010 15:25
moment().fromNow(); //'á ~féw ~sécó~ñds á~gó'
moment().calendar(); //'T~ódá~ý át 02:00'
```

## Customize

Moment.js is very easy to customize. In general, you should create a locale setting with your
customizations.
```javascript
moment.locale('en-my-settings', {
    // customizations.
});
```

Can remove a previously defined locale by passing `null` as the second argument. The deleted
locale will no longer be available for use.
```javascript
moment.locale('fr'); // 'fr'
moment.locale('en'); // 'en'
moment.locale('fr', null);
moment.locale('fr'); // 'en'
```

It is possible to create a locale that inherits from a parent locale.
```javascript
moment.defineLocale('en-foo', {
  parentLocale: 'en',
  /* */
});
```

To update an existing locale:
```javascript
moment.updateLocale('en', {
  /**/
});
```

To revert update changes:
```javascript
moment.updateLocale('en', null);
```

## Durations

Where a moment moment is defined as a single point in dime, a duration is defined as a length
of time.

Durations do not have a defined beginning and end date. They are contextless. As such, they are
not a good solution to converting between units that depend on context. For example, a year
can be defined as 366 days, 365 days, 365.25 days, 12 months, or 52 weeks. Trying to convert
years to days makes no sense without context. Use `moment.diff` instead.

### Creating

Create a duration as follows:
```javascript
moment.duration(100); // 100 milliseconds
moment.duration(2, 'seconds');
moment.duration(2, 'minutes');
moment.duration(2, 'hours');
moment.duration(2, 'days');
moment.duration(2, 'weeks');
moment.duration(2, 'months');
moment.duration(2, 'years');
```

Can pass an object of values if you need multiple different units of measurement.
```javascript
moment.duration({
    seconds: 2,
    minutes: 2,
    hours: 2,
    days: 2,
    weeks: 2,
    months: 2,
    years: 2
});
```

### Clone

Durations are mutable (they can be changed), just like moment objects. To make a copy of an existing
duration do the following:
```javascript
var d1 = moment.duration();
var d2 = d1.clone();
d1.add(1, 'second');
d1.asMilliseconds() !== d2.asMilliseconds();
```

### Humanize

Sometimes, you want all the goodness of `moment.from` but you don't want to have to create two
moments, you just want to display a length of time.

Quickly a display a duration as follows:
```javascript
moment.duration(1, "minutes").humanize(); // a minute
moment.duration(2, "minutes").humanize(); // 2 minutes
moment.duration(24, "hours").humanize();  // a day
```

### Getters

Durations support the following types of getters:
- `moment.duration(2349).<timescale>()`: returns number of timescale within range
- `moment.duration(2349).as<timescale>()`: returns total number of timescale

```javascript
//milliseconds returns between 0 and 999
moment.duration(15000).milliseconds(); // 0
moment.duration(15000).asMilliseconds(); // 15000

//seconds returns between 0 and 59
moment.duration(500).seconds(); // 0
moment.duration(15000).seconds(); // 15
moment.duration(500).asSeconds(); // 0.5
moment.duration(15000).asSeconds(); // 15

//minutes returns between 0 and 59
moment.duration().minutes();
moment.duration().asMinutes();

//hours returns between 0 and 23
moment.duration().hours();
moment.duration().asHours();

//days returns between 0 and 30
moment.duration().days();
moment.duration().asDays();

//weeks returns between 0 and 0
moment.duration().weeks();
moment.duration().asWeeks();

//months returns between 0 and 11
moment.duration().months();
moment.duration().asMonths();

//years is not bounded
moment.duration().years();
moment.duration().asYears();
```

### As Unit of Time

Get duration as a unit of time (alternative to `duration.as<timescale>()`).
```javascript
duration.as('hours');
duration.as('minutes');
duration.as('seconds');
duration.as('milliseconds');
```

### Get unit of time

Get duration unit of time (alternative to `duration.<timescale>()`):
```javascript
duration.get('hours');
duration.get('minutes');
duration.get('seconds');
duration.get('milliseconds');
```

### Add Time

Add durrations as follows:
```javascript
var a = moment.duration(1, 'd');
var b = moment.duration(2, 'd');
a.add(b).days(); // 3
```

### Subtract Time

Subtract duractions as follows:
```javascript
var a = moment.duration(3, 'd');
var b = moment.duration(2, 'd');
a.subtract(b).days(); // 1
```

### Using Duration with Diff

Can get the duration between two moments by using `moment.diff`:
```javascript
var x = new moment()
var y = new moment()
var duration = moment.duration(x.diff(y))
// returns duration object with the duration between x and y
```

### Locale

Get or set duration locale as follows:
```javascript
moment.duration(1, "minutes").locale("en").humanize(); // a minute
moment.duration(1, "minutes").locale("fr").humanize(); // une minute
moment.duration(1, "minutes").locale("es").humanize(); // un minuto
```

## Utilities

Moment exposes some methods which may be useful to people extending the library or writing custom
parsers.

### Normalize Units

Many of Moment's functions allow the caller to pass in aliases for unit enums. For example,
all of the gets below are equivalent.
```javascript
var m = moment();
m.get('y');
m.get('year');
m.get('years');
```

When extending the library, you may want access to Moment's facilities for that in order to better
align your functionality with Moment's.
```javascript
moment.normalizeUnits('y');      // 'year'
moment.normalizeUnits('Y');      // 'year'
moment.normalizeUnits('year');   // 'year'
moment.normalizeUnits('years');  // 'year'
moment.normalizeUnits('YeARS');  // 'year'
```

### Invalid

Create your own invalid Moment objects, which is useful in making your own parser.
```javascript
var m = moment.invalid();
m.isValid();                      // false
m.format();                       // 'Invalid date'
m.parsingFlags().userInvalidated; // true
```