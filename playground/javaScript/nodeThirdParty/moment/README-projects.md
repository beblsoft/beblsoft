# Moment.js Samples Documentation

## parse.js

Demonstrate parsing different inputs to create a moment object.

To run:
- Install: `npm install`
- Run: `node parse.js`

## getSet.js

Demonstrate getting and setting moment attributes.

To run:
- Install: `npm install`
- Run: `node getSet.js`

## manipulate.js

Demonstrate manipulating moments.

To run:
- Install: `npm install`
- Run: `node manipulate.js`

## display.js

Demonstrate displaying moments with different formats.

To run:
- Install: `npm install`
- Run: `node display.js`

## query.js

Demonstrate querying different times.

To run:
- Install: `npm install`
- Run: `node query.js`

## duration.js

Demonstrate duration functionality.

To run:
- Install: `npm install`
- Run: `node duration.js`

