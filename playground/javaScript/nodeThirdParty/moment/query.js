/**
 * @file Moment Query Examples
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let moment = require('moment');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Query Examples
 */
function query() {
  logTitle({ title: 'Query' });
  let a = moment();
  let b = moment().add(1, 'days');
  let c = moment().subtract(1, 'days');

  console.log(`a                   : ${a.format('YYYY-MM-DD')}`);
  console.log(`b                   : ${b.format('YYYY-MM-DD')}`);
  console.log(`c                   : ${c.format('YYYY-MM-DD')}`);
  console.log(`a.isSame(a)         : ${a.isSame(a)}`);
  console.log(`a.isSame(b)         : ${a.isSame(b)}`);
  console.log(`a.isSame(c)         : ${a.isSame(c)}`);
  console.log(`a.isBefore(b)       : ${a.isBefore(b)}`);
  console.log(`a.isBefore(c)       : ${a.isBefore(c)}`);
  console.log(`a.isSameOrBefore(b) : ${a.isSameOrBefore(b)}`);
  console.log(`a.isSameOrBefore(c) : ${a.isSameOrBefore(c)}`);
  console.log(`a.isAfter(b)        : ${a.isAfter(b)}`);
  console.log(`a.isAfter(c)        : ${a.isAfter(c)}`);
  console.log(`a.isSameOrAfter(b)  : ${a.isSameOrAfter(b)}`);
  console.log(`a.isSameOrAfter(c)  : ${a.isSameOrAfter(c)}`);
  console.log(`a.isBetween(b, c)   : ${a.isBetween(b, c)}`);
  console.log(`a.isBetween(c, b)   : ${a.isBetween(c, b)}`);
  console.log(`a.isDST()           : ${a.isDST()}`);
  console.log(`a.isLeapYear()      : ${a.isLeapYear()}`);
  console.log(`moment.isMoment(a)  : ${moment.isMoment(a)}`);
  console.log(`moment.isDate(a)    : ${moment.isDate(a)}`);
}


/* ------------------------ MAIN ------------------------------------------- */
query({ all: true });
