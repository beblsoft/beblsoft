# Glob Technology Documentation

In computer programming, glob patterns specify sets of filenames with wildcard characters.
For example, the Unix Bash Shell command `mv *.txt textFiles/` moves all files with names
ending in `.txt` to `textFiles/`.

Glob is a _glob_ implementation in JavaScript. It uses `minmatch` library to do its matching.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Glob_(programming)),
[NPM](https://www.npmjs.com/package/glob)

## Installation

```bash
npm install glob
```

## Basic Usage

```javascript
let glob = require("glob")

// Sync Usage
fileList = glob.sync("**/*.js")

// Async Usage
glob("**/*.js", function (er, fileList) {
  // fileList is an array of filenames.
  // If the `nonull` option is set, and nothing was found, then fileList is ["**/*.js"]
  // er is an error object or null.
})
```

## Glob Syntax

### Braces

Before parsing the path part patterns, braced sections are expanded into a set. Braced sections
start with `{` and end with `}`.

For example `a{/b/c,dcd}` would expand to `a/b/c` and `abcd`

### Special Characters

The following characters have special meaning when used in a path portion

| Character  | Meaning                                                                                                                                                                 |
|:-----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| *          | Matches 0 or more characters in a single path portion                                                                                                                   |
| ?          | Matches 1 character                                                                                                                                                     |
| [...]      | Matches a range of characters, similar to a RegExp range. If the first character of the range is ! or ^ then it matches any character not in the range.                 |
| !(pattern) | Matches anything that does not match any of the patterns provided.                                                                                                      |
| ?(pattern) | Matches zero or one occurrence  the patterns provided.                                                                                                                  |
| +(pattern) | Matches one or more occurrencesf the patterns provided.                                                                                                                 |
| *(a b c)   | Matches zero or more occurrences of the patterns provided                                                                                                               |
| @(pattern) | Matches exactly one of the patterns provided                                                                                                                            |
| **         | If a "globstar" is alone in a path portion, then it matches zero or more directories and subdirectories searching for matches. It does not crawl symlinked directories. |

### Dots

If a file or directory path portion has a `.` as the first character, then it will not match any
glob pattern unless that pattern's corresponding path part also has a `.` as its first character.

You can make glob treat does as normal characters by setting `dot:true` in the options

### Basename Matching

If you set `matchBase:true` in the options, and the pattern has no slashes in it, then it will
seek for any file anywhere in the tree with a matching basename. For example, *.js* would match
`test/simple/basic.js`

### Empty Sets

If no matching files are found, then an empty array is returned.

## Usage

### Async

```javascript
let options = {};
glob("**/*.js", function (er, options, fileList) {
    console.log(fileList);
});
```

### Sync

```javascript
let options = {};
let fileList = glob.sync("**/*.js", options);
console.log(fileList);
```

### Options

- `cwd`: The current working directory in which to search. Defaults to `process.cwd()`
- `root`: Place where patterns starting with `/` will be mounted onto. Defaults to
  `path.reslove(options.cmd, "/")`
- `dot`: Include `.dot` files in normal matches
- `mark`: Add a `/` character to directory matches.
- `nosort`: Don't sort the results
- `stat`: Set to true to stat all results. Only necessesary if readdir is presumed to be an
  untrustworth indicator of file existence.
- `silent`: Set to true to supress warnings.
- `strict`: Set to true to raise error if directory cannot be read.
- `cache`: Pass in previously generated cache object to save fs calls.
- `nounique`: In some cases, brace-expanded patterns can result in the same file showing up
  multiple times in the result set. By default, this implementation prevents duplicates in the
  result set. Set this flag to disable that behavior.
- `nonnull`: Set to never return an empty set
- `debug`: Set to enable debug logging in minimatch and glob.
- `nobrace`: Do not expand `{a,b}` and `{1..3}` brace sets
- `noglobstar`: Do not match `**` against multiple filenames
- `noext`: Do not match `(a|b)` "extglob" patterns
- `nocase`: Perform a case-insenistive match
- `matchBase`: Perform a basename-only match if the pattern does not contain any characters.
  That is `*.js` would be treated as equivalent to `**/*.js`
- `nodir`: Do not match directories
- `ignore`: Add a pattern or an array of glob patterns to exclude matches
- `follow`: Follow symlinked directories when expanding `**` patterns. Note that this can result
  in a lot of duplicate references in the presence of cyclic links
- `realpath`: Set to true to call `fs.realpath` on all the results. In the case of a symlink that
  cannot be resolved, the full absolute path to the matched entry is returned
- `absolute`: Set to true to always receive absolute paths for matched files


