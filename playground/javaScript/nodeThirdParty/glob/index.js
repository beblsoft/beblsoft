/**
 * @file Glob Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let glob = require('glob');
let path = require('path');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Glob examples
 */
function globFiles({
  basic = false,
  all = false
} = {}) {
  if (basic || all) {
    logTitle({ title: 'basic' });
    let options = { cwd: path.resolve(__dirname), absolute: true, ignore: [] };
    let fileList = glob.sync('**/*.js', options);
    console.log(fileList);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
globFiles({ all: true });
