/**
 * @file dummy module
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * @param  {number} num
 * @return {number}
 */
function third(num) {
  return num * 3;
}

/**
 * @param  {number} num
 * @return {number}
 */
function second(num) {
  return third(num * 2);
}

/**
 * @param  {number} num
 * @return {number}
 */
function first(num) {
  return second(num);
}

/**
 * Module main routine
 */
function run() {
  let start = 1;
  let end = first(start);
  console.log(`Start:${start} End:${end}`);
}

/* ------------------------ EXPORTS ---------------------------------------- */
/**
 * @module module A
 */
module.exports = {
  run
};
