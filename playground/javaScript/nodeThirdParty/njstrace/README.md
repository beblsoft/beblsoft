# NJStrace Technology Documentation

__njstrace__ lets you easily instrument and trace you code, see all function calls, arguments, return
values, as well as the time spent in each function.

Relevant URLs: [NPM](https://www.npmjs.com/package/njstrace)