/**
 * @file njstrace basic example
 */


/* ------------------------ OBJECTS ---------------------------------------- */
/**
 * Console Formatter
 * @type {Object}
 */
let consoleFormatter = {
  // {boolean|string|function}
  // Controls where the output should go
  stdout: true,

  // {string}, Default '  '
  // Character used for indentation of call stack
  indentationChar: '   ',

  // {number}, Default = 5
  // Number of arguments to inspec
  inspectArgsCount: 0,

  // {number}, Default = 500
  // Maximum number of characters to print for each argument
  // 0 = unlimited
  inspectArgsMaxLen: 100,

  // {object}, Default = null
  // Object to pass to util.inspect
  inspectOptions: { colors: true }
};

/**
 * File formatter
 * @type {Object}
 */
let fileFormatter = {
  stdout: 'trace.out',
  inspectArgsMaxLen: 0,
  indentationChar: '\t'
};

/**
 * Inject Config Object
 * @type {Object}
 */
let injectConfig = {
  // {boolean}, Default = true
  // Whether tracing is active
  enabled: true,

  // {String | string[]}, Default excludes all node_modules
  // A glob file pattern that matches the files to instrument
  // Any pattern supported by minimatch npm module
  // All relative to working directory
  files: ['**/*.js', '!**/node_modules/**'],

  // {boolean}, Default = true
  // Whether njstrace should wrap the instrumented functions in a try/catch block
  // Gives better tracing for uncaught exceptions
  wrapFunctions: true,

  // {boolean|string|function}, Default = false
  // Controls where the instrumentation logs should go (not tracing)
  // If boolean: true, log to console
  // If string: a path to an output file
  // If function: a custom log function that gets a {string} argument
  logger: false,

  // {boolean}, Default = true
  // Whether to inspect arguments or not
  inspectArgs: true,

  // {Formatter|object | (Formatter|object)[]}
  formatter: [consoleFormatter, fileFormatter]
};


/* ------------------------ IMPORTS ---------------------------------------- */
let njstrace = require('njstrace').inject(injectConfig);
let modA = require('./moduleA.js');


/* ------------------------ MAIN ------------------------------------------- */
modA.run();
