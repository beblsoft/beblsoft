# NJSTrace Projects Documentation

## basic.js

Basic usage with two different formatters. One to console and another to trace.out

To Run:
- Install Software : `npm install`
- Run program      : `npm run basic`

## customFormatter.js

Use a custom formatter to format output lines

To Run:
- Install Software   : `npm install`
- Run program        : `npm run customFormatter`
