/**
 * @file njstrace custom formatter example
 */


/* ------------------------ OBJECTS ---------------------------------------- */
// Get a reference to njstrace default Formatter class
let Formatter = require('njstrace/lib/formatter.js');


/* ------------------------------------------------------------------------- */
/**
 * Custom formatter class
 * @class
 */
function MyFormatter() {
  // No need to call Formatter ctor here
}
require('util').inherits(MyFormatter, Formatter);

/**
 * Implement onEntry method
 * @param  {Object} args
 * @param  {[type]} args.name {string} - The traced function name
 * @param  {[type]} args.file {string} - The traced file
 * @param  {[type]} args.line {number} - The traced function line number
 * @param  {[type]} args.args {object} - The function arguments object
 * @param  {[type]} args.stack {Tracer.CallStack} - The current call stack including the current traced function (see Tracer.CallStack below)
 */
MyFormatter.prototype.onEntry = function(args) {
  console.log('Got call to %s@%s::%s, num of args: %s, stack location: %s',
    args.name, args.file, args.line, args.args.length, args.stack.length);
};

/**
 * Implement onExit method
 * @param  {Object} args
 * @param  {[type]} args.name {string} - The traced function name

 * @param  {[type]} args.file {string} - The traced file
 * @param  {[type]} args.line {number} - The traced function line number
 * @param  {[type]} args.retLine {number} - The line number where the exit is (can be either a return statement of function end)
 * @param  {[type]} args.stack {Tracer.CallStack} - The current call stack AFTER popping the current traced function (see Tracer.CallStack below)
 * @param  {[type]} args.span {number} - The execution time span (milliseconds) of the traced function
 * @param  {[type]} args.exception {boolean} - Whether this exit is due to exception
 * @param  {[type]} args.returnValue {*|null} - The function return value
 */
MyFormatter.prototype.onExit = function(args) {
  console.log('Exit from %s@%s::%s, had exception: %s, exit line: %s, execution time: %s, has return value: %s',
    args.name, args.file, args.line, args.exception, args.retLine, args.span, args.returnValue !== null);
};


/* ------------------------ IMPORTS ---------------------------------------- */
let njstrace = require('njstrace').inject({ formatter: new MyFormatter() });
let modA = require('./moduleA.js');


/* ------------------------ MAIN ------------------------------------------- */
modA.run();
