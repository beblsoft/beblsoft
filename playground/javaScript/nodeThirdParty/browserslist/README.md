# Browserslist Documentation

Browserslist is the config to share target browsers and Node.js versions between different front-end
tools. It is used in tools like: Autoprefixer, Babel, and postcss-present-env

All tools will find target browsers automatically, when you add the following to `package.json`:

```json
"browserslist": [
  "last 1 version",
  "> 1%",
  "maintained node versions",
  "not dead"
]
```

Developers set versions list in queries like `last 2 versions` to be free from updating versions
manually. Browserslist will use [Can I Use](https://caniuse.com/) data for this queries.

Relevant URLs:
[GitHub](https://github.com/browserslist/browserslist),
[Can I Use](https://caniuse.com/),
[GitHub Example](https://github.com/browserslist/browserslist-example),
[Query Executor](https://browserl.ist/)

## Tools

- `browserslist-ga` and `browserslist-ga-export` download your website browsers statistics to
  use it in `> 0.5% in my stats` query.

- `browserslist-useragent-regexp` compiles Browserslist query to a RegExp to test browser useragent.

- `browserslist-useragent-ruby` is a Ruby library to checks browser by user agent string to match
  Browserslist.

- `browserslist-browserstack` runs BrowserStack tests for all browsers in Browserslist config.

- `caniuse-api` returns browsers which support some specific feature.

- Run `npx browserslist` in your project directory to see project’s target browsers. This CLI tool
  is built-in and available in any project with Autoprefixer.

## Best Practices

- Select browsers directly (`last 2 Chrome versions`) only if you are making a web app for a kiosk
  with one browser. There are a lot of browsers on the market. If you are making a general web app
  you should respect browsers diversity.

- There is a `defaults` query, which gives a reasonable configuration for must users.
  ```json
  "browserslist": [
    "defaults"
  ]
  ```

- If you want to change the default set of browsers we recommend to combine `last 1 version`,
  `not dead` with `> 0.2%`. `last n versions` adds too many dead browsers and does not add popular
  old versions. Choosing a percentage above `0.2%` will in the long run make popular browsers even
  more popular

- Don't remove browsers just because you don't know them. Opera Mini has 100 million users in Africa
  and it is more popular in the global market than Microsoft Edge. Chinese QQ Browsers has more market
  share than Firefox and desktop Safari combined.

## Queries

Browserslist will use browsers and Node.js versions query from one of these sources:

- `browserslist` key in package.json file in current or parent directories. __We recommend this way__.
- `.browserslistrc` config file in current or parent directories.
- `browserslist` config file in current or parent directories.
- `BROWSERSLIST` environment variable.
- If the above methods did not produce a valid result Browserslist will use defaults:
  `> 0.5%, last 2 versions, Firefox ESR, not dead`.

### Composition

- `or` combiner (Union): Ex. `last 1 version or > 1%`, `last 1 version, > 1%`

- `and` combiner (Intersection): Ex. `last 1 version and > 1%`

- `not` combiner (Relative Complement): Ex. `> .5% and not last 2 versions`

### Full List

You can specify the browser and Node.js versions by queries (case insensitive):

- `> 5%`: browsers versions selected by global usage statistics. >=, < and <= work too.
- `> 5% in US`: uses USA usage statistics. It accepts two-letter country code.
- `> 5% in alt-AS`: uses Asia region usage statistics. List of all region codes can be found at caniuse-lite/data/regions.
- `> 5% in my stats`: uses custom usage data.
- `cover 99.5%`: most popular browsers that provide coverage.
- `cover 99.5% in US`: same as above, with two-letter country code.
- `cover 99.5% in my stats`: uses custom usage data.
- `maintained node versions`: all Node.js versions, which are still maintained by Node.js Foundation.
- `node 10 and node 10.4`: selects latest Node.js 10.x.x or 10.4.x release.
- `current node`: Node.js version used by Browserslist right now.
- `extends browserslist-config-mycompany`: take queries from `browserslist-config-mycompany` npm package.
- `ie 6-8`: selects an inclusive range of versions.
- `Firefox > 20`: versions of Firefox newer than 20. >=, < and <= work too. It also works with Node.js.
- `iOS 7`: the iOS browser version 7 directly.
- `Firefox ESR`: the latest [Firefox ESR] version.
- `PhantomJS 2.1 and PhantomJS 1.9`: selects Safari versions similar to PhantomJS runtime.
- `unreleased versions or unreleased Chrome versions`: alpha and beta versions.
- `last 2 major versions or last 2 iOS major versions`: all minor/patch releases of last 2 major versions.
- `since 2015 or last 2 years`: all versions released since year 2015 (also since 2015-03 and since 2015-03-10).
- `dead`: browsers without official support or updates for 24 months. Right now it is IE 10, IE_Mob 10, BlackBerry 10, BlackBerry 7, Samsung 4 and OperaMobile 12.1.
- `last 2 versions`: the last 2 versions for each browser.
- `last 2 Chrome versions`: the last 2 versions of Chrome browser.
- `defaults`: Browserslist’s default browsers (> 0.5%, last 2 versions, Firefox ESR, not dead).
- `not ie <= 8`: exclude browsers selected by previous queries.

You can add not to any query.

### Debug

Run `npx browserslist` in project directory to see what browsers was selected by your queries.

### Browsers

Names are case insensitive:

- `Android` for Android WebView.
- `Baidu` for Baidu Browser.
- `BlackBerry` or `bb` for Blackberry browser.
- `Chrome` for Google Chrome.
- `ChromeAndroid` or `and_chr` for Chrome for Android
- `Edge` for Microsoft Edge.
- `Electron` for Electron framework. It will be converted to Chrome version.
- `Explorer` or `ie` for Internet Explorer.
- `ExplorerMobile` or `ie_mob` for Internet Explorer Mobile.
- `Firefox` or `ff` for Mozilla Firefox.
- `FirefoxAndroid` or `and_ff` for Firefox for Android.
- `iOS` or `ios_saf` for iOS Safari.
- `Node` for Node.js.
- `Opera` for Opera.
- `OperaMini` or op_mini for Opera Mini.
- `OperaMobile` or op_mob for Opera Mobile.
- `QQAndroid` or `and_qq` for QQ Browser for Android.
- `Safari` for desktop Safari.
- `Samsung` for Samsung Internet.
- `UCAndroid` or `and_uc` for UC Browser for Android.
- `kaios` for KaiOS Browser.

## Config File

### package.json

If you want to reduce config files in project root, you can specify browsers in `package.json` with
`browserslist` key:

```json
{
  "private": true,
  "dependencies": {
    "autoprefixer": "^6.5.4"
  },
  "browserslist": [
    "last 1 version",
    "> 1%",
    "IE 10"
  ]
}
```

### .browserslistrc

Separated Browserslist config should be named `.browserslistrc` and have browsers queries split by
a now line. Comments starts with `#` symbol

```
# Browsers that we support

last 1 version
> 1%
IE 10 # sorry
```

Browserslist will check config in every directory in `path`. So, if tool prcesses `app/styles/main.css`
you can put config to root `app/` or `app/styles`.

You can specify direct path in `BROWSERSLIST_CONFIG` environment variables.

### Shareable Configs

You can use the following query to reference an exported Browserslist config from another package.

```json
  "browserslist": [
    "extends browserslist-config-mycompany"
  ]
```

## Configuring Different Environments

You can also specify different browser queries for various environments. Browserslist will choose
query according to `BROWSERSLIST_ENV` or `NODE_ENV` variables. If none of them is declared, Browserslist
will firstly look for `production` queries and then use defaults

In `package.json`:

```json
  "browserslist": {
    "production": [
      "> 1%",
      "ie 10"
    ],
    "modern": [
      "last 1 chrome version",
      "last 1 firefox version"
    ],
    "ssr": [
      "node 12"
    ]
  }
```

## Custom Usage Data

If you have a website, you can query against the usage statistic of your site. `browserslist-ga` will
ask access to Google analytics and then generate `browserslist-stats.json`:

```bash
npx browserslist-ga
```

File format should look like:
```json
{
  "ie": {
    "6": 0.01,
    "7": 0.4,
    "8": 1.5
  },
  "chrome": {
    …
  },
  …
}
```

## JS API

Access the browserslist JavaScript API as follows:

```js
const browserslist = require('browserslist')

// Your CSS/JS build tool code
function process (source, opts) {
  const browsers = browserslist(opts.overrideBrowserslist, {
    stats: opts.stats,
    path:  opts.file,
    env:   opts.env
  })
  // Your code to add features for selected browsers
}
```

Options:
- `path`: file or a directory path to look for config file. Default is ..
- `env`: what environment section use from config. Default is production.
- `stats`: custom usage statistics data.
- `config`: path to config if you want to set it manually.
- `ignoreUnknownVersions`: do not throw on direct query (like ie 12). Default is false.
- `dangerousExtend`: Disable security checks for extend query. Default is false.
- `mobileToDesktop`: Use desktop browsers if Can I Use doesn’t have data about this mobile version.
  For instance, Browserslist will return `chrome 20` on `and_chr 20` query (Can I Use has only data
  only about latest versions of mobile browsers). Default is `false`.

Show coverage:
```js
browserslist.coverage(browserslist('> 1%'))
//=> 81.4

browserslist.coverage(browserslist('> 1% in US'), 'US')
//=> 83.1

browserslist.coverage(browserslist('> 1% in my stats'), 'my stats')
//=> 83.1

browserslist.coverage(browserslist('> 1% in my stats', { stats }), stats)
//=> 82.2
```

## CLI

Show coverage:

```bash
$ browserslist --coverage "> 1%"
These browsers account for 81.4% of all users globally

$ browserslist --coverage=US "> 1% in US"
These browsers account for 83.1% of all users in the US

$ browserslist --coverage "> 1% in my stats"
These browsers account for 83.1% of all users in custom statistics

$ browserslist --coverage "> 1% in my stats" --stats=./stats.json
These browsers account for 83.1% of all users in custom statistics
```

## Environment Variables

If some tools use Browserslist inside, you can change browsers settings by environment variables:

- `BROWSERSLIST` with browsers queries.

   ```bash
   BROWSERSLIST="> 5%" gulp css
   ```

- `BROWSERSLIST_CONFIG` with path to config file.

   ```bash
   BROWSERSLIST_CONFIG=./config/browserslist gulp css
   ```

- `BROWSERSLIST_ENV` with environments string.

   ```bash
   BROWSERSLIST_ENV="development" gulp css
   ```

- `BROWSERSLIST_STATS` with path to the custom usage data for > 1% in my stats query.

   ```bash
   BROWSERSLIST_STATS=./config/usage_data.json gulp css
   ```

- `BROWSERSLIST_DISABLE_CACHE` if you want to disable config reading cache.

  ```bash
  BROWSERSLIST_DISABLE_CACHE=1 gulp css
  ```

## Cache

Browserlist caches the configuration it reads from `package.json` and `browserslist` files, as well
as knowledge about the existence of files, for the duration of the hosting process.

To clear these caches, use:

```js
browserslist.clearCaches()
```
