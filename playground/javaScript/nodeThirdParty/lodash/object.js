/**
 * @file Lodash Object Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Object examples
 */
function object({
  at = false,
  forIn = false,
  get = false,
  has = false,
  invert = false,
  merge = false,
  set = false,
  all = false
} = {}) {
  if (at || all) {
    logTitle({ title: 'at' });
    let o = { 'a': [{ 'b': { 'c': 3 } }, 4] }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.at(o, ['a[0].b.c', 'a[1]']))}`);
  }
  if (forIn || all) {
    logTitle({ title: 'forIn' });
    function Foo() { /* eslint-disable-line */
      this.a = 1;
      this.b = 2;
    }
    Foo.prototype.c = 3;
    _.forIn(new Foo, function (value, key) { /* eslint-disable-line */
      console.log(`${JSON.stringify(key)}`);
    });
  }
  if (get || all) {
    logTitle({ title: 'get' });
    let o = { 'a': [{ 'b': { 'c': 3 } }, 4] }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.get(o, 'a[0].b.c'))}`);
  }
  if (has || all) {
    logTitle({ title: 'has' });
    let o = { 'a': [{ 'b': { 'c': 3 } }, 4] }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.has(o, 'a[0].b.c'))}`);
    console.log(`${JSON.stringify(_.has(o, 'a[0].b.c.d'))}`);
  }
  if (invert || all) {
    logTitle({ title: 'invert' });
    let o = { 'a': 1, 'b': 2, 'c': 1 }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.invert(o))}`);
  }
  if (merge || all) {
    logTitle({ title: 'merge' });
    let o = { 'a': 1, 'b': 2, 'c': 1 }; /* eslint-disable-line */
    let q = { 'c': 1, 'd': 2, 'e': 1 }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.merge(o, q))}`);
  }
  if (set || all) {
    logTitle({ title: 'set' });
    let o = { 'a': [{ 'b': { 'c': 3 } }, 4] }; /* eslint-disable-line */
    console.log(`${JSON.stringify(_.set(o, 'a[0].b.c', 4))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
object({ all: true });
