/**
 * @file Lodash Function Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Function examples
 */
function func({
  partial = false,
  wrap = false,
  all = false
} = {}) {
  if (partial || all) {
    logTitle({ title: 'partial' });
    let greet = (greeting, name) => { return `${greeting} ${name}`; };
    let sayHelloTo = _.partial(greet, 'Hello');
    let greetFred = _.partial(greet, _, 'Fred');

    console.log(`${JSON.stringify(sayHelloTo('Fred'))}`);
    console.log(`${JSON.stringify(greetFred('Hi'))}`);
  }
  if (wrap || all) {
    logTitle({ title: 'wrap' });
    let p = _.wrap(_.escape, function(func, text) {
    	return `<p> ${func(text)} </p>`;
    })
    console.log(`${JSON.stringify(p('fred, barney, & pebbles'))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
func({ all: true });
