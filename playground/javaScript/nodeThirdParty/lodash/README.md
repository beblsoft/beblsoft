# Lodash Technology Documentation

Lodash is a JavaScript library which provides utility functions for common programming tasks using
the functional programming paradigm.

It can be broken down into the following areas:
- Utilities: Simplifying common programming
- Function: binding, decorating, constraining, throttling, debouncing, currying, and changing the pointer
- String: trimming, converting cases
- Array: creating, splitting, combining, modifying, and compressing
- Collection: iterating, sorting, filtering, splitting, and building
- Object: accessing, extending, merging, defaults, and transforming
- Seq: chaining, wrapping, filtering, and testing

Relevant URLs:
[Home](https://lodash.com/),
[Documentation](https://lodash.com/docs/4.17.11),
[Wikipedia](https://en.wikipedia.org/wiki/Lodash),
[GitHub](https://github.com/lodash/lodash)
