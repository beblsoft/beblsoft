/**
 * @file Lodash String Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * String examples
 */
function string({
  camelCase = false,
  deburr = false,
  escape = false,
  kebabCase = false,
  lowerCase = false,
  pad = false,
  repeat = false,
  replace = false,
  snakeCase = false,
  split = false,
  trim = false,
  truncate = false,
  unescape = false,
  words = false,
  all = false
} = {}) {
  if (camelCase || all) {
    logTitle({ title: 'camelCase' });
    console.log(`${JSON.stringify(_.camelCase('Foo Bar'))}`);
    console.log(`${JSON.stringify(_.camelCase('--foo-bar--'))}`);
    console.log(`${JSON.stringify(_.camelCase('__FOO_BAR__'))}`);
  }
  if (deburr || all) {
    logTitle({ title: 'deburr' });
    console.log(`${JSON.stringify(_.camelCase('déjà vu'))}`);
  }
  if (escape || all) {
    logTitle({ title: 'escape' });
    console.log(`${JSON.stringify(_.escape('fred, barney, & pebbles'))}`);
  }
  if (kebabCase || all) {
    logTitle({ title: 'kebabCase' });
    console.log(`${JSON.stringify(_.kebabCase('Foo Bar'))}`);
    console.log(`${JSON.stringify(_.kebabCase('--foo-bar--'))}`);
    console.log(`${JSON.stringify(_.kebabCase('__FOO_BAR__'))}`);
  }
  if (lowerCase || all) {
    logTitle({ title: 'lowerCase' });
    console.log(`${JSON.stringify(_.lowerCase('Foo Bar'))}`);
    console.log(`${JSON.stringify(_.lowerCase('--foo-bar--'))}`);
    console.log(`${JSON.stringify(_.lowerCase('__FOO_BAR__'))}`);
  }
  if (pad || all) {
    logTitle({ title: 'pad' });
    console.log(`${JSON.stringify(_.pad('abc', 8))}`);
    console.log(`${JSON.stringify(_.pad('abc', 10, '_-'))}`);
    console.log(`${JSON.stringify(_.pad('abc', 3))}`);
  }
  if (repeat || all) {
    logTitle({ title: 'repeat' });
    console.log(`${JSON.stringify(_.repeat('*', 8))}`);
    console.log(`${JSON.stringify(_.repeat('abc', 2))}`);
    console.log(`${JSON.stringify(_.repeat('abc', 0))}`);
  }
  if (replace || all) {
    logTitle({ title: 'replace' });
    console.log(`${JSON.stringify(_.replace('Hi Fred', 'Fred', 'Barney'))}`);
  }
  if (snakeCase || all) {
    logTitle({ title: 'snakeCase' });
    console.log(`${JSON.stringify(_.snakeCase('Foo Bar'))}`);
    console.log(`${JSON.stringify(_.snakeCase('--foo-bar--'))}`);
    console.log(`${JSON.stringify(_.snakeCase('__FOO_BAR__'))}`);
  }
  if (split || all) {
    logTitle({ title: 'split' });
    console.log(`${JSON.stringify(_.split('a-b-c', '-', 2))}`);
  }
  if (trim || all) {
    logTitle({ title: 'trim' });
    console.log(`${JSON.stringify(_.trim('  abc  '))}`);
    console.log(`${JSON.stringify(_.trim('-_-abc-_-', '_-'))}`);
  }
  if (truncate || all) {
    logTitle({ title: 'truncate' });
    console.log(`${JSON.stringify(_.truncate('hi-diddly-ho there, neighborino', { 'length': 24, 'separator': ' ', 'omission': ' [...]'}))}`);
  }
  if (unescape || all) {
    logTitle({ title: 'unescape' });
    console.log(`${JSON.stringify(_.unescape('fred, barney, &amp; pebbles'))}`);
  }
  if (words || all) {
    logTitle({ title: 'words' });
    console.log(`${JSON.stringify(_.words('fred, barney, & pebbles'))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
string({ all: true });
