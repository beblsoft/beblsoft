# Lodash Projects Documentation

## array.js

Demonstrate lodash array functions.

To run:
- Install: `npm install`
- Run: `node array.js`

## collection.js

Demonstrate lodash collection functions.

To run:
- Install: `npm install`
- Run: `node collection.js`

## date.js

Demonstrate lodash date functions.

To run:
- Install: `npm install`
- Run: `node date.js`

## function.js

Demonstrate lodash function functions.

To run:
- Install: `npm install`
- Run: `node function.js`

## lang.js

Demonstrate lodash language functions.

To run:
- Install: `npm install`
- Run: `node lang.js`

## math.js

Demonstrate lodash math functions.

To run:
- Install: `npm install`
- Run: `node math.js`

## number.js

Demonstrate lodash number functions.

To run:
- Install: `npm install`
- Run: `node number.js`

## object.js

Demonstrate lodash object functions.

To run:
- Install: `npm install`
- Run: `node object.js`

## string.js

Demonstrate lodash string functions.

To run:
- Install: `npm install`
- Run: `node string.js`

## util.js

Demonstrate lodash utility functions.

To run:
- Install: `npm install`
- Run: `node util.js`
