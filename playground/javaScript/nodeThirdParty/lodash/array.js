/**
 * @file Lodash Array Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Array examples
 */
function array({
  chunk = false,
  compact = false,
  concat = false,
  uniqBy = false,
  drop = false,
  all = false
} = {}) {
  if (chunk || all) {
    logTitle({ title: 'chunk' });
    console.log(`${JSON.stringify(_.chunk(['a', 'b', 'c', 'd'], 2))}`);
    console.log(`${JSON.stringify(_.chunk(['a', 'b', 'c', 'd'], 3))}`);
  }
  if (compact || all) {
    logTitle({ title: 'compact' });
    console.log(`${JSON.stringify(_.compact([0, 1, false, 2, '', 3]))}`);
  }
  if (concat || all) {
    logTitle({ title: 'concat' });
    console.log(`${JSON.stringify(_.concat([1], 2, [3], [[4]]))}`);
  }
  if (uniqBy || all) {
    logTitle({ title: 'uniqBy' });
    console.log(`${JSON.stringify(_.uniqBy([{ 'x': 2 }, { 'x': 1 }, { 'x': 1 }], 'x'))}`);
  }
  if (drop || all) {
    logTitle({ title: 'drop' });
    console.log(`${JSON.stringify(_.drop([1, 2, 3]))}`);
    console.log(`${JSON.stringify(_.drop([1, 2, 3], 2))}`);
    console.log(`${JSON.stringify(_.drop([1, 2, 3], 5))}`);
    console.log(`${JSON.stringify(_.drop([1, 2, 3], 0))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
array({ all: true });
