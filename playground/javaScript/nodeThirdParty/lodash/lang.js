/**
 * @file Lodash Lang Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Lang examples
 */
function lang({
  cloneDeep = false,
  conformsTo = false,
  isBoolean = false,
  isEmpty = false,
  all = false
} = {}) {
  if (cloneDeep || all) {
    logTitle({ title: 'cloneDeep' });
    let objects = [{ 'a': 1 }, { 'b': 2 }];
    let deep = _.cloneDeep(objects);
    console.log(`${JSON.stringify(deep[0] === objects[0])}`);
  }
  if (conformsTo || all) {
    logTitle({ title: 'conformsTo' });
    let object = { 'a': 1, 'b': 2 };
    console.log(`${JSON.stringify(_.conformsTo(object, { 'b': function (n) { return n > 1; } }))}`);
    console.log(`${JSON.stringify(_.conformsTo(object, { 'b': function (n) { return n > 2; } }))}`);
  }
  if (isBoolean || all) {
    logTitle({ title: 'isBoolean' });
    console.log(`${JSON.stringify(_.isBoolean(false))}`);
    console.log(`${JSON.stringify(_.isBoolean(null))}`);
  }
  if (isEmpty || all) {
    logTitle({ title: 'isEmpty' });
    console.log(`${JSON.stringify(_.isEmpty(null))}`);
    console.log(`${JSON.stringify(_.isEmpty(true))}`);
    console.log(`${JSON.stringify(_.isEmpty(1))}`);
    console.log(`${JSON.stringify(_.isEmpty([1, 2, 3]))}`);
    console.log(`${JSON.stringify(_.isEmpty({'a': 1}))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
lang({ all: true });
