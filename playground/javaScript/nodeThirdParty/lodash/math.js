/**
 * @file Lodash Math Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Math examples
 */
function math({
  add = false,
  max = false,
  mean = false,
  round = false,
  sum = false,
  all = false
} = {}) {
  if (add || all) {
    logTitle({ title: 'add' });
    console.log(`${JSON.stringify(_.add(4, 6))}`);
  }
  if (max || all) {
    logTitle({ title: 'max' });
    console.log(`${JSON.stringify(_.max([4, 6]))}`);
  }
  if (mean || all) {
    logTitle({ title: 'mean' });
    console.log(`${JSON.stringify(_.mean([4, 6, 9, 10, 11, 12]))}`);
  }
  if (round || all) {
    logTitle({ title: 'round' });
    console.log(`${JSON.stringify(_.round(4.006))}`);
    console.log(`${JSON.stringify(_.round(4.006, 2))}`);
    console.log(`${JSON.stringify(_.round(41006, -2))}`);
  }
  if (sum || all) {
    logTitle({ title: 'sum' });
    console.log(`${JSON.stringify(_.sum([4, 6, 9, 10, 11, 12]))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
math({ all: true });
