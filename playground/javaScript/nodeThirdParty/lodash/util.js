/**
 * @file Lodash Util Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Util examples
 */
function util({
  defaultTo = false,
  identity = false,
  matches = false,
  noop = false,
  nthArg = false,
  range = false,
  times = false,
  toPath = false,
  uniqueId = false,
  all = false
} = {}) {
  if (defaultTo || all) {
    logTitle({ title: 'defaultTo' });
    console.log(`${JSON.stringify(_.defaultTo(1, 10))}`);
    console.log(`${JSON.stringify(_.defaultTo(undefined, 10))}`);
    console.log(`${JSON.stringify(_.defaultTo(null, 10))}`);
    console.log(`${JSON.stringify(_.defaultTo(NaN, 10))}`);
  }
  if (matches || all) {
    logTitle({ title: 'matches' });
    let oList = [
      { 'a': 1, 'b': 2, 'c': 3 },
      { 'a': 4, 'b': 5, 'c': 6 }
    ];
    console.log(`${JSON.stringify(_.filter(oList, _.matches({ 'a': 4, 'c': 6 })))}`);
  }
  if (noop || all) {
    logTitle({ title: 'noop' });
    console.log(`${JSON.stringify(_.times(2, _.noop))}`);
  }
  if (nthArg || all) {
    logTitle({ title: 'nthArg' });
    let func = _.nthArg(1);
    console.log(`${JSON.stringify(func('a', 'b', 'c', 'd'))}`);
  }
  if (range || all) {
    logTitle({ title: 'range' });
    console.log(`${JSON.stringify(_.range(4))}`);
    console.log(`${JSON.stringify(_.range(-4))}`);
    console.log(`${JSON.stringify(_.range(1, 5))}`);
    console.log(`${JSON.stringify(_.range(0, 20, 5))}`);
    console.log(`${JSON.stringify(_.range(0, -4, -1))}`);
  }
  if (times || all) {
    logTitle({ title: 'times' });
    console.log(`${JSON.stringify(_.times(4, String))}`);
    console.log(`${JSON.stringify(_.times(4, _.constant(0)))}`);
  }
  if (toPath || all) {
    logTitle({ title: 'toPath' });
    console.log(`${JSON.stringify(_.toPath('a.b.c'))}`);
    console.log(`${JSON.stringify(_.toPath('a[0].b.c'))}`);
  }
  if (uniqueId || all) {
    logTitle({ title: 'uniqueId' });
    console.log(`${JSON.stringify(_.uniqueId())}`);
    console.log(`${JSON.stringify(_.uniqueId('csontact_'))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
util({ all: true });
