/**
 * @file Lodash Number Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Number examples
 */
function number({
  clamp = false,
  inRange = false,
  random = false,
  all = false
} = {}) {
  if (clamp || all) {
    logTitle({ title: 'clamp' });
    console.log(`${JSON.stringify(_.clamp(-10, -5, 5))}`);
    console.log(`${JSON.stringify(_.clamp(10, -5, 5))}`);
  }
  if (inRange || all) {
    logTitle({ title: 'inRange' });
    console.log(`${JSON.stringify(_.inRange(3, 2, 4))}`);
    console.log(`${JSON.stringify(_.inRange(4, 8))}`);
    console.log(`${JSON.stringify(_.inRange(4, 2))}`);
    console.log(`${JSON.stringify(_.inRange(-3, -2, -6))}`);
  }
  if (random || all) {
    logTitle({ title: 'random' });
    console.log(`${JSON.stringify(_.random(0, 5))}`);
    console.log(`${JSON.stringify(_.random(5))}`);
    console.log(`${JSON.stringify(_.random(5, true))}`);
    console.log(`${JSON.stringify(_.random(1.2, 5.2))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
number({ all: true });
