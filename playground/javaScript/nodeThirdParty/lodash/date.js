/**
 * @file Lodash Date Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Date examples
 */
function date({
  now = false,
  all = false
} = {}) {
  if (now || all) {
    logTitle({ title: 'now' });
    console.log(`${JSON.stringify(_.now())}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
date({ all: true });
