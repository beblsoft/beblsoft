/**
 * @file Lodash Collection Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let _ = require('lodash');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Collection examples
 */
function collection({
  countBy = false,
  every = false,
  forEach = false,
  filter = false,
  groupBy = false,
  map = false,
  reduce = false,
  sample = false,
  size = false,
  sortBy = false,
  all = false
} = {}) {
  if (countBy || all) {
    logTitle({ title: 'countBy' });
    console.log(`${JSON.stringify(_.countBy([6.1, 4.2, 6.3], Math.floor))}`);
  }
  if (every || all) {
    logTitle({ title: 'every' });
    console.log(`${JSON.stringify(_.every([true, 1, null, 'yes'], Boolean))}`);
  }
  if (forEach || all) {
    logTitle({ title: 'forEach' });
    _.forEach([1, 2], function (value) {
      console.log(value);
    });
  }
  if (filter || all) {
    logTitle({ title: 'filter' });
    let users = [
      { 'user': 'barney', 'age': 36, 'active': true },
      { 'user': 'fred', 'age': 40, 'active': false }
    ];
    let filter = (o) => { return !o.active; };
    console.log(`${JSON.stringify(_.filter(users, filter))}`);
  }
  if (groupBy || all) {
    logTitle({ title: 'groupBy' });
    console.log(`${JSON.stringify(_.groupBy([6.1, 4.2, 6.3], Math.floor))}`);
  }
  if (map || all) {
    logTitle({ title: 'map' });
    let square = (n) => { return n * n; };
    console.log(`${JSON.stringify(_.map([4, 8], square))}`);
  }
  if (reduce || all) {
    logTitle({ title: 'reduce' });
    let accumulate = (sum, n) => { return sum + n; };
    console.log(`${JSON.stringify(_.reduce([1, 2, 3, 4, 5, 6, 7, 8], accumulate))}`);
  }
  if (sample || all) {
    logTitle({ title: 'sample' });
    console.log(`${JSON.stringify(_.sample([1, 2, 3, 4]))}`);
  }
  if (size || all) {
    logTitle({ title: 'size' });
    console.log(`${JSON.stringify(_.size([1, 2, 3, 4]))}`);
  }
  if (sortBy || all) {
    logTitle({ title: 'sortBy' });
    let reverse = (n) => { return -1 * n; };
    console.log(`${JSON.stringify(_.sortBy([1, 2, 3, 4]))}`);
    console.log(`${JSON.stringify(_.sortBy([1, 2, 3, 4], reverse))}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
collection({ all: true });
