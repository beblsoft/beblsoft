/**
 * @file chai assert examples
 *
 * Bibliography - https://www.chaijs.com/api/assert/
 *
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;


/* ------------------------ ASSERT ----------------------------------------- */
describe('Assert', function () {

  it('assert', function () {
    assert('foo' !== 'bar', 'foo is not bar');
    assert(Array.isArray([]), 'empty arrays are arrays');
  });

  it('fail', function () {
    // assert.fail('Will Fail Everytime');
  });

  it('isOk', function () {
    assert.isOk('everything', 'everything is ok');
    // assert.isOk(false, 'this will fail');
  });

  it('isNotOk', function () {
    // assert.isNotOk('everything', 'this will fail');
    assert.isNotOk(false, 'this will pass');
  });

  it('equal', function () {
    assert.equal(3, '3', '== coerces values to strings');
  });

  it('notEqual', function () {
    assert.notEqual(3, 4, 'these numbers are not equal');
  });

  it('strictEqual', function () {
    assert.strictEqual(true, true, 'these booleans are strictly equal');
  });

  it('notStrictEqual', function () {
    assert.notStrictEqual(3, '3', 'no coercion for strict equality');
  });

  it('deepEqual', function () {
    assert.deepEqual({ tea: 'green' }, { tea: 'green' });
  });

  it('notDeepEqual', function () {
    assert.notDeepEqual({ tea: 'green' }, { tea: 'jasmine' });
  });

  it('isAbove', function () {
    assert.isAbove(5, 2, '5 is strictly greater than 2');
  });

  it('isAtLeast', function () {
    assert.isAtLeast(5, 2, '5 is greater or equal to 2');
    assert.isAtLeast(3, 3, '3 is greater or equal to 3');

  });

  it('isBelow', function () {
    assert.isBelow(3, 6, '3 is strictly less than 6');
  });

  it('isAtMost', function () {
    assert.isAtMost(3, 6, '3 is less than or equal to 6');
    assert.isAtMost(4, 4, '4 is less than or equal to 4');

  });

  it('isTrue', function () {
    let teaServed = true;
    assert.isTrue(teaServed, 'the tea has been served');

  });

  it('isNotTrue', function () {
    let tea = 'tasty chai';
    assert.isNotTrue(tea, 'great, time for tea!');
  });

  it('isFalse', function () {
    let teaServed = false;
    assert.isFalse(teaServed, 'no tea yet? hmm...');

  });

  it('isNotFalse', function () {
    let tea = 'tasty chai';
    assert.isNotFalse(tea, 'great, time for tea!');
  });

  it('isNull', function () {
    let err = null;
    assert.isNull(err, 'there was no error');
  });

  it('isNotNull', function () {
    let tea = 'tasty chai';
    assert.isNotNull(tea, 'great, time for tea!');
  });

  it('exists', function () {
    let foo = 'hi';

    assert.exists(foo, 'foo is neither `null` nor `undefined`');
  });

  it('notExists', function () {
    let bar = null,
      baz; /* eslint-disable-line */

    assert.notExists(bar);
    assert.notExists(baz, 'baz is either null or undefined');
  });

  it('isUndefined', function () {
    let tea; /* eslint-disable-line */
    assert.isUndefined(tea, 'no tea defined');
  });

  it('isDefined', function () {
    let tea = 'cup of chai';
    assert.isDefined(tea, 'tea has been defined');
  });

  it('isFunction', function () {
    function serveTea() { return 'cup of tea'; } /* eslint-disable-line */
    assert.isFunction(serveTea, 'great, we can have tea now');
  });

  it('isObject', function () {
    let selection = { name: 'Chai', serve: 'with spices' };
    assert.isObject(selection, 'tea selection is an object');
  });

  it('isArray', function () {
    let menu = ['green', 'chai', 'oolong'];
    assert.isArray(menu, 'what kind of tea do we want?');
  });

  it('isString', function () {
    let teaOrder = 'chai';
    assert.isString(teaOrder, 'order placed');
  });

  it('isNumber', function () {
    let cups = 2;
    assert.isNumber(cups, 'how many cups');
  });

  it('isBoolean', function () {
    let teaReady = true,
      teaServed = false;

    assert.isBoolean(teaReady, 'is the tea ready');
    assert.isBoolean(teaServed, 'has tea been served');
  });

  it('typeOf', function () {
    assert.typeOf({ tea: 'chai' }, 'object', 'we have an object');
    assert.typeOf(['chai', 'jasmine'], 'array', 'we have an array');
    assert.typeOf('tea', 'string', 'we have a string');
    assert.typeOf(/tea/, 'regexp', 'we have a regular expression');
    assert.typeOf(null, 'null', 'we have a null');
    assert.typeOf(undefined, 'undefined', 'we have an undefined');
  });

  it('notTypeOf', function () {
    assert.notTypeOf('tea', 'number', 'strings are not numbers');
  });

  it('instanceOf', function () {
    function Tea(name) { /* eslint-disable-line */
      this.name = name;
    }
    let chai = new Tea('chai');

    assert.instanceOf(chai, Tea, 'chai is an instance of tea');
  });

  it('include', function () {
    assert.include([1, 2, 3], 2, 'array contains value');
    assert.include('foobar', 'foo', 'string contains substring');
    assert.include({ foo: 'bar', hello: 'universe' }, { foo: 'bar' }, 'object contains property');
  });

  it('deepInclude', function () {
    let obj1 = { a: 1 },
      obj2 = { b: 2 };
    assert.deepInclude([obj1, obj2], { a: 1 });
    assert.deepInclude({ foo: obj1, bar: obj2 }, { foo: { a: 1 } });
    assert.deepInclude({ foo: obj1, bar: obj2 }, { foo: { a: 1 }, bar: { b: 2 } });
  });

  it('match', function () {
    assert.match('foobar', /^foo/, 'regexp matches');
  });

  it('property', function () {
    assert.property({ tea: { green: 'matcha' } }, 'tea');
    assert.property({ tea: { green: 'matcha' } }, 'toString');
  });

  it('propertyVal', function () {
    assert.propertyVal({ tea: 'is good' }, 'tea', 'is good');
  });

  it('lengthOf', function () {
    assert.lengthOf([1, 2, 3], 3, 'array has length of 3');
    assert.lengthOf('foobar', 6, 'string has length of 6');
  });

  it('hasAnyKeys', function () {
    assert.hasAnyKeys({ foo: 1, bar: 2, baz: 3 }, ['foo', 'iDontExist', 'baz']);
    assert.hasAnyKeys({ foo: 1, bar: 2, baz: 3 }, { foo: 30, iDontExist: 99, baz: 1337 });
  });

  it('hasAllKeys', function () {
    assert.hasAllKeys({ foo: 1, bar: 2, baz: 3 }, ['foo', 'bar', 'baz']);
    assert.hasAllKeys({ foo: 1, bar: 2, baz: 3 }, { foo: 30, bar: 99, baz: 1337 });
  });

  it('throws', function () {
    assert.throws(() => { throw new ReferenceError(); }, ReferenceError);
  });

  it('operator', function () {
    assert.operator(1, '<', 2, 'everything is ok');
    // assert.operator(1, '>', 2, 'this will fail');
  });

  it('closeTo', function () {
    assert.closeTo(1.5, 1, 0.5, 'numbers are close');
  });

  it('approximately', function () {
    assert.approximately(1.5, 1, 0.5, 'numbers are close');
  });

  it('oneOf', function () {
    assert.oneOf(1, [2, 1], 'Not found in list');
  });

  it('changes', function () {
    let obj = { val: 10 };
    function fn() { obj.val = 22; } /* eslint-disable-line */
    assert.changes(fn, obj, 'val');
  });

  it('changesBy', function () {
    let obj = { val: 10 };
    function fn() { obj.val += 2; } /* eslint-disable-line */
    assert.changesBy(fn, obj, 'val', 2);
  });

  it('isEmpty', function () {
    assert.isEmpty([]);
    assert.isEmpty('');
    assert.isEmpty(new Map());
    assert.isEmpty({});
  });

});
