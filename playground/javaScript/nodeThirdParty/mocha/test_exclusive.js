/**
 * @file exclusive test examples
 *
 * Only .only tests are run
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ MAIN ------------------------------------------- */
describe('Exclusive', function() {
  let a = 'a';
  let b = 'b';
  let c = 'c';

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe.only('A', function() {
    it('Should have correct length', function() {
      assert.equal(a.length, 1);
    });

    it('Should contain correct data', function() {
      assert.equal(a, 'a');
    });
  });

  describe('B', function() {
    it.only('Should have correct length', function() {
      assert.equal(b.length, 1);
    });

    it('Should contain correct data', function() {
      assert.equal(b, 'b');
    });
  });

  describe('C', function() {
    it('Should have correct length', function() {
      assert.equal(c.length, 1);
    });

    it.only('Should contain correct data', function() {
      assert.equal(c, 'c');
    });
  });

});
