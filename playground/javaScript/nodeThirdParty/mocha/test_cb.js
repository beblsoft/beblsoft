/**
 * @file callback test examples
 */


/* ------------------------ PERSON CLASS ----------------------------------- */
class Person {

  /**
   * @constructs
   * @param  {string} name
   */
  constructor({
    name
  }) {
    this.name = name;
  }

  /**
   * Save Person to Database
   * @param  {int} timeMS - time in milliseconds it takes to save
   * @param  {function} compFunc - Function to be executed on success or failure
   */
  save({
    timeMS = 300,
    compFunc
  }) {
    let promise = new Promise((resolve) => {
      setTimeout(() => {
        let text = `\tSaved user:${this.name}`;
        console.log(text);
        resolve(text);
      }, timeMS);
    });

    promise
      .then(() => {
        compFunc();
      })
      .catch((err) => {
        compFunc(err);
      });
  }
}


/* ------------------------ MAIN ------------------------------------------- */
describe('Callback', function () {


  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe('Save1', function () {
    it('Person should save without error', function (done) {
      let person = new Person({
        name: 'Steve'
      });
      person.save({
        timeMS: 200,
        compFunc: (err) => {
          if (err) {
            done(err);
          } else {
            done();
          }
        }
      });
    });
  });

  describe('Save2', function () {
    it('Person should save without error', function (done) {
      let person = new Person({
        name: 'Jean'
      });
      person.save({
        timeMS: 300,
        compFunc: done
      });
    });
  });
});
