/**
 * @file pending examples
 */


/* ------------------------ MAIN ------------------------------------------- */
describe('Pending', function() {

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe('Foo', function() {
    it('Should be coming soon');
  });

  describe('Bar', function() {
    it('Should be coming next');
  });

});
