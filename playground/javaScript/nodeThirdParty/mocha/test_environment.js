/**
 * @file example accessing environment variables
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ MAIN ------------------------------------------- */
describe('Environment', function() {
  describe('Environment letiable', function() {
    it('Should have correct value', function() {
      assert.equal(process.env.FOO_KEY, 'FOO_VAL');
    });
  });
});
