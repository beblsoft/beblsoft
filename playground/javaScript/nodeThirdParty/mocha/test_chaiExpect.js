/**
 * @file chai examples
 *
 * Expect : Function used as a starting point for chaining language assertions
 *          Works on node.js and all browsers
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let expect = require('chai').expect;
let should = require('chai').should();


/* ------------------------ GLOBALS ---------------------------------------- */
let foo = 'bar';
let beverages = { tea: ['chai', 'matcha', 'oolong'] };


/* ------------------------ EXPECT ----------------------------------------- */
describe('Expect', function () {
  it('Should Expect Corectly', function () {
    expect(foo).to.be.a('string');
    expect(foo).to.equal('bar');
    expect(foo).to.have.lengthOf(3);
    expect(beverages).to.have.property('tea').with.lengthOf(3);
  });
});

