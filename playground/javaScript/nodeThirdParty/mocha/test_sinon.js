/**
 * @file Sinon examples
 *
 * Reference
 *   - Sinon : https://sinonjs.org/
 *   - Fakes : https://sinonjs.org/releases/v7.0.0/fakes/
 *   - Spies : https://sinonjs.org/releases/v7.0.0/spies/
 *   - Stubs : https://sinonjs.org/releases/v7.0.0/stubs/
 *   - Mocks : https://sinonjs.org/releases/v7.0.0/mocks/
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;
let sinon = require('sinon');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */


/* ------------------------ MAIN ------------------------------------------- */
describe('Sinon', function () {

  /* -------------------- HOOKS -------------------------------------------- */
  afterEach(() => {
    sinon.restore(); // Restore the default sandbox here

  });

  /* -------------------- FAKES -------------------------------------------- */
  // FAKE
  // Function that records arguments, return value, the value of this and
  // exception thrown (if any) for all of its calls
  it('Basic fake', function () {
    let fake = sinon.fake();
    fake();
    assert.equal(fake.callCount, 1);
  });
  it('Fake with rval', function () {
    let fake = sinon.fake.returns('apple pie');
    assert.equal(fake(), 'apple pie');
  });

  /* -------------------- SPIES -------------------------------------------- */
  // SPY
  // Function that records arguments, return value, and expections for any call
  // Two Types: 1. anonymous 2. Wrappers over existing methods
  it('Spy function', function () {
    function doWork() {} /* eslint-disable-line */
    let workSpy = sinon.spy(doWork);
    workSpy();
    assert.equal(workSpy.callCount, 1);
  });
  it('Spy a class function', function () {
    class Foo {
      static doWork() {} /* eslint-disable-line */
    }
    let workSpy = sinon.spy(Foo, 'doWork');
    Foo.doWork();
    assert.equal(workSpy.callCount, 1);
    assert(workSpy.called);
    assert(workSpy.calledOnce);
    assert.isFalse(workSpy.threw());
    // console.log(workSpy.firstCall);
    // console.log(workSpy.getCall(1));
    workSpy.restore();
  });

  /* -------------------- STUBS -------------------------------------------- */
  // STUB
  // Spies with pre-programmed behavior
  it('Stub function with args', function () {
    let stub = sinon.stub();
    stub.withArgs(42).returns(1);

    assert.equal(stub(42), 1);
  });
  it('Stub function on call', function () {
    let stub = sinon.stub();
    stub.onCall(0).returns(1);
    stub.onCall(1).returns(26);

    assert.equal(stub(42), 1);
    assert.equal(stub(42), 26);
  });
  it('Stub existing function', function () {
    class Foo {
      static doWork() { return 0;} /* eslint-disable-line */
    }
    let stub = sinon.stub(Foo, 'doWork');
    stub.returns(1);

    assert.equal(Foo.doWork(), 1);
  });

  /* -------------------- MOCKS -------------------------------------------- */
  // MOCKS
  // Fake functions (like spies) with pre-programmed behavior (like stubs) as
  // well as pre-programmed expectations.
  it('Mock api', function() {
    let myAPI = { doWork: function() {} };
    let mock = sinon.mock(myAPI).expects('doWork').once();
    myAPI.doWork();
    mock.verify();
  });
});
