/**
 * @file test promise examples
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');
let chai = require('chai');
let should = chai.should(); // eslint-disable-line no-unused-vars
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


/* ------------------------ DATABSE CLASS ---------------------------------- */
class Database {

  /**
   * Get all users from database
   * @param  {int} timeMS - time to get all users from database
   * @return {Promise}
   */
  getUsers({ timeMS } = { timeMS: 300 }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        let users = ['Billy', 'Johnny', 'Joe'];
        resolve(users);
      }, timeMS);
    });
  }
}


/* ------------------------ MAIN ------------------------------------------- */
describe('Promise', function() {
  let db = new Database();


  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe('Get1', function() {
    it('Should return users', function() {
      db.getUsers().should.eventually.have.length(3);
    });
  });

  describe('Get2', function() {
    it('Should return users', function(done) {
      db.getUsers()
        .then((users) => {
          assert.equal(users.length, 3);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
});
