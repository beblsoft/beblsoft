/**
 * @file lifecycle hook examples
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ MAIN ------------------------------------------- */
describe('Hooks', function() {

  /* -------------------- TEST HOOKS --------------------------------------- */
  before(function() {
    console.log('\tBefore Tests');
  });

  after(function() {
    console.log('\tAfter Tests');
  });

  beforeEach(function() {
    console.log('\tBefore Each Test');
  });

  afterEach(function() {
    console.log('\tAfter Each Test');
  });

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe('Addition', function() {
    it('Should add to 4', function() {
      assert.equal(2 + 2, 4);
    });
  });

  describe('Subtraction', function() {
    it('Should subtract to 0', function() {
      assert.equal(2 - 2, 0);
    });
  });

  describe('Multiplication', function() {
    it('Should multiply to 4', function() {
      assert.equal(2 * 2, 4);
    });
  });

  describe('Division', function() {
    it('Should divide to one', function() {
      assert.equal(2 / 2, 1);
    });
  });


});
