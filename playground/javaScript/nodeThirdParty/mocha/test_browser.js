/**
 * @file browser executing test example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
// Browser must load chia


/* ------------------------ MAIN ------------------------------------------- */
describe('HelloWorld', function() {
  describe('String.length', function() {
    it('Hello World String Length Is Correct', function() {
      chai.expect('Hello World'.length).to.equal(11); // eslint-disable-line no-undef
    });
  });
});
