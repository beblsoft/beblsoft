/**
 * @file dynamic test creation examples
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('chai').assert;


/* ------------------------ HELPER FUNTIONS -------------------------------- */
/**
 * Get sum of array
 * @param  {Array} array
 * @return {int} sum of array
 */
function arraySum(array) {
  let sum = 0;
  for (let i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return sum;
}


/* ------------------------ MAIN ------------------------------------------- */
describe('Dynamic', function() {
  let tests = [{
      args: [1, 2],
      expected: 3
    },
    {
      args: [1, 2, 3],
      expected: 6
    },
    {
      args: [1, 2, 3, 4],
      expected: 10
    }
  ];

  tests.forEach(function(test) {
    it(`Correctly adds ${test.args.length} args`, function() {
      let res = arraySum(test.args);
      assert.equal(res, test.expected);
    });
  });
});
