/**
 * @file inclusive test examples
 *
 * Tests with .skip are skipped
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ MAIN ------------------------------------------- */
describe('Inclusive', function() {
	let a = 'a';
	let b = 'b';
	let c = 'c';

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe.skip('A', function() {
    it('Should have correct length', function() {
      assert.equal(a.length, 1);
    });

    it('Should contain correct data', function() {
      assert.equal(a, 'a');
    });
  });

  describe('B', function() {
    it.skip('Should have correct length', function() {
      assert.equal(b.length, 1);
    });

    it('Should contain correct data', function() {
      assert.equal(b, 'b');
    });
  });

  /* eslint-env node, mocha */
  describe('C', function() {
    it('Should have correct length', function() {
      // Skip at runtime
      this.skip(); // eslint-disable-line no-invalid-this
      assert.equal(c.length, 1);
    });

    it.skip('Should contain correct data', function() {
      assert.equal(c, 'c');
    });
  });

});
