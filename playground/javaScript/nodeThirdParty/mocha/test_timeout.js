/**
 * @file test timeout examples
 */


/* ------------------------ MAIN ------------------------------------------- */
describe('Timeout', function() {
  this.timeout(500); // Everything timed out after .5 s
  this.slow(200); // Controls timer color

  it('Wicked Fast', function(done) {
    setTimeout(done, 10);
  });

  it('Fast', function(done) {
    setTimeout(done, 190);
  });

  it('Medium', function(done) {
    setTimeout(done, 400);
  });

  it.skip('Slow', function(done) {
    setTimeout(done, 1000);
  });

});
