/**
 * @file programmatically invoking test example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let Mocha = require('mocha');
let path = require('path');


/* ------------------------ GLOBALS ---------------------------------------- */
let mocha = new Mocha();
let testDir = '.';
let fileArray = [
  'test_helloWorld.js',
  'test_dynamic.js',
  'test_inclusive.js'
];


/* ------------------------ MAIN ------------------------------------------- */
for (let file of fileArray) {
  mocha.addFile(path.join(testDir, file));
}
mocha.run();
