/**
 * @file chai should examples
 *
 * Should : Extends Object.prototype to provide a single getter as starting point for
 *          language assertions. Works everywhere except Internet Explorer
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let should = require('chai').should();


/* ------------------------ GLOBALS ---------------------------------------- */
let foo = 'bar';
let beverages = { tea: ['chai', 'matcha', 'oolong'] };


/* ------------------------ SHOULD ----------------------------------------- */
describe('Should', function () {
  it('Should Should Corectly', function () {
    foo.should.be.a('string');
    foo.should.equal('bar');
    foo.should.have.lengthOf(3);
    beverages.should.have.property('tea').with.lengthOf(3);
  });
});
