/**
 * @file Hello World Example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ MAIN ------------------------------------------- */
describe('HelloWorld', function() {
  describe('String.length', function() {
    it('Hello World String Length Is Correct', function() {
      assert.equal('Hello World'.length, 11);
    });
  });
});
