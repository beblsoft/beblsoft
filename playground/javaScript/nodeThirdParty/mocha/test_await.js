/**
 * @file test using await
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let chai = require('chai');
let should = chai.should(); // eslint-disable-line no-unused-vars
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);


/* ------------------------ DATABSE CLASS ---------------------------------- */
class Database {

  /**
   * Get all database users
   * @param  {int} timeMS - time in milliseconds to return users
   * @return {Promise} - Array of users
   */
  getUsers({ timeMS } = { timeMS: 300 }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        let users = ['Billy', 'Johnny', 'Joe'];
        resolve(users);
      }, timeMS);
    });
  }
}


/* ------------------------ MAIN ------------------------------------------- */
describe('Await', function() {
  let db = new Database();

  /* -------------------- TEST FUNCTIONS ----------------------------------- */
  describe('Get2', function() {
    it('Should return users', async function() {
      const users = await db.getUsers(200);
      users.should.have.length(3);
    });
  });
});
