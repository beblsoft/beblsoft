# Mocha JS Technology Documentation

Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, making
asynchronous testing simple and fun. Mocha tests run serially, allowing for flexible and accurate
reporting, while mapping uncaught exceptions to the correct test cases. Hosted on GitHub.

Relevant URLs:
[Home](https://mochajs.org/),
[Wikipedia](https://en.wikipedia.org/wiki/Mocha_(JavaScript_framework))

## Installation

Mocha requires Node.js v6.x or newer.
Installation directions:

```bash
# Install globally
npm install -g mocha

# Install locally
npm install --save-dev mocha
```

## Usage Options

Common Usage Options:
- Bail after first exception raised: `-b, --bail`
- Add debugger support: `-d, --debug`
- Check for tests leaking variables: `--checkleaks`
- Allow tests to leak certain global variables: `--globals <names>`
- Require modules without having to state them explicitly:
  `-r, --require <module-name>` Ex. `--requure ./test/helper.js`
- Specify UI: `-u, --ui <name>` Defaults to `bdd`
- Specify Reporter: `-R, --reporter <name>` Defaults to `spec`
- Add a file included first in test suite: `--file <file>`
- Run tests that matcha particular grep pattern: `-g, --grep <pattern>``