# Mocha Projects Documentation

To Install: `npm install`

## All

Run all tests: `npm test`

## test_helloWorld.js

Mocha Hello World Test

Run: `npx mocha test_helloWorld.js`

## test_hooks.js

Demonstrate Mocha Test Lifecycle Hooks

Run: `npx mocha test_hooks.js`

## test_cb.js

Tests with asynchronous callbacks

Run: `npx mocha test_cb.js`

## test_promise.js

Test sending promises

Run: `px mocha test_promise.js`

## test_await.js

Test awaiting promises

Run: `npx mocha test_await.js`

## test_pending.js

Test pending (tests waiting to be written). Useful for test-driven development

Run: `npx mocha test_pending.js`

## test_exclusive.js

Test exclusive tests. Useful for just running specific tests

Run: `npx mocha test_exclusive.js`

## test_inclusive.js

Test inclusive tests. Useful for skipping certain tests

Run: `npx mocha test_inclusive.js`

## test_dynamic.js

Test creating dynamic tests

Run: `npx mocha test_dynamic.js`

## test_timeout.js

Test timeouts. Useful for timing out tests if they take too long

Run: `npx mocha test_timeout.js`

## test_browser.js, index.html

Test Running in browser

Run: Open index.html in Chrome

## test_environment.js

Test Passing and Reading Environment Variables

Run: `export FOO_KEY=FOO_VAL; npx mocha test_environment.js`

## programmatic.js

Run tests programmatically

Run: `node programmatic.js`

## test_chaiAssert.js

Tests with chai asserts

Run: `npx mocha test_chaiAssert.js`

## test_chaiExpect.js

Tests with chai expects

Run: `npx mocha test_chaiExpect.js`

## test_chaiShould.js

Tests with chai shoulds

Run: `npx mocha test_chaiShould.js`

## test_sinon.js

Test Sinon Functionality [Fakes, Spies, Stubs, Mocks]
Run: `npx mocha test_sinon.js`