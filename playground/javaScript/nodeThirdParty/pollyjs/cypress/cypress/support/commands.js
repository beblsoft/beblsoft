/**
 * @file Cypress Custom Commands
 */

/* ------------------------ IMPORTS ---------------------------------------- */
import { Polly } from '@pollyjs/core';
import FetchAdapter from '@pollyjs/adapter-fetch';
import XHRAdapter from '@pollyjs/adapter-xhr';
import LocalStoragePersister from '@pollyjs/persister-local-storage';
import RESTPersister from '@pollyjs/persister-rest';


/* ------------------------ MOCK VISIT ------------------------------------- */
Polly.register(FetchAdapter);
Polly.register(XHRAdapter);
Polly.register(LocalStoragePersister);
Polly.register(RESTPersister);

/**
 * Code to execute before page loads
 * @param {Window} win Window Object
 */
function onBeforeLoad(win) {
  win.Polly = Polly;
  win.polly = new Polly('polly-recording', {
    logging: true,
    adapters: ['xhr', 'fetch'],
    adapterOptions: {
      fetch: { context: win },
      xhr: { context: win },
    },
    persister: 'rest',
    persisterOptions: {
      rest: {
        host: 'http://localhost:3000', // Host where Polly exists
        apiNamespace: '/polly' // Namespace where the Polly API is mounted
      }
    }
  });
  const {server} = win.polly;
  server.get('http://jsonplaceholder.typicode.com/todos').intercept((req, res) => {
  	res.sendStatus(404);
  })
  console.log('Started Polly!');
}

Cypress.Commands.add('mockVisit', (url, options) => {
  cy.visit(url, Object.assign({}, options, { onBeforeLoad: onBeforeLoad }));
});


/* ------------------------ MOCK SAVE -------------------------------------- */
Cypress.Commands.add('mockSave', (url, options) => {
  cy.window().then(async (win) => {
    await win.polly.stop();
    console.log('Stopped polly!');
  });
});
