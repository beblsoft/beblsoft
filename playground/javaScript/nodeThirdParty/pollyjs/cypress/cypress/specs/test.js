/**
 * @file Test file using cypress and Polly
 */

/* ------------------------ TESTS ------------------------------------------ */
describe('My First Test', () => {

  beforeEach(function () {
    cy.mockVisit('/');
  });

  it('Visits the app root url', () => {
    cy.contains('h1', 'Welcome to Your Vue.js App');
    cy.wait(4000);
  	cy.mockSave();
  });
});
