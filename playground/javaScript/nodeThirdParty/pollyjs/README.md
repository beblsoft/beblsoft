# Polly.js Technology Documentation

Polly.JS is a standalone, framework-agnostic JavaScript library that enables recording, replaying,
and stubbing of HTTP interactions. By tapping into multiple request APIs across both Node & the browser,
Polly.JS is able to mock requests and responses with little to no configuration while giving you the
ability to take full control of each request with a simple, powerful, and intuitive API.

__Why Polly?__: Keeping fixtures and factories in parity with your APIs can be a time consuming process. Polly
alleviates this by recording and maintaining actual server responses without foregoing flexibility.
- Record your test suite's HTTP interactions and replay them during future test runs for fast,
  deterministic, accurate tests.
- Use Polly's client-side server to modify or intercept requests and responses to simulate
  different application states (e.g. loading, error, etc)

__Features__:
- Node & Browser Support
- Simple, Powerfule, & Intuitive API
- First Class Mocha & QUnit Test Helpers
- Intercept, Pass-Through, and Attach Events
- Record to Disk or Local Storage
- Slow Down or Speed Up Time

__Relevant URLs__:
[Home](https://netflix.github.io/pollyjs/#/README),
[GitHub](https://github.com/Netflix/pollyjs),
[NPM](https://www.npmjs.com/package/@pollyjs/core)

## Installation

```bash
# Install core with npm
npm install -D @pollyjs/core

# Install core with yarn
yarn add -D @pollyjs/core
```

## Basic Usage

Once instantiated, Polly will hook into native implementations (such as fetch & XHR) via adapters
to intercept any outgoing requests. Depending on its current mode as well as rules defined via
the client-side server, the request will either be replayed, recorded, passed-through, or intercepted.

The test case below would generate the following [HTTP Archive (HAR)](http://www.softwareishard.com/blog/har-12-spec/)
which Polly will use to replay the sign-in response when the test is rerun.

Example test case using Polly:

```js
import { Polly } from '@pollyjs/core';
import XHRAdapter from '@pollyjs/adapter-xhr';
import FetchAdapter from '@pollyjs/adapter-fetch';
import RESTPersister from '@pollyjs/persister-rest';

/*
  Register the adapters and persisters we want to use. This way all future
  polly instances can access them by name.
*/
Polly.register(XHRAdapter);
Polly.register(FetchAdapter);
Polly.register(RESTPersister);

describe('Netflix Homepage', function() {
  it('should be able to sign in', async function() {
    /*
      Create a new polly instance.

      Connect Polly to both fetch and XHR browser APIs. By default, it will
      record any requests that it hasn't yet seen while replaying ones it
      has already recorded.
    */
    const polly = new Polly('Sign In', {
      adapters: ['xhr', 'fetch'],
      persister: 'rest'
    });
    const { server } = polly;

    /* Intercept all Google Analytic requests and respond with a 200 */
    server
      .get('/google-analytics/*path')
      .intercept((req, res) => res.sendStatus(200));

    /* Pass-through all GET requests to /coverage */
    server.get('/coverage').passthrough();

    /* start: pseudo test code */
    await visit('/login');
    await fillIn('email', 'polly@netflix.com');
    await fillIn('password', '@pollyjs');
    await submit();
    /* end: pseudo test code */

    expect(location.pathname).to.equal('/browse');

    /*
      Calling `stop` will persist requests as well as disconnect from any
      connected browser APIs (e.g. fetch or XHR).
    */
    await polly.stop();
  });
});
```

## Adapters & Persisters

Before you start using Polly, you'll need to install the necessary adapters and persisters depending
on your application/environment.
- __Adapters__: provide functionality that allows Polly to intercept requests via different sources
  (e.g. XHR, fetch, Puppeteer)
- __Persisters__: Provide the functionality to read & write recorded data (e.g. fs, local-storage)

Install with npm or yarn:

```bash
# npm

npm install @pollyjs/adapter-{name} -D
npm install @pollyjs/persister-{name} -D

# yarn
yarn add @pollyjs/adapter-{name} -D
yarn add @pollyjs/persister-{name} -D
```

Once installed, you can register the adapters and persisters with Polly so they can easily be referenced
by name later:
```js
import { Polly } from '@pollyjs/core';
import FetchAdapter from '@pollyjs/adapter-fetch';
import XHRAdapter from '@pollyjs/adapter-xhr';
import LocalStoragePersister from '@pollyjs/persister-local-storage';

Polly.register(FetchAdapter);
Polly.register(XHRAdapter);
Polly.register(LocalStoragePersister);

new Polly('<Recording Name>', {
  adapters: ['fetch', 'xhr'],
  persister: 'local-storage'
});
```

## Client-Side Server

Every Polly instance has a reference to a client-side server which you can leverage to gain full control
of all HTTP interactions as well as dictate how the Polly instance should handle them.

Intercept a request and return a 404:
```js
describe('Simple Client-Side Server Example', function() {
  it('fetches an unknown post', async function() {
    /*
      Create a new polly instance.

      Connect Polly to fetch. By default, it will record any requests that it
      hasn't yet seen while replaying ones it has already recorded.
    */
    const polly = new Polly('Simple Client-Side Server Example', {
      adapters: ['fetch'], // Hook into `fetch`
      persister: 'local-storage', // Read/write to/from local-storage
      logging: true // Log requests to console
    });
    const { server } = polly;

    /*
      Add a rule via the client-side server to intercept the
      `https://jsonplaceholder.typicode.com/posts/404` request and return
      an error.
    */
    server
      .get('https://jsonplaceholder.typicode.com/posts/404')
      .intercept((req, res) => {
        res.status(404).json({ error: 'Post not found.' });
      });

    const response = await fetch(
      'https://jsonplaceholder.typicode.com/posts/404'
    );
    const post = await response.json();

    expect(response.status).to.equal(404);
    expect(post.error).to.equal('Post not found.');

    /*
      Calling `stop` will persist requests as well as disconnect from any
      connected adapters.
    */
    await polly.stop();
  });
});
```

## Test Frameworks

### Mocha Test Framework

The `@pollyjs/core` package provides a `setupMocha` utility which will setup a new polly instance for
each test as well as stop it once the test has ended. The Polly instance's recording name is derived
from the current test name as well as its parent module(s).

Simple example:
```js
import { setupMocha as setupPolly } from '@pollyjs/core';

describe('Netflix Homepage', function() {
  setupPolly({
    /* default configuration options */
  });

  it('should be able to sign in', async function() {
    /*
      The setupPolly test helper creates a new polly instance which you can
      access via `this.polly`. The recording name is generated based on the module
      and test names.
    */
    this.polly.configure({ recordIfMissing: false });

    /* start: pseudo test code */
    await visit('/login');
    await fillIn('email', 'polly@netflix.com');
    await fillIn('password', '@pollyjs');
    await submit();
    /* end: pseudo test code */

    expect(location.pathname).to.equal('/browse');

    /*
      The setupPolly test helper will call `this.polly.stop()` when your test
      has finished.
    */
  });
});
```

Accessing `this.polly` during a test run after the polly instance has been stopped and destroyed
produces an error. If you need to do some work before the polly instance gets destroyed or just
need more control on when each of the test hooks are called, see below.
```js
import { setupMocha as setupPolly } from '@pollyjs/core';

describe('Netflix Homepage', function() {
  setupPolly.beforeEach({
    /* default configuration options */
  });

  afterEach(function() {
    /* do something before the polly instance is destroyed... */
  });

  setupPolly.afterEach();

  it('should be able to sign in', async function() {
    /* ... */
  });
});
```

## Adapters

### Fetch Adapter

The fetch adapter wraps the global fetch method for seamless recording and replaying of requests.

Installation:
```bash
npm install @pollyjs/adapter-fetch -D
```

Usage:
```js
import { Polly } from '@pollyjs/core';
import FetchAdapter from '@pollyjs/adapter-fetch';

// Register the fetch adapter so its accessible by all future polly instances
Polly.register(FetchAdapter);

const polly = new Polly('<Recording Name>', {
  adapters: ['fetch']
});

// Disconnect using the `configure` API
polly.configure({ adapters: [] });

// Reconnect using the `connectTo` API
polly.connectTo('fetch');

// Disconnect using the `disconnectFrom` API
polly.disconnectFrom('fetch');
```

Options:
```js
polly.configure({
  adapters: ['fetch'],
  adapterOptions: {
    fetch: {
      context: window // Typically this is the window or self in the browser, and global in node
    }
  }
});
```

### Node HTTP Adapter

The node-http adapter provides a low level nodejs http request adapter that uses __nock__ to path
the __http__ and __https__ modues in nodejs for seamless recording and replaying of requests.

Installation:
```bash
npm install @pollyjs/adapter-node-http -D
```

Usage:
```js
import { Polly } from '@pollyjs/core';
import NodeHttpAdapter from '@pollyjs/adapter-node-http';

// Register the node http adapter so its accessible by all future polly instances
Polly.register(NodeHttpAdapter);

const polly = new Polly('<Recording Name>', {
  adapters: ['node-http']
});

// Disconnect using the `configure` API
polly.configure({ adapters: [] });

// Reconnect using the `connectTo` API
polly.connectTo('node-http');

// Disconnect using the `disconnectFrom` API
polly.disconnectFrom('node-http');
```

### XHR Adapter

The XHR adapter uses Sinon's __Nise__ library to fake the global `XMLHttpRequest` object while wrapping
the native one to allow for seamless recording and replaying of requests.

Installation:
```bash
npm install @pollyjs/adapter-xhr -D
```

Usage:
```js
import { Polly } from '@pollyjs/core';
import XHRAdapter from '@pollyjs/adapter-xhr';

// Register the xhr adapter so its accessible by all future polly instances
Polly.register(XHRAdapter);

const polly = new Polly('<Recording Name>', {
  adapters: ['xhr']
});

// Disconnect using the `configure` API
polly.configure({ adapters: [] });

// Reconnect using the `connectTo` API
polly.connectTo('xhr');

// Disconnect using the `disconnectFrom` API
polly.disconnectFrom('xhr');
```

## Persisters

### File System

Read and write recordings to and from the file system.

Installation:
```bash
npm install @pollyjs/persister-fs -D
```

Usage:
```js
import { Polly } from '@pollyjs/core';
import FSPersister from '@pollyjs/persister-fs';

// Register the fs persister so its accessible by all future polly instances
Polly.register(FSPersister);

new Polly('<Recording Name>', {
  persister: 'fs'
});
```

Options:
```js
polly.configure({
  persisterOptions: {
    fs: {
      // Root directory to store all recordings, supports absolute and relative paths
      recordingsDir: '__recordings__'
    }
  }
});
```

### Rest

Read and write recordings to and from the file system via a CRUD API hosted on a server.

Installation:
```bash
npm install @pollyjs/persister-rest -D
```

Usage:
```js
import { Polly } from '@pollyjs/core';
import RESTPersister from '@pollyjs/persister-rest';

// Register the rest persister so its accessible by all future polly instances
Polly.register(RESTPersister);

new Polly('<Recording Name>', {
  persister: 'rest'
});
```

Options:
```js
polly.configure({
  persisterOptions: {
    rest: {
      host: 'http://localhost.com:4000', // Host where Polly exists
      apiNamespace: '/pollyjs'  // Namespace where the Polly API is mounted
    }
  }
});
```

## Polly Instance

### Constructor

Creates a new Polly instance

```js
new Polly('<Recording Name>', {
  /* ... */
});
```

### Events

- __create__: Emitted when when Polly instance gets created
  ```js
  const listener = polly => {
    /* Do Something */
  };

  Polly.on('create', listener);
  Polly.off('create', listener);
  Polly.once('create', polly => {
    /* Do Something Once */
  });
  ```
- __stop__: Emitted when a Polly instance has successfully stopped
  ```js
  const listener = polly => {
    /* Do Something */
  };

  Polly.on('stop', listener);
  Polly.off('stop', listener);
  Polly.once('stop', polly => {
    /* Do Something Once */
  });
  ```

### Properties

- __recordingName__: The recording name the recordings will be stored under. The provided name is sanitized
  as well as postfixed with a GUID.
  ```js
  new Polly('Wants a Cracker/Cheddar', {
    /* ... */
  });
  ```
  Recordings saved:
  ```bash
  recordings
  └── Wants-a-Cracker_1234
      └── Cheddar_5678
          └── recording.json
  ```
- __mode__: Default: `replay`. The current mode polly is in.
- __persister__: The persister used to find and save recordings
- __server__: Default: `Server`. Every polly instance as a reference to a client-side server which you
  can leverage to gain full control of all HTTP interactions as well as dictate how the Polly instance
  should handle them
  ```js
  const { server } = polly;

  server.get('/movies').passthrough();
  server.get('/series').intercept((req, res) => res.sendStatus(200));
  ```

### Methods

- __configure__: Configure polly with the given configuration object
  ```js
  polly.configure({ recordIfMissing: false });
  ```
- __record__: Puts polly in recording mode. All requests going forward will be sent to the server and
  their responses will be recorded.
  ```js
  polly.record();
  ```
- __replay__: Puts polly in replay mode. All requests going forward will be played back from a saved
  recording.
- __pause__: Puts polly in paused mode. All requests going forward will pass through and will not be
  recorded or replayed. The previous mode will be saved and can be restored by calling `play`.
  ```js
  // polly.mode === 'replay'
  polly.pause();
  // polly.mode === 'passthrough'
  ```
- __play__: Restores the mode to the one before `pause` was called.
  ```js
  // polly.mode === 'replay'
  polly.pause();
  // polly.mode === 'passthrough'
  polly.play();
  // polly.mode === 'replay'
  ```
- __stop__: Persist all recordings and disconnect from all adapters. This method is `async` and will
  resolve once all recordings have persisted and the instance has successfully torn down.
  ```js
  await polly.stop();
  ```
- __connectTo__: Connect to an adapter.
  ```js
  polly.connectTo('xhr');
  polly.connectTo(XHRAdapter);
  ```
- __disconnectFrom__: Disconnect from an adapter
  ```js
  polly.disconnectFrom('xhr');
  polly.disconnectFrom(XHRAdapter);
  ```
- __disconnect__: Disconnect from all connected adapters.
  ```js
  polly.disconnect();
  ```
- __flush__: Returns a Promise that resolves once all requests handled by Polly have resolved.
  ```js
  await polly.flush();
  ```

## Polly Instance Configuration

A Polly instance can be configured by passing a configuration object to the contructor's 2nd argument.
```js
new Polly('<Recording Name>', {
  recordIfMissing: false
});
```
Or via the configure method on the instance:
```js
const polly = new Polly('<Recording Name>');

polly.configure({
  recordIfMissing: false
});
```

### Defaults

```js
import { MODES } from '@pollyjs/utils';

import Timing from '../utils/timing';

export default {
  mode: MODES.REPLAY,

  adapters: [],
  adapterOptions: {},

  persister: null,
  persisterOptions: {
    keepUnusedRequests: false
  },

  logging: false,

  recordIfMissing: true,
  recordIfExpired: false,
  recordFailedRequests: false,

  expiresIn: null,
  timing: Timing.fixed(0),

  matchRequestsBy: {
    method: true,
    headers: true,
    body: true,
    order: true,

    url: {
      protocol: true,
      username: true,
      password: true,
      hostname: true,
      port: true,
      pathname: true,
      query: true,
      hash: false
    }
  }
};
```

### Properties

- __logging__: If `true`, log requests and their responses to the console grouped by the recording name
- __recordIfMissing__: If a request's recording is not found, pass-through to the server and record the response.
- __recordIfExpired__: If a request's recording has expired, pass-through to the server and record a new response.
- __recordFailedRequests__: If `false`, Polly will throw when attempting to persist any failed requests. A request
  is considered to be a failed reaquest when its response's status code is `< 200` or `>= 300`
- __expiresIn__: Default: `null`. After how long the recorded request will be considered expired from
  the time it was persisted.
  ```js
  polly.configure({
    expiresIn: '30d5h10m' // expires in 30 days, 5 hours, and 10 minutes
  });

  polly.configure({
    expiresIn: '5 min 10 seconds 100 milliseconds' // expires in 5 minutes, 10 seconds, and 100 milliseconds
  });
  ```
- __mode__: The Polly mode. Can be one of the following:
    * `replay`: Replay responses from recordings
    * `record`: Force Polly to record all requests. This will overwrite recordings that already exist.
    * `passthrough`: Passes all requests through directly to the server without recording or replaying.
  ```js
  polly.configure({
    mode: 'record'
  });
  ```
- __adapters__: The adapter(s) polly will hook into.
  ```js
  import XHRAdapter from '@pollyjs/adapter-xhr';
  import FetchAdapter from '@pollyjs/adapter-fetch';

  // Register the xhr adapter so its accessible by all future polly instances
  Polly.register(XHRAdapter);

  polly.configure({
    adapters: ['xhr', FetchAdapter]
  });
  ```
- __adapterOptions__: Options to be passed into the adapters keyed by the adapter name.
  ```js
  polly.configure({
    adapterOptions: {
      fetch: {
        context: win
      }
    }
  });
  ```
- __persister__: The persister to use for recording and replaying requests.
  ```js
  import RESTPersister from '@pollyjs/persister-rest';
  import LocalStoragePersister from '@pollyjs/persister-local-storage';

  // Register the local-storage persister so its accessible by all future polly instances
  Polly.register(LocalStoragePersister);

  polly.configure({
    persister: 'local-storage'
  });

  polly.configure({
    persister: RESTPersister
  });
  ```
- __persisterOptions__: Options to be passed into the persister keyed by the persister name.
  ```js
  polly.configure({
    persisterOptions: {
      rest: {
        apiNamespace: '/pollyjs'
      }
    }
  });
  ```
    * __keepUnusedRequests__: When disabled, requests that have not been captured by the running Polly instance
      will be remved from any previous recording. This ensures that a recording will only contain the requests
      that were made during the lifespan of the Polly instance. When enabled, new requests will be appeneded
      to the recording file.
- __timing__: The timeout delay strategy used when replaying requests.
  ```js
  import { Timing } from '@pollyjs/core';

  polly.configure({
    // Replay requests at 300% the original speed to simulate a 3g connection
    timing: Timing.relative(3.0)
  });

  polly.configure({
    // Replay requests with a 200ms delay
    timing: Timing.fixed(200)
  });
  ```
- __matchRequestsBy__: Request matching configuration. Each of these options are used to generate a GUID
  for the request.
    * __method__: The request method (e.g. `GET`, `POST`)
    * __headers__
    * __body__
    * __order__: The order the request came in. In the following case, the order of the requests matter since
      the payload for the first and last fetch are different.
      ```js
      // Retrieve our model
      let model = await fetch('/models/1').then(res => res.json());

      // Modify the model
      model.foo = 'bar';

      // Save the model with our new change
      await fetch('/models/1', { method: 'POST', body: JSON.stringify(model) });

      // Get our updated model
      model = await fetch('/models/1').then(res => res.json());

      // Assert that our change persisted
      expect(model.foo).to.equal('bar');
      ```
    * __url.protocol__: The request url protocol (e.g. `http`)
    * __url.username__: Username of basic authentication
    * __url.password__: Password of basic authentication
    * __url.hostname__: Host name without port number
      ```js
      polly.configure({
        matchRequestsBy: {
          url: {
            hostname(hostname) {
              return hostname.replace('.com', '.net');
            }
          }
        }
      });
      ```
    * __url.port__: Port number
      ```js
      polly.configure({
        matchRequestsBy: {
          url: {
            port(port) {
              return 3000;
            }
          }
        }
      });
      ```
    * __url.pathname__: URL path
    * __url.query__: Sorted query string
    * __url.hash__: The "fragment" portion of the URL including the pound-sign

## Client Server

Every Polly instance has a reference to a client-side server which you can leverage to gain full
control of all HTTP interactions as well as dictate how the Polly instance should handle them.

The server uses [Route Recognizer](https://github.com/tildeio/route-recognizer) under the hood.
This allows you to define static routes, as well as dynamic, and starred segments.

For example:
```js
// Static Routes
server.get('/api/v2/users').intercept((req, res) => {
  res.sendStatus(200);
});

// Dynamic Segments
server.get('http://netflix.com/movies/:id').intercept((req, res) => {
  console.log(req.params.id); // http://netflix.com/movies/1 → '1'
  res.sendStatus(200);
});

// Starred Segments
server.get('/secrets/*path').intercept((req, res) => {
  console.log(req.params.path); // /secrets/foo/bar → 'foo/bar'
  res.status(401).send('Shhh!');
});
```

HTTPS methos as well as `.any()` accept a single string as well as an array of strings.
For example:
```js
// Match against '/api/v2/users' as well as any child route
server.get(['/api/v2/users', '/api/v2/users/*path']).passthrough();

// Register the same event handler on both '/session' and '/users/session'
server.any(['/session', '/users/session']).on('request', () => {});
```

### API

- __HTTP Methods__

  ```js
  server.get('/ping');
  server.put('/ping');
  server.post('/ping');
  server.patch('/ping');
  server.delete('/ping');
  server.merge('/ping');
  server.head('/ping');
  server.options('/ping');
  ```
  Each of these methods returs a Route Handler which you can use to pass-through, intercept, and attach
  events to.

  ```js
  server.get('/ping').passthrough();
  server.put('/ping').intercept((req, res) => res.sendStatus(200));
  server.post('/ping').on('request', req => {
    /* Do Something */
  });
  server.patch('/ping').off('request');
  ```

- __any__

  Declare Events & Middleware globally or for a specific route.
  ```js
  server.any('/session/:id').on('request', (req, res) => {
    req.query.email = 'test@netflix.com';
  });
  ```

- __host__

  Define a block where all methods will inherit the provided host.

  ```js
  server.host('http://netflix.com', () => {
    // Middleware will be attached to the host
    server.any().on('request', req => {});

    server.get('/session').intercept(() => {}); // → http://netflix.com/session
  });
  ```

- __namespace__

  Define a block where all methods will inherit the provided namespace.
  ```js
  server.namespace('/api', () => {
    // Middleware will be attached to the namespace
    server.any().on('request', req => {});

    server.get('/session').intercept(() => {}); // → /api/session

    server.namespace('/v2', () => {
      server.get('/session').intercept(() => {}); // → /api/v2/session
    });
  });
  ```

- __timeout__

  Returns a promise that will resolve after the given number of milliseconds
  ```js
  server.get('/ping').intercept(async (req, res) => {
    await server.timeout(500);
    res.sendStatus(200);
  });
  ```

### Events

Events can be attached to a server route using `.on` and detached via the `.off` methods. Event
handlers can be asynch. An `async` function can be used or a `Promise` can be returned.

```javascript
// Events
server
  .get('/')
  .on('request', req => {})
  .off('request');

// Passthrough w/ Events
server
  .get('/')
  .passthrough()
  .on('beforeResponse', (req, res) => {})
  .off('beforeResponse');

// Intercept w/ Events
server
  .get('/', (req, res) => {})
  .on('request', req => {})
  .on('beforeResponse', (req, res) => {});

// Middleware w/ Events
server
  .any('/')
  .on('request', req => {})
  .on('beforeResponse', (req, res) => {});
```

- __request__

  Fires right before the request goes out
  ```js
  server.get('/session').on('request', req => {
    req.headers['X-AUTH'] = '<ACCESS_TOKEN>';
    req.query.email = 'test@app.com';
  });
  ```

- __beforeResponse__

  Fires right before the response materializes and the promise resolves.
  ```js
  server.get('/session').on('beforeResponse', (req, res) => {
    res.setHeader('X-AUTH', '<ACCESS_TOKEN>');
  });
  ```

- ___response___

  Fires right after the response has been finalized for the request but before the response materializes
  and the promise resolves.
  ```js
  server.get('/session').on('response', (req, res) => {
    console.log(
      `${req.url} took ${req.responseTime}ms with a status of ${res.statusCode}.`
    );
  });
  ```
- __beforePersist__

  Fires before the request/response gets persisted

  ```js
  server.any().on('beforePersist', (req, recording) => {
    recording.request = encrypt(recording.request);
    recording.response = encrypt(recording.response);
  });
  ```

- __beforeReplay__

  Fires after retrieving the recorded request/response from the persister and before the recording
  materializes into a response.
  ```js
  server.any().on('beforeReplay', (req, recording) => {
    recording.request = decrypt(recording.request);
    recording.response = decrypt(recording.response);
  });
  ```

- __error__

  Fires when any error gets emitted during the request life-cycle.

  ```js
  server.any().on('error', (req, error) => {
    console.error(error);
    process.exit(1);
  });
  ```

### Middleware

Middleware can be added via the `.any()` method. Middleware events will be executed by the order in
which they were declared.

- __Global Middleware__

  The following is an example of a global middleware that will be attached to all routes. This middleware
  in specific overrides the `X-Auth-Token` with a test token.
  ```js
  server.any().on('request', (req, res) => {
    req.headers['X-Auth-Token'] = 'abc123';
  });
  ```

- __Route Level Middleware__

  The following is an example of a route level middleware that will be attached to any route that matches
  `/session/:id`. This middleware in specific overrides the email query param with that of a test email.
  ```js
  server.any('/session/:id').on('request', (req, res) => {
    req.query.email = 'test@netflix.com';
  });
  ```

### Route Handler

The Route Handler is an object that is returned when calling any of the HTTP methods as well as
`server.any()`

- __on__

  Registers an event handler

  ```js
  server
    .get('/session')
    .on('request', req => {
      req.headers['X-AUTH'] = '<ACCESS_TOKEN>';
      req.query.email = 'test@app.com';
    })
    .on('request', () => {
      /* Do something else */
    })
    .on(
      'request',
      () => {
        /* Do something else twice */
      },
      { times: 2 }
    );
  ```

- __once__

  Register a one-time event handler.

  ```js
  server
    .get('/session')
    .once('request', req => {
      req.headers['X-AUTH'] = '<ACCESS_TOKEN>';
      req.query.email = 'test@app.com';
    })
    .once('request', () => {
      /* Do something else */
    });
  ```

- __off__

  Un-register an event handler. If no handler is specified, all event handlers are un-registered for
  the given event name.

  ```js
  const handler = () => {};

  server
    .get('/session')
    .on('request', , handler)
    .on('request', () => {})
    .off('request', handler) /* Un-register the specified event/handler pair */
    .off('request'); /* Un-register all handlers */
  ```

- __intercept__

  Register an intercept handler. Once set, the request will never go to server but instead defer to
  the provided handler to handle the response. If multiple intercept handlers have been registered,
  each handler will be called in the order in which it was registered.

  ```js
  server.any('/session').intercept((req, res) => res.sendStatus(200));

  server.any('/twice').intercept((req, res) => res.sendStatus(200), { times: 2 });

  server.get('/session/:id').intercept((req, res, interceptor) => {
    if (req.params.id === '1') {
      res.status(200).json({ token: 'ABC123XYZ' });
    } else if (req.params.id === '2') {
      res.status(404).json({ error: 'Unknown Session' });
    } else {
      interceptor.abort();
    }
  });
  ```

  The `intercept` handler receives a third `interceptor` argument that provides some utilities.

  __abort__:
  ```js
  server.get('/session/:id').intercept((req, res, interceptor) => {
    if (req.params.id === '1') {
      res.status(200).json({ token: 'ABC123XYZ' });
    } else {
      interceptor.abort(); // Treat request as if it wasn't intercepted
    }
  });
  ```

  __passthrough__:
  ```js
  server.get('/session/:id').intercept((req, res, interceptor) => {
    if (req.params.id === '1') {
      res.status(200).json({ token: 'ABC123XYZ' });
    } else {
      interceptor.passthrough(); // Send through to server
    }
  });
  ```

  __stopPropagation__:
  ```js
  // First call should return the user and not enter the 2nd handler
  server
    .get('/session/:id')
    .times(1) // Remove this interceptor after it gets called once
    .intercept((req, res, interceptor) => {
      // Do not continue to the next intercept handler which handles the 404 case
      interceptor.stopPropagation();
      res.sendStatus(200);
    });

  server.delete('/session/:id').intercept((req, res) => res.sendStatus(204));

  // Second call should 404 since the user no longer exists
  server.get('/session/:id').intercept((req, res) => res.sendStatus(404));

  await fetch('/users/1'); // --> 200
  await fetch('/users/1', { method: 'DELETE' }); // --> 204
  await fetch('/users/1'); // --> 404
  ```

- __passthrough__

  Declare a route as a passthrough meaning any request that matches that route will directly use
  the native implementation. Passthrough requests will not be recorded.

  ```js
  server.any('/session').passthrough();
  server.get('/session/1').passthrough(false);  // Disable passthrough
  ```

- __filter__

  Filter requests matched by the route handler with a predicate callback function. This can be useful
  when trying to match a request by a part of the url, a header, and/or parts of the request body.

  ```js
  server
    .any()
    .filter(req => req.hasHeader('Authentication'));
    .on('request', req => {
      res.setHeader('Authentication', 'test123')
    });

  server
    .get('/users/:id')
    .filter(req => req.params.id === '1');
    .intercept((req, res) => {
      res.status(200).json({ email: 'user1@test.com' });
    });
  ```

- __times__

  Proceeding intercept and event handlers defined will be removed after being called the specified
  amount of times. The number of specified is used as a default value and can be overridden by
  passing a custime `times` option to the handler.

  ```js
  server
    .any()
    .times(2);
    .on('request', req => {});
    .intercept((req, res) => {});
    .times()
    .on('response', (req, res) => {});

  // Is the same as:

  server
    .any()
    .on('request', req => {}, { times: 2 });
    .intercept((req, res) => {}, { times: 2 });
    .on('response', (req, res) => {});
  ```

- __configure__

  Override configuration options for the given route. All matching middleware and route level configs
  are merged together and the overrides are applied to the current Polly instance's config.

  ```js
  server.any('/session').configure({ recordFailedRequests: true });
  server.get('/users/:id').configure({ timing: Timing.relative(3.0) });
  server.get('/users/1').configure({ logging: true });
  ```

- __recordingName__

  Override the recording name for the given route. This allows for grouping common requests to share
  a single recording which can drastically help de-clutter test recordings.

  For example, if your tests always make a `/users` or `/session` call, instead of having each of those
  requests be recorded for every single test, you can use this to create a common recording file for
  them.
  ```js
  server.any('/session').recordingName('User Session');
  server.get('/users/:id').recordingName('User Data');
  server
    .get('/users/1')
    .recordingName(); /* Fallback to the polly instance's recording name */
  ```

### Request

#### Properties

- method: Request method (`GET`, `POST`, `DELETE`, ...)
- url
- protocol
- hostname
- port
- pathname
- hash
- headers
- body
- query
- params
  ```js
  server.get('/movies/:id').intercept((req, res) => {
    console.log(req.params.id);
  });
  ```
- recordingName: the recording the request should be recorded under

#### Methods

- getHeader
- setHeader
- setHeaders
- removeHeader
- removeHeaders
- type
- send: Set's the request's body
  ```js
  req.send('Hello World');
  req.send(200);
  req.send(true);
  req.send();
  ```
- json: A shortcut method to set the content type to `application/json` if it hasn't been set already,
  and call `send` with the stringified object
  ```js
  req.json({ Hello: 'World' });
  ```
- jsonBody: A shortcut method that calls `JSON.parse` on the request's body

### Response

#### Properties

- statusCode
- headers
- body

#### Methods

- status
- getHeader
  ```js
  res.getHeader('Content-Type'); // → application/json
  ```
- setHeader
  ```js
  res.setHeader('Content-Length', 42);
  ```
- removeHeader
- removeHeaders
- hasHeader
- type
- send: Sets the response's body
  ```js
  res.send('Hello World');
  res.send(200);
  res.send(true);
  res.send();
  ```
- sendStatus
  ```js
  res.sendStatus(200);
  ```
- json: Set content type to `application/json` and call send with strigified object
  ```js
  res.json({ Hello: 'World' });
  ```
- jsonBody: Calls JSON.parse on the response's body
- end: Freese the response and headers so they can no longer be modified

### Event

#### Properties

- type

#### Methods

- stopPropagation:If several event listeners are attached to the same event type, they are called in
  the order in which they were added. If `stopPropagation` is invoked during one such call, no
  remaining listeners will be called.
  ```js
  server.get('/session/:id').on('beforeResponse', (req, res, event) => {
    event.stopPropagation();
    res.setHeader('X-SESSION-ID', 'ABC123');
  });

  server.get('/session/:id').on('beforeResponse', (req, res, event) => {
    // This will never be reached
    res.setHeader('X-SESSION-ID', 'XYZ456');
  });
  ```

## Node Server

The `@pollyjs/node-server` package provides a standalone node server as well as an express integration
to be able to support the Rest Persister so recordings can be saved to an read from disk.

### Installation

```bash
# Via Npm
npm install @pollyjs/node-server -D
```

### Server

This packages includes a fully working standalone node server that is pre-configured with the necessary
APIs and middleware to support the REST Persister.

The Server constructor accepts a configuration object that can be a combination of the below listed Server &
API options. Once instantiated, you will have full access to the Express app via the app property.

```js
const { Server } = require('@pollyjs/node-server');
const server = new Server({
  quiet: true,
  port: 4000,
  apiNamespace: '/pollyjs'
});

// Add custom business logic to the express server
server.app.get('/custom', () => {
  /* Add custom express logic */
});

// Start listening and attach extra logic to the http server
server.listen().on('error', () => {
  /* Add http server error logic */
});
```

### Server Configuration

- __port__
- __host__
- __quiet__: Enable/disabel the logging middleware (morgan)

### API Configuration

- __recordingsDir__: Default: `recordings`. the root directory to store all recordings
- __apiNamespace__: Default: `polly`. The namespace to mount the polly API on. THis should really
  only be changed if there is a conflict with the default apiNamespace
- __recordingSizeLimit__: Default: `50mb`. A recording size can not exceed 50mb by default. If your
  application exceeds this limit, bump this value to a reasonable limit

## CLI

The `@polly/cli` package provides a standalone CLI to quickly get your setup ready to go.

### Installation

```bash
# with npm
npm install -g @pollyjs/cli
```

### Usage

```bash
  Usage: polly [options] [command]

  Options:

    -v, --version       output the version number
    -h, --help          output usage information

  Commands:

    listen|l [options]  start the server and listen for requests
```

### Commands

- __listen__: Start a node server and listen for Polly requests via the Rest Persister to be able to
  record and replay recordings to and from disk.

  ```bash
  Usage: polly listen|l [options]

  start the server and listen for requests

  Options:

    -H, --host <host>                host
    -p, --port <port>                port number (default: 3000)
    -n, --api-namespace <namespace>  api namespace (default: polly)
    -d, --recordings-dir <path>      recordings directory (default: recordings)
    -q, --quiet                      disable the logging
    -h, --help                       output usage information
  ```


