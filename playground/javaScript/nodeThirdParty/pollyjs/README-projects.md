# Polly.JS Samples Documentation

## client-server

Demonstrate Polly client-server intercepting requests and responding to HTTP events. Taken from
[GitHub](https://github.com/Netflix/pollyjs/tree/master/examples/client-server).

To run:
```bash
cd client-server
npm install
npm run test
```

## rest-persister

Demonstrate rest persister. Stores recordings in `rest-persister/recordings`. Taken from
[GitHub](https://github.com/Netflix/pollyjs/tree/master/examples/rest-persister).

To run:
```bash
cd rest-persister
npm install
npm run test
```

## node-fetch

Demonstrate Node HTTP Adapter and FS Persister. Taken from
[GitHub](https://github.com/Netflix/pollyjs/tree/master/examples/node-fetch).

To run:
```bash
cd node-fetch
npm install
npm run test
```

## cypress

A failed attempt of trying to integrate Polly.js with Cypress.

Tried recipes at the following URLs:
[Mock Visit](https://gist.github.com/marcuslindfeldt/4d6dcb528e6c1ab734a0f78e609eff6a),
[cypress-polly-xhr-adapter](https://github.com/cypress-io/cypress/issues/687),
[Polly-axios](https://github.com/Netflix/pollyjs/issues/119)

Tried to build from the following stack:
```text
  Cypress
   |
   |
  Application. Polly.JS injected
   |             |
   |             |
  Server       Rest Persister Server
```

To try and run:

```bash
cd cypress;

# Start persister server
npm run persister

# Start application and cypress in separate terminal.
# Unfortunately, the Polly.js library is unable to intercept the axios requests made in App.vue.
# Polly.js does not send any information down to the REST persister.
npm run test:e2e
```


