# Pug Technology Documentation

Pug is a high performance template engine for Node.js and browsers.

Relevant URLs:
[NPM](https://www.npmjs.com/package/pug),
[Home](https://pugjs.org/api/getting-started.html),
[GitHub](https://github.com/pugjs/pug),
[API Reference](https://pugjs.org/api/reference.html),
[Language Reference](https://pugjs.org/language/attributes.html)

## Installation

Install with npm: `npm install pug`

## Usage

### pug.compile()

`pug.compile()` will compile the Pug source code into a JavaScript function that takes a data object
called `locals` as an argument. Call resultant function with data to generate output.

```text
//- template.pug
p #{name}'s Pug source code!
```

```javascript
const pug = require('pug');

// Compile the source code
const compiledFunction = pug.compileFile('template.pug');

// Render a set of data
console.log(compiledFunction({ name: 'Timothy' }));   // "<p>Timothy's Pug source code!</p>"

// Render another set of data
console.log(compiledFunction({  name: 'Forbes' } ));  // "<p>Forbes's Pug source code!</p>"
```

### pug.render()

`pug.render()` family of functions combine compiling and rendering into one step. However, the
template function will be re-compiled every time `render` is called which might impact performance.
```javascript
const pug = require('pug');

// Compile template.pug, and render a set of data
console.log(pug.renderFile('template.pug', { name: 'Timothy' })); // "<p>Timothy's Pug source code!</p>"
```

