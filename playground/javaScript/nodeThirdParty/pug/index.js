/**
 * @file Pug Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let pug = require('pug');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Pug examples
 */
function pugFunc({
  compileFile = false,
  renderFile = false,
  all = false
} = {}) {
  if (compileFile || all) {
    logTitle({ title: 'compileFile' });
    let compiledFunction = pug.compileFile('nameTemplate.pug');
    console.log(compiledFunction({ name: 'Timothy' }));
    console.log(compiledFunction({ name: 'Forbes' }));
  }
  if (renderFile || all) {
    logTitle({ title: 'renderFile' });
    console.log(pug.renderFile('nameTemplate.pug', { name: 'Timothy' }));
  }
}


/* ------------------------ MAIN ------------------------------------------- */
pugFunc({ all: true });
