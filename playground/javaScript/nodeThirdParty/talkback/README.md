# Talkback Technology Documentation

Talkback allows for the recording and replaying of HTTP requests.
Talkback is a pure Javascript standalone HTTP proxy. As long as you have node.js in your environment,
you can run Talkback to record requests from applications written in any language/framework. Talkback
is used to accelerate your integration tests by running your application against mocked HTTP servers.

Relevant URLs:
[NPM](https://www.npmjs.com/package/talkback),
[GitHub](https://github.com/ijpiantanida/talkback#readme),
[Blog](https://blog.10pines.com/2017/12/18/isolating-integration-tests-from-external-http-services-with-talkback/)

## Installation

```bash
# Install in project with npm
npm install talkback
```

## Usage

Talkback is pretty easy to setup.
Define the following:
- which host it will be proxying
- which port it should list to
- where to find the saved tapes

Point applications to send relevant API requests to the talkback proxy. When a request arrives to
talkback, it will try to match the request against a previously saved tape and quickly return the
tape's response. If no tape matches the request, talkback will forward it to the origin
host, save the tape to disk for future uses, and finally return the response.
```js
const talkback = require("talkback");

const opts = {
  host: "https://api.myapp.com/foo",
  record: talkback.Options.RecordMode.NEW,
  port: 5544,
  path: "./my-tapes"
};
const server = talkback(opts);
server.start(() => console.log("Talkback Started"));
server.close();
```

### talkback(opts) Options

| Name              | Type                  | Description                                                                                                       | Default                   |
|:------------------|:----------------------|:------------------------------------------------------------------------------------------------------------------|:--------------------------|
| host              | String                | Where to proxy unknown requests                                                                                   |                           |
| port              | String                | Talkback port                                                                                                     | 8080                      |
| path              | String                | Path where to load and save tapes                                                                                 | ./tapes/                  |
| https             | Object                | HTTPS server options                                                                                              | Defaults                  |
| record            | String Function       | Set record mode. More info                                                                                        | RecordMode.NEW            |
| fallbackMode      | String Function       | Fallback mode for unknown requests when recording is disabled. More info                                          | FallbackMode.NOT_FOUND    |
| name              | String                | Server name                                                                                                       | Defaults to host value    |
| latency           | String Array Function | Synthetic latency for requests (in ms). More info                                                                 | 0                         |
| errorRate         | Number Function       | Probabilty between 0 and 100 of injecting a synthetic error. More info                                            | 0                         |
| tapeNameGenerator | Function              | Customize how a tape name is generated for new tapes.                                                             | null                      |
| ignoreHeaders     | [String]              | List of headers to ignore when matching tapes. Useful when having dynamic headers like cookies or correlation ids | ['content-length', 'host] |
| ignoreQueryParams | [String]              | List of query params to ignore when matching tapes. Useful when having dynamic query params like timestamps       | []                        |
| ignoreBody        | Boolean               | Should the request body be ignored when matching tapes                                                            | false                     |
| bodyMatcher       | Function              | Customize how a request's body is matched against saved tapes. More info                                          | null                      |
| urlMatcher        | Function              | Customize how a request's URL is matched against saved tapes. More info                                           | null                      |
| responseDecorator | Function              | Modify responses before they're returned. More info                                                               | null                      |
| silent            | Boolean               | Disable requests information console messages in the middle of requests                                           | false                     |
| summary           | Boolean               | Enable exit summary of new and unused tapes at exit. More info                                                    | true                      |
| debug             | Boolean               | Enable verbose debug information                                                                                  | false                     |

### HTTPS options

| Name     | Type    | Description           | Default |
|:---------|:--------|:----------------------|:--------|
| enabled  | Boolean | Enables HTTPS server  | false   |
| keyPath  | String  | Path to the key file  | null    |
| certPath | String  | Path to the cert file | null    |

## Tapes

Tapes can be freely edited to match new requests or return a different response than the original.
They are loaded recursively from the `path` directory at startup. They use JSON5 format. JSON5 is an
extension to the JSON format that allows for features like comments, trailing commas, and keys without
quotes.

### Format

All tapes have the following 3 properties:
- __meta__: Stores metadata about the tape
- __req__: Request object. Used to match incoming requests against the tape.
- __res__: Response object. The HTTP response that will be returned in case the tape matches a request

### File Name

New tapes will be created under the `path` directory with the name `unamed-n.json5`, where `n` is
the tape number. Tapes can be renamed at will, for example to give some meaning to the scenario the
tape represents. If a custom `tapeNameGenerator` is provided, it will be called to produce an
alternate file path under `path` that can be based on the tape contents. Not that the file extension
`.json5` will be appended automatically
```js
function nameGenerator(tapeNumber, tape) {
  // organize in folders by request method
  // e.g. tapes/GET/unnamed-1.json5
  //      tapes/GET/unnamed-3.json5
  //      tapes/POST/unnamed-2.json5
  return path.join(`${tape.req.method}`, `unnamed-${tapeNumber}`)
}
```

### Request and Response Body

If the content type of the request or response is considered human readable and uncompressed, the body
will be saved in plain text. Otherwise, the body will be saved as a Base64 string, allowing to save
binary content.

If the request or response have a JSON content-type, their body will be pretty printed as an object in
the tape for easier readability. This means differences in formatting are ignored when comparing tapes,
and any special formatting in the response will be lost.

## Recording Modes

Talkback proxying and recording behavior can be controlled through th e`record` and `fallbackMode`
options.

There are 3 possible recording modes:

| Value     | Description                                                                                                    |
|:----------|:---------------------------------------------------------------------------------------------------------------|
| NEW       | If no tape matches the request, proxy it and save the response to a tape                                       |
| OVERWRITE | Always proxy the request and save the response to a tape, overwriting any existing one                         |
| DISABLED  | If a matching tape exists, return it. Otherwise, don't proxy the request and use fallbackMode for the response |

There are 2 possible fallback modes:

| Value     | Description                                                                |
|:----------|:---------------------------------------------------------------------------|
| NOT_FOUND | Log an error and return a 404 response                                     |
| PROXY     | Proxy the request to host and return its response, but don't create a tape |

It is recommened to __DISABLE__ recording when using talkback for test running. This way, there
are no side-effects and broken tests fail faster.

Both options accept either one of the possible modes to be used for all requests or a function that
takes the request as a parameter and returns a valid mode.
```js
const talkback = require("talkback")

const opts = {
  record: talkback.Options.RecordMode.DISABLED,
  fallbackMode: (req) => {
    if (req.url.includes("/mytest")) {
        return talkback.Options.FallbackMode.PROXY
      }
      return talkback.Options.FallbackMode.NOT_FOUND
  }
}
```

## Custom Request Body Matcher

By default, in order for a request to match against against a saved tape, both request and tape need
to have the exact same body. There might be cases where this is too strict (Ex. body contains time
dependent bits) but enabling `ignoreBody` is too lax.

Talkback lets you pass a custom matching function as the `bodyMatcher` option.
The function will receive a saved tape and the current request, and it has to return whether they
should be considered a match on their body.

Body matching is the last step when matching a tape. In order for this function to be called, everything
else about the request should match the tape too (url, method, headers...). The `bodyMatcher` is not
called if tape and request bodies are already the same.

Example:
```js
function bodyMatcher(tape, req) {
    if (tape.meta.tag === "fake-post") {
      const tapeBody = JSON.parse(tape.req.body.toString());
      const reqBody = JSON.parse(req.body.toString());

      return tapeBody.username === reqBody.username;
    }
    return false;
}
```

Note that the tape's and request's bodies are `Buffer` objects.

## Custom Request URL Matcher

Similar to the `bodyMatcher`, there's the `urlMatcher` option, which will let you customize how a
request and a tape are matched on their URL.

Example:
```js
function urlMatcher(tape, req) {
    if (tape.meta.tag === "user-info") {
      // Match if URL is of type /users/{username}
      return !!req.url.match(/\/users\/[a-zA-Z-0-9]+/);
    }
    return false;
}
```

## Custom Request Decorator

By default talkback will just proxy requests to the host as they are. If you want to customize
requests before they're proxied (or looked up in stored tapes) you can do so through the
`requestDecorator` option.

Example:
```js
function requestDecorator(req) {
    delete req.headers['accept-encoding'];
    return req;
}
```

## Custom Response Decorator

If you want to add a little bit of dynamism to the respnse coming from a matching existing tape or
adjust the response that the proxied server returns, you can do so by using the `responseDecorator` option.
This can be useful for example if your response needs to contain an ID that gets sent on the request, or
if your response has a time dependent field.

Example:
```js
function responseDecorator(tape, req) {
  if (tape.meta.tag === "auth") {
    const tapeBody = JSON.parse(tape.res.body.toString())
    const expiration = new Date()
    expiration.setDate(expiration.getDate() + 1)
    const expirationEpoch = Math.floor(expiration.getTime() / 1000)
    tapeBody.expiration = expirationEpoch

    const newBody = JSON.stringify(tapeBody)
    tape.res.body = Buffer.from(newBody)
  }
  return tape
}
```

## Latency

By default Talkback will try to reply to requests as fast as it can, but sometimes it's useful to
understand how application behave under real-world or even undesirably high response times.
Talkback lets you control response times both at a _global_ or at a _tape level_.

The `latency` option will aplly for all requests that match an existing tap or when using the
`PROXY fallback mode`. There are 3 possible types of values:
- A number: fixed number of milliseconds
- An array in the form `[min, max]`: Requests will take a random number of milliseconds in the given
  range
- A function `(req) => latency`: The function will be called for each request and it should return
  the desired number of milliseconds for the response time

Moreover, tapes can define their own specific response times by adding a `latency` property to the
`meta` object. This property accepts both numbers and ranges and will take precedence over the global
latency option. For example:
```json
{
  "meta": {
    "createdAt": "2017-09-10T23:19:27.010Z",
    "host": "http://localhost:8898",
    "resHumanReadable": true,
    "latency": [100, 500]
  },
  ...
}
```

## Error Rate

Similar to what the `latency` option does, you might want to test how your application behaves when
downstream services start failing. Talkback can aid here through the `errorRate` option, by returning
synthetic 503 errors back to your application.

2 possible values for `errorRate`:
- A number between 0 and 100 that defines the probability of returning an error for each request
- A function `(req) => errorRate`. The function will be called for each request and it should return
  the desired probability of error for that specific request.

Error rates can also be added to the request `meta` object:
```json
{
  "meta": {
    "createdAt": "2017-09-10T23:19:27.010Z",
    "host": "http://localhost:8898",
    "resHumanReadable": true,
    "errorRate": 50
  },
  ...
}
```

## Exit Summary

If you are using Talkback for your test suite, you will probably have tons of different tapes after
some time. It can be difficult to know if all of them are still required. To help, when talkback exits,
it will print a list of all the tapes that have NOT been used and a list of all the new tapes. If your
test suite is green, you can sefely delete anything that hasn't been used.
```text
===== SUMMARY (My Server) =====
New tapes:
- unnamed-4.json5
Unused tapes:
- not-valid-request.json5
- user-profile.json5
```

This can be disabled with the `summary` option.
