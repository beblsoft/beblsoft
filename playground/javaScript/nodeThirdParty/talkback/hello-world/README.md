# Talkback Hello World Project

## Architecture

Project has the following Stack
```text

  Web Client: index.html       - runs in Browser
      |
    REST Requests
      |
  Talkback Server: talkback.js - intercepts server requests and saves to tapes
      |
      |
  Server: server.js            - Answers client requests
```

## Running

```bash
# Install dependencies
npm install

# Start Server
node server.js

# Start Talkback Server
node talkback.js

# Open index.html in browser
# Click on buttons, this will cause requests to be sent to Talkback server. The Talkback server will
# forward the requests to the server and then save the responses for playback. After a while no requests
# will make it to the server.
```
