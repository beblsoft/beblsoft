/**
 * @file Routing Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let express = require('express');
let cors = require('cors');


/* ------------------------ GLOBALS ---------------------------------------- */
let app = express();
app.use(cors());
app.all("*", function (req, resp, next) {  // Add middleware to log requests
   console.log(`${req.method} ${req.baseUrl} ${req.path}`);
   next();
});
let port = 3000;


/* ------------------------ ROUTES ----------------------------------------- */

app.get('/get', function (req, res) {
  res.send('GET request!');
});

app.post('/post', function (req, res) {
  res.send('POST request!');
});

app.put('/put', function (req, res) {
  res.send('PUT request!');
});

app.delete('/delete', function (req, res) {
  res.send('DELETE request!');
});


/* ------------------------ MAIN ------------------------------------------- */
app.listen(port, () => { console.log(`Listening on port ${port}!`); });
