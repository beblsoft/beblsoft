/**
 * @file Talkback Server to Record and Replay Requests
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let talkback = require('talkback');
let path = require('path');


/* ------------------------ GLOBALS ---------------------------------------- */
let opts = {
  host: 'http://localhost:3000',
  record: talkback.Options.RecordMode.NEW,
  port: 5544,
  path: path.resolve(__dirname, 'tapes')
};

/* ------------------------ MAIN ------------------------------------------- */
const server = talkback(opts);
server.start(() => { console.log('Talkback Started'); });
// server.close();
