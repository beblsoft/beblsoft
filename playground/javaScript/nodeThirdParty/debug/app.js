/**
 * @file Sample application
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let debug = require('debug')('http');
let http = require('http');
let _ = require('./worker');
let name = 'My App';
let port = 3000;

/* ------------------------ MAIN ------------------------------------------- */
debug('booting %o', name);

http.createServer(function (req, res) {
  debug(`${req.method} ${req.url}`);
  res.end(`Response for: ${req.url}\n`);
}).listen(port, function () {
  debug(`listening on http://localhost:${port}`);
});
