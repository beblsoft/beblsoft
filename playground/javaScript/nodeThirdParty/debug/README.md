# Debug Technology Documentation

Debug is a tiny JavaScript debugging utility modelled after Node.js core's debugging technique.
Works in Node.js and web browsers.

Relevant URLs:
[NPM](https://www.npmjs.com/package/debug),
[GitHub](https://github.com/visionmedia/debug)

## Installation

Install with npm: `npm install debug`

## Usage

`debug` exposes a function; simply pass this function the current module name, and it will return
a decorated version of `console.error` for printing debug statements.

For example, `helloWorld.js`:

```javascript
let debug = require('debug')('helloWorld');
debug('HelloWorld!');
```

The `DEBUG` variable is then used to enable modules based on space or comma-deliminted names.
This allows developers to toggle the debug output for different parts of their applications.

```bash
$ node helloWorld.js
# nothing printed

$ DEBUG=helloWorld node helloWorld.js
helloWorld HelloWorld! +0ms

$ DEBUG=* node helloWorld.js
helloWorld HelloWorld! +0ms
```

## Enable Dynamically

Debug can be enabled dynamically as follows:

```javascript
let debug = require('debug');

console.log(debug.enabled('test')); // false
debug.enable('test');
console.log(debug.enabled('test')); // true
debug.disable();
console.log(debug.enabled('test')); // false
```

## Conditionally Debugging Code

After creating debugging instance, one can conditionally check whether it is enabled as follows:

```javascript
const debug = require('debug')('http');

if (debug.enabled) {
  // do stuff...
}
```
