/**
 * @file Sample application workers
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let a = require('debug')('worker:a');
let b = require('debug')('worker:b');


/* ------------------------ WORKER A --------------------------------------- */
function workerA() { /* eslint-disable-line */
  a('Worker A!');
  setTimeout(workerA, Math.random() * 5000);
}
workerA();


/* ------------------------ WORKER B --------------------------------------- */
function workerB() { /* eslint-disable-line */
  b('Worker B!');
  setTimeout(workerB, Math.random() * 5000);
}
workerB();
