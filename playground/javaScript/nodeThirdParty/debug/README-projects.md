# Debug Samples Documentation

## helloWorld.js

Debug helloWorld program.

To run:
- Install: `npm install`
- Run with no output: `node helloWorld.js`
- Run with output: `DEBUG=helloWorld node helloWorld.js`

## app.js

Demonstrate debug used for multiple namespaces in a sample web app.

To run:
- Install: `npm install`
- Run with no output: `node app.js`
- Run with app output: `DEBUG=http node app.js`
- Run with worker output: `DEBUG=worker:* node app.js`
- Run with all output: `DEBUG=* node app.js`
