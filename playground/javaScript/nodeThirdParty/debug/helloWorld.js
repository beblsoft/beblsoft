/**
 * @file Debug hello world
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let debug = require('debug')('helloWorld');

/* ------------------------ MAIN ------------------------------------------- */
debug('HelloWorld!');
