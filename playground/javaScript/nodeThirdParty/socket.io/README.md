# Socket.io Technology Documentation

Socket.io enables real-time bidirectional event-based communication. It consists of:

- __socket.io__: A Node.js server
- __socket.io-client__: A JavaScript client library

Features:

- __Reliability__: connections established through proxies and firewalls. It relies on Engine.io which
  establishes a long-polling connection.
- __Auto-reconnection support__: Unless instructed otherwise, a disconnected client will try to reconnect
  forever, until the server is available again.
- __Disconnection detection__: A heartbeat mechanism is implemented at the Engine.io level, allowing both
  the server and the client to know when the other one is not responding anymore
  This functionality is achieved with timers set on both the server and the client, with timeout
  values (the `pingInterval` and `pingTimeout` parameters) shared during the connection handshake.
  Those timers require any subsequent client calls to be directed to the same server, hence the
  `sticky-session` requirement when using multiple nodes.
- __Binary support__: any serializable data structures can be emitted
- __Simple API__
- __Cross-browser__
- __Multiplexing Support__: Can create several `Namespaces`, whcih act as separate communication channels
  but will share the same underlying connection.
- __Room support__: Within each `Namespace`, can define arbitrary channels, called `Rooms`, that sockets
  can join and leave. You can then broadcast to any given room, reaching every socket that has joined it.

Note: Socket.IO is __not__ a WebSocket implementation. Although Socket.IO indeed uses WebSocket as a
transport when possible, it adds some metadata to each packet: the packet type, the namespace, and
the ack id when a message acknowledgement is needed. A WebSocket client will not be able to connect
to a Socket.IO server and a Socket.IO client will not be able to connect to a WebSocket server.

Relevant URLs:
[NPM](https://www.npmjs.com/package/socket.io),
[GitHub](https://github.com/socketio/socket.io),
[Protocol Spec](https://github.com/socketio/socket.io-protocol)

## Installation

Install with npm: `npm install socket.io`
