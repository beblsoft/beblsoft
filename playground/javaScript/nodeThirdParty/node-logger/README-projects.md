# Node Logger Projects Documentation

__node-logger__ module example. Demonstrates logging at different levels: debug, info, ..., fatal.

To Run:
- Install Software : `npm install`
- Run program      : `npm run main`
