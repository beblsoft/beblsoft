/**
 * @file node-logger example usage
 *
 * Logging Levels: 'debug', 'info', 'warn', 'error', 'fatal'
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let loggerPackage = require('node-logger');


/* ------------------------ GLOBALS ---------------------------------------- */
// let logger = loggerPackage.createLogger('test.log')  // Log to file
let logger = loggerPackage.createLogger(); // Log to STDOUT

/* ------------------------ MAIN ------------------------------------------- */
// Logging Configuration
logger.setLevel('debug');
logger.format = function(level, date, message) {
  return `${date.getTime()} : ${message}`;
};

// Log Messages
logger.debug('Debug Message');
logger.info('Info Message');
logger.warn('Warn Message');
logger.error('Error Message');
logger.fatal('Fatal Message');
