# node-logger Technology Documentation

__node-logger__ is a simple logging library that combines the simple API's of Ruby's logger.rb
and browser-js console.log()

Relevant URLs:
[NPM](https://www.npmjs.com/package/logger)

