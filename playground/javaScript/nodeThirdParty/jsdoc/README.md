# JSDoc Technology Documentation

__JSDoc 3__ is an API documentation generator for JavaScript, similar to __Javadoc__ or __phpDocumentor__.
You add documentation comments directly to your source code, right alongside the code itself.
The JSDoc tool will scan your source code and generate an HTML documentation website for you.

Relevant URLs:
[Github](https://github.com/jsdoc3/jsdoc),
[Examples](http://usejsdoc.org/)

