/**
 * @file JSDoc Class Example
 */


/* ------------------------------------------------------------------------- */
/**
 * Class representing a point
 */
class Point {

  /**
   * @constructor
   * @param  {number} x - The x value
   * @param  {number} y - The y value
   */
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Get the x value
   * @return {number} The x value
   */
  getX() {
    return this.x;
  }

  /**
   * Get the y value
   * @return {number} The y value
   */
  getY() {
    return this.y;
  }

  /**
   * Say hello!
   * @param {string} name - hello name
   */
  static logVals() {
    console.log(`Hello ${name}!`);
  }
}

/* ------------------------------------------------------------------------- */
/**
 * Class representing a dot
 * @extends {Point}
 */
class Dot extends Point {

  /**
   * @constructor
   * @param {number} x - The x value
   * @param {number} y - The y value
   * @param {number} width - Dot width
   */
  constructor(x, y, width) {
    super(x, y);
    this.width = width;
  }

  /**
   * Get the width value
   * @return {number} The width value
   */
  getWidth() {
    return this.y;
  }
}


/* ------------------------------------------------------------------------- */
/**
 * @module class
 */
module.exports = {
  Point,
  Dot
};

