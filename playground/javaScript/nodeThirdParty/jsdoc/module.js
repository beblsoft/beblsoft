/**
 * @file JSDoc Module Example
 */


/**
 * Color mixer.
 * @module color/mixer
 */
module.exports = {

  /**
   * Foo function
   */
  foo: function() {
    // ...
  },

  /**
   * Bar function
   */
  bar: function() {
    // ..
  }
};
