/**
 * @file JSDoc Function Example
 */


/* --------------------------- FUNCTIONS ----------------------------------- */
/**
 * Usage of basic parameters
 * @param {string} somebody - Somebody's name.
 * @return {number} 0
 */
function basicParams(somebody) {
  console.log(`Hello ${somebody}`);
  return 0;
}

/**
 * Usage of different parameter types
 * @param  {boolean} bool
 * @param  {(number|boolean)} numberOrBool
 * @param  {number[]} array
 * @param  {?number} nullable
 * @param  {!number} nonNullable
 * @param  {...number} variableNumber
 * @param  {number} [optional]
 */
function differentParams(bool, numberOrBool, array, nullable, nonNullable,
	variableNumber, optional) {
	console.log(bool);
	console.log(numberOrBool);
	console.log(array);
	console.log(nullable);
	console.log(variableNumber);
	console.log(optional);
}

/**
 * Assign the project to a list of employees.
 * @param {Object[]} employees - The employees who are responsible for the project.
 * @param {string} employees[].name - The name of an employee.
 * @param {string} employees[].department - The employee's department.
 */
function arrayParams(employees) {
  console.log(employees);
}

/**
 * This callback type is called `requestCallback` and is displayed as a global symbol.
 *
 * @callback requestCallback
 * @param {number} responseCode
 * @param {string} responseMessage
 */

/**
 * Does something asynchronously and executes the callback on completion.
 * @param {requestCallback} cb - The callback that handles the response.
 */
function doSomethingAsynchronously(cb) {
  // Something async
  cb();
}

/**
 * @throws {Error}
 */
function throwError() {
  throw Error;
}

/**
 * @todo Write the documentation.
 * @todo Implement this function.
 */
function todoFunction() {
  // write me
}

/**
 * Provides access to user information.
 * @since 1.0.1
 */
function sinceFunction() {

}

/**
 * Generator function
 *
 * @generator
 * @yields {number} The next number in the Fibonacci sequence.
 */
function generatorFunction() {

}

/* --------------------------- EXPORTS ------------------------------------- */
/**
 * @module params
 */
module.exports = {
  basicParams,
  differentParams,
  arrayParams,
  doSomethingAsynchronously,
  throwError,
  todoFunction,
  sinceFunction,
  generatorFunction
};
