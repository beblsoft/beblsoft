/**
 * @file Hello World Example
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let winston = require('winston');


/* ------------------------ GLOBALS ---------------------------------------- */
let format = winston.format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});
let ignorePrivate = winston.format((info, opts) => {
  if (info.private) { return false; }
  return info;
});
let logger = winston.createLogger({
  level: 'info',
  levels: winston.config.npm.levels, // error, warn, info, verbose, debug
  format: winston.format.combine(
    ignorePrivate(),
    winston.format.label({ label: 'Sample Label!' }),
    winston.format.timestamp(),
    winston.format.colorize(),
    winston.format.json(),
    format
  ),
  defaultMeta: { foo: 'bar' },
  transports: [
    new winston.transports.File({ filename: 'helloWorld.log', level: 'debug' }),
    new winston.transports.Console({ level: 'debug', format: winston.format.simple() })
  ],
  exitOnError: true,
  silent: false
});


/* ------------------------ MAIN ------------------------------------------- */
logger.log({ level: 'error', message: 'Public error to share' });
logger.log({ level: 'error', message: 'Private error to filter', private: true });
logger.error('Error!');
logger.warn('Warn!');
logger.info('Info!');
logger.verbose('Verbose!');
logger.debug('Debug!');
