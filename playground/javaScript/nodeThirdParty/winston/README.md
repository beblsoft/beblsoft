# Winston Technology Documentation

Winston is designed to be a simple and universal logging library with support for multiple
transports. A transport is a storage device for your logs. Each winston logger can have multiple
transports configured at different levels. For example, one might want error logs to be stored in
a persistent remote location, while sending all logs directly to the console.

Relevant URLs:
[npm](https://www.npmjs.com/package/winston),
[GitHub](https://github.com/winstonjs/winston)

## Installation

Install via npm: `npm install winston`

## Usage

See `helloWorld.js`
