# Winston Projects Documentation

## helloWorld.js

Winston Hello World Program. Creates a single logger with a console transport and a file transport.
It then writes messages to the logger.

To run:
- Install: `npm install`
- Run: `node helloWorld.js`