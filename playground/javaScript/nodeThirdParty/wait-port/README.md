# wait-port Technology Documentation

wait-port is a simple binary to wait for a port to open or an HTTP endpoint to successfully respond.

Relevant URLs:
[npm](https://www.npmjs.com/package/wait-port),
[GitHub](https://github.com/dwmkerr/wait-port#readme)

## Basic Usage

```bash
# Install via npm:
npm install wait-port

# Wait for port 8080 indefinitely
npx wait-port 8080

# Wait for http connection for 10 seconds
npx wait-port --timeout 10000 http://localhost:8021/
```

## Usage

```bash
npx wait-port --help
$ npx wait-port
Usage: wait-port [options] <target>

Wait for a target to accept connections, e.g: wait-port localhost:8080

Options:
  -V, --version        output the version number
  -t, --timeout [n]    Timeout (ms)
  -o, --output [mode]  Output mode (silent, dots). Default is silent.
  -h, --help           output usage information
  Examples:

    $ wait-port 3000
    $ wait-port -t 10 :8080
    $ wait-port google.com:443
    $ wait-port http://localhost:5000/healthcheck

```

## Error Codes

| Code | Meaning                                                                                                              |
|:-----|:---------------------------------------------------------------------------------------------------------------------|
| 0    | The specified port on the host is accepting connections.                                                             |
| 1    | A timeout occured waiting for the port to open.                                                                      |
| 2    | Un unknown error occured waiting for the port to open. The program cannot establish whether the port is open or not. |

## API Usage

You can use `wait-port` programmatically:
```javascript
const waitPort = require('wait-port');

const params = {
  host: 'google.com',
  port: 443,
  timeout: 10000, //ms
};

waitPort(params)
  .then((open) => {
    if (open) console.log('The port is now open!');
    else console.log('The port did not open before the timeout...');
  })
  .catch((err) => {
    console.err(`An unknown error occured while waiting for the port: ${err}`);
  });
```

