# Wait-Port Projects Documentation

## helloWorld.js

Wait-port hello World program. Creates waits on www.google.com to come online.

To run:
- Install: `npm install`
- Run: `node helloWorld.js`