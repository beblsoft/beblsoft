/**
 * @file Wait Port Hello World Example
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let waitPort = require('wait-port');


/* ------------------------ MAIN ------------------------------------------- */
waitPort({
    host: 'google.com',
    port: 443,
    timeout: 10000
  })
  .then((open) => {
    if (open) console.log('The port is now open!');
    else console.log('The port did not open before the timeout...');
  })
  .catch((err) => {
    console.err(`An unknown error occured while waiting for the port: ${err}`);
  });
