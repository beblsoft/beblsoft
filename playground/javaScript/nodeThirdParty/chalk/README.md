# Chalk Technology Documentation

Chalk is a module used for terminal styling.

Highlights:
- Expressive API
- Highly performant
- Ability to nest styles
- 256/Truecolor support
- Auto-detects color support
- Doesn't extend String.prototype
- Clean and focused
- Actively maintained

Relevant URLs: [NPM](https://www.npmjs.com/package/chalk)

## Installation

```bash
npm install chalk
```

## Basic Usage

```javascript
const chalk = require('chalk');

console.log(chalk.blue('Hello world!'));
```

## API

Chalk allows you to chain styles together as follows:

```javascript
// Chainer Syntax
chalk.<style>[.<style>...](string, [string...])

// Examples
chalk.red.bold.underline('Hello', 'World');
chalk.underline.white.bgKeyword('black')('Underline, white, background black')
```

Chalk can be disabled globally by setting `chalk.enabled = false;`

Chalk supports the following color levels via `chalk.level = #;`:
1. All colors are disabled
2. Basic color support (16 colors)
3. 256 color support
4. Truecolor support (16 million colors)

Check whether the terminal supports color with `chalk.supportsColor;`

## Styles

### Modifiers

- __reset__
- __bold__
- __dim__
- __italic__
- __underline__
- __inverse__
- __hidden__
- __strikethrough__
- __visible__

Example: `chalk.bold('Bold!);`

### Colors

- __black__
- __red__
- __green__
- __yellow__
- __blue__
- __magenta__
- __cyan__
- __white__
- __gray__
- __redBright__
- __greenBright__
- __blueBright__
- __magentaBright__
- __cyanBright__
- __whiteBright__

Example: `chalk.red('Red!);`

### Background colors

- __bgBlack__
- __bgRed__
- __bgGreen__
- __bgYellow__
- __bgBlue__
- __bgMagenta__
- __bgCyan__
- __bgWhite__
- __bgBlackBright__
- __bgRedBright__
- __bgGreenBright__
- __bgYellowBright__
- __bgBlueBright__
- __bgMagentaBright__
- __bgCyanBright__
- __bgWhiteBright__

Example: `chalk.bgRed('Background Red!);`

## Truecolor Support

Chalk supports 256 colors and _Truecolor_ (16 million colors) on supported terminals.

The following color models can be used:
- __rgb__    : `chalk.rgb(255, 136, 0).bold('Orange!')`
- __hex__    : `chalk.hex('#FF8800').bold('Orange!')`
- __keyword__: `chalk.keyword('orange').bold('Orange!')`
- __hsl__    : `chalk.hsl(32, 100, 50).bold('Orange!')`
- __hsv__    : `chalk.hsv(32, 100, 100).bold('Orange!')`
- __hwb__    : `chalk.hwb(32, 0, 50).bold('Orange!')`
