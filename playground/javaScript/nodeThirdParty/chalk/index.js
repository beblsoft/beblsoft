/**
 * @file Chalk Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let chalk = require('chalk');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 * @param  {String}  options.title   Title
 * @param  {integer} options.nDashes number of dashes to print
 */
function logTitle({ title, nDashes = 80 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}


/* ------------------------ CHALK DEMONSTRATION FUNCTIONS ------------------ */
/**
 * log modifiers
 */
function logModifiers() {
  logTitle({ title: 'Modifiers' });
  console.log(chalk.reset('Modifier: reset'));
  console.log(chalk.bold('Modifier: bold'));
  console.log(chalk.dim('Modifier: dim'));
  console.log(chalk.italic('Modifier: italic'));
  console.log(chalk.underline('Modifier: underline'));
  console.log(chalk.inverse('Modifier: inverse'));
  console.log(chalk.hidden('Modifier: hidden'));
  console.log(chalk.strikethrough('Modifier: strikethrough'));
  console.log(chalk.visible('Modifier: visible'));
}

/**
 * log colors
 */
function logColors() {
  logTitle({ title: 'Colors' });
  console.log(chalk.red('Color: red'));
  console.log(chalk.black('Color: black'));
  console.log(chalk.green('Color: green'));
  console.log(chalk.yellow('Color: yellow'));
  console.log(chalk.blue('Color: blue'));
  console.log(chalk.magenta('Color: magenta'));
  console.log(chalk.cyan('Color: cyan'));
  console.log(chalk.white('Color: white'));
  console.log(chalk.gray('Color: gray'));
  console.log(chalk.redBright('Color: redBright'));
  console.log(chalk.greenBright('Color: greenBright'));
  console.log(chalk.blueBright('Color: blueBright'));
  console.log(chalk.magentaBright('Color: magentaBright'));
  console.log(chalk.cyanBright('Color: cyanBright'));
  console.log(chalk.whiteBright('Color: whiteBright'));
}

/**
 * log background colors
 */
function logBackgroundColors() {
  logTitle({ title: 'Background Colors' });
  console.log(chalk.bgBlack('Background Color:bgBlack'));
  console.log(chalk.bgRed('Background Color:bgRed'));
  console.log(chalk.bgGreen('Background Color:bgGreen'));
  console.log(chalk.bgYellow('Background Color:bgYellow'));
  console.log(chalk.bgBlue('Background Color:bgBlue'));
  console.log(chalk.bgMagenta('Background Color:bgMagenta'));
  console.log(chalk.bgCyan('Background Color:bgCyan'));
  console.log(chalk.bgWhite('Background Color:bgWhite'));
  console.log(chalk.bgBlackBright('Background Color:bgBlackBright'));
  console.log(chalk.bgRedBright('Background Color:bgRedBright'));
  console.log(chalk.bgGreenBright('Background Color:bgGreenBright'));
  console.log(chalk.bgYellowBright('Background Color:bgYellowBright'));
  console.log(chalk.bgBlueBright('Background Color:bgBlueBright'));
  console.log(chalk.bgMagentaBright('Background Color:bgMagentaBright'));
  console.log(chalk.bgCyanBright('Background Color:bgCyanBright'));
  console.log(chalk.bgWhiteBright('Background Color:bgWhiteBright'));
}

/**
 * log true colors
 */
function logTrueColors() {
  logTitle({ title: 'True Colors' });
  console.log(chalk.hex('#338CFF')('Hex Colors'));
  console.log(chalk.keyword('orange')('Keyword'));
  console.log(chalk.rgb(15, 100, 204)('RBG'));
  console.log(chalk.bgHex('#338CFF')('Background HEX'));
  console.log(chalk.bgKeyword('orange')('Background Keyword'));
  console.log(chalk.bgRgb(15, 100, 204)('Background RBG'));
}

/**
 * log chains on chalk functions
 */
function logChains() {
  logTitle({ title: 'Chains' });
  console.log(chalk.bold.red.bgBlack('Bold, red, background black'));
  console.log(chalk.underline.white.bgKeyword('black')('Underline, white, background black'));
}

/**
 * log different themes
 */
function logThemes() {
  logTitle({ title: 'Themes' });
  const error = chalk.bold.red;
  const warning = chalk.keyword('orange');

  console.log(error('Error!'));
  console.log(warning('Warning!'));
}

/**
 * log tagged template literal
 */
function logTaggedTemplateLiteral() {
  logTitle({ title: 'Tagged Template Literal' });
  const foo = { bar: 10, bur: 11, bir: 12 };

  // ES2015 template literal
  console.log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);

  // ES2015 tagged template literal
  console.log(chalk`
CPU: {red ${foo.bar}%}
RAM: {green.bold ${foo.bar / foo.bur * 100}% }
DISK: {rgb(255,131,0) ${foo.bar / foo.bir * 100}% }
`);
}


/* ------------------------ MAIN ------------------------------------------- */
logModifiers();
logColors();
logBackgroundColors();
logTrueColors();
logChains();
logThemes();
logTaggedTemplateLiteral();
