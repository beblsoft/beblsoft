# Browserify Documentation

Browserify lets you require('modules') in the browser by bundling up all of your dependencies.

Relevant URLs:
[Home](http://browserify.org/),
[GitHub Code](https://github.com/browserify/browserify),
[GitHub Handbook](https://github.com/browserify/browserify-handbook),
[Demos](http://browserify.org/demos.html)

## Installation

```bash
npm install -g browserify
```

## Hello World

Browsers don't have the `require` method defined, but Node.js does. With Browserify you can write
code that uses `require` in the same way that you would use it in Node.

Example main.js
```js
var unique = require('uniq');

var data = [1, 2, 2, 3, 4, 5, 5, 5, 6];

console.log(unique(data));
```

Install `uniq` with `npm`:
```bash
npm install uniq
```

Now recursively bundly up all the required modules starting at `main.js` into a single file called
`bundle.js` with the `browserify` command:

```bash
browserify main.js -o bundle.js
```

Browserify parses the Abstract Syntax Tree for `require()` calls to traverse the entire dependency
graph of your object.

Drop a single `<script>` tag into your html and you're done!

```html
<script src="bundle.js"></script>
```
