# Lolex Technology Docuemtation

Lolex is a JavaScript implementation of timer APIs: `setTimeout`, `clearTimeout`, `setImmediate`,
`clearImmediate`, `setInterval`, `clearInterval`, `requestAnimationFrame`, `cancelAnimationFrame`,
`requestIdleCallback`, and `cancelIdleCallback`, along with a clock instance that controls the
flow of time.

Lolex can be used to simulate passing time in automated tests and other situations where you want
the scheduling semantics but don't actually want to wait.

Relevant URLs:
[NPM](https://www.npmjs.com/package/lolex),
[Github](https://github.com/sinonjs/lolex)

## Installation

Install with npm: `npm install lolex`

## Usage

To use lolex, create a new clock, schedule events on it using the timer functions, and pass time
using the `tick` method.

```javascript
// In the browser distribution, a global `lolex` is already available
var lolex = require("lolex");
var clock = lolex.createClock();

clock.setTimeout(function () {
    console.log("Hello World!");
}, 15);

clock.tick(15);
```

## API Reference

### let clock = lolex.createClock([now[, loopLimit]])

Creates a clock. The default epoch is 0.

### let clock = lolex.install([config])

Installs lolex using the specified config. Options:

| Parameter                | Type        | Default                                                                                                                                                                                                         | Description                                                                                                                                                                                                               |
|:-------------------------|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| config.target            | Object      | global                                                                                                                                                                                                          | installs lolex onto the specified target context                                                                                                                                                                          |
| config.now               | Number/Date | 0                                                                                                                                                                                                               | installs lolex with the specified unix epoch                                                                                                                                                                              |
| config.toFake            | String[]    | ["setTimeout", "clearTimeout", "setImmediate", "clearImmediate","setInterval", "clearInterval", "Date", "requestAnimationFrame", "cancelAnimationFrame", "requestIdleCallback", "cancelIdleCallback", "hrtime"] | an array with explicit function names to hijack. When not set, lolex will automatically fake all methods except nextTick e.g., lolex.install({ toFake: ["setTimeout","nextTick"]}) will fake only setTimeout and nextTick |
| config.loopLimit         | Number      | 1000                                                                                                                                                                                                            | the maximum number of timers that will be run when calling runAll()                                                                                                                                                       |
| config.shouldAdvanceTime | Boolean     | false                                                                                                                                                                                                           | tells lolex to increment mocked time automatically based on the real system time shift (e.g. the mocked time will be incremented by 20ms for every 20ms change in the real system time)                                   |
| config.advanceTimeDelta  | Number      | 20                                                                                                                                                                                                              | relevant only when using with shouldAdvanceTime: true. increment mocked time by advanceTimeDelta ms every advanceTimeDelta ms change in the real system time.                                                             |

### let id = clock.setTimeout(callback, timeout)

Schedules the callback to be fired once timeout ms have ticked by.

### clock.clearTimeout(id)

Clears the timer given the ID or timer object

### let id = clock.setInterval(callback, timeout)

Schedules the callback to be fired every time `timeout` ms have ticked by.

### clock.clearInterval(id)

Clear the timer given the ID or timer object as long as it was created using setInterval.

### let id = clock.setImmediate(callback)

Schedules the callback to be fired once `0` milliseconds have ticked by. Note that you'll still
have to call `clock.tick()` for the callback to fire.

### clock.clearImmediate(id)

Clears the timer given the ID or timer object, as long as it was created using `setImmediate`.

### let id = clock.requestIdelCallback(callback[, timeout])

Queued the callback to be fired during idle periods to perform background and low priority work
on the main event loop. Callbacks which have a timeout option will be fired no later than timeout
in milliseconds.

### clock.cancleIdleCallback(id)

Cancels the callback scheduled by the provided id.

### clock.countTimers()

Returns the number of waiting timers. This can be used to assert that a test finishes without
leaking any timers.

### clock.nextTick(callback)

Only available in Node.js. Mimics `process.nextTick` to enable completely synchronous testing flows.

### clock.tick(time)

Advance the clock, firing callbacks if necessary. `time` may be the number of milliseconds to advance
the block by or a human-readable string. Valid strings:
- "08": 8 seconds
- "01:00": 1 minute
- "02:34:10": 2 hours, 34 minutes, 10 seconds

### clock.next()

Advances the clock to the moment of the first scheduled timer, firing it.

### clock.reset()

Removes all timers and ticks without firing them and resets `now`.

### clock.runAll()

Runs all pending timers until there are none remaining.

### clock.runMicrotasks()

Runs all pending microtasks scheduled with `nextTick` but none of the timers.

### clock.runToLast()

This takes note of the last scheduled timer when it is run, and advances the clock to that time
firing callbacks as necessary.

### clock.setSynstemTime([now])

This simulates a user changing the system clock while your program is running. It affects the current
time but it does not in itself cause timers to fire. They will fire exactly as they would have done
without the call to setSystemTime().

### clock.uninstall()

Restores the original methods on the `target` that was passed to `lolex.install`, or the native timers
if no target was given.


