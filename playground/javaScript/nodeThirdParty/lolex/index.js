/**
 * @file Lolex Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let lolex = require('lolex');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Lolex examples
 */
function lolexFunc({
  createClock = false,
  fakeGlobalTimeouts = false,
  autoIncGlobalTime = false,
  all = false
} = {}) {
  if (createClock || all) {
    logTitle({ title: 'createClock' });
    let clock = lolex.createClock();
    clock.setTimeout(function () {
      console.log('Created lolex clock and fired event');
    }, 5000);
    clock.tick(5000);
  }
  if (fakeGlobalTimeouts || all) {
    logTitle({ title: 'fakeGlobalTimeouts' });
    let clock = lolex.install();
    setTimeout(function () {
      console.log('Faking global timeouts');
    }, 5000);
    clock.tick(5000);
    clock.uninstall();
  }
  if (autoIncGlobalTime || all) {
    logTitle({ title: 'autoIncGlobalTime' });
    let clock = lolex.install({ shouldAdvanceTime: true, advanceTimeDelta: 40 });
    setImmediate(() => {
      console.log('setImmediate'); //executed after 40ms
    });
    setTimeout(() => {
      console.log('setTimeout1'); //executed after 40ms
    }, 30);
    setTimeout(() => {
      console.log('setTimeout2'); //executed after 80ms
      clock.uninstall();
    }, 50);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
lolexFunc({ all: true });
