# Semver Projects Docuemtation

## index.js

Demonstrate semantic version function usage. Among other things demonstrates version validation,
incrementing, parsing, comparisons, range intersections, and coercion.

To run:
- Install: `npm install`
- Run: `node index.js`