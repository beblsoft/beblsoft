/**
 * @file Semver Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let semver = require('semver');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Semver examples
 */
function versions({
  valid = false,
  inc = false,
  prerelease = false,
  major = false,
  minor = false,
  patch = false,
  intersects = false,
  parse = false,
  gt = false,
  gte = false,
  lt = false,
  lte = false,
  eq = false,
  neq = false,
  cmp = false,
  compare = false,
  rcompare = false,
  diff = false,
  validRange = false,
  satisfies = false,
  maxSatisfying = false,
  minSatisfying = false,
  minVersion = false,
  gtr = false,
  ltr = false,
  outside = false,
  coerce = false,
  all = false
} = {}) {
  if (valid || all) {
    logTitle({ title: 'valid' });
    console.log(`${semver.valid('1.2.3')}`);
    console.log(`${semver.valid('a.b.c')}`);
  }
  if (inc || all) {
    logTitle({ title: 'inc' });
    console.log(`${semver.inc('1.2.3', 'major')}`);
    console.log(`${semver.inc('1.2.3', 'premajor')}`);
    console.log(`${semver.inc('1.2.3', 'minor')}`);
    console.log(`${semver.inc('1.2.3', 'preminor')}`);
    console.log(`${semver.inc('1.2.3', 'patch')}`);
    console.log(`${semver.inc('1.2.3', 'prepatch')}`);
    console.log(`${semver.inc('1.2.3', 'prerelease')}`);
  }
  if (prerelease || all) {
    logTitle({ title: 'prerelease' });
    console.log(`${semver.prerelease('1.2.3')}`);
    console.log(`${semver.prerelease('1.2.3-alpha.1')}`);
  }
  if (major || all) {
    logTitle({ title: 'major' });
    console.log(`${semver.major('1.2.3')}`);
  }
  if (minor || all) {
    logTitle({ title: 'minor' });
    console.log(`${semver.minor('1.2.3')}`);
  }
  if (patch || all) {
    logTitle({ title: 'patch' });
    console.log(`${semver.patch('1.2.3')}`);
  }
  if (intersects || all) {
    logTitle({ title: 'intersects' });
    console.log(`${semver.intersects('1.2.x', '1.2.3')}`);
    console.log(`${semver.intersects('1.2.x', '1.3.3')}`);
  }
  if (gt || all) {
    logTitle({ title: 'gt' });
    console.log(`${semver.gt('1.2.3', '1.2.1')}`);
    console.log(`${semver.gt('1.2.3', '1.2.4')}`);
  }
  if (gt || all) {
    logTitle({ title: 'gt' });
    console.log(`${semver.gt('1.2.3', '1.2.1')}`);
    console.log(`${semver.gt('1.2.3', '1.2.4')}`);
  }
  if (gte || all) {
    logTitle({ title: 'gte' });
    console.log(`${semver.gte('1.2.3', '1.2.1')}`);
    console.log(`${semver.gte('1.2.3', '1.2.4')}`);
  }
  if (lt || all) {
    logTitle({ title: 'lt' });
    console.log(`${semver.lt('1.2.3', '1.2.1')}`);
    console.log(`${semver.lt('1.2.3', '1.2.4')}`);
  }
  if (lte || all) {
    logTitle({ title: 'lte' });
    console.log(`${semver.lte('1.2.3', '1.2.1')}`);
    console.log(`${semver.lte('1.2.3', '1.2.4')}`);
  }
  if (eq || all) {
    logTitle({ title: 'eq' });
    console.log(`${semver.eq('1.2.3', '1.2.3')}`);
    console.log(`${semver.eq('1.2.3', '1.2.4')}`);
  }
  if (neq || all) {
    logTitle({ title: 'neq' });
    console.log(`${semver.neq('1.2.3', '1.2.3')}`);
    console.log(`${semver.neq('1.2.3', '1.2.4')}`);
  }
  if (cmp || all) {
    logTitle({ title: 'cmp' });
    console.log(`${semver.cmp('1.2.3', '===', '1.2.3')}`);
    console.log(`${semver.cmp('1.2.3', '===', '1.2.4')}`);
  }
  if (compare || all) {
    logTitle({ title: 'compare' });
    console.log(`${semver.compare('1.2.3', '1.2.3')}`);
    console.log(`${semver.compare('1.2.3', '1.2.4')}`);
  }
  if (rcompare || all) {
    logTitle({ title: 'rcompare' });
    console.log(`${semver.rcompare('1.2.3', '1.2.3')}`);
    console.log(`${semver.rcompare('1.2.3', '1.2.4')}`);
  }
  if (diff || all) {
    logTitle({ title: 'diff' });
    console.log(`${semver.diff('1.2.3', '1.2.3')}`);
    console.log(`${semver.diff('1.2.3', '1.2.4')}`);
  }
  if (validRange || all) {
    logTitle({ title: 'validRange' });
    console.log(`${semver.validRange('1.2.3 - 2.3.4')}`);
    console.log(`${semver.validRange('~1')}`);
    console.log(`${semver.validRange('~1.2')}`);
    console.log(`${semver.validRange('~1.2.3')}`);
    console.log(`${semver.validRange('^1')}`);
    console.log(`${semver.validRange('^1.2')}`);
    console.log(`${semver.validRange('^1.2.3')}`);
    console.log(`${semver.validRange('1.x')}`);
    console.log(`${semver.validRange('1.2.x')}`);
  }
  if (satisfies || all) {
    logTitle({ title: 'satisfies' });
    console.log(`${semver.satisfies('1.2.4', '1.2.3 - 2.3.4')}`);
    console.log(`${semver.satisfies('1.2.1', '1.2.3 - 2.3.4')}`);
  }
  if (maxSatisfying || all) {
    logTitle({ title: 'maxSatisfying' });
    console.log(`${semver.maxSatisfying(['1.2.2', '1.2.4', '1.2.5'], '1.2.3 - 2.3.4')}`);
  }
  if (minSatisfying || all) {
    logTitle({ title: 'minSatisfying' });
    console.log(`${semver.minSatisfying(['1.2.2', '1.2.4', '1.2.5'], '1.2.3 - 2.3.4')}`);
  }
  if (minVersion || all) {
    logTitle({ title: 'minVersion' });
    console.log(`${semver.minVersion('1.2.3 - 2.3.4')}`);
  }
  if (gtr || all) {
    logTitle({ title: 'gtr' });
    console.log(`${semver.gtr('2.3.5', '1.2.3 - 2.3.4')}`);
    console.log(`${semver.gtr('2.3.2', '1.2.3 - 2.3.4')}`);
  }
  if (ltr || all) {
    logTitle({ title: 'ltr' });
    console.log(`${semver.ltr('1.2.2', '1.2.3 - 2.3.4')}`);
    console.log(`${semver.ltr('1.2.4', '1.2.3 - 2.3.4')}`);
  }
  if (outside || all) {
    logTitle({ title: 'outside' });
    console.log(`${semver.outside('1.2.2', '1.2.3 - 2.3.4', '<')}`);
    console.log(`${semver.outside('1.2.4', '1.2.3 - 2.3.4', '<')}`);
  }
  if (coerce || all) {
    logTitle({ title: 'coerce' });
    console.log(`${semver.coerce('1.2.a')}`);
    console.log(`${semver.coerce('1.b.a')}`);
  }
}


/* ------------------------ MAIN ------------------------------------------- */
versions({ all: true });
