# Semver Technology Docuemtation

Semver is the semantic versioner for npm. _Semantic Versions_ are described by the v2.0.0 specifcation found
at https://semver.org/.

Relevant URLs:
[npm](https://www.npmjs.com/package/semver),
[GitHub](https://github.com/npm/node-semver)

## Installation

Install with npm: `npm install semver`

## Node Usage

```javascript
semver.valid('1.2.3')                                        // '1.2.3'
semver.valid('a.b.c')                                        // null
semver.clean('  =v1.2.3   ')                                 // '1.2.3'
semver.satisfies('1.2.3', '1.x || >=2.5.0 || 5.0.0 - 7.2.3') // true
semver.gt('1.2.3', '9.8.7')                                  // false
semver.lt('1.2.3', '9.8.7')                                  // true
semver.minVersion('>=1.0.0')                                 // '1.0.0'
semver.valid(semver.coerce('v2'))                            // '2.0.0'
semver.valid(semver.coerce('42.6.7.9.3-alpha'))              // '42.6.7'
```

## Command Line Usage

```bash
$ npx semver -h
...

$ npx semver --increment major 1.2.3
2.0.0

$ npx semver --coerce 1.a.b
1.0.0

$ npx semver --range 1.2.x  1.3.0 1.2.0
1.2.0
```

