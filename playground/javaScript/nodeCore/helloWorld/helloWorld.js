/**
 * @file Hello World for Node
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let http = require('http');


/* ------------------------ MAIN ------------------------------------------- */
http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World!');
}).listen(8080);
