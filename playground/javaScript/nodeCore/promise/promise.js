/**
 * @file Promise Example
 */


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Return a promise to print
 * @async
 * @param  {string} text
 * @param  {int} timeMS
 * @param  {boolean} success - if true, resolve, else reject
 * @param  {any} rval - return value
 * @return {Promise}
 */
function delayPrint(text, timeMS, success, rval) {

  return new Promise((resolve, reject) => {
    setTimeout(function () {
      console.log(text);
      if (success) {
        resolve(rval);
      } else {
        reject(rval);
      }
    }, timeMS);
  });
}


/* ------------------------ MAIN ------------------------------------------- */
delayPrint('Promise Success', 500, true, 'Success Return Value')
  .then((resp) => {
    console.log(`Then:${resp}`);
  })
  .catch((err) => {
    console.log(`Catch:${err}`);
  });


delayPrint('Promise Failure', 500, false, 'Failure Return Value')
  .then((resp) => {
    console.log(`Then:${resp}`);
  })
  .catch((err) => {
    console.log(`Catch:${err}`);
  });
