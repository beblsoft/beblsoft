/**
 * @file Inheritance Example
 */


/* ------------------------------------------------------------------------- */
/**
 * Vehicle Class
 */
class Vehicle {

  /**
   * @constructor
   *
   * @param {string} name
   * @param {string} type - type of vehicle
   */
  constructor({
    name,
    type
  }) {
    this.name = name;
    this.type = type;
  }
}

/* ------------------------------------------------------------------------- */
/**
 * Car Class
 * @extends {Vehicle}
 */
class Car extends Vehicle {

  /**
   * @constructor
   *
   * @param {string} name
   */
  constructor({
    name
  }) {
    super({
      name: name,
      type: 'car'
    });
    this.nWheels = 4;
  }
}


/* ------------------------ MAIN ------------------------------------------- */
let vehicles = [
  new Vehicle({
    name: 'Ronda',
    type: 'boat'
  }),
  new Car({
    name: 'Tesla'
  })
];

for (let v of vehicles) {
  console.log(v);
  console.log(`${v.name} ${v.type}`);
}
