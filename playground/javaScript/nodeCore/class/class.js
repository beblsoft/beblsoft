/**
 * @file Class Example
 */

/* ------------------------------------------------------------------------- */
/**
 * Person Class
 */
class Person {

  /**
   * @constructor
   *
   * @param {string} firstName - First name
   * @param {string} lastName - Last name
   */
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * @return {string} full name
   */
  fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}


/* ------------------------ MAIN ------------------------------------------- */
let james = new Person('James', 'Bensson');
console.log(james.fullName());
