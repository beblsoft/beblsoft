/**
 * @file Property Example
 */

/* ------------------------------------------------------------------------- */
/**
 * Person Class
 */
class Person {

  /**
   * @constructor
   *
   * @param {string} firstName - First name
   * @param {string} lastName - Last name
   */
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * @return {string} fullname
   */
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  /**
   * @param {string} fullName
   */
  set fullName(fullName) {
    let names = fullName.split(' ', 2);
    this.firstName = names[0];
    this.lastName = names[1];
  }
}


/* ------------------------ MAIN ------------------------------------------- */
let p = new Person('John', 'Smith');
console.log(p);
p.fullName = 'Barry Johnson';
console.log(p);

