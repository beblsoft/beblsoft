/**
 * @file Event Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let events = require('events');


/* ------------------------ GLOBALS ---------------------------------------- */
let eventEmitter = new events.EventEmitter();
eventEmitter.on('SampleEvent', (index) => {
  console.log(`SampleEvent Handler! index=${index}`);
});


/* ------------------------ MAIN ------------------------------------------- */
for (let i = 0; i < 10; i++) {
  eventEmitter.emit('SampleEvent', i);
}
