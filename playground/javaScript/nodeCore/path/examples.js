/**
 * @file Path Examples
 *
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let path = require('path');


/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * log title
 */
function logTitle({ title, nDashes = 50 } = {}) {
  console.log('\n');
  console.log(`${title}`);
  console.log('-'.repeat(nDashes));
}

/**
 * Path examples
 */
function pathExamples({
  basename = false,
  delimiter = false,
  dirname = false,
  extname = false,
  format = false,
  isAbsolute = false,
  join = false,
  normalize = false,
  parse = false,
  relative = false,
  resolve = false,
  sep = false,
  all = false
} = {}) {
  if (basename || all) {
    logTitle({ title: 'basename' });
    console.log(`${JSON.stringify(path.basename('/foo/bar/baz/asdf/quux.html'))}`);
    console.log(`${JSON.stringify(path.basename('/foo/bar/baz/asdf/quux.html', '.html'))}`);
  }
  if (delimiter || all) {
    logTitle({ title: 'delimiter' });
    console.log(`${JSON.stringify(process.env.PATH)}`);
    console.log(`${JSON.stringify(process.env.PATH.split(path.delimiter))}`);
  }
  if (dirname || all) {
    logTitle({ title: 'dirname' });
    console.log(`${JSON.stringify(path.dirname('/foo/bar/baz/asdf/quux'))}`);
  }
  if (extname || all) {
    logTitle({ title: 'extname' });
    console.log(`${JSON.stringify(path.extname('index.html'))}`);
    console.log(`${JSON.stringify(path.extname('index.coffee.md'))}`);
    console.log(`${JSON.stringify(path.extname('index.'))}`);
    console.log(`${JSON.stringify(path.extname('index'))}`);
    console.log(`${JSON.stringify(path.extname('.index'))}`);
    console.log(`${JSON.stringify(path.extname('.index.md'))}`);
  }
  if (format || all) {
    logTitle({ title: 'format' });
    // Can specify:
    //  - dir
    //  - root
    //  - base
    //  - name
    //  - ext
    let p1 = path.format({
      dir: '/home/user/dir',
      base: 'file.txt'
    });
    let p2 = path.format({
      root: '/',
      name: 'file.txt'
    });
    console.log(`${JSON.stringify(p1)}`);
    console.log(`${JSON.stringify(p2)}`);
  }
  if (isAbsolute || all) {
    logTitle({ title: 'isAbsolute' });
    console.log(`${JSON.stringify(path.isAbsolute('/foo/bar'))}`);
    console.log(`${JSON.stringify(path.isAbsolute('foo/bar'))}`);
  }
  if (join || all) {
    logTitle({ title: 'join' });
    console.log(`${JSON.stringify(path.join('/foo/bar', 'baz', 'laz'))}`);
  }
  if (normalize || all) {
    logTitle({ title: 'normalize' });
    console.log(`${JSON.stringify(path.normalize('/foo/bar/..'))}`);
  }
  if (parse || all) {
    logTitle({ title: 'normalize' });
    let o = path.parse('/home/user/dir/file.txt');
    console.log(`${JSON.stringify(o)}`);
  }
  if (relative || all) {
    logTitle({ title: 'relative' });
    console.log(`${JSON.stringify(path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb'))}`);
  }
  if (resolve || all) {
    logTitle({ title: 'resolve' });
    console.log(`${JSON.stringify(path.resolve('wwwroot', 'static_files/png/', '../gif/image.gif'))}`);
  }
  if (sep || all) {
    logTitle({ title: 'sep' });
    console.log(`${JSON.stringify('foo/bar/baz'.split(path.sep))}`);
  }
 }


/* ------------------------ MAIN ------------------------------------------- */
pathExamples({ all: true });
