/**
 * @file Regex Examples
 *
 * Regular Expressions               - A way to describe patterns in string data
 *
 * Rules
 *  - var rgx = /^(\d+)/             - You can use a regular expression literal and enclose the pattern
 *                                     between slashes. This is evaluated at compile time and provides better
 *                                     performance if the regular expression stays constant.
 *  - var rgx = new RegExp('^(\d+)') - The constructor function is useful when the regular expression may
 *                                     change programmatically. These are compiled during runtime.
 *  - \w                             - Matches all the words characters. Word characters are alphanumeric
 *                                     (a-z, A-Z characters, and underscore).
 *  - \W                             - Matches non-word characters. Everything except alphanumeric
 *                                     characters and underscore.
 *  - \d                             - Matches digit characters. Any digit from 0 to 9.
 *  - \D                             - Matches non-digit characters. Everything except 0 to 9.
 *  - \s                             - Matches whitespace characters. This includes spaces, tabs, and line breaks.
 *  - \S                             - Matches all other characters except whitespace.
 *  - .                              - Matches any character except line breaks.
 *  - \b                             - Match the preceding token only if there is a word boundary.
 *  - \B                             - Match the preceding token only if there is no word boundary.
 *  - [A-Z]                          - Matches characters in a range. For example, [A-E] will match A, B, C, D, and E.
 *  - [ABC]                          - Matches a character in the given set. For example, [AMT] will only
 *                                     match A, M, and T.
 *  - [^ABC]                         - Matches all the characters not present in the given set.
 *                                     For example, [^A-E] will match all other characters except A, B, C, D, and E.
 *  - +                              - Matches one or more occurrences of the preceding token. For example, \w+ will
 *                                     return ABD12D as a single match instead of six different matches.
 *  - *                              - Matches zero or more occurrences of the preceding token. For example,
 *                                     b\w* matches the bold parts in b, bat, bajhdsfbfjhbe. Basically, it matches
 *                                     zero or more word characters after 'b'.
 *  - {m, n}                         - Matches at least m and at most n occurrences of the previous token.
 *                                     {m,} will match at least m occurrences, and there is no upper limit to the
 *                                     match. {k} will match exactly k occurrences of the preceding token.
 *  - ?                              - Matches zero or one occurrences of the preceding character. For example,
 *                                     this can be useful when matching two variations of spelling for the
 *                                     same work. For example, /behaviou?r/ will match both behavior and behaviour.
 *  - |                              - Matches the expression either before or after the pipe character.
 *                                     For example, /se(a|e)/ matches both see and sea.
 *  - (ABC)                          - This will group multiple tokens together and remember the substring matched
 *                                     by them for later use. This is called a capturing group.
 *  - (?:ABC)                        - This will also group multiple tokens together but won't remember the
 *                                     match. It is a non-capturing group.
 *  - \d+(?=ABC)                     - This will match the token(s) preceding the (?=ABC) part only if it is
 *                                     followed by ABC. The part ABC will not be included in the match.
 *                                     The \d part is just an example. It could be any other regular expression
 *                                     string.
 *  - \d+(?!ABC)                     - This will match the token(s) preceding the (?!ABC) part only if it is not
 *                                     followed by ABC. The part ABC will not be included in the match.
 *                                     The \d part is just an example. It could be any other regular expression
 *                                     string.
 *  - ^                              - Look for the regular expression at the beginning of the string or the
 *                                     beginning of a line if the multiline flag is enabled.
 *  - $                              - Look for the regular expression at the end of the string or the end of
 *                                     a line if the multiline flag is enabled.
 *  - g                              - Search the string for all matches of given expression instead of
 *                                     returning just the first one.
 *  - i                              - Make the search case-insensitive so that words like Apple, aPPLe and
 *                                     apple can all be matched at once.
 *  - m                              - This flag will make sure that the ^ and $ tokens look for a match at the
 *                                     beginning or end of each line instead of the whole string.
 *  - u                              - This flag will enable you to use Unicode code point escapes in your
 *                                     regular expression.
 *  - y                              - This will tell JavaScript to only look for a match at the current
 *                                     position in the target string.
 *
 * Bibliography                      - Start Here > https://eloquentjavascript.net/09_regexp.html
 *                                   - https://code.tutsplus.com/tutorials/a-simple-regex-cheat-sheet--cms-31278
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Demonstrate regex examples
 */
function regexExample({ type = 'testABC' }) {

  switch (type) {
  case 'testABC':
    {
      let re1 = new RegExp('abc'); // same as re1 = /abc/
      console.log(re1.test('abcde')); // true
      console.log(re1.test('abxde')); // false
      console.log('abcde'.indexOf('cde')); // 2
      break;
    }
  case 'testSet':
    {
      console.log((/[0123456789]/).test('in 1992')); // true
      console.log((/[0-9]/).test('in 1992')); // true
      break;
    }
  case 'testDigit':
    {
      console.log((/\d/).test('a')); // false
      console.log((/\d/).test('0')); // true
      break;
    }
  case 'testNotDigit':
    {
      console.log((/\D/).test(' ')); // true
      console.log((/\D/).test('a')); // true
      console.log((/\D/).test('3')); // false
      break;
    }
  case 'testAlphanumeric':
    {
      console.log((/\w/).test('a')); // true
      console.log((/\w/).test('0')); // true
      console.log((/\w/).test(' ')); // false
      break;
    }
  case 'testNotAlphanumeric':
    {
      console.log((/\W/).test('a')); // false
      console.log((/\W/).test('0')); // false
      console.log((/\W/).test(' ')); // true
      break;
    }
  case 'testWhitespace':
    {
      console.log((/\s/).test(' ')); // true
      console.log((/\s/).test('0')); // false
      break;
    }
  case 'testNotWhitespace':
    {
      console.log((/\S/).test(' ')); // false
      console.log((/\S/).test('0')); // true
      break;
    }
  case 'testAnyCharacter':
    {
      console.log((/./).test('4')); // true
      console.log((/./).test('\n')); // false
      break;
    }
  case 'testDatetime':
    {
      let dateTime = (/\d\d-\d\d-\d\d\d\d \d\d:\d\d/);
      console.log(dateTime.test('01-30-2003 15:20')); // true
      console.log(dateTime.test('30-jan-2003 15:20')); // false
      break;
    }
  case 'testExcept':
    {
      let notBinary = (/[^01]/);
      console.log(notBinary.test('0101010101011010101')); // false
      console.log(notBinary.test('3049872341212121211')); // true
      break;
    }
  case 'testRepeating':
    {
      console.log((/\d+/).test('123')); // true
      console.log((/\d+/).test('')); // false
      console.log((/\d*/).test('123')); // true
      console.log((/\d*/).test('')); // true
      break;
    }
  case 'testOptional':
    {
      let neighbor = (/neighbou?r/);
      console.log(neighbor.test('neighbor')); // true
      console.log(neighbor.test('neighbour')); // true
      console.log(neighbor.test('neighbouur')); // false
      break;
    }
  case 'testNTimes':
    {
      let dateTime = (/\d{1,2}-\d{1,2}-\d{4} \d{1,2}:\d{2}/);
      console.log(dateTime.test('1-30-2003 8:45')); // true
      console.log(dateTime.test('10aa-30-2003 8:45')); // false
      break;
    }
  case 'testGrouping':
    {
      let cartoonCrying = (/boo+(hoo+)+/i);
      console.log(cartoonCrying.test('Boohoooohoohooo')); // true
      console.log(cartoonCrying.test('Booaahoooohoohooo')); // false
      break;
    }
  case 'testChoice':
    {
      let animalCount = (/\b\d+ (pig|cow|chicken)s?\b/);
      console.log(animalCount.test('15 pigs')); // true
      console.log(animalCount.test('15 pigchickens')); // false
      break;
    }
  case 'execBasic':
    {
      let match = (/\d+/).exec('one two 100');
      console.log(match[0]); // '100'
      console.log(match.index); // 8
      console.log(match.input); // 'one two 100'
      break;
    }
  case 'execFindPort':
    {
      let re = (/:(\d+)/);
      let match = re.exec('http://localhost:8080');
      console.log(match[0]); // ':8080'
      console.log(match[1]); // '8080'
      let noMatch = re.exec('api.smeckn.com');
      console.log(noMatch); // null
      break;
    }
  case 'execGroups':
    {
      let re = (/quick\s(brown).+?(jumps)/ig);
      let match = re.exec('The Quick Brown Fox Jumps Over The Lazy Dog');
      console.log(match[0]); // 'Quick Brown Fox Jumps'
      console.log(match[1]); // 'Brown'
      console.log(match[2]); // 'Jumps'
      console.log(match.index); // 4
      console.log(match.input); // 'The Quick Brown Fox Jumps Over The Lazy Dog'
      break;
    }
  case 'execGroupConditional':
    {
      let re = (/bad(ly)?/);
      let match = re.exec('bad brown bears');
      console.log(match[0]); // 'bad'
      console.log(match[1]); // undefined
      console.log(match.index); // 0
      console.log(match.input); // 'bad brown bears'
      break;
    }
  case 'execGroupMultipleMatch':
    {
      let re = (/(\d)+/);
      let match = re.exec('123');
      console.log(match[0]); // 123
      console.log(match[1]); // 3
      console.log(match.index); // 0
      console.log(match.input); // 123
      break;
    }
  case 'execLoop':
    {
      let match = null;
      let numberRegex = (/\b\d+\b/g);
      let input = 'A string with 3 numbers in it... 42 and 88.';
      while ((match = numberRegex.exec(input)) !== null) {
        console.log(`Found ${match[0]} at ${match.index}`);
      }
      // Found 3 at 14
      // Found 42 at 33
      // Found 88 at 40
      break;
    }
  case 'execLoopReplace':
    {
      let match = null;
      let regex = (/\{(\w+)\}/g);
      let urlParams = { accountID: 1, profileID: 2, postID: 3 };
      let templateURL = '/account/{accountID}/profile/{profileID}/post/{postID}';
      let urlString = templateURL;

      // Replace names in template with parameters
      while ((match = regex.exec(templateURL)) !== null) {
        let param = match[1];
        urlString = urlString.replace(`{${param}}`, urlParams[param]);
      }
      console.log(urlString); // /account/1/profile/2/post/3
      break;
    }
  case 'replaceDynamic':
    {
      let name = 'Harry';
      let text = 'Harry is a suspicious character.';
      let regexp = new RegExp(`\\b(${name})\\b`, 'gi');
      console.log(text.replace(regexp, '_$1_')); // _Harry_ is a suspicious character.
      break;
    }
  default:
    break;

  }

}


/* ------------------------ MAIN ------------------------------------------- */
regexExample({ type: 'execFindPort' });
