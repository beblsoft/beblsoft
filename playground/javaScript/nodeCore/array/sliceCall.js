/**
 * @file Slice Call Example
 */

/* ------------------------ HELPER FUNCTIONS ------------------------------- */
/**
 * Log array
 * @param   {array}  arr   array to log
 */
function logArray(arr) {
  console.log(arr);
  console.log(arr.length);
  for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
  }
}

/**
 * Log slice call
 */
function logSliceCall() {
  let args = arguments;
  logArray(arguments);
  console.log('========================');
  args = [].slice.call(arguments);
  logArray(args);
}


/* ------------------------ MAIN ------------------------------------------- */
logSliceCall('arg1', 'arg2', 'arg3');
