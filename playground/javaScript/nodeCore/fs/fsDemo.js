/**
 * @file Class Example
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let http = require('http');
let url = require('url');
let fs = require('fs');


/* ------------------------ FILE HELPERS ----------------------------------- */
/**
 * Read file into http response
 * Response is completed after written
 * @param  {string} filePath
 * @param  {object} res - http response
 */
function readFileIntoResponse(filePath, res) {
  fs.readFile(filePath, function(err, data) { // eslint-disable-line handle-callback-err
    res.writeHead(200, {
      'Content-Type': 'text/html'
    });
    res.write(data);
    res.end();
    console.log('Read!');
  });
}

/**
 * Append Text to file
 * @param  {string} filePath
 * @param  {string} text
 */
function appendFile(filePath, text) {
  fs.appendFile(filePath, text, function(err) {
    if (err) {
      throw err;
    }
    console.log('Appended!');
  });
}

/**
 * Write text to file
 * @param  {string} filePath
 * @param  {string} text
 */
function writeFile(filePath, text) {
  fs.writeFile(filePath, text, function(err) {
    if (err) {
      throw err;
    }
    console.log('Written!');
  });
}

/* ------------------------ MAIN ------------------------------------------- */
let server = http.createServer(function(req, res) {

  // Perform file action specified by client
  let q = url.parse(req.url, true).query;
  let action = q.action;
  let filePath = '';

  switch (action) {
    case 'read':
      filePath = 'fsDemo.html';
      readFileIntoResponse(filePath, res);
      break;
    case 'append':
      filePath = '/tmp/append.html';
      appendFile(filePath, '<p>Appended Text</p>');
      readFileIntoResponse(filePath, res);
      break;
    case 'write':
      filePath = '/tmp/write.html';
      writeFile(filePath, '<p>Written Text</p>');
      readFileIntoResponse(filePath, res);
      break;
    default:
      res.write('Specify action');
      res.end();
      break;
  }
});

// Listen on port 8080
server.listen(8080);
