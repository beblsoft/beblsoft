/**
 * @file URL Parsing Example
 */

/* ------------------------ IMPORTS ---------------------------------------- */
let url = require('url');


/* ------------------------ GLOBALS ---------------------------------------- */
let adr = 'http://localhost:8080/default.htm?year=2017&month=february&day=7';
let q = url.parse(adr, true);
let qData = q.query;


/* ------------------------ MAIN ------------------------------------------- */
console.log(`Host = ${q.host}`);
console.log(`Pathname = ${q.pathname}`);
console.log(`Search = ${q.search}`);
console.log(`Query Data = ${JSON.stringify(qData, null, 2)}`);
