/**
 * @file Module Example
 */

/* ------------------------ EXPORTS ---------------------------------------- */
/**
 * @module Date
 * @return {Date} Current date
 */
exports.myDateTime = function() {
  return Date();
};
