/**
 * @file Params Example
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Destructure parameters
 * @param  {int} x
 * @param  {int} y
 * @param  {int} z
 */
function destructureParams({ x, y, z }) {
  console.log(`destructureParams: ${x}, ${y}, ${z}`);
}

/**
 * Rename parameters
 * @param  {int} x
 * @param  {int} y
 * @param  {int} z
 */
function renameParams({ x: newX, y: newY, z: newZ }) {
  console.log(`renameParams: ${newX}, ${newY}, ${newZ}`);
}

/**
 * Default parameters
 * @param  {int} x
 * @param  {int} y
 * @param  {int} z
 */
function defaultParams({ x, y, z = 4 } = { x: 1, y: 2 }) {
  console.log(`defaultParams: ${x}, ${y}, ${z}`);
}

/**
 * Optional Parameters
 * @param  {int} x
 * @param  {int} y
 * @param  {int} z
 */
function optionalParams({ x = 5, y = 8, z = 13 }) {
  console.log(`optionalParams: ${x}, ${y}, ${z}`);
}

/* ------------------------ MAIN ------------------------------------------- */
destructureParams({ x: 10, y: 20, z: 30 });
renameParams({ x: 10, y: 20, z: 30 });
defaultParams();
optionalParams({ x: 10, y: 20, a: 30, b: 30 });
