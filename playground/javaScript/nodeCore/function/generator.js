/**
 * @file Generator Example
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Generate number on each version of the loop
 * @generator
 * @param {number} num - Number of times to generate a number
 * @yiel  {number}
 */
function* generator(num) {
  for (let i = 0; i < num; i += 1) {
    console.log(`Generating ${i}`);
    yield i;
  }
}


/* ------------------------ MAIN ------------------------------------------- */
let num = 5;
const gen = generator(num);
for (let i = 0; i < num + 3; i++) {
  // When i >= num, generator doesn't do anything, no errors produced
  gen.next();
}
