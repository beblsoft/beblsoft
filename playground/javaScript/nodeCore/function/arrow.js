/**
 * @file Arrow Function Example
 *
 * Arrow Function Features
 *  - Shorter body
 *  - Don't have own this, arguments, super, or new.target
 *  - Cannot be used as generators
 *
 * Bibliography - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Demonstrate arrow function examples
 */
function arrowFunctionExample({ type = 'basic' }) {

  switch (type) {
  case 'basic':
    {
      let func = (a, b) => { return a + b; }; /* eslint-disable-line */
      console.log(func(2, 3)); // 5
      break;
    }
  case 'array':
    {
      let arr = [5, 6, 13, 0, 1, 18, 23];

      let sum = arr.reduce((a, b) => { return a + b; });
      console.log(sum); // 66
      let even = arr.filter((v) => { return v % 2 === 0; });
      console.log(even); // [6, 0, 18]
      let double = arr.map((v) => { return v * 2; });
      console.log(double); // [10, 12, 26, 0, 2, 36, 46]
      break;

    }
  case 'promiseChain':
    {
      new Promise((resolve, reject) => {
        setTimeout(() => { resolve(1); }, 200);
      }).then((result) => {
        console.log(result); // 1
        return result * 2;
      }).then((result) => {
        console.log(result); // 2
        return result * 2;
      }).then((result) => {
        console.log(result); // 4
        return result * 2;
      }).then((result) => {
        console.log(result); // 8
        return result * 2;
      });
      break;
    }
  default:
    break;

  }

}


/* ------------------------ MAIN ------------------------------------------- */
arrowFunctionExample({ type: 'promiseChain' });
