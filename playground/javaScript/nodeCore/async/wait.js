/**
 * @file Wait Examples
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Wait for maxWaitS, sleeping in intervals of sleepS
 * @param  {number} options.sleepS
 * @param  {[type]} options.maxWaitS
 */
async function wait({ sleepS = 1, maxWaitS = 10 } = {}) {
  let startDateS = new Date().getTime() / 1000;
  let curDateS = startDateS;
  let idx = 0;
  const sleep = (secs) => { return new Promise((resolve) => { setTimeout(resolve, secs * 1000); }); }; /* eslint-disable-line */

  /* Iterate, catching error and sleeping */
  while (curDateS - startDateS < maxWaitS) {
    console.log(`Idx=${idx}`);
    curDateS = new Date().getTime() / 1000;
    await sleep(sleepS);
    idx++;
  }
  console.log('Time to leave!');
  return 1;
}

/* ------------------------ MAIN ------------------------------------------- */
wait()
  .then((rval) => { console.log(rval); })
  .catch((err) => { console.log(err); });
