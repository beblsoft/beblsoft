/**
 * @file Async Examples
 *
 * async:
 *  - Put before function.
 *  - Means function will ALWAYS return a promise (if not natively, JS will wrap it in a promise)
 *  - Async functions always return promises
 *
 * await:
 *  - Makes JavaScript settle until promises settles and returns the result
 *  - Cannot use await in non-async functions
 *  - await supports .thenable objects
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Demonstrate async examples
 * @return {Promise}
 */
async function asyncExample({ type = 'basic' }) {

  switch (type) {

  case 'basic':
    {
      let value = await new Promise((resolve, reject) => { resolve(1); })
        .then((rval) => { console.log(rval); return rval * 2; }) // 1
        .then((rval) => { console.log(rval); return rval * 2; }) // 2
        .then((rval) => { console.log(rval); return rval * 2; }) // 4
        .then((rval) => { console.log(rval); return rval * 2; });// 8
      return value;
    }
  case 'errorThrown':
    {
      return await new Promise((resolve, reject) => { reject(new Error('Blah')); });
    }
  case 'errorCaught':
    {
      try {
        return await new Promise((resolve, reject) => { reject(new Error('Blah')); });
      } catch {
        return 'caught error';
      }
    }
  default:
    break;
  }
  return 0;

}


/* ------------------------ MAIN ------------------------------------------- */
asyncExample({ type: 'errorThrown' })
  .then((rval) => { console.log(rval); })
  .catch((err) => { console.log(err); });
