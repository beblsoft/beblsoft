# Node JS Core Projects Documentation

## helloWorld/helloWorld.js

Hello World Example.  Directory `./helloWorld`

To Run:
- Start server: `node helloWorld.js`
- View in browser: http://localhost:8080

## array/sliceCall.js

Example using array.slice.call()

To Run: `node sliceCall.js`

## assert/examples.js

Example assertion routines: ok, equal, deepEqual, ...

To Run: `node examples.js`

## async/examples.js

Example async, await usage.

To Run: `node examples.js`

## async/wait.js

Example sleeping inside an async function.

To Run: `node wait.js`

## class/class.js

Demonstrate basic person class.
To Run: `node class.js`

## class/inheritance.js

Demonstrate inheritance between vehicle and car.

To Run: `node inheritance.js`

## class/property.js

Demonstrate use of JavaScript class properties.

To Run: `node property.js`

## date/examples.js

Many date examples including creating, seting, and getting.

To Run: `node examples.js`

## error/basic.js

Basic paradigms for JS errors: sync, async, event, promise.

To Run: `node basic.js`

## error/class.js

Example class that subclasses the event class.

To Run: `node class.js`

## event/eventDemo.js

Demonstrate event emitters and handlers

To Run: `node eventDemo.js`

## fs/fsDemo.js

To Run:
- Start server             : `node fsDemo.js`
- Read file from browser   : http://localhost:8080/?action=read
- Write file from browser  : http://localhost:8080/?action=write
- Append file from browser : http://localhost:8080/?action=append
- Refresh page to see multiple appends

## function/arrow.js

Arrow function examples

To Run : `node arrow.js`

## function/params.js

Demonstrate parameter destructuring

To Run: `node params.js`

## function/generator.js

Example generator

To Run: `node generator.js`

## http/httpDemo

Node HTTP Server Example

To Run
- Start server: `node httpDemo.js`
- View in browser: http://localhost:8080/summer?arg1=2&arg2=10

## module/dateModuleClient.js

Node Module Usage Example. Create module and then have client require it.

To Run: `node dateModuleClient.js`

## path/examples.js

Demonstrate path manipulation techniques.

To Run: `node path/examples.js`

## promise/promise.js

Example using JavaScript Promises

To Run: `node promise.js`

## regex/examples.js

Many regex examples including (among others) test, exec, and replace

To Run: `node examples.js`

## url/urlparsing.js

Node URL Handling and Parsing

To Run: `node urlParsing.js`








