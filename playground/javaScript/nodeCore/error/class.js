/**
 * @file Error Class Example
 */

/* ------------------------------------------------------------------------- */
/**
 * ExampleError Class
 */
class ExampleError extends Error {

  /**
   * @constructor
   * @param  {string} message
   */
  constructor({ message, code = 5 }) {
    super(message);
    Error.captureStackTrace(this, this.constructor); // Properly capture the stack trace
    this.name = this.constructor.name;
    this.code = code;
  }

  /**
   * @return {string} object string
   */
  toString() {
    let s = `[${this.name} code=${this.code} message=${this.message} stack=${this.stack}]`;
    return s;
  }
}


/* ------------------------ MAIN ------------------------------------------- */
let e = new ExampleError({ message: 'test', code: 2 });
console.log(e.toString());
throw e;
