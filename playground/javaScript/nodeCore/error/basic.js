/**
 * @file Basic Error Examples
 *
 * 4 types of errors
 *   - Standard JavaScript Errors
 *     EvalError, SyntaxError, RangeError, ReferenceError, TypeError, URIError
 *   - System errors
 *     Ex. Attempting to open a bad file
 *   - User specified errors
 *     Ex. API request fails
 *   - AssertionError
 *     Logic violation
 */

/* ------------------------ GLOBALS ---------------------------------------- */
let fs = require('fs');
let EventEmitter = require('events');
let myEmitter = new EventEmitter();


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Handle sync reference error
 */
function syncReferenceError() {
  try {
    let m = z; /* eslint-disable-line */
  } catch (err) { /* eslint-disable-line */
    console.log(`Caught error: ${err}`);
  }
}

/**
 * Handle async read error
 */
function asyncReadError() {
  fs.readFile('a file that does not exist', (err) => {
    if (err) {
      console.error(`Caught error: ${err}`);
      return;
    }
    // Handle the data here
  });
}

/**
 * Handle event emitter error
 */
function emitEventError() {
  // Register handler
  myEmitter.on('error', (err) => {
    console.log(`Caught error: ${err}`);
  });

  // Send error
  myEmitter.emit('error', new Error('Event Emitter Error'));
}


/**
 * Return a promise that will create an error
 * @return {Promise}
 */
function promiseError() {
  return new Promise((resolve, reject) => {
    reject(new Error('Promise Error'));
  });
}


/* ------------------------ MAIN ------------------------------------------- */
syncReferenceError();
asyncReadError();
emitEventError();
promiseError().catch((err) => {
  console.log(`Caught error: ${err}`);
});
