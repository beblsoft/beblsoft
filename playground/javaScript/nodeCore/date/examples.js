/**
 * @file Date Examples
 *
 * Bibliography - https://www.digitalocean.com/community/tutorials/understanding-date-and-time-in-javascript
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Demonstrate date examples
 */
function dateExample({ type = 'now' }) {

  switch (type) {
  case 'now':
    {
      let now = new Date();
      console.log(now); // 2018-09-20T19:46:24.459Z
      console.log(now.getTime()); // 1537472835500 (Seconds since epoch)
      break;
    }
  case 'create':
    {
      // Three methods: Timestamp, Date String, Date and Time
      console.log(new Date(-6106015800000)); // 1776-07-04T12:30:00.000Z
      console.log(new Date('January 31 1980 12:30')); // 1980-01-31T17:30:00.000Z
      console.log(new Date(1776, 6, 4, 12, 30, 0, 0)); // 1776-07-04T17:26:02.000Z
      break;
    }
  case 'get':
    {
      let now = new Date();
      console.log(now.getFullYear()); // 2018 (Year)
      console.log(now.getMonth()); // 8 (Month, 0 = January)
      console.log(now.getDate()); // 20 (Day of month, 1 indexed)
      console.log(now.getDay()); // 4 (Day of week, 0 = sunday)
      console.log(now.getHours()); // 15 (Hour of day, 0-23)
      console.log(now.getMinutes()); // 52 (Minutes, 0-59)
      console.log(now.getSeconds()); // 58 (Seconds, 0-59)
      console.log(now.getMilliseconds()); // 181 (Milliseconds, 0-999)
      console.log(now.getTime()); // 1537473178181 (Milliseconds since epoch)
      break;
    }
  case 'set':
    {
      let d = new Date();
      d.setFullYear(1970);
      d.setMonth(0);
      d.setDate(1);
      d.setHours(0);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);
      console.log(d); // 1970-01-01T05:00:00.000Z (5 hours after UTC)
      console.log(d.getTime()); // 18000000
      break;
    }
  case 'compare':
    {
      let today = new Date();
      if (today.getDate() === 3 && today.getMonth() === 9) {
        console.log('It\'s October 3rd.');
      } else {
        console.log('It\'s not October 3rd.');
      }
      break;
    }
  default:
    break;

  }

}


/* ------------------------ MAIN ------------------------------------------- */
dateExample({ type: 'set' });
