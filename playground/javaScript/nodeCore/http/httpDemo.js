/**
 * @file HTTP Demo
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let http = require('http');
let url = require('url');


/* ------------------------ MAIN ------------------------------------------- */
let server = http.createServer(function(req, res) {
  // Add header
  res.writeHead(200, { 'Content-Type': 'text/html' });

  // Write url back to client
  let txt = '<p>';
  txt += `url=${req.url} `;
  txt += '</p>';
  res.write(txt);

  // Write arguments back to client
  let q = url.parse(req.url, true).query;
  txt = '<p>';
  txt += `arg1=${q.arg1}\n`;
  txt += `arg2=${q.arg2}\n`;
  txt += '</p>';
  res.write(txt);

  // End request
  res.end();
});

// Listen on port 8080
server.listen(8080);
