/**
 * @file Assertion Examples
 *
 * Bibliography - https://nodejs.org/api/assert.html
 */


/* ------------------------ IMPORTS ---------------------------------------- */
let assert = require('assert');


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Demonstrate assertion examples
 */
function assertionExample({ type = 'fields' }) {

  switch (type) {

  case 'fields':
    {
      try {
        assert.strictEqual(1, 2, 'Assertion Message!');
      } catch (err) {
        console.log(err.message); // Assertion Message!
        console.log(err.name); // AssertionError [ERR_ASSERTION]
        console.log(err.actual); // 1
        console.log(err.expected); // 2
        console.log(err.code); // ERR_ASSERTION
        console.log(err.operator); // strictEqual
        console.log(err.generatedMessage); // true
      }
      break;
    }
  case 'deepEqual':
    {
      let x = { a: { n: 0 } };
      let y = { a: { n: 0 } };
      let z = { a: { n: 1 } };
      assert.deepEqual(x, y, 'Bad deep equal'); // OK
      assert.deepEqual(x, z, 'Bad deep equal'); // AssertionError [ERR_ASSERTION]: Bad deep equal
      break;
    }
  case 'deepStrictEqual':
    {
      let x = { a: { n: 0 } };
      let z = { a: { n: '0' } };
      assert.deepStrictEqual(x, z, 'Bad deep strict equal'); // AssertionError [ERR_ASSERTION]: Bad deep strict equal
      break;
    }
  case 'equal':
    {
      assert.equal(50, 50); // OK
      assert.equal(50, '50'); // OK
      assert.equal(50, 70, '50 does not equal 70!'); // AssertionError [ERR_ASSERTION]: 50 does not equal 70!
      break;
    }
  case 'strictEqual':
    {
      assert.strictEqual(50, 50); // OK
      assert.strictEqual(50, '50', 'Numbers not strings!'); // AssertionError [ERR_ASSERTION]: Numbers not strings!
      break;
    }
  case 'ok':
    {
      assert.ok(50 > 70, '70 > 50'); // AssertionError [ERR_ASSERTION]: 70 > 50
      break;
    }
  default:
    break;

  }

}


/* ------------------------ MAIN ------------------------------------------- */
assertionExample({ type: 'ok' });
