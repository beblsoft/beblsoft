# GreenSock Documentation

GreenSock is an ultra high-performance, professional-grade animation platform for the modern web.

The GreenSock Animation Platform (GSAP) animates anything JavaScript can touch (CSS properties, SVG, React,
canvas, generic objects, whatever) and solves countless browser inconsistencies, all with blazing speed
(up to 20x faster than jQuery). It's used by over 8,000,000 sites and every major brand.

__Note: Green Sock is not free for commercial use!__

Relevant URLs:
[Home](https://greensock.com/),
[Getting Started](https://greensock.com/get-started-js),
[NPM](https://www.npmjs.com/package/gsap),
[GitHub](https://github.com/greensock/GreenSock-JS),
[Cheat Sheet](https://ihatetomatoes.net/wp-content/uploads/2016/07/GreenSock-Cheatsheet-4.pdf)

## Introduction

### Property Manipulation

Animation ultimately boils down to changing property values many times per second, making something
appear to move, fade, spin, etc. GSAP snags a starting value, an ending value, and then interpolates
them 60 times per second.

Your job an an animator is to decide which properties to change, how quickly, and the motion's style
(known as easing).

### DOM, SVG, canvas, and beyond

GSAP doesn't have a pre-defined list of properties it can handle. It's super flexible, adjusting to
almost anything you throw at it. GSAP can animate all of the following:

- __CSS__: 2D and 3D transforms, colors, `width`, `opacity`, `border-radius`, `margin`, and almost every CSS
  value with the help of CSSPlugin

- __SVG attributes__: `viewBox`, `width`, `height`, `fill`, `stroke`, `cs`, `r`, `opactiy`, etc.

- __Any numeric value__. For example, an object that gets rendered to an HTML5 `<canvas>`. Animate the
  camera position in a 3D scene of filter values. GSAP is often used with Three.js and Pixi.js.

### What's GSAP Exactly?

GSAP is a suite of tools for scripted animation. It includes:

- __TweenLite__: the lightweight core of the engine which animates any property of any object. It can be
  expanded using plugins

- __TweenMax__: the most feature-packed (and populat) tool in the arsenal. For convenience and loading
  efficiency, it includes TweenLite, TimelineLite, TimelineMax, CSSPlugin, AttrPlugin, RoundPropsPlugin,
  BezierPlugin, and EasePack

- __TimelineLite__ & __Timeline Max__: sequencing tools that act as containers for tweens, making it
  simple to control entire groups and precisely manage relative timing.

- Extras like easing tools, plugins, utilities like Draggable, and more.

## Installation

### CDN

The simplest way to load GSAP is from the CDN with a `<script>` tag. TweenMax and all public GSAP
files are hosted on Cloudfare.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
```

### NPM

```bash
npm install gsap
```

## Tweening Basics

### `TweenMax.to()`

To create an animation `TweenMax.to()` needs 3 things:

- __target__: the object you are animating. This an be a raw object, an array of objects, or selector
  text liek ".myClass"

- __duration__: time (in seconds)

- __vars__: an objet with property/value pairs you're animating (like `opacity:0.5`) and other optional
  special properties like `onComplete`

For example to move an element with id "logo" to and `x` position of 100 over the course of 1 second:

```js
TweenMax.to("#logo", 1, {x:100});
```

Note: Remember that GSAP isn't just for DOM elements, so you could even animate custom properties
of a raw object:

```js
var obj = {prop:10};
TweenMax.to(obj, 1, {
  prop:200,
  //onUpdate fires each time the tween updates; we'll explain callbacks later.
  onUpdate:function() {
    console.log(obj.prop); //logs the value on each update.
  }
});
```

### `TweenMax.from()`

Sometimes it's convenient to set up your elements where they should end up (after an intro animation)
and then animate __from__ othe values.

For example:
```js
TweenMax.from("#logo", 1, {x:100});
```

### `TweenMax.fromTo()`

There is also a `fromTo()` method that allows you to define the starting values __and__ the ending values.

For example:
```js
TweenMax.fromTo("#logo", 1.5, {width:0, height:0}, {width:100, height:200});
```

## Plugins

Plugins are like special properties that get dynamically added to GSAP in order to inject extra
abilities. This keeps the core engine small and efficient, yet allows for unlimited expansion.
Each plugin is associated with a specific property name.

Among the most popular plugins are:

- CSSPlugin: animates CSS values
- AttrPlugin: animates attributes of DOM nodes including SVG
- BezierPlugin: animates along a curved Bezier path
- MorphSVGPlugin: smooth morphing of complex SVG paths
- DrawSVGPlugin: animates the length and position of SVG strokes

### CSSPlugin

Features:
- __normalizes behavior__ across browsers and works around various browser bugs and inconsistencies
- __optimizes performace__ by auto-layerizing, cahcing transform components, preventing layout thrashing
- controls __2D and 3D transform components__
- reads computed values so __you don't have to manually define starting values__
- animates __complex values__ like `borderRadius: "50% 50%`
- applies vendor-specific _prefixes (`-mox-`, `-ms-`, `-webkit-`, etc.) when necessary
- animates __CSS Variables__
- handles __color interpolation__ (rgb, rgba, hsl, hsla, hex)
- normalizes behavior between __SVG__ and DOM elements

Because animating CSS properties is so common, GSAP automatically senses when the target is a DOM
element and adds a `css:{}` wrapper. So internally `{x: 100, opacity: 0.5, onComplete:myFunc}` becomes
`{css: {x:100, opacity:0.5}, onComplete:myFunc}`

CSSPlugin recognizes a number of short codes for transform-related properties:

| GSAP           | CSS                          |
|:---------------|:-----------------------------|
| x: 100         | transform: translateX(100px) |
| y: 100         | transform: translateY(100px) |
| rotation: 360  | transform: rotate(360deg)    |
| rotationX: 360 | transform: rotateX(360deg)   |
| rotationY: 360 | transform: rotateY(360deg)   |
| skewX: 45      | transform: skewX(45deg)      |
| skewY: 45      | transform: skewY(45deg)      |
| scale: 2       | transform: scale(2, 2)       |
| scaleX: 2      | transform: scaleX(2)         |
| scaleY: 2      | transform: scaleY(2)         |
| xPercent: 50   | transform: translateX(50%)   |
| yPercent: 50   | transform: translateY(50%)   |

Notes:
- Be sure to __camelCase__ all hyphenated properties. `font-size` becomes `fontSize`
- When animating positional properties such as `left` and `top`, its imperative that the elements you
  are trying to move also have a CSS position of `absolute`, `relative`, or `fixed`
- vw/vh units aren't currently support natively, but it's easy to mimic. `x: window.innerWidth * (50 / 100)`

## Special Properties

A __special property__ is a reserved keyword that GSAP handles differently from a normal (animated)
property. Special properties are used to define callbacks, delays, easing, and more.

A basic example of a special property is delay which delays the animation before starting.

```js
TweenMax.to("#logo", 1, {x:100, delay:3});
```

Other common special properties are:
- __onComplete__: a callback that should be triggered when the animation finishes
- __onUpdate__: a callback that should be triggered every time the animation updates/renders
- __ease__: the ease that should be used (ex. `Power2.easeInOut`)

## Easing

__Easing__ determines the _style_ of movement between point A and point B. It controls the rate of
change during a tween.

To use the __ease__ special property:

```js
TweenMax.to("#logo", 1, {x:300, ease:Bounce.easeOut});
```

## Callbacks

Callbacks invoke a function when a specific animation-related event occurs:

- __onComplete__: called when the animation has completed.
- __onStart__: called when the animation begins
- __onUpdate__: called every time the animation updates (on every frame while the animation is active).
- __onRepeat__: called each time the animation repeats (only available in TweenMax and TimelineMax).
- __onReverseComplete__: called when the animation has reached its beginning again when reversed.

For example:
```js
TweenMax.to("#logo", 1 {x:100, onComplete:tweenComplete});

function tweenComplete() {
  console.log("the tween is complete");
}
```

Each callback function can optionally be passed any amount of parameters. Since there can be multiple
parameters, they must be passed as an Array (even if there is only one).
```js
TweenMax.to("#logo", 1, {x:100, onComplete:tweenComplete, onCompleteParams:["done!"]});

function tweenComplete(message) {
  console.log(message);
}
```

## Controlling Animations

To control an animation, you need an instance to work with. The `to()`, `from()`, and `fromTo`
methods all return an instance, so you can store it as a variable and then control it.

```js
//create a reference to the animation
var tween = TweenMax.to("#logo", 1 {x:100});

//pause
tween.pause();

//resume (honors direction - reversed or not)
tween.resume();

//reverse (always goes back towards the beginning)
tween.reverse();

//jump to exactly 0.5 seconds into the tween
tween.seek(0.5);

//jump to exacty 1/4th into the tween's progress:
tween.progress(0.25);

//make the tween go half-speed
tween.timeScale(0.5);

//make the tween go double-speed
tween.timeScale(2);

//immediately kill the tween and make it eligible for garbage collection
tween.kill();
```

## Timeline Sequencing

Choreographing complex sequences is simple with GSAP's TimelineLite and TimelineMax.

A timeline is a __container__ for tweens where you place them in time (like a schedule). They can
overlap or have gaps between them; you have total control. As the timeline's playhead moves, it scrubs
across its child tweens and renders them accordingly! Insert as many as you want and control the
entire group as a whole with the standard methods (`play()`, `reverse()`, `pause()`, etc.)

You can even nest timelines within timelines.

### When to use a timeline

- To control a group of animations as a whole
- To build a sequence without messing with lots of delay values (progressively build so that timing
  adjustments to earlier animations automatically affect later ones, greatly simplifying experimentation
  and maintenance)
- To modularize your animation code
- To do any kind of complex choreographing
- To fire callbacks based on a group of animations (like "after all of these animations are done,
  call myFunction()")

### Basic Sequencing

Timelines have the familiar `to()` , `from()`, and `fromTo()` methods that provide a quick way to create
a tween and `add()` it to the timeline.

```js
//create a timeline instance
var tl = new TimelineMax();
//the following two lines do the SAME thing:
tl.add( TweenMax.to("#id", 2, {x:100}) );
tl.to("#id", 2, {x:100}); //shorter syntax!
```

By default, animations are inserted one-after-the-other, but it's easy to control precisely where
things go using the position parameter.

### Method Chaining

Method chaining can keep your code very concise.

```js
var tl = new TimelineMax();

// Bad: chain all to() methods together on one line
tl.to(".green", 1, {x:200}).to(".orange", 1, {x:200, scale:0.2}).to(".grey", 1, {x:200, scale:2, y:20});

// Good: we recommend breaking each to() onto its own line for legibility
tl.to(".green", 1, {x:200})
  .to(".orange", 1, {x:200, scale:0.2})
  .to(".grey", 1, {x:200, scale:2, y:20});
```

### Position Parameter

The position parameter controls the placement of your tweens, labels, callbacks, pauses, and even
nested timelines.

The position parameter follows the vars object as follows:
```js
tl.to(element, 1, {x:200})
  //1 second after end of timeline (gap)
  .to(element, 1, {y:200}, "+=1")
  //0.5 seconds before end of timeline (overlap)
  .to(element, 1, {rotation:360}, "-=0.5")
  //at exactly 6 seconds from the beginning of the timeline (absolute)
  .to(element, 1, {scale:4}, 6);
```

### Control

Timelines and tweens share a common set of control methods. And since any animation's playhead is
controlled by its parent timeline, that means pausing a timeline's playhead automatically affects
all of its children.

### Special Properties

Timelines have special properties that you can optionally pass into the constructor to configure
them.

The most commonly used are:
- __repeat__: the number of times the animation will repeat.
- __repeatDelay__: the amount of time (in seconds) between repeats.
- __yoyo__: if true, playback will alternate forwards and backwards on each repeat.
- __delay__: the time (in seconds) before the timeline should start
- __onComplete__: a function to call when the timeline has finished playing

## Getter / Setter Methods

Tweens and timelines not only share similar callbacks and control methods, but they also have
common methods for getting and setting specific properties of the animation. The most commonly used
getter / setter methods are:

- __time()__: the local position of the playhead (the current time, in seconds) not including any
  repeats or repeatDelays.

- __progress()__: the tween's progress which is a value between 0 and 1 indicating the position of the
  playhead where 0 is at the beginning, 0.5 is halfway complete, and 1 is at the end.

- __duration()__: the animation's duration (in seconds), not including any repeats or repeatDelays.

- __delay()__: the animation's initial delay (the length of time in seconds before the animation
  should begin).

