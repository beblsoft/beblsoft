# Velocity.js Documentation

Velocity is an animation engine with a similar API to jQuery's `$.animate()`. It has no dependencies.
It's incredibly fast, and it features color animation, transforms, loops, easings, SVG support, and
scrolling.

Relevant URLs:
[Github](https://github.com/julianshapiro/velocity),
[Docs](https://github.com/julianshapiro/velocity/wiki),
[NPM](https://www.npmjs.com/package/velocity-animate)

## Installation

### Velocity CDN

```html
<!-- Choose One -->
<script src="//cdn.jsdelivr.net/npm/velocity-animate@1.5/velocity.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/velocity/1.5.2/velocity.min.js"></script>
```

### Velocity UI Pack CDN

```html
<!-- Choose One -->
<script src="//cdn.jsdelivr.net/npm/velocity-animate@1.5/velocity.ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/velocity/1.5.2/velocity.ui.min.js"></script>
```

### NPM

```bash
npm install velocity-animate
```

```js
import "velocity-animate/velocity.ui.min.js";
```

## Basics

### Arguments

Velocity can be called in two distinct ways, either as a function call, or as a chained call.
In both cases the arguments are identical except for this way of calling Velocity itself.

#### Global Call

Can call `Velocity(...)` directly followed by elements and other arguments.

```js
var elements = document.querySelector("div");

Velocity(elements, {"color": "red"});
```

#### Chaining

In this preferred form the element or list is selected, and then the `.velocity()` call is chained
with the first argument jumping directly to the properties.

```js
document.querySelector("div").velocity({"color": "red"});
```

#### Properties

Velocity takes a map of CSS properties and values as its first argument. An options object can optionally
be passed in as a second argument:

```js
element.velocity({
    width: "500px"
}, {
    duration: 400,
    easing: "swing",
    queue: "",
    begin: undefined,
    progress: undefined,
    complete: undefined,
    loop: false,
    delay: false
});
```

Option defaults can be globally overriden by modifying `Velocify.defaults`

### Chaining

When multiple Velocity calls are stacked onto an element (or a series of elements), they automatically
queue onto one another - with each one firing once its prior animation has completed:

```js
element
    /* Animate the width property. */
    .velocity({ width: 75 })
    /* Then, when finished, animate the height property. */
    .velocity({ height: 0 });
```

### Colors

Internally Velocity will convert all colors to their `rgb` / `rgba` values before using.

```js
$element.velocity({
    backgroundColor: [ "rgba(red, 0.5)", "rgba(green, 0.25)" ]
});
```

### Properties

Velocity auto-prefixes properties (e.g. transform becomes webkit-transform on WebKit browsers); do
not prefix properties yourself.

Velocity animates any numeric values found. For example:

```js
{ padding: "10px" }
{ padding: "10px 5px 20px 5px" }
```

### Values

Velocity passes any units on unchanged where possible.

Velocity also allows for some very simple "from here" type maths by prefixing the number with a
`+=`. `-=`, `*=`, or `\=`.

```js
$element.velocity({
    top: 50, // Defaults to the px unit type
    left: "50%",
    height: "*=2" // Double the current height
});
```

