# Anime.js Documentation

Anime.js is a lightweight JavaScript animation library with a simple, yet powerful API.
It works with CSS properties, SVG, DOM attributes, and JavaScript Objects.

Relevant URLs:
[Home](https://animejs.com/),
[Github](https://github.com/juliangarnier/anime/),
[Documentation](https://animejs.com/documentation/),
[NPM](https://www.npmjs.com/package/animejs),
[CodePen](https://codepen.io/collection/XLebem/)

## Installation

### NPM

```bash
npm install animejs --save
```

ES6 module import:
```js
import anime from 'lib/anime.es.js';
```

CommonJS import:
```js
const anime = require('lib/anime.js');
```

### CDN

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js"></script>
```

## Hello World

```html
<!DOCTYPE html>
<html>

<head>
  <title>Hello World</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js"></script>
</head>

<body>
  <div class="box">Hello World!</div>
  <script>
  anime({
    targets: 'div',
    translateX: 250,
    rotate: '1turn',
    duration: 4000
  });

  </script>
  <style>
  .box {
    padding: 10px;
    width: 100px;
    height: 100px;
    background-color: green;
  }
  </style>

</body>

```
