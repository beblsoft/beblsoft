# ChartJS Documentation

ChartJS is a simple yet flexible JavaScript charing library for designers and developers.

Features:

- __Open Source__: Community maintained project
- __8 Chart Types__
- __HTML 5 Canvas__: Great rendering performance across all modern browsers (IE11+)
- __Responsive__: Redraws charts on window resize for perfect scale granularity

Relevant URLs:
[Home](https://www.chartjs.org/),
[Github](https://github.com/chartjs/Chart.js),
[Samples](https://www.chartjs.org/samples/latest/)

## Getting Started

### Hello World

First, add canvas to page:

```html
<canvas id="myChart"></canvas>
```

Import Chart.js:

```html
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
```

Create first chart:

```js
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45]
        }]
    },

    // Configuration options go here
    options: {}
});
```

### Installation

#### NPM

```bash
npm install chart.js --save
```

#### CDN

```html
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
```

### Integration

Chart.js can be integrated with plain JavaScript or with different module loaders.

#### Script Tag

```html
<script src="path/to/chartjs/dist/Chart.js"></script>
<script>
    var myChart = new Chart(ctx, {...});
</script>
```

#### Common JS

```js
var Chart = require('chart.js');
var myChart = new Chart(ctx, {...});
```

#### Bundlers (i.e. Webpack)

```js
import Chart from 'chart.js';
var myChart = new Chart(ctx, {...});
```

#### Content Security Policy

By default, Chart.js injects CSS directly into the DOM. For webpages secured using Content Security
Policy (CSP), this requires to allow `style-src 'unsafe-inline`. For stricter CSP environments,
where only `style-src 'self` is allowed, the following CSS file needs to be manually added to the
webpage.

```html
<link rel="stylesheet" type="text/css" href="path/to/chartjs/dist/Chart.min.css">
```

And the style injection must be turned off __before creating the first chart__:

```js
// Disable automatic style injection
Chart.platform.disableCSSInjection = true;
```

### Basic Usage

To create a chart, we need to instantiate the `Chart` class. To do this, we need to pass in the node,
jQuery instance, or 2d context of the canvas of where we want to draw the chart.

```html
<canvas id="myChart" width="400" height="400"></canvas>
```

```js
// Any of the following formats may be used
var ctx = document.getElementById('myChart');
var ctx = document.getElementById('myChart').getContext('2d');
var ctx = $('#myChart');
var ctx = 'myChart';
```

The following example instantiates a bar chart showing the number of votes for different colors.

```html
<canvas id="myChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
```

## Configuration

### Accessibility

Chart.js are rendered on user provided `canvas` elements. Thus, it is up to the user to create the
`canvas` element in a way that is accessible. The `canvas` element has support in all browsers and
will render on screen but the `canvas` content will not be accessible to screen readers.

With `canvas`, the accessibility has to be added with ARIA attributes on the `canvas` element.

#### Aria

By setting the `role` and `aria-label`, this `canvas` has an accessible name:

```html
<canvas id="goodCanvas1" width="400" height="100" aria-label="Hello ARIA World" role="img"></canvas>
```

#### Fallback

This `canvas` element has a text alternative via fallback content.

```html
<canvas id="okCanvas2" width="400" height="100">
    <p>Hello Fallback World</p>
</canvas>
```

### Responsive

Terms:
- display size: `width` and `height` are HTML. Can only used fixed size units (no `vw` or `vh`)
- render size: `style.width` and `style.height` can used relative sizes.

Chart.js provides a few options to enable responsiveness and control the resize behavior of charts by
detecting when the canvas _display_ size changes and update the _render_ size accordingly.

| Name                        | Type     | Default | Description                                                                                                                                                                                      |
|:----------------------------|:---------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| responsive                  | boolean  | true    | Resizes the chart canvas when its container does (important note...).                                                                                                                            |
| responsiveAnimationDuration | number   | 0       | Duration in milliseconds it takes to animate to new size after a resize event.                                                                                                                   |
| maintainAspectRatio         | boolean  | true    | Maintain the original canvas aspect ratio (width / height) when resizing.                                                                                                                        |
| aspectRatio                 | number   | 2       | Canvas aspect ratio (i.e. width / height, a value of 1 representing a square canvas). Note that this option is ignored if the height is explicitly defined either as attribute or via the style. |
| onResize                    | function | null    | Called when a resize occurs. Gets passed two arguments: the chart instance and the new size.                                                                                                     |

Detecting when the canvas size changes can not be done directly from the `canvas` element. Chart.js
uses the parent container to update the canvas _render_ and _display_ sizes. However, this requires
the container to be __relatively positioned__ and __dedicated to the chart canvas only__.
Responsiveness can then be achieved by setting relative values for the container size.

```html
<div class="chart-container" style="position: relative; height:40vh; width:80vw">
    <canvas id="chart"></canvas>
</div>
```

The chart can also be programmatically resized by modifying the container size:

```js
chart.canvas.parentNode.style.height = '128px';
chart.canvas.parentNode.style.width = '128px';
```

Note that in order fo rthe above code to correctly resize the chart height, the `maintainAspectRatio`
option must also be set to `false`.

### Pixel Ratio

By default the chart's canvas will use a 1:1 pixel ratio, unless the physical display has a higher
pixel ration (e.g. Retina Displays).

Setting `devicePixelRation` to a value other than 1 will force the canvas size to be scaled by that
amount, relative to the container size. THere should be no visible difference on screen; the difference
will only be visible when the image is zoomed or printed.

| Name             | Type   | Default                 | Description                                     |
|:-----------------|:-------|:------------------------|:------------------------------------------------|
| devicePixelRatio | number | window.devicePixelRatio | Override the window's default devicePixelRatio. |

### Interactions

The hover configuration is passed into the `options.hover` namespace. The global hover configuration is
at `Chart.defaults.global.hover`.

The following options allow interaction configuration:

| Name              | Type    | Default   | Description                                                                                                                                                             |
|:------------------|:--------|:----------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| mode              | string  | 'nearest' | Sets which elements appear in the tooltip. See Interaction Modes for details.                                                                                           |
| intersect         | boolean | true      | if true, the hover mode only applies when the mouse position intersects an item on the chart.                                                                           |
| axis              | string  | 'x'       | Can be set to 'x', 'y', or 'xy' to define which directions are used in calculating distances. Defaults to 'x' for 'index' mode and 'xy' in dataset and 'nearest' modes. |
| animationDuration | number  | 400       | Duration in milliseconds it takes to animate hover style changes.                                                                                                       |

#### Events

The following properties define how the chart interacts with events.

| Name    | Type     | Default                                                       | Description                                                                                                                                      |
|:--------|:---------|:--------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
| events  | string[] | ['mousemove', 'mouseout', 'click', 'touchstart', 'touchmove'] | The events option defines the browser events that the chart should listen to for tooltips and hovering. more...                                  |
| onHover | function | null                                                          | Called when any of the events fire. Called in the context of the chart and passed the event and an array of active elements (bars, points, etc). |
| onClick | function | null                                                          | Called if the event is of type 'mouseup' or 'click'. Called in the context of the chart and passed the event and an array of active elements.    |

To have the chart only respond to clikc events.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        // This chart will not respond to mousemove, etc
        events: ['click']
    }
});
```

#### Interaction Modes

When configuring interaction with the graph via hover or tooltips, a number of different modes are
available. The modes are detailed below and how they behave in conjunction with the `intersect` setting.

##### point

Finds all the items that intersect the point.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'point'
        }
    }
});
```

##### nearest

Gets the items that are at the nearest distance to the point. The nearest item is determined based
on the distance to the center of the chart item (point, bar). You can use the `axis` setting to define
which directions are used in distance calculation.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'nearest'
        }
    }
});
```

##### index

Finds items at the same index.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'index'
        }
    }
});
```

##### dataset

Finds items in the same dataset.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'dataset'
        }
    }
});
```

##### x

Returns all items that would intersect based on the `x` coordinate of the position only.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'x'
        }
    }
});
```

##### y

Returns all items that would intersect based on the `Y` coordinate of the position.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            mode: 'y'
        }
    }
})
```

### Options

#### Scriptable

Scriptable options also accept a function which is called for each of the underlying data values.
The function takes a unique `context` representing contextual information.

```js
color: function(context) {
    var index = context.dataIndex;
    var value = context.dataset.data[index];
    return value < 0 ? 'red' :  // draw negative values in red
        index % 2 ? 'blue' :    // else, alternate values in blue and green
        'green';
}
```

#### Indexable

Indexable options accapt an array in which each item corresponds to the element at the same index.
Note that this method requires to provide as many items as data, so, in must cases, using a function
is more appropriate.

```js
color: [
    'red',    // color for data at index 0
    'blue',   // color for data at index 1
    'green',  // color for data at index 2
    'black',  // color for data at index 3
    //...
]
```

#### Context

The option context is used to give contextual information when resolving options and currently only
applies to scriptable options.

The `context` object contains the following properties:
- `chart`: the associated chart
- `dataIndex`: index of the current data
- `dataset`: dataset at index datasetIndex
- `datasetIndex`: index of the current dataset

Since the context can represent different types of entities (dataset, daata, etc), some properties
may be `undefined` so be sure to test any context property before using it.

### Colors

When supplying colors to Chart options, you can use a number of formats. You can specify the color
as a string in hexadecimal, RGB, or HSL notations. If a color is needed, but not specified, Chart.js
will use the global default color. This color is stored at `Chart.defaults.global.defaultColor`.
It is initially set to 'rgba(0, 0, 0, 0.1)'.

#### Patters and Gradients

An alternative option is to pass a CanvasPatter or CanvasGradient object instead of a string colour.

```js
var img = new Image();
img.src = 'https://example.com/my_image.png';
img.onload = function() {
    var ctx = document.getElementById('canvas').getContext('2d');
    var fillPattern = ctx.createPattern(img, 'repeat');

    var chart = new Chart(ctx, {
        data: {
            labels: ['Item 1', 'Item 2', 'Item 3'],
            datasets: [{
                data: [10, 20, 30],
                backgroundColor: fillPattern
            }]
        }
    });
};
```

Using pattern fills for data graphics can help viewers with vision deficiencies (e.g. color-blindness or
partial sight) to more easily understand your data.

Use the [Patternomaly](https://github.com/ashiguruma/patternomaly) library to generate pattern fills.

```js
var chartData = {
    datasets: [{
        data: [45, 25, 20, 10],
        backgroundColor: [
            pattern.draw('square', '#ff6384'),
            pattern.draw('circle', '#36a2eb'),
            pattern.draw('diamond', '#cc65fe'),
            pattern.draw('triangle', '#ffce56')
        ]
    }],
    labels: ['Red', 'Blue', 'Purple', 'Yellow']
}
```

### Fonts

There are 4 special global settings that can change all the fonts on the cart. These are in
`Chart.defaults.global`. The global font settings only apply when more specific options are not
included in the config.

```js
Chart.defaults.global.defaultFontColor = 'red';
let chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        legend: {
            labels: {
                // This more specific font property overrides the global property
                fontColor: 'black'
            }
        }
    }
});
```

| Name                | Type   | Default                                              | Description                                                                                   |
|:--------------------|:-------|:-----------------------------------------------------|:----------------------------------------------------------------------------------------------|
| `defaultFontColor`  | Color  | '#666'                                               | Default font color for all text.                                                              |
| `defaultFontFamily` | string | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Default font family for all text.                                                             |
| `defaultFontSize`   | number | 12                                                   | Default font size (in px) for text. Does not apply to radialLinear scale point labels.        |
| `defaultFontStyle`  | string | 'normal'                                             | Default font style. Does not apply to tooltip title or footer. Does not apply to chart title. |

### Global Configuration

Introduced in Chart.js 1.0 to keep configuration DRY, and allow for changing options globally across
chart types, avoiding the need to specify options for each instance.

Chart.js mergest the options object passed to the chart with the global configuraiton using chart
type defaults and scales defaults appropriately. The global options are defined in
`Chart.defaults.global`.

```js
Chart.defaults.global.hover.mode = 'nearest';

// Hover mode is set to nearest because it was not overridden here
var chartHoverModeNearest = new Chart(ctx, {
    type: 'line',
    data: data
});

// This chart would have the hover mode that was passed in
var chartDifferentHoverMode = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        hover: {
            // Overrides the global setting
            mode: 'index'
        }
    }
});
```

### Animations

Chart.js animates charts out of the box.

#### Options

| Name       | Type     | Default        | Description                                    |
|:-----------|:---------|:---------------|:-----------------------------------------------|
| duration   | number   | 1000           | The number of milliseconds an animation takes. |
| easing     | string   | 'easeOutQuart' | Easing function to use.                        |
| onProgress | function | null           | Callback called on each step of an animation.  |
| onComplete | function | null           | Callback called at the end of an animation.    |

#### Easings

- `'linear'`
- `'easeInQuad'`
- `'easeOutQuad'`
- `'easeInOutQuad'`
- `'easeInCubic'`
- `'easeOutCubic'`
- `'easeInOutCubic'`
- `'easeInQuart'`
- `'easeOutQuart'`
- `'easeInOutQuart'`
- `'easeInQuint'`
- `'easeOutQuint'`
- `'easeInOutQuint'`
- `'easeInSine'`
- `'easeOutSine'`
- `'easeInOutSine'`
- `'easeInExpo'`
- `'easeOutExpo'`
- `'easeInOutExpo'`
- `'easeInCirc'`
- `'easeOutCirc'`
- `'easeInOutCirc'`
- `'easeInElastic'`
- `'easeOutElastic'`
- `'easeInOutElastic'`
- `'easeInBack'`
- `'easeOutBack'`
- `'easeInOutBack'`
- `'easeInBounce'`
- `'easeOutBounce'`
- `'easeInOutBounce'`

#### Callbacks

The `onProgress` and `onComplete` callbacks are useful for synchronizing an external draw to the chart
animation. The callback is passed a `Chart.Animation` instance.

```js
{
    // Chart object
    chart: Chart,

    // Current Animation frame number
    currentStep: number,

    // Number of animation frames
    numSteps: number,

    // Animation easing to use
    easing: string,

    // Function that renders the chart
    render: function,

    // User callback
    onAnimationProgress: function,

    // User callback
    onAnimationComplete: function
}
```

The following example fills a progress bar during the chart animation.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        animation: {
            onProgress: function(animation) {
                progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
            }
        }
    }
});
```

### Layout

The layout configuration is passed into the `options.layout` namespace. THe global options are
defined in `Chart.defaults.global.layout`.

| Name    | Type          | Default | Description                          |
|:--------|:--------------|:--------|:-------------------------------------|
| padding | number,object | 0       | The padding to add inside the chart. |

#### Padding

If this value is a number, it is applied to all sides of the chart (left, top, right, bottom). If
this value is an object, the `left` property defines the left padding. Similarly the `right`, `top`,
and `bottom` properties can also be specified.

```js
let chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        layout: {
            padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 0
            }
        }
    }
});
```

### Legend

#### Options

The legend configuration is passed into the `options.legend` namespace. The global options for the
chart legend is defined in `Chart.defaults.global.legend`.

| Name      | Type     | Default | Description                                                                                                                                        |
|:----------|:---------|:--------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| display   | boolean  | true    | Is the legend shown?                                                                                                                               |
| position  | string   | 'top'   | Position of the legend.                                                                                                                            |
| fullWidth | boolean  | true    | Marks that this box should take the full width of the canvas (pushing down other boxes). This is unlikely to need to be changed in day-to-day use. |
| onClick   | function |         | A callback that is called when a click event is registered on a label item.                                                                        |
| onHover   | function |         | A callback that is called when a 'mousemove' event is registered on top of a label item.                                                           |
| onLeave   | function |         | A callback that is called when a 'mousemove' event is registered outside of a previously hovered label item.                                       |
| reverse   | boolean  | false   | Legend will show datasets in reverse order.                                                                                                        |
| labels    | object   |         | See the Legend Label Configuration section below.                                                                                                  |

#### Position

Position of the legend. Options are:
- 'top'
- 'left'
- 'bottom'
- 'right'

#### Label Configuration

The legen label configuration is nested below the legend configuration using the `labels` key.

| Name           | Type     | Default                                              | Description                                                                                                                                            |
|:---------------|:---------|:-----------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------|
| boxWidth       | number   | 40                                                   | Width of coloured box.                                                                                                                                 |
| fontSize       | number   | 12                                                   | Font size of text.                                                                                                                                     |
| fontStyle      | string   | 'normal'                                             | Font style of text.                                                                                                                                    |
| fontColor      | Color    | '#666'                                               | Color of text.                                                                                                                                         |
| fontFamily     | string   | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Font family of legend text.                                                                                                                            |
| padding        | number   | 10                                                   | Padding between rows of colored boxes.                                                                                                                 |
| generateLabels | function |                                                      | Generates legend items for each thing in the legend. Default implementation returns the text + styling for the color box. See Legend Item for details. |
| filter         | function | null                                                 | Filters legend items out of the legend. Receives 2 parameters, a Legend Item and the chart data.                                                       |
| usePointStyle  | boolean  | false                                                | Label style will match corresponding point style (size is based on the mimimum value between boxWidth and fontSize).                                   |

#### Legend Item Iterface

Items passed to the legend `onClick` function are ones returned from `labels.generateLabels`.
These items must implement the following interface.

```js
{
    // Label that will be displayed
    text: string,

    // Fill style of the legend box
    fillStyle: Color,

    // If true, this item represents a hidden dataset. Label will be rendered with a strike-through effect
    hidden: boolean,

    // For box border. See https://developer.mozilla.org/en/docs/Web/API/CanvasRenderingContext2D/lineCap
    lineCap: string,

    // For box border. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash
    lineDash: number[],

    // For box border. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineDashOffset
    lineDashOffset: number,

    // For box border. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineJoin
    lineJoin: string,

    // Width of box border
    lineWidth: number,

    // Stroke style of the legend box
    strokeStyle: Color,

    // Point style of the legend box (only used if usePointStyle is true)
    pointStyle: string
}
```

### Title

The chart title defines text to draw at the top of the chart

#### Configuration

The title configuration is passed into the `options.title` namespace. The global options for the chart
title is defined in `Chart.defaults.global.title`.

| Name       | Type            | Default                                              | Description                                                                          |
|:-----------|:----------------|:-----------------------------------------------------|:-------------------------------------------------------------------------------------|
| display    | boolean         | false                                                | Is the title shown?                                                                  |
| position   | string          | 'top'                                                | Position of title. more...                                                           |
| fontSize   | number          | 12                                                   | Font size.                                                                           |
| fontFamily | string          | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Font family for the title text.                                                      |
| fontColor  | Color           | '#666'                                               | Font color.                                                                          |
| fontStyle  | string          | 'bold'                                               | Font style.                                                                          |
| padding    | number          | 10                                                   | Number of pixels to add above and below the title text.                              |
| lineHeight | number,string   | 1.2                                                  | Height of an individual line of text. See MDN.                                       |
| text       | string,string[] | ''                                                   | Title text to display. If specified as an array, text is rendered on multiple lines. |

#### Position

Posible title position values are:
- 'top'
- 'left'
- 'bottom'
- 'right'

#### Example

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        title: {
            display: true,
            text: 'Custom Chart Title'
        }
    }
});
```

### Tooltip

#### Configuration

The tooltip configuration is passed into the `options.tooltips` namespace. The global options for the
chart tooltips is defined in `Chart.defaults.global.tooltips`.

| Name               | Type     | Default                                              | Description                                                                                                                                 |
|:-------------------|:---------|:-----------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------|
| enabled            | boolean  | true                                                 | Are on-canvas tooltips enabled?                                                                                                             |
| custom             | function | null                                                 | See custom tooltip section.                                                                                                                 |
| mode               | string   | 'nearest'                                            | Sets which elements appear in the tooltip. more....                                                                                         |
| intersect          | boolean  | true                                                 | If true, the tooltip mode applies only when the mouse position intersects with an element. If false, the mode will be applied at all times. |
| position           | string   | 'average'                                            | The mode for positioning the tooltip. more...                                                                                               |
| callbacks          | object   |                                                      | See the callbacks section.                                                                                                                  |
| itemSort           | function |                                                      | Sort tooltip items. Allow sorting tooltip items.                                                                                            |
| filter             | function |                                                      | Filter tooltip items. Allows filtering of tooltip items.                                                                                    |
| backgroundColor    | Color    | 'rgba(0, 0, 0, 0.8)'                                 | Background color of the tooltip.                                                                                                            |
| titleFontFamily    | string   | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Title font.                                                                                                                                 |
| titleFontSize      | number   | 12                                                   | Title font size.                                                                                                                            |
| titleFontStyle     | string   | 'bold'                                               | Title font style.                                                                                                                           |
| titleFontColor     | Color    | '#fff'                                               | Title font color.                                                                                                                           |
| titleSpacing       | number   | 2                                                    | Spacing to add to top and bottom of each title line.                                                                                        |
| titleMarginBottom  | number   | 6                                                    | Margin to add on bottom of title section.                                                                                                   |
| bodyFontFamily     | string   | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Body line font.                                                                                                                             |
| bodyFontSize       | number   | 12                                                   | Body font size.                                                                                                                             |
| bodyFontStyle      | string   | 'normal'                                             | Body font style.                                                                                                                            |
| bodyFontColor      | Color    | '#fff'                                               | Body font color.                                                                                                                            |
| bodySpacing        | number   | 2                                                    | Spacing to add to top and bottom of each tooltip item.                                                                                      |
| footerFontFamily   | string   | "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" | Footer font.                                                                                                                                |
| footerFontSize     | number   | 12                                                   | Footer font size.                                                                                                                           |
| footerFontStyle    | string   | 'bold'                                               | Footer font style.                                                                                                                          |
| footerFontColor    | Color    | '#fff'                                               | Footer font color.                                                                                                                          |
| footerSpacing      | number   | 2                                                    | Spacing to add to top and bottom of each footer line.                                                                                       |
| footerMarginTop    | number   | 6                                                    | Margin to add before drawing the footer.                                                                                                    |
| xPadding           | number   | 6                                                    | Padding to add on left and right of tooltip.                                                                                                |
| yPadding           | number   | 6                                                    | Padding to add on top and bottom of tooltip.                                                                                                |
| caretPadding       | number   | 2                                                    | Extra distance to move the end of the tooltip arrow away from the tooltip point.                                                            |
| caretSize          | number   | 5                                                    | Size, in px, of the tooltip arrow.                                                                                                          |
| cornerRadius       | number   | 6                                                    | Radius of tooltip corner curves.                                                                                                            |
| multiKeyBackground | Color    | '#fff'                                               | Color to draw behind the colored boxes when multiple items are in the tooltip.                                                              |
| displayColors      | boolean  | true                                                 | If true, color boxes are shown in the tooltip.                                                                                              |
| borderColor        | Color    | 'rgba(0, 0, 0, 0)'                                   | Color of the border.                                                                                                                        |
| borderWidth        | number   | 0                                                    | Size of the border.                                                                                                                         |

#### Position Modes

Posible modes are:
- `'average'` - place tooltip at the average position of the items displayed in the tooltip
- `'nearest'` - will place the tooltip at the position of the element closest to the event position

Creating a custom positioner:
```js
/**
 * Custom positioner
 * @function Chart.Tooltip.positioners.custom
 * @param elements {Chart.Element[]} the tooltip elements
 * @param eventPosition {Point} the position of the event in canvas coordinates
 * @returns {Point} the tooltip position
 */
Chart.Tooltip.positioners.custom = function(elements, eventPosition) {
    /** @type {Chart.Tooltip} */
    var tooltip = this;

    /* ... */

    return {
        x: 0,
        y: 0
    };
};
```

#### Callbacks

The tooltip label configuration is nested below the tooltip configuration using the `callbacks` key.
The tooltip has the following callbacks for providing text.

All the functions are called with the same arguments: a tooltip item and the `data` object passed to
the chart. All functions must return either a string or an array of strings. Array of strings are treated
as multiple lines of text.

| Name           | Arguments             | Description                                                                                          |
|:---------------|:----------------------|:-----------------------------------------------------------------------------------------------------|
| beforeTitle    | TooltipItem[], object | Returns the text to render before the title.                                                         |
| title          | TooltipItem[], object | Returns text to render as the title of the tooltip.                                                  |
| afterTitle     | TooltipItem[], object | Returns text to render after the title.                                                              |
| beforeBody     | TooltipItem[], object | Returns text to render before the body section.                                                      |
| beforeLabel    | TooltipItem, object   | Returns text to render before an individual label. This will be called for each item in the tooltip. |
| label          | TooltipItem, object   | Returns text to render for an individual item in the tooltip. more...                                |
| labelColor     | TooltipItem, Chart    | Returns the colors to render for the tooltip item. more...                                           |
| labelTextColor | TooltipItem, Chart    | Returns the colors for the text of the label for the tooltip item.                                   |
| afterLabel     | TooltipItem, object   | Returns text to render after an individual label.                                                    |
| afterBody      | TooltipItem[], object | Returns text to render after the body section.                                                       |
| beforeFooter   | TooltipItem[], object | Returns text to render before the footer section.                                                    |
| footer         | TooltipItem[], object | Returns text to render as the footer of the tooltip.                                                 |
| afterFooter    | TooltipItem[], object | Text to render after the footer section.                                                             |

The `label` callback can change the text that displays for a given data point. A common example is to
round data values.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += Math.round(tooltipItem.yLabel * 100) / 100;
                    return label;
                }
            }
        }
    }
});
```

The `labelColor` callback can change the color of a label.

```js
var chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'rgb(255, 0, 0)',
                        backgroundColor: 'rgb(255, 0, 0)'
                    };
                },
                labelTextColor: function(tooltipItem, chart) {
                    return '#543453';
                }
            }
        }
    }
});
```

#### Tooltip Item Interface

The tooltip items passed to the tooltip callbacks implement the following interface.

```js
{
    // Label for the tooltip
    label: string,

    // Value for the tooltip
    value: string,

    // X Value of the tooltip
    // (deprecated) use `value` or `label` instead
    xLabel: number | string,

    // Y value of the tooltip
    // (deprecated) use `value` or `label` instead
    yLabel: number | string,

    // Index of the dataset the item comes from
    datasetIndex: number,

    // Index of this data item in the dataset
    index: number,

    // X position of matching point
    x: number,

// Y position of matching point
y: number
}
```

### Elements

While chart types provide settings to configure the styling of each dataset, you sometimes want to
style all datasets the same way. A common example would be to stroke all the bars in the bar chart
with the same colour but change the fill per dataset.

#### Global Configuration

The elements options can be specified per chart or globally. The global options for elements are defined
in `Chart.defaults.global.elements`. For example, to set the border width of all bar charts globally
you would do:

```js
Chart.defaults.global.elements.rectangle.borderWidth = 2;
```

#### Point Configuration

Point elements are used to represent the points in a line, radar, or bubble chart.

Global point options: `Chart.defaults.global.elements.point`

| Name             | Type         | Default              | Description                                           |
|:-----------------|:-------------|:---------------------|:------------------------------------------------------|
| radius           | number       | 3                    | Point radius.                                         |
| pointStyle       | string,Image | 'circle'             | Point style.                                          |
| rotation         | number       | 0                    | Point rotation (in degrees).                          |
| backgroundColor  | Color        | 'rgba(0, 0, 0, 0.1)' | Point fill color.                                     |
| borderWidth      | number       | 1                    | Point stroke width.                                   |
| borderColor      | Color        | 'rgba(0, 0, 0, 0.1)' | Point stroke color.                                   |
| hitRadius        | number       | 1                    | Extra radius added to point radius for hit detection. |
| hoverRadius      | number       | 4                    | Point radius when hovered.                            |
| hoverBorderWidth | number       | 1                    | Stroke width when hovered.                            |

The following points styles are supported:

- `'circle'`
- `'cross'`
- `'crossRot'`
- `'dash'`
- `'line'`
- `'rect'`
- `'rectRounded'`
- `'rectRot'`
- `'star'`
- `'triangle'`

#### Line Configuration

Line elements are used to represent the line in a line chart.

Global line options: `Chart.defaults.global.elements.line`

| Name             | Type           | Default              | Description                                                                   |
|:-----------------|:---------------|:---------------------|:------------------------------------------------------------------------------|
| tension          | number         | 0.4                  | Bézier curve tension (0 for no Bézier curves).                                |
| backgroundColor  | Color          | 'rgba(0, 0, 0, 0.1)' | Line fill color.                                                              |
| borderWidth      | number         | 3                    | Line stroke width.                                                            |
| borderColor      | Color          | 'rgba(0, 0, 0, 0.1)' | Line stroke color.                                                            |
| borderCapStyle   | string         | 'butt'               | Line cap style. See MDN.                                                      |
| borderDash       | number[]       | []                   | Line dash. See MDN.                                                           |
| borderDashOffset | number         | 0.0                  | Line dash offset. See MDN.                                                    |
| borderJoinStyle  | string         | 'miter'              | Line join style. See MDN.                                                     |
| capBezierPoints  | boolean        | true                 | true to keep Bézier control inside the chart, false for no restriction.       |
| fill             | boolean,string | true                 | Fill location: 'zero', 'top', 'bottom', true (eq. 'zero') or false (no fill). |
| stepped          | boolean        | false                | true to show the line as a stepped line (tension will be ignored).            |

#### Rectangle Configuration

Rectangle elements are used to represent the bars in a bar chart.

Global rectangle options: `Chart.defaults.global.elements.rectangle`

| Name            | Type   | Default              | Description                                                    |
|:----------------|:-------|:---------------------|:---------------------------------------------------------------|
| backgroundColor | Color  | 'rgba(0, 0, 0, 0.1)' | Bar fill color.                                                |
| borderWidth     | number | 0                    | Bar stroke width.                                              |
| borderColor     | Color  | 'rgba(0, 0, 0, 0.1)' | Bar stroke color.                                              |
| borderSkipped   | string | 'bottom'             | Skipped (excluded) border: 'bottom', 'left', 'top' or 'right'. |

#### Arc Configuration

Arcs are used in the polar area, doughnut, and pie charts.

Global arc options: `Chart.defaults.global.elements.arc`.

| Name            | Type   | Default              | Description           |
|:----------------|:-------|:---------------------|:----------------------|
| backgroundColor | Color  | 'rgba(0, 0, 0, 0.1)' | Arc fill color.       |
| borderAlign     | string | 'center'             | Arc stroke alignment. |
| borderColor     | Color  | '#fff'               | Arc stroke color.     |
| borderWidth     | number | 2                    | Arc stroke width.     |

## Charts

### Line

A line chart is a way of plotting data points on a line. Often it is used to show trend data, or
the comparison of two data sets.

Example:
```js
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
});
```

#### Data Structure

The `data` property of a dataset for a line chart can be passed two formats:
1. `number[]`
   Ex. `data: [20, 10]`
   When the `data` is passed as an array of numbers, the x axis is generall a category. The points
   are placed onto the axis using their position in the array.

2. `Point[]`
   Ex. `data:[{x: 10, y:20}, {x:15, y:10}]`
   Each point is specified using an object containing `x` and `y` properties

### Bar

A bar chart provides a way of showing data values represented as vertical bars. It is sometimes used
to show trend data, and the comparison of multiple data sets side by side.

Example:
```js
var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: options
});
```

Example horizontal:
```js
var myBarChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: data,
    options: options
});
```

### Radar

A radar chart is a way of showing multiple data points and the variation between them.
They are often useful for comparing the points of two or more different data sets.

Example:
```js
var myRadarChart = new Chart(ctx, {
    type: 'radar',
    data: data,
    options: options
});
```

### Doughnut and Pie

Pie and doughnut charts are probably the most commonly used charts. They are divided into segments,
the arc of each segment shows the proportional value of each piece of data.

Pie and doughnut charts are effectively the same class in Chart.js, but have one different default
value - their `cutoutPercentage`. This equates what percentage of the inner should be cut out. This
defaults to `0` for pie charts. and `50` for doughnuts.

Example pie:
```js
// For a pie chart
var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: data,
    options: options
});
```

Example doughnut:
```js
// And for a doughnut chart
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});
```

### Polar Area

Polar area charts are similar to pie charts, but each segments has the same angle - the radius of the
segment differs depending on the value.

This type of chart is often useful when we want to show a comparison data similar to a pie chart, but
also show a scale of values for context.

Example:
```js
new Chart(ctx, {
    data: data,
    type: 'polarArea',
    options: options
});
```

### Bubble

A bubble chart is used to display three dimensions of data at the same time. The location of the bubble
is determined by the first two dimensions and the corresponding horizontal and vertical axes. The
third dimension is represented by the size of the individual bubbles.

Example:
```js
// For a bubble chart
var myBubbleChart = new Chart(ctx, {
    type: 'bubble',
    data: data,
    options: options
});
```

### Scatter

Scatter charts are based on basic line charts with the x axis changed to a linear axis. To use a scatter
chart, data must be passed as objects containing X and Y properties.

For example:
```js
var scatterChart = new Chart(ctx, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'Scatter Dataset',
            data: [{
                x: -10,
                y: 0
            }, {
                x: 0,
                y: 10
            }, {
                x: 10,
                y: 5
            }]
        }]
    },
    options: {
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
});
```

### Area

Both line and radar charts support a `fill` option on the dataset object which can be used to create
area between two datasets or a dataset and a boundary.

For example:
```js
new Chart(ctx, {
    data: {
        datasets: [
            {fill: 'origin'},      // 0: fill to 'origin'
            {fill: '+2'},          // 1: fill to dataset 3
            {fill: 1},             // 2: fill to dataset 1
            {fill: false},         // 3: no fill
            {fill: '-2'}           // 4: fill to dataset 2
        ]
    }
});
```

### Mixed Chart Types

With Chart.js, it is possible to create mixed charts that are a combination of two or more different
chart types. A common example is a bar chart shat also includes a line dataset.

For example:
```js
var mixedChart = new Chart(ctx, {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Bar Dataset',
            data: [10, 20, 30, 40]
        }, {
            label: 'Line Dataset',
            data: [50, 50, 50, 50],

            // Changes this dataset to become a line
            type: 'line'
        }],
        labels: ['January', 'February', 'March', 'April']
    },
    options: options
});
```

## Axes

Axes are an integral part of a chart. They are used to determine how data maps to a pixel value on
the chart.

### Cartesian

Axes that follow a cartesian grid are known as 'Cartesian Axes'. Cartesian axes are used for line,
bar, and bubble charts. The following cartesian axes are included by default:
- linear
- logarithmic
- category
- time

### Radial

Radial axes are used specifically for the radar and polar area chart types. These axes overlay the
chart area, rather than being positioned on one of the edges. The following radial axis is included
by default:
- linear

### Labelling

When creating a chart, you want to tell the viewer what data they are viewing. To do this, you need
to label the axis.

### Styling

There are a number of options to allow styling an axis. There are settings to control grid lines and ticks.

## Developers

Developer features allow extending and enhancing Chart.js in many different ways.

