
 var todaysDate = moment().format('DD-MMM-YYYY');
 console.log("Today is: " + todaysDate)

 var backAMonth = moment().subtract(30, 'days').format('DD-MMM-YYYY');
 console.log("Back a month is: " + backAMonth);

 // set a variable
 var todaysDatePlus60 = moment().add(60, 'days').format('DD-MMM-YYYY');
 console.log("Today (+60) is: " + todaysDatePlus60);

 var todaysDatePlus30 = moment().add(30, 'days').format('DD-MMM-YYYY');
 console.log("Today (+30) is: " + todaysDatePlus30);

 let json = {'startDate': todaysDatePlus60, 'endDate': todaysDatePlus30};
 console.log('Date argument' + ' ' + JSON.stringify(json));