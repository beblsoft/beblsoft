# Moment Projects Documentation

## index.html

Demonstrate usage of moment.js time formatting in the browser.

To run: open `index.html` in Chrome. View log messages on Chrome console.

