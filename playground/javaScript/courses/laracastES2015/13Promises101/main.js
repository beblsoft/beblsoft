var button = document.querySelector('button');

console.log('Before');

// async behavior, call back when clicked, promises help us get away from callback hell
// A promise is a placeholder or holding spot for an operation that has not yet taken place
//
button.onclick = function() {
	console.log('I was clicked.');
};
console.log('After');

// PROMISE Example reads as promise to give me the data at some path and then when you have it
// do the following or catch errors returned
// var promise = this.$http.get('/some/path');
// promise.then(function(data) {
// 
// });
// promise.catch(function(err) {
//	
// });
//
//
// Same thing, less syntax
// promise.then(function(data) {
//	
// }, function(err) {
//	
// });
var timer = new Promise(function (resolve, reject) {
	console.log('Init Promise');

	setTimeout(function() {
		console.log('Timeout done.');
		resolve();
	}, 2000);
});

timer.then(function() {
	console.log("Proceed now that time has concluded.");
});