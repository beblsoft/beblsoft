///////////////////////////////////////////////////////////////////////////////
console.log('->01BabelSetup');

class Person {
    constructor(name) {
        this.name = name;
    }
    greet() {
        return this.name + ' says hello.'; // review template strings.

    }
}
console.log(new Person('Darryl').greet());

///////////////////////////////////////////////////////////////////////////////
console.log('->04Arrows');

class TaskCollection {
    constructor(tasks = []) {
        this.tasks = tasks;
    }

    log() {

        //this.tasks.forEach(function(task) {
        //    console.log(task);
        //});
        // calling a function here and can omitt the parathesis
        // notice how the return keyword is omitted, no need to return task.toGulp() for example
        this.tasks.forEach(task => console.log(task));
        this.tasks.forEach(task => task.toGulp());
    }
}

class Task {

    toGulp() {
        console.log('converting to gulp');
    }
}

new TaskCollection([new Task, new Task, new Task]).log();

// old pre ES2015 syntax
let names = ['Taylor', 'Jeffrey', 'Adam', 'Matt']
names = names.map(function(name) {
    return name + ' is cool';
});
console.log(names);

// This syntax is the same as above in ES2015
let names2 = ['Taylor', 'Jeffrey', 'Adam', 'Matt']
names2 = names2.map(name2 => name2 + ' is cool');
console.log(names2);

///////////////////////////////////////////////////////////////////////////////
console.log('->05DefaultParameters')

// Before ES2015
//function applyDiscount(cost, discount) {
//    discount = discount || .10;
//   return cost - (cost * discount);
//}


// ES2015
function getDefault() {
    return .10;
}

function applyDiscount(cost, discount = getDefault()) {
    // or do it this way:  function applyDiscount(cost, discount = .10) {
    discount = discount || .10;
    return cost - (cost * discount);
}
console.log(applyDiscount(100, .40)); // 40%
console.log(applyDiscount(100)); // use default of 10%

///////////////////////////////////////////////////////////////////////////////
console.log('->06RestSpread');

// Used for a variable number of arguments
// Hive me the 'rest' of the numbers, used for variable arguments
// Note that you can only have one rest operator and MUST be at the end of argument list

function sum(...numbers) {

    // numbers becomes an array of [1,2,3]
    // now reduce the array down to a single value
    return numbers.reduce(function(prev, current) {
        return prev + current;
    });
}

console.log(sum(1, 2, 3)); // 6

// Now lets do the spread operator, converts array into single arguments
function sum2(x, y) {
    return x + y;
}

let nums = [1, 2];

console.log(sum2(...nums));


///////////////////////////////////////////////////////////////////////////////
console.log('->07TemplateStrings');

//in the past
let template = [
    '<div class="Alert">',
    '<p>Foo</p>',
    '</div>'
].join('');
console.log(template);

// Template strings use the back tick, making templates much cleaner, can use all 
// string functions, like trim, etc. as it is a string.
let template2 = `<div class="Alert">,
    <p>Foo</p>,
    </div>
`;
console.log(template2);

//dynamic content
let name = 'Foo2';
let template3 = `<div class="Alert">,
    <p>${name}</p>,
    </div>
`;
console.log(template3);

///////////////////////////////////////////////////////////////////////////////
console.log('->08AwesomeObjectEnhancements');

//in the past with ES5
function getPerson() {
    let name = 'John';
    let age = 25;

    return {
        name: name,
        age: age,

        greet: function() {
            return 'Hello, ' + this.name;
        }

    };
}

//Now, if property name is same as value, can remove, ES6
function getPerson2() {
    let name = 'John';
    let age = 25;

    return {
        name,
        age,
        greet() {
            return `Hello, ${this.name}`;
        }
    };
}

console.log(getPerson().name);
console.log(getPerson().greet());
console.log(getPerson2().name);
console.log(getPerson().greet());

// In vue can get rid of value of where a view lives, e.g.
//import HomeView from './components/home-view.vue'
//new Vue({
//    components: {
//        HomeView
//    }
//})

// Now object destructing example in ES6, useful for AJAX, less code to write to
// pull out specific items in a returned block of data
let person = {
    name2: 'Karen',
    age2: 32
};

let { name2, age2 } = person;

console.log(name2);
console.log(age2);


//ES5
function getData(data) {
    var results = data.results;
    var count = data.count;
    console.log(results, count);
}

getData({
    results: ['foo', 'bar'],
    count: 30
});

//ES6, note the {}s which dereference the data
function getData2({ results, count }) {
    console.log(results, count);
}

getData2({
    results: ['foo', 'bar'],
    count: 30
});

//ES 5
function greet(person) {
    let name = person.name;
    let age = person.age;

    console.log('Hello, ' + name + '. You are ' + age);
}
greet({
    name: 'Luke',
    age: 24
});

//ES 6 with object destructoring
function greet2({ name, age }) {
    console.log('Hello, ' + name + '. You are ' + age);
}
greet2({
    name: 'Luke',
    age: 24
});

///////////////////////////////////////////////////////////////////////////////
// Note that you can pass classes as arguments
console.log('->09Classes');

//ES5
function User(username, email) {
    this.username = username;
    this.email = email;
}

User.prototype.changeEmail = function(newEmail) {
    this.email = newEmail;
}

var user = new User('Darryl', 'darryl@gmail.com');
user.changeEmail('dpb@user.com');

console.dir(user);

//ES6
class User2 {
    constructor(username, email) {
        this.username = username;
        this.email = email;
    }
    changeEmail(newEmail) {
        this.email = newEmail;
    }
    // note static methods work too!  Notice use of spread and rest
    // statics can be used without having to create an instance of the class
    static register(...args) {
        return new User(...args);
    }
    get foo() {
        return 'foo';
    }

}
var user2 = new User2('Darryl', 'darryl@gmail.com');
user2.changeEmail('dpb@user.com');
console.dir(user2);

///////////////////////////////////////////////////////////////////////////////
console.log('->14UsefulStringAdditions');

// ES5
let title = 'Red Rising';
if (title.indexOf('R') == 0) {
    console.log('R is the first char');
}
if (title.indexOf('Blue') == -1) {
    console.log('This book does not begin with Blue');
}
//ES6
if (title.includes('Red')) {
    console.log('This book includes Red');
}
if (title.startsWith('Red')) {
    console.log('This book starts with Red');
}
if (title.endsWith('ing')) {
    console.log('This book ends with Red');
}
if (title.startsWith('i', 5)) {
    console.log('Strip off 5, then i');
}
let str = 'lol';
console.log(str.repeat(100));
let heading = 'The heading is here';
console.log(
    '='.repeat(5) + heading + '='.repeat(5)
); // using template strings
console.log(
    `${'>='.repeat(5)} ${heading} ${'=<'.repeat(5)}`
);

///////////////////////////////////////////////////////////////////////////////
console.log('->15ArrayFindInclude');

// ES5
console.log(
    [2, 4, 6, 8].indexOf(8) > -1
);
// ES+ ES2017?
console.log(
    [2, 4, 6, 8].includes(8)
);
// ES5
console.log(
    [2, 4, 6, 8, 10, 11].find(function(item) {
        // first odd
        return item % 2 > 0;
    })
);
//ES+ ES2017?
console.log(
    [2, 4, 6, 8, 10, 11].find(item => item % 2)
);
// now get the index instead
console.log(
    [2, 4, 6, 8, 10, 11].findIndex(item => item % 2)
);
class User44 {
    constructor(name, isAdmin) {
        this.name = name;
        this.isAdmin = isAdmin;
    }
}
let users = [
    new User44('Jeffrey', false),
    new User44('Jane', true),
    new User44('Jack', true)
];

// Find first user by name who is an Admin
console.log(
    users.find(user => user.isAdmin).name
);
// Construct an associative array
let items = ['jeff', 'jordan', 'way'].entries();
for (let name of items) console.log(name);


///////////////////////////////////////////////////////////////////////////////
console.log('->17Sets');

// must be unique, second two ignored, set strips out duplicates
let items66 = new Set(['one', 'two', 'three', 'two']);
items66.add('seven');
console.log(items66);

// in Chrome debugger
// items66.size
// 3
// items66.delete("one")
// true
// items66
// Set(2) {"two", "three"}