var today = new Date();               //Get the date
var hourNow = today.getHours();       //Get the current hour
var greeting;

if (hourNow > 18) {
    greeting = 'Good evening!';       
} else if (hourNow > 12) {
    greeting = 'Good afternoon!';
} else if (hourNow > 0) {
    greeting = 'Good morning!';
} else {
    greeting = 'Welcome!';
}

document.write('<h3>Hello World!</h3>');        //Write hello world to document
document.write('<h3>' + greeting + '</h3>');    //Write greeting to document
