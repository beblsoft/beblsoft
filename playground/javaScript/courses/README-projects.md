# JavaScript Courses Projects Documentation

## jquery-codeacademy

Codeacademy jquery course.
jQuery is a cross-platform JavaScript library designed to simplify client-side scripting.

To run each project: open `jquery-codeacademy/<project>/index.html` in Chrome.

Overview of projects:
- `armandoPerez/`: highlighting objects, e.g. jpg, based on selection.
- `birdman/`: bring down a pull down menu.
- `bonsai/`: updating text based on input.
- `feedster/`: responding to button clicks.
- `forecast/`: adding/removing content based on button click. Shown in drilldown form.
- `gameBoard/`: opening choices based on button click.
- `listEasy/`: adding/removing items from a list.
- `madisonSquareMarket/`: pulldown menus.
- `milfordSchool/`: sign in checking.
- `move/`: jquery autocompletion.
- `moveGear/`: accordian, right login toggle.
- `pocketbook/`: error checking a bunch of form fields.
- `speakEasy/`: see listEasy (approximately same), add voice commands.
- `threadly/`: add to a list.

## jsjq-wiley

JSJQ-Wiley Javascript Book Examples.

To run: open `jsjq-wiley/index.html` in Chrome.
