var main = function() {
  
  $('#top-text').keyup(function() {
    var text = $(this).val(); 
    $('.top-caption').text(text); 
  });
  
  $('#bottom-text').keyup(function() {
    var text = $(this).val();
    $('.bottom-caption').text(text);
  });
  
  $('#image-url').keyup(function() {
    var text = $(this).val();
    $(".meme-image").attr("src", text);
  });                     
}
 
$(document).ready(main);