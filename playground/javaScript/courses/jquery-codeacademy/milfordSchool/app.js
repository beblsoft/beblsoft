var main = function() {
    $('.dropdown img').click(function() {
        $('.dropdown-menu').toggle();
    });

    $('form').submit(function() {
        var emailText = $('#email').val();
        var passwordText = $('#password').val();
        if (emailText === "") {
            $('.email-error').text('Please enter your email');
        } else {
            $('.email-error').text('');        	
        }
        if (passwordText === "") {
            $('.password-error').text('Please enter a password');
        }else {
            $('.password-error').text('');        	
        }
        return false;
    });
};

$(document).ready(main);
