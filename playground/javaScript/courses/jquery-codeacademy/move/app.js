var main = function() {
    var cities = ['Boston', 'San Francisco', 'Attleboro', 'Nashua', 'Providence'];
    $('#search').autocomplete({
        source: cities
    });
}

$(document).ready(main);
