var main = function() {
    $('.day').click(function() {

        /* Show the next sibling, i.e. the hourly row */
        $(this).next().toggle();

        /* Toggle the plus/minus sign */
        $(this).find('span.glyphicon').toggleClass('glyphicon-minus').toggleClass('glyphicon-plus')

        /* Note on finding elements using jQuery
          $('.outer.inner')    - all elements with class inner and outer
          $('.outer .inner')   - all elements with class inner, who have ancestor outer
          $('.outer > .inner') - all elements with class inner, who have direct ancestor outer 
        */
    });
};

$(document).ready(main);
