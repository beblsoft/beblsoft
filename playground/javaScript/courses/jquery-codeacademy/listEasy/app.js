var template = function(text) {
    return '<p><input type="checkbox"><i class="glyphicon glyphicon-star"></i><span>' + text + '</span><i class="glyphicon glyphicon-remove"></i></p>';
};

var main = function() {

	/* Form submission */
    $('form').submit(function() {
        var text = $('#todo').val();
        if (text !== "") {
            var html = template(text);
            $('.list').append(html)
            $('#todo').val("");
        };
        return false;
    });

    /* Stars */
    $(document).on('click', '.glyphicon-star', function() {
    	$(this).toggleClass('active');
    });

    /* Remove */
    $(document).on('click', '.glyphicon-remove', function() {
    	$(this).parent().remove();
    });
};

$(document).ready(main);
