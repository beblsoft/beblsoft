# JavaScript Core Projects Documentation

## workout

Demonstrate usage of JavaScript enumerations, classes, and program execution.

To Run: open `/workout/index.html` in chrome. View log messages on Chrome console.