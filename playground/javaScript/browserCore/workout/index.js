/*
  NAME:
    index.js

  DESCRIPTION:
    Index js file for workout site
*/


/* -------------------------- CONSTANTS --------------------------------------*/
const dblack = "boss";
const jamester = "Big boss";


/* -------------------------- GLOBALS ----------------------------------------*/
var exerciseArrayG = [];
var workoutArrayG = [];


/* -------------------------- EMUMERATIONS -----------------------------------*/
MuscleType = {
	CARDIO:     0,
	ABS:        1,
	BISCEPS:    2,
	TRICEPS:    3,
	SHOULDERS:  4,
	CHEST:      5,
	BACK:       6,
	CALFS:      7,
	QUADS:      8,
	HAMSTRINGS: 9
};

Duration = {
	SHORT:  30,
	MEDIUM: 45,
	LONG:   60
};


/* -------------------------- CLASSES ----------------------------------------*/
/* ------------------------------------------------- */
/*
  CLASS: Exercise

  DESCRIPTION:
    Contains all information pertinent to an exercise
*/
function Exercise(name, muscleType, duration) {
	this.name = name;
	this.muscleType = muscleType;
	this.duration = duration;
	this.videoURL = "";

	this.getVideoURL = function() {
		return this.videoURL;
	};

	this.setVideoURL = function(videoURL) {
		this.videoURL = videoURL
	};

	this.toString = function() {
		var str = "";
		str += "Name=\"" + this.name + "\" ";
		str += "MT=" + this.muscleType + " ";
		str += "Duration=" + this.duration;
		return str;
	};

};

/* ------------------------------------------------- */
/*
  CLASS: Workout

  DESCRIPTION:
    Contains all information pertinent to a workout
*/
function Workout(name, restTime, exerciseArray) {
	this.name = name;
	this.restTime = restTime;
	this.exerciseArray = exerciseArray;

	this.toString = function() {
		var str = "";
		str += "Name=\"" + this.name + "\" ";
		str += "Rest Time=" + this.restTime + "\n";
		str += "Exercises: \n";
		for (var i = 0; i < this.exerciseArray.length; i++) {
			str += "   " + this.exerciseArray[i].toString() + "\n";
		}
		return str;
	};
};


/* -------------------------- INITIALIZATION ---------------------------------*/
function initializeExerciseArray() {
	/* Abs */
	exerciseArrayG.push(new Exercise("Crunch", MuscleType.ABS, Duration.MEDIUM));
	exerciseArrayG.push(new Exercise("Situps", MuscleType.ABS, Duration.MEDIUM));

	/* Chest*/
	exerciseArrayG.push(new Exercise("Pushups", MuscleType.CHEST, Duration.MEDIUM));

	/* Hamstrings */
	exerciseArrayG.push(new Exercise("Pseudo Squats", MuscleType.HAMSTRINGS, Duration.LONG));

	/* Cardio */
	exerciseArrayG.push(new Exercise("One Leg Hop", MuscleType.CARDIO, Duration.LONG));
	exerciseArrayG.push(new Exercise("Run In Place", MuscleType.CARDIO, Duration.LONG));
	exerciseArrayG.push(new Exercise("Butt Kicks", MuscleType.CARDIO, Duration.LONG));

	/* Back */
	exerciseArrayG.push(new Exercise("Pull ups", MuscleType.BACK, Duration.SHORT));
};


function initializeWorkoutArray() {
	workoutArrayG.push(new Workout("All exercises", Duration.SHORT, exerciseArrayG));
};



/* -------------------------- MAIN -------------------------------------------*/
function main() {
	initializeExerciseArray();
	initializeWorkoutArray();

	for (var i = 0; i < workoutArrayG.length; i++) {
		console.group("Workout " + i);
		console.log(workoutArrayG[i].toString());
		console.groupEnd();
    }

};

main();




















