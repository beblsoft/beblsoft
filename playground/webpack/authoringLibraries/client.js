/*
 How a client would use webpackNumbers
*/

// ES2015 module import
import * as webpackNumbers from 'webpack-numbers';
// CommonJS module require
var webpackNumbers = require('webpack-numbers');
// ...
// ES2015 and CommonJS module use
webpackNumbers.wordToNum('Two');
// ...
// AMD module require
require(['webpackNumbers'], function(webpackNumbers) {
    // ...
    // AMD module use
    webpackNumbers.wordToNum('Two');
    // ...
});