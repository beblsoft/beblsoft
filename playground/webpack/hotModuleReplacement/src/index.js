import _ from 'lodash';
import printMe from './print.js';
import './styles.css';

function component() {
    /* Use lodash */
    var element = document.createElement('div');
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello')

    /* Import javascript */
    var btn = document.createElement('button');
    btn.innerHTML = 'Click ms and check the consoole!';
    btn.onclick = printMe;
    element.appendChild(btn)

    return element;
}

let element = component()
document.body.appendChild(element);


/* Tie in with hot module replacement */
if (module.hot) {
    module.hot.accept('./print.js', function() {
        console.log('Accepting the updated printMe module!');
        printMe();

        /* To update the button on the screen, we must rerender the button
           as the old rendering is still bound to the old printMe function */
        document.body.removeChild(element);
        element = component();
        document.body.appendChild(element);
    })

}