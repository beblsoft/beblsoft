import _ from 'lodash';
import './style.css';
import Icon from './fun.jpg';
import Data from './data.xml';

function component() {
	/* Use lodash */
    var element = document.createElement('div');
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello')

	/* Add icon */
    var myIcon = new Image();
    myIcon.src = Icon;
    element.appendChild(myIcon)

    /* Log data to console */
    console.log(Data)

    return element;
}

document.body.appendChild(component());