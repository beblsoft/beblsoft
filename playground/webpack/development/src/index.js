import _ from 'lodash';
import printMe from './print.js';

function component() {
	/* Use lodash */
    var element = document.createElement('div');
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello')

    /* Import javascript */
    var btn = document.createElement('button');
    btn.innerHTML = 'Click ms and check the consoole!';
    btn.onclick = printMe;
    element.appendChild(btn)

    return element;
}

document.body.appendChild(component());