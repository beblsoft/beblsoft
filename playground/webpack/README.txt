OVERVIEW
===============================================================================
Webpack Technology Documentation
Description
  - Webpack is a static module bundler for modern JavaScript applications.
    While processing an application, webpack recursively builds a dependency
    graph that includes every module the application needs, then packages all
    of those modules into one or more bundles.
URLS
  - Home                 : https://webpack.js.org/
  - Github               : https://github.com/webpack
  - Github examples      : https://github.com/webpack/webpack/tree/master/examples


INSTALLATION
===============================================================================
- Install latest version of npm  : See nodejs.sh
- Install webpack locally for
  project [best practice]        : cd <projectDir>
                                   npm install --save-dev webpack
                                   npm install --save-dev webpack@<version>
- Global installation
  [not best practice]            : npm install --global webpack
- Install bleeding edge          : npm install webpack@beta
- Usually invoked via npm scripts
  in package.json                : Example package.json scripts:
                                   "scripts": {
                                    "dev": "cross-env NODE_ENV=development webpack-dev-server --open --hot --watch-poll",
                                    "version": "webpack-dev-server -v",
                                    "build": "cross-env NODE_ENV=production webpack --progress --hide-modules"
                                   },
- Begin a project with webpack   : mkdir <projectDir> && cd <projectDir>
                                   npm init -y
                                   npm install --save-dev webpack webpack-cli
- Configuration                  : Modify project's webpack.config.js


TERMS
===============================================================================
- Entry point            : Indicates which module webpack should use to being building
                           its internal dependency graph
- Output                 : Tells webpack where to emit and how to name bundles
                           Defaults to ./dist
- Loaders                : Loaders enable webpack to process more than just JS files
                           Convert any type of file into valid modules that webpack
                           can process
                           test: which file or files should be transformed
                           use:  indicates which loader should be used
- Plugins                : Perform a wider range of task than loaders.
                           Ex tasks: bundle optimization, minification, env vars
- Mode                   : Set to development or production to enable Webpack's
                           build-in optimizations
- Module                 : A component of a program
- Source Maps            : Map compiled code back to the original source code
- Hot Module Replacement : Allows many modules to be updated at runtime without the
                           need for a full refresh. Modules must implement HMR
                           interfaces.
- Tree Shaking           : Dead code elimination
- Code Splitting         : Split code into various bundles
- Lazy Loading           : Split code into logical chunks, and then load
                           the chunk only when user does something requiring the
                           chunk
- Polyfill               : Code that implements a feature on web browsers that
                           do not support the feature
- Shimming               : Provide global dependencies (ex. $ for jQuery) for
                           libraries which expect these globals. Can also
                           deliver polyfills
- Progressiv Web App(PWA): App that deliver a similar experience to native
                           applications
- Targets                : End user of output. I.e. "web" (default) or "node"
- Resolver               : Library which helps locate a module by
                           absolute, relative, or module path


MODULES
===============================================================================
- Description:
  * Component of a program
  * Smaller surface area than a full program, making verification, debugging,
    and testing trivial.
  * Solid abstraction and encapsulation boundary. Each module has a coherent
    design and a clear purpose within the overall application.
- Webpack modules express dependencies in a variety of ways
  * ES2015 import
  * CommonJS require()
  * AMD define and require
  * @import statement inside css/sass/less
  * image urale in a style sheet url(...)


LOADERS
===============================================================================
- Description
  * Describe how to process non-JavaScript files and include them into
    webpack bundles
- Exist for: CoffeeScript, TypeScript, ESNext, Sass, Less, Stylus, ...


HOT MODULE REPLACEMENT (HMR)
===============================================================================
- Description
  * Exchanges, adds, or removes modules while an application is running,
    without a full reload.
  * Not intended for production use!
- How it works
  1 The application asks the HMR runtime to check for updates.
  2 The runtime asynchronously downloads the updates and notifies the application.
  3 The application then asks the runtime to apply the updates.
  4 The runtime synchronously applies the updates.
- Other HMR in community
  * React Hot Loader    : Tweak react components in real time.
  * Vue Loader          : This loader supports HMR for vue components out of the box.
  * Elm Hot Loader      : Supports HMR for the Elm programming language.
  * Redux HMR           : A simple change to your main store file is all that's required.
  * Angular HMR         : A simple change to your main NgModule file is all that's required
                          to have full control over the HMR APIs.

TREE SHAKING
===============================================================================
- Description
  * Dead code elimination
- Allow all unused
  code to be eliminated : package.json
                          "sideEffects" : false
- Side Effect           : Code that performs a special behavior when imported
                          Other than exposing one or more exports. Ex. polyfills
- Set side effects      : package.json
                          "sideEffects" : {
                              "./src/some-side-effectful-file.js",
                              "*.css"
                          }


CODE SPLITTING
===============================================================================
- Description
  * Split code into various bundles which can then be loaded on demand or in parallel
- Three approaches
  1 Entry Points: manually split using entry configuration [Easiest, most intuitive]
  2 Prevent Duplication: Use the CommonsChunkPlugin to dedupe and split chunks
  3 Dynamic Imports: split code via inline funciton calls
- Community Plugins and Loaders for splitting code:
  ExtractTextPlugin, bundle-loader, promise-loader

