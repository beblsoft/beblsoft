OVERVIEW
===============================================================================
Webpack Sample Projects Documentation


PROJECT: GETTING STARTED
===============================================================================
- Description:
  * Basic webpack bundling of lodash
  * Basic webpack configuration file webpack.config.js
- Directory                                 : ./gettingStarted
- URL                                       : https://webpack.js.org/guides/getting-started/
- To Run
  * Install dependencies                    : npm install
  * Create dist/bundle.js                   : npx webpack --webpack.config.js
                                              - OR -
                                              npm run build
  * View in browser                         : Point chrome at: dist/index.html


PROJECT: ASSET MANAGEMENT
===============================================================================
- Description:
  * Asset managment demonstration
  * Load a css stylesheet using style-loader css-loader
  * Load an image         using file-loader
  * Load fonts            using file-loader
  * Load JSON             using built in loader [Not done in this project]
  * Load CSV              using csv-loader      [Not done in this project]
  * Load XML              using xml-loader
- Directory                                 : ./assetManagement
- URL                                       : https://webpack.js.org/guides/asset-management/
- To Run
  * Install dependencies                    : npm install
  * Create dist/bundle.js                   : npm run build
  * View in browser                         : Point chrome at: dist/index.html
                                              XML data printed to console

PROJECT: OUTPUT MANAGEMENT
===============================================================================
- Description:
  * Output managment demonstration
  * Dynamically change output name based on entry point name
  * html-webpack-plugin to autogenerate dist/index.html
  * clean-webpack-plugin to clean dist
- Directory                                 : ./outputManagement
- URL                                       : https://webpack.js.org/guides/output-management/
- To Run
  * Install dependencies                    : npm install
  * Create dist/*                           : npm run build
  * View in browser                         : Point chrome at: dist/index.html


PROJECT: DEVELOPMENT
===============================================================================
- Description:
  * Set up development environment
  * Enabled source maps
  * Use watch mode to build sources when file in dependency map changes
  * Use webpack-dev-server to serve, update on change. localhost:8080
  * Use express and webpack-dev-middleware to serve, update on change. localhost:3000
- Directory                                 : ./development
- URL                                       : https://webpack.js.org/guides/development/
- To Run
  * Install dependencies                    : npm install
  * Create dist/*                           : npm run build
  * View source maps                        : print.js. Change "console" to "cosnole"
                                              Rebuild, see error in chrome, examine source maps
  * Watch for changed code and rebuild      : npm run watch
  * Launch webpack development server       : npm start
                                              Open browser to localhost:8080
  * Launch auto-refreshing express erver    : npm run server
                                              Open browser to localhost:3000


PROJECT: HOT MODULE REPLACEMENT
===============================================================================
- Description:
  * Demonstrate how modules can tie into hot module replacement API
  * Responding to JS changes
  * Responding to style changes using style-loader css-loader
- Directory                                 : ./hotModuleReplacement
- URL                                       : https://webpack.js.org/guides/hot-module-replacement/
- To Run
  * Install dependencies                    : npm install
  * Start dev server                        : npm start
                                              Chrome opens to localhost:8080
  * Open chrome console
  * Change print.js: console.log            : Chrome console should show button update
  * Change styles.css: background color     : Chrome background should change


PROJECT: TREE SHAKING
===============================================================================
- Description:
  * Eliminate dead code by package.json "sideEffects": false and
    set webpack.config.js mode to "production"
  * Production mode enables UglifyJsPlugin
- Directory                                 : ./treeShaking
- URL                                       : https://webpack.js.org/guides/tree-shaking/
- To Run
  * Install dependencies                    : npm install
  * Run build                               : npm run build
  * Examine ./dist/app.bundle.js            : Contains square() function
  * Change webpack.config.js mode
    to "production"
  * Run build                               : npm run build
  * Examine ./dist/app.bundle.js            : No square() function, is minified


PROJECT: PRODUCTION
===============================================================================
- Description:
  * Different goals for production/development
    Development : source mapping, live reload, hot module replacement
    Production  : minified bundles, lightweight source maps, optimized assets
  * Implement separate configurations, putting commonalities in a single file
    Use webpack-merge to merge configuations
  * Use process.env.NODE_ENV variable
- Directory                                 : ./production
- URL                                       : https://webpack.js.org/guides/production/
- To Run
  * Install dependencies                    : npm install
  * Run development server                  : npm start.
                                              Chrome opens to localhost:8080
  * Run production build                    : npm run build


PROJECT: CODE SPLITTING
===============================================================================
- Description:
  * Split code into multiple chunks
  * Remove duplication using CommonsChunkPlugin
- Directory                                 : ./codeSplitting
- URL                                       : https://webpack.js.org/guides/code-splitting/
- To Run
  * Install dependencies                    : npm install
  * Run build, building multiple chunks
    dist/*.bundle.js                        : npm run build


PROJECT: AUTHORING LIBRARIES
===============================================================================
- Description:
  * Use webpack to bundle JavaScript applications
  * Bundle src/index.js into dist/webpack-numbers.js library
  * client.[html,js] show how the library can be called
  * Use externals keyword to depend on lodash
- Directory                                 : ./authoringLibraries
- URL                                       : https://webpack.js.org/guides/author-libraries/
- To Run
  * Install dependencies                    : npm install
  * Run build                               : npm run build


PROJECT: TYPESCRIPT
===============================================================================
- Description:
  * TypeScript is a typed superset of JavaScript that compiles to plain JavaScript
  * Setup tsconfig.json to transpile Typescript to ES5
  * Use ts-loader to load *.ts files
  * Enable source maps in tsconfig.json and webpack.config.js
  * Import arbitrary svg assets with custom.d.ts file
- Directory                                 : ./typescript
- URL                                       : https://webpack.js.org/guides/typescript/
- To Run
  * Install dependencies                    : npm install
  * Run build                               : npm run build
  * Run development server                  : npm start.
                                              Chrome opens to localhost:8080
                                              See "Hello World" on console


PROJECT: ENVIRONMENT VARIABLES
===============================================================================
- Description:
  * Distinguish build setting using command line environment variables
  * See package.json scripts for setting of NODE_ENV and production
    These values are then logged in webpack.config.js
- Directory                                 : ./environmentVariables
- URL                                       : https://webpack.js.org/guides/environment-variables/
- To Run
  * Install dependencies                    : npm install
  * Run build                               : npm run build
  * Run development server                  : npm start
                                              Chrome opens to localhost:8080


PROJECT: LARACAST
===============================================================================
- Description:
  * Laracast webpack demo
- Directory                                 : ./laracast
- To Run
  * Install dependencies                    : npm install
  * Run build                               : npm run build
  * Can't get dev server working