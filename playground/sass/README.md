# SASS Technology Documentation

Sass (Syntactically awesome style sheets) is a style sheet language. It is a preprocessor scripting
language that is interpreted or compiled into Cascading Style Sheets (CSS).

The original syntax, called "the indented syntax", uses indendation to separate code blocks and
newline characters to separate rules. The indented syntax files have ".sass" extensions.

The newer syntax, "SCSS" (Sassy CSS), uses block formatting like that of CSS. It uses braces to denote
code blocks and semicolons to separate lines within a block. SCSS files have ".scss" extensions.

Facts:
- __CSS Compatible__: Sass is completely compatible with all versions of CSS
- __Feature Rich__: Sass boasts more features and abilities than any other CSS extension language
- __Mature__: Sass has been actively supported for over 12 years by its core team
- __Industry Approved__: The industry is choosing Sass as the premier CSS extension language
- __Large Community__: Sass is actively supported and developed by a consortium of several tech
  companies and hundreds of developers
- __Frameworks__: There are an endless number of frameworks built with Sass. `Compass`, `Bourbon`, and
  `Susy` just to name a few.

Relevant URLs:
[Home](https://sass-lang.com/),
[Wikipedia](https://en.wikipedia.org/wiki/Sass_(stylesheet_language)),
[Documentation](https://sass-lang.com/documentation),
[Online Compiler](https://www.sassmeister.com/)

Implementations:
[Dart Sass](https://sass-lang.com/dart-sass),
[LibSass](https://sass-lang.com/libsass)

## Installation

```bash
# Install via NPM
# Installs the pure JavaScript implementation of Sass, which runs somewhat slower than other versions
npm install -g sass

# See help
sass --help
```

## Basics

### Preprocessing

CSS on its own can be fun, but stylesheets are getting larger, more complex, and harder to maintain.
This is where a preprocessor can help. Sass lets you use features that don't exist in CSS yet like
variables, nesting, mixins, inheritance and other nifty goodies that make CSS fun again.

Once you start tinkering with Sass, it will take your preprocessed Sass file and save it as a normal
CSS file that you can use in your website.

Use the Sass binary as follows:

```bash
# Watch for index.scss for changes. When a change occurs rebuild index.css
sass --watch source/stylesheets/index.scss build/stylesheets/index.css

# Watch all files in app/sass, and compile to public/stylesheets
sass --watch app/sass:public/stylesheets
```

### Variables

Variables are a way to store information that you want to reuse throughout your stylesheet. You can
store things like colors, font stacks, or any CSS value you think you'll want to reuse. Sass uses
the `$` symbol to make something a variable.

SCSS:
```scss
$font-stack:    Helvetica, sans-serif;
$primary-color: #333;

body {
  font: 100% $font-stack;
  color: $primary-color;
}
```

Compiles to CSS:
```css
body {
  font: 100% Helvetica, sans-serif;
  color: #333;
}
```

### Nesting

When writing HTML you've probably noticed that it has a clear nested and visual hierarchy. CSS on
the other hand, doesn't.

Sass will let you nest your CSS selectors in a way that follows the same visual hierarchy of your HTML.
Be aware that overly nested rules will result in over-qualified CSS that you prove hard to maintain and
is generally considered bad practice.

SCSS:
```scss
nav {
  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  li { display: inline-block; }

  a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
}
```

Compiles to CSS:
```css
nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
nav li {
  display: inline-block;
}
nav a {
  display: block;
  padding: 6px 12px;
  text-decoration: none;
}
```

### Partials

You can create partial Sass files that contain little snippets of CSS that you can include in other
Sass files. This is a great way to modularize your CSS and help keep things easier to maintain.

A partial is simply a Sass file named with a leading underscore, for example `_partial.scss`.
The underscore lets Sass know that the file is only a partial file and that it should not be generated
into a CSS file. Sass partials anre used with the `@import` directive.

### Import

CSS has an import option that lets you split your CSS into smaller, more maintainable portions. The
only drawback is that each time you use `@import` it creates another HTTP request. Sass builds on top
of the current CSS `@import` but instead of requiring an HTTP request, Sass will take the file that you
want to import and combine it with the file you're importing into so you can serve a singel CSSS file
to the web browser.

SCSS `_reset.scss`:
```scss
html,
body,
ul,
ol {
  margin:  0;
  padding: 0;
}
```

SCSS `base.scss`:
```scss
@import 'reset'; /* Note: don't need to use .scss extension */
body {
  font: 100% Helvetica, sans-serif;
  background-color: #efefef;
}
```

Generated CSS:
```css
html,
body,
ul,
ol {
  margin:  0;
  padding: 0;
}
body {
  font: 100% Helvetica, sans-serif;
  background-color: #efefef;
}
```

### Mixins

Some things in CSS are a bit tedious to write, especially with CSS3 and the many vendor prefixes that exist.
A mixin lets you make a group of CSS declarations that you want to reuse throughout your site. You can even
pass in values to make sure your mixin more flexible.

To create a mixing use the `@mixin` directive. After a mixin is created, it can be used with the `@include`
directive.

SCSS:
```scss
@mixin transform($property) {
  -webkit-transform: $property;
  -ms-transform: $property;
  transform: $property;
}
.box { @include transform(rotate(30deg)); }
```

Generated CSS:
```css
.box {
  -webkit-transform: rotate(30deg);
  -ms-transform: rotate(30deg);
  transform: rotate(30deg);
}
```

### Extend/Inheritance

This is one of the most useful features of Sass. Using `@extend` lets you share a set of CSS properties
from one selector to another. It helps keep your Sass very DRY.

SCSS:
```scss
/* This CSS will print because %message-shared is extended. */
%message-shared {
  border: 1px solid #ccc;
  padding: 10px;
  color: #333;
}

/* This CSS won't print because %equal-heights is never extended. */
%equal-heights {
  display: flex;
  flex-wrap: wrap;
}

.message {
  @extend %message-shared;
}

.success {
  @extend %message-shared;
  border-color: green;
}

.error {
  @extend %message-shared;
  border-color: red;
}

.warning {
  @extend %message-shared;
  border-color: yellow;
}
```

Generatd CSS:
```css
/* This CSS will print because %message-shared is extended. */
.message, .success, .error, .warning {
  border: 1px solid #ccc;
  padding: 10px;
  color: #333;
}

.success {
  border-color: green;
}

.error {
  border-color: red;
}

.warning {
  border-color: yellow;
}
```

### Operators

Doing math in your CSS is very helpful. Sass has a handful of standard math operators like `+`, `-`,
`*`, `/`, and `%`.

SCSS:
```scss
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 600px / 960px * 100%;
}

aside[role="complementary"] {
  float: right;
  width: 300px / 960px * 100%;
}
```

Generated CSS:
```css
.container {
  width: 100%;
}

article[role="main"] {
  float: left;
  width: 62.5%;
}

aside[role="complementary"] {
  float: right;
  width: 31.25%;
}
```

## Syntax

Sass supports two different syntaxes. Each one can import the other, so it's up to you and your team
which one to choose.

### SCSS

The SCSS syntax uses the file extension `.scss`. With a few small exceptions, it's a superset of CSS,
which means essentially all valid css is valid scss as well. Because of its similarity to CSS, it's
the easiest syntax to get used to and the most popular.

SCSS looks like this:
```scss
@mixin button-base() {
  @include typography(button);
  @include ripple-surface;
  @include ripple-radius-bounded;

  display: inline-flex;
  position: relative;
  height: $button-height;
  border: none;
  vertical-align: middle;

  &:hover { cursor: pointer; }

  &:disabled {
    color: $mdc-button-disabled-ink-color;
    cursor: default;
    pointer-events: none;
  }
}
```

### The Indented Syntax

The indented syntax was Sass's original syntax, and so it uses the file extension `.sass`. Because
of this extension, it's sometimes just called "Sass". The indented syntax supports all the same
features as SCSS, but it uses indentation instead of curly braces and semicolons to describe the
format of the document.

The indented syntax looks like this:
```sass
@mixin button-base()
  @include typography(button)
  @include ripple-surface
  @include ripple-radius-bounded

  display: inline-flex
  position: relative
  height: $button-height
  border: none
  vertical-align: middle

  &:hover
    cursor: pointer

  &:disabled
    color: $mdc-button-disabled-ink-color
    cursor: default
    pointer-events: none
```

### Input Encoding

It's often the case that a document is initially available only as a sequence of bytes, which must be
decoded into Unicode. Sass performs this decoding as follows:
- If the sequency of bytes begins with the UTF-8 or UTF-16 encoding the U_FEFE BYTE ORDER MARK, the
  corresponding encoding is used
- If the sequence of bytes begins with the plain ASCII string `@charset`, Sass determines the encoding
  using step 2 of the [CSS algorithm](https://drafts.csswg.org/css-syntax-3/#input-byte-stream)
- Otherwise, UTF-8 is used

### Parse Errors

When Sass encounters invalid syntax in a stylesheet, parsing will fail and an error will be presented to
the user with informatino about the location of the invalid syntax and the reason it was invalid.

Not that this is different than CSS, whcih specifies how to recover from most errors rather than failing
immediately. This is one of the few cases where SCSS isn't _strictly_ a superset of CSS. However, it's
much more useful to Sass users to see error immediately, rather than having them passed through to CSS
output.

### Statements

A Sass stylesheet is made up of a series of _statements_, whcih are evaluated in order to build the
resulting CSS. Some statements may have _blocks_, defined using brackets, which may contain other
statements.

In SCSS, statements are separated by semicolons.

Statement types:

- __Universal Statements__: These types of statements can be used anywhere in a Sass stylesheet
    * Veriable declarations. Ex. `$var: value`
    * Flow control at-rules. Ex. `@if`, `@each`
    * The `@error`, `@warn`, `@debug` rules

- __CSS Statements__: These statements produce CSSS. They can be used anywhere except within a `@function`:
    * Style rules. Ex. `h1 { /* ... */ }`
    * CSS at-rules, like `@media`, `@font-face`
    * Mixin uses using `@include`
    * The `@at-root` rule

- __Top-Level Statements__: These statements can only be used at the top level of a stylesheet, or nested
  within a CSS statement at the top level.
    * Imports, using `@import`
    * Mixin definitions, using `@mixin`
    * Function definitions using `@function`

- __Other Statements__
    * Property declarations like `width: 100px` may only be used within style rules
    * `@extend` rule may only be used within style rules

### Expressions

An _expression_ is anything that goes on the right-hand side of a property or variable declaration.
Each expression produces a value. Any valid CSS property value is also a Sass expression, but Sass
expressions are much more powerful than plain CSS values.

Expression types:
- __Literals__: The simplest expressions just represent static values.
    * Numbers. Ex. `12` or `100px`
    * Strings. Ex. `Helvetica Neue`
    * Colors. Ex. `#c6538c`
    * Boolean. Ex. `true`
    * Singleton `null`
    * List of values. Ex. `1.5em 1em 2em, Helvetica, Arial, sans-serif`
    * Maps. Ex. ("background": red, "foreground": pint)

- __Operations__
    * `== and !=` are used to check if two values are the same
    * `+, -, *, /, and %` have the usual mathematical meaning for numbers
    * `<, <=, >, and >=` compare numbers
    * `and, or, and not` have usual boolean behavior. Sass considers every value `true` except for
      `false` and `null`
    * `(and)` can be used to explicity control the precedence order of operations

- __Other Expressions__
    * Variables. Ex. `$var`
    * Function calls. Ex. `nth($list, 1)` or `var(--main-bg-color)`
    * Special functions. Ex. `calc(1px + 100%)`
    * The parent selector `&`
    * The value `!important`, whcih is parsed as an unquoted string

### SCSS Comments

Comments in SCSS work similarly to comments in other languages like JavaScript. __Single-line
comments__ start with `//`, and go until the end of the line. Nothing in the single-line comment
is emitted as CSS. They're also called __silent comments__, because they don't produce CSS.

__Multi-line comments__ start with a `/*` and and at the next `*/`. They are also called __loud comments__
because they are compiled into CSS.

SCSS

```scss
// This comment won't be included in the CSS.

/* But this comment will, except in compressed mode. */

/* It can also contain interpolation:
 * 1 + 1 = #{1 + 1} */

/*! This comment will be included even in compressed mode. */

p /* Multi-line comments can be written anywhere
   * whitespace is allowed. */ .sans {
  font: Helvetica, // So can single-line commments.
        sans-serif;
}
```

Generated CSS:
```css
/* But this comment will, except in compressed mode. */
/* It can also contain interpolation:
 * 1 + 1 = 2 */
/*! This comment will be included even in compressed mode. */
p .sans {
  font: Helvetica, sans-serif;
}
```

### Indented Comments

Comments in the indented syntax are indendation-based, just like the rest of the syntax.

Sass
```sass
// This comment won't be included in the CSS.
   This is also commented out.

/* But this comment will, except in compressed mode.

/* It can also contain interpolation:
   1 + 1 = #{1 + 1}

/*! This comment will be included even in compressed mode.

p .sans
  font: Helvetica, /* Inline comments must be closed. */ sans-serif
```

Generated CSS:
```css
/* But this comment will, except in compressed mode. */
/* It can also contain interpolation:
 * 1 + 1 = 2 */
/*! This comment will be included even in compressed mode. */
p .sans {
  font: Helvetica, sans-serif;
}
```

### Documentation Comments

When writing style libraries using Sass, you can use comments to document mixins, functions, variables,
and placeholder selectors that your library provides, as well as the library itself.

```scss
/// Computes an exponent.
///
/// @param {number} $base
///   The number to multiply by itself.
/// @param {integer (unitless)} $exponent
///   The number of `$base`s to multiply together.
/// @return {number} `$base` to the power of `$exponent`.
@function pow($base, $exponent) {
  $result: 1;
  @for $_ from 1 through $exponent {
    $result: $result * $base;
  }
  @return $result;
}
```

## Style Rules

### Overview

Style rules are the foundation of Sass, just like they are for CSS. And they work the same way:
you choose which elements to style witha selector, and declare properties that affect how those
elements look.

SCSS
```scss
.button {
  padding: 3px 10px;
  font-size: 12px;
  border-radius: 3px;
  border: 1px solid #e1e4e8;
}
```

- __Nesting__:

  Rather than repeating the same selectors over and over again, you can write one style rules inside
  another. Sass will automatically combine the outer rule's selector with the inner rule's.

  SCSS:
  ```scss
  nav {
    ul {
      margin: 0;
      padding: 0;
      list-style: none;
    }

    li { display: inline-block; }

    a {
      display: block;
      padding: 6px 12px;
      text-decoration: none;
    }
  }
  ```

  Generated CSS:
  ```css
  nav ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }
  nav li {
    display: inline-block;
  }
  nav a {
    display: block;
    padding: 6px 12px;
    text-decoration: none;
  }
  ```

  Note: Nested rules are helpful, but they can also make it hard to visualize how much CSS you're
  actually generating. The deeper you nest, the more bandwidth it takes to serve your CSS and the
  more work it takes the browser to render. Keep selectors shallow!

- __Interpolation__

  You can use interpolation to inject values from expressions like variables and function calls into
  selectors.

  SCSS
  ```scss
  @mixin define-emoji($name, $glyph) {
    span.emoji-#{$name} {
      font-family: IconFont;
      font-variant: normal;
      font-weight: normal;
      content: $glyph;
    }
  }

  @include define-emoji("women-holding-hands", "👭");
  ```

  Generated CSS:
  ```css
  @charset "UTF-8";
  span.emoji-women-holding-hands {
    font-family: IconFont;
    font-variant: normal;
    font-weight: normal;
    content: "👭";
  }
  ```

### Property Declarations

Property declarations define how elements that match a selector are styled. But Sass adds extra
features to make them easier to write and automate.

SCSS:
```scss
.circle {
  $size: 100px;
  width: $size;
  height: $size;
  border-radius: $size / 2;
}
```

Generated CSS:
```css
.circle {
  width: 100px;
  height: 100px;
  border-radius: 50px;
}
```

- __Interpolation__

  A property's name can include interpolation, which makes it possible to dynamically generate properties
  as needed.

  SCSS:
  ```scss
  @mixin prefix($property, $value, $prefixes) {
    @each $prefix in $prefixes {
      -#{$prefix}-#{$property}: $value;
    }
    #{$property}: $value;
  }

  .gray {
    @include prefix(filter, grayscale(50%), moz webkit);
  }
  ```

  Generated CSS:
  ```css
  .gray {
    -moz-filter: grayscale(50%);
    -webkit-filter: grayscale(50%);
    filter: grayscale(50%);
  }
  ```

- __Nesting__

  Many CSS properties start with the same prefix that acts as a kind of namespace. Sass makes this easier
  and less redunant by testing.

  SCSS:
  ```scss
  .enlarge {
    font-size: 14px;
    transition: {
      property: font-size;
      duration: 4s;
      delay: 2s;
    }

    &:hover { font-size: 36px; }
  }
  ```

  Generated CSS:
  ```css
  .enlarge {
    font-size: 14px;
    transition-property: font-size;
    transition-duration: 4s;
    transition-delay: 2s;
  }
  .enlarge:hover {
    font-size: 36px;
  }
  ```

- __Hidden Declarations__

  Sometimes you only want a property declaration to show up some of the time. If a declaration is
  null or an empyth unquoted string, Sass won't compile that declaration to CSSS at all.

  SCSS:
  ```scss
  $rounded-corners: false;

  .button {
    border: 1px solid black;
    border-radius: if($rounded-corners, 5px, null);
  }
  ```

  Generated CSS:
  ```css
  .button {
    border: 1px solid black;
  }
  ```

### Parent Selector

The parent selector `&`, is a special selector invented by Sass that's used in nested selectors
to refer to the outer selector. It makes it possible to re-use the outer selector in more complex
ways, like adding a pseudo-class or adding a selector before the parent.

SCSS:
```scss
.alert {
  // The parent selector can be used to add pseudo-classes to the outer
  // selector.
  &:hover {
    font-weight: bold;
  }

  // It can also be used to style the outer selector in a certain context, such
  // as a body set to use a right-to-left language.
  [dir=rtl] & {
    margin-left: 0;
    margin-right: 10px;
  }

  // You can even use it as an argument to pseudo-class selectors.
  :not(&) {
    opacity: 0.8;
  }
}
```

Generated CSS:
```css
.alert:hover {
  font-weight: bold;
}
[dir=rtl] .alert {
  margin-left: 0;
  margin-right: 10px;
}
:not(.alert) {
  opacity: 0.8;
}
```

Other uses:
- __Adding Suffixes__:

  You can use the parent selector to add extra suffixes to the outer selector.

  SCSS:
  ```scss
  .accordion {
    max-width: 600px;
    margin: 4rem auto;
    width: 90%;
    font-family: "Raleway", sans-serif;
    background: #f4f4f4;

    &__copy {
      display: none;
      padding: 1rem 1.5rem 2rem 1.5rem;
      color: gray;
      line-height: 1.6;
      font-size: 14px;
      font-weight: 500;

      &--open {
        display: block;
      }
    }
  }
  ```

  Generated CSS:
  ```css
  .accordion {
    max-width: 600px;
    margin: 4rem auto;
    width: 90%;
    font-family: "Raleway", sans-serif;
    background: #f4f4f4;
  }
  .accordion__copy {
    display: none;
    padding: 1rem 1.5rem 2rem 1.5rem;
    color: gray;
    line-height: 1.6;
    font-size: 14px;
    font-weight: 500;
  }
  .accordion__copy--open {
    display: block;
  }
  ```

- __In SassScript__:

  The parent selector can also be used within SassScript. It's a special expression that returns
  the current parent selector in the same format used by selector functions

  SCSS:
  ```scss
  .main aside:hover,
  .sidebar p {
    parent-selector: &;
    // => ((unquote(".main") unquote("aside:hover")),
    //     (unquote(".sidebar") unquote("p")))
  }
  ```

  Generated CSS:
  ```css
  .main aside:hover,
  .sidebar p {
    parent-selector: .main aside:hover, .sidebar p;
  }
  ```

### Placeholder Selectors

Sass has a special kind of selector known as a "placeholder". It looks and acts a lot like a class
selector, but it starts with a `%` and it's not included in the CSS output. Placeholders allow for
base classing functionality that later can extended by other statements.

SCSS:
```scss
%toolbelt {
  box-sizing: border-box;
  border-top: 1px rgba(#000, .12) solid;
  padding: 16px 0;
  width: 100%;

  &:hover { border: 2px rgba(#000, .5) solid; }
}

.action-buttons {
  @extend %toolbelt;
  color: #4285f4;
}

.reset-buttons {
  @extend %toolbelt;
  color: #cddc39;
}
```

Generated CSS:
```css
.action-buttons, .reset-buttons {
  box-sizing: border-box;
  border-top: 1px rgba(0, 0, 0, 0.12) solid;
  padding: 16px 0;
  width: 100%;
}
.action-buttons:hover, .reset-buttons:hover {
  border: 2px rgba(0, 0, 0, 0.5) solid;
}

.action-buttons {
  color: #4285f4;
}

.reset-buttons {
  color: #cddc39;
}
```

Placeholder selectors are useful when writing a Sass library where each style rule may or may not
be used. As a rule of thumb, if you're writing a stylesheet just for your own app, it's often
better to just extend a class selector if one is available.

## Variables

Sass variables are simple: you assign a value to a name that begins with `$`, and then you can refer
to that name instead of the value itself. But despite their simplicity, they're one of the most useful
tools Sass brings to the table. Variables make it possible to reduce repitition, do complex math,
configure libraries, and much more.

### Declaration

Variables can be declared wherever.

SCSS:
```scss
$base-color: #c6538c;
$border-dark: rgba($base-color, 0.88);

.alert {
  border: 1px solid $border-dark;
}
```

Generated CSS:
```css
.alert {
  border: 1px solid rgba(198, 83, 140, 0.88);
}
```

Note: CSS has variables of its own, which are totally different than Sass variables:
- Sass variables are compiled away by Sass. CSS variables are included in the CSS output.

- CSS variables can have different values for different elements, but Sass variables only have
  one value at a time

- Sass variables are _imperative_, which means if you use a variable and then change its value, the
  earlier use will stay the same. CSS variables are _declarative_, which means if you change the value,
  it'll affect both earlier uses and later uses

  SCSS:
  ```scss
  $variable: value 1;
  .rule-1 {
    value: $variable;
  }

  $variable: value 2;
  .rule-2 {
    value: $variable;
  }
  ```

  Generated CSS:
  ```css
  .rule-1 {
    value: value 1;
  }

  .rule-2 {
    value: value 2;
  }
  ```

### Default Values

Normally when you assign a value to a variable, if that variable already had a value, its old
value is overwritten. But if you're writing a Sass library, you might want to allow your users
to customize your library's variables before you use them to generate CSS.

To make this possible, Sass provides the `!default` flag. This assigns a value to a variable _only_
if that variable isn't defined or its value is `null`. Otherwise, the existing value will be used.
This way, users can set variables before they import your library to customize its behavior.

SCSS:
```scss
// _library.scss
$black: #000 !default;
$border-radius: 0.25rem !default;
$box-shadow: 0 0.5rem 1rem rgba($black, 0.15) !default;

code {
  border-radius: $border-radius;
  box-shadow: $box-shadow;
}
```

SCSS:
```scss
// style.scss
$black: #222;
$border-radius: 0.1rem;

@import 'library';
```

Generated CSS:
```css
code {
  border-radius: 0.1rem;
  box-shadow: 0 0.5rem 1rem rgba(#222, 0.15);
}
```

### Scope

Variables declared at the top level of a stylesheet are _global_. This means taht they can be accessed
anywhere after they've been declared - even in another stylesheet! That's not true for all variables.
Those declared in blocks (curly braces in SCSSS or indented code in Sass) are usually _local_, and
can only be accessed within the block they were declared.

SCSS:
```scss
$global-variable: global value;

.content {
  $local-variable: local value;
  global: $global-variable;
  local: $local-variable;
}

.sidebar {
  global: $global-variable;

  // This would fail, because $local-variable isn't in scope:
  // local: $local-variable;
}
```

Generated CSS:
```css
.content {
  global: global value;
  local: local value;
}

.sidebar {
  global: global value;
}
```

### Shadow Scope

Local variables can even be declared with the same name as a global variable. If this happens, there
are actually two different variables with the same name: one local and one global. This helps ensure
that an author writing a local variable doesn't accidentally change the value of the global variable
they aren't aware of.

SCSS:
```scss
$variable: global value;

.content {
  $variable: local value;
  value: $variable;
}

.sidebar {
  value: $variable;
}
```

Generated CSS:
```css
.content {
  value: local value;
}

.sidebar {
  value: global value;
}
```

### Flow Control Scope

Variables declared in flow control rules have special scoping rules: they don't shadow variables at
the same level as the flow control rule. Instead, they just assign to those variables.

SCSS:
```scss
$dark-theme: true !default;
$primary-color: #f8bbd0 !default;
$accent-color: #6a1b9a !default;

@if $dark-theme {
  $primary-color: darken($primary-color, 60%);
  $accent-color: lighten($accent-color, 60%);
}

.button {
  background-color: $primary-color;
  border: 1px solid $accent-color;
  border-radius: 3px;
}
```

Generated CSS:
```css
.button {
  background-color: #750c30;
  border: 1px solid #f5ebfc;
  border-radius: 3px;
}
```

### Advanced Variable Functions

The Sass core library provides a couble advanced functions for working with variables.

- `variable-exists()` function: returns whether a variable with the given name exists in the
  current scope
- `global-variable-exists()` function: returns whether a variable with the given name exists in the
  global scope

## Interpolation

Interpolation can be used almost anywhere in a Sass stylesheet to embed the result of a SassScript
expression into a chunk of CSS. Just wrap an expression in `#{}`.

### In SassScript

Interpolation can be used in SassScript to inject SassScript into unquoted strings. This is particularly
useful when dynamically generating names, or when using slash-separated values.

SCSS:
```scss
@mixin inline-animation($duration) {
  $name: inline-#{unique-id()};

  @keyframes #{$name} {
    @content;
  }

  animation-name: $name;
  animation-duration: $duration;
  animation-iteration-count: infinite;
}

.pulse {
  @include inline-animation(2s) {
    from { background-color: yellow }
    to { background-color: red }
  }
}
```

Generated CSS:
```css
.pulse {
  animation-name: inline-udipajnn8;
  animation-duration: 2s;
  animation-iteration-count: infinite;
}
@keyframes inline-udipajnn8 {
  from {
    background-color: yellow;
  }
  to {
    background-color: red;
  }
}
```

### Quoted Strings

In most cases, interpolation injects the exact same text that would be used if the expression were used
as a property value. But there is one exception: the quotation marks around quoted strings are removed
(even if those quoted strings are in lists).

SCSS:
```scss
.example {
  unquoted: #{"string"};
}
```

Generated CSS:
```css
.example {
  unquoted: string;
}
```

Note: while it's tempting to use this feature to convert quoted string to unquoted strings, it's a lot
clearer to use the `unquote()` function.

## At-Rules

Much of Sass's extra functionality comes in the form of new at-rules it adds on top of CSS:

Sass also has some special behavior for plain CSS at-rules: they can contain interpolation, and they
can be insted in style rules. Some of them, like `@media` and `@supports`, also allow SassScript
to be used directly in the rule itself without interpolation

### `@import`

Sass extends CSS's `@import` rule with the ability to import Sass and CSS stylesheets, providing access
to mixins, functions, and variables and combining multiple stylesheets' css together. Unlike CSSS imports,
which require the browser to make multiple HTTP requests as it renders your page, Sass imports are
handled entirely during compilation.

Sass imports have the same syntax as CSS imports, except that they allow multiple imports to be separated
by commas rather than requiring each one to have its own `@import`.

SCSS:
```scss
// foundation/_code.scss
code {
  padding: .25em;
  line-height: 0;
}
```

SCSS:
```scss
// style.scss
@import 'foundation/code';
```

Generated CSS:
```css
code {
  padding: .25em;
  line-height: 0;
}
```

When Sass imports a file, that file is evaludated as though its contents appeared directly in place
of the `@import`. Any mixins, functions, and variables from the imported file are made available,
and all ifs CSS is included at the exact point where the `@import` was written. What's more, any
mixins, functions, or variables that were defined before the `@import` (inluding from other `@import`s)
are available in the imported stylesheet.

If the same stylesheet is imported more than once, it will be evaluated again each time. If it just
defines functions and mixins, this usually isn't a big deal. If it contains style rules they'll
be compiled to CSS more than once.

#### Finding a file

Extensions are assumed to be `.sass` or `.scss`. I.e. `@import "variables"` will automatically load
`variables.scss`

As a convention, Sass files that are only meanted to be imported, not compiled on their own begin with
`_`. For example, `_code.scss`. These are called __partials__.

If you write an `_index.scss` in a folder, when the folder itself is imported that file will be loaded
int its palce.

SCSS:
```scss
// foundation/_code.scss
code {
  padding: .25em;
  line-height: 0;
}
```

```scss
// foundation/_lists.scss
ul, ol {
  text-align: left;

  & & {
    padding: {
      bottom: 0;
      left: 0;
    }
  }
}
```

```scss
// foundation/_index.scss
@import 'code', 'lists';
```

```scss
// style.scss
@import 'foundation';
```

Generated CSS:
```css
code {
  padding: .25em;
  line-height: 0;
}

ul, ol {
  text-align: left;
}
ul ul, ol ol {
  padding-bottom: 0;
  padding-left: 0;
}
```

#### Nesting

Imports are usually written at the top level of a stylesheet, but they don't have to be.
They can be nested within style rules or plain CSS at-riles. The imported CSS is nested in that
context, which makes nested imorts useful for scoping a chunk of CSSS to a particular element
or media quert. Top-level mixins, functions, and variables defined in the nested import are still
defined globally.

SCSS:
```scss
// _theme.scss
pre, code {
  font-family: 'Source Code Pro', Helvetica, Arial;
  border-radius: 4px;
}
```

```scss
// style.scss
.theme-sample {
  @import "theme";
}
```

Generated CSS:
```css
.theme-sample pre, .theme-sample code {
  font-family: 'Source Code Pro', Helvetica, Arial;
  border-radius: 4px;
}
```

#### Importing CSS

In addition to importing `.sass` and `.scss` files, Sass can import poain old `.css` files. The only
rule is that the import must not explicitly include the `.css` extension, because that's used to indicate
a plain CSS @import.

SCSS:
```scss
// code.css
code {
  padding: .25em;
  line-height: 0;
}
```

```scss
// style.scss
@import 'code';
```

Generated CSS:
```css
code {
  padding: .25em;
  line-height: 0;
}
```

#### Plain CSS `@imports`

Because `@import` is also defined in CSS, Sass needs a way of compiling plain CSSS `@import`s without
trying to import the files at compile time. To accomplish this, and to ensure SCSS is as much of a superset
of CSS as possible, Sass will compile any `@import`s with the following characteristics:
- URL ends in `.css`
- URL begins with `http://` or `https://`
- URL is written as `url()`
- Imports that have media queries

SCSS:
```scss
@import "theme.css";
@import "http://fonts.googleapis.com/css?family=Droid+Sans";
@import url(theme);
@import "landscape" screen and (orientation: landscape);
```

Generated CSS:
```css
@import url(theme.css);
@import "http://fonts.googleapis.com/css?family=Droid+Sans";
@import url(theme);
@import "landscape" screen and (orientation: landscape);
```

Although Sass imports can't use interpolation, plain CSS imports can. This makes it possible to dynamically
generate imports.

SCSS:
```scss
@mixin google-font($family) {
  @import url("http://fonts.googleapis.com/css?family=#{$family}");
}

@include google-font("Droid Sans");
```

Generated CSS:
```css
@import url("http://fonts.googleapis.com/css?family=Droid Sans");
```

### `@mixin` and `@include`

Mixins allows you to define styles that can be re-used throughout your stylesheet.

Mixins are defined using the `@mixin` runle which is written `@mixin <name>(args) { ... }`. A mixin
can be any Sass identifier, and it can contain any statement other than top-level statements.

Mixins are included into the current context using the `@include` rule, which is written `@include <name>(args)`.

SCSS:
```scss
@mixin reset-list {
  margin: 0;
  padding: 0;
  list-style: none;
}

@mixin horizontal-list {
  @include reset-list;

  li {
    display: inline-block;
    margin: {
      left: -2px;
      right: 2em;
    }
  }
}

nav ul {
  @include horizontal-list;
}
```

Generated CSS:
```css
nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
nav ul li {
  display: inline-block;
  margin-left: -2px;
  margin-right: 2em;
}
```

#### Arguments

Mixins can also take arguments, whcih allows their behavior to be customized each time they're called.

SCSS:

```scss
@mixin rtl($property, $ltr-value, $rtl-value) {
  #{$property}: $ltr-value;

  [dir=rtl] & {
    #{$property}: $rtl-value;
  }
}

.sidebar {
  @include rtl(float, left, right);
}
```

Generated CSS:
```css
.sidebar {
  float: left;
}
[dir=rtl] .sidebar {
  float: right;
}
```

#### Optional Arguments

Normally, every argment a mixin declares must be passed when that mixin is included. However, you can
make an argument optional by defining a default value which will be used if that argument isn't passed

SCSS:
```scss
@mixin replace-text($image, $x: 50%, $y: 50%) {
  text-indent: -99999em;
  overflow: hidden;
  text-align: left;

  background: {
    image: $image;
    repeat: no-repeat;
    position: $x $y;
  }
}

.mail-icon {
  @include replace-text(url("/images/mail.svg"), 0);
}
```

Generated CSS:
```css
.mail-icon {
  text-indent: -99999em;
  overflow: hidden;
  text-align: left;
  background-image: url("/images/mail.svg");
  background-repeat: no-repeat;
  background-position: 0 50%;
}
```

#### Keyword Arguments

When a mixin is included, arguments can be passed by name in addition to passing them by their
position in the argument list

SCSS:
```scss
@mixin square($size, $radius: 0) {
  width: $size;
  height: $size;

  @if $radius != 0 {
    border-radius: $radius;
  }
}

.avatar {
  @include square(100px, $radius: 4px);
}
```

Generated CSS:
```css
.avatar {
  width: 100px;
  height: 100px;
  border-radius: 4px;
}
```

#### Arbitrary Arguments

Sometimes its useful for a mixin to be able to take any number of arguments.

SCSS:

```scss
@mixin order($height, $selectors...) {
  @for $i from 0 to length($selectors) {
    #{nth($selectors, $i + 1)} {
      position: absolute;
      height: $height;
      margin-top: $i * $height;
    }
  }
}

@include order(150px, "input.name", "input.address", "input.zip");
```

```css
input.name {
  position: absolute;
  height: 150px;
  margin-top: 0px;
}

input.address {
  position: absolute;
  height: 150px;
  margin-top: 150px;
}

input.zip {
  position: absolute;
  height: 150px;
  margin-top: 300px;
}
```

#### Arbitrary Keyword Arguments

Generated CSS:
SCSS:
```scss
@mixin syntax-colors($args...) {
  @debug keywords($args); // (string: #080, comment: #800, $variable: $60b)

  @each $name, $color in keywords($args) {
    pre span.stx-#{$name} {
      color: $color;
    }
  }
}

@include syntax-colors(
  $string: #080,
  $comment: #800,
  $variable: #60b,
)
```

Generated CSS:
```css
pre span.stx-string {
  color: #080;
}

pre span.stx-comment {
  color: #800;
}

pre span.stx-variable {
  color: #60b;
}
```

#### Content Blocks

In addition to taking arguments, a mixin can take an entire block of styles, known as a _content block_.
A mixin can declare that it takes a content block by including the `@content` rule in its body.

SCSS:
```scss
@mixin hover {
  &:not([disabled]):hover {
    @content;
  }
}

.button {
  border: 1px solid black;
  @include hover {
    border-width: 2px;
  }
}
```

Generated CSS:
```css
.button {
  border: 1px solid black;
}
.button:not([disabled]):hover {
  border-width: 2px;
}
```

Note: content blocks are _lexically scoped_, which means they can only see local variables in the
scope where the mixin is included.

### `@function`

Functions allow you to define complex opeations on SassScript values that you can re-use throughout your
stylesheet. They make it easy to abstract out common formulas and behaviors in a readable way.

Functions are defined using the `@function` rule, which is written `@function <name>(arguments) { ... }`.

SCSS:
```scss
@function pow($base, $exponent) {
  $result: 1;
  @for $_ from 1 through $exponent {
    $result: $result * $base;
  }
  @return $result;
}

.sidebar {
  float: left;
  margin-left: pow(4, 3) * 1px;
}
```

Generated CSS:
```css
.sidebar {
  float: left;
  margin-left: 64px;
}
```

While its technically possible for functions to have side-effects like setting global variables,
this is strongly discouraged. Use mixins for side-effects, and use function to compute values.

#### Optional Arguments

SCSS:
```scss
@function invert($color, $amount: 100%) {
  $inverse: change-color($color, $hue: hue($color) + 180);
  @return mix($inverse, $color, $amount);
}

$primary-color: #036;
.header {
  background-color: invert($primary-color, 80%);
}
```

Generated CSS:
```css
.header {
  background-color: #523314;
}
```

#### Keyword Arguments

SCSS:
```scss
$primary-color: #036;
.banner {
  background-color: $primary-color;
  color: scale-color($primary-color, $lightness: +40%);
}
```

Generated CSS:
```css
.banner {
  background-color: #036;
  color: #0a85ff;
}
```

#### Arbitrary Arguments

SCSS:
```scss
@function sum($numbers...) {
  $sum: 0;
  @each $number in $numbers {
    $sum: $sum + $number;
  }
  @return $sum;
}

.micro {
  width: sum(50px, 30px, 100px);
}
```

Generated CSS:
```css
.micro {
  width: 180px;
}
```

#### `@return`

The `@return` rule indicates the value to use as the result of calling a function. It's only
allowed within a `@function` body, and each `@function` must end with a `@return`.

When `@return` is encountered, it immediately ends the function and returns its result.

SCSS:
```scss
@function str-insert($string, $insert, $index) {
  // Avoid making new strings if we don't need to.
  @if str-length($string) == 0 {
    @return $insert;
  }

  $before: str-slice($string, 0, $index);
  $after: str-slice($string, $index);
  @return $before + $insert + $after;
}
```

### `@extend`

There are often cases when designing a page when one class should have all the styles of another class,
as well as its own specific styles. This can created clutterd HTML, it's prone to errors from forgetting
to included both classes, and it can bering non-semantic style concerns into your markup.

Sass's @extend rule solves this. It's written `@extend <selector>`, and it tells Sass that one selector
should inherit the styles of another.

SCSS:
```scss
.error {
  border: 1px #f00;
  background-color: #fdd;

  &--serious {
    @extend .error;
    border-width: 3px;
  }
}
```

Generated CSS:
```css
.error, .error--serious {
  border: 1px #f00;
  background-color: #fdd;
}
.error--serious {
  border-width: 3px;
}
```

Sass knows _everywhere_ the selector is used. This ensures that your elements are styled exactly
as if they matched the extended selector.

SCSS:
```scss
.error:hover {
  background-color: #fee;
}

.error--serious {
  @extend .error;
  border-width: 3px;
}
```

Generated CSS:
```css
.error:hover, .error--serious:hover {
  background-color: #fee;
}

.error--serious {
  border-width: 3px;
}
```

#### Mandatory and Optional

Normally, if an `@extend` doesn't match any selectors in the stylesheet, Sass will produce an error.
This helps protect from typos or from renaming a selector without renaming the selectors that inherit
from it.

This may not always be the desired behavior. If you want the `@extend` to do nothing if the extended
selector doesn't exist, just add the `!optional` to the end.

#### Extend vs Mixin

As a rule of thumb, extends are the best option when you're expressing a relationship between
semantic clases. For instance `.error--series` is an `.error`, it makes sense for it to extent
`.error`.

For non-semantic collections of styles, writing a mixing can avoid cascade headaches and make it
easier to configure down the line.

#### Limitations

Only simple selectors like `.info` or `h1` can be extended.

SCSS:
```scss
.alert {
  @extend .message.info;
  //      ^^^^^^^^^^^^^
  // Error: Write @extend .message, .info instead.

  @extend .main .info;
  //      ^^^^^^^^^^^
  // Error: write @extend .info instead.
}
```

Extending in `@media` is allowed, it's not allowed to extend selectors that appear outside its at-rule.

SCSS
```scss
@media screen and (max-width: 600px) {
  .error--serious {
    @extend .error;
    //      ^^^^^^
    // Error: ".error" was extended in @media, but used outside it.
  }
}

.error {
  border: 1px #f00;
  background-color: #fdd;
}
```

### `@error`

When writing mixins and functions that take arguments, you usually want to ensure that those arguments
have the types and formats your API expects. If they aren't, the user needs to be notified and your
mixin/function needs to stop running.

Sass makes this easy with the `@error` rule wich is writting `@error <expression>`.

SCSS:
```scss
@mixin reflexive-position($property, $value) {
  @if $property != left and $property != right {
    @error "Property #{$property} must be either left or right.";
  }

  $left-value: if($property == right, initial, $value);
  $right-value: if($property == right, $value, initial);

  left: $left-value;
  right: $right-value;
  [dir=rtl] & {
    left: $right-value;
    right: $left-value;
  }
}

.sidebar {
  @include reflexive-position(top, 12px);
  //       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // Error: Property top must be either left or right.
}
```

### `@warn`

When writing mixins and functions, you may want to discourage users from passing certain arguments
or certain values. They may be passing legacy arguments that are now deprecated, or they may be
calling your API in a way that's not quite optimal.

The `@warn` rule is designed for these scenarios. It's written `@warn <expression>`.

SCSS:
```scss
$known-prefixes: webkit, moz, ms, o;

@mixin prefix($property, $value, $prefixes) {
  @each $prefix in $prefixes {
    @if not index($known-prefixes, $prefix) {
      @warn "Unknown prefix #{$prefix}.";
    }

    -#{$prefix}-#{$property}: $value;
  }
  #{$property}: $value;
}

.tilt {
  // Oops, we typo'd "webkit" as "wekbit"!
  @include prefix(transform, rotate(15deg), wekbit ms);
}
```

### `@debug`

Sometimes it's useful to see the value of a variable or expression while you're developing your
stylesheet. That's what the `@debug` rule is for. It's written `@debug <expression>`, and it prints
the value of that expression, along with the filename and line number.

SCSS:
```scss
@mixin inset-divider-offset($offset, $padding) {
  $divider-offset: (2 * $padding) + $offset;
  @debug "divider offset: #{$divider-offset}";

  margin-left: $divider-offset;
  width: calc(100% - #{$divider-offset});
}
```

### `@at-root`

The `@at-root` rule is usually written `@at-root <selector> { ... }` and causes everything within it
to be emitted at the root of the document instead of using the normal nesting.

### `@if`

The `@if` rule is written `@if <expression> { ... }`, and it controls whether or not its block gets
evaluated. The expression usually returns either `true` or `false`.

SCSS:
```scss
@mixin avatar($size, $circle: false) {
  width: $size;
  height: $size;

  @if $circle {
    border-radius: $size / 2;
  }
}

.square-av { @include avatar(100px, $circle: false); }
.circle-av { @include avatar(100px, $circle: true); }
```

Generated CSS:
```css
.square-av {
  width: 100px;
  height: 100px;
}

.circle-av {
  width: 100px;
  height: 100px;
  border-radius: 50px;
}
```

### `@else`

SCSS:
```scss
$light-background: #f2ece4;
$light-text: #036;
$dark-background: #6b717f;
$dark-text: #d2e1dd;

@mixin theme-colors($light-theme: true) {
  @if $light-theme {
    background-color: $light-background;
    color: $light-text;
  } @else {
    background-color: $dark-background;
    color: $dark-text;
  }
}

.banner {
  @include theme-colors($light-theme: true);
  body.dark & {
    @include theme-colors($light-theme: false);
  }
}
```

Generated CSS:
```css
.banner {
  background-color: #f2ece4;
  color: #036;
}
body.dark .banner {
  background-color: #6b717f;
  color: #d2e1dd;
}
```

### `@else-if`

SCSS:
```scss
@mixin triangle($size, $color, $direction) {
  height: 0;
  width: 0;

  border-color: transparent;
  border-style: solid;
  border-width: $size / 2;

  @if $direction == up {
    border-bottom-color: $color;
  } @else if $direction == right {
    border-left-color: $color;
  } @else if $direction == down {
    border-top-color: $color;
  } @else if $direction == left {
    border-right-color: $color;
  } @else {
    @error "Unknown direction #{$direction}.";
  }
}

.next {
  @include triangle(5px, black, right);
}
```

Generated CSS:
```css
.next {
  height: 0;
  width: 0;
  border-color: transparent;
  border-style: solid;
  border-width: 2.5px;
  border-left-color: black;
}
```

### `@each`

The `@each` rule makes it easy to emit styles or evaluate code for each element of a list or pair
in a mapy. It's great for repetitive styles that only have a few variations between them. It's
usually written `@each <variable> in <expression> { ... }` where the expression returns a list.

#### Lists

SCSS:
```scss
$sizes: 40px, 50px, 80px;

@each $size in $sizes {
  .icon-#{$size} {
    font-size: $size;
    height: $size;
    width: $size;
  }
}
```

Generated CSS:
```css
.icon-40px {
  font-size: 40px;
  height: 40px;
  width: 40px;
}

.icon-50px {
  font-size: 50px;
  height: 50px;
  width: 50px;
}

.icon-80px {
  font-size: 80px;
  height: 80px;
  width: 80px;
}
```

#### Maps

SCSS:
```scss
$icons: ("eye": "\f112", "start": "\f12e", "stop": "\f12f");

@each $name, $glyph in $icons {
  .icon-#{$name}:before {
    display: inline-block;
    font-family: "Icon Font";
    content: $glyph;
  }
}
```

Generated CSS:
```css
@charset "UTF-8";
.icon-eye:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
}

.icon-start:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
}

.icon-stop:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
}
```

#### Destructuring

SCSS:
```scss
$icons:
  "eye" "\f112" 12px,
  "start" "\f12e" 16px,
  "stop" "\f12f" 10px;

@each $name, $glyph, $size in $icons {
  .icon-#{$name}:before {
    display: inline-block;
    font-family: "Icon Font";
    content: $glyph;
    font-size: $size;
  }
}
```

Generated CSS:
```css
@charset "UTF-8";
.icon-eye:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
  font-size: 12px;
}

.icon-start:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
  font-size: 16px;
}

.icon-stop:before {
  display: inline-block;
  font-family: "Icon Font";
  content: "";
  font-size: 10px;
}
```

### `@for`

The `@for` rule, written `@for <variable> from <expression> to <expression> { ... }`, counts up
or down from one number to another and evaluates a block for each number in between.

If `to` is used, the final number is excluded.
If `through` is used, the final number is included.

SCSS:
```scss
$base-color: #036;

@for $i from 1 through 3 {
  ul:nth-child(3n + #{$i}) {
    background-color: lighten($base-color, $i * 5%);
  }
}
```

Generated CSS:
```css
ul:nth-child(3n + 1) {
  background-color: #004080;
}

ul:nth-child(3n + 2) {
  background-color: #004d99;
}

ul:nth-child(3n + 3) {
  background-color: #0059b3;
}
```

### `@while`

The `@while` rule, written `@while <expression> { ... }` evaluates its block as long as its expression
returns `true`.

SCSS:
```scss
/// Divides `$value` by `$ratio` until it's below `$base`.
@function scale-below($value, $base, $ratio: 1.618) {
  @while $value > $base {
    $value: $value / $ratio;
  }
  @return $value;
}

$normal-font-size: 16px;
sup {
  font-size: scale-below(20px, 16px);
}
```

Generated CSS:
```css
sup {
  font-size: 12.36094px;
}
```

## Values

Sass supports a number of value types, most of which come straight from CSS. Every expression produces
a value, variables hold values.

### Numbers

Numbers in Sass have two components: the number itself, and its unit. Numbers can have no units or complex
units.

SCSS:
```scss
@debug 100; // 100
@debug 0.8; // 0.8
@debug 16px; // 16px
@debug 5px * 2px; // 10px*px (read "square pixels")
@debug 5.2e3; // 5200
@debug 6e-2; // 0.06
```

Note: Sass doesn't distinguish between whole numbers and decimals, so `5 / 2 => 2.5, not 2`

#### Units

Sass has powerful support for manipulating units based on how real-world unit calculations work.
When two numbers are multiplied, their units are multiplied as well.

SCSS:
```scss
@debug 4px * 6px; // 24px*px (read "square pixels")
@debug 5px / 2s; // 2.5px/s (read "pixels per second")
@debug 5px * 30deg / 2s / 24em; // 3.125px*deg/s*em
                                // (read "pixel-degrees per second-em")

$degrees-per-second: 20deg/1s;
@debug $degrees-per-second; // 20deg/s
@debug 1 / $degrees-per-second; // 0.05s/deg
```

Note: because CSSS doesn't support complex units like square pixels, using a number with complex
units as a property value will produce an error.

Sass will automatically convert between compatible units.

SCSS:
```scss
// CSS defines one inch as 96 pixels.
@debug 1in + 6px; // 102px or 1.0625in

@debug 1in + 1s;
//     ^^^^^^^^
// Error: Incompatible units s and in.
```

#### Precision

Sass numbers support up to 10 digits of precision after the decimal point. This has the following
implications:
- Only the first 10 digits after the decimal point will be included in CSS
- Operations like `==` and `>=` will consider two numbers equivalent if they're the same up to the
  10th digit after the decimal point
- If a number is less than `0.0000000001` away from an integer, it's considered to be an integer for the
  purposes of `nth()` that require integer arguments

SCSS:
```scss
@debug 0.012345678912345; // 0.0123456789
@debug 0.01234567891 == 0.01234567899; // true
@debug 1.00000000009; // 1
@debug 0.99999999991; // 1
```

### Strings

Strings are sequences of characters (specifically Unicode code points). Sass supports two kinds of
strings whose internal strucutr is the same but which render differently.
- Quoted strings, like `"Helvetica Neue"`
- Unquoted strings, like `bold`

SCSS:
```scss
@debug unquote(".widget:hover"); // .widget:hover
@debug quote(bold); // "bold"
```

#### Escapes

SCSS:
```scss
@debug "\""; // '"'
@debug \.widget; // \.widget
@debug "\a"; // "\a" (a string containing only a newline)
@debug "line1\a line2"; // "line1\a line2"
@debug "Nat + Liz \1F46D"; // "Nat + Liz 👭"
```

#### Quoted

Quoted strings are written between either single or double quotes, as in `"Helvetica Neue`. They
can contain interpolation, as well as any unescapted character.

SCSS:
```scss
@debug "Helvetica Neue"; // "Helvetica Neue"
@debug "C:\\Program Files"; // "C:\\Program Files"
@debug "\"Don't Fear the Reaper\""; // "\"Don't Fear the Reaper\""
@debug "line1\a line2"; // "line1\a line2"

$roboto-variant: "Mono";
@debug "Roboto #{$roboto-variant}"; // "Roboto Mono"
```

#### Unquoted

Unquoted strings are written as CSS identifiers.

SCSS:
```scss
@debug bold; // bold
@debug -webkit-flex; // -webkit-flex
@debug --123; // --123

$prefix: ms;
@debug -#{$prefix}-flex; // -ms-flex
```

#### Indexes

Sass has a number of string functions that take or return numbers, called indexes, that refer to
the characters in a string. The index `1` indicates the first character of the string. The index
`-1` refers to the last character in a string.

SCSS:
```scss
@debug str-index("Helvetica Neue", "Helvetica"); // 1
@debug str-index("Helvetica Neue", "Neue"); // 11
@debug str-slice("Roboto Mono", -4); // "Mono"
```

### Colors

Sass has built-in support for color values. Just like CSS colors, they represent points in the sRGB color
space, although many Sass color functions operate in the HSL color space as well.

SCSS:
```scss
@debug #f2ece4; // #f2ece4
@debug #b37399aa; // rgba(179, 115, 153, 67%)
@debug midnightblue; // #191970
@debug rgb(204, 102, 153); // #c69
@debug rgba(107, 113, 127, 0.8); // rgba(107, 113, 127, 0.8)
@debug hsl(228, 7%, 86%); // #dadbdf
@debug hsla(20, 20%, 85%, 0.7); // rgb(225, 215, 210, 0.7)
```

Sass supports many useful color functions that can be used to create new colors based on existing
ones by mixing colors together or scaling their hue, saturation, or lightness.

SCSS:
```scss
$venus: #998099;

@debug scale-color($venus, $lightness: +15%); // #a893a8
@debug mix($venus, midnightblue); // #594d85
```

### Lists

Lists contain a sequence of other values. In Sass, elements in lists can be separated by commas
(`Helvetica, Arial, sans-serif`) or by spaces (`10px 15px 0 0`) as long as it's consistent within
the list.

Sass lists are 1 indexed.

#### Functions

Sass provies a handful of functions that make it possible to use lists:
- __nth__

  Returns the nth element in the list

  SCSS:
  ```scss
  @debug nth(10px 12px 16px, 2); // 12px
  @debug nth([line1, line2, line3], -1); // line3
  ```

- __each__

  Do something to each element of the list

  SCSS:
  ```scss
  $sizes: 40px, 50px, 80px;

  @each $size in $sizes {
    .icon-#{$size} {
      font-size: $size;
      height: $size;
      width: $size;
    }
  }
  ```

  Generated CSS:
  ```css
  .icon-40px {
    font-size: 40px;
    height: 40px;
    width: 40px;
  }

  .icon-50px {
    font-size: 50px;
    height: 50px;
    width: 50px;
  }

  .icon-80px {
    font-size: 80px;
    height: 80px;
    width: 80px;
  }
  ```

- __Add to a list__

  SCSS:
  ```scss
  @debug append(10px 12px 16px, 25px); // 10px 12px 16px 25px
  @debug append([col1-line1], col1-line2); // [col1-line1, col1-line2]
  ```

- __Find an element in a list__

  SCSS:
  ```scss
  @debug index(1px solid red, 1px); // 1
  @debug index(1px solid red, solid); // 2
  @debug index(1px solid red, dashed); // null
  ```

  SCSS:
  ```scss
  $valid-sides: top, bottom, left, right;

  @mixin attach($side) {
    @if not index($valid-sides, $side) {
      @error "#{$side} is not a valid side. Expected one of #{$sides}.";
    }

    // ...
  }
  ```

#### Immutability

Lists in Sass are immutable, which means that the contents of a list value never changes. Sass's
list functions all return new lists rather than modifying the originals.

### Maps

Maps in Sass hold pairs of keys and values, and make it easy to look up a value by its corresponding key.
They're written `(<expression> : <expression>, <expression> : <expression>)`. Keys must be unique, but the
values can be duplicated.

#### Functions

Sass provides various functions for manipulating maps.
- __Look up a value__

  SCSS:
  ```scss
  $font-weights: ("regular": 400, "medium": 500, "bold": 700);

  @debug map-get($font-weights, "medium"); // 500
  @debug map-get($font-weights, "extra-bold"); // null
  ```

- __Iterate over values__

  SCSS:
  ```scss
  $icons: ("eye": "\f112", "start": "\f12e", "stop": "\f12f");

  @each $name, $glyph in $icons {
    .icon-#{$name}:before {
      display: inline-block;
      font-family: "Icon Font";
      content: $glyph;
    }
  }
  ```

  Generated CSS:
  ```css
  @charset "UTF-8";
  .icon-eye:before {
    display: inline-block;
    font-family: "Icon Font";
    content: "";
  }

  .icon-start:before {
    display: inline-block;
    font-family: "Icon Font";
    content: "";
  }

  .icon-stop:before {
    display: inline-block;
    font-family: "Icon Font";
    content: "";
  }
  ```

- __Add to a Map__

  SCSS:
  ```scss
  $light-weights: ("lightest": 100, "light": 300);
  $heavy-weights: ("medium": 500, "bold": 700);

  @debug map-merge($light-weights, $heavy-weights);
  // (
  //   "lightest": 100,
  //   "light": 300,
  //   "medium": 500,
  //   "bold": 700
  // )
  ```

  SCSS:
  ```scss
  $font-weights: ("regular": 400, "medium": 500, "bold": 700);

  @debug map-merge($font-weights, ("medium": 600));
  // ("regular": 400, "medium": 600, "bold": 700)
  ```

#### Immutability

Maps in Sass are immutable, which means that the contents of a map value never changes. Sass's
map functions all return new maps rather than modifying the originals.

### Booleans

Booleans are the logical values `true` and `false`. In addition their literal forms, booleans are
returned by equality and relational operators as well as many built-in functions.

SCSS:
```scss
@debug 1px == 2px; // false
@debug 1px == 1px; // true
@debug 10px < 3px; // false
@debug comparable(100px, 3in); // true
@debug true and true; // true
@debug true and false; // false
@debug true or false; // true
@debug false or false; // false
@debug not true; // false
@debug not false; // true
```

Booleans can be used with the `@if` rule.

SCSS:
```scss
@mixin avatar($size, $circle: false) {
  width: $size;
  height: $size;

  @if $circle {
    border-radius: $size / 2;
  }
}

.square-av { @include avatar(100px, $circle: false); }
.circle-av { @include avatar(100px, $circle: true); }
```

Generated CSS:
```css
.square-av {
  width: 100px;
  height: 100px;
}

.circle-av {
  width: 100px;
  height: 100px;
  border-radius: 50px;
}
```

Anywhere `true` or `false` are allowed, you can use other values as well. The values `false` and `null`
are _falsey_, which means Sass considers them to indicate falsehood and cause conditions to fail. Every
other value is considered _truthy_, so Sass considers them to work like `true` and cause conditions
to succeed.

### null

The value `null` is the only value of its type. It represents the absence of a value, and is often
returned by functions to indicate the lack of a result

SCSS:
```scss
@debug str-index("Helvetica Neue", "Roboto"); // null
@debug map-get(("large": 20px), "small"); // null
@debug &; // null
```

If a list contains a `null`, that `null` is omitted from the generated CSS.

SCSS:
```scss
$fonts: ("serif": "Helvetica Neue", "monospace": "Consolas");

h3 {
  font: 18px bold map-get($fonts, "sans");
}
```

Generated CSS:
```css
h3 {
  font: 18px bold;
}
```

`null` is also _falsey_, which means it counts as `false` for any rules or operators that take booleans.

### Functions

Functions can be values too! You can't directly write a function as a value, but you can pass a function's
name to the `get-function()` function to get it as a value. Once you have a function value, you can
pass it to the `call()` function to call it. This is useful for writing higher-order functions that
call other functions.

SCSS:
```scss
/// Return a copy of $list with all elements for which $condition returns `true`
/// removed.
@function remove-where($list, $condition) {
  $new-list: ();
  $separator: list-separator($list);
  @each $element in $list {
    @if not call($condition, $element) {
      $new-list: append($new-list, $element, $separator: $separator);
    }
  }
  @return $new-list;
}

$fonts: Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif;

content {
  @function contains-helvetica($string) {
    @return str-index($string, "Helvetica");
  }
  font-family: remove-where($fonts, get-function("contains-helvetica"));
}
```

## Operators

Sass supports a handful of useful operators for working with different values.

### Order of Operations

Sass has a pretty standard order of operations, from tightest to loosest:

1. The unary operators `not`, `+`, `-`, and `/`.
2. The `*`, `/`, and `%`` operators.
3. The `+` and `-` operators.
4. The `>`, `>=`, `<` and `<=` operators.
5. The `==` and `!=` operators.
6. The `and` operator.
7. The `or` operator.
8. The `=` operator, when it’s available.

SCSS:
```scss
@debug 1 + 2 * 3 == 1 + (2 * 3); // true
@debug true or false and false == true or (false and false); // true
```

You can always control the order of operations using parentheses.

SCSS:
```scss
@debug (1 + 2) * 3; // 9
@debug ((1 + 2) * 3 + 4) * 5; // 65
```

### Equality

The equality operators return whether or not two values are the same.

SCSS:
```scss
@debug 1px == 1px; // true
@debug 1px != 1em; // true
@debug 1 != 1px; // true
@debug 96px == 1in; // true

@debug "Helvetica" == Helvetica; // true
@debug "Helvetica" != "Arial"; // true

@debug hsl(34, 35%, 92.1%) == #f2ece4; // true
@debug rgba(179, 115, 153, 0.5) != rgba(179, 115, 153, 0.8); // true

@debug (5px 7px 10px) == (5px 7px 10px); // true
@debug (5px 7px 10px) != (10px 14px 20px); // true
@debug (5px 7px 10px) != (5px, 7px, 10px); // true
@debug (5px 7px 10px) != [5px 7px 10px]; // true

$theme: ("venus": #998099, "nebula": #d2e1dd);
@debug $theme == ("venus": #998099, "nebula": #d2e1dd); // true
@debug $theme != ("venus": #998099, "iron": #dadbdf); // true

@debug true == true; // true
@debug true != false; // true
@debug null != false; // true

@debug get-function("rgba") == get-function("rgba"); // true
@debug get-function("rgba") != get-function("hsla"); // true
```

### Relational

Relational operators determine whether numbers are larger or smaller than one another.

SCSS:
```scss
@debug 100 > 50; // true
@debug 10px < 17px; // true
@debug 96px >= 1in; // true
@debug 1000ms <= 1s; // true
```

Unitless numbers can be compared with any number. They're automatically converted to that number's unit

SCSS:
```scss
@debug 100 > 50px; // true
@debug 10px < 17; // true
```

Numbers with incompatible units can't be compared.

SCSS:
```scss
@debug 100px > 10s;
//     ^^^^^^^^^^^
// Error: Incompatible units px and s.
```

### Numeric

Sass supports the standard set of mathematical opeators for numbers. They automatically convert between
compatible units.

SCSS:
```scss
@debug 10s + 15s; // 25s
@debug 1in - 10px; // 0.8958333333in
@debug 5px * 3px; // 15px*px
@debug (12px/4px); // 3
@debug 1in % 9px; // 0.0625in
```

#### Unary Operators

SCSS:
```scss
@debug +(5s + 7s); // 12s
@debug -(50px + 30px); // -80px
@debug -(10px - 15px); // 5px
```

#### Slash-Separated Values

SCSS:
```scss
@debug 15px / 30px; // 15px/30px
@debug (10px + 5px) / 30px; // 0.5
@debug #{10px + 5px} / 30px; // 15px/30px

$result: 15px / 30px;
@debug $result; // 0.5

@function fifteen-divided-by-thirty() {
  @return 15px / 30px;
}
@debug fifteen-divided-by-thirty(); // 0.5

@debug (15px/30px); // 0.5
@debug (bold 15px/30px sans-serif); // bold 15px/30px sans-serif
@debug 15px/30px + 1; // 1.5
```

#### Units

SCSS:
```scss
@debug 4px * 6px; // 24px*px (read "square pixels")
@debug 5px / 2s; // 2.5px/s (read "pixels per second")
@debug 5px * 30deg / 2s / 24em; // 3.125px*deg/s*em
                                // (read "pixel-degrees per second-em")

$degrees-per-second: 20deg/1s;
@debug $degrees-per-second; // 20deg/s
@debug 1 / $degrees-per-second; // 0.05s/deg

// CSS defines one inch as 96 pixels.
@debug 1in + 6px; // 102px or 1.0625in

@debug 1in + 1s;
//     ^^^^^^^^
// Error: Incompatible units s and in.
```

### String

Sass supports a few operators that generate strings.

SCSS:
```scss
@debug "Helvetica" + " Neue"; // "Helvetica Neue"
@debug sans- + serif; // sans-serif
@debug #{10px + 5px} / 30px; // 15px/30px
@debug sans - serif; // sans-serif
@debug "Elapsed time: " + 10s; // "Elapsed time: 10s";
@debug true + " is a boolean value"; // "true is a boolean value";
```

#### Unary Operators

SCSS:
```scss
@debug / 15px; // /15px
@debug - moz; // -moz
```

### Boolean

SCSS:
```scss
@debug not true; // false
@debug not false; // true

@debug true and true; // true
@debug true and false; // false

@debug true or false; // true
@debug false or false; // false
```

## Built-In Functions

In addition to allowing users to define their own functions, there are many useful functions built
right into Sass. These functions are called using the normal CSS function syntax, with the addition of
special Sass argument syntax.

### Plain CSS

Any function call that's not either a built-in or user-defined function is compiled to a plain CSS
function. The arguments will be compiled to CSS and included as-is in the function call. This ensures
that Sass supports all CSS functions without needing to release new versions every time a new one
is added.

SCSS:
```scss
@debug var(--main-bg-color); // var(--main-bg-color)

$primary: #f2ece4;
$accent: #e1d7d2;
@debug radial-gradient($primary, $accent); // radial-gradient(#f2ece4, #e1d7d2)
```

Note: because any unknown function will be compiled to CSS, it's easy to miss when you typo a
function name. Consider using [stylelint](https://stylelint.io/) on your stylesheet's output
to be notified when this happens.

### Numbers

#### `abs($number)`

Returns the absolute value of `$number`

#### `ceil(number)`

Rounds `$number` up to the next highest whole number

#### `comparable($number1, $number2)`

Returns whether `$number1` and `$number2` have compatible units

```scss
@debug comparable(2px, 1px); // true
@debug comparable(100px, 3em); // false
@debug comparable(10cm, 3mm); // true
```

#### `floor($number)`

Rounds `$number` down to the next lowest whole number

#### `max($number...)`

Returns the highest of one or more numbers

```scss
@debug max(1px, 4px); // 4px

$widths: 50px, 30px, 100px;
@debug max($widths...); // 100px
```

#### `min($number...)`

Returns the lowest of one or more numbers.

```scss
@debug min(1px, 4px); // 1px

$widths: 50px, 30px, 100px;
@debug min($widths...); // 30px
```

#### `percentage($number)`

Converts a unitless `$number` to a percentage

```scss
@debug percentage(0.2); // 20%
@debug percentage(100px / 50px); // 200%
```

#### `random($limit: null)`

If `$limit` is `null`, returns a random decimal number between 0 and 1.

```scss
@debug random(); // 0.2821251858
@debug random(); // 0.6221325814
```

If `$limit` is a number greater than or equal to 1, returns a random whole number between 1 and `$limit`

```scss
@debug random(10); // 4
@debug random(10000); // 5373
```

#### `round($number)`

Rounds `$number` to the nearest whole number

```scss
@debug round(4); // 4
@debug round(4.2); // 4
@debug round(4.9); // 5
```

#### `unit($number)`

Returns a string representation of `$number`'s units'

```scss
@debug unit(100); // ""
@debug unit(100px); // "px"
@debug unit(5px * 10px); // "px*px"
@debug unit(5px / 1s); // "px/s"
```

#### `unitless($number)`

Returns whether `$number` has no units

```scss
@debug unitless(100); // true
@debug unitless(100px); // false
```

### String Functions

#### `quote($string)`

Returns `$string` as a quoted string

```scss
@debug quote(Helvetica); // "Helvetica"
@debug quote("Helvetica"); // "Helvetica"
```

#### `str-index($string, $substring)`

Returns the first index of `$substring` in `$string`, or `null` if `$string` doesn't contain `$substring`.

```scss
@debug str-index("Helvetica Neue", "Helvetica"); // 1
@debug str-index("Helvetica Neue", "Neue"); // 11
```

#### `str-insert($string, $insert, $index)`

Returns a copy of `$string` with `$insert` inserted at `$index`

```scss
@debug str-insert("Roboto Bold", " Mono", 7); // "Roboto Mono Bold"
@debug str-insert("Roboto Bold", " Mono", -6); // "Roboto Mono Bold"

// If $index is higher/smaller than length/negative length $insert is put at end/beginning
@debug str-insert("Roboto", " Bold", 100); // "Roboto Bold"
@debug str-insert("Bold", "Roboto ", -100); // "Roboto Bold"
```

#### `str-length($string)`

Returns the number of characters in `$string`

```scss
@debug str-length("Helvetica Neue"); // 14
@debug str-length(bold); // 4
@debug str-index(""); // 0
```

#### `str-slice($string, $start=at, $end-at: -1)`

Returns the slice of `$string` stating at `$start-at` and ending at `$end-at` (both inclusive)

```scss
@debug str-slice("Helvetica Neue", 11); // "Neue"
@debug str-slice("Helvetica Neue", 1, 3); // "Hel"
@debug str-slice("Helvetica Neue", 1, -6); // "Helvetica"
```

#### `to-upper-case($string)`

Returns a copy of `$string` with the ASCII letters converted to upper case

```scss
@debug to-upper-case("Bold"); // "BOLD"
@debug to-upper-case(sans-serif); // SANS-SERIF
```

#### `to-lower-case($string)`

Returns a copy of `$string` with the ASCII letters converted to lower case

```scss
@debug to-lower-case("Bold"); // "bold"
@debug to-lower-case(SANS-SERIF); // sans-serif
```

#### `unique-id()`

Returns a randomly-generated unquoted string that's guaranteed to be a valid CSS identifier and
to be unique within the current Sass compilation

```scss
@debug unique-id(); // uabtrnzug
@debug unique-id(); // u6w1b1def
```

#### `unquote()`

Returns `$string` as an unquoted string. This can produce strings that aren't valid CSS, so use
with caution

```scss
@debug unquote("Helvetica"); // Helvetica
@debug unquote(".widget:hover"); // .widget:hover
```

### Color Functions

#### `adjust-color()`

#### `adjust-hue()`

#### `alpha()`

#### `opacity()`

#### `blue()`

#### `change-color()`

#### `complement()`

#### `darken()`

#### `desaturate()`

#### `grayscale()`

#### `green()`

#### `hsl()`

#### `hsla()`

#### `hue()`

#### `ie-hex-str()`

#### `invert()`

#### `lighten()`

#### `lightness()`

#### `mix()`

#### `opacify()`

#### `fade-in()`

#### `red()`

#### `rgb()`

#### `rgba()`

#### `saturate()`

#### `saturation()`

#### `scale-color()`

#### `transparntize()`

#### `fade-out()`

### List Functions

#### `append()`

Returns a copy of `$list` with `$val` added to the end

```scss
@debug append(10px 20px, 30px); // 10px 20px 30px
@debug append((blue, red), green); // blue, red, green
@debug append(10px 20px, 30px 40px); // 10px 20px (30px 40px)
@debug append(10px, 20px, $separator: comma); // 10px, 20px
@debug append((blue, red), green, $separator: space); // blue red green
```

#### `index($list, $value)`

Returns the index of `$value` in `$list`

```scss
@debug index(1px solid red, 1px); // 1
@debug index(1px solid red, solid); // 2
@debug index(1px solid red, dashed); // null
```

#### `is-bracketed($list)`

Returns whether `$list` has square brackets

```scss
@debug is-bracketed(1px 2px 3px); // false
@debug is-bracketed([1px, 2px, 3px]); // true
```

#### `join($list1, $list2, $separator: auto, $bracketed: auto)`

Returns a new list containing the elements of `$list1` followed by the elements of `$list2`

```scss
@debug join(10px 20px, 30px 40px); // 10px 20px 30px 40px
@debug join((blue, red), (#abc, #def)); // blue, red, #abc, #def
@debug join(10px, 20px); // 10px 20px
@debug join(10px, 20px, $separator: comma); // 10px, 20px
@debug join((blue, red), (#abc, #def), $separator: space); // blue red #abc #def
@debug join([10px], 20px); // [10px 20px]
@debug join(10px, 20px, $bracketed: true); // [10px 20px]
```

#### `length($list)`

Returns the length of `$list`

```scss
@debug length(10px); // 1
@debug length(10px 20px 30px); // 3
@debug length((width: 10px, height: 20px)); // 2
```

#### `list-separator($list)`

Returns the name of the separator used by `$list`, either `space` or `comma`

```scss
@debug list-separator(1px 2px 3px); // space
@debug list-separator(1px, 2px, 3px); // comma
@debug list-separator('Helvetica'); // space
@debug list-separator(()); // space
```

#### `nth($list, $n)`

Returns the element of `$list` at index `$n`

```scss
@debug nth(10px 12px 16px, 2); // 12px
@debug nth([line1, line2, line3], -1); // line3
```

#### `set-nth($list, $n, $value)`

Returns a copy of `$list` with the element at index `$n` replaced with `$value`.

```scss
@debug set-nth(10px 20px 30px, 1, 2em); // 2em 20px 30px
@debug set-nth(10px 20px 30px, -1, 8em); // 10px, 20px, 8em
@debug set-nth((Helvetica, Arial, sans-serif), 3, Roboto); // Helvetica, Arial, Roboto
```

#### `zip($lists...)`

Combines every list in `$lists` into a single list of sub-lists

```scss
@debug zip(10px 50px 100px, short mid long); // 10px short, 50px mid, 100px long
@debug zip(10px 50px 100px, short mid); // 10px short, 50px mid
```

### Map Functions

#### `keywords($args)`

Returns the keywords passed to a mixin or function that takes arbitrary arguments.

```scss
@mixin syntax-colors($args...) {
  @debug keywords($args); // (string: #080, comment: #800, $variable: $60b)

  @each $name, $color in keywords($args) {
    pre span.stx-#{$name} {
      color: $color;
    }
  }
}

@include syntax-colors(
  $string: #080,
  $comment: #800,
  $variable: #60b,
)
```

#### `map-get($map, $key)`

Returns the value in `$map` associated with `$key`

```scss
$font-weights: ("regular": 400, "medium": 500, "bold": 700);
@debug map-get($font-weights, "medium"); // 500
@debug map-get($font-weights, "extra-bold"); // null
```

#### `map-has-key($map, $key)`

Returns whether `$map` contains a value associated with `$key`

```scss
$font-weights: ("regular": 400, "medium": 500, "bold": 700);

@debug map-has-key($font-weights, "regular"); // true
@debug map-has-key($font-weights, "bolder"); // false
```

#### `map-keys($map)`

Returns a comma-separated list of all the keys in `$map`

```scss
$font-weights: ("regular": 400, "medium": 500, "bold": 700);

@debug map-keys($font-weights); // "regular", "medium", "bold"
```

#### `map-merge($map1, $map2)`

Returns a new map with all the keys and values from both `$map1` and `$map2`

```scss
$light-weights: ("lightest": 100, "light": 300);
$heavy-weights: ("medium": 500, "bold": 700);

@debug map-merge($light-weights, $heavy-weights);
// (
//   "lightest": 100,
//   "light": 300,
//   "medium": 500,
//   "bold": 700
// )

// map-merge() can be used to add a single key/value pair to a map.
@debug map-merge($light-weights, ("lighter": 200));
// ("lightest": 100, "light": 300, "lighter": 200)

// It can also be used to overwrite a value in a map.
@debug map-merge($light-weights, ("light": 200));
// ("lightest": 100, "light": 200)
```

#### `map-remove($map, $keys...)`

Returns a copy of `$map` without any values associated with `$keys`

```scss
$font-weights: ("regular": 400, "medium": 500, "bold": 700);

@debug map-remove($font-weights, "regular"); // ("medium": 500, "bold": 700)
@debug map-remove($font-weights, "regular", "bold"); // ("medium": 500)
@debug map-remove($font-weights, "bolder");
// ("regular": 400, "medium": 500, "bold": 700)
```

#### `map-values($map)`

Returns a comma-separated list of all the values in `$map`

```scss
$font-weights: ("regular": 400, "medium": 500, "bold": 700);

@debug map-values($font-weights); // 400, 500, 700
```

### Selector Functions

These functions inspect and manipulate selectors. Whenever they return a selector, it's always
a comma-separated list (the selector list) that contains space-separated lists (complex selectors)
that contain unquoted strings (the compound selectors)

For example:
```scss
@debug ((unquote(".main") unquote("aside:hover")),
        (unquote(".sidebar") unquote("p")));
// .main aside:hover, .sidebar p
```

#### `is-superselector($super, $sub)`

Returns whether the selector `$super` matches all the elements that the selector `$sub` matchs.

```scss
@debug is-superselector("a", "a.disabled"); // true
@debug is-superselector("a.disabled", "a"); // false
@debug is-superselector("a", "sidebar a"); // true
@debug is-superselector("sidebar a", "a"); // false
@debug is-superselector("a", "a"); // true
```

#### `selector-append($selectors...)`

Combines `$selectors` without descendant combinators - that is, without whitespace between them.

```scss
@debug selector-append("a", ".disabled"); // a.disabled
@debug selector-append(".accordion", "__copy"); // .accordion__copy
@debug selector-append(".accordion", "__copy, __image");
// .accordion__copy, .accordion__image
```

#### `selector-extend($selector, $extendee, $extender)`

Extends `$selector` as with `@extend` rule. Returns a copy of `$selector` modified with the following
rule:

```scss
#{$extender} {
  @extend #{$extendee};
}
```

```scss
@debug selector-extend("a.disabled", "a", ".link"); // a.disabled, .link.disabled
@debug selector-extend("a.disabled", "h1", "h2"); // a.disabled
@debug selector-extend(".guide .info", ".info", ".content nav.sidebar");
// .guide .info, .guide .content nav.sidebar, .content .guide nav.sidebar
```

#### `selector-nest($selectors...)`

Combines `$selectors` as though they were nested within one another in a stylesheet.

```scss
@debug selector-nest("ul", "li"); // ul li
@debug selector-nest(".alert, .warning", "p"); // .alert p, .warning p
@debug selector-nest(".alert", "&:hover"); // .alert:hover
@debug selector-nest(".accordion", "&__copy"); // .accordion__copy
```

#### `selector-parse($selector)`

Returns `$selector` in selector value format.

```scss
@debug selector-parse(".main aside:hover, .sidebar p");
// ((unquote(".main") unquote("aside:hover")),
//  (unquote(".sidebar") unquote("p")))
```

#### `selector-replace($selector, $original, $replacement)`

Returns a copy of `$selector` with all instances of `$original` replaced by `$replacement`

```scss
@debug selector-replace("a.disabled", "a", ".link"); // .link.disabled
@debug selector-replace("a.disabled", "h1", "h2"); // a.disabled
@debug selector-replace(".guide .info", ".info", ".content nav.sidebar");
// .guide .content nav.sidebar, .content .guide nav.sidebar
```

#### `selector-unify($selector1, $selector2)`

Returns a selector that matches only elements matched by both `$selector1` and `$selector2`

```scss
@debug selector-unify("a", ".disabled"); // a.disabled
@debug selector-unify("a.disabled", "a.outgoing"); // a.disabled.outgoing
@debug selector-unify("a", "h1"); // null
@debug selector-unify(".warning a", "main a"); // .warning main a, main .warning a
```

### Introspection Functions

#### `call($function, $args...)`

Invokes `$function` with `$args` and returns the result

```scss
/// Return a copy of $list with all elements for which $condition returns `true`
/// removed.
@function remove-where($list, $condition) {
  $new-list: ();
  $separator: list-separator($list);
  @each $element in $list {
    @if not call($condition, $element) {
      $new-list: append($new-list, $element, $separator: $separator);
    }
  }
  @return $new-list;
}

$fonts: Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif;

content {
  @function contains-helvetica($string) {
    @return str-index($string, "Helvetica");
  }
  font-family: remove-where($fonts, get-function("contains-helvetica"));
}
```

#### `content-exists()`

Return whether the current mixin was passed a `@content` block

```scss
@mixin debug-content-exists {
  @debug content-exists();
  @content;
}

@include debug-content-exists; // false
@include debug-content-exists { // true
  // Content!
}
```

#### `feature-exists($feature)`

Returns whether the current Sass implementation supports `$feature`

```scss
@debug feature-exists("at-error"); // true
@debug feature-exists("unrecognized"); // false
```

#### `function-exists($name)`

Returns whether a function named `$name` is defined, either as a built-in function or as a user-defined
function.

```scss
@debug function-exists("scale-color"); // true
@debug function-exists("add"); // false

@function add($num1, $num2) {
  @return $num1 + $num2;
}
@debug function-exists("add"); // true
```

#### `get-function($name, $css: false)`

Returns the function named `$name`

#### `global-variable-exists($name)`

Returns whether the global variable name `$name` exists.

```scss
@debug global-variable-exists("var1"); // false

$var1: value;
@debug global-variable-exists("var1"); // true

h1 {
  // $var2 is local.
  $var2: value;
  @debug global-variable-exists("var2"); // false
}
```

#### `inspect($value)`

Returns a string representation of `$value`

```scss
@debug inspect(10px 20px 30px); // unquote("10px 20px 30px")
@debug inspect(("width": 200px)); // unquote('("width": 200px)')
@debug inspect(null); // unquote("null")
@debug inspect("Helvetica"); // unquote('"Helvetica"')
```

#### `mixin-exists($name)`

Returns whether a mixin named `$name` exists

```scss
@debug mixin-exists("shadow-none"); // false

@mixin shadow-none {
  box-shadow: none;
}

@debug mixin-exists("shadow-none"); // true
```

#### `type-of($value)`

Returns the type of `$value`

Can return one of the following values:
- number
- string
- color
- list
- map
- bool
- null
- function
- arglist

```scss
@debug type-of(10px); // number
@debug type-of(10px 20px 30px); // list
@debug type-of(()); // list
```

#### `variable-exists($name)`

Returns whether a variable named `$name` exists in the current scope.

```scss
@debug variable-exists("var1"); // false

$var1: value;
@debug variable-exists("var1"); // true

h1 {
  // $var2 is local.
  $var2: value;
  @debug variable-exists("var2"); // true
}
```

## Breaking Changes

New versions of Sass are as backwards-compatible as possible, but sometimes a breaking change is
necessary. Sass needs to keep up with the evolving CSS specification, and old language design mistakes
occasionally need to be fixed.

Before each breaking change is released, Sass implmentations will produce deprecation warnings for
stylesheets whose behavior will change

## Dart Sass Command Line Interface

### Usage

The Dart Sass executable can be invoked in one of two modes.

#### One-to-One Mode

Compiles a single input file to a single output location.

```bash
sass <input.scss> [output.css]
```

#### Many-to-Many Mode

Compiles one or more input files to one or more output files. It can also compile all Sass files
in a directory to CSS files with the same names in another directory.

```bash
sass [<input.css>:<output.css>] [<input/>:<output/>]...
```

### Options

#### `--stdin`

This flag is an alternative way of telling Sass that it should read its input file from standard input.

```bash
$ echo "h1 {font-size: 40px}" | sass --stdin h1.css
$ echo "h1 {font-size: 40px}" | sass --stdin
h1 {
  font-size: 40px;
}
```

#### `--indented`

This flag tells Sass to parse the input file as indented syntax

```bash
$ echo -e 'h1\n  font-size: 40px' | sass --indented -
h1 {
  font-size: 40px;
}
```

#### `--load-path`

This option adds an additional load path for Sass to look for imports.

```bash
$ sass --load-path=node_modules/bootstrap/dist/css style.scss style.css
```

#### `--style`

This option controls the output style of the resulting CSS. Two styles are supported
1. `expanded` (the default): writes each selector and declaration on its own line
2. `compressed`: removes as many extra characters as possible, and writes the entire stylesheet on a single line

```bash
$ sass --style=expanded style.scss
h1 {
  font-size: 40px;
}

$ sass --style=compressed style.scss
h1{font-size:40px}
```

#### `--no-charset`

This flag tells Sass never to emit a @charset declaration or a UTF-8 byte-order mark.

```bash
$ echo 'h1::before {content: "👭"}' | sass --no-charset
h1::before {
  content: "👭";
}

$ echo 'h1::before {content: "👭"}' | sass --charset
@charset "UTF-8";
h1::before {
  content: "👭";
}
```

#### `--error-css`

This flag tells Sass whether to emit a CSS file when an error occurs during compilation.

#### `--update`

If set, Sass will only compile stylesheets whose dependencies have been modified more recently than
the corresponding CSS file was generated.

```bash
$ sass --update themes:public/css
Compiled themes/light.scss to public/css/light.css.
```

#### `--no-source-map`

If set, Sass won’t generate any source maps.

#### `--source-map-urls`

This option controls how the source maps that Sass generates link back to the Sass files that
contributed to the generated CSS. Dart Sass supports two types of URLs:

1. `relative` (the default) uses relative URLs from the location of the source map file to the
   locations of the Sass source file.

2. `absolute` uses the absolute file: URLs of the Sass source files. Note that absolute URLs will
   only work on the same computer that the CSS was compiled.

```bash
​# Generates a URL like "../sass/style.scss".
$ sass --source-map-urls=relative sass/style.scss css/style.css

​# Generates a URL like "file:///home/style-wiz/sassy-app/sass/style.scss".
$ sass --source-map-urls=absolute sass/style.scss css/style.css
```

#### `--embed-source-map`

If set, Sass will embed the contents of the source map file in the generated CSS.

#### `--watch`

This flag acts like the `--update` flag, but after the first round compilation is done Sass stays open
and continues compiling stylesheets whenever they or their dependencies change.

```bash
$ sass --watch themes:public/css
Compiled themes/light.scss to public/css/light.css.

​# Then when you edit themes/dark.scss...
Compiled themes/dark.scss to public/css/dark.css.
```

#### `--poll`

This flag, which may only be passed along with `--watch`, tells Sass to manually check for changes to the
source files every so often instead of relying on the operating system to notify it when something
changes. This may be necessary if you’re editing Sass on a remote drive where the operating system’s
notification system doesn’t work.

#### `--stop-on-error`

This flag tells Sass to stop compiling immediately when an error is detected, rather than trying to
compile other Sass files that may not contain errors. It’s mostly useful in many-to-many mode.

#### `--interactive`

This flag tells Sass to run in interactive mode, where you can write SassScript expressions and see
their results. Interactive mode also supports variables.

```bash
$ sass --interactive
>> 1px + 1in
97px
>> $map: ("width": 100px, "height": 70px)
("width": 100px, "height": 70px)
>> map-get($map, "width")
100px
```

#### `--color`

This flag tells Sass to emit terminal colors, and the inverse `--no-color` tells it not to emit
colors. By default, it will emit colors if it looks like it’s being run on a terminal that supports them.

#### `--no-unicode`

If set, Sass only emits ASCII characters to the terminal as part of error messages.

#### `--quiet`

Uf set, Sass will not emit any warnings when compiling.

#### `--trace`

If set, Sass will print the full Dart or JavaScript stack trace when an error is encountered.

#### `--help`

Prints summary documentation.

#### `--version`

Prints the current version of Sass.

## JavaScript API

Both major Sass implementations support the same JavaScript API.
- Dart Sass is distributed as a pure-JavaSscript [sass package](https://www.npmjs.com/package/sass)
- LibSass is distributed as a native extension in the [node-sass package](https://www.npmjs.com/package/node-sass)

### Usage

The Sass module provides two functions with similar APIs.

#### `sass.renderSync()`

This function synchronously compiles a Sass file to CSS. If it succeeds, it returns the result.

```js
var sass = require('sass'); // or require('node-sass');

var result = sass.renderSync({file: "style.scss"});
// ...
```

#### `sass.render()`

This function asynchronously compiles a Sass file to CSS, and calls a standard Node callback with
the result or an error.

```js
var sass = require('sass'); // or require('node-sass');

sass.render({
  file: "style.scss"
}, function(err, result) {
  // ...
});
```

#### Result Object

When `renderSync()` or `render()` succeed, they provide a result object that contains information about
the compilation. This object has the following properties
- __result.css__: The compiled CSS, as a Buffer. This can be converted to a string by calling `Buffer.toString()`

  ```js
  var result = sass.renderSync({file: "style.scss"});

  console.log(result.css.toString());
  ```

- __result.map__: The source map that maps the compiled CSS to the source files from which it was generated

  This is `null` or `undefined` unless either: the sourceMap option is a string, or the sourceMap option
  is true.

  ```js
  var result = sass.renderSync({
    file: "style.scss",
    sourceMap: true,
    outFile: "style.css"
  })

  console.log(result.map.toString());
  ```

- __result.stats.includedFiles__:

  An array of the absolute paths of all Sass files loaded during compilation

- __result.stats.entry__:

  The absolute path of the inpute file passed as the file option

- __result.stats.start__:

  The number of milliseconds between UTC and time which compilation began

- __result.stats.end__:

  The number of milliseconds between UTC and time compilation completed

- __result.stats.duration__:

  The number of milliseconds to combile the sass file

#### `sass.info`

The `info` property contains a string that includes tab-separated information about the Sass implementation.

```js
console.log(sass.info);
// dart-sass    1.20.3  (Sass Compiler) [Dart]
// dart2js  2.0.0   (Dart Compiler) [Dart]
```

### Integrations

Most populater Node.js build systems have integrations available for the JS API:
- Webpack uses the [sass-loader package](https://www.npmjs.com/package/sass-loader).
- Gulp uses the [gulp-sass package](https://www.npmjs.com/package/gulp-sass).
- Broccoli uses the [broccoli-sass-source-maps package](https://www.npmjs.com/package/broccoli-sass-source-maps).
- Ember uses the [ember-cli-sass package](https://www.npmjs.com/package/ember-cli-sass).
- Grunt uses the [grunt-sass package](https://www.npmjs.com/package/grunt-sass).