# Sass Projects Documentation

## scss

Isolated SCSS examples.

To Run:

```bash
# Install SASS
sudo npm install -g sass

# Compile each scss/*.scss file to its css/*.css equilvalent:
# Inline source maps to save on files created
rm css/*
sass --watch --embed-source-map scss/:css/
```