# Cypress Plugins Documentation

Plugins provide a way to support and extend the behavior of Cyrpress.

Relevant URLs:
[Plugin Home](https://docs.cypress.io/plugins/),

## cypress-log-to-output

cypress-log-to-output is a plugin that sends all logs that occur in the browser to stdout in the
terminal. This means that you can see any kind of error that occurs in the borwser, even if your
tests are running headlessly.

Note: `Electron` is not currently supported.

Relevant URLs:
[GitHub](https://github.com/flotwig/cypress-log-to-output)

### Installation

```bash
# Via NPM
npm install --save-dev cypress-log-to-output
```

### Usage

In `cypress/plugins/index.js`:
```js
module.exports = (on, config) => {
  /** the rest of your plugins... **/
  require('cypress-log-to-output').install(on)
}
```

You will now see all browser logs in your console output.

### Filtering Events

If you want to filter events, use custom filtering.
```js
module.exports = (on, config) => {
  /** the rest of your plugins... **/
  require('cypress-log-to-output').install(on, (type, event) => {
    // return true or false from this plugin to control if the event is logged
    // `type` is either `console` or `browser`
    // if `type` is `browser`, `event` is an object of the type `LogEntry`:
    //  https://chromedevtools.github.io/devtools-protocol/tot/Log#type-LogEntry
    // if `type` is `console`, `event` is an object of the type passed to `Runtime.consoleAPICalled`:
    //  https://chromedevtools.github.io/devtools-protocol/tot/Runtime#event-consoleAPICalled

    // for example, to only show error events:

    if (event.level === 'error' || event.type === 'error') {
      return true
    }

    return false
  })
}
```

## cypress-failed-log

Shows the failing Cypress commands in the terminal.

Relevant URLs:
[GitHub](https://github.com/bahmutov/cypress-failed-log)

### Installation

```bash
# Via NPM
npm install --save-dev cypress-failed-log
```

### Usage

Include in `cypress/support/index.js`:
```js
require('cypress-failed-log')
```

Include in `cypress/plugins/index.js`:
```js
module.exports = (on, config) => {
  on('task', {
    failed: require('cypress-failed-log/src/failed')(),
  })
}
```

### Terminal Output

All failed commands will be written to the termainl output.

### JSON Output File

All failed commands will be saved into a JSON file. The saved JSON file will live in `cypress/logs`
and have the following properties:
- specName: filename of the spec
- title: the name of the test
- suiteName: the parent suite name
- testName: full name of the test, including the suite name
- testError: error message string
- testCommands: array of strings, last failing command is last
- screenshot: filename of PNG file taken right after failure

## cypress-pipe

`cy.pipe` as a replacement for Cypress custom commands. It works very similarly to `cy.then`
except for a few key differences:
- `pipe` will try to document the function name in the Command log
- `pipe` will create DOM snapshots to aid in debugging
- If the function passed to `pipe` resolves synchronously
    * AND returns a jQuery element, `pipe` will retry until the jQuery element list is not empty
    * AND is followed by a `cy.should`, the function will be retried until the assertion passes
      or times out

Relevant URLs:
[NPM](https://www.npmjs.com/package/cypress-pipe),
[GitHub](https://github.com/NicholasBoll/cypress-pipe),
[Test Click Blog](https://cypress-io.ghost.io/blog/2019/01/22/when-can-the-test-click/)

### Installation

```bash
# Via NPM
npm install cypress-pipe -D
```

Add the following to `cypress/support/index.js`
```js
import 'cypress-pipe'
```

### Usage

With Cypress pipe the assertion below passes after 1 second.

```js
const obj = { foo: 'bar' }
const getFoo = s => s.foo
setTimeout(() => { obj.foo = 'baz' }, 1000)

cy.wrap(obj)
  .pipe(getFoo)
  .should('equal', 'baz')
```

The cypress command log will look like:
```text
WRAP       {foo: bar}
 - PIPE    getFoo
 - ASSERT  expected bar to equal bar
```

### Best Practices

#### Use Sync functions when possible

Synchronous functions can be retried, async functions cannot. Retrying allows implicit waiting which
avoids confusing flaky failures where tests are dependant on timing.
```js
// bad
// The `cy.*` command inside the function prevents automatic retries. The following will actually
// fail if the text `'foobar'` isn't immediately available in the DOM
const getFirst = $el => cy.wrap($el).find('#first')
cy.get('body')
  .pipe(getFirst)
  .should('contain', 'foobar')

// good
// synchronous resolution - pipe will retry `getFirst` until it returns a non-empty jQuery element list
// and contains the text 'foobar'
const getFirst = $el => $el.find('#first')
cy.get('body')
  .pipe(getFirst)
  .should('contain', 'foobar')
```

#### Use cy commands for actions

```js
const submitForm = $el => cy.wrap($el).find('#submit').click()
cy.get('form')
  .pipe(submitForm) // has before/after of submitting form

// Command Log:
// GET        <form>
//  -PIPE     submitForm
//  -WRAP     <form>
//  -FIND     #submit
//  -CLICK
```
## cypress-autorecord

Cypress Autorecord is a plugin built to be used with Cypress.io. It simplifies mocking by
auto-recording/stubbing HTTP interactions and automating the process of updating/deleting recordings.
Spend more time writing integration tests instead of managing your mock data.

Relevant URLs:
[NPM](https://www.npmjs.com/package/cypress-autorecord),
[GitHub](https://github.com/Nanciee/cypress-autorecord)

### Installation

```bash
# Via NPM
npm install --save-dev cypress-autorecord
```

Add snippet to project's `cypress/plugins/index.js`:
```js
const fs = require('fs');
const autoRecord = require('cypress-autorecord/plugin');

module.exports = (on, config) => {
  autoRecord(on, config, fs);
};
```

### Usage

To allow for auto-recording and stubbing to work, require cypress-autorecord in each of your test
files and call the function at the beginning of the parent `describe` block.
```js
const autoRecord = require('cypress-autorecord'); // Require the autorecord function

describe('Home Page', function() {
  // Do not use arrow functions
  // Call the autoRecord function at the beginning of your describe block and pass
  // in `__filename`, other values will not work at this point
  autoRecord(__filename);

  // Your hooks (beforeEach, afterEach, etc) goes here

  it('...', function() {
    // Do not use arrow functions
    // Your test goes here
  });
});
```

### Updating Mocks

In case you need to update your mocks for a particular test:
```js
const autoRecord = require('cypress-autorecord');

describe('Home Page', function() {
  autoRecord(__filename);

  it('[r] my awesome test', function() { // Insert [r] at the beginning of your test name
    // ...
  });
});
```
This will fore the test to record over your existent mocks for __ONLY__ thhis test on your next run.
This can also be done through the configurations by adding the test name in `cypress.json`:
```json
{
  "autorecord": {
    "recordTests": ["my awesome test"]
  }
}
```

Can update the recordings for all tests as follows:
```json
{
  "autorecord": {
    "forceRecord": true
  }
}
```

### Removing Stale Mocks

Stale mocks that are no longer being used can be automatically removed as follows:
```json
{
  "autorecord": {
    "cleanMocks": true
  }
}
```
Note: make sure to only set `cleanMocks` to `true` when you are running `ALL` tests. Remove any
unintentional `.only` or `.skip`.

### How it Works

Cypress Autorecord uses Cypress' built-in `cy.server` to hook into every request. If mocks doesn't
exist for a test, the http calls (requests and responses) are captured and automatically written
to a local file. If mocks exist for a test, each http call will be stubbed using `cy.route` in the
`beforeEach` hook.

The mocks will be automatically generated and saved in the `cypress/mocks` folder. Mocks are grouped
by test name and test file name.

Mocks are saved as simple json objects and can be updated manually. However, this is __not__ recommended
since any manual change you make will be overwritten when you automatically update the mocks. Leave
the data management to cypress-autorecord. Make any modifications to the http calls inside your test
so that it will be consistent across recordings.

```js
it('should display an error message when send message fails', function() {
  cy.route({
    url: '/message',
    method: 'POST',
    status: 404,
    response: { error: 'It did not work' },
  });

  cy.get('[data-cy="msgInput"]').type('Hello World!');
  cy.get('[data-cy="msgSend"]').click();
  cy.get('[data-cy="errorMessage"]').should('contain', 'Looks like we ran into a problem. Please try again.');
});
```

## cypress-auto-mock

This cypress plugin allows recording API results and replaying the APIs as a mock server.

Relevant URLs:
[NPM](https://www.npmjs.com/package/cypress-auto-mock),
[GitHub](https://github.com/mannyyang/cypress-auto-mock#readme)

### Installation

Add to project:
```bash
# Via NPM
npm install --save cypress-auto-mock
```

Add cypress web hooks to your application:
```js
import installCypressHooks from 'cypress-auto-mock/include-in-webapp';
installCypressHooks();
```

Add the following to `cypress/support/commands.js`:
```js
import registerAutoMockCommands from 'cypress-auto-mock/include-in-tests';
registerAutoMockCommands();
```

### Usage

In each of your tests add the following

```js
describe('Mocked Test', () => {
  // The name of the JSON file that contains the recorded mock data.
  const MOCK_FILENAME = 'testCounter';

  before(() => {
    // default options
    let options = {
      isCustomMock: true,
      outDir: '/tests/e2e/mocks'
    };
    cy.automock(MOCK_FILENAME, {...options});
  });

  after(() => {
    cy.automockEnd();
  });
})
```

### Configuration

You can control the recording and playback behavior using the (optional) automocker field in
`cypress.json`:

```json
"automocker": {
  "record": true,
  "playback": true
}
```
The default (that is, if "automocker" doesn't exist) is to treat both `record` and `playback` as `true`,
which means that it will automatically record API calls (if the proper commands are called in the
tests) if the mock file does not exits, and will play them back as mocks if they do exist.

| Desired Behavior             | record | playback |
|:-----------------------------|:-------|:---------|
| Creating a new test and tape | true   | false    |
| Rerecording a test and tape  | true   | false    |
| CI Testing                   | false  | true     |

## cypress-image-snapshot

Cypress Image snapshot binds jest-image-snapshot's image diffing logic to Cypress.io commands. The
goal is to catch visual regressions during integration tests.

Relevant URLs:
[Github](https://github.com/palmerhq/cypress-image-snapshot),
[NPM](https://www.npmjs.com/package/cypress-image-snapshot)

### Installation

Install with npm:
```bash
npm install --save-dev cypress-image-snapshot
```

Add to `<rootDir>/cypress/plugins/index.js`:
```js
const { addMatchImageSnapshotPlugin } = require('cypress-image-snapshot/plugin');

module.exports = (on, config) => {
  addMatchImageSnapshotPlugin(on, config);
};
```

Add to `<rootDir>/cypress/support/commands.js`:
```js
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();
```

### Syntax

```js
// addMatchImageSnapshotPlugin
addMatchImageSnapshotPlugin(on, config);

// addMatchImageSnapshotCommand
addMatchImageSnapshotCommand();
addMatchImageSnapshotCommand(commandName);
addMatchImageSnapshotCommand(options);
addMatchImageSnapshotCommand(commandName, options);

// matchImageSnapshot
.matchImageSnapshot();
.matchImageSnapshot(name);
.matchImageSnapshot(options);
.matchImageSnapshot(name, options);

// ---or---

cy.matchImageSnapshot();
cy.matchImageSnapshot(name);
cy.matchImageSnapshot(options);
cy.matchImageSnapshot(name, options);
```

### Usage In Tests

```js
describe('Login', () => {
  it('should be publicly accessible', () => {
    cy.visit('/login');

    // snapshot name will be the test title
    cy.matchImageSnapshot();

    // snapshot name will be the name passed in
    cy.matchImageSnapshot('login');

    // options object passed in
    cy.matchImageSnapshot(options);

    // match element snapshot
    cy.get('#login').matchImageSnapshot();
  });
});
```

#### Updating Snapshots

Run Cypress with `--env updateSnapshots=true` in order to update the base image for all your tests.

#### Prevent Failures

Run Cypress with `--env failOnSnapshotDiff=false` in order to prevent test failures when an image diff
does not pass.

#### Reporter

Run Cypress with `--reporter cypress-image-snapshot/reporter` in order to report snapshot diffs in your
test results. This can be helpful to use with `--env failOnSnapshotDiff=false` in order to quickly
view all failing snapshots and their diffs.

### Options

- `customSnapshotsDir` : Path to the directory that snapshot images will be written to, defaults to
  `<rootDir>/cypress/snapshots`.

- `customDiffDir`: Path to the directory that diff images will be written to, defaults to a
  sibling __diff_output__ directory alongside each snapshot.

The local options in `cy.matchImageSnapshot()` will overwrite the default options set in `addMatchImageSnapshot`.

The default options are:
```js
addMatchImageSnapshotCommand({
  failureThreshold: 0.03, // threshold for entire image
  failureThresholdType: 'percent', // percent of image or number of pixels
  customDiffConfig: { threshold: 0.1 }, // threshold for each pixel
  capture: 'viewport', // capture viewport in screenshot
});
```
