# Cypress Documentation

Cypress is a next generation, open source, front end testing tool built for the modern web.

Cypress makes it simple to:
- setup tests
- write tests
- run tests
- record tests
- debug tests

Cypress can be used to write end-to-end tests, integration tests, unit tests. Simply put, Cypress
can test anything that runs in a browser.

Relevant URLs:
[Home](https://www.cypress.io/),
[Docs](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell),
[NPM](https://www.npmjs.com/package/cypress),
[Plugins](https://docs.cypress.io/plugins/)

Debugging URLs:
[Gitter](https://gitter.im/cypress-io/cypress),
[GitHub Issues](https://github.com/cypress-io/cypress/issues),
[Stack Overflow Channel](https://stackoverflow.com/questions/tagged/cypress)

## Features

Cypress is different:

1. Cypress does not use Selenium
2. Cypress focuses on doing end-to-end testing REALLY well
3. Cypress works on any front-end framework or website
4. Cypress tests are only written in JavaScript
5. Cypress is an all in one package
6. Cypress is made for developers and QA engineers
7. Cypress runs much, much faster

Relevant Features:

- __Time travel__: Cypress takes snapshots as tests run, simply hover over commands to see exactly what happened.
- __Real time reloads__: Cypress automatically reloads whenever you make changes to your tests.
- __Spies, stubs, and clocks__: Verify and control the behavior of functions, server responses, or times
- __Consistent Results__: Architecture doesn't use Selenium or WebDriver. Say hello to fast, consistent,
  and reliable results that are flake-free.
- __Debuggability__: Stop guessing why your tests are failing. Debug directly from familiar tools like
  Chrome DebTools.
- __Automatic waiting__: Never add waits or sleeps to your tests. Cypress automatically waits for commands
  and assertions before moving on. No more async hell.
- __Network Tracffic Control__: Easily control, stup, and test edge cases without involving your server.
  You can stub network traffic however you like.
- __Screenshots and Videos__: View screenshots taken automatically on failure, or videos of your entire
  test suite when run headlessly.

## Getting Started

### Installation

Cypress supports the following operating systems:
- macOS 10.9 and above
- Linux Ubuntu 12.04 and above, Fedora 21, and Debian 8
- Windows 7 and above

Commands:

```bash
cd /your/project/path

# Install Cypress locally as a dev dependency for your project
npm install cypress --save-dev
```

### Opening Cypress

If you used `npm` to install, Cypress has now been installed to your `./node_modules` directory, with
its binary executable accessible from `./node_modules/.bin`.

Launch cypress test runner:
```bash
npx cypress open
```

Can also add an npm script to `package.json`:
```json
{
  "scripts": {
    "cypress:open": "cypress open"
  }
}
```

Now you can invoke the command from your project root:
```bash
npm run cypress:open
```

### Writing Your First Test

Create your first spec file:
```bash
touch ${YOUR_PROJECT}/cypress/integration/sample_spec.js
```

The Cypress test runner will pick up that a new test file was just created.

Example first test:
```js
// sample_spec.js
describe('My First Test', function() {
  it('Does not do much!', function() {
    expect(true).to.equal(true)
  })
})
```

### Write a Real Test

A solid test generally covers 3 phases:

1. Set up application state (Arrange)
2. Take an action (Act)
3. Make an assertion about the resulting application state (Assert)

As an example:
```js

describe('My First Test', function() {
  it('Visits the Kitchen Sink', function() {
    cy.visit('https://example.cypress.io')          // Visit page
    cy.contains('type').click()                     // Click an element
    cy.url().should('include', '/commands/actions') // Assert state
    cy.get('.action-email')                         // Get an input
      .type('fake@email.com')                       //    Type into it
      .should('have.value', 'fake@email.com')       //    Verify that it is updated
  })
})
```

### Testing Application

Follow the following steps:

1. Start application locally
   Should be served on http://localhost:8080 or an equivalent URL
2. Create spec file to test home page.
   ```bash
   touch cypress/integration/home_page_spec.js
   ```
   Add the following code:
   ```js
   // home_page_spec.js
   describe('The Home Page', function() {
     it('successfully loads', function() {
       cy.visit('http://localhost:8080') // change URL to match your dev URL
     })
   })
   ```
3. Configure Cypress
   Open `cypress.json` and set your project root
   ```json
   {
     "baseUrl": "http://localhost:8080"
   }
   ```
   This will automatically prefix `cy.visit()` and `cy.request()` commands with this baseUrl.
   Now the following test code will work:
   ```js
   // home_page_spec.js
   describe('The Home Page', function() {
     it('successfully loads', function() {
       cy.visit('/') // change URL to match your dev URL
     })
   })
   ```

### Testing Strategies

#### Seeding Data

Depending on how application is built it's likely that your web application is going
to be affected and controlled by the server. Typcially these days servers communicate with front end
apps via JSON. Generally one needs to seed the initial database so API requests can return meaningful
data. Cypress enables this via:
- `cy.exec()`: run system commands
- `cy.task()`: run code in Node.js via the `pluginsFile`
- `cy.request()`: make HTTP requests

Example using `beforeEach` to populate test data:
```js
describe('The Home Page', function () {
  beforeEach(function () {
    // reset and seed the database prior to every test
    cy.exec('npm run db:reset && npm run db:seed')
  })

  it('successfully loads', function() {
    cy.visit('/')
  })
})
```

#### Stubbing the server

Another valid approach opposed to seeding and talking to your server is to just bypass it altogether.
Much simpler!

While you'll still receive all of the regular HTML/JS/CSS assets from your server and you'll continue
to `cy.visit()` it in the same way - you can instead stub the JSON responses coming from it.

This means that instead of seeding a database, you can simply force the server responses you want.
This also allows you to build out your application without needing the _contract_ of the server
to exist.

While stubbing is greate, it means that you don't have the guarantees that these response payloads
actually match what the server will send. However, therer are still many valid ways to get around this:
- Generate fixture stub ahead of time
- Write a single e2e test without stubs, and then stub the rest (Hybrid Approach)

#### Logging In

Fully test the login flow, but only once!
For example:
```js
describe('The Login Page', function () {
  beforeEach(function () {
    // reset and seed the database prior to every test
    cy.exec('npm run db:reset && npm run db:seed')

    // seed a user in the DB that we can control from our tests
    // assuming it generates a random password for us
    cy.request('POST', '/test/seed/user', { username: 'jane.lane' })
      .its('body')
      .as('currentUser')
  })

  it('sets auth cookie when logging in via form submission', function () {
    // destructuring assignment of the this.currentUser object
    const { username, password } = this.currentUser
    cy.visit('/login')
    cy.get('input[name=username]').type(username)
    cy.get('input[name=password]').type(`${password}{enter}`) //enter to submit
    cy.url().should('include', '/dashboard')
    cy.getCookie('your-session-cookie').should('exist')
    cy.get('h1').should('contain', 'jane.lane')
  })
})
```

After this, __don't__ use the UI to build up state. It's enormously slow, cumbersome, and unnecessary.
Do the login under the hood with `cy.request`:
```js
describe('The Dashboard Page', function () {
  beforeEach(function () {
    // reset and seed the database prior to every test
    cy.exec('npm run db:reset && npm run db:seed')

    // seed a user in the DB that we can control from our tests
    // assuming it generates a random password for us
    cy.request('POST', '/test/seed/user', { username: 'jane.lane' })
      .its('body')
      .as('currentUser')
  })

  it('logs in programmatically without using the UI', function () {
    // destructuring assignment of the this.currentUser object
    const { username, password } = this.currentUser

    // programmatically log us in without needing the UI
    cy.request('POST', '/login', {
      username,
      password
    })
    cy.visit('/dashboard')
    cy.getCookie('your-session-cookie').should('exist')
    cy.get('h1').should('contain', 'jane.lane')
  })
})
```

## Core Concepts

### Introduction to Cypress

#### Querying Elements

Cypress leverages jQuery's powerful selector engine to help make tests familiar and readable for
modern web developers. However, Cypress is __not__ like jQuery. All Cypress commands run asyncronusly
and automatically retry themselves. This makes cypress robust and immune to dozens of common problems
that occur in other testing tools. This allows cypress tests to run flake-free!

1. Element is found
   ```js
   cy
    // cy.get() looks for '#element', repeating the query until...
    .get('#element')

    // ...it finds the element!
    // You can now work with it by using .then
    .then(($myElement) => {
      doSomething($myElement)
    })
   ```
2. Element is not found
   ```js
   cy
     // cy.get() looks for '#my-nonexistent-selector', repeating the query until...
     // ...it doesn't find the element before its timeout.
     // Cypress halts and fails the test.
     .get('#element-does-not-exist')

     // ...this code is never run...
     .then(($myElement) => {
       doSomething($myElement)
     })
   ```

When you want to interact with a DOM element directly, call `.then()` with a callback function that
receives the element as its first argument. When you want to skip the retry-and-timeout functionality
entirely and perform tradidional synchronous work, use `Cypress.$`.

Query by text content:
```js
// Find an element in the document containing the text 'New Post'
cy.contains('New Post')

// Find an element within '.main' containing the text 'New Post'
cy.get('.main').contains('New Post')
```

When elements are missing, Cypress waits a configurable `timeout` for them to appear before erroring
out. The default timeout is 4 seconds. The timeout can be configured globally or on each call.
```js
// Give this element 10 seconds to appear
cy.get('.my-slow-selector', { timeout: 10000 })
```

#### Chains of Commands

It's very important to understand the mechanism Cypress uses to chain commands together. It manages
a Promise chain on your behalf, with each command yielding a 'subject' to the next command, until
the chain ends or an error is encountered. Note: the developer should not use promises directly.

For example:
```js
cy.get('textarea.post-body')
  .type('This is an excellent post.')

cy.get('form').should('have.class', 'form-horizontal')
```

A new Cypress chain always starts with `cy.[command]`, where what is yielded by the `command` establishes
what other commands can be called next (chained).
- Some methods yield `null` and thus cannot be chained, such as `cy.clearCookies()`
- Some methods, such as `cy.get()` or `cy.contains()` yield a DOM element, allowing further commands
  to be chained onto them like `.click()`

To access the subject directly add a `.then()`:

  ```js
  cy
    // Find the el with id 'some-link'
    .get('#some-link')

    .then(($myElement) => {
      // ...massage the subject with some arbitrary code

      // grab its href property
      const href = $myElement.prop('href')

      // strip out the 'hash' character and everything after it
      return href.replace(/(#.*)/, '')
    })
    .then((href) => {
      // href is now the new subject
      // which we can work with now
    })
  ```
To access pass subject use aliases as follows:
```js
cy
  .get('.my-selector')
  .as('myElement') // sets the alias
  .click()

/* many more actions */

cy
  .get('@myElement') // re-queries the DOM as before (only if necessary)
  .click()
```

It is important to understand that Cypress commands don't do anything at the moment they are invoked,
but rather enqueue themselves to be run later. This allows cypress to wait for events to fire and
elements to load before failing immediately.

```js
it('changes the URL when "awesome" is clicked', function() {
  cy.visit('/my/resource/path') // Nothing happens yet

  cy.get('.awesome-selector')   // Still nothing happening
    .click()                    // Nope, nothing

  cy.url()                      // Nothing to see, yet
    .should('include', '/my/resource/path#awesomeness') // Nada.
})

// Ok, the test function has finished executing...
// We've queued all of these commands and now
// Cypress will begin running them in order!
```

Cypress is built using Promises from [Bluebird](http://bluebirdjs.com/). The Cypress API is not
an exact 1:1 implementation of Promises. They have promise like qualities and yet there are important
differences to be aware of:
- You cannot race or run multiple commands at the same time (in parallel). Cypress guarantees that
  it will execute all of its commands deterministically and identically every time they are run.
  This serial execution guarantees consistency.
- You cannot ‘accidentally’ forget to return or chain a command. Cypress enforces commands run only
  serially by enqueuing commands onto a global singleton. Because there is only a single command
  queue, it's impossible for commands to ever be 'lost'.
- You cannot add a `.catch` error handler to a failed command. If one fails, all remaining commands
  are not run and the test fails

#### Assertions

Assertions describe the desired state of your elements, your objects, and your application. What makes
Cypress unique from other testing tools is that commands automatically retry their assertions. Assertions
are guards. Use your guards to describe what your application should look like, and Cypress will
automatically block, wait, and retry until it reaches that state.

For example:
```js
cy.get('button').click().should('have.class', 'active')
```

Sometimes assertions aren't necessary as Cypress has several built in assertions:
- `cy.visit()` expects the page to send text/html content with a 200 status code.
- `cy.request()` expects the remote server to exist and provide a response.
- `cy.contains()` expects the element with content to eventually exist in the DOM.
- `cy.get()` expects the element to eventually exist in the DOM.
- `.find()` also expects the element to eventually exist in the DOM.
- `.type()` expects the element to eventually be in a typeable state.
- `.click()` expects the element to eventually be in an actionable state.
- `.its()` expects to eventually find a property on the current subject.

The following code has several implicit assertions:
```js
cy.visit('/home')

cy.get('.main-menu')
  .contains('New Project')
  .click()

cy.get('.title')
  .type('My Awesome Project')

cy.get('form')
  .submit()
```

Cypress bundles [Chai](https://docs.cypress.io/guides/references/bundled-tools.html#Chai),
[Chai-jQuery](https://docs.cypress.io/guides/references/bundled-tools.html#Chai-jQuery), and
[Sinon-Chai](https://docs.cypress.io/guides/references/bundled-tools.html#Sinon-Chai) to provide
built-in assertions.

Write assertions with implicit subjects as follows:
```js
// the implicit subject here is the first <tr>
// this asserts that the <tr> has an .active class
cy.get('tbody tr:first').should('have.class', 'active')
```

Write assertions with explicit subjects as follows:
```js
// the explicit subject here is the boolean: true
expect(true).to.be.true
```

Make sure `.should()` callback functions can be executed multiple times without side effect. Cypress
applies retry logic to these functions: if there's a failure, it will be repeatedly rerun the assertions
until the timeout is reached. That means your code should be retry-safe (idempotent).

#### Timeouts

Almost all commands can time out in some way. All assertions, whether they’re the default ones or
whether they’ve been added by you all share the same timeout values.

Timeouts can be set as follows:
```js
// we've modified the timeout which affects default + added assertions
cy
  .get('.mobile-nav', { timeout: 10000 })
  .should('be.visible')
  .and('contain', 'Home')
```

Cypress offers several different timeout values based on the type of command.

### Writing and Organizing Tests

#### Folder Structure

After adding a new project, Cypress will automatically scaffold out a suggested folder structure.
```text
/cypress
  /fixtures
    - example.json

  /integration
    /examples
      - actions.spec.js
      - aliasing.spec.js
      - assertions.spec.js
      - connectors.spec.js
      - cookies.spec.js
      - cypress_api.spec.js
      - files.spec.js
      - local_storage.spec.js
      - location.spec.js
      - misc.spec.js
      - navigation.spec.js
      - network_requests.spec.js
      - querying.spec.js
      - spies_stubs_clocks.spec.js
      - traversal.spec.js
      - utilities.spec.js
      - viewport.spec.js
      - waiting.spec.js
      - window.spec.js

  /screenshots
  /videos

  /plugins
    - index.js

  /support
    - commands.js
    - index.js
```
The default configuration can be modifed in your `cypress.json`.

__Fixtures__ are used as external pieces of static data that can be used for your tests. Fixture
files are located in `cypress/fixtures` by default, but can be configured. Use fixtures by calling
`cy.fixture()`

__Test Files__ are located in `cypress/integration` by default, but can be configured. Test files can
be written as `.js`, `.jsx`, `.coffee`, `.cjsx`. Cypress supports `ES2015` out of the box. To start writing
tests for your app, simply create a new file like `app_spec.js` within your `cypress/integration`
folder. Refersh your Cypress Test Runner and your new file should have appeared in the list.

The __Plugin File__ `cypress/plugins/index.js` will be automatically included by a Cypress child process
before every single spec file it runs. The Plugins API allows you to hook into and extend Cypress behavior.

The __Support File__ `cypress/support/index.js` is automatically included before running each
single spec file. The support file is a great place to put reusable behavior such as Custom Commands
or global overrides that you want applied and available to all spec files.

#### Writing Tests

Cypress is built on top of [Mocha](https://docs.cypress.io/guides/references/bundled-tools.html#Mocha)
and [Chai](https://docs.cypress.io/guides/references/bundled-tools.html#Chai). Both `BDD` and `TDD`
styles are supported.

```js
// -- Start: Our Application Code --
function add (a, b) {
  return a + b
}

function subtract (a, b) {
  return a - b
}

function divide (a, b) {
  return a / b
}

function multiply (a, b) {
  return a * b
}
// -- End: Our Application Code --

// -- Start: Our Cypress Tests --
describe('Unit test our math functions', function() {
  context('math', function() {
    it('can add numbers', function() {
      expect(add(1, 2)).to.eq(3)
    })

    it('can subtract numbers', function() {
      expect(subtract(5, 12)).to.eq(-7)
    })

    specify('can divide numbers', function() {
      expect(divide(27, 9)).to.eq(3)
    })

    specify('can multiply numbers', function() {
      expect(multiply(5, 4)).to.eq(20)
    })
  })
})
// -- End: Our Cypress Tests --
```

Cypress provides Mocha hooks.
```js
describe('Hooks', function() {
  before(function() {
    // runs once before all tests in the block
  })

  after(function() {
    // runs once after all tests in the block
  })

  beforeEach(function() {
    // runs before each test in the block
  })

  afterEach(function() {
    // runs after each test in the block
  })
})
```

To run a specific test use `.only`:
```js
it.only('returns "fizz" when number is multiple of 3', function () {
  numsExpectedToEq([9, 12, 18], 'fizz')
})
```

To skip a test use `.skip`:
```js
it.skip('returns "fizz" when number is multiple of 3', function () {
  numsExpectedToEq([9, 12, 18], 'fizz')
})
```

Tests can be dynamically created as follows:
```js
describe('if your app uses jQuery', function () {
  ['mouseover', 'mouseout', 'mouseenter', 'mouseleave'].forEach((event) => {
    it('triggers event: ' + event, function () {
      // if your app uses jQuery, then we can trigger a jQuery
      // event that causes the event callback to fire
      cy
        .get('#with-jquery').invoke('trigger', event)
        .get('#messages').should('contain', 'the event ' + event + 'was fired')
    })
  })
})
```

When running in interactive mode using `cypress open` Cypress watches the filesystem for changes
to your spec files. Soon after adding or updating a test Cypress will reload it an run all the tests
in that spec file.

Files/Directories watched:

- `cypress.json`
- `cypres.env.json`
- `cypress/integration`
- `cypress/support`
- `cypress/plugins`

Files/Directories __not__ watched:

- Application code
- `node_modules`
- `cypress/fixtures/`

### Retryability

Cypress only retries commands that query the DOM: `cy.get()`, `.find()`, `.contains()`, etc.

Commands are not retried when they could potentially change the state of the application under test.
For example, Cypress will not retry the `.click()` command.

Some commands that cannot be retried still have built-in waiting. For example, the `.click()`
command will not “blindly” send a click event to an element. As described in the “Assertions” section
of `.click()`, it waits to click until the element becomes actionable. Cypress tries to act like a
human user would using the browser:
- Can a user click on the element?
- Is the element invisible?
- Is the element behind another element?
- Does the element have the `disabled` attribute?

For a variety of implementation reasons, Cypress commands only retry the last command before
the assertion.

To prevent flaky tests, whenever you write a longer test, alternate commands with assertions.
```js
it('adds two items', function () {
  cy.visit('/')

  cy.get('.new-todo').type('todo A{enter}')
  cy.get('.todo-list li')         // command
    .should('have.length', 1)     // assertion
    .find('label')                // command
    .should('contain', 'todo A')  // assertion

  cy.get('.new-todo').type('todo B{enter}')
  cy.get('.todo-list li')         // command
    .should('have.length', 2)     // assertion
    .find('label')                // command
    .should('contain', 'todo B')  // assertion
})
```

### Interacting with Elements

Some commands in Cypress are for interacting with the DOM such as: `.click()`, `.dblclick()`, `.check()`.
These commands simulate a user interacting with your application. Under the hood, Cypress fires the events
a browser would fire thus causing your application's event bindings to fire. Prior to issuing any commands,
Cypress checks the current DUM state to ensure the DOM element is ready for action.

Checks and actions performed:
- __Visibility__: Cypress ensures the element is visible
- __Disability__: Cypress checks whether an element's `disabled` property is `true`
- __Animations__: Cypress will automatically determine if an element is animating and wait until it stops.
- __Covering__: Cypress ensures parent element is not covering element.
- __Scrolling__: Before interacting with an element, Cypress scrolls it into view
- __Coordinates__: After the element is actionable, Cypress fires all of the appropriate events and
  corresponding default actions.

It can be difficult to debug problems when elements are not considered actionable by Cypress. Although
you should see a nice error message, nothing beats visually inspecting and poking at the DOM by yourself.
Snapshots do not necessarily represent what that page looked like when the command ran. The only way to
"see" and debug why Cypress though an element was not visible is to use a `debugger` statement. As
of `0.20.0` you can also bind to events.
```js
// break on a debugger before the action command
cy.get('button').debug().click()
```

While the above checks are super helpful at finding situations that would prevent your users from
interacting with elements, sometimes they get in the way. Use the `{ force: true }` option to disable checks.
```js
// force the click and all subsequent events
// to fire even if this element isn't considered 'actionable'
cy.get('button').click({ force: true })
```

### Return Values

When using Cypress's async APIs you cannot assign or work with return values. Commands are enqueued
and run asynchronously.
```js
// ...this won't work...
// nope
const button = cy.get('button')

// nope
const form = cy.get('form')

// nope
button.click()
```

To access what each Cypress command yields use `.then()`:
```js
cy.get('button').then(($btn) => {
  // $btn is the object that the previous
  // command yielded us
})
```

Each nested command has access to the work done in previous commands.
```js
cy.get('button').then(($btn) => {

  // store the button's text
  const txt = $btn.text()

  // submit a form
  cy.get('form').submit()

  // compare the two buttons' text
  // and make sure they are different
  cy.get('button').should(($btn2) => {
    expect($btn2.text()).not.to.eq(txt)
  })
})

// these commands run after all of the
// other previous commands have finished
cy.get(...).find(...).should(...)
```

__debugger__ allows you to stop and examine the current state of the system:
```js
cy.get('button').then(($btn) => {
  // inspect $btn <object>
  debugger

  cy.get('#countries').select('USA').then(($select) => {
    // inspect $select <object>
    debugger

    cy.url().should((url) => {
      // inspect the url <string>
      debugger

      $btn    // is still available
      $select // is still available too
    })
  })
})
```

### Aliases

__Aliases__ allow context to be shared accross closures.
```js
beforeEach(function () {
  // alias the $btn.text() as 'text'
  cy.get('button').invoke('text').as('text')
})

it('has access to text', function () {
  this.text // is now available
})
```

The most common use case for sharing context is when dealing with `cy.fixture()`.
```js
beforeEach(function () {
  // alias the users fixtures
  cy.fixture('users.json').as('users')
})

it('utilize users in some way', function () {
  // access the users property
  const user = this.users[0]

  // make sure the header contains the first
  // user's name
  cy.get('header').should('contain', user.name)
})
```

Aliases can be accessed with `cy.get(@)`:
```js
beforeEach(function () {
  // alias the users fixtures
  cy.fixture('users.json').as('users')
})

it('utilize users in some way', function () {
  // use the special '@' syntax to access aliases
  // which avoids the use of 'this'
  cy.get('@users').then((users) => {
    // access the users argument
    const user = users[0]

    // make sure the header contains the first
    // user's name
    cy.get('header').should('contain', user.name)
  })
})
```

Aliases can also be used with routes. This enables your routes to:
- ensure application makes the intended requests
- wait for server to send response
- access the actual XHR object for assertions

```js
cy.server()
cy.route('POST', '/users', { id: 123 }).as('postUser')
cy.get('form').submit()
cy.wait('@postUser').its('requestBody').should('have.property', 'name', 'Brian')
cy.contains('Successfully created user: Brian')
```

### Conditional Testing

Conditional testing refers to the common programming pattern:

> If X, then Y, else Z

The only way to do conditional testing on the DOM is if you are 100% sure that the state has "settled"
and there is no possible way for it to change.

If you are unable to guarantee that the DOM is stable - don't worry, there are other ways you can do
conditional testing or work around the problems inherent with it.
- Remove the need to ever do conditional testing
- Force your application to behave deterministically
- Check other sources of truth (e.g. a database, server api, url query params)
- Embed data into other places (cookies / local storage)
- Add data to the DOM that you can read off to know how to proceed

### The Test Runner

Cypress runs tests in a unique interactive runner that allow you to see commands as they execute while
also viewing the application under test.

The __command log__ is on the lefthand side. It displays every Cypress command and assertion.
Hovering over a past command restores the application under test to the state that it was in
when the command executed. This allows "time-travel" back to previous states of your application.
By default Cypress keeps 50 tests worth of snapshots and command data for time traveling.

The righthand side of the Test Runner is used to display the __Application Under Test (AUT)__.
The appplication that was navigated to using a `cy.visit()` or any subsequent routing calls made
from the visited application.

The __Selector Playground__ is an interactive feature that helps you determine a unique selector
for an element, see what elements match a selector, and see what element matches a string of text.
Cypress will automatically calculate a unique selector favoring:
- data-cy
- data-test
- data-testid
- id
- class
- tag
- attributes
- nth-child

## Command Line

Invoke the cypress binary:
```bash
./node_modules/.bin/cypress run
npx cypress run
```

### `cypress run`

Runs CYpress tests to completion. By default will run all tests headlessly in the `Electron` browser.

| Option Name            | Description                                                  |
|:-----------------------|:-------------------------------------------------------------|
| --browser, -b          | Specify a different browser to run tests in                  |
| --config, -c           | Specify configuration                                        |
| --env, -e              | Specify environment variables                                |
| --headed               | Display the electron browser instead of running headlessly   |
| --help, -h             | Output usage information                                     |
| --port, -p             | Override default port                                        |
| --project, -P          | Path to a specific project                                   |
| --record               | Whether to record the test run                               |
| --reporter, -r         | Specify a mocha reporter                                     |
| --reporter-options, -o | Specify mocha reporter options                               |
| --spec, -s             | Specify spec files to run                                    |
| --no-exit              | Keep Cypress test runner open after tests in a spec file run |

Examples:

- `cypress run --browser chrome`
- `cypress run --browser chromium`
- `cypress run --config pageLoadTimeout=100000,watchForFileChanges=false`
- `cypress run --env host=api.dev.local`
- `cypress run --env host=api.dev.local,port=4222`
- `cypress run --env flags='{"feature-a":true,"feature-b":false}'`
- `cypress run --headed --no-exit`
- `cypress run --reporter json`
- `cypress run --reporter junit --reporter-options mochaFile=result.xml,toConsole=true`
- `cypress run --spec "cypress/integration/examples/actions.spec.js"`
- `cypress run --spec "cypress/integration/examples/actions.spec.js,cypress/integration/examples/files.spec.js"

### `cypress open`

Opens the Cypress Test Runner in interactive mode.

| Option Name    | Description                                 |
|:---------------|:--------------------------------------------|
| --browser, -b  | Specify a different browser to run tests in |
| --config, -c   | Specify configuration                       |
| --detached, -d | Open cypress in detached mode               |
| --env, -e      | Specify environment variables               |
| --global       | run in global mode                          |
| --help, -h     | Output usage information                    |
| --port, -p     | Override default port                       |
| --project, -P  | Path to a specific project                  |

Examples:

- `cypress open --browser /usr/bin/chromium`
- `cypress open --port 8080`

### `cypress verify`

Verify that Cypress is installed correcty and is executable
```bash
cypress verify
✔  Verified Cypress! /Users/jane/Library/Caches/Cypress/3.0.0/Cypress.app
```

### `cypress version`

Output both the versions of the installed Cypress binary application and the npm module
```bash
cypress version
Cypress package version: 3.0.0
Cypress binary version: 3.0.0
```

### `cypress cache [command]`

Command for managing the global Cypress cache. The Cypress cache applies to all installs of Cypress
across your machine, global or not.

```bash
# Print the path to the Cypress cache folder.
$ cypress cache path
/Users/jane/Library/Caches/Cypress

# Print all existing installed versions of Cypress. The output will be a space delimited list of version numbers.
$ cypress cache list
3.0.0 3.0.1 3.0.2

# Clear the contents of the Cypress cache. This is useful when you want Cypress to clear out all installed
# versions of Cypress that may be cached on your machine. After running this command, you will need to run
# cypress install before running Cypress again.
$ cypress cache clear
```

### Debugging commands

Cypress is built using the `debug` module. That means you can receive helpful debugging output by
running Cypress with this turned on prior to running.

```bash
# Debug open
DEBUG=cypress:* cypress open

# Debug run
DEBUG=cypress:* cypress run

# Run, filtering out a specific module
DEBUG=cypress:server:project cypress run
```

## Module API

You can require Cypress as a Node module from your application under test.

### `cypress.run()`

Runs Cypress tests and resolve with all test results.

```js
const cypress = require('cypress')

cypress.run({
  reporter: 'junit',
  browser: 'chrome',
  config: {
    baseUrl: 'http://localhost:8080',
    chromeWebSecurity: false,
  },
  env: {
    foo: 'bar',
    baz: 'quux',
  }
})
.then((results) => {
  console.log(results)
})
.catch((err) => {
  console.error(err)
})
```

### `cypress.open()`

Open cypress console.

```js
const cypress = require('cypress')

cypress.open({
  browser: 'chrome',
  env: {
    foo: 'bar',
    baz: 'quux',
  }
})
```

## Debugging

Your Cypress test code runs in the same run loop as your application. This means you have acces to
the code running on the page, as well as the things the browser makes available to you, like `document`,
`window`, and `debugger`.

Use `debugger`:
```js
it('let me debug when the after the command executes', function () {
  cy.visit('/my/page/path')

  cy.get('.selector-in-question')
    .then(($selectedElement) => {
      // Debugger is hit after the cy.visit
      // and cy.get command have completed
      debugger
    })
})
```

Cypress exposes a `.debug()` command for debugging.
```js
it('let me debug like a fiend', function() {
  cy.visit('/my/page/path')

  cy.get('.selector-in-question')
    .debug()
})
```

When debugging a failing test, follow these general principles to isolat the problem:
- Look at video recordings and screenshots
- Split large spec files into smaller ones
- Split long tests into smaller tests
- Run the same test using `--browser chrome`. The problem might be isolated to the Electron
  browser. If so, compare run videos of different browsers.

If having an issue during installation of Cypress, clear the Cypress cache.
```bash
cypress cache clear
```

`.pause()` pauses the test allowing developers to inspect the web application, the DOM, the network,
and any storage after each command.
```js
it('adds items', function () {
  cy.pause()
  cy.get('.new-todo')
  // more commands
})
```

To print debug logs do the following:

```bash
# print all debug messages
DEBUG=cypress:* cypress open

# prints messages only from config parsing
DEBUG=cypress:server:config ...
```

Can print debug logs in the browser. Open the browser's DevTools and set a localStorage property.

```bash
# Must restart browser after command
localStorage.debug = 'cypress*'

# to disable debug messages
delete localStorage.debug
```
Reload the browser to see debug messages within the DebTools console.

You can include the plugin `cypress-failed-log` in your tests. This plugin writes the list of
Cypress commands to the terminal as well as a JSON file if a test fails.

## Network Requests

Cypress makes it easy to test the entire lifecycle of Ajax / XHR requests within your application.
Cypress provides you direct access to the XHR objects, enabling you to make assertions about its
properties. Additionally you can even stub and mock a request's response.

Common testing scenarios:
- Asserting on a request's body
- Asserting on a request's url
- Asserting on a request's headers
- Stubbing a response's body
- Stubbing a response's status code
- Stubbing a response's headers
- Delaying a response
- Waiting for a response to happen

Within cypress, you have the ability to choose whether to stub responses or allow them to actually
hit your server. You can also mix and match within the same test by choosing to stub certain requests,
while allowing others to hit your server.

### Don't Stub Responses

Requests that are not stubbed actually reach your server. By not stubbing your responses, you are
writing true `end-to-end` tests. This means you are driving your application the same way a real
user would. When requests are not stubbed, this guarantees that the contract between your client
and server is working correctly. It is a good idea to have `end-to-end` tests around your application's
critical paths: login, signup, or billing.

Benefits:
- Guaranteed to work in production
- Test coverage around server endpoints
- Great for traditional server-side HTML rendering

Downsides of end-to-end:
- Require seeding a database
- Much slower than stubbing
- Harder to test edge cases

Suggested Use:
- Use sparingly
- Great for application critical paths
- Helpful to have one test around the working path of a feature

### Stub Responses

Stubbing responses enables you to control every aspect of the response, including the response
`body`, the `status`, `headers`, and even network `delay`. Stubbing is extremely fast, most resposes
will be returned in less than 20ms.

Benefits:
- Easy control of response bodies, status, and headers
- Can force responses to take longer to simulate network delay
- No code changes to your server or client code
- Fast, < 20ms response times

Downsides:
- No guarantee your stubbed responses match the actual data the server sends
- No test coverage on some server endpoints
- Not as useful if you’re using traditional server side HTML rendering

Suggested Use:
- Use for the vast majority of tests
- Mix and match, typically have one true end-to-end test, and then stub the rest
- Perfect for JSON APIs

### Stubbing

To begin stubbing responses:
1. Start a `cy.server()`
2. Provide a `cy.route()`

```js
cy.server()           // enable response stubbing
cy.route({
  method: 'GET',      // Route all GET requests
  url: '/users/*',    // that have a URL that matches '/users/*'
  response: []        // and force the response to be: []
})
```

### Fixtures

A __fixture__ is a fixed set of data located in a file that is used in your tests. The purpose of
a test fixture is to ensure that there is a well known and fixed environment in which tests are run
so that results are repeatable. Fixtures are accessed within tests by calling the `cy.fixture()` command.

When stubbing a response, you typically need to manage potentially large and complex JSON objects.
Cypress allows you to integrate fixture synax directly into responses.
```js
cy.server()
// we set the response to be the activites.json fixture
cy.route('GET', 'activities/*', 'fixture:activities.json')
```

Alternatively, use aliases:
```js
cy.server()
cy.fixture('activities.json').as('activitiesJSON')
cy.route('GET', 'activities/*', '@activitiesJSON')
```

Cypress automatically scaffolds out a suggested folder structure for organizing your fixtures on
every new project. By default it will create an `example.json` fixture file when you add a project to
Cypress.

> /cypress/fixtures/example.json

Your fixtures can be further organized within additional folders as follows:

> /cypress/fixtures/images/cats.png

To access nested fixtures:
```js
cy.fixture('images/dogs.png') //returns dogs.png as Base64
```

### Waiting

Whether or not you shoose to stub responses, Cypress enables you to declaratively `cy.wait()` for
requests and their responses.
```js
cy.server()
cy.route('activities/*', 'fixture:activities').as('getActivities')
cy.route('messages/*', 'fixture:messages').as('getMessages')

// visit the dashboard, which should make requests that match
// the two routes above
cy.visit('http://localhost:8888/dashboard')

// pass an array of Route Aliases that forces Cypress to wait
// until it sees a response for each request that matches
// each of these aliases
cy.wait(['@getActivities', '@getMessages'])

// these commands will not run until the wait command resolves above
cy.get('h1').should('contain', 'Dashboard')
```

One can also check response data:
```js
cy.server()
cy.route({
  method: 'POST',
  url: '/myApi',
}).as('apiCheck')
cy.visit('/')
cy.wait('@apiCheck').then((xhr) => {
  assert.isNotNull(xhr.response.body.data, '1st API call has data')
})
cy.wait('@apiCheck').then((xhr) => {
  assert.isNotNull(xhr.response.body.data, '2nd API call has data')
})
cy.wait('@apiCheck').then((xhr) => {
  assert.isNotNull(xhr.response.body.data, '3rd API call has data')
})
```

Waiting on aliases routes have big advantages:
1. Tests are more robust with much less flake
2. Failure messages are much more precise
3. You can assert about the underlying XHR object

## Continuous Integration

Cypress can run on continuous integration servers to verify no changes have regressed functionality.
Cypress should run all all CI providers.
- AppVeyor
- Azure DevOps
- BitBucket
- BuildKit
- CircleCI
- CodeShip Basic, Pro
- Concourse
- Docker
- GitLab
- Jenkins
- Semaphore
- Shippable
- Solano
- TravisCI

## Environment Variables

Environment variables are useful when:
- Values are different across developer machines
- Values are different across multiple environments (dev, staging, qa, prod)
- Values change frequently and are highly dynamic

Access environment variables as follows:
```js
cy.request(Cypress.env('EXTERNAL_API')) // points to a dynamic env var
```

There are 5 different places to set environment variables:

1. cypress.json
   ```json
   {
     "projectId": "128076ed-9868-4e98-9cef-98dd8b705d75",
     "env": {
       "foo": "bar",
       "some": "value"
     }
   }
   ```
2. cypress.env.json. Overwrites cypress.json. Can add to .gitignore so each developer can have
   their own values.
3. Export as `CYPRESS_*`. Cypress will strip off the `CYPRESS_` when adding environment variables.
   ```bash
   export cypress_api_server=http://localhost:8888/api/v1/
   ```
4. Pass in the CLI as `--env`
   ```bash
   cypress run --env host=kevin.dev.local,api_server=http://localhost:8888/api/v1
   ```
5. Set an environment variable within plugins

## Stubs, Spies, and Clocks

Cypress automatically bundles and wraps these libraries:
- sinon: provides `cy.stub()` and `cy.spy()`
- lolex: provies `cy.clock()` and `cy.tick()`
- sinon-chai: adds chai assertions for stubs and spies

### Stubs

A stub is a way to modify a function and delegate control over its behavior to you (the programmer).
```js
// create a standalone stub (generally for use in unit test)
cy.stub()

// replace obj.method() with a stubbed function
cy.stub(obj, 'method')

// force obj.method() to return "foo"
cy.stub(obj, 'method').returns('foo')

// force obj.method() when called with "bar" argument to return "foo"
cy.stub(obj, 'method').withArgs('bar').returns('foo')

// force obj.method() to return a promise which resolves to "foo"
cy.stub(obj, 'method').resolves('foo')

// force obj.method() to return a promise rejected with an error
cy.stub(obj, 'method').rejects(new Error('foo'))
```

### Spies

A spy gives you the ability to "spy" on a function, by letting you capture and then assert that
the function was called with the right arguments, or that the function was called a certain number
of times. A spy does not modify the behavior of the function.
```js
cy.spy(obj, 'method')
```

### Assertions

Stubs and spies can be asserted as follows:
```js
const user = {
  getName: (arg) => {
    return arg
  },

  updateEmail: (arg) => {
    return arg
  },

  fail: () => {
    throw new Error('fail whale')
  }
}

// force user.getName() to return "Jane"
cy.stub(user, 'getName').returns('Jane Lane')

// spy on updateEmail but do not change its behavior
cy.spy(user, 'updateEmail')

// spy on fail but do not change its behavior
cy.spy(user, 'fail')

// invoke getName
const name  = user.getName(123)

// invoke updateEmail
const email = user.updateEmail('jane@devs.com')

try {
  // invoke fail
  user.fail()
} catch (e) {

}

expect(name).to.eq('Jane Lane')                            // true
expect(user.getName).to.be.calledOnce                      // true
expect(user.getName).not.to.be.calledTwice                 // true
expect(user.getName).to.be.calledWith(123)
expect(user.getName).to.be.calledWithExactly(123)          // true
expect(user.getName).to.be.calledOn(user)                  // true

expect(email).to.eq('jane@devs.com')                       // true
expect(user.updateEmail).to.be.calledWith('jane@devs.com') // true
expect(user.updateEmail).to.have.returned('jane@devs.com') // true

expect(user.fail).to.have.thrown('Error')
```

### Clock

There are situations when it is useful to control your application's `date` and `time` in order to
override its behavior or avoid slow tests.

`cy.clock()` gives you the ability to control:

- `Date`
- `setTimeout`
- `setInterval`

Once you've enabled `cy.clock` you can control time by ticking it ahead by milliseconds.
```js
cy.clock()
cy.visit('http://localhost:3333')
cy.get('#search').type('foobarbaz')
cy.tick(1000)
```

## Screenshots and Videos

### Screenshots

Cypress comes with the ability to take screenshots, whether you are running in interactive mode
using `cypress open` or run mode using `cypress run`. To manually take a screenshot use `cy.screenshot()`.
Cypress will automatically capture screenshots when a failure happens during runs outside of
interactive mode.

Screen shots are stored in the `screenshotsFolder` which is set to `cypress/screenshots` by default.

### Videos

Cypress also records videos when running from the CLI. This behavior can be turned off by setting
`video` to `false`. Videos are stored in the `videosFolder` which is set to `cypress/videos` by default.

After cypress run completes, Cypress will automatically compress the video in order to save on file
size. By default it compresses to a 32 CRF but this is configurable with the `videoCompression` property.

## Launching Browsers

Whe you run tests in Cypress, Cypress launches a browser for you. This emables
1. Creation of a clean, pristine testing environment
2. Access to the privileged browser APIs for automation

### Electron

The Electron browser has two unique advantages:

1. It can be run headlessly.
2. It comes baked into Cypress and does not need to be installed separately.

By default, when running cypress run from the CLI, Cypress launches Electron headlessly.

### Chrome

Supported Chrome browsers:
- Chrome
- Chromium
- Canary

### Browser Environment

Cypress launches the browser in a way that’s different from a regular browser environment. But it
launches in a way that we believe makes testing more reliable and accessible.

Cypress generates its own isolated profile apart from your normal browser profile. This means things
like history entries, cookies, and 3rd party extensions from your regular browsing session will not
affect your tests in Cypress.

The Cypress launched browser automatically:
- Ignores certificate errors
- Allows blocked pop-ups
- Disables saving passwords
- Disables autofill forms and passwords
- Disables asking to become your primary browser
- Disables device discovery notifications
- Disables language translations
- Disables retoring sessions
- Disables background network traffic
- Disables background and renderer throttling
- Disables prompts requesting permission to use devices like cameras or mics
- Disables user gesture requirements for autoplaying videos

## Web Security

### Limitations

Only one superdomain can be tested for the entirety of a test.
```js
cy.visit('https://www.cypress.io')
cy.visit('https://docs.cypress.io') // yup all good
```
```js
cy.visit('https://apple.com')
cy.visit('https://google.com')      // this will immediately error
```

If your site embeds an `<ifame>` that is a cross-origin frame, Cypress will not be able to automate
or communicate with this `<iframe>`.

Cypress will fail if any HTTPS page links to an HTTP page.

### Disabling Web Security

Setting `chromeWebSecurity` to false allows you to do the following:
- Display insecure content
- Navigate to any superdomain without cross origin errors
- Access cross origin iframes that are embedded in your application.

To disable add the following to `cypress.json`:
```json
{
  "chromeWebSecurity": false
}
```

## Tooling

### Plugins

Plugins enable you to tap into, modify, or extend the internal behavior of Cypress. Normally, as
a user, all of your test code, your application, and Cypress commands are executed in the browser.
Cypress is also a Node.js process that plugins can use.

Use Cases
- Configuration: With plugins, you can programmatically alter the resolved configuration and environment
  variables that come from cypress.json, cypress.env.json, the CLI, or system environment variables.
- Preprocessors: The event file:preprocessor is used to customize how your test code is transpiled and sent
  to the browser. By default Cypress handles CoffeeScript and ES6 using babel and then uses browserify
  to package it for the browser.
- Browser Launching: The event before:browser:launch can be used to modify the launch arguments for
  each particular browser.
- The event task is used in conjunction with the cy.task() command. It allows you to write arbitrary
  code in Node.js to accomplish tasks that aren’t possible in the browser.

To install plugins:
```bash
npm install <plugin name> --save-dev
```

Using a plugin:
Whether you install an npm module, or just want to write your own code - you should do all of that
in `cypress/plugins/index.js`.
```js
// export a function
module.exports = (on, config) => {

  // bind to the event we care about
  on('<event>', (arg1, arg2) => {
    // plugin stuff here
  })
}
```

### Reporters

Cypress is built on top of Mocha. Therefore, any reporter built for Mocha can be used with Cypress.
Can also use multiple reporters with Mocha. This is useful for running in CI. Typically users using
a default spec reporter to show up in `stdout` but then also generat an actual report file for `junit`.

### Visual Testing

Cypress is a _functional_ Test Runner. It drives the web application the way a user would, and
checks if the app functions as expected: if the expected message appears, an element is removed, or
a CSS class is added after the appropriate user action.

Cypress does NOT see how the page actually looks though. For example, Cypress will not see if the
CSS class completed grays out the label element and adds a strike-through line.

Many Cypress plugins attempt _visual testing_. Typically such plugins take an image snapshot of the
entire application under test or a specific element, and then compare the image to a previously
approved baseline image.

Best Practices for visual testing:
- Take a snapshot after you confirm the page is done changing
- Control the timestamp inside the application under test.

  ```js
  const now = new Date(2018, 1, 1)
  cy.clock(now)
  // ... test
  cy.mySnapshotCommand()
  ```
- Use `cy.fixture()` and network mocking to set the application state.
- Use visual diffing to check individual DOM elements rather than the entire page.
- Use Component Testing plugins to test the individual components functionality in addition to
  end-to-end and visual tests.

## Assertions

Cypress bundles Chai assertion library

### BDD Assertions

These chainers are available for BDD assertions (expect, should).

| Chainer                      | Example                                                                             |
|:-----------------------------|:------------------------------------------------------------------------------------|
| not                          | expect(name).to.not.equal('Jane')                                                   |
| deep                         | expect(obj).to.deep.equal({ name: 'Jane' })                                         |
| nested                       | expect({a: {b: ['x', 'y']}}).to.have.nested.property('a.b[1]')                      |
| ordered                      | expect([1, 2]).to.have.ordered.members([1, 2]).but.not.have.ordered.members([2, 1]) |
| any                          | expect(arr).to.have.any.keys('name', 'age')                                         |
| all                          | expect(arr).to.have.all.keys('name', 'age')                                         |
| a(type)                      | expect('test').to.be.a('string')                                                    |
| include(value)               | expect([1,2,3]).to.include(2)                                                       |
| ok                           | expect(undefined).to.not.be.ok                                                      |
| true                         | expect(true).to.be.true                                                             |
| false                        | expect(false).to.be.false                                                           |
| null                         | expect(null).to.be.null                                                             |
| undefined                    | expect(undefined).to.be.undefined                                                   |
| exist                        | expect(myVar).to.exist                                                              |
| empty                        | expect([]).to.be.empty                                                              |
| arguments                    | expect(arguments).to.be.arguments                                                   |
| equal(value)                 | expect(42).to.equal(42)                                                             |
| deep.equal(value)            | expect({ name: 'Jane' }).to.deep.equal({ name: 'Jane' })                            |
| eql(value)                   | expect({ name: 'Jane' }).to.eql({ name: 'Jane' })                                   |
| greaterThan(value)           | expect(10).to.be.greaterThan(5)                                                     |
| least(value)                 | expect(10).to.be.at.least(10)                                                       |
| lessThan(value)              | expect(5).to.be.lessThan(10)                                                        |
| most(value)                  | expect('test').to.have.length.of.at.most(4)                                         |
| within(start, finish)        | expect(7).to.be.within(5,10)                                                        |
| instanceOf(constructor)      | expect([1, 2, 3]).to.be.instanceOf(Array)                                           |
| property(name, [value])      | expect(obj).to.have.property('name')                                                |
| deep.property(name, [value]) | expect(deepObj).to.have.deep.property('tests[1]', 'e2e')                            |
| ownProperty(name)            | expect('test').to.have.ownProperty('length')                                        |
| ownPropertyDescriptor(name)  | expect({a: 1}).to.have.ownPropertyDescriptor('a')                                   |
| match(regexp)                | expect('testing').to.match(/^test/)                                                 |
| string(string)               | expect('testing').to.have.string('test')                                            |
| key(key1, [key2], […])       | expect({ pass: 1, fail: 2 }).to.have.key('pass')                                    |
| throw(constructor)           | expect(fn).to.throw(Error)                                                          |
| respondTo(method)            | expect(obj).to.respondTo('getName')                                                 |
| itself                       | expect(Foo).itself.to.respondTo('bar')                                              |
| satisfy(method)              | expect(1).to.satisfy((num) => { return num > 0 })                                   |
| closeTo(expected, delta)     | expect(1.5).to.be.closeTo(1, 0.5)                                                   |
| members(set)                 | expect([1, 2, 3]).to.include.members([3, 2])                                        |
| oneOf(values)                | expect(2).to.be.oneOf([1,2,3])                                                      |
| change(function)             | expect(fn).to.change(obj, 'val')                                                    |
| increase(function)           | expect(fn).to.increase(obj, 'val')                                                  |
| decrease(function)           | expect(fn).to.decrease(obj, 'val')                                                  |

### TDD Assertions

These assertions are available for TDD assertions (`assert`).

| Assertion                                             | Example                                              |
|:------------------------------------------------------|:-----------------------------------------------------|
| .isOk(object, [message])                              | assert.isOk('everything', 'everything is ok')        |
| .isNotOk(object, [message])                           | assert.isNotOk(false, 'this will pass')              |
| .equal(actual, expected, [message])                   | assert.equal(3, 3, 'vals equal')                     |
| .notEqual(actual, expected, [message])                | assert.notEqual(3, 4, 'vals not equal')              |
| .strictEqual(actual, expected, [message])             | assert.strictEqual(true, true, 'bools strict eq')    |
| .notStrictEqual(actual, expected, [message])          | assert.notStrictEqual(5, '5', 'not strict eq')       |
| .deepEqual(actual, expected, [message])               | assert.deepEqual({ id: '1' }, { id: '1' })           |
| .notDeepEqual(actual, expected, [message])            | assert.notDeepEqual({ id: '1' }, { id: '2' })        |
| .isAbove(valueToCheck, valueToBeAbove, [message])     | assert.isAbove(6, 1, '6 greater than 1')             |
| .isAtLeast(valueToCheck, valueToBeAtLeast, [message]) | assert.isAtLeast(5, 2, '5 gt or eq to 2')            |
| .isBelow(valueToCheck, valueToBeBelow, [message])     | assert.isBelow(3, 6, '3 strict lt 6')                |
| .isAtMost(valueToCheck, valueToBeAtMost, [message])   | assert.isAtMost(4, 4, '4 lt or eq to 4')             |
| .isTrue(value, [message])                             | assert.isTrue(true, 'this val is true')              |
| .isNotTrue(value, [message])                          | assert.isNotTrue('tests are no fun', 'val not true') |
| .isFalse(value, [message])                            | assert.isFalse(false, 'val is false')                |
| .isNotFalse(value, [message])                         | assert.isNotFalse('tests are fun', 'val not false')  |
| .isNull(value, [message])                             | assert.isNull(err, 'there was no error')             |
| .isNotNull(value, [message])                          | assert.isNotNull('hello', 'is not null')             |
| .isNaN(value, [message])                              | assert.isNaN(NaN, 'NaN is NaN')                      |
| .isNotNaN(value, [message])                           | assert.isNotNaN(5, '5 is not NaN')                   |
| .exists(value, [message])                             | assert.exists(5, '5 is not null or undefined')       |
| .notExists(value, [message])                          | assert.notExists(null, 'val is null or undefined')   |
| .isUndefined(value, [message])                        | assert.isUndefined(undefined, 'val is undefined')    |
| .isDefined(value, [message])                          | assert.isDefined('hello', 'val has been defined')    |
| .isFunction(value, [message])                         | assert.isFunction(x => x * x, 'val is func')         |
| .isNotFunction(value, [message])                      | assert.isNotFunction(5, 'val not funct')             |
| .isObject(value, [message])                           | assert.isObject({num: 5}, 'val is object')           |
| .isNotObject(value, [message])                        | assert.isNotObject(3, 'val not object')              |
| .isArray(value, [message])                            | assert.isArray(['unit', 'e2e'], 'val is array')      |
| .isNotArray(value, [message])                         | assert.isNotArray('e2e', 'val not array')            |
| .isString(value, [message])                           | assert.isString('e2e', 'val is string')              |
| .isNotString(value, [message])                        | assert.isNotString(2, 'val not string')              |
| .isNumber(value, [message])                           | assert.isNumber(2, 'val is number')                  |
| .isNotNumber(value, [message])                        | assert.isNotNumber('e2e', 'val not number')          |
| .isFinite(value, [message])                           | assert.isFinite('e2e', 'val is finite')              |
| .isBoolean(value, [message])                          | assert.isBoolean(true, 'val is bool')                |
| .isNotBoolean(value, [message])                       | assert.isNotBoolean('true', 'val not bool')          |
| .typeOf(value, name, [message])                       | assert.typeOf('e2e', 'string', 'val is string')      |
| .notTypeOf(value, name, [message])                    | assert.notTypeOf('e2e', 'number', 'val not number')  |

### Chai-jQuery

These chainers are available when asserting about a DOM object.

| Chainers              | Assertion                                                   |
|:----------------------|:------------------------------------------------------------|
| attr(name, [value])   | expect($el).to.have.attr('foo', 'bar')                      |
| prop(name, [value])   | expect($el).to.have.prop('disabled', false)                 |
| css(name, [value])    | expect($el).to.have.css('background-color', 'rgb(0, 0, 0)') |
| data(name, [value])   | expect($el).to.have.data('foo', 'bar')                      |
| class(className)      | expect($el).to.have.class('foo')                            |
| id(id)                | expect($el).to.have.id('foo')                               |
| html(html)            | expect($el).to.have.html('I love testing')                  |
| text(text)            | expect($el).to.have.text('I love testing')                  |
| value(value)          | expect($el).to.have.value('test@dev.com')                   |
| visible               | expect($el).to.be.visible                                   |
| hidden                | expect($el).to.be.hidden                                    |
| selected              | expect($option).not.to.be.selected                          |
| checked               | expect($input).not.to.be.checked                            |
| focus[ed]             | expect($input).not.to.be.focused                            |
| enabled               | expect($input).to.be.enabled                                |
| disabled              | expect($input).to.be.disabled                               |
| empty                 | expect($el).not.to.be.empty                                 |
| exist                 | expect($nonexistent).not.to.exist                           |
| match(selector)       | expect($emptyEl).to.match(':empty')                         |
| contain(text)         | expect($el).to.contain('text')                              |
| descendants(selector) | expect($el).to.have.descendants('div')                      |

### Sinon-Chai

| Sinon.JS property/method | Assertion                                                             |
|:-------------------------|:----------------------------------------------------------------------|
| called                   | expect(spy).to.be.called                                              |
| callCount                | expect(spy).to.have.callCount(n)                                      |
| calledOnce               | expect(spy).to.be.calledOnce                                          |
| calledTwice              | expect(spy).to.be.calledTwice                                         |
| calledThrice             | expect(spy).to.be.calledThrice                                        |
| calledBefore             | expect(spy1).to.be.calledBefore(spy2)                                 |
| calledAfter              | expect(spy1).to.be.calledAfter(spy2)                                  |
| calledWithNew            | expect(spy).to.be.calledWithNew                                       |
| alwaysCalledWithNew      | expect(spy).to.always.be.calledWithNew                                |
| calledOn                 | expect(spy).to.be.calledOn(context)                                   |
| alwaysCalledOn           | expect(spy).to.always.be.calledOn(context)                            |
| calledWith               | expect(spy).to.be.calledWith(...args)                                 |
| alwaysCalledWith         | expect(spy).to.always.be.calledWith(...args)                          |
| calledWithExactly        | expect(spy).to.be.calledWithExactly(...args)                          |
| alwaysCalledWithExactly  | expect(spy).to.always.be.calledWithExactly(...args)                   |
| calledWithMatch          | expect(spy).to.be.calledWithMatch(...args)                            |
| alwaysCalledWithMatch    | expect(spy).to.always.be.calledWithMatch(...args)                     |
| returned                 | expect(spy).to.have.returned(returnVal)                               |
| alwaysReturned           | expect(spy).to.have.always.returned(returnVal)                        |
| threw                    | expect(spy).to.have.thrown(errorObjOrErrorTypeStringOrNothing)        |
| alwaysThrew              | expect(spy).to.have.always.thrown(errorObjOrErrorTypeStringOrNothing) |

### should callback

If built-in assertions are not enough, you can easily write your own assertion function and pass it
as a callback to the `.should()`` command. Cypress will automatically retry the callback function until
it passes or the command times out.
```js
cy.get('div')
  .should(($div) => {
    expect($div).to.have.length(1)

    const className = $div[0].className

    // className will be a string like "main-abc123 heading-xyz987"
    expect(className).to.match(/heading-/)
  })
```

## Configuration

`cypress.json` configuration file has the following options.

| Option                     | Default                  | Description                                                                                                                                                                                                                                                                            |
|:---------------------------|:-------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| baseUrl                    | null                     | Url used as prefix for cy.visit() or cy.request() command’s url                                                                                                                                                                                                                        |
| env                        | {}                       | Any values to be set as environment variables                                                                                                                                                                                                                                          |
| ignoreTestFiles            | *.hot-update.js          | A String or Array of glob patterns used to ignore test files that would otherwise be shown in your list of tests. Cypress uses minimatch with the options: {dot: true, matchBase: true}. We suggest using http://globtester.com to test what files would match.                        |
| numTestsKeptInMemory       | 50                       | The number of tests for which snapshots and command data are kept in memory. Reduce this number if you are experiencing high memory consumption in your browser during a test run.                                                                                                     |
| port                       | null                     | Port used to host Cypress. Normally this is a randomly generated port                                                                                                                                                                                                                  |
| reporter                   | spec                     | The reporter used during cypress run                                                                                                                                                                                                                                                   |
| reporterOptions            | null                     | The reporter options used. Supported options depend on the reporter.                                                                                                                                                                                                                   |
| testFiles                  | **/*.*                   | A String glob pattern of the test files to load                                                                                                                                                                                                                                        |
| watchForFileChanges        | true                     | Whether Cypress will watch and restart tests on test file changes                                                                                                                                                                                                                      |
| defaultCommandTimeout      | 4000                     | Time, in milliseconds, to wait until most DOM based commands are considered timed out                                                                                                                                                                                                  |
| execTimeout                | 60000                    | Time, in milliseconds, to wait for a system command to finish executing during a cy.exec() command                                                                                                                                                                                     |
| taskTimeout                | 60000                    | Time, in milliseconds, to wait for a task to finish executing during a cy.task() command                                                                                                                                                                                               |
| pageLoadTimeout            | 60000                    | Time, in milliseconds, to wait for page transition events or cy.visit(), cy.go(), cy.reload() commands to fire their page load events. Network requests are limited by the underlying operating system, and may still time out if this value is increased.                             |
| requestTimeout             | 5000                     | Time, in milliseconds, to wait for an XHR request to go out in a cy.wait() command                                                                                                                                                                                                     |
| responseTimeout            | 30000                    | Time, in milliseconds, to wait until a response in a cy.request(), cy.wait(), cy.fixture(), cy.getCookie(), cy.getCookies(), cy.setCookie(), cy.clearCookie(), cy.clearCookies(), and cy.screenshot() commands                                                                         |
| fileServerFolder           | root project folder      | Path to folder where application files will attempt to be served from                                                                                                                                                                                                                  |
| fixturesFolder             | cypress/fixtures         | Path to folder containing fixture files (Pass false to disable)                                                                                                                                                                                                                        |
| integrationFolder          | cypress/integration      | Path to folder containing integration test files                                                                                                                                                                                                                                       |
| pluginsFile                | cypress/plugins/index.js | Path to plugins file. (Pass false to disable)                                                                                                                                                                                                                                          |
| screenshotsFolder          | cypress/screenshots      | Path to folder where screenshots will be saved from cy.screenshot() command or after a test fails during cypress run                                                                                                                                                                   |
| supportFile                | cypress/support/index.js | Path to file to load before test files load. This file is compiled and bundled. (Pass false to disable)                                                                                                                                                                                |
| videosFolder               | cypress/videos           | Path to folder where videos will be saved during cypress run                                                                                                                                                                                                                           |
| trashAssetsBeforeRuns      | true                     | Whether Cypress will trash assets within the screenshotsFolder and videosFolder before tests run with cypress run.                                                                                                                                                                     |
| videoCompression           | 32                       | The quality setting for the video compression, in Constant Rate Factor (CRF). The value can be false to disable compression or a value between 0 and 51, where a lower value results in better quality (at the expense of a higher file size).                                         |
| videosFolder               | cypress/videos           | Where Cypress will automatically save the video of the test run when tests run with cypress run.                                                                                                                                                                                       |
| video                      | true                     | Whether Cypress will capture a video of the tests run with cypress run.                                                                                                                                                                                                                |
| videoUploadOnPasses        | true                     | Whether Cypress will process, compress, and upload videos to the Dashboard even when all tests in a spec file are passing. This only applies when recording your runs to the Dashboard. Turn this off if you’d like to only upload the spec file’s video when there are failing tests. |
| chromeWebSecurity          | true                     | Whether Chrome Web Security for same-origin policy and insecure mixed content is enabled. Read more about this here                                                                                                                                                                    |
| userAgent                  | null                     | Enables you to override the default user agent the browser sends in all request headers. User agent values are typically used by servers to help identify the operating system, browser, and browser version. See User-Agent MDN Documentation for example user agent values.          |
| blacklistHosts             | null                     | A String or Array of hosts that you wish to block traffic for. Please read the notes for examples on using this.                                                                                                                                                                       |
| modifyObstructiveCode      | true                     | Whether Cypress will search for and replace obstructive JS code in .js or .html files. Please read the notes for more information on this setting.                                                                                                                                     |
| viewportHeight             | 660                      | Default height in pixels for the application under tests’ viewport (Override with cy.viewport() command)                                                                                                                                                                               |
| viewportWidth              | 1000                     | Default width in pixels for the application under tests’ viewport. (Override with cy.viewport() command)                                                                                                                                                                               |
| animationDistanceThreshold | 5                        | The distance in pixels an element must exceed over time to be considered animating                                                                                                                                                                                                     |
| waitForAnimations          | true                     | Whether to wait for elements to finish animating before executing commands                                                                                                                                                                                                             |

Configuration variables can be overridden on the command line using the `--config` flag.
```bash
cypress run --config integrationFolder=tests,fixturesFolder=false
```

You can use environment variables to override configuration values. This is especially useful in CI or when
working locally. Cypress will automatically strip off the `CYPRESS_`, camelcase any keys and convert
values into number of Boolean. For instance both exports do the same thing:
```bash
export CYPRESS_pageLoadTimeout=100000
export CYPRESS_PAGE_LOAD_TIMEOUT=100000
```

Can also overrid configuration values within your test using `Cypress.config()`.
```js
Cypress.config('pageLoadTimeout', 100000)
Cypress.config('pageLoadTimeout') // => 100000
```

## Best Practices

- Test specs in isolation, programmatically log into your application, and take control of your
  application's state

- Use `data-cy` attributes to provide context to your selectors and insulate them from CSS or JS changes.
  Targeting the element above by tag, class, or id is very volatile and highly subject to change. Instead
  adding the `data-cy` attribute to the element gives you a targeted selector that's only used for testing.

- Use closures to access and store what Commands yield you.

- Never test applications that you don't control.
    * They are liable to change at any moment which will break the tests
    * They may do A/B testing which makes it impossible to get consistent results
    * They may detect you are a script and block your access
    * They may have security features enabled whcih prevent Cypress from working

  Cypress is not a general purpose web automation tool. It is poorly suited for scripting
  live, production websites, not under your control.

- Tests should always be able to run independently from one another and still pass. Should be
  able to put a `.only` on any test and have it run.

- Clean up state before tests run. State reset should go before each test.

- Use route aliases or assertions to guard Cypress from processeding until an explicit condition
  is met. You should almost never need to use `cy.wait()`

- Start a separate web server prior to running Cypress in the Test Runner or headless mode.
  Do NOT start backend web server from within Cypress.

## `cy` API

### Navigation

#### go

Navigate back or forward to the previous or next URL in the browser’s history.

```js
cy.go('back')
```

#### reload

Reload the page.

```js
cy.reload()
```

#### url

Get the current URL of the page that is currently active.

```js
cy.get('#user-edit a').click()
cy.url().should('include', '/users/1/edit') // => true
cy.url().should('eq', 'http://localhost:8000/users/1/edit') // => true
```

#### visit

Visit a remote URL.

```js
cy.visit('http://localhost:3000')    // Yields the window of the remote page
```

### Remote Objects

#### document

Get the window.document of the page that is currently active.

```js
cy.document()     // yield the window.document object
cy.document().then((doc) => {
  // work with document element
})
```
#### hash

Get the current URL hash of the page that is currently active.

```js
cy.hash()     // Get the url hash
```

#### location

Get the global window.location object of the page that is currently active.

```js
cy.visit('http://localhost:8000/app/index.html?q=dan#/users/123/edit')

cy.location().should((loc) => {
  expect(loc.hash).to.eq('#/users/123/edit')
  expect(loc.host).to.eq('localhost:8000')
  expect(loc.hostname).to.eq('localhost')
  expect(loc.href).to.eq('http://localhost:8000/app/index.html?q=dan#/users/123/edit')
  expect(loc.origin).to.eq('http://localhost:8000')
  expect(loc.pathname).to.eq('/app/index.html')
  expect(loc.port).to.eq('8000')
  expect(loc.protocol).to.eq('http:')
  expect(loc.search).to.eq('?q=dan')
  expect(loc.toString()).to.eq('http://localhost:8000/app/index.html?q=brian#/users/123/edit')
})
```
#### title

Get the `document.title` property of the page that is currently active.

```js
cy.title().should('eq', 'My Awesome Application')
```

#### window

Get the window object of the page that is currently active.

```js
cy.visit('http://localhost:8080/app')
cy.window().then((win) => {
  // win is the remote window
  // of the page at: http://localhost:8080/app
})
```

### Querying Elements

#### as

Assign an alias for later use. Reference the alias later within a `cy.get()` or `cy.wait()`.

```js
cy.get('.main-nav').find('li').first().as('firstNav') // Alias first 'li' as @firstNav
cy.route('PUT', 'users', 'fx:user').as('putUser')     // Alias that route as @putUser
cy.stub(api, 'onUnauth').as('unauth')                 // Alias that stub as @unauth
cy.spy(win, 'fetch').as('winFetch')
```
#### children

Get the children of each DOM element within a set of DOM elements.

```js
cy.get('nav').children()     // Yield children of nav
```

#### closest

Get the first DOM element that matches the selector (whether it be itself or one of its ancestors).

```js
cy.get('td').closest('.filled') // Yield closest el with class '.filled'
```

#### contains

Get the DOM element containing the text. DOM elements can contain more than the desired text and
still match. Additionally, Cypress prefers some DOM elements over the deepest element found.

```js
cy.get('.nav').contains('About')  // Yield el in .nav containing 'About'
cy.contains('Hello')              // Yield first el in document containing 'Hello'
```

#### each

Iterate through an array like structure (arrays or objects with a length property).

```js
cy.get('ul>li').each(function () {...}) // Iterate through each 'li'
cy.getCookies().each(function () {...}) // Iterate through each cookie
```

#### end

End a chain of commands.

```js
cy.contains('ul').end()   // Yield 'null' instead of 'ul' element
```

#### eq

Get A DOM element at a specific index in an array of elements.

```js
cy.get('tbody>tr').eq(0)    // Yield first 'tr' in 'tbody'
cy.get('ul>li').eq(4)       // Yield fifth 'li' in 'ul'
```
#### filter

Get the DOM elements that match a specific selector.

```js
cy.get('td').filter('.users') // Yield all el's with class '.users'
```

#### find

Get the descendent DOM elements of a specific selector.

```js
cy.get('.article').find('footer') // Yield 'footer' within '.article'
```

#### first

Get the first DOM element within a set of DOM elements.

```js
cy.get('nav a').first()     // Yield first link in nav
```

#### focused

Get the DOM element that is currently focused.

```js
cy.focused()    // Yields the element currently in focus
```

#### get

Get one or more DOM elements by selector or alias.

```js
cy.get('.list > li') // Yield the <li>'s in .list
```
#### its

Get a property’s value on the previously yielded subject.

```js
cy.wrap({ width: '50' }).its('width') // Get the 'width' property
cy.window().its('angular')          // Get the 'angular' property
```

#### last

Get the last DOM element within a set of DOM elements.

```js
cy.get('nav a').last()     // Yield last link in nav
```

#### next

Get the immediately following sibling of each DOM element within a set of DOM elements.

```js
cy.get('nav a:first').next() // Yield next link in nav
```

#### nextAll

Get all following siblings of each DOM element in a set of matched DOM elements.

```js
cy.get('.active').nextAll() // Yield all links next to `.active`
```

#### nextUntil

Get all following siblings of each DOM element in a set of matched DOM elements up to, but not
including, the element provided.

```js
cy.get('div').nextUntil('.warning') // Yield siblings after 'div' until '.warning'
```

#### not

Filter DOM element(s) from a set of DOM elements.

```js
cy.get('input').not('.required') // Yield all inputs without class '.required'
```

#### parent

Get the parent DOM element of a set of DOM elements.

```js
cy.get('header').parent() // Yield parent el of `header`
```

#### parents

Get the parent DOM elements of a set of DOM elements.

```js
cy.get('aside').parents()  // Yield parents of aside
```

#### parentsUntil

Get all ancestors of each DOM element in a set of matched DOM elements up to, but not including, the
element provided.

```js
cy.get('p').parentsUntil('.article') // Yield parents of 'p' until '.article'
```

#### prev

Get the immediately preceding sibling of each element in a set of the elements.

```js
cy.get('tr.highlight').prev() // Yield previous 'tr'
```

#### root

Get the root DOM element.

```js
cy.root()   // Yield root element <html>
cy.get('nav').within(($nav) => {
  cy.root()  // Yield root element <nav>
})
```

#### siblings

Get sibling DOM elements.

```js
cy.get('td').siblings()           // Yield all td's siblings
cy.get('li').siblings('.active')  // Yield all li's siblings with class '.active'
```

#### spread

Expand an array into multiple arguments.

```js
cy.getCookies().spread(() => {}) // Yield all cookies
```

#### within

Scopes all subsequent cy commands to within this element. Useful when working within a particular group of
elements such as a `<form>`.

```js
cy.get('form').within(($form) => {
  // cy.get() will only search for elements within form,
  // not within the entire document
  cy.get('input[name="email"]').type('john.doe@email.com')
  cy.get('input[name="password"]').type('password')
  cy.root().submit()
})
```

### Action Commands

#### blur

Blur a focused element.

```js
cy.get('[type="email"]').type('me@email.com').blur() // Blur email input
cy.get('[tabindex="1"]').focus().blur()
```

#### check

Check checkbox(es) or radio(es)

```js
cy.get('[type="checkbox"]').check()       // Check checkbox element
cy.get('[type="radio"]').first().check()
```

#### clear

Clear the value of an input or textarea.

```js
cy.get('[type="text"]').clear()        // Clear text input
cy.get('textarea').type('Hi!').clear() // Clear textarea
cy.focused().clear()
```

#### click

Click a DOM element.

```js
cy.get('button').click()          // Click on button
cy.focused().click()              // Click on el with focus
cy.contains('Welcome').click()    // Click on first el containing 'Welcome'
```

#### dbclick

Double-click a DOM element.

```js
cy.get('button').dblclick()          // Double click on button
cy.focused().dblclick()              // Double click on el with focus
cy.contains('Welcome').dblclick()    // Double click on first el containing 'Welcome'
```
#### focus

Focus on a DOM element.

```js
cy.get('input').first().focus() // Focus on the first input
```
#### screenshot

Take a screenshot of the application under test and, optionally, the Cypress Command Log.

```js
cy.screenshot()
cy.get('.post').screenshot()
```
#### scrollIntoView

Scroll an element into view.

```js
cy.get('footer').scrollIntoView() // Scrolls 'footer' into view
```

#### scrollTo

Scroll to a specific position.

```js
cy.scrollTo(0, 500)                     // Scroll the window 500px down
cy.get('.sidebar').scrollTo('bottom')   // Scroll 'sidebar' to its bottom
```

#### select

Select an `<option>` within a `<select>`.

```js
cy.get('select').select('user-1') // Select the 'user-1' option
```
#### submit

Submit a form.

```js
cy.get('form').submit()   // Submit a form
```

#### trigger

Trigger an event on a DOM element.

```js
cy.get('a').trigger('mousedown') // Trigger mousedown event on link
```

#### type

Type into a DOM element.

```js
cy.get('input').type('Hello, World') // Type 'Hello, World' into the 'input'
```

#### uncheck

Uncheck checkbox(es).

```js
cy.get('[type="checkbox"]').uncheck()   // Unchecks checkbox element
```

#### viewport

Control the size and orientation of the screen for your application.

```js
cy.viewport(550, 750)    // Set viewport to 550px x 750px
cy.viewport('iphone-6')  // Set viewport to 375px x 667px
```

#### wait

Wait for a number of milliseconds or wait for an aliased resource to resolve before moving on to the next command.

```js
cy.wait(500)
cy.wait('@getProfile')
```

### Assertions

#### and

Create an assertion.

```js
cy.get('.err').should('be.empty').and('be.hidden') // Assert '.err' is empty & hidden
cy.contains('Login').and('be.visible')             // Assert el is visible
cy.wrap({ foo: 'bar' })
  .should('have.property', 'foo')                  // Assert 'foo' property exists
  .and('eq', 'bar')
```
#### should

Create an assertion. Assertions are automatically retried until they pass or time out.

```js
cy.get('.error').should('be.empty')                    // Assert that '.error' is empty
cy.contains('Login').should('be.visible')              // Assert that el is visible
cy.wrap({ foo: 'bar' }).its('foo').should('eq', 'bar') // Assert the 'foo' property equals 'bar'
```

### Execution

#### exec

Execute a system command.

```js
cy.exec('npm run build')
```

#### invoke

Invoke a function on the previously yielded subject.

```js
cy.wrap({ animate: fn }).invoke('animate') // Invoke the 'animate' function
cy.get('.modal').invoke('show')          // Invoke the jQuery 'show' function
```

#### task

Execute code in Node.js via the task plugin event.

```js
// in test
cy.task('log', 'This will be output to the terminal')
```

```js
// in plugins file
on('task', {
  log (message) {
    console.log(message)

    return null
  }
})
```

#### then

Enables you to work with the subject yielded from the previous command.

```js
cy.get('.nav').then(($nav) => {})  // Yields .nav as first arg
cy.location().then((loc) => {})   // Yields location object as first arg
```

### Route Stubbing

#### fixture

Load a fixed set of data located in a file.

```js
cy.fixture('users').as('usersJson')  // load data from users.json
cy.fixture('logo.png').then((logo) => {
  // load data from logo.png
})
```

#### request

Make an HTTP request.

```js
cy.request('http://dev.local/seed')
```

#### route

Use cy.route() to manage the behavior of network requests.

```js
cy.server()
cy.route('/beetles', []).as('getBeetles')
cy.get('#search').type('Weevil')

// wait for the first response to finish
cy.wait('@getBeetles')

// the results should be empty because we
// responded with an empty array first
cy.get('#beetle-results').should('be.empty')

// now re-define the /beetles response
cy.route('/beetles', [{ name: 'Geotrupidae' }])

cy.get('#search').type('Geotrupidae')

// now when we wait for 'getBeetles' again, Cypress will
// automatically know to wait for the 2nd response
cy.wait('@getBeetles')

// we responded with 1 beetle item so now we should
// have one result
cy.get('#beetle-results').should('have.length', 1)
```

#### server

Start a server to begin routing responses to cy.route() and cy.request().

```js
cy.server(options)
```

### Sinon Stubs and Spies

#### spy

Wrap a method in a spy in order to record calls to and arguments of the function.

```js
// assume App.start calls util.addListeners
cy.spy(util, 'addListeners')
App.start()
expect(util.addListeners).to.be.called
```

#### stub

Replace a function, record its usage and control its behavior.

```js
// assume App.start calls util.addListeners
let listenersAdded = false

cy.stub(util, 'addListeners', () => {
  listenersAdded = true
})

App.start()
expect(listenersAdded).to.be.true
```

### Lolex Clocks

#### clock

`cy.clock()` overrides native global functions related to time allowing them to be controlled synchronously
via `cy.tick()` or the yielded clock object. This includes controlling:
- setTimeout
- clearTimeout
- setInterval
- clearInterval
- Date Objects

The clock starts at the unix epoch (timestamp of 0). This means that when you instantiate new Date in
your application, it will have a time of January 1st, 1970.

```js
cy.clock()
```

#### tick

Move time after overriding a native time function with `cy.clock()`.

```js
cy.clock()
cy.visit('/index.html')
cy.window().invoke('addIntro')
cy.tick(500)
cy.get('#header').should('have.text', 'Hello, World')
```

### Debugging

#### debug

Set a debugger and log what the previous command yields.

```js
cy.debug().getCookie('app') // Pause to debug at beginning of commands
```

#### log

Print a message to the Cypress Command Log.

```js
cy.log('created new user')
```

#### pause

Stop cy commands from running and allow interaction with the application under test. You can then
“resume” running all commands or choose to step through the “next” commands from the Command Log.

```js
cy.pause().getCookie('app') // Pause at the beginning of commands
cy.get('nav').pause()       // Pause after the 'get' commands yield
```

### Cookies, Local Storage

#### clearCookie

Clear a specific browser cookie.

```js
cy.clearCookie('authId')    // clear the 'authId' cookie
```

#### clearLocalStorage

Clear data in local storage for current domain and subdomain.

```js
cy.clearLocalStorage()  // clear all local storage
```
#### getCookie

Get a browser cookie by its name.

```js
cy.getCookie('auth_key')     // Get cookie with name 'auth_key'
```
#### setCookie

Set a browser cookie.

```js
cy.setCookie('auth_key', '123key') // Set the 'auth_key' cookie to '123key'
```

### File IO

#### writeFile

Write to a file with the specified contents.

```js
cy.writeFile('menu.json')
```
#### readFile

Read a file and yield its contents.

```js
cy.readFile('menu.json')
```

## Utilities

### _

Cypress automatically includes lodash and exposes it as `Cypress._`.

```js
// set local reference to lodash and jquery
const { _, $ } = Cypress

cy.get('li').then(($li) => {
  // use the _.each function
  _.each($li.get(), (el, i) => {

    // use $(...) to wrap the DOM element
    // into a jQuery object
    expect($(el).parent()).to.match('ul')
  })
})
```

### $

Cypress automatically includes jQuery and exposes it as `Cypress.$`.

```js
const $li = Cypress.$('ul li:first')

cy.wrap($li)
  .should('not.have.class', 'active')
  .click()
  .should('have.class', 'active')
```

### Blob

Cypress automatically includes a Blob library and exposes it as `Cypress.Blob`.

```js
// programmatically upload the logo
cy.fixture('images/logo.png').as('logo')
cy.get('input[type=file]').then(function($input) {

  // convert the logo base64 string to a blob
  return Cypress.Blob.base64StringToBlob(this.logo, 'image/png')
    .then((blob) => {

      // pass the blob to the fileupload jQuery plugin
      // used in your application's code
      // which initiates a programmatic upload
      $input.fileupload('add', { files: blob })
    })
})
```

### minimatch

Cypress automatically includes minimatch and exposes it as `Cypress.minimatch`.

```js
// test that the glob you're writing matches the XHR's url

// returns true
Cypress.minimatch('/users/1/comments', '/users/*/comments', {
  matchBase: true
})

// returns false
Cypress.minimatch('/users/1/comments/2', '/users/*/comments', {
  matchBase: true
})
```

### moment

Cypress automatically includes moment.js and exposes it as `Cypress.moment`.

```js
const todaysDate = Cypress.moment().format('MMM DD, YYYY')

cy.get('span').should('contain', 'Order shipped on: ' + todaysDate)
```

### Promise

Cypress automatically includes Bluebird and exposes it as `Cypress.Promise`.

```js
cy.get('button').then(($button) => {
  return new Cypress.Promise((resolve, reject) => {
    // do something custom here
  })
})
```

## Cypress API

### Commands

Cypress comes with its own API for creating custom commands and overwriting existing commands. The
built in Cypress commands use the very same API that’s defined below.

```js
Cypress.Commands.add('login', (email, pw) => {})
Cypress.Commands.overwrite('visit', (orig, url, options) => {})
```

### Cookies

`Cookies.preserveOnce()` and `Cookies.defaults()` enable you to control Cypress’ cookie behavior.

`Cookies.debug()` enables you to log out whenever any cookies are modified.

Cypress automatically clears all cookies before each test to prevent state from building up.

You can take advantage of `Cypress.Cookies.preserveOnce()` or even whitelist cookies by their name
to preserve values across multiple tests. This enables you to preserve sessions through several tests.

```js
Cypress.Cookies.debug(true) // now Cypress will log out when it alters cookies

cy.clearCookie('foo')
cy.setCookie('foo', 'bar')
```

### Screenshot

The Screenshot API allows you set defaults for how screenshots are captured during `.screenshot` and
automatic screenshots taken during test failures. You can set defaults for the following:
- Which parts of the screen to capture.
- Whether to scale your application under test in the screenshot.
- Whether to disable JavaScript timers and CSS animations when taking the screenshot.
- Whether to automatically take screenshots when there are run failures.
- Which, if any, elements to black out when taking the screenshot.
- Whether to wait for the Command Log to synchronize before taking the screenshot.

```js
Cypress.Screenshot.defaults(options)
```

### SelectorPlayground

The Selector Playground exposes APIs that enable you to:

- Change the default selector strategy
- Override the selectors that are returned per element

```js
Cypress.SelectorPlayground.defaults({
  selectorPriority: ['id', 'class', 'attributes']
})
```

### Server

Permanently change the default options for all `cy.server()` instances

```js
// pass anything here you'd normally pass to cy.server().
Cypress.Server.defaults({
  delay: 500,
  force404: false,
  whitelist: (xhr) => {
    // handle custom logic for whitelisting
  }
})
```

### arch

Cypress.arch returns you the CPU architecture name of the underlying OS, as returned from Node’s `os.arch()`.

```js
it('has expected CPU architecture', function () {
  expect(Cypress.arch).to.be.oneOf(['x64', 'ia32'])
})
```

### browser

`Cypress.browser` returns you properties of the browser.

```js
it('log browser info', function() {
  console.log(Cypress.browser)
  // {
  //   name: 'chrome',
  //   displayName: 'Chrome',
  //   version: '67.123.456.90',
  //   majorVersion: '67',
  //   path: '/path/to/browser',
  //   isHeaded: true,
  //   isHeadless: false
  // }
})
```

### config

get and set configuration options in your tests.

```js
Cypress.config('viewportWidth', 800)
Cypress.config('viewportWidth') // => 800
```

### dom

`Cypress.dom.method()` is a collection of DOM related helper methods.

```js
cy.get('button').then(($el) => {
  Cypress.dom.isDetached($el) // false
})
```

### env

get and set environment variables in your tests.

```js
Cypress.env('host', 'http://server.dev.local')
Cypress.env('host') // => http://server.dev.local
```

### isCy

`Cypress.isCy()` checks if a variable is a valid instance of cy or a cy chainable.

This utility may be useful when writing plugins for Cypress and you want to determine if a value is
a valid Cypress chainable.

```js
Cypress.isCy(cy) // true

const chainer = cy.wrap().then(() => {
  Cypress.isCy(chainer) // true
})

Cypress.isCy(undefined) // false

Cypress.isCy(() => {}) // false
```

### log

This is the internal API for controlling what get’s printed to the Command Log.

```js
Cypress.Commands.add('myCustomCommand', (arg1, arg2) => {
  const log = Cypress.log({
    consoleProps: () => {
      // return an object literal which will
      // be printed to the dev tools console
      // on click
      return {
        'Some': 'values',
        'For': 'debugging'
      }
    }
  })
})
```

### platform

`Cypress.platform` returns the underlying OS name, as returned from Node’s `os.platform()`.

```js
it('has JSON files', function () {
  // if windows do one thing, else do another
  const cmd = Cypress.platform === 'win32' ? 'dir *.json' : 'ls *.json'

  cy.exec(cmd)
    .its('stdout')
    .should('include', 'package.json')
})
```

### spec

`Cypress.spec` returns you the properties of the spec under test.

```js
it('log spec info', function() {
  console.log(Cypress.spec)
  // {
  //   name: 'filter.spec.js',
  //   relative: 'cypress/integration/filter.spec.js',
  //   absolute: '/Users/janelane/Dev/web-app/cypress/integration/filter.spec.js',
  // }
})
```

### version

`Cypress.version` returns you the current version of Cypress you are running.

```js
const semver = require('semver')

if (semver.gte(Cypress.version, '1.1.3')) {
  it('has Cypress.platform', () => {
    expect(Cypress.platform).to.be.a('string')
  })
}
```

## Plugin API

The Plugins API allows you to hook into and extend Cypress.
The Plugins file `cypress/plugins/index.js` must export a function with the following signature:
```js
// cypress/plugins/index.js

// export a function
module.exports = (on, config) => {
  // configure plugins here
}
```
The exported function is called whenever a project is opened either with `cypress open` or `cypress run`.

Your function will receive 2 arguments: `on` and `config`.

You can return a synchronous function, or you can also return a Promise, and it will be awaited until
it resolves. This enables you to perform asynchronous actions in your exported function such as reading
files in from the filesystem. If you return or resolve with an object, Cypress will then merge this
object into the config which enables you to overwrite configuration or environment variables.

`on` is a function that you will use to register listeners on various events that Cypress exposes.
Registering to listen on an event looks like this:
```js
module.exports = (on, config) => {
  on('<event>', (arg1, arg2) => {
    // plugin stuff here
  })
}
```
The following events are available:

| Event                 | Description                                                                     |
|:----------------------|:--------------------------------------------------------------------------------|
| file:preprocessor     | Occurs when a spec or spec-related file needs to be transpiled for the browser. |
| before:browser:launch | Occurs immediately before launching a browser.                                  |
| task                  | Occurs in conjunction with the cy.task command.                                 |
| after:screenshot      | Occurs after a screenshot is taken.                                             |

`config` is the resolved Cypress configuration of the opened project. You can programmatically
modify config values.

### Execution Context

Your `pluginsFile` is invoked when Cypress opens a project. Cypress does this by spawing an independent
`child_process` which then requires in your `pluginsFile`. You will need to keep in mind it is Cypress
who is requiring your file - not your local project, not your local Node version, and not anything
else under your control.

Your `pluginsFile` can require anything installed in the local project directory. If your `pluginsFile`
has an uncaught exception, an unhandled rejection from a promise, a syntax error, or anything else -
Cypress will automatically catch them and display them in the console. Errors from plugins will
not crash Cypress.

Cypress automatially watches the `pluginsFile` and any changes made take effect immediately.

### Configuration API

Cypress enables you to dynamically modify configuration values and environment variables from your plugin file.

```js
// cypress/plugins/index.js
module.exports = (on, config) => {
  console.log(config) // see what all is in here!

  // modify config values
  config.defaultCommandTimeout = 10000
  config.baseUrl = 'https://staging.acme.com'

  // modify env var value
  config.env.ENVIRONMENT = 'staging'

  // return config
  return config
}
```

Cypress will respect and await promises you return.
```js
// promisified fs module
const fs = require('fs-extra')
const path = require('path')

function getConfigurationByFile (file) {
  const pathToConfigFile = path.resolve('..', 'config', `${file}.json`)

  return fs.readJson(pathToConfigFile)
}

// plugins file
module.exports = (on, config) => {
  // accept a configFile value or use development by default
  const file = config.env.configFile || 'development'

  return getConfigurationByFile(file)
}
```

### Preprocessors API

A preprocessor is the plugin responsible for preparing a support file or a test file for the browser.
A preprocessor could transpile your file from another language (CoffeeScript or ClojureScript) or from
a newer version of JavaScript (ES2017).

By default, Cypress comes packaged with the Browserify Preprocessor already installed. The Browserify
Preprocessor handles:
- CoffeeScript 1.x.x
- ES2015 via Babel
- JSX and CJSX
- Watching and caching files

To use the preprocessor, you should bind to the `file:preprocessor` event in your `pluginsFile`:
```js
// plugins file
module.exports = (on, config) => {
  on('file:preprocessor', (file) => {
    // ...
  })
}
```

### Browser Launch API

Before Cypress launches a browser, it gives you the ability to modify the arguments used to launch it.
This is helpful to modify, remove, or add your own arguments. The most common use case is adding your
own chrome extension.

```js
module.exports = (on, config) => {
  on('before:browser:launch', (browser = {}, args) => {
    // browser will look something like this
    // {
    //   name: 'chrome',
    //   displayName: 'Chrome',
    //   version: '63.0.3239.108',
    //   path: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    //   majorVersion: '63'
    // }

    if (browser.name === 'chrome') {
      args.push('--start-fullscreen')

      // whatever you return here becomes the new args
      return args
    }

    if (browser.name === 'electron') {
      args['fullscreen'] = true

      // whatever you return here becomes the new args
      return args
    }
  })
}
```

### After Screenshot API

After a screenshot is taken, you can get details about the screenshot via the `after:screenshot` plugin
event. This event is called whether a screenshot is taken with `cy.screenshot()` or as a result of a
test failure. The event is called after the screenshot image is written to disk.

This allows you to record those details or manipulate the image as needed. You can also return updated
details about the image.

```js
// cypress/plugins/index.js

const fs = require('fs')

module.exports = (on, config) => {
  on('after:screenshot', (details) => {
    // details will look something like this:
    // {
    //   size: 10248
    //   takenAt: '2018-06-27T20:17:19.537Z'
    //   duration: 4071
    //   dimensions: { width: 1000, height: 660 }
    //   multipart: false
    //   pixelRatio: 1
    //   name: 'my-screenshot'
    //   specName: 'integration/my-spec.js'
    //   testFailure: true
    //   path: '/path/to/my-screenshot.png'
    //   scaled: true
    //   blackout: []
    // }

    // example of renaming the screenshot file

    const newPath = '/new/path/to/screenshot.png'

    return new Promise((resolve, reject) => {
      fs.rename(details.path, newPath, (err) => {
        if (err) return reject(err)

        // because we renamed/moved the image, resolve with the new path
        // so it is accurate in the test results
        resolve({ path: newPath })
      })
    })
  })
}
```
