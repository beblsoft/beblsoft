let path = require('path');

describe('Mocked Test', () => {
  before(() => {
    cy.automock('test.json', {
      isCustomMock: false,
      outDir: '/tests/e2e/mocks/'
    });
  });

  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
    cy.get('[data-cy=pollNetwork]').click()
    cy.contains('h1', '20/20')
  })

  after(() => {
    cy.automockEnd();
  });
})
