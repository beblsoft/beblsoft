import Vue from 'vue';
import App from './App.vue';
import installCypressHooks from 'cypress-auto-mock/include-in-webapp';

installCypressHooks();
Vue.config.productionTip = false;

new Vue({
  render: (h) => { return h(App); }
}).$mount('#app');
