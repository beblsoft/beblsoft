/**
 * @file Test Visiting a Page
 */

/* ------------------------ TESTS ------------------------------------------ */
describe('Vist Page', function() {
  it('Visits the Kitchen Sink', function() {

  	// Visit page
    cy.visit('https://example.cypress.io');

    // Contains finds element with specified text
    cy.contains('type').click();

    cy.url().should('include', '/commands/actions')

    // Get an input, type into it and verify that the value has been updated
    cy.get('.action-email')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com')

  });
});
