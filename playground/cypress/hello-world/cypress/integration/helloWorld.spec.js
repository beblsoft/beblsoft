/**
 * @file Hello World Test
 */

/* ------------------------ TESTS ------------------------------------------ */
describe('Hello World Tests', function() {
  it('Simple passing test!', function() {
    expect(true).to.equal(true);
  });

   it('Simple failing test!', function() {
    expect(true).to.equal(false);
  });
});
