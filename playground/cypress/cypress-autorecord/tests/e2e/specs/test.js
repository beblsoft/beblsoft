// https://docs.cypress.io/api/introduction/api.html

const autoRecord = require('cypress-autorecord'); // Require the autorecord function


describe('My First Test', () => {
  autoRecord(__filename);

  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
    cy.get('[data-cy=pollNetwork]').click()
    cy.contains('h1', '20/20')
  })
})
