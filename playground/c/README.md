# C Technology Documentation

C is a general-purpose, compiled computer programming language.

C was originally developed at Bell Labs by Dennis Ritchie, between 1972 and 1973.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/C_(programming_language))
