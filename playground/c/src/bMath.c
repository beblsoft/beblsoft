/*
  FILE
    bMath.c

  DESCRIPTION:
    Beblsoft Math Function Implementation

  RUNNING
    cd ..
    make bMath
    ./bin/bMath
*/

/* --------------------- INCLUDES ------------------------------------------ */
#include <math.h>
#include <bMath.h>


/* --------------------- EXPORT FUNCTIONS ---------------------------------- */
/* Compute the greatest common divisor */
int gcd(int x, int y) {
	int g = y;

	while (x > 0) {
		g = x;
		x = y % x;
		y = g;
	}
	return g;
}

/* Divide two numbers */
int divide(int a, int b, int *remainder) {
  int quot = a / b;

  *remainder = a % b;
  return quot;
}

/* Average values in an array */
double avg(double *a, int n) {
  int i;
  double total = 0.0;

  for (i = 0; i < n; i++) {
    total += a[i];
  }
  return total / n;
}

 /* Distance between two points */
double distance(Point *p1, Point *p2) {
  return hypot(p1->x - p2->x, p1->y - p2->y);
}