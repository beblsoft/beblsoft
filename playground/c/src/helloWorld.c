/*
  FILE
    helloWorld.c

  DESCRIPTION:
    C Hello World Program
*/

/* --------------------- INCLUDES ------------------------------------------ */
#include <stdio.h>


/* --------------------- MAIN ---------------------------------------------- */
int main()
{
	printf("Hello World!\n");
	return 0;
}