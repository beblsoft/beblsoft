/*
  FILE
    bMath.h

  DESCRIPTION:
    Beblsoft Math Functions Interface
*/

/* --------------------- INCLUDES ------------------------------------------ */
#include <stdio.h>
#include <math.h>


/* --------------------- STUCTURES ----------------------------------------- */
typedef struct Point {
  double x,y;
} Point;


/* --------------------- EXPORT FUNCTIONS ---------------------------------- */
int gcd(int x, int y);
int divide(int a, int b, int *remainder);
double avg(double *a, int n);
double distance(Point *p1, Point *p2);