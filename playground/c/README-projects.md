# C Projects Documentation

Source Layout:
```text
.
├── bin               - Binary Location
├── if                - Header file location (*.h)
├── lib               - Library Location (*.so)
├── Makefile          - Instructions for building code
├── src               - Source file location (*.c)
├── README.md
└── README-projects.md
```

## helloWorld.c

Hello World C Program

To Run:

```bash
# Build
make all

# Run
./bin/helloWorld

```

## bMath.c

Example builds the shared library file, `lib/libbMath.so`.

To build library: `make all`;
