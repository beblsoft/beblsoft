#!/usr/bin/env python3
"""
NAME
 search.py

DESCRIPTION
 Demonstrate search functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import pprint
import enum
import random
import logging
from datetime import datetime, timedelta
from contextlib import contextmanager
from sqlalchemy import create_engine, func, extract
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Boolean, DateTime
from sqlalchemy import Enum, Float, Text
from sqlalchemy.orm import sessionmaker
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)
wordList     = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"]


# ------------------------- TABLES ------------------------------------------ #
class SampleEnum(enum.Enum):
    """
    Sample Enum

    """
    FOO = 1
    BAR = 2
    CAR = 3
    FAR = 4


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)

    # Simple
    boolean        = Column(Boolean)
    enum           = Column(Enum(SampleEnum), default=SampleEnum.FOO)

    # Numeric
    integer        = Column(Integer)
    bigInteger     = Column(BigInteger)
    float_         = Column(Float(precision=8))

    # String
    string         = Column(String(64))
    text           = Column(Text)
    dateTime       = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "[{} id={} enum={} string={} dateTime={}]".format(
            self.__class__.__name__, str(self.id).ljust(3), self.enum.name.ljust(4),
            self.string.ljust(40), self.dateTime)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate(nPoints=500, minInt=0, maxInt=1000000, nFloatBits=10,
             secondsBackMax = 60 * 60 * 24 * 365 * 10):
    """
    Populate tables
    """
    printTitle("Populating")
    with sessionScope() as s:
        for _ in range(0, nPoints):

            samp = Sample(
                boolean    = random.random() > .5,
                enum       = random.choice(list(SampleEnum)),
                integer    = random.randint(minInt, maxInt),
                bigInteger = random.randint(minInt, maxInt),
                float_     = random.getrandbits(nFloatBits),
                string     = forgery_py.basic.text(length=32) + random.choice(wordList),
                text       = forgery_py.basic.text(length=500),
                dateTime   = (datetime.utcnow() -
                              timedelta(seconds = random.randint(0, secondsBackMax))))
            s.add(samp)


@logFunc()
def search(title, qEnum=None, qString=None, qMinDate=None,
           oByTimeAsc=None,
           qOffset=None, qLimit=None,
           qCount=False):
    """
    Search
    """
    printTitle(title)
    with sessionScope(commit=False) as s:
        q = s.query(Sample)

        if qEnum is not None:
            q = q.filter(Sample.enum == qEnum)
        if qString is not None:
            q = q.filter(Sample.string.ilike("%{}%".format(qString)))
        if qMinDate is not None:
            q = q.filter(Sample.dateTime >= qMinDate)
        if oByTimeAsc is not None:
            q = q.order_by(Sample.dateTime.desc() if oByTimeAsc else Sample.dateTime.asc())
        if qOffset is not None:
            q = q.offset(qOffset)
        if qLimit is not None:
            q = q.limit(qLimit)

        result = q.count() if qCount else q.all()
        print(pprint.pformat(result))


@logFunc()
def group(title, gDateTimeDay=False, gDateTimeMonth=False, gDateTimeYear=False,
          gBool=False, gEnum=False):
    """
    Group
    """
    printTitle(title)
    with sessionScope(commit=False) as s:
        q      = None
        result = None
        if gDateTimeDay:
            dateTimeDay = func.date(extract('day', Sample.dateTime))
            q = s.query(func.count(Sample.dateTime))
            q = q.group_by(dateTimeDay)
        if gDateTimeMonth:
            dateTimeMonth = func.date(extract('month', Sample.dateTime))
            q = s.query(func.count(Sample.dateTime))
            q = q.group_by(dateTimeMonth)
        if gDateTimeYear:
            dateTimeYear = func.date(extract('year', Sample.dateTime))
            q = s.query(func.count(Sample.dateTime))
            q = q.group_by(dateTimeYear)
        if gBool:
            q = s.query(Sample.boolean, func.count(Sample.boolean))
            q = q.group_by(Sample.boolean)
        if gEnum:
            q = s.query(Sample.enum, func.count(Sample.enum))
            q = q.group_by(Sample.enum)

        if q:
            result = q.all()
        print(pprint.pformat(result))


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    search(
        title         = "Count",
        qCount        = True)
    search(
        title         = "Foo Enum: First 10",
        qEnum         = SampleEnum.FOO,
        qOffset       = 0,
        qLimit        = 10)
    search(
        title         = "Foo Enum: Second 10",
        qEnum         = SampleEnum.FOO,
        qOffset       = 10,
        qLimit        = 10)
    search(
        title         = "Text containing 'five': First 10",
        qString       = "five",
        qOffset       = 0,
        qLimit        = 10)
    search(
        title         = "This Year: First 10",
        qMinDate      = datetime(2018, 1, 1, 0, 00),
        oByTimeAsc    = False,
        qOffset       = 0,
        qLimit        = 10)
    search(
        title         = "This Year: Last 10",
        qMinDate      = datetime(2018, 1, 1, 0, 00),
        oByTimeAsc    = True,
        qOffset       = 0,
        qLimit        = 10)
    group(
        title         = "Group By DateTimeDay",
        gDateTimeDay  = True)
    group(
        title         = "Group By DateTimeMonth",
        gDateTimeMonth= True)
    group(
        title         = "Group By DateTimeYear",
        gDateTimeYear = True)
    group(
        title         = "Group By Boolean",
        gBool         = True)
    group(
        title         = "Group By Enum",
        gEnum         = True)
    deleteTables()
