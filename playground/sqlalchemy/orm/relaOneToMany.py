#!/usr/bin/env python3
"""
NAME
 relaOneToMany.py

DESCRIPTION
 Demonstrates One To Many Relationship

BIBLIOGRAPHY
 http://docs.sqlalchemy.org/en/latest/orm/basic_relationships.html#one-to-many
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import random
import pprint
from contextlib import contextmanager
from sqlalchemy import create_engine, func, desc, asc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Parent(Base):
    """
    Parent Class
    """
    __tablename__ = "parent"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(64))

    # Relationship: 1 Parent to N Children
    children      = relationship("Child", back_populates="parent")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class Child(Base):
    """
    Child Class
    """
    __tablename__ = "ChildTable"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(64))

    # Relationship: 1 Parent to N Children
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="children")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate(nParents=10, maxKidsPerParent=10):
    """
    Populate tables
    """
    printTitle("Populating")
    with sessionScope() as s:
        for _ in range(0, nParents):
            p = Parent(name = forgery_py.name.full_name())

            # Add Children
            nKids = random.randint(0, maxKidsPerParent)
            for __ in range(0, nKids):
                p.children.append(
                    Child(name=forgery_py.name.full_name()))
            s.add(p)


@logFunc()
def printState():
    """
    Print database state
    """
    with sessionScope(commit=False) as s:
        parents = s.query(Parent).all()
        for parent in parents:
            print(pprint.pformat(parent))
            print(pprint.pformat(parent.children))


@logFunc()
def queryCount():
    """
    Count queries
    """
    printTitle("Count Queries")
    with sessionScope(commit=False) as s:
        nParents = s.query(Parent).count()
        print("nParents={}\n".format(nParents))

        nChildren = s.query(Child).count()
        print("nChildren={}\n".format(nChildren))

        parentMostKids = s.query(Parent, func.count(Parent.children)).\
            outerjoin(Child).\
            group_by(Parent).\
            order_by(desc(func.count(Parent.children))).\
            first()
        print("Parent with most kids:\n{}\n".format(parentMostKids))

        parentLeastKids = s.query(Parent, func.count(Parent.children)).\
            outerjoin(Child).\
            group_by(Parent).\
            order_by(asc(func.count(Parent.children))).\
            first()
        print("Parent with least kids:\n{}\n".format(parentLeastKids))

        parentMostKids = s.query(Parent, func.count(Parent.children)).\
            outerjoin(Child).\
            group_by(Parent).\
            order_by(desc(func.count(Parent.children))).\
            all()
        print("Parents sorted by number of kids descending:\n{}\n".format(
            pprint.pformat(parentMostKids)))


@logFunc()
def queryAverage():
    """
    Average Queries.
    Also shows example subquery.
    """
    printTitle("Average Queries")
    with sessionScope(commit=False) as s:
        parentsNKidsSubQuery = s.query(Parent, func.count(Parent.children).label("kidCount")).\
            outerjoin(Child).\
            group_by(Parent).\
            subquery()
        averageKidsPerParent = s.query(
            func.avg(parentsNKidsSubQuery.c.kidCount)).scalar()
        print("Average kids per parent:\n{}\n".format(averageKidsPerParent))


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    # printState()
    queryCount()
    queryAverage()
    deleteTables()
