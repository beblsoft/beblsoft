#!/usr/bin/env python3
"""
NAME
 loadingColumns.py

DESCRIPTION
 Demonstrates Different Column Loading Strategies

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/loading_columns.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
from contextlib import contextmanager
from sqlalchemy import create_engine, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.orm import sessionmaker, deferred, defer, undefer_group, undefer
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Parent(Base):
    """
    Parent Class
    """
    __tablename__ = "parent"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(64))
    regular       = Column(String(64))
    text          = deferred(Column(Text))
    group1        = deferred(Column(Text), group="loadingGroup")
    group2        = deferred(Column(Text), group="loadingGroup")
    group3        = deferred(Column(Text), group="loadingGroup")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    with sessionScope() as s:
        p = Parent(
            name    = forgery_py.name.full_name(),
            regular = forgery_py.basic.text(length=49),
            text    = forgery_py.lorem_ipsum.paragraph(),
            group1  = forgery_py.lorem_ipsum.paragraph(),
            group2  = forgery_py.lorem_ipsum.paragraph(),
            group3  = forgery_py.lorem_ipsum.paragraph())
        s.add(p)


@logFunc()
def loadAndInspect(title="",
                   deferRegular=False, accessRegular=False,
                   undeferText=False, accessText=False,
                   undeferGroup=False, accessGroup=False):
    """
    Load and inspect parent at different states
    """
    printTitle(title)
    with sessionScope(commit=False) as s:
        q = s.query(Parent)

        # Loading Options
        if deferRegular:
            q = q.options(defer("regular"))
        if undeferText:
            q = q.options(undefer("text"))
        if undeferGroup:
            q = q.options(undefer_group("loadingGroup"))

        # Issue query to load parent
        p = q.first()

        # Load fields, fields will be loaded when accessed
        if accessRegular:
            _ = p.regular
        if accessText:
            _ = p.text
        if accessGroup:
            _ = p.group1

        inspectObj(p)


@logFunc()
def inspectObj(obj):
    """
    Inspect object
    """
    insp = inspect(obj)
    meta = {
        "unloaded": insp.unloaded,
    }
    print(pprint.pformat(meta))


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    loadAndInspect(
        title         = "Normal case, not overriding mapping options")
    loadAndInspect(
        title         = "Defer regular",
        deferRegular  = True)
    loadAndInspect(
        title         = "Defer regular, then load it on access",
        deferRegular  = True,
        accessRegular = True)
    loadAndInspect(
        title         = "Undefer text",
        undeferText   = True)
    loadAndInspect(
        title         = "Access text",
        accessText    = True)
    loadAndInspect(
        title         = "Undefer group",
        undeferGroup  = True)
    loadAndInspect(
        title         = "Access group",
        accessGroup   = True)
    deleteTables()
