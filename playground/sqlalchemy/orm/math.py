#!/usr/bin/python3
"""
NAME
 math.py

DESCRIPTION
 Demonstrates Math Functionality

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/functions.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
from datetime import datetime, timedelta
import pprint
import logging
import random
from contextlib import contextmanager
from sqlalchemy import create_engine, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger, DateTime
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class Data(Base):
    """
    Data Class
    """
    __tablename__ = 'data'
    id            = Column(Integer, primary_key=True)
    value         = Column(BigInteger)
    creationDate  = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "[{} id={} value={} creationDate={}]".format(
            self.__class__.__name__, self.id, self.value, self.creationDate)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def getRandomDate(startTimeDelta=timedelta(hours=24 * 365)):
    """
    Get a random date
    Args
      startTimeDelta:
        Time delta before now at which to start range of potential dates

    Note
      Random date is taken from range
      [utcnow - startTimeDelta] to [utcnow]
    """
    endDate      = datetime.utcnow()
    startDate    = endDate - startTimeDelta
    deltaSeconds = startTimeDelta.total_seconds()
    randomSec    = random.randrange(deltaSeconds)
    return startDate + timedelta(seconds=randomSec)


@logFunc()
def getRandomValue(minVal=0, maxVal=100):
    """
    Return random value inside range
    """
    return minVal + (maxVal - minVal) * random.random()


@logFunc()
def populate(nPoints=100):
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        for _ in range(0, nPoints):
            value = getRandomValue()
            date  = getRandomDate()
            d     = Data(value=value, creationDate=date)
            s.add(d)


@logFunc()
def printStats(startDate):
    """
    Print data stats
    Args
      title
      startDate:
        Examine creationDates since this start date
    """
    printTitle("Stats Since {}".format(startDate))

    with sessionScope() as s:
        q = s.query(func.min(Data.value),
                    func.max(Data.value),
                    func.count(Data.value),
                    func.sum(Data.value),
                    func.avg(Data.value))
        q = q.filter(Data.creationDate >= startDate)
        q = q.filter(Data.creationDate <= datetime.utcnow())
        # print(q)

        # Execute query and display results
        result = q.all()[0]
        meta   = {
            "minVal": result[0],
            "maxVal": result[1],
            "count": result[2],
            "sum": result[3],
            "avg": result[4]
        }
        print(pprint.pformat(meta))


@logFunc()
def printIntervalStats():
    """
    Print stats at different intervals
    """
    startDateList = [
        datetime.utcnow() - timedelta(hours=24 * 365),
        datetime.utcnow() - timedelta(hours=24 * 170),
        datetime.utcnow() - timedelta(hours=24 * 30),
        datetime.utcnow() - timedelta(hours=24 * 1),
    ]
    for startDate in startDateList:
        printStats(startDate=startDate)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printIntervalStats()
    deleteTables()
