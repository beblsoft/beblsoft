#!/usr/bin/env python3
"""
NAME
 relaManyToManyAssoc.py

DESCRIPTION
 Demonstrates Many To Many Relationship with Association Table

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/basic_relationships.html#association-object
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Association(Base):
    """
    Association Table Between Parents and Children
    """
    __tablename__ = 'association'
    parentID      = Column(Integer, ForeignKey('parent.id'), primary_key=True)
    parent        = relationship("Parent", back_populates="children")
    childID       = Column(Integer, ForeignKey('child.id'), primary_key=True)
    child         = relationship("Child", back_populates="parents")
    extraData     = Column(String(50))

    def __repr__(self):
        return "[{} parent={} child={} extraData={}]".format(
            self.__class__.__name__, self.parent, self.child, self.extraData)

class Parent(Base):
    """
    Parent Class
    """
    __tablename__ = 'parent'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50))

    # Relationship: N Parents to N Children
    children      = relationship("Association", back_populates="parent")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class Child(Base):
    """
    Child Class
    """
    __tablename__ = 'child'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50))

    # Relationship: N Parents to N Children
    parents       = relationship("Association", back_populates="child")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate(nParents=5, nChildren=5):
    """
    Populate tables
    """
    printTitle("Populating")
    with sessionScope() as s:
        parentList = []
        childList  = []
        for _ in range(0, nParents):
            parentList.append(Parent(name = forgery_py.name.full_name()))
        for _ in range(0, nChildren):
            c = Child(name = forgery_py.name.full_name())
            for p in parentList:
                a = Association(extraData = "Extra Data!")
                a.parent = p
                c.parents.append(a)
        s.add_all(childList + parentList)


@logFunc()
def printState():
    """
    Print database state
    """
    printTitle("Database State")
    with sessionScope(commit=False) as s:
        parents = s.query(Parent).all()
        for p in parents:
            print(pprint.pformat(p))
            print(pprint.pformat(p.children))
            print("\n")


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
