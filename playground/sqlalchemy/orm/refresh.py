#!/usr/bin/env python3
"""
NAME
 refresh.py

DESCRIPTION
 Session Refresh Functionality

DOCS
 https://docs.sqlalchemy.org/en/latest/orm/session_state_management.html#refreshing-expiring
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    integer        = Column(Integer)

    def __repr__(self):
        return "[{} id={}]".format(self.__class__.__name__, self.id)


# ----------------------------- HELPERS ------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    with sessionScope() as s:
        samp = Sample(integer=10)
        s.add(samp)
        s.commit()
        s.refresh(samp)
        print(samp)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    deleteTables()
