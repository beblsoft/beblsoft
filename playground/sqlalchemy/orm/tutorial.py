#!/usr/bin/python3.5
"""
NAME
 tutorial.py

DESCRIPTION
 Demonstrates Basic usage of SQLAlchemy Object Relational Mapper (ORM).
 An ORM maps python classes to database tables.

BIBLIOGRAPHY
 http://docs.sqlalchemy.org/en/rel_1_0/orm/tutorial.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Sequence, ForeignKey, Table, Text
from sqlalchemy import or_, and_, text, func
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.orm import contains_eager, joinedload, subqueryload
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base = declarative_base()
engine = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)


# ------------------------- TABLES ------------------------------------------ #
class User(Base):
    """
    User Class
    Relationships
      User-Address:  One-to-Many
      User-BlogPost: One-to-Many
    Note
      Cascading deletes on addresses. When a user is deleted, all of their
      addresses will also be deleted.
    """
    __tablename__ = "users"
    id            = Column(Integer, Sequence("user_id_seq"), primary_key=True)
    name          = Column(String(50))
    fullname      = Column(String(50))
    password      = Column(String(30))
    addresses     = relationship("Address", back_populates="user",
                                 cascade="all, delete, delete-orphan")
    posts         = relationship("BlogPost", back_populates="author",
                                 lazy="dynamic")

    def __repr__(self):
        return "User(id={}, name={}, fullname={}, password={}, addresses={})".format(
            self.id, self.name, self.fullname, self.password, self.addresses)


class Address(Base):
    """
    Email Address Class
    Relationships
      User-Address: One-to-Many
    """
    __tablename__ = "addresses"
    id            = Column(Integer, primary_key=True)
    email_address = Column(String, nullable=False)
    user_id       = Column(Integer, ForeignKey("users.id"))
    user          = relationship("User", back_populates="addresses")

    def __repr__(self):
        return "Address(email_address={})".format(self.email_address)


# Post-Keyword Many-to-Many Association table
post_keywords = Table("post_keywords", Base.metadata,
                      Column("post_id", ForeignKey("posts.id"), primary_key=True),
                      Column("keyword_id", ForeignKey("keywords.id"), primary_key=True))


class BlogPost(Base):
    """
    BlogPost Class
    Relationships
      BlogPost-Keyword: Many-to-Many
      User-BlogPost:    One-to-Many
    """
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    headline = Column(String(255), nullable=False)
    body = Column(Text)
    author = relationship("User", back_populates="posts")

    # Many-to-Many, BlogPost<->Keyword
    keywords = relationship(
        "Keyword", secondary=post_keywords, back_populates="posts")

    def __init__(self, headline, body, author):
        self.author = author
        self.headline = headline
        self.body = body

    def __repr__(self):
        return "BlogPost({},{},{})".format(self.headline, self.body, self.author)


class Keyword(Base):
    """
    Keyword Class
    Relationships
      BlogPost-Keyword: Many-to-Many
    """
    __tablename__ = "keywords"
    id = Column(Integer, primary_key=True)
    keyword = Column(String(50), nullable=False, unique=True)
    posts = relationship('BlogPost', secondary=post_keywords,
                         back_populates='keywords')

    def __init__(self, keyword):
        self.keyword = keyword

    def __repr__(self):
        return "Keyword={}".format(self.keyword)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def addUsers():
    """
    Add various users to the database.
    """
    printTitle("Adding users")
    s = sessionMaker()
    s.add_all([
        User(name="darryl", fullname="Darryl Black", password="1234"),
        User(name="james", fullname="James Bensson", password="5678"),
        User(name="ed", fullname="Ed Jones", password="91011"),
        User(name="wendy", fullname="Wendy Williams", password="foobar"),
        User(name="mary", fullname="Mary Contrary", password="xxg527"),
        User(name="fred", fullname="Fred Flinstone", password="blah")])
    s.commit()


@logFunc()
def demoRollback():
    """
    Rollback a transaction within a session
    """
    printTitle("Demo rollback")
    s = sessionMaker()
    user = User(name="fake", fullname="fake user", password="1234")
    s.add(user)
    print("Is user in session?{}".format(user in s))
    s.rollback()
    print("Is user in session?{}".format(user in s))


@logFunc()
def demoQueryOps():
    """
    Demo different query, filter operations
    """
    printTitle("Demo query ops")
    s = sessionMaker()
    s.query(User).order_by(User.id).all()                          # Order by id
    s.query(User).order_by(User.fullname).all(
    )                    # Order by fullname
    s.query(User).filter(User.name == "ed").all()                  # Equal
    s.query(User).filter(User.name != "ed").all()                  # Not Equal
    s.query(User).filter(User.name.like("%ed%")).all()             # Like
    s.query(User).filter(User.name.ilike("%ed%")
                         ).all()                                   # Case-Insensitive Like
    s.query(User).filter(User.name.in_(["ed", "wendy"])).all()     # In
    s.query(User).filter(~User.name.in_(["ed", "wendy"])).all()    # Not In
    s.query(User).filter(User.name == None).all()                  # NULL
    s.query(User).filter(User.name != None).all()                  # Not NULL
    s.query(User).filter(and_(User.name == "ed",
                              User.fullname == "Ed Jones")).all()  # AND with and_
    s.query(User).filter(User.name == "ed",
                         User.fullname == "Ed Jones").all()        # AND with filter
    s.query(User).filter(User.name == "ed").filter(
        User.fullname == "Ed Jones").all()                         # AND with chain
    s.query(User).filter(or_(User.name == "ed",
                             User.name == "wendy")).all()          # OR with or_
    s.query(User).filter(text("id<5")).order_by(
        text("id")).all()                                          # SQL Text filter, order
    s.query(User).from_statement(text("SELECT * FROM users where name =:name")
                                 ).params(name="ed").all()         # from_statement,params
    s.query(User).count()                                          # count
    s.query(func.count(User.name), User.name).group_by(
        User.name).all()                                           # Count distinct user names


@logFunc()
def demoListsAndScalars():
    """
    Demo retrieving single or multiple results
    """
    printTitle("Demo lists and scalars")
    s = sessionMaker()
    q = s.query(User).order_by(User.name)
    q.all()          # All: return list of all elements
    _ = q.all()[0:4] # Retrieve subarray with elements 0,1,2,3
    q.first()        # First

    q = s.query(User).filter(User.id == 1)
    q.one()          # One: return only one
    # MultipleResultsFound error if more than one
    # NoResultFound if more than one
    q.one_or_none()  # One or none: like one except don"t error if None
    q.scalar()       # Scalar: invokes one(), on success returns first column of row


@logFunc()
def demoOneToMany():
    """
    Demo adding and querying a one-to-many relationship
    """
    printTitle("Demo one to many")
    s = sessionMaker()
    u = User(name="Jack", fullname="Jack Bean", password="gjffdd")
    u.addresses = [Address(email_address="jack@google.com"),
                   Address(email_address="jack@yahoo.com")]
    # User Backlink automatically added to addresses
    # Cascading: when user added to session, addresses also added
    print("User Addresses:{}".format(u.addresses))
    print("Addresses user:{}".format(u.addresses[0].user.name))
    s.add(u)
    s.commit()

    # Query all users by email address
    s.query(User).join(Address).filter(
        Address.email_address == "jack@google.com").all()

    # By default, when querying the user table, the address table will not be queried.
    # However when user.addresses is accessed, the address table must be queried.
    # 3 ways to eagerly load:
    #   - subquery load : better for loading related collections
    #   - joined load   : better for loading many-to-one
    #   - contains_eager: custom
    u = s.query(User).options(subqueryload(
        User.addresses)).filter_by(name="Jack").one()
    u = s.query(User).options(joinedload(User.addresses)
                              ).filter_by(name="Jack").one()
    _ = s.query(Address).join(Address.user).filter(
        User.name == "Jack").options(contains_eager(Address.user)).all()

    # Deleting a user
    u = s.query(User).filter(User.name == "Jack").one()
    s.delete(u)
    s.commit()
    _ = s.query(Address).filter(Address.email_address.in_(["jack@google.com"])).all()


@logFunc()
def demoManyToMany():
    """
    Demo add and querying a many-to-many relationship
    """
    printTitle("Demo many to many")
    s = sessionMaker()
    u = User(name="Tom", fullname="Tom Siddall", password="1994")
    u.addresses = [Address(email_address="tom.siddall@google.com"),
                   Address(email_address="tom.siddall@yahoo.com")]
    bp1 = BlogPost("Tom's first post", "I love sports", u)
    bp1.keywords.append(Keyword("Cool"))
    bp1.keywords.append(Keyword("First"))
    bp2 = BlogPost("Tom's second post", "I love cars", u)
    bp2.keywords.append(Keyword("Gas"))
    bp2.keywords.append(Keyword("Cars"))
    s.add_all([u, bp1, bp2])
    s.commit()

    # Query BlogPosts for specific user, keyword
    bps = s.query(BlogPost).filter(BlogPost.keywords.any(keyword="Cool")).all()
    print(bps)
    bps = s.query(BlogPost).filter(BlogPost.author == u).filter(
        BlogPost.keywords.any(keyword="Gas")).all()
    print(bps)


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    addUsers()

    demoRollback()
    demoQueryOps()
    demoListsAndScalars()
    demoOneToMany()
    demoManyToMany()

    deleteTables()
