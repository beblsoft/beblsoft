#!/usr/bin/env python3
"""
NAME
 inspector.py

DESCRIPTION
 Inspect SQLAlchemy Metadata

BIBLIOGRAPHY
 Runtime inspection     : https://docs.sqlalchemy.org/en/latest/orm/mapping_styles.html#runtime-introspection-of-mappings-objects
 Runtime inspection api : https://docs.sqlalchemy.org/en/latest/core/inspection.html#sqlalchemy.inspection.inspect
 Inspector Class        : https://docs.sqlalchemy.org/en/latest/core/reflection.html#sqlalchemy.engine.reflection.Inspector
 Instance State         : https://docs.sqlalchemy.org/en/latest/orm/internals.html#sqlalchemy.orm.state.InstanceState
"""

# ------------------------- IMPORTS ----------------------------------------- #
import pprint
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    colFoo         = Column(Integer)
    colBar         = Column(Integer)

    def __repr__(self):
        """
        Return string with each column printed
        """
        cls     = self.__class__
        columns = cls.__table__.columns
        s       = ""
        for col in columns:
            colName = str(col).split(".")[1]
            colVal  = getattr(self, colName)
            s += "\n{}:{}".format(colName.ljust(15), colVal)
        return s


# ----------------------------- HELPERS ------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def inspectEngine():
    """
    Inspect Engine
    """
    insp        = inspect(engine)
    schemaNames = insp.get_schema_names()
    tables      = insp.get_table_names()

    printTitle("Inspect Engine Metadata")
    # Schemas
    for schemaName in schemaNames:
        print("Schema: {}".format(schemaName))

    # Tables
    for table in tables:
        print("Table : {}".format(table))
        meta = {
            'checkConstraints': insp.get_check_constraints(table),
            'columns': insp.get_columns(table),
            'foreignKeys': insp.get_foreign_keys(table),
            'indexes': insp.get_indexes(table),
            'primaryKeyConstraint': insp.get_pk_constraint(table),
            'primaryKeys': insp.get_primary_keys(table),
            'options': insp.get_table_options(table),
            'uniqueConstrains': insp.get_unique_constraints(table)
        }
        print(pprint.pformat(meta))


@logFunc()
def inspectInstance():
    """
    Inspect Instance
    """
    printTitle("Inspect Instance Metadata")
    with sessionScope() as s:
        samp = Sample(colFoo = 1, colBar = 2)
        insp = inspect(samp)
        print(pprint.pformat(samp.__dict__))
        meta = {
            'deleted': insp.deleted,
            'detached': insp.detached,
            'dict': insp.dict,
            'has_identity': insp.has_identity,
            'identity': insp.identity,
            'identity_key': insp.identity_key,
            'object': insp.object,
            'pending': insp.pending,
            'persistent': insp.persistent,
            'session': insp.session,
            'transient': insp.transient,
            'unloaded': insp.unloaded,
            'unloaded_expirable': insp.unloaded_expirable,
            'unmodified': insp.unmodified,
            'was_deleted': insp.was_deleted,
        }
        print(pprint.pformat(meta))
        s.add(samp)


@logFunc()
def inspectQuery():
    """
    Inspect Query
    """
    printTitle("Inspect Query SQL")
    with sessionScope() as s:
        q = s.query(Sample)
        print(q)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    inspectEngine()
    inspectInstance()
    inspectQuery()
    deleteTables()
