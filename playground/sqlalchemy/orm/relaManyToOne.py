#!/usr/bin/env python3
"""
NAME
 relaManyToOne.py

DESCRIPTION
 Demonstrates Many To One Relationship

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/basic_relationships.html#many-to-one
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Parent(Base):
    """
    Parent Class
    """
    __tablename__ = 'parent'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50))

    # Relationship: N Parents to 1 Child
    childID       = Column(Integer, ForeignKey('child.id'))
    child         = relationship("Child", back_populates="parents")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class Child(Base):
    """
    Child Class
    """
    __tablename__ = 'child'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50))

    # Relationship: N Parents to 1 Child
    parents       = relationship("Parent", back_populates="child")

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate(nChildren=5, nParentsPerKid=2):
    """
    Populate tables
    """
    printTitle("Populating")
    with sessionScope() as s:
        for _ in range(0, nChildren):
            c = Child(name = forgery_py.name.full_name())

            # Add Parents
            for __ in range(0, nParentsPerKid):
                p = Parent(name = forgery_py.name.full_name())
                c.parents.append(p)
            s.add(c)


@logFunc()
def printState():
    """
    Print database state
    """
    printTitle("Database State")
    with sessionScope(commit=False) as s:
        children = s.query(Child).all()
        for c in children:
            print(pprint.pformat(c))
            print(pprint.pformat(c.parents))
            print("\n")


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
