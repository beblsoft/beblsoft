#!/usr/bin/env python3
"""
NAME
 loading.py

DESCRIPTION
 Demonstrates Different Loading Strategies

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/loading_relationships.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
from contextlib import contextmanager
import sqlalchemy
from sqlalchemy import create_engine, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, lazyload, joinedload
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Parent(Base):
    """
    Parent Class
    """
    __tablename__ = "parent"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(64))

    # Relationships: 1 Parent to N Children
    rela_lazy     = relationship("LazyChild", back_populates="parent",
                                 lazy="select")                             # Default, lazy
    rela_joined   = relationship("JoinedChild", back_populates="parent",
                                 lazy="joined")                             # eager
    rela_subquery = relationship("SubqueryChild", back_populates="parent",
                                 lazy="subquery")                           # eager
    rela_selectIn = relationship("SelectInChild", back_populates="parent",
                                 lazy="selectin")                           # eager
    rela_raise    = relationship("RaiseChild", back_populates="parent",
                                 lazy="raise")                              # raise exception
    rela_noLoad   = relationship("NoLoadChild", back_populates="parent",
                                 lazy="noload")                             # don't load

    def __repr__(self):
        return "[{} id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class LazyChild(Base):  # pylint: disable=C0111
    __tablename__ = "lazyChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_lazy")


class JoinedChild(Base):  # pylint: disable=C0111
    __tablename__ = "joinedChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_joined")


class SubqueryChild(Base):  # pylint: disable=C0111
    __tablename__ = "subqueryChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_subquery")


class SelectInChild(Base):  # pylint: disable=C0111
    __tablename__ = "selectInChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_selectIn")


class RaiseChild(Base):  # pylint: disable=C0111
    __tablename__ = "raiseChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_raise")


class NoLoadChild(Base):  # pylint: disable=C0111
    __tablename__ = "noLoadChild"
    id            = Column(Integer, primary_key=True)
    parentID      = Column(Integer, ForeignKey("parent.id"))
    parent        = relationship("Parent", back_populates="rela_noLoad")


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    with sessionScope() as s:
        p = Parent(name = forgery_py.name.full_name())

        p.rela_lazy.append(LazyChild())
        p.rela_joined.append(JoinedChild())
        p.rela_subquery.append(SubqueryChild())
        p.rela_selectIn.append(SelectInChild())
        p.rela_raise.append(RaiseChild())
        p.rela_noLoad.append(NoLoadChild())
        s.add(p)


@logFunc()
def loadAndInspect(title="", #pylint: disable=R0912
                   undeferLazy=False, accessLazy=False,
                   deferJoined=False, accessJoined=False,
                   deferSubquery=False, accessSubquery=False,
                   deferSelectIn=False, accessSelectIn=False,
                   undeferRaise=False, accessRaise=False,
                   undeferNoLoad=False, accessNoLoad=False):
    """
    Load and inspect parent at different states
    """
    printTitle(title)
    with sessionScope(commit=False) as s:
        q = s.query(Parent)

        # Loading Options
        if undeferLazy:
            q = q.options(joinedload("rela_lazy"))
        if deferJoined:
            q = q.options(lazyload("rela_joined"))
        if deferSubquery:
            q = q.options(lazyload("rela_subquery"))
        if deferSelectIn:
            q = q.options(lazyload("rela_selectIn"))
        if undeferRaise:
            q = q.options(joinedload("rela_raise"))
        if undeferNoLoad:
            q = q.options(joinedload("rela_noLoad"))

        # Issue query to load parent
        p = q.first()

        # Access fields
        if accessLazy:
            _ = p.rela_lazy
        if accessJoined:
            _ = p.rela_joined
        if accessSubquery:
            _ = p.rela_subquery
        if accessSelectIn:
            _ = p.rela_selectIn
        if accessRaise:
            try:
                _ = p.rela_raise
            except sqlalchemy.exc.InvalidRequestError as _:
                print("Exception raised trying to access rela_raise")
        if accessNoLoad:
            _ = p.rela_noLoad

        # Load fields, fields will be loaded when accessed
        inspectParent(p)


@logFunc()
def inspectParent(parent):
    """
    Inspect parent object
    """
    insp = inspect(parent)
    # print(pprint.pformat(parent.__dict__))
    meta = {
        "unloaded": insp.unloaded,
    }
    print(pprint.pformat(meta))


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    loadAndInspect(
        title           = "Normal case, not overriding mapping options")
    loadAndInspect(
        title           = "Undefer lazy",
        undeferLazy     = True)
    loadAndInspect(
        title           = "Access lazy",
        accessLazy      = True)
    loadAndInspect(
        title           = "Defer Joined",
        deferJoined     = True)
    loadAndInspect(
        title           = "Defer Joined, then access joined",
        deferJoined     = True,
        accessJoined    = True)
    loadAndInspect(
        title           = "Defer Subquery",
        deferSubquery   = True)
    loadAndInspect(
        title           = "Defer Subquery, then access subquery",
        deferSubquery   = True,
        accessSubquery  = True)
    loadAndInspect(
        title           = "Defer Selectin",
        deferSelectIn   = True)
    loadAndInspect(
        title           = "Defer Selectin, then access selectin",
        deferSelectIn   = True,
        accessSelectIn  = True)
    loadAndInspect(
        title           = "Undefer raise",
        undeferRaise    = True)
    loadAndInspect(
        title           = "Access raise",
        accessRaise     = True)
    loadAndInspect(
        title           = "Undefer no load",
        undeferNoLoad   = True)
    loadAndInspect(
        title           = "Access no load",
        accessNoLoad    = True)

    deleteTables()
