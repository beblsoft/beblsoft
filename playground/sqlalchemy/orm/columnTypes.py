#!/usr/bin/env python3
"""
NAME
 columnTypes.py

DESCRIPTION
 Column Types Functionality

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/type_basics.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import enum
from datetime import datetime, timedelta
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, BigInteger, Date, Boolean, DateTime, Unicode, UnicodeText
from sqlalchemy import Enum, Float, Interval, Time, Text, SmallInteger, Numeric, LargeBinary, PickleType
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class SampleEnum(enum.Enum):
    """
    Sample Enum

    """
    FOO = 1
    BAR = 2


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)

    # Simple
    boolean        = Column(Boolean)
    enum           = Column(Enum(SampleEnum), default=SampleEnum.FOO)

    # Numeric
    integer        = Column(Integer)
    bigInteger     = Column(BigInteger)
    smallInteger   = Column(SmallInteger)
    float_         = Column(Float(precision=8))
    numeric        = Column(Numeric(precision=8))

    # String
    string         = Column(String(64))
    text           = Column(Text)
    unicode_       = Column(Unicode)
    unicodeText    = Column(UnicodeText)

    # Time
    date           = Column(Date)
    time           = Column(Time)
    dateTime       = Column(DateTime, default=datetime.utcnow)
    interval       = Column(Interval)

    # Binary
    largeBinary    = Column(LargeBinary)
    pickleType     = Column(PickleType)

    def __repr__(self):
        cls     = self.__class__
        columns = cls.__table__.columns
        s       = ""
        for col in columns:
            colName = str(col).split(".")[1]
            colVal  = getattr(self, colName)
            s += "\n{}:{}".format(colName.ljust(15), colVal)

        return s


# ----------------------------- HELPERS ------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    with sessionScope() as s:
        samp = Sample(
            boolean        = True,
            enum           = SampleEnum.FOO,
            integer        = 10,
            bigInteger     = 10000000000,
            smallInteger   = 129082,
            float_         = 1298.1294308732498,
            numeric        = -238.129038712398,
            string         = "I am a string!",
            text           = "23049823498q2erpjsofhczso9df87yasdkljhsdklvfjhasdf",
            unicode_       = "UNNNNNNNNNNNNNNNNNNNNNNNNNNNNICCCCCCCCCCCCCCCCCCCORN",
            unicodeText    = "q293084rulsdkfjhasdf98y4rksnvcbapa9ow8eytlkmisodufhasdklfj",
            date           = datetime.utcnow(),
            dateTime       = datetime.utcnow(),
            interval       = timedelta(seconds=0),
            largeBinary    = bytes("Python is awesome!", "utf-8"),
            pickleType     = {'hello': 'world'}
        )
        s.add(samp)


@logFunc()
def printState():
    """
    Print Database State
    """
    with sessionScope() as s:
        for samp in s.query(Sample).all():
            print(samp)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()

# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
