#!/usr/bin/python3
"""
NAME
 reconstructor.py

DESCRIPTION
 Demonstrates Reconstructor Usage

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/constructors.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine, orm
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    name           = Column(String(64))

    def __init__(self, name):
        self.name = name
        print("Initializing object")
        self.stuff = []

    @orm.reconstructor
    def initOnLoad(self):
        """
        Initialize object from database
        """
        print("Initializing after load from database")
        self.stuff = []

    def __repr__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        samp = Sample(name="Bob")
        s.add(samp)


@logFunc()
def printState():
    """
    Print database state
    """
    printTitle("Printing State")
    with sessionScope() as s:
        sampList = s.query(Sample).all()
        for samp in sampList:
            print(samp)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
