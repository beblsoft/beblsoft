#!/usr/bin/python3
"""
NAME
 update.py

DESCRIPTION
 Demonstrates Basic usage of SQLAlchemy Update Capabilities

BIBLIOGRAPHY
 http://docs.sqlalchemy.org/en/latest/orm/session_state_management.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class User(Base):
    """
    User Class
    """
    __tablename__ = "user"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50), nullable=False)
    age           = Column(Integer)
    addresses     = relationship("Address", backref="user")

    def __repr__(self):
        return "[{} name={} age={} addresses={}]".format(
            self.__class__.__name__, self.name, self.age, self.addresses)


class Address(Base):
    """
    Address Class
    """
    __tablename__ = "address"
    id            = Column(Integer, primary_key=True)
    email_address = Column(String(50), nullable=False)
    home_address  = Column(String(256))
    user_id       = Column(Integer, ForeignKey("user.id"), nullable=False)

    def __repr__(self):
        return "[{} home={} email={}]".format(
            self.__class__.__name__, self.home_address, self.email_address)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate
    """
    printTitle("Populate")
    s = sessionMaker()
    s.add_all([
        User(name      = "ed",
             age       = 50,
             addresses = [Address(email_address="ed@ed.com")]),
    ])
    s.commit()


@logFunc()
def updateAge1():
    """
    Update user age
    """
    printTitle("Update age 1")
    s      = sessionMaker()
    u1     = s.query(User).filter(User.name == "ed").one_or_none()
    u1.age = 60
    s.commit()


@logFunc()
def updateAge2():
    """
    Update user age
    Link
      https://stackoverflow.com/questions/9667138/how-to-update-sqlalchemy-row-entry
    Note
      This update happens entirely at the database layer
    """
    printTitle("Update age 2")
    s = sessionMaker()
    s.query(User).filter(User.name == "ed").update({"age": 5})
    s.commit()


@logFunc()
def updateAddress():
    """
    Update user address
    """
    printTitle("Update address")
    s      = sessionMaker()
    u1     = s.query(User).filter(User.name == "ed").one_or_none()
    a1     = u1.addresses[0]
    a1.home_address = "20 Park Place, Atlanta GA"
    s.commit()


@logFunc()
def printState(users=False, addresses=False):
    """
    Print database state
    """
    printTitle("Dump tables")
    s         = sessionMaker()
    if users:
        users     = s.query(User).all()
        print("users=\n{}".format(pprint.pformat(users)))
    if addresses:
        addresses = s.query(Address).all()
        print("addresses=\n{}".format(pprint.pformat(addresses)))


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState(users=True, addresses=True)
    updateAge1()
    printState(users=True)
    updateAge2()
    printState(users=True)
    updateAddress()
    printState(addresses=True)
    deleteTables()
