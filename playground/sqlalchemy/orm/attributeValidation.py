#!/usr/bin/env python3
"""
NAME
 attributeValidation.py

DESCRIPTION
 Attribute Validation Example

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/mapped_attributes.html#simple-validators
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker, validates
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    email          = Column(String(50))

    @validates("email")
    def validateEmail(self, key, address):  # pylint: disable=R0201,W0613
        """
        Validate email address
        """
        assert "@" in address
        return address


# ----------------------------- HELPERS ------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    try:
        with sessionScope() as s:
            samp = Sample(email="1234")
            s.add(samp)
            s.commit()
    except AssertionError as _:
        print("No @ in address threw assertion error")


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    deleteTables()
