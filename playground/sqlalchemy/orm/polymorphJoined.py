#!/usr/bin/env python
"""
NAME
 polymorphJoined.py

DESCRIPTION
 Joined Table [One Table per subclass] inheritance example

 Each class along a hierarchy of classes is represented by a distinct table.
 Querying for a particular subclass renders as a SQL JOIN along all tables in inheritance path
 Default behavior is to include only base table in SELECT statements

BIBLIOGRAPHY
 http://docs.sqlalchemy.org/en/latest/_modules/examples/inheritance/joined.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import Column, Integer, String, ForeignKey, create_engine, or_
from sqlalchemy.orm import relationship, with_polymorphic, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine('sqlite://', echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Company(Base):
    """
    Company Class
    """
    __tablename__ = 'company'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50))
    employees     = relationship("Person",
                                 back_populates='company',
                                 cascade='all, delete-orphan')

    def __repr__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


class Person(Base):
    """
    Person Class
    """
    __tablename__ = 'person'
    id            = Column(Integer, primary_key=True)
    company_id    = Column(ForeignKey('company.id'))
    name          = Column(String(50))
    type          = Column(String(50))
    company       = relationship("Company", back_populates="employees")

    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'person'
    }

    def __repr__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


class Engineer(Person):
    """
    Engineer Class
    """
    __tablename__    = 'engineer'
    id               = Column(ForeignKey('person.id'), primary_key=True)
    status           = Column(String(30))
    engineer_name    = Column(String(30))
    primary_language = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'engineer',
    }

    def __repr__(self):
        return "[{} name={} status={} engineer_name={} primary_language={}]".format(
            self.__class__.__name__, self.name, self.status,
            self.engineer_name, self.primary_language)


class Manager(Person):
    """
    Manager Class
    """
    __tablename__ = 'manager'
    id            = Column(ForeignKey('person.id'), primary_key=True)
    status        = Column(String(30))
    manager_name  = Column(String(30))

    __mapper_args__ = {
        'polymorphic_identity': 'manager',
    }

    def __repr__(self):
        return "[{} name={} status={} manager_name={}]".format(
            self.__class__.__name__, self.name, self.status, self.manager_name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate DB
    """
    printTitle("Populating")
    with sessionScope() as s:
        c = Company(name = 'company1', employees = [
            Manager(
                name='pointy haired boss',
                status='AAB',
                manager_name='manager1'),
            Engineer(
                name='dilbert',
                status='BBA',
                engineer_name='engineer1',
                primary_language='java'),
            Person(name='joesmith'),
            Engineer(
                name='wally',
                status='CGG',
                engineer_name='engineer2',
                primary_language='python'),
            Manager(
                name='jsmith',
                status='ABA',
                manager_name='manager2')
        ])
        s.add(c)


@logFunc()
def printState():
    """
    Print database state
    """
    printTitle("Database State")
    with sessionScope() as s:
        c = s.query(Company).get(1)
        print(c)
        for e in c.employees:
            print(e)
        print("\n")


@logFunc()
def queryPolymorphic():
    """
    Query to return polymorphic result set
    """
    printTitle("Query Polymorphic")
    with sessionScope(commit=False) as s:
        engMgrPoly = with_polymorphic(Person, [Engineer, Manager])
        engMgrs    = s.query(engMgrPoly).\
            filter(or_(engMgrPoly.Engineer.engineer_name == 'engineer1',
                       engMgrPoly.Manager.manager_name == 'manager2')).\
            all()
        print("Engineer1 or Manager2:")
        for engMgr in engMgrs:
            print(engMgr)
        print("\n")


@logFunc()
def queryPolymorphicJoin():
    """
    Query join for  company join
    """
    printTitle("Query Polymorphic Join")
    with sessionScope(commit=False) as s:
        # illustrate join from Company.
        # flat=True means the tables inside the "polymorphic join" will be aliased.
        # not strictly necessary in this example but helpful for the more general
        # case of joins involving inheritance hierarchies as well as joined eager
        # loading.
        engMgrPoly = with_polymorphic(Person, [Engineer, Manager], flat=True)
        engMgrs    = s.query(Company).\
            join(Company.employees.of_type(engMgrPoly)).\
            filter(or_(engMgrPoly.Engineer.engineer_name == 'engineer1',
                       engMgrPoly.Manager.manager_name == 'manager2')).\
            all()
        print("Companies with Engineer1 or Manager2:")
        for engMgr in engMgrs:
            print(engMgr)
        print("\n")


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    queryPolymorphic()
    queryPolymorphicJoin()
    deleteTables()
