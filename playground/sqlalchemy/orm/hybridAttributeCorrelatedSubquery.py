#!/usr/bin/env python3
"""
NAME
 hybridAttributeCorrelatedSubquery.py

DESCRIPTION
 Hybrid Attribute Correlated Subquery Demo

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/extensions/hybrid.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, select, func
from sqlalchemy.orm import sessionmaker, relationship
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class SavingsAccount(Base):  # pylint: disable=C0111
    __tablename__ = 'account'
    id            = Column(Integer, primary_key=True)
    user_id       = Column(Integer, ForeignKey('user.id'), nullable=False)
    balance       = Column(Integer)


class User(Base):  # pylint: disable=C0111
    __tablename__ = 'user'
    id            = Column(Integer, primary_key=True)
    name          = Column(String(100), nullable=False)
    accounts      = relationship("SavingsAccount", backref="owner")

    def __repr__(self):
        return "[{} name={} balance={}]".format(
            self.__class__.__name__, self.name, self.balance)

    @hybrid_property
    def balance(self):
        """
        Balance python expression
        """
        return sum(acc.balance for acc in self.accounts)

    @balance.expression
    def balance(cls):  # pylint: disable=E0213
        """
        Balance sql expression
        """
        return select([func.sum(SavingsAccount.balance)])\
            .where(SavingsAccount.user_id == cls.id)\
            .label('total_balance')


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        sa1 = SavingsAccount(balance=50)
        sa2 = SavingsAccount(balance=50)
        sa3 = SavingsAccount(balance=50)
        u   = User(name="Mara", accounts=[sa1, sa2, sa3])
        s.add(u)


@logFunc()
def printBalance(greaterThan=100):
    """
    Print balance
    """
    printTitle("Balance")
    with sessionScope() as s:
        q     = s.query(User).filter(User.balance > greaterThan)
        uList = q.all()
        print(uList)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printBalance()
    deleteTables()
