#!/usr/bin/python3
"""
NAME
 composite.py

DESCRIPTION
 Demonstrates Composite Functionality

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/composites.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from sqlalchemy.orm import sessionmaker, composite
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- POINT ------------------------------------------- #
class Point(object):
    """
    Point Class
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __composite_values__(self):
        return self.x, self.y

    def __repr__(self):
        return "[{} x={} y={}]".format(
            self.__class__.__name__, self.x, self.y)

    def __eq__(self, other):
        return isinstance(other, Point) and \
            other.x == self.x and \
            other.y == self.y

    def __ne__(self, other):
        return not self.__eq__(other)


# ------------------------- TABLES ------------------------------------------ #
class Vertex(Base):
    """
    Vertex Class
    """
    __tablename__ = 'vertex'
    id            = Column(Integer, primary_key=True)
    x1            = Column(Integer)
    y1            = Column(Integer)
    x2            = Column(Integer)
    y2            = Column(Integer)
    start         = composite(Point, x1, y1)
    end           = composite(Point, x2, y2)

    def __repr__(self):
        return "[{} start={} end={}]".format(self.__class__.__name__, self.start, self.end)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        p1     = Point(3, 4)
        p2     = Point(4, 5)
        vertex = Vertex(start=p1, end=p2)
        s.add(vertex)


@logFunc()
def printState():
    """
    Print database
    """
    printTitle("Dumping tables")
    with sessionScope() as s:
        vertexList = s.query(Vertex).all()
        for v in vertexList:
            print(v)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
