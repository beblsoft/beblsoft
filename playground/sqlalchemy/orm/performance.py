#!/usr/bin/env python3
"""
NAME
 performance.py

DESCRIPTION
 Simple SQLAlchemy Performance Testing

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/faq/performance.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import time
import cProfile
import io
import pstats
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    value          = Column(Integer)

    def __repr__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate(nPoints=100, value=1):
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        for _ in range(0, nPoints):
            samp = Sample(value=value)
            s.add(samp)


@logFunc()
def profileSimpleQuery():
    """
    Profile a simple query
    """
    printTitle("Profiling a simple query")
    with profiled():
        with sessionScope() as s:
            _ = s.query(Sample).all
            time.sleep(1)


@contextmanager
def profiled():
    """
    Yield context that will be profiled
    """
    pr = cProfile.Profile()
    pr.enable()
    yield
    pr.disable()
    s  = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats('nfl')
    ps.print_stats()
    # ps.print_callers()
    print(s.getvalue())


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    profileSimpleQuery()
    deleteTables()
