#!/usr/bin/python3
"""
NAME
 versionCounter.py

DESCRIPTION
 Demonstrates Version Counter Functionality

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/versioning.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class User(Base):
    """
    User Class
    """
    __tablename__ = 'user'
    id            = Column(Integer, primary_key=True)
    versionID     = Column(Integer, nullable=False)
    name          = Column(String(50), nullable=False)

    __mapper_args__ = {
        "version_id_col": versionID
    }

    def __repr__(self):
        return "[{} id={} versionID={} name={}]".format(
            self.__class__.__name__, self.id, self.versionID, self.name)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        u      = User(name="Billy")
        s.add(u)


@logFunc()
def update():
    """
    Update database
    """
    printTitle("Updating")
    with sessionScope() as s:
        u = s.query(User).filter(User.name == "Billy").one_or_none()
        u.name = "Johnny"


@logFunc()
def printState():
    """
    Print database state
    """
    printTitle("Database state")
    with sessionScope() as s:
        for u in s.query(User).all():
            print(u)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    update()
    printState()
    deleteTables()
