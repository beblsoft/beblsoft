#!/usr/bin/python3
"""
NAME
 events.py

DESCRIPTION
 Demonstrates Various Events

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/events.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
from contextlib import contextmanager
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import event
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- GLOBALS ------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ----------------------------- TABLES -------------------------------------- #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    name           = Column(String(64))

    def __repr__(self):
        return "[{} name={}]".format(self.__class__.__name__, self.name)


# ----------------------------- EVENTS HANDLERS ----------------------------- #
# @event.listens_for(Sample.name, 'bulk_replace')
# @event.listens_for(Sample.name, 'init_scalar')
# @event.listens_for(Sample.name, 'modified')
@event.listens_for(Sample.name, 'append')
@event.listens_for(Sample.name, 'dispose_collection')
@event.listens_for(Sample.name, 'init_collection')
@event.listens_for(Sample.name, 'remove')
@event.listens_for(Sample.name, 'set')
def receiveAttributeEvent(*args, **kwargs):  # pylint: disable=C0111
    logEvent("Attribute", args, kwargs)


# @event.listens_for(Sample, 'after_configured')
@event.listens_for(Sample, 'after_delete')
@event.listens_for(Sample, 'after_insert')
@event.listens_for(Sample, 'after_update')
@event.listens_for(Sample, 'before_delete')
@event.listens_for(Sample, 'before_insert')
@event.listens_for(Sample, 'before_update')
@event.listens_for(Sample, 'instrument_class')
@event.listens_for(Sample, 'mapper_configured')
def receiveMapperEvent(*args, **kwargs):  # pylint: disable=C0111
    logEvent("Mapper", args, kwargs)


# @event.listens_for(Sample, 'expire')
# @event.listens_for(Sample, 'first_init')
@event.listens_for(Sample, 'init')
@event.listens_for(Sample, 'init_failure')
@event.listens_for(Sample, 'load')
@event.listens_for(Sample, 'pickle')
@event.listens_for(Sample, 'refresh')
@event.listens_for(Sample, 'refresh_flush')
@event.listens_for(Sample, 'unpickle')
def receiveInstanceEvent(*args, **kwargs):  # pylint: disable=C0111
    logEvent("Instance", args, kwargs)


@event.listens_for(Sample, 'attribute_instrument')
@event.listens_for(Sample, 'class_instrument')
@event.listens_for(Sample, 'class_uninstrument')
def receiveInstrumentationEvent(*args, **kwargs):  # pylint: disable=C0111
    logEvent("Instrumentation", args, kwargs)


def registerSessionEvents(session):
    """
    Register session events
    """
    @event.listens_for(session, 'after_attach')
    @event.listens_for(session, 'after_begin')
    @event.listens_for(session, 'after_bulk_delete')
    @event.listens_for(session, 'after_bulk_update')
    @event.listens_for(session, 'after_commit')
    @event.listens_for(session, 'after_flush')
    @event.listens_for(session, 'after_flush_postexec')
    @event.listens_for(session, 'after_rollback')
    @event.listens_for(session, 'after_soft_rollback')
    @event.listens_for(session, 'after_transaction_create')
    @event.listens_for(session, 'after_transaction_end')
    @event.listens_for(session, 'before_attach')
    @event.listens_for(session, 'before_commit')
    @event.listens_for(session, 'before_flush')
    @event.listens_for(session, 'transient_to_pending')
    @event.listens_for(session, 'pending_to_persistent')
    @event.listens_for(session, 'pending_to_transient')
    @event.listens_for(session, 'loaded_as_persistent')
    @event.listens_for(session, 'persistent_to_transient')
    @event.listens_for(session, 'persistent_to_deleted')
    @event.listens_for(session, 'deleted_to_detached')
    @event.listens_for(session, 'persistent_to_detached')
    @event.listens_for(session, 'detached_to_persistent')
    @event.listens_for(session, 'deleted_to_persistent')
    def receiveSessionEvent(*args, **kwargs):  # pylint: disable=C0111,W0612
        logEvent("Session", args, kwargs)


def registerQueryEvents(query):
    """
    Register query events
    """
    @event.listens_for(query, 'before_compile', retval=True)
    def receiveQueryEvent(query):  # pylint: disable=C0111,W0612
        logEvent("Query")
        return query


def logEvent(string, args=None, kwargs=None):  # pylint: disable=C0111
    """
    Log Event
    """
    argStr    = ""
    kwargsStr = ""
    try:
        argStr    = str(args) if args else ""
        kwargsStr = str(kwargs) if kwargs else ""
    except sqlalchemy.orm.exc.DetachedInstanceError as _:
        pass
    finally:
        print("EVENT: {}: args={} kwargs={}".format(string, argStr, kwargsStr))


# ----------------------------- HELPER FUNCTIONS ---------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with sessionScope() as s:
        registerSessionEvents(s)
        samp = Sample(name="Bob")
        s.add(samp)


@logFunc()
def printState():
    """
    Print database
    """
    printTitle("Dumping Tables")
    with sessionScope() as s:
        registerSessionEvents(s)
        query = s.query(Sample)
        registerQueryEvents(query)
        sampList = query.all()
        for samp in sampList:
            print(samp)


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
