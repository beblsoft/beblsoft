#!/usr/bin/env python3
"""
NAME
 adjacencyList.py

DESCRIPTION
 Demonstrates Adjacency List Pattern

 Creates tree of related nodes, all stored on the same table:
 TreeNode(name='rootnode', id=1, parent_id=None)
    TreeNode(name='node1', id=2, parent_id=1)
       TreeNode(name='node1_1', id=5, parent_id=2)
    TreeNode(name='node2', id=3, parent_id=1)
       TreeNode(name='node2_1', id=6, parent_id=3)
    TreeNode(name='node3', id=4, parent_id=1)
       TreeNode(name='node2_2', id=7, parent_id=4)
       TreeNode(name='node3_1', id=8, parent_id=4)
       TreeNode(name='node3_2', id=9, parent_id=4)
       TreeNode(name='node3_3', id=10, parent_id=4)
       ...

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/_modules/examples/adjacency_list/adjacency_list.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy import Column, ForeignKey, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.collections import attribute_mapped_collection
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine('sqlite://', echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
class TreeNode(Base):
    """
    TreeNode table that has parents and children on same table
    """
    __tablename__ = 'tree'
    id            = Column(Integer, primary_key=True)
    parent_id     = Column(Integer, ForeignKey(id))
    name          = Column(String(50), nullable=False)

    # Relationship: 1 Tree Node: 1 Parent, N Children
    children      = relationship(
        "TreeNode",
        cascade           = "all, delete-orphan",
        # many to one + adjacency list - remote_side
        # is required to reference the 'remote'
        # column in the join condition.
        backref           = backref("parent", remote_side=id),
        # children will be represented as a dictionary
        # on the "name" attribute.
        collection_class  = attribute_mapped_collection('name'))

    def __init__(self, name, parent=None):
        self.name   = name
        self.parent = parent

    def __repr__(self):
        return "[{} name={} id={} parent_id={}]".format(
        	self.__class__.__name__,
            self.name,
            self.id,
            self.parent_id)

    def dump(self, _indent=0):
        """
        Recursively dump node and its children
        """
        s = "{}{}\n".format("   " * _indent, repr(self))
        for child in self.children.values():
            s += child.dump(_indent + 1)
        return s


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    printTitle("Populating tree")
    with sessionScope() as s:
        node    = TreeNode('rootnode')
        node1   = TreeNode('node1', parent=node)
        node2   = TreeNode('node2', parent=node)
        node3   = TreeNode('node3', parent=node)

        _       = TreeNode('node1_1', parent=node1)
        _       = TreeNode('node2_1', parent=node2)
        _       = TreeNode('node2_2', parent=node3)
        _       = TreeNode('node3_1', parent=node3)
        _       = TreeNode('node3_2', parent=node3)
        _       = TreeNode('node3_3', parent=node3)
        _       = TreeNode('node3_4', parent=node3)
        _       = TreeNode('node3_5', parent=node3)
        _       = TreeNode('node3_6', parent=node3)
        _       = TreeNode('node3_7', parent=node3)
        _       = TreeNode('node3_8', parent=node3)
        _       = TreeNode('node3_9', parent=node3)
        s.add(node)


@logFunc()
def depopulateNode(name):
    """
    Depopulate a node
    """
    printTitle("Depopulating node name={}".format(name))
    with sessionScope() as s:
        node = s.query(TreeNode).filter(TreeNode.name == name).first()
        s.delete(node)

@logFunc()
def dumpTree():
    """
    Dump the tree
    """
    printTitle("Dumping tree")
    with sessionScope(commit=False) as s:
        rootNode = s.query(TreeNode).filter(TreeNode.name == "rootnode").first()
        print(rootNode.dump())


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == '__main__':
    createTables()
    populate()
    dumpTree()
    depopulateNode("node3")
    dumpTree()
    deleteTables()
