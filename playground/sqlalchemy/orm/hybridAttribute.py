#!/usr/bin/env python3
"""
NAME
 hybridAttribute.py

DESCRIPTION
 Hybrid Attribute Demo

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/orm/extensions/hybrid.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
from contextlib import contextmanager
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from base.bebl.python.log.bLogFunc import logFunc


# ------------------------- GLOBALS ----------------------------------------- #
Base         = declarative_base()
engine       = create_engine("sqlite:///:memory:", echo=False)
sessionMaker = sessionmaker(bind=engine)
logger       = logging.getLogger(__file__)


# ------------------------- TABLES ------------------------------------------ #
class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    firstName      = Column(String(50))
    lastName       = Column(String(50))

    @hybrid_property
    def fullName(self):
        """
        Return full name
        """
        rval = None
        if self.firstName is not None:
            rval = "{} {}".format(self.firstName, self.lastName)
        else:
            rval = self.lastName
        return rval


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    Base.metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    Base.metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    with sessionScope() as s:
        samp = Sample(firstName="Mara", lastName="Bensson")
        s.add(samp)
        print("Hybrid Attribute Full Name : {}".format(samp.fullName))
        s.commit()


@contextmanager
def sessionScope(commit=True):
    """
    Yield session scope
    """
    s = sessionMaker()
    yield s
    try:
        if commit:
            s.commit()
    except Exception as e:
        s.rollback()
        logger.exception(e)
        raise e
    finally:
        s.close()


# ------------------------- MAIN -------------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    deleteTables()
