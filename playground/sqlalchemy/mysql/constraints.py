#!/usr/bin/env python3
"""
NAME
 constraints.py

DESCRIPTION
 Constraint Examples

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/constraints.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
import logging
import getpass
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, Column, Integer
# from sqlalchemy import CheckConstraint
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
metaData = MetaData(
    naming_convention = {
    # Naming convention allows all constraints to be named consistently
    # If these aren't defined, some DB's give constraints arbitrary names
    # Aribtrary names make migration difficult
        "ix": "ix_%(column_0_label)s",                                        # Index
        "uq": "uq_%(table_name)s_%(column_0_name)s",                          # Unique Constraint
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",  # Foreign Key
        "pk": "pk_%(table_name)s"                                             # Primary Key
        # "ck": "ck_%(table_name)s_%(column_0_name)s",                        # No Check on MySQL
    })
Base = declarative_base(metadata=metaData)


class User(Base):  # pylint: disable=C0111
    __tablename__ = "user"
    id            = Column(Integer, primary_key=True)
    uniqueColumn  = Column(Integer, unique=True)
    # MySQL does NOT support check constraints
    # checkColumn   = Column(Integer, CheckConstraint('checkColumn>5'))

    def __repr__(self):
        return "[{}: id={}]".format(self.__class__.__name__, self.id)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ----------------------------- HELPER FUNCTIONS ---------------------------- #
@logFunc()
def populate():
    """
    Populate database
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        u = User(uniqueColumn = 5)
        s.add(u)


@logFunc()
def tryAddDuplicateUniqueColumn():
    """
    Try to add another user with duplicate unique column value
    """
    printTitle("Trying to add row with duplicate uniqueColumn")
    try:
        with bDB.sessionScope() as s:
            u = User(uniqueColumn = 5)
            s.add(u)
    except sqlalchemy.exc.IntegrityError as e:  # pylint: disable=W0703
        print(e)


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password="", echo=False)
bDB    = Database(bServer=bDBS, name="Constraints", echo=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    tryAddDuplicateUniqueColumn()
    bDB.delete()
