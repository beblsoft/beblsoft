#!/usr/bin/env python3
"""
NAME
 foreignKeyCascades.py

DESCRIPTION
 Foreign Key Cascade Examples

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/constraints.html#defining-foreign-keys
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
import getpass
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class User(Base):  # pylint: disable=C0111
    __tablename__ = "user"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50), nullable=False)
    csnList       = relationship("ChildSetNull", passive_deletes=True)
    ccList        = relationship("ChildCascade", passive_deletes=True)

    def __repr__(self):
        return "[{}: id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class ChildSetNull(Base):  # pylint: disable=C0111
    __tablename__ = "childSetNull"
    id            = Column(Integer, primary_key=True)
    userID        = Column(Integer, ForeignKey("user.id", onupdate="SET NULL", ondelete="SET NULL"))

    def __repr__(self):
        return "[{}: id={} userID={}]".format(self.__class__.__name__, self.id, self.userID)


class ChildCascade(Base):  # pylint: disable=C0111
    __tablename__ = "childCascade"
    id            = Column(Integer, primary_key=True)
    userID        = Column(Integer, ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"))

    def __repr__(self):
        return "[{}: id={} userID={}]".format(self.__class__.__name__, self.id, self.userID)


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPER FUNCTIONS -------------------------------- #
@logFunc()
def insertData():
    """
    Add Data
    """
    with bDB.sessionScope() as s:
        u = User(name     = "ed",
                 csnList  = [ChildSetNull()],
                 ccList   = [ChildCascade()])
        s.add(u)


@logFunc()
def updateUser():
    """
    Update user
    """
    with bDB.sessionScope() as s:
        u    = s.query(User).filter(User.name == "ed").one_or_none()
        u.id = 2


@logFunc()
def deleteUser():
    """
    Delete user
    """
    with bDB.sessionScope() as s:
        s.query(User).filter(User.name == "ed").delete()


@logFunc()
def dumpTables(title):
    """
    Dump database contents
    """
    classList = [User, ChildSetNull, ChildCascade]
    printTitle(title)
    with bDB.sessionScope() as s:
        for c in classList:
            instances = s.query(c).all()
            print("{}:{}".format(c.__name__, pprint.pformat(instances)))
        print("\n")


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password= "")
bDB    = Database(bServer = bDBS, name = "ForeignKeyCascades")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    insertData()
    dumpTables(title="After inserting data")
    updateUser()
    dumpTables(title="After updating user ID")
    deleteUser()
    dumpTables(title="After deleting user")
    bDB.delete()
