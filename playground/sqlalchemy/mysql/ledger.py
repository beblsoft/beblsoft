#!/usr/bin/env python3
"""
NAME
 ledger.py

DESCRIPTION
 Ledger functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum
import logging
import getpass
from datetime import datetime, timedelta
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Enum, DateTime, func, and_
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.attrDict.bAttrDict import BeblsoftAttrDict
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class ItemType(enum.Enum):
    """
    Item Type Enum
    """
    TEXT_ANALYSIS_UNIT  = 1
    PHOTO_ANALYSIS_UNIT = 2


class LedgerType(enum.Enum):
    """
    Ledger Entry Type Enum
    """
    TRANSACTION  = 1
    HOLD         = 2


class Ledger(Base):
    """
    Ledger Class
    """
    __tablename__  = "ledger"
    id             = Column(Integer, primary_key=True)
    itemType       = Column(Enum(ItemType))
    entryType      = Column(Enum(LedgerType))
    count          = Column(Integer)
    creationDate   = Column(DateTime, default=datetime.utcnow)

    def __str__(self):
        return "[{} id={} itemType={} entryType={} count={} creationDate={}]".format(
            self.__class__.__name__, self.id, self.itemType.name, self.entryType.name,
            self.count, self.creationDate)


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- DAO --------------------------------------------- #
class LedgerDAO():
    """
    Ledger Data Access Object
    """
    @staticmethod
    def createHold(s, itemType, count):
        """
        Create a hold
        """
        s.execute("LOCK TABLES ledger WRITE")
        balance = LedgerDAO.getBalance(s, itemType=itemType)
        if balance.available < count:
            raise Exception("Available={} Desired={}".format(balance.available, count))
        hold = Ledger(itemType=itemType, entryType=LedgerType.HOLD, count=count)
        s.add(hold)
        return hold

    @staticmethod
    def delete(s, ledgerID):
        """
        Delete Object
        """
        ledger = s.query(Ledger).filter(Ledger.id == ledgerID).one_or_none()
        if ledger:
            s.delete(ledger)

    @staticmethod
    def createNegativeTransaction(s, itemType, count, holdID):
        """
        Create a negative transaction
        """
        if count >= 0:
            raise Exception("Count must be negative")

        s.execute("SELECT * from ledger where id = {} for UPDATE".format(holdID))
        hold = s.query(Ledger).filter(Ledger.id == holdID).one_or_none()

        if hold is None:
            raise Exception("Must specify hold for negative transactions")

        if hold and hold.itemType != itemType:
            raise Exception("ItemType={} HoldItemType={}".format(itemType, hold.itemType))

        newHoldCount = hold.count + count
        if newHoldCount < 0:
            raise Exception("HoldCount={} count={}".format(hold.count, count))

        hold.count = newHoldCount
        if newHoldCount == 0:
            s.delete(hold)

        transaction = Ledger(itemType=itemType, entryType=LedgerType.TRANSACTION,
                             count = count)
        s.add(transaction)

    @staticmethod
    def createPositiveTransaction(s, itemType, count):
        """
        Create a positive transaction
        """
        if count <= 0:
            raise Exception("Count must be positive")
        transaction = Ledger(itemType=itemType, entryType=LedgerType.TRANSACTION,
                             count = count)
        s.add(transaction)

    @staticmethod
    def getBalance(s, itemType, holdInterval=timedelta(days=1)):
        """
        Return balance

        Returns
          BeblsoftAttrDict
            total       # Total balance
            held        # Balance on hold
            available   # Balance available for charging
        """
        total = s.query(func.sum(Ledger.count))\
            .filter(Ledger.itemType == itemType)\
            .filter(Ledger.entryType == LedgerType.TRANSACTION)\
            .scalar()
        if not total:
            total = 0

        held = s.query(func.sum(Ledger.count))\
            .filter(Ledger.itemType == itemType)\
            .filter(and_(Ledger.entryType == LedgerType.HOLD,
                         Ledger.creationDate >= datetime.utcnow() - holdInterval))\
            .scalar()
        if not held:
            held = 0

        available = total - held
        return BeblsoftAttrDict(total=total, held=held, available=available)

    @staticmethod
    def getAll(s):
        """
        Returns
          All ledger entries
        """
        return s.query(Ledger).all()


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate():
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        LedgerDAO.createPositiveTransaction(s, ItemType.TEXT_ANALYSIS_UNIT, 20)
    printBalance()


def createHoldAndDecrement():
    """
    Create hold and then decrement
    """
    holdID = None

    printTitle("Creating Hold")
    with bDB.sessionScope() as s:
        hold = LedgerDAO.createHold(s, ItemType.TEXT_ANALYSIS_UNIT, 15)
        s.commit()
        holdID = hold.id
    printBalance()

    printTitle("Decrementing Balance")
    with bDB.sessionScope() as s:
        LedgerDAO.createNegativeTransaction(s, ItemType.TEXT_ANALYSIS_UNIT, -4, holdID)
    printBalance()

    printTitle("Removing Hold")
    with bDB.sessionScope() as s:
        LedgerDAO.delete(s, holdID)
    printBalance()


@logFunc()
def printBalance():
    """
    Print database balance
    """
    with bDB.sessionScope() as s:
        print(LedgerDAO.getBalance(s, ItemType.TEXT_ANALYSIS_UNIT))


@logFunc()
def printDatabase():
    """
    Print database
    """
    with bDB.sessionScope() as s:
        entries = LedgerDAO.getAll(s)
        for entry in entries:
            print(entry)


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password = "", echo = False)
bDB    = Database(bServer = bDBS, name = "Ledger", echo = False)

if __name__ == "__main__":
    logging.basicConfig(level = logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    createHoldAndDecrement()
    bDB.delete()
