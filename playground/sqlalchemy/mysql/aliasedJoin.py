#!/usr/bin/env python3
"""
NAME
 aliasedJoin.py

DESCRIPTION
 Aliased Join Example
 Search Facebook Post Message and Comment Text Concurrently

PICTURE
   FACEBOOKPOST        Text
     messageText------/
     commentText-----/
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import getpass
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, or_
from sqlalchemy.orm import relationship, aliased
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class FacebookPost(Base):  # pylint: disable=C0111
    __tablename__ = "facebookPost"
    id            = Column(Integer, primary_key=True)

    # Relationship 1 FBPost - 1 Message
    messageText   = relationship("Text", back_populates="messageFBPost", passive_deletes=True,
                                 uselist=True, foreign_keys="Text.messageFBPostID")

    # Relationship 1 FBPost - 1 Comment
    commentText   = relationship("Text", back_populates="commentFBPost", passive_deletes=True,
                                 uselist=True, foreign_keys="Text.commentFBPostID")

    def __repr__(self):
        return "[{}: id={} messageText={} commentText={}]".format(
            self.__class__.__name__, self.id, self.messageText, self.commentText)


class Text(Base):  # pylint: disable=C0111
    __tablename__   = "text"
    id              = Column(Integer, primary_key=True)
    string          = Column(String(64))

    # Relationship 1 FBPost - 1 Message
    messageFBPostID = Column(Integer, ForeignKey("facebookPost.id", onupdate="CASCADE", ondelete="CASCADE"))
    messageFBPost   = relationship("FacebookPost", back_populates="messageText", foreign_keys=[messageFBPostID])

    # Relationship 1 FBPost - 1 Comment
    commentFBPostID = Column(Integer, ForeignKey("facebookPost.id", onupdate="CASCADE", ondelete="CASCADE"))
    commentFBPost   = relationship("FacebookPost", back_populates="commentText", foreign_keys=[commentFBPostID])

    def __repr__(self):
        return "[{}: id={} string={}]".format(self.__class__.__name__, self.id, self.string)


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPER FUNCTIONS -------------------------------- #
@logFunc()
def populate():
    """
    Populate
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        fbPost1 = FacebookPost()
        _       = Text(messageFBPost=fbPost1, string="Hello World!")
        fbPost2 = FacebookPost()
        _       = Text(commentFBPost=fbPost2, string="GoodBye World!")
        s.add_all([fbPost1, fbPost2])


@logFunc()
def searchText(string):
    """
    Search text
    """
    printTitle("Search text \"{}\"".format(string))
    with bDB.sessionScope() as s:
        q = s.query(FacebookPost)

        # Aliases
        MessageText = aliased(Text)
        CommentText = aliased(Text)

        # Outerjoins on same table
        q = q.outerjoin(MessageText, FacebookPost.id == MessageText.messageFBPostID)
        q = q.outerjoin(CommentText, FacebookPost.id == CommentText.commentFBPostID)

        # Filters
        q = q.filter(
            or_(
                MessageText.string.ilike("%{}%".format(string)),
                CommentText.string.ilike("%{}%".format(string))
            )
        )
        fbPostList = q.all()
        print(fbPostList)


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password= "")
bDB    = Database(bServer=bDBS, name=__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    searchText(string="Hello")
    searchText(string="GoodBye")
    searchText(string="World")
    bDB.delete()
