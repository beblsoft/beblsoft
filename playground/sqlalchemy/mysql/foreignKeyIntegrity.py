#!/usr/bin/env python3
"""
NAME
 foreignKeyIntegrity.py

DESCRIPTION
 Foreign Key Integrity Example
 Try inserting with bad foreign keys

OUTPUT
 $ ./foreignKeyIntegrity.py
 INFO:__main__:Caught integrity error!
 INFO:__main__:IntegrityError(1452, 'Cannot add or update a child row: a foreign key constraint fails (`ForeignKeyIntegrity`.`childCascade`, CONSTRAINT `childCascade_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE)')
"""

# ------------------------- IMPORTS ----------------------------------------- #
import pprint
import logging
import getpass
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class User(Base):  # pylint: disable=C0111
    __tablename__ = "user"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50), nullable=False)
    ccList        = relationship("ChildCascade", passive_deletes=True)

    def __repr__(self):
        return "[{}: id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class ChildCascade(Base):  # pylint: disable=C0111
    __tablename__ = "childCascade"
    id            = Column(Integer, primary_key=True)
    userID        = Column(Integer, ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"))

    def __repr__(self):
        return "[{}: id={} userID={}]".format(self.__class__.__name__, self.id, self.userID)


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPER FUNCTIONS -------------------------------- #
@logFunc()
def insertData():
    """
    Add Data
    """
    try:
        with bDB.sessionScope() as s:
            cc = ChildCascade(userID=5)
            s.add(cc)
    except sqlalchemy.exc.IntegrityError as e:
        logger.info("Caught integrity error!")
        logger.info(pprint.pformat(e.orig))
        # logger.info(pprint.pformat(e.__dict__))


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc=lambda: "localhost", user=getpass.getuser(),
                             password="")
bDB    = Database(bServer=bDBS, name="ForeignKeyIntegrity")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    insertData()
    bDB.delete()
