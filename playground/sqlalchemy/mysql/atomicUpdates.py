#!/usr/bin/env python3
"""
NAME
 atomicUpdates.py

DESCRIPTION
 Have multiple threads concurrently update a counter.
 Use different methods to ensure concurrent access:
   - Version Counter
   - Table locking
   - Row locking
"""

# ------------------------- IMPORTS ----------------------------------------- #
import threading
import logging
import getpass
from datetime import datetime
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    count          = Column(Integer)
    versionID      = Column(Integer, nullable=False)

    __mapper_args__ = {
        "version_id_col": versionID
    }

    def __repr__(self):
        return "[{} id={} count={} versionID={}]".format(
            self.__class__.__name__, self.id, self.count, self.versionID)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate(sID=1):
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        samp = Sample(
            id     = sID,
            count  = 0)
        s.add(samp)


@logFunc()
def updaterThread_Versioning(sID=1, nIncs=100):
    """
    Update sample count, using row versioning to retry
    """
    while nIncs > 0:
        try:
            with bDB.sessionScope() as s:
                samp = s.query(Sample).filter(Sample.id == sID).one()
                samp.count = samp.count + 1
        except sqlalchemy.orm.exc.StaleDataError as e:  # pylint: disable=W0612
            # print("Hit Stale Data!")
            pass
        else:
            nIncs -= 1


@logFunc()
def updaterThread_TableLocking(sID=1, nIncs=100):
    """
    Update sample count by locking table
    """
    while nIncs > 0:
        with bDB.sessionScope() as s:
            with bDB.lockTables(s, "sample WRITE"):
                bDB.lockAssert(s, "sample WRITE")
                samp = s.query(Sample).filter(Sample.id == sID).one()
                samp.count = samp.count + 1
                s.commit()
        nIncs -= 1


@logFunc()
def updaterThread_RowLocking(sID=1, nIncs=100):
    """
    Update sample count by locking row
    """
    while nIncs > 0:
        with bDB.sessionScope() as s:
            s.execute("SELECT * from sample where id = {} for UPDATE".format(sID))
            samp = s.query(Sample).filter(Sample.id == sID).one()
            samp.count = samp.count + 1
            s.commit()
        nIncs -= 1


@logFunc()
def printState(sID=1):
    """
    Print database state
    """
    printTitle("Database state")
    with bDB.sessionScope() as s:
        samp = s.query(Sample).filter(Sample.id == sID).one()
        print(samp)


# ----------------------------- MAIN ---------------------------------------- #
logger          = logging.getLogger(__name__)
bDBS            = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                                      user = getpass.getuser(), password="", echo=False)
bDB             = Database(bServer=bDBS, name="AtomicUpdates", echo=False)
sampID          = 1
nThreads        = 10
nThreadIncs     = 100
updaterFuncList = [updaterThread_Versioning, updaterThread_TableLocking, updaterThread_RowLocking]

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate(sID=sampID)

    for func in updaterFuncList:
        printTitle("{} nThreads={}, nIncs={}".format(func.__name__, nThreadIncs, nThreads * nThreadIncs))
        timeStart = datetime.now()
        tList = []
        for _ in range(nThreads):
            t = threading.Thread(target=func, kwargs={'sID': sampID, 'nIncs': nThreadIncs})
            t.start()
            tList.append(t)
        for t in tList:
            t.join()
        timeEnd = datetime.now()
        print("Time={}".format(timeEnd - timeStart))
        printState(sID=sampID)

    bDB.delete()
