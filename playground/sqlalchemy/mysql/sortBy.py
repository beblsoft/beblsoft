#!/usr/bin/env python3
"""
NAME
 sortBy.py

DESCRIPTION
 Demonstrate sorting a data set off unique and nonunique attributes
"""

# ------------------------- IMPORTS ----------------------------------------- #
import random
import enum
import logging
import getpass
import operator
from datetime import datetime, timedelta
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Enum, DateTime
from sqlalchemy.sql import or_, and_, asc, desc
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class SampleEnum(enum.Enum):
    """
    Sample Enum
    """
    FOO = 1
    BAR = 2
    CAR = 3
    FAR = 4


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    enum           = Column(Enum(SampleEnum), default=SampleEnum.FOO)
    integer        = Column(Integer)
    dateTime       = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "[{} id={} enum={} integer={} dateTime={}]".format(
            self.__class__.__name__, str(self.id).ljust(3), self.enum.name.ljust(4),
            str(self.integer).ljust(4), self.dateTime)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ----------------------------- SORTING CLASSES ----------------------------- #
class SortOrder(enum.Enum):  # pylint: disable=C0111
    """
    Sort Order
    """
    ASCENDING  = 1
    DESCENDING = 0

    @property
    def func(self):
        """
        Return corresponding sqlalchemy func
        """
        funcDict = {
            SortOrder.ASCENDING: asc,
            SortOrder.DESCENDING: desc,
        }
        return funcDict[self]

    @property
    def nextCompOperator(self):
        """
        Next Comparison Operator
        Return operator which must return True for next element in the set
        Assumes all elements are unique

        Ex. SortOrder.ASCENDING
            [1, 2, 3, 4, 5]
            Returns operator.__gt__ as 2 > 1
        """
        compOperatorDict = {
            SortOrder.ASCENDING: operator.__gt__,
            SortOrder.DESCENDING: operator.__lt__
        }
        return compOperatorDict[self]


class SortByID():
    """
    Sort by Sample.id, a unique field
    """

    def __init__(self, idSortOrder=SortOrder.ASCENDING,
                 lastID=None):
        """
        Initialize object
        """
        self.lastID              = lastID
        self.idSortOrder         = idSortOrder
        self.idNextCompOp        = idSortOrder.nextCompOperator
        self.idSortFunc          = idSortOrder.func

    @property
    def filterList(self):
        """
        Get list of filters
        """
        filterList = []
        if self.lastID:
            filterList.append(self.idNextCompOp(Sample.id, self.lastID))
        return filterList

    @property
    def orderByArgs(self):
        """
        Return list of sqlalchemy.order_by arguments
        Passed to order_by as:
          order_by(*args)
        """
        return [self.idSortFunc(Sample.id)]


class SortByDateTime():
    """
    Sort by Sample.dateTime, a non-unique field
    To make it unique, we sort by (dateTime, id)
    """

    def __init__(self, dateTimeSortOrder, idSortOrder=SortOrder.ASCENDING,
                 lastDateTime=None, lastID=None):
        """
        Initialize object
        """
        self.lastDateTime        = lastDateTime
        self.lastID              = lastID
        self.dateTimeSortOrder   = dateTimeSortOrder
        self.dateTimeNextCompOp  = dateTimeSortOrder.nextCompOperator
        self.dateTimeSortFunc    = dateTimeSortOrder.func
        self.idSortOrder         = idSortOrder
        self.idNextCompOp        = idSortOrder.nextCompOperator
        self.idSortFunc          = idSortOrder.func

    @property
    def filterList(self):
        """
        Get list of filters
        """
        filterList = []
        if self.lastDateTime:
            filterList.append(
                or_(
                    and_(Sample.dateTime == self.lastDateTime, self.idNextCompOp(Sample.id, self.lastID)),
                    self.dateTimeNextCompOp(Sample.dateTime, self.lastDateTime)
                )
            )
        return filterList

    @property
    def orderByArgs(self):
        """
        Return list of sqlalchemy.order_by arguments
        Passed to order_by as:
          order_by(*args)
        """
        return [self.dateTimeSortFunc(Sample.dateTime), self.idSortFunc(Sample.id)]


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate(nPoints=20, minInt=0, maxInt=100, secondsBackMax=5):
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        for _ in range(0, nPoints):
            samp = Sample(
                enum     = random.choice(list(SampleEnum)),
                integer  = random.randint(minInt, maxInt),
                dateTime = (datetime.utcnow() -
                            timedelta(seconds = random.randint(0, secondsBackMax))))
            s.add(samp)


@logFunc()
def getAll(sortBy, limit=10):
    """
    Get all
    """
    with bDB.sessionScope(commit=False) as session:
        q = session.query(Sample)

        # Sort By
        if sortBy:
            for f in sortBy.filterList:
                q = q.filter(f)
            q = q.order_by(*sortBy.orderByArgs)

        # Limit
        q = q.limit(limit)
        return q.all()


@logFunc()
def paginateID(idSortOrder=SortOrder.ASCENDING, limit=1):
    """
    Paginate ID
    """
    loop    = True
    sortBy  = SortByID(idSortOrder=idSortOrder)
    counter = 0

    printTitle("Paginating ID")
    while loop:
        sampleList = getAll(sortBy=sortBy, limit=limit)

        print(counter, sampleList)
        if len(sampleList) < limit:
            loop = False
        else:
            lastSample = sampleList[-1]
            sortBy = SortByID(idSortOrder=idSortOrder, lastID=lastSample.id)
        counter   += 1


@logFunc()
def paginateDateTime(dateTimeSortOrder=SortOrder.ASCENDING, limit=1):
    """
    Paginate datetime
    """
    loop    = True
    sortBy  = SortByDateTime(dateTimeSortOrder=dateTimeSortOrder)
    counter = 0

    printTitle("Paginating DateTime")
    while loop:
        sampleList = getAll(sortBy=sortBy, limit=limit)

        print(counter, sampleList)
        if len(sampleList) < limit:
            loop = False
        else:
            lastSample = sampleList[-1]
            sortBy = SortByDateTime(dateTimeSortOrder=dateTimeSortOrder, lastDateTime=lastSample.dateTime,
                                    lastID=lastSample.id)
        counter   += 1


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password="", echo=False)
bDB    = Database(bServer=bDBS, name="SortBy", echo=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    paginateID()
    paginateDateTime()
    bDB.delete()
