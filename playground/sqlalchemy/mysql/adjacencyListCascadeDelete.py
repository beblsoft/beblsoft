#!/usr/bin/env python3
"""
NAME
 adjacencyListCascadeDelete.py

DESCRIPTION
 Demonstrates Adjacency List Pattern with Cascade Deletion

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/_modules/examples/adjacency_list/adjacency_list.html
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import getpass
from sqlalchemy import Column, ForeignKey, Integer, Boolean, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ------------------------- GLOBALS ----------------------------------------- #
Base   = declarative_base()
logger = logging.getLogger(__name__)


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class TwitterTweet(Base):
    """
    Twitter Tweet Table
    """
    __tablename__                = 'twitter_tweet'
    id                           = Column(Integer, primary_key=True)
    text                         = Column(String(64))
    topLevel                     = Column(Boolean, default=False)
    replyToIDStr                 = Column(String(64))

    # Relationship: 1 Tweet : 1 Retweeted Tweet
    # Retweeted Tweet stores ID of Tweet. This allows the Retweeted Tweet to be
    # cascade deleted when the Tweet is deleted.
    retweetedByID                = Column(Integer, ForeignKey(id, onupdate="CASCADE", ondelete="CASCADE"))
    retweetedByTwitterTweetModel = relationship("TwitterTweet", foreign_keys=[retweetedByID], remote_side=[id])
    retweetedTwitterTweetModel   = relationship("TwitterTweet", foreign_keys=[retweetedByID],
                                                back_populates="retweetedByTwitterTweetModel",
                                                cascade="all, delete-orphan", passive_deletes=True, uselist=False)

    # Relationship: 1 Tweet : 1 Quoted Tweet
    # Quoted Tweet stores ID of Tweet. This allows the Quoted Tweet to be cascade
    # deleted when the Tweet is deleted.
    quotedByID                   = Column(Integer, ForeignKey(id, onupdate="CASCADE", ondelete="CASCADE"))
    quotedByTwitterTweetModel    = relationship("TwitterTweet", foreign_keys=[quotedByID], remote_side=[id])
    quotedTwitterTweetModel      = relationship("TwitterTweet", foreign_keys=[quotedByID],
                                                back_populates="quotedByTwitterTweetModel",
                                                cascade="all, delete-orphan", passive_deletes=True, uselist=False)

    def __repr__(self):
        return "[{} id={} text='{}'{}{}]".format(
            self.__class__.__name__, self.id, self.text,
            " retweetedByID={}".format(self.retweetedByID) if self.retweetedByID else "",
            " quotedByID={}".format(self.quotedByID) if self.quotedByID else "")

    def dump(self, _action="", _indent=0):
        """
        Recursively dump node and its children
        """
        s = "{}{}{}\n".format("   " * _indent, _action, repr(self))
        if self.retweetedTwitterTweetModel:
            s += self.retweetedTwitterTweetModel.dump(_action="Retweeted", _indent=_indent + 1)
        if self.quotedTwitterTweetModel:
            s += self.quotedTwitterTweetModel.dump(_action="Quoted", _indent=_indent + 1)
        return s


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate():
    """
    Populate tables
    """
    printTitle("Populating tweets")
    with bDB.sessionScope() as s:
        # Regular Tweet
        tweet          = TwitterTweet(text='regular tweet', topLevel=True)
        s.add(tweet)

        # Reply Tweet
        tweet          = TwitterTweet(text='reply tweet', replyToIDStr="12987123987123129387", topLevel=True)
        s.add(tweet)

        # Quote Tweet
        quotedTweet    = TwitterTweet(text='quoted tweet')
        tweet          = TwitterTweet(text='quoting tweet', topLevel=True, quotedTwitterTweetModel=quotedTweet)
        s.add(tweet)

        # Retweet Tweet
        retweetedTweet = TwitterTweet(text='retweeted tweet')
        tweet          = TwitterTweet(text='retweeting tweet', topLevel=True, retweetedTwitterTweetModel=retweetedTweet)
        s.add(tweet)

        # Retweet Quote Tweet
        quotedTweet    = TwitterTweet(text='quoted tweet')
        retweetedTweet = TwitterTweet(text='retweeted tweet', quotedTwitterTweetModel=quotedTweet)
        tweet          = TwitterTweet(text='retweeting quoting tweet', topLevel=True,
                                      retweetedTwitterTweetModel=retweetedTweet)
        s.add(tweet)


@logFunc()
def testCascadeDelete():
    """
    Test Cascade Deletion
    """
    printTitle("Testing Cascade Delete")
    tweetID     = None
    retweetedID = None
    quotedID    = None

    with bDB.sessionScope() as s:
        tweet          = s.query(TwitterTweet).filter(TwitterTweet.text == "retweeting quoting tweet").first()
        tweetID        = tweet.id
        retweetedTweet = tweet.retweetedTwitterTweetModel
        retweetedID    = retweetedTweet.id
        quotedTweet    = retweetedTweet.quotedTwitterTweetModel
        quotedID       = quotedTweet.id
        print(tweet.dump())
        s.delete(tweet)

    with bDB.sessionScope() as s:
        tweet          = s.query(TwitterTweet).filter(TwitterTweet.id == tweetID).one_or_none()
        retweetedTweet = s.query(TwitterTweet).filter(TwitterTweet.id == retweetedID).one_or_none()
        quotedTweet    = s.query(TwitterTweet).filter(TwitterTweet.id == quotedID).one_or_none()
        print(tweet, retweetedTweet, quotedTweet)


@logFunc()
def dumpTweets():
    """
    Dump the tweets
    """
    printTitle("Dumping Tweets")
    with bDB.sessionScope(commit=False) as s:
        for twitterTweetModel in  s.query(TwitterTweet):
            print(twitterTweetModel.dump())


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc=lambda: "localhost", user=getpass.getuser(), password="")
bDB    = Database(bServer=bDBS, name="AdjacencyListCascadeDelete", echo=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    dumpTweets()
    testCascadeDelete()
    bDB.delete()
