#!/usr/bin/env python3
"""
NAME
 multiCascadeDelete.py

DESCRIPTION
 Multiple Cascade Delete Example

PICTURE
  User      Company
     1      1
     |     /
     |    /
     n   n
     Post

NOTES
 Model assumes that post is associated with either User OR Company, but not both.
 If either the User or Company is deleted, the post will also be deleted.
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import pprint
import getpass
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class User(Base):  # pylint: disable=C0111
    __tablename__ = "user"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50), nullable=False)

    # Relationship: 1 User - N Posts
    postList      = relationship("Post", back_populates="user", passive_deletes=True)

    def __repr__(self):
        return "[{}: id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class Company(Base):  # pylint: disable=C0111
    __tablename__ = "company"
    id            = Column(Integer, primary_key=True)
    name          = Column(String(50), nullable=False)

    # Relationship: 1 User - N User Text
    postList      = relationship("Post", back_populates="company", passive_deletes=True)

    def __repr__(self):
        return "[{}: id={} name={}]".format(
            self.__class__.__name__, self.id, self.name)


class Post(Base):  # pylint: disable=C0111
    __tablename__   = "post"
    id              = Column(Integer, primary_key=True)
    string          = Column(String(64))

    # Relationship: 1 User - N Posts
    userID          = Column(Integer, ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"))
    user            = relationship("User", back_populates="postList")

    # Relationship: 1 Company - N Posts
    companyID       = Column(Integer, ForeignKey("company.id", onupdate="CASCADE", ondelete="CASCADE"))
    company         = relationship("Company", back_populates="postList")

    def __repr__(self):
        return "[{}: id={} string={} userID={} companyID={}]".format(
            self.__class__.__name__, self.id, self.string, self.userID, self.companyID)


# ------------------------- DATABASE ---------------------------------------- #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPER FUNCTIONS -------------------------------- #
@logFunc()
def populate(nTextPer=2):
    """
    Populate
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        u = User(name = "ed")
        c = Company(name = "Beblsoft")
        for i in range(nTextPer):
            up  = Post(string = str(i))
            cp  = Post(string = str(i))
            ucp = Post(string = str(i))
            u.postList += [up, ucp]
            c.postList += [cp, ucp]
        s.add(u)
        s.add(c)

@logFunc()
def deleteUser():
    """
    Delete user
    """
    printTitle("Deleting User")
    with bDB.sessionScope() as s:
        s.query(User).filter(User.name == "ed").delete()


@logFunc()
def dumpTables():
    """
    Dump database contents
    """
    classList = [User, Company, Post]
    printTitle("Dumping Tables")
    with bDB.sessionScope() as s:
        for c in classList:
            instances = s.query(c).all()
            print("{}:{}".format(c.__name__, pprint.pformat(instances)))
        print("\n")


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password= "")
bDB    = Database(bServer = bDBS, name = "multiCascadeDelete")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    dumpTables()
    deleteUser()
    dumpTables()
    bDB.delete()
