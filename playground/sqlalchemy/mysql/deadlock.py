#!/usr/bin/env python3
"""
NAME
 deadlock.py

DESCRIPTION
 Demonstrate a deadlock scenario
"""

# ------------------------- IMPORTS ----------------------------------------- #
import time
import threading
import logging
import getpass
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    count          = Column(Integer)

    def __repr__(self):
        return "[{} id={} count={}]".format(
            self.__class__.__name__, self.id, self.count)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate(sIDList=1):
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        for sID in sIDList:
            samp = Sample(id=sID, count=0)
            s.add(samp)


@logFunc()
def lockerThread(sIDList, sleepS=2):
    """
    Lock ids in sIDList in order
    """
    try:
        with bDB.sessionScope() as s:
            for sID in sIDList:
                logger.info("Trying to lock sID={}".format(sID))
                s.execute("SELECT * from sample where id = {} for UPDATE".format(sID))
                logger.info("Acquired lock  sID={}".format(sID))
                time.sleep(sleepS)
    except sqlalchemy.exc.OperationalError as e:
        logger.info("Caught deadlock!")
        logger.info(e.__dict__)


@logFunc()
def printState(sID=1):
    """
    Print database state
    """
    printTitle("Database state")
    with bDB.sessionScope() as s:
        samp = s.query(Sample).filter(Sample.id == sID).one()
        print(samp)


# ----------------------------- MAIN ---------------------------------------- #
logger          = logging.getLogger(__name__)
bDBS            = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                                      user = getpass.getuser(), password="", echo=False)
bDB             = Database(bServer=bDBS, name="Deadlock", echo=False)
sampID          = 1
nThreads        = 2
nThreadIncs     = 100
sampIDList      = [1, 2]
tList           = []

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(threadName)8s: %(message)s",)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate(sIDList=sampIDList)

    printTitle("Spawning threads to deadlock")
    for i in range(nThreads):
        kwargs = {'sIDList': sampIDList} if i % nThreads == 0 else {'sIDList': reversed(sampIDList)}
        t = threading.Thread(target=lockerThread, kwargs=kwargs)
        t.start()
        tList.append(t)
    for t in tList:
        t.join()

    bDB.delete()
