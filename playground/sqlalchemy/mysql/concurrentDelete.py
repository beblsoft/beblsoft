#!/usr/bin/env python3
"""
NAME
 concurrentDelete.py

DESCRIPTION
 Demonstrate a thread trying to update a deleted row
 To handle this, async threads must catch StaleDataErrors
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import threading
import getpass
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    integer        = Column(Integer)

    def __repr__(self):
        return "[{} id={} integer={}]".format(
            self.__class__.__name__, self.id, self.integer)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate():
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        samp = Sample(id=1, integer=1)
        s.add(samp)


@logFunc()
def deleterThread():
    """
    Delete
    """
    printTitle("Deleting")
    with bDB.sessionScope() as s:
        samp = s.query(Sample).filter(Sample.id == 1).one()
        s.delete(samp)


@logFunc()
def updaterThread():
    """
    Updater Thread
    """
    printTitle("Update enter")
    try:
        with bDB.sessionScope() as s:
            samp = s.query(Sample).filter(Sample.id == 1).one()
            dt = threading.Thread(target=deleterThread)
            dt.start()
            dt.join()
            printTitle("Trying to update...")
            samp.integer = 2
    except sqlalchemy.orm.exc.StaleDataError as e:
        print(e)
        print("\n!!!!!!Caught StaleDataError!!!!!\n")


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password="", echo=False)
bDB    = Database(bServer=bDBS, name="ConcurrentDelete", echo=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()
    ut = threading.Thread(target=updaterThread)
    ut.start()
    ut.join()
    bDB.delete()
