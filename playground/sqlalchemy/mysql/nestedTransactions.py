#!/usr/bin/env python3
"""
NAME
 nestedTransactions.py

DESCRIPTION
 Nested Transaction functionality
"""

# ------------------------- IMPORTS ----------------------------------------- #
import logging
import getpass
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    integer        = Column(Integer)

    def __repr__(self):
        return "[{} id={} integer={}]".format(
            self.__class__.__name__, self.id, self.integer)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def nestedTransactionRollback():
    """
    Do nested transaction but rollback changes
    """
    printTitle("Nested Transaction Rollback")
    with bDB.sessionScope() as s:

        with bDB.transactionScope(s, nested=True):
            samp = Sample(integer=3)
            s.add(samp)

            with bDB.transactionScope(s, nested=True):
                samp = Sample(integer=4)
                s.add(samp)
        s.rollback()


@logFunc()
def nestedTransactionSuccess():
    """
    Do nested transaction successfully
    """
    printTitle("Nested Transaction Success")
    with bDB.sessionScope() as s:

        with bDB.transactionScope(s, nested=True):
            samp = Sample(integer=1)
            s.add(samp)

            with bDB.transactionScope(s, nested=True):
                samp = Sample(integer=2)
                s.add(samp)
        s.commit()


@logFunc()
def dumpTables():
    """
    Dump database contents
    """
    printTitle("Dumping tables")
    with bDB.sessionScope() as s:
        sampList = s.query(Sample).all()
        print(sampList)


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password = "", echo = False)
bDB    = Database(bServer = bDBS, name = "NestedTransactions", echo = False)

if __name__ == "__main__":
    logging.basicConfig(level = logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    nestedTransactionRollback()
    dumpTables()
    nestedTransactionSuccess()
    dumpTables()
    bDB.delete()
