#!/usr/bin/env python3
"""
NAME
 groupBy.py

DESCRIPTION
 Demonstrate grouping a data set

FURTHER EXAMPLES
  smeckn/server/aDB/groupBy/*
  smeckn/server/aDB/histogram/*
  smeckn/server/aDB/statistic/*
"""

# ------------------------- IMPORTS ----------------------------------------- #
import pprint
import random
import enum
import logging
import getpass
from datetime import datetime, timedelta
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Enum, DateTime
from sqlalchemy.sql import func
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.print.bTitle import printTitle


# ------------------------- TABLES ------------------------------------------ #
Base = declarative_base()


class SampleEnum(enum.Enum):
    """
    Sample Enum
    """
    FOO = 1
    BAR = 2
    CAR = 3
    FAR = 4


class Sample(Base):
    """
    Sample Class
    """
    __tablename__  = "sample"
    id             = Column(Integer, primary_key=True)
    enum           = Column(Enum(SampleEnum), default=SampleEnum.FOO)
    integer        = Column(Integer)
    dateTime       = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "[{} id={} enum={} integer={} string={} dateTime={}]".format(
            self.__class__.__name__, str(self.id).ljust(3), self.enum.name.ljust(4),
            str(self.integer).ljust(4), self.string.ljust(40), self.dateTime)

    @staticmethod
    def getGroupBy(groupBy):
        """
        Return sqlalchemy group by statement
        """
        if groupBy is GroupBy.ENUM:
            rval = Sample.enum
        else:
            rval = func.date_format(Sample.dateTime, groupBy.getMySQLStrfTime())
        return rval


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)


class GroupBy(enum.Enum):
    """
    Quantities to group by
    """
    ENUM   = enum.auto()
    SECOND = enum.auto()
    MINUTE = enum.auto()
    HOUR   = enum.auto()
    DAY    = enum.auto()
    MONTH  = enum.auto()
    YEAR   = enum.auto()

    def getMySQLStrfTime(self):
        """
        Return str format time
        """
        strfTimeDict = {
            GroupBy.SECOND: "%Y-%m-%d %H:%i:%s",
            GroupBy.MINUTE: "%Y-%m-%d %H:%i",
            GroupBy.HOUR: "%Y-%m-%d %H",
            GroupBy.DAY: "%Y-%m-%d",
            GroupBy.MONTH: "%Y-%m",
            GroupBy.YEAR: "%Y"
        }
        return strfTimeDict[self]

    @staticmethod
    def getFromTimeInterval(startTime, endTime):
        """
        Return GroupBy from time interval
        """
        delta = endTime - startTime
        rval  = None

        if delta < timedelta(seconds = 120):
            rval = GroupBy.SECOND
        elif delta < timedelta(minutes = 120):
            rval = GroupBy.MINUTE
        elif delta < timedelta(hours = 48):
            rval = GroupBy.HOUR
        elif delta < timedelta(days = 120):
            rval = GroupBy.DAY
        elif delta < timedelta(weeks = 52 * 3):
            rval = GroupBy.MONTH
        else:
            rval = GroupBy.YEAR
        return rval


# ------------------------- HELPERS ----------------------------------------- #
@logFunc()
def populate(nPoints=500, minInt=0, maxInt=100,
             secondsBackMax = 60 * 60 * 24 * 365 * 5):
    """
    Populate tables
    """
    printTitle("Populating")
    with bDB.sessionScope() as s:
        for _ in range(0, nPoints):
            samp = Sample(
                enum       = random.choice(list(SampleEnum)),
                integer    = random.randint(minInt, maxInt),
                dateTime   = (datetime.utcnow() -
                              timedelta(seconds = random.randint(0, secondsBackMax))))
            s.add(samp)

@logFunc()
def getIntegerHistogram(startDate, endDate, groupBy):  # pylint: disable=W0621
    """
    Get Integer Histogram
    """
    print("Histogram")

    with bDB.sessionScope() as s:
        q = s.query(Sample.getGroupBy(groupBy),
                    func.count(Sample.integer),
                    func.avg(Sample.integer),
                    func.min(Sample.integer),
                    func.max(Sample.integer),
                    func.stddev(Sample.integer))
        q = q.filter(Sample.dateTime >= startDate)
        q = q.filter(Sample.dateTime <= endDate)
        q = q.group_by(Sample.getGroupBy(groupBy))
        result = q.all()
        print(pprint.pformat(result))


@logFunc()
def getIntegerStats(startDate, endDate):  # pylint: disable=W0621
    """
    Get Integer Statistics
    """
    print("Stats")
    with bDB.sessionScope() as s:
        q = s.query(func.count(Sample.integer),
                    func.avg(Sample.integer),
                    func.min(Sample.integer),
                    func.max(Sample.integer),
                    func.stddev(Sample.integer))
        q = q.filter(Sample.dateTime >= startDate)
        q = q.filter(Sample.dateTime <= endDate)
        result = q.all()
        print(pprint.pformat(result))


# ----------------------------- MAIN ---------------------------------------- #
logger = logging.getLogger(__name__)
bDBS   = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password="", echo=False)
bDB    = Database(bServer=bDBS, name="GroupBy", echo=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    bDB.delete()
    bDB.create()
    bDB.createTables()
    populate()

    intervals = [(datetime(2000, 1, 1), datetime.utcnow()),
                 (datetime(2018, 1, 1), datetime.utcnow()),
                 (datetime(2018, 12, 1), datetime.utcnow()),
                 (datetime(2018, 12, 17), datetime.utcnow())]

    for interval in intervals:
        startDate = interval[0]
        endDate   = interval[1]
        # Group by date
        gb        = GroupBy.getFromTimeInterval(startDate, endDate)
        printTitle("Group By {}: Start={} End={}".format(gb.name, startDate, endDate))
        getIntegerHistogram(startDate, endDate, groupBy=gb)
        getIntegerStats(startDate, endDate)

        # Group by enum
        gb = GroupBy.ENUM
        printTitle("Group By {}: Start={} End={}".format(gb.name, startDate, endDate))
        getIntegerHistogram(startDate, endDate, groupBy=gb)

    bDB.delete()
