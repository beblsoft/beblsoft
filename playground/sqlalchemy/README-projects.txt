OVERVIEW
===============================================================================
SQLAlchemy Projects Documentation



DIRECTORY: CORE
===============================================================================
- Directory                         : ./core
- Description                       : Single file SQLAlchemy Core applications
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- tutorial.py --------------------- : Core Tutorial
  * Description                     : Demonstrate table creation, deletion, row insertion, selection
  * Run                             : ./tutorial.py

- columnDefaults.py --------------- : Column Default Demo
  * Description                     : Example setting column defaults for insert and update
  * Run                             : ./columnDefaults.py

- pool.py ------------------------- : Connection Pool Demo
  * Description                     : Example creating engines with different connection pool objects
  * Run                             : ./pool.py



DIRECTORY: ORM
===============================================================================
- Directory                         : ./orm
- Description                       : Single file SQLAlchemy ORM applications
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- tutorial.py --------------------- : ORM Tutorial
  * Description                     : Demonstrate One-to-Many, and Many-to-Many Relationships
  * Run                             : ./tutorial.py

- columnTypes.py ------------------ : Different column types in use
  * Description                     : Demonstrate core sqlalchemy types: Integer, String, BigInteger,
                                      Date, Boolean, DateTime, Unicode, UnicodeText, ...
  * Run                             : ./columnTypes.py

- update.py ----------------------- : Update Demo
  * Description                     : Demonstrate updating various database records
  * Run                             : ./update.py

- polymorphSingle.py -------------- : Inheritance in single table
  * Run                             : ./polymorphSingle.py

- polymorphJoined.py -------------- : Inheritance in multiple tables
  * Run                             : ./polymorphJoined.py

- inspector.py -------------------- : Inspect SQLAlchemy metadata
  * Run                             : ./inspector.py

- hybridAttribute.py -------------- : Hybrid attribute Example
  * Description                     : A hybrid attribute is combination of other class properties
  * Run                             : ./hybridAttribute.py

- attributeValidation.py ---------- : Attribute validation example
  * Run                             : ./attributeValidation.py

- composite.py -------------------- : Composite example
  * Description                     : Maps a point(x,y) object to vertex table
  * Run                             : ./composite.py

- versionCounter.py --------------- : Version Counter example
  * Description                     : Increment a version counter any time an object is updated
  * Run                             : ./versionCounter.py

- relaOneToOne.py ----------------- : One To One Relationship Demo
  * Description                     : Demonstrate populating and using a one to one relationship
  * Run                             : ./relaOneToOne.py

- relaOneToMany.py ---------------- : One To Many Relationship Demo
  * Description                     : Demonstrate populating and using a one to many relationship
  * Run                             : ./relaOneToMany.py

- relaManyToOne.py ---------------- : Many To One Relationship Demo
  * Description                     : Demonstrate populating and using a many to one relationship
  * Run                             : ./relaManyToOne.py

- relaManyToMany.py --------------- : Many To Many Relationship Demo
  * Description                     : Demonstrate populating and using a many to many relationship
  * Run                             : ./relaManyToMany.py

- relaManyToManyAssoc.py ---------- : Many To Many Relationship with Association Table Demo
  * Description                     : Demonstrate populating and using a many to many relationship with association table
  * Run                             : ./relaManyToManyAssoc.py

- events.py ----------------------- : Events Demo
  * Description                     : Demonstrate handling various SQLAlchemy ORM Events
  * Run                             : ./events.py

- reconstructor.py ---------------- : Reconstructor Demo
  * Description                     : Demonstrate how to "reconstruct" or repopulate an object after it has been loaded from the database
  * Run                             : ./reconstructor.py

- loadingColumns.py --------------- : Column Loading Demo
  * Description                     : Demonstrate how to eagerly and lazily load columns
  * Run                             : ./loadingColumns.py

- loadingRelationships.py --------- : Relationships Loading Demo
  * Description                     : Demonstrate how to eagerly and lazily load relationships
  * Run                             : ./loadingRelationships.py

- adjacencyList.py ---------------- : Adjacency List Demo
  * Description                     : Demonstrate the adjacency list pattern. I.e. Having parents and children on same table
  * Run                             : ./adjacencyList.py

- math.py ------------------------- : Math Demo
  * Description                     : Perform various math operations (min, max, count, sum, avg) on a
                                      set of data over various time intervals
  * Run                             : ./math.py

- performance.py ------------------ : Performance Demo
  * Description                     : Profile a simple query and display function duration results
  * Run                             : ./performance.py

- search.py ----------------------- : Search Demo
  * Description                     : Demonstrate searching and grouping a sample table
  * Run                             : ./search.py

- refresh.py ---------------------- : Refresh Demo
  * Description                     : Demonstrate refreshing an object in the session after it has been changed
  * Run                             : ./refresh.py



DIRECTORY: MYSQL
===============================================================================
- Directory                         : ./mysql
- Description                       : Single file SQLAlchemy MySQL applications
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- foreignKeyCascades.py ----------- : Foreign Key Cascade Examples
  * Description                     : Example with onupdate,ondelete "SET NULL" and "CASCADE" foreign keys
  * Run                             : ./foreignKeyCascades.py

- foreignKeyIntegrity.py ---------- : Foreign Key Integrity Example
  * Description                     : Example trying to insert with bad foreign key and catching error
  * Run                             : ./foreignKeyIntegrity.py

- multiCascadeDelete.py ----------- : Multiple Cascade Delete Example
  * Description                     : Example with 2 one-to-many relationships, both cascade deleting
  * Run                             : ./multiCascadeDelete.py

- constraints.py ------------------ : Contstraint examples
  * Run                             : ./constraints.py

- concurrentDelete.py ------------- : Contstraint examples
  * Description                     : Demonstrate a thread attempting to update a row that has since been deleted
  * Run                             : ./concurrentDelete.py

- groupBy.py ---------------------- : Group by example
  * Description                     : Demonstrate using group by clause to generate histograms
  * Run                             : ./groupBy.py

- sortBy.py ----------------------- : Sort by example
  * Description                     : Demonstrate sorting, paginating off unique and non-unique keys
  * Run                             : ./sortBy.py

- atomicUpdates.py ---------------- : Atomic Update Examples
  * Description                     : Demonstrate concurrent updates via: versions, table locking, row locking
  * Run                             : ./atomicUpdates.py

- atomicNestedUpdates.py ---------- : Atomic Nested Update Examples
  * Description                     : Demonstrate concurrent updates and nested transactions
  * Run                             : ./atomicNestedUpdates.py

- deadlock.py --------------------- : Deadlock example
  * Description                     : Demonstrate two threads locking rows out of order and creating a deadlock
  * Run                             : ./deadlock.py

- ledger.py ----------------------- : Ledger example
  * Description                     : Demonstrate adding and deleting transactions and holds from a ledger
  * Run                             : ./ledger.py

- nestedTransactions.py ----------- : Nested Transaction example
  * Description                     : Demonstrate use of nested transactions (transaction inside transaction)
                                      Note: nested transactions aren't commited until their parent transactions are
  * Run                             : ./nestedTransactions.py

- aliasedJoin.py ------------------ : Aliased Join example
  * Description                     : Demonstrate joining on the same table twice using aliases
  * Run                             : ./aliasedJoin.py

- adjacencyListCascadeDelete.py --- : Adjacency List Pattern with Cascade Deletion
  * Description                     : Demonstrate twitter tweets refering to one another inside the same table
  * Run                             : ./adjacencyListCascadeDelete.py