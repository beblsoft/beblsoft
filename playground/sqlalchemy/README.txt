OVERVIEW
===============================================================================
SQLAlchemy Documentation
- Description                       : SQLAlchemy is the Python SQL toolkit and Object
                                      Relational Mapper that gives application developers the
                                      full power and flexibility of SQL.
                                    : It provides a full suite of well known enterprise-level persistence
                                      patterns, designed for efficient and high-performing database access,
                                      adapted into a simple and Pythonic domain language.
- Documentation URLS ----------------
  * Home                            : https://www.sqlalchemy.org/
  * Github                          : https://github.com/zzzeek/sqlalchemy
  * Guthub Examples                 : https://github.com/zzzeek/sqlalchemy/examples
  * Result Proxy                    : https://docs.sqlalchemy.org/en/latest/core/connections.html#sqlalchemy.engine.ResultProxy
  * Statements and Expressions      : https://docs.sqlalchemy.org/en/latest/core/expression_api.html
  * Inheritance Overview            : http://docs.sqlalchemy.org/en/latest/orm/inheritance.html#joined-table-inheritance
  * Inheritance Recipes             : http://docs.sqlalchemy.org/en/latest/orm/examples.html#examples-inheritance
  * SQL Join Explanations           : https://www.codeproject.com/Articles/33052/Visual-Representation-of-SQL-Joins
- Code Example URLs -----------------
  * Usage Recipes                   : https://github.com/sqlalchemy/sqlalchemy/wiki/UsageRecipes
  * ORM Examples                    : https://docs.sqlalchemy.org/en/latest/orm/examples.html



TERMS
===============================================================================
- Object Relational Mapper          : A technique that lets you query and manipulate data
                                      from a database using an object-oriented paradigm
- SQL Expression Language           : Toolkit used to construct manipulable SQL expressions which
                                      can be programmatically constructed, modified, and executed,
                                      returning cursor-like result sets
- Dialects                          : Interface to DBAPI backends
- Engine                            : Core interface to the database
                                      engine = create_engine('sqlite:///:memory:', echo=True)
- Database Metadata                 : Collection of Table objects and their associated child objects
- Table Reflection                  : Where SQLAlchemy imports metdata from existing database
- Foreign Key                       : A table-level construct that constrains one or more columns in that
                                      table to only allow values that are present in a different set of columns,
                                      typically but not always located on a different table



GETTING STARTED
===============================================================================
- Architecture                      : -----------------------------------------------------------------------
                                      | SQLAlchemy Object Relational Mapper (ORM)                           |
                                      -----------------------------------------------------------------------

                                      -----------------------------------------------------------------------
                                      | SQLAlchemy Core                                                     |
                                      | ----------------  ------------------   --------------------------   |
                                      | | Schema/Types |  | SQL Expression |   | Engine                 |   |
                                      | |              |  | Language       |   |                        |   |
                                      | ----------------  ------------------   --------------------------   |
                                      |                                        -------------- -----------   |
                                      |                                        | Connection | | Dialect |   |
                                      |                                        | Pooling    | |         |   |
                                      |                                        -------------- -----------   |
                                      -----------------------------------------------------------------------
                                                                               --------------------------
                                                                               | DBAPI                  |
                                                                               |                        |
                                                                               --------------------------
- Supported Platforms               : cPython2.7
                                    : cPython3.4 and higher
                                    : Pypy 2.1 or greater
- Installation
  * Via pip                         : pip install SQLAlchemy
  * Via distirbution                : python setup.py intall
- Check version                     : >>> import sqlalchemy
                                      >>> sqlalchemy.__version__
                                      1.3.0



CORE: SQL STATEMENTS AND EXPRESSIONS API
===============================================================================
- Column Elements and Expressions ---------------------------------------------
  * Class paths                     : sqlalchemy.sql.expression
                                      sqlalchemy.sql.operators
                                      sqlalchemy.sql.elements
  * all_(expr)                      : Produce an ALL expression
                                      Ex.
                                      expr = 5 == all_(select([table.c.value]))
  * and_(*clauses)                  : Produce a conjunction of expressions joined by AND
                                      Ex.
                                      stmt = select([users_table]).where(
                                                      and_(
                                                          users_table.c.name == 'wendy',
                                                          users_table.c.enrolled == True
                                                      )
                                                  )
  * any_(expr)                      : Produce an ANY expression
                                      Ex.
                                      expr = 5 == any_(select([table.c.value]))
  * asc(column)                     : Produce an ascending ORDER BY clause element
                                      Ex.
                                      stmt = select([users_table]).order_by(asc(users_table.c.name))
  * between(expr, lower, upper,
            symmetric=False)        : Produce a BETWEEN predicate clause
                                      Ex.
                                      stmt = select([users_table]).where(between(users_table.c.id, 5, 7))
  * bindparam                       : Produce a bound expression
                                      Ex.
                                      stmt = select([users_table]).\
                                                where(users_table.c.name == bindparam('username'))
  * case(when, value=None,
         else_=None)                : Produce a CASE expression
                                      Ex.
                                      stmt = select([users_table]).\
                                                  where(
                                                      case(
                                                          [
                                                              (users_table.c.name == 'wendy', 'W'),
                                                              (users_table.c.name == 'jack', 'J')
                                                          ],
                                                          else_='E'
                                                      )
                                                  )
  * cast(expr, type_)               : Produce a CAST expression
                                      Ex.
                                      stmt = select([
                                                  cast(product_table.c.unit_price, Numeric(10, 4))
                                              ])
  * column(text, type=None, ...)    : Produce a ColumnClause object
                                      Ex.
                                      id, name = column("id"), column("name")
                                      stmt = select([id, name]).select_from("user")
  * collate(expression, collation)  : Return the clause expression COLLATE collation
                                      Ex.
                                      collate(mycolumn, 'utf8_bin')
  * desc(column)                    : Produce a descending ORDER BY clause element
                                      Ex.
                                      stmt = select([users_table]).order_by(desc(users_table.c.name))
  * distinct(expr)                  : Produce a column-expression-level unary DISTICT clause
                                      Ex.
                                      stmt = select([func.count(distinct(users_table.c.name))])
  * extract(field, expr, **kwargs)  : Return a Extract construct
  * false()                         : Return a False_ construct
                                      Ex.
                                      print select([t.c.x]).where(false())
  * func()                          : Generate Function objects based on getattr calls
  * funcfilter()
  * label()
  * literal()
  * literal_column()
  * not_(clause)                    : Return a negation of the given clause. NOT(clause)
  * null()                          : Return a constant Null construct
  * nullsfirst()
  * nullslast()
  * or_(*clauses)                   : Produce a conjunction of expressions joined by OR
                                      Ex.
                                      stmt = select([users_table]).where(
                                                      or_(
                                                          users_table.c.name == 'wendy',
                                                          users_table.c.name == 'jack'
                                                      )
                                                  )
  * outparam()
  * over()                          : Produce an Over object against a function
                                      Ex.
                                      func.row_number().over(order_by=mytable.c.some_column)
  * text(text, bind, bindparams, ..): Construct a TextClause clause
                                      Ex.
                                      t = text("SELECT * FROM users")
                                      result = connection.execute(t)
  * true()                          : Return a constant True_ construct
  * tuple_(*claws, **kw)            : Return a Tuple
                                      Ex.
                                      tuple_(table.c.col1, table.c.col2).in_(
                                          [(1, 2), (5, 12), (10, 19)]
                                      )
  * type_coerce()
  * within_group()
  * BinaryExpression
  * BindParameter
  * Case
  * Cast
  * ClauseElement                   : Base class for elements of a programmatically constructed SQL expression.
    compare()
    compile()
    get_children()
    params()
  * ClauseList
  * ColumnClause
  * ColumnCollection
  * ColumnElement                   : epresent a column-oriented SQL expression suitable for usage in the
                                      “columns” clause, WHERE clause etc. of a statement.
    __eq__()
    __init__()
    __le__()
    __lt__()
    __ne__()
    all_()
    anan_label()
    any_()
    asc()
    base_columns
    betwen()
    bind
    bool_op()
    cast()
    collate()
    comparator
    compare()
    compile(default, bind, dialect) : Compile this SQL expression
    concat()
    contains()
    desc()
    descrption
    distict()
    endswith()
    expression
    foreign_keys
    get_children()
    ilike()
    in_()
    is_()
    is_clause_element
    is_distict_from()
    is_selectable
    isnot()
    isnot_distinct_from(other)
    key
    label(name)
    like(other, escape=None)
    match(other, **kwargs)
    notilike(other, escape=None)
    notin_(other)
    notlike(other, escape=None)
    nullsfirst()
    nullslast()
    op()
    operate()
    params()
    primary_key
    proxy_set
    reverse_operate()
    self_group()
    shares_lineage(othercolum)
    startswith(other)
    supports_execution
    timetuple
    type
    unique_params
  * ColumnOperators                 : Defines boolean, comparison, and other operators for ColumnElement expressions
  * DialectKWArgs                   : Dialect specific args
  * Extract                         : SQL Extract clause
  * False_
  * FunctionFilter
  * Label
  * Null
  * Over
  * TextClause
  * Tuple
  * WithinGroup
  * True_
  * TypeCoerce
  * custom_op
  * Operators
  * quoted_name
  * UnaryExpression
- Selectables, Tables, FROM objects -------------------------------------------
- Insert, Updates, Deletes ----------------------------------------------------
- SQL and Generic Functions ---------------------------------------------------
  * Class path                      : sqlalchemy.sql.functions
  * array_agg()
  * char_length()
  * coalesce()
  * concat()
  * count()
  * cube()
  * cume_dist()
  * current_date()
  * current_time()
  * current_timestamp()
  * current_user()
  * dense_rank()
  * grouping_sets()
  * localtime()
  * localtimestamp()
  * max()
  * min()
  * mode()
  * next_value()
  * now()
  * percent_rank()
  * percentile_count()
  * percentile_disc()
  * random()
  * rank()
  * rollup()
  * session_user()
  * sum()
  * sysdate()
  * user()
- Custom SQL Constructs and Compilation Extension -----------------------------
- Expression Serializer Extension ---------------------------------------------



CORE: SCHEMA DEFINITION LANGUAGE
===============================================================================
- Describing Databases with MetaData ------------------------------------------
  * Description
    Ex                              : metadata = MetaData()
                                      user = Table('user', metadata,
                                          Column('user_id', Integer, primary_key=True),
                                          Column('user_name', String(16), nullable=False),
                                          Column('email_address', String(60)),
                                          Column('password', String(20), nullable=False)
                                      )
    MetaData                        : Container that keeps sqlalchemy object metadata
    Table                           : Represents a database table
    Column                          : Represents a column in a database table

  * Accessing Tables and Columns
    Access tables                   : >>> for t in metadata.sorted_tables:
                                      ...    print(t.name)
                                      user
                                      user_preference
                                      invoice
                                      invoice_item
  * Creating and Dropping Database
    Tables
    Create single table             : employees.create(engine)
    Drop single table               : employees.drop(engine)
    Create tables                   : metadata.create_all(engine)
    Drop all tables                 : metadata.drop_all(engine)
  * Altering Schemas through
    Migrations                      : See Alembic documentation
  * Specifying the Schema Name      : Some databases support the concept of multiple schemas
                                      Specify schema: schema="dbo.scott"
  * Backend-Specific Options        : Database specific options can be set on each table
    Ex. Specify MySQL engine        : addresses = Table('engine_email_addresses', meta,
                                          Column('address_id', Integer, primary_key=True),
                                          Column('remote_user_id', Integer, ForeignKey(users.c.user_id)),
                                          Column('email_address', String(20)),
                                          mysql_engine='InnoDB')
  * Column, Table, MetaData API
- Reflecting Database Objects -------------------------------------------------
  * Description                     : A table object can be instructed to load information about itself
                                      from the corresponding database schema object already existing
                                      within the database. This process is called reflection
                                    : Ex.
                                      >>> messages = Table('messages', meta, autoload=True, autoload_with=engine)
                                      >>> [c.name for c in messages.columns]
                                      ['message_id', 'message_name', 'date']
  * Overriding Reflected Columns    : Individual columns can be overriden with explicit values when reflecting
                                      tables
  * Reflecting Views                : Views can also be reflected
                                      Ex.
                                      my_view = Table("some_view", metadata, autoload=True)
  * Reflecting All Tables at Once   : Metadata can reflect all database tables at once
                                      Ex.
                                      meta = MetaData()
                                      meta.reflect(bind=someengine)
                                      users_table = meta.tables['users']
                                      addresses_table = meta.tables['addresses']
  * Fine Grained Reflection with
    Inspector
  * Limitations of Reflection
    State not reflected             : Client side defaults
                                      Column information
                                      Association of particular sequence with a column
- Column Insert/Update Defaults -----------------------------------------------
  * Scalar Defaults
  * Python-Executed Functions
  * Client-Invoked SQL Expressions
  * Server-invoked DDL-Explicit
    Default Expressions
  * Marking Implicitly Generated
    Values, timestamps, and
    Triggered Columns
  * Defining Sequences
  * Default Objects API
- Defining Constraints and Indexes --------------------------------------------
  * Defining Foreign Keys
  * UNIQUE Constraint
  * CHECK Constraint
  * PRIMARY KEY Constraint
  * Setting up Constraints when
    using the Declarative ORM
    Extension
  * Configuring Constraint Naming
    Conventions
  * Constraints API
  * Indexes
  * Index API
- Customizing DDL -------------------------------------------------------------
  * Custom DDL
  * Controlling DDL Sequences
  * Using the built-in DDLElement Classes
  * DDL Expression Constructs API



CORE: COLUMN AND DATA TYPES
===============================================================================
- Column and Data Types -------------------------------------------------------
  * Description                     : SQLAlchemy provides abstractions for most common database types,
                                      and a mechanism for specifying custom data types
  * Generic Types
    BigInteger
    Boolean
    Date                            : Type for datetime.date() objects
    DateTime                        : Type for datetime.datetime() objects
    Enum                            : Generic Enum Type
                                      Ex.
                                      class MyEnum(enum.Enum):
                                          one   = 1
                                          two   = 2
                                          three = 3
                                      t = Table(
                                          'data', MetaData(),
                                          Column('value', Enum(MyEnum)))
    Float
    Integer
    Interval                        : Type for datetime.timedelta() objects
    LargeBinary
    MatchType
    Numeric
    PickleType                      : Holds python objects, which are serialized using pickle
    SchemaType
    SmallInteger
    String                          : Base for all string and character types, VARCHAR
    Text                            : Variable sized string
    Time                            : Type for datetime.time()
    Unicode
    UnicodeText
  * SQL Standard and Multiple
    Vendor Types
    Description                     : Types that are either part of the SQL standard, or found
                                      on a subset of database backends. No guarantee of working on all backents
    ARRAY
    BIGINT
    BINARY
    BLOG
    BOOLEAN
    CHAR
    CLOB
    DATE
    DATETIME
    DECIMAL
    FLOAT
    INT
    JSON                            : SQL JSON Type
    INTEGER
    NCHAR
    NVARCHAR
    NUMERIC
    REAL
    SMALLINT
    TEXT
    TIMESTAMP
    VARBINARY
    VARCHAR
  * Vendor-Specific Types           : Database-specific types are also available for import from each
                                      database’s dialect module. See the Dialects reference for the
                                      database you’re interested in.
- Custom Types ----------------------------------------------------------------
  * Overriding Type Compilation
  * Augmenting Existing Types
  * TypeDecorator Recipes
  * Replacing the Bind/Result Processing of Existing Types
  * Applying SQL-level Bind/Result Processing
  * Redefining and Creating New Operators
  * Creating New Types
- Base Type API ---------------------------------------------------------------



CORE: ENGINE AND CONNECTION USE
===============================================================================
- Engine Configuration --------------------------------------------------------
  * Description                     : Engine is the starting point for any SQLAlchemy application
                                    : Architecture
                                                 Pool
                                              /        \
                                       Engine            DBAPI  - Database
                                              \        /
                                                Dialect
                                    : Ex.
                                      from sqlalchemy import create_engine
                                      engine = create_engine('postgresql://scott:tiger@localhost:5432/mydatabase')
                                    : Engine and underlying pool do not establish connection until
                                      engine.connect() or engine.execute() is called
  * Database Urls                   : URLs follow RFC-1738    dialect+driver://username:password@host:port/database
    PostgreSQL                      : default:                'postgresql://scott:tiger@localhost/mydatabase'
                                      psycopg2:               'postgresql+psycopg2://scott:tiger@localhost/mydatabase'
                                      pg8000                  'postgresql+pg8000://scott:tiger@localhost/mydatabase'
    MySQL                           : default:                'mysql://scott:tiger@localhost/foo')
                                      mysql-python:           'mysql+mysqldb://scott:tiger@localhost/foo'
                                      MySQL-connector-python: 'mysql+mysqlconnector://scott:tiger@localhost/foo'
                                      OurSQL:                 'mysql+oursql://scott:tiger@localhost/foo'
    Oracle                          : default:                'oracle://scott:tiger@127.0.0.1:1521/sidname'
                                      cxOralce:               'oracle+cx_oracle://scott:tiger@tnsname'
    Microsoft SQL Server            : pyodbc:                 'mssql+pyodbc://scott:tiger@mydsn')
                                      pymssql:                'mssql+pymssql://scott:tiger@hostname:port/dbname')
    SQLite                          : Unix/Mac:               'sqlite:////absolute/path/to/foo.db')
                                      Windows:                'sqlite:///C:\\path\\to\\foo.db')
                                      Windows:                'sqlite:///C:\path\to\foo.db')
                                      memory:                 'sqlite:////:memory:'

  * Engine Creation API
  * Pooling                         : Engine will ask pool for connection when connect() or execute() occurs
                                      Default pool is QueuePool
  * Custom DBAPI connect()
    arguments                       : Custom args can be passed to DBAPI
    Specify Args                    : db = create_engine('postgresql://scott:tiger@localhost/test?argument1=foo&argument2=bar')
    Specify connect args            : db = create_engine('postgresql://scott:tiger@localhost/test', connect_args = {'argument1':17, 'argument2':'bar'})
  * Configuring Logging             : SQLAlchemy leverages Python's standard logging module
    Namespaces
    sqlalchemy                      : Global namespace
    sqlalchemy.engine               : Controls SQL echoing
    sqlalchemy.dialects             : Controls custom logging for SQL dialects
    sqlalchemy.pool                 : Contorls connection pool logging
    sqlalchemy.orm                  : controlls logging of various ORM functions
    Default level                   : logging.WARN
- Working with Engines and Connections ----------------------------------------
  * Basic Usage                     : Ex.
                                      engine     = create_engine('mysql://scott:tiger@localhost/test')
                                      connection = engine.connect()
                                      result     = connection.execute("select username from users")
                                      for row in result:
                                          print("username:", row['username'])
                                      connection.close()
  * Using Transactions
  * Understanding Autocommit
  * Connectionless Execution,
    Implicit Execution
  * Translation of Schema Names
  * Engine Disposal
  * Using the Threadlocal Execution
    Strategy
  * Working with Raw DBAPI
    Connections
  * Registering New Dialects
  * Connection / Engine API
- Connection Pooling ----------------------------------------------------------
  * Description                     : A connection pool is a standard technique used to maintain long running
                                      connections in memory for efficient re-use, as well as to provide
                                      management for the total number of connections an application might
                                      use simultaneously
  * Connection Pool Configuration   : By default, engine uses QueuePool
                                      Ex. QueuePool args passed to create engine
                                      engine = create_engine('postgresql://me@localhost/mydb',
                                                             pool_size=20, max_overflow=0)
  * Switching Pool Implementations  : Provide pool class as an argument
                                      Ex.
                                      engine = create_engine(
                                                'postgresql+psycopg2://scott:tiger@localhost/test',
                                                poolclass=NullPool)
  * Using a Custom Connection
    Function                        : Ex.
                                      def getconn():
                                          c = psycopg2.connect(username='ed', host='127.0.0.1', dbname='test')
                                          # do things with 'c' to set up
                                          return c
                                      engine = create_engine('postgresql+psycopg2://', creator=getconn)
  * Constructing a Pool, using it   : Ex.
                                      def getconn():
                                          c = psycopg2.connect(username='ed', host='127.0.0.1', dbname='test')
                                          return c
                                      mypool = pool.QueuePool(getconn, max_overflow=10, pool_size=5)
                                      e      = create_engine('postgresql://', pool=mypool)
                                      # get a connection, use it, and close it returning the connection to the pool
                                      conn   = mypool.connect()
                                      cursor = conn.cursor()
                                      cursor.execute("select foo")
                                      conn.close()
  * Pool Events
  * Dealing with Disconnects        : The connection pool has the ability to refresh individual connections as well
                                      as its entire set of connections, setting the previously pooled connections
                                      as “invalid”.
    Disconnect Handling-Pessimistic : Issue a ping on the connection whenever it is checked out of the pool.
                                      This ensures that no stale connections will be checked out, but adds
                                      the overhead of a ping to each checkout
    Disconnect Handling-Optimistic  : Deal with stale connection errors as they occur. When one is hit
                                      pool will recreate all connections
    pool_recyle                     : Prevents pool from using a particular connection that has passed a certain
                                      age (good for MySQL)
                                      Ex.
                                      e = create_engine("mysql://scott:tiger@localhost/test", pool_recycle=3600)
  * Using FIFO vs. LIFO
  * Using Connection Pools with
    Multiprocessing
  * Available Pool Implementations
    Pool                            : Abstract base class for connection pools
    QueuePool                       : Pool that imposes a limit on the number of open connections
    SingletonThreadPool             : Pool that maintains one connection per thread
    AssertionPool                   : Pool that allows at most one checked out connection at any given time
    NullPool                        : Pool which does not pool connections. It opens and closes the underlying
                                      DB-API connection per each connection open/close
    StaticPool                      : Pool of exactly one connection, used for all requests
- Core Events -----------------------------------------------------------------
  * Connection Pool Events
  * SQL Execution and Connection Events
  * Schema Events



CORE: CORE API BASICS
===============================================================================
- Events ----------------------------------------------------------------------
  * Description                     : SQLAlchemy includes an event API which publishes a wide
                                      variety of hooks into the internals of both SQLAlchemy Core and ORM.
  * Event Registration              : Register for events via listen() and listens_for() apis
                                      Ex.
                                      from sqlalchemy.event import listens_for
                                      from sqlalchemy.pool import Pool
                                      @listens_for(Pool, "connect")
                                      def my_on_connect(dbapi_con, connection_record):
                                          print("New DBAPI connection:", dbapi_con)
  * Named Argument Styles           : Have arguments be passed named
                                      @listens_for(Pool, "connect", named=True)
                                      def my_on_connect(dbapi_connection, **kw):
                                          print("New DBAPI connection:", dbapi_connection)
                                          print("Connection record:", kw['connection_record'])
  * Targets                         : listen() and listen_for() accept a variety of objects
  * Modifiers                       : Some listeners allow modifiers to be passed to listen()
                                      Set retval=True to require return value
                                      Ex.
                                      def validate_phone(target, value, oldvalue, initiator):
                                          """Strip non-numeric characters from a phone number"""
                                          return re.sub(r'\D', '', value)

                                      # setup listener on UserContact.phone attribute, instructing
                                      # it to use the return value
                                      listen(UserContact.phone, 'set', validate_phone, retval=True)
  * Event Reference
- API Reference ---------------------------------------------------------------
  * Runtime Inspection API          : inspect() function delivers runtime information about a wide variety of
                                      SQLAlchemy objects. Public API for viewing the configuration and
                                      construction of in-memory objects
  * Available Inspection Targets
   Inspection Targets
   Connectable(Engine, Connection)  : Returns Inspector object
   ClauseElement                    : All SQL expression components, Table, Column
   object                           : An object given will be check by ORM for a mapping
   type (i.e. class)                : Mapper returned
   AliasedClass                     : Returns AliasedInsp object
- Deprecated Event Interfaces -------------------------------------------------
  * Execution, Connection and Cursor Events
  * Connection Pool Events
- Core Exceptions -------------------------------------------------------------
  * Class Path                      : sqlalchemy.exc
  * AmbiguousForeignKeysError       : Raised when more than one foreign key matching can be located
                                      between two selectables during a join.
  * ArgumentError                   : Raised when an invalid or conflicting function argument is supplied.
  * CircularDependencyError         : Raised by topological sorts when a circular dependency is detected.
  * DBAPIError                      : Raised when the execution of a database operation fails.
  * DataError                       : Wraps a DB-API DataError.
  * DatabaseError                   : Wraps a DB-API DatabaseError.
  * DisconnectionError              : A disconnect is detected on a raw DB-API connection. This error is
                                      raised and consumed internally by a connection pool. It can be raised
                                      by the PoolEvents.checkout() event so that the host pool forces a retry;
                                      the exception will be caught three times in a row before the pool gives
                                      up and raises InvalidRequestError regarding the connection attempt.
  * DontWrapMixin                   : A mixin class which, when applied to a user-defined Exception class,
                                      will not be wrapped inside of StatementError if the error is emitted within
                                      the process of executing a statement.
  * IdentifierError                 : Raised when a schema name is beyond the max character limit
  * IntegrityError                  : Wraps a DB-API IntegrityError.
  * InterfaceError                  : Wraps a DB-API InterfaceError.
  * InternalError                   : Wraps a DB-API InternalError.
  * InvalidRequestError             : SQLAlchemy was asked to do something it can’t do.
                                      This error generally corresponds to runtime state errors.
  * InvalidatePoolError             : Raised when the connection pool should invalidate all stale connections.
  * NoForeignKeysError              : Raised when no foreign keys can be located between two selectables
                                      during a join.
  * NoInspectionAvailable           : A subject passed to sqlalchemy.inspection.inspect() produced no
                                      context for inspection.
  * NoReferenceError                : Raised by ForeignKey to indicate a reference cannot be resolved.
  * NoReferencedColumnError         : Raised by ForeignKey when the referred Column cannot be located.
  * NoReferencedTableError          : Raised by ForeignKey when the referred Table cannot be located.
  * NoSuchColumnError               : A nonexistent column is requested from a RowProxy.
  * NoSuchModuleError               : Raised when a dynamically-loaded module (usually a database dialect) of a
                                      particular name cannot be located.
  * NoSuchTableError                : Table does not exist or is not visible to a connection.
  * NotSupportedError               : Wraps a DB-API NotSupportedError.
  * ObjectNotExecutableError        : Raised when an object is passed to .execute() that can’t be executed as SQL.
  * OperationalError                : Wraps a DB-API OperationalError.
  * ProgrammingError                : Wraps a DB-API ProgrammingError.
  * ResourceClosedError             : An operation was requested from a connection, cursor, or other object
                                      that’s in a closed state.
  * SADeprecationWarning            : Issued once per usage of a deprecated API.
  * SAPendingDeprecationWarning     : Issued once per usage of a deprecated API.
  * SAWarning                       : Issued at runtime.
  * SQLAlchemyError                 : Generic error class.
  * StatementError                  : An error occurred during execution of a SQL statement.
                                      StatementError wraps the exception raised during execution, and features
                                      statement and params attributes which supply context regarding the specifics
                                      of the statement which had an issue.
  * TimeoutError                    : Raised when a connection pool times out on getting a connection.
  * UnboundExecutionError           : SQL was attempted without a database connection to execute it on.
  * UnreflectableTableError         : Table exists but can’t be reflectted for some reason.
  * UnsupportedCompilationError     : Raised when an operation is not supported by the given compiler.
- Core Internals --------------------------------------------------------------



ORM: MAPPER CONFIGURATION
===============================================================================
- Types of Mappings -----------------------------------------------------------
  1 Declarative Mapping             : Typical way mappings are constructed in SQLAlchemy
                                      User defined class and associated table are defined together
                                      Ex.
                                      Base = declarative_base()
                                      class User(Base):
                                          __tablename__ = 'user'
                                          id            = Column(Integer, primary_key=True)
                                          name          = Column(String)
                                          fullname      = Column(String)
                                          password      = Column(String)
  2 Classical Mappings              : Classical mapping has class separated from table
                                      The two are linked with the mapper function
                                      Ex.
                                      metadata = MetaData()
                                      user = Table('user', metadata,
                                                  Column('id', Integer, primary_key=True),
                                                  Column('name', String(50)),
                                                  Column('fullname', String(50)),
                                                  Column('password', String(12))
                                              )
                                      class User(object):
                                          def __init__(self, name, fullname, password):
                                              self.name     = name
                                              self.fullname = fullname
                                              self.password = password

                                      mapper(User, user)
  * Interchangable                  : Classical as well as Declarative approaches are fully interchangeable
  * Runtime Introspection of
    Mappings, Objects               : Mapper object is available from any mapped class, regardless of method
                                      using the runtime inspection api
                                      Ex.
                                      from sqlalchemy import inspect
                                      insp = inspect(user)
  * Inspection Targets
    Connectable                     : Engine, Connection. Returns Inspector object
    ClauseElement                   : All SQL expression components
    object                          : Returns InstanceState
    type
    mapped attribute                : Returns QueryableAttribute
    AliasedClass                    : Returns AliasedInsp
- Mapping Columns and Expressions ---------------------------------------------
  * Mapping Table Columns
    Naming columns distinct from
    attribute names                 : Ex. User.id resolves to a column named user_id
                                      class User(Base):
                                         __tablename__ = 'user'
                                         id            = Column('user_id', Integer, primary_key=True)
                                         name          = Column('user_name', String(50))
    Automating column name schems
    for reflected tables            : Properties on reflected tables can also vary from underlying table columns
    Name all columns with prefix    : Ex. Put "_" before each column name
                                      class User(Base):
                                          __table__       = user_table
                                          __mapper_args__ = {'column_prefix':'_'}
    Mapping a subset of reflected
    table columns                   : Ex. Only include a subset of reflected columns
                                      class User(Base):
                                         __table__       = user_table
                                         __mapper_args__ = {
                                             'include_properties' :['user_id', 'user_name']
                                         }
                                      Ex. Exclude a subset of columns
                                      class Address(Base):
                                          __table__       = address_table
                                          __mapper_args__ = {
                                              'exclude_properties' : ['street', 'city', 'state', 'zip']
                                          }

  * SQL Expressions as Mapped
    Attributes                      : Attributes on a mapped class can be linked to SQL expressions,
                                      which can be used in queries
    Hybrid                          : Ex. Link fullname to firstname and lastname
                                      from sqlalchemy.ext.hybrid import hybrid_property
                                      class User(Base):
                                          __tablename__ = 'user'
                                          id            = Column(Integer, primary_key=True)
                                          firstname     = Column(String(50))
                                          lastname      = Column(String(50))

                                          @hybrid_property
                                          def fullname(self):
                                              return self.firstname + " " + self.lastname
    column_property                 : Ex.
                                      from sqlalchemy.orm import column_property
                                      class User(Base):
                                          __tablename__ = 'user'
                                          id            = Column(Integer, primary_key=True)
                                          firstname     = Column(String(50))
                                          lastname      = Column(String(50))
                                          fullname      = column_property(firstname + " " + lastname)
  * Changing Attribute Behavior
    Validators                      : Validators called when populating obj from userspace (not from DB)
                                      Can halt process of mutating attribute, or change value to something different
                                      Ex. Ensure email has '@'
                                      from sqlalchemy.orm import validates

                                      class EmailAddress(Base):
                                          __tablename__ = 'address'

                                          id = Column(Integer, primary_key=True)
                                          email = Column(String)

                                          @validates('email')
                                          def validate_email(self, key, address):
                                              assert '@' in address
                                              return address
    Synonyms                        : Mapper-level construct that allow any attribute to mirror another mapped attribute
                                      Ex.
                                      class MyClass(Base):
                                         __tablename__ = 'my_table'
                                         id            = Column(Integer, primary_key=True)
                                         job_status    = Column(String(50))
                                         status        = synonym("job_status")
  * Composite Column Types          : Sets of columns can be associated with a single user-defined datatype
                                      Ex. Point
                                      class Vertex(Base):
                                          __tablename__ = 'vertices'
                                          id            = Column(Integer, primary_key=True)
                                          x1            = Column(Integer)
                                          y1            = Column(Integer)
                                          x2            = Column(Integer)
                                          y2            = Column(Integer)
                                          start         = composite(Point, x1, y1)
                                          end           = composite(Point, x2, y2)

- Mapping Class Inheritance Hierarchies ---------------------------------------
  1 Joined Table Inheritance        : Class hierarchy is broken up among dependent tables
                                      Querying renders join along hierarchy of classes
                                      Default is for subclass attributes to be lazily loaded
                                      Subclass tables have foreign key back to base class tables
                                      Ex.
                                      class Employee(Base):
                                          __tablename__   = 'employee'
                                          id              = Column(Integer, primary_key=True)
                                          name            = Column(String(50))
                                          type            = Column(String(50))
                                          __mapper_args__ = {
                                              'polymorphic_identity':'employee',
                                              'polymorphic_on':type   # Discriminator
                                          }

                                      class Engineer(Employee):
                                          __tablename__   = 'engineer'
                                          id              = Column(Integer, ForeignKey('employee.id'), primary_key=True)
                                          engineer_name   = Column(String(30))
                                          __mapper_args__ = {
                                              'polymorphic_identity':'engineer',
                                          }

                                      class Manager(Employee):
                                          __tablename__   = 'manager'
                                          id              = Column(Integer, ForeignKey('employee.id'), primary_key=True)
                                          manager_name    = Column(String(30))
                                          __mapper_args__ = {
                                              'polymorphic_identity':'manager',
                                          }

  2 Single Table Inheritance        : Several types of classes are represented by a single table
                                      Subclasses have NULL values in columns that represent other subclasses
                                      Ex.
                                      class Employee(Base):
                                          __tablename__   = 'employee'
                                          id              = Column(Integer, primary_key=True)
                                          name            = Column(String(50))
                                          type            = Column(String(20))
                                          __mapper_args__ = {
                                              'polymorphic_on':type,
                                              'polymorphic_identity':'employee'
                                          }

                                      class Manager(Employee):
                                          manager_data    = Column(String(50))
                                          __mapper_args__ = {
                                              'polymorphic_identity':'manager'
                                          }

                                      class Engineer(Employee):
                                          engineer_info   = Column(String(50))
                                          __mapper_args__ = {
                                              'polymorphic_identity':'engineer'
                                          }
  3 Concrete Table Inheritance      : Each type of class is represented by independent tables
                                      Much more complicated that joined or single table inheritance
                                      and much more limited in functionality.
- Non-Traditional Mappings ----------------------------------------------------
  * Mapping a Class against Multiple
    Tables                          : Mappers can be constructed against arbitrary relational units (selectables)
                                      Ex. Mapping class against join table
                                      metadata = MetaData()

                                      # Define two Table objects
                                      user_table = Table('user', metadata,
                                                  Column('id', Integer, primary_key=True),
                                                  Column('name', String),
                                              )
                                      address_table = Table('address', metadata,
                                                  Column('id', Integer, primary_key=True),
                                                  Column('user_id', Integer, ForeignKey('user.id')),
                                                  Column('email_address', String)
                                                  )

                                      # define a join between them.  This takes place across the user.id and
                                      # address.user_id columns.
                                      user_address_join = join(user_table, address_table)

                                      # Map to join
                                      class AddressUser(Base):
                                          __table__  = user_address_join
                                          id         = column_property(user_table.c.id, address_table.c.user_id)
                                          address_id = address_table.c.id
  * Mapping a Class against
    Arbitrary Selects               : Can map a class against a select
  * Multiple Mappers for One Class  : Non primary mappers
- Configuring a Version Counter -----------------------------------------------
  * Simple Version Counting         : Every commit sqlalchemy will increment version counter
                                      If the version is stale, a StaleDataError will be raised
                                      Ex.
                                      class User(Base):
                                          __tablename__   = 'user'
                                          id              = Column(Integer, primary_key=True)
                                          version_id      = Column(Integer, nullable=False)    # Non nullable
                                          name            = Column(String(50), nullable=False) # Non nullable
                                          __mapper_args__ = {
                                              "version_id_col": version_id
                                          }

  * Custom Version Counters / Types : Create custom versions
                                      Ex. Custom versioning with uuid4
                                      import uuid
                                      class User(Base):
                                          __tablename__   = 'user'
                                          id              = Column(Integer, primary_key=True)
                                          version_uuid    = Column(String(32), nullable=False)
                                          name            = Column(String(50), nullable=False)
                                          __mapper_args__ = {
                                              'version_id_col':version_uuid,
                                              'version_id_generator':lambda version: uuid.uuid4().hex
                                          }
  * Server Side Version Counters    : version_id_generator can be configured to use database generated version
  * Programmatic or Conditional
    Version Counters                : When version_id_generator is set to False, one can also programmatically
                                      (and conditionally) set the version identifier on our object in the
                                      same way we assign any other mapped attribute.



ORM: RELATIONSHIP CONFIGURATION
===============================================================================
- Basic Relationship Patterns -------------------------------------------------
  * One To Many                     : Foreign key on child referencing parent
                                      Ex.
                                      class Parent(Base):
                                          __tablename__ = 'parent'
                                          id            = Column(Integer, primary_key=True)
                                          children      = relationship("Child", back_populates="parent")

                                      class Child(Base):
                                          __tablename__ = 'child'
                                          id            = Column(Integer, primary_key=True)
                                          parent_id     = Column(Integer, ForeignKey('parent.id'))
                                          parent        = relationship("Parent", back_populates="children")
  * Many To One                     : Foreign key on parent referencing child
                                      Ex.
                                      class Parent(Base):
                                          __tablename__ = 'parent'
                                          id            = Column(Integer, primary_key=True)
                                          child_id      = Column(Integer, ForeignKey('child.id'))
                                          child         = relationship("Child", back_populates="parents")

                                      class Child(Base):
                                          __tablename__ = 'child'
                                          id            = Column(Integer, primary_key=True)
                                          parents       = relationship("Parent", back_populates="child")

  * One To One                      : Bidirectional relationship with scalar attribute on both sides
                                      Use the relationship "uselist=False" flag
  * Many To Many                    : Adds an association table between the two classes
  * Association Object              : Variant on many-to-many. Used when association table contains
                                      additional columns beyond foreign keys
                                      Ex.
                                      class Association(Base):
                                          __tablename__ = 'association'
                                          left_id       = Column(Integer, ForeignKey('left.id'), primary_key=True)
                                          right_id      = Column(Integer, ForeignKey('right.id'), primary_key=True)
                                          extra_data    = Column(String(50))
                                          child         = relationship("Child", back_populates="parents")
                                          parent        = relationship("Parent", back_populates="children")

                                      class Parent(Base):
                                          __tablename__ = 'left'
                                          id            = Column(Integer, primary_key=True)
                                          children      = relationship("Association", back_populates="parent")

                                      class Child(Base):
                                          __tablename__ = 'right'
                                          id            = Column(Integer, primary_key=True)
                                          parents       = relationship("Association", back_populates="child")

                                      Ex. Usage
                                      p       = Parent()
                                      a       = Association(extra_data="some data")
                                      a.child = Child()
                                      p.children.append(a)

                                      # iterate through child objects via association, including association attributes
                                      for assoc in p.children:
                                          print(assoc.extra_data)
                                          print(assoc.child)
- Adjacency List Relationships ------------------------------------------------
  * Description                     : The adjacency list pattern is a common relational pattern whereby
                                      a table contains a foreign key reference to itself
                                      Ex. Parent and child nodes stored in same table
                                      class Node(Base):
                                          __tablename__ = 'node'
                                          id            = Column(Integer, primary_key=True)
                                          parent_id     = Column(Integer, ForeignKey('node.id'))
                                          data          = Column(String(50))
                                          children      = relationship(
                                                          "Node", backref=backref('parent', remote_side=[id]))
  * Composite Adjacency Lists       : Sub category of adjacency list where foreign key is multiple
                                      columns
  * Self-Referential Query
    Strategies                      : Extra care is needed to join along the foreign key from one level of
                                      the tree to the next. Need to alias
  * Configuring Self-Referential
    Eager Loading                   : Can configure the join depth for eager loading
- Linking Relationships with Backref ------------------------------------------
  * Description                     : Backref keyword is only a common shortcut for placing a second
                                      relationship onto the other mapping and using back_populates
                                      Ex. One to Many
                                      class User(Base):
                                          __tablename__ = 'user'
                                          id            = Column(Integer, primary_key=True)
                                          name          = Column(String)
                                          addresses     = relationship("Address", backref="user")

                                      class Address(Base):
                                          __tablename__ = 'address'
                                          id            = Column(Integer, primary_key=True)
                                          email         = Column(String)
                                          user_id       = Column(Integer, ForeignKey('user.id'))

  * Backref Arguments               : Backref arguments allow the relationship to be configured on the other side
                                      addresses = relationship("Address", backref=backref("user", lazy="joined"))
  * One Way Backrefs                : An unusual case is that of the “one way backref”. This is where the
                                      “back-populating” behavior of the backref is only desirable in one direction.
- Configuring how Relationship Joins ------------------------------------------
  * Handling Multiple Join Paths    : More than one foreign key path between two tables
                                      Ex.
                                      class Customer(Base):
                                          __tablename__       = 'customer'
                                          id                  = Column(Integer, primary_key=True)
                                          name                = Column(String)
                                          billing_address_id  = Column(Integer, ForeignKey("address.id"))
                                          billing_address     = relationship("Address", foreign_keys=[billing_address_id])
                                          shipping_address_id = Column(Integer, ForeignKey("address.id"))
                                          shipping_address    = relationship("Address", foreign_keys=[shipping_address_id])
  * Specifying Alternate Join
    Conditions                      : Default join behavior is to match primary key with foreign key
                                      This behavior can be changed
                                      Ex.
                                      class User(Base):
                                          __tablename__    = 'user'
                                          id               = Column(Integer, primary_key=True)
                                          name             = Column(String)
                                          boston_addresses = relationship(
                                                              "Address",
                                                              primaryjoin="and_(User.id==Address.user_id, "
                                                                          "Address.city=='Boston')")
  * Creating Custom Foreign
    Conditions                      : Can manually specify foreign_keys to use in join
  * Using custom operators in join
    conditions                      : Can use operators in primary join syntax
  * Overlapping Foreign Keys
  * Non-relational Comparisons /
    Materialized Path
  * Self-Referential Many-to-Many
    Relationship                    : Ex.
                                      Base = declarative_base()
                                      node_to_node = Table("node_to_node", Base.metadata,
                                          Column("left_node_id", Integer, ForeignKey("node.id"), primary_key=True),
                                          Column("right_node_id", Integer, ForeignKey("node.id"), primary_key=True)
                                      )
                                      class Node(Base):
                                          __tablename__ = 'node'
                                          id            = Column(Integer, primary_key=True)
                                          label         = Column(String)
                                          right_nodes   = relationship("Node",
                                                              secondary=node_to_node,
                                                              primaryjoin=id==node_to_node.c.left_node_id,
                                                              secondaryjoin=id==node_to_node.c.right_node_id,
                                                              backref="left_nodes")
  * Composite “Secondary” Joins
  * Relationship to Non Primary
    Mapper
  * Row-Limited Relationships with
    Window Functions
  * Building Query-Enabled Properties
- Collection Configuration and Techniques -------------------------------------
  * Working with Large Collections  : Default relationship behavior is to fully load the collection of items
                                      according the loading strategy in the relationship
                                      When parent is marked for deletion, all child objects will potentially
                                      be loaded and also marked for deletion
    Dynamic Relationship Loaders    : Returns Query object instead of collection
                                      Ex.
                                      class User(Base):
                                          __tablename__ = 'user'
                                          posts         = relationship(Post, lazy="dynamic")
                                      jack  = session.query(User).get(id)
                                      posts = jack.posts.filter(Post.headline=='this is a post')
    Noload                          : Never loads from database even when accessed
                                      Ex.
                                      children = relationship(MyOtherClass, lazy='noload')
    RaiseLoad                       : Raises InvalidRequestError when attribute would normally lazy load
                                      Ex.
                                      children = relationship(MyOtherClass, lazy='raise')
    Passive Deletes                 : Set passive deletes to disable child object loading on a DELETE operation
                                      Must be in conjunction with ON DELETE (CASCADE|SET NULL)
                                      Ex.
                                      class MyClass(Base):
                                          __tablename__ = 'mytable'
                                          id            = Column(Integer, primary_key=True)
                                          children      = relationship("MyOtherClass",
                                                              cascade="all, delete-orphan",
                                                              passive_deletes=True)

                                      class MyOtherClass(Base):
                                          __tablename__ = 'myothertable'
                                          id            = Column(Integer, primary_key=True)
                                          parent_id     = Column(Integer,
                                                             ForeignKey('mytable.id', ondelete='CASCADE'))
  * Customizing Collection Access
    List                            : Default collection_class is a list
    Sets                            : children = relationship(Child, collection_class=set)
    Dictionary                      : children = relationship("Parent",
                                                    collection_class=attribute_mapped_collection('name'),
                                                    cascade="all, delete-orphan")
  * Custom Collection Implementatins
  * Collection Internals
- Special Relationship Persistence Patterns -----------------------------------
  * Rows that point to themselves /
    Mutually Dependent Rows
  * Mutable Primary Keys /
    Update Cascades



ORM: LOADING OBJECTS
===============================================================================
- Loading Columns -------------------------------------------------------------
  * Deferred Column Loading         : Feature allows particular columns of a table to be loaded only
                                      upon direct access, instead of when the entity is queried
                                      Useful to avoid loading a large text or binary field
                                      Ex.
                                      class Book(Base):
                                          __tablename__ = 'book'
                                          book_id       = Column(Integer, primary_key=True)
                                          summary       = Column(String(2000))
                                          excerpt       = deferred(Column(Text))
                                          photo         = deferred(Column(Binary))
    Load deferred columns together
    in a group                      : Ex. Load all photos together
                                      class Book(Base):
                                          __tablename__ = 'book'
                                          book_id       = Column(Integer, primary_key=True)
                                          title         = Column(String(200), nullable=False)
                                          summary       = Column(String(2000))
                                          excerpt       = deferred(Column(Text))
                                          photo1        = deferred(Column(Binary), group='photos')
                                          photo2        = deferred(Column(Binary), group='photos')
                                          photo3        = deferred(Column(Binary), group='photos')
    Have query load specific
    columns                         : Ex.
                                      session.query(Book).options(load_only("summary", "excerpt"))
    Deferred Loading with multiple
    entities                        : Ex.
                                      query = session.query(Book, Author).join(Book.author)
                                      query = query.options(
                                                  Load(Book).load_only("summary", "excerpt"),
                                                  Load(Author).defer("bio"))
  * Column Bundles                  : Bundles allow columns to be grouped together
                                      Ex.
                                      from sqlalchemy.orm import Bundle
                                      bn = Bundle('mybundle', MyClass.data1, MyClass.data2)
                                      for row in session.query(bn).filter(bn.c.data1 == 'd1'):
                                          print(row.mybundle.data1, row.mybundle.data2)
- Relationship Loading Techniques ---------------------------------------------
  * Description                     : Many different strategies for loading different relationships
                                      3 categories: lazy, eager, and no
    lazy loading                    : Emits a SELECT statement at attribute access time
                                      lazy='select', lazyload()
    joined loading                  : Load applies a JOIN to parent SELECT
                                      lazy='joined', joinedload()
    subquery loading                : Two SELECT statements joined
                                      lazy='subquery', subqueryload()
    select IN loading               : Emits a second(or more) SELECT statements to load collections by primary key
                                      lazy='selectin', selectinload()
    raise loading                   : Raises error to prevent application from accessing field
                                      lazy='raise', raiseload()
                                      lazy='raise_on_sql'
    no loading                      : Attribute turns into an empty attribute that will never load
                                      lazy='noload', noload()
  * Configuring Loader Strategies
    at Mapping Time                 : Ex.
                                      class Parent(Base):
                                          __tablename__ = 'parent'
                                          id            = Column(Integer, primary_key=True)
                                          children      = relationship("Child", lazy='joined')
  * Controlling Loading via Options : Can change load behavior at query time
    Load lazily                     : session.query(Parent).options(lazyload('children')).all()
    Load eagerly                    : session.query(Parent).options(joinedload('children')).all()
  * Lazy Loading                    : Default for all relationship loading
  * Joined Eager Loading
  * Subquery Eager Loading
  * Select IN loading
  * What Kind of Loading to Use?
  * Polymorphic Eager Loading
  * Wildcard Loading Strategies     : Ex1. Lazily load all attributes
                                      session.query(MyClass).options(lazyload('*'))
                                      Ex2. Override lazy load from specific attribute
                                      session.query(MyClass).options(lazyload('*'), joinedload(MyClass.widget))
  * Routing Explicit Joins/
    Statements into Eagerly Loaded
    Collections
  * Creating Custom Load Rules
  * Relationship Loader API
- Loading Inheritance Hierarchies ---------------------------------------------
  * Description                     : When classes are mapped in inheritance hierarchies using the “joined”,
                                      “single”, or “concrete” table inheritance styles as described at
                                      Mapping Class Inheritance Hierarchies, the usual behavior is that a
                                      query for a particular base class will also yield objects corresponding
                                      to subclasses as well.
                                    : When a single query is capable of returning a result with a different
                                      class or subclasses per result row, we use the term “polymorphic loading”.
  * Using with_polymorphic
    Base class                      : session.query(Employee).all()
    Query with polymorphic          : Ex.
                                      from sqlalchemy.orm import with_polymorphic
                                      eng_plus_manager = with_polymorphic(Employee, [Engineer, Manager])
                                      query = session.query(eng_plus_manager)
    One subclass                    : entity = with_polymorphic(Employee, Engineer)
    Multiple Subclasses             : entity = with_polymorphic(Employee, [Engineer, Manager])
    All Mapped Subclasses           : entity = with_polymorphic(Employee, '*')
  * Polymorphic Selectin Loading    : Alternative to orm.with_polymorphic
                                      Ex.
                                      from sqlalchemy.orm import selectin_polymorphic
                                      query = session.query(Employee).options(
                                          selectin_polymorphic(Employee, [Manager, Engineer]))
  * Referring to specific subtypes
    on relationships
  * Loading objects with joined
    table inheritance
  * Loading objects with single
    table inheritance
  * Inheritance Loading API
- Constructors and Object Initialization --------------------------------------
  * Description                     : Mapping imposes no restrictions or requirements on the constructor
                                      orm.reconstructor allows object state to set after it is loaded from db
                                      Ex.
                                      class MyMappedClass(object):
                                          def __init__(self, data):
                                              self.data = data
                                              # we need stuff on all instances, but not in the database.
                                              self.stuff = []
                                          @orm.reconstructor
                                          def init_on_load(self):
                                              self.stuff = []
- Query API -------------------------------------------------------------------
  * The Query Object                : class sqlalchemy.orm.query.Query(entities, session=None)
    add_column()
    add_columns()
    add_entity()
    all()                           : Return results represented by query as a list
    as_scalar()                     : Return results as scalar
    autoflush()
    column_descriptions
    correlate()
    count()                         : Return count of rows this query would return
    cte()
    delete()                        : Perform bulk delete query
    distict()
    enable_assertions()
    enable_eagerloads()
    except_()                       : Produce an except of this query against one or more queries
    except_all()
    execution_options()
    exists()
    filter()                        : Apply given filter criterion to query
    filter_by()
    first()                         : Return the first result of query
    from_self()
    from_statement()
    get()
    group_by()                      : Apply one or more group_by criterin
    having()                        : Apply having criterion
    instances()
    intersect()
    intersect_all()
    join()                          : Create SQL JOIN against query
    label()
    lazy_loaded_from
    limit()                         : Apply a LIMIT to the query
    merge_result()
    offset()                        : Apply an OFFSET to the query
    one()                           : Return exactly one result or raise exception
    one_or_none()
    only_return_tuples()
    options()
    order_by()                      : Apply one or more ORDER BY criterion
    outerjoin()
    params()
    populate_existing()
    prefix_with()
    reset_joinpoint()
    scalar()
    select_entity_from()
    select_from()
    selectable
    slice()                         : Compute slice of query represented by the given indecies
    statement
    subquery()                      : Return the full SELECT statement represented by the query within an Alias
    suffix_with()
    union()                         : Produce UNION of this Query against one or more queries
    union_all()
    update()
    value()
    values()
    whereclause
    with_entities()
    with_for_update()
    with_hint()
    with_labels()
    with_lockmode()
    with_parent()
    with_polymorphic()              : Load columns for inheriting classes
    with_session()                  : Return query that will use given session
    with_statement_hint()
    with_transformation()
    yield_per()                     : Yield only count rows at a time
  * ORM-Specific Query Constructs



ORM: USING THE SESSION
===============================================================================
- Session Basics --------------------------------------------------------------
  * What does the Session do?       : The session establishes all conversations with the database and represents
                                      a "holding zone" for all objects which have been loaded or assoiciated
                                      during its lifespace
                                    : Begins in stateless form
                                      Gets connection resource from engin
                                      Connection represents and ongoing transaction
                                      Transaction remains in effect until session commits or rollsback
                                    : Unit of Work Pattern
                                      All changes maintained by session are tracked. Before the database
                                      is queried again or before the current transaction is committed, it flushes
                                      all pending changes to the database
  * Getting a Session               : Ex.
                                      from sqlalchemy import create_engine
                                      from sqlalchemy.orm import sessionmaker

                                      # an Engine, which the Session will use for connection resources
                                      some_engine = create_engine('postgresql://scott:tiger@localhost/')

                                      # create a configured "Session" class, then session
                                      Session = sessionmaker(bind=some_engine)
                                      session = Session()

                                      # work with sess
                                      myobject = MyObject('foo', 'bar')
                                      session.add(myobject)
                                      session.commit()
  * Session Frequently Asked Q's
    When do I make a sessionmaker?  : Just one time, in application global scope
    When do I construct a session,
    commit it, and close it?        : Session lifecycle should be separate and external from functions or objects
                                      that manipulate database data
                                      Keep transactions short
                                      Session constructed at beginning of logical operatoin
                                      Sessions can last multiple transactions
                                      Common to have session scope = transaction scope
                                      Ex. Use context manager to manage session scope
                                      from contextlib import contextmanager

                                      @contextmanager
                                      def session_scope():
                                          """Provide a transactional scope around a series of operations."""
                                          session = Session()
                                          try:
                                              yield session
                                              session.commit()
                                          except:
                                              session.rollback()
                                              raise
                                          finally:
                                              session.close()

                                      def run_my_program():
                                          with session_scope() as session:
                                              ThingOne().go(session)
                                              ThingTwo().go(session)
    Is the session a cache?         : Yee... no. Implements the identity map pattern.
                                      Does not do query caching.
                                      Use dogpile.cache for second level cache.
    How to get session for an
    object?                         : session = Session.object_session(someobject)
                                      -or-
                                      session = inspect(someobject).session
    Is the session thread-safe?     : No. Share nothing approach to concurrency.
                                      All objects that are associated with that session must be kept in scope
                                      of single thread
                                      Common paradigm is to have one session per concurrent thread and use
                                      Session.merge() to copy state of an object into a new object local to a
                                      different session
  * Basics of Using a Session
    Querying                        : Ex.
                                      # query from a class
                                      session.query(User).filter_by(name='ed').all()

                                      # query with multiple classes, returns tuples
                                      session.query(User, Address).join('addresses').filter_by(name='ed').all()

                                      # query using orm-enabled descriptors
                                      session.query(User.name, User.fullname).all()

                                      # query from a mapper
                                      user_mapper = class_mapper(User)
                                      session.query(user_mapper)
   Adding new or existing items     : Ex1.
                                      user1 = User(name='user1')
                                      user2 = User(name='user2')
                                      session.add(user1)
                                      session.add(user2)
                                      session.commit()     # write changes to the database

                                      Ex2.
                                      session.add_all([item1, item2, item3])
                                      session.commit()
   Delete                           : Ex.
                                      # mark two objects to be deleted
                                      session.delete(obj1)
                                      session.delete(obj2)
                                      # commit (or flush)
                                      session.commit()
   Flush                            : Flushing communicates a series of operations to the database (insert, update, delete)
                                      The database maintains them as pending operations in a transaction
                                      The changes aren't perissted permanently to disk, or visible to other transactions
                                      until the database receives a commit for the current transaction
                                      session.flush()
   autoflush                        : Default=True. When the Session is used with its default configuration, 
                                      the flush step is nearly always done transparently. Specifically, 
                                      the flush occurs before any individual Query is issued, as well as 
                                      within the commit() call before the transaction is committed. It also 
                                      occurs before a SAVEPOINT is issued when begin_nested() is used.
                                      When set to False, flush-on-query is disabled
   Commit                           : Commits (persists) transaction changes to database
                                      session.commit()
   autocommit                       : Default=False. When set to True, SQLAlchemy does not automicatically 
                                      open a transaction during the session. Transactions begin with session.begin()
                                      If any data-changing operation (INSERT, UPDATE, DELETE) occurs outside
                                      of a transaction, SQLAlchemy will automatically commit it
   Roll Back                        : Rolls back the current transaction
                                      Sessopm may safely continue usage after a rollback occurs
   Close                            : Issues expunge_all() and releases any transactional/connection resources
                                      session.close()
- State Management ------------------------------------------------------------
  * Object States
    Transient                       : An instance that's not in a session, and is not saved to the database
                                      Has a mapper() associated with it
    Pending                         : When one add()'s a transient instance, it becomes bending. It
                                      still wasn't actually flushed to the database yet, but will be when
                                      the next flush occurs
    Persistent                      : Instance which is present in the session and has a record in the database
    Deleted                         : An instance which has been deleted within a flush, but the transaction has not
                                      yet completed.
                                      If transaction committed -> detached
                                      If transaction rolled back -> persistent
    Detached                        : An instance which corresponds, or previously corresponded, to a record
                                      in the database, but is not currently in any session
  * Session Attributes              : Session acts like a set-like collection
                                      Ex1.
                                      for obj in session:
                                          print(obj)
                                      Ex2.
                                      if obj in session:
                                          print("Object is present")

    Pending objects recently added  : session.new
    Persistent objects which
    currently have changes          : session.dirty
    Persistent objects that have
    been marked for deletion        : session.deleted
    Dictionary of all persistent
    objects, keyed on indentity key : session.identity_map
  * Session Referencing Behavior    : Objects within the session are weakly referenced. This means that when they
                                      are dereferenced in the outside application, they fall out of scope from within
                                      the session as well and are subject to garbage collection by the Python
                                      interpreter
  * Merge                           : Merge transfers state from an outside object into a new or already existing
                                      instance within a session
                                      Ex.
                                      merged_object = session.merge(existing_object)
  * Expunge                         : Removes an object from the session. Sends persistent instances to detached
                                      and pending instances to transient state.
                                      Ex1.
                                      session.expunge(obj1)
                                      Ex2.
                                      session.expung_all()
  * Refresh / Expire                : Expiring means that the database-persisted data held inside a series of
                                      object attributes is erased
    Expire, print state             : Ex.
                                      user = session.query(User).filter_by(name='user1').first()
                                      >>> user.__dict__
                                      {
                                        'id': 1, 'name': u'user1',
                                        '_sa_instance_state': <...>,
                                      }
                                      >>> session.expire(user)
                                      >>> user.__dict__
                                      {'_sa_instance_state': <...>}  # Instance state is expired

    Expire then refresh attributes  : Ex. Expire specific attributes, then refresh them
                                      session.expire(obj1, ['attr1', 'attr2'])
                                      session.refresh(obj1)

- Cascades --------------------------------------------------------------------
  * Description                     : Mappers support the concept of configurable cascade behavior on relationship() constructs
                                      Default cascade is save-update and merge
                                      Ex.
                                      class Order(Base):
                                          __tablename__ = 'order'
                                          items         = relationship("Item", cascade="all, delete-orphan")
                                          customer      = relationship("User", cascade="save-update")
  * save-update                     : When an object is placed into a session via session.add() all the
                                      objects associated with it in this relationship should also be
                                      added to that same session
  * delete                          : When parent object is marked for deletion, its related child objects
                                      should also be marked for deletion
  * delete-orphan                   : Adds behavior to the delete cascade, such that a child object will
                                      be marked for deletion when it is deassociated from the parent
  * merge                           : Propagate session.merge() from parent down to referred objects
  * refresh-expire                  : Propagate session.expire() from parent down to referred objects
  * expunge                         : Propagage session.expunge() from parent down to referred objects
  * all                             : Alias for save-update, merge, refresh-expire, expunge, delete
  * Controlling Cascade on Backrefs

- Transactions and Connection Management --------------------------------------
  * Managing Transactions           : Ex. Session lifecycle
                                      engine = create_engine("...")
                                      Session = sessionmaker(bind=engine)

                                      # new session. no connections are in use.
                                      session = Session()
                                      try:
                                          # first query.  a Connection is acquired
                                          # from the Engine, and a Transaction
                                          # started.
                                          item1 = session.query(Item).get(1)

                                          # second query.  the same Connection/Transaction
                                          # are used.
                                          item2 = session.query(Item).get(2)

                                          # pending changes are created.
                                          item1.foo = 'bar'
                                          item2.bar = 'foo'

                                          # commit.  The pending changes above
                                          # are flushed via flush(), the Transaction
                                          # is committed, the Connection object closed
                                          # and discarded, the underlying DBAPI connection
                                          # returned to the connection pool.
                                          session.commit()
                                      except:
                                          # on rollback, the same closure of state
                                          # as that of commit proceeds.
                                          session.rollback()
                                          raise
                                      finally:
                                          # close the Session.  This will expunge any remaining
                                          # objects as well as reset any existing SessionTransaction
                                          # state.  Neither of these steps are usually essential.
                                          # However, if the commit() or rollback() itself experienced
                                          # an unanticipated internal failure (such as due to a mis-behaved
                                          # user-defined event handler), .close() will ensure that
                                          # invalid state is removed.
                                          session.close()
    SAVEPOINT transactions          : Are delineated by begin_nested() method
                                      begin_nested() may be called any number of times, for each begin_nested()
                                      call, a corresponding rollback() or commit() must be issued
                                      Useful for catching unique constraint violations
    Autocommit mode
    Enabling Two-Phase Commit
    Setting Transaction Isolation Lvl
  * Joining a Session into an
    External Transaction
- Additional Persistence Techniques -------------------------------------------
  * Embedding SQL Insert/Update
    Expressions into a Flush
  * Using SQL Expressions with
    Sessions                        : Can execute SQL expressions within session's transactional context
                                      Ex.
                                      # execute a string statement
                                      result = session.execute("select * from table where id=:id", {'id':7})
                                      # execute a SQL expression construct
                                      result = session.execute(select([mytable]).where(mytable.c.id==7))
  * Forcing NULL on a column with
    a default                       : ORM considers any attribute that was never set on an object as a "default"
                                      case; object will be omitted from the insert statement
    Attribute omitted               : class MyObject(Base):
                                          __tablename__ = 'my_table'
                                          id            = Column(Integer, primary_key=True)
                                          data          = Column(String(50), nullable=True)
                                      obj = MyObject(id=1)
                                      session.add(obj)
                                      session.commit()  # INSERT with the 'data' column omitted; the database
                                                        # itself will persist this as the NULL value
    server_default used             : class MyObject(Base):
                                          __tablename__ = 'my_table'
                                          id            = Column(Integer, primary_key=True)
                                          data          = Column(String(50), nullable=True, server_default="default")
                                      obj = MyObject(id=1)
                                      session.add(obj)
                                      session.commit()  # INSERT with the 'data' column omitted; the database
                                                        # itself will persist this as the value 'default'
    server_default overrides None   : class MyObject(Base):
                                          __tablename__ = 'my_table'
                                          id            = Column(Integer, primary_key=True)
                                          data          = Column(String(50), nullable=True, server_default="default")
                                      obj = MyObject(id=1, data=None)
                                      session.add(obj)
                                      session.commit()  # INSERT with the 'data' column explicitly set to None;
                                                        # the ORM still omits it from the statement and the
                                                        # database will still persist this as the value 'default'
    null()                          : obj = MyObject(id=1, data=null())
                                      session.add(obj)
                                      session.commit()  # INSERT with the 'data' column explicitly set as null();
                                                        # the ORM uses this directly, bypassing all client-
                                                        # and server-side defaults, and the database will
                                                        # persist this as the NULL value
    evaluates_none()                : class MyObject(Base):
                                          __tablename__ = 'my_table'
                                          id            = Column(Integer, primary_key=True)
                                          data          = Column(
                                            String(50).evaluates_none(),  # indicate that None should always be passed
                                            nullable=True,
                                            server_default="default")
                                      obj = MyObject(id=1, data=None)
                                      session.add(obj)
                                      session.commit()  # INSERT with the 'data' column explicitly set to None;
                                                        # the ORM uses this directly, bypassing all client-
                                                        # and server-side defaults, and the database will
                                                        # persist this as the NULL value

  * Fetching Server-Generated
    Defaults
  * Partitioning Strategies
    Simple Veritcal Partitioning    : Place different classes, class hierarchies, or mapped tables across
                                      multiple databases, by configuring the session with the session.binds
                                      argument
    Custom Vertical Partitioning    : Send reads to slave DBs and writes to the master
    Horizontal Partitioning         : Partitions the rows of a single table(or set of tables) across multiple
                                      databases. Also known as Horizontal Sharding
  * Bulk Operations                 : Directly expose internal elements of unit of work system,
                                      bypassing the normal unit of work mechanics of state, relationship,
                                      and attribute management
    Relevant functions              : bulk_save_objects()
                                      bulk_insert_mappings()
                                      bulk_update_mappings()
- Contextual/Thread-local Sessions --------------------------------------------
  * Description                     : Ex.
                                      >>> from sqlalchemy.orm import scoped_session
                                      >>> from sqlalchemy.orm import sessionmaker
                                      >>> session_factory = sessionmaker(bind=some_engine)
                                      >>> Session         = scoped_session(session_factory) # Create registry of session objects
                                      >>> some_session    = Session()
                                      >>> other_session   = Session()                       # Registry returns same session
                                      >>> some_session is other_session
                                      True
                                      >>> Session.remove()                                  # Remove session
                                      >>> new_session     = Session()                       # Create new session
                                      >>> new_session is some_session
                                      False
  * Implicit Method Access          : scoped_session holds onto a Session for all who ask for it
                                      Registry is a proxy itself to the underlying session
                                      Ex.
                                      Session = scoped_session(some_factory)
                                      # equivalent to:
                                      # session = Session()
                                      #
                                      print(Session.query(MyClass).all())
  * Thread-Local Scope              : scoped_session is stored in thread local storage
  * Using Thread-Local Scope with
    Web Applications
  * Using Custom Created Scopes     : Can create custom session scopes
                                      Ex. Tie session scope to web request
                                      from my_web_framework import get_current_request, on_request_end
                                      from sqlalchemy.orm import scoped_session, sessionmaker

                                      Session = scoped_session(sessionmaker(bind=some_engine), scopefunc=get_current_request)
                                      @on_request_end
                                      def remove_session(req):
                                          Session.remove()
  * Contextual Session API
- Tracking Object and Session Changes with Events -----------------------------
  * Description                     : SQLAlchemy has extensive Event Listening system used throughout Core and ORM
  * Persistence Events              : See ORM: EVENTS AND INTERNALS
  * Object Lifecycle Events         : See ORM: EVENTS AND INTERNALS
  * Transaction Events              : See ORM: EVENTS AND INTERNALS
  * Attribute Change Events         : See ORM: EVENTS AND INTERNALS
- Session API -----------------------------------------------------------------
  * Session and sessionmaker()
  * Session Utilities
  * Attribute and State Management
    Utilities



ORM: EVENTS AND INTERNALS
===============================================================================
- ORM Events ------------------------------------------------------------------
  * Usage                           : from sqlalchemy import event
                                      # standard decorator style
                                      @event.listens_for(SomeSessionOrFactory, 'after_attach')
                                      def receive_after_attach(session, instance):
                                          "listen for the 'after_attach' event"
                                          # ... (event handling logic) ...
  * Attribute Events
    append                          : Collection append event
    bulk_replace                    : Colllection bulk replace
    dispose_collection              : After collection is replaced
    init_collection                 : After collection is first generated
    init_scalar                     : After uninitialized, unpersisted scalar is accessed
    modified                        : After attribute has been modified
    remove                          : After attribute has been removed
    set                             : After attribute has been set
  * Mapper Events
    after_configured                : After mapper configured
    after_delete                    : After mapper deleted
    after_insert                    : After insert emitted for that instance
    after_update                    : After update emitted for that instance
    before_configured               : Before a series of mappers have been configured
    before_delete                   : Before delete statement emitted
    before_insert                   : Before insert
    before_update                   : Before update
    instrument_class                : Before instrumentation applied to class
    mapper_configured               : After mapper completed configuration
  * Instance Events
    expire                          : After attributes have expired
    first_init                      : After first instance of mapping is called
    init                            : After constructor is called
    init_failure                    : After constructor raised an exception
    load                            : After object instance created via __new__ and after initial attribute population
    pickle                          : Before pickle
    refresh                         : After one or more attributes has been refreshed
    refresh_flush                   : After one or more attributes has been refreshed within the persistence of the object
    unpickle                        : After associated state has been unpickled
  * Session Events
    after_attach                    : After instance attached to session
    after_begin                     : After transaction is begun on a connection
    after_bulk_delete               : After bulk delet operation
    after_bulk_update               : After bulk update operation
    after_commit                    : After commit occurred
    after_flush                     : After flush occured, before commit
    after_flush_postexec            : After flush completed and after post-exec state
    after_rollback                  : After DBAPI rollback has occurred
    after_soft_rollback             : After any rollback, includeing soft rollbacks
    after_transaction_create        : After new SessionTransaction is created
    after_transaction_end           : After SessionTransaction ends
    before_attach                   : Before instance is attached to a session
    before_commit                   : Before commit is called
    before_flush                    : Before flush has started
    transient_to_pending            : After session.add()
    pending_to_persistent           : After session.flush()
    pending_to_transient            : After session.rollback()
    loaded_as_persistent            : After object loaded from db
    persistent_to_transient         : After session.rollback()
    persistent_to_deleted           : After object deleted and session.flush()
    deleted_to_detached             : After deleted object and session.commit()
    persistent_to_detached          : After session.expunge()
    detached_to_persistent          : After session.add()
    deleted_to_persistent           : After deleted object and session.rollback()
  * Query Events
    before_compile                  : Before query is composed into a core Select object
  * Instrumentation Events
    attribute_instrument            : When attribute is instrumented
    class_instrument                : After class is instrumented
    class_uninstrument              : After class is uninstrumented
- ORM Internals ---------------------------------------------------------------
- ORM Exceptions --------------------------------------------------------------
  * Class path                      : sqlalchemy.orm.exc
  * ConcurrentModificationError     : Alias of StaleDataError
  * DetachedInstanceError           : Attempt to access unloaded attributes
  * FlushError                      : An invalid condition detected during flush()
  * MultipleResultsFound            : A single database result was required but more than one was found
  * NoResultFound                   : A database result was required but non was found
  * ObjectDeletedError              : Underlying database object deleted
  * ObjectDereferencedError         : Operation cannot complete due to object being garbage collected
  * StaleDataError                  : Version incremented and is now stale
  * UnmappedClassError              : Mapping created for unknown class
  * UnmappedColumnError             : Mapping operation requested on an unknown column
  * UnmappedError                   : Base for expecptions that involve expected mappings not present
  * UnmappedInstanceError           : Mapping operation was request for unknown instance



ORM: EXTENSIONS
===============================================================================
- Association Proxy                 : associationproxy is used to create a read/write view of a target attribute
                                      across a relationship. It essentially conceals the usage of a “middle”
                                      attribute (association) between two endpoints, and can be used to cherry-pick
                                      fields from a collection of related objects or to reduce the verbosity of
                                      using the association object pattern.
- Automap                           : Define an extension to the sqlalchemy.ext.declarative system which
                                      automatically generates mapped classes and relationships from a
                                      (usually reflected) database schema
- Baked Queries                     : Baked provides an alternative creational pattern for Query objects,
                                      which allows for caching of the object’s construction and string-compilation
                                      steps. This means that for a particular Query building scenario that is
                                      used more than once, all of the Python function invocation involved
                                      in building the query from its initial construction up through generating
                                      a SQL string will only occur once, rather than for each time that query
                                      is built up and executed.
- Declarative                       : Way to map classes, layers over classical
- Mutation Tracking                 : Provide support for tracking of in-place changes to scalar values,
                                      which are propagated into ORM change events on owning parent objects.
- Ordering List                     : orderinglist is a helper for mutable ordered relationships. It will
                                      intercept list operations performed on a relationship()-managed collection
                                      and automatically synchronize changes in list position onto a target
                                      scalar attribute.
- Horizontal Sharding               : Horizontal sharding support. Defines a rudimental ‘horizontal sharding’
                                      system which allows a Session to distribute queries and persistence
                                      operations across multiple databases.
- Hybrid Attributes                 : Define attributes on ORM-mapped classes that have “hybrid” behavior.
                                      “hybrid” means the attribute has distinct behaviors defined at the class
                                      level and at the instance level.
- Indexable                         : Define attributes on ORM-mapped classes that have “index” attributes for
                                      columns with Indexable types. “index” means the attribute is associated
                                      with an element of an Indexable column with the predefined index to
                                      access it. The Indexable types include types such as ARRAY, JSON and HSTORE.
                                      The indexable extension provides Column-like interface for any element of
                                      an Indexable typed column. In simple cases, it can be treated as a Column
                                      - mapped attribute.
                                      Ex. Have name property index into data JSON column
- Alternate Class Instrumentation   : Extensible class instrumentation



DIALECTS
===============================================================================
- MySQL -----------------------------------------------------------------------
  * DBAPI Support
    MySQL-Python
    PyMySQL
    MySQL Connector/Python
    CyMySQL
    OurSQL
    Google Cloud SQL
    PyODBC
    zxjdbc for Jython
  * Supported Versions              : Support starts with MySQL 4.1
  * Connection Timeouts and
    Disconnects                     : MySQL features an automtic connection close behavior for connections
                                      that have been idle for 8 hours
                                      Workaround
                                      engine = create_engine('mysql+mysqldb://...', pool_recycle=3600)
- Firebird --------------------------------------------------------------------
- Microsoft SQL Server --------------------------------------------------------
- Oracle ----------------------------------------------------------------------
- PostgreSQL ------------------------------------------------------------------
- SQLite ----------------------------------------------------------------------
- Sybase ----------------------------------------------------------------------

