#!/usr/bin/python3.5
"""
NAME
 tutorial.py

DESCRIPTION
 Demonstrates Basic usage of SQLAlchemy Core

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/tutorial.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, select
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- GLOBALS ------------------------------------- #
Base     = declarative_base()
engine   = create_engine("sqlite:///:memory:", echo=False)
metadata = MetaData()


# ----------------------------- TABLES -------------------------------------- #
users = Table(
    'users', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    Column('fullname', String))

addresses = Table(
    'addresses', metadata,
    Column('id', Integer, primary_key=True),
    Column('user_id', None, ForeignKey('users.id')),
    Column('email_address', String, nullable=False))


# ----------------------------- FUNCTIONS ----------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate database
    """
    conn = engine.connect()
    conn.execute(
        users.insert(),  # pylint: disable=E1120
        [{'id': 1, 'name': 'Jack', 'fullname': 'Jack Jones'},
         {'id': 2, 'name': 'Wendy', 'fullname': 'Wendy Williams'}])
    conn.execute(addresses.insert(), [  # pylint: disable=E1120
        {'user_id': 1, 'email_address': 'jack@yahoo.com'},
        {'user_id': 1, 'email_address': 'jack@msn.com'},
        {'user_id': 2, 'email_address': 'www@www.org'},
        {'user_id': 2, 'email_address': 'wendy@aol.com'},
    ])


@logFunc()
def printState():
    """
    Print database state
    """
    conn = engine.connect()

    printTitle("Users")
    result = conn.execute(select([users]))
    for row in result:
        print(row)
        print("name:{}, fullname:{}".format(row[users.c.name], row[users.c.fullname]))
    result.close()

    printTitle("Users and Addresses")
    result = conn.execute(select([users, addresses]).where(users.c.id == addresses.c.user_id))
    for row in result:
        print(row)
    result.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState()
    deleteTables()
