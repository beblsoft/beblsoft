#!/usr/bin/env python3
"""
NAME
 columnDefaults.py

DESCRIPTION
 Demonstrates Column Defaults

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/defaults.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, select
from sqlalchemy import Table, Column, Integer
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- GLOBALS ------------------------------------- #
Base     = declarative_base()
engine   = create_engine("sqlite:///:memory:", echo=False)
metadata = MetaData()
i        = 0


# ----------------------------- TABLE FUNCTIONS ----------------------------- #
def defaultFunction():
    """
    Used to calculate default column value
    """
    global i
    i += 1
    return i


def updateFunction():
    """
    Used to update column
    """
    global i
    i += 1
    return i


# ----------------------------- TABLES -------------------------------------- #
users = Table(
    'users', metadata,
    Column('id', Integer, primary_key=True),
    Column('int1', Integer, default=12),              # 12 bound on INSERT if no other value supplied
    Column('int2', Integer, onupdate=25),             # 25 bound on UPDATE if no other value supplied
    Column('int3', Integer, default=defaultFunction),
    Column('int4', Integer, onupdate=updateFunction),
    Column('lastUpdated', Integer, onupdate=datetime.now),
)


# ----------------------------- FUNCTIONS ----------------------------------- #
@logFunc()
def createTables():  # pylint: disable=C0111
    metadata.create_all(engine)


@logFunc()
def deleteTables():  # pylint: disable=C0111
    metadata.drop_all(engine)


@logFunc()
def populate():
    """
    Populate tables
    """
    printTitle("Populating")
    conn = engine.connect()
    conn.execute(
        users.insert(),  # pylint: disable=E1120
        [{'id': 1, }])


@logFunc()
def updateData():
    """
    Update data
    """
    conn = engine.connect()
    stmt = users.update().where(users.c.id == 1).values(id=2)  # pylint: disable=E1120
    conn.execute(stmt)


@logFunc()
def printState(title):
    """
	Print database state
    """
    conn = engine.connect()
    # Users
    result = conn.execute(select([users]))
    printTitle(title)
    for row in result:
        print(row)
    print("\n")
    result.close()


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    createTables()
    populate()
    printState(title="After Populate")
    updateData()
    printState(title="After Update")
    deleteTables()
