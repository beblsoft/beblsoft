#!/usr/bin/env python3
"""
NAME
 pool.py

DESCRIPTION
 Demonstrates Different Connection Pools

BIBLIOGRAPHY
 https://docs.sqlalchemy.org/en/latest/core/engines.html#engine-creation-api
 https://docs.sqlalchemy.org/en/latest/core/pooling.html
"""

# ----------------------------- IMPORTS ------------------------------------- #
import pprint
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import QueuePool, SingletonThreadPool, AssertionPool, NullPool, StaticPool
from base.bebl.python.log.bLogFunc import logFunc
from base.bebl.python.print.bTitle import printTitle


# ----------------------------- GLOBALS ------------------------------------- #
Base       = declarative_base()
engineStr  = "sqlite:///:memory:"
engineEcho = False
engineDict = {
    'queue': create_engine(engineStr, echo=engineEcho, poolclass=QueuePool),
    'singletonThread': create_engine(engineStr, echo=engineEcho, poolclass=SingletonThreadPool),
    'assertion': create_engine(engineStr, echo=engineEcho, poolclass=AssertionPool),
    'null': create_engine(engineStr, echo=engineEcho, poolclass=NullPool),
    'static': create_engine(engineStr, echo=engineEcho, poolclass=StaticPool)
}


# ----------------------------- FUNCTIONS ----------------------------------- #
@logFunc()
def iterEngines():
    """
    Iterate through engines0
    """
    for k, engine in engineDict.items():
        printTitle("Engine Pool: {}".format(k))
        print(pprint.pformat(engine.__dict__))
        print(pprint.pformat(engine.pool.__dict__))


# ----------------------------- MAIN ---------------------------------------- #
if __name__ == "__main__":
    iterEngines()
