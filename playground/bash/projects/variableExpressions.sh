#!/bin/bash
#
# Description
#  Demonstrate different variable expressions
#

# --------------------------- MAIN ------------------------------------------ #
#Display environmental variables
echo "User info for userid: $USER"
echo UID: $UID
echo HOME: $HOME

#Variables
var0=10
echo The variable is $var0

#Backtick
testing=`date`
echo "The data and time are: " $testing

#Expression
var1=10
var2=20
var3=$[$var2 / $var1]
var4=$[$var3 + 1]
echo $var1 divided by $var2 is $var3
echo $var3 plus 1 is $var4
