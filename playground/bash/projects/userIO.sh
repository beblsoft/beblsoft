#!/bin/bash
#
# Description
#  Program to simulate user io
#

# --------------------------- MAIN ------------------------------------------ #
if read -t 5 -p "Please enter your name:" first last
then
  echo "Hello $first $last"
else
  echo "Sorry, too slow"
fi

read -n1 -p "Do you want to continue [Y/N]?" answer
case $answer in
  Y | y) echo
         echo "Fine, continue on..." ;;
  N | n) echo
         echo "Ok, goodbye"
         exit ;;
esac
echo "This is the end of the script."


