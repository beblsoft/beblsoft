#!/bin/bash
#
# Description
#  Demonstrate signal handling
#

# --------------------------- MAIN ------------------------------------------ #
trap "echo ' Sorry! I have trapped Ctrl-C'" SIGINT SIGTERM
trap "echo byebye" EXIT

echo This is the start
sleep 10
echo This is the end