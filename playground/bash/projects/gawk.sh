#!/bin/bash
#
# Description
#  Demonstrate basic gawk capabilities
#

# --------------------------- MAIN ------------------------------------------ #
gawk '{print $0}' static/gawk_test

echo "My name is Rich" | gawk '{$4="Christine"; print $0}'