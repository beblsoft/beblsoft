#!/bin/bash
#
# Description
#  Script to multiply two numbers
#


# --------------------------- MAIN ------------------------------------------ #
echo "There were $# parameters given"

if [ $# -ne 2 ]
then
  echo "Please insert two numbers."
  exit 1
fi

count=1
for param in "$@"
do
  echo "Parameter #$count = $param"
  count=$[ $count + 1 ]
done

sol=$[ $1 * $2 ]
echo "The product of $1 and $2 is $sol."

