#!/bin/bash
#
# Description
#  Demonstrate conditional usage
#

# --------------------------- MAIN ------------------------------------------ #
#Exit status of program
if date
then
  echo "Date command exited 0."
fi

#Variable Comparitors
val1=10
val2=11
if [ $val1 -gt 5 ]
then
  echo "The test value $val1 is greater than 5"
fi

#If, Else
if [ $val1 -eq $val2 ]
then
  echo "The values are equal"
else
  echo "The values, $val1 and $val2, are different"
fi

#User Comparitor
me=jbensson
if [ $USER != $me ]
then
  echo "This is not $me"
else
  echo "Welcome $me"
fi

#File Comparitor
testfile="struc_cmds"
if [ -f $testfile ]
then
  echo "$testfile is in "
  pwd
else
  echo "$testfile is not in "
  pwd
fi

#File Comparisons
file1="struc_cmds"
file2="hello_world"
if [ $file1 -nt $file2 ]
then
  echo "$file1 is newer than $file2"
else
  echo "$file2 is newer than $file1"
fi

#Double Parentheses
val1=10
if (( $val1 ** 2 > 90  ))
then
  (( val2 = $val1 ** 2  ))
  echo "The square of $val1 is $val2"
fi

#Case command
case $USER in
rich | barbara | jbensson)
  echo "Welcome, $USER"
  echo "Please enjoy your visit";;
testing)
  echo "Special testing account";;
*)
  echo "Sorry, you are not allowed here";;
esac

