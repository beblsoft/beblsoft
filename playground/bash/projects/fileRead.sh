#!/bin/bash
#
# Description
#  Read from a file
#


# --------------------------- MAIN ------------------------------------------ #
read -p "Please enter a valid filename:" file

if [ -f $file ]
then
  count=1
  cat $file | while read line
  do
    echo "Line $count: $line"
    count=$[ $count + 1 ]
  done
  echo "Finished processing the file."
else
  echo "$file, is not a valid file."
fi




