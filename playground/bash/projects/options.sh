#!/bin/bash
#
# Description
#  Parse Options
#

# --------------------------- MAIN ------------------------------------------ #
#Extracting command line options as parameters
set -- `getopt -q ab "$@"`

while [ -n "$1" ]
do
  case "$1" in
  -a) echo "Found the -a option" ;;
  -b) echo "Found the -b option" ;;
  *) echo "$1 is not an option" ;;
  esac
  shift
done


