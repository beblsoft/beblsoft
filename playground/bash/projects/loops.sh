#!/bin/bash
#
# Description
#  Demonstrate loop usage
#

# --------------------------- MAIN ------------------------------------------ #
#For Loop
for state in Alabama Alaska Arizona Arkansas California Colorado "New Mexico"
do
  echo The next state is $state
done

#Reading Values from a file
file="letters"
for letter in $(cat $file)
do
  echo $letter
done

#Iterate through all files in a directory
for file in /home/jbensson/*
do
  if [ -d "$file" ]
  then
    echo "$file is a directory"
  elif [ -f "$file" ]
  then
    echo "$file is a file"
  fi
done

#C-Style For Loop
for (( a=1, b=10 ; a <= 10; a++, b-- ))
do
  echo "($a,$b)"
done

#While Loop
var1=10
while [ $var1 -gt 0 ]
do
  echo $var1
  var1=$[ $var1 -1 ]
done

#Until Loop
var1=100
until [ $var1 -eq 0 ]
do
  echo $var1
  var1=$[ $var1 - 25 ]
done

#Break- terminates innner most loop
for var1 in 1 2 3 4 5 6 7 8 9 10
do
  if [ $var1 -eq 5 ]
  then
    break
  fi
  echo "Iteration number: $var1"
done


#Contine
for (( var1 = 1; var1 < 15; var1++ ))
do
  if [ $var1 -gt 5 ] && [ $var1 -lt 10 ]
  then
    continue
  fi
  echo "Iteration number: $var1"
done

#Pipe to file
for (( i=1; i < 10; i++ ))
do
  echo "$i"
done > static/count.txt
