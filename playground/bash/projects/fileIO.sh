#!/bin/bash
#
# Description
#  Read and write to a file
#


# --------------------------- MAIN ------------------------------------------ #
testFile="static/test"

#Write to a file, then print contents
who > ${testFile}
cat ${testFile}

#Write to both test and STDOUT
date | tee ${testFile}
cat ${testFile}

#Map STOUT to test
echo "Appending a line to test" >> ${testFile}

#Read into STDIN
exec 0<${testFile}
count=1
while read line
do
  echo "Line #$count: $line"
  count=$[ $count + 1 ]
done

#Reasigning file descriptors
exec 3>&1
exec 1>${testFile}
echo "This should store in test"
exec 1>&3
echo "This should go to console"

#Making a temporary directory and file
tempfile=`mktemp -t tmp.XXXXXX`
echo "This is a test file." >> $tempfile
echo "The temp file is located at: $tempfile"
cat $tempfile
rm -f $tempfile
