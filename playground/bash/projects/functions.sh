#!/bin/bash
#
# Description
#  Demonstrate loop usage
#
# Note
#  Must write function before calling it


# --------------------------- HELPER FUNCTIONS ------------------------------ #
function iterator {
  echo "Iterating!"
  local count #Variable only visible inside function
  count=1
  while [ $count -le 5 ]
  do
    echo "Count=$count"
    count=$[ $count + 1 ]
  done
  return $count
}

function dbl {
  read -p "Enter a value: " value
  echo $[ $value * 2 ]
}

function factorial {
  if [ $1 -eq 1 ]
  then
    echo 1
  else
    local temp=$[ $1 -1 ]
    local result=`factorial $temp`
    echo $[ $result * $1 ]
  fi
}

# --------------------------- MAIN ------------------------------------------ #
#Simple Function
iterator

#Returning arguments
twoTimes=`dbl`
echo "The doubled value is $twoTimes"

#Passing args in and out
read -p "Eter value: " value
result=`factorial $value`
echo "The factorial of $value is: $result"
