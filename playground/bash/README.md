# Bash Technology Documentation

Bash is a Unix shell and command language written by Brian Fox for the GNU Project as a free software
replacement for the Bourne SHell. Since its first release in 1989, it has been distributed widely as the
default login on most Linux distributions and other platforms.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Bash_(Unix_shell)),
[Home](https://www.gnu.org/software/bash/)

## Keywords

```bash
#  expr, $[operation], eval            - Evaluate expression
#  if, [], elif, else, then            - Conditional statement
#  &&, ||                              - Logical operators
#  (()), ++, --, !, ~, **, >>, <<,     - Double parentheses command symbols
#  case, esac, *);;                    - Switch case
#  for var in list; do ...; done;      - For loop
#  for i in $(seq 1 1 10); do; done;   - For loop
#  while, do, done                     - While loop
#  until, do, done                     - Until loop
#  break n                             - Break out of n loops
#  continue                            - Continue loop
#  $0, $1, $2, ... ${10}, $#           - Access command line parameters
#  $#, ${!#}, $*, $@, shift n          - #, last, one line, string, downgrade param
#  set -- 'getopt -q opstr "$@"'       - Option parsing
#  read, -p, -t, -n1, -s               - Reading, options
#  function name () {cmds}             - Function definition
#  return, echo                        - Methods to return
#  source, .                           - Execute commands within context, load library
#  caller                              - Access info on calling function
```

## Variables

```bash
#   $0, $1,...  - positional parameters
#   $#          - number of command line arguments
#   "$*"        - all of parameters seen as a single word
#   "$@"        - same as $*, but each parameter is in quoted string
#   $-          - flags passed to script
#   $!          - pid of last background job
#   $_          - final arg of previous command executed
#   $?          - exit status of a command
#   $$          - pid of the script itself
```

## Integer Comparison

```bash
#   -eq       - equal
#   -ge       - greater than or equal
#   -le       - less than or equal
#   -lt       - less than
#   -ne       - not equal
```

## String Comparison

```bash
#  =, ==      - is equal                - if [ "$a" = "$b" ]
#  !=         - is not equal            - if [ "$a" != "$b" ]
#  \<         - less than per ASCI      - if [ "$a" \< "$b" ]
#  \>         - greater than per ASCI   - if [ "$a" \> "$b" ]
#  -n         - string is not null
#  -z         - string is null
```

## File Comparison

```bash
#  -e         - exists
#  -s         - not size zero
#  -d         - directory
#  -f         - regular file (not device)
#  -b         - block device
#  -c         - character device
#  -p         - pipe
#  -h, -L     - symbolic link
#  -S         - socket
#  -t         - associated with terminal device
#  -r         - read permission
#  -w         - write permission
#  -x         - execute permission
#  -g         - group id flag set on file
#  -u         - user id flag set on file
#  -k         - sticky bit set
#  -O         - you are owner
#  -G         - you are part of group
#  f1 -nt f2  - file f1 newer than f2
#  f1 -ot f2  - file f1 older than f2
#  f1 -et f2  - f1 and f2 are hard links to same file
#
```

## String Operations

```bash
# ${#string}                              - Length of $string
# ${string:position}                      - Extract substring from $string at $position
# ${string:position:length}               - Extract $length characters substring from $string at $position
#                                           [zero-indexed, first character is at position 0]
# ${string#substring}                     - Strip shortest match of $substring from front of $string
# ${string##substring}                    - Strip longest match of $substring from front of $string
# ${string%substring}                     - Strip shortest match of $substring from back of $string
# ${string%%substring}                    - Strip longest match of $substring from back of $string
# ${string/substring/replacement}         - Replace first match of $substring with $replacement
# ${string//substring/replacement}        - Replace all matches of $substring with $replacement
# ${string/#substring/replacement}        - If $substring matches front end of $string, substitute $replacement for $substring
# ${string/%substring/replacement}        - If $substring matches back end of $string, substitute $replacement for $substring
# expr match "$string" '$substring'       - Length of matching $substring* at beginning of $string
# expr "$string" : '$substring'           - Length of matching $substring* at beginning of $string
# expr index "$string" $substring         - Numerical position in $string of first character in $substring*
#                                           that matches [0 if no match, first character counts as position 1]
# expr substr $string $position $length   - Extract $length characters from $string starting at $position
#                                           [0 if no match, first character counts as position 1]
# expr match "$string" '\($substring\)'   - Extract $substring*, searching from beginning of $string
# expr "$string" : '\($substring\)'       - Extract $substring* , searching from beginning of $string
# expr match "$string" '.*\($substring\)' - Extract $substring*, searching from end of $string
# expr "$string" : '.*\($substring\)'     - Extract $substring*, searching from end of $string
# ${#string}                              - Length of $string
# ${string:position}                      - Extract substring from $string at $position
# ${string:position:length}               - Extract $length characters substring from $string at $position
#                                           [zero-indexed, first character is at position 0]
# ${string#substring}                     - Strip shortest match of $substring from front of $string
# ${string##substring}                    - Strip longest match of $substring from front of $string
# ${string%substring}                     - Strip shortest match of $substring from back of $string
# ${string%%substring}                    - Strip longest match of $substring from back of $string
# ${string/substring/replacement}         - Replace first match of $substring with $replacement
# ${string//substring/replacement}        - Replace all matches of $substring with $replacement
# ${string/#substring/replacement}        - If $substring matches front end of $string, substitute $replacement for $substring
# ${string/%substring/replacement}        - If $substring matches back end of $string, substitute $replacement for $substring
# expr match "$string" '$substring'       - Length of matching $substring* at beginning of $string
# expr "$string" : '$substring'           - Length of matching $substring* at beginning of $string
# expr index "$string" $substring         - Numerical position in $string of first character in $substring* that matches
#                                           [0 if no match, first character counts as position 1]
# expr substr $string $position $length   - Extract $length characters from $string starting at $position
#                                           [0 if no match, first character counts as position 1]
# expr match "$string" '\($substring\)'   - Extract $substring*, searching from beginning of $string
# expr "$string" : '\($substring\)'       - Extract $substring* , searching from beginning of $string
# expr match "$string" '.*\($substring\)' - Extract $substring*, searching from end of $string
# expr "$string" : '.*\($substring\)'     - Extract $substring*, searching from end of $string
```

## Parameter Substitution and Expansion

```bash
# ${var}               - Value of var (same as $var)
# ${var-$DEFAULT}      - If var not set, evaluate expression as $DEFAULT *
# ${var:-$DEFAULT}     - If var not set or is empty, evaluate expression as $DEFAULT *
# ${var=$DEFAULT}      - If var not set, evaluate expression as $DEFAULT *
# ${var:=$DEFAULT}     - If var not set or is empty, evaluate expression as $DEFAULT *
# ${var+$OTHER}        - If var set, evaluate expression as $OTHER, otherwise as null string
# ${var:+$OTHER}       - If var set, evaluate expression as $OTHER, otherwise as null string
# ${var?$ERR_MSG}      - If var not set, print $ERR_MSG and abort script with an exit status of 1.*
# ${var:?$ERR_MSG}     - If var not set, print $ERR_MSG and abort script with an exit status of 1.*
# ${!varprefix*}       - Matches all previously declared variables beginning with varprefix
# ${!varprefix@}       - Matches all previously declared variables beginning with varprefix${var}Value of var (same as $var)
```

## Arrays

```bash
# declare -a array                                - Declare array
# array=(value1 value2 … valuen)                  - Assignment
# ${array[@]}  or ${array[*]}                     - Expand all values
# ${!array[@]}  or ${!array[*]}                   - Expand to all keys
# ${#array[@]} or ${#array[*]}                    - Length
# unset array or unset array[@] or unset array[*] - Remove array
# unset array[index]                              - Remove element
# joinarray1+=(${array2[@]})                      - Join arrays
# Length of an element:${#array[index]}           - Length of element
```

## Environmental Variables

```bash
#  PS1, PS2, HOME, COLORS, BASH, USER, TERM, MAIL, BASH_VERSION, UID,
#  USERNAME, WINDOWID, PATH, IFS, REPLY, LINENO
```

## Linux Signals

```bash
# SIGHUP, SIGINT, SIGQUIT, SIGKILL, SIGTERM, SIGSTOP, SIGSTP, SIGCONT
```

## Terminal Access

```bash
# CTRL - ALT - T       - Access regular terminal on Mint
# CTRL - ALT - F[1-6]  - Access fullscreen tty on Mint
# CTRL - ALT - F8      - Access desktop mode on Mint
```

## Command Line Editing

```bash
#  CTRL-B              - Move the cursor left
#  CTRL-F              - Move the cursor right
#  CTRL-P              - View previous command
#  CTRL-N              - View the next command
#  CTRL-A              - Move the cursor to the beginning of the line
#  CTRL-E              - Move the cursor to the end of the line
#  CTRL-W              - Erase the preceeding word
#  CTRL-U              - Erase from cursor to beginning of line
#  CTRL-K              - Erase from cursor to end of line
#  CTRL-Y              - Paste erased text
```

## Special Characters

```bash
#  &                   - Run job in background
#  |                   - Pipe, send output of one process to input of another process
#  >                   - Overwrite file
#  >>                  - Append to file
#  <                   - Redirect file
#  <<                  - Redirect cmd
```

## Kernel Booting

```bash
#  cat /proc/cmdline   - View system parameters
#  Bootloader langs
#   BIOS               - Basic Input/Output System
#   MBR                - Master Boot record
#   UEFI               - Unified Extensible Firmware Interface
#  Bootloaders
#   GRUB               - Grand Unified Boot Loader
#   LILO
#   SYSLINUX
#   LOADLIN
#  To access GRUB      - Reboot
#                        Hold SHIFT
#                        Press 'e' to view bootloader configuration
#  GRUB CLI commands
#   ls                 - Show partitions
#   set                - Set, show variables
#   echo               - Print information
#   grub2-mkconfig     - Create GRUB configuration file.
#                        sudo grub2-mkconfig -o /boot/grub2/grub2.cfg
#   grub2-set-default  - Updates /boot/grub2/grubenv with default kernel to boot
#                        grep menuentry /boot/grub2/grub2.cfg #Enumerates all kernels, indexed at 0
#                        sudo grub2-set-default 0
#   grub2-editenv list - List saved entry in grubenv file
#   grub2-install      - Install GRUB on device
#  Boot load process with GRUB:
#                      1. PC BIOS initializes hardware, searches for boot-order storage devices for boot code.
#                         Either finds bootloader via MBR or UEFI scheme.
#                      2. Loads and executes book code. GRUB begins.
#                      3. GRUB core loads
#                      4. With core loaded, GRUB can access disks, filesystems. Might have to install RAM FS.
#                      5. GRUB identifies boot partition and loads a configuration.
#                      6. GRUB gives user a change to change configuration.
#                      7. GRUB executes sequence of commands in the grub.cfg file.
#                      8. GRUB optionally load additional code (modules) in the boot parition
#                      9. GRUB executes the boot command to load and execute the kernel specified in config file
#  Important directories:
#    /boot             - Filesystem that grub loaded from
#    /boot/grub2       - Grub configuration
#    /boot/vmlinuz-*   - Linux executable kernel
```

## Userspace Start, Stop

```bash
#  init implementations
#   System V init      - "System 5", Traditional, contains Runlevels
#                        /etc/inittab
#   systemd            - Standard init, Goal Oriented
#                        /usr/lib/systemd, /etc/systemd
#   Upstart            - On Ubuntu prior to version 15.04.
#                        Reactionary, Actions based on events which spawn more events
#                        /etc/init/*.conf contain various upstart stanzas for process initialization, termination
#                        Stanzas:description, start, stop, expect, task, service, console, script, respawn, exec
#                        Logs: /var/log/upstart, /var/log/kern.log, /var/log/syslog
#                        Service Jobs, Task Jobs
#  who -r              - Show runlevel
#  systemctl           - Systemd control utility
#  initctl             - Upstart control utility
#                        Subcmds:list, status <job>, start <job>, stop <job>,
#                        stop <job>, restart <job>, emit <event>, log-priority <priority>
#  telinit             - Switch runlevel
#  shutdown            - Shutdown system
#                        -h=halt, -r=reboot, -f=force, +<time>=in time minutes
#  reboot              - reboot
#  halt                - stop system
#  poweroff            - poweroff system
#  mkinitramfs         - low-level tool for generating initramfs image
```

## Command Line Basics

```bash
#  info                - Information
#  man [-k word]       - Manual
#  uname               - OS Information
#  lsb_release         - Version information
#  infocmp             - Terminal info
#  help                - Help
#  clear               - Clear console
#  reset               - Reset console
#  echo, printf        - Print text
#  printenv            - Print environment, modify environmental vars
#  tee                 - Split output
#  yes                 - Repeat string continuously
#                        Write 10,000,000 lines of 1111 to /tmp/foo.txt
#                        yes "1111" |  head -10000000 | dd of=/tmp/foo.txt
```

## User Control

```bash
#  sudo                - Run as root
#  su                  - Change user
#  who, whoami         - Show user
#  useradd             - Add user
#  userdel             - Delete user
#  usermod             - Modify user
#  passwd              - Modify password
#  pwconv, pwunconv,   - Convert to and from shadow passwords
#  grpconv, grpunconv  - Convert to and from shadow groups
#  chpasswd            - Modify password
#  chsh, chfn, chage   - Modify account
#  groupadd            - Add group
#  groupmod            - Modify group
#  wall                - Write a message to all useres
#  nsswitch.conf       - Tells OS where to go to resolve names/passwords
#  /etc/passwd         - Stores user information
#                        jbensson:x:709974:8500:James Bensson,,,:/home/jbensson:/bin/bash
#                        Entries: 1.Username 2.Encrypted Password (x=in /etc/shadow)
#                        3.UserID(UID) 4.GroupID(GID) 5.Real Name 6.Home Directory 7.User Shell
#  vipw                - Edit /etc/passwd safely
#  /etc/group          - Stores group information
#                        cdrom:x:24:jbensson
#                        Entries: 1.Group Name 2.Group Password 3.GroupID(GID) 4.Optional List of Users
#  groups              - Display groups to which user belongs
#  /etc/shadow         - Contains user authentication information
#  getty               - Prompts username
#  login               - Checks password
#  UserID Types        - effective(actor), real(owner), saved, filesystem user
#  PAM                 - Pluggable Authentication Modules
#                        Config files: /etc/pam.conf, /etc/pam.d/*, ex. /etc/pam.d/mdm
#                        User programs ask PAM to: auth, account, session, password
#                        PAM authentication stack control args: sufficient, requisite, required
#                        Modules: /lib/x86_64-linux-gnu/security/*. Ex. pam_unix.so
```

## Filesystem Control

### Creation

```bash
#  mkefs, mkfs.ext3,.. - Make filesystem
#                        -t=Type
#  mount               - Mount filesystem
#                        -r=read-only, -n=don't update /etc/mtab, -t=type, -o=longopts
#                        -a=mount all of /etc/fstab
#  unmount             - Unmount filesystem
#  fsck                - Filesystem checker
#  tune2fs             - Change filesystem device UUID
#                        -p=fix normal errors with no prompt, -n=check without modifying
#                        -b=replace corrupted superblock with block at number
#  debugfs             - Look at filesystem files while filesystem is offline, copy to alternate location
#  mkswap              - Make swap partition
```

### Info/Stats

```bash
#  ls                  - List directory contents
#                        -i=show inode, -a=all, -l=long format, -t=sort by mod time
#  tree                - Display directory tree
#                        -P 'README.*|CLIENT_*.txt'     # Match regular expression pattern
#                        -I '*.component.*|*.gif'       # Ignore .component and .gif files
#                        -d                             # Directories only
#                        -a                             # Show hidden files
#                        -L 2                           # Only show 2 levels of directory tree
#  pwd                 - Display current directory
#  find                - Find file in hierarchy, optionally run program
#                        find . -name *alert*.*         # file name
#                        find . -type d -name foo       # directory
#                        find . -type f -perm 0777      # have 777 permissions
#                        find . -type f ! -perm 0777    # don't have 777 permissions
#                        find . -type f -perm 0777 -print -exec chmod 644 {} \;
#                                                       # change 777 perms to 644
#                        find . -perm /u=r              # read only
#                        find . -perm /a=x              # executable
#                        find . -type f -name "foo.txt" -exec rm -f {} \;
#                                                       # find and remove a specific file
#                        find . -type f -empty          # find empty files
#                        find . -type d -empty          # find empty directories
#                        find . -type f -name ".*"      # find hidden files
#                        find . -user jbensson          # owned by jbensson
#                        find . -group dev              # owned by dev group
#                        find . -cmin -60               # change time
#                        find . -mmin -60 -mmin +30     # modification time between 30 and 60 min ago
#                        find . -amin -60               # access time
#                        find . -size 50M               # 50M files
#                        find . -size +50M -size -100M  # Files between 50M and 100M
#  file                - Display file information
#  stat                - File status
#  wc                  - Lines in file
#  locate              - Use OS index to look for file
#  whereis             - Search for file
#  which               - Location of executable
#  sum                 - Checksum, blocks
#  free                - Display swap space usage
#  df                  - Show size, utilization of currently mounted filesystems
#  du                  - Report file system disk space usage
#  quota -vs           - Shows allocated filesystem quota
#  lsof                - Display which processes have file open
#  diff                - Show differences between text files
#  showmount           - Show mount information for an NFS server
#  ranger              - Traverse file tree interminal
```

### Manipulation

```bash
#  cd                  - Change working directory
#  chmod               - Change file permissions
#  chown               - Change file owner
#  chgrp               - Change file group
#  umask               - Default permissions given to a user created file
#  touch               - Create file
#  mv                  - Rename file
#  rm                  - Remove file
#  ln                  - Create link
#  mkdir               - Create directory
#  rmdir               - Remove directory
#  mknod               - Create special device
#  mkfifo              - Create named pipe
#  mktemp              - Temporary files
#  sync                - Flush dirty cache blocks
#  swapon              - enable device for paging/swapping
#  swapoff             - disable device for paging/swapping
#  sort                - Sort input alphanumerically
#  od                  - octal dump
#  hd                  - hex dump
#  grep                - Search file, (-e "pattern1" -e "pattern2")
#                        -v "ExcludedWord", -m <num>=stop after num matches
#  cat                 - Concatenate inputs, dump to STDOUT
#  more, less          - Paginate input
#  tail, head          - Display end, beginning of file
#  uniq                - Remove duplicate lines in file
```

### Copying

```bash
#  cp                  - Copy file
#  cpio                - Copy files to and from archives
#  scp                 - secure copy
#  sftp                - secure file transport protocol
#  rsync               - fast, versatile, remote (and local) file-copying tool
#                        Basic: rsync file1 file2 ... user@host:path
#                        -a=directory recursively, -n=dry-run, -v=verbose, -z=compress
#                        --exclude=files to exculde, --include=files to include
#                        --checksum=checksum files, --ignore-existing=don't clobber on dest side
#                        --backup=rename clobbered files, --suffix=backup file suffix
#                        --update=doesn't clobber any file with later modification date
#                        --bwlimit=limit bandwidth of transfer
#                        --stats=report stats, --delete=fully synchronize
#                        rsync -a dir  host:dest_dir: move dir into dest_dir
#                        rsync -a dir/ host_dest_dir: move dir/* into dest_dir
#  Samba               - Server to provide AD and ServerMessageBlock(SMB)/CommonInternetFilesystem(CIFS) services to clients
#                        Can share files between Windows, Linux
#                        /etc/samba=configuration directory
#                        /var/log/samba=log file
#                        smbpasswd=create/modify user/password
#                        nmbd -D -s smb_config_file=start NetBIOS name server
#                        smbd -D -s smb_config_file=Share requests
#                        smbclinet -L -U username SERVER=client program to connect to server
```

### Compression

```bash
#  zip, unzip          - Windows compatibple compression
#  tar                 - Create, Unpack compress archives (multiple files, directories)
#  gzip, gunzip        - Standard unix compression
#  bzip2, bunzip2      - Tightest compression
```

### Misc

```bash
#  aspell              - Interactive spell checker
#  mc                  - midnight commander
#  sed                 - Stream editor
#  gawk                - Stream editor with programmable features
```

## Program Control

### Building

```bash
#  rpm                 - Package manager
#  yum                 - Yellowdog Updater Modifier. Interactive, RPM based, package manager.
#  apt-get             - APT package handling utility
#  apt-cache           - Query APT cache
#                        apt-cache policy libmysqlclient+ :Show package version details
#  aptitude            - Interface to package manager
#  dpkg                - Package manager for Debian
#  diff                - Display differences between source files
#  patch               - Apply a diff file to the original
#  GNU Autoconf        - popular system for makefile generation
#                        configure; Makefile.in, config.h.in (*.in = template) (used to generate makefile for particular OS)
#                        Steps to install: ./configure; make; make check; make -n install; make install
#                        Autoconf make targets: clean, distclean, check, install-strip
#  pkg-config          - Program used to advertise binaries include files, libraries and specify flags needed to compile
#                        pkg-config --libs openssl: show libraries used by openssl
#  /usr/local          - Default place to install programs
#  /ect                - Configuration directory
#  cmake               - Cross-platform makefile generator
#  scons               - Software construction tool
#  ar                  - Create, modify, and extract from archives
```

### Binary Analysis

```bash
#  ldd                 - Print shared library dependencies
#  strings             - Sifts through object file printing out ascii strings (BUILD, vermagic)
#  nm                  - Prints out binary symbols
#  objdump             - Prints out binary information
#  readelf             - Examine Executable and Linkable Format (ELF) file
#  addr2line           - Convert address to source line number
```

### Debugging

```bash
#  truss               - Examine program I/O
#  gdb                 - Debugger
#  pstack              - Examine stack
#  ltrace              - Library call trace
#  strace              - System call trace
#  bash                - Run program in bash shell. bash -x <shell prog> = debug a shell program
```

### Logging

```bash
#  rsyslogd            - Daemon logs to /var/log/*
#                        Configuration files /etc/rsyslog.conf, /etc/rsyslog.d/*.cong
#  logger              - Send rsyslogd a message, logged to /var/log/syslog
#                        logger -p daemon.info "something bad just happened"
#  logrotate           - Rotates, compresses, and mails system log files
#                        logger -p dae
#  dmesg               - Print or control kernnel ring buffer
#                        See /var/log/messages
```

### Control

```bash
#  &, bg, fg           - Background, foreground
#  nice                - Run program with modified scheduled priority
#  renice              - Alter priority of running process
#  at                  - Execute command at specified time
#  atq                 - List the user's pending jobs
#  atrm                - Deletes an at job
#  cron                - Daemon to execute schedules commands
#  crontab             - Maintain crontab files for individual users
#                        -l=list, -r=remove, -e=edit
#                        Cron file syntax: 15 09 * * * /home/jbensson/bin/demoProg
#                        1.Minute(0-59) 2.Hour(0-23) 3.MonthDay(1-31) 4.Month(1-12) 5.WeekDay(0-7(both Sunday))
#  watch               - Execute program periodically
#  time                - Time a program
#  kill, killall       - Send signal
#  exit                - Exit terminal
#  sleep               - Sleep
#  trap                - Assign Signal Handlers
#  nohup               - Ignore signals
#  exec                - Execute command
```

### Statistics

```bash
#  top                 - Display linux processes
#                        Sorts: M=mem usage, T=total CPU, P=current CPU
#                        u=users processes, f=different stats, ?=usage, -H=threads, -p=specify pid
#  atop, htop          - Other versions of top
#  ps                  - Report a snapshot of processes
#                        m=show treads, ax=all processes
#                        o=define format, ex o pid,tid,class,ni,pri
#                        o min_flt,maj_flt=view page faults
#  lsof                - List open files, network connections, ...
#                        Fields: Command  PID   TID   USER      FD   TYPE  DEVICE  SIZE    INODE   NAME
#                        Ex row: dconf    3009  3025  jbensson  mem  REG   8,5     141574  683706  /lib/x86_64-linux-gnu/libpthread-2.19.so
#                        -p=pid, <file path start>
#  time                - Run program and summarize system resource usage
#                        user time   = # seconds CPU spent running user code
#                        system time = # seconds CPU spent running kernel code
#                        elapsed     = # seconds duration
#  uptime              - Time computer has been running
#                        Displays load average = average # of processes currently ready to run
#  free                - Displays amount of free and used memory in the system
#                        See /proc/meminfo
#  vmstat              - Report virtual memory statistics, specify number of seconds to update
#                        -d=per disk partition
#  iostat              - Report IO statistics, specify number of seconds to update
#                        -p ALL= show partition information
#  iotop               - I/O utilization tool
#  ionice              - Set or get process I/O scheduling class and priority
#                        I/O scheduling priorities: be=besteffort, rt=realtime, idle
#  pidstat             - Report statistics for Linux tasks, specify number of seconds to update
#  sar                 - System Activity Reporter
#  acct                - Account Processing
#  Quotas              - Limit system resources on per-process basis
#                        See: /etc/security/limits.conf
```

## Network Control

### Application Layer

```bash
# Example programs     -  ssh, apache, cups, nfs)
#  curl                - Network program that can read,write HTTP
#  ngrok               - Set up remote secure tunnel to localhost.
#                        Opens local app from the internet
#  httpd               - Webserver
#  apache, apache2     - Webserver
#  tomcat              - Apache webserver for Java programs
#  ssh,sshd            - Secure shell, Secure shell daemon
#                        ssh remote_username@host
#                        tar zcvf - dir | ssh remote_host tar zxvf
#                        -Y=enable X11 forwarding
#  scp                 - Secure copy
#  sftp                - Secure File transfer protocol
#  postfix             - Mailserver
#  qmail               - Mailserver
#  sendmail            - Mailserver
#  cupsd               - Print server
#  nfsd                - Network filesystem daemon
#  mountd              - Network filesystem daemon
#  rpcbind             - Remote procedure call portmap service daemon
```

### Transport Layer

```bash
# Example protocols    - TCP, UDP
#  nm-tool             - Utility to report NetworkManager state
#  nfslookup           - Look up a particular hostname
#  nmcli               - Command-line tool for controlling Network Manager
#  netstat             - Print network connections, routing tables, interface stats,
#                        masquerade connections, and multicast memberships
#                        -n=no hostnames, -t=tcp connections, -u=udp ports
#                        -l=listening ports, -a=active ports
#  netcat              - arbitrate TCP and UDP connections
#                        -l port_number=listen on port number
#  lsof                - List open files
#                        -i=ports, -i:port#, -iprotocol@host:port
#  tcpdump             - Dump traffic on a network
#  nmap                - Network exploration tool and security scanner
#  telnet              - interactive communication with host using telnet protocol
#  rpcinfo             - Display remote procedure call info
```

### Iternet Layer

```bash
# Example protocols    - IP, IPv6
#  host                - Translate IP address and name
#  ifconfig            - configure network interfaces
#                        -a=all, ifconfig interface address netmask mask
#  ping                - use Internet Control MEssage Protocol (ICMP) echo request to host
#  route               - Show, manipulate the IP routing table
#                        -n=use IP addresses instead of names
#                        add,del
#  iptables            - Administration tool for IPv4/IPv6 packet filtering and NAT
#                        -L=list, -D=delete, -I=insert, -P=policy
#  hostname            - hostname of computer logged into
#  dig                 - DNS Lookup utility
#  traceroute          - Print the route packets trace to network host
#  tracepath           - Trace path to network host
#  ifup, ifdown, ifcfg - Configure interfaces
#  findsmb             - List info about machines that respond to SMB queries
```

### Physical Layer

```bash
# Example protocols    - Ethernet, Wifi
#  arp                 - Manipulate the system ARP cache
#                        -d=delete, i=interface
#  iw                  - Show, manipulate wireless devices and configuration
#  ethtool             - Display, change ethernet interface
```

## Device Control

```bash
#  fdisk               - Disk partitioning facility
#                        (no Globally Unique Identifier Partition Table (GPT) format)
#  gdisk               - Disk partitioning facility (no Master Boot Record (MBR) format)
#  parted              - Better disk partioning facility
#                        -l: view partition table
#  partprobe           - Reread disk partitions
#  blockdev            - Force reread
#  blkid               - Display UUID's of devices
#  pvcreate            - Create physical volume
#  pvdisplay           - Display physical volume
#  lvcreate            - Create logical volume
#  lvdisplay           - Display logical volume
#  dd                  - Read blocks from one entity onto another
#  udevadm             - Administration tool for udev
#  udevadm monitor     - Monitor device changes
#  lsscsi              - List scsi devices
#  lsusb               - List usb devices
#  mknod               - Create device
```

## Module Control

```bash
#  sysctl              - Configure kernel parameters at runtime
#  lsmod               - View loaded modules
#  insmod              - Install module
#  rmmod               - Remove module
#  depmod              - Inspect module dependencies
#                        sudo depmod --all, outputs to modules.dep
#  modprobe            - Add module
#  modinfo             - Information on module
#  /sbin/weak-modules  - Load/Unload series of drivers
#  chkconfig           - check configuration settings
```

## Programming languages, tools

```bash
#  C                   - Enhancement of Ken Thompson's B Language
#                        cc          - C, C++ Compiler
#                                      -o=specify output name, -c=create object (not executable)
#                                      -I=include path
#                                      -L=load static library path -l=link with specified object
#                                      -Wl,rpath=specify shared load library path
#                        gcc         - GNU C, C++ Compiler
#                        cpp         - C preprocessor
#                                      -n=Print steps without executing, -f=run code from specified makefile
#                        ldd         - Print shared library dependencies
#                        ldconfig    - Configure dynamic linker run-time config
#                        patchelf    - Modify preexisting binary
#                        gdb         - GNU debugger
#                        valgrind    - Code profiler, memory debugger, other debugging tools
#  C++                 - "C with Classes"
#  C#                  - Microsoft .NET C
#  Java                - "Write once, run anywhere"
#                        javac       - Java compiler
#  Smalltalk           - Created as language to underpin "New World" of computer exemplified by "human-conputer symbiosis"
#  Python              - Interpreted, interactive, object-oriented programming language
#                        Useful libraries: Django, Flask
#  JavaScript          - Client-side web scripting language
#  Python              - Designed for code readability, simplicity
#  Perl                - "Swiss army chainsaw" of programming tools
#  PHP                 - Server side scripting language for web-development
#  Ruby                - OOP Scripting Language
#  Ruby on Rails       - Web application framework
#  Rails               - Web development framework
#  Erlang              - "Erricsson Language" designed for telephony switches
#  Emacs Lisp          - Dialect of list for GNU Emacs
#  Powershell          - Windows shell language
#  Bash                - Unix shell language
#  Matlab, Octave      - Numerical computing
#  R                   - Statistical analysis language
#  Mathematica         - Symbolic mathematical computation
#  SQL                 - Structured Query Language for Relational Database Management Systems (RDBMS)
#                        Common SQL DBs: MySQL, Oracle, MariaDB
#  NoSQL               - Non Relational Database
#                        Common NoSQL DBs: MongoDB, redis
#  make                - Program to maintain groups of programs
#  Tcl                 - Tool Command Language
#  Lex                 - Tokenizer
#  Yacc                - Parser
#  Latex               - Document preparation system
#  m4                  - Macro processor
```

## Desktop Control

### Desktop Environments

```bash
#  Tasks               - Bundle toolkits and other libraries together to
#                        provide uniform outward appearance and force different
#                        applications to cooperate
#                      - Includes a window manager and builds on top of it.
#  Examples            - GNOME
#                        KDE
#                        Unity
#                        Xfce
#                        Cinnamon
```

### Toolkits

```bash
#  Tasks               - Provide libraries of graphical widgets
#  Examples            - GTK+
#                        Qt Framework
```

### Window Managers

```bash
#  Tasks               - Controls placement and appearance of windows on screen
#  Examples            - Mutter
#                        Compiz
#                        Xfce
#                        Metacity
#                        Muffin
#                        Enlightenment
#                        Afterstep
#                        X-Window-Manager
```

### Display Manager

```bash
#  Tasks               - Starts X server, displays login box. After login,
#                        starts set of clients, including window manager
#  Examples            - gdm
#                        kdm
#                        mdm
```

### X Window Layer

```bash
#  randr               - Divide displays
#  x                   - Portable, network-transparent window system
#  xdotool             - Command-line X11 automation tool
#  xwininfo            - Window information utility for X
#  startx              - Start X session
#  initx               - Start X session
#  xev                 - Print contents of xevents
#  xinput              - Utility to configure and test X input devices
#                        By itself, shows mouse, keyboard, and other devices
#  xkbcomp             - Compile keyboard map
#  setxkbmap           - Activate keyboard map
#  dbus-monitor        - Debug probe to print bus messages
```

## Misc Commands

```bash
#  date                - Display the date
#  tzselect            - View timezones
#                        /ect/localtime controls time zone
#                        /usr/share/zoneinfo has timezone information
#  hwclock             - Query or set the harware clock
#  adjtimex            - Smoothly update clock
#  ntpdate             - Set date and time via network
#  compgen -v          - List all global varibales
#  bc                  - Bash calculator
#  scale, let          - Computations
#  seq                 - Sequence of numbers
#  mail                - mailutil GNU program
#  screen              - Program to multiplex terminals
#  tmux                - Display multiple terminals on the same screen
```

## Solaris Specific Commands

## Filesystem/Storage

```bash
# prtconf,sysdef - system configuration information
# format         - format devices
# zpool          - managage storage pools
# zfs            - manage zfs filesystems
# greadefl       - read ELF file
```

## ASCII Chart

```bash
# Emacs command: M-x man RET ascii RET
#
#    2 3 4 5 6 7       30 40 50 60 70 80 90 100 110 120
#  -------------      ---------------------------------
# 0:   0 @ P ` p     0:    (  2  <  F  P  Z  d   n   x
# 1: ! 1 A Q a q     1:    )  3  =  G  Q  [  e   o   y
# 2: " 2 B R b r     2:    *  4  >  H  R  \  f   p   z
# 3: # 3 C S c s     3: !  +  5  ?  I  S  ]  g   q   {
# 4: $ 4 D T d t     4: "  ,  6  @  J  T  ^  h   r   |
# 5: % 5 E U e u     5: #  -  7  A  K  U  _  i   s   }
# 6: & 6 F V f v     6: $  .  8  B  L  V  `  j   t   ~
# 7: ´ 7 G W g w     7: %  /  9  C  M  W  a  k   u  DEL
# 8: ( 8 H X h x     8: &  0  :  D  N  X  b  l   v
# 9: ) 9 I Y i y     9: ´  1  ;  E  O  Y  c  m   w
# A: * : J Z j z
# B: + ; K [ k {
# C: , < L \ l |
# D: - = M ] m }
# E: . > N ^ n ~
# F: / ? O _ o DEL
```