# Bash Projects Documentation

All projects are found in the `./projects` directory.

## helloWorld.sh

Bash Hello World Program

To Run: `./helloWorld.sh`

## variableExpressions.sh

Demonstrate different variable expressions.

To Run: `./variableExpressions.sh`

## conditionals.sh

Demonstrate conditional usage: `if`, `else`, `case`

To Run: `./conditionals.sh`

## loops.sh

Demonstrate various looping constructs: `for` and `while`

To Run: `./loops.sh`

## functions.sh

Demonstrate basic function usage.

To Run: `./functions.sh`

## fileRead.sh

Demonstrate reading from a file

To Run: `./fileRead.sh`

## fileIO.sh

Demonstrate reading and writing to a file

To Run: `./fileIO.sh`

## multiply.sh

Demonstrate multiplying two numbers

To Run: `./multiply.sh 10 20`

## options.sh

Demonstrate basic option parsing.

To Run: `./options.sh -a`

## gawk.sh

Demonstrate basic gawk capabilities

To Run: `./gawk.sh`

## sed.sh

Demonstrate basic sed capabilities

To Run: `./sed.sh`

## trap.sh

Demonstrate basic signal handling

To Run: `./trap.sh`. After 1 second `Ctrl+C`

## userIO.sh

Demonstrate basic user io with `read` command

To Run: `./userIO.sh`

