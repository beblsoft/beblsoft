#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Docker Compose Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import time
import redis
from flask import Flask


# ------------------------ GLOBALS ------------------------------------------ #
app   = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


# ------------------------ HELPERS ------------------------------------------ #
def get_hit_count(retries=5): #pylint: disable=C0111
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


# ------------------------ ROUTES ------------------------------------------- #
@app.route('/')
def hello(): #pylint: disable=C0111
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
