OVERVIEW
===============================================================================
Docker Compose Technology Documentation
- Description                       : Compose is a tool for defining and running multi-container
                                      Docker applications. With Compose, you use a YAML file to
                                      configure application's services.
                                      With a single command, you create and start all the services
                                      from your configuration
- Features --------------------------
  * Multiple isolated envs on 1 host: Composes uses a project name to isolate environments from each other
                                      Good for multiple environments on same machine (dev, ci, ...)
  * Preserve volume data            : Preserves all volumes used by services
                                      When docker compose runs, it copies volumes from old runs to new runs
  * Only recreate changed containers: Compose caches the configuration used to create a container
                                      When you restart a service that has not changed, compose re-uses the
                                      existing containers
                                      Allows for quick and iterative changes
  * Variables between envs          : Variables supported in compose file
- Using docker compose with 3 steps--
                                    1 Define app's environment with a Dockerfile
                                    2 Define services that make up app in docker-compose.yml
                                      So they can be run together in an isolated environment
                                    3 Run docker-compose up and compose starts and runs entire app

- Common Use Cases ------------------
  * Development environments        : Run application in isolated environment during development
                                      Compose file provides way to document and configure all application services
                                      Transform multi-page getting started guid to single machine readable
                                      compose file
  * Automated testing environments  : Compose provides a convenient way to create and destroy isolated
                                      testing environments
  * Single host deployments         : Use compose to deploy to a remote Docker engine with Docker Machine
                                      or entire Docker Swarm cluster
- Relevant URLs ---------------------
  * Documentation                   : https://docs.docker.com/compose/overview/
- Installation ----------------------
  * Install from dockerCompose.sh   : dockerCompose_install
  * Uninstall from dockerCompose.sh : dockerCompose_uninstall



COMPOSE FILE REFERENCE
===============================================================================
- Description                       : Compose file is a YAML file defining services, networks, and volumes
                                    : Default path is ./docker-compose.yml
- Service Definition                : Configuration that is applied to each container started for that service
- Environment variables             : Can be used in configuration files. ${VARIABLE}
- Specifying durations -------------: Some configurations accept a duration as a string
                                      Supported units are us, ms, s, m and h
  * Examples                        : 2.5s
                                      10s
                                      1m30s
                                      2h32m
                                      5h34m56s
- Specifying byte values -----------: Some configurations accept byte value
                                      Supported units are b, k, kb, m, mb, g, and gb
  * Examples                        : 2b
                                      1024kb
                                      2048k
                                      300m
                                      1gb
- Variable substitution ------------: Configuration options can contain environment variables
                                      Compose uses the variable values from the shell environment in which
                                      docker-compose is run
  * Unset values                    : If environemt variable is not set, compose substitutes with an empty string
  * Env default values              : Set default values using .env file
  * Inline default value            : Can provide default inline
                                      ${VARIABLE:-default} evaluates to default if VARIABLE is unset or empty
                                      ${VARIABLE-default} evaluates to default only if VARIABLE is unset
  * Error value                     : Can throw error if variable not present
                                      ${VARIABLE:?err} throw error if variable unset or empty
                                      ${VARIABLE?err} throw error if variable unset
  * Double dollar sign              : Use $$ (double-dollar sign) when configuration needs literal dollar sign
  * Example                         : db:
                                        image: "postgres:${POSTGRES_VERSION}"
- Extension fields -----------------: Possible to re-use configuration fragments using extension fields
                                      Can be any format as long as they are located at the root of compose file
                                      and name starts with x-
                                      Compose leverages YAML anchors
  * Example common                  : version: '3.4'
                                      x-logging:
                                        &default-logging
                                        options:
                                          max-size: '12m'
                                          max-file: '5'
                                        driver: json-file
                                      services:
                                        web:
                                          image: myapp/web:latest
                                          logging: *default-logging
                                        db:
                                          image: mysql:latest
                                          logging: *default-logging
  * Example merge override          : version: '3.4'
                                      x-volumes:
                                        &default-volume
                                        driver: foobar-storage
                                      services:
                                        web:
                                          image: myapp/web:latest
                                          volumes: ["vol1", "vol2", "vol3"]
                                      volumes:
                                        vol1: *default-volume
                                        vol2:
                                          << : *default-volume
                                          name: volume02
                                        vol3:
                                          << : *default-volume
                                          driver: default
                                          name: volume-local
- Multiple copies, same host -------: Compose uses the project name to create unique identifiers for
                                      all of a project's containers and other resources
                                    : To run multiple copies of a project, set a custom project
                                      name using the -p CLI option or COMPOSE_PROJECT_NAME env variable
- docker-compose up, run, start ----:
  * up                              : Typical usage
                                    : Starts all services defined in docker-compose.yml in attached mode
  * run                             : Used for running one-off or adhoc tasks
                                      Requires the service name you want to run and only starts containers
                                      or services that the running service depends on.
                                    : Used to run tests or perform administrative tasks
  * start                           : Command is useful only to restart containers that were previously
                                      created, but were stopped



COMPOSE FILE REFERENCE: FIELDS
===============================================================================
- build ----------------------------: Config options that are applied at build time
  Representation                    : Path to build context, or object with patch specified under context
  Build with context                : version: '3'
                                      services:
                                        webapp:
                                          build:
                                            context: ./dir
                                            dockerfile: Dockerfile-alternate
                                            args:
                                              buildno: 1
                                            image: webapp:tag
  args                              : Environment variables accessible during build process
- deploy ---------------------------: Specify configuration related to the deployment and running of services
- dns ------------------------------: Custom DNS servers
- dns_search -----------------------: Custom DNS search domains
- entrypoint -----------------------: Override the default entrypoint, can also be a list
  Example                           : entrypoint:
                                        - php
                                        - -d
                                        - zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20100525/xdebug.so
                                        - -d
                                        - memory_limit=-1
                                        - vendor/bin/phpunit
- env_file -------------------------: Add environment variables from a file. Paths relative to compose file
  Example                           : env_file: .env
                                      env_file:
                                        - ./common.env
                                        - ./apps/web.env
                                        - /opt/secrets.env
- environment ----------------------: Add environment variables
                                      Environment variables with only a key are resolved to their values
                                      on the machine Compose is running on
  Example                           : environment:
                                        - RACK_ENV=development
                                        - SHOW=true
                                        - SESSION_SECRET
- expose ---------------------------: Expose ports without publishing them to the host machine (only for links)
  Example                           : expose:
                                        - "3000"
                                        - "8000"
- external_links -------------------: Links to containers started outside this docker-compose.yml
- extra_hosts ----------------------: Add hostname mappings
- healthcheck ----------------------: Configure a check that's run to determine whether or not containers
                                      for this service are healthy
  Example                           : healthcheck:
                                        test: ["CMD", "curl", "-f", "http://localhost"]
                                        interval: 1m30s
                                        timeout: 10s
                                        retries: 3
                                        start_period: 40s
- image ----------------------------: Specify the image to start the container from
                                      Can be a repository/tag or a partial image ID
  Examples                          : image: redis
                                      image: ubuntu:14.04
                                      image: tutum/influxdb
                                      image: example-registry.com:4000/postgresql
                                      image: a4bc65fd
- init -----------------------------: Run init inside the container that forwards signals and reaps processes
- isolation ------------------------: Specify the container's isolation technology
- labels ---------------------------: Add metadata to containers using Docker labels.
                                      Recommened to use reverse-DNS notation
  Example                           : labels:
                                        - "com.example.description=Accounting webapp"
                                        - "com.example.department=Finance"
                                        - "com.example.label-with-empty-value"
- links ----------------------------: Link to containers in another service
                                      Either specify both the service name and a link alias or just the service
                                      By default, any service can reach any other service at that service's name
                                      Links also express dependency between services in the same way as depends_on
  Example                           : web:
                                        links:
                                         - db
                                         - db:database
                                         - redis
- logging --------------------------: Logging configuration for the service
- network_mode ---------------------: Network mode
- networks -------------------------: Networks to join, referencing entries under the top-level networks key
  Example                           : services:
                                        some-service:
                                          networks:
                                           - some-network
                                           - other-network
- pid ------------------------------: Sets the PID mode to the host PID mode
- ports ----------------------------: Expose ports
  Examples                          : ports:
                                        - "3000"
                                        - "3000-3005"
                                        - "8000:8000"
                                        - "9090-9091:8080-8081"
                                        - "49100:22"
                                        - "127.0.0.1:8001:8001"
                                        - "127.0.0.1:5000-5010:5000-5010"
                                        - "6060:6060/udp"
- restart --------------------------: no is the default restart policy, and doesn't restart container under
                                      any circumstance
  Examples                          : restart: "no"
                                      restart: always
                                      restart: on-failure
                                      restart: unless-stopped
- secrets --------------------------: Grant access to secrets on a per-service using the pre-service
                                      secrets configuration
                                      Grants container access to secret and mounts it at
                                      /run/secrets/<secret_name>
  external                          : Secret has already been defined by running docker secret create
  Example                           : version: "3.1"
                                      services:
                                        redis:
                                          image: redis:latest
                                          deploy:
                                            replicas: 1
                                          secrets:
                                            - my_secret
                                            - my_other_secret
                                      secrets:
                                        my_secret:
                                          file: ./my_secret.txt
                                        my_other_secret:
                                          external: true
- security_opt ---------------------: Override the default labeling scheme for each container
- stop_grace_period ----------------: Specify how long to wait when attempting to stop a container if it doesn't
                                      handle stop_signal before sending SIGKILL
  Examples                          : stop_grace_period: 1s
                                      stop_grace_period: 1m30s
- stop_signal ----------------------: Sets an alternative signal to stop the container
                                      Default is SIGTERM
  Example                           : stop_signal: SIGUSR1
- sysctls --------------------------: Kernel parameters to set in the container
- tmpfs ----------------------------: Mount a temporary file system inside the container
- ulimits --------------------------: Override the default ulimits for a container
- userns_mode ----------------------: Disables user namespace for this service
- volumes --------------------------: Mount host paths or named volumes, specifed as a sub-options to a service
  Examples                          : version: "3.2"
                                      services:
                                        web:
                                          image: nginx:alpine
                                          volumes:
                                            - type: volume
                                              source: mydata
                                              target: /data
                                              volume:
                                                nocopy: true
                                            - type: bind
                                              source: ./static
                                              target: /opt/app/static

                                        db:
                                          image: postgres:latest
                                          volumes:
                                            - "/var/run/postgres/postgres.sock:/var/run/postgres/postgres.sock"
                                            - "dbdata:/var/lib/postgresql/data"

                                      volumes:
                                        mydata:
                                        dbdata:



ENVIRONMENT VARIABLES
===============================================================================
- Compose CLI Variables ------------: COMPOSE_API_VERSION
                                      COMPOSE_CONVERT_WINDOWS_PATHS
                                      COMPOSE_FILE
                                      COMPOSE_HTTP_TIMEOUT
                                      COMPOSE_TLS_VERSION
                                      COMPOSE_PROJECT_NAME
                                      DOCKER_CERT_PATH
                                      DOCKER_HOST
                                      DOCKER_TLS_VERIFY
- .env File ------------------------: Compose supports declaring default environment variables in
                                      an environment file named .env placed in the folder where the
                                      docker-compose command is executed (CWD)
  * Rules                           : Compose expects each line in an env file to be in VAR=VAL format.
                                      Lines beginning with # are processed as comments and ignored.
                                      Blank lines are ignored.
                                      There is no special handling of quotation marks.
  * Example                         : TAG=v1.5
                                      AWS_ACCESS_KEY_ID=sldfkjhasdflkjh
                                      AWS_SECRET_ACCESS_KEY=23459238475kjhsadfh
- Use cases -------------------------
  * Substitute env variable in
    compose file                    : web:
                                        image: "webapp:${TAG}"
  * Set emv variable in container   : web:
                                        environment:
                                          - DEBUG=1
  * Pass env variable to container  : web:
                                        environment:
                                          - DEBUG
  * Pass env_file to container      : web:
                                        env_file:
                                          - web-variables.env
  * Pass env variable via CLI       : docker-compose run -e DEBUG web python console.py
  * Set env variable via CLI        : docker-compose run -e DEBUG=1 web python console.py
  * Verify environment variables
    in compose file                 : docker-compose config
- Priority -------------------------: When same environment variable is set in multiple files, priority
                                      shown below is used to choose
                                    1 Compose File
                                    2 Shell environment variables
                                    3 Environment file
                                    4 Dockerfile
                                    5 Variable is not defined



EXTENDING SERVICES
===============================================================================
- Description ----------------------: Compose supports two methods of sharing common configuration
                                      1. Extending an entire Compose file by using multiple Compose files
                                      2. Extending individual services with the extends field
- Multiple Compose Files -----------: Using multiple compose files enables you to customize a Compose
                                      application for different environments or different workflows
  * Default                         : By default, compose reads two files, a docker-compose.yml
                                      and an optional docker-compose.override.yml
                                    : docker-compose.yml contains base configuration
                                    : docker-compose.override.yml contains overrides
  * Sepecify multiple files         : All subsequent files must be relative to base docker-compose.yml
                                    : Compose merges files in the order they’re specified on the command line
                                    : Ex. docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d



NETWORKING
===============================================================================
- Description ----------------------: Compose sets up a single network for app
                                    : Each container for a service joins the default network and is both
                                      reachable by other containers, and discoverable by them at a hostname
                                      identical to the container name
                                    : App's network is given a name based on "project name", which is based
                                      on the name of the directory it lives in
- Example ---------------------------
  * docker-compose.yml              : version: "3"
                                      services:
                                        web:
                                          build: .
                                          ports:
                                            - "8000:8000"
                                        db:
                                          image: postgres
                                          ports:
                                            - "8001:5432"
  * Explanation                     : After docker-compose up, the following happens
                                      1. A network called myapp_default is created.
                                      2. A container is created using web’s configuration.
                                         It joins the network myapp_default under the name web.
                                      3. A container is created using db’s configuration.
                                         It joins the network myapp_default under the name db.
                                    : Each container can now loop up the hostname web
                                      Web app could connect to db at postgress://db:5432
                                    : Postgres HOST_PORT      = 8801
                                      Postgres CONTAINER_PORT = 5432
                                      Containers connect to each other using container ports
- Update containers ----------------: If configuration change made to a service and docker-compose up rerun,
                                      the old container is removed and the new one joins the network under
                                      a different IP address but the same name
                                    : Running containers can look up that name and connect to the new
                                      address, but the old address stops working
- Custom networks ------------------: Instead of just using the default app network, you can specify your
                                      own networks with the top-level networks key
                                    : Each service can specify what networks to connect to with the
                                      service-level networks key, which is a list of names referencing top-level
                                      networks key
- Configure default network --------: Instead of (or as well as) specifying your own networks, you can also
                                      change the settings of the app-wide default network by defining an entry
                                      under networks named default
                                    : networks:
                                        default:
                                          # Use a custom driver
                                          driver: custom-driver-1
- Use a pre-existing network        : Can join a pre-existing network by using external keyword
                                    : networks:
                                        default:
                                          external:
                                            name: my-pre-existing-network



STARTUP, SHUTDOWN ORDER
===============================================================================
- Description ----------------------: Control the order of service startup with the depends_on option
                                    : Dependencies determined by:
                                      depends_on, links, volumes_from, and network_mode
                                    : Design applications to attempt to re-establish a connection
                                      after dependency failures
                                    : Best solution is to perform check in application code, both at startup
                                      and whenever a connection is lost for any reason
                                    : Useful tools: wait-for-it, dockerize, or sh-compatible wait-for
- wait-for-it ----------------------: Useful tool for dependencies to wait on each other
                                    : Ex.
                                      version: "2"
                                      services:
                                        web:
                                          build: .
                                          ports:
                                            - "80:8000"
                                          depends_on:
                                            - "db"
                                          command: ["./wait-for-it.sh", "db:5432", "--", "python", "app.py"]
                                        db:
                                          image: postgres



COMMANDS
===============================================================================
- Generic ---------------------------
  * help                            : docker-compose --help
  * command help                    : Get help on a command
                                      docker-compose help COMMAND
  * Version                         : docker-compose --version
  * Verbose                         : docker-compose --verbose ...
  * Specify alternate compose file  : docker-compose --file foo/bar/docker-compose.yml
                                      Default is docker-compose.yml, overridden by docker-compose.override.yml
  * Specify an alternate project
    name (allows app namespacing)   : docker-compose --project fooBar
  * Specify an alternate project dir: docker-compose --project-directory foo/bar
  * version                         : Show the Docker-Compose version information
                                      docker-compose version [--short]

- CLI Environment Variables ---------
  * COMPOSE_PROJECT_NAME            : Ex. myapp_web_1
  * COMPOSE_FILE                    : Ex. COMPOSE_FILE=docker-compose.yml:docker-compose.prod.yml
  * COMPOSE_API_VERSION             :
  * DOCKER_HOST                     : Ex. DOCKER_HOST=unix:///var/run/docker.sock
  * COMPOSE_HTTP_TIMEOUT            : Time (in seconds) a request to Docker daemon is allowed to hang (Default=60)
                                      Ex. COMPOSE_HTTP_TIMEOUT=10

- Compose File ----------------------
  * bundle                          : Generate a Docker bundle from the Compose file
                                      docker-compose bundle [options]
  * config                          : Validate and view the Compose file
                                      docker-compose config [options]

- Service Images --------------------
  * build                           : Build or rebuild services
                                      docker-compose build [options] [--build-arg key=val...] [SERVICE...]
    parallel                          docker-compose build --parallel fooBarService
  * images                          : List images
                                      docker-compose images [options] [SERVICE...]
  * pull                            : Pull service images
                                      docker-compose pull [options] [SERVICE...]
  * push                            : Push service images
                                      Push images for services to their respective registry/repository
                                      Assumes image built locally
                                      docker-compose push [options] [SERVICE...]

- Services --------------------------
  * down                            : Stop and remove containers, networks, images, and volumes
                                      docker-compose down [options]
    shutdown timeout in sec         : docker-compose down --timeout 3
  * events                          : Receive real time events from containers
                                      docker-compose events [options] [SERVICE...]
  * exec                            : Execute a command in a running container
                                      docker-compose exec [options] [-e KEY=VAL...] SERVICE COMMAND [ARGS...]
  * kill                            : Kill containers
                                      docker-compose kill [options] [SERVICE...]
    with sigint                     : docker-compose kill -s SIGINT
  * logs                            : View output from containers
                                      docker-compose logs [options] [SERVICE...]
  * pause                           : Pause services
                                      docker-compose pause [SERVICE...]
  * port                            : Print the public port for a port binding
                                      docker-compose port [options] SERVICE PRIVATE_PORT
  * ps                              : List containers
                                      docker-compose ps [options] [SERVICE...]
  * restart                         : Restart services
                                      docker-compose restart [options] [SERVICE...]
  * rm                              : Remove stopped containers
                                      docker-compose rm [options] [SERVICE...]
  * run                             : Run a one-off command
                                      docker-compose run [options] [-v VOLUME...] [-p PORT...] \
                                         [-e KEY=VAL...] [-l KEY=VALUE...] SERVICE [COMMAND] [ARGS...]
    Specify multiple config files
    (subsequent override former)    : docker-compose -f docker-compose.yml -f docker-compose.admin.yml run backup_db

  * scale                           : Set number of containers for a service
                                      docker-compose scale [SERVICE=NUM...]
  * start                           : Start services
                                      docker-compose start [SERVICE...]
  * stop                            : Stop services
                                      docker-compose stop [options] [SERVICE...]
  * top                             : Display the running processes
                                      docker-compose top [SERVICE...]
  * unpause                         : Unpause services
                                      docker-compose unpause [SERVICE...]
  * up                              : Create and start containers
                                      Builds, re(creates), starts, and attaches to containers for a service
                                      Aggregates output of each container
                                      docker-compose up [options] [--scale SERVICE=NUM...] [SERVICE...]
    background                      : docker-compose up --detach
