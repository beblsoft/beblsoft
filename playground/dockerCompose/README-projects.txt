OVERVIEW
===============================================================================
Docker Compose Sample Projects Documentation


PROJECT: FLASK REDIS
===============================================================================
- Description                       : Creates a flask container and a redis container. Flask uses the redis
                                      container as a database to run its simple application.
                                    : docker-compose.yml creates a bind mount between the host and container
                                      so changing app.py will cause the app to reload
- Directory                         : ./flaskRedis
- To run
  * Build dockerfile                : docker build -t="jbensson/flaskredis" ./flaskRedis
  * Run                             : cd flaskRedis
                                      docker-compose up
  * See website                     : Navigate chrome to location: localhost:5000
                                      Stops on Ctrl-C
  * Change app.py                   : Verify app reloads
  * Run in background               : docker-compose up -d
  * Stop services                   : docker-compose stop
  * Bring all services down, also
    dropping volumes                : docker-compose down --volumes
  * View web container environment  : docker-compose run web printenv



PROJECT: DJANGO
===============================================================================
- Description                       : Use Docker Compose to set up and run a simple Django/PostgreSQL app
- Directory                         : ./django
- Bibliography                      : https://docs.docker.com/compose/django/
- To run
  * Run                             : docker-compose up
                                      View in Chrome  localhost:8000
                                      View admin page localhost:8000/admin
                                      Ctrl-C to exit



PROJECT: RAILS
===============================================================================
- Description                       : Use Docker Compose to set up and run a simple Rails/PostgreSQL app
- Directory                         : ./rails
- Bibliography                      : https://docs.docker.com/compose/rails/
- To run
  * Run                             : docker-compose up
                                      Ctrl-C to exit
  * Create DB (another terminal)    : docker-compose run web rake db:create
  * View                            : View in Chrome localhost:3000



PROJECT: WORDPRESS
===============================================================================
- Description                       : Use Docker Compose to easily run WordPress in an isolated environment
                                      built with Docker containers
- Directory                         : ./wordPress
- Bibliography                      : https://docs.docker.com/compose/wordpress/
- To run
  * Run                             : docker-compose up
                                      View in Chrome localhost:8000
                                      Ctrl-C to exit
  * Delete data volume              : docker-compose down --volumes
