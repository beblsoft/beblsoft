# Search Engine Optimization (SEO)

Search engine optimization (SEO) is the process of increasing the quality and quantity of website traffic by increasing the visibility of a website or a web page to users of a web search engine.

Relevant URLs:

- [Wikipedia](https://en.wikipedia.org/wiki/Search_engine_optimization)

## Optimizing metadata

As one experiment and optimization we have recorded the source for two competing websites, scrubber.social and brandyourself.com.  The idea here is to study their metadata
and enhance ours to help organic searches for smeckn.

## Notes from the book SEO 2019 by Adam Clark

### Chapter 1: Introduction

1. Info on bloggers for SEO is unreliable
2. Google wants its search engine to return relavent results, good experience
3. Never rely on one tactic for SEO as Google algorithms are constantly evolving, rely on SEO best practices
4. Three principles of best practice:  authority, trust and relevance
   Trust:  keep high quality, legit sites on top, low quality, poor quality on bottom
   Authority:  Sites strength in market, more backlinks equates to more authority
   Relevance:  Build out website with relevant content, build links from relative sites
5. Important attributes of sites that rank high: content relevance, click-through-rate, time-on-site, low bounce rate, https, larger fonts, #images, #internal likes, social media activity, #backlinks, mobile support
6. There are many tools to help with SEO
   Moz.com SEO software and data to help you increase traffic, rankings, and visibility in search results, one month free, then $179 per month, tool to help with SEO
   Searchmetrics.com/knowledge-base/ranking-factors/ - give email and phone and get free key ranking factors
   Seroundtable.com/ - news on Google Search Engine updates, Bert just came out.

### Chapter 2: Keywords

1. Keyword research most important step of SEO
2. Right keywords and right level of competivitity:  too competitive implies difficult to get on page one of search
3. A Keyword can be a single word, combo of words, single words are very competitive
4. Types of keywords:
   * head-term:  one or two words, too competitive, not worth time
   * *long-tail* keywords, 3+ words, worth time
   * navigational keywords, words used to locate brand, e.g. Facebook, not worth time
   * *informational* keywords, find a topic, e.g., what is the best..., worth time
   * *transactional* keywords, e.g. buy jackets online, worth time.
5. There are many tools/techniques for getting keywords:
   * Look at organic search traffic.  https://ahrefs.com/site-explorer provides an in-depth look at the organic search traffic and backlink profile of any website or URL.
   * Use a keyword density checker.  Enter page will give you keywords.There is a free tools at:  https://www.seoreviewtools.com/(find keywords) and  https://www.seoreviewtools.com/ (to merge words together
6. Find the right keywords to send traffic to your site.
7. To use Google's Keyword Planner tool you need be spending modest $ on Google Ad Campaign. https://keywordtool.io/ presents an alternative to Google Keyword Planner.  $159 per month, $79 per month, $69 per month, billed one a year.
8. kwfinder.com is a keyword finder tool, $29, $59, $79 per month. Annual plans are cheaper.  You can enter a region (customer focus) and find out what keywords have the best
traffic potential. Kwfinder will give you a realistic idea of your chances for ranking high for particular keyword. Most important stats are domain authority, page authority, links and facebook shares.  Focus on building backlinks to your page.
9. Use ahref's site explorer tool to see stats for your site. https://ahrefs.com/site-explorer, $179 per month.
10. Find keywords with reasonable levels of traffic, weak competition and then set targets for how many links you need for a top listing.
11. Separate really competive keywords from less competitive.
12. Do not use the ~results from googles search to help w/ SEO, ignore, very unreliable. I.e. there may be a low number of competing pages but these pages could be unbeatable.\

## Chapter 3: Let Google Know What Your Page is About

1. Make sure site is visible to search engines, not blocking search engines and picking up keywords.
2. Ensure URLs are clean English.  We are good here.
3. Follow standard of menu on the top.  We are good here.
4. Ensure navigation is not via pictures!  We are good here.
5. Create content around your keywords. Keywords must appear natural.
6. Have a handful of Latin Semantic Indexing (LSI) keywords on page.
7. Have keywords in meta description and meta title tags.  We are good here.  Limit on title is 70 chars, 155 chars on description.
8. Make sure navigation, headings, content, internal links, images and videos all use keywords.
9. **How fast site loads is important.  Check picture sizes should be <200kb.** See https://developers.google.com/speed/pagespeed/insights. We got a score of 2 on mobile, 31 on desktop.
brandyourself got 26/81. Scrubber.social got 59/94. https://tools.pingdom.com is another good tool.Some suggestions:  enable text compression, remove unused css, resize images,
show images in ng format, eliminate rendor blocking images. Reduce impact of 3rd party code,
blocked main thread by 570ms. Reduce JS execution time, 2.6 seconds.
10.  **Add sitemaps.xml file. Use https://www.xml-sitemaps.com/ to generate a sitemap. Then submit to Google Search Console.**
11. Make sure you don't have a robots.txt file that is blocking your site.
12. Ensure that you do not have duplicate content.
13. Ensure site accessible from all devices, good content quality, clean code, no popups and minimal ads, good operability, no 404s.
14. Ensure mobile friendly. https://search.google.com/test/mobile-friendly?utm_source=mft&utm_medium=redirect&utm_campaign=mft-redirect. We are mobile friendly!!
15. Ensure real information on contact page, avoid excessive monetization of content, list editors and contributers, provide sources, have policy pages and high quality financial pages.
16. Check readability index with https://readable.io/
17. Publish new content on regular basis to increase traffic.
18. Leverage sm accounts to make users aware of new content.
19. Link site to blog aggregators.
20. Include faqs to help get into target feature sections of google.