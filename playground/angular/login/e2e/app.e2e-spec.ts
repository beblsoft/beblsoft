import { Login2Page } from './app.po';

describe('login2 App', () => {
  let page: Login2Page;

  beforeEach(() => {
    page = new Login2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
