import { TestlearnerPage } from './app.po';

describe('testlearner App', () => {
  let page: TestlearnerPage;

  beforeEach(() => {
    page = new TestlearnerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
