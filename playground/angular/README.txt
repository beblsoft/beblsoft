OVERVIEW, TERMS
===============================================================================
Angular2          :commonly referred to as "Angular" or "Angular.js"),
                   A complete JavaScript-based open-source front-end web application
                   framework.
nodejs            :An open-source, cross-platform JavaScript runtime environment
                   for developing a diverse variety of tools and applications.
npm               :npm is the default package manager for the JavaScript runtime
                   environment Node.js.
Angular CLI       :Command line interface for managing Angular2 projects
Augury            :A Chrome development tool for debugging Angular 2 applications
Bootstrap         :Library of HTML and CSS components


ANGULAR CLI
===============================================================================
- Notes
  * Command line interface to automate Angular Application
  * Requirements: Node.js version >= 4.0.0, npm version >= 3.0.0
  * URL: https://www.sitepoint.com/ultimate-angular-cli-reference
  * Doc URL: https://github.com/angular/angular-cli/tree/master/docs/documentation
- Install Angular CLI                       : sudo npm install -g @angular/cli@1.0.0-beta.32.3
- Get angular-cli version                   : ng version
- Initialize a new angular application
  inside the current directory              : ng init
- Create a new application and new dir      : ng new my-app
- Start development server listening at
  http://localhost:4200 (ctrl-c to exit)    : ng serve
                                              --aot: Build using ahead of time compilation
                                              --base-href:Base URL for application being built
                                              --environment <value>:Define build environment
                                              --hmr:Enable hot module replacement
                                              --output-hashing=<none|all|media|bundles>:Define the output filename cache-busting hashing mode
                                              --poll <milliseconds>:Enable and define the file watching period
                                              --port <port#>:port to listen to for serving
                                              --ssl: Serve using HTTPS
                                              --ssl-cert <value>:SSL certificate for serving HTTPS
                                              --ssl-key <value>:SSL key for serving HTTPS
                                              --sourcemap:output sourcemaps
                                              --target=<development|production>
                                              --vendor-chunk:Use a separate bundle containing ventor libraries
                                              --watch:Run build when files change
- Create a new class                        : ng generate class <my-new-class>
- Create a new component                    : ng generate component <my-new-component>
                                              --flat
                                              --inline-template
                                              --inline-style
                                              --prefix
                                              --spec
- Create a new directive                    : ng generate directive <my-new-directive>
- Create a new enum                         : ng generate enum <my-new-enum>
- Create a new module                       : ng generate module <my-new-module>
                                              --routine
                                              --spec
- Create a new pipe                         : ng generate pipe <my-new-pipe>
- Create a new service                      : ng generate service <my-new-service>
                                              --flat
                                              --spec
- Run Karma unit tests                      : ng test
                                              --code-coverage:coverage report in coverage/
                                              --colors:enable colors
                                              --config <file>:Specify config file (default is angular-cli.json)
                                              --log-level <value>
                                              --poll <ms>
                                              --progress:log progress to console while in progress
                                              --sourcemap:output sourcemap
                                              --single-run: don't rerun on file change
                                              --watch=false
- Run end-to-end (E2E) tests                : ng e2e
                                              --config
                                              --element-explorer:start protractors element explorer
                                              --serve
                                              --specs:override specs in protractor config
                                              --webdrive-update:try to update webdriver
- Lint code using tslint                    : ng lint
                                              --fix:fixes linting errors
                                              --type-check: controls the type check for linting
                                              --format <prose|json|stylish|verbose|pmd|msbuild|checkstyle|vso|filelist>
- Build for development/production          : ng build
                                              --aot:ahead of time compilation
                                              --base-href:base url for application being build
                                              --environment
                                              --output-path:path where output will be placed
                                              --sourcemap:ouput sourcemaps
                                              --target=<production|development>
                                              --watch:run build when files change
- Eject the webpack configuration           : ng eject
- Deploy to github                          : ng github-pages:deploy
- Filesystem hierarchy created with ng new
  my-app
   |-README.md
   |-angular-cli.json             - Angular CLI configuration
   |-e2e
   | |-app.e2e-spec.ts
   | |-app.po.ts
   | |-tsconfig.json
   |-karma.conf.js
   |-package.json                 - Contains all npm package information
   |-protractor.conf.js
   |-tslint.json
   |-src
     |-app
     | |-app.component.css
     | |-app.component.html
     | |-app.component.spec.ts
     | |-app.component.ts
     | |-app.module.ts
     | |-index.ts
     | |-shared
     |   |-index.ts
     |-assets
     |-environments
     | |-environment.prod.ts
     | |-environment.ts
     |-favicon.ico
     |-index.html
     |-main.ts
     |-polyfills.ts
     |-styles.css
     |-test.ts
     |-tsconfig.json
     |-typings.d.ts


DEBUGGING
===============================================================================
- Adding breakpooints
  Serve with source maps                    : ng serve --sourcemap
  Open debugger in Chrome                   : Ctrl + Shift + I
  Open an angular file                      : Ctrl + p + app.component.ts
  Add a break point                         : Click on a row number in the source file
  Rerun the code                            : F5


AUGURY
===============================================================================
- Notes
  * Augury is a chrome development tool for debugging Angular 2 apps
  - https://augury.angular.io/
- Install                                   : Chrome Web Store
                                              Type 'Augury'
                                              Click 'Add to Chrome'
- Open debugger console in Chrome           : Ctrl + Shift + I


BOOTSTRAP
===============================================================================
- URLs
  * Google ng2-bootstrap
  * https://valor-software.com/ng2-bootstrap/
  * https://github.com/valor-software/ng2-bootstrap/blob/development/docs/getting-started/ng-cli.md
- Angular cli install                       : npm install ng2-bootstrap bootstrap --save
- src/app/app.module.ts
   import { AlertModule } from 'ng2-bootstrap';
   @NgModule({
     ...
     imports: [AlertModule.forRoot(), ... ],
     ...
   })
- .angular-cli.json
   "styles": [
      "styles.css",
      "../node_modules/bootstrap/dist/css/bootstrap.min.css"
    ],
- src/app/app.component.html
   <alert type="success">hello</alert>