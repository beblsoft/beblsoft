import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
      <app-alert></app-alert>
  	  <router-outlet></router-outlet>
    `,
    styles: [``]
})
export class AppComponent {
    title = 'App Works!';

    constructor() {}
}
