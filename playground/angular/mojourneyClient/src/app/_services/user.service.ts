import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import * as _ from "lodash";

import { MockUsers } from "../_mocks/user.mock";
import { User } from "../_models/user";
import { SR, SRStatus } from "../_models/server-response"


/*---------------------------- REAL SERVICE ---------------------------------*/
@Injectable()
export class UserService {

    private currentUser: User;
    private url: string = 'http://localhost:8080/';

    constructor(private http: Http) {
        this.currentUser = null;
    }


    /**
     * @returns true if a user is logged in, false otherwise
     */
    public isLoggedIn(): boolean {
        return true;
    }

    /**
     * @returns Deep copy of current user, null if current user doesn't exist.
     */
    public getCurrentUser(): User {
        return this.currentUser;
    }

    /**
     * Log out the user from the server, clear current user  
     */
    public logOut(): void {
        this.currentUser = null;
    }

    /**
     * Issue server message to log in user. 
     * On success, set current user.
     * @param  email: User email
     * @param  password: User password
     * @param  rememberMe: If true, remember the user after the browser exits
     * @return SRStatus:SUCCESS or ERROR_LOGIN 
     */
    public logIn(email: String, password: String, rememberMe: boolean):
        Observable < SR > {
            return null;
        }

    /**
     * Issue server message to create user.
     * Prompts user to send an email to the user to verify their account.
     * Note: user is created with verified=false, User must verify account to login
     * @param  email: User email
     * @param  password: User password
     * @return SRStatus:SUCCESS or ERROR_DUPLICATE
     */
    public createUser(userNam: String, password: String): Observable <
        SR > {

            return null;
        }

    /**
     * Issue server message to verify the user hash.
     * Called after the user clicks the verification email. 
     * On success, update current user.
     * @param  hash: Hash value associated with user record
     * @return SRStatus:SUCCESS or ERROR_NOEXISTS
     */
    public updateUser(user: User): Observable < SR > {
        return null;
    }

    /**
     * Issue server message to update user. 
     * On success update current user.
     * @param  user: User record
     * @return SRStatus:SUCCESS or ERROR_INVALID 
     */
    public verifyUser(hash: String): Observable < SR > {

        return null;

    }

    public test_Server(): Observable < String > {
        return this.http.get(this.url)
            .map(res => {
                return res.text()
            });
    }

    public test_getFromServer(): Observable < any > {
        return this.http.get(this.url + "/data")
            .map(res => res.json());
    }

    public test_postToServer(): Observable < any > {
        return this.http.post(this.url + "/data", {'data':'Hello World From Angular2!'})
            .map(res => res.json())

    }
}
