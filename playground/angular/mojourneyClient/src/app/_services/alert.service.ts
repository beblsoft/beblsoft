import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class AlertService {

    /*--------------- Instance variables ---------------------*/
    private subject = new Subject < any > ();
    private keepAfterNavChange = false;

    /*--------------- Constructor ----------------------------*/
    constructor(private router: Router) {
        /* Subscribe to router events so we can clear the alert message
           when changing pages */
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavChange) {
                    this.keepAfterNavChange = false;
                } else {
                    this.subject.next();
                }
            }
        })
    }

    /*--------------- Export Functions -----------------------*/
    success(msg: string, keepAfterNavChange = false) {
        this.keepAfterNavChange = keepAfterNavChange;
        this.subject.next({ type: 'success', text: msg });
    }

    error(msg: string, keepAfterNavChange = false) {
        this.keepAfterNavChange = keepAfterNavChange;
        this.subject.next({ type: 'error', text: msg });
    }

    getMsg(): Observable<any> {
    	return this.subject.asObservable();
    }
}
