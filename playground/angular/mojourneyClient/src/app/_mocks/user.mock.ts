import { User } from "../_models/user";

export const MockUsers: User[] = [{
    "firstName": "foo",
    "lastName": "barr",
    "email": "foobar@gmail.com",
    "password": "1234",
    "numberLogins": 4,
    "id": 1000,
    "verified": true,
    "verifyHash": "asdfkljhasdflkjsdahflakdjsfhasdljkh",
    "dormant": false
}, {
    "firstName": "Jamester",
    "lastName": "Bensson",
    "email": "bensson.james@gmail.com",
    "password": "OneGreatMojourney",
    "numberLogins": 3,
    "id": 999,
    "verified": true,
    "verifyHash": "asdfkljhasdflkjsdahflakdjsfhasdljkh",
    "dormant": false
}, {
    "firstName": "D",
    "lastName": "Black",
    "email": "darrylpblack@gmail.com",
    "password": "AnotherGreatMojourney",
    "numberLogins": 4,
    "id": 1000,
    "verified": true,
    "verifyHash": "asdfkljhasdflkjsdahflakdjsfhasdljkh",
    "dormant": false
}];
