import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginGuard } from './_utl/login.guard';

const appRoutes: Routes = [
   {
        path: 'not-logged-in',
        loadChildren: 'app/not-logged-in/not-logged-in.module#NotLoggedInModule'
    }, {
        path: 'logged-in',
        loadChildren: 'app/logged-in/logged-in.module#LoggedInModule',
        canActivate:[LoginGuard]
    },
    { path: '', redirectTo: 'logged-in', pathMatch: 'full' },
    { path: '**', redirectTo: 'logged-in', pathMatch: 'full' }
    /* TODO
      - Handle default paths { path: '**', component: PageNotFoundComponent }
    - Authentication guard on logged-in module */
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class AppRoutingModule {}
