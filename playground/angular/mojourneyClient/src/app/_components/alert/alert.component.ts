import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../_services/alert.service';

/* ----------------------------------------------------------------*/
/*
  NAME
    AlertComponent

  DESCRIPTION
     Component displays AlertService messages at the top of the application
*/
@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

    /*--------------- Instance variables ---------------------*/
    msgObj: any;
    
    /*--------------- Constructor ----------------------------*/
    constructor(private alertService: AlertService) {}

    /* --------------- Lifecyle ------------------------------*/
    ngOnInit() {
    	this.alertService.getMsg().subscribe(msgObj => {this.msgObj = msgObj})
    }

}
