import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';


/* ----------------------------------------------------------------*/
/*
  NAME
    Validator

  DESCRIPTION
    Function Interface to which all validators must adhere
    See: https://blog.thoughtram.io/angular/2016/03/14/custom-validators-in-angular-2.html    
  
  PARAMS
    FormControl
  
  RETURNS
    If control is valid:   null
    If control is invalid: error object
*/
interface Validator < T extends FormControl > {
    (c: T): {
        [error: string]: any
    };
}


/* ----------------------------------------------------------------*/
/*
  NAME:
    MJValidators

  DESCRIPTION
    Mojourney Custom Validators
*/
export class MJValidators {
    /*-------------------------------------------------------------*/
    /* 
      NAME:
        email

      DESCRIPTION
        Verify that the input Form control contains a valid email address

      RETURNS
        null if valid email
        error object otherwise
     */
    static email(c: FormControl) {
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (c.value == "" ||
            !EMAIL_REGEXP.test(c.value)) {
            return { "badEmailFormat": true };
        }
        return null;
    }

    /*-------------------------------------------------------------*/
    /* 
      NAME
        areEqual

      DESCRIPTION
        Iterate through all controls in group, checking to see if
        they all have the same value

      RETURNS
        null if all controls in group have same value
        error object otherwise
     */
    static areEqual(g: FormGroup) {
        let valid = true;
        let val;
        let first = true;

        for (let name in g.controls) {
            let c: AbstractControl = g.get(name)
            if (first) {
                val = c.value;
                first = false
            } else if (val !== c.value) {
                valid = false;
                break;
            }
        }

        if (valid)
            return null;
        else
            return { "notEqual": true }
    }
}
