import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";

import { UserService } from "../_services/user.service";

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private userService: UserService) {}

    canActivate(destination: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log("login.guard.ts: canActivate")
        return this.userService.isLoggedIn();
    }
}
