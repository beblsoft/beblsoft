import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';

import { AppComponent } from './app/app.component';
import { AppRoutingModule } from './app-routing.module';
import { AlertComponent } from './_components/alert/alert.component';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { LoginGuard } from './_utl/login.guard';

@NgModule({
   imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      AppRoutingModule
   ],
   declarations: [
      AppComponent,
      AlertComponent
   ],
   providers: [
     AlertService,
     UserService,
     LoginGuard
   ],
   bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    //console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
}
