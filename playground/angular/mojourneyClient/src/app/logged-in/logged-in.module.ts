import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';

import { LeftNavComponent } from './left-nav/left-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { LoggedInComponent } from './logged-in/logged-in.component';
import { LoggedInRoutingModule } from './logged-in-routing.module';
import { HomeComponent } from './home/home.component';
import { DailyComponent } from './daily/daily.component';
import { BrowseComponent } from './browse/browse.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { WorkoutsComponent } from './workouts/workouts.component';
import { PlansComponent } from './plans/plans.component';

@NgModule({
   imports: [
      CommonModule,
      DataTablesModule,
      LoggedInRoutingModule,
   ],
   declarations: [
      HomeComponent,
      LeftNavComponent,
      TopNavComponent,
      LoggedInComponent,
      DailyComponent,
      BrowseComponent,
      InstructorsComponent,
      WorkoutsComponent,
      PlansComponent,
      ExercisesComponent,
   ],
   bootstrap: [
      LoggedInComponent
   ]
})
export class LoggedInModule {}
