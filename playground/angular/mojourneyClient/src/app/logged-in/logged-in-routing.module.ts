import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoggedInComponent } from './logged-in/logged-in.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { HomeComponent } from './home/home.component';
import { DailyComponent } from './daily/daily.component';
import { BrowseComponent } from './browse/browse.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { WorkoutsComponent } from './workouts/workouts.component';
import { PlansComponent } from './plans/plans.component';

const loggedInRoutes: Routes = [{
   path: '',
   component: LoggedInComponent,
   /* Router outlet located here */
   children: [
      { path: 'home', component: HomeComponent },
      { path: 'daily', component: DailyComponent },
      { path: 'browse', component: BrowseComponent },
      { path: 'instructors', component: InstructorsComponent },
      { path: 'exercises', component: ExercisesComponent },
      { path: 'workouts', component: WorkoutsComponent },
      { path: 'plans', component: PlansComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full'}
   ]
}];

@NgModule({
   imports: [
      RouterModule.forChild(loggedInRoutes)
   ],
   exports: [
      RouterModule
   ]
})
export class LoggedInRoutingModule {}
