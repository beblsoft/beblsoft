import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';

@Component({
    template: `
  		<h2 class="mj-top-title">My Home</h2>
  		<h2>String from server: {{str}}
  		<h2>Data from server: {{getData | json}}
  		<h2>Post response: {{postResponse | json}}
  `,
    styles: [``]
})
export class HomeComponent implements OnInit {

	private str:String = "Empty";
	private postResponse:any;
	private getData:any;

    constructor(private userService:UserService) {}

    ngOnInit() {
    	this.userService.test_Server().subscribe(str => this.str = str)
    	this.userService.test_getFromServer().subscribe(data => this.getData = data)
    	this.userService.test_postToServer().subscribe(resp => this.postResponse = resp)
    }

}
