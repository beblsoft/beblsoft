/*---------------------------- ENUMS/CONSTS ---------------------------------*/
export const enum SRStatus {
    SUCCESS,
    ERROR_LOGIN,
    ERROR_DUPLICATE,
    ERROR_INVALID,
    ERROR_NOEXISTS
}

const SRMessages: String[] = [
    "Success",
    "Invalid username or password",
    "Record already exists; unable to create",
    "Invalid data send to server; unable to update",
    "Record does not exist"

];


/*---------------------------- CLASS ----------------------------------------*/
export class SR {
    public status: number;
    public message: String;

    constructor(status:SRStatus) {
    	this.status = status;
    	this.message = SRMessages[status];
    }
}