export class User {
    public firstName: String;
    public lastName: String;
    public email: String;
    public password: String;
    public numberLogins: number;
    public id: number;
    public verified: boolean;
    public verifyHash: String;
    public dormant: boolean;

    constructor(email:String, password:String) {
    	this.email = email;
    	this.password = password;
    }

}

