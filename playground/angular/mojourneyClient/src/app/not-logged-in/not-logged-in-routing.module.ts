import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotLoggedInComponent } from './not-logged-in/not-logged-in.component'
import { HomeComponent } from './home/home.component';

const notloggedInRoutes: Routes = [{
	path: '',
	component: NotLoggedInComponent,
	children: [
		{ path: 'account', loadChildren: 'app/not-logged-in/account/account.module#AccountModule'},
    { path: 'home', component: HomeComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
	]
}];

@NgModule({
   imports: [
      RouterModule.forChild(notloggedInRoutes)
   ],
   exports: [
      RouterModule
   ]
})
export class NotLoggedInRoutingModule {}
