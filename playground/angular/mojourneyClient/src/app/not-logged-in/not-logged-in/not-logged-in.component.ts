import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-not-logged-in',
    template: `
  		<router-outlet></router-outlet>
  `,
    styles: [``]
})
export class NotLoggedInComponent implements OnInit {

    constructor() {}

    ngOnInit() {}

}
