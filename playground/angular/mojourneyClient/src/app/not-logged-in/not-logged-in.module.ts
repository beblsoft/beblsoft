import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { NotLoggedInComponent } from './not-logged-in/not-logged-in.component';
import { NotLoggedInRoutingModule } from './not-logged-in-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        NotLoggedInRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        HomeComponent,
        NotLoggedInComponent
    ],
    bootstrap: [
        NotLoggedInComponent
    ]
})
export class NotLoggedInModule {}
