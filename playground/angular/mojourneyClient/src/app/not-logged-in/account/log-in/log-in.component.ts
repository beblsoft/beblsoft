import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { MJValidators } from '../../../_utl/validator';
import { AlertService } from '../../../_services/alert.service';

@Component({
    templateUrl: './log-in.component.html',
    styleUrls: ['./log-in.component.css']
})
export class LoginComponent {
    /*--------------- Instance variables ---------------------*/
    loginForm: FormGroup;

    /*--------------- Constructor ----------------------------*/
    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService) {

        this.createForm();
    }

    /*--------------- Button Methods -------------------------*/
    onSubmit() {
        console.log("Logging in username/password:" + this.loginForm.get('email') + "/" + this.loginForm.get('password'));
        this.router.navigate(['/logged-in/home']);
        /* TODO:
        	loggedIn = userService.login(username, password, remember)
        	if (loggedIn)
        	  navigate to /logged-in/home
        	else
        	  display error message
        */
    }

    onSignup() {
        this.router.navigate(['../sign-up'], {relativeTo: this.route});        
    }

    onForgotPassword() {
        this.router.navigate(['../reset-password'], {relativeTo: this.route});        
    }

    /*--------------- Helper Functions -----------------------*/
    createForm() {
        this.loginForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required],
            rememberMe: ['']
        });
    }

}
