import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'; 

import { AccountComponent } from './account/account.component'
import { LoginComponent} from './log-in/log-in.component'
import { ResetPasswordComponent } from './reset-password/reset-password.component'
import { SignUpComponent} from './sign-up/sign-up.component'
import { AccountRoutingModule } from './account-routing.module'

@NgModule({
   imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      AccountRoutingModule
   ],
   declarations: [
      LoginComponent,
      ResetPasswordComponent,
      SignUpComponent,
      AccountComponent
   ],
   providers: [],
   bootstrap: [AccountComponent]
})
export class AccountModule {}