import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountComponent } from './account/account.component'
import { LoginComponent} from './log-in/log-in.component'
import { ResetPasswordComponent } from './reset-password/reset-password.component'
import { SignUpComponent} from './sign-up/sign-up.component'

const accountRoutes: Routes = [{
   path: '',
   component: AccountComponent,
   /* Router outlet located here */
   children: [
      { path: 'sign-up', component: SignUpComponent },
      { path: 'log-in', component: LoginComponent },
      { path: 'reset-password', component: ResetPasswordComponent },
      { path: '', redirectTo: 'sign-up', pathMatch: 'full' }
   ]
}];

@NgModule({
   imports: [
      RouterModule.forChild(accountRoutes)
   ],
   exports: [
      RouterModule
   ]
})
export class AccountRoutingModule {}
