import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { MJValidators } from '../../../_utl/validator';
import { AlertService } from '../../../_services/alert.service';

@Component({
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {

    /*--------------- Instance variables ---------------------*/
    signUpForm: FormGroup;

    /*--------------- Constructor ----------------------------*/
    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService) {

        this.createForm();
    }

    /*--------------- Button Methods -------------------------*/
    onSubmit() {
        if (!this.signUpForm.get('email').valid) {
            this.alertService.error("Invalid email address.", false);
        } else if (!this.signUpForm.get('passwords').valid) {
            this.alertService.error("Passwords must exist and match.", false);
        } else {
            /* TODO: 
               Have user verify email 
             */
            this.router.navigate(['/logged-in/home']);
            console.log("SigningUp username/password:" + this.signUpForm.get('email') + "/" + this.signUpForm.get('passwords').get('first'))
        }

    }

    onCancel() {
        this.router.navigate(['/not-logged-in/home']);
    }

    /*--------------- Helper Functions -----------------------*/
    createForm() {
        this.signUpForm = this.fb.group({
            email: ['', [Validators.required, MJValidators.email]],
            passwords: this.fb.group({
                'first': ['', Validators.required],
                'second': ['', Validators.required],
            }, { validator: MJValidators.areEqual })
        });
    }

}
