import { MojourneyPage } from './app.po';

describe('mojourney App', () => {
  let page: MojourneyPage;

  beforeEach(() => {
    page = new MojourneyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
