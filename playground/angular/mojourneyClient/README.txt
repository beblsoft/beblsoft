OVERVIEW
===============================================================================
- Mojourney Client-side documentation


STARTING THE APPLICATION
===============================================================================
- 'ng serve' transpiles all the typescript code into JavaScript and starts a
   webserver listening on http://localhost:4200
- main.ts bootstraps AppModule (app.module.ts). This kicks off the entire application.


APPLICATION STRUCTURE
===============================================================================
- At a high level the application is composed of a hierarchy of modules. Each
  module represents a distinct feature set
- The modules are tied together in a tree as follows:
                   ---------------
                   | AppModule   |
                   ---------------
                      /       \
                     /         \
       -------------------    ----------------
       |NotLoggedInModule|    |LoggedInModule|
       -------------------    ----------------
               |                   |   ....
        ---------------       --------------
        |AccountModule|       |BrowseModule|
        ---------------       --------------
 - Modules are composed of components, services, directives, and routing modules.
   See the ROUTING section to understand how a module routes between other modules
   and its components.


CONTROL FLOW
===============================================================================
- main.ts                      - Bootstraps app.module.ts
    |
    |
  app.module.ts                - Bootstraps app.component.ts
    |   app.component.ts       - Contains 'app-root' selector, matches html element in index.html
    |                            This tells angular to but AppComonent in index.html
    |                            Contains <router-outlet> where child routes are places
    |   app-routing.module.ts  - Defines the routes for app.module.ts
    |                            A route is a mapping from url->component (ex. /home -> HomeComponent)
    |                            Components are placed in the <router-outlet> tag in app.component.ts
    |                            The is the ROOT routing module
    |                            Default route, '', defines where to go if no route is defined
    |                            Wildcard route, '**', defines where to go if path has no matches
    |
    |
  url:/not-logged-in           - Bootstraps not-logged-in.component.ts
  not-logged-in.module.ts
    |   not-logged-in.component.ts
    |                          - Defines a generic router outlet for child routes
    |   not-logged-in-routing.module.ts
    |                          - Defines child routes to /not-logged-in
    |   home.component.ts      - Contains the not-logged-in home component
    |   ....                   - More not-logged-in routable components
    |
    |
  url:/logged-in
  logged-in.module.ts          - Bootstraps logged-in.component.ts
    |
    |   logged-in.component.ts - Segments screen into <top-nav>, <left-nav>, and <router-outlet>
    |                            <router-outlet> changes between all of the child routes
    |   logged-in-routing.module.ts
    |                          - Defines child routes to /logged-in
    |                            Maps 'daily' to DailyComponent, etc.
    |   home.component.ts      - Contains the logged-in home component
    |   ....                   - More logged-in routable components


ROUTING
===============================================================================
- Routing is an extremely rich aspect of Angular as it localizes program flow at the
  various points in your application--very different from centralized routing commonly
  used. Here is an example module routing directory structure:
  x/
   - x.module.ts                    - X Module file
   - x-routing.module.ts            - Holds routing table (maps URLs to components)
   - x/
     - x.component.ts
     - x.component.html             - Holds <router-outlet> defining where dynamic components are displayed
     - x.component.css
     - x.component.spec.ts
   - submoduleA/                    - Submodule directory
     ...
   - submoduleZ/                    - Submodule directory
   - subcomponentA/                 - Component used by the module
     ...
   - subcomponentZ/                 - Component used by the module

- x.component.[ts,html]
  Segments the screen into static components (those that will never change)
  and routable components (those that change based on the path). Routable components are
  denoted using the <router-outlet> HTML tag.
- x-routing.module.ts
  Contains the routes. These are mappings between URL paths and the component
  that should be loaded into the <router-outlet> HTML tag.
  The order in the routing table is important; for child routes think of it as a big
  ordered switch/case, with a default at the end. The FIRST pattern that matches will be used.
  RouterModule.forRoute is used only once in the application at the root module;
  subsequent child routing modules always use the RouterModule.forChild construct.
- x-module.ts
  Imports the routing module and the bootstrap which binds
  the next component to the application. It is required that all routable components are included
  in the x.routing.module.ts.
- Button linkage is done using the routerLink="url" directive. The url can either be relative
  or absolute. This tells the router to navigate to the desired component.
  As an example the logged-in.component.ts file divides the screen into a top-nav, left-nav and dynamic
  content panel (basic router-outlet). The left-nav is where the buttons (home, daily,
  browse, instructors, ...) are placed. Given that lef-nav.component.html has all
  the html to define the buttons. The routerLink directive within each of the button definitions
  define the component to invoke when the button is pressed. So, pressing on daily has
  a routerLink of daily and sure enough that rendors to the DailyComponent.
- <activate guards, leave guard, resolve guard>
- Routing Gotchas
  routerLink directive must be outside a html <form> element
  Navigate to child route, routerLink="./child"
  Navigate to a sibling route, routerLink="../sibling"
  Navigate to an absolute path, routerLink="/absolute/path"


FILE/MODULE HIERARCHY
===============================================================================
Useful tree commands:
 - List all files except component and gif      : tree -I '*.component.*|*.gif'
 - List contents of single directory            : tree -L 1

# ----------- SRC CONENTS --------------------- #
├── app/                         - Application
├── assets/                      - Static Assets
├── environments/                - Environment definitions
├── index.html                   - Main index.html file
├── main.ts
├── polyfills.ts
├── test.ts
└── tsconfig.json                - TypeScript configuration

# -------- APP MODULE DIRECTORY --------------- #
├── _guards/                     - router guards
├── _mocks/                      - mock data
├── _models/                     - model classes
├── _providers/                  - service providers
├── _services/                   - services
├── _utl/                        - utilities
├── app/                         - app component
├── alert/                       - alert component
├── logged-in/                   - logged-in module
├── not-logged-in/               - not-logged in module
├── app.module.ts                - app module file
└── app-routing.module.ts        - app routing file

# ------- LOGGED-IN MODULE DIRECTORY ---------- #
├── browse/                       - browse module
├── daily/                        - daily component
├── exercises/                    - exercises component
├── home/                         - home component
├── instructors/                  - instructors component
├── left-nav/                     - left navigation component
├── plans/                        - plans component
├── top-nav/                      - top navication component
├── workouts/                     - workouts component
├── logged-in/                    - logged-in component
├── logged-in.module.ts           - logged-in module file
└── logged-in-routing.module.ts   - logged-in routing file



CONVENTIONS
===============================================================================
Components
 - Button functions should be prefixed with 'on'. Ex. onSubmit, onCancel
 - Refact html, css into files if html is greater than 10 lines
 Commenting
 - When global or non-obvious, use flower box style as illustrated in _utl/validator.ts
 - Component classes should be split up into logical subsections as shown in sign-up.component.ts


ANGULAR CLI (.angular-cli.json)
===============================================================================
 - Currently loading two versions of jQuery
   * BootstrapMade/NiceAdmin/js/jquery-1.8.3.min.js   - Used for dropdowns
   * node_modules/jquery/dist/jquery.js               - Used for dataTables