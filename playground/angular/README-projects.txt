OVERVIEW
===============================================================================
Angular 2 Sample Projects Documentation


PROJECT: DEMO
===============================================================================
- Description:
  * Angular cli base project created with 'ng new demo'
  * Prints 'app works!' message
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: TODO
===============================================================================
- Description
  * Create and remove items from a todo list.
  * URL: https://www.sitepoint.com/angular-2-tutorial/
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: ROUTER
===============================================================================
- Description
  * Various routing techniques available in the Angular 2 framework
  * URL: https://angular.io/docs/ts/latest/guide/router.html
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: TOUR OF HEROES
===============================================================================
- Description
  * Multiple components, services, routing, and HTTP.
  * URL:https://angular.io/docs/ts/latest/tutorial/
- To Run
  * Install dependencies                    : Update package.json to include dependency "angular-in-memory-web-api": "~0.2.4",
                                              npm install
  * Issue with angular-cli                  : npm uninstall angular-cli
                                              npm install @angular/cli --save-dev
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: LOGIN
===============================================================================
- Description
  * Sample login workflow
  * URL: https://github.com/cornflourblue/angular2-registration-login-example
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: FORMS
===============================================================================
- Description
  * Template-form processing
  * URL: https://embed.plnkr.co/?show=preview
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: REACTIVE FORMS
===============================================================================
- Description
  * Reactive-form processing
  * URL: https://angular.io/docs/ts/latest/guide/reactive-forms.html
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200

PROJECT: FANCYTABLE
===============================================================================
- Description
  * Tables with pagination, searching, sorting
  * Setup URL: https://l-lin.github.io/angular-datatables/#/getting-started

  * Basic Table URL: https://l-lin.github.io/angular-datatables/#/basic/zero-config
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200


PROJECT: TEST_FIRST
===============================================================================
- Description
  * Basic angular hello world test, verifying that true is true
  * See src/app/1st.spec.ts
- To Run
  * Install dependencies                    : npm install
  * Run unit test                           : npm test


PROJECT: TEST_BANNERINLINE
===============================================================================
- Description
  * Angular component test. Test modifies the component title and verifies that
    the title changed
  * See src/app/banner-inline.component.spec.ts
- To Run
  * Install dependencies                    : npm install
  * Run unit test                           : npm test


PROJECT: ANGULAR2-FLASK
===============================================================================
- Description
  * Angular application that sends backend requests to a flask server
  * Git repo with instructions: https://github.com/ansrivas/angular2-flask
- To Run Server
  * cd backend
  * sudo pip install -r requirements.txt
  * python run.py
- To Run Client
  * cd front
  * npm install webpack-dev-server rimraf webpack typescript -g
  * npm install
  * npm run server:dev:hmr


PROJECT: CREATIVETIM-MATERIAL_FREE
===============================================================================
- Description
  * Showcases the free version of Creative Tim Material UI
  * Website: https://www.creative-tim.com/product/material-dashboard-angular2
  * Demo Website: http://md-angular2.creative-tim.com/
  * Documationation: See ./documentation/tutorial-components.htm
- To Run
  * Install dependencies                    : npm install
  * Start development server                : npm start; See Chrome: http://localhost:3000/#/dashboard


PROJECT: ANGULAR2TYPESCRIPTBOOK
===============================================================================
- Description
  * Angular examples from "Angular 2 Development with TypeScript"
  * Git repo: https://github.com/Farata/angular2typescript
- Notes
  * For projects that don't start a live web server, must use firefox
- Chapter 7: Working with Forms
  * form_samples/
    - See see all examples modify app/index.html
      Changing System.import('app/*')
    - Simple template-driven form, enhanced
      with showing username, password         : 01_template-driven.ts
    - Dynamically changing form size          : 02_growable-items-form.ts
    - Basic reactive form                     : 03_reactive.ts
    - Reactive form using FormBuilder         : 04_form-builder.ts
    - Simple Custom validation, reactive form : 05_custom-validator.ts
    - Simple Custom validation, template form : 06_custom-validator-directive.ts
    - Displaying validator error messages
      form.getError(), form.hasError()        : 07_custom-validator-error-message.ts
    - Simulate server-side validation         : 08_asyc-validator.ts
    - Demo multiple reactive validators--both
      form and group validators               : 09_reactive-with-validation.ts
    - Same as above but template-based
      validation                              : 10_template-driven-with-validation.ts
    - Show use of embedded components         : 11_default-value-accessor.ts
    - Show add/remove items in array          : 12_growable-items-form-with-remove.ts
- Chapter 8: Interacting with servers using HTTP and WebSockets
  * http_websocket_samples/
    - For these examples, the Angular code (client) and REST interface (server)
      are served from same server
    - tsconfig.json: Add "watch" : true, this gets ts transpiler to watch for changes
    - nodemon only listens on changes to the js files, not ts files; indirectly if
      the ts file changes and is retranspiled, changing the js file nodemon will restart.
    - build the code                          : npm run tsc
    - run the specified server                : npm run <server>
                                                <server>=one of: dev,hello,devRest, restServer,
                                                simple-websocket-server,two-way-websocket-server,
                                                bid-server
    - Navigate to page                        : Open chrome: localhost:8000
    - npm run restServer
      Must change client/systemjs.config.js, app:main.ts
    - npm run twowayWsServer
      Must change client/systemjs.config.js, app:websocket-observable-service-subscriber
    - npm run bidServer
      Must change client/systemjs.config.js, app:bids/bid-component
- Chapter 10: Bundling and Deploying Applications with Webpack
  * hello-world/
    - Build bundle.js from main.js            : webpack main.js bundle.js
    - Fix index.html                          : <script src="bundle.js"></script>
  * hello-world-deverver/
    - Install and run webpack-dev-server      : npm install; npm start
  * basic-webpack-starter/
    - Reflect.ts transpilation errors on npm start
  * angular2-webpack-starter
    - Live development webserver              : npm start
    - Build JIT                               : npm run build
    - Build and Serve JIT                     : npm run serve:dist
    - Build AOT                               : npm run build:aot
    - Build and Serve AOT                     : npm run serve:dist:aot
    - Run unit tests                          : npm run test


PROJECT: MOJOURNEYCLIENT
===============================================================================
- Description
  * Angular 2 implementation of the mojourney client
  * See mojourneyClient/README.txt for more details
- To Run
  * Install dependencies                    : npm install
  * Start development server                : ng serve; See Chrome: http://localhost:4200