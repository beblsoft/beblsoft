import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';

// interface dt {
//     DataTable(): any;
// }

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
    // @ViewChild('example') el: dt;
    title = 'app works!';

    ngOnInit() {

    }
    ngAfterViewInit() {
    	// this.el.DataTable();
    }
}
