import { FancytablePage } from './app.po';

describe('fancytable App', () => {
  let page: FancytablePage;

  beforeEach(() => {
    page = new FancytablePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
