const path = require('path');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'main': './src/main.ts',
        'vendor': './src/vendor.ts'
    },
    output: {
        path: path.resolve('./dist'),
        filename: 'bundle.js'
    },
    plugins: [
        new CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.bundle.js' }),
        new CopyWebpackPlugin([{ from: './src/index.html', to: 'index.html' }]),
        new ContextReplacementPlugin(
            // needed as a workaround for the Angular's internal use System.import()
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            path.join(__dirname, 'src') // location of your src
        ),
    ],
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ],
        noParse: [path.join(__dirname, 'node_modules', 'angular2',
            'bundles')]
    },
    devServer: {
        contentBase: 'src',
        historyApiFallback: true
    },
    devtool: 'source-map'
};
