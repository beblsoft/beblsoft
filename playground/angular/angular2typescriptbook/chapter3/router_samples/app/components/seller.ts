import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'seller',
    template: 'The seller of this product is Mary Lou (98%) {{ sellerID1 }} {{ sellerID2 }}',
    styles: [':host {background: yellow}']
})
export class SellerInfoComponent {
   sellerID1: string;
   sellerID2: string;

   constructor(route: ActivatedRoute){
     this.sellerID1 = route.snapshot.params['id1'];
     this.sellerID2 = route.snapshot.params['id2'];
     console.log(`The SellerInfoComponent got the seller id ${this.sellerID1}`);
   }
}
