import {Component} from '@angular/core';

@Component({
    selector: 'product',
    template: '<h1 class="product">Product Detail Component</h1>',
    styles: ['.product {background: red}']
})
export class ProductDetailComponent {}
