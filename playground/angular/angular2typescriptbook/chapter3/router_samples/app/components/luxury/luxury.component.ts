import {Component} from '@angular/core';

@Component({
    selector: 'luxury',
    template: `<h1 class="gold">Luxury Component</h1>`,
    styles: ['.gold {background: blue}']
})
export class LuxuryComponent {}