OVERVIEW
===============================================================================
Flask RestPLUS Projects Documentation



PROJECT: CORE
===============================================================================
- Description                       : Core single file applications
- Directory                         : ./singleFileApps
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- quickstart.py ------------------- : Quickstart
  * Start Flask app                 : ./quickstart.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Get Hello World JSON            : curl -X GET localhost:5000/hello
  * Get TODOS                       : curl -X GET localhost:5000/todos
  * Get TODO todo1                  : curl -X GET localhost:5000/todos/todo1
  * Update todo1                    : curl -X PUT localhost:5000/todos/todo1 -d "data=Change by brakepads"
  * Get TODOS                       : curl -X GET localhost:5000/todos
  * Multiple routes to the same
    endpoint. Get TODOS via alias   : curl -X GET localhost:5000/mytodos

- errorHandling.py ---------------- : Abort and handle errors
  * Start Flask app                 : ./exceptions.py
  * Raise Bad Request               : curl -X GET localhost:5000/BadRequest
  * Raise Bad Request with message  : curl -X GET localhost:5000/BadRequestCustom
  * Raise Bad Request with data     : curl -X GET localhost:5000/BadRequestCustomData
  * Raise Abort                     : curl -X GET localhost:5000/Abort

- exceptions.py ------------------- : Handle and document exceptions
  * Start Flask app                 : ./exceptions.py
  * Handle Generic Exception        : curl -X GET localhost:5000/GenericException
  * Handle Beblsoft Exception       : curl -X GET localhost:5000/BeblsoftException

- enumField.py -------------------- : Create a field from a python enumeration
  * Start Flask app                 : ./enumField.py
  * Get enum                        : curl -X GET localhost:5000/enum
  * Post to enum incorrectly        : curl -X POST -d '{"enum":"INVALID"}' --header "Content-Type: application/json" 'localhost:5000/enum'
  * Post to enum correctly          : curl -X POST -d '{"enum":"CAR"}' --header "Content-Type: application/json" 'localhost:5000/enum'

- model.py ------------------------ : Define JSON object schema
  * Start Flask app                 : ./model.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Retreive all objects            : $ curl -X GET 'http://localhost:5000/object'
  * Retreive object by ID           : $ curl -X GET 'http://localhost:5000/object/1'
  * Delete object by ID             : $ curl -X DELETE 'http://localhost:5000/object/1'

- modelNested.py ------------------ : Example with a nested model three layers deep
  * Start Flask app                 : ./modelNested.py
  * Get                             : curl -X GET localhost:5000/object
  * Get without using marshal
    decorator                       : curl -X PUT localhost:5000/object

- parser.py ----------------------- : Examples using parser
  * Start Flask app                 : ./requestParsing.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Default argument, don't specify : curl -X GET 'http://localhost:5000/default'
  * Default argument, specify       : curl -X GET 'http://localhost:5000/default?arg=6'
  * Destination change              : curl -X GET 'http://localhost:5000/dest?arg=6'
  * Required argument,don't specify : curl -X GET 'http://localhost:5000/required'
  * Required argument,specify       : curl -X GET 'http://localhost:5000/required?arg=2'
  * Multiple args                   : curl -X GET 'http://localhost:5000/append?arg=1&arg=2&arg=3'
  * Required cookie,  don't spcify  : curl -X GET 'http://localhost:5000/cookie'
  * Required cookie,  specify       : curl -X GET --cookie 'COOKIE-ARG=1' 'http://localhost:5000/cookie'
  * Required header,  don't spcify  : curl -X GET 'http://localhost:5000/header'
  * Required header,  specify       : curl -X GET --header 'HEADER-ARG:1' 'http://localhost:5000/cookie'
  * Choice argument,  specify wrong : curl -X GET 'http://localhost:5000/choice?arg=6'
  * Choice argument,  specify right : curl -X GET 'http://localhost:5000/choice?arg=4'
  * JSON Data,        specify wrong : curl -P POST -d '{}' -H "Content-Type: application/json" 'http://localhost:5000/json'
  * JSON Data,        specify right : curl -P POST -d '{"arg":1}' -H "Content-Type: application/json" 'http://localhost:5000/json'

- parserInputs.py -------------------
  * Start Flask app                 : ./requestParsing.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Default argument, don't specify : curl -X GET 'http://localhost:5000/default'
  * URL                             : curl -X GET 'http://localhost:5000/default?url=https://www.google.com'
  * Boolean Bad                     : curl -X GET 'http://localhost:5000/default?boolean=bad'
  * Boolean true                    : curl -X GET 'http://localhost:5000/default?boolean=true'
  * Boolean 1                       : curl -X GET 'http://localhost:5000/default?boolean=1'
  * Boolean false                   : curl -X GET 'http://localhost:5000/default?boolean=false'
  * Boolean 0                       : curl -X GET 'http://localhost:5000/default?boolean=0'
  * date                            : curl -X GET 'http://localhost:5000/default?date=1900-11-12'
  * date_from_iso8601               : curl -X GET 'http://localhost:5000/default?date_from_iso8601=2012-01-01T23%3A30%3A00%2B02%3A00'
  * datetime_from_rfc822            : curl -X GET 'http://localhost:5000/default?datetime_from_rfc822=Wed%2C%2002%20Oct%202002%2008%3A00%3A00%20EST'
  * email                           : curl -X GET 'http://localhost:5000/default?email=bensson.james@gmail.com'
  * int_range                       : curl -X GET 'http://localhost:5000/default?int_range=0'
  * ip                              : curl -X GET 'http://localhost:5000/default?ip=192.168.12.12'
  * ipv4                            : curl -X GET 'http://localhost:5000/default?ipv4=192.168.12.12'
  * ipv6                            : curl -X GET 'http://localhost:5000/default?ipv6=2001:db8:85a3::8a2e:370:7334'
  * iso8601interval                 : curl -X GET 'http://localhost:5000/default?iso8601interval=2013-01-01T06%3A00%2F2013-01-01T12%3A00'
  * natural                         : curl -X GET 'http://localhost:5000/default?natural=9'
  * positive                        : curl -X GET 'http://localhost:5000/default?positive=1'
  * pattern                         : curl -X GET 'http://localhost:5000/default?pattern=98'

- fieldsMask.py ------------------- : Demonstrate model field masking. Force client to specify desired fields
  * Start Flask app                 : ./fieldsMask.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Get default fields              : curl -X GET 'http://localhost:5000/default'
  * Get str1, str2                  : curl -X GET -H "X-Fields: str1,str2" 'http://localhost:5000/default'
  * Get all fields                  : curl -X GET 'http://localhost:5000/all'

- swaggerDocumentation.py --------- : Document input, output, errors
  * Start Flask app                 : ./swaggerDocumentation.py
  * View swagger UI                 : Chrome localhost:5000
  * View swagger spec               : Chrome localhost:5000/swagger.json
  * Retreive first object           : curl -X GET 'http://localhost:5000/object/1'
  * Post Invalid JSON               : curl -X POST -d '{"name" : "John Smith"}' --header 'Content-Type: application/json' http://localhost:5000/object/5
  * Post valid JSON                 : curl -X POST -d '{"id" : 4, "name" : "John Smith"}' --header 'Content-Type: application/json' http://localhost:5000/object/4
  * Get Invalid Args                : curl -X GET 'http://localhost:5000/WithParser'
  * Get Valid Args                  : curl -X GET 'http://localhost:5000/WithParser?name=James'
  * Deprecated Route                : http://localhost:5000/Deprecated
  * Hidden Route (not on spec)      : http://localhost:5000/Hidden
  * Security Route                  : http://localhost:5000/Security
  * Return JSON Schema              : curl -X GET 'http://localhost:5000/Schema'

- xml.py -------------------------- : Return XML Data
  * Start Flask app                 : ./xml.py
  * Hello to James in XML           : curl -X GET localhost:5000/jamesbensson



PROJECT: TODO
===============================================================================
- Description                       : Various todo list apps
- Directory                         : ./todo
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt

- todo.py ------------------------- : Todos grouped in namespace
                                      Returning multiple objects in list
                                      Put objects in collection
  * Start Flask app                 : ./todo.py
  * Get all todos                   : curl -X GET localhost:5000/todos
  * View todo1                      : curl -X GET localhost:5000/todos/todo1

- todomvc.py ---------------------- : Same as todo.py, but leveraging a
                                      Data Access Object
  * Get all todos                   : curl -X GET localhost:5000/todos
  * View first todo                 : curl -X GET localhost:5000/todos/1

- todo_blueprint.py --------------- : Flask-restplus combined with blueprints
                                      Create a versioned API
  * Start Flask app                 : ./todo_blueprint.py
  * Get all todos                   : curl -X GET localhost:5000/api/1/todos
  * View todo1                      : curl -X GET localhost:5000/api/1/todos/todo1
  * View swagger UI                 : curl -X GET localhost:5000/api/1
  * View swagger spec               : curl -X GET localhost:5000/api/1/swagger.json



PROJECT: ZOO
===============================================================================
- Description                       : Break up an app into multiple namespace files
- Directory                         : ./zoo
- To Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- To Run
  * Start Flask app                 : ./zoo.py
  * List cats                       : localhost:5000/cats
  * List dogs                       : localhost:5000/dogs
  * View swagger UI                 : localhost:5000/
  * View swagger spec               : localhost:5000/swagger.json



PROJECT: SECURITY TOKENS
===============================================================================
- Description                       : Secure endpoints with API keys
                                      Demonstrate client usage
- Directory                         : ./securityTokens
- Server Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- Server Usage
  * Start Flask app                 : ./app.py
  * View swagger spec               : localhost:5000/swagger.json
  * Get with no auth                : curl -X GET 'http://localhost:5000/SecureGet'
  * Get with invalid API key        : curl -X GET  --header 'X-API-KEY:foo'   --header 'X-ACCOUNT-URN:1234' 'http://localhost:5000/SecureGet'
  * Get with invalid URN            : curl -X GET  --header 'X-API-KEY:admin' --header 'X-ACCOUNT-URN:123'  'http://localhost:5000/SecureGet'
  * Get with correct credentials    : curl -X GET  --header 'X-API-KEY:admin' --header 'X-ACCOUNT-URN:1234' 'http://localhost:5000/SecureGet'
  * Post with correct credentials   : curl -X POST --header 'X-API-KEY:admin' --header 'X-ACCOUNT-URN:1234'  -d '{"foo":1, "bar": 2}' --header "Content-Type: application/json" 'http://localhost:5000/SecurePost'
- Client Usage
  * Note                            : Server must be running
  * Open path path in chrome        : file:///home/jbensson/git/beblsoft/playground/flaskRestplus/securityTokens/index.html
  * Buttons                         : Click buttons to send requests to server



PROJECT: SECURITY COOKIES
===============================================================================
- Description                       : Secure endpoints with cookies
                                      Demonstrate client browser usage
- Files
  app.py                            : Flask Application
  index.html                        : Vue Web Client
  index.js                          : Express Web Client Server
- Directory                         : ./securityCookies
- Server Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- Server Usage
  * Start Flask app                 : ./app.py
  * View swagger spec               : https://localhost:5001/swagger.json
- Client Install
  * Install dependencies            : npm install
- Client Usage
  * Note                            : Server must be running
  * Start client server             : npm start
                                      Open chrome to https://localhost:3000/
                                      Open Dev Tools -> Application -> Cookies -> https://localhost:3000
  * Click Login                     : __demo_auth_cookie, __demo_account_cookie should be set
  * Click Get Auth Status           : status: LOGGED_IN
  * Click Get Account               : email: foo@bar.com
  * Click Logout                    : __demo_auth_cookie, __demo_account_cookie should NOT be set
  * Click Get Auth Status           : status: LOGGED_OUT



PROJECT: PAGINATION
===============================================================================
- Description                       : Demonstrate resource pagination
- Directory                         : ./pagination
- Files
  * main.py                         : main program
  * app.py                          : application code
  * database.py                     : database code
- Serer Install
  * Create virtualenv               : virtualenv -p /usr/bin/python3.6 venv
  * Enter virtualenv                : source venv/bin/activate
  * Install requirements            : pip3 install -r requirements.txt
- Server Usage
  * Start Flask app                 : ./app.py
  * View swagger spec               : localhost:5000/swagger.json
  * Get, Sort ID's ASCENDING        : curl -X GET 'localhost:5000/object?sortType=ID&sortOrder=ASCENDING'
  * Get, Sort ID's ASCENDING nxt pg : curl -X GET 'localhost:5000/object?sortType=ID&sortOrder=ASCENDING&cursor=eyJ2YWwiOiA1fQ=='
  * Get, Sort ID's DESCENDING       : curl -X GET 'localhost:5000/object?sortType=ID&sortOrder=DESCENDING'
  * Get, Sort ID's DESCENDING       : curl -X GET 'localhost:5000/object?sortType=ID&sortOrder=DESCENDING'
  * Get, Sort ID's DESCENDING lim=30: curl -X GET 'localhost:5000/object?sortType=ID&sortOrder=DESCENDING&limit=30'
  * Get, Sort Date ASCENDING        : curl -X GET 'localhost:5000/object?sortType=DATETIME&sortOrder=ASCENDING'
  * Get, Sort Date DESCENDING       : curl -X GET 'localhost:5000/object?sortType=DATETIME&sortOrder=DESCENDING'
  * Get String, Sort ID             : curl -X GET 'localhost:5000/object?sortType=ID&searchString=Five'
  * Get MinID, Sort ID              : curl -X GET 'localhost:5000/object?sortType=ID&minID=10'

