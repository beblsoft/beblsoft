#!/usr/bin/env python3
"""
NAME:
 modelNested.py

DESCRIPTION
 Flask-RestPlus Nested Model Example
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask
from flask_restplus import Resource, Api, fields, marshal  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ API MODEL ---------------------------------------- #
thirdLevel = api.model("ThirdLevel",
                       {
                           "id": fields.Integer(),
                       })

secondLevel = api.model("SecondLevel",
                        {
                            "id": fields.Integer(),
                            "thirdLevel": fields.Nested(thirdLevel),
                            "thirdLevelList": fields.List(fields.Nested(thirdLevel))
                        })

firstLevel = api.model("FirstLevel",
                       {
                           "id": fields.Integer(),

                           "secondLevelNone": fields.Nested(secondLevel, allow_null=True),
                           # Produces
                           # "secondLevelNone": null,

                           "secondLevelNullValues": fields.Nested(secondLevel, allow_null=False),
                           # Produces
                           # "secondLevelNullValues": {
                           #     "id": null,
                           #     "thirdLevel": {
                           #         "id": null
                           #     },
                           #     "thirdLevelList": null
                           # },

                           "secondLevel": fields.Nested(secondLevel),
                           "secondLevelList": fields.List(fields.Nested(secondLevel))
                       })


# ------------------------ DAO ---------------------------------------------- #
def getFirstLevel(nSecondLevel=3):  # pylint: disable=C0111
    return {
        "id": 1,
        "secondLevel": getSecondLevel(),
        "secondLevelList": [getSecondLevel() for _ in range(nSecondLevel)]
    }


def getSecondLevel(nThirdLevel=3):  # pylint: disable=C0111
    return {
        "id": 2,
        "thirdLevel": getThirdLevel(),
        "thirdLevelList": [getThirdLevel() for _ in range(nThirdLevel)]
    }


def getThirdLevel():  # pylint: disable=C0111
    return {
        "id": 3
    }


# ------------------------ ROUTES ------------------------------------------- #
@api.route("/object")
class Object(Resource):
    """
    Object Routes
    """
    @api.marshal_with(firstLevel)
    def get(self):  # pylint: disable=R0201
        """
        Get resource
        """
        return getFirstLevel()

    def put(self):  # pylint: disable=R0201
        """
        Marshal without using decorator
        Useful when data needs to be marshalled within the scope of a database session
        """
        rval = marshal(getFirstLevel(), firstLevel)
        return rval


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
