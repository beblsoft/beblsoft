#!/usr/bin/env python3
"""
NAME:
 parserInputs.py

DESCRIPTION
 Flask-RestPlus Parser Inputs

BIBLIOGRAPHY
  Docs    : https://flask-restplus.readthedocs.io/en/stable/api.html#module-flask_restplus.inputs
  Sources : https://flask-restplus.readthedocs.io/en/stable/_modules/flask_restplus/inputs.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask
from flask_restplus import Resource, Api, inputs  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ PARSER ------------------------------------------- #
requestGetParser = api.parser()
requestGetParser.add_argument("url",
                              type     = inputs.URL(schemes=['http', 'https']),  # pylint: disable=E1101
                              required = False,
                              location = "args")
requestGetParser.add_argument("boolean",
                              type     = inputs.boolean,
                              required = False,
                              location = "args")
requestGetParser.add_argument("date",
                              type     = inputs.date,
                              required = False,
                              location = "args")
requestGetParser.add_argument("date_from_iso8601",
                              type     = inputs.datetime_from_iso8601,
                              required = False,
                              location = "args")
requestGetParser.add_argument("datetime_from_rfc822",
                              type     = inputs.datetime_from_rfc822,
                              required = False,
                              location = "args")
requestGetParser.add_argument("email",
                              type     = inputs.email(),
                              required = False,
                              location = "args")
requestGetParser.add_argument("int_range",
                              type     = inputs.int_range(low=0, high=10),
                              required = False,
                              location = "args")
requestGetParser.add_argument("ip",
                              type     = inputs.ip,
                              required = False,
                              location = "args")
requestGetParser.add_argument("ipv4",
                              type     = inputs.ipv4,
                              required = False,
                              location = "args")
requestGetParser.add_argument("ipv6",
                              type     = inputs.ipv6,
                              required = False,
                              location = "args")
requestGetParser.add_argument("iso8601interval",
                              type     = inputs.iso8601interval,
                              required = False,
                              location = "args")
requestGetParser.add_argument("natural",
                              type     = inputs.natural,
                              required = False,
                              location = "args")
requestGetParser.add_argument("positive",
                              type     = inputs.positive,
                              required = False,
                              location = "args")
requestGetParser.add_argument("pattern",
                              type     = inputs.regex("^[0-9]+$"),
                              required = False,
                              location = "args")


# ------------------------ ROUTE -------------------------------------------- #
@api.route('/default')  # pylint: disable=C0111
class Default(Resource):

    @api.expect(requestGetParser)
    def get(self):  # pylint: disable=R0201,C0111

        args = requestGetParser.parse_args()
        # Convert everything to a string to avoid JSON encode errors
        for k, v in args.items():
            args[k] = str(v)

        return {"args": args}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
