#!/usr/bin/env python3
"""
NAME:
 model.py

DESCRIPTION
 Flask-RestPlus Model Demo

BIBLIOGRAPHY
  Walkthrough    : http://flask-restplus.readthedocs.io/en/stable/marshalling.html
  Raw Fields     : http://flask-restplus.readthedocs.io/en/stable/api.html#flask_restplus.fields.Raw
  Url Fields     : http://flask-restplus.readthedocs.io/en/stable/api.html#flask_restplus.fields.Url
  Field-Specific : http://flask-restplus.readthedocs.io/en/stable/swagger.html#documenting-the-fields
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import random
from datetime import datetime
import forgery_py
from flask import Flask
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ DATA --------------------------------------------- #
objectDict = {}
objectList = []
nObjects   = 5
for idx in range(0, nObjects):
    obj = {
        "id_db": idx,
        "name_db": forgery_py.name.full_name(),
        "nested1_db": {"nestedInner_db": 3 * idx},
        "nested2_db": {"nestedList_db": [{"nestedInner_db": 3 * idx}]},

        # Flags:
        # 0x1 = Urgent
        # 0x2 = Unread
        "flags_db": idx,

        "stringList_db": ["Hello", "{}".format(idx), "!"],
        "innerObject_db": {"id_db": 5 * idx},
        "dateCreated_db": datetime.now(),
    }
    objectDict[idx] = obj
    objectList.append(obj)


# ------------------------ CUSTOM FIELDS ------------------------------------ #
class UrgentField(fields.Raw):  # pylint: disable=C0111

    def format(self, value):  # pylint: disable=R0201,C0111
        return "Urgent" if value & 0x01 else "Normal"


class UnreadField(fields.Raw):  # pylint: disable=C0111

    def format(self, value):  # pylint: disable=R0201,C0111
        return "Unread" if value & 0x02 else "Read"


class RandomNumber(fields.Raw):  # pylint: disable=R0201,C0111

    def output(self, key, obj):  # pylint: disable=R0201,W0613,W0621,C0111
        return random.random()


class MyIntField(fields.Integer):  # pylint: disable=C0111
    __schema_format__  = "int64"
    __schema_example__ = 0


# ------------------------ API MODEL ---------------------------------------- #
innerObject = api.model("InnerObject",
                        {
                            "id": fields.Integer(
                                default     = 0,
                                attribute   = "id_db",
                                title       = "Inner Object ID",
                                example     = 5),
                        })

objectModel = api.model("Object",
                        {
                            "id": fields.Integer(
                                default     = 0,
                                attribute   = "id_db",
                                title       = "Object ID",
                                description = "Object Database ID",
                                required    = True,
                                readonly    = True,
                                example     = 5),
                            "name": fields.String(
                                default     = "Anonymous",
                                attribute   = lambda x: x.get("name_db"),
                                title       = "Object Name",
                                description = "Object Name",
                                required    = True,
                                readonly    = False,
                                example     = "John Smith"),
                            "nested1": fields.Integer(
                                attribute   = "nested1_db.nestedInner_db",
                                example     = 5),
                            "nested2": fields.Integer(
                                attribute   = lambda x: x["nested2_db"][
                                    "nestedList_db"][0]["nestedInner_db"],
                                example     = 5),
                            "priority": UrgentField(
                                attribute   = "flags_db",
                                title       = "Object priority",
                                description = ""),
                            "status": UnreadField(
                                attribute   = "flags_db"),
                            "stringList": fields.List(
                                fields.String,
                                attribute   = "stringList_db"),
                            "innerObject": fields.Nested(
                                innerObject,
                                attribute   = "innerObject_db"),
                            "dateCreated": fields.DateTime(
                                attribute   = "dateCreated_db",
                                dt_format   = "rfc822",
                                title       = "Date object created",
                                description = "Date object created",
                                example     = 5),
                            "random": RandomNumber(
                                title       = "Random Number"),
                            "int": MyIntField(
                                attribute   = lambda x: 5,
                                titile      = "Integer Field"),
                            # URL Fields don't work on /objectID/1
                            # See: https://github.com/flask-restful/flask-restful/issues/418
                            # "url": fields.Url(
                            #     endpoint    = "ObjectID",
                            #     absolute    = False),
                            # "httpsUrl": fields.Url(
                            #     endpoint    = None,  # Defaults to request.endpoint
                            #     absolute    = True,
                            #     scheme      = "https"),
                        },
                        #mask = "{id,name,nestedInner,priority,status,random,uri,httpsUri,dateCreated}"
                        )


# ------------------------ ROUTES ------------------------------------------- #
@api.route("/object")
class Object(Resource):
    """
    Object Routes
    """
    @api.marshal_with(objectModel)
    def get(self):  # pylint: disable=R0201
        """
        Get resource
        """
        return objectList


@api.route("/object/<int:objectID>", endpoint="ObjectID")
class ObjectID(Resource):
    """
    Object ID Routes
    """

    @api.marshal_with(objectModel)
    def get(self, objectID):  # pylint: disable=R0201
        """
        Get resource
        """
        return objectDict[objectID]

    def delete(self, objectID):  # pylint: disable=R0201
        """
        Delete resource
        """
        del objectDict[objectID]
        for currObj in objectList:
            if currObj["id_db"] == objectID:
                objectList.remove(currObj)

# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
