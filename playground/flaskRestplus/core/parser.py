#!/usr/bin/env python3
"""
NAME:
 parser.py

DESCRIPTION
 Flask-RestPlus Request Parsing Demo

BIBLIOGRAPHY
  Walkthrough                 : http://flask-restplus.readthedocs.io/en/stable/parsing.html
  Argument                    : http://flask-restplus.readthedocs.io/en/stable/api.html#flask_restplus.reqparse.Argument

ARGUMENT CONSTRUCTOR PARAMETERS
  name                        : Either a name or a list of option strings, e.g. foo or -f, –foo.
  default                     : The value produced if the argument is absent from the request.
  dest                        : The name of the attribute to be added to the object returned by parse_args().
  required (bool)             : Whether or not the argument may be omitted (optionals only).
  action (string)             : The basic type of action to be taken when this argument is
                                encountered in the request. Valid options are “store” and “append”.
  ignore (bool)               : Whether to ignore cases where the argument fails type conversion
  type                        : The type to which the request argument should be converted.
                                If a type raises an exception, the message in the error will be returned
                                in the response. Defaults to unicode in python2 and str in python3.
  location                    : The attributes of the flask.Request object to source the arguments from
                                (ex: headers, args, etc.), can be an iterator. The last item listed takes
                                precedence in the result set.
  choices                     : A container of the allowable values for the argument.
  help                        : A brief description of the argument, returned in the response when
                                the argument is invalid. May optionally contain an “{error_msg}”
                                interpolation token, which will be replaced with the text of the
                                error raised by the type converter.
  case_sensitive (bool)       : Whether argument values in the request are case sensitive or not
                                (this will convert all values to lowercase)
  store_missing (bool)        : Whether the arguments default value should be stored if the argument
                                is missing from the request.
  trim (bool)                 : If enabled, trims whitespace around the argument.
  nullable (bool)             : If enabled, allows null value in argument.
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import forgery_py
import werkzeug
from flask import Flask
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ DEFAULT ROUTE ------------------------------------ #
defaultParser = api.parser()
defaultParser.add_argument("arg",
                           type     = int,
                           required = False,
                           default  = 5,
                           location = "args")


@api.route('/default')  # pylint: disable=C0111
class Default(Resource):

    @api.expect(defaultParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": defaultParser.parse_args()}


# ------------------------ DEST ROUTE --------------------------------------- #
destParser = api.parser()
destParser.add_argument("arg",
                        type     = int,
                        required = False,
                        default  = 5,
                        dest     = "argDest",
                        location = "args")


@api.route('/dest')  # pylint: disable=C0111
class Dest(Resource):

    @api.expect(destParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": destParser.parse_args()}


# ------------------------ REQUIRED ROUTE ----------------------------------- #
requiredParser = api.parser()
requiredParser.add_argument("arg",
                            type     = int,
                            required = True,
                            location = "args")

@api.route('/required')  # pylint: disable=C0111
class Required(Resource):

    @api.expect(requiredParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": requiredParser.parse_args()}


# ------------------------ APPEND ROUTE ------------------------------------- #
appendParser = api.parser()
appendParser.add_argument("arg",
                          type     = int,
                          required = True,
                          action   = "append",
                          location = "args")

@api.route('/append')  # pylint: disable=C0111
class Append(Resource):

    @api.expect(appendParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": appendParser.parse_args()}


# ------------------------ COOKIE ROUTE ------------------------------------- #
cookieParser = api.parser()
cookieParser.add_argument("COOKIE-ARG",
                          type     = int,
                          required = True,
                          location = "cookies")

@api.route('/cookie')  # pylint: disable=C0111
class Cookie(Resource):

    @api.expect(cookieParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": cookieParser.parse_args()}


# ------------------------ HEADER ROUTE ------------------------------------- #
headerParser = api.parser()
headerParser.add_argument("HEADER-ARG", # Note: doesn't work with underscore "_"
                          type     = int,
                          required = True,
                          location = "headers")

@api.route('/header')  # pylint: disable=C0111
class Header(Resource):

    @api.expect(headerParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": headerParser.parse_args()}


# ------------------------ CHOICE ROUTE ------------------------------------- #
choiceParser = api.parser()
choiceParser.add_argument("arg",
                          type     = int,
                          required = True,
                          choices  = (1,2,3,4,5),
                          location = "args",
                          help     = "Bad choice: {error_msg}")

@api.route('/choice')  # pylint: disable=C0111
class Choice(Resource):

    @api.expect(choiceParser)
    def get(self):  # pylint: disable=R0201,C0111
        return {"res": choiceParser.parse_args()}


# ------------------------ JSON ROUTE --------------------------------------- #
jsonParser = api.parser()
jsonParser.add_argument("arg",
                        type     = int,
                        required = True,
                        location = "json")

@api.route('/json')  # pylint: disable=C0111
class Json(Resource):

    @api.expect(jsonParser)
    def post(self):  # pylint: disable=R0201,C0111
        return {"res": jsonParser.parse_args()}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
