#!/usr/bin/env python3
"""
NAME:
 errorHandling.py

DESCRIPTION
 Flask-RestPlus Error Handling Demo

BIBLIOGRAPHY
  Walkthrough  : http://flask-restplus.readthedocs.io/en/stable/errors.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask
from flask_restplus import Resource, Api, abort  # pylint: disable=E0401
from werkzeug.exceptions import BadRequest


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ ROUTES ------------------------------------------- #
@api.route('/BadRequest') #pylint: disable=C0111
class BadRequestRoutes(Resource):

    def get(self): #pylint: disable=R0201,C0111
        raise BadRequest()

@api.route('/BadRequestCustom') #pylint: disable=C0111
class BadRequestCustomRoutes(Resource):

    def get(self): #pylint: disable=R0201,C0111
        raise BadRequest("My Custom Message")

@api.route('/BadRequestCustomData') #pylint: disable=C0111
class BadRequestCustomDataRoutes(Resource):

    def get(self): #pylint: disable=R0201,C0111
        e      = BadRequest("My Custom Message")
        e.data = {"customValue1": 1, "customValue2": 2} #pylint: disable=W0201
        raise e

@api.route('/Abort') #pylint: disable=C0111
class AbortRoutes(Resource):

    def get(self): #pylint: disable=R0201,C0111
        abort(400, message="My Custom Message", custValue1=1, customValue2=2)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
