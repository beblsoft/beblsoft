#!/usr/bin/env python3
"""
NAME:
 fieldsMask.py

DESCRIPTION
 Flask-RestPlus Fields Mask Demo

BIBLIOGRAPHY
  Walkthrough : http://flask-restplus.readthedocs.io/en/stable/mask.html

"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ MODEL -------------------------------------------- #
model = api.model("InnerObject",
                  {
                      "str1": fields.String(),
                      "str2": fields.String(),
                      "str3": fields.String(),
                      "str4": fields.String(),
                      "int1": fields.Integer(),
                      "int2": fields.Integer(),
                      "int3": fields.Integer(),
                      "int4": fields.Integer(),
                  },
                  mask="{str1, int1}")

# ------------------------ DEFAULT ROUTE ------------------------------------ #
@api.route('/default')  # pylint: disable=C0111
class Default(Resource):

    @api.marshal_with(model)
    def get(self):  # pylint: disable=R0201,C0111

        return {
          "str1" : "Hello1",
          "str2" : "Hello2",
          "str3" : "Hello3",
          "str4" : "Hello4",
          "int1" : 1,
          "int2" : 2,
          "int3" : 3,
          "int4" : 4
        }


# ------------------------ ALL ROUTE ---------------------------------------- #
@api.route('/all')  # pylint: disable=C0111
class All(Resource):

    @api.marshal_with(model, mask="*")
    def get(self):  # pylint: disable=R0201,C0111

        return {
          "str1" : "Hello1",
          "str2" : "Hello2",
          "str3" : "Hello3",
          "str4" : "Hello4",
          "int1" : 1,
          "int2" : 2,
          "int3" : 3,
          "int4" : 4
        }



# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
