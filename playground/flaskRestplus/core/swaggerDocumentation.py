#!/usr/bin/env python3
"""
NAME:
 swaggerDocumentation.py

DESCRIPTION
 Flask-RestPlus Swagger Documentation Demo

BIBLIOGRAPHY
  Walkthrough : http://flask-restplus.readthedocs.io/en/stable/swagger.html
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import forgery_py
from flask import Flask, request
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app, authorizatons={
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
})
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ DATA --------------------------------------------- #
objectDict = {}
objectList = []
nObjects   = 5
for idx in range(0, nObjects):
    obj = {
        "id_db": idx,
        "name_db": forgery_py.name.full_name(),
    }
    objectDict[idx] = obj
    objectList.append(obj)


# ------------------------ OBJECT ROUTES ------------------------------------ #
objectModel = api.model("Object",
                        {
                            "id": fields.Integer(
                                default     = 0,
                                attribute   = "id_db",
                                title       = "Object ID",
                                description = "Object Database ID",
                                required    = True,
                                readonly    = True,
                                example     = 5),
                            "name": fields.String(
                                default     = "Anonymous",
                                attribute   = lambda x: x.get("name_db"),
                                title       = "Object Name",
                                description = "Object Name",
                                required    = True,
                                readonly    = False,
                                example     = "John Smith"),
                        },
                        )


@api.route("/object")
class Object(Resource):
    """
    Object Routes
    """
    @api.marshal_with(objectModel, code=200, as_list=True)
    @api.response(500, 'Server Error')
    def get(self):  # pylint: disable=R0201
        """
        Get resource
        """
        return objectList


@api.route("/object/<int:objectID>", endpoint="ObjectID")
@api.param("objectID", "Object Identifier")
class ObjectID(Resource):
    """
    Object ID Routes
    """

    @api.marshal_with(objectModel)
    @api.response(200, 'Success')
    @api.response(400, 'Validation Error')
    def get(self, objectID):  # pylint: disable=R0201
        """
        Get resource
        """
        return objectDict[objectID]

    @api.marshal_with(objectModel)
    @api.expect(objectModel, validate=True)
    def post(self, objectID):  # pylint: disable=R0201
        """
        Post resource
        """
        del objectDict[objectID]
        for currObj in objectList:
            if currObj["id_db"] == objectID:
                objectList.remove(currObj)

        postObj  = request.json
        dbobj    = {
            "id_db": postObj.get("id"),
            "name_db": postObj.get("name"),
        }
        objectDict[objectID] = dbobj
        objectList.append(dbobj)
        return dbobj

    def delete(self, objectID):  # pylint: disable=R0201
        """
        Delete resource
        """
        del objectDict[objectID]
        for currObj in objectList:
            if currObj["id_db"] == objectID:
                objectList.remove(currObj)


# ------------------------ PARSER ROUTE ------------------------------------- #
parser = api.parser()
parser.add_argument('name', required=True, help="Name cannot be blank!")


@api.route('/WithParser', endpoint='WithParser')  # pylint: disable=C0111
class WithParser(Resource):

    @api.expect(parser)
    def get(self):  # pylint: disable=R0201,C0111
        args = parser.parse_args()
        print(args)
        return args


# ------------------------ DEFAULT ROUTE ------------------------------------ #
@api.route('/DefaultError', endpoint='DefaultError')  # pylint: disable=C0111
class DefaultError(Resource):

    @api.response('3**', 'Redirction')
    @api.response('4**', 'Client Errors')
    @api.response('5**', 'Server Errors')
    def get(self):  # pylint: disable=C0111
        pass


# ------------------------ DEPRECATED ROUTE --------------------------------- #
@api.route('/Deprecated', endpoint='Deprecated')  # pylint: disable=C0111
@api.deprecated
class Deprecated(Resource):

    def get(self):  # pylint: disable=C0111
        pass


# ------------------------ HIDDEN ROUTE ------------------------------------- #
@api.route('/Hidden', endpoint='Hidden')  # pylint: disable=C0111
#@api.doc(False)
@api.hide
class Hidden(Resource):

    def get(self):  # pylint: disable=C0111
        pass


# ------------------------ SCHEMA ROUTE ------------------------------------- #
@api.route('/Schema', endpoint='Schema')  # pylint: disable=C0111
class Schema(Resource):

    def get(self):  # pylint: disable=R0201,C0111
        return api.__schema__


# ------------------------ SECURITY ROUTE ----------------------------------- #
@api.route('/Security', endpoint='Security')  # pylint: disable=C0111
class Security(Resource):

    @api.doc(security='apiKey')
    def get(self):  # pylint: disable=R0201,C0111
        return {1: 1}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
