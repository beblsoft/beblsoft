#!/usr/bin/env python3
"""
NAME:
 enumField.py

DESCRIPTION
 Flask-RestPlus Enum Field Demo
"""

# ------------------------ IMPORTS ------------------------------------------ #
import enum
import logging
from flask import Flask, request
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ ENUM --------------------------------------------- #
class SampleEnum(enum.Enum):
    """
    Sample Enum
    """
    FOO = enum.auto()
    BAR = enum.auto()
    CAR = enum.auto()


# ------------------------ ENUM FIELD --------------------------------------- #
class SampleEnumField(fields.String):
    """
    Sample Enum Field
    """

    def __init__(self, required=True, **kwargs):
        super().__init__(
            title       = "Sample Enum Field",
            description = "Sample Enum Field",
            example     = SampleEnum.FOO.name,
            enum        = [e.name for e in list(SampleEnum)],
            required    = required,
            ** kwargs)

    def format(self, value):
        if isinstance(value, str):  # pylint: disable=R1705
            return value
        elif isinstance(value, SampleEnum):
            return value.name
        else:
            return None


# ------------------------ MODEL -------------------------------------------- #
enumModel = api.model("EnumModel",
                      {
                          "enum": SampleEnumField(
                              required = True)
                      })


# ------------------------ DEFAULT ROUTE ------------------------------------ #
@api.route('/enum')  # pylint: disable=C0111
class EnumResource(Resource):

    @api.marshal_with(enumModel)
    def get(self):  # pylint: disable=R0201,C0111

        return {
            "enum": SampleEnum.FOO
        }


    @api.expect(enumModel, validate=True)
    @api.marshal_with(enumModel)
    def post(self):  # pylint: disable=R0201,C0111
        return request.json



# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
