#!/usr/bin/env python3
"""
NAME:
 exceptions.py

DESCRIPTION
 Flask-RestPlus Exceptions Example

BIBLIOGRAPHY
  https://github.com/noirbizarre/flask-restplus/blob/master/examples
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger()
logger.setLevel(level=logging.NOTSET)


# ------------------------ MODEL -------------------------------------------- #
errorModel = api.model("Error", {
    "code": fields.Integer(
        attribute   = "code",
        title       = "Server Error Code",
        example     = 1140),
    "message": fields.String(
        attribute   = "extMsg",
        title       = "Server Error Message",
        example     = "No token attached to request object"),
})


# ------------------------ ROUTES ------------------------------------------- #
@api.route('/BeblsoftException')
class BeblsoftException(Resource):
    """
    Exeptions resource
    """

    def get(self):  # pylint: disable=R0201,W0613
        """
        Fetch resource
        """
        raise BeblsoftError(code=BeblsoftErrorCode.JWT_TOKEN_SPACES)


@api.route('/GenericException')
class GenericException(Resource):
    """
    Exeptions resource
    """

    def get(self):  # pylint: disable=R0201,W0613
        """
        Fetch resource
        """
        raise Exception


# ------------------------ EXCEPTION HANDLERS ------------------------------- #
@api.errorhandler(BeblsoftError)
@api.marshal_with(errorModel)
def handleBeblsoftError(error):
    """
    Beblsoft Error Handler
    """
    return error, error.httpStatus


@api.errorhandler
def handleGenericException(error):  # pylint: disable=W0613
    """
    Default error handler
    """
    return {'message': 'Unknown Exception'}, 400


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
