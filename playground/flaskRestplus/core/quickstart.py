#!/usr/bin/env python3
"""
NAME:
 quickstart.py

DESCRIPTION
 Flask-RestPlus Quickstart

BIBLIOGRAPHY
  http://flask-restplus.readthedocs.io/en/stable/quickstart.html#
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
from flask import Flask, request
from flask_restplus import Resource, Api # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
api    = Api(app)
logger = logging.getLogger(__name__)
todos  = {
    "todo1": "Get Milk",
    "todo2": "Get Cheese",
    "todo3": "Get Eggs"
}


# ------------------------ HELLO WORLD ROUTE -------------------------------- #
@api.route("/hello")
class HelloWorld(Resource):
    """
    Hello World Resource
    """

    def get(self):  # pylint: disable=R0201
        """
        Get Hello World
        """
        return {"hello": "world"}


# ------------------------ TODO ROUTES -------------------------------------- #
@api.route('/todos', '/mytodos')
class Todos(Resource):
    """
    TODOS Routes
    """

    def get(self):  # pylint: disable=R0201
        """
        Fetch resource
        """
        return todos

@api.route('/todos/<string:todo_id>')
class TodoID(Resource):
    """
    TODO ID Routes
    """

    def get(self, todo_id):  # pylint: disable=R0201
        """
        Fetch resource
        """
        return {todo_id: todos[todo_id]}

    def put(self, todo_id):  # pylint: disable=R0201
        """
        Put resource
        """
        todos[todo_id] = request.form['data']
        return {todo_id: todos[todo_id]}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == '__main__':
    app.run(debug=True)
