#!/usr/bin/env python3
"""
NAME:
 todomvc.py

DESCRIPTION
 Flask-RestPlus Data Access Objects Example

BIBLIOGRAPHY
  https://github.com/noirbizarre/flask-restplus/blob/master/examples
"""


# ------------------------ IMPORTS ------------------------------------------ #
from flask import Flask
from flask_restplus import Api, Resource, fields #pylint: disable=E0401
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version="1.0", title="TodoMVC API",
    description="A simple TodoMVC API",
)

ns = api.namespace("todos", description="TODO operations")

todoModel = api.model("Todo", {
    "id": fields.Integer(readOnly=True, description="The task unique identifier"),
    "task": fields.String(required=True, description="The task details")
})


# ------------------------ TODO DATA ACCESS OBJECT -------------------------- #
class TodoDAO(object):
    """
    TODO Data Access Object
    """
    def __init__(self):
        self.counter = 0
        self.todos = []

    def get(self, id):
        """
        Get TODO
        """
        for todo in self.todos:
            if todo["id"] == id:
                return todo
        api.abort(404, "Todo {} doesn't exist".format(id))

    def create(self, data):
        """
        Create TODO
        """
        todo = data
        todo["id"] = self.counter = self.counter + 1
        self.todos.append(todo)
        return todo

    def update(self, id, data): #pylint: disable=W0622
        """
        Update TODO
        """
        todo = self.get(id) #pylint: disable=W0621
        todo.update(data)
        return todo

    def delete(self, id): #pylint: disable=W0622
        """
        Delete TODO
        """
        todo = self.get(id) #pylint: disable=W0621
        self.todos.remove(todo)


DAO = TodoDAO()
DAO.create({"task": "Build an API"})
DAO.create({"task": "?????"})
DAO.create({"task": "profit!"})


# ------------------------ TODO LIST API ------------------------------------ #
@ns.route("/")
class TodoList(Resource):
    """
    Shows a list of all todos, and lets you POST to add new tasks
    """

    @ns.doc("list_todos")
    @ns.marshal_list_with(todoModel)
    def get(self): #pylint: disable=R0201
        """
        List all tasks
        """
        return DAO.todos

    @ns.doc("create_todo")
    @ns.expect(todoModel)
    @ns.marshal_with(todoModel, code=201)
    def post(self): #pylint: disable=R0201
        """
        Create a new task
        """
        return DAO.create(api.payload), 201


# ------------------------ TODO API ----------------------------------------- #
@ns.route("/<int:id>")
@ns.response(404, "Todo not found")
@ns.param("id", "The task identifier")
class Todo(Resource):
    """
    Show a single todo item and lets you delete them
    """
    @ns.doc("get_todo")
    @ns.marshal_with(todoModel)
    def get(self, id): #pylint: disable=R0201,W0622
        """
        Fetch a given resource
        """
        return DAO.get(id)

    @ns.doc("delete_todo")
    @ns.response(204, "Todo deleted")
    def delete(self, id): #pylint: disable=R0201,W0622
        """
        Delete a task given its identifier
        """
        DAO.delete(id)
        return "", 204

    @ns.expect(todoModel)
    @ns.marshal_with(todoModel)
    def put(self, id): #pylint: disable=R0201,W0622
        """
        Update a task given its identifier
        """
        return DAO.update(id, api.payload)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(debug=True)
