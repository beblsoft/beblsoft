#!/usr/bin/env python3
"""
NAME
 main.py

DESCRIPTION
 Main pagination application

USEFUL LINKS
 Pagination at Slack : https://slack.engineering/evolving-api-pagination-at-slack-1c1f644f8e12
 Hacker Noon         : https://hackernoon.com/guys-were-doing-pagination-wrong-f6c18a91b232
"""

# ------------------------- IMPORTS ----------------------------------------- #
import getpass
import logging
from base.mysql.python.bServer import BeblsoftMySQLServer
from playground.flaskRestplus.pagination.database import Database
from playground.flaskRestplus.pagination.app import app


# ----------------------------- MAIN ---------------------------------------- #
logger  = logging.getLogger(__name__)
logging.basicConfig(level=logging.WARNING)
bDBS    = BeblsoftMySQLServer(domainNameFunc = lambda: "localhost",
                             user = getpass.getuser(), password="", echo=False)
bDB     = Database(bServer=bDBS, name="Pagination", echo=False)
app.bDB = bDB

if __name__ == "__main__":
    try:
        bDB.delete()
        bDB.create()
        bDB.createTables()
        bDB.populate()
        app.run(debug=True)
    finally:
        bDB.delete()
