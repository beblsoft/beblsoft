#!/usr/bin/env python3
"""
NAME
 app.py

DESCRIPTION
 App for pagination example
"""

# ------------------------- IMPORTS ----------------------------------------- #
import json
import base64
from flask import Flask, current_app, g
from flask_restplus import Resource, Api, fields  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode
from playground.flaskRestplus.pagination.database import ObjectSortType, ObjectSortOrder
from playground.flaskRestplus.pagination.database import ObjectEnum


# ------------------------- PAGINATION MODELS ------------------------------- #
app  = Flask(__name__)
api  = Api(app)

paginationParser = api.parser()
paginationParser.add_argument("cursor",
                              type     = str,
                              required = False,
                              location = "args")
paginationParser.add_argument("limit",
                              type     = int,
                              required = False,
                              default  = 5,
                              choices  = list(range(100)),
                              location = "args")

paginationModel = api.model(
    "PaginationModel",
    {
        "nextCursor": fields.String(
            title       = "Next opaque cursor string",
            description = ("Next opaque cursor string to pass on susequent pagination request. "
                           "If '', no more next"),
            example     = "",
            default     = "")
    }
)


class PaginationCursor():
    """
    Pagination Cursor
    """

    def __init__(self, val=None):
        """
        Initialize Object
        """
        self.val = val

    def toString(self):
        """
        Return Base64 encoded string
        """
        string      = json.dumps(self.__dict__)
        stringBytes = bytes(string, "utf-8")
        b64String   = base64.b64encode(stringBytes).decode("utf-8")
        return b64String

    @staticmethod
    def fromString(b64String):
        """
        Return Cursor from Base64 encoded string
        """
        try:
            stringBytes = base64.b64decode(b64String).decode("utf-8")
            metaDict    = json.loads(stringBytes)
            obj         = PaginationCursor(**metaDict)
            return obj
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="Cursor={}".format(b64String))

    def castVal(self, castFunc):
        """
        Cast value
        Args
          castFunc:
            Function to cast value
            Ex. str, int, json.loads
        """
        try:
            return castFunc(self.val)
        except Exception as _:  # pylint: disable=W0703
            raise BeblsoftError(code=BeblsoftErrorCode.GEN_BAD_CURSOR,
                                msg="Cursor={}".format(self.val))


# ------------------------- OBJECT MODELS ----------------------------------- #
objectGetParser = paginationParser.copy()
objectGetParser.add_argument("sortType",
                             type     = str,
                             required = True,
                             default  = ObjectSortType.DATETIME.name,
                             choices  = [e.name for e in list(ObjectSortType)],
                             location = "args")
objectGetParser.add_argument("sortOrder",
                             type     = str,
                             required = False,
                             default  = ObjectSortOrder.ASCENDING.name,
                             choices  = [e.name for e in list(ObjectSortOrder)],
                             location = "args")
objectGetParser.add_argument("minID",
                             type     = int,
                             required = False,
                             default  = None,
                             location = "args")
objectGetParser.add_argument("maxID",
                             type     = int,
                             required = False,
                             default  = None,
                             location = "args")
objectGetParser.add_argument("searchString",
                             type     = str,
                             required = False,
                             default  = None,
                             location = "args")

objectModel = api.model(
    "ObjectModel",
    {
        "id": fields.Integer(
            default     = 0,
            title       = "Object ID",
            description = "Object Database ID",
            required    = True,
            readonly    = True,
            example     = 5),
        "boolean": fields.Boolean(
            default     = False,
            title       = "Boolean",
            description = "Sample Boolean"),
        "enum": fields.String(
            title       = "Enum",
            description = "Sample Enum",
            attribute   = lambda x: x.enum.name,
            example     = ObjectEnum.FOO.name,
            enum        = [str(name) for name, _ in ObjectEnum.__members__.items()]),
        "integer": fields.Integer(
            title       = "Integer",
            description = "Sample Integer",
            example     = 0,
            default     = 0),
        "string": fields.String(
            default     = "Anonymous",
            title       = "String",
            description = "Sample String",
            example     = "John Smith"),
        "dateTime": fields.DateTime(
            dt_format   = "rfc822",
            title       = "DateTime",
            description = "Sample DateTime")
    },
)

objectGetModel = api.inherit(
    "ObjectGetModel",
    paginationModel,
    {
        "data": fields.List(fields.Nested(objectModel)),
    }
)


# ------------------------ API OBJECTS -------------------------------------- #
@api.route("/object")
class ObjectResource(Resource):
    """
    Object Routes
    """
    @api.expect(objectGetParser)
    @api.marshal_with(objectGetModel)
    def get(self):  # pylint: disable=R0201
        """
        Get resource
        """
        g.args                         = objectGetParser.parse_args()
        g.sortType                     = ObjectSortType[g.args.get("sortType")]
        g.sortOrder                    = ObjectSortOrder[g.args.get("sortOrder")]
        g.cursorStr                    = g.args.get("cursor")
        if g.cursorStr:
            g.cursor                   = PaginationCursor.fromString(g.cursorStr)
        else:
            g.cursor                   = None
        g.nextCursor                   = None
        g.data                         = []
        g.limit                        = g.args.get("limit")
        g.searchKwargs                 = {}
        g.searchKwargs["sortType"]     = g.sortType
        g.searchKwargs["sortOrder"]    = g.sortOrder
        g.searchKwargs["limit"]        = g.limit
        g.searchKwargs["minID"]        = g.args.get("minID")
        g.searchKwargs["maxID"]        = g.args.get("maxID")
        g.searchKwargs["searchString"] = g.args.get("searchString")

        # Call approriate get function. Function updates g.data, g.nextCursor.
        getFuncDict                 = {
            ObjectSortType.ID: self.getSortedByID,
            ObjectSortType.ENUM: self.getSortedByEnum,
            ObjectSortType.DATETIME: self.getSortedByDateTime
        }
        getFuncDict[g.sortType]()

        return {
            "nextCursor": g.nextCursor.toString() if g.nextCursor else "",
            "data": g.data
        }

    def getSortedByID(self):  # pylint: disable=R0201
        """
        Get results sorted by ID
        """
        if g.cursor:
            val = g.cursor.castVal(int)
            if g.sortOrder == ObjectSortOrder.ASCENDING:
                g.searchKwargs["minID"] = val
            else:
                g.searchKwargs["maxID"] = val

        g.data = current_app.bDB.search(**g.searchKwargs)

        if len(g.data) == g.limit:
            lastObj      = g.data[-1]
            g.nextCursor = PaginationCursor(val=lastObj.id)

    def getSortedByEnum(self):  # pylint: disable=R0201
        """
        Get results sorted by Enum
        """
        offset = g.cursor.castVal(int) if g.cursor else 0

        g.searchKwargs["offset"] = offset
        g.data = current_app.bDB.search(**g.searchKwargs)

        if len(g.data) == g.limit:
            g.nextCursor = PaginationCursor(val=offset + g.limit)

    def getSortedByDateTime(self):  # pylint: disable=R0201
        """
        Get results sorted by DateTime
        """
        offset = g.cursor.castVal(int) if g.cursor else 0

        g.searchKwargs["offset"] = offset
        g.data = current_app.bDB.search(**g.searchKwargs)

        if len(g.data) == g.limit:
            g.nextCursor = PaginationCursor(val=offset + g.limit)
