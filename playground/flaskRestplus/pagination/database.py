#!/usr/bin/env python3
"""
NAME
 database.py

DESCRIPTION
 Database for pagination example
"""

# ------------------------- IMPORTS ----------------------------------------- #
import enum
import random
from datetime import datetime, timedelta
from sqlalchemy import asc, desc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, DateTime, Enum
import forgery_py
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase


# ------------------------- DATABASE MODELS --------------------------------- #
Base = declarative_base()


class ObjectSortType(enum.Enum):
    """
    Sort Types
    """
    ID       = enum.auto()
    ENUM     = enum.auto()
    INTEGER  = enum.auto()
    DATETIME = enum.auto()

    def getColumn(self):
        """
        Return object column corresponding to sort type
        """
        columnDict = {
            ObjectSortType.ID: Object.id,
            ObjectSortType.ENUM: Object.enum,
            ObjectSortType.INTEGER: Object.integer,
            ObjectSortType.DATETIME: Object.dateTime
        }
        return columnDict[self]


class ObjectSortOrder(enum.Enum):
    """
    Sort Orders
    """
    ASCENDING  = enum.auto()
    DESCENDING = enum.auto()

    def getFunc(self):
        """
        Return sqlalchemy ordering func based on sort order
        """
        return asc if self == ObjectSortOrder.ASCENDING else desc


class ObjectEnum(enum.Enum):
    """
    Object Enum
    """
    FOO = 1
    BAR = 2
    CAR = 3
    FAR = 4


class Object(Base):
    """
    Object Class
    """
    __tablename__  = "object"
    id             = Column(Integer, primary_key=True, autoincrement=True)
    boolean        = Column(Boolean)
    enum           = Column(Enum(ObjectEnum), default=ObjectEnum.FOO)
    integer        = Column(Integer)
    string         = Column(String(64))
    dateTime       = Column(DateTime, default=datetime.utcnow())

    def __repr__(self):
        return "[{} id={} enum={} string={} dateTime={}]".format(
            self.__class__.__name__, str(self.id).ljust(3), self.enum.name.ljust(4),
            self.string.ljust(40), self.dateTime)


# ----------------------------- DATABASE ------------------------------------ #
class Database(BeblsoftMySQLDatabase):  # pylint: disable=C0111

    @logFunc()
    def createTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.create_all(self.engine)

    @logFunc()
    def deleteTables(self):  # pylint: disable=R0201,C0111
        Base.metadata.drop_all(self.engine)

    @logFunc()
    def populate(self,
                 nPoints = 500, minInt = 0, maxInt = 1000000,
                 secondsBackMax = 60 * 60 * 24 * 365 * 10):
        """
        Populate tables
        """
        wordList = ["One", "Two", "Three", "Four", "Five"]
        with self.sessionScope() as s:
            for _ in range(0, nPoints):
                samp = Object(
                    boolean    = random.random() > .5,
                    enum       = random.choice(list(ObjectEnum)),
                    integer    = random.randint(minInt, maxInt),
                    string     = forgery_py.basic.text(length=32) + random.choice(wordList),
                    dateTime   = (datetime.utcnow() -
                                  timedelta(seconds = random.randint(0, secondsBackMax))))
                s.add(samp)

    @logFunc()
    def search(self,
               minID = None, maxID = None,
               minDateTime = None, maxDateTime = None,
               searchString = None,
               sortType = None, sortOrder = ObjectSortOrder.ASCENDING,
               limit = None, offset = None):
        """
        Search
        """
        with self.sessionScope(commit=False) as s:
            q = s.query(Object)

            # Filters
            if minID is not None:
                q = q.filter(Object.id > minID)

            if maxID is not None:
                q = q.filter(Object.id < maxID)

            if minDateTime is not None:
                q = q.filter(Object.dateTime > minDateTime)

            if maxDateTime is not None:
                q = q.filter(Object.dateTime < maxDateTime)

            if searchString is not None:
                q = q.filter(Object.string.ilike("%{}%".format(searchString)))

            # Sorting
            if sortType and sortOrder:
                orderFunc = sortOrder.getFunc()
                column    = sortType.getColumn()
                q         = q.order_by(orderFunc(column))

            # Offset
            if offset is not None:
                q = q.offset(offset)

            # Limit
            if limit is not None:
                q = q.limit(limit)

            # print(q)
            result = q.all()

            return result
