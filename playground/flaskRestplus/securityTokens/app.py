#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Flask-RestPlus Security Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import json
import logging
from functools import wraps
from flask import Flask, abort, g, request
from flask_cors import CORS
from flask_restplus import Resource, Api  # pylint: disable=E0401


# ------------------------ GLOBALS ------------------------------------------ #
app    = Flask(__name__)
CORS(app)
api    = Api(app,
             security       = ["API_AUTH_KEY", "API_ACCOUNT_URN"],
             authorizations = {
                 "API_AUTH_KEY": {
                     "type": "apiKey",
                     "in": "header",
                     "name": "X-API-KEY"
                 },
                 "API_ACCOUNT_URN": {
                     "type": "apiKey",
                     "in": "header",
                     "name": "X-ACCOUNT-URN"
                 },
             })
logger = logging.getLogger()
logger.setLevel(level = logging.NOTSET)


# ------------------------ SECURITY ----------------------------------------- #
securityParser = api.parser()
securityParser.add_argument("X-API-KEY",
                            type     = str,
                            required = True,
                            location = "headers")
securityParser.add_argument("X-ACCOUNT-URN",
                            type     = int,
                            required = True,
                            location = "headers")


def verifyAccess(admin=True):
    """
    Verify client access
    Args
      admin:
        If True, ensure client is an admin
    """
    def verifyAccessInner(func):
        """
        Real decorator needed to pass arguments
        Args
          func:
            function being decorated
        Returns
          wrapper:
            wrapped function
        """
        @wraps(func)
        def wrapper(*args, **kwargs):  # pylint: disable=C0111
            sArgs = securityParser.parse_args()
            key   = sArgs.get("X-API-KEY")
            urn   = sArgs.get("X-ACCOUNT-URN")

            # Key
            if admin:
                if key != "admin":
                    abort(400, "Valid Admin Required")
            else:
                if key != "user":
                    abort(400, "Valid User Required")

            # URN
            if urn != 1234:
                abort(400, "Valid URN Required")

            g.user = key
            g.urn  = urn

            rval = func(*args, **kwargs)
            return rval

        return wrapper
    return verifyAccessInner


# ------------------------ DEFAULT PARSER ----------------------------------- #
defaultParser = api.parser()
defaultParser.add_argument("arg",
                           type     = int,
                           required = False,
                           default  = 5,
                           location = "headers")


# ------------------------ SECURE GET ROUTE --------------------------------- #
@api.route('/SecureGet')  # pylint: disable=C0111
class SecureGet(Resource):

    @api.expect(defaultParser)
    @verifyAccess(admin=True)
    def get(self):  # pylint: disable=R0201,C0111
        gDict = g.__dict__
        args  = defaultParser.parse_args()
        return {"gDict": gDict, "args": args}


# ------------------------ SECURE POST ROUTE -------------------------------- #
@api.route('/SecurePost')  # pylint: disable=C0111
class SecurePost(Resource):

    @verifyAccess(admin=True)
    def post(self):  # pylint: disable=R0201,C0111
        gDict = g.__dict__
        return {"gDict": gDict, "args": request.json}


# ------------------------ UNSECURE GET ROUTE ------------------------------- #
@api.route('/UnsecureGet')  # pylint: disable=C0111
class UnsecureGet(Resource):

    @api.expect(defaultParser)
    @api.doc(security=None)
    def get(self):  # pylint: disable=R0201,C0111
        args  = defaultParser.parse_args()
        return {"args": args}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    # Create Swagger Spec
    dirName  = os.path.dirname(os.path.realpath(__file__))
    fileName = "swagger.json"
    pathName = "{}/{}".format(dirName, fileName)
    indent   = 4
    with open(pathName, "w") as f:
        with app.test_request_context("/"):
            f.write(json.dumps(api.__schema__, indent=indent))

    # Run application
    app.run(debug=True, port=5001)
