#!/usr/bin/env python3
"""
NAME:
 complex.py

DESCRIPTION
 Flask-RestPlus Complex Example

BIBLIOGRAPHY
  https://github.com/noirbizarre/flask-restplus/blob/master/examples
"""

# ------------------------ IMPORTS ------------------------------------------ #
from flask import Flask
from werkzeug.contrib.fixers import ProxyFix
from zoo import api


# ------------------------ GLOBALS ------------------------------------------ #
app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)


# ------------------------ GLOBALS ------------------------------------------ #
if __name__ == "__main__":
    api.init_app(app)
    app.run(debug=True)
