OVERVIEW
===============================================================================
Flask-RESTPlus Plus Technology Documentation
- Description                        : Fkask-RESTPlus is an extension for Flask that adds support for quickly
                                       building REST APIs
                                     : It provies collection of decorators and tools to describe API and expose
                                       its documentation via Swagger
- Relevant URLS ----------------------
  * Home                             : http://flask-restplus.readthedocs.io/en/stable/
  * Github                           : https://github.com/noirbizarre/flask-restplus
- Installation -----------------------
  * pip                              : pip install flask-restplus
  * easy_install                     : easy_install flask-restplus

