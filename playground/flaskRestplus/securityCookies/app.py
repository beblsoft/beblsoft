#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Flask-RestPlus Security Cookies Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import json
import logging
import enum
from datetime import timedelta
from functools import wraps
from flask import Flask, abort, request
from flask_cors import CORS
from flask_restplus import Resource, Api, fields, marshal  # pylint: disable=E0401
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ------------------------ GLOBALS ------------------------------------------ #
app                        = Flask(__name__)
API_AUTH_COOKIE            = "__demo_auth_token"
API_AUTH_COOKIE_SUCCESS    = "APIAuthTokenSuccess"
API_AUTH_COOKIE_EXPDAYS    = 14
API_ACCOUNT_COOKIE         = "__demo_account_token"
API_ACCOUNT_COOKIE_SUCCESS = "APIAccountTokenSuccess"
API_ACCOUNT_COOKIE_EXPDAYS = 14
LOGIN_EMAIL                = "foo@bar.com"
LOGIN_PASSWORD             = "1234"
CORS(app, origins=["https://localhost:3000"], supports_credentials=True)
api                       = Api(app,
                                security       = [API_AUTH_COOKIE, API_ACCOUNT_COOKIE],
                                authorizations = {
                                    API_AUTH_COOKIE: {
                                        "type": "apiKey",
                                        "in": "cookie",
                                        "name": API_AUTH_COOKIE,
                                    },
                                    API_ACCOUNT_COOKIE: {
                                        "type": "apiKey",
                                        "in": "cookie",
                                        "name": API_ACCOUNT_COOKIE
                                    },
                                })
logger = logging.getLogger()
logger.setLevel(level = logging.NOTSET)


# ------------------------ MODELS ------------------------------------------- #
authTokenParser = api.parser()
authTokenParser.add_argument(API_AUTH_COOKIE,
                             type     = str,
                             required = False,
                             default  = None,
                             location = ["cookies"])
accountTokenParser = api.parser()
accountTokenParser.add_argument(API_ACCOUNT_COOKIE,
                                type     = str,
                                required = False,
                                default  = None,
                                location = ["cookies"])


class AuthStatusType(enum.Enum):
    """
    Authorization Status Type
    """
    LOGGED_OUT = enum.auto()
    LOGGED_IN  = enum.auto()


loginInputModel = api.model("LoginInputModel", {
    "email": fields.String(
        title       = "Login Email",
        required    = True),
    "password": fields.String(
        title       = "Login Password",
        required    = True),
})
loginOutputModel = api.model("LoginOutputModel", {
    "email": fields.String(
        title       = "Login Email",
        required    = True),
})
authStatusOutputModel = api.model("AuthStatusOutputModel", {
    "status": fields.String(
        title       = "Auth Status",
        enum        = [authStatusType.name for authStatusType in list(AuthStatusType)],
        required    = True),
})
accountOutputModel = api.model("AccountOutputModel", {
    "email": fields.String(
        title       = "Login Email",
        required    = True),
})


# ------------------------ SECURITY ----------------------------------------- #
def verifyAccessFunc():
    """
    Verify Access

    On Success:
      Return nothing

    On Failure
      Throw Error
    """
    # Auth Token
    authArgs  = authTokenParser.parse_args()
    authToken = authArgs.get(API_AUTH_COOKIE)

    if authToken != API_AUTH_COOKIE_SUCCESS:
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_TOKEN_FOR_ID)

    # Account Token
    accountArgs  = accountTokenParser.parse_args()
    accountToken = accountArgs.get(API_ACCOUNT_COOKIE)

    if accountToken != API_ACCOUNT_COOKIE_SUCCESS:
        raise BeblsoftError(code=BeblsoftErrorCode.AUTH_INVALID_TOKEN_FOR_ID)


def requestStart(verifyAccess=False):
    """
    Request Start
    """
    def requestStartInner(func):
        """
        Real Decorator
        """
        @wraps(func)
        def wrapper(*args, **kwargs):  # pylint: disable=C0111
            if verifyAccess:
                try:
                    verifyAccessFunc()
                except BeblsoftError as _:
                    abort(401, "Invalid credentials")

            # Execute Wrapped Function
            rval = func(*args, **kwargs)

            # Return
            return rval
        return wrapper
    return requestStartInner


# ------------------------ ROUTES ------------------------------------------- #
@api.route('/auth/login')  # pylint: disable=C0111
class LoginResource(Resource):

    @api.expect(loginInputModel)
    def post(self):  # pylint: disable=R0201
        """
        Login to server
        On Success: Set Login Cookies
        """
        inData      = request.json
        email       = inData.get("email")
        badEmail    = email != LOGIN_EMAIL
        password    = inData.get("password")
        badPassowrd = password != LOGIN_PASSWORD

        if badEmail or badPassowrd:
            abort(401, "Bad Credentials")

        data = marshal({"email": email}, loginOutputModel)
        resp = api.make_response(data, code=200)
        resp.set_cookie(API_AUTH_COOKIE, API_AUTH_COOKIE_SUCCESS,
                        max_age=timedelta(days=API_AUTH_COOKIE_EXPDAYS).total_seconds(),
                        httponly=True, samesite="Strict", secure=True)
        resp.set_cookie(API_ACCOUNT_COOKIE, API_ACCOUNT_COOKIE_SUCCESS,
                        max_age=timedelta(days=API_ACCOUNT_COOKIE_EXPDAYS).total_seconds(),
                        httponly=True, samesite="Strict", secure=True)
        return resp


@api.route('/auth/logout')  # pylint: disable=C0111
class LogoutResource(Resource):

    def post(self):  # pylint: disable=R0201
        """
        Logout of Server
        Clear Login Cookies
        """
        resp = api.make_response({}, code=200)
        resp.set_cookie(API_AUTH_COOKIE, "", max_age=0)
        resp.set_cookie(API_ACCOUNT_COOKIE, "", max_age=0)
        return resp


@api.route('/auth/status')  # pylint: disable=C0111
class StatusResource(Resource):

    @api.marshal_with(authStatusOutputModel)
    def post(self):  # pylint: disable=R0201
        """
        Return Auth Status
        """
        status = ""
        try:
            verifyAccessFunc()
        except BeblsoftError as _:
            status = AuthStatusType.LOGGED_OUT.name
        else:
            status = AuthStatusType.LOGGED_IN.name

        return {"status": status}


@api.route('/account')  # pylint: disable=C0111
class AccountResource(Resource):

    @requestStart(verifyAccess=True)
    @api.marshal_with(accountOutputModel)
    def get(self):  # pylint: disable=R0201
        """
        Return Account
        """
        return {"email": LOGIN_EMAIL}


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    # Create Swagger Spec
    dirName  = os.path.dirname(os.path.realpath(__file__))
    fileName = "swagger.json"
    pathName = "{}/{}".format(dirName, fileName)
    indent   = 4
    with open(pathName, "w") as f:
        with app.test_request_context("/"):
            f.write(json.dumps(api.__schema__, indent=indent))

    # Run application
    app.run(ssl_context="adhoc", debug=True, port=5001)
