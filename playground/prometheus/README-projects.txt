OVERVIEW
===============================================================================
Prometheus Sample Projects Documentation



PROJECT: HELLO WORLD
===============================================================================
- Description                       : Hello world prometheus program
- Directory                         : ./helloWorld
- To run
  * Run                             : prometheus --config.file=promethus.yml
  * View prometheus metrics         : Open Chrome http://localhost:9090/metrics
  * View graphs                     : Open Chrome http://localhost:9090
