OVERVIEW
===============================================================================
Prometheus Technology Documentation
- Description                       : Prometheus is an open-source systems monitoring and alerting
                                      toolkit originally built at SoundCloud
                                    : Prometheus joined the Cloud Native Computing Foundation in 2016
                                      to clarify the project's governance structure
- Features --------------------------
  * Multi-dimensional data model    : Time series data identified by metric name and key/value pairs
  * PromQL                          : Flexible query language to leverage dimensionality
  * No distributed storage
  * HTTP Pull Model                 : Mechanism to collect time series data
  * Target discovery                : Targets are discovered via service discovery or static configuration
  * Multiple modes of graphing
- Installation ----------------------
  * Install (see prometheus.sh)     : prometheus_install
  * Uninstall (see prometheus.sh)   : prometheus_uninstall
  * Stop                            : prometheus_stop
  * Start (Default Config)          : prometheus_start
- Relevant URLS ---------------------
  * Home                            : https://prometheus.io/
  * Documentation                   : https://prometheus.io/docs/introduction/overview/
  * Github                          : https://github.com/prometheus/prometheus



ARCHITECTURE
===============================================================================
- Components ------------------------
  * Prometheus server               : Scrapes and stores time series data
  * Client libraries                : To connect long-lived applications
  * Push Gateway                    : Supports short-lived jobs
  * Special purpose exporters       : For services like HAProxy, StatsD, Graphite, etc
  * Alertmanager                    : Handles alers
  * Support Tools

- Picture --------------------------: Prometheus scrapes metrics from instrumented jobs, either directly
                                      or via an intermediary push gateway for short-lived jobs
                                    : It stores all scraped samples locally and runs rules over this data to either
                                      aggregate and record new time series from existing or generate alerts
                                    : Grafana or other API consumers can be used to visualize the collected data

    Short-Lived Jobs    Service Discovery                                PagerDuty   Email    Other
       |                  DNS                                                 |       |        |
       v                  Kubernetes                                          ------------------
    Pushgateway           Custom                                                      |
       |                    |                                                  Alert Manager
       |                    |                                                         ^
       |                    |            -----------------------------------          |
       v                    v            |        Prometheus Server        |-----------
       |                    |            |                                 |
       .------->----Promethus Scraping --| Retrieval -> Storage ->- PromQL |-----------
       |                    |            |              HDD/SSD            |          |
       ^                    ^            |                                 |          |
       |                    |            -----------------------------------          v
   Jobs/Exporters          Prometheus                                          -----------------
                           Server                                              |      |        |
                                                                             WebUI   Grafana  API Clients

- When does it fit? ----------------: Prometheus works well for recording any purely numeric time series
                                    : Fits machine-centric monitoring as well as monitoring dynamic
                                      service-oriented architectures
                                    : Designed for reliability, to be the system during an outage to quickly
                                      diagnose problems
                                    : Each server is standalone, not depending on network storage or other
                                      remote services
- When does it not fit? ------------: Does not fit use case where 100% accuracy is needed.
                                      Ex. per-request billing



GLOSSARY
===============================================================================
- Alert                             : Outcome of an alerting rule that is actively firing
                                      Sent from Prometheus to Alertmanager
- Alertmanager                      : Takes in alerts, aggregates them into groups, de-duplicates, applies
                                      silences, throttles, and then sends out notifications to email,
                                      Pagerduty, Slack, etc
- Bridge                            : Component that takes samples from a client library and exposes them
                                      to a non-Prometheus monitoring system
                                      Ex. Python clients that export metrics to Graphite
- CLient library                    : Library in some language (e.g. Go, Java, Python, Ruby) that makes it
                                      easy to directly instrument your code, write custom collectors to pull
                                      metrics from other systems and expose metrics to Prometheus
- Collector                         : Part of an exporter that represents a set of metrics
- Direct Instrumentation            : Instrumentation added inline as part of the source code of a program
- Endpoint                          : Source of metrics that can be scraped, usually corresponding to a
                                      single process
- Exporter                          : Binary that exposes prometheus metrics, commonly by converting metrics
                                      that are exposed in a non-Prometheus format into a format that Prometheus
                                      supports
- Instance                          : Label that uniquely identifies a target in a job
- Job                               : Collection of targets with the same purpose, for example monitofing a
                                      group of like processes replicated for scalability or reliability, is
                                      called a job
- Notification                      : A group of one or more alerts, and is set by the Alertmanager to email,
                                      Pagerduty, Slack, etc
- Promdash                          : Was a native dashboard builder for Prometheus. It has been deprecated
                                      and replaced by Grafana
- Prometheus                        : Usually refers to the core binary of the Prometheus system. It may
                                      also refer to the Prometheus monitoring system as a whole
- PromQL                            : Prometheus Query Language. Allows for a wide range of operations including
                                      aggegation, slicing and dicing, prediction and joins
- Pushgateway                       : Persists the most recent push of metics from batch jobs. This allows
                                      Prometheus to scrape their metrics after they have terminated
- Remote read                       : Prometheus feature that allows transparent reading of time series from
                                      other systems (such as long term storage) as part of queries
- Remote read adapter               : Not all systems directly support remote read. A remote read adapter
                                      sits between Prometheus and another system, converting time series requests
                                      and responses
- Remote read endpoint              : What Prometheus talks to when doing a remote read
- Remote write                      : Prometheus feature that allows sending ingested samples on the fly to other
                                      systems, such as long term storage
- Remote write adapter              : Not all systems directly support remote write. A remote write adapter
                                      sits between Prometheus and another system, converting time series requests
                                      and responses
- Remote write endpoint             : Endpoint Prometheus talks to when doing a remote write
- Sample                            : Single value at a point in time in a time series
                                      In Prometheus, each sample consists of a float64 value and a millisecond-
                                      precision timestamp
- Silence                           : Silence in the alertmanager prevents alerts, with labels matching the
                                      silence, from being included in notifications
- Target                            : Definition of an object to scrape. For example, what labels apply,
                                      any authentication required to connect, or other information that defines
                                      how the scrape will occur

