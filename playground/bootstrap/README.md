# Bootstrap Technology Documentation

Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your
ideas or build your entire app with their Sass variables and mixins, responsive grid system,
extensive prebuilt components, and powerful plugins built on jQuery.

Relevant URLs:
[Home](https://getbootstrap.com/),
[Wikipedia](),
[Docs](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

## Starter Template

Below is a simple HTML template to start using Bootstrap quickly.

```html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
```

Notes:
- __HTML5 doctype__:

  Bootstrap requires the use of HTML5 doctype. Without it, you'll see some funky incomplete styling

  ```html
  <!doctype html>
  <html lang="en">
    ...
  </html>
  ```

- __Responsive meta tag__:

  Boostrap is developed _mobile first_, a strategy in which we optimize code for mobile devices first
  and then scale up components as necessary using CSS media queries. To ensure proper rendering and
  touch zooming for all devices, add the responsive viewport meta data to your `<head>`

  ```html
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  ```

## Download

Download Bootstrap to get the compiled CSS and JavaScript, source code, or include it with your favority
package managers like npm, RubyGems, and more.

### Compiled CSS and JS

Download ready-to-use compiled code for Bootstrap which includes
- Compiled and minified CSS bundles
- Compiled and minified JavaScript plugins

[Download](https://github.com/twbs/bootstrap/releases/download/v4.3.1/bootstrap-4.3.1-dist.zip)

### Source files

Compile Boostrap with your own asset pipeline by downloading their Sass, JavaScript, and documentation
files.

[Download](https://github.com/twbs/bootstrap/archive/v4.3.1.zip)

### Bootstrap CDN

Useful when you only need to include Bootstrap's compiled CSS or JS.

CSS only:
```html
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
```

JS, Popper.js, and jQuery
```html
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
```

### NPM

When you need to include Bootstrap's source Sass and JavaScript files, use npm.

```bash
# Install via NPM
npm install bootstrap
```

### RubyGems

```bash
gem install bootstrap -v 4.3.1
```

## Contents

### Precompiled

Once downloaded, unzip the compressed folder and you'll see something like this:
```bash
bootstrap/
├── css/
│   ├── bootstrap-grid.css
│   ├── bootstrap-grid.css.map
│   ├── bootstrap-grid.min.css
│   ├── bootstrap-grid.min.css.map
│   ├── bootstrap-reboot.css
│   ├── bootstrap-reboot.css.map
│   ├── bootstrap-reboot.min.css
│   ├── bootstrap-reboot.min.css.map
│   ├── bootstrap.css                   - Compiled CSS
│   ├── bootstrap.css.map               - Source Map
│   ├── bootstrap.min.css               - Minified
│   └── bootstrap.min.css.map           - Minified Source Map
└── js/
    ├── bootstrap.bundle.js
    ├── bootstrap.bundle.js.map
    ├── bootstrap.bundle.min.js
    ├── bootstrap.bundle.min.js.map
    ├── bootstrap.js
    ├── bootstrap.js.map
    ├── bootstrap.min.js
    └── bootstrap.min.js.map
```

This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any
web project.

### Source Code

The Bootstrap source code download includes the precompiled CSS and JavaScript assets, along with the
source Sass, JavaScript, and documentation. More specifically, it includes the following and more:
```bash
bootstrap/
├── dist/
│   ├── css/
│   └── js/
├── site/
│   └──docs/
│      └── 4.3/
│          └── examples/
├── js/                    - Source JavaScript
└── scss/                  - Source Sass
```

## Browsers and Devices

### Supported Browsers

Bootstrap supports the latest, stable releases of all major browsers and platforms.

You can find the range of supported browsers and their versions in the `.browserslistrc` file:
```text
>= 1%
last 1 major version
not dead
Chrome >= 45
Firefox >= 38
Edge >= 12
Explorer >= 10
iOS >= 9
Safari >= 9
Android >= 4.4
Opera >= 30
```

### Supported Mobile Devices

Generally speaking, Bootstrap supports the latest versions of each major platform's default browsers.

|                   | Chrome    | Firefox   | Safari    | Android Browser & WebView | Microsoft Edge |
|:------------------|:----------|:----------|:----------|:--------------------------|:---------------|
| Android           | Supported | Supported | N/A       | Android v5.0+ supported   | Supported      |
| iOS               | Supported | Supported | Supported | N/A                       | Supported      |
| Windows 10 Mobile | N/A       | N/A       | N/A       | N/A                       | Supported      |

### Desktop Browsers

|         | Chrome    | Firefox   | Internet Explorer | Microsoft Edge | Opera     | Safari        |
|:--------|:----------|:----------|:------------------|:---------------|:----------|:--------------|
| Mac     | Supported | Supported | N/A               | N/A            | Supported | Supported     |
| Windows | Supported | Supported | Supported, IE10+  | Supported      | Supported | Not supported |

## Theming

Theming is accomplished by Sass variables, Sass maps, and custom CSS. There's no more dedicated
theme stylesheet; instead, you can enable the built-in theme to add gradients, shadows, and more.

### Sass Imports

Utilize bootstrap's Sass files to take advantage of variables, maps, mixins, and more.

File structure:
```bash
your-project/
├── scss
│   └── custom.scss
└── node_modules/
    └── bootstrap
        ├── js
        └── scss
```

In your `custom.scss`, you'll import Bootstrap's source Sass files. You have two options:

1. Include all of Bootstrap

  ```scss
  @import "../node_modules/bootstrap/scss/bootstrap";
  ```

2. Pick the parts that you need

  ```scss
  // Required
  @import "../node_modules/bootstrap/scss/functions";
  @import "../node_modules/bootstrap/scss/variables";
  @import "../node_modules/bootstrap/scss/mixins";

  // Optional
  @import "../node_modules/bootstrap/scss/reboot";
  @import "../node_modules/bootstrap/scss/type";
  @import "../node_modules/bootstrap/scss/images";
  @import "../node_modules/bootstrap/scss/code";
  @import "../node_modules/bootstrap/scss/grid";
  ```

### Sass Variable Defaults

Every Sass variable in Bootstrap 4 includes the `!default` flag allowing you to override the variable's
default value in your own Sass without modifying Bootstrap's source code. Copy and paste variables
as needed, modify their values, and remove the `!default` flag. If a variable has already been assigned,
then it won't be re-assigned by the default values in Bootstrap.

For a complete list of Bootstrap's variables see `scss/_variables.scss`.

Overrides can be does as follows:
```scss
// Your variable overrides
$body-bg: #000;
$body-color: #111;

// Bootstrap and its default variables
@import "../node_modules/bootstrap/scss/bootstrap";
```

### Sass Functions

Bootstrap utilizes several Sass functions, but only a subset are applicable to general theming.
```scss
@function color($key: "blue") {
  @return map-get($colors, $key);
}

@function theme-color($key: "primary") {
  @return map-get($theme-colors, $key);
}

@function gray($key: "100") {
  @return map-get($grays, $key);
}
```

These allow you to pick one color from a Sass map.

```scss
.custom-element {
  color: gray("100");
  background-color: theme-color("dark");
}
```

### Sass Options

Customize Bootstrap 4 with Bootstraps' built-in custom variables file and easily toggle global
CSS preferences with the new `$enable-*` Sass variables. These variables are in Bootstrap's
`scss/_variables.scss` file:

| Variable                                   | Values                           | Description                                                                                                                                               |
|:-------------------------------------------|:---------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------|
| $spacer                                    | 1rem (default), or any value > 0 | Specifies the default spacer value to programmatically generate our spacer utilities.                                                                     |
| $enable-rounded                            | true (default) or false          | Enables predefined border-radius styles on various components.                                                                                            |
| $enable-shadows                            | true or false (default)          | Enables predefined box-shadow styles on various components.                                                                                               |
| $enable-gradients                          | true or false (default)          | Enables predefined gradients via background-image styles on various components.                                                                           |
| $enable-transitions                        | true (default) or false          | Enables predefined transitions on various components.                                                                                                     |
| $enable-prefers-reduced-motion-media-query | true (default) or false          | Enables the prefers-reduced-motion media query, which suppresses certain animations/transitions based on the users’ browser/operating system preferences. |
| $enable-hover-media-query                  | true or false (default)          | Deprecated                                                                                                                                                |
| $enable-grid-classes                       | true (default) or false          | Enables the generation of CSS classes for the grid system (e.g., .container, .row, .col-md-1, etc.).                                                      |
| $enable-caret                              | true (default) or false          | Enables pseudo element caret on .dropdown-toggle.                                                                                                         |
| $enable-pointer-cursor-for-buttons         | true (default) or false          | Add “hand” cursor to non-disabled button elements.                                                                                                        |
| $enable-print-styles                       | true (default) or false          | Enables styles for optimizing printing.                                                                                                                   |
| $enable-responsive-font-sizes              | true or false (default)          | Enables responsive font sizes.                                                                                                                            |
| $enable-validation-icons                   | true (default) or false          | Enables background-image icons within textual inputs and some custom forms for validation states.                                                         |
| $enable-deprecation-messages               | true or false (default)          | Set to true to show warnings when using any of the deprecated mixins and functions that are planned to be removed in v5.                                  |

### Sass Maps

Many of Bootstrap's components and utilities are built with `@each` loops that iterate over a Sass map.
This is especially helpful for generating variants of a component by the `theme-colors` and creating
responsive variants for each breakpoint.

For instance generating alerts at different colors:
```scss
// Generate alert modifier classes
@each $color, $value in $theme-colors {
  .alert-#{$color} {
    @include alert-variant(theme-color-level($color, -10), theme-color-level($color, -9), theme-color-level($color, 6));
  }
}
```

Generating responsive breakpoints:
```scss
@each $breakpoint in map-keys($grid-breakpoints) {
  @include media-breakpoint-up($breakpoint) {
    $infix: breakpoint-infix($breakpoint, $grid-breakpoints);

    .text#{$infix}-left   { text-align: left !important; }
    .text#{$infix}-right  { text-align: right !important; }
    .text#{$infix}-center { text-align: center !important; }
  }
}
```

### CSS Variables

Bootstrap 4 includes around two dozen CSS custom properties (variables) in its compiled CSS.
These provide easy access to commonly used values like theme colors, breakpoints, and primary
font stacks.

Available variables (see `_root.scss`):

```css
:root {
  --blue: #007bff;
  --indigo: #6610f2;
  --purple: #6f42c1;
  --pink: #e83e8c;
  --red: #dc3545;
  --orange: #fd7e14;
  --yellow: #ffc107;
  --green: #28a745;
  --teal: #20c997;
  --cyan: #17a2b8;
  --white: #fff;
  --gray: #6c757d;
  --gray-dark: #343a40;
  --primary: #007bff;
  --secondary: #6c757d;
  --success: #28a745;
  --info: #17a2b8;
  --warning: #ffc107;
  --danger: #dc3545;
  --light: #f8f9fa;
  --dark: #343a40;
  --breakpoint-xs: 0;
  --breakpoint-sm: 576px;
  --breakpoint-md: 768px;
  --breakpoint-lg: 992px;
  --breakpoint-xl: 1200px;
  --font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
}
```

Body font can then be set as follows:
```css
body {
  font: 1rem/1.5 var(--font-family-sans-serif);
}
```

## Accessibility

Bootstrap provides an easy-to-use framework of ready-made styles, layout tools, and interactive
components, allowing developers to create websites and applications that are visually appealing,
functionally rich, and accessible out of the box.

Standards:

- [Web Content Accessibility Guidelines (WCAG) 2.0](https://www.w3.org/TR/WCAG20/)
- [WAI-ARIA](https://www.w3.org/WAI/standards-guidelines/aria/)
- [Color Contrast](https://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html)

Content which should be visually hidden, but remain accessible to assistive technologies such as
screen readers, can be styled using the `.sr-only` class. For example:
```html
<p class="text-danger">
  <span class="sr-only">Danger: </span>
  This action is not reversible
</p>
```

For visually hidden interactive controls, such as traditional "skip" links, `.sr-only` can be combined
with `.sr-only-focusable` class. This ensures that the control becomes visible once focues.
```html
<a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>
```

Bootstrap includes support for the `prefers-reduced-motion` media feature. In browser/environments
that allow the user to specify their preference for reduced motion.

## Layout

### Containers

Containers are the most basic layout element in Bootstrap and are __required when using the default
grid system__. Container types.
- __.container__: Responsive, fixed-width container. It's max-width changes at each breakpoint
  ```html
  <div class="container">
  <!-- Content here -->
  </div>

- __.container-fluid__: Fluid width container with width 100% of the time
  ```html
  <div class="container-fluid">
  ...
  </div>
  ```

### Responsive Breakpoints

Since Boostrap is developed to be mobile first, it uses a handful of media queries to creae sensible
breakpoints for layouts and interfaces. These breakpoints are mostly based on minimum viewport
widths and allow for scaling elements as the viewport changes.

```scss
// Extra small devices (portrait phones, less than 576px)
// No media query for `xs` since this is the default in Bootstrap

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) { ... }

// Medium devices (tablets, 768px and up)
@media (min-width: 768px) { ... }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) { ... }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { ... }
```

Since all source CSS is in Sass, all media queries are available via Sass mixins:
```scss
// No media query necessary for xs breakpoint as it's effectively `@media (min-width: 0) { ... }`
@include media-breakpoint-up(sm) { ... }
@include media-breakpoint-up(md) { ... }
@include media-breakpoint-up(lg) { ... }
@include media-breakpoint-up(xl) { ... }

// Example: Hide starting at `min-width: 0`, and then show at the `sm` breakpoint
.custom-class {
  display: none;
}
@include media-breakpoint-up(sm) {
  .custom-class {
    display: block;
  }
}
```

Alternatively, one can use media queries that go in the other direction (the given screen size
or _smaller_):
```scss
// Extra small devices (portrait phones, less than 576px)
@media (max-width: 575.98px) { ... }

// Small devices (landscape phones, less than 768px)
@media (max-width: 767.98px) { ... }

// Medium devices (tablets, less than 992px)
@media (max-width: 991.98px) { ... }

// Large devices (desktops, less than 1200px)
@media (max-width: 1199.98px) { ... }

// Extra large devices (large desktops)
// No media query since the extra-large breakpoint has no upper bound on its width
```

Via Sass mixins:
```scss
@include media-breakpoint-down(xs) { ... }
@include media-breakpoint-down(sm) { ... }
@include media-breakpoint-down(md) { ... }
@include media-breakpoint-down(lg) { ... }
// No media query necessary for xl breakpoint as it has no upper bound on its width

// Example: Style from medium breakpoint and down
@include media-breakpoint-down(md) {
  .custom-class {
    display: block;
  }
}
```

There are also media queries for targeting a single segment of screen sizes using the minumum
and maximum breakpoints.
```scss
// Extra small devices (portrait phones, less than 576px)
@media (max-width: 575.98px) { ... }

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) and (max-width: 767.98px) { ... }

// Medium devices (tablets, 768px and up)
@media (min-width: 768px) and (max-width: 991.98px) { ... }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) and (max-width: 1199.98px) { ... }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { ... }
```

Via Sass mixins:
```scss
@include media-breakpoint-only(xs) { ... }
@include media-breakpoint-only(sm) { ... }
@include media-breakpoint-only(md) { ... }
@include media-breakpoint-only(lg) { ... }
@include media-breakpoint-only(xl) { ... }
```

Media queries that span multiple breakpoint widths:
```scss
// Example
// Apply styles starting from medium devices and up to extra large devices
@media (min-width: 768px) and (max-width: 1199.98px) { ... }
```

Via Sass mixins:
```scss
@include media-breakpoint-between(md, xl) { ... }
```

### Z-index

Several Bootstrap components utilize `z-index`, the CSS property that helps control layout by providing
a third axis to arrange content. Bootstrap utilizes a default z-index scale that's been designed to
properly layer several UI components.

These higher values start an an arbitrary number, high and specifici enough to ideally avoid conflicts.
Customization is encouraged.
```scss
$zindex-dropdown:          1000 !default;
$zindex-sticky:            1020 !default;
$zindex-fixed:             1030 !default;
$zindex-modal-backdrop:    1040 !default;
$zindex-modal:             1050 !default;
$zindex-popover:           1060 !default;
$zindex-tooltip:           1070 !default;
```

To handle overlapping borders within components (e.g. buttons and inputs in input grouns), Bootstrap
uses the low single digit `z-index` values of `1`, `2`, and `3` for default hover and active states.

## Grid

Use Bootstrap's powerful mobile-first flexbox grid to build layouts of all shapes and sizes thanks to a
twelve column system, five default responsive tiers, Sass variables and mixins, and dozens of predefined
classes.

For examples, see [grid.html](./examples/grid.html)

### How it works

Bootstrap's grid system uses a series of containers, rows, and columns to layout and align content.
It's built with flexbox and is fully responsive.

Example:
```html
<div class="container">
  <div class="row">
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
  </div>
</div>
```

### Sass Variables

Variables and maps determine the number of columns, the gutter width, and the media query point
at which to begin floating columns.

```scss
$grid-columns:      12;
$grid-gutter-width: 30px;

$grid-breakpoints: (
  // Extra small screen / phone
  xs: 0,
  // Small screen / phone
  sm: 576px,
  // Medium screen / tablet
  md: 768px,
  // Large screen / desktop
  lg: 992px,
  // Extra large screen / wide desktop
  xl: 1200px
);

$container-max-widths: (
  sm: 540px,
  md: 720px,
  lg: 960px,
  xl: 1140px
);
```

### Sass Mixins

Mixins are used in conjunction with the grid variables to generate semantic CSS for individual
grid columns.

```scss
// Creates a wrapper for a series of columns
@include make-row();

// Make the element grid-ready (applying everything but the width)
@include make-col-ready();
@include make-col($size, $columns: $grid-columns);

// Get fancy by offsetting, or changing the sort order
@include make-col-offset($size, $columns: $grid-columns);
```

Example usage:
```scss
.example-container {
  width: 800px;
  @include make-container();
}

.example-row {
  @include make-row();
}

.example-content-main {
  @include make-col-ready();

  @include media-breakpoint-up(sm) {
    @include make-col(6);
  }
  @include media-breakpoint-up(lg) {
    @include make-col(8);
  }
}

.example-content-secondary {
  @include make-col-ready();

  @include media-breakpoint-up(sm) {
    @include make-col(6);
  }
  @include media-breakpoint-up(lg) {
    @include make-col(4);
  }
}
```

## Reboot

Reboot, a collection of element-specific CSS changes in a single file, kickstart Bootstrap to provide
an elegant, consistent, and simple baseline to build upon.

Reboot builds upon Normalize, providing many HTML elements with somewhat opinionated styles using
using only element selectors. Additional styling is done only with classes.

### Page Defaults

The `<html>` and `<bod>` elements are updated to probide better page-wide defaults. More specifically:
- __box-sizing__

  The `box-sizing` is globally set on every element - including `*::before` and `*::after`, to `border-box`.
  This ensures that the declared width of element is never exceeded due to padding or border

- __font-size__

  No base `font-size` is declared on the `<html>`, but `16px` is assumed (the browser default).
  `font-size: 1rem` is applied on the `<body>` for easy responsive type-scaling via media queries
  while respecing user preferences

- __body fonts__

  The `<body>` also sets a global `font-family`, `line-height`, and `text-align`
  This is inherited later by some form elements to prevent font inconsistencies

### Native font stack

The default web font stack is shown below:

```scss
$font-family-sans-serif:
  // Safari for macOS and iOS (San Francisco)
  -apple-system,
  // Chrome < 56 for macOS (San Francisco)
  BlinkMacSystemFont,
  // Windows
  "Segoe UI",
  // Android
  "Roboto",
  // Basic web fallback
  "Helvetica Neue", Arial, sans-serif,
  // Emoji fonts
  "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !default;
```

This `font-family` is applied to the `<body>` and automatically inherited globall throughout
Bootstrap. To switch the `font-family`, update the `$font-family-base` and recompile.

### Headings and paragraphs

Heading elements e.g. `<h1>` are reset to have their `margin-top` removed and have `margin-bottom: .5rem;`
added.

Paragraph elements e.g. `<p>` are reset to have their `margin-top` removed and have `margin-bottom: 1rem;`
added.

### Lists

All lists - `<ul>`, `<ol>`, and `<dl>` - have their `margin-top` removed and a `margin-bottom: 1rem`.
Nested lists have no `margin-bottom`.

## Typography

Typography is the style and appearance of printed matter.

For examples, see [typography.html](./examples/typography.html)

### Responsive Font Sizes

Bootstrap v4.3 ships with the option to enable responsive font sizes (RFS), allowing text to scale more
naturally across device and viewport sizes. RFS can be enabled by changing the
`$enable-responsive-font-sizes` Sass variable to `true` abd recompiling. See the RFS
[Github Repository](https://github.com/twbs/rfs) for more information.

## Code

Bootstrap supports displaying code in html.

For examples, see [code.html](./examples/code.html)

## Images

Bootstrap contains several responsive and lightweight image classes.

For examples, see [images.html](./examples/images.html)

## Tables

Bootstrap contains several table classes.

For examples, see [tables.html](./examples/tables.html)

Create responsive tables by wrapping any `.table` with `.table-responsive{-sm|-md|-lg|-xl}`, making
the table scroll horizontally at each `max-width` breakpoint.

## Figures

Bootstrap contains classes for showing related images and text inside a figure.

For examples, see [figures.html](./examples/figures.html)

## Components

### Alerts

Alerts are used to provide contextual feedback messages for typical user actions.

For examples, see [alerts.html](./examples/alerts.html)

### Badge

A badge is a small count and labeling component.

For examples, see [badge.html](./examples/badge.html)

### Breadcrumb

Breadcrumbs indicate the current page's location within a navigational hierarchy that automatically
adds separators via CSS.

For examples, see [breadcrumb.html](./examples/breadcrumb.html)

### Buttons

Bootstrap has custom button styes of actions in forms, dialogs, and more.

For examples, see [buttons.html](./examples.buttons.html)

### Button Groups

Button groups group a series of buttons together on a single line.

For examples, see [buttonGroups.html](./examples/buttonGroups.html)

### Cards

Cards provide a flexible and extensible content container with multiple variants and options.

For examples, see [cards.html](./examples/cards.html)

### Carousel

Carousel is a slideshow component for cycling through elements.

For examples, see [carousel.html](./examples/carousel.html)

### Collapse

Collapse allows fisibility of content to be toggled.

For examples, see [collapse.html](./examples/collapse.html)

### Dropdowns

Dropdowns toggle the visibility of lists of links.

For examples, see [dropdown.html](./examples/dropdown.html)

### Forms

Forms are groupings of user input fields.

For examples, see [forms.html](./examples/forms.html)

### Input Group

Input Groups allow form controls to be extended by adding text, buttons, or button groups.

For examples, see [inputGroup.html](./examples/inputGroup.html)

### Jumbotron

The Jumbotron is a lightweight, flexible component for showcasing hero unit style content.

For examples, see [jumbotron.html](./examples/jumbotron.html)

### List Group

List groups are a flexible component for displaying a series of content.

For examples, see [listGroup.html](./examples/listGroup.html)

### Media Object

Use media objects to contstruct highly repetitive components like blog comments, tweets, or the like.

For examples, see [mediaObject.html](./examples/mediaObject.html)

### Modal

A modal is a lightweight dialog for user notifications.

For examples, see [modal.html](./examples/modal.html)

### Navs

Navigation components assist in navigating around website.

For examples, see [navs.html](./examples/navs.html)

### NavBar

Bootstrap contains a responsive navigation header, the navbar. It includes support for branding,
navigation, collapsability, and more.

For examples, see [navbar.html](./examples/navbar.html)

### Pagination

Pagination allows pages to indicate a series of related content across multiple pages.

For examples, see [pagination.html](./examples/pagination.html)

### Popovers

Popovers display content when clicked upon.

For examples, see [popovers.html](./examples/popovers.html)

### Progress

Progress bars display the status of an operation.

For examples, see [progress.html](./examples/progress.html)

### ScrollSpy

ScrollSpy automatically updates navigation or list group components based on scroll position to indicate
which link is currently active in the viewport.

For examples, see [scrollspy.html](./examples/scrollspy.html)

### Spinners

Spinners indicate that a page is loading.

For examples, see [spinners.html](./examples/spinners.html)

### Toasts

Push notifications to visitors witha toast, a lightweight and easily customizable alert message.

For examples, see [toasts.html](./examples/toasts.html)

### Tooltips

Tooltips become visible after hovering over a button.

For examples, see [tooltips.html](./examples/tooltips.html)

## Utilities

### Borders

Use border utilities to quickly style the border and border-radius of an element. Great for images,
buttons, or any other element.

For examples, see [borders.html](./examples/borders.html)

### Clearfix

Quickly and easily clear floated content within a container by adding a clearfix utility.

For examples, see [clearfix.html](./examples/clearfix.html)

### Close Icon

Use a generic close icon for dismissing content like modals and alerts.

### Colors

Convey meaning through color with a handful of color utility classes.

For examples, see [colors.html](./examples/colors.html)

### Display

Quickly and responsively toggle the display value of components and more with display utilites.

Display utility classes are named using the formats:

- `.d-{value}` for all sizes (`xs` and up)
- `.d-{breakpoint}-{value}` for `sm`, `md`, `lg`, and `xl`

Where _value_ is one of:

- `none`
- `inline`
- `inline-block`
- `block`
- `table`
- `table-cell`
- `table-row`
- `flex`
- `inline-flex`

For examples, see [display.html](./examples/display.html)

### Embed

Create responsive video or slideshow embeds based on the width of the parent by creating an intrinsic
ration that scales on any device.

For examples, see [embed.html](./examples/embed.html)

### Flex

Quickly manage the layout, alignment, and sizing of grid columns, navigation, components, and more
with a full suite of responsive flexbox utilities.

For examples, see [flex.html](./examples/flex.html)

### Float

Use floats to toggle floats on any element, across any breakpoint.

For examples, see [float.html](./examples/float.html)

### Image Replacement

Swap text for background images with the image replacement class.

For examples, see [imageReplacement.html](./examples/imageReplacement.html)

### Overflow

Overflow utilities to configure how content overflows its parent element.

For examples, see [overflow.html](./examples/overflow.html)

### Position

Quickly configure the position of an element.

For examples, see [position.html](./examples/position.html)

### Screen Readers

Use screen reader utilities to hide elements on all devices except screen readers.

For examples, see [screenReader.html](./examples/screenReader.html)

### Shadows

Add or remove shadows to elements with box-shadow utilities.

For examples, see [shadow.html](./examples/shadow.html)

### Sizing

Easily make an element as wide or as tall with width and height utilities.

For examples, see [sizing.html](./examples/sizing.html)

### Spacing

Bootstrap includes a wide range of shorthand responsive margin and padding utility classes to modify
an element's appearance.

Spacing utility classes are named using the formats:

- `{property}{sides}-{size}` for all sizes (`xs` and up)
- `{property}{sides}-{breakpoint}-{size}` for `sm`, `md`, `lg`, and `xl`

Where _property_ is one of:

- `m`: Margin
- `p`: Padding

Where _sides_ is one of:

- `t`: top
- `b`: bottom
- `l`: left
- `r`: right
- `x`: left and right
- `y`: top and bottom
- blank: all 4 sides

Where _size_ is one of:

- `0`: set to 0
- `1`: `$spacer * .25`
- `2`: `$spacer * .5`
- `3`: `$spacer`
- `4`: `$spacer * 1.5`
- `5`: `$spacer * 3`
- `auto`: set auto margin

For examples, see [.html](./examples.html)

### Stretched Link

Make any HTML element or Bootstrap component clickable by "stretching" a nested link via CSS.

For examples, see [stretcedLink.html](./examples/stretchedLink.html)

### Text

Control alignment, wrapping, weight and more.

For examples, see [text.html](./examples/text.html)

### Vertical Alignment

Easily change the vertical alignment of inline, inline-block, inline-table and table cell elements.

For examples, see [verticalAlignment.html](./examples/verticalAlignment.html)

### Visibility

Control the visibility, without modifying the display.

For examples, see [visibility.html](./examples/visibility.html)

## Extend

### Responsive

Bootstrap's styles are built to be responsive, an approach that's often referred to as _mobile-first_.

Across Bootstrap, you'll see this most clearly in media queries. In most cases, Boostrap uses
`min-width` queries that begin to apply a specific breakpoint and carry up through the higher
breakpoints.

### Classes

Aside from our Reboot, a cross-browser normalization stylesheet, all bootstrap styles aim to use
classes as selectors. This means steering clear of type selectors (e.g. `input[type="text]`) and
extraneous parent slectors (e.g. `.parent .child`) that makes styles too specific to easily override.

As such, components should be built with a base class that houses common, not-to-be overridden property-value
pairs (e.g. `btn`). Then use modifiers like `.btn-primary` to add color, background-color, border-color, etc.

### z-index scales

There are two `z-index` scales in Boostraps - elements within a component and overlay component.

Component elements:
- 0: initial(default)
- 1: `:hover`
- 2: `:active`
- 3: `:focus`

Overlay components:
- Each has a `z-index` that starts at `1000`

### HTML and CSS over JS

Whenever possible, prefer to write HTML and CSS over JavaScript.
Benefits:
- HTML and CSS are more prolific and accessible to more people of all different experience levels.
- HTML and CSS are faster in browser than JavaScript

### Utilities

Utility classes - formerly helpers in Bootstrap 3 - are a powerful ally in combatting CSS bloat
and poor page performance. A utility class is typically a single, immutable property-value pairing
expressed as a class (e.g. `d-block`). Their primary appeal is speed of use while writing HTML and
limiting the amount of custom CSS you have to write.

### Flexible HTML

While not always possible, strive to avoid being overly dogmatic in HTML requirements and components.
Thus, focus on single classes in CSS selectors and try to avoid immediate children selectors (`>`).
This gives more flexibility and keeps CSS simpler and less specific.

