#
# NAME
#  caseExpression.rb
#
# DESCRIPTION
#  Ruby Case Expression
#

# -------------------------- MAIN ------------------------------------------- #
capacity = rand(100)
print "capacity=#{capacity}\n"
case capacity
when 0
  print "You ran out of gas.\n"
when 1..20
  print "The tank is almost empty. Quickly, find a gas station!\n"
when 21..70
  print "You should be ok for now.\n"
when 71..100
  print "The tank is almost full.\n"
else
  print "Error: capacity has an invalid value (#{capacity})\n"
end