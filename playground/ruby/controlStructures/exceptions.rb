#
# NAME
#  exceptions.rb
#
# DESCRIPTION
#  Ruby Exceptions
#

# -------------------------- MAIN ------------------------------------------- #
begin
    y = 1 / 0
rescue
    print "Excpeption:"
	print $!
    print "\n"
else
    print "Success\n"
ensure
	print "I always run\n"
end