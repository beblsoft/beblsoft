#
# NAME
#  helloWorld.rb
#
# DESCRIPTION
#  Ruby Hello World Program
#


# -------------------------- CLASSES ---------------------------------------- #
class Greeter
  def initialize(name)
    @name = name.capitalize
  end

  def salute
    puts "Hello #{@name}!"
  end
end

# -------------------------- MAIN ------------------------------------------- #
g = Greeter.new("world")
g.salute