#
# NAME
#  lineBreaking.rb
#
# DESCRIPTION
#  Ruby Line Breaks
#

# -------------------------- MAIN ------------------------------------------- #
a = 10
b = a +
        10
c = [ 5, 4,
        10 ]
d = [ a ] \
        + c
print "#{a} #{b} [", c.join(" "), "] [", d.join(" "), "]\n";

