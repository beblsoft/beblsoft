# Ruby Documentation

Ruby is a dynamic, open source programming language with a focus on simplicity and productivity.
It has an elegant syntax that is natural to read and easy to write.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Ruby_(programming_language)),
[Home](https://www.ruby-lang.org/en/),
[RVM](http://rvm.io/),
[Examples](http://sandbox.mc.edu/~bennet/ruby/code/)

## Ruby Version Manager (RVM)

RVM is a command-line tool which allows you to easily install, manage, and work with multiple ruby
environments from interpreters to sets of gems.

Installation: See `ruby.sh: ruby_install`

Removal: See `ruby.sh: ruby_uninstall`
