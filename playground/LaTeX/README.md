# LaTeX Technology Documentation

[LaTeX]() is a high-quality typesetting system. It includes features designed for the production of
technical and scientific documenation. LaTeX is the de facto standard for the communication and
publication of scientific documents. It is free software.

Relevant URLs:
[Home](https://www.latex-project.org/)

## TexMaker

TexMaker is a free cross-platform LaTex editor. It runs on linux, macos, and windows systems.
It includes unicode support, spell checking, auto-completion, code folding and a built-in pdf
viewer with synctex support and continuous view mode.

Installation: `sudo apt-get install -y texmaker`

Steps to edit:
- Open a file with "*.tex" suffix. Ex. `Math/equations.tex`
- Hit the `View PDF` button to see result

Relevant URLs: [Home](http://www.xm1math.net/texmaker/)