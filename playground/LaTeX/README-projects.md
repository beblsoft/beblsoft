# LaTeX Projects Documentation

## Brackets,Lists,Tables

`brackets.tex`: Demonstrate using brackets

`lists.tex`: Demonstrate using lists

`tables.tex`: Demonstrate using tables

## Document Sectioning

`DocumentSectioning.tex`: Demonstrate various headers inside a document

## Font-Size,Position

`fontSizePosition.tex`: Demonstrate using different font types, sizes, and page positions

## Images

`images.tex`: Example displaying and rotating an image.

## Macros

`macros.tex`: Example definine and using a LaTeX macro.

## Math

`equations.tex`: Create various equations

`symbols.tex`: Use various symbols
