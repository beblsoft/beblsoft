OVERVIEW
===============================================================================
Alembic Sample Projects Documentation


PROJECT: SINGLE DB
===============================================================================
- Description                       : Use alembic to migrate and track single database changes
                                    : Leverages BeblsoftMySQLDatabase and BeblsoftMySQLDatabaseMigrations classes
                                      These classes call native alembic internal api commands
- Directory                         : ./singleDB
- Install ---------------------------
  * Install virtual env             : python3_setupVenv
  * Enter venv                      : source venv/bin/activate

- Create / Delete -------------------
  * Create initial database with
    no tables                       : ./cli.py create
  * Delete database                 : ./cli.py delete

- Describe --------------------------
  * Create initial database         : ./cli.py create
  * Describe database               : ./cli.py describe
  * Show first version              : ./cli.py describeversion --version=9760145e52bd
  * Show head version               : ./cli.py describeversion --version=head

- Upgrade / Downgrade ---------------
  * Create initial database         : ./cli.py create
  * Upgrade to first revision       : ./cli.py upgrade --version=9760145e52bd
  * Downgrade to base               : ./cli.py downgrade --version=base
  * Upgrade to latest version       : ./cli.py upgrade --version=head

- Create new revision ---------------
  * Create initial database         : ./cli.py create
  * Upgrade database up to head     : ./cli.py upgrade --version=head
  * Add new tables/fields           : sublime ./db/users.py
  * Generate new version based on
    change                          : ./cli.py revise --msg="Add newColumn4"
    Update underlying db to latest
    revision                        : ./cli.py upgrade --version=head
