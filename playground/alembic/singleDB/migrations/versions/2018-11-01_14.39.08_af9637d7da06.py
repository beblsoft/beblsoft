"""
Add new column 2

Revision ID: af9637d7da06
Revises: ff918d14ceeb
Create Date: 2018-11-01 14:39:08.548654-05:00
"""

# ----------------------------- IMPORTS ------------------------------------- #
from alembic import op
import sqlalchemy as sa



# ----------------------------- GLOBALS ------------------------------------- #
revision      = 'af9637d7da06'
down_revision = 'ff918d14ceeb'
branch_labels = None
depends_on    = None


# ----------------------------- UPGRADE ------------------------------------- #
def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('newColumn2', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


# ----------------------------- DOWNGRADE ----------------------------------- #
def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'newColumn2')
    # ### end Alembic commands ###
