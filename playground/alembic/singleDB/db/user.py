#!/usr/bin/env python
"""
NAME
 user.py

DESCRIPTION
 User Table
"""

# ----------------------------- IMPORTS ------------------------------------- #
from sqlalchemy import Column, Integer, String, Sequence
from . import base


# ------------------------- USER TABLE -------------------------------------- #
class User(base):
    """
    User Class
    Relationships
      User-Address:  One-to-Many
    Note
      Cascading deletes on addresses. When a user is deleted, all of their
      addresses will also be deleted.
    """
    __tablename__ = "users"
    id            = Column(Integer, Sequence("user_id_seq"), primary_key=True)
    name          = Column(String(50))
    fullname      = Column(String(50))
    password      = Column(String(30))
    newColumn1    = Column(Integer)
    newColumn2    = Column(Integer)
    newColumn3    = Column(Integer)

    def __repr__(self):
        return "[{} id={}, name={}, fullname={}, password={}, addresses={}]".format(
            self.__class__.__name__, self.id, self.name, self.fullname,
            self.password, self.addresses)
