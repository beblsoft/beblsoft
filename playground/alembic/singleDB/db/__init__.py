#!/usr/bin/env python3
"""
 NAME
  __init__.py

 DESCRIPTION
  Database Functionality
"""

# ----------------------------- IMPORTS ------------------------------------- #
import os
from pathlib import Path
import logging
import pprint
import uuid
from datetime import datetime
from sqlalchemy.sql.expression import func
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base
from base.bebl.python.log.bLogFunc import logFunc
from base.mysql.python.bDatabase import BeblsoftMySQLDatabase
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.error.bError import BeblsoftError
from base.bebl.python.error.bCode import BeblsoftErrorCode


# ----------------------------- GLOBALS ------------------------------------- #
logger   = logging.getLogger(__name__)
metaData = MetaData(naming_convention = {  # Constraint naming for migration
    # Index
    "ix": "ix_%(column_0_label)s",
    # Unique Constraint
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    # Check
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    # Foreign Key
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    # Primary Key
    "pk": "pk_%(table_name)s"
})
base     = declarative_base(metadata=metaData)


# ----------------------------- IMPORTS(2) ---------------------------------- #
from .user import User  # pylint: disable=C0413


# ----------------------------- DATABASE CLASS ------------------------------ #
class Database(BeblsoftMySQLDatabase):
    """
    Database Class
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize object
        """
        migrationsDir     = os.path.join(Path(__file__).parents[1], "migrations")
        alembicConfigPath = os.path.join(migrationsDir, "alembic.ini")
        super().__init__(*args, **kwargs, alembicConfigPath=alembicConfigPath)

    # --------------------- CREATE/DELETE TABLES ---------------------------- #
    @logFunc()
    def createTables(self, fromMigrations=True, checkFirst=True):
        """
        Create all database tables
        Args
          fromMigrations:
            If True, create database from migration scripts
          checkFirst:
            If True, don't issue CREATEs for tables already present
        """
        if fromMigrations:
            self.bDBMigrations.upgrade(toVersion="head")
        else:
            base.metadata.create_all(self.engine, checkfirst=checkFirst)
            self.bDBMigrations.stamp(version="head")
        base.metadata.create_all(self.engine, checkfirst=checkFirst)

    @logFunc()
    def deleteTables(self, fromMigrations=False, checkFirst=True):
        """
        Destroy all database tables
        Args
          fromMigrations:
            If True, delete database from migration scripts
          checkFirst:
            If True, don't issue DELETE's for tables not present
        """
        if fromMigrations:
            self.bDBMigrations.downgrade(toVersion="base")
        else:
            base.metadata.drop_all(self.engine, checkfirst=checkFirst)
            self.bDBMigrations.stamp(version="base")
