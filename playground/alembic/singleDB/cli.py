#!/usr/bin/env python3
"""
NAME:
 __init__.py

DESCRIPTION
 CLI Module
"""

# ------------------------ IMPORTS ------------------------------------------ #
import os
import sys
from pathlib import Path
import traceback
import logging
import getpass
import click
from base.mysql.python.bServer import BeblsoftMySQLServer
from base.bebl.python.log.bLog import BeblsoftLog
from playground.alembic.singleDB.db import Database
from playground.alembic.singleDB.db.user import User


# ----------------------- GLOBALS ------------------------------------------- #
logger             = logging.getLogger(__file__)
bLog               = BeblsoftLog(logFile="/tmp/singleDB.txt")
singleDBDir        = os.path.abspath(Path(__file__).parents[0])
alembicCfgFile     = os.path.join(singleDBDir, "migrations", "alembic.ini")
bServer            = BeblsoftMySQLServer(
    domainNameFunc     = lambda: "localhost",
    user               = getpass.getuser(),
    password           = "")
bDatabase          = Database(
    bServer            = bServer,
    name               = "AlembicSingleDB")


# ----------------------- COMMAND LINE INTERFACE ---------------------------- #
@click.group(context_settings=dict(help_option_names=["-h", "--help"]),
             options_metavar="[options]")
def cli():
    """
    Command Line Interface
    """
    pass


# ----------------------------- CRUD ---------------------------------------- #
@cli.command()
def create():
    """
    Create Database
    Note
      Tables are not created
    """
    bDatabase.create()


@cli.command()
def delete():
    """
    Delete Database
    """
    bDatabase.delete()


@cli.command()
def addUsers():
    """
    Add Users
    """
    with bDatabase.sessionScope() as s:
        s.add_all([
            User(name="darryl", fullname="Darryl Black", password="1234"),
            User(name="james", fullname="James Bensson", password="5678"),
            User(name="ed", fullname="Ed Jones", password="91011"),
            User(name="wendy", fullname="Wendy Williams", password="foobar"),
            User(name="mary", fullname="Mary Contrary", password="xxg527"),
            User(name="fred", fullname="Fred Flinstone", password="blah")])


@cli.command()
def describe():
    """
    Describe database
    """
    bDatabase.describe()


# ----------------------------- MIGRATION ----------------------------------- #
@cli.command()
@click.option("--version", default="head", help="Specify version")
def stamp(version):  # pylint: disable=C0111,W0622
    """
    Stamp database at a revision
    """
    bDatabase.bDBMigrations.history(version=version)


@cli.command()
@click.option("--version", default="head", help="Specify version")
def upgrade(version):  # pylint: disable=C0111,W0622
    """
    Upgrade database
    """
    bDatabase.bDBMigrations.upgrade(toVersion=version)


@cli.command()
@click.option("--version", default="base", help="Specify version")
def downgrade(version):  # pylint: disable=C0111,W0622
    """
    Downgrade database
    """
    bDatabase.bDBMigrations.downgrade(toVersion=version)


@cli.command()
@click.option("--msg", default="Default Revision Message", help="Specify message")
def revise(msg):  # pylint: disable=C0111
    """
    Revise database
    """
    bDatabase.bDBMigrations.revise(msg=msg)


@cli.command()
@click.option("--version", default="head", help="Specify version")
def describeversion(version):
    """
    Describe version
    """
    bDatabase.bDBMigrations.describeVersionData(version=version)


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    try:
        cli(obj={})  # pylint: disable=E1120,E1123
    except Exception as _:  # pylint: disable=W0703
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
