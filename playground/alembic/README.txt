OVERVIEW
===============================================================================
Alembic Technology Documentation
- Description                       : Alembic is a lightweight database migration tool for usage with the
                                      SQLAlchemy Database Toolkit for Python.
                                    : It provides creation, management, and invocation of change
                                      management scripts for a relational database using SQLAlchemy
                                      as the underlying engine
- Relevant URLs
  * Home, Documentation             : http://alembic.zzzcomputing.com/en/latest/
  * Bitbucket Source Code           : https://bitbucket.org/zzzeek/alembic/src/master/
  * pypi                            : https://pypi.org/project/alembic/
  * Bitbucket Issue Tracker         : https://bitbucket.org/zzzeek/alembic/issues?status=new&status=open
  * Operation Reference             : http://alembic.zzzcomputing.com/en/latest/ops.html#ops
  * Command Reference               : https://alembic.zzzcomputing.com/en/latest/api/commands.html
- Install
  * Notes                           : Alembic can be installed via pip or through source
                                      Install adds "alembic" command to environment
  * Install via pip                 : pip install alembic
  * Install via source              : python setup.py install
  * Dependencies                    : SQLAlchemy version 0.9.0
                                      Python >= 2.7, 3.4


REVISIONS, VERSIONS
===============================================================================
- Terminology
  revision                          : A version of the database
  head                              : Most recent revision
  base                              : First revision
- Stamping                          : When invoked, alembic marks the underlying
                                      database with alembic_version table
                                    : mysql> select * from alembic_version;
                                      +--------------+
                                      | version_num  |
                                      +--------------+
                                      | f8dc26897900 |
                                      +--------------+
- Example version tree:
        ----------------------------------------------------
  head  | Revision ID : f8dc26897900                       |
        | Create Date : 2018-10-30 13:42:03.090775-05:00   |
        | Comment     : Add newColumn3 to User             |
        ----------------------------------------------------
             |
             ^
             |
        ----------------------------------------------------
        | Revision ID : 0264c054754f                       |
        | Create Date : 2018-10-30 13:40:03.724828-05:00   |
        | Comment     : Add newColumn2 to User             |
        ----------------------------------------------------
             |
             ^
             |
        ----------------------------------------------------
        | Revision ID : 47176e998f42                       |
        | Create Date : 2018-10-30 13:34:59.403696-05:00   |
        | Comment     : Add newColumn1 to User             |
        ----------------------------------------------------
             |
             ^
             |
        ----------------------------------------------------
  base  | Empty Database                                   |
        ----------------------------------------------------


FILE TREE
===============================================================================
- Suggested Project Structure
  project/                          : Root of application source code
    alembic.ini                     : Alembic init files. See below
    alembic/                        : Home of migration environment
                                      Can be named anything. Can have multiple for multiple DBs
      env.py                        : Python script run whenever the migration tool is invoked
                                      Procures DB connection, then invokes migration
      README                        : Contains environment templates
      script.py.mako                : Make template file used to generated migration scripts
                                      Generates scripts in versions/
      versions/                     : Holds individual version scripts
                                      In alembic, the ordering of version scripts is relative
                                      to directives within the scripts themselves
        3512b954651e_add_account.py
        2b1ae634e5cd_add_order_id.py
        3adcc9a56557_rename_username_field.py


CLI USAGE
===============================================================================
- Create migration environment      : cd yourproject
                                      alembic init alembic  # Creates migration directory called "alembic"
- Create using a template           : alembic init --template pylons ./scripts
                                      Templates: generic, multidb, pylons
- Show templates                    : alembic list_templates
- Invoke with different init file   : alembic --config <file path>
- Create a migration script         : alembic revision -m "create account table"
- Autogenerate migration scripts    : alembic revision --autogenerate -m "Add account table"
- Run migration                     : alembic upgrade head
- Run migration to specific revision: alembic uprade 1975ea83b712
- Run migration to two revisions
  ahead                             : alembic upgrade +2
- Run migration to one revision
  behind                            : alembic downgrade -1
- Run migration to beginning        : alembic downgrade base
- View current revision             : alembic current
- View revision history             : alembic history [--verbose]
- View revision history range       : alembic history -r[start]:[end]
- Generate migrations as SQL scripts: alembic upgrade head --sql



PYTHON COMMAND REFERENCE
===============================================================================
- Description                       : Internal Alembic API
- Setup Config                      : from alembic.config import Config
                                      from alembic import command
                                      alembicConfig = Config("/path/to/yourapp/alembic.ini")
- Show branch points                : alembic.command.branches(config, verbose=False)
- Show current revision             : alembic.command.current(config, verbose=False, head_only=False)
- Revert to previous revision       : alembic.command.downgrade(config, revision, sql=False, tag=None)
- Edit revision scripts             : alembic.command.edit(config, rev)
- Show current available heads      : alembic.command.heads(config, verbose=False, resolve_dependencies=False)
- List changeset scripts in
  chronological order               : alembic.command.history(config, rev_range=None, verbose=False, indicate_current=False)
- Initialize a new scripts directory: alembic.command.init(config, directory, template='generic')
- List available templates          : alembic.command.list_templates(config)
- Merge two revisions together      : alembic.command.merge(config, revisions, message=None, branch_label=None, rev_id=None)
- Create a new revision file        : alembic.command.revision(config, message=None, autogenerate=False, sql=False, head='head', splice=False, branch_label=None, version_path=None, rev_id=None, depends_on=None, process_revision_directives=None)¶
- Show revision denoted by symbol   : alembic.command.show(config, rev)
- Stamp the revision table with the
  given revision, but don't run
  migrations                        : alembic.command.stamp(config, revision, sql=False, tag=None)
- Upgrade to a later revision       : alembic.command.upgrade(config, revision, sql=False, tag=None)



INVOCATION
===============================================================================
- Alembic is invoked                : alembic --config ./migrations/alembic.ini upgrade head
    |
    v
- Alembic locates ini file          : ./migrations/alembic.ini
  Init file points to directory       script_location = %(here)s
  containing env
    |
    v
- Alembic invokes env.py            : ./migrations/env.py
  env.py loades target_metadata       run_migrations_online()
  and database engine string
  then runs migrations



.INI FILE
===============================================================================
- Options
  [alembic]                         : When alembic reads in the config file
                                      Can be named differently to have multiple
                                      db's use the same config file
  script_location                   : Location of the alembic environment
                                      relative or absolute
                                      Ex. %(here)s/alembic
  file_template                     : Naming scheme used to generate new migration files
                                      Ex Tokens:
                                      %%(rev)s - revision id
                                      %%(slug)s - a truncated string derived from the revision message
                                      %%(year)d, %%(month).2d, %%(day).2d, %%(hour).2d, %%(minute).2d, %%(second).2d -
  timezone                          : Optional timezone name
                                      Ex. UTC, EST5EDT
  tuncate_slug_length               : Max number of charaters to include in slug field
                                      Default 40
  sqlalchemy.url                    : URL to connect to database via SQLAlchemy
                                      Only referenced by env.py
                                      Ex. postgresql://scott:tiger@localhost/test
  revision_environment              : If true, env.py should be run unconditionaly
  sourceless                        : If true, revision files only exist as .pyc, or .pyo
  version_locations                 : Optional lis of revision file locations
  output_encoding                   : Encoding to use when Alembic writes the script.py.mako
                                      Default 'utf-8'
  [loggers], [handlers],
  [formatters], [logger_*],
  [handler_*], [formatter_*]        : Part of python's standard logging configuration
                                      Used by env.py: logging.config.fileConfig()



AUTO GENERATING MIGRATIONS
===============================================================================
- Description                       : Alembic can view the status of the database and compare against
                                      the table metadata in the application, generating the "obvious"
                                      migrations based on the comparison
                                    : env.py needs to be modified correctly to point at the application's
                                      Base.metadata
                                    : It is ALWAYS necessary to manually review and correct
                                      candidate migrations that autogenerate produces
- Autogenerate migration scripts    : alembic revision --autogenerate -m "Add account table"
- Autogeneration
  Will detect                       : Table additions, removals
                                      Column additions, removals
                                      Change of nullable status of columns
                                      Basic changes in indexes and explcitly-named constraints
                                      Basic changes in foreign key constraints
  Optionally detects                : Change of column type. Set EnvironmentContext.configure.compare_type to True
                                      Change of server default. Set EnvironmentContext.configure.compare_server_default to True
  Cannot detect                     : Changes of table name
                                      Changes of column name
                                      Anonymously named constraints
                                      Special SQLAlchemy types such as Enum when generated on a
                                      backend which doesn’t support ENUM directly
  Cannot currently, but eventually  : Free-standing constraint additions and removals
                                      Sequence additions, removals



OPERATION REFERENCE
===============================================================================
- Description                       : Operations are used to migrate tables
- import                            : from alembic import op
- Add Column
  * Basic                           : op.add_column('organization', Column('name', String()))
  * Add foreign key                 : op.add_column('organization', Column('account_id', INTEGER, ForeignKey('accounts.id')))
  * Add default value               : op.add_column('account', Column('timestamp', TIMESTAMP, server_default=func.now()))
- Alter Column                      : op.alter_column
- Batch alter table                 : op.batch_alter_table
- Bulk insert                       : op.bulk_insert
- Create check contraint            : op.create_check_constraint("ck_user_name_len", "user", func.len(column('name')) > 5)
- Create exclude constraint         : op.create_exclude_constraint("user_excl", "user", ("period", '&&'), ("group", '='), where=("group != 'some group'"))
- Create foreign key                : op.create_foreign_key("fk_user_address", "address", "user", ["user_id"], ["id"])
- Create index                      : op.create_index('ik_test', 't1', ['foo', 'bar'])
- Create primary key                : op.create_primary_key("pk_my_table", "my_table", ["id", "version"])
- Create table                      : op.create_table(
                                          'account',
                                          Column('id', INTEGER, primary_key=True),
                                          Column('name', VARCHAR(50), nullable=False),
                                          Column('description', NVARCHAR(200)),
                                          Column('timestamp', TIMESTAMP, server_default=func.now())
                                      )
- Create unique constraint          : op.create_unique_constraint("uq_user_name", "user", ["name"])
- Drop column                       : drop_column('organization', 'account_id')
- Drop constraint                   : drop_index("accounts")
- Drop table                        : drop_table("accounts")
- Execute SQL                       : op.execute(
                                        account.update().\
                                            where(account.c.name==op.inline_literal('account 1')).\
                                            values({'name':op.inline_literal('account 2')})
                                            )
- Rename table                      : op.rename_table
- Get bind                          : op.get_bind
- Get MigrationContext              : op.get_context
- Register new operation            : Operations.register_operation



ADDING SUPPORT TO EXISTING SQLALCHEMY DATABASE
===============================================================================
- Stamp the existing database to be
  at the head                       : alembic stamp head
- Continue development, making
  changes to sqlalchemy schema.
  When ready to commit, generate a
  migration revision                : alembic revision --autogenerate -m "Added column foo to table bar"
- CHECK THAT THE MIGRATION SCRIPT
  ACCURATELY REFLECTS CHANGES!      : sublime <revision script>
- Update the underlying database
  to new revision                   : alembic upgrade head
- Show history of changes           : alembic history



GENERATING SQL SCRIPTS (OFFLINE MODE)
===============================================================================
- Description                     : A major capability of Alembic is to generate migrations
                                    as SQL scripts instead of running them against database
                                  : This is a required feature when developers don't have
                                    access to customer databases
- Support                         : Migration scripts can't access data
                                  : Migration scripts should use Alembic operations
- Upgrade to a version            : alembic upgrade ae1027a6acf --sql > migration.sql
- Migrate from version to version : alembic upgrade 1975ea83b712:ae1027a6acf --sql > migration.sql



NAMING CONSTRAINTS
===============================================================================
- Reason                          : If constrain names aren't specified, underlying database
                                    will name the constrains randomly
- Fix                             : Python Code
                                    metaData = MetaData(naming_convention = {                                 # Constrain naming for migrations
                                        "ix": "ix_%(column_0_label)s",                                        # Index
                                        "uq": "uq_%(table_name)s_%(column_0_name)s",                          # Unique Constraint
                                        "ck": "ck_%(table_name)s_%(constraint_name)s",                        # Check
                                        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",  # Foreign Key
                                        "pk": "pk_%(table_name)s"                                             # Primary Key
                                    })
                                    base    = declarative_base(metadata=metaData)
- Autogenerate                    : Will suck in contrain names automatically
                                    def upgrade():
                                        op.create_unique_constraint(op.f('uq_const_x'), 'some_table', 'x')
