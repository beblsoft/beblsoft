package workout;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import exercise.Exercise;
import exercise.ExerciseDB;
import exercise.ExerciseType;

public class WorkoutDB {
	/************************** INSTANCE VARIABLES ***************************/
	private HashMap<String, Workout> workoutMap;
	private ExerciseDB eDB;
	/************************** END INSTANCE VARIABLES ***********************/


	/************************** CONSTRUCTOR **********************************/
	public WorkoutDB(String initFilePath, ExerciseDB eDB){
		workoutMap = new HashMap<String, Workout>();
        this.eDB = eDB;
        
		if(!loadDBFromFile(initFilePath)){
			System.out.println("Error: couldn't load exercise database.");
		}
	}
	/************************** END CONSTRUCTOR ******************************/


	/************************** PUBLIC FUNCTIONS *****************************/
	public Workout getWorkout(String name){
		return workoutMap.get(name);
	}

	/**
	 * Returns a string consisting of all the Workouts in the DB
	 */
	public String toString(){
		String str = "";
		str += "Workout Database Contents:" + "\n";

		for(String name : workoutMap.keySet()){
			Workout w = workoutMap.get(name);
			str += w.parametersToString();
		}
		return str;
	}
	
	/**
	 * Returns a string consisting of all the workout names in DB
	 */
	public String workoutNamesToString(){
		String str =  "";

		for(String name : workoutMap.keySet()){
			str += name + "\n";
		}
		return str;
	}
	/************************** END PUBLIC FUNCTIONS *************************/


	/************************** PRIVATE FUNCTIONS ****************************/
	/**
	 * Opens the workout initialization file and adds corresponding fields to the workoutMap.
	 * @param initFilePath
	 * @return
	 */
	private boolean loadDBFromFile(String initFilePath){

		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (new File(initFilePath));	

			// normalize text representation
			doc.getDocumentElement().normalize();
			
			// load all the exercises found in the XML file
			NodeList listOfExercises = doc.getElementsByTagName("workout");

			for(int s = 0; s < listOfExercises.getLength() ; s++){
				Node firstElementNode = listOfExercises.item(s);
				if(firstElementNode.getNodeType() == Node.ELEMENT_NODE){
					if(!addWorkout((Element)firstElementNode))
							System.out.println("Invalid Workout");			
				}
			}
		}
		catch (SAXParseException err) {
			System.out.println ("** Parsing error" + ", line " 
					+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());
			return false;

		}catch (SAXException e) {
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
			return false;

		}catch (Throwable t) {
			t.printStackTrace ();
			return false;
		}
		return true;
	}

	/**
	 * Adds an Workout in XML format to workoutMap.
	 * @param element
	 * @return
	 */
	private boolean addWorkout(Element element){
         
		String name = getAttributeValue(element, "name");
        Workout w = new Workout(name,eDB);
        
        //Load workout parameters from xml
        w.setNumSets(Integer.parseInt(getAttributeValue(element,"numSets")));
        w.setNumExercisesInWarmup(Integer.parseInt(getAttributeValue(element,"numExercisesInWarmup")));
        w.setNumExercisesInSet(Integer.parseInt(getAttributeValue(element,"numExercisesInSet")));
        w.setDifficulty(Double.parseDouble(getAttributeValue(element,"difficulty")));
        
        String[] eTypes = getAttributeValue(element,"exerciseTypes").split(",");
        for(int i=0; i<eTypes.length; i++){
        	w.addExerciseType(ExerciseType.fromString(eTypes[i]));
        }
      
        workoutMap.put(name, w);
		return true;
	}

	/**
	 * Retrieves specific attribute for exercise.
	 * @param line
	 * @return
	 */
	private String getAttributeValue(Element element, String attribute)
	{
		NodeList list = element.getElementsByTagName(attribute);
		Element theElement = (Element)list.item(0);
		NodeList textList = theElement.getChildNodes();
		return ((Node)textList.item(0)).getNodeValue().trim();

	}
}
