package workout;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import misc.FileIO;
import exercise.Exercise;
import exercise.ExerciseDB;
import exercise.ExerciseType;

public class Workout {

	/************************** INSTANCE VARIABLES ***************************/
	private String							name 					= "";
	private ExerciseDB						eDB  					= null;
	private int								numSets					= 0;
	private int								numExercisesInWarmup	= 0;
	private int								numExercisesInSet 		= 0;
	private HashSet<ExerciseType>			exerciseTypes			= null;
	private double							difficulty				= 0.0;
	
	private Boolean							exercisesLoaded			= false;
	private ArrayList< Exercise>			warmupExercises			= null;
	private ArrayList< ArrayList<Exercise>> sets					= null;
	/************************** END INSTANCE VARIABLES ***********************/
	
	
	/************************** CONSTRUCTOR **********************************/
	public Workout(String name, ExerciseDB eDB){
		this.eDB = eDB;
		this.name = name;
	}	
	/************************** END CONSTRUCTOR ******************************/
	

	/************************** PUBLIC FUNCTIONS *****************************/
	/**
	 * Given the workout parameters, loads the appropriate exercises
	 */
	public Boolean loadExercises(){
		if(!validParametersLoaded()){
			System.out.println("Workout: Error, need to load workout parameters");
			return false;
		}
		exercisesLoaded = false;
		
		HashSet<Exercise> exerciseSet;
		Iterator<Exercise> eIt;
		int counter;
		
		//Load Warmup
		warmupExercises = new ArrayList<Exercise>();
		exerciseSet = eDB.getExercisesByType(ExerciseType.WARMUP);
		eIt = exerciseSet.iterator();
		counter = 0;
		while(counter++ < this.numExercisesInWarmup){
			if(!eIt.hasNext()){
				System.out.println("Workout: Error ran out of warmup exercises");
				return false;
			}
			warmupExercises.add(eIt.next());
		}
		
		//Load Main Workout Sets
		//Step 1: Add all exercises from exerciseTypes from eDB into possible exerciseSet
		exerciseSet = new HashSet<Exercise>();
		Iterator<ExerciseType> etIt = exerciseTypes.iterator();
		while(etIt.hasNext()){
			exerciseSet.addAll(eDB.getExercisesByType(etIt.next()));
		}		
		eIt = exerciseSet.iterator();
		
		//Step 2: Load potential exercises into sets
		sets = new ArrayList< ArrayList<Exercise>>();
		for(int setNum=0; setNum<numSets; setNum++){
			ArrayList<Exercise> set = new ArrayList<Exercise>(); 
			for(int eNum=0; eNum<numExercisesInSet; eNum++){
                if(!eIt.hasNext()){
    				System.out.println("Workout: Error ran out of exercises");
    				return false;
                }
				set.add(eIt.next());
			}
			sets.add(set);
		}
		
		exercisesLoaded = true;
		return true;
	}
	
	/**
	 * Returns a string of all the exercises
	 */
	public String exercisesToString(){
		if(!exercisesLoaded) loadExercises();
		
		String str = "";
		str += "Workout name: "	+ name + "\n\n";
		str += warmupToString() + "\n";
		str += setsToString() + "\n";
		return str;
	}
	
	public String warmupToString(){
		String str = "";
		str += "Warmup: \n";
		for(int i=0; i<warmupExercises.size(); i++){
			str += warmupExercises.get(i).getName() + "\n";
		}
		return str;
	}
	
	public String setsToString(){
		String str = "";
		str += "Main Sets: \n";
		for(int setNum=0; setNum<sets.size(); setNum++){
			str += "Set Number: " + Integer.toString(setNum + 1) + "\n";
			ArrayList<Exercise> set = sets.get(setNum);
			for(int eNum=0; eNum<set.size(); eNum++){
				str += set.get(eNum).getName() + "\n";
			}
			//Formatting: Don't Add extra new line at end
			if(setNum != sets.size() - 1) str += "\n";
		}
		return str;
	}
	
	/**
	 * Returns a string of the instance variables in the workout
	 * @return
	 */
	public String parametersToString(){
		String str = "";
		str += "Workout parameters: \n";
		
		str += "[";
		str += "Name = " 					+ name 					+ ", ";
		str += "ExerciseDB = " 				+ eDB.hashCode() 		+ ", ";
		str += "NumSets = " 				+ numSets 				+ ", ";
		str += "NumExercisesInWarmup = " 	+ numExercisesInWarmup 	+ ", ";
		str += "NumExercisesInSet = " 		+ numExercisesInSet 	+ ", ";
		str += "Difficulty = " 				+ difficulty 			+ "";
		str += "] \n";
		
		return str;
	}
	
	/**
	 * Writes the workout details the specified input file.
	 * @param filePath
	 * @return
	 */
	public Boolean outputToFile(String filePath){
		
		BufferedWriter bw = FileIO.openFile("autoExercise.txt");
		if(bw == null)return false;
		
		FileIO.outputText(bw, "Here is your workout, good luck!\n");
		FileIO.outputText(bw, this.exercisesToString());
		FileIO.outputText(bw, "Great Job Finishing, See you next time!\n");
		FileIO.outputText(bw, "Cheers :)\n");
		
		FileIO.closeFile(bw);
		return true;
	}
	/************************** END PUBLIC FUNCTIONS *************************/
	
	
	/************************** PRIVATE FUNCTIONS ****************************/	
	/**
	 * Returns true if workout has all necessary parameters initialized, false otherwise
	 * @return
	 */
	private Boolean validParametersLoaded(){
		if( eDB 				== null	||
			numSets 			== 0 	||
			numExercisesInSet 	== 0 	||
			exerciseTypes 		== null		){
			System.out.println(this.parametersToString());
			return false;
		}
		return true;
	}
    /************************** END PRIVATE FUNCTIONS ************************/
	
	
	/************************** PUBLIC GETTERS/SETTERS ***********************/
	public ExerciseDB geteDB() {
		return eDB;
	}

	public void seteDB(ExerciseDB eDB) {
		this.eDB = eDB;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumSets() {
		return numSets;
	}

	public void setNumSets(int numSets) {
		this.numSets = numSets;
	}

	public int getNumExercisesInWarmup() {
		return numExercisesInWarmup;
	}

	public void setNumExercisesInWarmup(int numExercisesInWarmup) {
		this.numExercisesInWarmup = numExercisesInWarmup;
	}

	public int getNumExercisesInSet() {
		return numExercisesInSet;
	}

	public void setNumExercisesInSet(int numExercisesInSet) {
		this.numExercisesInSet = numExercisesInSet;
	}

	public HashSet<ExerciseType> getExerciseTypes() {
		return exerciseTypes;
	}

	public void addExerciseType(ExerciseType et) {
		if(exerciseTypes == null) exerciseTypes = new HashSet<ExerciseType>();
		this.exerciseTypes.add(et);
	}	

	public void removeExerciseType(ExerciseType et) {
		if(exerciseTypes == null) return;
		this.exerciseTypes.remove(et);
	}

	public double getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(double difficulty) {
		this.difficulty = difficulty;
	}	
	/************************** END PUBLIC GETTERS/SETTERS *******************/
}
