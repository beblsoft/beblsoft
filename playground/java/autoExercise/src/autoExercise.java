import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import workout.Workout;
import workout.WorkoutDB;
import exercise.ExerciseDB;

public class autoExercise {

	/************************** CONSTANTS ************************************/
	private static final String exerciseDBInitPath 	= "txt/exerciseDBInit.xml";
	private static final String workoutDBInitPath 	= "txt/workoutDBInit.xml";
	private static final String outputFilePath 		= "autoExercise.txt";
	/************************** END CONSTANTS ********************************/


	/************************** MAIN *****************************************/
	public static void main(String[] args)
	{
		ExerciseDB eDB = new ExerciseDB(exerciseDBInitPath);
		System.out.println("Successfully Loaded Exercise DB...");

		WorkoutDB wDB = new WorkoutDB(workoutDBInitPath, eDB);
		System.out.println("Successfully Loaded Workout DB...");

		System.out.println("Workout DB = " + wDB.toString());

		Workout w = promptUserForWorkout(wDB);
		w.outputToFile(outputFilePath);
	}
	/************************** END MAIN *************************************/


	/************************** PRIVATE FUNCTIONS ****************************/
    private static Workout promptUserForWorkout(WorkoutDB wDB){
		Workout w;
		Scanner reader = new Scanner(System.in);

		while(true){
			System.out.println("Workouts to Choose From:");

			System.out.println(wDB.workoutNamesToString());

			System.out.println("Please enter a workout name:");
			String name = reader.nextLine();

			w = wDB.getWorkout(name);
			if(w != null) break;

			System.out.println("Please enter a valid workout name.");
		}
		reader.close();

		return w;
    }
    /************************** END PRIVATE FUNCTIONS ************************/



}
