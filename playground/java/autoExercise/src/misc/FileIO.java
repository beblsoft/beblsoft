package misc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileIO {
	/************************** PUBLIC FUNCTIONS ********************************/
	public static BufferedWriter openFile(String fileName)
	{
		try {
			FileWriter fileWriter = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fileWriter);
			return bw;
		}
		catch(IOException ex) {
			System.out.println("Error writing to file: "+ fileName + "");
			return null;
		}
	}

	public static boolean closeFile(BufferedWriter bw)
	{
		try {
			bw.close();
			return true;
		}
		catch(IOException ex) {
			System.out.println("Error clsoing file");
			return false;
		}
	}

	public static boolean outputText(BufferedWriter bw, String text)
	{
		System.out.println(text);

		try {
			bw.write(text);
			return true;
		}
		catch(IOException ex) {
			System.out.println("Error writing to file");
			return false;
		}
	}
	/************************** END PUBLIC FUNCTIONS ****************************/
}
