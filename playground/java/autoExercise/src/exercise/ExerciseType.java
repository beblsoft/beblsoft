package exercise;

public enum ExerciseType {
	/************************** ENUMERATION TYPES ****************************/
	ERROR		("ERROR"),
	NONE		("NONE"),
	WARMUP		("WARMUP"),
	BICEP 		("BICEP"),
	TRICEP 		("TRICEP"),
	BACK		("BACK"),
	CHEST		("CHEST"),
	SHOULDER	("SHOULDER"),
	ABS			("ABS"),
	HAMSTRING	("HAMSTRING"),
	QUAD		("QUAD"),
	CORE		("CORE"),
	RUN			("RUN"),
	SWIM		("SWIM"),
	BIKE		("BIKE"),
	ROW			("ROW"),
	YOGA		("YOGA");
	/************************** END ENUMERATION TYPES ************************/


	/************************** INSTANCE VARIABLES ***************************/
	private final String name;
	/************************** END INSTANCE VARIABLES ***********************/


	/************************** CONSTRUCTOR **********************************/
	ExerciseType(String name){
		this.name = name;
	}
	/************************** END CONSTRUCTOR ******************************/


	/************************** PUBLIC FUNCTIONS *****************************/
	public String toString(){
		return this.name;
	}

	public static ExerciseType fromString(String type){
		for(ExerciseType et : ExerciseType.values()){
			if(type.toUpperCase().equals(et.toString())){
				return et;
			}
		}
		return ExerciseType.ERROR;
	}
	/************************** END PUBLIC FUNCTIONS *************************/








}
