package exercise;

public class Exercise {
	
	/************************** CONSTANTS ************************************/
	public final double RANK_MIN = 0;
	public final double	RANK_MAX = 10;

	/************************** END CONSTANTS ********************************/

	
	/************************** INSTANCE VARIABLES ***************************/
	private String name	= "";
	private ExerciseType type = ExerciseType.NONE;
	private int reps = 0;
	private int duration = 0;      //seconds
	private int	weight = 0;
	private String videoURL	= "";
	private String descriptionURL = "";
	private double rank	= RANK_MIN;	
	/************************** END INSTANCE VARIABLES ***********************/
	
	
	/************************** CONSTRUCTOR **********************************/
	public Exercise(String name, ExerciseType type){
		this.name = name;
		this.type = type;
	}
	/************************** END CONSTRUCTOR ******************************/
	

	/************************** PUBLIC FUNCTIONS *****************************/
	/**
	 * Returns comma separated string of the exercise's attributes.
	 * @param e
	 * @return
	 */
	public String toString(){
		String str = "";
		str += "Exercise:";
		str += "Type:" + this.type.toString() + ",";
		str += "Name:" + this.name + ",";
		str += "Reps:" + Integer.toString(this.reps) +  ",";
		str += "Duration:" + Integer.toString(this.duration) + ",";
		str += "Weight:" + Integer.toString(this.weight) + ",";
		str += "VideoURL:" + this.videoURL + ",";
		str += "DescriptionURL:" + this.descriptionURL + ",";
		str += "Rank:" + Double.toString(this.rank);
		return str;
	}
	
	public boolean equals(Exercise e){
		//Two exercises are the same if same type and name
		return ((this.type == e.type) && this.name.equals(e.name));
	}
	/************************** END PUBLIC FUNCTIONS *************************/
	
	
	/************************** PUBLIC GETTERS/SETTERS ***********************/
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public ExerciseType getType() {
		return type;
	}


	public void setType(ExerciseType type) {
		this.type = type;
	}


	public int getReps() {
		return reps;
	}


	public void setReps(int reps) {
		this.reps = reps;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}


	public String getVideoURL() {
		return videoURL;
	}


	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public String getDescriptionURL() {
		return descriptionURL;
	}

	public void setDescriptionURL(String descriptionURL) {
		this.descriptionURL = descriptionURL;
	}

	public double getRank() {
		return rank;
	}


	public void setRank(double rank) {
		this.rank = rank;
	}

	/************************** END PUBLIC GETTERS/SETTERS *******************/
}
