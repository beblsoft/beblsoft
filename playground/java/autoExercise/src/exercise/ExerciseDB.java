package exercise;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ExerciseDB {

	/************************** INSTANCE VARIABLES ***************************/
	private HashMap<ExerciseType, HashSet<Exercise> > exerciseMap;
	/************************** END INSTANCE VARIABLES ***********************/


	/************************** CONSTRUCTOR **********************************/
	public ExerciseDB(String initFilePath){
		exerciseMap = new HashMap<ExerciseType, HashSet<Exercise>>();

		if(!initExerciseMap()){
			System.out.println("Error: couldn't initialize exercise map.");
		}

		if(!loadDBFromFile(initFilePath)){
			System.out.println("Error: couldn't load exercise database.");
		}
	}
	/************************** END CONSTRUCTOR ******************************/


	/************************** PUBLIC FUNCTIONS *****************************/
	public HashSet<Exercise> getExercisesByType(ExerciseType et){
		return exerciseMap.get(et);
	}

	/**
	 * Returns a string consisting of all the Exercises in the DB
	 */
	public String toString(){
		String str = "";
		str += "Exercise Database Contents:" + "\n";

		//Iterate through all exercise types, printing all data
		for(ExerciseType et : exerciseMap.keySet()){
			HashSet<Exercise> eSet = exerciseMap.get(et);
			if(!eSet.isEmpty()){
				str += "Exercise of Type: " + et.toString() + "\n";
				for(Exercise e: eSet){
					str += e.toString();
					str += "\n";
				}	
			}
		}
		return str;
	}
	/************************** END PUBLIC FUNCTIONS *************************/


	/************************** PRIVATE FUNCTIONS ****************************/
	/**
	 * Initializes all the ExerciseType Sets in the exerciseMap
	 * @return
	 */
	private boolean initExerciseMap(){
		for(ExerciseType et : ExerciseType.values()){
			HashSet<Exercise> eSet = new HashSet<Exercise>();
			exerciseMap.put(et, eSet);
		}
		return true;
	}

	/**
	 * Opens the initialization file and adds corresponding fields to the exercise database.
	 * @param initFilePath
	 * @return
	 */
	private boolean loadDBFromFile(String initFilePath){

		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (new File(initFilePath));	

			// normalize text representation
			doc.getDocumentElement ().normalize ();
			//System.out.println ("Root element of the doc is " + doc.getDocumentElement().getNodeName());

			// load all the exercises found in the XML file
			NodeList listOfExercises = doc.getElementsByTagName("exercise");
			//System.out.println("Total number of exercises : " + listOfExercises.getLength() + "\n");

			for(int s = 0; s < listOfExercises.getLength() ; s++){
				Node firstElementNode = listOfExercises.item(s);
				if(firstElementNode.getNodeType() == Node.ELEMENT_NODE){
					if(!addExercise((Element)firstElementNode))
							System.out.println("Invalid Exercise or Exercise Type");			
				}
			}
		}
		catch (SAXParseException err) {
			System.out.println ("** Parsing error" + ", line " 
					+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());
			return false;

		}catch (SAXException e) {
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
			return false;

		}catch (Throwable t) {
			t.printStackTrace ();
			return false;
		}
		return true;
	}

	/**
	 * Adds an Exercise to DB.
	 * @param element
	 * @return
	 */
	private boolean addExercise(Element element){

		String name = getAttributeValue(element, "name");
		String type = getAttributeValue(element, "type");

		// Only add exercises that have a name and valid type
		ExerciseType et = ExerciseType.fromString(type);
		if(et != ExerciseType.ERROR && !name.isEmpty()){
			Exercise e = new Exercise(name, et);
			e.setReps(Integer.parseInt(getAttributeValue(element, "reps")));
			e.setDuration(Integer.parseInt(getAttributeValue(element, "duration")));
			e.setWeight(Integer.parseInt(getAttributeValue(element, "weight")));
			e.setDescriptionURL(getAttributeValue(element, "description"));
			e.setVideoURL(getAttributeValue(element, "videoURL"));
			e.setRank(Integer.parseInt(getAttributeValue(element, "rank")));
			HashSet<Exercise> eSet = exerciseMap.get(et);
			eSet.add(e);
			return true;
		}
		return false;
	}

	/**
	 * Retrieves specific attribute for exercise.
	 * @param line
	 * @return
	 */
	private String getAttributeValue(Element element, String attribute)
	{
		NodeList list = element.getElementsByTagName(attribute);
		Element theElement = (Element)list.item(0);
		NodeList textList = theElement.getChildNodes();
		return ((Node)textList.item(0)).getNodeValue().trim();

	}
	/************************** END PRIVATE FUNCTIONS ************************/
}
