# Java Technology Documentation

Java is a general-purpose programming language that is class-based, object-oriented, and designed
to have as few implementation dependencies as possible. It is intended to let application developers
"write once, run anywhere" (WORA), meaning that compiled Java code can run on all platforms that
support Java without the need for recompilation.

Java was originally developed by James Gosling at Sun Microsystems and released in 1995 as a core
component of Sun Microsystem's Java Platform. As of May 2007, in compilance with the specifications
of the Java Community Process, Sun had relicensed most of its Java technologies under the GNU
General Public License.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Java_(programming_language)),
[Home](https://www.java.com/en/)