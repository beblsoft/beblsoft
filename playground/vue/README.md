# Vue.js Technology Documentation

Vue is an approachable, versatile, performant and progressive framework for building user interfaces
in a maintainable and testable manner. Most notably, it allows developers to split up web page
into reusable components.

Vue satisfies the following UI Technology requirements
- Rock solid from customer perspective.
- Slick, professional appearance.
- Fast to load from AWS, ability to incrementally load if needed for performance.
- Does not violate standards reducing lock in if something better comes along.
- Easy ability to add in 3rd party integrations: Google, facebook, twitter.
- Readily supports Web service API backend.
- Simple to add new screens, routes to screens, and back end services.

Relevant URLs:
[Home](https://vuejs.org/),
[Guide](https://vuejs.org/v2/guide/),
[Github](https://github.com/vuejs/vue),
[Awesome Vue](https://github.com/vuejs/awesome-vue),
[Live examples](https://vuejs.org/v2/examples/),
[Development Tools](https://github.com/vuejs/vue-devtools),
[Style Guide](https://vuejs.org/v2/style-guide/)

## Directory Structure

```bash
.
├── core             : Core Feature Examples
│   ├── essentials   : Vue Essential concepts and examples, start here!
│   ├── vuex         : Vue state management subsystem
│   ├── vueRouter    : Vue application routing subsystem
│   ├── vuePress     : Vue static documentation generator
│   ├── vueDevTools  : Chrome extension to help debug Vue Applications
│   └── vueCli       : Vue Development Command Line Interface
├── apps             : Complete Vue Sample Applications
├── courses          : Vue Book and Course Examples
├── smeckn           : Smeckn prototypes
├── thirdParty       : Third Party Integration Examples
└── widgets          : Candy store of useful widgets for app construction
```

Where to begin:
- Essentials: [README](./core/essentials/README.md), [HelloWorld.html](./core/essentials/HelloWorld.html)
- VueRouter: [README](./core/vueRouter/README.md), [README-projects](./core/vueRouter/README-projects.md)
- Vuex: [README](./core/vuex/README.md), [README-projects](./core/vuex/README-projects.md)
- VueCLI: [README](./core/vueCLI/README.md)
- VuePress: [README](./core/vuePress/README.md), [README-projects](./core/vuePress/README-projects.md)
- VueDevTools: [README](./core/vueDevTools/README.md)

## Installation

Install node/npm: See `nodejs.sh`

Install vue with npm: `npm install vue`

Install in a html file via CDN:
```html
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
```
