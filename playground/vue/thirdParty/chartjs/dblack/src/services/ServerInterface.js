/* ServerInterface.js - ServerInterface Component provides access to all server data */
/*import autoGen from '../../autogen'
import * as apiV1 from './apiV1/api.js'*/

/*apiV1.setDomain(autoGen.APIDomain)
console.log(apiV1.getDomain())*/

import Firebase from "firebase"

// Firebase config - this is provided when you create your app
// Swap out these settings for your project settings
const config = {
    apiKey: "AIzaSyARIllJX5NwVZf9T5brOqhx3dDvLgGZfl8",
    authDomain: "playing-218ba.firebaseapp.com",
    databaseURL: "https://playing-218ba.firebaseio.com",
    projectId: "playing-218ba",
    storageBucket: "playing-218ba.appspot.com",
    messagingSenderId: "1092711989826"
};

class ServerInterface {
    constructor() {
        this.smecknUser = null
    }

    checkForLoginCookie() {
        console.log("checkForLoginCookie called")

    }
    initializeServerInterface() {
        console.log("initializeServerInterface")
        this.checkForLoginCookie()
        Firebase.initializeApp(config)
    }

    isReady(onStateChange) {
        console.log("call it isReady")
        // Wrap the vue instance in a Firebase onAuthStateChanged method
        // Call the function passed in once the auth stat change has happened
        Firebase.auth().onAuthStateChanged(onStateChange)

    }

    signInWithEmailAndPassword(email, password) {
        console.log("signInWithEmailAndPassword called")
        var thisPointer = this // must make copy of this pointer locally prior to Promise
        var promise = Firebase.auth()
            .signInWithEmailAndPassword(email, password)
            .then(
                user => {
                    thisPointer.smecknUser = "darryl black"
                },
                error => {
                    alert(error.message)
                    this.smecknUser = null
                }
            );
        return promise;
    }

    createUserWithEmailAndPassword(email, password) {
        console.log("createUserWithEmailAndPassword called")
        var thisPointer = this
        var promise = Firebase.auth()
            .createUserWithEmailAndPassword(email, password)
            .then(
                user => {
                    thisPointer.smecknUser = "james bensson"
                },
                error => {
                    alert(error.message)
                }
            );
        return promise
    }

    getCurrentUser() {
        console.log("getCurrentUser called")
        // simulate a user has remained logged in
        if (Firebase.auth().currentUser != null) this.smecknUser = "darryl black"
        return this.smecknUser
    }

    getUserRecord() {
        console.log("getUserRecord called")
        return Firebase.auth().currentUser
    }

    signOut() {
        console.log("signOut called")
        var thisPointer = this // must make copy of this pointer locally prior to Promise
        var promise = Firebase.auth()
            .signOut()
            .then(
                user => {
                    thisPointer.smecknUser = null
                },
            );
        return promise
    }

}
var server = new ServerInterface()

export default server