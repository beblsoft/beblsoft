import TabsHome from './components/TabsHome.vue'
import Error404 from './components/Error404.vue'
import VVCBar from './components/VVCBar.vue'
import VVCDoubleLine from './components/VVCDoubleLine.vue'
import VVCDoughnutTabs from './components/VVCDoughnutTabs.vue'
import VVCPies from './components/VVCPies.vue'
import VVCMultiple from './components/VVCMultiple.vue'
import SmecknDash from './components/SmecknDash.vue'
import SimpleVertical from './components/SimpleVertical.vue'

// This is where you add all your site routes
// Each route is set as an obect in the array
// For a the most basic route just set
// the path & component to load

export const routes = [{
        path: '',
        name: 'tabsHome',
        component: TabsHome
    },
    {
        path: '/404',
        name: '404',
        component: Error404
    },
    {
        path: '*',
        redirect: '/404'
    },
    {
        path: '/VVCBar',
        name: 'vvcBar',
        component: VVCBar
    },
    {
        path: '/vvcDoubleLine',
        name: 'vvcDoubleLine',
        component: VVCDoubleLine
    },
    {
        path: '/vvcDoughnutTabs',
        name: 'vvcDoughnutTabs',
        component: VVCDoughnutTabs
    },
    {
        path: '/vvcPies',
        name: 'vvcPies',
        component: VVCPies
    },
    {
        path: '/vvcMultiple',
        name: 'vvcMultiple',
        component: VVCMultiple
    },
    {
        path: '/smecknDash',
        name: 'smecknDash',
        component: SmecknDash
    },
    {
        path: '/simpleVertical',
        name: 'simpleVertical',
        component: SimpleVertical
    }
]