# ChartJS Samples Documentation

Demonstrates Use of Visual View Components (VVC), focused mainly on chartjs.
Shows tabs too, triggered via Doughnut button

To Run:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

