import Vue from 'vue';
import Router from 'vue-router';
import BarBasic from './components/BarBasic.vue';
import BarTime from './components/BarTime.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/barBasic',
      name: 'rnBarBasic',
      component: BarBasic,
      alias: '/'
    },
    {
      path: '/barTime',
      name: 'rnBarTime',
      component: BarTime
    },
  ]
})
