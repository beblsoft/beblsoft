# vue-sidebar-menu Docuemntation

vue-sidebar-menu is a Vue.js Sidebar Menu Component

Relevant URLs:
[NPM](https://www.npmjs.com/package/vue-sidebar-menu),
[GitHub](https://github.com/yaminncco/vue-sidebar-menu),
[Demo](https://yaminncco.github.io/vue-sidebar-menu/#/page)

## Installation

Install with NPM:
```bash
npm i vue-sidebar-menu --save
```

Import into project globally:
```js
import Vue from 'vue'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
Vue.use(VueSidebarMenu)
```

Or import component locally:
```js
//App.vue
import { SidebarMenu } from 'vue-sidebar-menu'
export default {
  components: {
    SidebarMenu
  }
}
```

## Usage

```html
<template>
  <sidebar-menu :menu="menu" />
</template>

<script>
    export default {
        data() {
            return {
                menu: [
                    {
                        header: true,
                        title: 'Main Navigation',
                        // component: componentName
                        // visibleOnCollapse: true
                        // class:''
                        // attributes: {}
                    },
                    {
                        href: '/',
                        title: 'Dashboard',
                        icon: 'fa fa-user',
                        // disabled: true
                        // class:''
                        // attributes: {}
                        // alias: '/path'
                        /*
                        badge: {
                            text: 'new',
                            // class:''
                            // attributes: {}
                        }
                        */
                    },
                    {
                        title: 'Charts',
                        icon: 'fa fa-chart-area',
                        child: [
                            {
                                href: '/charts/sublink',
                                title: 'Sub Link',
                            }
                        ]
                    }
                ]
            }
        }
    }
</script>
```

## Vue-router support

If you are using vue-router, the component will use `<router-link>` instead of hyperlink `<a>`.

## Props

```js
props: {
    menu: {
        type: Array,
        required: true
    },
    collapsed: {
        type: Boolean,
        default: false
    },
    width: {
        type: String,
        default: '350px'
    },
    widthCollapsed: {
        type: String,
        default: '50px'
    },
    showChild: {
        type: Boolean,
        default: false
    },
    theme: { // available themes: 'white-theme'
        type: String,
        default: ''
    },
    showOneChild: {
      type: Boolean,
      default: false
    },
    rtl: {
      type: Boolean,
      default: false
    }
}
```

## Events

```html
<sidebar-menu @collapse="onCollapse" @itemClick="onItemClick" />
...
methods: {
    onCollapse(collapsed) {
        console.log('This is triggered on btn-collaps toggling!');
    },
    onItemClick(event, item) {
        console.log('This is triggered whenever an item is clicked!');
    }
}
```

## Styles

All style custimization can be done in normal CSS by using these classes:
```css
.v-sidebar-menu {}
.v-sidebar-menu.vsm-default {}
.v-sidebar-menu.vsm-collapsed {}
.v-sidebar-menu .vsm-header {}
.v-sidebar-menu .vsm-list {}
.v-sidebar-menu .vsm-dropdown > .vsm-list {}
.v-sidebar-menu .vsm-item {}
.v-sidebar-menu .vsm-item.first-item {}
.v-sidebar-menu .vsm-item.mobile-item {}
.v-sidebar-menu .vsm-item.open-item {}
.v-sidebar-menu .vsm-item.active-item {}
.v-sidebar-menu .vsm-item.parent-active-item {}
.v-sidebar-menu .vsm-link {}
.v-sidebar-menu .vsm-title {}
.v-sidebar-menu .vsm-icon {}
.v-sidebar-menu .vsm-arrow {}
.v-sidebar-menu .vsm-arrow.open-arrow {}
.v-sidebar-menu .vsm-mobile-bg {}
.v-sidebar-menu .vsm-badge {}
```

or you can override SASS variables and create your own theme:
```css
/*app.scss*/
@import "custom-var.scss";
@import "vue-sidebar-menu/src/scss/vue-sidebar-menu.scss";
```


