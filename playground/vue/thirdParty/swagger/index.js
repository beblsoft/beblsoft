/*
 NAME
   index.js

 DESCRIPTION
   Sample file to generate a Vue Api given a swagger spec

 USAGE
   node ./index.js
*/

const swaggerGen = require('swagger-vue')
const jsonData   = require('./swagger.json')
const fs         = require('fs')
const path       = require('path')

let opt = {
  swagger: jsonData,
  moduleName: 'api',
  className: 'api'
}
const codeResult = swaggerGen(opt)
console.log(codeResult)
