# swagger Samples Documentation

## index.js

Autogenerate vue client API file given a swagger spec

To Run
- Install dependencies: `npm install`
- Autogenerate Vue API from swagger.json: `node index.js`
