var STATUS = {
    FAILURE: 0,
    SUCCESS: 1
};
var BS_Response = {
    status: STATUS.SUCCESS,
    data: null,
    error: null
};

var BS_ErrorResponse = {
    status: STATUS.FAILURE,
    data: null,
    error: null
};

var firstMethod = function(url) {
    var promise = new Promise(function(resolve, reject) {
        axios.get(url)
            .then((response) => {
                console.log('Axios Data Returned:' + JSON.stringify(response));
                //console.log('Axios Data Returned:' + JSON.stringify(response.data[0]));
                resolve({ status: STATUS.SUCCESS, data: response.data[0], error: null });
            })
            .catch((error) => {
                console.log(error)
                reject({ status: STATUS.FAILURE, data: null, error: null });
            });
    });
    return promise;
};

var secondMethod = function(BS_Response) {
    console.log('secondMethod:' + BS_Response.data.price_usd + ' ' + BS_Response.status + ' ' + BS_Response.error);
    var promise = new Promise(function(resolve, reject) {
        setTimeout(function() {
            console.log('second method completed');
            resolve({ newData: BS_Response.data.price_usd + ' some more data' });
        }, 1000);
    });
    return promise;
};

var thirdMethod = function(someStuff) {
    console.log('thirdMethod:' + someStuff.newData);
    var promise = new Promise(function(resolve, reject) {
        setTimeout(function() {
            console.log('third method completed');
            resolve({ result: someStuff.newData });
        }, 1000);
    });
    return promise;
};

var madeIt = function(BS_Response) {
    console.log('Completed Transaction')
    console.log('data example ' + BS_Response.data.price_usd);
    console.log('status ' + BS_Response.status);
};


firstMethod('https://api.coinmarketcap.com/v1/ticker/Bitcoin/')
    .then(secondMethod)
    .catch((error) => { console.log(error) })
    .then(thirdMethod)
    .catch((error) => { console.log(error) });