# Vue Form Wizard Documentation

`vue-form-wizard` is a dynamic wizard used to easily split up forms. It is a Vue based component
with no external dependencies.

Relevant URLs:
[Github](https://github.com/BinarCode/vue-form-wizard),
[Docs](https://binarcode.github.io/vue-form-wizard/#/),
[NPM](https://www.npmjs.com/package/vue-form-wizard)

## Usage

### NPM Installation

```bash
npm install vue-form-wizard
```

### Direct Script Include

```html
<link rel="stylesheet" href="https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.min.css">
<script src="https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.js"></script>
```

### Component Registration

```js
//global registration
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)

//local registration
import {FormWizard, TabContent} from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
//component code
components: {
  FormWizard,
  TabContent
}
```

### Template Usage

```html
<form-wizard>
  <tab-content title="Personal details">
    My first tab content
  </tab-content>
  <tab-content title="Additional Info">
      My second tab content
   </tab-content>
   <tab-content title="Last step">
     Yuhuuu! This seems pretty damn simple
   </tab-content>
</form-wizard>
```

## Props

### Form Wizard

```js
props: {
  title: {
    type: String,
    default: 'Awesome Wizard'
  },
  subtitle: {
    type: String,
    default: 'Split a complicated flow in multiple steps'
  },
  nextButtonText: {
    type: String,
    default: 'Next'
  },
  backButtonText: {
    type: String,
    default: 'Back'
  },
  finishButtonText: {
    type: String,
    default: 'Finish'
  },
  stepSize: {
    type: String,
    default: 'md',
    validator: (value) => {
      let acceptedValues = ['xs', 'sm', 'md', 'lg']
      return acceptedValues.indexOf(value) !== -1
    }
  },
  /***
  *  Sets validation (on/off) for back button. By default back button ignores validation
  */
  validateOnBack: Boolean,
  /***
  * Applies to text, border and circle
  */
  color: {
    type: String,
    default: '#e74c3c' //circle, border and text color
  },
  /***
  *  Is set to current step and text when beforeChange function fails
  */
  errorColor: {
    type: String,
    default: '#8b0000'
  },
  /**
  * Can take one of the following values: 'circle|square|tab`
  */
  shape: {
    type: String,
    default: 'circle'
  },
  /**
  * name of the transition when transition between steps
  */
  transition: {
    type: String,
    default: '' //name of the transition when transition between steps
  },
  /***
  * Index of the initial tab to display
  */
  startIndex: {
    type: Number,
    default: 0
  }
}
```

### Tab Content

```js
props: {
  title: {
    type: String,
    default: ''
  },
  /***
   * Icon name for the upper circle corresponding to the tab
   * Supports themify icons only for now.
   */
  icon: {
    type: String,
    default: ''
  },
  /***
   * Function to execute before tab switch. Return value must be boolean
   * If the return result is false, tab switch is restricted
   */
  beforeChange: {
    type: Function
  }
}
```

### Events

Vue-form-wizard emits certain events when certain actions happen inside the component.

- __on-complete__: Called when the finish button is clicked and the `before-change` for the last
  step (if present) was executed. No parameters are sent. `this.$emit('on-complete')`

- __on-loading__: Called whenever an async `before-change` is executed.
  `this.$emit('on-loading', value)`

- __on-validate__: Called whenever the execution of a `before-change` method is completed
  `this.$emit('on-validate', validationResult, this.activeTabIndex')`

- __on-error__: Called when `before-change` is a promised and is rejected with a message. The message
  is passed along `this.$emit('on-error', error)`.

- __on-change__: Called upon step changes. Has prevIndex and nextIndex as params. `this.$emit('on-change',
  prevIndex, nextIndex)`

## Slots

- __Default__: Used for tab-contents
- __title__: Upper title section including sub-title
- __prev__: Previous button content (no need to worry about handling the button functionality)
- __next__: Next button content
- __finish__: Finish button content
- __custom-buttons-left__: Appears on right of "Back" button
- __custom-buttons-right__: Appears on the left of "Next/Finish" button

## Methods

- __reset__: will reset the wizard to the initial state
- __activateAll__: will activate all steps as if the user went through all
- __nextTab__: navigates to the next tab. The same method is used when clicking next button
- __prevTab__: navigates to the prev tab. The same method is used when clicking prev button
- __changeTab(oldIndex, newIndex)__: Navigates from one tab to another. Note that this method does
  not trigger validation methods. Use it with caution!

## Scoped Slots

Form-wizard exposes multiple scoped slots which can be used to customize some parts of the wizard.

- __nextTab__: will go to the next tab/step when called

- __prevTab__: ill got to the prev tab/step when called

- __activeTabIndex__: current active tab index

- __isLastStep__: boolean to tell whether it's the last step or not

- __fillButtonStyle__: object with styles for wizard-buttons (contains background and color passed

  through wizard props)