/* store.js - the central store managing state of application */
import Vue from 'vue'
import Vuex from 'vuex'
import server from './services/ServerInterface'

Vue.use(Vuex)

/* export const store = new Vuex.Store({ */
const store = new Vuex.Store({
  state: {
    user: null
  },
  getters: {
    getUser: state => {
      return state.user
    }
  },
  mutations: {
    setUser: state => {
      state.user = server.getUserRecord()
    }
  },
  actions: {
    setUser: context => {
      context.commit('setUser')
    }
  }
})

export default store

