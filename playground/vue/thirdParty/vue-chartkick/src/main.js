/* The main javascript for Application, sets up all dependencies, loads application */
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue' // Bootstrap 4
import VueGoodTable from 'vue-good-table' // VueGoodTable

import VueChartkick from 'vue-chartkick' // Charting
import Chart from 'chart.js'

Vue.use(BootstrapVue)
Vue.use(VueGoodTable)
Vue.use(VueChartkick, { adapter: Chart })
Vue.use(VueRouter)

import routes from './routes'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'
import server from './services/ServerInterface'

server.initializeServerInterface()

// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history'
})

// Check before each page load whether the page requires authentication
// if it does check whether the user is signed into the web app or
// redirect to the sign-in page to enable them to sign-in
router.beforeEach((to, from, next) => {
  /* console.log('**router.beforeEach called') */
  const currentUser = server.getCurrentUser()
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !currentUser) {
    next('/sign-in')
  } else if (requiresAuth && currentUser) {
    next()
  } else {
    next()
  }
})

/* eslint-disable no-new */
// Wrap the vue instance in a server isReady function
// This stops the execution of the navigation guard 'beforeEach'
// method until the server initialization ends
server.isReady(function(onStateChange) {
  /* console.log('render Vue') */
  new Vue({
    el: '#app',
    store: store,
    router: router,
    render: h => h(App)
  })
})
