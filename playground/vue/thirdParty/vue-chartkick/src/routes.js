/* routes.js - The routing table for the application */

// Every Application has these so in base

// Read/Write Applications have these

// Application-specific Code
import Hello from './components/Hello'

import ProfileGroupDrilldown from './components/ProfileGroupDrilldown'

import SummaryAnalysis from './components/SummaryAnalysis'
import FacialAnalysis from './components/FacialAnalysis'
import PostAnalysis from './components/PostAnalysis'

import SummaryStatistics from './components/SummaryStatistics'
import SummaryGraph from './components/SummaryGraph'

import FacialStatistics from './components/FacialStatistics'
import FacialGraph from './components/FacialGraph'
import FacialPie from './components/FacialPie'

import PostStatistics from './components/PostStatistics'
import PostGraph from './components/PostGraph'
import PostPie from './components/PostPie'


const routes = [{
    path: '/',
    name: 'Hello',
    component: Hello
  },
  {
    path: '/profileGroupDrilldown',
    name: 'ProfileGroupDrilldown',
    component: ProfileGroupDrilldown,
    props: true,
    children: [{
        path: 'summaryAnalysis',
        component: SummaryAnalysis,
        props: true,
        children: [{
            path: 'summaryStatistics',
            component: SummaryStatistics
          },
          {
            path: 'summaryGraph',
            component: SummaryGraph
          }
        ]
      },
      {
        path: 'facialAnalysis',
        component: FacialAnalysis,
        props: true,
        children: [{
            path: 'facialStatistics',
            component: FacialStatistics
          },
          {
            path: 'facialGraph',
            component: FacialGraph
          },
          {
            path: 'facialPie',
            component: FacialPie
          }
        ]
      },
      {
        path: 'postAnalysis',
        component: PostAnalysis,
        props: true,
        children: [{
            path: 'postStatistics',
            component: PostStatistics
          },
          {
            path: 'postGraph',
            component: PostGraph
          },
          {
            path: 'postPie',
            component: PostPie
          }
        ]
      },
    ]
  }

]

export default routes
