import Vue from 'vue'
import Router from 'vue-router'
import Hello from './components/Hello'
import FacialPie from './components/FacialPie'
import FacialGraph from './components/FacialGraph'
import FacialAnalysis from './components/FacialAnalysis'
import FacialStatistics from './components/FacialStatistics'

const routes = [{
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
        path: '/facialAnalysis',
        component: FacialAnalysis,
        children: [{
            path: 'facialStatistics',
            component: FacialStatistics
          },
          {
            path: 'facialGraph',
            component: FacialGraph
          },
          {
            path: 'facialPie',
            component: FacialPie
          }
        ]
      },
  ]

export default routes
