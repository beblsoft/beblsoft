# Vue-chartjs Documentation

Easy and beautiful charts with Chart.js and Vue.js

Relevant URLs:
[Home](https://vue-chartjs.org/),
[NPM](https://www.npmjs.com/package/vue-chartjs),
[Github](https://github.com/apertureless/vue-chartjs)

## Getting Started

