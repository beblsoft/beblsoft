# vue2-date-rangepicker Documentation

vue2-daterange-picker is a component for vue made without any jQuery dependencies. It is heavily
inspired by [boostrap-daterangepicker](https://github.com/dangrossman/daterangepicker).

Relevant URLs:
[Home](https://innologica.github.io/vue2-daterange-picker/),
[Github](https://github.com/Innologica/vue2-daterange-picker),
[NPM](https://www.npmjs.com/package/vue2-daterange-picker)

## Installation

Install with npm:
```bash
npm i vue2-daterange-picker --save
```

## Basic Usage

Import and register component:
```js
import DateRangePicker from 'vue2-daterange-picker'
//you need to import the CSS manually (in case you want to override it)
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'

export default {
    components: { DateRangePicker },
}
```

Use component:
```html
<date-range-picker
        ref="picker"
        :opens="opens"
        :locale-data="{ firstDay: 1, format: 'DD-MM-YYYY HH:mm:ss' }"
        :minDate="minDate" :maxDate="maxDate"
        :singleDatePicker="singleDatePicker"
        :timePicker="timePicker"
        :timePicker24Hour="timePicker24Hour"
        :showWeekNumbers="showWeekNumbers"
        :showDropdowns="showDropdowns"
        :autoApply="autoApply"
        v-model="dateRange"
        @update="updateValues"
        @toggle="checkOpen"
        :linkedCalendars="linkedCalendars"
        :dateFormat="dateFormat">
    <div slot="input" slot-scope="picker" style="min-width: 350px;">
        {{ picker.startDate | date }} - {{ picker.endDate | date }}
    </div>
</date-range-picker>
```

## Props

| Name                  | Type            | Default    | Description                                                                                                                                                                                                               |
|:----------------------|:----------------|:-----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| min-date              | String, Date    | null       | minimum date allowed to be selected                                                                                                                                                                                       |
| max-date              | String, Date    | null       | maximum date allowed to be selected                                                                                                                                                                                       |
| show-week-numbers     | Boolean         | false      | Show the week numbers on the left side of the calendar                                                                                                                                                                    |
| linked-calendars      | Boolean         | true       | Each calendar has separate navigation when this is false                                                                                                                                                                  |
| single-date-picker    | Boolean         | false      | Allows you to select only one date (instead of range). This will hide the ranges with different start/end                                                                                                                 |
| show-dropdowns        | Boolean         | false      | Show the dropdowns for month and year selection above the calendars                                                                                                                                                       |
| time-picker           | Boolean         | false      | Show the dropdowns for time (hour/minute) selection below the calendars                                                                                                                                                   |
| time-picker-increment | Number          | 5          | Determines the increment of minutes in the minute dropdown                                                                                                                                                                |
| time-picker24-hour    | Boolean         | true       | Use 24h format for the time                                                                                                                                                                                               |
| time-picker-seconds   | Boolean         | false      | Allows you to select seconds except hour/minute                                                                                                                                                                           |
| auto-apply            | Boolean         | false      | Auto apply selected range. If false you need to click an apply button                                                                                                                                                     |
| locale-data           | Object          | *see below | Object containing locale data used by the picker. See example below the table                                                                                                                                             |
| date-range            | undefined       | null       | This is the v-model prop which the component uses.                                                                                                                                                                        |
| ranges                | Object, Boolean | *see below | You can set this to false in order to hide the ranges selection. Otherwise it is an object with key/value. See below                                                                                                      |
| opens                 | String          | center     | which way the picker opens - "center", "left" or "right"                                                                                                                                                                  |
| date-format           | Function        |            | function(classes, date) - special p"classes" - the classes that the component's logic has defined,"date" - tha date currently processed. You should return Vue class object which should be applied to the date rendered. |
| always-show-calendars | Boolean         | true       | *WIP*                                                                                                                                                                                                                     |

Sample Locale data:
```js
{
    direction: 'ltr',
    format: moment.localeData().longDateFormat('L'),
    separator: ' - ',
    applyLabel: 'Apply',
    cancelLabel: 'Cancel',
    weekLabel: 'W',
    customRangeLabel: 'Custom Range',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek()
}
```

Default Ranges Object:
```js
{
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'This month': [moment().startOf('month'), moment().endOf('month')],
    'This year': [moment().startOf('year'), moment().endOf('year')],
    'Last week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
    'Last month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
}
```

## Events

### toggle

Emits whenever the picker opens/closes

Parameters
- (boolean) open - the current state of the picker
- (Function) togglePicker - function (show, event) which can be used to control the picker. where "show" is the new state and "event" is boolean indicating whether a new event should be raised

### update

Emits when the user selects a range from the picker and clicks "apply" (if autoApply is true).

Parameters:
- (json) value - json object containing the dates: {startDate, endDate}

## Slots

### input

Allows you to change the input which is visible before the picker opens

```js
@param {Date} startDate - current startDate
@param {Date} endDate - current endDate
@param {object} ranges - object with ranges
```

### ranges

Allows you to change the range

```js
@param {Date} startDate - current startDate
@param {Date} endDate - current endDate
@param {object} ranges - object with ranges
```