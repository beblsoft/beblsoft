// vue.config.js

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        // @/ is an alias to src/
        data: `@import "~@/styles.scss";`
      },
    }
  }
}
