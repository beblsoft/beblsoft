# Sass Projects Documentation

## sass-app

A Vue CLI 3 based project that incorporates sass.

To setup the project:

```bash
# Make sure Vue CLI 3 is installed
# Accept defaults
vue create sass-app;

cd sass-app;

# Install sass
npm install -D sass-loader sass

# Updated files
# - src/styles.scss
#   Added global styles
#
# - vue.config.js
#   Added css object to load global styles
#   This allows them to be available to vue components
#
# - src/HelloWorld.vue
#   <style> script is written in scss and leverages the global styles
```

To run the project:

```bash
cd sass-app

# Install dependencies
npm install

# Compiles and hot-reloads for development
npm run serve

# Compiles and minifies for production
npm run build
```

