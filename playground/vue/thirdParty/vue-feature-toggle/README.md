# vue-feature-toggle Documentation

Vue-Feature-Toggle enables advanced feature-toggling within Vue applications.

Relevant URLs:
[NPM](https://www.npmjs.com/package/vue-feature-toggle),
[GitHub](https://github.com/bassdman/vue-feature-toggle),
[Examples](https://github.com/bassdman/vue-feature-toggle/tree/master/examples),
[Martin Fowler on Feature Toggles](https://martinfowler.com/articles/feature-toggles.html)

## Installation

```bash
# Via NPM
npm install --save vue-feature-toggle
```

## The Problem

```html
<content-area>
    <!-- Show important debugging information for testmode -->
    <testmode-nav v-if="testmode"></testmode-nav>

    <!-- That's the old one, in a few days the new one, commented out here will be released
        <left-nav-new></left-nav-new>
    -->
    <left-nav></left-nav>

    <!-- Every shop has a slider with amazing foodinfo on the startpage-->
    <startpage-slider-de ref="food/bratwurst" v-if="shop == 'de'"></startpage-slider-de>
    <startpage-slider-en ref="food/fishnchips" v-if="shop == 'en'"></startpage-slider-en>
    <startpage-slider-fr ref="food/croissant" v-if="shop == 'fr'"></startpage-slider-fr>

    <footer-new></footer-new>
    <!--
    New footer just went live. When there are some problems, we rollback and comment out the new footer and uncomment the old one
    <footer-old></footer-old> -->
</content-area>
```

The problem here is the view-logic is spread in `.html` and `.js` files and if a change is needed,
you have to make changes all over the entire system.

## The Solution

Feature-toggle. All view-logic is placed in one place. This can be a config file, a webservice,
or a tool with a User interface. When you want to change a visibilty rule, you just have to update
the UI config, and no developer is needed.

## Usage

Example `App.vue`:
```html
<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <h1>Hello World!</h1>
    <feature name="feature1">This is "Feature1"</feature>
    <feature name="feature2">This is "Feature2"</feature>
    <feature name="feature2" variant="new">This is "Feature2 Variant New"</feature>
  </div>
</template>

<script>
import * as feature from 'vue-feature-toggle';

feature.visibility('feature1', true);
feature.visibility('feature2', function () {
  return false;
});
feature.showLogs()

export default {
  name: 'app',
  components: { feature }
}

</script>
```

## Features

### Basic visibility

Enable and disable:
```js
// Shows Feature1
feature.visibility('feature1',true);

// You can also write it like this
feature.visibility('feature1',function (rule) {
  //here would be some more complex logic, in this example we keep it simple
  return true;
});
```

Enable and disable variants:
```js
/*
  shows all features with name feature2, in this case:
  <feature name="feature2"/>
  <feature name="feature2" variant="new"/>
 */
feature.visibility('feature2', function (rule) {
  return true;
});

/*
  This overwrites the rule above for "feature2", variant "new"
  <feature name="feature2"/> -> shown
  <feature name="feature2" variant="new"/> -> hidden
*/
feature.visibility('feature2','new', function (rule) {
  return false;
});
```

### Default Visibility

Set default visibility for all features:
```js
// All features are shown
feature.defaultVisibility(function(rule){
    return true;
});
```

### Required Visibilty

This rule is always executed, before the other rules. When it returns false the other rules are
ignored.
```js
let globalConfig = { "feature1" : false, "feature2" : true };
feature.requiredVisibility(function(rule){
    // feature1 will never be shown, as it fails required visibility
    // feature2 will never be shown
    return globalConfig[rule.name] === true;
});
```

### Show Logs

Print logs explaining why something feature is visible vs. invisible.
```js
feature.showLogs(); //or feature.showLogs(true);
```

### Log

Log a custom message when `showLogs() == true`:
```js
feature.log("Here's my custom message");
```

