# BootstrapVue Documentation

BoostrapVue allows developers to build responsive, mobile-first projects on the web using
[Vue.js](https://vuejs.org/) in combination with [Bootstrap](https://getbootstrap.com/).

Relevant URLs:
[Home](https://bootstrap-vue.js.org/),
[Github](https://github.com/bootstrap-vue/bootstrap-vue)

## Getting Started

### Project Setup

Installation
```bash
# Install with npm
npm install --save vue boostrap-vue bootstrap
```

Application entry point:
```js
// app.js
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
```

Import bootstrap scss:
```scss
// custom.scss

// Define override variables
@import 'node_modules/bootstrap/scss/bootstrap';
@import 'node_modules/bootstrap-vue/src/index.scss';
```

```js
// app.js
import 'custom.scss'
```

### Browser Setup

Add the Boostrap and BootstrapVue CSS URLs in HTML `<head>` secion as follows.
```html
<!-- Add this to <head> -->

<!-- Load required Bootstrap and BootstrapVue CSS -->
<link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css" />

<!-- Load polyfills to support older browsers -->
<script src="https://polyfill.io/v3/polyfill.min.js?features=es2015%2CMutationObserver" crossorigin="anonymous"></script>

<!-- Load Vue followed by BootstrapVue -->
<script src="https://unpkg.com/vue@latest/dist/vue.min.js"></script>
<script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js"></script>
```

For example, see [starterTemplate.html](./components/starterTemplate.html)

## Components

### Alerts

Provide contextual feedback messages for typical user actions with alert messages.

For examples, see [alerts.html](./components/alerts.html)

### Badges

Small and adaptive tag for adding context to just about any content.

For examples, see [badges.html](./components/badges.html)

### Breadcrumbs

Indicate the current page's location within a navigational hierarchy.

For examples, see [breadcrumbs.html](./components/breadcrumbs.html)

### Buttons

Use `b-button` component for actions in forms, dialogs, and more. It includes support for a handful
of contextual variations, sizes, states, and more.

For examples, see [buttons.html](./components/buttons.html)

### Button Groups

Group a series of buttons together on a single line.

For examples, see [buttonGroups.html](./components/buttonGroups.html)

### Button ToolBars

Group a series of button-groups and/or input-groups together on a single line, with optional keyboard
navigation.

For examples, see [buttonToolbars.html](./components/buttonToolbars.html)

### Cards

A card is a flexible and extensible content container. It includes options for headers and footers,
a wide variety of content, contextual background colors, and powerful display options.

For examples, see [cards.html](./components/cards.html)

### Carousels

The carousel is a slideshow for cycling through a series of content, built with CSS 3D transforms. It
works with a series of images, text, or custoom markup.

For examples, see [carousels.html](./components/carousels.html)

### Collapses

Easily toggle visibility of almost any content on your pages. It includes support for making accordions.

For examples, see [collapses.html](./components/collapses.html)

### Dropdowns

Dropdowns are toggleable, contextual overlays for displaying list of links and actions in a dropdown
menu format.

For examples, see [dropdowns.html](./components/dropdowns.html)

### Embeds

Create responsive video or slideshow embeds based on the width of the parent by creating an instrinsic
ration that scales on any device.

For examples, see [embeds.html](./components/embeds.html)

### Forms

BootstrapVue form components and helper components optionally support inline form styles and validation
states. Pair them with form control components for an easy customized, and responsive layout with
a consistent look and feel.

For examples, see [forms.html](./components/forms.html)

### Form Checkboxes

For cross browser consistency, `<b-form-checkbox-group>` and `<b-form-checkbox>` use Bootstrap's
custom checkbox input to replace the browser's default. It is built on top of semantic and accessible
markup, so it is a solid replacement for the default checkbox input.

For examples, see [formCheckboxes.html](./components/formCheckboxes.html)

### Form Files

Customized, cross-browser consistent, file input control that supports single file, multiple files,
and directory upload.

For examples, see [formFiles.html](./components/formFiles.html)

### Form Groups

The `<b-form-group>` component is the easiest to add some structure to forms. Its purpose is to
pair form controls with a legend or label, and to provide help text and invalid/valid feedback text,
as well as visual (color) contextual feedback.

For examples, see [formGroups.html](./components/.html)

### Form Inputs

Create various type inputs such as: text, password, number, url, email, search, range, date, and more.

For examples, see [formInputs.html](./components/formInputs.html)

### Form Radios

For cross browser consistency, `<b-form-radio-group>` and `<b-form-radio>` use Bootstrap's custom radio
input to replace the browser default radio input. It is built on top of semantic and accessible markup,
so it is a solid replacement for the default radio input.

For examples, see [formRadios.html](./components/formRadios.html)

### Form Selects

Boostrap custom `<select>` using custom styles. Optionally specify options based on an array, array
of objects, or an object.

For examples, see [formSelects.html](./components/formSelects.html)

### Form Textareas

Create multi-line inputs with support for auto height sizing, minimum and maximum number of rows,
and contextual states.

For examples, see [formTextareas.html](./components/formTextareas.html)

### Images

The `<b-img>` component gives image responsive behavior.

For examples, see [images.html](./components/images.html)

### Input Groups

Easily extend form controls by adding text, buttons, or button groups on either side of textual inputs.

For examples, see [inputGroups.html](./components/inputGroups.html)

### Jumbotrons

A lightweight, flexible component that can optionally extend the entire viewport to showcase key
marketing messages on your site.

For examples, see [jumbotrons.html](./components/jumbotrons.html)

### Grids

Use the powerful mobile-first flexbox grid (via the `<b-container>`, `<b-row>`, `<b-form-row`, and `<b-col`
components) to build layouts of all shapes and sizes thanks to a twelve column system, five default
responsive tiers, CSS Sass variables and mixins, and dozens of predefined classes.

For examples, see [grids.html](./components/grids.html)

### Links

Use BootstrapVue's custom `b-link` component for generating a standard `<a>` link or `<router-link>`.
`<b-link>` supports the `disabled` state and `click` event propagation.

For examples, see [links.html](./components/links.html)

### List Groups

List groups are a flexible and powerful component for displaying a series of content. List group items
can be modified to support just about any content within. They can also be used as navigation via various
props.

For examples, see [listGroups.html](./components/listGroups.html)

### Media

The media object helps build complex and repetitive components where some media is positioned alongside
content that doesn't wrap around said media.

For examples, see [media.html](./components/media.html)

### Modals

Modals are streamlined, but flexible dialog prompts powered by JavaScript and CSS. They support a number
of use cases for user notification to completely custom content and feature a handful of helpful
sub-components, sizes, variants, accessibility, and more.

For examples, see [modals.html](./components/modals.html)

### Navs

Navigation available in Bootstrap share general markup and styles, from the base `<b-nav>` class to the
`active` and `disabled` states. Swap modifier props to switch between each style.

For examples, see [navs.html](./components/navs.html)

### Navbars

The component `<b-navbar>` is a wrapper that positions branding, navigation, and other elements into
a concise header. It's easily extensible and thanks to the `<b-collapse>` component, it can easily
integrate responsive behaviors.

For examples, see [navbars.html](./components/navbars.html)

### Pagination

Quick first, previous, next, last, and page buttons for pagination control of another component (such
as `<b-table>` or lists)

For examples, see [pagination.html](./components/pagination.html)

### Pagination Navs

Quick first, previous, next, last and page buttons for navigation based pagination, supporting regular
links or router links.

For examples, see [paginationNavs.html](./components/paginationNavs.html)

### Popovers

The popover feature, which provies a tooltip-like behavior, can be easily applied to any interactive
element viat the `<b-popover>` component or `<v-b-popover>` directive.

For examples, see [popovers.html](./components/popovers.html)

### Progress

Use custom progress component for displaying simple or complex progress bars, featuring support for
horizontally stacked bars, animated backgrounds, and text labels.

For examples, see [progress.html](./components/progress.html)

### Spinners

The `<b-spinner>` component can be used to show the loading state in your projects. They're rendered
only with basic HTML and CSS as a lightweight Vue functional component. Their appearance, alignment,
and sizing can be easily customized with a few built-in props and/or Bootstrap V4 utility classes.

For examples, see [spinners.html](./components/spinners.html)

### Tables

Tables are used for displaying tabular data, `<b-table>` supports pagination, filtering, sorting,
custom rendering, various style options, events, and asynchronous data.

For examples, see [tables.html](./components/tables.html)

### Tabs

Create a widget of tabbable panes of local content. The tabs component is built upon navs and cards
internally, and provides full keyboard navigation control to the tabs.

For examples, see [tabs.html](./components/tabs.html)

### Toasts

Push notifications to your visitors with the `<b-toast>` and `<b-toaster>`, lightweight components
which are easily customizable for generating alert messages.

For examples, see [toasts.html](./components/toasts.html)

### Tooltips

Easily add tooltips to elements or components via the `<b-tooltip>` or `<v-b-tooltip>` directive.

For examples, see [tooltips.html](./components/tooltips.html)

## Directives

### Popovers

Popovers can be triggered by hovering, focusing, or clicking an element, and can contain both content
and a title heading. Popovers are tooltips on steroids.

For examples, see [popovers.html](./directives/popovers.html)

### Scollspies

Automatically update Boostrap navigation or list group components based on scroll position to indicate
which link is currently active in the viewport.

For examples, see [scollspies.html](./directives/scollspies.html)

### Tooltips

Tooltips can be triggered by hovering, focusing, or clicking an element. Use the `v-b-tooltip` directive
on any element or component where you would like the tooltip to appear.

For examples, see [tooltips.html](./directives/tooltips.html)

## Reference

### Color Variants

Can use Component `variant` syntax to set color or native boostrap classes to set color.

### Component img src resolving

vue-loader automatically converts project relative src attributes on `<img>` tags, but doesn't
automatically for BootstrapVue custom components that accept image src url tags.

To fix this, manually require the image as follows:
```html
<b-img :src="require('../static/picture.jpg')"></b-img>

<b-card-img :img-src="require('../static/picture.jpg')"></b-card-img>
```

### Starter Template

```html
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Load required Bootstrap and BootstrapVue CSS -->
  <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" />
  <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css" />

  <!-- Load polyfills to support older browsers -->
  <script src="https://polyfill.io/v3/polyfill.min.js?features=es2015%2CMutationObserver"
    crossorigin="anonymous"></script>

  <!-- Load Vue followed by BootstrapVue -->
  <script src="https://unpkg.com/vue@latest/dist/vue.min.js"></script>
  <script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js"></script>
  <title>Base</title>
</head>

<body>
  <div id="app">
    <h1 class="red">Hello World!</h1>
  </div>
</body>

<script>
var app = new Vue({
  el: '#app'
})

</script>
<style>
.red {
  text-decoration: underline;
  text-decoration-color: red;
}
</style>
</html>
```

### Utility Classes

Use [Bootstrap4 Utility Classes](https://getbootstrap.com/docs/4.3/utilities/).

## Settings

BootstrapVue provides a few options for customizing component default values, and more.

### Default Configuration

```js
{
  "breakpoints": [
    "xs",
    "sm",
    "md",
    "lg",
    "xl"
  ],
  "BAlert": {
    "dismissLabel": "Close",
    "variant": "info"
  },
  "BBadge": {
    "variant": "secondary"
  },
  "BButton": {
    "variant": "secondary"
  },
  "BButtonClose": {
    "textVariant": null,
    "ariaLabel": "Close"
  },
  "BCardSubTitle": {
    "subTitleTextVariant": "muted"
  },
  "BCarousel": {
    "labelPrev": "Previous Slide",
    "labelNext": "Next Slide",
    "labelGotoSlide": "Goto Slide",
    "labelIndicators": "Select a slide to display"
  },
  "BDropdown": {
    "toggleText": "Toggle Dropdown",
    "variant": "secondary",
    "splitVariant": null
  },
  "BFormFile": {
    "browseText": "Browse",
    "placeholder": "No file chosen",
    "dropPlaceholder": "Drop files here"
  },
  "BFormText": {
    "textVariant": "muted"
  },
  "BImg": {
    "blankColor": "transparent"
  },
  "BImgLazy": {
    "blankColor": "transparent"
  },
  "BJumbotron": {
    "bgVariant": null,
    "borderVariant": null,
    "textVariant": null
  },
  "BListGroupItem": {
    "variant": null
  },
  "BModal": {
    "titleTag": "h5",
    "size": "md",
    "headerBgVariant": null,
    "headerBorderVariant": null,
    "headerTextVariant": null,
    "headerCloseVariant": null,
    "bodyBgVariant": null,
    "bodyTextVariant": null,
    "footerBgVariant": null,
    "footerBorderVariant": null,
    "footerTextVariant": null,
    "cancelTitle": "Cancel",
    "cancelVariant": "secondary",
    "okTitle": "OK",
    "okVariant": "primary",
    "headerCloseLabel": "Close"
  },
  "BNavbar": {
    "variant": null
  },
  "BNavbarToggle": {
    "label": "Toggle navigation"
  },
  "BProgress": {
    "variant": null
  },
  "BProgressBar": {
    "variant": null
  },
  "BSpinner": {
    "variant": null
  },
  "BTable": {
    "selectedVariant": "primary",
    "headVariant": null,
    "footVariant": null
  },
  "BToast": {
    "toaster": "b-toaster-top-right",
    "autoHideDelay": 5000,
    "variant": null,
    "toastClass": null,
    "headerClass": null,
    "bodyClass": null,
    "solid": false
  },
  "BToaster": {
    "ariaLive": null,
    "ariaAtomic": null,
    "role": null
  },
  "BTooltip": {
    "delay": 0,
    "boundary": "scrollParent",
    "boundaryPadding": 5
  },
  "BPopover": {
    "delay": 0,
    "boundary": "scrollParent",
    "boundaryPadding": 5
  }
}
```

### Customizing

When you `Vue.use(BootstrapVue)`, you can optionally pass a configuration object which specifies
new values to replace the default values.

For example:
```js
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue, {
  BAlert: { variant: 'danger' },
  BButton: { variant: 'primary' }
})
```

When importing individual components use the `BVConfigPlugin` to customize as follows:
```js
// Import BootstrapVue configuration helper plugin and Individual components
import { BVConfigPlugin, BAlert, BButton, BRow, BCol } from 'bootstrap-vue'

// Supply complete config to the BVConfig helper plugin
Vue.use(BVConfigPlugin, {
  breakpoints: ['xs', 'sm', 'lg', 'xl', 'xxl'],
  BAlert: { variant: 'danger' },
  BButton: { variant: 'primary' }
})

// Then install components globally
Vue.component('b-alert', BAlert)
Vue.component('b-button', BButton)
Vue.component('b-row', BRow)
Vue.component('b-col', BCol)

// Or register components as local to your custom component
export default {
  name: 'MyComponent',
  components: {
    BAlert,
    BButton,
    BRow,
    BCol
  }
  // ...
}
```

### Disabling Warnings

BootstrapVue will warn (via `console.warn`) when you try and use a deprecated prop, or pass an invalid
value to certain props. These warning are provided to help you ensure that your application is using
the correct props and values.

In some cases, you may want to disable these warnings. You can do so as follows:
```js
process.env.BOOTSTRAP_VUE_NO_WARN = true
```
