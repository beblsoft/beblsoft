# BootstrapVue Projects Documentation

## components

Single html files that demonstrate BootstrapVue components.

To run open each html file in chrome.

## directives

Single html files that demonstrate BootstrapVue directives.

To run open each html file in chrome.