# vueGoodTable documentation

Vue Good Table demonstrations. For example: Filtering, sorting columns. Handles different number
of rows per page

## Setup fancy table, aka a vue-good-table

1. Install the code:
    ```bash
    npm install --save vue-good-table@beta
    ```

2. Add import within main.js.

    ```javascript
    import VueGoodTable from 'vue-good-table';

    // import the styles
    import 'vue-good-table/dist/vue-good-table.css'

    Vue.use(VueGoodTable);
    ```

3. Add FancyTable model and invoke it.

4. Fix eslintrc.js to make less strict, as needed.

## Basic Commandline for Sample App

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## Below is a file tree with text indicating what the files are used for.

```text
├── .babelrc - bable setup
├── build - standard build files produced by vue cli, note changes in webpack.dev.conf.js to force auto reload
│   ├── build.js
│   ├── check-versions.js
│   ├── dev-client.js
│   ├── dev-server.js
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   ├── webpack.prod.conf.js
│   └── webpack.test.conf.js
├── config - standard configuration files produced by vue cli
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── test.env.js
├── .editorconfig - editor customization
├── .eslintignore - files to not do ES lint on
├── .eslintrc.js - where custom ES lint configuration is
├── .gitignore - git overrides
├── index.html - start of program, pull in dependencies like bootstrap
├── package.json - contains all project dependencies, scripts, metadata about application
├── package-lock.json - automatically generated exact tree of dependencies generated.
├── .postcssrc.js - a file defining module.exports that is ignored, used to add JS logic to generate your config.
├── README.md - this file, documentation for application
├── src
│   ├── App.vue - icon and navigation to route to, loads Hello page (Home page)
│   ├── components
│   │   ├── VueGoodTable.vue - demo of basic table integration
│   │   ├── Home.vue - Base view of application, starting point once App.vue executes, '/'
│   ├── main.js - instantiates Vue from index, which loads App Component and instantiates the routes
│   ├── routes - the routes for the application
├── static
│   ├── css
│   │   └── bootstrap.min.css - css for bootstrap
│   └── img
│       ├── favicon.ico - icon used in tab when invoked (referenced in index.html)
│       └── logo.png - large logo used in sample (referenced in App.vue, Login.vue, Component404.vue)
└── test - vue auto generated test files
    ├── e2e
    │   ├── custom-assertions
    │   │   └── elementCount.js
    │   ├── nightwatch.conf.js
    │   ├── runner.js
    │   └── specs
    │       └── test.js
    └── unit
        ├── index.js
        ├── karma.conf.js
        └── specs
            └── Hello.spec.js
```