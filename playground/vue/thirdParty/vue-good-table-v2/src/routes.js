import Home from './components/Home'
import Basic from './components/Basic'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '/basic',
    name: 'Basic',
    component: Basic
  }
]

export default routes
