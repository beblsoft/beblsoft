// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routes from './routes'
import VueGoodTablePlugin from 'vue-good-table' //added for VueGoodTable,globally

// import the styles for VueGoodTable
import 'vue-good-table/dist/vue-good-table.css'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VueGoodTablePlugin) // added for  VueGoodTable

// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  scrollBehavior: function(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

/* temporary user always logged in, set in some global place */
var userNotLoggedIn = false
router.beforeEach((to, from, next) => {
  if (to.meta.auth && userNotLoggedIn) {
    next('/not-logged-in/login')
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  template: '<App/>',
  components: {
    App
  }
})
