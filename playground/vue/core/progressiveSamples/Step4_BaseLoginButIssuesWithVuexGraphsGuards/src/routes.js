/* routes.js - The routing table for the application */

// Every Application has these so in base
import Component404 from './components/baseApp/Component404'
import About from './components/baseApp/About'
import PrivacyPolicy from './components/baseApp/PrivacyPolicy'

// Read/Write Applications have these
import SignIn from './components/login/SignIn.vue'
import SignUp from './components/login/SignUp.vue'
import ForgotPassword from './components/login/ForgotPassword.vue'
import ResetPassword from './components/login/ResetPassword.vue'
import ConfirmAccount from './components/login/ConfirmAccount.vue'
import CheckEmail from './components/login/CheckEmail.vue'
import SomethingWentWrong from './components/login/SomethingWentWrong.vue'
import Banner from './components/login/Banner.vue'

// Application-specific Code
import Dashboard from './components/Dashboard'
import SMAnalysis from './components/SMAnalysis.vue'
import AddUser from './components/AddUser'

const routes = [{
    path: '/',
    name: 'Banner',
    component: Banner
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/SMAnalysis',
    name: 'SMAnalysis',
    component: SMAnalysis,
    props: true
  }, {
    path: '/About',
    name: 'About',
    component: About
  }, {
    path: '/PrivacyPolicy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  }, {
    path: '/addUser',
    name: 'AddUser',
    component: AddUser
  },
  // login additions below and 404 fixup
  {
    path: '/sign-in',
    name: 'signIn',
    component: SignIn
  }, {
    path: '/forgotPassword',
    name: 'forgotPassword',
    component: ForgotPassword
  }, {
    path: '/resetPassword',
    name: 'resetPassword',
    component: ResetPassword
  }, {
    path: '/sign-up',
    name: 'signUp',
    component: SignUp
  }, {
    path: '/confirmAccount',
    name: 'confirmAccount',
    component: ConfirmAccount
  }, {
    path: '/checkEmail',
    name: 'checkEmail',
    component: CheckEmail
  }, {
    path: '/somethingWentWrong',
    name: 'somethingWentWrong',
    component: SomethingWentWrong
  }, {
    path: '/404',
    name: 'Component404',
    component: Component404
  }, {
    path: '*',
    redirect: '/404'
  }
]

export default routes
