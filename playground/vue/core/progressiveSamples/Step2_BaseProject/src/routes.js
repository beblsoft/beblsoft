import Home from './components/Home'
import Component404 from './components/Component404'
import About from './components/About'
import PrivacyPolicy from './components/PrivacyPolicy'
import VVCDoubleLine from './components/VVCDoubleLine.vue'
import HorzTabs from './components/HorzTabs.vue'
import VVCBar from './components/VVCBar.vue'
import VVCDoughnutTabs from './components/VVCDoughnutTabs.vue'
import VVCPies from './components/VVCPies.vue'
import VVCMultiple from './components/VVCMultiple.vue'
import VueGoodTable from './components/VueGoodTable'
import Form from './components/Form'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '*',
    name: 'Component404',
    component: Component404
  }, {
    path: '/vvcDoubleLine',
    name: 'vvcDoubleLine',
    component: VVCDoubleLine
  }, {
    path: '/horzTabs',
    name: 'horzTabs',
    component: HorzTabs,
    props: true
  },
  {
    path: '/VVCBar',
    name: 'vvcBar',
    component: VVCBar
  },
  {
    path: '/vvcDoughnutTabs',
    name: 'vvcDoughnutTabs',
    component: VVCDoughnutTabs
  },
  {
    path: '/vvcPies',
    name: 'vvcPies',
    component: VVCPies
  },
  {
    path: '/vvcMultiple',
    name: 'vvcMultiple',
    component: VVCMultiple
  },
  {
    path: '/About',
    name: 'About',
    component: About
  },
  {
    path: '/PrivacyPolicy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  },
  {
    path: '/vuegoodtable',
    name: 'VueGoodTable',
    component: VueGoodTable
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  }
]

export default routes
