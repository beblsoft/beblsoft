export default class ResponseObject {
  BS_SUCCESS = 1;
  BS_FAILURE = 0;

  constructor() {
    this.status = this.BS_SUCCESS
    this.errorString = 'none'
    this.payload = null
  }
}
