import axios from 'axios'
import ResponseObject from './ResponseObject'

export default class ServerInterface {

  postRequest(url) {
    console.log('ServerInterface: call to postRequest')
    return axios.get(url)
  }

  static getRequest(url) {
    console.log('ServerInterface: call to getRequest')
    var responseObj = new ResponseObject()
    var promise = new Promise(function(resolve, reject) {
      axios.get(url)
        .then((response) => {
          console.log('Axios Data Returned:' + JSON.stringify(response.data[0]))
          responseObj.payload = response
          resolve(responseObj)
        })
        .catch((error) => {
          console.log(error)
          responseObj.status = ResponseObject.BS_FAILURE
          responseObj.error = 'bad error'
          reject(responseObj)
        })
    })
    return promise
  }
}
