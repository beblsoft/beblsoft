# Progressive Samples Documentation

A series of three examples designed to faciliate fast Vue integration. The following three examples
exploit the best of Vue:  `Vue`, `Vue-Cli`, `Vue-Router`, `Bootstrap-Vue`, `Vuex`, `Vue-Good-Table`,
`Simple Forms`

## Step1_SingleFileDemos

Vue simple, single self-contained .html files (Great base for playing)

To Run
- Enter Directory: `cd ./progressiveSamples/Step1_SingleFileDemos`
- Open html files in browser

## Step2_BaseProject

Vue with WebPack Hot Reload, Vue Routing, and an NavBar Full of Components demontrating Controls
including: Cards, Tabs, Tables, HorzVertTabs, Charts, Embedded Charts, Multiple Charts with Grid,
Simple Form (Great base for read-only application)

Relevant URLs:
[ChartKick](https://www.chartkick.com/vue),
[BootstrapVue](https://bootstrap-vue.js.org/docs/)

Component Files
- `About.vue`: Sample about component
- `Component404.vue`: Sample bad route component
- `Footer.vue`: Sample Beblsoft footer component
- `Form.vue`: Sample simple form component
- `Header.vue`: Sample Beblsoft header component
- `Home.vue`: Basic home showing cards with drilldown
- `HorzTabs.vue`: Drill down to Horizonal Tabs from Home
- `PrivacyPolicy.vue`: Sample Beblsoft Privacy Policy
- `VertTabsDrilldown.vue`: Vertical Tabs Drilldown from HorzTabs
- `VueGoodTable.vue`: Sample simple table
- `VVCBar.vue`: Sample bar graph
- `VVCDoubleLine.vue`: Sample doubleline graph
- `VVCDoughnutTabs.vue`: Sample doughnut tabs
- `VVCMultiple.vue`: Sample use of embedded graph components
- `VVCPies.vue`: Sample use of embedded pies

To Run
- Enter Directory: `./progressiveSamples/Step2_BaseProject`
- Installation: `npm install`
- Server: `npm run dev`. Open browser to http://localhost:8080

## Step3_BaseLoginProject

Vue with conditional routing for login/logout. (Great base forread/write application)

Relevant URLs:
[Firebase](https://firebase.google.com/)

Component Files
- `/baseApp/About.vue`: Sample about component
- `/baseApp/Component404.vue`: Sample bad route component
- `/baseApp/Footer.vue`: Sample Beblsoft footer component
- `/baseApp/Header.vue`: Sample Beblsoft header component
- `/baseApp/PrivacyPolicy.vue`: Sample Beblsoft Privacy Policy
- `/login/Banner.vue`: Sample initial Banner page for Application
- `/login/CheckEmail.vue`: Sample component to go check Email
- `/login/ConfirmAccount.vue`: Sample component to confirm account
- `/login/ForgotPassword.vue`: Sample form to get info if forgot passoword
- `/login/ResetPassword.vue`: Sample component to reset the password
- `/login/SignIn.vue`: Sample sign in form
- `/login/SignUp.vue`: Sample sign up form
- `/login/SomethingWentWrong.vue`: Sample SomethingWentWrong component
- `AddUser.vue`: Sample simple form component only visible by Admin User, BeblsoftMgr
- `Dashboard.vue`: Sample Dashboard for an application
- `SMAnalysis.vue`: Sample Drilldown for Dashboard
- `SMDrilldown.vue`: Sample Drilldown for SMAlysis
- `VueGoodTable.vue`: Sample simple table component
- `VVCBar.vue`: Sample bar graph

To Run
- Enter Directory: `./progressiveSamples/Step3_BaseLoginProject`
- Install dependencies: `npm install`
- View in browser: `npm run dev`. Open chrome to http://localhost:8080
