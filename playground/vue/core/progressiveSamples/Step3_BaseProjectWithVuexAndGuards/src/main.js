/* main.js - binding for all application components, resources and initialization */
import Vue from 'vue'
import App from './App'

// Bootstrap 4 with Vue integration initalization
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

// Table implementation initialization
import VueGoodTable from 'vue-good-table'
Vue.use(VueGoodTable)

// Charting Initialization
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
Vue.use(VueChartkick, { adapter: Chart })

// Contained Store Initialization, must be before router initialization
import store from './store'

// Contained Router setup
import router from './router'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  router: router,
  template: '<App/>',
  components: {
    App
  }
})
