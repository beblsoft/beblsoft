/* store.js - the central store managing state of application */
import Vue from 'vue'
import Vuex from 'vuex'
// import server from './services/ServerInterface'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // user: null
    loggedInState: false
  },
  mutations: {
    userLogsIn(state) {
      state.loggedInState = true
    },
    userLogsOut(state) {
      state.loggedInState = false
    }
  }
    /* getUser: state => {
      return state.user
    }
  } */
  /* mutations: {
    setUser: state => {
      state.user = server.getUserRecord()
    }
  },
  actions: {
    setUser: context => {
      context.commit('setUser')
    }
  } */
})

export default store
