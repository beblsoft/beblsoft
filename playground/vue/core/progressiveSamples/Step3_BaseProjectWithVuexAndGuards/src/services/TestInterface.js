import ServerInterface from './ServerInterface'

export default class TestInterface {
  postTestRequest(url) {
    console.log('TestInterface: postRequest')
    const serverInterface = new ServerInterface()
    serverInterface.getRequest(url)
  }

  getTestRequest(url) {
    console.log('TestInterface: getRequest')
    return ServerInterface.getRequest(url)
  }
}
