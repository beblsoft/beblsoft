/* router.js - Centralized router setup with all application routes */
import Home from './components/Home'
import Component404 from './components/Component404'
import About from './components/About'
import PrivacyPolicy from './components/PrivacyPolicy'
import Vuex from './components/Vuex.vue'
import HorzTabs from './components/HorzTabs.vue'
import VVCBar from './components/VVCBar.vue'
import VVCDoughnutTabs from './components/VVCDoughnutTabs.vue'
import Guards from './components/Guards.vue'
import VVCMultiple from './components/VVCMultiple.vue'
import VueGoodTable from './components/VueGoodTable'
import Form from './components/Form'

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import store from './store'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '*',
    name: 'Component404',
    component: Component404
  },
  {
    path: '/vuex',
    name: 'vuex',
    component: Vuex
  },
  {
    path: '/guards',
    name: 'guards',
    component: Guards
  },
  {
    path: '/horzTabs',
    name: 'horzTabs',
    component: HorzTabs,
    props: true
  },
  {
    path: '/VVCBar',
    name: 'vvcBar',
    component: VVCBar
  },
  {
    path: '/vvcDoughnutTabs',
    name: 'vvcDoughnutTabs',
    component: VVCDoughnutTabs
  },
  {
    path: '/vvcMultiple',
    name: 'vvcMultiple',
    component: VVCMultiple
  },
  {
    path: '/About',
    name: 'About',
    component: About,
    meta: { requiresAuth: true }
  },
  {
    path: '/PrivacyPolicy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  },
  {
    path: '/vuegoodtable',
    name: 'VueGoodTable',
    component: VueGoodTable
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  }
]

// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  scrollBehavior: function(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    // console.log("made it here: " + store.state.loggedIn)
    if (!store.state.loggedInState) {
      next({
        path: '/vvcBar'
        // query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
