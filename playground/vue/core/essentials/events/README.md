# Vue Custom Events Documentation

Relevant URLs: [Docs](https://vuejs.org/v2/guide/components-custom-events.html)

## Event Names

The name of an emitted event must exactly match the name used to listen to that event.
Should __always use kebab-case for event names__.

For example: "mv-event" instead of "myEvent".


