# Vue Event Bus Documentation

An Event Bus allows you to emit an event in one component and listen for that event in another.

## Usage

Create the bus:
```javascript
// event-bus.js
import Vue from 'vue';
const EventBus = new Vue();
export default EventBus;
```

Emit an event in one component:
```javascript
// component-a.js
import Vue from 'vue';
import EventBus from './event-bus';
Vue.component('component-a', {
  methods: {
    emitMethod () {
       EventBus.$emit('EVENT_NAME', payLoad);
    }
  }
});
```

Register a listener in another component:
```javascript
// component-b.js
import Vue from 'vue';
import EventBus from './event-bus';
Vue.component('component-b', {
  mounted () {
    EventBus.$on('EVENT_NAME', function (payLoad) {
      //Handle Event
    });
  }
});
```

Pull all the components together into an application:
```javascript
// app.js
import Vue from 'vue';
import ComponentA form './component-a'
import ComponentB form './component-b';

new Vue({
  el: "#app",
  components: {
    ComponentA,
    ComponentB
  }
});
```
