# Vue Essentials Documentation

Relevant URLs:
[Docs](https://vuejs.org/v2/guide/index.html)

## Introduction

Vue is a progressive framework for building user interfaces. Unlike other monolithic frameworks,
Vue is designed from the ground up to be incrementally adoptable.

### Declarative Rendering

At the core of Vue.js is a system that enables DOM declarative rendering with easy syntax.
```html
<div id="app">
  {{ message }}
</div>
```

```js
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
})
```

Vue links the data and the DOM so that everything is __reactive__.

### Conditionals and Loops

Toggle the presence of an element:
```html
<div id="app-3">
  <span v-if="seen">Now you see me</span>
</div>
```
```js
var app3 = new Vue({
  el: '#app-3',
  data: {
    seen: true
  }
})
```

Loop over elements in an array:
```html
<div id="app-4">
  <ol>
    <li v-for="todo in todos">
      {{ todo.text }}
    </li>
  </ol>
</div>
```

```js
var app4 = new Vue({
  el: '#app-4',
  data: {
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue' },
      { text: 'Build something awesome' }
    ]
  }
})
```

### User Input

To let users interact with app, use the `v-on` directive to attach event listeners that invoke methods
on your Vue instance.

```html
<div id="app-5">
  <p>{{ message }}</p>
  <button v-on:click="reverseMessage">Reverse Message</button>
</div>
```

```js
var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Hello Vue.js!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})
```

### Components

Vue allows apps to decompose themselves into a tree of nested components. Each time a component is
used a new component instance is created. One might have a component for header, sidebar, content area, posts.

```text
  ---------------------------------------                  -----
  | ----------------------------------- |                  |   |
  | |                                 | |                  |   |
  | |                                 | |                  -----
  | ----------------------------------- |                    |
  | ---------------------- ------------ |                ---------
  | |  ----------------- | |   ---    | |   ---->       /    |    \
  | |  |               | | |   | |    | |           -----  -----  -----
  | |  |               | | |   ---    | |           |   |  |   |  |   |
  | |  |               | | |   ---    | |           |   |  |   |  |   |
  | |  ----------------- | |   | |    | |           -----  -----  -----
  | |  ----------------- | |   ---    | |                  --|--       |----------
  | |  |               | | |   ---    | |                 /     \      |    \     \
  | |  |               | | |   ---    | |              -----  -----   ----- ----- -----
  | |  |               | | |   | |    | |              |   |  |   |   |   | |   | |   |
  | |  ----------------- | |   ---    | |              |   |  |   |   |   | |   | |   |
  | ---------------------- ------------ |              -----  -----   ----- ----- -----
  ---------------------------------------
```

In Vue, a component is essentially a Vue instance with pre-defined options. Registering a component
in Vue is straightforward.
```javascript
// Define a new component called todo-item
Vue.component('todo-item', {
  template: '<li>This is a todo</li>'
})
```

Now you can compose it in another component's template:
```html
<ol>
  <!-- Create an instance of the todo-item component -->
  <todo-item></todo-item>
</ol>
```

However, this would render the same componenet in multiple places. One can pass props to the todo
component to make it dynamic.

```html
<div id="app-7">
  <ol>
    <!--
      Now we provide each todo-item with the todo object
      it's representing, so that its content can be dynamic.
      We also need to provide each component with a "key",
      which will be explained later.
    -->
    <todo-item
      v-for="item in groceryList"
      v-bind:todo="item"
      v-bind:key="item.id"
    ></todo-item>
  </ol>
</div>
```

```js
Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

var app7 = new Vue({
  el: '#app-7',
  data: {
    groceryList: [
      { id: 0, text: 'Vegetables' },
      { id: 1, text: 'Cheese' },
      { id: 2, text: 'Whatever else humans are supposed to eat' }
    ]
  }
})
```

## The Vue Instance

Every Vue application starts by creating a new Vue instance. Applications typically consist of root
Vue instance and a tree of reusable components. As a convention, we often use the variable `vm` (short
for ViewModel) to refer to the Vue instance.

Vue's design was inspired by the MVVM pattern.

### Data and Methods

When a Vue instance is created, it adds all the properties found in its `data` object to Vue's
__reactivity system__. When the values of those properties change, the view will "react", updating
to match the new values.

```js
// Our data object
var data = { a: 1 }

// The object is added to a Vue instance
var vm = new Vue({
  data: data
})

// Getting the property on the instance
// returns the one from the original data
vm.a == data.a // => true

// Setting the property on the instance
// also affects the original data
vm.a = 2
data.a // => 2

// ... and vice-versa
data.a = 3
vm.a // => 3
```

In addition to data properties, Vue instances expose a number of useful instance properties and
methods. These are prefixed with `$` to differentiate them from user-defined properties.

For example:
```js
var data = { a: 1 }
var vm = new Vue({
  el: '#example',
  data: data
})

vm.$data === data // => true
vm.$el === document.getElementById('example') // => true

// $watch is an instance method
vm.$watch('a', function (newValue, oldValue) {
  // This callback will be called when `vm.a` changes
})
```

### Instance Lifecycle Hooks

Each Vue instance goes through a series of initialization steps when it's created - for example, it
needs to set up data observation, compile the template, mount the instance to the DOM, and update
the DOM when data changes. Along the way, it runs functions called __lifecycle hooks__, giving
users the opportunity to add their own code at specific stages.

For example, the `created` hook can be used to run code after an instance is created:
```js
new Vue({
  data: {
    a: 1
  },
  created: function () {
    // `this` points to the vm instance
    console.log('a is: ' + this.a)
  }
})
// => "a is: 1"
```

Note: Don't use arrow functions on an options property or callback, such as `created: () => console.log(this.a)`.
Since an arrow fucntion have a `this`, `this` will be treated as any other variable and lexically looked
up through parent scopes until found, often resulting in errors.

### Lifecycle diagram

```txt

             new Vue()
                |
            Init Events & Lifecyle
                |
                |-------------------------- beforeCreate
                |
            Init injections & reactivity
                |
                |-------------------------- created
                |
            Mount el / Compile template
                |
                |-------------------------- beforeMount
                |
            Create vm.$el and replace el with it
                |
                |-------------------------- mounted
                |
    ------->Mounted
    |           |
    |       When data changes
    |           |
    |           |-------------------------- beforeUpdate
    |           |
    |       Virtual DOM re-render and patch
    |           |
    |           |-------------------------- updated
    ------------|
                |
            When vm.$destroy() called
                |
                |-------------------------- beforeDestroy
                |
            Teardown watchers, child components and event listeners
                |
            Destroyed
                |
                |-------------------------- destroyed
```

### API

Instantiation:
```javascript
let vm = new Vue({  /* VM=view model */
   /* Options, data */
   data:                     {},
   props:                    [],
   propsData:                [],
   methods:                  {}.
   computed:                 {},
   watch:                    {},
   /* Options, DOM */
   el:                       "",
   template:                 "",
   render:                   "",
   renderError:              "",
   /* Lifecycle */
   beforeCreate:  function() {},
   created:       function() {},
   beforeMount:   function() {},
   mounted:       function() {},
   beforeUpdate:  function() {},
   updated:       function() {},
   activated:     function() {},
   deactivated:   function() {},
   beforeDestroy: function() {},
   destroyed:     function() {},
   errorCaptured: function() {},
   /* Options, Assets */
   directives:               {},
   filters:                  {},
   components:               {},
});
```

#### data property

Properties in data object are added to Vue's reactivity system. When those properties change, the
view will "react", updating to match the new values.

#### computed Property

Similar to methods except computed properties are cached based on their dependencies. They are only
reevaluated if their dependencies change.

#### watch Property

Useful when you want to perform asychronous or expensive operations in response to data changing.

#### props property

Custom attributes that you can register on a component. They allow parent components to pass data
to child components. Props can be validated as String, Number, Boolean, Function, Object, Array,
or Symbol. Don't modify prop value in child, this will modify the parent variable.

## Template Syntax

Vue.js uses an HTML-based template syntax that allows you to declaratively bind the rendered DOM
to the underlying Vue instance's data. All Vue.js templates are valid HTML and can be parsed by
spec-compliant browsers and HTML parsers.

Under the hood, Vue compiles the templates into Virtual DOM render functions. Combined with the reactivity
system, Vue is able to intelligently figure out the minimal number of components to re-render and
apply the minimal amount of DOM manipulations when the app state changes.

### Interpolations

#### Text

The most basic form of data binding is text interpolation using the "Mustache" syntax.

```html
<span>Message: {{ msg }}</span>
```

You can also perform one-time interpolations that do not update on data change using the `v-once`
directive.

```html
<span v-once>This will never change: {{ msg }}</span>
```

#### Raw HTML

In order to interpolate raw html use the `v-html` directive. Below, the contents of the `span` will
be replaced with the value of the `rawHtml` property.

```html
<p>Using v-html directive: <span v-html="rawHtml"></span></p>
```

Note: dynamically rendered arbitrary HTML on your website can be very dangerous because it can
easily lead to XSS vulnerabilities.

#### Attributes

To bind to HTML attributes use the `v-bind` directive:

```html
<div v-bind:id="dynamicId"></div>
```

In the case of boolean attributes, where their existince implies `true`, `v-bind` works a little differently.

```html
<button v-bind:disabled="isButtonDisabled">Button</button>
```

If `isButtonDisabled` has the value of `null`, `undefined`, or `false`, the `disabled` attribute
will not be included in the rendered `<button`

#### JavaScript Expressions

Can use all of JavaScript inside data bindings:
```html
{{ number + 1 }}

{{ ok ? 'YES' : 'NO' }}

{{ message.split('').reverse().join('') }}

<div v-bind:id="'list-' + id"></div>
```

These expressions will be evaluated as JavaScript in the data scope of the owner Vue instance.
One restriction is that each binding can only contain __one single expression__, so the following
will __NOT__ work:

```html
<!-- this is a statement, not an expression: -->
{{ var a = 1 }}

<!-- flow control won't work either, use ternary expressions -->
{{ if (ok) { return message } }}
```

### Directives

Directives are special attributes with the `v-` prefix. Directive attribute values are expected
to be a __single JavaScript express__. A directive's job is to reactively apply side effects to the
DOM when the value of its expression changes.

```html
<p v-if="seen">Now you see me</p>
```

Here, the `v-if` would remove/insert the `<p>` element based on the truthiness of the value of the
expression `seen`.

#### Arguments

Some directives can take an "argument", denoted by a colon after the directive name. For example,
the `v-bind` directive is used to reactively update an HTML attribute:

```html
<a v-bind:href="url"> ... </a>

```

#### Dynamic Arguments

It is possible to use a JavaScript expression in a directive argument by wrapping it with square
brackets.

```html
<a v-bind:[attributeName]="url"> ... </a>
```

Here, `attributeName` will be dynamically evaluated as a JavaScript expression, and its evaluated
value will be used as the final value for the argument.

Dynamic arguments are expected to evaluated to a string, with the exception of `null`. The special
value `null` can be used to explicitly remove the binding.

#### Modifiers

Modifiers are special postfixes denoted by a dot, which indicate that a directive should be bound in
some special way. For example, the `.prevent` modifier tells the `v-on` directive to call `event.preventDefault()`
on the triggered event:

```html
<form v-on:submit.prevent="onSubmit"> ... </form>
```

### Shorthands

The `v-` prefix serves as a visual cue for identifying Vue-specific attributes in yuor templates.
This is useful when you are using Vue.js to apply dynamic behavior to some existing markup, but can
feel verbose for some frequently used directives. At the same time, the need for the `v-` prefix
becomes less important when you are building a SPA, where Vue manages every template.

Therefore, Vue provides special shorthands

#### `v-bind` shorthand

```html
<!-- full syntax -->
<a v-bind:href="url"> ... </a>

<!-- shorthand -->
<a :href="url"> ... </a>

<!-- shorthand with dynamic argument (2.6.0+) -->
<a :[key]="url"> ... </a>
```

#### `v-on` shorthand

```html
<!-- full syntax -->
<a v-on:click="doSomething"> ... </a>

<!-- shorthand -->
<a @click="doSomething"> ... </a>

<!-- shorthand with dynamic argument (2.6.0+) -->
<a @[event]="doSomething"> ... </a>
```

### Computed Properties

In-template expressions are very convenient, but they are meant for simple operations. Putting
too much login in your templates can make them bloated and hard to maintain.

For example:

```html
<div id="example">
  {{ message.split('').reverse().join('') }}
</div>
```

The template is no longer declarative. And its hard to reuse the logic somewhere else. Enter computed
properties.

#### Basic Example

```html
<div id="example">
  <p>Original message: "{{ message }}"</p>
  <p>Computed reversed message: "{{ reversedMessage }}"</p>
</div>
```

```js
var vm = new Vue({
  el: '#example',
  data: {
    message: 'Hello'
  },
  computed: {
    // a computed getter
    reversedMessage: function () {
      // `this` points to the vm instance
      return this.message.split('').reverse().join('')
    }
  }
})
```

#### Computed Caching vs Methods

Computed properties are cached based on their deactive dependencies. A computed property will only
re-evaluate when some of its reactive dependencies have changed.

This means the following computed property will never update, because `Date.now()` is not a reactive
dependency:
```js
computed: {
  now: function () {
    return Date.now()
  }
}
```

In comparison, a method invocation will always run the function whenever a re-render happens

#### Computed vs Watched Property

Vue does provide a more generic way to observe and react to data changes on a Vue instance:
__watch properties__.  However, it is often a better idea to use a computed property rather than
an imperative `watch` callback.

#### Computed Setter

Computed properties are by default getter-only, but you can also provide a setter when you need it:
```js
// ...
computed: {
  fullName: {
    // getter
    get: function () {
      return this.firstName + ' ' + this.lastName
    },
    // setter
    set: function (newValue) {
      var names = newValue.split(' ')
      this.firstName = names[0]
      this.lastName = names[names.length - 1]
    }
  }
}
// ...
```

### Watchers

watchers are most useful when you want to perform asynchronous or expensive operations in response
to changing data.

```html
<div id="watch-example">
  <p>
    Ask a yes/no question:
    <input v-model="question">
  </p>
  <p>{{ answer }}</p>
</div>
```
```html
<!-- Since there is already a rich ecosystem of ajax libraries    -->
<!-- and collections of general-purpose utility methods, Vue core -->
<!-- is able to remain small by not reinventing them. This also   -->
<!-- gives you the freedom to use what you're familiar with.      -->
<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script>
var watchExampleVM = new Vue({
  el: '#watch-example',
  data: {
    question: '',
    answer: 'I cannot give you an answer until you ask a question!'
  },
  watch: {
    // whenever question changes, this function will run
    question: function (newQuestion, oldQuestion) {
      this.answer = 'Waiting for you to stop typing...'
      this.debouncedGetAnswer()
    }
  },
  created: function () {
    // _.debounce is a function provided by lodash to limit how
    // often a particularly expensive operation can be run.
    // In this case, we want to limit how often we access
    // yesno.wtf/api, waiting until the user has completely
    // finished typing before making the ajax request. To learn
    // more about the _.debounce function (and its cousin
    // _.throttle), visit: https://lodash.com/docs#debounce
    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
  },
  methods: {
    getAnswer: function () {
      if (this.question.indexOf('?') === -1) {
        this.answer = 'Questions usually contain a question mark. ;-)'
        return
      }
      this.answer = 'Thinking...'
      var vm = this
      axios.get('https://yesno.wtf/api')
        .then(function (response) {
          vm.answer = _.capitalize(response.data.answer)
        })
        .catch(function (error) {
          vm.answer = 'Error! Could not reach the API. ' + error
        })
    }
  }
})
</script>
```

### Class Bindings

A common need for data binding is manipulating an element’s class list and its inline styles.

#### Object Syntax

We can pass an object to `v-bind:class` to dynamically toggle classes:

```html
<div v-bind:class="{ active: isActive }"></div>
```

The above syntax means the presence of the `active` class will be determined by the truthiness of
the data property `isActive`.

#### Array Syntax

Can pass an array to `v-bind:class` to apply a list of classes:

```html
<div v-bind:class="[activeClass, errorClass]"></div>
```
```js
data: {
  activeClass: 'active',
  errorClass: 'text-danger'
}
```

Which will render

```html
<div class="active text-danger"></div>
```

#### With Components

When you use the `class` attribute on a custom component, those classes will be added to the
component's root element. Existing classes on this element will not be overwritten.

Example component:
```js
Vue.component('my-component', {
  template: '<p class="foo bar">Hi</p>'
})
```

Use with extra classes:
```html
<my-component class="baz boo"></my-component>
```

Will be rendered as:
```html
<p class="foo bar baz boo">Hi</p>
```

### Style Bindings

#### Object Syntax

The object syntax for `v-bind:style` is pretty straightforward - it looks almost like CSS, except it’s
a JavaScript object.

```html
<div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
```

```js
data: {
  activeColor: 'red',
  fontSize: 30
}
```

It is often a good idea to bind a style object directly:
```html
<div v-bind:style="styleObject"></div>
```

```js
data: {
  styleObject: {
    color: 'red',
    fontSize: '13px'
  }
}
```

#### Array Syntax

The array syntax allows you to apply multiple style objects to the same element.

```html
<div v-bind:style="[baseStyles, overridingStyles]"></div>
```

#### Auto-prefixing

When you use a CSS property that requires vendor prefixes in `v-bind:style`, (e.g. `transform`),
Vue will automatically detect and add appropriate prefixes to the applied styles.

### Conditional Rendering

#### `v-if`

The directive `v-if` is used to conditionally render a block.

```html
<h1 v-if="awesome">Vue is awesome!</h1>
<h1 v-else>Oh no 😢</h1>
```

#### Conditional groups

Because `v-if` is a directive, it has to be attached to a single element. But what if we want to toggle
more than one element? In this case use `v-if` on a `<template>`. The rendered result will not include
the `<template>` element.

```html

<template v-if="ok">
  <h1>Title</h1>
  <p>Paragraph 1</p>
  <p>Paragraph 2</p>
</template>
```

#### `v-else`

A `v-else` element must immediately follow a `v-if` or a `v-else-if` element - otherwise it will
not be recognized.

```html
<div v-if="Math.random() > 0.5">
  Now you see me
</div>
<div v-else>
  Now you don't
</div>
```

#### `v-else-if`

Similar to `v-else`, a `v-else-if` element must immediately follow a `v-if` or a `v-else-if` element.

```html
<div v-if="type === 'A'">
  A
</div>
<div v-else-if="type === 'B'">
  B
</div>
<div v-else-if="type === 'C'">
  C
</div>
<div v-else>
  Not A/B/C
</div>
```

#### Controlling Reusable Elements with `key`

Vue tries to render elements as efficiently as possible, often re-using them instead of rendering
from scratch. Beyond helping make Vue very fast, this can have some useful advantages.

For example, if you allow users to toggle between multiple login types:
```html
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username" key="username-input">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address" key="email-input">
</template>
```

The `key` on input will force the `input` to be re-rendered and no reused.

#### `v-show`

An element with `v-show` will always be rendered and remain in the DOM; `v-show` only toggles the
display CSS property of the element.

```html
<h1 v-show="ok">Hello!</h1>
```

Note that `v-show` doesn't support the `<template>` element.

#### `v-if` vs `v-show`

`v-if` is "real" conditional rendering because it ensures that event listeners and child components
inside the conditional block are properly destroyed and re-created during toggles. `v-if` is also
__lazy__; if the condition is false on initial render, it will not do anything - the conditional
block won't be rendered until the condition becomes true for the first time.

In comparison, `v-show` is much simpler - the element is always rendered regardless of the initial
condition.

Generally speaking, `v-if` has higher toggle costs while `v-show` has higher initial render costs.
So prefer `v-show` if you need to toggle something very often, and prefer `v-if` if the condition
is unlikely to change at runtime.

### List Rendering

#### `v-for`

Can use the `v-for` directive to render a list of items based on an array.

```html
<ul id="example-1">
  <li v-for="item in items">
    {{ item.message }}
  </li>
</ul>
```

```js
var example1 = new Vue({
  el: '#example-1',
  data: {
    items: [
      { message: 'Foo' },
      { message: 'Bar' }
    ]
  }
})
```

`v-for` also supports and optional second argument to index the current item.

```html
<ul id="example-2">
  <li v-for="(item, index) in items">
    {{ parentMessage }} - {{ index }} - {{ item.message }}
  </li>
</ul>
```

```js
var example2 = new Vue({
  el: '#example-2',
  data: {
    parentMessage: 'Parent',
    items: [
      { message: 'Foo' },
      { message: 'Bar' }
    ]
  }
})
```

#### `v-for` with an Object

Can also use `v-for` to iterate through the properties of an object.

```html
<div v-for="(value, name) in object">
  {{ name }}: {{ value }}
</div>
<div v-for="(value, name, index) in object">
  {{ index }}. {{ name }}: {{ value }}
</div>
```

```js
new Vue({
  el: '#v-for-object',
  data: {
    object: {
      title: 'How to do lists in Vue',
      author: 'Jane Doe',
      publishedAt: '2016-04-10'
    }
  }
})
```

When iterating over an object, the order is based on the enumeration order of `Object.keys()` which
is __not__ guaranteed to be consistent across JavaScript engine implementation.s

#### Maintaining State

When Vue is updating a list of elements rendered with `v-for`, by default it uses an "in-place
patch" strategy. If the order of the data items has changed, intead of moving the DOM elements
to match the order of the items, Vue will patch each element in place and make sure it reflects
what should be rendered at that particular index.

To give Vue a hint so that it can track each node's identity, and thus reuse and reorder existing
elements, you need to privide a unique `key` attribute for each item:

```html
<div v-for="item in items" v-bind:key="item.id">
  <!-- content -->
</div>
```

#### Mutation Methods

Vue wraps an observed array's mutation methods so they will also trigger view updates.

- `push()`
- `pop()`
- `shift()`
- `unshift()`
- `splice()`
- `sort()`
- `reverse()`

#### Caveats

Due to limitations in JavaScript, Vue __cannot__ detect the following chages to an array:

1. When you directly set an item with the index. e.g. `vm.items[indexOfItem] = newValue`
2. When you modify the length of the array, e.g. `vm.items.length = newLength`

#### `v-for` with a Range

`v-for` can also take an integer.

```html
<div>
  <span v-for="n in 10">{{ n }} </span>
</div>
```

#### `v-for` on a template

Similar to template `v-if`, you can also use a `<template>` tag with `v-for` to render a block of
multiple elements.

```html
<ul>
  <template v-for="item in items">
    <li>{{ item.msg }}</li>
    <li class="divider" role="presentation"></li>
  </template>
</ul>
```

#### `v-for` with `v-if`

It's __not__ recommended to use `v-if` and `v-for` together.

When they exist on the same node, `v-for` has a higher priority than `v-if`. That means the `v-if`
will be run on each iteration of the loop separately. This can be useful when you want to render nodes
for only some items.

```html
<li v-for="todo in todos" v-if="!todo.isComplete">
  {{ todo }}
</li>
```

If instead, your intent is to conditionally skip execution of the loop, you can place the `v-if`
on a wrapper element.

```html
<ul v-if="todos.length">
  <li v-for="todo in todos">
    {{ todo }}
  </li>
</ul>
<p v-else>No todos left!</p>
```

#### `v-for` with a Component

When using `v-for` with a component a `key` is now required.

Pass data using prop binding.

```html
<my-component
  v-for="(item, index) in items"
  v-bind:item="item"
  v-bind:index="index"
  v-bind:key="item.id"
></my-component>
```

### Event Handling

#### Listening to Events

We can use the `v-on` directive to listen to DOM events and run JavaScript when they're triggered.

For example:

```html
<div id="example-1">
  <button v-on:click="counter += 1">Add 1</button>
  <p>The button above has been clicked {{ counter }} times.</p>
</div>
```

```js
var example1 = new Vue({
  el: '#example-1',
  data: {
    counter: 0
  }
})
```

#### Method Event Handlers

The logic of event handlers can go in a method.

```html
<div id="example-2">
  <button v-on:click="greet">Greet</button>
</div>
```

```js
var example2 = new Vue({
  el: '#example-2',
  data: {
    name: 'Vue.js'
  },
  // define methods under the `methods` object
  methods: {
    greet: function (event) {
      // `this` inside methods points to the Vue instance
      alert('Hello ' + this.name + '!')
      // `event` is the native DOM event
      if (event) {
        alert(event.target.tagName)
      }
    }
  }
})

// you can invoke methods in JavaScript too
example2.greet() // => 'Hello Vue.js!'
```

#### Methods and Inline Handlers

```html
<div id="example-3">
  <button v-on:click="say('hi')">Say hi</button>
  <button v-on:click="say('what')">Say what</button>
</div>
```

```js
new Vue({
  el: '#example-3',
  methods: {
    say: function (message) {
      alert(message)
    }
  }
})
```

Can also pass the input event.

```html
<button v-on:click="warn('Form cannot be submitted yet.', $event)">
  Submit
</button>
```

```js
// ...
methods: {
  warn: function (message, event) {
    // now we have access to the native event
    if (event) event.preventDefault()
    alert(message)
  }
}
```

#### Event Modifiers

It is very common need to call `event.preventDefault()` or `event.stopPropagation()` inside
event handlers. Althgouh we can do this easily inside methods, it would be better if the methods
can be purely about data logic rather than having to deal with DOM event details.

To address this problem, Vue provides __event modifiers__ for `v-on`. Recall that modifiers are directive
postfixes denoted by a dot.

- `.stop`
- `.prevent`
- `.capture`
- `.self`
- `.once`
- `.passive`

```html
<!-- the click event's propagation will be stopped -->
<a v-on:click.stop="doThis"></a>

<!-- the submit event will no longer reload the page -->
<form v-on:submit.prevent="onSubmit"></form>

<!-- modifiers can be chained -->
<a v-on:click.stop.prevent="doThat"></a>

<!-- just the modifier -->
<form v-on:submit.prevent></form>

<!-- use capture mode when adding the event listener -->
<!-- i.e. an event targeting an inner element is handled here before being handled by that element -->
<div v-on:click.capture="doThis">...</div>

<!-- only trigger handler if event.target is the element itself -->
<!-- i.e. not from a child element -->
<div v-on:click.self="doThat">...</div>

<!-- the click event will be triggered at most once -->
<a v-on:click.once="doThis"></a>

<!-- the scroll event's default behavior (scrolling) will happen -->
<!-- immediately, instead of waiting for `onScroll` to complete  -->
<!-- in case it contains `event.preventDefault()`                -->
<div v-on:scroll.passive="onScroll">...</div>
```

Order matters when using modifiers. For example `v-on:click.prevent.self` will prevent __all clicks__
while `v-on:click.self.prevent` will only prevent clicks on the element itself.

#### Key Modifiers

When listening for keyboard events, we often need to check for specific keys. Vue allows adding
key modifiers for `v-on` when listening for key events:

```html
<!-- only call `vm.submit()` when the `key` is `Enter` -->
<input v-on:keyup.enter="submit">
```

You can directly use any valid key names exposed via
[Keyboard Event](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values)
by converting them to kebab-case.

```html
<input v-on:keyup.page-down="onPageDown">
```

#### Key Codes

Vue provies aliases for the most commonly used key codes when necessary for legacy browser support.

- `.enter`
- `.tab`
- `.delete` (captures both “Delete” and “Backspace” keys)
- `.esc`
- `.space`
- `.up`
- `.down`
- `.left`
- `.right`

You can also define custom key modifier aliases via the global `config.keyCodes` object:

```js
// enable `v-on:keyup.f1`
Vue.config.keyCodes.f1 = 112
```

#### System Modifier Keys

You can use the following modifiers to trigger mouse or keyboard event listeners only when the
corresponding modifier key is pressed:

- `.ctrl`
- `.alt`
- `.shift`
- `.meta`

For example:
```html
<!-- Alt + C -->
<input @keyup.alt.67="clear">

<!-- Ctrl + Click -->
<div @click.ctrl="doSomething">Do something</div>
```

#### `.exact` modifier

The `.exact` modifier allows control of the exact combination of system modifiers needed to trigger
an event.

```html
<!-- this will fire even if Alt or Shift is also pressed -->
<button @click.ctrl="onClick">A</button>

<!-- this will only fire when Ctrl and no other keys are pressed -->
<button @click.ctrl.exact="onCtrlClick">A</button>

<!-- this will only fire when no system modifiers are pressed -->
<button @click.exact="onClick">A</button>
```

#### Mouse Button Modifiers

These modifiers restrict the handler to events triggered by a specific mouse button.

- `.left`
- `.right`
- `.middle`

#### Why Listeners in HTML?

Several benefits to using `v-on`:

1. It’s easier to locate the handler function implementations within your JS code by skimming the
   HTML template.
2. Since you don’t have to manually attach event listeners in JS, your ViewModel code can be pure
   logic and DOM-free. This makes it easier to test.
3. When a ViewModel is destroyed, all event listeners are automatically removed. You don’t need to
   worry about cleaning it up yourself.

### Form Input Bindings

#### Basic Usage

You can use the `v-model` directive to create two-way data bindings on form input, textarea, and
select elements. `v-model` will ignor the initial value, checked, or selected attributes found
on any form elements. It will always treat the Vue instance data as the source of truth. You should
declare the initial value on the JS side, inside the `data` component option.

`v-model` internall uses different properties and emits different events for different input
elements:

- text and textarea elements use value property and input event;
- checkboxes and radiobuttons use checked property and change event;
- select fields use value as a prop and change as an event.

#### Text

```html
<input v-model="message" placeholder="edit me">
<p>Message is: {{ message }}</p>
```

#### Multiline Text

```html
<span>Multiline message is:</span>
<p style="white-space: pre-line;">{{ message }}</p>
<br>
<textarea v-model="message" placeholder="add multiple lines"></textarea>
```

#### Checkbox

```html
<input type="checkbox" id="checkbox" v-model="checked">
<label for="checkbox">{{ checked }}</label>
```

#### Radio

```html
<input type="radio" id="one" value="One" v-model="picked">
<label for="one">One</label>
<br>
<input type="radio" id="two" value="Two" v-model="picked">
<label for="two">Two</label>
<br>
<span>Picked: {{ picked }}</span>
```

#### Select

```html
<select v-model="selected">
  <option disabled value="">Please select one</option>
  <option>A</option>
  <option>B</option>
  <option>C</option>
</select>
<span>Selected: {{ selected }}</span>
```

#### `.lazy` Modifier

By default,m `v-model` syncs the input with the data after each `input` event. You can add the `lazy`
modifier to instead sync after `change` eveents:

```html
<!-- synced after "change" instead of "input" -->
<input v-model.lazy="msg" >
```

#### `.number` Modifier

If you want user input to be automatically typecast as a number, you can use the `number` modifier
to your `v-model` managed inputs:

```html
<input v-model.number="age" type="number">
```

#### `.trim` Modifier

If you want whitespace from user input to be trimmed automatically, you can add the `trim` modifier
to your `v-model` inputs:

```html
<input v-model.trim="msg">
```

## Components

### Basics

#### Example

Components are reusable Vue instances with a name. Every component must have a single root element.

For example:

```js
// Define a new component called button-counter
Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
  template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
})
```

The component can then be used in templates as follows:

```html
<div id="components-demo">
  <button-counter></button-counter>
</div>
```

```js
new Vue({ el: '#components-demo' })
```

Since components are reusable Vue instances, they accept the same options as new Vue, such as
`data`, `computed`, `watch`, `methods`, and lifecycle hooks. The only exceptions are a few
root-specific options like `el`.

#### Reuse

Components can be reused as many times as you want:

```html
<div id="components-demo">
  <button-counter></button-counter>
  <button-counter></button-counter>
  <button-counter></button-counter>
</div>
```

Even when reused, each component instance maintains its own, separate state.

#### `data` function

A component’s `data` option must be a function, so that each instance can maintain an independent
copy of the returned data object.

```js
data: function () {
  return {
    count: 0
  }
}
```

If Vue didn't have this rule, clicking on one button would affect the data of _all other instances_.

#### Organization

It's common for an app to be organized into a tree of nested components.
For example, you might have components for a header, sidebar, and content area.

To use components in templates, they muyst be registered so that Vue knows about them. There are two
types of component registration:

- __global Registration__

  ```js
  Vue.component('my-component-name', {
    // ... options ...
  })
  ```
  Globally registered components can be used in the template of any root Vue instance and even inside
  all subcomponents of that Vue instance's component tree.

- __local Registration__

  ```js
  var ComponentA = { /* ... */ }
  var ComponentB = { /* ... */ }
  var ComponentC = { /* ... */ }

  new Vue({
    el: '#app',
    components: {
      'component-a': ComponentA,
      'component-b': ComponentB
    }
  })
  ```

#### Props

Props are custom attributes you can register on a component. When a value is passed to a prop
attribute, it becomes a property on that component instance.

```js
Vue.component('blog-post', {
  props: ['title'],
  template: '<h3>{{ title }}</h3>'
})
```

A component can have as many props as you’d like and by default, any value can be passed to any prop.
In the template above, you’ll see that we can access this value on the component instance, just like
with data.

Once a prop is registered, you can pass data to it as a custom attribute.

```html
<blog-post title="My journey with Vue"></blog-post>
<blog-post title="Blogging with Vue"></blog-post>
<blog-post title="Why Vue is so fun"></blog-post>
```

#### Child Component Events

Vue instances provide a custom events system. The parent can choose to listen to any event on the
child component instance with `v-on`, just as we would with a native DOM event.

Parent listening to child:

```html
<blog-post
  ...
  v-on:enlarge-text="postFontSize += 0.1"
></blog-post>
```

Then the child component can emit an event on itself by calling the built in `$emit` method, passing
the name of the event.

```html
<button v-on:click="$emit('enlarge-text')">
  Enlarge text
</button>
```

#### Emiting Event Values

The child can emit a value with the event as follows:

```html
<button v-on:click="$emit('enlarge-text', 0.1)">
  Enlarge text
</button>
```

The parent can process the event value with `$event`:
```html
<blog-post
  ...
  v-on:enlarge-text="postFontSize += $event"
></blog-post>
```

#### `v-model` and Components

Custom events can also be used to create custom inputs that work with `v-model`.

Remember that:

```html
<input v-model="searchText">
```

Does the same thing as:

```html
<input
  v-bind:value="searchText"
  v-on:input="searchText = $event.target.value"
>
```

For this to actually work, the `<input>` inside the component must:
- Bind the `value` attribute to a `value` prop
- On `input`, emit its own custom `input` event with the new value

For example:

```html
Vue.component('custom-input', {
  props: ['value'],
  template: `
    <input
      v-bind:value="value"
      v-on:input="$emit('input', $event.target.value)"
    >
  `
})
```

Now `v-model` should work perfectly with this component:

```html
<custom-input v-model="searchText"></custom-input>
```

#### Slots

Just like with HTML elements, it's often useful to pass content to a component like this:

```html
<alert-box>
  Something bad happened.
</alert-box>
```

Fortunately, this task is made simply by Vue's custom `<slot>` element:

```js
Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})
```

#### Dynamic Components

Sometimes, it's useful to dynamically switch between components, like in a tabbed interface.

```html
<!-- Component changes when currentTabComponent changes -->
<component v-bind:is="currentTabComponent"></component>
```

In the example above, `currentTabComponent` can contain either:
- a name of a registered component
- a component's options object

### Registration

#### Names

When registering a component, it will always be given a name.

```js
Vue.component('my-component-name', { /* ... */ })
```

There are two options when defining component names:

1. kebab-case

   ```js
   Vue.component('my-component-name', { /* ... */ })
   ```

2. PascalCase

   ```js
   Vue.component('MyComponentName', { /* ... */ })
   ```

   Can reference component as `<my-component-name>` or `<MyComponentName>`

#### Global

To globally register a component use `Vue.component`.

```js
Vue.component('component-a', { /* ... */ })
Vue.component('component-b', { /* ... */ })
Vue.component('component-c', { /* ... */ })

new Vue({ el: '#app' })
```

```html
<div id="app">
  <component-a></component-a>
  <component-b></component-b>
  <component-c></component-c>
</div>
```

Globally registered components can be used in the template of the root instance as well as in
any subcomponent. Global registration must take place before the root Vue instance is created
(with `new Vue`).

#### Local

Global registration often isn't ideal. In these case you can define components as plain JavaScript
objects:

```js
var ComponentA = { /* ... */ }
var ComponentB = { /* ... */ }
var ComponentC = { /* ... */ }
```

Then define the components you'd like to use in the a `components` option:

```js
new Vue({
  el: '#app',
  components: {
    'component-a': ComponentA,
    'component-b': ComponentB
  }
})
```

Note that __locally registered components are not available in subcomponents__.

If you are using ES2014 modules, such as through Babel and Webpack, it might look like:

```js
import ComponentA from './ComponentA.vue'

export default {
  components: {
    ComponentA
  },
  // ...
}
```

#### Module Systems

Recommended that you create a `components` directory, with each component in its own file.

Programmatically find, import, and register all components at a particular folder.

```js
import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context(
  // The relative path of the components folder
  './components',
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  // Gets the file name regardless of folder depth
  const componentName = upperFirst(
    camelCase(
      fileName
        .split('/')
        .pop()
        .replace(/\.\w+$/, '')
    )
  )

  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})
```

### Props

#### Casing

HTML attribute names are case-insensitive, so browsers will interpret any uppercase characters as
lowercase. That means when you're using in-DOM templates, camelCased prop names need to use their
kebab-cased (hyphen-delimited) equivalents.

```js
Vue.component('blog-post', {
  // camelCase in JavaScript
  props: ['postTitle'],
  template: '<h3>{{ postTitle }}</h3>'
})
```

```html
<!-- kebab-case in HTML -->
<blog-post post-title="hello!"></blog-post>
```

#### Types

Props can be listed as an object with types:

```js
props: {
  title: String,
  likes: Number,
  isPublished: Boolean,
  commentIds: Array,
  author: Object,
  callback: Function,
  contactsPromise: Promise // or any other constructor
}
```

This documents your component and warns users in the browser's JS console if they pass the wrong
type.

#### Static

```html
<blog-post title="My journey with Vue"></blog-post>
```

#### Dynamic

```html
<!-- Dynamically assign the value of a variable -->
<blog-post v-bind:title="post.title"></blog-post>

<!-- Dynamically assign the value of a complex expression -->
<blog-post
  v-bind:title="post.title + ' by ' + post.author.name"
></blog-post>

##### Passing a number

```html
<!-- Even though `42` is static, we need v-bind to tell Vue that -->
<!-- this is a JavaScript expression rather than a string.       -->
<blog-post v-bind:likes="42"></blog-post>

<!-- Dynamically assign to the value of a variable. -->
<blog-post v-bind:likes="post.likes"></blog-post>
```

##### Passing a boolean

```html
<!-- Including the prop with no value will imply `true`. -->
<blog-post is-published></blog-post>

<!-- Even though `false` is static, we need v-bind to tell Vue that -->
<!-- this is a JavaScript expression rather than a string.          -->
<blog-post v-bind:is-published="false"></blog-post>

<!-- Dynamically assign to the value of a variable. -->
<blog-post v-bind:is-published="post.isPublished"></blog-post>
```

##### Passing an Array

```html
<!-- Even though the array is static, we need v-bind to tell Vue that -->
<!-- this is a JavaScript expression rather than a string.            -->
<blog-post v-bind:comment-ids="[234, 266, 273]"></blog-post>

<!-- Dynamically assign to the value of a variable. -->
<blog-post v-bind:comment-ids="post.commentIds"></blog-post>
```

##### Passing an Object

```html
<!-- Even though the object is static, we need v-bind to tell Vue that -->
<!-- this is a JavaScript expression rather than a string.             -->
<blog-post
  v-bind:author="{
    name: 'Veronica',
    company: 'Veridian Dynamics'
  }"
></blog-post>

<!-- Dynamically assign to the value of a variable. -->
<blog-post v-bind:author="post.author"></blog-post>
```

##### Pasing the properties of an object

If you want to pass all the proerties of an object as props, you can use `v-bind` without an argument
(`v-bind` instead of `v-bind:prop-name`)

```js
post: {
  id: 1,
  title: 'My Journey with Vue'
}
```

The following template:

```html
<blog-post v-bind="post"></blog-post>
```

Will be equivalent to:

```html
<blog-post
  v-bind:id="post.id"
  v-bind:title="post.title"
></blog-post>
```

#### One-Way Data Flow

All props form a __one-way-down binding__ between the child property and the parent.
When the parent property updates, it will flow down to the child, but not the other way around.
This prevents child components from accidentally mutating parent's state, which can make your
app's data flow harder to understand.

In addition, every time the parent component is updated, all props in the child component will be
refreshed with the latest value. This means you should __not__ attempt to mutate a prop inside a
child component. If you do, Vue will warn you in the console.

There are usually two cases where its tempting to mutate a prop:

1. The prop is passes as an initial value. Use local data property that uses prop as initial value.

   ```js
   props: ['initialCounter'],
   data: function () {
     return {
       counter: this.initialCounter
     }
   }
   ```

2. The prop is passed in as a raw value that needs to be transformed. Use computed property that uses
   the prop's value.

   ```js
   props: ['size'],
   computed: {
     normalizedSize: function () {
       return this.size.trim().toLowerCase()
     }
   }
   ```

Note that objects and arrays in JS are passed by reference, so if the prop is an array or object,
mutating the object or array itself inside the child __will__ affect parent state.

#### Validation

Components can specify requirements for its props, such as types. To specify prop validations, you
can provide an object with validation requirements to the vale of `props.

```js
Vue.component('my-component', {
  props: {
    // Basic type check (`null` and `undefined` values will pass any type validation)
    propA: Number,
    // Multiple possible types
    propB: [String, Number],
    // Required string
    propC: {
      type: String,
      required: true
    },
    // Number with a default value
    propD: {
      type: Number,
      default: 100
    },
    // Object with a default value
    propE: {
      type: Object,
      // Object or array defaults must be returned from
      // a factory function
      default: function () {
        return { message: 'hello' }
      }
    },
    // Custom validator function
    propF: {
      validator: function (value) {
        // The value must match one of these strings
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  }
})
```

When prop validation fails, Vue will produce a console warning.

The `type` can be one of the following native constructors:

- String
- Number
- Boolean
- Array
- Object
- Date
- Function
- Symbol

In addition, `type` can also be a custom constructor function and the assertion will be made with
`instanceof` check.

```js
function Person (firstName, lastName) {
  this.firstName = firstName
  this.lastName = lastName
}
```

```js
Vue.component('blog-post', {
  props: {
    author: Person
  }
})
```

#### Non-Prop Attributes

A non-prop attribute is an attribute that is passed to a component, but does not have a corresponding
prop defined. The attribute will be added to the component's root element.

##### Replaceing/Merging Existing Attributes

`class` attributes will be merged if parent and child both specify `class`.

Other parent attributes will be override child attributes.

##### Disabling Attribute Inheritance

If you do __not__ want the root element of a component to inherit attributes, you can set
`inheritAttrs: false` in the component's options.

```js
Vue.component('my-component', {
  inheritAttrs: false,
  // ...
})
```

With `inheritAttrs: false` and `$atrs`, you can manually decide which element you want to forward
attributes to.

```js
Vue.component('base-input', {
  inheritAttrs: false,
  props: ['label', 'value'],
  template: `
    <label>
      {{ label }}
      <input
        v-bind="$attrs"
        v-bind:value="value"
        v-on:input="$emit('input', $event.target.value)"
      >
    </label>
  `
})
```

Note that `inheritAttrs: false` option does __not__ affect `style` and `class` bindings

### Custom Events

#### Names

Unlike components and props, event names don't provide any automatic case transformation.
Instead, the name of an emitted event must exactly match that name used to listen to that event.

Event names should always be in __kebab-case__ dueo to HTML case-insensitivity.

```js
this.$emit('my-event')
```

```html
<my-component v-on:my-event="doSomething"></my-component>
```

#### Customizing Component `v-model`

By default, `v-model` on a component uses `value` as the prop and `input` as the event. But some
input types such as checkboxes and radion buttons may want to use the `value` attribute for a different
purpose. Using the `model` option can avoid a conflict in such cases:

```js
Vue.component('base-checkbox', {
  model: {
    prop: 'checked',
    event: 'change'
  },
  props: {
    checked: Boolean
  },
  template: `
    <input
      type="checkbox"
      v-bind:checked="checked"
      v-on:change="$emit('change', $event.target.checked)"
    >
  `
})
```

Now when using `v-model` on this component:

```html
<base-checkbox v-model="lovingVue"></base-checkbox>
```

The value of `lovingVue` will be passed to the `checked` prop. The `lovingVue` property will then
be updated when the `<base-checkbox>` emits a `change` event with a new value.

#### `.sync` Modifier

In some cases, we may need "two-way binding" for a prop. Unfortunately, true two-way binding can
create maintenance issues, because child components can mutate the parent without the source of that
mutation being obvious in both the parent and the child.

That's why instead, use the pattern of `update:myPropName`.

```js
this.$emit('update:title', newTitle)
```

Then the parent can listen to that event and update a local data property, if it wants to.

For example:

```html
<text-document
  v-bind:title="doc.title"
  v-on:update:title="doc.title = $event"
></text-document>
```

For convenience, this can be shorthanded with the `.sync` modifier:

```html
<text-document v-bind:title.sync="doc.title"></text-document>
```

### Slots

#### Content

Vue implements a content distribution API inspired by
[Web Components spec draft](https://github.com/w3c/webcomponents/blob/gh-pages/proposals/Slots-Proposal.md)
using the `<slot>` element to serve as distribution outlets for content.

This allows you to componse components like this:

```html
<navigation-link url="/profile">
  Your Profile
</navigation-link>
```

In the `<navigation-link>` template, you might have:

```html
<a v-bind:href="url" class="nav-link">
  <slot></slot>
</a>
```

When the component renders `<slot></slot>` will be replaced by "Your Profile".

Slots can contain any template code, including HTML. If `<navigation-link>`'s template did __not__
contain a `<slot>` element, any content provided between its opening and closing tag would be discarded.

#### Compilation Scope

When you want to use data inside a slot, such as in:

```html
<navigation-link url="/profile">
  Logged in as {{ user.name }}
</navigation-link>
```

The slot has access to the same instance properties (i.e. the same "scope") as the rest of the template.
The slot does __not__ have access to `<navigation-link>`'s scope.

As a rule, remeber that:
__Everything in the parent template is compiled in the parent scope; everything in the child template
is compiled in the child scope__.

#### Fallback Content

There are cases when it’s useful to specify fallback (i.e. default) content for a slot, to be rendered
only when no content is provided.

For example:

```html
<button type="submit">
  <slot>Submit</slot>
</button>
```

Now when we use `<submit-button>` in the parent component, providing no content for the slot:

```html
<submit-button></submit-button>
```

will render the fallback content, "Submit":

```html
<button type="submit">
  Submit
</button>
```

#### Named Slots

There are times when it's useful to have multiple slots. For example, in a `<base-layout>` component
with the following template. In these cases, the `<slot>` element has a special attribute, `name`,
which can be used to define additional slots.

```html
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>
```

A `<slot>` outlet without `name` implicitly has the name "default".

To provide content to named slots, use the `v-slot` directive on a `<template>`, providing the name
of the slot as `v-slot`'s argument:

```html
<base-layout>
  <template v-slot:header>
    <h1>Here might be a page title</h1>
  </template>

  <p>A paragraph for the main content.</p>
  <p>And another one.</p>

  <template v-slot:footer>
    <p>Here's some contact info</p>
  </template>
</base-layout>
```

#### Scoped Slots

Sometimes, it’s useful for slot content to have access to data only available in the child component.

To make data available to the slot content in the parent, we can bind the data as an attribute of
the `<slot>` element.

```html
<span>
  <slot v-bind:user="user">
    {{ user.lastName }}
  </slot>
</span>
```

Attributes bound to a `<slot>` are called __slot props__. Now, in the parent scope, we can use `v-slot`
with a value to define a name for the slot props we've been provided.

```html
<current-user>
  <template v-slot:default="slotProps">
    {{ slotProps.user.firstName }}
  </template>
</current-user>
```

#### Destructuring Slot Props

Destructure to access user:

```html
<current-user v-slot="{ user }">
  {{ user.firstName }}
</current-user>
```

Define fallbacks if a slot prop is not defined:

```html
<current-user v-slot="{ user = { firstName: 'Guest' } }">
  {{ user.firstName }}
</current-user>
```

#### Dynamic Slot Names

Dynamic directive arguments also work with `v-slot`, allowing the definition of dynamic slot names.

```html
<base-layout>
  <template v-slot:[dynamicSlotName]>
    ...
  </template>
</base-layout>
```

### Dynamic and Async Components

#### `keep-alive`

Earlier, we used the `is` attribute to switch between components in a tabbed interface:

```html
<component v-bind:is="currentTabComponent"></component>
```

When switching between these components though, you’ll sometimes want to maintain their state or
avoid re-rendering for performance reasons.

Recreating dynamic components is normally useful behavior, but in this case, we’d really like those
tab component instances to be cached once they’re created for the first time.
To solve this problem, we can wrap our dynamic component with a `<keep-alive>`

```html
<!-- Inactive components will be cached! -->
<keep-alive>
  <component v-bind:is="currentTabComponent"></component>
</keep-alive>
```

#### Async Components

In large applications, we may need to divide the app into smaller chunks and only load a component
from the server when it's needed. TO make that easier, Vue allows you to define your component as
a factory function that asynchronously resolves your component definition.

```js
Vue.component('async-example', function (resolve, reject) {
  setTimeout(function () {
    // Pass the component definition to the resolve callback
    resolve({
      template: '<div>I am async!</div>'
    })
  }, 1000)
})
```

A more concrete example is as follows:

```js
Vue.component('async-webpack-example', function (resolve) {
  // This special require syntax will instruct Webpack to
  // automatically split your built code into bundles which
  // are loaded over Ajax requests.
  require(['./my-async-component'], resolve)
})
```

You can also return a `Promise` in the factory function, so with Webpack 2 and ES2015 syntax you can
do:

```js
Vue.component(
  'async-webpack-example',
  // The `import` function returns a Promise.
  () => import('./my-async-component')
)
```

#### Handling Loading State

The async component factory can also return an object of the following format:

```js
const AsyncComponent = () => ({
  // The component to load (should be a Promise)
  component: import('./MyComponent.vue'),
  // A component to use while the async component is loading
  loading: LoadingComponent,
  // A component to use if the load fails
  error: ErrorComponent,
  // Delay before showing the loading component. Default: 200ms.
  delay: 200,
  // The error component will be displayed if a timeout is
  // provided and exceeded. Default: Infinity.
  timeout: 3000
})
```

### Handling Edge Cases

#### Accessing the Root Instance

In every subcomponent of a `new Vue` instance, this root instance can be accessed with the `$root`
property.

For example:

```js
// The root Vue instance
new Vue({
  data: {
    foo: 1
  },
  computed: {
    bar: function () { /* ... */ }
  },
  methods: {
    baz: function () { /* ... */ }
  }
})
```

All subcomponents will now be able to access this instance and use it as a global store.

```js
// Get root data
this.$root.foo

// Set root data
this.$root.foo = 2

// Access root computed properties
this.$root.bar

// Call root methods
this.$root.baz()
```

#### Accessing the Parent Component Instance

Similar to `$root`, the `$parent` property can be used to access the parent instance from a child.
This can be tempting to reach for as a lazy alternative to passing data with a prop.

#### Accessing Child Component Instances and Elements

Despite the existence of props and events, sometimes you might still need to directly access a
child component in JavaScript. To achieve this you can assign a reference ID to the child component
using the `ref` attribute.

```html
<base-input ref="usernameInput"></base-input>
```

Now in the component where you've defined this `ref`, you can access it via `this.$refs`.

```js
this.$refs.usernameInput
```

When `ref` is used together with `v-for`, the ref you get will be an array containing the child
components mirroring the data source.

`$refs` are only populated after the component has been rendered, and they are not reactive. It is
only meant as an escape hatch for direct child manipulation. You should avoid using `$refs` from
within templates or computed properties.

#### Dependency Injection

Dependency injection allows all nested descendants of a parent component to access a specific
parent component data/method.

The `provide` options allow us to specify the data/method we want to __provide__ to descendant
components.

```js
provide: function () {
  return {
    getMap: this.getMap
  }
}
```

Then in any descendants, we can use the `inject` option to receive specific properties we'd like to
add to that instance.

Dependency injection can be thought of as "long-range-props" except:

- Ancestor components don't need to know which descendants use the properties it provides
- Descendany components don't need to know where injected properties are coming from

However, there are downsides to dependency injection. It couples components in your application to
the way they’re currently organized, making refactoring more difficult. Provided properties are
also not reactive. This is by design, because using them to create a central data store scales just
as poorly as using `$root` for the same purpose. If the properties you want to share are specific
to your app, rather than generic, or if you ever want to update provided data inside ancestors,
then that’s a good sign that you probably need a real state management solution like Vuex instead.

#### Programmatic Event Listeners

So far, you’ve seen uses of `$emit`, listened to with `v-on`, but Vue instances also offer other methods
in its events interface. We can:

- Listen for an event with `$on(eventName, eventHandler)`
- Listen for an event only once with `$once(eventName, eventHandler)`
- Stop listening for an event with `$off(eventName, eventHandler)`

#### Recursive Components

Components can recursively invoke themselves in their own template. However, they can only do so
with the `name` option.

Be careful not creating infinite component loops.

```js
name: 'stack-overflow',
template: '<div><stack-overflow></stack-overflow></div>'
```

Make sure recursive invocation is conditional (uses `v-if` that will eventually be `false`)

#### Inline Templates

When the `inline-template` special attribute is present on a child component, the component will
use its inner content as a template, rather than treating it as distributed content.

```html
<my-component inline-template>
  <div>
    <p>These are compiled as the component's own template.</p>
    <p>Not parent's transclusion content.</p>
  </div>
</my-component>
```

However, `inline-template` makes scope harder to reason about.  As a best practice, prefer defining
templates inside the component using the `template` option or in a `<template>` element in a `.vue` file.

#### X-Templates

ANother way to define templates is inside of a script element with the type `text/x-template`,
then referencing the template by an id.

```html
<script type="text/x-template" id="hello-world-template">
  <p>Hello hello hello</p>
</script>
```

```js
Vue.component('hello-world', {
  template: '#hello-world-template'
})
```

Your x-template needs to be defined outside the DOM element to which Vue is attached.

These can be useful for demos with large templates or in extremely small applications, but should
otherwise be avoided, because they separate templates from the rest of the component definition.

#### Forcing an Update

However, if you’ve ruled out the above and find yourself in this extremely rare situation of having
to manually force an update, you can do so with `$forceUpdate`.

#### Cheap Static Components with `v-once`

Rendering plain HTML elements is very fast in Vue, but sometimes you might have a component that contains
a __lot__ of static content. In these cases, you can ensure that it’s only evaluated once and then
cached by adding the `v-once` directive to the root element, like this:

```js
Vue.component('terms-of-service', {
  template: `
    <div v-once>
      <h1>Terms of Service</h1>
      ... a lot of static content ...
    </div>
  `
})
```

Try not to overuse this pattern. While convenient in those rare cases when you have to render a lot
of static content, it's simply not necessary unless you actually notice slow rendering - plus, it
could cause a lot of confusion later.

## Transitions and Animations

### Transitioning Single Elements/Components

Vue provides a `transition` wrapper component, allowing you to add entering/leaving transitions for
any element or component in the following contexts:

- Conditional rendering (using `v-if`)
- Conditional display (using `v-show`)
- Dynamic components
- Component root nodes

For example:

```html
<div id="demo">
  <button v-on:click="show = !show">
    Toggle
  </button>
  <transition name="fade">
    <p v-if="show">hello</p>
  </transition>
</div>
```

```js
new Vue({
  el: '#demo',
  data: {
    show: true
  }
})
```

```css
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
```

When an element wrapped in a `transition` component is inserted or removed, this is what happens:

1. Vue will automatically sniff whether the target element has CSS transitions or animations applied.
   If it does, CSS transition classes will be added/removed at appropriate timings.

2. If the transition component provided JavaScript hooks, these hooks will be called at appropriate
   timings.

3. If no CSS transitions/animations are detected and no JavaScript hooks are provided, the DOM
   operations for insertion and/or removal will be executed immediately on next frame (Note: this
   is a browser animation frame, different from Vue’s concept of `nextTick`).

#### Transition Clases

There are six classes applied for enter/leave transitions.

1. `v-enter`: Starting state for enter. Added before element is inserted, removed one frame after
   element inserted.

2. `v-enter-active`: Active state for enter. Applied during the entire entering phase. Added before
   element is inserted, removed, when transition/animation finishes. This class can be used to define
   the duration, delay and easing curve for the entering transition.

3. `v-enter-to`: Ending state for enter. Added one frame after element is inserted (at the same time
   `v-enter` is removed), removed when transition/animation finishes.

4. `v-leave`: Starting state for leave. Added immediately when a leaving transition is triggered,
   removed after one frame.

5. `v-leave-active`: Active state for leave. Applied during the entire leaving phase. Added immediately
   when leave transition is triggered, removed when the transition/animation finishes. This class
   can be used to define the duration, delay and easing curve for the leaving transition.

6. `v-leave-to`: Ending state for leave. Added one frame after a leaving transition is triggered
   (at the same time v-leave is removed), removed when the transition/animation finishes.

`v-enter-active` and `v-leave-active` agive you the ability to specify different easing curves for
enter/leave transitions.

```text

                 Enter                                       Leave

  --------------        --------------        --------------        --------------
  |            |        |            |        |            |        |            |
  | Opacity: 0 | -----> | Opacity: 1 |        | Opacity: 1 | -----> | Opacity: 0 |
  |            |        |            |        |            |        |            |
  --------------        --------------        --------------        --------------
       |                      |                     |                     |
    v-enter                v-enter-to             v-leave              v-leave-to
  |                                  |        |                                  |
  ------------------------------------        ------------------------------------
                  |                                            |
            v-enter-active                                 v-leave-active

```

Each of these classes will be prefixed with the name of the transition. Here the `v-` prefix is the
default when you use a `<transition>` element with no name. If you use `<transition name="my-transition">`
for example, then the `v-enter` class would instead be `my-transition-enter`.

#### CSS Transitions

One of the most common transition types uses CSS transitions.

```html
<div id="example-1">
  <button @click="show = !show">
    Toggle render
  </button>
  <transition name="slide-fade">
    <p v-if="show">hello</p>
  </transition>
</div>
```

```js
new Vue({
  el: '#example-1',
  data: {
    show: true
  }
})
```

```css
/* Enter and leave animations can use different */
/* durations and timing functions.              */
.slide-fade-enter-active {
  transition: all .3s ease;
}
.slide-fade-leave-active {
  transition: all .8s cubic-bezier(1.0, 0.5, 0.8, 1.0);
}
.slide-fade-enter, .slide-fade-leave-to
/* .slide-fade-leave-active below version 2.1.8 */ {
  transform: translateX(10px);
  opacity: 0;
}
```

#### CSS Animations

CSS animations are applied in the same way as CSS transitions, the difference being that `v-enter`
is not removed immediately after the element is inserted, but on an `animationed` event.

```html
<div id="example-2">
  <button @click="show = !show">Toggle show</button>
  <transition name="bounce">
    <p v-if="show">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris facilisis enim libero, at lacinia diam fermentum id. Pellentesque habitant morbi tristique senectus et netus.</p>
  </transition>
</div>
```

```js
new Vue({
  el: '#example-2',
  data: {
    show: true
  }
})
```

```css
.bounce-enter-active {
  animation: bounce-in .5s;
}
.bounce-leave-active {
  animation: bounce-in .5s reverse;
}
@keyframes bounce-in {
  0% {
    transform: scale(0);
  }
  50% {
    transform: scale(1.5);
  }
  100% {
    transform: scale(1);
  }
}
```

#### Custom Transition Classes

You can also specify custom transition classes by providing the following attributes:

- `enter-class`
- `enter-active-class`
- `enter-to-class`
- `leave-class`
- `leave-active-class`
- `leave-to-class`

These override the conventtional class names. This is especially useful when you want to combine
Vue's transition system with an existing CSS animation library, such as
[Animate.css](https://daneden.github.io/animate.css/).

```html
<link href="https://cdn.jsdelivr.net/npm/animate.css@3.5.1" rel="stylesheet" type="text/css">

<div id="example-3">
  <button @click="show = !show">
    Toggle render
  </button>
  <transition
    name="custom-classes-transition"
    enter-active-class="animated tada"
    leave-active-class="animated bounceOutRight"
  >
    <p v-if="show">hello</p>
  </transition>
</div>
```

```js
new Vue({
  el: '#example-3',
  data: {
    show: true
  }
})
```

#### Using Transitions and Animations together

Vue needs to attach event listeners in order to know when a transition has ended. It can either be
`transitionend` or `animationend`, depending on the type of CSS rules applied. If you are only
using one or the other, Vue can automatically detect the correct type.

However, in some cases you may want to have both on the same element, for example having a CSS
animation triggered by Vue, along with a CSS transition effect on hover. In these cases, you will
have to explicitly declare the type you want Vue to care about in a type attribute, with a value of
either `animation` or `transition`.

#### Explicit Transition Durations

In most cases, Vue can autatically figure out when the transition has finished. By default, Vue
waits for the first `transitionend` or `animationend` event on the root transition element.
However, this may not always be desired - for example, we may have a choreographed transition sequence
where some nested inner elements have a delayed transition or a longer transition duration than the root
transition element.

In such cases you can specify an explicit transition duration (in milliseconds) using the `duration`
prop on the `<transition>` component.

```html
<transition :duration="1000">...</transition>
<transition :duration="{ enter: 500, leave: 800 }">...</transition>
```

#### JavaScript Hooks

You can also define JavaScript hooks in attributes:

```html
<transition
  v-on:before-enter="beforeEnter"
  v-on:enter="enter"
  v-on:after-enter="afterEnter"
  v-on:enter-cancelled="enterCancelled"

  v-on:before-leave="beforeLeave"
  v-on:leave="leave"
  v-on:after-leave="afterLeave"
  v-on:leave-cancelled="leaveCancelled"
>
  <!-- ... -->
</transition>
```

```js
// ...
methods: {
  // --------
  // ENTERING
  // --------

  beforeEnter: function (el) {
    // ...
  },
  // the done callback is optional when
  // used in combination with CSS
  enter: function (el, done) {
    // ...
    done()
  },
  afterEnter: function (el) {
    // ...
  },
  enterCancelled: function (el) {
    // ...
  },

  // --------
  // LEAVING
  // --------

  beforeLeave: function (el) {
    // ...
  },
  // the done callback is optional when
  // used in combination with CSS
  leave: function (el, done) {
    // ...
    done()
  },
  afterLeave: function (el) {
    // ...
  },
  // leaveCancelled only available with v-show
  leaveCancelled: function (el) {
    // ...
  }
}
```

When using JavaScript-only transitions, the `done` callbacks are required for the `enter` and `leave`
hooks. Otherwise, the hooks will be called synchronously and the transition will finish immediately.

It's also a good idea to explicitly add `v-bind:css="false"` for JavaScript-only transitions so that
Vue can skip the CSS detection. This also prevents CSS rules from accidentally interfering with the
transition.

Here's an example:

```html
<!--
Velocity works very much like jQuery.animate and is
a great option for JavaScript animations
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>

<div id="example-4">
  <button @click="show = !show">
    Toggle
  </button>
  <transition
    v-on:before-enter="beforeEnter"
    v-on:enter="enter"
    v-on:leave="leave"
    v-bind:css="false"
  >
    <p v-if="show">
      Demo
    </p>
  </transition>
</div>
```

```js
new Vue({
  el: '#example-4',
  data: {
    show: false
  },
  methods: {
    beforeEnter: function (el) {
      el.style.opacity = 0
    },
    enter: function (el, done) {
      Velocity(el, { opacity: 1, fontSize: '1.4em' }, { duration: 300 })
      Velocity(el, { fontSize: '1em' }, { complete: done })
    },
    leave: function (el, done) {
      Velocity(el, { translateX: '15px', rotateZ: '50deg' }, { duration: 600 })
      Velocity(el, { rotateZ: '100deg' }, { loop: 2 })
      Velocity(el, {
        rotateZ: '45deg',
        translateY: '30px',
        translateX: '30px',
        opacity: 0
      }, { complete: done })
    }
  }
})
```

### Transitions on Initial Render

If you also want to apply a transition on the initial render of a node, you can add the `appear`
attribute:

```html
<transition appear>
  <!-- ... -->
</transition>
```

By default, this will use the transitions specified for entering and leaving. If you'd like however,
you can also specify custom CSS classes:

```html
<transition
  appear
  appear-class="custom-appear-class"
  appear-to-class="custom-appear-to-class" (2.1.8+)
  appear-active-class="custom-appear-active-class"
>
  <!-- ... -->
</transition>
```

and custom JavaScript hooks:

```html
<transition
  appear
  v-on:before-appear="customBeforeAppearHook"
  v-on:appear="customAppearHook"
  v-on:after-appear="customAfterAppearHook"
  v-on:appear-cancelled="customAppearCancelledHook"
>
  <!-- ... -->
</transition>
```

In the example above, either `appear` or `v-on:appear` hook will cause an appear transition.

### Transitioning Between Elements

You can transition between raw `v-if`/`v-else`.

```html
<transition>
  <button v-if="isEditing" key="save">
    Save
  </button>
  <button v-else key="edit">
    Edit
  </button>
</transition>
```

Note: when toggling between elements that have __the same tag name__, you must tell Vue that they are
distinct elements by giving them unique `key` attributes. Otherwie, Vue's compiler will only replace
content of the element for efficiency. Even when technically unnecessary, __it's considered good
practice to always key multiple items within a `<transition>` component__.

The above example could be written as:

```html
<transition>
  <button v-bind:key="isEditing">
    {{ isEditing ? 'Save' : 'Edit' }}
  </button>
</transition>
```

There's still one problem. As it's transitioning between Save and Edit, both buttons are rendered -
one transitioning out while the other transitions in. This is the default behavior of `<transition>` -
entering and leaving happens simultaneously.

Simultaneous entering and leaving transitions aren't always desireable. so Vue offers some alternative
__transition modes__:

- `in-out`: New element transitions in first, then when complete, the current element transitions out.
- `out-in`: Current element transitions out first, then when complete, the new element transitions in.

```html
<transition name="fade" mode="out-in">
  <!-- ... the buttons ... -->
</transition>
```

### Transitioning Between Components

Transitioning between components is even simpler - we don't even need the `key` attribute. Instead,
we wrap a dynamic component.

```html
<transition name="component-fade" mode="out-in">
  <component v-bind:is="view"></component>
</transition>
```

```js
new Vue({
  el: '#transition-components-demo',
  data: {
    view: 'v-a'
  },
  components: {
    'v-a': {
      template: '<div>Component A</div>'
    },
    'v-b': {
      template: '<div>Component B</div>'
    }
  }
})
```

```css
.component-fade-enter-active, .component-fade-leave-active {
  transition: opacity .3s ease;
}
.component-fade-enter, .component-fade-leave-to
/* .component-fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
```

### List Transitions

When transitioning a whole list of items simultaneously, use a `<transition-group>` component.
Here are some important things to know about `<transition-group>`:

- Unlike `<transition>`, it renders and actual element: a `<span>` by default. You can change the
  element that's rendered with the `tag` attribute
- Transition modes are not available, because we are no longer alternating between mutually exclusive
  elements.
- Elements inside are __always requred__ to have a unique `key` attribute
- Css transition classes will be applied to inner elements and not to the group/container itself

#### List Entering/Leaving Transitions

```html
<div id="list-demo">
  <button v-on:click="add">Add</button>
  <button v-on:click="remove">Remove</button>
  <transition-group name="list" tag="p">
    <span v-for="item in items" v-bind:key="item" class="list-item">
      {{ item }}
    </span>
  </transition-group>
</div>
```

```js
new Vue({
  el: '#list-demo',
  data: {
    items: [1,2,3,4,5,6,7,8,9],
    nextNum: 10
  },
  methods: {
    randomIndex: function () {
      return Math.floor(Math.random() * this.items.length)
    },
    add: function () {
      this.items.splice(this.randomIndex(), 0, this.nextNum++)
    },
    remove: function () {
      this.items.splice(this.randomIndex(), 1)
    },
  }
})
```

```css
.list-item {
  display: inline-block;
  margin-right: 10px;
}
.list-enter-active, .list-leave-active {
  transition: all 1s;
}
.list-enter, .list-leave-to /* .list-leave-active below version 2.1.8 */ {
  opacity: 0;
  transform: translateY(30px);
}
```

There's one problem with this example. When you add or remove an item, the ones around it instantly
snap into their new place instead of smoothly transitioning.

#### List Move Transitions

The `<transition-group>` component can not only animate entering and leaving, but also changes in
position. The only new concept you need to know to use this feature is the addition of the `v-move`
class, whcih is added when items are changing positions. Like the other classes, its prefix will match
the value of a provided `name` attribute and you can also manually specify a class with the `move-class`
attribute.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.14.1/lodash.min.js"></script>

<div id="flip-list-demo" class="demo">
  <button v-on:click="shuffle">Shuffle</button>
  <transition-group name="flip-list" tag="ul">
    <li v-for="item in items" v-bind:key="item">
      {{ item }}
    </li>
  </transition-group>
</div>
```

```js
new Vue({
  el: '#flip-list-demo',
  data: {
    items: [1,2,3,4,5,6,7,8,9]
  },
  methods: {
    shuffle: function () {
      this.items = _.shuffle(this.items)
    }
  }
})
```

```css
.flip-list-move {
  transition: transform 1s;
}
```

#### Staggering List Transitions

By communicating with JavaScript transitions through data attributes, it's also possible to stagger
transitions in a list.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>

<div id="staggered-list-demo">
  <input v-model="query">
  <transition-group
    name="staggered-fade"
    tag="ul"
    v-bind:css="false"
    v-on:before-enter="beforeEnter"
    v-on:enter="enter"
    v-on:leave="leave"
  >
    <li
      v-for="(item, index) in computedList"
      v-bind:key="item.msg"
      v-bind:data-index="index"
    >{{ item.msg }}</li>
  </transition-group>
</div>
```

```js
new Vue({
  el: '#staggered-list-demo',
  data: {
    query: '',
    list: [
      { msg: 'Bruce Lee' },
      { msg: 'Jackie Chan' },
      { msg: 'Chuck Norris' },
      { msg: 'Jet Li' },
      { msg: 'Kung Fury' }
    ]
  },
  computed: {
    computedList: function () {
      var vm = this
      return this.list.filter(function (item) {
        return item.msg.toLowerCase().indexOf(vm.query.toLowerCase()) !== -1
      })
    }
  },
  methods: {
    beforeEnter: function (el) {
      el.style.opacity = 0
      el.style.height = 0
    },
    enter: function (el, done) {
      var delay = el.dataset.index * 150
      setTimeout(function () {
        Velocity(
          el,
          { opacity: 1, height: '1.6em' },
          { complete: done }
        )
      }, delay)
    },
    leave: function (el, done) {
      var delay = el.dataset.index * 150
      setTimeout(function () {
        Velocity(
          el,
          { opacity: 0, height: 0 },
          { complete: done }
        )
      }, delay)
    }
  }
})
```

### Reusable Transitions

Transitions can be reused through Vue's component system. To create a reusable transition, all
you have to do is place a `<transition>` or `<transition-group>` component at the root, then pass
an children into the transition component.

```js
Vue.component('my-special-transition', {
  template: '\
    <transition\
      name="very-special-transition"\
      mode="out-in"\
      v-on:before-enter="beforeEnter"\
      v-on:after-enter="afterEnter"\
    >\
      <slot></slot>\
    </transition>\
  ',
  methods: {
    beforeEnter: function (el) {
      // ...
    },
    afterEnter: function (el) {
      // ...
    }
  }
})
```

### Dynamic Transitions

Even transitions in Vue are data-driven! The most basic example of a dynamic transition binds the
`name` attribute to a dynamic property.

```html
<transition v-bind:name="transitionName">
  <!-- ... -->
</transition>
```

This can be useful when you've defined CSS transitions/animations using Vue's transition class conventions
and want to switch between them.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>

<div id="dynamic-fade-demo" class="demo">
  Fade In: <input type="range" v-model="fadeInDuration" min="0" v-bind:max="maxFadeDuration">
  Fade Out: <input type="range" v-model="fadeOutDuration" min="0" v-bind:max="maxFadeDuration">
  <transition
    v-bind:css="false"
    v-on:before-enter="beforeEnter"
    v-on:enter="enter"
    v-on:leave="leave"
  >
    <p v-if="show">hello</p>
  </transition>
  <button
    v-if="stop"
    v-on:click="stop = false; show = false"
  >Start animating</button>
  <button
    v-else
    v-on:click="stop = true"
  >Stop it!</button>
</div>
```

```js
new Vue({
  el: '#dynamic-fade-demo',
  data: {
    show: true,
    fadeInDuration: 1000,
    fadeOutDuration: 1000,
    maxFadeDuration: 1500,
    stop: true
  },
  mounted: function () {
    this.show = false
  },
  methods: {
    beforeEnter: function (el) {
      el.style.opacity = 0
    },
    enter: function (el, done) {
      var vm = this
      Velocity(el,
        { opacity: 1 },
        {
          duration: this.fadeInDuration,
          complete: function () {
            done()
            if (!vm.stop) vm.show = false
          }
        }
      )
    },
    leave: function (el, done) {
      var vm = this
      Velocity(el,
        { opacity: 0 },
        {
          duration: this.fadeOutDuration,
          complete: function () {
            done()
            vm.show = true
          }
        }
      )
    }
  }
})
```

Finally, the ultimate way of creating dynamic transitions is through components that accept props
to change the nature of the transition(s) to be used.

### State Transitions

#### Animating State with Watchers

Watchers allow us to animate changes of any numerical property into another property.

Here is an example using [GreenSock](https://greensock.com/)

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>

<div id="animated-number-demo">
  <input v-model.number="number" type="number" step="20">
  <p>{{ animatedNumber }}</p>
</div>
```

```js
new Vue({
  el: '#animated-number-demo',
  data: {
    number: 0,
    tweenedNumber: 0
  },
  computed: {
    animatedNumber: function() {
      return this.tweenedNumber.toFixed(0);
    }
  },
  watch: {
    number: function(newValue) {
      TweenLite.to(this.$data, 0.5, { tweenedNumber: newValue });
    }
  }
})
```

#### Dynamic State Transitions

With Vue's transition components, the data backing state transitions can be updated in real time,
which is especially useful for prototying.

#### Organizing Transitions into Components

Managing many state transitions can quickly increase the complexity of a Vue instance or component.
Fortunately, many animations can be extracted out into dedicated child components.

#### Bringing Designs to Life

To animate, by one definition, means to bring to life.

## Reusablity and Composition

### Mixins

#### Basics

Mixins are a flexible way to distribute reusable functionalities for Vue components. A mixin object
can contain any component options. When a component uses a mixin, all options in the mixin will be
"mixed" into the component's own options.

```js
// define a mixin object
var myMixin = {
  created: function () {
    this.hello()
  },
  methods: {
    hello: function () {
      console.log('hello from mixin!')
    }
  }
}

// define a component that uses this mixin
var Component = Vue.extend({
  mixins: [myMixin]
})

var component = new Component() // => "hello from mixin!"
```

#### Option Merging

When a mixin and the component itself contain overlapping options, they will be "merged" using appropriate
strategies. For example, data objects undergo a recursive merge, with the component's data taking
priority in cases of conflicts.

```js
var mixin = {
  data: function () {
    return {
      message: 'hello',
      foo: 'abc'
    }
  }
}

new Vue({
  mixins: [mixin],
  data: function () {
    return {
      message: 'goodbye',
      bar: 'def'
    }
  },
  created: function () {
    console.log(this.$data)
    // => { message: "goodbye", foo: "abc", bar: "def" }
  }
})
```

Hook functions with the same name are merged into an array that all of them will be called. Mixin
hooks will be called __before__ the component's own hooks.

```js
var mixin = {
  created: function () {
    console.log('mixin hook called')
  }
}

new Vue({
  mixins: [mixin],
  created: function () {
    console.log('component hook called')
  }
})

// => "mixin hook called"
// => "component hook called"
```

Options that expect object values, for example `methods`, `components`, and `directives`, will be
merged into the same object. The component's options will take priority when there are conflicting
keys in these objects.

```js
var mixin = {
  methods: {
    foo: function () {
      console.log('foo')
    },
    conflicting: function () {
      console.log('from mixin')
    }
  }
}

var vm = new Vue({
  mixins: [mixin],
  methods: {
    bar: function () {
      console.log('bar')
    },
    conflicting: function () {
      console.log('from self')
    }
  }
})

vm.foo() // => "foo"
vm.bar() // => "bar"
vm.conflicting() // => "from self"
```

#### Global Mixin

You can also apply a mixin globally. However, use this with caution! Once you apply a mixin globally,
it will affect __every__ Vue instance created afterwards. When used properly, this can be used to inject
processing logic for custom options:

```js
// inject a handler for `myOption` custom option
Vue.mixin({
  created: function () {
    var myOption = this.$options.myOption
    if (myOption) {
      console.log(myOption)
    }
  }
})

new Vue({
  myOption: 'hello!'
})
// => "hello!"
```

Use global mixins sparsely and carefully, because it affects every single Vue instance created,
including third party components. In most cases, you should only use it for custom option handling.

#### Custom Option Merge Strategies

When custom options are merged, they use the default strategy which overwrites the existing value.

If you want a custom option to be merged using custom logic, you need to attach a function to
`Vue.config.optionMergeStrategies`.

```js
Vue.config.optionMergeStrategies.myOption = function (toVal, fromVal) {
  // return mergedVal
}
```

### Custom Directives

In addition to the default set of directives shipped in core (`v-model` and `v-show`), Vue also
allows you to register your own custom directives.

#### Basic

Global Registration:

```js
// Register a global custom directive called `v-focus`
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
})
```

Local Registration:

```js
directives: {
  focus: {
    // directive definition
    inserted: function (el) {
      el.focus()
    }
  }
}
```

Usage: Will cause the element to focus on page load.

```html
<input v-focus>
```

#### Hook Functions

A directive definition object can provide several hook functions (all optional):

- `bind`: called only once, when the directive is first bound to the element. This is where you can
  do one-time setup work.

- `inserted`: called when the bound element has been inserted into its parent node
  (this only guarantees parent node presence, not necessarily in-document).

- `update`: called after the containing component's VNode has updated, __but possibly before its
  children have updated__. The directive's value may or may not have changed.

- `componentUpdated`: called after the containing component's VNode __and the VNodes of its children__
  have updated

- `unbind`: called only once, when the directive is unbound from the element.

#### Hook Arguments

Directive hooks are passed these arguments:

- `el`: The element the directive is bound to. This can be used to directly manipulate the DOM.
- `binding`: An object containing the following properties.
    * `name`: The name of the directive, without the v- prefix.
    * `value`: The value passed to the directive. For example in v-my-directive="1 + 1", the value would be 2.
    * `oldValue`: The previous value, only available in update and componentUpdated. It is available
      whether or not the value has changed.
    * `expression`: The expression of the binding as a string. For example in v-my-directive="1 + 1",
      the expression would be "1 + 1".
    * `arg`: The argument passed to the directive, if any. For example in v-my-directive:foo, the arg
      would be "foo".
    * `modifiers`: An object containing modifiers, if any. For example in v-my-directive.foo.bar, the
      modifiers object would be { foo: true, bar: true }.
- `vnode`: The virtual node produced by Vue’s compiler. See the VNode API for full details.
- `oldVnode`: The previous virtual node, only available in the update and componentUpdated hooks.

Apart from `el`, you should treat these arguments as read-only and never modify them.

#### Dynamic Directive Arguments

Directive arguments can be dynamic. For example in `v-mydirective:[argument]="value"` the `argument`
can be updated based on data properties in the component instance.

Pin based on direction:

```html
<div id="dynamicexample">
  <h3>Scroll down inside this section ↓</h3>
  <p v-pin:[direction]="200">I am pinned onto the page at 200px to the left.</p>
</div>
```

```js
Vue.directive('pin', {
  bind: function (el, binding, vnode) {
    el.style.position = 'fixed'
    var s = (binding.arg == 'left' ? 'left' : 'top')
    el.style[s] = binding.value + 'px'
  }
})

new Vue({
  el: '#dynamicexample',
  data: function () {
    return {
      direction: 'left'
    }
  }
})
```

#### Object Literals

If your directive needs multiple values, you can also pass in a JavaScript object literal.

```html
<div v-demo="{ color: 'white', text: 'hello!' }"></div>
```

```js
Vue.directive('demo', function (el, binding) {
  console.log(binding.value.color) // => "white"
  console.log(binding.value.text)  // => "hello!"
})
```

### Render Functions and JSX

#### Basics

Vue recommends using templates to build your HTML in the vast majority of cases. There are situations
however, where you really need the full programmatic power of JavaScript. That's where you can use
the __render function__, a closer-to-compiler alternative to templates.

Basic example:
```html
<anchored-heading :level="1">Hello world!</anchored-heading>
```

Component
```js
Vue.component('anchored-heading', {
  render: function (createElement) {
    return createElement(
      'h' + this.level,   // tag name
      this.$slots.default // array of children
    )
  },
  props: {
    level: {
      type: Number,
      required: true
    }
  }
})
```

#### Nodes, Trees

When a browser reads code, it builds a tree of DOM nodes to help it keep track of everything.

Every HTML element is a node. Every piece of text is a node. Even comments are nodes! A node
is just a piece of the page. And as in a family tree, each node can have children.

Updating all these nodes efficiently can be difficult, but thankfull, you never have to do it manually.
Instead, you tell Vue what HTML you want on the page by template or render function and Vue automatically
keeps the page updated even when data changes.

#### Virtual DOM

Vue accomplishes this by building a __virtual DOM__ to keep track of the changes it needs to make
to the real DOM.

```js
return createElement('h1', this.blogTitle)
```

`createElement` returns information describing to Vue what kind of node it should render on the page,
including descriptions of any child nodes. We call this node description a _virtual node_, usually
abbreviated __VNode__. _Virtual DOM_ is what we call the entire tree of VNodes, built by a tree
of Vue components.

#### `createElement` Arguments

```js
// @returns {VNode}
createElement(
  // {String | Object | Function}
  // An HTML tag name, component options, or async
  // function resolving to one of these. Required.
  'div',

  // {Object}
  // A data object corresponding to the attributes
  // you would use in a template. Optional.
  {
    // (see details in the next section below)
  },

  // {String | Array}
  // Children VNodes, built using `createElement()`,
  // or using strings to get 'text VNodes'. Optional.
  [
    'Some text comes first.',
    createElement('h1', 'A headline'),
    createElement(MyComponent, {
      props: {
        someProp: 'foobar'
      }
    })
  ]
)
```

##### Data Object In-Depth

```js
{
  // Same API as `v-bind:class`, accepting either
  // a string, object, or array of strings and objects.
  class: {
    foo: true,
    bar: false
  },
  // Same API as `v-bind:style`, accepting either
  // a string, object, or array of objects.
  style: {
    color: 'red',
    fontSize: '14px'
  },
  // Normal HTML attributes
  attrs: {
    id: 'foo'
  },
  // Component props
  props: {
    myProp: 'bar'
  },
  // DOM properties
  domProps: {
    innerHTML: 'baz'
  },
  // Event handlers are nested under `on`, though
  // modifiers such as in `v-on:keyup.enter` are not
  // supported. You'll have to manually check the
  // keyCode in the handler instead.
  on: {
    click: this.clickHandler
  },
  // For components only. Allows you to listen to
  // native events, rather than events emitted from
  // the component using `vm.$emit`.
  nativeOn: {
    click: this.nativeClickHandler
  },
  // Custom directives. Note that the `binding`'s
  // `oldValue` cannot be set, as Vue keeps track
  // of it for you.
  directives: [
    {
      name: 'my-custom-directive',
      value: '2',
      expression: '1 + 1',
      arg: 'foo',
      modifiers: {
        bar: true
      }
    }
  ],
  // Scoped slots in the form of
  // { name: props => VNode | Array<VNode> }
  scopedSlots: {
    default: props => createElement('span', props.text)
  },
  // The name of the slot, if this component is the
  // child of another component
  slot: 'name-of-slot',
  // Other special top-level properties
  key: 'myKey',
  ref: 'myRef',
  // If you are applying the same ref name to multiple
  // elements in the render function. This will make `$refs.myRef` become an
  // array
  refInFor: true
}
```

##### Complete Example

```js
var getChildrenTextContent = function (children) {
  return children.map(function (node) {
    return node.children
      ? getChildrenTextContent(node.children)
      : node.text
  }).join('')
}

Vue.component('anchored-heading', {
  render: function (createElement) {
    // create kebab-case id
    var headingId = getChildrenTextContent(this.$slots.default)
      .toLowerCase()
      .replace(/\W+/g, '-')
      .replace(/(^-|-$)/g, '')

    return createElement(
      'h' + this.level,
      [
        createElement('a', {
          attrs: {
            name: headingId,
            href: '#' + headingId
          }
        }, this.$slots.default)
      ]
    )
  },
  props: {
    level: {
      type: Number,
      required: true
    }
  }
})
```

##### VNodes must be unique

All VNodes in the component tree must be unique. That means the followin render function is invalid:

```js
render: function (createElement) {
  var myParagraphVNode = createElement('p', 'hi')
  return createElement('div', [
    // Yikes - duplicate VNodes!
    myParagraphVNode, myParagraphVNode
  ])
}
```

To create identical paragraphs:

```js
render: function (createElement) {
  return createElement('div',
    Array.apply(null, { length: 20 }).map(function () {
      return createElement('p', 'hi')
    })
  )
}
```

#### Template Compilation

You may be interested to know that Vue's templates actually compile to render functions. This is
an implementation detail you don't need to know.

### Plugins

Plugins usually add global-level functionality to Vue. There are typically several types of plugins:
1. Add some global methods or properties. e.g. `vue-custom-element`
2. Add one or more global assets: directives/filters/transitions etc. e.g. `vue-touch`
3. Add some component options by global mixin. eg. `vue-router`
4. A library that provides an API of its own, while at the same time injecting some
   combination of the above. e.g. `vue-router`

Relevant URLs: [Plugin Docs](https://vuejs.org/v2/guide/plugins.html)

#### Using a plugin

Use plugins by calling `Vue.use()`

```javascript
// calls `MyPlugin.install(Vue)`
Vue.use(MyPlugin, { someOption: true });

new Vue({
  //... options
});
```

`Vue.use` automatically prevents you from using the same plugin more than once, so calling it
multiple times on the same plugin will install the plugin only once.

#### Writing a plugin

A Vue.js plugin should expose an `install` method. The method will be called with the `Vue` constructor
as the first argument, along with possible options.

```javascript
MyPlugin.install = function (Vue, options) {
  // 1. add global method or property
  Vue.myGlobalMethod = function () {
    // some logic ...
  }

  // 2. add a global asset
  Vue.directive('my-directive', {
    bind (el, binding, vnode, oldVnode) {
      // some logic ...
    }
    ...
  })

  // 3. inject some component options
  Vue.mixin({
    created: function () {
      // some logic ...
    }
    ...
  })

  // 4. add an instance method
  Vue.prototype.$myMethod = function (methodOptions) {
    // some logic ...
  }
}
```

### Filters

Vue.js allows you to define filters that can be used to apply common text formatting. Filters are
usable in two places: __mustache interpolations and `v-bind` expressions__. Filters should be
appended to the end of the JavaScript expression, denoted by the "pipe" symbol.

```html
<!-- in mustaches -->
{{ message | capitalize }}

<!-- in v-bind -->
<div v-bind:id="rawId | formatId"></div>
```

#### Local Filter

```js
filters: {
  capitalize: function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  }
}
```

#### Global Filter

```js
Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

new Vue({
  // ...
})
```

When the global filter has the same name as the local filter, the local filter will be preferred.

#### Chaining

Filters can be chained:

```html
{{ message | filterA | filterB }}
```

The filter's function always receives the expression's value (the result of the former chain)
as its first argument.

#### Multi Args

Filters are JavaScript functions, therefore than can take arguments:

```html
{{ message | filterA('arg1', arg2) }}
```

Here `filterA` is defined as a function taking three arguments. The value of `message` will be passed
into the first argument. The plain string `'arg1'` will be passed into its second argument, and the value
of expression `arg2` will be passed into the third argument.

## Tooling

### Single File Components

#### Introduction

In many Vue projects, global components will be defined using `Vue.component`, followed by
`new Vue({ el: '#container'})` to target a container element in the body of every page.

This can work very well for small to medium-sized projects, where JavaScript is only used to enhance
certain views. In more complex projects however, or when your frontend is entirely driven by JavaScript,
these disadvantages become apparent:

- __Global definitions__ force unique names for every component
- __String templates__ lack syntax highlighting and require ugly slashes for multiline HTML
- __No CSS support__ means that while HTML and JavaScript are modularized into components, CSS is
  conspicuously left out
- __No build step__ restrics us to HTML and ES5 JavaScript, rather than preprocessors like Pug and Babel

All of these are solved by __single-file components__ with a `.vue` extnsion, made possible with build
tools such as Webpack or Browserify.

For example:.
```html
<template>
    <p>{{ greeting }} World! </p>
</template>
<script>
module.exsports = {
    data: function() {
        return {
            greeting: 'Hello'
        }
    }
}
</script>
<style scoped>
p {
    font-size: 2em;
    text-align: center;
}
</style>
```

#### Separation of Concerns

One important thing to note is that __separation of concerns is not equal to separation of file types__.
In modern UI development, we have found that instead of dividing the codebase into three huge layers
that interwave with one another, it makes much more sense to divide them into loosely-coupled components
and compose them. Inside a component, its template, logic and styles are inherently coupled, and
collocating them actually makes the component more cohesive and maintainable.

Even if you don't like the idea of Single-File Components, you can still leverage its hot-reloading
and pre-compilation features by separating your JavaScript and CSS into separate files:

```html
<!-- my-component.vue -->
<template>
  <div>This will be pre-compiled</div>
</template>
<script src="./my-component.js"></script>
<style src="./my-component.css"></style>
```

### Unit Testing

#### Simple Assertions

You don't have to do anything special in your components to make them testable. Export raw options:

```html
<template>
  <span>{{ message }}</span>
</template>

<script>
  export default {
    data () {
      return {
        message: 'hello!'
      }
    },
    created () {
      this.message = 'bye!'
    }
  }
</script>
```

Then import the component options along with Vue, and you can make many common assertions.

```js
// Import Vue and the component being tested
import Vue from 'vue'
import MyComponent from 'path/to/MyComponent.vue'

// Here are some Jasmine 2.0 tests, though you can
// use any test runner / assertion library combo you prefer
describe('MyComponent', () => {
  // Inspect the raw component options
  it('has a created hook', () => {
    expect(typeof MyComponent.created).toBe('function')
  })

  // Evaluate the results of functions in
  // the raw component options
  it('sets the correct default data', () => {
    expect(typeof MyComponent.data).toBe('function')
    const defaultData = MyComponent.data()
    expect(defaultData.message).toBe('hello!')
  })

  // Inspect the component instance on mount
  it('correctly sets the message when created', () => {
    const vm = new Vue(MyComponent).$mount()
    expect(vm.message).toBe('bye!')
  })

  // Mount an instance and inspect the render output
  it('renders the correct message', () => {
    const Constructor = Vue.extend(MyComponent)
    const vm = new Constructor().$mount()
    expect(vm.$el.textContent).toBe('bye!')
  })
})
```

#### Writing Testable Components

A component's render output is primarily determined by the props it receives, If a component's
render output solely depends on its props it becomes straightforward to test, similar to asserting
the return value of a pure function with different arguments.

For example:
```html
<template>
  <p>{{ msg }}</p>
</template>

<script>
  export default {
    props: ['msg']
  }
</script>
```

You can assert is render output with different props using the `propsData` option:

```js
import Vue from 'vue'
import MyComponent from './MyComponent.vue'

// helper function that mounts and returns the rendered text
function getRenderedText (Component, propsData) {
  const Constructor = Vue.extend(Component)
  const vm = new Constructor({ propsData: propsData }).$mount()
  return vm.$el.textContent
}

describe('MyComponent', () => {
  it('renders correctly with different props', () => {
    expect(getRenderedText(MyComponent, {
      msg: 'Hello'
    })).toBe('Hello')

    expect(getRenderedText(MyComponent, {
      msg: 'Bye'
    })).toBe('Bye')
  })
})
```

#### Asserting Asynchronous Updates

Since Vue performs DOM updates, assertions on DOM updates resulting from state change will have to
be made in a `Vue.nextTick` callback.

```js
// Inspect the generated HTML after a state update
it('updates the rendered message when vm.message updates', done => {
  const vm = new Vue(MyComponent).$mount()
  vm.message = 'foo'

  // wait a "tick" after state change before asserting DOM updates
  Vue.nextTick(() => {
    expect(vm.$el.textContent).toBe('foo')
    done()
  })
})
```

### Production Deployment

#### Turn on Production Mode

During development, Vue provides a lot of warnings to help you with common errors and pitfalls.
However, these warning strings become useless in production and bloat your app's payload size.
In addition, some of these warning checks have small runtime costs that can be avoided in
production mode.

##### Without Build Tools

If you are using the full build, i.e. directly including Vue via a script tag without a build tool,
make sure to use the minified version (`vue.min.js`) for production.

##### With Build Tools

When using a build tool like Webpack or Browserify, the production mode will be determined by
`process.env.NODE_ENV` inside Vue's source code, and it will be in development mode by default.

In Webpack 4+:
```js
module.exports = {
  mode: 'production'
}
```

#### Pre-Compiling Templates

When using in-DOM templates or in-JavaScript template strings, the template-to-render-function
compilation is performed on the fly. This is usually fast enough in most cases, but is best avoided
if your application is performance-sensitive.

The easiest way to pre-compile templates is using Single-File Components. The associated build setups
automatically performs pre-compilation for you, so the built code contains the already compiled
render functions instead of raw template strings.

If you are using Webpack, and prefer separating JavaScript and template files, you can use
__vue-template-loader__, which also transforms the template files into JavaScript render functions
during the build step.

#### Tracking Runtime Errors

If a runtime error occurs during a component's render, it will be passed to the global
`Vue.config.errorHandler` config function if it has been set.

## Internals

### Reactivity in Depth

One of Vue's most distinct features is the unobtrusive reactivity system. Models are just plain
JavaScript objects. When you modify them, the view updates. It makes state management simple and
intuitive.

#### How Changes are Tracked

When you pass a plain JavaScript object to a Vue instance as its `data` option, Vue will walk through
all of its properties and convert them to getters/setters using `Object.defineProperty`. The getter/setters
are invisible to the user, but under the hood they enable Vue to perform dependency-tracking and
change-notification when properties are accessed or modified.

Every component instance has a corresponding __watcher__ instance, which records any properties
"touched" during the component's render as dependencies. Later on when a dependency's setter
is triggered, it notifies the water, which in turn causes the component to re-render.

```text

  Component
  Render
  Function  <------ Trigger Re-render -- Watcher
   |                                      ^
   |                                      |
  render                                Notify
   |                                      |
   v                                      |
 Virtual DOM Tree ------ touch --------> Data
                                          getter
                                          setter
```

#### Change Detection Caveats

Due to the limitations of modern JavaScript, Vue __cannot detect property addition or deletion__.
Since Vue performs the getter/setter conversion processing during instance initialization, a property
must be present in the `data` object in order for Vue to convert it and make it reactive.

For example:
```js
var vm = new Vue({
  data: {
    a: 1
  }
})
// `vm.a` is now reactive

vm.b = 2
// `vm.b` is NOT reactive
```

Vue does not allow dynamically adding new root-level reactive properties to an already crated instance.
However, it's possible to add reactive properties to a nested object as follows:

```js
Vue.set(vm.someObject, 'b', 2)
// or
this.$set(this.someObject, 'b', 2)
```

#### Declaring Reactive Properties

Since Vue doesn’t allow dynamically adding root-level reactive properties, you have to initialize
Vue instances by declaring all root-level reactive data properties upfront, even with an empty value:

```js
var vm = new Vue({
  data: {
    // declare message with an empty value
    message: ''
  },
  template: '<div>{{ message }}</div>'
})
// set `message` later
vm.message = 'Hello!'
```

#### Async Update Queue

Vue performs DOM updates __asynchronously__. Whenever a data change is observed, it will open a queue
and buffer all the data changes that happen in the same event loop. If the same watcher is triggered
multiple times, it will be pushed into the queue only once. This buffered de-duplication is important
in avoiding unnecessary calculations and DOM manipulations. Then, in the next event loop "tick", Vue
flushes the queue and performs the actual (already de-duped) work. Internally Vue tries native
`Promise.then`, `MutationObserver`, and `setImmediate` for the asynchronous queueing and falls back
to `setTimeout(fn, 0)`.

In order to wait until Vue.js has finished updating the DOM after a data change, you can use
`Vue.nextTick(callback)` immediately after the data is changed. The callback will be called after the
DOM has been updated. For example:

```html
<div id="example">{{ message }}</div>
```

```js
var vm = new Vue({
  el: '#example',
  data: {
    message: '123'
  }
})
vm.message = 'new message' // change data
vm.$el.textContent === 'new message' // false
Vue.nextTick(function () {
  vm.$el.textContent === 'new message' // true
})
```

There is also the `vm.$nextTick()` instance method, which is especially handy inside components,
because it doesn't need global `Vue` and its callback's `this` context will be automatically bound
to the current Vue instance:

```js
Vue.component('example', {
  template: '<span>{{ message }}</span>',
  data: function () {
    return {
      message: 'not updated'
    }
  },
  methods: {
    updateMessage: async function () {
      this.message = 'updated'
      console.log(this.$el.textContent) // => 'not updated'
      await this.$nextTick() // Returns a promise
      console.log(this.$el.textContent) // => 'updated'
    }
  }
})
```