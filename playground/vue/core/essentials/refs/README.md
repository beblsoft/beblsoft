# Vue Refs Documentation

Despite the exsitence of propes and events, sometimes you still need to directly access a child
component in JavaScript. To achieve this you can use Vue references.

## Assigning a reference

Assign a reference ID to the child component using the `ref` attribute.

```html
<base-input ref="usernameInput"></base-input>
```

## Reference in javascript

The element can then be referenced in javascript.

```javascript
this.$refs.usernameInput
```

## Notes

`$refs` are only populated after the component has been rendered, and they are not reactive.
It is only meant as an escape hatch for direct child manipulation. Avoid accessing `$refs` from
within templates or computed properties.




