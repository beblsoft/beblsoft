# Vue Slots Docuemntation

Just like with HTML elements, it's often useful to be able to pass content to a component, like this:
```html
<alert-box>
  Something bad happened.
</alert-box>
```

This task is made simple by Vue's custom `<slot>` element:
```javascript
Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})
```

When the component renders, `<slot></slot>` will be replaced by "Your Profile". Slots can contain
any template code, including HTML or even other components.

Note: If the component template for `alert-box` did not contain a `<slot>` element, any content
provided between its opening and closing tags would be discarded.

Relevant URLs:
[Basic Docs](https://vuejs.org/v2/guide/components.html),
[Advanced Docs](https://vuejs.org/v2/guide/components-slots.html)

## Compilation Scope

When you want to use data inside a slot, such as in:
```html
<navigation-link url="/profile">
  Logged in as {{ user.name }}
</navigation-link>
```

The slot has access to the same instance properties (i.e. the same "scope") as the rest of the template.
The slot does __not__ have access to `<navigation-link>`'s scope. For example trying to access `url` would
not work:
```html
<navigation-link url="/profile">
  Clicking here will send you to: {{ url }}
  <!--
  The `url` will be undefined, because this content is passed
  _to_ <navigation-link>, rather than defined _inside_ the
  <navigation-link> component.
  -->
</navigation-link>
```

As a rule, remeber that: Everything in the parent template is compiled in parent scope;
everything in the child template is compiled in the child scope.

## Default Content

There are cases when it's useful to specify fallback content for a slot, to be rendered only when
no content is provided. For example we might want "Submit" to be rendered inside the `<button>`
most of the time.
```html
<button type="submit">
  <slot>Submit</slot>
</button>
```

Now when we use the `<submit-button>` in a parent component, providing no content for the slot the
rendered content will be:
```html
<button type="submit"> Submit </button>
```

But if we provide content:
```html
<submit-button> Save </submit-button>
```

The content will render as:
```html
<button type="submit"> Save </button>
```

## Named Slots

There are times when it's useful to have multiple slots. For these cases, the `<slot>` element has
a special attribute, `<name>`, whcih can be used to define additional slots:
```html
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>
```

The `<slot>` outlet without `name` implicitly has the name "default".

To provide content to named slots, we can use the `v-slot` directive on a `<template>`, providing
the name of the slot as `v-slot`'s argument:
```html
<base-layout>
  <template v-slot:header>
    <h1>Header Content!</h1>
  </template>

  <template v-slot:default>
    <p>A paragraph for the main content.</p>
    <p>And another one.</p>
  </template>

  <template v-slot:footer>
    <p>Footer Content!</p>
  </template>
</base-layout>
```

The rendered HTML will be:
```html
<div class="container">
  <header>
    <h1>Header Content!</h1>
  </header>
  <main>
    <p>A paragraph for the main content.</p>
    <p>And another one.</p>
  </main>
  <footer>
    <p>Footer Content!</p>
  </footer>
</div>
```