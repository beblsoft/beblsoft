# Vue Router

The Vue Router is the official router for Vue.js. It deeply integrates with Vue.js core to make
building Single Page Applications with Vue.js a breeze.

Relevant URLs:
[Home](https://router.vuejs.org/),
[GitHub](https://github.com/vuejs/vue-router),
[Examples](https://github.com/vuejs/vue-router/tree/dev/examples)

## Features

- Nested route/view mapping
- Modular, component-based router configuration
- Route params, query, wildcards
- View transition effects powered by Vue.js transition system
- Fine-grained navigation control
- Links with automatic active CSS classes
- HTML5 history mode or hash mode, with auto-fallback in IE9
- Customizable Scroll Behavior

## Installation

### CDN Download

```html
<script src="https://unpkg.com/vue@2.6.10/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
```

### NPM

```bash
# Install in project
npm install --save vue-router
```

Usage:
```js
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
```

## Dynamic Route Matching

Very often we will nedd to map routes with the given pattern to the same component.
```js
const User = {
  template: '<div>User</div>'
}

const router = new VueRouter({
  routes: [
    // dynamic segments start with a colon
    { path: '/user/:id', component: User }
  ]
})
```
Now URLs like `/user/foo` and `/user/bar` both map to the same route.

A dynamic segment is denoted by a colon `:`. When a route is matched, the value of the dynamic
segments will be exposed as `this.$route.params` in every component. Therefore, we can render
the current user ID as follows:

```js
const User = {
  template: '<div>User {{ $route.params.id }}</div>'
}
```

### Reacting to parameter changes

One thing to note when using routes with params is that when the user navigates from `user/foo`
to `user/bar`, __the same component instance will be reused__. Since both routes render the same
component, this is more efficient than destroying the old instance and then creating a new one.
However, this means that the lifecycle hooks of the component will not be called.

To react to param changes in the same component:
```js
const User = {
  template: '...',
  watch: {
    '$route' (to, from) {
      // react to route changes...
    }
  }
}
```

### 404 not found

Regular parameters will only match characters in between url fragments. You can use the asterisk
`*` to match anything.
```js
{
  // will match everything
  path: '*'
}
{
  // will match anything starting with `/user-`
  path: '/user-*'
}
```

When using asterisk routes, make sure to correctly order your routes so that the asterisk ones
are at the end. The route `{ path: '*' }` is usually used to 404 client side.

When using an asterisk, a param named `pathMatch` is automatically added to `$route.params`. It
contains the rest of the url matched by the asterisk.

```js
// Given a route { path: '/user-*' }
this.$router.push('/user-admin')
this.$route.params.pathMatch // 'admin'

// Given a route { path: '*' }
this.$router.push('/non-existing')
this.$route.params.pathMatch // '/non-existing'
```

### Matching Priority

Sometimes the same URL may be matched by multiple routes. In such a case the matching priority is
determined by the order of route definition: the earlier a route is defined, the higher priority
it gets.

## Nested Routes

Real app UIs are usually composed of components that are nested multiple levels deep. It is also
very common that the segments of a URL corresponds to a certain structure of nested components.
For example:
```text
/user/foo/profile                     /user/foo/posts
+------------------+                  +-----------------+
| User             |                  | User            |
| +--------------+ |                  | +-------------+ |
| | Profile      | |  +------------>  | | Posts       | |
| |              | |                  | |             | |
| +--------------+ |                  | +-------------+ |
+------------------+                  +-----------------+
```

With `vue-router`, it is very simple to express this relationship using nested route configurations.

```js
const User = {
  template: `
    <div class="user">
      <h2>User {{ $route.params.id }}</h2>
      <router-view></router-view>
    </div>
  `
}
const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User,
      children: [
        {
          // UserProfile will be rendered inside User's <router-view>
          // when /user/:id/profile is matched
          path: 'profile',
          component: UserProfile
        },
        {
          // UserPosts will be rendered inside User's <router-view>
          // when /user/:id/posts is matched
          path: 'posts',
          component: UserPosts
        }
      ]
    }
  ]
})
```

The `User` component has its own `<router-view>` which can be toggled between the `UserProfile`
and `UserPosts` components.

Note that paths that start with `/` will be treated as a root path. This allows you to leverage the
component nesting without having to use a nested URL.

The `children` option is just another Array of route configuration objects like the `routes` itself.
This pattern can continue for an arbitrary level of nesting.

## Programmatic Navigation

Aside from using `<router-link>` to create anchor tags for declarative navigation, we can do this
programmatically using the router's instance methods.

Note: Inside the Vue instance, you have access to the router instance as `$router`.

### `router.push`

Full definition: `router.push(location, onComplete?, onAbort?)`

This method pushes a new entry into the history stack, so when the user clicks the browser back
button they will be taken to the previous URL. This is the method called internally when you click
a `<router-link>`.

Examples:
```js
// literal string path
router.push('home')

// object
router.push({ path: 'home' })

// named route
router.push({ name: 'user', params: { userId: '123' } })

// with query, resulting in /register?plan=private
router.push({ path: 'register', query: { plan: 'private' } })
```

### `router.replace`

Full definition: `router.replace(location, onComplete?, onAbort?)`

It acts like `router.push`, the only difference is that it navigates without pushing a new history
entry, just replacing the current entry.

### `router.go`

Full definition: `router.go(n)`

This method takes a single integer as parameter that indicates by how many steps to go forwards
or backwards in the history stack, similar to `window.history.go(n)`.

Examples:
```js
// go forward by one record, the same as history.forward()
router.go(1)

// go back by one record, the same as history.back()
router.go(-1)

// go forward by 3 records
router.go(3)

// fails silently if there aren't that many records.
router.go(-100)
router.go(100)
```

### History Manipulation

It is worth mentioning that Vue Router navigation methods (`push`, `replace`, and `go`) work
consistently in all router modes (`history`, `hash`, and `abstract`)

## Named Routes

Sometimes it is more convenient to identify a route with a name, especially when linking to a route
or performing navigations.

```js
const router = new VueRouter({
  routes: [
    {
      path: '/user/:userId',
      name: 'user',
      component: User
    }
  ]
})
```

To link to a named route, you can pass an object to the `router-link` component's `to` prop:
```html
<router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>
```

This is the same object used programatically with `router.push()`:
```js
router.push({ name: 'user', params: { userId: 123 }})
```

In both cases, the router will navigate to the path `/usr/123`.

## Named Views

Sometimes you need to display multiple views at the same time instead of nesting them, e.g. creating
a layout with a `sidebar` view and a `main` view. Instead of having one single outlet in your view,
you can have multiple and give each of them a name. A `router-view` without a name will be given
`default` as its name.

```html
<router-view class="view one"></router-view>
<router-view class="view two" name="a"></router-view>
<router-view class="view three" name="b"></router-view>
```

A view is rendered by using a comonent, therefore multiple views require multiple components for
the same route. Make sure to use the `components` (with an s) option.

```js
const router = new VueRouter({
  routes: [
    {
      path: '/',
      components: {
        default: Foo,
        a: Bar,
        b: Baz
      }
    }
  ]
})
```

### Nested Named Views

It is possible to create complex layouts using named views with nested views. When doing so, you
will also need to name nested `router-view` components used.
```text
/settings/emails                                       /settings/profile
+-----------------------------------+                  +------------------------------+
| UserSettings                      |                  | UserSettings                 |
| +-----+-------------------------+ |                  | +-----+--------------------+ |
| | Nav | UserEmailsSubscriptions | |  +------------>  | | Nav | UserProfile        | |
| |     +-------------------------+ |                  | |     +--------------------+ |
| |     |                         | |                  | |     | UserProfilePreview | |
| +-----+-------------------------+ |                  | +-----+--------------------+ |
+-----------------------------------+                  +------------------------------+
```

The `<template>` section for `UserSettings` looks like:
```html
<!-- UserSettings.vue -->
<div>
  <h1>User Settings</h1>
  <NavBar/>
  <router-view/>
  <router-view name="helper"/>
</div>
```

Then you can achieve the layout above with this route configuration:
```js
{
  path: '/settings',
  // You could also have named views at the top
  component: UserSettings,
  children: [{
    path: 'emails',
    component: UserEmailsSubscriptions
  }, {
    path: 'profile',
    components: {
      default: UserProfile,
      helper: UserProfilePreview
    }
  }]
}
```

## Redirect

Redirecting is also done in the `routes` configuration. To redirect from `/a` to `/b`:
```js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: '/b' }
  ]
})
```

The redirect can also target a named route:
```js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: { name: 'foo' }}
  ]
})
```

Or even use a function for dynamic redirection:
```js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: to => {
      // the function receives the target route as the argument
      // return redirect path/location here.
    }}
  ]
})
```

Note that Navigation guards are not applied on the route that redirects, only on its target. In
the example above adding `beforeEnter` or `beforeLeave` guards to `/a` route would not have any
effect.

## Alias

An alias of `/a` as `/b` means when the user visits `/b`, the URL remains `/b`, but it will be
matched as if the user is visiting `/a`.

The above can be expressed in the route configuration as:
```js
const router = new VueRouter({
  routes: [
    { path: '/a', component: A, alias: '/b' }
  ]
})
```

An alias gives you the freedom to map a UI structure to an arbitrary URL, instead of being constrained
by the configuration's nesting structure.

## Passing Props to Route Components

Using `$route` in your component creates a tight coupling with the route which limits the flexibility
of the component as it can only be used on certain URLs. To decouple the component from the router use
option `props`.

Instead of coupling to `$route`:
```js
const User = {
  template: '<div>User {{ $route.params.id }}</div>'
}
const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User }
  ]
})
```

Decouple it by using `props`:
```js
const User = {
  props: ['id'],
  template: '<div>User {{ id }}</div>'
}
const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User, props: true },

    // for routes with named views, you have to define the `props` option for each named view:
    {
      path: '/user/:id',
      components: { default: User, sidebar: Sidebar },
      props: { default: true, sidebar: false }
    }
  ]
})
```
This allows you to use the component anywhere, which makes the component easier to reuse and test.

### Boolean mode

When `props` is set to `true`, the `route.params` will be set as the component props.

### Object mode

When `props` is an object, this will be set as the component props as-is. Useful for when the props
are static.

```js
const router = new VueRouter({
  routes: [
    { path: '/promotion/from-newsletter', component: Promotion, props: { newsletterPopup: false } }
  ]
})
```

### Function mode

You can create a function that returns props. This allows you to cast parameters into other types,
combine static values with route-based values, etc.

```js
const router = new VueRouter({
  routes: [
    { path: '/search', component: SearchUser, props: (route) => ({ query: route.query.q }) }
  ]
})
```

The URL `/search?q=vue` would pass `{query: 'vue'}` as props to the `SearchUser` component.

Try to keep the `props` function stateless, as it's only evaluated on route changes. Use a wrapper
component if you need state to define the props, that way vue can react to state changes.

## HTML5 History Mode

The default mode for `vue-router` is hash mode - it uses the URL hash to simulate a full URL so that
the page won't be reloaded when the URL changes. To get rid of the hash, we can use the router's
__history mode__, which leverages the `history.pushState` API to achieve URL navigation without a
page reload.

```js
const router = new VueRouter({
  mode: 'history',
  routes: [...]
})
```

When using history mode, the URL will look "normal," e.g. http://oursite.com/user/id. Beautiful!

Here comes a problem, though: Since our app is a single page client side app, without a proper server
configuration, the users will get a 404 error if they access `http://oursite.com/user/id` directly in
their browser. Now that's ugly.

To fix the issue, all you need to do is add a simple catch-all fallback route to your server. If
the URL doesn't match any static assets, it should serve the same `index.html` page that your app
lives in.

### Apache

```text
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.html [L]
</IfModule>
```

### Firebase

firbase.json
```json
{
  "hosting": {
    "public": "dist",
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html"
      }
    ]
  }
}
```

### Caveat

There is a caveat to this: Your server will no longer report 404 errors as all not-found paths
now serve up your `index.html` file. To get around the issue, you should implement a catch-all
route within your Vue app to show a 404 page.
```js
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '*', component: NotFoundComponent }
  ]
})
```

## Navigation Guards

As the name suggests, the navigation guards provided by the `vue-router` are primarily used to guard
navigations either by redirecting it or canceling it. There are a number of ways to hook into the
route navigation process: globally, per-route, or in-component.

Remember that params or query changes won't trigger enter/leave navigation guards. You can either
watch the `$route` object to react to those changes, or use the `beforeRouteUpdate` in-component
guard.

### Global Before Guards

You can register global before guards using `router.beforeEach`.

```js
const router = new VueRouter({ ... })

router.beforeEach((to, from, next) => {
  // ...
})
```

Global before guards are called in creation order, whenever a navigation is triggered. Guards may
be resolved asynchronously, and the navigation is considered pending before all hooks have been
resolved.

Every guard function receives three arguments:
- `to: Route`: the target Route object being navigated to
- `from: Route`: the current route being navigated away from
- `next: Function`: this function must be called to resolve the hook. The action depends on the
  arguments provided to `next`:
    * `next()`: Move on to the next hook in the pipeline. If no hooks are leve, the navigation
      is confirmed
    * `next(false)`: abort the current navigation. If the browser URL was changed, it will be reset
      to that of the `from` route
    * `next('/') or next({ path: '/' })`: redirect to a different location. The current navigation will
      be aborted and a new one will be started. You can pass any location to `next`, which allows you to
      specify options like `replace:true`, `name:home`, ...
    * `next(error)`: if the argument passed to `next` is an instance of `Error`, the navigation will
      be aborted and the error will be passed to callbvacks registerd via `router.onError()`

### Global Resolve Guards

You can register a global guard with `router.beforeResolve`. This is similar to `router.beforeEach`,
with the difference that resolve guards will be called right before navigation is confirmed, after
all in-component guards and async route components are resolved.

### Global After Hooks

You can also register global after hooks. Unlike guards, these hooks do not get a `next` function
and cannot affect the navigation.
```js
router.afterEach((to, from) => {
  // ...
})
```

### Per-Route Guard

You can define `beforeEnter` guards directly on a route's configuration object:
```js
const router = new VueRouter({
  routes: [
    {
      path: '/foo',
      component: Foo,
      beforeEnter: (to, from, next) => {
        // ...
      }
    }
  ]
})
```

These guards have the exact same signature as global before guards.

### In-Component Guards

Finally, you can directly define route navigation guards inside route components.
```js
const Foo = {
  template: `...`,
  beforeRouteEnter (to, from, next) {
    // called before the route that renders this component is confirmed.
    // does NOT have access to `this` component instance,
    // because it has not been created yet when this guard is called!
  },
  beforeRouteUpdate (to, from, next) {
    // called when the route that renders this component has changed,
    // but this component is reused in the new route.
    // For example, for a route with dynamic params `/foo/:id`, when we
    // navigate between `/foo/1` and `/foo/2`, the same `Foo` component instance
    // will be reused, and this hook will be called when that happens.
    // has access to `this` component instance.
  },
  beforeRouteLeave (to, from, next) {
    // called when the route that renders this component is about to
    // be navigated away from.
    // has access to `this` component instance.
  }
}
```

The `beforeRouteEnter` guard does __NOT__ have access to `this`, because the guard is called before
the navigation is confirmed, thus the new entering component has not even been created yet. However,
you can access the instacne by passing a callback to `next`. The callback will be called when the
navigation is confirmed, and the component instance will be passed to the callback as the argument.

```js
beforeRouteEnter (to, from, next) {
  next(vm => {
    // access to component instance via `vm`
  })
}
```

The __leave guard__ is usually used to prevent the user from accidentally leaving the route with
unsaved edits. The navigation can be canceld by calling `next(false)`.
```js
beforeRouteLeave (to, from, next) {
  const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
  if (answer) {
    next()
  } else {
    next(false)
  }
}
```

### The Full Navigation Resolution Flow

1. Navigation triggered
2. Call leave guards in deactivated components
3. Call global `beforeEach` guards
4. Call `beforeRouteUpdate` guards in reused components
5. Call `beforeEnter` in route configs
6. Resolve async route components
7. Call `beforeRouteEnter` in activated components
8. Call global `beforeResolve` guards
9. Navigation confirmed
10. Call global `afterEach` hooks
11. Dome updates triggered
12. Call callbacks passed to `next` in `beforeRouteEnter` guards with instantiated instances

## Route Meta Fields

You can include a `meta` field when defining a route.
```js
const router = new VueRouter({
  routes: [
    {
      path: '/foo',
      component: Foo,
      children: [
        {
          path: 'bar',
          component: Bar,
          // a meta field
          meta: { requiresAuth: true }
        }
      ]
    }
  ]
})
```

Since routes can be nested, when a route is matched, it can potentially match more than one
route record. All route records matched by a route are exposed on the `$route` object as the
`$route.matched` Array. Therefore, we will need to iterate over `$route.matched` to check for meta
fields in route records.

For example:
```js
router.beforeEach((to, from, next) => {
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requriesAuth && !auth.loggedIn()) {
    // this route requires auth. if not logged in, redirect to login page.
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    });
  }
  next() // make sure to always call next()!
})
```

## Transitions

Since the `<router-view>` is essentially a dynamic component, we can apply transition effects to it
the same way using the `<transition>` component.

```html
<transition>
  <router-view></router-view>
</transition>
```

### Per-Route Transition

The above usage will apply the same transition for all routes. If you want each route's component
to have different transitions, you can instead use `<transition>` with different names inside each
route component.
```js
const Foo = {
  template: `
    <transition name="slide">
      <div class="foo">...</div>
    </transition>
  `
}

const Bar = {
  template: `
    <transition name="fade">
      <div class="bar">...</div>
    </transition>
  `
}
```

### Route-Based Dynamic Transition

If is also possible to determine the transition dynamically based on the relationship between
the target route and current route.

```html
<!-- use a dynamic transition name -->
<transition :name="transitionName">
  <router-view></router-view>
</transition>
```

```js
// then, in the parent component,
// watch the `$route` to determine the transition to use
watch: {
  '$route' (to, from) {
    const toDepth = to.path.split('/').length
    const fromDepth = from.path.split('/').length
    this.transitionName = toDepth < fromDepth ? 'slide-right' : 'slide-left'
  }
}
```

## Data Fetching

Sometimes you need to fetch data from the server when a route is activated. For example,
before rendering a user profile when a route is activated. For example, before rendering a user
profile, you need to fetch the user's data from the server. This can be achieved in two ways.
- Fetching after navigation: perform the navigation first, and fetch data in the incoming component's
  lifecycle hook
- Fetching Before navigation: fetch data before navigation in the route enter guard, and perform
  the navigation after data has been fetched

### Fetching After Navigation

When using this approach, we navigate and render the incoming component immediately, and fetch data
in the component's `created` hook. If gives us the opportunity to display a loading state while the data
is being fetched over the network, and we can also handle loading differently for each view.

Let's assume we have a `Post` component that needs to fetch the data for a post based on `route.params.id`.
```html
<template>
  <div class="post">
    <div v-if="loading" class="loading">
      Loading...
    </div>

    <div v-if="error" class="error">
      {{ error }}
    </div>

    <div v-if="post" class="content">
      <h2>{{ post.title }}</h2>
      <p>{{ post.body }}</p>
    </div>
  </div>
</template>
```

```js
export default {
  data () {
    return {
      loading: false,
      post: null,
      error: null
    }
  },
  created () {
    // fetch the data when the view is created and the data is
    // already being observed
    this.fetchData()
  },
  watch: {
    // call again the method if the route changes
    '$route': 'fetchData'
  },
  methods: {
    fetchData () {
      this.error = this.post = null
      this.loading = true
      // replace `getPost` with your data fetching util / API wrapper
      getPost(this.$route.params.id, (err, post) => {
        this.loading = false
        if (err) {
          this.error = err.toString()
        } else {
          this.post = post
        }
      })
    }
  }
}
```

### Fetching Before Navigation

With this approach we fetch data before actually navigating to the new route. We can perform the
data fetching in the `beforeRouteEnter` guard in the incoming component, and only call `next` when the
fetch is complete.

```js
export default {
  data () {
    return {
      post: null,
      error: null
    }
  },
  beforeRouteEnter (to, from, next) {
    getPost(to.params.id, (err, post) => {
      next(vm => vm.setData(err, post))
    })
  },
  // when route changes and this component is already rendered,
  // the logic will be slightly different.
  beforeRouteUpdate (to, from, next) {
    this.post = null
    getPost(to.params.id, (err, post) => {
      this.setData(err, post)
      next()
    })
  },
  methods: {
    setData (err, post) {
      if (err) {
        this.error = err.toString()
      } else {
        this.post = post
      }
    }
  }
}
```

The user will stay on the previous view while the resource is being fetched for the incoming view.
It is therefore recommended to display a progress bar or some kind of indicator while the data is
being fetched. If the data fetch fails, it's also necessary to display some kind of global warning
message.

## Scroll Behavior

When using client-side routing, we may want to scroll to top when navigating to a new route, or
preserve the scrolling position of history entries just like real page reload does. `vue-router`
allows you to achieve these and even better, allows you to completely customize the scroll
behavior on route navigation.

Note: this feature only works if the browser supports `history.pushState`.

When creating the router instance, you can provide the `scrollBehavior` function.
```js
const router = new VueRouter({
  routes: [...],
  scrollBehavior (to, from, savedPosition) {
    // return desired position
  }
})
```

Returning the `savedPosition` will result in a native-like behavior when navigating with back/forward
buttons.
```js
scrollBehavior (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  } else {
    return { x: 0, y: 0 }
  }
}
```

If you want to simulat the "scrool to anchor" behavior:
```js
scrollBehavior (to, from, savedPosition) {
  if (to.hash) {
    return {
      selector: to.hash
      // , offset: { x: 0, y: 10 }
    }
  }
}
```

Can also use route meta fields to implement fine-grained scroll behavior control.

### Async Scrolling

You can also return a Promise that resolves to the desired position descriptor.
```js
scrollBehavior (to, from, savedPosition) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ x: 0, y: 0 })
    }, 500)
  })
}
```

## Lazy Loading Routes

When building apps with a bundler, the JavaScript bundle can become quite large, and this affect
the page load time. If would be more efficient if we can split each route's components into a separate
chunk, and only load them when the route is visited.

Combining Vue's async component feature and webpack's code splitting feature, it's trivially
easy to lazy-load route components.

First, an async component can be defined as a factory function that returns a Promise(whcih should
resolve to the component itself):
```js
const Foo = () => Promise.resolve({ /* component definition */ })
```

Second, in webpack 2, we can use the dynamic import syntax to indicate a code-split point.
```js
import('./Foo.vue') // returns a Promise
```

Combinding th etwo, this is how to define an async component that will automatically code-split
by webpack.
```js
const Foo = () => import('./Foo.vue')

const router = new VueRouter({
  routes: [
    { path: '/foo', component: Foo }
  ]
})
```

### Grouping Components in the Same Chunk

Sometimes we may want to group all the components nested under the same route into the same async
chunk. To achieve that we need to use named chunks by providing a chunk name using special comment
syntax (requires webpack > 2.4).
```js
const Foo = () => import(/* webpackChunkName: "group-foo" */ './Foo.vue')
const Bar = () => import(/* webpackChunkName: "group-foo" */ './Bar.vue')
const Baz = () => import(/* webpackChunkName: "group-foo" */ './Baz.vue')
```

Webpack will group any async module with the same chunk name into the same async chunk.

## API Reference

### `router-link`

`<router-link>` is the component for enabling user navigation in a router-enabled app.

### `router-link` props

- __to__

  type: `string | Location`

  Denotes the target route of the link. When clicked, the value of the `to` prop will be passed
  to `router.push()` internally.
  ```html
  <!-- literal string -->
  <router-link to="home">Home</router-link>
  <!-- renders to -->
  <a href="home">Home</a>

  <!-- javascript expression using `v-bind` -->
  <router-link v-bind:to="'home'">Home</router-link>

  <!-- Omitting `v-bind` is fine, just as binding any other prop -->
  <router-link :to="'home'">Home</router-link>

  <!-- same as above -->
  <router-link :to="{ path: 'home' }">Home</router-link>

  <!-- named route -->
  <router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>

  <!-- with query, resulting in `/register?plan=private` -->
  <router-link :to="{ path: 'register', query: { plan: 'private' }}">Register</router-link>
  ```

- __replace__

  type: `boolean`

  Setting `replace` prop will call `router.replace()` instead of `router.push()`

  ```html
  <router-link :to="{ path: '/abc'}" replace></router-link>
  ```

- __append__

  type: `boolean`

  Setting `apend` always appends the relative path to the current path

  ```html
  <router-link :to="{ path: 'relative/path'}" append></router-link>
  ```

- __tag__

  type: `string`

  default: `a`

  Sometimes we want `<router-linl>` to render as another tag, e.g. `<li>`. Then we can use `tag`
  prop to specify which tag to render to, and it will still listen to click events for navigation.

  ```html
  <router-link to="/foo" tag="li">foo</router-link>
  <!-- renders as -->
  <li>foo</li>
  ```

- __active-class__

  type: `string`

  default: `'router-link-active'`

  Configure the active CSS class applied when the link is active. Note the default value can also
  be configured globally via the `linkActiveClass` router constructor option

- __exact__

  type: `boolean`

  The default active class matching behavior is __inclusive match__. One consequence of this is that
  `<router-link to="/">` will be active for every route!. To force the link into "exact match mode"
  use the `exact` prop

  ```html
  <!-- this link will only be active at `/` -->
  <router-link to="/" exact>
  ```

- __event__

  type: `string | Array<string>`

  default: `'click'`

  Specify the event(s) that can trigger the link navigation

- __exact-active-class__

  type: `string`

  default: `'router-link-exact-active'`

  Configure the active CSS class applied when the link is active with exact match.

### `router-view`

The `<router-view>` component is a functional component that renders the matched component for the
given path. Components rendered in `<router-view>` can also contain its own `<router-view>`, which
will render componsnets for nested paths.

### `router-view` props

- __name__

  type: `string`

  default: `'default'`

  When a `<router-view>` has a name, it will render the component with the corresponding name
  in the matched route record's `components` option

### Router Construction Options

- __routes__

  type: `Array<RouteConfig>`

  RouteConfig Type:
  ```js
  declare type RouteConfig = {
    path: string;
    component?: Component;
    name?: string; // for named routes
    components?: { [name: string]: Component }; // for named views
    redirect?: string | Location | Function;
    props?: boolean | Object | Function;
    alias?: string | Array<string>;
    children?: Array<RouteConfig>; // for nested routes
    beforeEnter?: (to: Route, from: Route, next: Function) => void;
    meta?: any;

    // 2.6.0+
    caseSensitive?: boolean; // use case sensitive match? (default: false)
    pathToRegexpOptions?: Object; // path-to-regexp options for compiling regex
  }
  ```

- __mode__

  type: `string`

  default: `"hash" (in browser) | "abstract" (in Node.js)`

  Configure the router mode

    * `hash`: uses the URL hash for routing. Works in all Vue-supported browsers
    * `history`: requires HTML5 History API and server config
    * `abstract`: works in all JavaScript environments

- __base__

  type: `string`

  default: `'/'`

  The base URL of the app. For example, if the entire SPA is served under `/app/`, then `base` should
  use the value '/app/'

- __linkActiveClass__

  type: `string`

  default: `'router-link-active'`

  Globally configure `<router-link>`

- __scrollBehavior__

  type: `Function`

  Signature:
  ```js
  type PositionDescriptor =
    { x: number, y: number } |
    { selector: string } |
    ?{}

  type scrollBehaviorHandler = (
    to: Route,
    from: Route,
    savedPosition?: { x: number, y: number }
  ) => PositionDescriptor | Promise<PositionDescriptor>
  ```

- __parseQuery / stringifyQuery__

  type: `Function`

  Provide custom query string parse / stringify functions. Overrides the default.

- __fallback__

  type: `boolean`

  default: `true`

  Contorls wheter the router should fallback to `hash` mode when the browser does not support
  `history.pushState` but mode is set to `history`

  Setting this to `false` essentiall makes every `router-link` navigation a full page refresh in IE9

### Router Instance Properties

- __router.app__

  type: `Vue instance`

  The root Vue instance the `router` was injected into

- __router.mode__

  type: `string`

  The mode (hash, history, abstract) the router is using.

- __router.currentRoute__

  type: `Route`

  The current route represented as a Route Object

### Router Instance Methods

- __router.beforeEach__, __router.beforeResolve__, __router.afterEach__

  Signatures:
  ```js
  router.beforeEach((to, from, next) => {
    /* must call `next` */
  })

  router.beforeResolve((to, from, next) => {
    /* must call `next` */
  })

  router.afterEach((to, from) => {})
  ```

- __router.push__, __router.replace__, __router.go__, __router.back__, __router.forward__

  Signatures:
  ```js
  router.push(location, onComplete?, onAbort?)
  router.replace(location, onComplete?, onAbort?)
  router.go(n)
  router.back()
  router.forward()
  ```

- __router.getMatchedComponents__

  Signature:
  ```js
  const matchedComponents: Array<Component> = router.getMatchedComponents(location?)
  ```

  Returns an Array of the components (definition/constructor, not instances) matched by the provided
  location or the current route. This is mostly used during server-side rendering to perform
  data prefecting.

- __router.resolve__

  Signature:
  ```js
  const resolved: {
    location: Location;
    route: Route;
    href: string;
  } = router.resolve(location, current?, append?)
  ```

  Reverse URL resolving. Given location in form same as used in `<router-link/>`

- __router.addRoutes__

  Signature:
  ```js
  router.addRoutes(routes: Array<RouteConfig>)
  ```

  Dynamically add more routes to the router. The argument must be an Array using the same route config
  format with the `routes` constructor option

- __router.onReady__

  Signature:
  ```js
  router.onReady(callback, [errorCallback])
  ```

  This method queues a callback to be called when the router has completed the initial navigation,
  which means it has resolved all async enter hooks and async components that are associated with
  the initial route.

- __router.onError__

  Signature:
  ```js
  router.onError(callback)
  ```

  Register a callback which will be called when an error is caught during a route navigation.
  An error could be called for the following reasons:
    * The error is thrown synchronously inside a route guard function
    * The error is caught and asynchronously handled by calling `next(err)` inside a route guard function
    * An error occurred when trying to resolve an async component that is required to render a route

### The Route Object

A route object represents the state of the current active route. It contains parsed information of
the current URL and the route records matched by the URL.

The route object is immutable. Every successful navigation will result in a fresh route object.
The route object can be found in multiple places:
- Inside components as `this.$route`
- Inside `$route` watch callbacks
- As the return value of calling `router.match(location)`
- Inside navigation guards as the first two arguments:

  ```js
  router.beforeEach((to, from, next) => {
    // `to` and `from` are both route objects
  })
  ```
- Inside the `scrollBehavior` function as the first two arguments:

  ```js
  const router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
      // `to` and `from` are both route objects
    }
  })
  ```

Route Object Properties
- __$route.path__

  type: `string`

  A string the equals the path of the current route, always resolved as an absolute path.
  E.g. `"/foo/bar"`

- __$route.params__

  type: `Object`

  An object that contains key/value pairs of dynamic segments and star segments. If there are no params
  the value will be an empty object

- __$route.query__

  type: `Object`

  An object that contains key/value pairs of the query string. For example, for a path `/foo?user=1`,
  we get `$route.query.user == 1`. If there is no query the value will be an empty object.

- __$route.hash__

  type: `string`

  The hash of the current route (with the `#`), if it has one. If no hash is present the value will
  be an empty string

- __$route.fullPath__

  type: `string`

  The full resolved URL including query and hash

- __$route.matched__

  type: `Array<RouteRecord>`

  An arrray containing route records for all nested path segments of the current route. Route records are
  the copies of the objects in the `routes` configuration Array (and in `children` Arrays)

- __$route.name__

  The name of the current route, if it has one.

- __$route.redirectedFrom__

  The name of the route being redirected from, if there were one.

### Component Injections

Component Injected Properties. These properties are injected into every child componented by passing
the router instance to the root instance as the `router` option.

- __this.$router__

  The router instance

- __this.$route__

  The current active Route. This property is read-only and its properties are immutable, but it
  can be watched.

Component Enabled options:

- __beforeRouteEnter__
- __beforeRouteUpdate__
- __beforeRouteLeave__
