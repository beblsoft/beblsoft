# Vue Router Samples Documentation

Applications leveraging the Vue Router.

## single files

Files
- `basic.html`: basic dynamic routing
- `guardLifecycle.html`: Ties into route lifecycle to log route transitions
- `loginGuards.html`: Example router guard
- `programaticNav.html`: Demo programatic navigation
- `props.html`: Pass props while routing
- `redirects.html`: Demo redirecting routes to others
- `variables.html`: Print out routing parameters
- `namedViews.html`: Route to multiple components at once in one route
- `dataFetching.html`: Demo a component displaying a loading spinner while it is fetching data on load
- `transition.html`: Demo transitioning between routes.

To Run
- Open html files in browser

## router-app

Demonstrates routing amongst different component files.

Relevant URLs:
[Getting Started with Vue Router](https://scotch.io/tutorials/getting-started-with-vue-router),
[Webpack Router App](https://vuejs-templates.github.io/webpack)

Starting File:
- `index.html`:starting point for single page application (SPA), link `app`.

Router Files
- `index.js`: defines all routes
- `App.vue`: main page, link template 'App' from main.js (add top level content here)
- `main.js`: main view component 'app' is the key from index.html

Component Files
- `About.vue`: view displayed when About text pressed, hot text in App.vue
- `Coins.vue`: view displayed when Ethereum/BitCoin text pressed, hot text in App.vue
- `Hello.vue`: view displayed when Home text pressed, hot text in App.vue
- `DarrylTest.vue`: view displayed when DarrylTest text pressed, hot text in App.vue

To add a new route:

1. Add new component in components directory.
2. Add reference to new component in `App.vue`.
3. Import component in `route/index.js` and new route in routes table

To Run:

```bash
# Enter Directory
cd ./router-app

# Install dependencies
npm install

# Start application
npm run dev # Open chrome to http://localhost:8080
```

## lazy-load-app

This is a Vue CLI app that demonstrates lazily loading components so the whole SPA doesn't need to
be genenerated in massive JS file that can take a long time to load.

The lazy loading is done in `lazy-load-app/src/router.js` where the `About.vue` component is lazily
loaded. When the app is built via `npm run build`, a separate js file, for example `dist/js/about.0332771a.js`,
is built for the `About` component.

To Run:

```bash
# Enter Directory
cd ./lazy-load-app

# Install dependencies
npm install

# Start application
npm run serve # Open chrome to http://localhost:8080
```
