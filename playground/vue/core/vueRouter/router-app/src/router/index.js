/* This is the boilerplate, replace with below
import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    }
  ]
}) */
import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import About from '@/components/About'
import List from '@/components/List'
import Coins from '@/components/Coins'
import DarrylTest from '@/components/DarrylTest'
import Component404 from '@/components/Component404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/list',
      name: 'List',
      component: List
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/darryltest',
      name: 'DarrylTest',
      component: DarrylTest
    },
     {
      path: '/component404',
      name: 'Component404',
      component: Component404
    },
    {
      path: '/coins/:id',
      name: 'Coins',
      component: Coins
    }
  ]
})

