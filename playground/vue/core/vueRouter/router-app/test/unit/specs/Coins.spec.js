import Vue from 'vue'
import Coins from '@/components/Coins'

describe('Coins.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Coins)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.coins h1').textContent)
      .toEqual('The Coins')
  })
})
