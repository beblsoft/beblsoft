import Vue from 'vue'
import DarrylTest from '@/components/DarrylTest'

describe('DarrylTest.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(DarrylTest)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.darrylTest h1').textContent)
      .toEqual('Extend with new DarrylTest route')
  })
})
