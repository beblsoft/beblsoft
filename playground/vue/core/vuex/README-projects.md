# Vuex Samples Documentation

## single files

Files
- `counter.html`: Increments a counter in the store
- `dynamic.html`: Demonstrate dynamically rendering component content based on store state
- `formHandling.html`: Bind contents of a form directly into the store.
- `getters.html`: Map store getters into component.
- `login.html`: Demonstrate conditional rendering based on login state
- `mapState.html`: Map store state directly into a component
- `mutations.html`: Map store mutations directly into component methods
- `modules.html`: Demo vuex store among two different modules

To Run
- Enter Directory: `cd ./routing`
- Open html files in browser

## vuex-note-app

This code is for the tutorial on building a Notes App using VueJs and Vuex
Full tutorial is [here](http://coligo.io/learn-vuex-by-building-notes-app/).

To Run
- Enter Directory: `cd ./vuex/vuex-note-app`
- Install dependencies: `npm install`
- Start application: `npm run dev`. Open Chrome to http://localhost:8080
- Build for production: `npm run build`