# Vuex Documentation

Vuex is a state management pattern and library for Vue.js applications. It serves as a centralized store
for all the components in an application and integrates with Vue's devtools extension to provide advanced
features such as zero-config and time-travel debugging. It handles the following cases:
1. Multiple views depend on the same piece of state
2. Actions from different views need to mutate the same piece of state

Relevant URLs:
[Home](https://vuex.vuejs.org/),
[GitHub](https://github.com/vuejs/vuex)

## Architecture

```text
                        Backend API
                              |
                              |
                    ----------------------------
                    |   Actions          Vuex  |
                    |  /       \               |
               1 Dispatch       2 Commit       |
                 /  |               \          |
     Vue Components |                Mutations ---- Devtools
                \   |               /          |
                4 Render         3 Mutate      |
                    |  \        /              |
                    |    State                 |
                    ----------------------------
```

## Store

At the center of every Vuex application, the store is a container that holds application state.
It is reactive, meaning components will reactively update on store changes. Committing Mutations is the
only way to change store state.

## State

Vuex uses a single state tree.
```javascript
state: {
  counter: 0
}
```

## Mutation

A mutation is a function that changes store state. Each mutation has a string type and handler.
Mutations are the only way to change store state. Mutations must be synchronous.
```javascript
mutations: {
  increment(state) {
    state.counter++;
  }
}
```

## Commit

Commit is the name for calling a mutation: `this.$store.commit('increment')`

## Getters

Mechanism to compute derived state based on store state variables.
```javascript
getters: {
  numNotDoneTodos: state => {
    return state.todos.filter(todo => !todo.done).length;
  },
  numDoneTodos: state => {
    return state.todos.filter(todo => todo.done).length;
  }
}
```

## Actions

Actions are similar to mutations, but actions can be async and can commit mutations.

```javascript
actions: {
  increment ({ commit }) {
    commit('increment')
  }
```

To trigger actions: `this.$store.dispatch('increment')`

## Modules

Due to using a single state tree, all state of the application is contained inside one big object.
However, as the application grows in scale, the store can get very bloated.

To help with that, Vuex allows us to divide our store into modules. Each module can contain its
own state, mutations, actions, getters and even nested modules.

```javascript
const moduleA = {
  state: { ... },
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}

const moduleB = {
  state: { ... },
  mutations: { ... },
  actions: { ... }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})

store.state.a // -> `moduleA`'s state
store.state.b // -> `moduleB`'s state
```

### Namespacing

By default, actions, mutations, and getters inside modules are still registered under the
__global namespace__. This allows multiple modules to react to the same mutation/action type.

If you want your modules to be more self-contained and reusable, you can mark them as `namespaced: true`.
When the module is registered, all of its getters, actions, and mutations will be automatically namespaced
based on the path the module is registered at.

For example:
```javascript
const store = new Vuex.Store({
  modules: {
    account: {
      namespaced: true,

      // module assets
      state: { ... }, // module state is already nested and not affected by namespace option
      getters: {
        isAdmin () { ... } // -> getters['account/isAdmin']
      },
      actions: {
        login () { ... } // -> dispatch('account/login')
      },
      mutations: {
        login () { ... } // -> commit('account/login')
      },

      // nested modules
      modules: {
        // inherits the namespace from parent module
        myPage: {
          state: { ... },
          getters: {
            profile () { ... } // -> getters['account/profile']
          }
        },

        // further nest the namespace
        posts: {
          namespaced: true,

          state: { ... },
          getters: {
            popular () { ... } // -> getters['account/posts/popular']
          }
        }
      }
    }
  }
})
```

### Dyamic Module Registration

You can register a module __after__ the store has been created as follows:
```javascript
// register a module `myModule`
store.registerModule('myModule', {
  // ...
})

// register a nested module `nested/myModule`
store.registerModule(['nested', 'myModule'], {
  // ...
})
```

## Strict mode

Strict mode enforces that vuex state is not mutated outside of mutations. Note: do not use this in
production as it degrades performance.
```javascript
const store = new Vuex.Store({
  // ...
  strict: true
})
```

