# Vue Dev Tools Documentation

Vue Dev Tools is a browser extension that assists in developing and debugging Vue applications.

Cool Features:
- Live edit component data
- Debug Vuex with Time Travel
- Track events in application

Browser Options:
[Chrome Plugin](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd),
[Firefox Addon](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/)

Relevant URLs:
[Medium](https://medium.com/vue-mastery/how-to-use-the-vue-devtools-af95191ff472),
[GitHub](https://github.com/vuejs/vue-devtools)

