# Vue CLI

Vue CLI is the standard tooling command line interface for Vue.js development.

Relevant URLs:
[Home](https://cli.vuejs.org/),
[GitHub](https://github.com/vuejs/vue-cli),
[Medium](https://medium.com/the-vue-point/vue-cli-3-0-is-here-c42bebe28fbb)

## Features

- __Feature rich__: Out-of-the-box support for babel, typescript, eslint, postCSS, PWA, unit testing, and
  end-to-end testing
- __Extensible__: Extended via the plugin system
- __No need to eject__: Fully configurable without the need for ejecting. Allows your project to stay up-to-date
  for the long run.
- __Graphical User Interface__: Create, develop, and manage projects with GUI
- __Instant Prototyping__: Instantly prototype new ideas with single Vue file
- __Future Ready__: Effortlessly ship native ES2015 code for modern browsers

## Components

- __CLI__ (`@vue/cli`) is a globally install npm package and provides the `vue` command in your terminal.
  It provides the ability to quickly scaffold a new project via `vue create`, or prototype new ideas
  via `vue serve`.

- __CLI Service__ (`@vue/cli-service`) is a development dependency. It's an npm package installed
  locally into every project created by `@vue/cli`. It is built on top of webpack and webpack-dev-server.

- __CLI Plugins__: CLI plugins are npm packages that provide optional features to your Vue CLI projects,
  such as Babel/TypeScript transiplation, ESLint integration, unit testing, and end-to-end testing. All
  plugin names should start with `@vue/cli-plugin-` or `vue-cli-plugin-`

## Installation

To install the new package, use one of the following commands:
```bash
sudo npm install -g @vue/cli
# OR
sudo yarn global add @vue/cli
```

After installation, you will have access to the `vue` binary in your command line. You can verify
that it is properly installed by simply running `vue`. Check the version as follows:
```javascript
vue --version
```

## Instant Prototyping

You can rapidly prototype with a single `*.vue` file with the `vue serve` and `vue build` commands,
but they require an additional global addon to be installed first:
```bash
sudo npm install -g @vue/cli-service-global
# or
sudo yarn global add @vue/cli-service-global
```

### vue serve

Allows one to quickly serve a vue file in the browser:
```bash
vue serve --open instantPrototype.vue
```

The drawback of `vue serve` is that it relies on globally installed dependencies which may be
inconsistent across machines. Therefore this is only recommended for rapid prototyping.

### vue build

Quickly build for production:
```bash
vue build --target app --dest dist instantPrototype.vue
```

## Creating a project

To create a new project, run:
```bash
vue create hello-world
```

To run the new project

```bash
cd hello-world

# Run for development
npm run serve

# Build for production
npm run build
```

Manually select presets. If you chose to manually select features, the prompts will have you save your
selections as a preset for the future. These configuration options are stored in `~/.vuerc`.

## Plugins

Vue CLI uses a plugin-based architecture. If you inspect a newly created project's `package.json`,
you will find dependencies that start with `@vue/cli-plugin-`. Plugins can modify the internal
webpack configuration and inject commands to `vue-cli-service`. Most of the features listed during
the project creation process are implemented as plugins. The plugin architecture makes Vue CLI
flexible and extensible.

Plugins are composed of two components:
1. Generator - create files
2. Runtine plugin - tweaks the core webpack config and injects commands

### Install plugins

To install plugins into an existing project use the `vue add` command:
```bash
vue add @vue/eslint
vue add @vue/eslint --config airbnb --lintOn save  # skips promps
```

## Presets

A Vue CLI preset is a JSON object that contains pre-defined options and plugins for creating a new
project so that the user doesn't have to go through the prompts to select them.

Presets are saved during `vue create` and are stored in the configuration file `~/.vuerc`.
An example preset file is as follows:
```json
{
  "useConfigFiles": true,
  "router": true,
  "vuex": true,
  "cssPreprocessor": "sass",
  "plugins": {
    "@vue/cli-plugin-babel": {},
    "@vue/cli-plugin-eslint": {
      "config": "airbnb",
      "lintOn": ["save", "commit"]
    }
  }
}
```

### Local filesystem presets

You can save a preset to anywhere in the local filesystem and then use if for subsequent project creations.
```bash
vue create --preset ./foo/bar/my-preset my-project
```

## The CLI Service

Inside a Vue CLI project, `@vue/cli-service` installs a binary named `vue-cli-service`.
The default scripts in `package.json` are as follows:
```json
{
  "scripts": {
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build"
  }
}
```

You can invoke the `vue-cli-service` directly with `npx`.
```bash
npx vue-cli-service serve
```

### vue-cli-service serve

The `vue-cli-service serve` command starts a dev server that comes with Hot-Module-Replacement (HMR)
working out of the box.

Relevant Options:
- `--open`: open browser on server start
- `--copy`: copy url to clipboard on server start
- `--mode`: specify env mode (default: development)
- `--host`: specify host (default: 0.0.0.0)
- `--port`: specify port (default: 8080)
- `--https`: use https (default: false)

### vue-cli-service build

The `vue-cli-service build` command produces a production-ready bundle in the `dist/` directory,
with minification for JS/CSS/HTML and auto vendor chunk splitting for better caching. The chunk
manifest is inlined into the HTML.

Relevant Options:
- `--mode`: specify env mode (default: production)
- `--dest`: specify output directory (default: dist)
- `--modern`: build app targeting modern browsers with auto fallback
- `--target`: app | lib | wc | wc-async (default: app)
- `--formats`: list of output formats for library builds (default: commonjs,umd,umd-min)
- `--name`: name for lib or web-component mode (default: "name" in package.json or entry filename)
- `--no-clean`: do not remove the dist directory before building the project
- `--report`: generate report.html to help analyze bundle content
- `--report-json`: generate report.json to help analyze bundle content
- `--watch`: watch for changes

### vue-cli-service inspect

Use the `vue-cli-service inspect` command to inspect the webpack config inside a Vue CLI project.

Relevant Options:
- `--mode`: specify env mode (default: production)

## Browser Compatibility

### browserslist

You will notice a `browserslist` field in package.json (or a separate `browserslistrc` file) specifying
a range of browsers the project is targeting. The value is used by @babel/preset-env and autoprevixer
to automatically determine the JavaScript features that need to be transiled and the CSS vendor
prefixes needed.

### Polyfills

A default Vue CLI project uses @vue/babel-preset-app, which uses @babel/preset-env and the browserslist
config to determine the Polyfills needed for your project.

By default it passes `useBuiltIns: 'usage` to `@babel/present-env` which automatically detects the
polyfills needed based on the language features used in your source code.

### Modern Mode

With Bebel we are able to leverage all the newest language features in ES2015+, but that also
means we have to ship transiled and polyfilled bundles in order to support older browsers.
Transpiled bundles are often more verbose, parse, and run slower than the original. Given that most
modern browsers have decent support for native ES2015, it is a waste that we have to ship the heavier
and less efficient code to those browsers just because we have to support older ones.

Vue CLI offers a "Modern Mode" to help solve this problem: `vue-cli-service build --modern`
Here Vue CLI will produce two versions of your app:
1. A modern bundle targeting modern browsers that support ES modules
2. A legacy bindle targeting older browsers that do not

The generated HTML file is able to detect that browser it is running in and load the correct version.
Vue CLI uses two environment variables to communicate the mode:
- `VUE_CLI_MODERN_MODE`: The build was started with the --modern flag
- `VUE_CLI_MODERN_BUILD`: when true, the current config is the modern build, otherwise it is legacy

## HTML

### The Index File

The file `public/index.html` is a template that will be processed with `html-webpack-plugin`.
During build, asset links will be injected automatically. In addition, Vue CLI also automatically
injects resource hints (`preload/prefecch`), manifest/icon links, and the asset links for the JavaScript
and CSS files produced during the build

### Interpolation

Since the index file is used as a template, you can use the `lodash template` synax to interpolate
values in it:
- `<%= VALUE %>` for unescaped interpolation;
- `<%- VALUE %>` for HTML-escaped interpolation;
- `<% expression %>` for JavaScript control flows.

All client-side env variables are also directly avaiable. For example, to use the `BASE_URL` value:
```html
<link rel="icon" href="<%= BASE_URL %>favicon.ico">
```

### Preload

`<link rel="preload">` is a type of resource hint that is used to specify resources that your pages
will need very soon after loading, which you therefore want to start preloading early in the lifecycle
of a page laod, before the browser's main rendering machinery kicks in.

By default, a Vue CLI app will automatically generate preload hints for all files that are needed
for the initial rendering of your app.

### Prefetch

<`link rel="prefetch">` is a type of resource hint that tells the browser to prefetch content that
the user may visit in the near future in the browser's idle time, after the page finishes loading.

By default, a Vue CLI app will automatically generate prefetch hints for all JavaScript files
generated for async chunks (as a result of on-demand code)

### Building a Multi-Page App

Not every app has to be an SPA. Vue CLI supports building a multi-paged app using the `pages` option
in `vue.config.js`. The built app will efficiently share common chunks between multiple entries
for optimal loading performance.

## Static Assets

Static assets can be hanlded in two different ways:
- Imported in JavaScript or referenced in templates/CSS via relative paths. Such references will be
  handled by webpack
- Placed in the `public` directory and referenced via absolute paths. These assets will simply be
  copied and not go through webpack.

### Relative Path Imports

When you reference a static asset using relative path (must start with `.`) inside JavaScript,
CSS or `*.vue` files, the asset will be included in webpack's dependency graph. During this compilation
process, all asset URLs will be resolved as module dependencies.

For example:
```html
<img src="./image.png">
```
will be compiled into:
```javascript
h('img', { attrs: { src: require('./image.png') }})
```

### URL Transform Rules

- If the URL is an absolute path (e.g. `/images/foo.png`), it will be preserved as-is.
- If the URL starts with `.`, it's interpreted as a relative module request and resolved based
  on the folder structure on your file system
- If the URL starts with `~`, anything after its interpreted as a module request. This means you
  can reference assets inside node modules:
  ```html
  <img src="~some-npm-package/foo.png">
  ```
- If the URL starts with `@`, it's also interpreted as a module request. This is useful because Vue
  CLI by default aliases `@` to `<projectRoot>/src`.

### The `public` folder

Any static assets placed in the `public` folder will simply be copied and not go through webpack.
You need to reference them using absolute paths.

The `public` directory is provided as an __escape hatch__, and when you reference it via absolute path,
you need to take into account where your app will be deployed. If your app is not deployed at the root
of a domain, you will need to prefix your URLs with `publicPath`:
```html
<link rel="icon" href="<%= BASE_URL %>favicon.ico">
```

## CSS

Vue CLI projects come with support for `PostCSS`, `CSS Modules`, and pre-processors including
`Sass`, `Less`, and `Stylus`.

All compiled CSS are processed by `css-loader`, which parses `url()` and resolves them as module
requests.

### Pre-Processors

You can select pre-processors (Sass/Less/Stylus) when creating the project. Just manually install
the corresponding webpack loaders:
```bash
# Sass
npm install -D sass-loader sass

# Less
npm install -D less-loader less

# Stylus
npm install -D stylus-loader stylus
```

Then you can import the corresponding file types or use them in `*.vue` files:
```html
<style lang="scss">
$color: red;
</style>
```

### Passing Options to Pre-Processor Loaders

Sometimes you may want to pass options to the pre-processor's webpack loader. You can do that using the
`css.loaderOptions` option in `vue.config.js`. For example, to pass some shared global variables to all
your Sass/Less styles:
```js
// vue.config.js
module.exports = {
  css: {
    loaderOptions: {
      // pass options to sass-loader
      sass: {
        // @/ is an alias to src/
        // so this assumes you have a file named `src/variables.scss`
        data: `@import "~@/variables.scss";`
      },
      // pass Less.js Options to less-loader
      less:{
        // http://lesscss.org/usage/#less-options-strict-units `Global Variables`
        // `primary` is global variables fields name
        globalVars: {
          primary: '#fff'
        }
      }
    }
  }
}
```

Loaders which can be configured via the `loaderOptions` include:
- css-loader
- postcss-loader
- sass-loader
- less-loader
- stylus-loader

## Webpack

### Simple Configuration

The easiest way to tweak the webpack config is providing an object to the `configureWebpack` option
in `vue.config.js`:
```javascript
// vue.config.js
module.exports = {
  configureWebpack: {
    plugins: [
      new MyAwesomeWebpackPlugin()
    ]
  }
}
```

The object will be merged into the final webpack config using `webpack-merge`.

If you need conditional behavior based on environment, or want to directly mutate the config, use
a function (which will be lazy evaluated after the env variables are set). The function
receives the resolved config as the argument. Inside the function, you can either mutate the config
directly, OR return an object which will be merged:
```javascript
// vue.config.js
module.exports = {
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      // mutate for development...
    }
  }
}
```

### Chaining (Advanced)

The internal webpack config is maintained using `webpack-chain`. The library provides an abstraction
over the raw webpack config, with the ability to define named loader rules and named plugins, and later
"tap" into those rules and modify their options.

This allows us finer-grained control over the internal config.

To modify the options of a loader:
```javascript
// vue.config.js
module.exports = {
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
        .loader('vue-loader')
        .tap(options => {
          // modify the options...
          return options
        })
  }
}
```

To add a new loader:
```javascript
// vue.config.js
module.exports = {
  chainWebpack: config => {
    // GraphQL Loader
    config.module
      .rule('graphql')
      .test(/\.graphql$/)
      .use('graphql-tag/loader')
        .loader('graphql-tag/loader')
        .end()
  }
}
```

Modifying options of a plugin:
```javascript
// vue.config.js
module.exports = {
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        return [/* new args to pass to html-webpack-plugin's constructor */]
      })
  }
}
```

### Inspecting the Config

Since `@vue/cli-service` abstracts away the webpack config, it may be more difficult to understand
what is included in the config, especially when you are trying to make tweaks yourself.

`vue-cli-service` exposes the `inspect` command for inspecting the resolved webpack config.
```bash
vue inspect > output.js
```
Note that the output is not a valid webpack config file, it's a serialized format only meant for inspection.
Can also inspect a subset of the config by specifying a path:
```bash
# only inspect the first rule
vue inspect module.rules.0
```

## Modes

__Mode__ is an import concet in Vue CLI projects. By default, there are three modes:
1. `development` is used by the `vue-cli-service serve`
2. `test` is used by `vue-cli-service test:unit`
3. `production` is used by `vue-cli-service build` and `vue-cli-service test:e2e`

You can overwrite the default mode used for a command by passing the `--mode` option flag.
```bash
vue-cli-service build --mode development
```

When running `vue-cli-service`, environment variables are loaded from all corresponding files.
If they don't contain a `NODE_ENV` variable, it will be set to the mode.

## Environment Variables

You can specify env variables by placing the following files in your project root:
```bash
.env                # loaded in all cases
.env.local          # loaded in all cases, ignored by git
.env.[mode]         # only loaded in specified mode
.env.[mode].local   # only loaded in specified mode, ignored by git
```

An env file simply contains `key=value` pairs of environment variables:
```text
FOO=bar
VUE_APP_SECRET=secret
```

Loaded variables will become available to all `vue-cli-service` commands, plugins, and dependencies.
An env file for a specific mode (e.g. `.env.production`) will take higher priority than a generic one
(e.g. `.env`). In addition, environment variables that already exist when Vue CLI is executed have the
highest priority and will not be overwritten by `.env` files.

### Client-side code

Only variables that start with `VUE_APP_` will be statically embedded into the client bundle with
`webpack.DefinePlugin`. You can access them in your application code:
```javascript
console.log(process.env.VUE_APP_SECRET)
```

In addition to `VUE_APP_*` variables, there are also two special variables that will always be
available in your app code:
- `NODE_ENV`: this will be one of `"development"`, `"production"`, or `"test"` depending on the `mode`
  in which the app is running.
- `BASE_URL`: this corresponds to the `publicPath` option in `vue.config.js` and is the base path your
  app is deployed at.

### Local Only Variables

Sometimes you might have env variables that should not be committed into the codebase, especially if
your project is hosted in a public repository. In that case you should use an `.env.local` file instead.
Local env files are ignored in `.gitignore` by default.

## Build Targets

When you run `vue-cli-service build`, you can specify different build targets via the `--target` option.
This allows you to use the same code base to produce different builds for different use cases.

### App

App is the default build target. In this mode:
- `index.html` with asset and resource hints injection
- vendor libraries split into a separate chunk for better caching
- static assets under 4kb are inlined into JavaScript
- static assets in `public` are copied into output directory

### Library

You can build a single entry as a library using:
```
vue-cli-service build --target lib --name myLib [entry]
...
File                     Size                     Gzipped
dist/myLib.umd.min.js    13.28 kb                 8.42 kb
dist/myLib.umd.js        20.95 kb                 10.22 kb
dist/myLib.common.js     20.57 kb                 10.09 kb
dist/myLib.css           0.33 kb                  0.23 kb
```

The entry can be either a `.js` or a `.vue` file. If no entry is specified, `src/App.vue` will be
used.

A lib build outputs:
- `dist/myLib.common.js`: A CommonJS bundle for consuming via bundlers
- `dist/myLib.umd.js`: A UMD bundle for consuming directly in browsers or with AMD loaders
- `dist/myLib.umd.min.js`: Minified version of the UMD build
- `dist/myLib.css`: Extracted CSS file (can be forced into inlined by setting `css: {extract: false}`
  in `vue.config.js`)

### Web Component

You can build a single entry as a web component using:
```bash
vue-cli-service build --target wc --name my-element [entry]
```
Note that entry should be a `*.vue` file. Vue CLI will automatically wrap and register the component
as a Web Component.

The build will produce a single JavaScript file (and its minified version) with everything inlined.
Te script, when included on a page, registers the `<my-element>` custom element, which wraps the target
Vue component using `@vue/web-component-wrapper`. The wrapper automatically proxies properties, attributes,
events and slots.

Note the bundle relies on `Vue` being globally avaiable on the page.
```html
<script src="https://unpkg.com/vue"></script>
<script src="path/to/my-element.js"></script>

<!-- use in plain HTML, or in any other framework -->
<my-element></my-element>
```

### Async Web Component

When targeting multiple web components, the bundle may become quite large, and the user may only
use a few of the components your bundle registers. The async web component produces a code-split bundle
with a small entry file that provides the shared runtime between all the components, and registers
all the custom elements upfront. The actual implementation of a component is then fetched on-demand
only when an instance of the corresponding custom element is used on the page.

```bash
vue-cli-service build --target wc-async --name foo 'src/components/*.vue'
...
File                Size                        Gzipped
dist/foo.0.min.js    12.80 kb                    8.09 kb
dist/foo.min.js      7.45 kb                     3.17 kb
dist/foo.1.min.js    2.91 kb                     1.02 kb
dist/foo.js          22.51 kb                    6.67 kb
dist/foo.0.js        17.27 kb                    8.83 kb
dist/foo.1.js        5.24 kb                     1.64 kb
```

Now on the page, the user only needs to include Vue and the entry file:
```html
<script src="https://unpkg.com/vue"></script>
<script src="path/to/foo.min.js"></script>

<!-- foo-one's implementation chunk is auto fetched when it's used -->
<foo-one></foo-one>
```

## Deployment

If you are using Vue CLI along with a backend framework that handles static assets as part of its
own deployment, all you need to do is make sure Vue CLI generates the built files in the correct
location, and then follow the deployment instruction of your backend framework.

If you are developing your frontenv app separately from your backend- i.e. your backend exposes an
API for your frontend to talk to, then your frontend is essentially a purely static app. You can
deploy the built content in the `dist` directory to any static file server, but make sure to set
the correct `publicPath`.

### Previewing Locally

The `dist` directory is meant to be served by an HTTP server, so it will not work if you open
`dist/index.html` directly over `file://` protocol. The easiest way to preview your production build
locally is using a Node.js static file server, for example `serve`:
```bash
npm install -g serve
# -s flag means serve it in Single-Page Application mode
# which deals with the routing problem below
serve -s dist
```

## Configuration

### Global CLI Config

Some global configurations of `@vue/cli` are stored in a JSON file named `~/.vuerc`. You can
edit this file manually or use the `vue config` command to inspect and modify it.

### vue.config.js

`vue.config.js` is an optional config file that will be automatically loaded by `@vue/cli-service`.
This file should export an object containing options:
```javascript
// vue.config.js
module.exports = {
  // options...
}
```

Options:
- __publicPath__:
  Default: `/`. This is the base URL your application will be deployed at. By default, Vue CLI assumes
  your app will be deployed at the root of a domain, e.g `https://www.my-app.com/`. If your app is
  deployed at a sub-path, you will need to specify that sub-path using this option.
  The value is also respected during development. If you want your dev server to be served at root
  instead, you can use a conditional value:
  ```javascript
  module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/production-sub-path/'
      : '/'
  }
  ```
- __outputDir__:
  Default: `dist`. The directory where the production build files will be generated in when running
  `vue-cli-service build`. Note that the target directory will be removed before building (this can
  be disabled by passing `--no-clean` when building.)

- __assetsDir__:
  Default: `''`. A directory (relative to `outputDir`) to rest generated static assets
  (js, css, img, fonts) under.

- __indexPath__:
  Default: `index.html`. THe output path for the generated `index.html` relative to `outputDir`

- __filenameHashing__:
  Default: `true`. By default, generated static assets contain hashes in their filenames for better
  caching control.

- __pages__:
  Default: `undefined`. Build the app in multi-page mode. Each "page" should have a corresponding
  JavaScript entry file.

- __lintOnSave__:
  Default: `true`. Whether to perform lint-on-save during development using eslint-loader. This
  value is respected only when `@vue/cli-plugin-eslint` is installed

- __runtimeCompiler__:
  Default: `false`. Whether to use the build of Vue core that includes the runtime compiler. Setting
  it to `true` will allow you to use the `template` option in Vue components, but will incur around
  an extra 10kb payload for your app.

- __transpileDependencies__:
  Default: `[]`. By default `babel-loader` ignores all files inside `node_modules`. If you want to
  explicity transpile a dependency with Babel, add it to the list.

- __productionSourceMap__:
  Default: `true`. If `true`, build source maps for production.

- __integrity__:
  Default: `false`. Set to `true` to enable Subresource integrity (SRI)

- __configureWebpack__:
  Type: `Object | Function`. If the value is an object, it will be merged into the final config
  using webpack-merge. If the value is a function, it will receive the resolved config as the
  argument. The function can either mutate the config and return nothing, OR return a cloned or merged
  version of the config.

- __chainWebpack__:
  Type: `Function`. A function that will receive an instance of `ChainableConfig` powered by
  webpack-chain. Allows for more fine-grained modification of the internal webpack config.

- __css.modules__:
  Default: `false`. Setting to `true` will allows you to drop `.module` in the filenames and treat
  all `*.(css|scss|sass|less|stul(us)?)` files as CSS modules.

- __css.extract__:
  Default: `true` in production, `false` in development. Whether to extract CSS into standalone
  CSS files instead of inlined.

- __css.sourceMap__:
  Default: `false`. Whether to enable source maps for CSS. Setting this to `true` may affect build
  performance.

- __css.loaderOptions__:
  Default: `{}`. Pass options to CSS-related loaders.

- __devServer__:
  Type:`Object`. All options for `webpack-dev-server` are supported.

- __devServer.proxy__:
  Type: `string | Object`. If your frontend app and the backend API server are not running on the
  same host, you will need to proxy API requests to the API server during development.

- __parallel__:
  Default: `require('os').cpus().length > 1`. Whether to use `thread-loader` for Babel or TypeScript
  transpulation

- __pwa__:
  Type: `Object`. Pass options to the PWA plugin.

- __pluginOptions__:
  Type: `Object`. This is an object that doesn't go through any schema validation, so it can be used
  to pass arbitrary options to 3rd party plugins

