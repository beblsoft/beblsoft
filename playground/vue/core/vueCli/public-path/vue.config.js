/**
 * @file Vue CLI Configuration
 */

module.exports = {
  publicPath: '/app/',
  outputDir: 'dist/app',
  lintOnSave: true,
  chainWebpack: (config) => {
    config.stats({ warnings: false });
    config.resolve.symlinks(false);
    config.devtool('source-map');
  },
  productionSourceMap: true
};
