# Vue Cli Samples Documentation

Projects demonstrating use of Vue CLI to build an application. Projects require the following
global installation:
- Install cli: `sudo npm install -g @vue/cli`
- Install cli-service: `sudo npm install -g @vue/cli-service-global`

## instantPrototype

Directory contains simple vue file to use for instant prototyping.

To run:
- Serve in browser: `vue serve --open instantPrototype.vue`
- Build for production: `vue build --target app --dest dist instantPrototype.vue`

## hello-world

Hello World application built with `vue-cli create hello-world`

To run:
- Serve: `npm run serve`. Open chrome to http://localhost:8080/
- Build: `npm run build`. See results in `dist/`

## public-path

Vue CLI project that changes the public path to serve under /app

To run:
- Serve: `npm run serve`. Open chrome to http://localhost:8080/app
- Build: `npm run build`. See results in `dist/`