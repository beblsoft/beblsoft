---
home: true
heroImage: /assets/img/logo.png
actionText: Dive into Topics
actionLink: /topicA/
features:
- title: Home Page
  details: This site has an amazing home page that is SEO optimized.
- title: Topic A
  details: Extensive Research has been done on topic A.
- title: Topic B
  details: Even more extensive research has been done on topic B.
footer: Beblsoft LLC | Copyright © 2019-present
---

# Usage

```bash
# Start server
npm run dev

# Build for production
npm run build
```

<my-header></my-header>
