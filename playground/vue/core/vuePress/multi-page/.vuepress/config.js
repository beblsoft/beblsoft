/**
 * @file Vue Press Configuration
 */

module.exports = {
  title: 'MultiPage App',
  description: 'Beblsoft\'s Example VuePress Multi Page Application',
  head: [
    ['link', { rel: 'icon', href: '/assets/img/logo.png' }]
  ],
  port: 8081,
  base: '',
  dest: 'dist/',
  themeConfig: {
    logo: '/assets/img/logo.png',
    lastUpdated: 'Last Updated', // string | boolean
    displayAllHeaders: true, // Default: false
    nav: [
      // { text: 'Home', link: '/' },
      // { text: 'Topic A', link: '/topicA/' },
      // { text: 'Topic B', link: '/topicB/' },
    ],

    // Auto Sidebar: Put links for the current page on the sidebar
    // sidebar: 'auto',

    // Single Sidebar
    // Same sidebar always present regardless of what page currently visiting
    // sidebar: [
    //   '/',
    //   ['/topicA/', 'Topic A'],
    //   ['/topicB/', 'Topic B'],
    // ]

    // Sidebar Groups
    // Single Sidebar always present, but contains groups
    sidebar: [{
        title: 'Topic A',
        collapsable: true,
        children: [
          '/topicA/',
          '/topicA/point1',
          '/topicA/point2',
          '/topicA/point3',
        ]
      },
      {
        title: 'Topic B',
        collapsable: true,
        children: [
          '/topicB/',
          '/topicB/point1',
          '/topicB/point2',
          '/topicB/point3',
        ]
      }
    ]

    // Multiple SideBars based on base path
    // Each has a base path that and becomes active when its base is part of the URL
    // sidebar: {
    //   '/topicA/': [
    //     '',
    //     'point1',
    //     'point2',
    //     'point3',
    //   ],
    //   '/topicB/': [
    //     '',
    //     'point1',
    //     'point2',
    //     'point3',
    //   ],
    //   // fallback, should be last
    //   '/': [
    //     '',
    //   ]
    // }
  },
  markdown: {
    lineNumbers: false
  }
}
