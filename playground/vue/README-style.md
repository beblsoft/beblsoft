# Vue Style Guide Documentation

## Essential: Rules to Help Prevent Errors

- Multi-word component names: Ex1. todo-item. Ex2. TodoItem
- Component data: Must be a function, or it will be shared by all instances
  ```javascript
  Vue.component('some-comp', {
    data: function () {
      return {
        foo: 'bar'
      }
    }
  })
  ```

- Detailed prop definitions
  ```javascript
   props: {
    status: String
  }
  ```
- Keyed v-for
  ```html
  <ul>
    <li v-for="todo in todos":key="todo.id">
      {{ todo.text }}
    </li>
  </ul>
  ```
- Avoid v-if with v-for
- All components should have scoped styles
  ```html
  <template>
    <button class="button button-close">X</button>
  </template>
  <style scoped>
  .button {
    border: none;
    border-radius: 2px;
  }
  </style>
  ```
- Private property names: Always use the `$_` prefix for custom private properties
  in a plugin, mixin, etc. Then to avoid conflicts with code by other authors, also include a named scope
  (e.g. $_yourPluginName_)
  ```javascript
  var myGreatMixin = {
    // ...
    methods: {
      $_myGreatMixin_update: function () {
        // ...
      }
    }
  }
  ```

## Strongly Recommended: Rules for Readability

- Component Files: 1 file per component
- Component File Names (1): Pascal Case (Ex. `MyComponent.vue`) or kebab-case  (Ex. `my-component.vue`)
- Component File Names (2): Use full words, no abbreviations. Ex. `StudentDashboardSettings.vue`
- Component File names (3): Start with highest-level (most general) words and end with descriptive
  modifying words Ex. `SearchButtonClear.vue`, `SearchButtonRun.vue`
- Component File names (Base): Begin with "Base", "App", or "V". Ex. `BaseButton.vue`, `AppTable.Vue`, `VIcon.vue`
- Component File Names (Single instance): Begin with "The" prefix Ex. `TheHeading.vue`
- Component File names (Tightly coupled): Child components that are tightly coupled with their parent should
  include parent component name as a prefix. Ex. `SearchSidebar.vue`, `SearchSidebarNavigation.vue`
- Component names: Define with pascal case
  ```javascript
  Vue.component('MyComponentName', { /* ... */ })
  ```
  Reference in HTML
  ```html
  <!-- In single-file components and string templates -->
  <MyComponent/>
  <!-- In DOM templates -->
  <my-component></my-component>
  ```
- Self closing components
  ```html
  <my-component></my-component>
  ```
- Prop Names: camelCase during declaration, kebab-case in templates
  ```javascript
  props: {
    greetingText: String
  }
  ```
  ```html
  <WelcomeMessage greeting-text="hi"/>
  ```
- Multi-attribute elements: One attribute per line
  ```html
   <MyComponent
     foo="a"
     bar="b"
     baz="c"
   />
  ```
- Simple expressions in templates: Component templates should only include simple
  expressions, with more complex expressions refactored into computed properties or methods.
  ```javascript
  {{ normalizedFullName }}
  // The complex expression has been moved to a computed property
  computed: {
    normalizedFullName: function () {
      return this.fullName.split(' ').map(function (word) {
        return word[0].toUpperCase() + word.slice(1)
      }).join(' ')
    }
  }
  ```
- Simple computed properties: Split complex properties into many simple properties
  ```javascript
  computed: {
    basePrice: function () {
      return this.manufactureCost / (1 - this.profitMargin)
    },
    discount: function () {
      return this.basePrice * (this.discountPercent || 0)
    },
    finalPrice: function () {
      return this.basePrice - this.discount
    }
  }
  ```
- Quoted attribute values: Non-empty HTML attribute values should be inside quotes
  ```html
  <input type="text">
  ```
- Avoid directive shorthands: Ex. don't use `@` instead of `v-bind`
  ```html
  <input
    v-on:input="onInput"
    v-on:focus="onFocus"
  >
  ```

## Recommended: Arbitrary for Consistency

- Component/instance option order: Categories, options:
    1.  Side Effects (triggers effects outside the component) `el`
    2.  Global Awareness (requires knowledge beyond the component) `name` `parent`
    3.  Component Type (changes the type of the component) `functional`
    4.  Template Modifiers (changes the way templates are compiled) `delimiters` `comments`
    5.  Template Dependencies (assets used in the template) `components` `directives` `filters`
    6.  Composition (merges properties into the options) `extends` `mixins`
    7.  Interface (the interface to the component) `inheritAttrs` `model` `props/propsData`
    8.  Local State (local reactive properties) `data` `computed`
    9.  Events (callbacks triggered by reactive events) `watch`. Lifecycle Events (in the order they are called)
    10. Non-Reactive Properties (instance properties independent of the reactivity system) `methods`
    11. Rendering (the declarative description of the component output) `template/render` `renderError`
- Component file order: Single-file components should order tags as follows
  ```html
  <template>...</template>
  <script>/* ... */</script>
  <style>/* ... */</style>
  ```

## Use with Caution: Highlight Risky Features

- v-if/v-else without key: Use key with v-if/v-else if they are the same element type
  ```html
  <div
    v-if="error"
    key="search-status"
  >
    Error: {{ error }}
  </div>
  <div
    v-else
    key="search-results"
  >
    {{ results }}
  </div>
  ```
- Avoid element selectors with scoped: Use class selectors instead
  ```html
  <template>
    <button class="btn btn-close">X</button>
  </template>
  <style scoped>
  .btn-close {
    background-color: red;
  }
  </style>
  ```
- Implicit parent-child comm: Use props and events for parent/child communication instead of using this.$parent
- Non-flux state management: Vuex should be preffered for global state management, instead of this.$root
  or a global event bus
