# Vue Auth Demo with Firebase

Demonstrates Conditional Routing based on login, not focused on asthetics. Uses Google Firebase as
backend authenticator, all isolated down to one file, `ServiceInterface.js`. Lots of useful (albeith minimal)
components: `CheckEmail.vue`, `ConfirmAccount.vue`, `Dashboard.vue`, `Error404.vue`, `ForgotPassword.vue`, `Header.vue`,
`Home.vue`, `ResetPassword.vue`, `SignIn.vue`, `SignUp.vue`, `SomethingWentWrong.vue`

Relevant URLs:
[Tutorial](https://medium.freecodecamp.org/managing-user-state-with-vuex-firebase-77eebc64f546),
[GitHub](https://github.com/garethredfern/vue-auth-demo)

## Basic usage

```bash
# Install
npm install

## serve with hot reload at localhost:8080
npm run dev
```


