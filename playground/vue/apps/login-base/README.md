# Vue Auth Demo with Firebase

Has a fake Backend for now, just designed to facilate login/logout flow.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work [read the tutorial](https://medium.freecodecamp.org/managing-user-state-with-vuex-firebase-77eebc64f546).
