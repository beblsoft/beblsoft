import Spinners from './components/Spinners'
import SimpleSpinners from './components/SimpleSpinners'

const routes = [{
  path: '/',
  name: 'Spinners',
  component: Spinners
},
{
  path: '/spinners',
  name: 'Spinners',
  component: Spinners
},
{
  path: '/SimpleSpinners',
  name: 'SimpleSpinners',
  component: SimpleSpinners
}]

export default routes
