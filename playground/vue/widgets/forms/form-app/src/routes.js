import Home from './components/Home'
import Coins from './components/Coins'
import SupportedControls from './components/SupportedControls'
import LoginForm from './components/LoginForm'
const routes = [{
  path: '/',
  name: 'Home',
  component: Home
}, {
  path: '/coins/:id',
  name: 'Coins',
  component: Coins
}, {
  path: '/supportedControls',
  name: 'SupportedControls',
  component: SupportedControls
}, {
  path: '/loginForm',
  name: 'LoginForm',
  component: LoginForm
}]

export default routes
