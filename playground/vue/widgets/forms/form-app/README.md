# Vue Form App Documentation

## Basic Commandline for Sample App

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## Assets

Social Media Icons: ​https://www.iconfinder.com/iconsets/social-media-2169​

Font from: https://fonts2u.com/alishader-demo.font

## File tree

```text
├── .babelrc - bable setup
├── build - standard build files produced by vue cli, note changes in webpack.dev.conf.js to force auto reload
│   ├── build.js
│   ├── check-versions.js
│   ├── dev-client.js
│   ├── dev-server.js
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   ├── webpack.prod.conf.js
│   └── webpack.test.conf.js
├── config - standard configuration files produced by vue cli
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── test.env.js
├── .editorconfig - editor customization
├── .eslintignore - files to not do ES lint on
├── .eslintrc.js - where custom ES lint configuration is
├── .gitignore - git overrides
├── index.html - start of program, pull in dependencies like bootstrap
├── package.json - contains all project dependencies, scripts, metadata about application
├── package-lock.json - automatically generated exact tree of dependencies generated.
├── .postcssrc.js - a file defining module.exports that is ignored, used to add JS logic to generate your config.
├── README.md - this file, documentation for application
├── src
│   ├── App.vue - icon and navigation to route to, loads Hello page (Home page)
│   ├── components
│   │   ├── Coins.vue - demo view showing AJAX with parameter
│   │   ├── Component404.vue - integration of 404 Component, you are lost
│   │   ├── Home.vue - Base view of application, starting point once App.vue executes, '/'
│   │   ├── Login.vue - integration of login page, needs work
│   │   └── MojourneyTest.vue - test view
│   ├── main.js - instantiates Vue from index, which loads App Component and instantiates the routes
│   ├── routes - the routes for the application
│   └── services
│       ├── ServerInterface.js - actual AJAX goes out here, base service
│       ├── TestInterface.js - encapsulates all interfaces for Test, uses ServerInterface
│       └── UserInterface.js - encapsulates all interfaces for User, uses ServerInterface
├── static
│   ├── css
│   │   └── bootstrap.min.css - css for bootstrap
│   └── img
│       ├── favicon.ico - icon used in tab when invoked (referenced in index.html)
│       └── logo.png - large logo used in sample (referenced in App.vue, Login.vue, Component404.vue)
└── test - vue auto generated test files
    ├── e2e
    │   ├── custom-assertions
    │   │   └── elementCount.js
    │   ├── nightwatch.conf.js
    │   ├── runner.js
    │   └── specs
    │       └── test.js
    └── unit
        ├── index.js
        ├── karma.conf.js
        └── specs
            └── Hello.spec.js
```

