import Vue from 'vue';
import Router from 'vue-router';
import HomeComponent from './components/base/Home.vue';
import GenericComponent from './components/base/GenericComponent.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'rnHome',
      component: HomeComponent,
      alias: '/home'
    },
    {
      path: '/addProfile',
      name: 'rnAddProfile',
      component: GenericComponent,
    },

    // Facebook --------------------------------
    {
      path: '/facebook/overview',
      name: 'rnFacebookOverview',
      component: GenericComponent,
    },
    {
      path: '/facebook/posts/all',
      name: 'rnFacebookPostsAll',
      component: GenericComponent,
    },
    {
      path: '/facebook/posts/frequency',
      name: 'rnFacebookPostsFrequency',
      component: GenericComponent,
    },
    {
      path: '/facebook/posts/likes',
      name: 'rnFacebookPostsLikes',
      component: GenericComponent,
    },
    {
      path: '/facebook/posts/comments',
      name: 'rnFacebookPostsComments',
      component: GenericComponent,
    },
    {
      path: '/facebook/posts/sentiment',
      name: 'rnFacebookPostsSentiment',
      component: GenericComponent,
    },
    {
      path: '/facebook/manage',
      name: 'rnFacebookManage',
      component: GenericComponent,
    },
    // Twitter --------------------------------
    {
      path: '/twitter/overview',
      name: 'rnTwitterOverview',
      component: GenericComponent,
    },
    {
      path: '/twitter/tweets/all',
      name: 'rnTwitterTweetsAll',
      component: GenericComponent,
    },
    {
      path: '/twitter/tweets/frequency',
      name: 'rnTwitterTweetsFrequency',
      component: GenericComponent,
    },
    {
      path: '/twitter/tweets/likes',
      name: 'rnTwitterTweetsLikes',
      component: GenericComponent,
    },
    {
      path: '/twitter/tweets/comments',
      name: 'rnTwitterTweetsComments',
      component: GenericComponent,
    },
    {
      path: '/twitter/tweets/sentiment',
      name: 'rnTwitterTweetsSentiment',
      component: GenericComponent,
    },
    {
      path: '/twitter/manage',
      name: 'rnTwitterManage',
      component: GenericComponent,
    },
  ]
});
