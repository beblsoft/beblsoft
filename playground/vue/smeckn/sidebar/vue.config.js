/**
 * @file Vue CLI Configuration
 */

module.exports = {
  lintOnSave: true,
  css: {
    loaderOptions: {
      sass: {
        // @/ is an alias to src/
        data: '@import "~@/index.scss";'
      },
    }
  },
  chainWebpack: (config) => {
    config.stats({ warnings: false });
    config.resolve.symlinks(false);
    config.devtool('source-map');
  },
  productionSourceMap: true
};

