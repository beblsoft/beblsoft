module.exports = {
  title: 'Welcome to Social Media Checking',
  description: "A social media analysis application",
  head: [
    ['link', { rel: 'icon', href: '/assets/img/info-logo-title.png' }]
  ],
  port: 8081,
  base: "/documentation/",
  themeConfig: {
    logo: '/assets/img/info-logo-title.png',
    lastUpdated: 'Last Updated', // string | boolean
    nav: [
       { text: 'Overview', link: '/overview/WhatIsSmeckn' },
       { text: 'Company', link: '/company/About' },
       { text: 'Blog', link: '/blog/SocialMedia' }
    ],
   // Sidebar
    sidebar: {
      '/company/': [
        'About',
        'Press',
        'Contact',
        'Jobs',
        'Legal'
      ],
      '/overview/': [
        'WhatIsSmeckn',
        'GettingStarted',
        'AIAnalysis',
        'Facebook',
        'AccountManagement',
        'Feedback',
        'Faq'
      ],
      '/blog/': [
        'SocialMedia',
        'UseSMToLiftYouUp',
        'BuildYourBrand',
        'HelpOthers',
        'YourDigitalFootprint'
      ],
      // fallback, should be last
      '/': []
    }
  }
}
