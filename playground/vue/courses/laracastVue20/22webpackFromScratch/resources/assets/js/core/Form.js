import Errors from './Errors';

class Form {
	constructor(data) {
		this.originalData = data;

		for(let field in data) {
			this[field] = data[field];
		}

		this.errors = new Errors();
	}
	reset() {
		for(let field in originalData) {
			this[field] = '';
		}

		this.errors.clear();

	}

	data() {

		// introduce slightly cleaner way to get data, no need to delete at end
		let data = {};

		for (let property in this.originalData) {
			data[property] = this[property];
		}

		//let data = Object.assign({}, this);

		//delete data.originalData;
		//delete data.errors;

		return data;

	}

	post(url) {
		return this.submit('POST', url);
	}

	delete(url) {
		return this.submit('DELETE', url);
	}

	submit(requestType, url) {  // form.submit('POST', '/some-endpoint')
		alert('submtting form');

		return new Promise((resolve, reject) => {

          /* ensure bind to current instance of this w/ bind call */
		  axios[requestType](url, this.data())
				.then(response => {
					this.onSuccess(response.data);

					/* notify requestor */
					resolve(response.data);
				})

				.catch(error => {
					this.onFail(error.response.data);

					/* notfiy requestor */
					reject(error.response.data);
				})

		});
	}

	onSuccess(data) {
		alert(data.message);
		this.reset();

	}

	onFail(errors) {
		this.errors.record(errors);
	}
}

export default Form;


