

// these are installed via npm
import Vue from 'vue';
import axios from 'axios';

import Form from './core/Form';

import Example from './components/Example';

// make these two globally available, so no need to import Form within every file
window.axios = axios;
window.Form = Form;

new Vue({
	el: "#app",

	components: {
		Example // <example></example>
	}
	data() {
		form: new Form({
			name: '',
			description: ''
		})

	},
	methods: {
		onSubmit() {
			this.form.delete('/projects');
		}
	}
});