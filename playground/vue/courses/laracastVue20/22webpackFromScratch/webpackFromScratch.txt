We want to separate the various pieces of our last project into separate files for reuse.  
For example, we will use the Form and Errors class for all forms so it should be separate.
Given browsers do not support Modules, we need to use a bundler to put them together.  
Introducing WebPack!!!

Rather than explicitly calling out vue and axios in html with <script src=... </script
statements, install them via npm so they get bundled in with webpack


# npm install vue --save-dev
# npm install axios --save-dev


To bundle things up, call webpack, hide-modules supresses all the details, watch watches
for changes.  Note that you can move this to  the package.json scripts are so that you
only have to type webpack to run this command, e.g.,

"scripts : {
	"webpack" : "webpack --hide-modules",
	"dev" : "webpack --hide-modules --watch"
}
	

# node_modules/bin/webpack --hide-modules --watch


By default npm pulls in the runtime-only build.  to use standalone build which
we have used throughout our projects, add the vue$ alias to your webpack config.
(see vue.js guide, Installation/NPM/Standalone vs. Runtime-only Build). You will
get warnings as to size of things, ignore


Add production to webpack.config.js which does minification (see file at end).
At prompt, type:

# export NODE_ENV=production && node_modles/.bin/webpack

Note that you will need to add Babel refreference in module to get code to compile
down to something all browsers will understand, down from latest js standard (es2015 code)
Need to install complementatary babel loader with npm.
# npm install babel-loader --save-dev
# npm install babel-core --save-dev
# npm install babel-preset-es2015 --save-dev

Consider separating app bundle from vendor bundle to faciliate quicker development, no 
need to reload everything for small change in app.
