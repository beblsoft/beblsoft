let webpack = require('webpack');
let path = require('path');

module.exports = {

	 // we divide up the output in to app specific and vendor specific chunks
	entry: { 
		app:  './resources/assets/js/app.js',
		vendor: ['vue', 'axios']
	},

   
	output : {
			path: path.resolve(__dirname, 'public/js');
			filename: '[name].js',
			publicPath: './public'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			}
		]
	}

	resolve: {
		alias: {
			'vue$' : 'vue/dist/vue.common.js'
		}
	}

	// this optimizes loads by not reloading vendor (above) resources during development
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			names: ['vendor']

		})
	]
};

// apply minification if production
if(process.env.NODE_ENV === 'production') {

	module.exports.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			sourcemap: true,
			compress: {
				warnings: false
			}

		});

	module.exports.plugins.push(
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: 'production'
			}
		});
}