Vue.component('component2', {

    template: `<div>
    	<h1>{{ message }}</h1>
    </div`,


    data() {
    	return {
    		message: 'Component2'
    	}
    }
});


new Vue({
    el: '#root2'

});
