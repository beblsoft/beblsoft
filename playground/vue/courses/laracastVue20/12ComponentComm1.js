Vue.component('coupon', {

    template: `
        /* blur means the element loses focus */
        <input placeholder="Enter your coupon code" @blur="onCouponApplied"> 
    `,

    methods: {
        onCouponApplied() {
            /*alert("Coupon was applied in component");*/
            /* can optionally pass an argument to $emit */
            this.$emit('applied');
            }
    }
}); 



new Vue({
    el: '#root',

    data: {
        couponApplied: false
    },

    methods: {
        onCouponAppliedParent() {
            alert("Parent: Coupon was applied");

            /* by setting this boolean, the .html displays 'You used a coupon' */
            this.couponApplied = true;
            }
    }
});