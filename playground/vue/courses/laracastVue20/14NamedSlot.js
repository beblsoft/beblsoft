
Vue.component('modal', {

    template: `
       <div class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">
                    <!-- use named slot for header -->
                    <slot name="header"></slot>
                </p>
                <button class="delete"></button>
            </header>
  
        <section class="modal-card-body">
            <!-- use default slot for body -->
            <slot>
                Default Content (Not really appropriate for body)
            </slot>
        </section>
    
        <footer class="modal-card-foot">
             <!-- define footer named slot, have default content Okay if not defined -->
             <slot name="footer">
                <a class="button is-success">Okay</a>
             </slot>
        </footer>
    </div>> 
    `
}); 

new Vue({
    el: '#root'

});