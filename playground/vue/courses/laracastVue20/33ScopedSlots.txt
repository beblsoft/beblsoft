- Motivation: Want to give complete control over markup if you need it
- As creator, can make things more flexible by using slots, gives access to all
properties of slot
- First code shows how we are overriding with inline markup and accessing item.
props is just a variable.


welcome.blade.php

...

<body>
	<div id="app>"

		<menu-list :items="["one, "two", "three"}></menu-list>
			<template scope="props"
				<h2 v-text="props.item"></h2>
			</template>
		</menu-list>
	</div>
</body>







.... Example.vue

<template>
	<div>
		<ul>
			<li v-for "item in items">
				<slot :item="item"> {{ item}} </slot>
			</li.
		</ul>
	</div>
</template>

<script>
	export default {
		props:['items']

	}
</script>


