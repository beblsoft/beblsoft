 // need to reference standalone build file to resolve warning that t
 // template compiler is not available, DO NOT DO THIS in production
 // as could end up w/ two copies of vue, will generate a warning when
 // run ava
 import Vue from 'vue/dist/vue.js';
 import test from 'ava';
 import Notification from '../src//Notification';
 test('that it renders a notification', t => {
     t.is(Notification.data().message, 'Hello World');
 });


let vm;

// This executes before every test
test.beforeEach(t => {
	let N = Vue.extend(Notification);
     
     vm = new N({ propsData: {
     	message: 'Foobar'
     }}).$mount();

})


 test('that text Content is Foobar', t => {
 	 

     t.is(vm.$el.textContent, 'FOOBAR');

 });

