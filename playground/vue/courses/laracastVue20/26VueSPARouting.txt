1. Uses laravel as a backend.
2. Reduce package.json files to axios and vue as devDependencies
3. Install dependencies with npm install
4. Added laravel mix (does not require laravel)
5. In bootstrap.js, pull in vue and axios  
   import Vue from 'vue'; 
   import axios from 'axios'; 
   window.Vue = Vue;
   window.axios = axios;
6. import bootstrap in app.js.
7. Compile down w/ webpack, node_modules/.bin/webpack  --progress  --watch (to force recompile)
8. Install router: npm install vue-router --save-dev
9. Define a new file, routes.js
10.  Import router in app.js
11. In bootstrap.js - add VueRouter.
12.  <router-link to="/">Home</router-lnk>
13. Use axios to get remote data.

