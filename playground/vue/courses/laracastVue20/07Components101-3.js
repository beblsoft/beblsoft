Vue.component('component3', {

    template: `<div> 
      <h1>{{ message }} : {{ message2 }} </h1>
      <li v-for="name in names" v-text="name"></li> 
      </div>`,


    data() {
    	return {
    		message: 'Component3',
    		message2: 'List of Stars',
    		names: ['joe', 'darryl', 'james']
    	}
    }
});


new Vue({
    el: '#root3'

});
