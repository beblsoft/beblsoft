/* Shared event instance facilates any component listening on events or sending */
/* add a wrapper class to centralize and rename event processing, can reuse and store in
   any file */
/*window.Event = new Vue(); */
window.Event = new class {
    constructor() {
        this.vue = new Vue();
    }

    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    listen(event, callback) {
        this.vue.$on(event, callback);
    }
}

Vue.component('coupon', {

    template: `
        /* blur means the element loses focus */
        <input placeholder="Enter your coupon code" @blur="onCouponApplied"> 
    `,

    methods: {
        onCouponApplied() {
            
            /* $emit sends an event, notice Event above */
            Event.fire('applied');
        }

    }
}); 

new Vue({
    el: '#root',

    data: {
        couponApplied: false
    },

    created() {
        /* $on listens on an event */
        Event.listen('applied', () => alert('Handling it!'));
    },

    methods: {
        onCouponAppliedParent() {
          
            /* by setting this boolean, the .html displays 'You used a coupon' */
            this.couponApplied = true;
            }
    }
});