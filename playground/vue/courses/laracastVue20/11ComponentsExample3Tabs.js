Vue.component('tabs', {

    template: `
    <div>

         <div>
             <div class="tabs">
                <ul>
                    <li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }">
                        <a :href="tab.href" @click="selectTab(tab)">{{ tab.name }}</a>
                    </li>
              </ul>
            </div>
        </div>

        <div class="tabs-details"
            <slot></slot>
        </div>
    </div>

    `,

     data() {
        return {tabs: [] }
    },


    created() {
        this.tabs = this.$children;
    },

    mounted() {
        console.log(this.$children);
    },

    methods: {
        selectTab(selectedTab) {
            this.tabs.forEach(tab => {
            tab.isActive = (tab.name == selectedTab.name);
            });
        }
    }

}); 

Vue.component('tab', {
    template: `
        <div v-show="isActive"><slot></slot></div>
    `,

    props: {
        name: { required: true},
        selected: { default: false}
    },
   
    data() {
        return { 
            isActive: false
        }      
    },

    /* called when component is mounted, init isActive */
    mounted() {
        this.isActive = this.selected;
    },

    computed: {
        href() {
            return '#' + this.name.toLowerCase().replace(/ /g, '-');
        }
    }
});

new Vue({
    el: '#root',

    data: {
        showModal: false
    }
});