# Markdown Technology Documentation

## Overview

Markdown is a lightweight markup language with plain text formatting syntax. Its design allows it
to be converted to many output formats, but the original tool by the same name only supports.
Often the format used for README files, writing messages in online discussion forums, and to
create rich text using a plain text editor

John Gruber created Markdown language in 2004. He wrote Markdown.pl for converting Markdown to HTML.
Key design goal is readability - that the language is reabable as-is, without looking like
ot has been marked up with tags or formatting instructions

Markdown has various variants: GitHub, Bitbucket, Reddit, Diaspora, Stack Exchange,
OpenStreetMap, and SourceForge use variants of Markdown to facilitate discussion between users

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Markdown),
[Common Mark](https://commonmark.org/),
[Common Mark Spec](https://spec.commonmark.org/0.29/)

## Sublime Integration

Sublime can be configured with various packages to work with markdown.
See `playground/sublime/README.txt` for more information.

Relevant URLs:

- Markdown Editing Package:
  [Package Control](https://packagecontrol.io/packages/MarkdownEditing),
  [GitHub](https://github.com/SublimeText-Markdown/MarkdownEditing)
- Markdown Preview Package:
  [Package Control](https://packagecontrol.io/packages/MarkdownPreview),
  [Documentation](https://facelessuser.github.io/MarkdownPreview/)
- Markdown Table Formatter Package:
  [Package Control](https://packagecontrol.io/packages/Markdown%20Table%20Formatter)

## GitHub Flavored Markdown (GFM)

GFM is a dialect of markdown that is currently supported for user content on GitHub.com and GitHub Enterprise.
It is a superset on CommonMark Spec.

Relevant URLs:
[Markdown Spec](https://github.github.com/gfm/),
[Mastering Markdown](https://guides.github.com/features/mastering-markdown/),
[Emoji Cheatsheet](https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md),
[Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Gitlab Flavored Markdown

Gitlab flavored Markdown is a ialect of markdown that is supported for comments, issues, merge reqs,
milestones, snippets wiki pages, epics, and repository markdown documents on GitLab.com. It is a
superset of CommonMark Spec.

Relevant URLs:
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)

## Linting

Markdown lint tool is a tool to check markdown files and flag style issues. It is written in ruby
and distributed as a rubygem

Relevant URLs:
[Github](https://github.com/markdownlint/markdownlint),
[Rules](https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md),
[Configuration](https://github.com/markdownlint/markdownlint/blob/master/docs/configuration.md)

Usage:

```bash
# Install
gem install mdl

# Basic usage
mdl README.md

# Usage with config:
mdl --config ${TOOLS}/markdownlint/mdlrc.rb README.md
```

## Markdown Viewer Chrome Extension

The chrome markdown extension can be installed with the following steps:
- [Open the chrome web store](https://chrome.google.com/webstore/category/extensions)
- Search for "Markdown Viewer"
- Install Markdown Viewer

Configure Markdown Viewer by clicking "m" in Chrome upper right.

- __Content__: autoreload, emoji, mathjax, scroll, toc (Table of contents)
- __Theme__: Github-Dark
- __Compiler__: gfm
- __Advanced Options__: Allow All Local Files

Usage:
- Open a markdown file (either remote or local), and chrome will render it as markdown
- Save the file and Chrome will reload it