# GitLab Markdown Demo

## Gitlab Markdown Extensions

### Newlines

Roses are red [followed by two or more spaces]
Violets are blue

Sugar is sweet

### Multiple underscores in a word

perform_complicated_task

do_this_and_do_that_and_another_thing

### URL Auto Linking

- https://www.google.com
- https://google.com/
- ftp://ftp.us.debian.org/debian/
- smb://foo/bar/baz
- irc://irc.freenode.net/gitlab
- http://localhost:3000

### Multiline Blockquote

>>>
If you paste a message from somewhere else

that

spans

multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>

### Syntax Highlighting

Inline `code` has `back-ticks around` it.

Javascript:
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

Python:
```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

Ruby:
```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

No Language Specified:
```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```

### Inline Diff

- {+ additions +}
- [+ additions +]
- {- deletions -}
- [- deletions -]

### Emoji

Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:.
Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches.
And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you are new to this, don't be :fearful:. You can easily join the emoji :family:.
All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:

Most emoji are natively supported on macOS, Windows, iOS,
Android and will fallback to image-based emoji where there is lack of support.

On Linux, you can download [Noto Color Emoji](https://www.google.com/get/noto/help/emoji/) to
get full native emoji support.

Ubuntu 18.04 (like many modern Linux distros) has this font installed by default.

### Special GitLab References

| Input                    | References                      |
|:-------------------------|:--------------------------------|
| @user_name               | specific user                   |
| @group_name              | specific group                  |
| @all                     | entire team                     |
| namespace/project>       | project                         |
| #12345                   | issue                           |
| !123                     | merge request                   |
| $123                     | snippet                         |
| &123                     | epic                            |
| ~123                     | label by ID                     |
| ~bug                     | one-word label by name          |
| ~"feature request"       | multi-word label by name        |
| %123                     | project milestone by ID         |
| %v1.23                   | one-word milestone by name      |
| %"release candidate"     | multi-word milestone by name    |
| 9ba12248                 | specific commit                 |
| 9ba12248...b19a04f5      | commit range comparison         |
| [README](doc/README)     | repository file references      |
| [README](doc/README#L13) | repository file line references |

### Task Lists

- [x] Completed task
- [ ] Incomplete task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3

### Videos

Valid video extensions are: `.mp4`, `.m4v`, `.mov`, `.webm`, `.ogv`

### Math

Math is written in LaTeX sytax rendered using [KaTeX](https://github.com/Khan/KaTeX)

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line
```math
a^2+b^2=c^2
```

### Colors

- Formats
    - HEX: `#RGB[A]` or `#RRGGBB[AA]`
    - RGB: `RGB[A](R, G, B[, A])`
    - HSL: `HSL[A](H, S, L[, A])`
- Colors
    - `#F00`
    - `#F00A`
    - `#FF0000`
    - `#FF0000AA`
    - `RGB(0,255,0)`
    - `RGB(0%,100%,0%)`
    - `RGBA(0,255,0,0.7)`
    - `HSL(540,70%,50%)`
    - `HSLA(540,70%,50%,0.7)`

### Mermaid

It is possible to generate diagrams and flowcharts using [Mermaid](https://mermaidjs.github.io/)

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

## Standard Markdown

### Headers

#### H4

##### H5

###### H6

### Header IDs

All Markdown-rendered headers automatically get IDs, which can be linked to.
On hover, a link to those IDs becomes visible to make it easier to copy the link to the header to
use it somewhere else. The IDs are generated from the content of the header according to the following rules:
- All text is converted to lowercase.
- All non-word text (e.g., punctuation, HTML) is removed.
- All spaces are converted to hyphens.
- Two or more hyphens in a row are converted to one.
- If a header with the same ID has already been generated, a unique incrementing number is appended, starting at 1.

### Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

### Lists

#### Basic

1. First ordered list item
2. Another item
    * Unordered sub-list.
3. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.

* Unordered list can use asterisks
- Or minuses
+ Or pluses

#### With Paragraphs

1. First ordered list item

   Second paragraph of first item.

2. Another item

### Links

- [I'm an inline-style link](https://www.google.com)
- [I'm a link to a repository file in the same directory](index.md)
- [I am an absolute reference within the repository](/doc/user/index.md)
- [I'm a relative link to the Milestones page](../README.md)
- [I link to a section on a different markdown page, using a header ID](index.md#overview)
- [I link to a different section on the same page, using the header ID](#header-ids-and-links)
- [I'm a reference-style link][Arbitrary case-insensitive reference text]
- [You can use numbers for reference-style link definitions][1]
- Or leave it empty and use the [link text itself][]

<!-- Some text to show that the reference links can follow later. -->
[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: https://www.reddit.com

### Images

Here's our logo (hover to see the title text):

Inline-style:
![alt text](static/markdown_logo.png)

Reference-style:
![alt text1][logo]

[logo]: static/markdown_logo.png

### Blockquotes

> Blockquotes are very handy in email to emulate reply text. This line is part of the same quote.
Quote break.
> This is a very long line that will still be quoted properly when it wraps.
> Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone.
> Oh, you can *put* **Markdown** into a blockquote.

### Inline HTML

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

### Details and Summary

<details>
<summary>Click me to collapse/fold.</summary>

These details _will_ remain **hidden** until expanded.

    PASTE LOGS HERE

</details>

### Horizontal Rule

***

Asterisks

### Line Breaks

Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also a separate paragraph, but...
This line is only separated by a single newline, so it *does not break* and just follows the previous line in the *same paragraph*.

This line is also a separate paragraph, and...
This line is *on its own line*, because the previous line ends with two spaces. (but still in the *same paragraph*)

spaces.

### Tables

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |

### Footnotes

You can add footnotes to your text as follows.[^2]
[^2]: This is my awesome footnote.

### Superscripts / Subscripts

CommonMark and GFM currently do not support the superscript syntax ( x^2 ) that Redcarpet does. You can use the standard HTML syntax for superscripts and subscripts.

The formula for water is H<sub>2</sub>O
while the equation for the theory of relativity is E = mc<sup>2</sup>.
