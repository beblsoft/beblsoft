# GitHub Markdown Demo

## Headings

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6 (Smallest)

## Styled Text

Styled Text: __bold1__, **bold2**, _italic1_, *italic2*, ~~strikethrough~~, **_bold and italic_**

## Quotes

### Block

In the words of Abraham Lincoln:
> Pardon my French.

### Inline

Use `git status` to list all new or modified files that haven't been committed

### Multiline

Bash:
```bash
$ git status
$ git add
$ git commit
```

JavaScript:
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
````

Python:
```python
def foo():
    if not bar:
        return True
```

## Horizontal Rule

Three or more...

Asterisks

***

## Line Breaks

Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also a separate paragraph, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.

## Links

### Absolute

This site was built using [GitHub Pages](https://pages.github.com/).

### Section

Go back up to [Heading 2](#heading-2).

Go back up to [Heading 3](#heading-3).

### Relative

Over to [README.md](./README.md).
Relative links are easier than absolute links

## Lists

### Unordered

- George Washington
- John Adams
- Thomas Jefferson

### Ordered

1. James Madison
2. James Monroe
3. John Quincy Adams

### Nested

1. First list item
    - First nested list item
        - Second nested list item

### Tasks

- [X] Finish my changes
- [ ] Push my commits
- [ ] Open a merge request

## Ignoring

Let's rename \*our-new-project\* to \*our-old-project\*.

## Images

Inline-style:

![Alternative Text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Title")

Local Picture. Picture must be appropriately sized.

![Alternative Text](static/circle.png)

## Youtube Videos

[![Funniest America's Home Video](https://img.youtube.com/vi/35_FVzA0XU0/0.jpg)](https://www.youtube.com/watch?v=35_FVzA0XU0)

## Tables

| First Header                | Second Header                |
|:----------------------------|:-----------------------------|
| Content form cell 1         | Content from cell 2          |
| Content in the first column | Content in the second column |

## Referencing People, Items

Mention team @team. All members of team will receive a notification.

## Emoji

@octocat :+1: This PR looks great - it's ready to merge! :shipit: