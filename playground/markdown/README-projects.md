# Overview

Markdown Samples Documentation

## Samples

- `github.md`

    * Demonstrate github markdown sytax
    * [Reference](https://guides.github.com/features/mastering-markdown/)

- `gitlab.md`

    * Demonstrate gitlab markdown samples
    * [Reference](https://docs.gitlab.com/ee/user/markdown.html)