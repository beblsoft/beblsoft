OVERVIEW
===============================================================================
Docker Machine Technology Documentation
- Description ----------------------: Docker Machine is a tool that lets you install Docker Engine
                                      on virtual hosts, and manage the hosts with docker-machine commands
                                    : Use Machine to create docker hosts on local Mac and Windows box, in
                                      data center, or on cloud providers like Azure, AWS, or Digital Ocean
                                    : Point Machine CLI at a running, managed host, and you can run docker
                                      commands directly on that host
- Use Cases ------------------------1 Install and run Docker on Mac or Windows
                                    2 Provision and manage multiple remote Docker hosts
                                    3 Provision Swarm clusters
- Picture ---------------------------: -------------------
                                       | Laptop          |
                                       | docker commands |
                                       -------------------
                                             |
                                          ----------------
                                          |              |
                                       ------------   ------------
                                       |  VM i    |   |  VM k    |
                                       |  docker  |   |  docker  |
                                       ------------   ------------
- Relevant URLS ---------------------
  * Home                            : https://www.docker.com/
  * Documentation                   : https://docs.docker.com/machine/overview/



