OVERVIEW
===============================================================================
Docker Sample Projects Documentation


PROJECT: (OFFICIAL) HELLO WORLD
===============================================================================
- Description                       : Run official docker hello world
- To run
  * Run                             : docker run hello-world



PROJECT: HELLO WORLD
===============================================================================
- Description                       : Create docker image to echo hello world repeatedly
- To run
  * Build dockerfile                : docker build -t="jbensson/helloworld" ./helloWorld
  * Run                             : docker run -d --name hw jbensson/helloworld
  * Examine logs                    : docker logs -tf hw
  * Kill                            : docker kill hw



PROJECT: STATIC WEB
===============================================================================
- Description                       : Create docker image that runs a static web server
- To run
  * Build dockerfile                : docker build -t="jbensson/staticweb" ./staticWeb
  * Run                             : docker run -d -p 32760:80 --name staticWeb jbensson/staticweb
  * Ping nginx server in container  : curl localhost:32760
  * Kill                            : docker kill staticWeb



PROJECT: ECS PHP SIMPLE
===============================================================================
- Description                       : AWS Elastic Container Service Simple PHP Application
- To run
  * Build dockerfile                : docker build -t="jbensson/awsphpsimple" ./ecs-php-simple
  * Run                             : docker run -p 5000:80 jbensson/awsphpsimple
  * See website                     : Navigate chrome to location: localhost:5000
                                      Stops on Ctrl-C



PROJECT: SAMPLE WEB
===============================================================================
- Description                       : Sample nginx web site
                                    : Maps read only volume from host into container
- To run
  * Build dockerfile                : docker build -t="jbensson/sampleweb" ./sampleWeb
  * Show image history              : docker history jbensson/sampleweb
  * Run                             : docker run -d -p 32773:80                     \
                                      -v $PWD/sampleWeb/website:/var/www/html/website:ro \
                                      --name sw                                          \
                                      jbensson/sampleweb nginx
  * See website                     : Navigate chrome to location: localhost:32773
  * Kill                            : docker kill sw


PROJECT: HELLO FLASK
===============================================================================
- Description                       : Sample flask application
- To run
  * Build dockerfile                : docker build -t="jbensson/helloflask" ./helloFlask
  * Run                             : docker run -d -p 5000:5000 --name helloFlask  \
                                      jbensson/helloflask
  * See website                     : Navigate chrome to location: localhost:5000
  * Kill                            : docker kill helloFlask
