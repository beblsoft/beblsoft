#!/usr/bin/env python3
"""
NAME:
 app.py

DESCRIPTION
 Flask Hello World Application
"""

# ------------------------ IMPORTS ------------------------------------------ #
import logging
import socket
from datetime import datetime
from flask import Flask


# ------------------------ GLOBALS ------------------------------------------ #
app = Flask(__name__)
logger = logging.getLogger(__file__)


# ------------------------ ROUTES ------------------------------------------- #
@app.route("/")
def root(): #pylint: disable=C0111
    return "Hello from Flask! Server name:{}".format(socket.gethostname())


@app.route("/time")
def time(): #pylint: disable=C0111
    return "The time is: {}".format(datetime.now())


# ------------------------ MAIN --------------------------------------------- #
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
