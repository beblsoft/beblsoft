OVERVIEW
===============================================================================
Docker Technology Documentation
- Description                       : Docker is a platform for developers and sysadmins to develop,
                                      deploy, and run applications with containers
                                    : Containers run in user space on top of an operating system's kernel
                                    : Multiple isolated user space instances can be run on a single host
                                    : Docker is an open-source engine that automates the deployment
                                      of applications into containers.
- Use Cases -------------------------
  * Fast, consistent delivery       : Streamlines development lifecycle by allowing developers
                                      to work in standardized environments using local containers
  * Responsive deployment and scale : Highly portable workloads. Can run on laptop, physical or VMs
                                      in data centers, or mixture of envs. Dynamically
                                      change workloads by scaling up or down
  * Running workloads on hardware   : Viable, cost-effective alternative to hypervisor-based
                                      virtual machines
- Container features ----------------
  * Flexible                        : Even most complex apps can be containerized
  * Lightweight                     : Leverage and share the host kernel
  * Repeatable                      : Run consistently in different environments.
  * Interchangeable                 : Deploy updates and upgrades on the fly
  * Portable                        : Build locally, deploy to cloud, and run anywhere
  * Scalable                        : Increase and automatically distribute container replicas
  * Stackable                       : Stack services vertically and on-the-fly
  * Reduce cycle time               : Quick turnover between code being tested, deployed, and used.
  * Microservices                   : Service-oriented and microservices architectures. Design each container
                                      to house a single process or application.
- Installation -----------------------
  * Install from docker.sh          : docker_install
  * Uninstall from docker.sh        : docker_uninstall
- Relevant URLS ---------------------
  * Home                            : https://www.docker.com/
  * Documentation                   : https://docs.docker.com/
  * Command Cheat Sheet             : https://github.com/wsargent/docker-cheat-sheet
  * Inspect Formatting cheat sheet  : https://container-solutions.com/docker-inspect-template-magic/
- Books -----------------------------
  * The Docker Book, James Turnbull : https://dockerbook.com/



GETTING STARTED
===============================================================================
- Docker Engine --------------------: Is a client-server application
  * Server                          : Long running daemon process
  * REST API                        : Specifies interfaces that programs can use to talk to daemon
  * Command Line Interface (CLI)    : The docker command allowing clients to talk to daemon
- Architecture ----------------------
                                    : Docker uses client-server architecture. Docker client talks to Docker daemon
                                      which does the heavy lifting of building, running, and distributing containers

                                      ---------------     -----------------------  ---------------
                                      | Client      |     |Docker Host (ex. VM) |  | Registry    |
                                      |             |     |                     |  |             |
                                      | docker build--------Docker Daemon ------------Images     |
                                      | docker pull-/     |                     |  |   o o o     |
                                      | docker run--|     | Containers   Images |  |   o o o     |
                                      ---------------     |  o o o        o o   |  ---------------
                                                          |  o o o        o o   |
                                                          |  o o o        o o   |
                                                          |  o o o              |
                                                          |                     |
                                                          -----------------------
- Processes -------------------------
  * Docker daemon                   : Docker daemon (dockerd) listens for Docker API requests and manages
                                      Docker objects such as images, containers, networks, and volumes
                                      Can also communicate with other daemons to manage services
  * Docker client                   : The docker client (docker) is the primary way that many docker
                                      users interact with Docker. When you use commands such as docker run,
                                      the client sends these commands to dockerd, which carries them out
  * Docker registries               : A Docker registry stores Docker images.
                                      Docker Hub is a public registry that everyone can use, and Docker
                                      is configured to look for images on Docker Hub by itself
- Objects ---------------------------
  * Images                          : An images is a read-only template with instructions for creating a
                                      Docker container. Often, an image is based on another image, with
                                      some additional customization.
                                    : Docker creates images by layering parent and child read
                                      only images on top of one another. After the container
                                      is spawned, the processes running inside the container have
                                      copy-on-write semantics, changing the data within the
                                      container, without modifying the underlying image.
                                      -----------------------
                                      |Container            |
                                      |Copy-On-Write        |
                                      -----------------------
                                      |Image                |
                                      |Add Apache           |
                                      |---------------------|
                                      |Image                |
                                      |Add Emacs            |
                                      |---------------------|
                                      |Base Image           |
                                      |Ubuntu               |
                                      |---------------------|
                                      |bootfs               |
                                      |cgroups, namespace,  |
                                      |device mapper        |
                                      |Kernel               |
                                      -----------------------
                                    : Naming: <registry>/<user>/<repository>:<tag>
                                      registry.gitlab.com/beblsoft/beblsoft/smeckn/base:v1
  * Dockerfile                      : File used to create a docker image
                                      Created with simple syntax for defining the steps needed to create
                                      the image and run it.
                                      Each instruction in a dockerfile creates a layer in the image
                                      When a Dockerfile is changed and rebuilt, only changed layers are rebuilt
  * Container                       : A container is a runtime instance of an image
                                      Can create, start, stop, move, and delete a container using the Docker API.
                                      By default, container is isolated from other containers on host
                                      Can control how isolated container's network, storage, or other underlying
                                      subsystems are from other containers on host
                                      ------------------------------
                                      |  -----------  -----------  |
                                      |  |Container|  |Container|  |
                                      |  -----------  -----------  |
                                      |  | App A   |  | App B   |  |
                                      |  -----------  -----------  |
                                      |  |Bins/Libs|  |Bins/Libs|  |
                                      |  -----------  -----------  |
                                      | -------------------------- |
                                      | | Docker                 | |
                                      | -------------------------- |
                                      | -------------------------- |
                                      | | Host OS                | |
                                      | -------------------------- |
                                      | -------------------------- |
                                      | | Infrastructure         | |
                                      | -------------------------- |
                                      ------------------------------
  * Services                        : Services allow you to scale containers across multiple docker daemons,
                                      which all worker together as a swarm with multiple managers and workers
                                      Each member of a swarm is a docker daemon, and the daemons all communicate
                                      using the Docker API.
                                      A service allows you to define the desired state, such as the number
                                      of replicas of the service that must be available at any given time
                                      By default, the service is load balanced across all worker nodes
- Underlying Technology -------------
  * Language                        : Docker is written in Go
  * Namespaces                      : Docker uses a technology called namespaces to provide the isolated
                                      workslace called the container
                                      Namespaces provide a layer of isolation
                                      pid namespace: process isolation
                                      net namespace: network
                                      ipc namespace: interprocess communication
                                      mnt namespace: managing filesystme mount points
                                      uts namespace: Unix Timesharing System
  * Control Groups                  : A control group (cgroup) limits an application to a specific set of resources
                                      Allow docker engine to share available resources to containers and
                                      optionally enforce limits and constraits
  * Union file systems              : Union file systems, or UnionFS, are file systems that operate by creating
                                      layers, making them lightweight and fast
                                      Docker Engine uses UnionFS to provide the building blocks for containers
  * Container format                : Combines namespaces, control groups, and UnionFS into a wrapper called
                                      a container format. The default container format is libcontainer
- Technical components --------------
  * Native linux container format   : libcontainer
  * Linux kernel namespaces         : provide isolation for filesysems, processes, and networks
  * Filesystem isolation            : each container is its own root filesystem
  * Process isolation               : each container runs in its own process environment
  * Network isolation               : separate virtual interfaces and IP addressing between containers
  * Resource isolation and grouping : cgroups allows CPU and memory to be allocated on a per container basis
  * Copy-on-write                   : Layerd filesystems require limited disk usage
  * Logging                         : STDOUT, STDERR, and STDIN are collected, logged, and available
                                      for analysis
  * Interactive shell               : Create a pseudo-tty and attach to STDIN to provide interactive shell
- Relevant Paths --------------------
  * Docker configuration directory  : /var/lib/docker
  * Docker container metadata       : /var/lib/docker/containers
  * Docker image metadata           : /var/lib/docker/aufs (or devicemapper)
  * Docker socket to daemon         : /var/run/docker.sock
  * Docker user credentials         : ~/.docker/config.json



DEVELOPMENT BEST PRACTICES
===============================================================================
- Keeping images small--------------: Small images are faster to pull over the network and faster to load
                                      into memory
  * Start with appropriate base     : If need JDK start with openjdk, not ubuntu
  * Use multistage builds           : Allows including artifacts of build without including layers themselves
  * Minimize run statements
  * Base image w/ shared components : Can be used my multiple images with shared needs
  * Use production as base for debug: Keep production image lean
  * Tag images appropriately        : Example prod, test.

- Persisting application data -------
  * Store data using volumes
  * Bind mounts                     : Useful in development to mount source directory or binary into container
                                      For production, use a volume instead
  * Secrets                         : Use in production to store sensitive application data used by services
  * Configs                         : Use for non-sensitive data

- Use swarm services when possible --
- Use CI/CD for test and deploy ----: Automatically build, tag, and test docker image on each checkin
                                    : Sign images before they can be deployed into production

- Differences in prod/development ---
  * Data                            : Development uses bind mounds to give container access to source code
                                      Production uses volumes to store container data
  * Time drift                      : Development doesn't worry
                                      Production needs NTP client on Docker host and within each container
                                      process



DOCKERFILE
===============================================================================
- About                             : Docker can build images automatically by reading the
                                      instructions from a Dockerfile
                                      A Dockerfile is a text document that contains all the commands a user
                                      could call on the command line to assemble an image
                                      Using docker build users can create an automated build that executes
                                      several command-line instructions in succession
- Usage                             : docker build commands builds an image from a Dockerfile and a context
                                      The build's context is the set of files at a specified location
                                      PATH or URL
                                      Context is processed recursively. So, a PATH includes any subdirectories
                                      and the URL includes the repositories and its submodules
                                    : Traditionally, Dockerfile is called Dockerfile and located in the root
                                      of the context
                                    : Docker daemon runs the instructions in the Dockerfile one-by-one,
                                      committing the result of each instruction to a new image if necessary

- Buildkit                          : New backend for builds provided by moby/buildkit project
                                      Many benefits
                                      - Detect and skip executing unused build stages
                                      - Parallelize building independent build stages
                                      - Incrementally transfer only the changed files in your build context between builds
                                      - Detect and skip transferring unused files in your build context
                                      - Use external Dockerfile implementations with many new features
                                      - Avoid side-effects with rest of the API (intermediate images and containers)
                                      - Prioritize your build cache for automatic pruning
                                      To use set DOCKER_BUILDKIT=1 before calling docker build
- Format                            : Structure of the Dockerfile:
                                      # Comment
                                      INSTRUCTION arguments
                                    : Instructions should be UPPERCASE
                                    : Must start with FROM that specifies a Base image
                                    : Lines that start with # are comments, or a parser directive
- Parser directives ----------------: Optional, and affect the way in whcih subsequent lines in a Dockerfile
                                      are handled
                                    : Written in form # directive=value
                                    : All parser directives muyst be at the very top of a Dockerfile
                                    : Convention is to be lowercase
  * Supported Directives            : syntax, escape
  * Syntax                          : Defines the location of the Dockerfile builder that is used for building
                                      the current Dockerfile
                                    : # syntax=docker/dockerfile
                                      # syntax=docker/dockerfile:1.0
                                      # syntax=docker.io/docker/dockerfile:1
                                      # syntax=docker/dockerfile:1.0.0-experimental
                                      # syntax=example.com/user/repo:tag@sha256:abcdef...
  * Escape                          : Sets the character to be used to escape characters in a Dockerfile
                                      Default is \
                                      Escape allows docker commands to span multiple lines
                                    : # escape=\ (backslash)
                                      # escape=` (backtick)
- Environment replacement ----------: Environment variables (declared with the ENV statement) can also be
                                      used in certain instructions as variables to be interpreted by
                                      the Dockerfile
  * Default                         : ${variable:-word} indicates that if variable is set then the result will be
                                                        that value. If variable is not set then word will be the
                                                        result.
  * Escaping                        : \${foo} will be translated to ${foo}
- .dockeringnore -------------------: In the root directory of the context
                                      If file exists, CLI modifies the context to exclude files and
                                      directories that match patterns in it
  * Matching                        : Done with Go's filepath.Match rules
                                      ! - exceptions to exclusions
  * Example syntax
    # comment                       : Ignored.
    */temp*                         : Exclude files and directories whose names start with temp in any
                                      immediate subdirectory of the root. For example, the plain file
                                      /somedir/temporary.txt is excluded, as is the directory
                                      /somedir/temp.
    */*/temp*                       : Exclude files and directories starting with temp from any subdirectory
                                      that is two levels below the root.
                                      For example, /somedir/subdir/temporary.txt is excluded.
    temp?                           : Exclude files and directories in the root directory whose names
                                      are a one-character extension of temp. For example,
                                      /tempa and /tempb are excluded.


DOCKERFILE COMMANDS
===============================================================================
- FROM -----------------------------: Initializes a new build stage and sets the Base image for subsequent
                                      instructions
                                    : Whenever possible, use current official images as basis for images
                                      Recommend Alpine image, as it is tightly controlled and small in size
                                      (under 5MB)
  * Syntax                          : FROM <image> [AS <name>]
                                      FROM <image>[:<tag>] [AS <name>]
                                      FROM <image>[@<digest>] [AS <name>]
  * Multilayer                      : FROM can appear multiple times within a single Dockerfile to create
                                      multiple images or use one build stage as a dependency for another
- RUN ------------------------------: Executes any commands in a new layer on top of the current image
                                      and commit the results
                                    : Split long or complex RUN statements on multiple lines
                                      separated by backslashes to make more readable, understandable, and maintainable
  * Syntax                          : RUN <command>                           (shell form, uses /bin/sh -c on Linux)
                                      RUN ["executable", "param1", "param2"]  (exec form)
  * Examples                        : RUN /bin/bash -c 'source $HOME/.bashrc; echo $HOME'
                                      RUN ["/bin/bash", "-c", "echo hello"]
  * apt-get                         : Avoid "RUN apt-get upgrade" or "dist-upgrade" as they install multiple
                                      unneeded packages
                                    : RUN api-get update and install in same line
                                      RUN apt-get update && apt-get install -y \
                                          aufs-tools \
                                          automake \
                                          build-essential \
                                          curl \
                                          dpkg-sig \
                                          libcap-dev \
                                          libsqlite3-dev \
                                          mercurial \
                                          reprepro \
                                          ruby1.9.1 \
                                          ruby1.9.1-dev \
                                          s3cmd=1.1.* \
                                       && rm -rf /var/lib/apt/lists/*
- CMD ------------------------------: Provides defaults for an executing container
                                      There can only be one CMD instruction in a Dockerfile
                                      If ENTRYPOINT used as well, CMD and ENTRYPOINT should eb a JSON array
                                      If container should run the same executable every time, use the
                                      ENTRYPOINT in combination with CMD
                                      CMD does not execute anything at build time
  * Syntax                          : CMD ["executable","param1","param2"] (exec form, this is the preferred form)
                                      CMD ["param1","param2"]              (as default parameters to ENTRYPOINT)
                                      CMD command param1 param2            (shell form)
  * Example                         : CMD ["/usr/bin/wc","--help"]
- LABEL ----------------------------: Adds metadata to an image.
                                      Is a key-value paire
  * Syntax                          : LABEL <key>=<value> <key>=<value> <key>=<value> ...
  * Examples                        : LABEL "com.example.vendor"="ACME Incorporated"
                                      LABEL com.example.label-with-value="foo"
                                      LABEL version="1.0"
                                      LABEL description="This text illustrates \
                                                         that label-values can span multiple lines."
- EXPOSE ---------------------------: Informs Docker that the container listens on the specified network
                                      ports at runtime.
                                    : Does not actually publish the port. This is done when running the
                                      container with the -p flag
                                    : It functions as a type of documentation between the person who
                                      builds the image and the person who runs the container
                                      EXPOSE assumes TCP
  * Syntax                          : EXPOSE <port> [<port>/<protocol>...]
  * Examples                        : EXPOSE 80/tcp
                                      EXPOSE 80/udp
- ENV ------------------------------: Sets the environment variable key value pair
                                    : Use ENV to update the PATH environment variable
                                      ENV PATH /usr/local/nginx/bin:$PATH
                                      Useful for providing required environment variables to specific services
                                      Can be used to set commonly used version numbers
  * Syntax                          : ENV <key> <value>
                                      ENV <key>=<value> ...
  * Examples                        : ENV myName="John Doe" myDog=Rex\ The\ Dog \ myCat=fluffy
                                      ENV myName John Doe
                                      ENV myDog Rex The Dog
                                      ENV myCat fluffy
- ADD ------------------------------: Copies new files, directories or remote file URLS from src to dest
                                    : All new files and directories are created with UID and GID of 0 unless --chown
                                    : src must be in build context
  * Syntax                          : ADD [--chown=<user>:<group>] <src>... <dest>
                                      ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]
                                        (this form is required for paths containing whitespace)
  * Examples                        : ADD hom* /mydir/        # adds all files starting with "hom"
                                      ADD hom?.txt /mydir/    # ? is replaced with any single character, e.g., "home.txt"
                                      ADD test relativeDir/   # adds "test" to `WORKDIR`/relativeDir/
                                      ADD test /absoluteDir/  # adds "test" to /absoluteDir/
- COPY -----------------------------: Copies new files, directories or remote file URLS from src to dest
                                    : All new files and directories are created with UID and GID of 0 unless --chown
                                    : src must be in build context
                                    : COPY is preferred over ADD even though ADD can extract tars
                                    : COPY only supports the basic copying of files into the container
                                    : If you have multiple Dockerfile steps that use different files from context
                                      COPY them individually rather than all at once
  * Syntax                          : COPY [--chown=<user>:<group>] <src>... <dest>
                                      COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]
                                        (this form is required for paths containing whitespace)
  * Examples                        : COPY hom* /mydir/        # adds all files starting with "hom"
                                      COPY hom?.txt /mydir/    # ? is replaced with any single character, e.g., "home.txt"
                                      COPY test relativeDir/   # adds "test" to `WORKDIR`/relativeDir/
                                      COPY test /absoluteDir/  # adds "test" to /absoluteDir/
- ENTRYPOINT -----------------------: Configures container to run as an executable
                                    : Command line arguments to docker run will be appended after
                                      all elements in an exec form ENTRYPOINT, and will override all elements
                                      specified using CMD
                                    : Can be overriden on command line using docker run --entrypoint
                                    : Dockerfile should specify at least one of CMD or ENTRYPOINT commands
                                    : ENTRYPOINT should be defined when using the container as an executable
                                    : CMD should be used as a way of defining default arguments for an
                                      ENTRYPOINT command or for executing ad-hoc command in a container
                                    : CMD will be overridden when running the container with alternative args
                                    : Useful to leverage helper scripts for ENTRYPOINT, can do processing inside script
                                      COPY ./docker-entrypoint.sh /
  * Syntax                          : ENTRYPOINT ["executable", "param1", "param2"] (exec form, preferred)
                                      ENTRYPOINT command param1 param2 (shell form)
  * Examples                        : FROM ubuntu
                                      ENTRYPOINT ["top", "-b"]
                                      CMD ["-c"]
- VOLUME ---------------------------: Creates a mount point with the specified name and marks it
                                      as holding externally mounted volumes from native host or
                                      other containers
                                    : Note: the host directory must be declared at container run-time
  * Syntax                          : VOLUME ["/data"]
  * Examples                        : FROM ubuntu
                                      RUN mkdir /myvol
                                      RUN echo "hello world" > /myvol/greeting
                                      VOLUME /myvol
- USER -----------------------------: Sets the user name (or UID) and optionall the user group (or GID)
                                      to use when running th eimage and for any RUN, CMD and ENTRYPOINT
                                      instructions that follow it in the Dockerfile
                                    : If a service can run without privileges, use USER to change
                                      to a non-root user.
  * Syntax                          : USER <user>[:<group>] or
                                      USER <UID>[:<GID>]
  * Example                         : USER patrick
- WORKDIR --------------------------: Sets the working directory for any RUN, CMD, ENTRYPOINT, COPY, and ADD
                                      instructions
                                    : Can be used multiple times in a Dockerfile
                                    : Always use absolute paths for WORKDIR
                                      Use WORKDIR instead of RUN cd ... && do something
  * Syntax                          : WORKDIR /path/to/workdir
  * Example                         : WORKDIR /a
                                      WORKDIR b
                                      WORKDIR c
                                      RUN pwd  # Runs in /a/b/c
- ARG ------------------------------: Defines a variable that users can pass at build-time to the builder
                                      with the docker build command
                                    : More than one ARG may be defined
                                    : ARG instruction goes out of scope at the end of the build stage where
                                      it was defined. To use ARG in multiple stages, each stage must include
                                      the ARG instruction
                                    : Environment variables override ARG variables of same name
                                    : Set on command line with docker build --build-arg <varname>=<value>
  * Syntax                          : ARG <name>[=<default value>]
  * Example                         : ARG user1=someuser
                                      ARG buildno=1
- ONBUILD --------------------------: Adds to the image a trigger instruction to be executed at a later
                                      time, when the image is used as the base for another build
                                    : The trigger will be executed in the context of the downstream
                                      build as if it had been inserted immediately after the FROM
                                      instruction in the downstream Dockerfile
  * Syntax                          : ONBUILD [INSTRUCTION]
- STOPSIGNAL -----------------------: Sets the system call signal that will be sent to the container to exit
                                    : Signal can be a valid unsigned number that matches a position in
                                      the kernel's syscall table, or a signal name.
  * Syntax                          : STOPSIGNAL signal
  * Examples                        : STOPSIGNAL 9
                                      STOPSIGNAL SIGKILL

- HEALTHCHECK ----------------------: Tells Docker how to test a continaer to check that it is still working
  * Options                         : --interval=DURATION (default: 30s)
                                      --timeout=DURATION (default: 30s)
                                      --start-period=DURATION (default: 0s)
                                      --retries=N (default: 3)
  * Syntax                          : HEALTHCHECK [OPTIONS] CMD command
                                      (check container health by running a command inside the container)
                                      HEALTHCHECK NONE
                                      (disable any healthcheck inherited from the base image)
  * Example                         : HEALTHCHECK --interval=5m --timeout=3s \
                                        CMD curl -f http://localhost/ || exit 1
- SHELL ----------------------------: Allows the default shell used for the shell form of commands
                                      to be overridden
                                    : Can appear multiple times, most recent overrides previous
  * Syntax                          : SHELL ["executable", "parameters"]
  * Example                         : SHELL ["/bin/sh", "-c"]  (default)



DOCKERFILE BEST PRACTICES
===============================================================================
- Each instruction creates one layer: FROM creates a layer from the ubuntu:15.04 Docker image.
                                      COPY adds files from your Docker client’s current directory.
                                      RUN builds your application with make.
                                      CMD specifies what command to run within the container.
- Create ephemeral containers       : The image defined by your Dockerfile should generate containers
                                      that are as ephemeral as possible
                                      This means that the container can be stopped and destroyed, then
                                      rebuilt and replaced with an absolute minimum set up and configuration
- Understand build context          : When you issue a docker build command, the current working directory
                                      is called the build context.
                                      Be default, the Dockerfile is assumed to be located there
                                      Can specify a different location with the flag -f
- Pipe Dockerfile through stdin     : docker build -t foo context/dir1 -f-<<EOF
                                      FROM busybox
                                      RUN echo "hello world"
                                      COPY . /my-copied-files
                                      EOF
- Exclude with .dockerignore        : Exclude files not relevant to build with .dockerignore file
- Multistage builds                 : Leverage build cache to create single layer at end of the build
                                      Order less frequently changed commands in front of more freque
                                      - Install tools needed to build application
                                      - Install library dependencies
                                      - Generate application
- Don't install unnecessary packages: Reduce complexity, dependencies, file sizes and build times,
                                      avoid installing extra or unnecessary packages
- Decouple applications             : Each container should have only one concern
                                      Decoupling applications into multiple containers
                                      makes it easier to scale horizontally and reuse containers
                                      Keep clean and modular
- Sort Multi-line arguments         : Sort multiline arguments alphanumerically
                                      RUN apt-get update && apt-get install -y \
                                        bzr \
                                        cvs \
                                        git \
                                        mercurial \
                                        subversion
- Levarage build cache              : When building an image, Docker steps through
                                      instructions in Dockerfile, executing each in order specified
                                      If docker sees duplicates in other existing images it can reuse them
- Multi-Stage Builds ----------------
  * Goal                            : Keep image size down
  * Before multi-stage builds       : Elaborate shell commands to remove unneeded artifacts before moving
                                      to the next layer
                                      Builder pattern: common to have one Development Dockerfile and
                                      slimmed down version for production
  * Multi-stage builds              : Use multiple FROM statements in Dockerfile
                                      Each FROM instruction can use a different base, and each of them begins
                                      a new stage of the build
                                      Then selectively copy artifacts from one stage to another, leaving
                                      behind unwanted programs from final image
  * Example                         : FROM golang:1.7.3 as builder
                                      WORKDIR /go/src/github.com/alexellis/href-counter/
                                      RUN go get -d -v golang.org/x/net/html
                                      COPY app.go .
                                      RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

                                      FROM alpine:latest
                                      RUN apk --no-cache add ca-certificates
                                      WORKDIR /root/
                                      COPY --from=builder /go/src/github.com/alexellis/href-counter/app .
                                      CMD ["./app"]
  * Name build stages               : Name build stages so they aren't referred to as integers
  * Stop at a specific stage        : When building image, don't need to build the entire Dockerfile including
                                      every stage. Can specify a target build stage
                                      docker build --target builder -t jbensson/foo:1 .
  * Use cases                       : Debugging a specific build stage
                                      Using a debug stage with all debugging symbols or tools enabled
                                      Creating a lean production stage
                                      Using a test stage where app gets populated with test data
  * Copy from existing images       : Not limited to copying from stages you created earlier in Dockerfile
                                      COPY --from=nginx:latest /etc/nginx/nginx.conf /nginx.conf



OBJECT CONFIGURATION
===============================================================================
- Object Labels --------------------: Labels are a mechanism for applying metadata to Docker objects, including:
                                      Images, Containers, Local Daemons, Volumes, Networks, Swarm nodes,
                                      Swarm Services
  * Keys and values                 : A label is a key-value pair, stored as a string
                                      Each key-value pair must be unique within an object
                                      Most-recently-written overwrites all previous values
  * Key format recomentations       : Keys are alphanumeric strings which can contain periods and hyphens
                                      Reverse dns notation, com.example.some-label
                                      Only use lowercase letters
  * Value guidelines                : Can contain any data type that can be represented as a string
                                      JSON, XML, CSV, or YAML
  * Examples                        : LABEL "com.example.vendor"="ACME Incorporated"
                                      LABEL com.example.label-with-value="foo"
                                      LABEL version="1.0"
                                      LABEL description="This text illustrates \
                                      that label-values can span multiple lines."
- Prune unused objects -------------: Docker takes a conservative approach to cleaning up unused objects
                                      Objects not removed unless Docker is told to do so
- Format command and log output ----: Docker uses Go templates whcih can be used to manipulate the output
                                      format of certain commands and log drivers
                                      Many CLI commands have the --format flag with can be used to format output



CONTAINERS
===============================================================================
- Start Automatically --------------: Docker provides restart policies to controle whether your
                                      containers start automatically when they exit, or when Docker restarts
  * Restart Policies                : docker run --restart <policy>
                                      Only takes affect after container starts successfully
    no                              : Default, don't restart container
    on-failure                      : Restart container if it exits due to an error
    always                          : Always restart if it stops
    unless-stopped                  : Always restarted except when the container is stopped
- Alive during daemon downtime -----: By default, when the Docker daemon terminates,
                                      it shuts down running containers
                                      Can configure daemon so containers remain running when daemon unavailable
                                      This is called live restore
  * Enable live restore             : /etc/docker/daemon.json
                                      "live-restore": true
                                      or
                                      dockerd --live-restore
- Runtime metrics -------------------
  * Live stream                     : docker stats redis1 redis2
  * Control groups                  : Linux containers rely on control groups which track groups of processes
                                      and expose metrics about CPU, memory, and block I/O usage
                                      Stats through filesystem: /sys/fs/cgroup
    Enumerate                       : cat /proc/cgroups
                                      grep cgroup /proc/mounts
- Limit resources ------------------: By default, a container has no resource contstraints and can use
                                      as much of the given resource as the host's kernel scheduler allows
                                      Docker provides ways to control how much momory, or CPU a container can use
  * Memory
    Risks of running out            : Important not to allow a running container to consume too much of
                                      host machine's memory
                                      On linux hosts, if the kernel detects not enough memory, it throws an
                                      Out of Memory Exception, and starts killing processes to free up memory
                                      Any process is subject to killing, including Docker and other important
                                      applications. This can bring the entire systemt down if wrong process
                                      is killed
    Mitigate risk                   : Tests to understand memory
                                      Ensure app runs only on hosts with adequate resources
                                      Limit amount of memory container can use
                                      Be mindful when configuring swap
                                      Consider converting container to a service, and using service level constraints
    Limit Memory access             : --memory, --memory-swap, --kernel-memory
  * CPU
    Default                         : By default, each container's access to host CPU is unlimited
                                      Docker provides mechanisms to limit
                                      Use the default CFS scheduler or the realtime scheduler
    Configure CFS scheduler         : --cpus, --cpu-period, --cpu-quota, --cpuset-cpus, --cpu-shares
- Logging ---------------------------
  * View                            : docker logs
                                      docker service logs
                                      Shows commands STDOUT and STDERR
  * Logging drivers                 : Docker includes multiple logging mechanisms to help get information
                                      from running containers and services
                                      In addition to logging drivers included with Docker, one can also
                                      implement and use logging driver plugins
    Default                         : json-file
    Print current                   : docker info --format '{{.LoggingDriver}}'
    Configure in daemon file        : /etc/docker/daemon.json
                                      {
                                        "log-driver": "json-file",
                                        "log-opts": {
                                          "max-size": "10m",
                                          "max-file": "3",
                                          "labels": "production_status",
                                          "env": "os,customer"
                                        }
                                      }
    Delivery Mode                   : 1. Default, direct, blocking delivery from container to driver
                                      2. non-blocking delivery that stores log message in an intermediate
                                         per-container ring buffer for consumption by driver
                                         --log-opt mode=non-blocking --log-opt max-buffer-size=4m
  * Supported drivers
    none                            : No logss are available for the container and docker logs does not
                                      return output
    json-file                       : Logs formatted as JSON, default driver
    local                           : Writes log messages to local filesystem in binary fileds using Protobuf
    syslog                          : Writes log messages to syslog facility
    journald                        : Writes log messages to journald
    gelf                            : Writes log messages to a Graylog Extended Log Format (GELF) endpoint
                                      such as Graylog or Logstash.
    fluentd                         : Writes log messages to fluentd (forward input).
    awslogs                         : Writes log messages to Amazon CloudWatch Logs.
    splunk                          : Writes log messages to splunk using the HTTP Event Collector.
    etwlogs                         : Writes log messages as Event Tracing for Windows (ETW) events.
                                      Only available on Windows platforms.
    gcplogs                         : Writes log messages to Google Cloud Platform (GCP) Logging.
    logentries                      : Writes log messages to Rapid7 Logentries.
  * Customize log driver output     : Tag option specifieds how to format a tag that identifies the container's
                                      log messages. By default the system uses the first 12 characters of the
                                      container ID
    Tag Markup
     {{.ID}}                        : The first 12 characters of the container ID.
     {{.FullID}}                    : The full container ID.
     {{.Name}}                      : The container name.
     {{.ImageID}}                   : The first 12 characters of the container’s image ID.
     {{.ImageFullID}}               : The container’s full image ID.
     {{.ImageName}}                 : The name of the image used by the container.
     {{.DaemonName}}                : The name of the docker program (docker).
    Example                         : docker run --log-opt tag="{{.ImageName}}/{{.Name}}/{{.ID}}"
                                      Produces Aug  7 18:33:19 HOSTNAME hello-world/foobar/5790672ab6a0[9103]: Hello from Docker.



SECURITY
===============================================================================
- Areas                             1 Intrinsic security of the kernel and its support for namespaces
                                      and cgroups
                                    2 Attack surface of the docker daemon
                                    3 Loopholes in container config profile
                                    4 Hardening security features of the kernel
- Kernel namespaces ----------------: Docker containers are very similar to LXC containers, and they
                                      have similar security features.
                                      When a container is started, Docker creates a set of namespaces
                                      and control groups
                                      Around since July 2008
  * Namespaces                      : Provide the first and most straightforward form of isolation
                                      Processes running within a container can't see other processes in another
                                      container or on the host
  * Network stack                   : Each container gets its own network stack
- Control groups -------------------: Implement resource accountind and limiting
                                      Prevent denial of service attacks
                                      Around since 2006
- Docker Daemon Attack Surface -----: Daemon requires root privileges
                                      Only trusted users allowed to control daemon
- Linux kernel capabilities --------: By default, Docker starts containers with a restricted set of capabilities
                                      Makes it difficult of intruder inside container to do damage to host
                                      Best practice is to remove all capabilities except those required
  * Full list of capabilities       : http://man7.org/linux/man-pages/man7/capabilities.7.html
- Kernel Hardening -----------------: Many other ways to harden a docker host
                                      Ex. TOMOYO, AppArmor, SELinux, GRSEC



DOCKER-IN-DOCKER (DIND)
===============================================================================
- Description                       : Sometimes it is necessary for containers to build or run
                                      other containers
- Relevant URLs ---------------------
  * Recipe                          : https://github.com/jpetazzo/dind
  * Dangers of nesting              : https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
  * Nested success                  : https://blog.docker.com/2013/09/docker-can-now-run-within-docker/
  * Gitlab issue                    : https://gitlab.com/gitlab-org/gitlab-ce/issues/17769
- 1. Nested Containrs --------------: Spawn containers within themselves

                                      ------------------------------
                                      | Host                       |
                                      | docker                     |
                                      |                            |
                                      |  ------------------------- |
                                      |  | Container (Privileged)| |
                                      |  | docker (in docker)    | |
                                      |  |                       | |
                                      |  | --------------------  | |
                                      |  | | Nested Container |  | |
                                      |  | |                  |  | |
                                      |  | --------------------  | |
                                      |  ------------------------- |
                                      ------------------------------
  Pros                              : Nested containers are contained, don't interfere with host namespace
                                      Easily map volumes from Container to Sub Container
  Cons                              : Inner docker might try to apply security profiles (AppArmor,
                                      SELinux) that conflict with outer docker
                                    : Storage driver mismatch from outer to inner docker
  Issues                            : No AUFS on AUFS in /var/lib/docker
                                      Resolved: DIND dockerfile needs to make VOLUME /var/lib/docker
                                      See tools/docker/ubuntuDIND/Dockerfile
                                    : HTTP Services timing out
                                      Resolved export DOCKER_CLIENT_TIMEOUT=120
                                               export COMPOSE_HTTP_TIMEOUT=120
                                    : MySQL Nested Container
                                      Need to disable MySQL on host
                                      sudo ln -s /etc/apparmor.d/usr.sbin.mysqld /etc/apparmor.d/disable/
                                      sudo apparmor_parser -R /etc/apparmor.d/usr.sbin.mysqld
- 2 Sibling Containers -------------: Allow containers to spawn siblings by mounting
                                      docker host socket
                                    : docker run -v /var/run/docker.sock:/var/run/docker.sock -ti docker
                                      --------------------------------------------------
                                      | Host                                           |
                                      | docker                                         |
                                      |  /var/run/docker.sock                          |
                                      |          |                                     |
                                      |  --------|-------------- --------------------- |
                                      |  | /var/run/docker.sock| | Sibling Container | |
                                      |  | Container           | |                   | |
                                      |  ----------------------- --------------------- |
                                      |                                                |
                                      --------------------------------------------------
  Pros                              : Allows container to build, run, and push
                                    : Image caching on the host
                                    : Less overhead: don't need another docker container
  Cons                              : Namespace clashing: if two containers try to spawn sibling
                                      with the same name
                                    : Lose encapsulation, hard to clean up on failure
                                    : Bad if specific versions of docker are needed
                                    : Difficult to map volumes between Container and Sibling Container



NETWORKING
===============================================================================
- Overview                          : Can connect containers together, or connect them to non-Docker workloads
- Network drivers ------------------: Docker's networking subsystem is pluggable, using drivers
  * bridge                          : The default network driver. Bridge networks are usually used when
                                      your applications run in standalone containers that need to
                                      communicate
  * host                            : For standalone containers, remove network isolation between
                                      the container the the Docker host and use the host's networking
                                      directly
  * overlay                         : Overlay networks connect multiple Docker daemons together and enable
                                      swarm services to communicate with each other
  * macvlan                         : Macvlan networks allow you to assign a MAC address to a container
                                      making it appear as a physical device on your network
  * none                            : Disable all networking for container. Usually used in conjunction
                                      with a custom network driver
  * Network Plugins                 : Can install and use third-party network plugins with Docker
- Bridge Networks ------------------: A bridge network is a Link Layer device that forwards traffic
                                      between network segments
                                      Docker uses software bridges to allow containers connected
                                      over the same bridge to communicate
  * Default bridge                  : When Docker is started, a default bridge network is automatically
                                      created. User-defined bridges are preferred
                                      Not recommended for production use
  * User-defined bridge             : Manually created by the user
                                    : Provide better isolation and interoperability
                                    : Provide automatic DNS resoltion between containers
                                    : Can attach and detach containers on the fly
                                    : Is highly configurable
    Create                          : docker network create my-net
    Remove                          : docker network rm my-net
    Connect on container create     : docker create --name my-nginx --network my-net --publish 8080:80 nginx:latest
    Connect after container create  : docker network connect my-net my-nginx
    Disconnect                      : docker network disconnect my-net my-nginx



STORAGE: VOLUMES, MOUNTS
===============================================================================
- Overview                          : By default all files created inside a container are stored on a
                                      container layer. This means that:
                                        - Data doesn't persist after container leaves
                                        - Can't easily move data somewhere else
                                        - Writing to UnionFS is not performant
                                    : Two options to store files in host machine: volumes and bind mounts
                                    : Docker on linux can use tmpfs mount
- Mount types ----------------------: No matter which type of mount is chosen, the data looks the same from
                                      within the container. It is exposed as either a directory or a file
                                      in the container's file system
                                      The difference is where the data lives on the host
  * Volumes                         : Stored as part of the host filesystem managed by Docker
                                      /var/lib/docker/volumes on Linux
                                      Best way to persist data
  * Bind Mounts                     : May be stored anywhere on the host system
                                      Non-Docker processes on the Docker host or a Docker container can
                                      modify them at any time
  * tmpfs Mounts                    : Are store in the host system's memory only. Never written
                                      to the host system's filesystem
- Volumes --------------------------: Created and managed by Docker
                                      When created, stored within a directory on Docker host
                                      When mounted to container, directory is made available on the container
                                      Volume can be named or anonymous (given random name)
                                      Volumes support volume drivers, which allow you to store data on remote hosts
                                      or cloud providers
  create                            : docker volume create my-vol
  remove unused                     : docker volume prune
  use cases                         : Sharing data among multiple running containers
                                      When Docker host is not guaranteed to have a given directory or file structure
                                      Storing container's data on a remote host or cloud partner
                                      Backup, restore, or migrate data from one Docker host to another
- Bind Mounts ----------------------: When you use a bind mount, a file or directory on the host machine
                                      is mounted into a container. The file or directory is referenced by its
                                      full path on the host machine. Created on demand in host if it doesn't exist
                                      Bind mounts are very performant
  use cases                         : Sharing configuration files from host machine to containers
                                      Sharing source code or build artifacts between development and container
                                      If you use Docker of development, production would copy production-ready
                                      artifacts directly into image and not bind mount
- tmpfs Mounts ---------------------: A tmpfs mount is not persisted on disk, either on Docker
                                      host or in container
  use cases                         : Best used when do not want the data to persist



STORAGE DRIVERS
===============================================================================
- Description                       : To use storage drivers effectively it's important to know how Docker builds
                                      and stores images, and how images are used by containers
                                    : Storage drivers allow you to create data in the writable layer of
                                      your container. The files won't be persisted afte the container is
                                      deleted, and both read and write speeds are low
                                    : Volumes increase performance
- Images and layers ----------------: A docker image is built up from a series of layers. Each layer
                                      represents an instruction in the image's Dockerfile
                                    : Each layer is only a set of differences from the layer before it
                                      The laywers are stacked on top of each other.
                                    : When you create a new container, you add a new writable layer on top of
                                      the underlying layers. This layer is called the container layer.
                                    : A storage driver handles the detauls about how these layers interact
                                      with each other. Different storage drivers are available

                                        -----------------------------------
                                        | Thin R/W Layer                  |    Container Layer (R/W)
                                        -----------------------------------
                                      ---------------------------------------
                                      | ----------------------------------- |
                                      | | C243098uasdflkasd            0B | |
                                      | ----------------------------------- |
                                      | ----------------------------------- |
                                      | | 10mdslkih498aaskd       1.23 KB | |  Image Layers (R/)
                                      | ----------------------------------- |
                                      | ----------------------------------- |
                                      | | flkjasdaskdsdklsd      194.5 KB | |
                                      | ----------------------------------- |
                                      | ----------------------------------- |
                                      | | gpoqdkshcnvlfdsms      188.1 MB | |
                                      | ----------------------------------- |
                                      ---------------------------------------
- Container and layers -------------: Difference between container and an image is the top writable layer
                                      When the container is deleted, the top writable layer is deleted
                                    : Multiple containers can share access to the same underlying image
                                      and yet have their own data state
- Container size on disk ------------
  * size                            : The amount of data (on disk) that is used for the writable layer of
                                      each container
  * virtual size                    : Amount of data used for the read-only image data used by the container
                                      plus the container's writable layer size
- Copy-on-write (COW) strategy -----: Copy-on-write is a strategy of sharing and copying files for maximum
                                      efficiency. If a file or directory exists in a lower layer within the
                                      image, and another layer needs read access to it, it just used the existing
                                      file
                                    : The first time another layer needs to modify the file (when building
                                      the image or running the container), the file is copied into that layer
                                      and modified. This minimizes I/O and the size of each of the subsequent
                                      layers
  * Sharing -> smaller images       : Docker pull pulls each image layer separately. Stored in docker's local
                                      storage area
                                      /var/lib/docker/<storage-driver>/layers
                                      /var/lib/docker/aufs/layers
                                    : docker image history shows all layers inside the image
  * Copying makes containers efficnt: When file in existing container is modified the following COW operation occurs:
                                       - Search through image layers for file to update. Start at newest layer
                                         and work down to the base layer
                                       - Perform a copy_up operation on the first copy of the file that is found,
                                         to copy the file to the container's writable layer
                                       - Any modifications are made to this copy of the file, and the
                                         container cannot see the read-only copy of the file that
                                         exists in the lower layer
                                    : copy_up incurs performance overhead
- Types ----------------------------:
  * overlay2                        : Preferred storage driver
  * aufs                            : Preferred storage driver for Docker 18.06 and older
  * devicemapper                    : Old
  * btrfs, zfs                      : Used if they are the backing filesystem (filesystem on which
                                      Docker is installed)
                                      Allow for creating snapshots, but require more maintenance
                                      and setup
  * vfs                             : Intended for testing purposes
  * current storage driver          : docker info shows the storage driver
- AUFS -----------------------------: AUFS is a union filesystem.
                                    : aufs storage driver was previously the default storage driver
                                      used for managing images and layers on Docker for Debian
                                    : Union filesystem. Layers multiple directories on a single Linux
                                      host and presents them as a single directory
                                      Directories called branches in AUFS terminology, layers in Docker terminology
                                    : Unification process is referred to as a union mount
  * Docker directory                : /var/lib/docker/aufs/
                                      diff/   - Contents of each layer
                                      layers/ - metadata about how image layers are stacked
                                      mnt/    - Mount points, one per image or container layer
- OverlayFS ------------------------: OverlayFS is a modern union file, that is similar to but faster than AUFS.
                                    : OverlayFS layers two directories on a single Linux host and presents
                                      them as a single directory. These directories are called layers and
                                      the unification process is referred to as a union mount
                                    : lowerdirectory = lowerdir, upper directory = upperdir
                                      unified = merged
                                    : overlay2 supports up to 128 lower OverlayFS layers
  * Install with docker.sh          : docker_installOverlay2
  * Docker directory                : /var/lib/docker/overlay2



DOCKER DAEMON
===============================================================================
- Description                       : dockerd is the persistent process that manages containers
                                      Docker uses different binaries for the daemon and client
                                    : dockerd is a root-privileged daemon used to handle
                                      operations that can't be executed by regular users
- Configure -------------------------
  * JSON config file                : Preferred option, keeps all config in single place
                                      /etc/docker/daemon.json
    Example                         : {
                                        "debug": true,
                                        "tls": true,
                                        "tlscert": "/var/docker/server.pem",
                                        "tlskey": "/var/docker/serverkey.pem",
                                        "hosts": ["tcp://192.168.59.3:2376"]
                                      }
  * Flags when starting dockerd
    Example                         : dockerd --debug \
                                              --tls=true \
                                              --tlscert=/var/docker/server.pem \
                                              --tlskey=/var/docker/serverkey.pem \
                                              --host tcp://192.168.59.3:2376
- Directory -------------------------
  * Default Linux                   : /var/lib/docker
  * Default windows                 : C:\ProgramData\docker
  * Configure                       : Use data-root configuration option



COMMANDS
===============================================================================
- Generic ---------------------------
  * Help                            : docker --help
  * Version                         : Show the Docker version information
                                      docker version [OPTIONS]
  * Information                     : docker info
  * Specify host dockerd            : Default=/var/run/docker.sock
                                      docker -H tcp://0.0.0.0:2375 ...
                                      docker -H unix://home/docker/docker.sock ...
  * events                          : Get real time events from the server
                                      docker events [OPTIONS]
  * inspect                         : Return low-level information on Docker objects
                                      docker inspect [OPTIONS] NAME|ID [NAME|ID...]
    Join, concatenate list of string: docker inspect --format '{{join .Args " , "}}' container
    JSON                            : docker inspect --format '{{json .Mounts}}' container
    lower                           : docker inspect --format "{{lower .Name}}" container
    split                           : docker inspect --format '{{split .Image ":"}}'
    title, cap first char of str    : docker inspect --format "{{title .Name}}" container
    upper                           : docker inspect --format "{{upper .Name}}" container
    println, print val on newline   : docker inspect --format='{{range .NetworkSettings.Networks}}{{println .IPAddress}}{{end}}' container
    hint, find out what content     : docker container ls --format='{{json .}}'

- Regisistry ------------------------
  * login                           : Log in to a Docker registry
                                      docker login [OPTIONS] [SERVER]
  * logout                          : Log out from a Docker registry
                                      docker logout [SERVER]
  * search                          : Search the Docker Hub for images
                                      docker search [OPTIONS] TERM

- System ----------------------------
  * df                              : Show docker disk usage
                                      docker system df [OPTIONS]
  * events                          : Get real time events from the server
                                      docker system events [OPTIONS]
  * info                            : Display system-wide information
                                      docker system info [OPTIONS]
  * prune                           : Remove unused data
                                      docker system prune [OPTIONS]
    Prune without prompt            : docker system prune --force

- Engine ----------------------------
  * activate                        : Activate Enterprise Edition
                                      docker engine activate [OPTIONS]
  * check                           : Check for available engine updates
                                      docker engine check [OPTIONS]
  * update                          : Update a local engine
                                      docker engine update [OPTIONS]

- Images ----------------------------
  * build                           : Build an image from a Dockerfile
                                      docker build [OPTIONS] PATH | URL | -
    Dockerfile in curdir            : docker build .
    Specifying a Dockerfile
    and context directory           : docker build -f Dockerfile.debug context/dir1/dir2
  * commit                          : Create a new image from a container's changes
                                      docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]
    Generic                         : docker commit --change "ENV DEBUG true" c3f279d17e0a  svendowideit/testimage:version3
  * history                         : Show the history of an image
                                      docker image history [OPTIONS] IMAGE
  * import                          : Import the contents from a tarball to create a filesystem image
                                      docker image import [OPTIONS] file|URL|- [REPOSITORY[:TAG]]
  * inspect                         : Display detailed information on one or more images
                                      docker image inspect [OPTIONS] IMAGE [IMAGE...]
  * load                            : Load an image from a tar archive or STDIN
                                      docker image load [OPTIONS]
  * ls                              : List images
                                      docker image ls [OPTIONS] [REPOSITORY[:TAG]]
    intermediate                    : docker image ls --all
  * prune                           : Remove unused images
                                      docker image prune [OPTIONS]
    Prune all unused by containers  : docker image prune -a
    Prune without prompt            : docker image prune -f
    Prune with filter               : docker image prune --filter "until=24h"

  * pull                            : Pull an image or a repository from a registry
                                      docker image pull [OPTIONS] NAME[:TAG|@DIGEST]
  * push                            : Push an image or a repository to a registry
                                      docker image push [OPTIONS] NAME[:TAG]
  * rm                              : Remove one or more images
                                      docker image rm [OPTIONS] IMAGE [IMAGE...]
  * save                            : Save one or more images to a tar archive (streamed to STDOUT by default)
                                      docker image save [OPTIONS] IMAGE [IMAGE...]
  * tag                             : Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
                                      docker image tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
    image reference by name         : docker tag <image> username/repository:tag


- Containers ------------------------
  * attach                          : Attach local standard input, output, and error streams to a
                                      running container
                                      docker container attach [OPTIONS] CONTAINER
  * commit                          : Create a new image from a container's changes
                                      docker container commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]
  * copy                            : Copy files/folders between a container and the local fs
                                      docker container cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|-
                                      docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH
  * create                          : Create a new container
                                      docker container create [OPTIONS] IMAGE [COMMAND] [ARG...]
  * diff                            : Inspect changes to files or directories on a container’s filesystem
                                      docker container diff CONTAINER
  * exec                            : Run a command in a running container
                                      docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]
    Connect to terminal             : docker exec -it my-cont /bin/bash
  * export                          : Export a container's filesystem as a tar archive
                                      docker container export [OPTIONS] CONTAINER
  * inspect                         : Display detailed information on one or more containers
                                      docker container inspect [OPTIONS] CONTAINER [CONTAINER...]
    Name and volumes                : docker inspect \
                                      --format="{{println}}{{.Name}}{{range .Mounts}}{{println}}{{json .}}{{end}}" \
                                      $(docker ps -q) > /tmp/out2
    Name and network settings       : docker inspect \
                                      --format="{{println}}{{.Name}}{{println}}{{json .NetworkSettings}}" \
                                      $(docker ps -q)
    View State                      : sudo docker inspect --format='{{.State.Running}}' daemon_dave
  * kill                            : Kill one or more running containers
                                      docker container kill [OPTIONS] CONTAINER [CONTAINER...]
  * logs                            : Fetch the
                                      docker container logs [OPTIONS] CONTAINER
  * ls                              : List containrs
                                      docker container ls [OPTIONS]
    running                         : docker container ls
    all                             : docker container ls --all
  * pause                           : Pause all processes within one or more containers
                                      docker container pause CONTAINER [CONTAINER...]
  * port                            : List port mappings or a specific mapping for the container
                                      docker container port CONTAINER [PRIVATE_PORT[/PROTO]]
  * prune                           : Remove all stopped containers
                                      docker container prune [OPTIONS]
  * rename                          : Rename a container
                                      docker container rename CONTAINER NEW_NAME
  * restart                         : Restart one or more containers
                                      docker container restart [OPTIONS] CONTAINER [CONTAINER...]
  * rm                              : Remove one or more containers
                                      docker container rm [OPTIONS] CONTAINER [CONTAINER...]
    Normal                          : docker rm my-cont
    Force (uses SIGKILL)            : docker rm --force my-cont
    All                             : docker rm -f $(docker ps -aq)
  * run                             : Run a command in a new container
                                      docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]
    container                       : docker run --name my-cont username/repository:tag
    bash container                  : docker run -i -t --name my-cont ubuntu /bin/bash
    in background                   : docker run --detach --name my-cont username/repository:tag
    mapping host port 5000 to
    container port 80               : docker run --name my-cont --publish 5000:80 username/repository:tag
    override entrypoint             : docker run username/repository:tag --entrypoint "/bin/bash"
    volume mapping                  : docker run -d -v /tmp/host:/tmp/container:rw username/repository:tag cmd
  * start                           : Start one or more stopped containers
                                      docker container start [OPTIONS] CONTAINER [CONTAINER...]
  * stats                           : Display a live stream of container(s) resource usage statistics
                                      docker container stats [OPTIONS] [CONTAINER...]
  * stop                            : Stop one or more running containers
                                      docker container stop [OPTIONS] CONTAINER [CONTAINER...]
  * top                             : Display the running processes of a container
                                      docker container top CONTAINER [ps OPTIONS]
  * unpause                         : Unpause all processes within one or more containers
                                      docker container unpause CONTAINER [CONTAINER...]
  * update                          : Update configuration of one or more containers
                                      docker container update [OPTIONS] CONTAINER [CONTAINER...]
  * wait                            : Block until one or more containers stop, then print their exit codes
                                      docker container wait CONTAINER [CONTAINER...]

- Networks --------------------------
  * connect                         : Connect a container to a network
                                      docker network connect [OPTIONS] NETWORK CONTAINER
  * create                          : Create a network
                                      docker network connect [OPTIONS] NETWORK CONTAINER
    connect on container create     : docker create --name my-nginx --network my-net --publish 8080:80 nginx:latest
  * disconnect                      : Disconnect a container from a network
                                      docker network disconnect [OPTIONS] NETWORK CONTAINER
  * inspect                         : Display detailed information on one or more networks
                                      docker network inspect [OPTIONS] NETWORK [NETWORK...]
  * ls                              : List networks
                                      docker network ls [OPTIONS]
  * prune                           : Remove all unused networks
                                      docker network prune [OPTIONS]
  * rm                              : Remove one or more networks
                                      docker network rm NETWORK [NETWORK...]

- Volumes ---------------------------
  * create                          : Create a volume
                                      docker volume create [OPTIONS] [VOLUME]
  * inspect                         : Display detailed information on one or more volumes
                                      docker volume inspect [OPTIONS] VOLUME [VOLUME...]
  * ls                              : List volumes
                                      docker volume ls [OPTIONS]
  * prune                           : Remove all unused local volumes
                                      docker volume prune [OPTIONS]
  * rm                              : Remove one or more volumes
                                      docker volume rm [OPTIONS] VOLUME [VOLUME...]

- Volumes and Containers ------------
  * Start Container
    -v Arguments                    : <hostpath>,<contpath>,options
    --mount Arguments               : type=bind,source=<hostpath>,dst=<containerpath>,
                                      [readonly],bind-propagation=<>,consistency=<>
    with --mount                    : docker run -d --name devtest --mount source=myvol2,target=/app nginx:latest
    with -v                         : docker run -d --name devtest -v myvol2:/app nginx:latest
  * Stop Container                  : docker container stop devtest
  * Remove Contaier Note: volume has
    not been deleted                : docker container rm devtest
  * Start service                   : docker service create -d --replicas=4 --name devtest-service \
                                        --mount source=myvol2,target=/app nginx:latest

- Bind Mounts and Containers --------
  * Run Container
    -v Arguments                    : <hostpath>,<contpath>,options
    --mount Arguments               : type=bind,source=<hostpath>,dst=<containerpath>,
                                      [readonly],bind-propagation=<>,consistency=<>
    Regular Mount                   : docker run -d -it --name devtest \
                                                 --mount type=bind,source="$(pwd)"/target,target=/app nginx:latest
    Bind to non-empty container
    dir, existing contents obscured : docker run -d -it --name devtest \
                                       --mount type=bind,source=/tmp,target=/existingpath nginx:latest
    Read only bind mount            : docker run -d -it --name devtest \
                                       --mount type=bind,source="$(pwd)"/target,target=/app,readonly nginx:latest
  * Stop Container                  : docker container stop devtest
  * Remove Contaier                 : docker container rm devtest

- Plugins ---------------------------
  * create                          : Create a plugin from a rootfs and configuration.
                                      Plugin data directory must contain config.json and rootfs directory.
                                      docker plugin create [OPTIONS] PLUGIN PLUGIN-DATA-DIR
  * disable                         : Disable a plugin
                                      docker plugin disable [OPTIONS] PLUGIN
  * enable                          : Enable a plugin
                                      docker plugin enable [OPTIONS] PLUGIN
  * inspect                         : Display detailed information on one or more plugins
                                      docker plugin inspect [OPTIONS] PLUGIN [PLUGIN...]
  * install                         : Install a plugin
                                      docker plugin install [OPTIONS] PLUGIN [KEY=VALUE...]
  * ls                              : List plugins
                                      docker plugin ls [OPTIONS]
  * push                            : Push a plugin to a registry
                                      docker plugin push [OPTIONS] PLUGIN[:TAG]
  * rm                              : Remove one or more plugins
                                      docker plugin rm [OPTIONS] PLUGIN [PLUGIN...]
  * set                             : Change settings for a plugin
                                      docker plugin set PLUGIN KEY=VALUE [KEY=VALUE...]
  * upgrade                         : Upgrade an existing plugin
                                      docker plugin upgrade [OPTIONS] PLUGIN [REMOTE]

- Daemon ----------------------------
  * Help                            : dockerd --help
  * Stop                            : Ctrl+C
  * Start
    With binary                     : dockerd
    With binary, debugging          : dockerd -D
    As service                      : service docker start
  * Daemon status                   : sudo service docker status
  * Stop daemon                     : service docker stop
