OVERVIEW
===============================================================================
NPM Technology Documentation
- Description
  * NPM is a package manager for the JavaScript programming language. It is the
    default package manager for the JavaScript runtime environment Node.js.
  * It consists of a command line client, also called npm, and an online database
    of public and paid-for private packages, called the npm registry. The registry
    is accessed via the client, and the available packages can be browsed and
    searched via the npm website. The package manager and the registry are
    managed by npm, Inc.
- Components
  * Website                         : Discover pacages, set up profies, manage experience
  * Command Line Interface (CLI)    : Runs from terminal. Developer interaction
  * Registry                        : Large public database of JavaScript software and metadata
- Uses Cases
  * Adapt packages of code to apps
  * Download standalone tools
  * Share code
  * Restrict code to specific developers
  * Form organizations (Orgs) to coordinate package maintenance, coding, and developers
  * Manage multiple version of code and code dependencies
  * Update applications easily
- Relevant URLS
  * NPM Home                        : https://www.npmjs.com/
  * NPM Documentation               : https://docs.npmjs.com/
  * Wikipedia                       : https://en.wikipedia.org/wiki/Npm_(software)
  * CLI man pages                   : https://docs.npmjs.com/cli/help


INSTALLATION
===============================================================================
- Install node and npm              : See scripts nodejs.sh
- Install node                      : sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
                                      sudo apt-get install -y nodejs
- Node Version Manager (nvm)        : Used to reconcile differences between node
                                      versions and npm version
- Install latest npm                : npm install npm@latest -g
- Login to npm                      : npm login
- Experiment with next release      : npm install npm@next -g


CONFIGURATION
===============================================================================
- Location                          : NPM gets its config from the following sources, sorted by priority
  1 Command Line Flags              : --foo bar
  2 Environment Variables           : export npm_config_foo=bar
                                      Sets npm config variable foo = bar
  3 npmrc files
    Per Project                     : /path/to/my/project/.npmrc
    Per User                        : $HOME/.npmrc or $NPM_CONFIG_USERCONFIG
    Global                          : $PREFIX/etc/npmrc
    Built In                        : /path/to/npm/npmrc
- Set config key                    : npm config set key value
                                      npm config set init-author-name "James Bensson"
                                      npm config set init-author-email "bensson.james@gmail.com"
- Set config license                : npm config set init-license "Beblsoft LLC"
- Delete config key                 : npm config delete key
- Edit Configuration                : npm config edit
- Show config json                  : npm config list --json
- Per Package Config Settings       : package.json
                                      {
                                        "name" : "foo"
                                        "config" : { "port" : "8080" },
                                        "scripts" : { "start" : "node server.js" },
                                      }
                                      server.js
                                      http.createServer(...).listen(process.env.npm_package_config_port)
                                      Changed by user command
                                      npm config set foo:port 80


PACKAGES AND MODULES
===============================================================================
- Definitions
  * Package                         : A file or directory described by a package.json
                                      a) A folder containing a program described by a package.json
                                      b) A gzipped tarball containing (a)
                                      c) A url that resolves to (b)
                                      d) A <name>@<version> that is published on the registry with (c)
                                      e) A <name>@<tag> that points to (d)
                                      f) A <name> that has a latest tag satisfying (e)
                                      g) A git url that, when cloned, results in (a)
  * Module                          : A module is anything that can be loaded with require()
                                      in a Node.js program
                                      - A folder with a package.json file containing a main field
                                      - A folder with an index.js file in it
                                      - A JavaScript file
- Relationship                      : Most npm packages are modules. They are used in
                                      Node.js programs and loaded with require()
                                      However, this is not a requirement.
- NPM Sorting Analyzer Rankings
  * Popularity                      : How many times package has been downloaded
  * Quality                         : Includes considerations such as presence
                                      of readme, stability, tests, up-to-date dependencies,
                                      website, code complexity
  * Maintenance                     : Ranks by attention given by developers
  * Optimal                         : Combines other three categories in meaninful ways
- Packagages contain
  * README                          : Explains purpose of package
  * Dependencies                    : Packages incorporated by this package
  * Dependents                      : Packages that incorporate this package
  * Versions                        : List of package versions


GLOBAL PACKAGES
===============================================================================
- Packages: install local or
  global?                           : Is package a CLI tool? If yes, install globally
                                    : Does my package depend on package? If yes, install locally
- Install global package            : npm install -g vue-cli
- Update global package             : npm update -g vue-cli
- Update all global packages        : npm update -g
- Find global outdated packages     : npm outdated -g --depth=0
- Uninstall global package          : npm uninstall -g vue-cli


LOCAL PACKAGES
===============================================================================
- Install package and add it to
  dependencies section              : npm install lodash --save-prod
- Install package and add it to
  dev dependencies section          : npm install lodash --save-dev
- Install a package with tag        : npm install <package>@<tag>
- Update local packages             : npm update
- Show outdated local packages      : npm outdated
                                      Should show no results
- Uninstall local package           : npm uninstall lodash
- Uninstall package and remove if
  from the dependencies section     : npm uninstall lodash --save
- Linking local packages (1)        : Allows local packages to leverage each other
                                      cd ~/projects/node-redis    # go into the package directory
                                      npm link                    # creates global link
                                      cd ~/projects/node-bloggy   # go into some other package directory.
                                      npm link redis              # link-install the package
- Linking local packages (2)        : cd ~/projects/node-bloggy   # go into the dir of your main project
                                      npm link ../node-redis      # link the dir of your dependency


PACKAGE.JSON
===============================================================================
- Description                       : Each npm package contains a package.json to define its metadata
                                    : Lists the packages on which the current package depends
                                    : Leverages semantic versioning
                                    : Makes build reproducible and easy to share with other developers
- Required Fields
  * name                            : name of package
  * version                         : X.X.X follows semantic versioning
- Optional Fields
  * description                     : Describe package
  * main                            : The main field is a module ID that is the primary entry point
                                      to your program. That is, if your package is named foo, and a user
                                      installs it, and then does require("foo"), then your main module's
                                      exports object will be returned.
  * scripts                         : scripts to run
    ~ prepublish, prepare,
      prepublishOnly, prepack,
      postpack, publish             : Packaging commands
    ~ preinstall, install,
      postinstall                   : Install commands
    ~ preuninstall, postuninstall   : Uninstall commands
    ~ preversion, version,
      postversion                   : Version commands
    ~ pretest, test, posttest       : Run by npm test command
    ~ prestop, stop, poststop       : Run by npm stop command
    ~ prestart, start, poststart    : Run by npm start command
    ~ prerestart, restart,
      postrestart                   : Run by npm restart command
    ~ preshrinkwrap, shrinkwrap,
      postshrinkwrap                : Run by npm shrinkwrap command
  * keywords                        : Keywords for seaching for package
  * author                          : Ex. James Bensson <bensson.james@gmail.com>
  * license                         : Package license ex. "MIT"
  * bugs                            : Url to project's issue tracker ex. "https://github.com/owner/project/issues"
  * homepage                        : Homepage url ex. "https://github.com/owner/project#readme"
  * dependencies                    : Packages required for production
                                      { "dependencies" :
                                        { "foo" : "1.0.0 - 2.9999.9999"
                                        , "bar" : ">=1.0.2 <2.1.2"
                                        , "baz" : ">1.0.2 <=2.3.4"
                                        , "boo" : "2.0.1"
                                        , "qux" : "<1.0.0 || >=2.3.1 <2.4.5 || >=2.5.2 <3.0.0"
                                        , "asd" : "http://asdf.com/asdf.tar.gz"
                                        , "til" : "~1.2"          // Approximately
                                        , "elf" : "~1.2.3"        // Compatible
                                        , "two" : "2.x"           // x is wildcard
                                        , "thr" : "3.3.x"         // x is widcard
                                        , "lat" : "latest"        // Latest version
                                        , "dyl" : "file:../dyl"   // File
                                        }
                                      }
  * devDependencies                 : Packages required for development and testing
  * peerDependencies                : In some cases, you want to express the compatibility of your package
  * bundledDependencies             : Packages bundled when publishing
  * optionalDependencies            : Optional dependencies

                                      with a host tool or library.
  * files                           : Opposite of .gitigore. Files to include in package
                                      Works in tandem with .npmignore
  * bin                             : Binaries to add to npm path
                                      ./node_modules/.bin
                                      Files in bin should start with #!/usr/bin/env node
  * man                             : Single file or array of files for man pages
  * repository                      : Specify where code lives
                                      { "type" : "git",
                                        "url" : "https://github.com/npm/cli.git" }
  * engine                          : Specify node version. { "engines" : { "node" : ">=0.10.3 <0.12" } }
  * cpu                             : OS module will run on "os" : [ "darwin", "linux" ]
  * private                         : If true, npm will not publish
  * publishConfig                   : Set of config values that will be used at publish-time
- Create package.json               : npm init
                                      prompts questionaire
- Create default package.json       : npm init --yes
                                      Will generate based on information from current directory
- Set init command default field    : npm set init.author.email "bensson.james@gmail.com"



USER MANAGEMENT
===============================================================================
- Create user account               : npm adduser
- Login to accout                   : npm login
- See what account is logged in     : npm whoami


PUBLISHING, UPDATING PACKAGES
===============================================================================
- Create user account               : See above
- Review package contents           : Everything in directory will be included
                                      unless ignored by local .gitignore or
                                      .npmignore files
- Package names must be unique      : Not already used by someone else
                                      Meets npm policy guidelines (isn't offensive)
- Include README.md file            : Description in markdown
- Publish package                   : npm publish
                                      npm adds @latest tag
- Updating a package as simple as
  1 Update code
  2 Update package version          : npm version X.Y.Z
  3 Publish package again           : npm publish
- Update README.md only (no code)
  1 Change README.md
  2 Create patch version            : npm version patch
  3 Publish                         : npm publish
- Add distribution tag              : npm dist-tag add <package>@<version> [<tag>]
                                      Avoid tag names that begin with "v" to not conflict with semver
- Publish with specific tag         : npm publish --tag beta


COMMAND SUMMARY
===============================================================================
- access                            : Set access level on published packages
- adduser                           : Add a registry user account
- audit                             : Run a security audit
- bin                               : Display npm bin folder
- bugs                              : Bugs for a package in a web browser maybe
- build                             : Build a package
- bundle                            : REMOVED
- cache                             : Manipulates packages cache
- ci                                : Install a project with a clean slate
- completion                        : Tab Completion for npm
- config                            : Manage the npm configuration files
- dedupe                            : Reduce duplication
- deprecate                         : Deprecate a version of a package
- distag                            : Modify package distribution tags
- docs                              : Docs for a package in a web browser maybe
- doctor                            : Check your environments
- edit                              : Edit an installed package
- explore                           : Browse an installed package
- help                              : Get help on npm
- helpearch                         : Search npm help documentation
- hook                              : Manage registry hooks
- init                              : create a package.json file
- install                           : Install a package
- link                              : Symlink a package folder
- logout                            : Log out of the registry
- ls                                : List installed packages
- npm                               : javascript package manager
- outdated                          : Check for outdated packages
- owner                             : Manage package owners
- pack                              : Create a tarball from a package
- ping                              : Ping npm registry
- prefix                            : Display prefix
- profile                           : Change settings on your registry profile
- prune                             : Remove extraneous packages
- publish                           : Publish a package
- rebuild                           : Rebuild a package
- repo                              : Open package repository page in the browser
- restart                           : Restart a package
- root                              : Display npm root
- runcript                          : Run arbitrary package scripts
- search                            : Search for packages
- shrinkwrap                        : Lock down dependency versions for publication
- star                              : Mark your favorite packages
- stars                             : View packages marked as favorites
- start                             : Start a package
- stop                              : Stop a package
- team                              : Manage organization teams and team memberships
- test                              : Test a package
- token                             : Manage your authentication tokens
- uninstall                         : Remove a package
- unpublish                         : Remove a package from the registry
- update                            : Update a package
- version                           : Bump a package version
- view                              : View registry info
- whoami                            : Display npm username


SEMANTIC VERSIONING
===============================================================================
- Description
  * Semantic versioning is a standard that communicates the extent of changes in
    a new release of code
- For publishers
  * First release                   : New Product
                                      Ex. 1.0.0
  * Bug fixes, minor changes        : Patch release, increment third digit
                                      Ex. 1.0.1
  * New features that don't break
    existing features               : Minor release, increment middle digit
                                      Ex. 1.1.1
  * Changes that break backward
    compatibility                   : Major release, increment first digit
                                      Ex. 2.0.0
- For consumers
  * Patch releases                  : 1.0, 1.0.x, ~1.0.4
  * Minor releases                  : 1, 1.x, or ^1.0.4
  * Major releases                  : * or x


- Check config                      : npm config ls
- Publish with a tag                : npm publish --tag <tag-name>
- Run command inside
  node_modules/.bin                 : npx <command>
