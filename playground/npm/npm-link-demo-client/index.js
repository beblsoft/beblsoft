/**
 * @file Example usage of linked module
 */


/* ------------------------ IMPORTS ---------------------------------------- */
logHelloWorld = require('npm-link-demo')


/* ------------------------ MAIN ------------------------------------------- */
logHelloWorld();