OVERVIEW
===============================================================================
NPM Projects Documentation


PROJECT: NPM LINK DEMO
===============================================================================
- Description                     : Example of how to link an npm package
                                    This makes it accessible to other packages on the computer
- Directory                       : ./npm-link-demo
- To Link Globaly                 : cd ./npm-link-demo
                                    sudo npm link
                                    Creates link from global to local
                                    /usr/local/lib/node_modules/npm-link-demo ->
                                    /home/jbensson/git/beblsoft/playground/npm/npm-link-demo

PROJECT: NPM LINK DEMO CLIENT
===============================================================================
- Description                     : Example client who leverages npm-link-demo package functionality
- Directory                       : ./npm-link-demo-client
- To Link npm-link-demo           : cd ./npm-link-demo-client
                                    npm link npm-link-demo
                                    Creates the followgin symlinks:
                                    ~/git/beblsoft/playground/npm/npm-link-demo-client/node_modules/npm-link-demo ->
                                    /usr/local/lib/node_modules/npm-link-demo ->
                                    ~/git/beblsoft/playground/npm/npm-link-demo
- To Run                          : node index.js
