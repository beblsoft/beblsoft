/**
 * @file Example module to be linked
 */


/* ------------------------ FUNCTIONS -------------------------------------- */
/**
 * Simple function to log Hello World!
 */
function logHelloWorld() {
	console.log('Hello World!');
}


/* ------------------------ EXPORTS ---------------------------------------- */
module.exports = logHelloWorld;
