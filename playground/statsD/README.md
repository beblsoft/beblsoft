# StatsD Documentation

StatsD was originally a simple daemon developed and released by Etsy to aggregate and summarize
application metrics.

With StatsD, applications are to be instrumented by developers using language-specific client libraries.
These libraries then communicated with the StatsD daemon using it dead-simple protocol, and the daemon aggregates
the metrics and relays them to various graphing and monitoring backends

Relevant URLS:
[Github Wiki](https://github.com/statsd/statsd/wiki),
[Datadog Blog](https://www.datadoghq.com/blog/statsd/),
[Protocol Spec](https://github.com/b/statsd_spec)

## How it works

1. __Application code__: Instrument it with one of the many StatsD libraries corresponding to app language.
  StatsD allows one to capture different types of metrics depending on needs: Gauges, Counters,
  Timing Summary Statistics, and Sets Simple one-liners in code

2. __StatsD client library__: Sends each individual call to the StatsD server over a UDP datagram.
  Since UDP is a disconnected protocol, the sender doesn't block when submitting data as it would with TCP

3. __StatsD daemon__: Listens to the UDP traffic from all application libraries, aggregate data over
  time and flush it at the desired interval to the backend of choice

4. __Monitoring backend__: Turns metrics from a stream of numbers on the wire into usable charts and
  alert you when needed Ex. Graphite, Datadog, Prometheus

## Differentiation

- __Simplicity__: Easy to instrument inside app. Protocol is text-based and straightforward to write and read

- __Decouple application from instrumentation__:
  No upstream dependency between metrics collection and the app itself.
  StatsD can't crash your app and doesn't need to be written in the same language or even run on the same machine.

- __Tiny Footprint__:
  StatsD clients are thin, carry no state, need no threads and add negligible
  overhead. StatsD supports sampling calls to arbirarily reduce network utilization

- __Ubiquity and ecosystem__:
  Clients for Ruby, Python, Java, Erlang, Node, Scala, Go, Haskell and many other languages
  Multiple different servers. Open source and commercial. No vendor lock-in.

## Metric Types and Formats

The format of exported metrics is UTF-8 text, with metrics separated by newlines. Metrics are generally
of the form `<metric name>:<value>|<type>`, with exceptions noted in the metric type definitions below.

The protocol allows for both integer and floating point values. Most implementations store values
internally as IEEE 754 double precision float, but many implementations are graphing systems only support
integer values. For compatibility all values should be integers in the range (-2^53, 2^53)

### Guages

A guage is an instantaneous measurement of a value, like the gas guage in a car. It differs from a counter
by being calculated at the client rather than at the server. Valid guage values are in the range
[0, 2^64)

```
<metric name>:<value>|g
```

### Counters

A counter is a guage calculated at the server. Metrics sent by the client increment or decrement the
value of the guage rather than giving its current value. Counters may also have an associated sample
rat, given as a decimal of the number of samples per event count. For example, a sample rate of 1/10
should be exported as 0.1. Valid counter values are in the range (-2^63, 2^63).

```
<metric name>:<value>|c[|@<sample rate>]
```

### Timers

A timer is a measure of the number of milliseconds elapsed between a start and end time, for example
the time to complete rendering of a web page for a user. Valid timer values are in the range
[0, 2^64).

```
<metric name>:<value>|ms
```

### Histogams

A histogram is a measure of the distribution of timer values over time, calculated at the server. As
the data exported for timers and histograms is the same, this is currently an alias for a timer. Valid
histogram values are in the range [0, 2^64).

```
<metric name>:<value>|h
```

### Meters

A meter measures the rate of events over time, calculated at the server. They may also be thought of as
increment-only counters. Valid meter values are in the range [0, 2^64^).

```
<metric name>:<value>|m
```

