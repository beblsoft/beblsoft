#!/usr/bin/perl
#
# File
#  userInput.pl
#
# Description
#  Perl Script to Demonstrate IO Capabilities
#

# --------------------------- MAIN ------------------------------------------ #
while (1) {
	print("Type something: ");
	$a = <STDIN>; # read the next line
	print("You typed     : " , $a);
}
