#!/usr/bin/perl
#
# File
#  bash.pl
#
# Description
#  Perl Script To Demonstrate Hashes
#

# --------------------------- MAIN ------------------------------------------ #
%hash;
$hash{"aaa"} = "bbb"; # creates key "aaa", value "bbb"
$hash{234.5} = 456.7; # creates key "234.5", value 456.7
$hash{234.5} += 3;  # makes it 459.7
print("\nPrinting hash values...\n");
while ( ($k,$v) = each %hash ) {
    print "$k => $v\n";
}

print("\nPrinting reverse hash values...\n");
%backwardsHash = reverse %hash; #reverse key-value ordering
while ( ($k,$v) = each %backwardsHash ) {
    print "$k => $v\n";
}

#Access Keys
print("\nPrinting hash keys...\n");
@ks = keys(%hash);
print(@ks, "\n");


#Access Values
print("\nPrinting hash values...\n");
@vs = values(%hash);
print(@vs, "\n");

#Deletion
print("\nDeleting hash value...\n");
delete $hash{"aaa"};


