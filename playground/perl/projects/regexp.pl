#!/usr/bin/perl
#
# File
#  regexp.pl
#
# Description
#  Demonstrating Regular Expressions
#

# --------------------------- MAIN ------------------------------------------ #
# [0123456789]    # match any single digit
# [0-9]           # same thing
# [0-9\-]         # match 0-9, or minus
# [a-z0-9]        # match any single lowercase letter or digit
# [a-zA-Z0-9_]    # match any single letter, digit, or underscore
#
# There's also a negated character class, which is the same as a character class, but has a
# leading  up-arrow (or caret: ^) immediately after the left bracket. This character class
# matches any single character that is not in the list. For example:
# [^0-9]        # match any single non-digit
# [^aeiouAEIOU] # match any single non-vowel
# [^\^]         # match single character except an up-arrow
#
# abc*             # matches ab, abc, abcc, abccc, abcccc, and so on
# (abc)*           # matches "", abc, abcabc, abcabcabc, and so on
# ^x|y             # matches x at the beginning of line, or y anywhere
# ^(x|y)           # matches either x or y at the beginning of a line
# a|bc|d           # a, or bc, or d
# (a|b)(c|d)       # ac, ad, bc, or bd
# (song|blue)bird  # songbird or bluebird

#Test $_ against regular expression
$_ = "abc";
if(/abc/) {
    print $_ . "\n";
}

#Any number of b's: *
$_ = "abbbbbbbbbc";
if(/ab*c/) {
    print $_ . "\n";
}

#Any character:.
$_ = "abdc";
if(/ab.c/) {
    print $_ . "\n";
}

#Any character in set:[]
$_ = "c";
if(/[abc]/) {
    print $_ . "\n";
}

#Match fewest number of x's:*?
$_ = "a xxx c xxxxxxxx c xxx d";
/a.*?c.*d/;

#Repeat Symbols: ()
$_= fredxbarneyx;
if(/fred(.)barney\1/){
    print $_ . "\n";
}

$_=axbycydx;
if(/a(.)b(.)c\2d\1/){
    print $_ . "\n";
}

#For strings not in $_: =~
$a = "hello world";
$a =~ /^he/;         # true
$a =~ /(.)\l/;       # also true (matches the double l)
if ($a =~ /(.)\l/) { # true, so yes...
   print $a . "\n"; # some stuff
}

#Case insensitive: /i
$b="Yoga";
if ($b =~ /^y/i) {
  print $b . "\n";
}

#Delimiters:\ or m infront of nonalphanumeric
#  /^\/usr\/etc/     # using standard slash delimiter
#  m@^/usr/etc@      # using @ for a delimiter
#  m#^/usr/etc#      # using # for a delimiter (my favorite)

$what = "bird";
$sentence = "Every good bird does fly.";
if ($sentence =~ /\b$what\b/) {
    print "The sentence contains the word $what!\n";
}

# $1, $2, ...
$_ = "this is a test";
/(\w+)\W+(\w+)/; # match first two words
                 # $1 is now "this" and $2 is now "is"
print $1 . "\n";
print $2 . "\n";

# $`,$&, $'
$_ = "this is a sample string";
/sa.*le/; # matches "sample" within the string
          # $` is now "this is a "
          # $& is now "sample"
          # $' is now " string"

#Substitution
$_ = "hello, world";
$new = "goodbye";
s/hello/$new/; # replaces hello with goodbye
print $_ . "\n";

#Split Function
$line = "merlyn::118:10:Randal:/home/merlyn:/usr/bin/perl";
@fields = split(/:/,$line); # split $line, using : as delimiter
print "@fields\n";

#Join Function
$outline = join(":", @fields); #Append ":" at end of each element in fields
print $outline . "\n";
