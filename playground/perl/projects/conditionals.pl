#!/usr/bin/perl
#
# File
#  conditionals.pl
#
# Description
#  Perl Conditionals
#

# --------------------------- MAIN ------------------------------------------ #
$age = 19;

if($age < 18) {
    print "So, you're not old enough to vote, eh?\n";
} else {
    print "Old enough, so go vote!\n";
}