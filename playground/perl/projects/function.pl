#!/usr/bin/perl
#
# File
#  function.pl
#
# Description
#  Perl script to demonstrate function capabilities
#

# --------------------------- USE ------------------------------------------- #
# If you place this pragma "strict" at the beginning of your file, you will no longer be able to use
# variables (scalars, arrays, and hashes) until you have first "declared" them.
use strict;


# --------------------------- HELPER FUNCTIONS ------------------------------ #
# Print Hello
sub say_hello {
    print "hello, world!\n";
}

# Return value
sub sum_of_a_and_b {
    my $a = shift;
    my $b = shift;
    return $a + $b;
}

# Return list
sub list_of_a_and_b {
    my $a = shift;
    my $b = shift;
    return($a,$b);
}

# Arguments referenced through $_[<arg num>]
sub say_hello_to {
    print "hello, $_[0]!\n"; # first parameter is target
}

# @_ is list of all vars
sub add {
    my ($sum);            # make sum local
    $sum = 0;             # initialize the sum
    foreach $_ (@_) {
        $sum += $_;       # add each element
    }
    return  $sum;         # last expression evaluated: sum of all elements
}

# --------------------------- MAIN ------------------------------------------ #
say_hello();

my $c = sum_of_a_and_b(3, 4);     # $c gets 7
my $d = 3 * sum_of_a_and_b(3, 4); # $d gets 21
print("$c $d \n");

my @c = list_of_a_and_b(3, 4); # @c gets (5,6)
print("@c\n");

say_hello_to("Jon");

my $a = add(4,5,6);      # adds 4+5+6 = 15, and assigns to $a
print("$a \n");