#!/usr/bin/perl
#
# File
#  list.pl
#
# Description
#  Perl Script which examines lists and arrays
#

# --------------------------- MAIN ------------------------------------------ #
#Useful List Functions:
# push, pop, shift, unshift, reverse, sort, chomp

#Defining and Manipulating Arrays
@a = (1 .. 5);     # same as (1,2,3,4,5)
@b = (1.2 .. 5.2); # same as (1.2, 2.2, 3.2 .. 5.2)
@c = (1.3 .. 6.1); # same as (1.3, .. 5.3)

#Note: $names and @names are in different namespaces
@names = qw(james john joe jimmy);
print("\nThe names are: ", @names, "\n");

@huh = 1; #1 is promoted to list automatically

@d = qw(one two);
@d = (@d, three);
@d = (zero, @d);
print("\nD list: ", @d, "\n");


#Note: a list cannot contain another list as an element
($a,$b,$c) = (1,2,3);    # give 1 to $a, 2 to $b, 3 to $c
($a,$b) = ($b,$a);       # swap $a and $b
($d,@lst) = ($a,$b,$c);  # give $a to $d, and ($b,$c) to @lst
($e,@lst) = @lst;        # remove first element of @lst to $e
                         # this makes @lst = ($c) and $e = $b

@lst = (1.. 10);
$length = @lst;       #length of @lst
($first) = @lst;      #first element of @lst
$und = $lst[20];      #$und is undef
$last = $lst[-1];     #last element in @arr
$last_index = $#lst;  #last index in @arr
print("\nLength: $length\n");
print("\nFirst Element: $first\n");
print("\nLast Element: $last\n");
print("\nLast Index: $last_index\n");
print("\nUndefined Element: $und\n");

#Accessing Elements
print("\nAccessing elements... \n");
@arr = (1 .. 3);
print ($arr[0],"\n");
$arr[0] = 2;
print ($arr[0],"\n");
$arr[0]++;
print ($arr[0],"\n");


print("\nSwapping elements... \n");
@arr = (1 .. 3);
print(@arr, "\n");
@arr[0,1] = @arr[1,0];     #Swap elemnts
print(@arr,"\n");
@arr[0,1,2] = @arr[1,1,1]; #Make first three elements like the second
print(@arr, "\n");
@arr[1,2] = (3,4);         #Set indicies 1,2 to values 3,4
print(@arr, "\n");
