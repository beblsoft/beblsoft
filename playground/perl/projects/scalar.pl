#!/usr/bin/perl
#
# File
#  scalar.pl
#
# Description
#  Perl Script to Modify Scalar Data
#

# --------------------------- MAIN ------------------------------------------ #
#All math done using floats
printf("%.51f\n", 5.1 - 2.4);


#. operator concatenates strings
$str="hello" . " " . "world";
$str .= "...Appending to String!\n";
print ($str x 2) ;
print ((3+2) x 4 . "\n"); #"5555" not "20"


$a = 17;
$b = $a + 3;
$b *= 2;
$a = ++$b; #a and b are the same
$a = $b++; #a == b -1


#chop - shown below, chomp - only removes \n if it exists
$c="chomped";
print ($c . " ");
chop $c;
print ($c . " \n");

# String examples
$fred   = "pay"; $fredday = "wrong!";
print $barney = "It's $fredday \n";         # not payday, but "It's wrong!"
print $barney = "It's ${fred}day\n";        # now, $barney gets "It's payday"
print $barney2 = "It's $fred"."day\n";      # another way to do it
print $barney3 = "It's " . $fred . "day\n"; # and another way


