#!/usr/bin/perl
#
# File
#  loops.pl
#
# Description
#  Perl Loops
#

# --------------------------- USE ------------------------------------------- #
use strict;


# --------------------------- MAIN ------------------------------------------ #
# For loop
print("For loop counting to 10\n");
for (my $i = 1; $i <= 10; $i++) {
  print("$i \n")
}

# While loop
print("While loop breaking at 9\n");
my $counter = 0;
while(1){
    if(++$counter > 9){
        last;
    }
    print($counter,"\n");
}

# While Loop
print("While loop skipping 4\n");
my $counter = 0;
while(1){
    if(++$counter > 10){
        last;
    }
    if($counter == 4){
        next;
    }
    print($counter,"\n");
}
