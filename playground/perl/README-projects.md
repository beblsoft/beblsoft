# Perl Projects Documentation

All projects are found in the `./projects` directory.

## helloWorld.pl

Perl Hello World Program

To Run: `./helloWorld.pl`

## conditionals.pl

Demonstrate use of `if` and `else` statements.

To Run: `./conditionals.pl`

## loops.pl

Perl Loop Demonstration: `for` and `while`

To Run: `./loops.pl`

## scalar.pl

Modify scalar data: integers, floats, and strings.

To Run: `./scalar.pl`

## list.pl

Demonstrate list creation and management.

To Run: `./list.pl`

## hash.pl

Demonstrate using perl hashes: key value mappings.

To Run: `./hash.pl`

## regexp.pl

Demonstrating Perl regular expressions.

To Run: `./regexp.pl`

## function.pl

Demonstrate creating and calling functions.

To Run: `./function.pl`

## userInput.pl

Demonstrate reading user input.

To Run: `./userInput.pl`



