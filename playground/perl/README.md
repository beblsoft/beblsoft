# Perl Technology Documentation

Perl is a family of two high-level, general-purpose, interpreted, dynamic programming languages.
Perl usually refers to Perl 5, but it may also refer to its redesigned sister language, Perl 6.

Perl was originally developed by Larry Wall in 1987 as a general purpose Unix scripting language
to make report processing easier.

Relevant URLs:
[Wikipedia](https://en.wikipedia.org/wiki/Perl),
[Home](https://www.perl.org/),
[Docs](https://www.perl.org/docs.html)
