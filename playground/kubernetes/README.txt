OVERVIEW
===============================================================================
Kubernetes Technology Documentation
- Description                       : Kubernetes is an open source container orchestration engine for
                                      automating deployment, scaling, and management of containerized
                                      application
                                    : Google open-sourced the project in 2014.
                                      Hosted by Cloud Native Computing Foundation (CNCF)
                                    : The name Kubernetes originates from Greek, meaning helmsman or pilot
                                      K8s is an abbreviation derived from replacing the 8 leters "ubernete"
                                      with "8"
- Fatures ---------------------------
  * Container platform
  * Microservices platform
  * Portable Cloud platform
- Relevant URLS ---------------------
  * Home                            : https://kubernetes.io/
  * Documentation                   : https://kubernetes.io/docs/home/
  * Kubernetes vs Docker Swarm      : https://platform9.com/blog/kubernetes-docker-swarm-compared/



COMPONENT ARCHITECTURE
===============================================================================
- Master Components ----------------: Provide the cluster's control plane
                                      Make global decisions about the cluster (detecting and responding)
                                      Can be run on any machine in cluster; however, for simplicity set
                                      up scripts put all master components on same machine and run containers
                                      on a different machine
  * kupe-apierver                   : Exposes the Kubernetes API
                                      Front end for kubernetes control plane
  * etcd                            : Consistent and highly-available key value store used as kubernetes backing
                                      store for all cluster data
  * kube-scheduler                  : Component on master that watches newly created pods that have no node
                                      assigned, and selects a node for them to run on
                                      Factors taken into account: individual, collective resource requirements,
                                      hardware/software/policy constraints, affinity and anti-affinity
                                      specifications, data locality, inter-workload interference, and deadlines
  * kube-controller-manager         : Runs controllers
    Controllers include
    node controller                 : Responsible for noticing and responding when nodes go down
    replication controller          : Responsible for maintaining the correct number of pods for every
                                      replication controller
    endpoints controller            : Populates the endpoints object (that is, joins Services and Pods)
    service account, token ctrls    : Create default accounts and API access tokens for new namespaces
  * cloud-controller-manager        : Runs controllers that interact with underlying cloud providers

- Node Components ------------------: Run on every node, maintaining running pods and providing kubernetes
                                      runtime environment
  * kubelet                         : Agent that runs on each node in the cluster. Makes sure that containers
                                      are running in a pod
                                      kubelet takes a set of PodSpecs that are provided through various mechanisms
                                      and ensures that the containers described in PodSpecs are running
                                      and healthy
  * kube-proxy                      : Enables kubernetes service abstraction by maintaining network rules
                                      on the host and performing connection forwarding
  * container runtime               : Container runtime software that is responsible for running containers
                                      Kubernetes supports several runtimes: Docker, containerd, cri-o,
                                      rktlet, and any implementation of Kubernetes CRI (Container Runtime Interface)
- Picture ---------------------------
                                                       kubectl
                                                         |
                                                         |
                                          ---------------|---------------------
                                          | Master       v                    |
                                          | Node         |                    |
                                          |           API Server----------    |
                                          |          ^  ^   ^  ^         |    |
                                          |          |  |   |  |         v    |
                                          | Controller  |   |  Scheduler |    |
                                          | Manager     |   |            |    |
                                          |             |   |            |    |
                                          |             |   |           etcd  |
                                          --------------|---|------------------
                                                       /     \
                                                      /        -----------------------------------------
                                                     /                                                  \
                                       -------------|---------------------------         ---------------|-------------------------
                                       | Worker     v                          |         | Worker       v                        |
                                       | Node       |                          |         | Node         |                        |
                                       |         kubelet                       |         |           kubelet                     |
                                       |                                       |         |                                       |
                                       | ------------------------------------- |         | ------------------------------------- |
                                       | | --------------     -------------- | |         | | --------------     -------------- | |
                                       | | |Pod         |     |Pod         | | |         | | |Pod         |     |Pod         | | |
                                       | | |            |     |            | | |         | | |            |     |            | | |
                                       | | | Containers | ... | Containers | | |   ...   | | | Containers | ... | Containers | | |
                                       | | | o o o o    |     | o o o o    | | |         | | | o o o o    |     | o o o o    | | |
                                       | | | o o o o    |     | o o o o    | | |         | | | o o o o    |     | o o o o    | | |
                                       | | --------------     -------------- | |         | | --------------     -------------- | |
                                       | |Docker                             | |         | |Docker                             | |
                                       | ------------------------------------- |         | ------------------------------------- |
                                       |                                       |         |                                       |
                                       |           kub-proxy                   |         |           kub-proxy                   |
                                       ---------------|-------------------------         ---------------|-------------------------
                                                      ^                                                 ^
                                                      |                                                 |
                                                   Internet                                          Internet

- Addons ---------------------------: Addons are pods and services that implement cluster features
  * DNS                             : Kubernetes clusters have cluster DNS
                                      Serves DNS records for kubernetes services
  * Web UI                          : Dashboard is a general purpose, web-based UI for kubernetes clusters.
                                      It allows users to manage and troubleshoot applications running in
                                      the cluster
  * Container Resource Monitoring   : Records generic time-series metrics about containers in a central
                                      database, and provides a UI for browsing that data
  * Cluster-level logging           : Responsible for saving container logs to a central log store with
                                      search/browsing interface



NODES
===============================================================================
- Nodes ----------------------------: A node is a worker machine, previously known as a minion
                                      A node may be a VM or a physical machine, depending on the cluster
                                      Each node contains services necessary to run pods and is
                                      managed by the master components
  * Status
    Addresses                       : HostName, ExternalIP, InternalIP
    Conditions                      : OutOfDisk, Ready, MemoryPressure, PIDPressure, DiskPressure,
                                      NetworkUnavailable
    Capacity                        : CPU, memory, maximum number of pods that can be scheduled
    Info                            : General information on node: kernel, kubernetes version, docker version, OS Name, ...
  * Management                      : Node is not inherently created by kubernetes, created by cloud
                                      providers like Google Compute Engine
- Master-Node Communication ---------
  * Cluster to Master               : API server listens on HTTPS port 443
                                      Nodes provisioned with public root certificate for the cluster
  * Master to Cluster               : Two primary communication paths
    1 API Server to kubelet         : Fetching logs, attaching to running pods, providing port-forwarding
    2 API Server to nodes, pods,
      services



CONTAINERS
===============================================================================
- Images ---------------------------: You create your Docker image and push it to a registry before referring
                                      to it in a kubernetes pod
                                      image property of a container suppors the same syntax as the docker command
  * Updating images                 : Default pull policy is IfNotPresent, which skips pulling if it exists
  * Using a private registry        : Just need to provide correct credentials
- Container Environment Variables ---
  * Resources available
    filesystem                      : Combination of an image and one or more volumes
    container info                  : information about container
    cluster info                    : information about other cluster objects
- Lifecycle Hooks ------------------: Allows containers to be aware of events in their management lifecycle
                                      and run code implemented in a handler when a lifecycle hook is executed
  * PostStart                       : Executes immediately after the container is created
                                      No guarantee that the hook will execute before container ENTRYPOINT
  * PreStop                         : Called immediately before a container is terminated due to an API request